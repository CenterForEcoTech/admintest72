<?php
require_once('Header.php');
$IsHomePage = true;
$showMain = true;
?>
<body id="home-page">

<div id="wrapper">
	<div id="header_band" class="band">
		<div id="header_nav" class="container">
			<?php include_once("TopNav.php"); ?>
		</div>
	</div><!-- End of header_band -->
	<div id="banner_band" class="band">
		<div id="upper_stitch"></div>
	</div><!-- End of banner_band -->
	<div id="featured_section_band" class="band">
		<div class="featured_section container">
			<?php include("_home_featuredSection.php");?>
		</div>
	</div><!-- End of featured_section_band -->
	<?php include("_footer_band.php")?>
</div>
<?php
require_once('Footer.php');
?>