<?php
$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
if ($load_getHeader){
	$PageTitle = "Sign up as a Nonprofit Organization";
	$PageHeader1 = "Partner with Local Businesses for Charitable Promotions";
	$pageSubtitle = "It&rsquo;s easy, quick, and FREE to register.";

}
if ($load_getContent){
    popValidationMessage();
	?>
	<div id="sign-up-info" class="twelve columns">

		<p>When customers designate your nonprofit during <?php echo $SiteName;?> promotions, you automatically receive a percentage of sales!</p>
		<p>Enter your charity&rsquo;s name or EIN below to get started.</p>

        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.LoginForm = CETDASH.LoginForm || {};
            CETDASH.LoginForm.successHandler = function(data){
                location.href = '<?php echo $CurrentServer;?>Home?LoggedIn';
            } // end success handler
        </script>
	    <?php include_once('views/campaign/_nonprofitRegistration.php'); ?>
	</div>
<?php } // end load_getContent?>