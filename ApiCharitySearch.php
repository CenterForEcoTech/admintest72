<?php
include_once("Global_Variables.php");
include_once($repositoryApiFolder."ServiceAPI.php");
include_once($dbProviderFolder."MySqliCharitySearchProvider.php");

$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        $results->message = "GET is not supported at this time.";
        break;
    case "POST":
        if ($_POST['action'] == 'ein-lookup' || $_POST['action'] == 'ein-lookup-test'){
            include_once($siteRoot."_setupExternalDataConnection.php");
            $provider = new MySqliCharityLookupProvider($externalDataConn);
            $results = $provider->getPublicCharity($_POST['ein'], $_POST['action'] == 'ein-lookup-test');
        } else if ($_POST['action'] == 'org-lookup' && $_POST['orgId']){
            include_once($dbProviderFolder."OrgProvider.php");
            $provider = new OrgProvider($mysqli);
            $results = $provider->getByOrgID($_POST['orgId']);
        } else if ($_POST['flavor'] == 'boolean'){
            include_once($siteRoot."_setupSearchConnection.php");
            $provider = new MySqliFullTextBooleanCharitySearchProvider($searchConn);
            $criteria = new CharitySearchCriteria();
            $criteria->term = $_POST['term'];
            $criteria->state = StateNameAndAbbrConverter::nameToAbbr($_POST['state']);
            $criteria->isRegistered = filter_var($_POST['isRegistered'], FILTER_VALIDATE_BOOLEAN);
            $criteria->start = 0;
            $criteria->size = 10;
            $result = $provider->search($criteria);
            $results = $result;
        } else if ($_POST['flavor'] == 'sphinx-default'){
            include_once($siteRoot."_setupSearchConnection.php");
            $provider = new MySqliSphinxCharitySearchProvider($searchConn);
            $criteria = new CharitySearchCriteria();
            $criteria->term = $_POST['term'];
            $criteria->state = StateNameAndAbbrConverter::nameToAbbr($_POST['state']);
            $criteria->isRegistered = filter_var($_POST['isRegistered'], FILTER_VALIDATE_BOOLEAN);
            $criteria->start = $_POST['start'] ? $_POST['start'] : 0;
            $criteria->size = $_POST['pageLength'] ? $_POST['pageLength'] : 10;
            $result = $provider->search($criteria);
            $results = $result;
        } else if ($_POST['flavor'] == 'default' || !(isset($_POST['flavor']))){
            include_once($siteRoot."_setupSearchConnection.php");
			$provider = new MySqliFullTextCharitySearchProvider($searchConn);
            $criteria = new CharitySearchCriteria();
            $criteria->term = $_POST['term'];
            $criteria->state = StateNameAndAbbrConverter::nameToAbbr($_POST['state']);
            $criteria->isRegistered = filter_var($_POST['isRegistered'], FILTER_VALIDATE_BOOLEAN);
            $criteria->start = $_POST['start'] ? $_POST['start'] : 0;
            $criteria->size = $_POST['pageLength'] ? $_POST['pageLength'] : 10;
            $result = $provider->search($criteria);
            $results = $result;
        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();