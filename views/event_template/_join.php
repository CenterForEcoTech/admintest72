<?php
/* @var $viewModel EventTemplateViewModel */
echo $viewModel->getTagHeader($CurrentServer, $campaignImageUrlPath);
$imageUrl = $viewModel->getCalendarIcon($CurrentServer, $siteRoot);
if ($imageUrl){
    ?>
    <style type="text/css">
        #event-preview-panel li.draft .calendar_box{
            background: transparent url(<?php echo $imageUrl;?>) no-repeat left center;
            height:100px;
            background-size:contain;
        }
        #event-preview-panel .calendar_box span{
            visibility: hidden;
        }
    </style>
<?php
}
?>
<div class="instructions twelve columns template-process">
    <?php echo $viewModel->instructions;?>
</div>
<div id="join-campaign" class="template-process">
    <?php

    $VenueMissingPlaceholder = $viewModel->getVenueMissingPlaceholder($CurrentServer);
    $eventRecord = $viewModel->getEventRecord("10:00", "22:00");

    $restrictions = EventRecord::getCustomerChoiceRestrictions();

    if (count($viewModel->premiums)){
        $restrictions["pricingPlan"] = new stdClass();
        $restrictions["pricingPlan"]->isEditable = false;
    }
    $customerChoicePercentageLabel = "Percentage of purchases you will give to charity when customers mention ".$SiteName.".";

    // adjust dates if they are in the past
    if (time() > strtotime($restrictions["timeRangeLimit"]->minValue)){
        $eventRecord->startDate = null;
        $eventRecord->endDate = null;
    }

    // add form parameters if desired
    $formId = "create-event-form";
    $formAction = ""; // we're gonna use ajax
    $formClass = "override_skel hidden";
    $formHeaderValue = "Customize Event Details";
    $formIntermediateButtonValue = "Submit";
    $formFinalButtonValue = "Publish";
    $formSubmitValue = count($viewModel->premiums) ? $formIntermediateButtonValue : $formFinalButtonValue;
    $customizeButtonValue = "Customize";
    $premiumPaneContent = $viewModel->getPremiumPaneContent();
    if (isMerchant()){
        $formPublishEnabled = true;
    } else {
        $formPublishEnabled = false;
    }
    ?>

    <div id="slide-display-position" class="twelve columns">
        <span id="action-message" class="alert"></span>
    </div>
    <div style="display:none;">
        <div id="cc-percentage-help" class="help-modal">
            <h4>How to select a Customer Choice Percentage</h4>
            <p>
                &ldquo;Customer Choice&rdquo; is simply our name for the unique <?php echo $SiteName;?> feature
                that enables every customer to choose their own favorite charity &mdash; any
                of more than a million nonprofits and school groups across the
                country &mdash; to receive the percentage of their eligible purchase that you
                indicate here.
            </p>
            <p>
                This turns a single promotion into a fundraiser for
                every nonprofit, increasing the number of organizations and
                community&ndash;minded customers motivated to rally their friends to shop
                and dine.
            </p>
            <p>You decide what an &ldquo;eligible purchase&rdquo; entails &mdash; it could
                include all sales or instead be limited to a specific item or product
                line.  We recommend offering a donation equal to 20% of the purchase
                in order to motivate nonprofits to put extra effort into creating a
                fundraising event out of your promotion.
            </p>
            <p>
                Remember: one great thing
                about <?php echo $SiteName;?> is that customers must mention the promotion to
                trigger a donation; <em>it doesn&rsquo;t cost you anything unless it drives
                    sales.</em>
            </p>
        </div>
    </div>
    <div id="js-warning" class="alert">Please enable javascript to continue.</div>
    <?php include($siteRoot."_EventFormBase.php"); ?>

    <div class="btn right">
        <a href="#join" id="join-event-button">Sign Up Now!</a>
    </div>
    <div id="register-or-login" class="hidden">
        <div class="step_btn right wide">
            <span>Step 1</span>
            <a href="#register" id="register-action-button">Register or Log In</a>
        </div>
    </div>
</div> <!-- end initially hidden form -->
<div id="registration-success" class="hidden">
    <h3>You&rsquo;re Done!</h3>
    <p>Your promotion will be visible to the public in a matter of hours.</p>
    <p>We also sent you a link to activate your account so you can customize your business profile page.</p>

    <div class="sub_section">
        <ul id="draft-event-list">
        </ul>
    </div>
</div><!-- success page -->
<?php echo $viewModel->getTagReferences();?>
<script type="text/javascript">
    $("#js-warning").hide();

    var CETDASH = CETDASH || {}; // namespace
    CETDASH.CurrentServer = '<?php echo $CurrentServer;?>';
    CETDASH.EventForm = CETDASH.EventForm || {};
    CETDASH.EventForm.formId = '<?php echo $formId?>';
    CETDASH.EventForm.saveAsDraft = '<?php echo !isMerchant();?>';
    CETDASH.EventForm.templateId = '<?php echo $viewModel->id;?>';
    CETDASH.EventForm.hideLocationOnPreview = false;
    CETDASH.EventTemplate = CETDASH.EventTemplate || {};
    CETDASH.EventTemplate.skipJoinButton = <?php echo $viewModel->skipJoin ? "true" : "false";?>;
    CETDASH.EventTemplate._state = {
        isLoggedIn: <?php echo isLoggedIn() ? "true" : "false";?>,
        isRegistered: <?php echo isLoggedIn() ? "true" : "false";?>,
        isEventCustomized: false,
        hasPremiums: <?php echo count($viewModel->premiums) ? "true" : "false";?>,
        eventSubmitButton: "<?php echo $formSubmitValue;?>",
        intermediateButtonValue: "<?php echo count($viewModel->premiums) ? $formIntermediateButtonValue : $formFinalButtonValue;?>",
        finalButtonValue: "<?php echo $formFinalButtonValue;?>"
    };
</script>
<script type="text/javascript" src="<?php echo $CurrentServer;?>js/causetown.eventtemplate.js?y=3"></script>
