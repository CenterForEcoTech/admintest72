<div id="social-media-section" class="profile-section twelve columns hidden">
    <div class="legend">Social Media Handles:</div>
    <div id="social-media-wrap" class="profile-subsection eleven columns">
        <p class="help">To support future social media communications, please provide your Facebook Page URL and your Twitter handle.</p>
        <div >
            <a href="#EditSocialMediaHandles" id="edit-social-media-handles" class="action-link" style="float:right">Edit</a>
            <div class="social-media-handle-content">

            </div>
        </div>
    </div>
    <div id="social-media-form" class="eleven columns hidden fake-form override_skel">
        <label>
            <div>Facebook Page:</div>
                <span class="highlight">
                    <input type="text" class="wide auto-watermarked" id="facebook-handle" title="https://www.facebook.com/myFacebookPage">
                </span>
        </label>
        <label>
            <div>Twitter Handle:</div>
                <span class="highlight">
                    <input type="text" class="wide auto-watermarked" id="twitter-handle" title="@myTwitterHandle">
                </span>
        </label>
        <span class="btn" style="float:right;">
            <a href="#SaveEdit" id="save-social-media" >Save</a>
        </span>
        <span class="btn cancel" style="float:right;">
            <a href="#CancelEdit" id="cancel-social-media-edit" class="cancel-button">Cancel</a>
        </span>
    </div>
    <script type="text/javascript">
        $(function(){
            var socialMediaWrap = $("#social-media-wrap"),
                socialMediaForm = $("#social-media-form"),
                fbInput = $("#facebook-handle"),
                twitterInput = $("#twitter-handle"),
                renderSocialMediaText = function(record){
                    var template = "<div class='{networkName}-handle social-media-handle' data-orig='{origText}'><span class='social-network-name'>{networkName}:</span> <a href='{url}' target='_blank'>{handle}</a></div>",
                        text = template.replace(/{networkName}/g, record.socialNetworkName)
                            .replace(/{handle}/g, (record.socialNetworkName == "twitter") ? "@" + record.socialHandle : record.originalIdentifierText)
                            .replace(/{origText}/g, record.originalIdentifierText)
                            .replace(/{url}/g, record.url);
                    return text;
                };

            $("#edit-social-media-handles").click(function(e){
                e.preventDefault();
                socialMediaForm.slideDown();
                socialMediaWrap.slideUp();
                $(".action-link").hide();
            });

            $.ajax({
                url: CETDASH.Profile.serviceURL,
                data:{
                    action: "get_social_media"
                },
                success: function(data){
                    var record, renderedText = "", container = socialMediaWrap.find(".social-media-handle-content");
                    if ($.isArray(data)) {
                        if (data.length){
                            for (var i = 0; i < data.length; i++){
                                record = data[i];
                                renderedText += renderSocialMediaText(record);
                                if (record.socialNetworkName == "twitter"){
                                    twitterInput.val(record.originalIdentifierText);
                                    twitterInput.data("orig", record.originalIdentifierText);
                                } else if (record.socialNetworkName == "facebook") {
                                    fbInput.val(record.originalIdentifierText);
                                    fbInput.data("orig", record.originalIdentifierText);
                                }
                            }
                            container.html(renderedText);
                        } else {
                            container.html("");
                        }
                    }
                }
            });

            $("#cancel-social-media-edit").click(function(e){
                e.preventDefault();
                socialMediaForm.slideUp();
                socialMediaWrap.slideDown();
                $(".action-link").show();
                twitterInput.val(twitterInput.data("orig"));
                fbInput.val(fbInput.data("orig"));
            });

            $("#save-social-media").click(function(e){
                e.preventDefault();
                $.ajax({
                    url: CETDASH.Profile.serviceURL,
                    type: "POST",
                    data: {
                        "action" : "social_media",
                        "facebookPageUrl" : fbInput.val(),
                        "twitterHandle" : twitterInput.val()
                    },
                    success: function(data){
                        var record, renderedText = "", container = socialMediaWrap.find(".social-media-handle-content");
                        if ($.isArray(data)) {
                            if (data.length){
                                for (var i = 0; i < data.length; i++){
                                    record = data[i];
                                    renderedText += renderSocialMediaText(record);
                                    if (record.socialNetworkName == "twitter"){
                                        twitterInput.data("orig", record.originalIdentifierText);
                                    } else if (record.socialNetworkName == "facebook"){
                                        fbInput.data("orig", record.originalIdentifierText);
                                    }
                                }
                                container.html(renderedText);
                            } else {
                                container.html("");
                            }
                        }
                        socialMediaForm.slideUp();
                        socialMediaWrap.slideDown();
                        $(".action-link").show();
                    }
                });
            });
        });
    </script>
</div>