<?php
/* @var $integration InvoiceIntegration */
/* @var $invoiceProvider InvoiceProvider */
if (isMerchant()){
    $merchantBillingInfo = $invoiceProvider->getMerchantBillingInfo(getMerchantId());
    $billingUrl = $integration->getPaymentUrl($merchantBillingInfo);
    ?>
    <style type="text/css">
        #invoices-table tbody tr.overdue td.alert-element{
            color:red;
        }
        #merchant-invoices-report blockquote{
            border-left:none;
        }
        #merchant-invoices-report a{
            color: #7c9a6c;
            text-decoration: none;
        }
        #merchant-invoices-report a:hover{
            text-decoration: underline;
        }
        #merchant-invoices-report .total-amount-due{
            font: 16px "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
            border-bottom: 1px solid #ddd;
            padding: 5px 5px 1px 5px;
        }
        #merchant-invoices-report .notice-box{
            border-bottom:1px dotted #ddd;
            margin-bottom:10px;
            padding-left:25px;
        }
        #merchant-invoices-report .info-box{
            border-bottom:none;
            padding-left:0;
            font-size:14px;
        }
        #merchant-invoices-report .check-payable-notice{
            margin-top:25px;
        }
    </style>
    <div id="invoices-loading">
        <span class="alert">Loading ...</span>
    </div>

    <div id="merchant-invoices-report" class="hidden">
        <?php if ($hasOverdueInvoices){?>
            <div class="notice-box eleven columns hidden">
                Please mail a check for <span class="total-amount-due"></span> to:
                <blockquote>
                    Causetown Solutions, Inc.<br>
                    107 S West St #548<br>
                    Alexandria, VA 22314<br>
                </blockquote>

                Or, <a href="<?php echo $billingUrl;?>" target="_blank">pay online through Bill.com</a>
                <a href="#pay-online-help" class="help-modal-icon colorbox-inline">Help on paying online</a>
            </div>
        <?php } ?>
        <div class="twelve columns">
            <h4>Invoices</h4>
            <?php if (!$hasOverdueInvoices) {?>
                <div class="notice-box info-box eleven columns hidden">
                    <a href="<?php echo $billingUrl;?>" target="_blank">Pay online through Bill.com</a> or mail a check for <span class="total-amount-due"></span> to Causetown Solutions, Inc.
                    <a href="#how-to-pay-help" class="help-modal-icon colorbox-inline">Help on paying invoice</a>
                </div>
            <?php } ?>
            <span id="no-invoices-message" class="alert info hidden">You have no invoices</span>
            <table id="invoices-table" class="sales-data hidden">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Invoice #</th>
                    <th>Donation<br>Amount</th>
                    <th><?php echo $SiteName;?><br>Fee</th>
                    <th>Invoice<br>Total</th>
                    <th>Balance<br>Due</th>
                    <th>Status</th>
                    <th>Due</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="twelve columns check-payable-notice hidden">
            Make checks payable to:

            <blockquote>
                Causetown Solutions, Inc.<br>
                107 S West St #548<br>
                Alexandria, VA 22314<br>
            </blockquote>
        </div>
    </div>
    <div style="display:none;">
        <div id="pay-online-help" class="help-modal">
            <h3>Pay Online through Bill.com</h3>
            <p>
                If you click the link to pay online,
                you will be taken to Bill.com, a separate website that facilitates secure online payments for <?php echo $SiteName;?>.
                Your Bill.com password is separate from your <?php echo $SiteName;?> password.
            </p>
            <p>
                If you are new to Bill.com, click the &ldquo;Don&rsquo;t have an account?&rdquo; link after arriving at their site.
            </p>
            <p>
                Through Bill.com, you can pay by electronic check.
            </p>
            <p>Or, you can mail a check to the following address:</p>
            <blockquote>
                Causetown Solutions, Inc.<br>
                107 S West St #548<br>
                Alexandria, VA 22314<br>
            </blockquote>
        </div>
        <div id="how-to-pay-help" class="help-modal">
            <h3>How to pay an invoice</h3>
            <p>
                If you click the link to pay online,
                you will be taken to Bill.com, a separate website that facilitates secure online payments for <?php echo $SiteName;?>.
                Your Bill.com password is separate from your <?php echo $SiteName;?> password.
            </p>
            <p>
                If you are new to Bill.com, click the &ldquo;Don&rsquo;t have an account?&rdquo; link after arriving at their site.
            </p>
            <p>
                Through Bill.com, you can pay by electronic check.
            </p>
            <p>Or, you can mail a check to the following address:</p>
            <blockquote>
                Causetown Solutions, Inc.<br>
                107 S West St #548<br>
                Alexandria, VA 22314<br>
            </blockquote>
        </div>
    </div>
    <script type="text/template" id="invoice-table-tmpl">
        <tr class="{rowclass}">
            <td>{date}</td>
            <td class="numeric">{id}</td>
            <td class="numeric">{donationAmount}</td>
            <td class="numeric">{feeAmount}</td>
            <td class="numeric">{totalAmount}</td>
            <td class="numeric balance-due alert-element">{balanceDue}</td>
            <td class="invoice-status alert-element">{status}</td>
            <td class="due-date alert-element">{dueDate}</td>
        </tr>
    </script>
    <script type="text/javascript">
        $(function(){
            var table = $("#invoices-table"),
                tableBody = table.find("tbody"),
                rowTemplate = $("#invoice-table-tmpl").html(),
                totalDue = 0.00,
                renderRow = function(item, reportBody, rowTemplate){
                    var rowClass = (item.isOverdue) ? "overdue" : "",
                        htmlString = rowTemplate.replace(/{rowclass}/g, rowClass)
                            .replace(/{date}/g, item.date)
                            .replace(/{id}/g, item.id)
                            .replace(/{dueDate}/g, item.dueDate)
                            .replace(/{donationAmount}/g, "$$" + item.donationAmount.toFixed(2))
                            .replace(/{feeAmount}/g, "$$" + item.feeAmount.toFixed(2))
                            .replace(/{totalAmount}/g, "$$" + item.totalAmount.toFixed(2))
                            .replace(/{balanceDue}/g, "$$" + item.balanceDue.toFixed(2))
                            .replace(/{status}/g,  item.status),
                        rowElement = $(htmlString);
                    reportBody.append(rowElement);
                },
                renderTable = function(data){
                    if (data.collection && data.collection.length){
                        $.each(data.collection, function(index, item){
                            renderRow(item, tableBody, rowTemplate);
                            totalDue += parseFloat(item.balanceDue);
                        });
                        if (totalDue){
                            $(".total-amount-due").html("$" + totalDue.toFixed(2));
                            $("#merchant-invoices-report .notice-box").show();
                            $(".check-payable-notice").show();
                        }
                        table.show();
                    } else {
                        // no data
                        $("#no-invoices-message").show();
                    }
                },
                loadReportData = function(){
                    $.ajax({
                        url: '<?php echo $CurrentServer;?>ApiMerchants',
                        data: {
                            action : "get_invoices",
                            start : 0,
                            pageLength : 0
                        },
                        type: "GET",
                        success: function(data){
                            renderTable(data);
                            $("#invoices-loading").hide();
                            $("#merchant-invoices-report").show();
                        }
                    });
                };

            loadReportData();
        });
    </script>
<?php
} // is a merchant