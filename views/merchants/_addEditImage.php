<?php
// NOTE: the form uses all array names using square brackets because the image manager handles that better.
if (!isset($merchantImageRecord)){
    $merchantImageRecord = new MerchantImageRecord();
    $merchantImageRecord->imageRecord = new ImageRecord();
    if (isAdmin()){
        // when this is a new record, and an admin is logged in, it is automatically approved
        $merchantImageRecord->status = MerchantImageRecord::APPROVED_STATUS;
    }
}

$imageRecord = $merchantImageRecord->imageRecord;
if ($imageRecord->fileName){
    $fileMetadata = $merchantImageFileManager->getMetadata($imageRecord->fileName);
    $imageDimensionCssClass = "";
    if ($fileMetadata == null){
        $fileMetadata = $imageManager->getMetadata($imageRecord->fileName);
    }
    if ($imageRecord->width < 370){
        $imageTooSmallAlert = "This image will be stretched to fit the canvas. You are advised to upload a larger version if possible.";
        $imageDimensionCssClass = "alert";
    }
}
?>
<?php
// TODO: Remove this section when a more robust image management is built
if ($merchantImageRecord->id){
    ?>
    <p>
        <span class="btn right">
            <a href="<?php echo $CurrentServer;?>Merchant/ManageImages/new">Add Another Image</a>
        </span>
        To edit the Description or any other field, make your changes below and click Submit.
    </p>
    <hr>
<?php
}
?>
<form id="merchant-images" method="POST" action="<?php echo $CurrentServer;?>Merchant/ManageImages" enctype="multipart/form-data">
    <input type="hidden" name="id[]" value="<?php echo $imageRecord->id;?>">
    <input type="hidden" name="merchant_images_id[]" value="<?php echo $merchantImageRecord->id;?>">
    <div class="twelve columns">
        <div class="six columns alpha">
            <div class="preview five columns">
                <span>
                    <?php if (isset($fileMetadata)){?>
                        <a href="<?php echo $fileMetadata->url;?>" target="image-preview" title="Click to view original image"><img src="<?php echo $fileMetadata->url;?>" style="max-width: 265px; max-height: 400px;"></a>
                    <?php } ?>
                </span>
            </div>
            <div class="five columns">
                <div class="metadata <?php echo $fileMetadata ? "" : "hidden"; ?>">
                    <label class="name"><strong>Name: </strong><span><?php echo $fileMetadata->name;?></span></label>
                    <label class="size"><strong>Size: </strong><span><?php echo $fileMetadata->size;?><span></label>
                    <label class="type"><strong>Type: </strong><span><?php echo $imageRecord->fileType;?><span></label>
                    <label class="xy"><strong>Dimensions: </strong><span class="<?php echo $imageDimensionCssClass;?>"><?php echo $imageRecord->width;?> x <?php echo $imageRecord->height;?><span></label>
                    <span class="alert"><?php echo $imageTooSmallAlert;?></span>
                </div>
            </div>
        </div>
        <div class="six columns omega">
            <label class="six columns alpha omega">
                <div class="required">Select Image:</div>
                <input type="text" id="uploadFile" class="auto-watermarked" title="Choose File" disabled="disabled" value="<?php echo $fileMetadata->name;?>" />
                <div class="file-upload">
                    <span>Upload</span>
                    <input type="file" name="files[]">
                </div>
            </label>
            <label>
                <div class="required">Product Name:</div>
                <input type="text" name="title[]" class="auto-watermarked" title="Product Name" value="<?php echo htmlspecialchars($imageRecord->title);?>">
            </label>
            <label>
                <div>Image Caption:</div>
                <input type="text" name="caption[]" class="auto-watermarked" title="Custom Image Caption" value="<?php echo htmlspecialchars($merchantImageRecord->imageCaption);?>">
            </label>
            <label>
                <div>External Link:</div>
                <input type="text" name="externalLink[]" class="auto-watermarked" title="http://" value="<?php echo $merchantImageRecord->externalLinkUrl;?>">
            </label>
            <label>
                <div>Expiration Date:</div>
                <input type="text" name="expirationDate[]" class="auto-watermarked auto-datepicker" title="<?php echo date("n/j/Y", strtotime("+6 month"));?>" value="<?php echo $merchantImageRecord->expirationDate ? date("n/j/Y", strtotime($merchantImageRecord->expirationDate)) : "";?>">
            </label>
        </div>
    </div>
    <fieldset class="twelve columns">
        <legend>Product Price</legend>
        <p class="help">Enter a price range, or if this image is for a single product with a specific price, enter just the first price, and check the box for &ldquo;There is only one price&rdquo; below.</p>
        <label class="five columns">
            <div class="required">Lower range of price:</div>
            <input type="text" name="priceRangeLow[]" class="auto-watermarked" title="0.00" value="<?php echo is_numeric($merchantImageRecord->priceRangeLow) ? $merchantImageRecord->priceRangeLow : "";?>">
        </label>
        <label class="five columns">
            <div>Upper range of price</div>
            <input type="text" name="priceRangeHigh[]" class="auto-watermarked" title="1000.00" value="<?php echo is_numeric($merchantImageRecord->priceRangeHigh) ? $merchantImageRecord->priceRangeHigh : "";?>">
        </label>
        <label class="five columns">
            <input type="checkbox" value="1" name="isOnePrice[]" title="Check this if this product has only one price." <?php echo $merchantImageRecord->isOnePrice ? "checked" : "";?>>
            There is only one price (not a range).
        </label>
    </fieldset>

    <div class="markdown-editor twelve columns" >

        <label for="markdown-source" class="required">Description</label>
        <div id="notes-button-bar" class="wmd-button-bar"></div>
        <textarea id="markdown-source" name="descriptionMarkdown[]" class="wmd-input auto-watermarked" cols="92" rows="15" title="Enter a sales or marketing description for this image."><?php echo $merchantImageRecord->descriptionMarkDownSource;?></textarea>

        <div style="display:none;">
            <label for="html-source">Preview</label>
            <div id="notes-preview" class="wmd-preview"></div>
            <textarea id="html-source"  name="descriptionHtml[]" style="display:none;"><?php echo $merchantImageRecord->description;?></textarea>
        </div>
    </div>
    <?php if (isAdmin()){ ?>
        <fieldset class="twelve columns">
            <legend>Admin Only</legend>
            <div class="five columns">
            <label>
                <div>Status:</div>
             </label>
            <?php foreach (MerchantImageRecord::$APPROVAL_STATUSES as $statusValue) {
                $selected = $merchantImageRecord->status == $statusValue ? "checked" : "";
                ?>
                <label>
                    <input type="radio" name="status[]" value="<?php echo $statusValue;?>" <?php echo $selected; ?>><?php echo ucwords(strtolower($statusValue))?>
                </label>
            <?php } ?>
            </div>
            <label class="six columns">
                <div>Keywords:</div>
                <textarea class="small" name="keywords[]"><?php echo $imageRecord->keywords?></textarea>
            </label>
            <p class="help eleven columns">To set the sort order, visit <a href="<?php echo $CurrentServer.$adminFolder?>merchants/?nav=profile-edit&id=<?php echo getMerchantId();?>" target="merchant-profile">this merchant's profile page in the admin console</a>.</p>
        </fieldset>
    <?php } ?>
    <div class="twelve columns">
        <div id="submit-button" class="btn right">
            <input id="submit-button-trigger" type="submit" value="Submit">
        </div>
    </div>
</form>
<script type="text/javascript" src="/js/jquery-file-upload/jquery-1.8.3.min.js"?>" ></script>
<script src="<?php echo $CurrentServer;?>js/jquery-ui-1.9.2.custom.min.js"></script>
    <?php
    $Footer_excludeJqueryUi = true;
    ?>
<script type="text/javascript" src="/js/jquery-file-upload/load-image.js"?>" ></script>
<script type="text/javascript" src="/js/wmd/jquery.wmd.min.js"></script>
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.iframe-transport.js"></script>
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload.js"></script>
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-fp.js"></script>
<script type="text/javascript">
    $(function(){
        var formElement = $("#merchant-images"),
            uploadFileName = formElement.find("#uploadFile"),
            fileInput = formElement.find("input[type='file']"),
            editorField = $("#markdown-source"),
            previewNode = formElement.find(".preview span"),
            renderPreview = function (file) {
                var node = previewNode,
                    metadataContainer = formElement.find(".metadata"),
                    nameNode = metadataContainer.find("label.name span"),
                    typeNode = metadataContainer.find("label.type span"),
                    sizeNode = metadataContainer.find("label.size span"),
                    dimensionNode = metadataContainer.find("label.xy span");

                return (loadImage && loadImage(
                    file,
                    function (img) {
                        node.html(img);
                        nameNode.html(file.name);
                        typeNode.html(file.type);
                        sizeNode.html(file.size);
                        dimensionNode.html("Pending . . .").addClass("warning");
                        metadataContainer.show();
                    },
                    {
                        maxWidth: 265,
                        maxHeight: 400,
                        canvas: true
                    }
                ));
            },
            dataObject,
            successReload = function(data){
                var dataId = data.id;
                if (opener && opener.CETDASH && opener.CETDASH.onChildClosed && $.isFunction(opener.CETDASH.onChildClosed)){
                    window.onunload = function(){
                        opener.CETDASH.onChildClosed();
                    };
                    window.close();
                } else {
                    $(window).scrollTop(0);
                    window.location.href = "<?php echo $CurrentServer;?>Merchant/ManageImages/" + dataId;
                }
            };

        editorField.wmd({
            "helpLink": "http://daringfireball.net/projects/markdown/",
            "helpHoverTitle": "Markdown Help",
            "button_bar": "notes-button-bar",
            "preview": "notes-preview",
            "output": "html-source"
        });

        formElement.on("submit", function (e) {
            e.preventDefault();
            var button = $(e.currentTarget),
                data = dataObject;
            if (data && data.submit && !data.jqXHR && data.submit().success(function (result, textStatus, jqXHR) {
                successReload(result);
            })) {
                button.prop('disabled', true);
            } else {
                // do a standard submit
                $.ajax({
                    url: "<?php echo $CurrentServer;?>ApiMerchantImageLibrary",
                    type: "POST",
                    data: JSON.stringify(formElement.serializeObject()),
                    dataType: "json",
                    contentType: "application/json",
                    statusCode:{
                        200: function(data){
                            successReload(data);
                        },
                        409: function(jqXHR, textStatus, errorThrown){
                            var data = $.parseJSON(jqXHR.responseText),
                                record = data.record;
                            if (data.message){
                                alert(data.message);
                            } else {
                                alert("Unexpected condition encountered. Please refresh the page and try again.")
                            }
                        }
                    }
                });
            }
        });

        formElement.fileupload({
            url: '<?php echo $CurrentServer;?>ApiMerchantImageLibrary',
            dropZone: formElement,
            pasteZone: formElement,
            uploadTemplateId: null,
            drop: function (e, data) {
                $.each(data.files, function (index, file) {
                    renderPreview(file);
                });
            },
            change: function (e, data) {
                $.each(data.files, function (index, file) {
                    var fullPath = fileInput.val(),
                        filename = fullPath.replace(/^.*[\\\/]/, '');
                    uploadFileName.val(filename);
                    renderPreview(file);
                });
            },
            add: function (e, data) {
                dataObject = data;
            }
        });

        formElement.bind('fileuploadsubmit', function (e, data) {
            data.formData = formElement.serializeArray();
        });
    });
</script>