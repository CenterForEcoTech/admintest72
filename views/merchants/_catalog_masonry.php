<?php
/* @var $imageRecord MerchantImageRecord */
//$Footer_includeInfiniteScroll = true;
$Footer_includeMasonry = true;
$refreshUrl = rtrim($CurrentServer, "/").$_SERVER['REDIRECT_URL'];
if ($_GET['isEmbed']){
    $refreshUrl .= "?isEmbed=1";
}
function embedThis_getEmbedUrl(){
    global $CurrentServer;
    $embedUrl = rtrim($CurrentServer,"/").$_SERVER['REDIRECT_URL']."?isEmbed=1";
    if ($_GET['keyword']){
        $embedUrl .= "&keyword=".$_GET['keyword'];
    }
    return $embedUrl;
}
?>
<div id="catalog-search" class="twelve columns offset-by-four stamp">
    <form class="override_skel">
        <?php if ($_GET['isEmbed']) {?>
        <input type="hidden" name="isEmbed" value="1">
        <?php } ?>
        <span class="highlight">
            <input type="text" name="keyword" class="auto-complete auto-watermarked" title="Search">
        </span>
            <a href="<?php echo $refreshUrl;?>" class="reset <?php echo ($_GET['keyword']) ? "" : "hidden"?>"">Show All</a>
        <input type="submit" style="display:none"/>
    </form>
</div>
<div id="catalog-container">
<?php foreach ($imageResult->collection as $imageRecord){ ?>
    <div class="box">
        <a href="#meta-<?php echo $imageRecord->id;?>" class="colorbox-inline-2">
        <div class="img-container">
            <img src="<?php echo $imageRecord->getLargeImageUrl($CurrentServer, $siteRoot);?>">
        </div>
        <div class="meta caption">
            <?php echo $imageRecord->getCaptionDisplay();?>
        </div>
        <div class="meta venue">
            <span class="name"><?php echo $imageRecord->merchantName;?></span><br>
            <?php echo $imageRecord->city.", ".$imageRecord->state;?>
        </div>
        </a>
    </div>
<?php } ?>
</div>
<div style="display:none;" class="twelve columns">
    <?php foreach ($imageResult->collection as $imageRecord){ ?>
    <div id="meta-<?php echo $imageRecord->id;?>" class="image-meta-content">
        <h6><?php echo $imageRecord->getCaptionDisplay();?></h6>
        <img class='preview' src="<?php echo $imageRecord->getLargeImageUrl($CurrentServer, $siteRoot);?>">
        <p class="product-info">
            <?php echo $imageRecord->imageRecord->title;?><br>
            <?php echo $imageRecord->getPriceDisplay();?>
        </p>
        <p class='venue'>
            <?php echo $imageRecord->merchantName;?><br>
            <?php echo $imageRecord->getSingleLineAddress();?>
        </p>
        <?php
        echo $imageRecord->description;
        $linkUrl = $imageRecord->externalLinkUrl ? $imageRecord->externalLinkUrl : $imageRecord->website;

        if ($linkUrl){?>
            <p class="external-link"><a href="<?php echo $linkUrl?>" target="img_link" title="Opens in a new window">Click here for more information</a></p>
        <?php } ?>
    </div>
    <?php } ?>
</div>
<script type="text/javascript">
    $(function(){
        var container = $('#catalog-container');

        $("#main").removeClass("fifteen").removeClass("twelve").addClass("sixteen").addClass("alpha").addClass("omega");
        container.masonry({
            itemSelector: ".box"
        });

        container.imagesLoaded( function() {
            container.masonry();
            $(".box img").animate({opacity: 1}, 500);
        });
    });
</script>