<?php
if (!isset($webPageModel) || !($webPageModel instanceof CampaignModel)){
    trigger_error("views/_campaignV2.php called without a valid CampaignModel", E_USER_ERROR);
    die();
}
$primaryEventTemplate = $webPageModel->getPrimaryEventTemplate();
$thePid = SessionManager::getUserPID();  // logged in user's pid

?>
    <div class="campaign-left columns">
        <h2><?php echo $webPageModel->getPageHeader();?></h2>
        <h3><?php echo $webPageModel->getPageSubtitle();?></h3>
        <div class="content">
            <?php
            $campaignImageObject = $webPageModel->getCampaignImage();
            if ($campaignImageObject){
                $imgCssClass = ($campaignImageObject->width < 250 || !$campaignImageObject->isWide) ? "square-image" : "wide-image";
                $imgTag = "<img src='".$campaignImageObject->url."' class='".$imgCssClass."'>";

                if ($webPageModel->getCampaignImageLink()){?>
                    <a href="<?php echo $webPageModel->getCampaignImageLink();?>" target="_blank"><?php echo $imgTag;?></a>
                <?php } else {
                    echo $imgTag;
                }
            }
            ?>
            <?php echo $webPageModel->getCampaignDescription();?>
        </div>
    </div>
    <div class="campaign-right columns">
        <div class="action-block">
            <div class="message-block">
                <?php echo $webPageModel->getCallToAction();?>
            </div>
            <div id="join-action-section" class="button-block">
                <div>
                    <span class="btn" id="sign-up-button"><a href="#" class="do-not-navigate">Count Me In!</a></span>
                </div>
                <div class="join-type-selectors">
                    <label><input type="radio" name="join_type" value="business" <?php echo $webPageModel->getIsBusinessType() ? "checked" : "";?>> I&rsquo;m a Business</label>
                    <label><input type="radio" name="join_type" value="nonprofit" <?php echo $webPageModel->getIsNonprofitType() ? "checked" : "";?>> I&rsquo;m a Nonprofit</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <?php
        // resources
        $campaignResources = $webPageModel->getCampaignResources();
        if (count($campaignResources)){
            $variables = (object)Array(
                "tagReferencesArray" => $campaignResources,
                "maximumNumber" => count($campaignResources)
            );
            $referenceDisplayData = render_template("CampaignResourcesV2.snippet", $variables);
            echo $referenceDisplayData;
        }

        $poster = $webPageModel->getPoster();
        if ($poster){
            ?>
            <div class="poster-block">
                <a href="<?php echo $poster->url;?>" target="_blank">
                    <img src="<?php echo $poster->imageUrl;?>" height="100%">
                </a>
                <span>Customers Show This Flier<img src="<?php echo $CurrentServer;?>images/right.png"></span>
            </div>
        <?php }

        ?>
        <div class="social-add-this">
            <?php
            $AddThisURLExtra = "campaign/".urlencode($webPageModel->getCampaignName()).$webPageModel->getPidLink();
            $AddThisTitleExtra = " - ".htmlspecialchars(htmlspecialchars_decode($webPageModel->getPidLink()));
            $AddThisDescription = " addthis:description=\"".htmlspecialchars(htmlspecialchars_decode($webPageModel->getCampaignDescription()))."...\"";
            include_once($siteRoot.'Library/AddThis.php');
            ?>
        </div>
    </div>
<div id="participating-businesses" class=" fifteen columns">
    <span class="print-icon" title="Print this list"></span>
    <style type="text/css">
        #participating-businesses{
            padding:20px;
            position:relative;
        }
        ul.business-list li a span{
            font-weight:bold;
        }
        #participating-businesses .print-icon{
            position:absolute;
            width:84px;
            height:32px;
            right:0;
            top:-31px;
            margin-top:45px;
            cursor: pointer;
            background: transparent url(../images/print_w32.png) no-repeat 0 0;
            text-indent:100%;
            white-space: nowrap;
            overflow:hidden;
        }
        @media print{
            .container .campaign-left .content *, .container .campaign-right, .container .social-add-this,
            .container .poster-block, .container .resource-block, #participating-businesses .print-icon,
            body#campaign-page #main .content .wide-image,
            #header_band, #footer{
                display:none;
            }
            body#campaign-page #main .content{
                border-bottom:none;
                padding:0;
            }
        }
    </style>
    <h3>Businesses within 100 Miles of Your Location<?php echo ($defaultUserLocation) ? " (".$defaultUserLocation.")" : "";?></h3>
<?php
$NoResults = "<p>There are no participating businesses near your location that match your search criteria.</p>";
if ($isValidCampaign && isset($paginatedResult) && property_exists($paginatedResult, "collection") && is_array($paginatedResult->collection)){
    if (count($paginatedResult->collection)){
        echo "<ul class='business-list'>";
        foreach ($paginatedResult->collection as $eventRecord){
            /* @var $eventRecord EventRecord */
            $addressVar = $eventRecord->venue->getSingleLineAddress();
            if ($eventRecord->isOnlineEvent){
                // call shop online snippet
                $shopOnlineVariables = (object)array(
                    "event" => $eventRecord
                );
                $addressVar = render_template("ShopOnline.snippet", $shopOnlineVariables);
            }
            // call event timespan snippet
            $eventVariable = (object)array(
                "eventRecord" => $eventRecord
            );
            $eventTimespan = render_template("EventTimespanMicroformat.snippet", $eventVariable);
            ?>
            <li>
                <a href="<?php echo $CurrentServer."event/".$eventRecord->id;?>" target="_blank"><span><?php echo $eventRecord->venue->name."</span>: ".$addressVar;?></a><br>
                <?php echo $eventTimespan;?> (<?php echo $eventRecord->getFullOfferTextForBusinessList();?>)
            </li>
        <?php
        }
        echo "</ul>";
    } else {
        echo $NoResults;
    }
} else {
    echo $NoResults;
}
?>
</div>
<script type="text/javascript">
    $(function(){
        var windowResizeTimeout;
        $(window).on("resize load", function(e){
            if (windowResizeTimeout){
                clearTimeout(windowResizeTimeout);
            }
            windowResizeTimeout = setTimeout(function(){
                // set the min-height of the left column to the height of the right column
                if ($(document).width() > 940){
                    $(".campaign-left").css("min-height", $(".campaign-right").height());
                } else {
                    $(".campaign-left").css("min-height", "0");
                }
            }, 300);
        });

        $("#participating-businesses").on("click", ".print-icon", function(e){
            window.print();
        });
    });
</script>
