<div class="nonprofit-registration-section">
    <form class="override_skel">
        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.NonprofitRegistration = CETDASH.NonprofitRegistration || {};
            CETDASH.NonprofitRegistration.campaign = CETDASH.NonprofitRegistration.campaign || "";

            CETDASH.LoginForm = CETDASH.LoginForm || {};
            CETDASH.LoginForm.successHandler = CETDASH.LoginForm.successHandler || function(data){};

            CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                sourceType : "POST",
                showState: true,
                minLength: 3,
                pageLength: 10,
                onSearch: function(event,ui){
                    $("#result-address").html("");
                },
                onSelect: function( event, ui ) {
                    var record = ui.item.record,
                        loadRecord = function(record){
                            var template = record.isChildOrg ? $("#location-template-child").html() : $("#location-template-parent").html(),
                                container = $("#result-address"),
                                isChildOrg = record.isChildOrg || record.isFiscallySponsored,
                                contentHtml = template.replace(/{name}/g, record.name)
                                    .replace(/{address}/g, record.address1)
                                    .replace(/{city}/g, record.city)
                                    .replace(/{state}/g, record.region)
                                    .replace(/{zip}/g, record.postalCode),
                                alreadyRegisteredTemplate = isChildOrg ? $("#already-registered-child").html() : $("#already-registered-parent").html(),
                                alreadyRegisteredContent = alreadyRegisteredTemplate.replace(/{name}/g, record.name)
                                    .replace(/{ein}/g, record.ein ? record.ein : "")
                                    .replace(/{orgId}/g, record.orgId ? record.orgId : "")
                                    .replace(/{campaign}/g, CETDASH.NonprofitRegistration.campaign)
                                    .replace(/{memberId}/g, record.memberId),
                                newCharityTemplate, newCharityContent;
                            container.html(contentHtml);

                            if (record.memberId){
                                container.append(alreadyRegisteredContent);
                                if (record.ncesSchoolId){
                                    container.find(".on-login-hide").hide();
                                }
                            } else {
                                newCharityTemplate = $("#new-charity").html();
                                newCharityContent = newCharityTemplate.replace(/{name}/g, record.name)
                                    .replace(/{ein}/g, record.ein ? record.ein : "")
                                    .replace(/{orgId}/g, record.orgId ? record.orgId : "")
                                    .replace(/{campaign}/g, CETDASH.NonprofitRegistration.campaign);
                                container.append(newCharityContent);
                                if (record.ncesSchoolId || isChildOrg){
                                    container.find("input[name='company_differentnamethanfiscalsponsor']").prop("disabled", true).closest("label").hide();
                                }
                            }
                            if (CETDASH.NonprofitRegistration.campaign){
                                container.find(".show-for-campaign").show();
                            }
                        };

                    if (record.orgId){
                        $.ajax({
                            url: CETDASH.CharitySearchWidget.sourceUrl,
                            type: "POST",
                            data: {
                                action: "org-lookup",
                                orgId: record.orgId
                            },
                            success: function(data){
                                if (data.memberId){
                                    record.memberId = data.memberId;
                                }
                                if (data.ncesSchoolId){
                                    record.ncesSchoolId = data.ncesSchoolId;
                                }
                                loadRecord(record);
                            }
                        });
                    } else {
                        $.ajax({
                            url: CETDASH.CharitySearchWidget.sourceUrl,
                            type: "POST",
                            data: {
                                action: "ein-lookup",
                                ein: record.ein
                            },
                            success: function(data){
                                var message = "";
                                if (data.orgId){
                                    record.orgId = data.orgId;
                                }
                                loadRecord(record);
                            },
                            dataType: 'json'
                        });
                    }
                }
            };
        </script>
        <label for="charity-search-box" class="on-login-hide">
            <div>Search for your organization:
                <a href="#combined-search-help" class="help-modal-icon colorbox-inline">Help on Search</a></div>
        </label>
            <?php include($siteRoot."views/_charityPickerWidget.php");?>
        <div id="result-address"></div>
    </form>
    <div class="login-form-container twelve columns"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("body").on("click", ".nonprofit-registration-section .login-link", function(e){
            var loginFormUrl = "<?php echo $CurrentServer;?>ajax_loginForm",
                clicked = $(this),
                memberId = clicked.data("member-id"),
                button = clicked.closest("div.btn"),
                formContainer = clicked.closest("div.nonprofit-registration-section").find(".login-form-container");
            e.preventDefault();
            button.hide();
            formContainer.load(loginFormUrl, function(){
                var username = formContainer.find("#username"),
                    usernameContainer = username.closest("label"),
                    password = formContainer.find("input[name='password']"),
                    rememberMe = formContainer.find("input[type='checkbox']").closest("label");
                username.val("ct" + memberId);
                usernameContainer.hide();
                rememberMe.hide();
                $(".charity-search-widget").slideUp();
                $(".on-login-hide").slideUp();
                formContainer.slideDown();
                password.focus();
            });
        });
    });
</script>
<script type="text/template" id="location-template-parent">
    <div id="profile-read-only" class="profile-section twelve columns ">
        <div class="eleven columns">
            <p class="help">The address on file with the IRS is as follows:</p>
            <div class="address ten columns">
                <p class="business-official-name">{name}</p>
                <p class="street-address">{address}</p>
                <p class="city-state">{city}, {state} {zip}</p>
                <p class="business-phone"></p>
            </div>
            <p class="help" style="clear:both;padding-top:10px;">For security reasons, <?php echo $SiteName; ?> will only mail donations to this location.
                If this address is incorrect, please <a href="<?php echo $CurrentServer;?>ContactUs" target="_blank">Contact Us</a> for assistance.
            </p>
        </div>
    </div>
</script>
<script type="text/template" id="already-registered-parent">
    <div class="twelve columns">
        <div class="btn" title="Login as this organization"><a href="<?php echo $CurrentServer;?>Login" data-member-id="{memberId}" class="login-link">Login as this organization<span class="show-for-campaign hidden"> and join campaign</span></a></div>
    </div>
    <form class="twelve columns on-login-hide" action="<?php echo $CurrentServer."Organization/SignUp_ContactInfo";?>" method="post" style="margin-top:5px;">
        <input type="hidden" name="company_differentnamethanfiscalsponsor" value="true">
        <input type="hidden" name="parentOrgId" value="{orgId}">
        <input type="hidden" name="EIN" value="{ein}">
        <input type="hidden" name="campaign" value="{campaign}">
        <div class="btn administer" title="Register as a fiscally sponsored charity">
            <input type="submit" value="{name} is my fiscal sponsor">
        </div>
    </form>
</script>
<script type="text/template" id="location-template-child">
    <div id="profile-read-only" class="profile-section twelve columns ">
        <div class="eleven columns">
            <div class="address ten columns">
                <p class="business-official-name">{name}</p>
                <p class="street-address">{address}</p>
                <p class="city-state">{city}, {state} {zip}</p>
                <p class="business-phone"></p>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="already-registered-child">
    <div class="twelve columns">
        <div class="btn" title="Login as this organization"><a href="<?php echo $CurrentServer;?>Login" data-member-id="{memberId}" class="login-link">Login as this organization<span class="show-for-campaign hidden"> and join campaign</span></a></div>
    </div>
</script>
<script type="text/template" id="new-charity">
    <div class="twelve columns">
        <p>Please register your organization. <span class="show-for-campaign hidden">When you complete registration, you will be automatically added to this campaign.</span></p>
        <form action="<?php echo $CurrentServer."Organization/SignUp_ContactInfo";?>" method="post">
            <input type="hidden" name="EIN" value="{ein}">
            <input type="hidden" name="orgId" value="{orgId}">
            <input type="hidden" name="campaign" value="{campaign}">
            <label>
                <input type="checkbox" name="company_differentnamethanfiscalsponsor" value="true"> {name} is our fiscal sponsor
            </label>
            <div class="btn administer" title="Register my Charity">
                <input type="submit" value="Register">
            </div>
        </form>
    </div>
</script>
<?php include("_NonprofitSearchRegistrationHelp.php");?>