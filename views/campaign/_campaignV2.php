<?php
if (!isset($webPageModel) || !($webPageModel instanceof CampaignModel)){
    trigger_error("views/_campaignV2.php called without a valid CampaignModel", E_USER_ERROR);
    die();
}
$primaryEventTemplate = $webPageModel->getPrimaryEventTemplate();
$thePid = SessionManager::getUserPID();  // logged in user's pid

?>
<div class="campaign-left columns">
    <h2><?php echo $webPageModel->getPageHeader();?></h2>
    <h3><?php echo $webPageModel->getPageSubtitle();?></h3>
    <div class="content">
        <?php
        $campaignImageObject = $webPageModel->getCampaignImage();
        if ($campaignImageObject){
            $imgCssClass = ($campaignImageObject->width < 250 || !$campaignImageObject->isWide) ? "square-image" : "wide-image";
            $imgTag = "<img src='".$campaignImageObject->url."' class='".$imgCssClass."'>";

            if ($webPageModel->getCampaignImageLink()){?>
                <a href="<?php echo $webPageModel->getCampaignImageLink();?>" target="_blank"><?php echo $imgTag;?></a>
            <?php } else {
                echo $imgTag;
            }
        }
        ?>
        <?php echo $webPageModel->getCampaignDescription();?>
    </div>
</div>
<style type="text/css">
    .loading-gif{
        min-height:60px;
        text-align: center;
        background:url(/images/ajaxLoader.gif) center center no-repeat;
    }
    .loading-gif img{
        vertical-align: middle;
    }
    #campaign-map .loading-gif{
        width:100%;
        min-height:500px;
        border:1px solid #444;
    }
    #events_section .loading-gif{
        width:100%;
        min-height:150px;
    }
</style>
<div id="campaign-map" class="fifteen columns">
    <div class=" loading-gif"></div>
</div>
<div class="<?php echo $webPageModel->getCampaignName();?>">
    <div id="event_search" class="fifteen columns">
    </div>
    <div id="events_section" class="fifteen columns">
        <ul id="event-list">
            <div class=" loading-gif"></div>
        </ul>
    </div>
</div>
<div class="campaign-right columns">
    <div class="action-block">
        <div class="message-block">
            <?php echo $webPageModel->getCallToAction();?>
        </div>
        <div id="join-action-section" class="button-block">
            <div>
                <span class="btn" id="sign-up-button"><a href="#" class="do-not-navigate">Count Me In!</a></span>
            </div>
            <div class="join-type-selectors">
                <label><input type="radio" name="join_type" value="business" <?php echo $webPageModel->getIsBusinessType() ? "checked" : "";?>> I&rsquo;m a Business</label>
                <label><input type="radio" name="join_type" value="nonprofit" <?php echo $webPageModel->getIsNonprofitType() ? "checked" : "";?>> I&rsquo;m a Nonprofit</label>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?php
    // resources
    $campaignResources = $webPageModel->getCampaignResources();
    if (count($campaignResources)){
        $variables = (object)Array(
            "tagReferencesArray" => $campaignResources,
            "maximumNumber" => count($campaignResources)
        );
        $referenceDisplayData = render_template("CampaignResourcesV2.snippet", $variables);
        echo $referenceDisplayData;
    }

    $poster = $webPageModel->getPoster();
    if ($poster){
    ?>
    <div class="poster-block">
        <a href="<?php echo $poster->url;?>" target="_blank">
            <img src="<?php echo $poster->imageUrl;?>" height="100%">
        </a>
        <span>Customers Show This Flier<img src="<?php echo $CurrentServer;?>images/right.png"></span>
    </div>
    <?php }

    // relatedLinks
    $relatedLinks = $webPageModel->getRelatedLinks();
    if (count($relatedLinks)){
        $variables = (object)Array(
            "tagReferencesArray" => $relatedLinks,
            "maximumNumber" => count($relatedLinks),
            "header" => "Partners and Related Promotions",
            "blockCssClasses" => "resource-block related-campaign-block"
        );
        $referenceDisplayData = render_template("CampaignResourcesV2.snippet", $variables);
        echo $referenceDisplayData;
    }
    ?>
    <div class="social-add-this">
        <?php
        $AddThisURLExtra = "campaign/".urlencode($webPageModel->getCampaignName()).$webPageModel->getPidLink();
        $AddThisTitleExtra = " - ".htmlspecialchars(htmlspecialchars_decode($webPageModel->getPidLink()));
        $AddThisDescription = " addthis:description=\"".htmlspecialchars(htmlspecialchars_decode($webPageModel->getCampaignDescription()))."...\"";
        include_once($siteRoot.'Library/AddThis.php');
        ?>
    </div>
</div>
<script type="text/template" id="search-message-template">
    <span class='alert info'>Filtering by {filterText}</span>
    <a class='indexfilter-resetlink do-not-navigate'>remove filter</a>
</script>
<script type="text/javascript">
    $(function(){
        var buttonBlock = $("#join-action-section"),
            businessSignupLink = "<?php echo $primaryEventTemplate ? $CurrentServer."template/".$primaryEventTemplate->url : "";?>",
            nonprofitSignupLink = "<?php echo $CurrentServer."campaign-charity/".$webPageModel->getCampaignName(); ?>",
            campaignMap = $("#campaign-map"),
            eventSearch = $("#event_search"),
            paginationDefaults = {
                startIndex: 0,
                pageLength: <?php echo $webPageModel->getPaginationPageLength();?>,
                tagName: encodeURI("<?php echo $webPageModel->getCampaignName();?>"),
                pidLink: '<?php echo $webPageModel->getPidLink();?>',
                KW: '',
                RF: '',
                term: '',
                EZ: ''
            },
            paginationOptions = $.extend({}, paginationDefaults),
            eventList = $("#event-list"),
            loadingSpinner = '<div class="paginationbtn"><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif" style="vertical-align:middle;">loading campaign events</div>',
            loadData = function(callback){
                var loadOptions = $.extend({}, paginationOptions);
                $(".paginationNavigation").html(loadingSpinner);
                $.ajax({
                    url: "<?php echo $CurrentServer;?>CampaignEvents",
                    type: "GET",
                    data: loadOptions,
                    success: function(data){
                        eventList.html(data);
                        if ($.isFunction(callback)){
                            callback(data);
                        }
                    }
                });
            },
            searchingSpinner = '<div class="indexfilter-searching "><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif" style="vertical-align:middle;">Searching...</div>',
            buildFilterText = function(formData){
                var filterTemplate = $("#search-message-template").html(),
                    filterText = "";
                if (formData.KW){
                    filterText += formData.KW;
                }
                if (formData.term){
                    if (filterText){
                        filterText += ", ";
                    }
                    filterText += formData.term;
                }
                if (filterText){
                    return filterTemplate.replace(/{filterText}/g, filterText);
                }
                return "";
            },
            submitSearch = function(){
                var searchForm = eventSearch.find("#indexFilterForm"),
                    filterAlert = searchForm.find("#filter-alert"),
                    searchData = searchForm.serializeObject();
                paginationOptions = $.extend({}, paginationDefaults, searchData);
                filterAlert.html(searchingSpinner);
                loadData(function(data){
                    var noSearchResults = eventList.find("#no-search-results");
                    filterAlert.html(buildFilterText(searchData));
                    if (noSearchResults.length){
                        noSearchResults.html("<p>No events found. Click &ldquo;remove filter&rdquo; to show all events, or just try a different search!</p>");
                    }
                });
            },
            windowResizeTimeout;

        $(window).on("resize load", function(e){
            if (windowResizeTimeout){
                clearTimeout(windowResizeTimeout);
            }
            windowResizeTimeout = setTimeout(function(){
                // set the min-height of the left column to the height of the right column
                if ($(document).width() > 940){
                    $(".campaign-left").css("min-height", $(".campaign-right").height());
                } else {
                    $(".campaign-left").css("min-height", "0");
                }
            }, 300);
        });

        eventList.on("click", ".event-nav", function(e){
            var self = $(this);
            e.preventDefault();
            paginationOptions.startIndex = self.attr("data-start");
            paginationOptions.pageLength = self.attr("data-length");
            $(".paginationNavigation").html(loadingSpinner);
            loadData();
        });

        buttonBlock.on("click", "#sign-up-button", function(e){
            var selectedType = buttonBlock.find("input[name='join_type']:checked").val();
            e.preventDefault();
            if (selectedType == 'business'){
                if (businessSignupLink){
                    window.location.href = businessSignupLink;
                } else {
                    alert("We're not quite ready for businesses to sign up. Try again soon!");
                }
            } else {
                window.location.href = nonprofitSignupLink;
            }
        });

        buttonBlock.on("contextmenu", "#sign-up-button", function(e){
            return false;
        });

        $.ajax({
            url: "<?php echo $CurrentServer;?>SocialMessageSamples",
            type: "GET",
            data: {
                relevant_entity: 'campaign',
                relevant_entity_id: <?php echo $webPageModel->getCampaignId();?>,
                relevant_url: "<?php echo $CurrentServer."campaign/".urlencode($webPageModel->getCampaignName()).$webPageModel->getPidLink(); ?>"
            },
            success: function(data){
                $("#wrapper").after($(data));
                if (window.location.hash === "#social"){
                    $(".social-media-sample-handle").trigger("click");
                }
            }
        });

        $.ajax({
            url: "<?php echo $CurrentServer."CampaignMap?tagName=".$webPageModel->getCampaignName()."&render=wide";?>",
            type: "GET",
            success: function(data){
                campaignMap.html(data);
            }
        });

        $.ajax({
            url: "<?php echo $CurrentServer."CampaignSearch?tagName=".$webPageModel->getCampaignName();?>",
            type: "GET",
            success: function(data){
                eventSearch.html(data);
                $("#indexFilterForm").submit(function(e){
                    e.preventDefault();
                    submitSearch();
                });
            }
        });

        loadData(function(data){
            // on page load, if no events, hide search panel
            if (!eventList.find(".event_text").length){
                eventSearch.hide();
            }
        });

        $("body").on("click", ".print-link", function(e){
            var  searchForm = eventSearch.find("#indexFilterForm"),
                searchData = searchForm.serialize();
            window.open("<?php echo $CurrentServer."CampaignList/".$webPageModel->getCampaignName()."?";?>" + searchData + "#participating-businesses" );
        });
    });
</script>