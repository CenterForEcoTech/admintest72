<?php
/* @var $viewModel CharitySignUpViewModel */
echo $viewModel->getTagHeader($CurrentServer, $campaignImageUrlPath);
?>
<?php
// Use Case: User is logged in and not yet attached to campaign:
if ($viewModel->joinedCampaign){ ?>
    <h3>You&rsquo;ve signed up!</h3>
    <p>
        Thank you for supporting this campaign.
        You&rsquo;ll receive additional information, including sample email and social media messaging, in the near future.
        Please contact <?php echo $SiteName;?> at 855-MY-CAUSE or <?php echo encode_email("campaigns@causetown.org");?> for immediate assistance.
    </p>
<?php }

// Use Case: User is logged in and had been previously attached to campaign:
 else if ($viewModel->alreadyOnCampaign){ ?>
    <h3>You&rsquo;re already signed up!</h3>
    <p>
        Thank you for supporting this campaign.
        You&rsquo;ll receive additional information, including sample email and social media messaging, in the near future.
        Please contact <?php echo $SiteName;?> at 855-MY-CAUSE or <?php echo encode_email("campaigns@causetown.org");?> for immediate assistance.
    </p>
<?php }

// Use Case: User is not logged in at all, or is not a charity (most common case)
else if (!isOrganization()){ ?>

    <div>
        <p>
            To join this campaign, simply search for your nonprofit organization below and then register with <?php echo $SiteName;?>.
            (If you are already registered with <?php echo $SiteName;?>, you will have the option to log in.)
        </p>
        <p>
            Once you have joined the campaign, you will receive an electronic toolkit to help you maximize your participation,
            including messaging to help you share the campaign
            and a flyer you can distribute to your supporters.
            If you have any trouble finding your organization, please <a href="https://causetown.zendesk.com/account/dropboxes/220049508" onclick="script: Zenbox.show(); return false;" title="Contact Us">contact us</a>.
        </p>
    </div>
<script type="text/javascript">
    var CETDASH = CETDASH || {};
    CETDASH.LoginForm = CETDASH.LoginForm || {};
    CETDASH.LoginForm.successHandler = function(data){
        location.href = location.href;
    } // end success handler

    CETDASH.NonprofitRegistration = CETDASH.NonprofitRegistration || {};
    CETDASH.NonprofitRegistration.campaign = '<?php echo $_GET['campaign'];?>';
</script>
<?php
    include("_nonprofitRegistration.php");
} else { ?>
    <h3>You&rsquo;re almost done!</h3>
    <p>Now that you have logged in, please confirm that you want to join this campaign by clicking the button below.</p>
    <div class="btn"><a id="join-the-campaign" href="#" class="do-not-navigate">Join the Campaign!</a></div>
    <script type="text/javascript">
        $(function(){
            $("#join-the-campaign").click(function(e){
                e.preventDefault();
                location.href = location.href;
            });
        });
    </script>
<?php
}

