<?php
/**
 * Content that displays donation summary report
 */

?>
<div id="checks-summary" class="hidden">
    <h4>Checks Mailed to You</h4>
    <span class="alert" style="display:inline;">Loading...</span>
    <div class="message hidden"></div>

    <table id="checks-list" class="sales-data hidden two columns">
        <thead>
        <tr>
            <th>Date</th>
            <th>Amount <a href="#checks-help" class="help-modal-icon colorbox-inline">Help on checks received</a></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="seven columns">
    <p class="funds-due hidden"><strong>Funds still due to you: $<span>0.00</span></strong></p>
    <p class="funds-disbursible hidden">Funds <?php echo $SiteName;?> has in hand: $<span>0.00</span></p>
    <p class="funds-receivable hidden">Funds <?php echo $SiteName;?> is waiting to receive from businesses: $<span>0.00</span></p>
    </div>
</div>

<div style="display:none;">
    <div id="checks-help" class="help-modal">
        <h3>Details on Checks Received</h3>
        <p>
            Click to see a list of businesses and donations included in the check.
        </p>
    </div>
    <div id="check-detail-section" class="form-modal">
        <label>Check Date: <span class="check-date"></span></label>
        <label>Total: $<span class="check-amount"></span></label>
        <table id="check-detail" class="sales-data">
            <thead>
            <tr>
                <th>Merchant</th>
                <th>Donation Amount</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<script type="text/template" id="check-row-tmpl">
    <tr>
        <td class="numeric">{date}</td>
        <td class="numeric">${amount}</td>
    </tr>
</script>
<script type="text/template" id="check-detail-row-tmpl">
    <tr>
        <td><a href="{pidLink}" target="_blank">{businessName}</a></td>
        <td class="numeric">${donationAmount}</td>
    </tr>
</script>
<script type="text/javascript">
    $(function(){
        var checksTable = $("#checks-list"),
            pendingFundsCalculated = false,
            pendingFundsInterval = setInterval(function(){
                var totalDonations = $("#donation-data").data("grand-total"),
                    totalCheckAmount = checksTable.data("check-total"),
                    fundsReceivable = checksTable.data("funds-receivable"),
                    fundsDue,
                    fundsDisbursible,
                    dueDataWrapper = $("#checks-summary .funds-due"),
                    receivableDataWrapper = $("#checks-summary .funds-receivable"),
                    disbursibleDataWrapper = $("#checks-summary .funds-disbursible");

                if (totalDonations !== undefined && totalCheckAmount !== undefined && totalDonations){
                    fundsDue = (totalDonations - totalCheckAmount).toFixed(2);
                    fundsDisbursible = (fundsDue - fundsReceivable).toFixed(2);

                    dueDataWrapper.find("span").html(fundsDue);
                    dueDataWrapper.show();

                    receivableDataWrapper.find("span").html(fundsReceivable);
                    receivableDataWrapper.show();

                    disbursibleDataWrapper.find("span").html(fundsDisbursible);
                    disbursibleDataWrapper.show();
                    pendingFundsCalculated = true;
                    $("#checks-summary").show();
                }
                // loop until elements on page load
                if (pendingFundsCalculated){
                    clearInterval(pendingFundsInterval);
                }
            }, 500),
            loadSummaryData = function(){
                $.ajax({
                    url: '<?php echo $CurrentServer;?>ApiOrganizations',
                    data: {
                        action : "checks_report"
                    },
                    type: "GET",
                    success: function(data){
                        var reportWrapper = $("#checks-summary"),
                            reportTable = reportWrapper.find("#checks-list"),
                            reportBody = reportTable.find("tbody"),
                            messageDiv = reportWrapper.find("div.message"),
                            rowTemplate = $("#check-row-tmpl").html(),
                            runningTotal = 0.00, htmlString, rowElement;
                        reportWrapper.find("span.alert").html(""); // clear alert
                        reportBody.html(""); // clear current rows
                        messageDiv.hide();
                        reportTable.data("check-total", data.total);
                        reportTable.data("funds-receivable", parseFloat(data.fundsPending).toFixed(2));
                        if (data.checks && data.checks.length){
                            $.each(data.checks, function(index, item){
                                htmlString = rowTemplate.replace(/{date}/g, item.check_date)
                                        .replace(/{amount}/g, item.check_amount);
                                rowElement = $(htmlString);
                                rowElement.data("record", item);
                                rowElement.addClass("has-detail");
                                reportBody.append(rowElement);
                                runningTotal += parseFloat(item.check_amount);
                            });
                            // add total row
                            htmlString = rowTemplate.replace(/{date}/g, "Total").replace(/{amount}/g, runningTotal.toFixed(2));
                            rowElement = $(htmlString);
                            rowElement.addClass("grand-total");
                            reportBody.append(rowElement);
                            reportTable.show();
                            reportWrapper.show();
                        } else {
                            messageDiv.html("No checks have been disbursed yet.");
                            messageDiv.show();
                        }
                    }
                });
            };

        checksTable.on("click", "tbody tr.has-detail", function(e){
            var rowElement = $(this),
                record = rowElement.data("record"),
                batchId = record.batch_id,
                checkDate = record.check_date,
                checkAmount = record.check_amount;
            e.preventDefault();

            $.ajax({
                url: '<?php echo $CurrentServer;?>ApiOrganizations',
                data: {
                    action : "check_detail_report",
                    batchId : batchId
                },
                type: "GET",
                success: function(data){
                    var detailSection = $("#check-detail-section"),
                        reportWrapper = $("#receipts"),
                        reportTable = reportWrapper.find("#check-detail"),
                        reportBody = reportTable.find("tbody"),
                        rowTemplate = $("#check-detail-row-tmpl").html();
                    reportBody.html(""); // clear current rows
                    detailSection.find("label .check-date").html(checkDate);
                    detailSection.find("label .check-amount").html(checkAmount);
                    if (data.checkData && data.checkData.length){
                        $.each(data.checkData, function(index, item){
                            var pidLink = "<?php echo $CurrentServer."ct/";?>" + item.merchantPid,
                                htmlString = rowTemplate.replace(/{pidLink}/g, pidLink)
                                    .replace(/{businessName}/g, item.merchant_name)
                                    .replace(/{donationAmount}/g, item.merchant_amount);
                            reportBody.append(htmlString);
                        });
                        $.colorbox({
                            inline:true,
                            opacity: 0.8,
                            href: "#check-detail-section",
                            onComplete: function(){
                                window.setTimeout('$.colorbox.resize()',10);
                            }
                        });
                    }
                }
            });
        });

        loadSummaryData();
    });
</script>