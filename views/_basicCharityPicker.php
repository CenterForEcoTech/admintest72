<?php
// set defaults
if (!isset($NextStepValue)){$NextStepValue = "Next >";}
if (!isset($ParentTarget)){$ParentTarget = false;}
if (!isset($favoriteCharities)){
    // if the merchant had favorite charities, they can be passed in as an array
    $favoriteCharities = array();
}
if (empty($showHelpIcons)){
    $showHelpIcons = false;
}
if (!$showHelpIcons){
    ?>
    <style type="text/css">
        a.help-modal-icon {display:none;}
    </style>
<?php } ?>

<?php if (!isAjaxRequest()) {
    $Footer_excludeJqueryUi = true;
}
    ?>
    <script src="<?php echo $CurrentServer;?>js/jquery-ui-1.8.20.custom.min.js"></script>

<div>
    <form class="override_skel" method="post" id="FindOrgForm" action="<?php echo $PreferredOrganizationFormAction;?>" name="FindOrgForm"<?php if ($ParentTarget){echo " target=\"_top\"";}?>>

        <label for="charity-search-box">
            <div>
                Search for a charity:
                <a href="#alt-search-help" class="help-modal-icon colorbox-inline">Help on alternate search</a>
            </div>
        </label>

        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                sourceType : "POST",
                showState: true,
                useSelect2: ('<?php echo !isAjaxRequest();?>') ? true : false,
                minLength: 3,
                pageLength: 10,
                onSearch: function(event,ui){
                    var einField = $("#EIN"),
                        orgNameField = $("#OrgName"),
                        orgIdField = $("#OrgID"),
                        einLookupResults = $("#EINLookupResults"),
                        lookupErrorElement = $("#lookup-error"),
                        autoCompletedFlag = $("#auto-completed");

                    einLookupResults.html("None Selected");
                    lookupErrorElement.html("");
                    orgNameField.val('');
                    orgIdField.val('');
                    einField.val('');
                    autoCompletedFlag.val('');
                },
                onSelect: function( event, ui ) {
                    var searchInput = $(this),
                        einElement = $("#EIN"),
                        orgIdField = $("#OrgID"),
                        orgNameElement = $("#OrgName"),
                        einLookupResults = $("#EINLookupResults"),
                        charityRecord = ui.item.record,
                        ein = charityRecord.ein,
                        formattedEin = ein ? ein.replace(/(\d{2})(\d{7})/, '$1-$2') : "",
                        displayValue = ein ? charityRecord.name + " (EIN #" + formattedEin + ")" : charityRecord.name,
                        autoCompletedFlag = $("#auto-completed");

                    einElement.val(ein);
                    orgNameElement.val(charityRecord.name);
                    $("#FindOrgForm").find("input[name='quickPick']:radio").prop("checked", false);
                    einLookupResults.html(displayValue);
                    orgIdField.val(charityRecord.orgId);
                    $("#lookup-error").html('');

                    $("#FindOrgForm").find("input[name='quickPick']:radio").prop("checked", false);
                    autoCompletedFlag.val("yes");

                    searchInput.val(ui.item.value);
                    return false;
                }
            };
            // site-wide watermark default
            $(".watermarked").each(function(){
                var self = $(this),
                    watermarkText = self.attr("title");
                if (watermarkText && self.watermark){
                    self.watermark(watermarkText);
                }
            });
        </script>
        <?php include($siteRoot."views/_charityPickerWidget.php");?>
        <div id="lookup-error" class="alert"></div>
        <input type="hidden" name="OrgName" id="OrgName" >
        <input type="hidden" name="OrgID" id="OrgID" >
        <input type="hidden" name="EIN" id="EIN" >
        <input type="hidden" id="auto-completed">
        <?php if (is_array($favoriteCharities) && count($favoriteCharities)){ ?>
            <fieldset style="clear:both;">
                <label>
                    <div>
                        Need a Recommendation?
                        <a href="#quick-pick-help" class="help-modal-icon colorbox-inline">Help on recommendations</a>
                    </div>
                </label>
                <?php foreach($favoriteCharities as $quickPick) {
                    /* @var $quickPick MerchantPreferredOrg */
                    $quickPickHoverText = $quickPick->orgName." - ".$quickPick->orgAddress;
                    if ($quickPick->orgEin){
                        $quickPickHoverText .= " (EIN #".preg_replace("/(\d{2})(\d{7})/", "$1-$2", $quickPick->orgEin).")";
                    }
                    ?>
                    <label  title="<?php echo htmlentities($quickPickHoverText);?>">
                        <input type="radio" name="quickPick" value="<?php echo $quickPick->orgID;?>" data-name="<?php echo $quickPick->orgName;?>" data-address="<?php echo htmlentities($quickPick->orgAddress);?>" data-ein="<?php echo $quickPick->orgEin;?>">
                        <?php echo $quickPick->orgName?>
                    </label>
                <?php } // end each favorite charity?>
            </fieldset>
<?php } // end if favorite charities?>
        <div style="clear:both;">
            <label>
                <div>Selected Charity:</div>
                <span id="EINLookupResults">None Selected</span>
            </label>
            <?php if ($shareWithNonprofit !== null) { ?>
                <label class="checkbox" title="This changes your profile setting.">
                    <input type="checkbox" name="shareWithNonprofit" value="1" <?php echo $shareWithNonprofit ? "checked" : ""?>>
                    <span class="checkbox-label">Share my name and email with nonprofits when I designate them to receive a portion of my purchases</span>
                </label>
            <?php } ?>

            <?php if ($NextStepValue == "Submit"){ ?>
                <div class="btn next-step-button">
                    <input type="submit" value="Submit">
                </div>
            <?php } else {?>
                <div class="step_btn next-step-button">
                    <span>Step 1</span>
                    <a href="#" title="next"><?php echo $NextStepValue;?></a>
                </div>
            <?php }?>
        </div>
    </form>

</div>

<div style="display:none;">
    <div id="alt-search-help" class="help-modal">
        <h3>Search for a Charity</h3>
        <p>
            You may begin typing either the name or the EIN number. If there are matching charities already registered with <?php echo $SiteName; ?>,
            or an IRS public charity, they will be presented to you in a drop down for your selection.
        </p>
        <p>
            If you type in a complete valid EIN number, the IRS database will be searched for a valid public charity.
        </p>
    </div>
    <div id="quick-pick-help" class="help-modal">
        <h3>Need a recommendation?</h3>
        <p>A few selected charities are listed below. These are charities that the business recommends as well as charities that may have participated in promotions with the business.</p>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var EINForm = $("#FindOrgForm"),
            einField = $("#EIN"),
            orgNameField = $("#OrgName"),
            orgIdField = $("#OrgID"),
            einLookupResults = $("#EINLookupResults"),
            lookupErrorElement = $("#lookup-error"),
            nextStepElement = $(".next-step-button"),
            autoCompletedFlag = $("#auto-completed"),
            trySubmit = function(){
                var einValue = einField.val().replace('-',''),
                    orgNameValue = orgNameField.val(),
                    possibleEinValue = orgNameValue.replace('-',''),
                    orgIdValue = orgIdField.val();

                if (autoCompletedFlag.val()){
                    EINForm.submit();
                    return true;
                }

                if ($.isNumeric(possibleEinValue) && possibleEinValue.length === 9){
                    // do the lookup now!
                    $.ajax({
                        url: "<?php echo $CurrentServer;?>/ApiCharitySearch",
                        type: "POST",
                        data: {
                            action: 'ein-lookup',
                            ein: possibleEinValue
                        },
                        success: function(data){
                            if (data.orgId){
                                orgIdValue = data.orgId;
                                orgIdField.val(orgIdValue);
                            }
                            if (data.orgName){
                                orgNameValue = data.orgName;
                                orgNameField.val(orgNameValue);
                                einLookupResults.html(orgNameValue);
                            }
                            if (data.ein){
                                einValue = data.ein;
                                einField.val(einValue);
                                einLookupResults.append(" (" + einValue + ")");
                            }

                            if (data.message.length){
                                if (data.message == data.notfoundmessage){
                                    lookupErrorElement.html(data.message);
                                    einLookupResults.html('');
                                    orgNameField.val('');
                                    orgNameValue = '';
                                    einField.val('');
                                    einValue = '';
                                    orgIdField.val('');
                                    orgIdValue = '';
                                    orgNameField.focus();
                                }else{
                                    EINForm.submit();
                                    return true;
                                }
                            } else {
                                EINForm.submit();
                                return true;
                            }
                        },
                        dataType: 'json'
                    });
                }

                if ($.isNumeric(orgIdValue) || einValue.replace('-','').length === 9){
                    EINForm.submit();
                    return true;
                }
            };

        nextStepElement.click(function(e){
            e.preventDefault();
            trySubmit();
        });

        //run the trySubmit if just the enter key is pressed on the form if they knew the EIN already
        nextStepElement.keypress(function(e) {
            if(e.which === 13) {
                e.preventDefault();
                trySubmit();
            }
        });

        EINForm.on("change", "input[name='quickPick']:radio", function(e){
            var element = $(this),
                orgId = element.val(),
                orgName = element.attr("data-name"),
                orgAddress = element.attr("data-address"),
                orgEin = element.attr("data-ein"),
                lookupResult = orgName + " - " + orgAddress + " " + (orgEin ? "(EIN #" + orgEin.replace(/(\d{2})(\d{7})/, '$1-$2') + ")" : "");
            if (element.is(":checked")){
                lookupErrorElement.html("");
                orgIdField.val(orgId);
                einField.val(orgEin);
                orgNameField.val(orgName);
                autoCompletedFlag.val('');
                einLookupResults.html(lookupResult);
            }
        });
    });

</script>