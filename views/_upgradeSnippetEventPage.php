<?php
/**
 * This snippet displays only on the event form above the submit buttons and is expected to allow a merchant
 * to upgrade to a better plan.
 */
if (isMerchant()){
    include_once($dbProviderFolder."MemberProvider.php");
    $merchantMemberProvider = new MerchantMemberProvider($mysqli);
    $criteria = new MerchantProfilesCriteria();
    $criteria->id = getMerchantId();
    $results = $merchantMemberProvider->get($criteria);
    if (count($results->collection) == 1){
        $rateAwareVenue = $results->collection[0];
        $upgradeReady = true;
        if ($rateAwareVenue->billingCode){
            $referralCodeRate = new ReferralCodeRate(
                $rateAwareVenue->billingCode,
                $rateAwareVenue->validFrom,
                $rateAwareVenue->validTo,
                ReferralCodeRate::getRateTypeFromString($rateAwareVenue->billingType),
                $rateAwareVenue->billingRate
            );
        } else {
            $referralCodeRate = ReferralCodeRate::getDefaultRate();
        }
        $upgradeReady = $referralCodeRate->isUpgradeReady();
        if ($upgradeReady){ ?>

<p><?php echo "The ".$SiteName." fee for this event is ".$referralCodeRate->getTransactionFeePercentage()."% of sales generated.";?> <a href="<?php echo $CurrentServer;?>upgrade" target="_blank">Eliminate this fee and enjoy more features with a subscription plan</a>.</p>

<?php
        } // upgradeReady
    } // merchant record retrieved
} // is a logged in merchant