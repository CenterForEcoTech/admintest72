<?php

// setup model
include_once($rootFolder."_setupGeolookupConnection.php");
if (empty($geoConn->connect_error)){
    include_once($dbProviderFolder."GeoLookupProvider.php");
    $geoLookupProvider = new GeoLookupProvider($geoConn);
}
$model = new EventSearchModel($geoLookupProvider);
$model->setDefaultSize(3);

// handle URL parameters in case this is a bookmarked search
$model->setRequest($_GET);
?>
<div>
    <form id="indexFilterForm" class="override_skel hidden">
        <div class="message alert info">
            <span></span>&nbsp;<a class="indexfilter-resetlink hidden">remove filter</a>
            <span class="indexfilter-processing hidden">
                <img src="<?php echo $CurrentServer;?>images/loadingSearch.gif" style="vertical-align:middle;" height="16px">
                <span class="init">Searching for your next community event</span>
                <span class="ongoing hidden">Retrieving results...</span>
            </span>
        </div>
        <label class="six columns filter-keyword">
            <div>Search for <span class="help">(e.g. Pizza Gallery, RCC Preschool)</span></div>
				<span class="highlight">
					<input type="text" name="KW" value="<?php echo htmlspecialchars($model->keyword);?>">
				</span>
        </label>
        <label class="five columns filter-zip">
            <div>Near <span class="help">(City, State or Zip)</span></div>
				<span class="highlight">
					<input type="text" name="L" title="Enter your location to sort by distance" value="<?php echo htmlspecialchars($model->cityStateZip);?>">
				</span>
        </label>
        <label class="two columns filter-pagelength">
            <div>View</div>
            <span class="highlight">
                <select name="PL" class="submit-on-change">
                    <?php
                    foreach ($model->sizeArray as $value => $text) {
                        $selected = ($model->size == $value) ? "selected" : "";
                        echo '<option value="'.$value.'" '.$selected.'>'.$text.'</option>';
                    }?>
                </select>
            </span>
        </label>
        <label class="one column filter-go">
            <div>&nbsp;</div>
                <span class="round-green-button">
                    <span>Go</span>
                </span>
        </label>
    </form>
</div>
<div id="events_section" class="twelve columns">
    <ul id="event-list">
    </ul>
</div>