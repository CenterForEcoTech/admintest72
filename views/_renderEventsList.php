<?php
/**
 * Renders the find events results as HTML.
 * Expects the following:
 *
 * @var $upcomingEvents array of EventRecord
 *
 * See UpcomingEventsIndexed as an example
 */
?>
<?php if ($showPagination){ ?>
<div id="pagination-top" class="pagination-buttons">
    <?php if ($startIndex > 0) {?>
    <span class="btn">
			<a href="#" class="event-nav prev button" data-start="<?php echo $startIndex - $pageLength; ?>" data-length="<?php echo $pageLength; ?>"><< Previous <?php echo $pageLength;?></a>
		</span>
    <?php }
    if ($hasMore){?>
        <span class="btn">
				<a href="#" class="event-nav next button" data-start="<?php echo $startIndex + $pageLength; ?>" data-length="<?php echo $pageLength?>">Next <?php echo $pageLength;?> >></a>
			</span>
        <?php } ?>
    <div class="loading-spinner hidden"><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif">loading</div>
</div>
<div class="clear"></div>
<?php } // show pagination ?>


<?php foreach($upcomingEvents as $currentEventRecord) {
    include("_eventBrief.php");
} ?>

<?php if ($showPagination){ ?>
<div id="pagination-bottom" class="pagination-buttons">
    <?php if ($startIndex > 0) {?>
    <span class="btn">
		<a href="#" class="event-nav prev button" data-start="<?php echo $startIndex - $pageLength; ?>" data-length="<?php echo $pageLength; ?>"><< Previous <?php echo $pageLength;?></a>
		</span>
    <?php }
    if ($hasMore){
        ?>
        <span class="btn">
			<a href="#" class="event-nav next button" data-start="<?php echo $startIndex + $pageLength; ?>" data-length="<?php echo $pageLength?>">Next <?php echo $pageLength;?> >></a>
		</span>
        <?php } ?>
    <div class="loading-spinner hidden"><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif">loading</div>
</div>
<?php } // show pagination ?>
<?php
//$ShowIndexBadge should always be false because we are not required to show it based on our TOS from Indexden
if ($Config_IndexingService == "IndexDen" && $ShowIndexBadge){?>
<div class="badgeText">
    <a href="http://indexden.com" target="indexDen"><img class="badgeImage" src="<?php echo $CurrentServer;?>images/indexDenBadge.png"></a>
</div>
<?php }?>