<?php
// TODO: refactoring: integrate with security functions and permissions
// TODO: refactoring when needed: integrate with database

class PricingFeatureV1 implements IPricingFeature{
    const TRANSACTION_FEE = "Transaction Fee";
    const CUSTOMER_CHOICE = "Customer choice";
    const ONLINE_PROMOTION = "Online promotion";
    const PRINT_FLYERS = "Print flyers";
    const JOIN_CAMPAIGNS = "Join Campaigns";
    const FEATURED_CAUSE = "Feature 1 charity";
    const REPORTING = "Reporting";
    const FLAT_DONATION = "Flat Donation";
    const CUSTOM_PROFILE = "Custom Profile Page";
    const MAX_EVENTS = "Events per month";
    const TECH_SUPPORT = "Tech Support";
    const CONSULTING = "Consulting";

    public static $FEATURES_ARRAY = array(
        self::TRANSACTION_FEE,
        self::CUSTOMER_CHOICE,
        self::ONLINE_PROMOTION,
        self::PRINT_FLYERS,
        self::JOIN_CAMPAIGNS,
        self::FEATURED_CAUSE,
        self::REPORTING,
        self::FLAT_DONATION,
        self::CUSTOM_PROFILE,
        self::MAX_EVENTS,
        self::TECH_SUPPORT,
        self::CONSULTING
    );

    public static function getSupportedFeatures(){
        return self::$FEATURES_ARRAY;
    }

    public $name;
    public $value;
    public function __construct($name, $value){
        $this->name = $name;
        $this->value = $value;
    }

    public function getCheckedClass(){
        if ($this->value == "yes"){
            return "checked";
        }
        if ($this->value == "no"){
            return "unchecked";
        }
        return "";
    }

    public function getValue(){
        if (empty($this->value)){
            return "&nbsp;";
        }
        if ($this->value == "0%"){
            return "&ndash;";
        }
        return $this->value;
    }
}
$basicPlanFeatures = array(
    new PricingFeatureV1(PricingFeatureV1::TRANSACTION_FEE, "3%"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOMER_CHOICE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::ONLINE_PROMOTION, "yes"),
    new PricingFeatureV1(PricingFeatureV1::PRINT_FLYERS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::JOIN_CAMPAIGNS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::FEATURED_CAUSE, "no"),
    new PricingFeatureV1(PricingFeatureV1::REPORTING, "no"),
    new PricingFeatureV1(PricingFeatureV1::FLAT_DONATION, "no"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOM_PROFILE, "no"),
    new PricingFeatureV1(PricingFeatureV1::MAX_EVENTS, ""),
    new PricingFeatureV1(PricingFeatureV1::TECH_SUPPORT, "Email only"),
    new PricingFeatureV1(PricingFeatureV1::CONSULTING, "")
);
$basicPlanTarget = "For businesses that are just getting started";
$basicPrice = "pay per use";
$basicRate = ReferralCodeRate::getDefaultRate();
$basicPlan = new PricingPlanModel("Basic", $basicPlanTarget, $basicPrice, $basicRate, "PricingFeatureV1");
$basicPlan->features = $basicPlanFeatures;

$monthlyPlanFeatures = array(
    new PricingFeatureV1(PricingFeatureV1::TRANSACTION_FEE, "0%"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOMER_CHOICE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::ONLINE_PROMOTION, "yes"),
    new PricingFeatureV1(PricingFeatureV1::PRINT_FLYERS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::JOIN_CAMPAIGNS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::FEATURED_CAUSE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::REPORTING, "yes"),
    new PricingFeatureV1(PricingFeatureV1::FLAT_DONATION, "yes"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOM_PROFILE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::MAX_EVENTS, "10"),
    new PricingFeatureV1(PricingFeatureV1::TECH_SUPPORT, "Email/Phone"),
    new PricingFeatureV1(PricingFeatureV1::CONSULTING, "")
);
$monthlyPlanTarget = "For businesses that have hit their stride";
$monthlyPrice = "$49/month";
$monthlyRate = new ReferralCodeRate("monthly","","",ReferralCodeRate::FlatRateType, 49);
$monthlyPlan = new PricingPlanModel("Pro", $monthlyPlanTarget, $monthlyPrice, $monthlyRate, "PricingFeatureV1");
$monthlyPlan->features = $monthlyPlanFeatures;

$annualPlanFeatures = array(
    new PricingFeatureV1(PricingFeatureV1::TRANSACTION_FEE, "0%"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOMER_CHOICE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::ONLINE_PROMOTION, "yes"),
    new PricingFeatureV1(PricingFeatureV1::PRINT_FLYERS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::JOIN_CAMPAIGNS, "yes"),
    new PricingFeatureV1(PricingFeatureV1::FEATURED_CAUSE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::REPORTING, "yes"),
    new PricingFeatureV1(PricingFeatureV1::FLAT_DONATION, "yes"),
    new PricingFeatureV1(PricingFeatureV1::CUSTOM_PROFILE, "yes"),
    new PricingFeatureV1(PricingFeatureV1::MAX_EVENTS, "unlimited"),
    new PricingFeatureV1(PricingFeatureV1::TECH_SUPPORT, "Priority Email/Phone"),
    new PricingFeatureV1(PricingFeatureV1::CONSULTING, "1 event per month")
);
$annualPlanTarget = "Be the talk of the town!";
$annualPrice = "$399/year";
$annualRate = new ReferralCodeRate("annually","","",ReferralCodeRate::AnnualRateType, 399);
$annualPlan = new PricingPlanModel("Premium", $annualPlanTarget, $annualPrice, $annualRate, "PricingFeatureV1");
$annualPlan->isBestValue = true;
$annualPlan->features = $annualPlanFeatures;

$pageModel = new PricingPageModel();
$pageModel->addPlan($annualPlan);
$pageModel->addPlan($monthlyPlan);
$pageModel->addPlan($basicPlan);

$upgradeReady = false;
$signUpReady = false; // TODO: make true for non-logged-in users or non merchants??
if (isMerchant()){
    $signUpReady = false;
    include_once($dbProviderFolder."MemberProvider.php");
    $merchantMemberProvider = new MerchantMemberProvider($mysqli);
    $criteria = new MerchantProfilesCriteria();
    $criteria->id = getMerchantId();
    $results = $merchantMemberProvider->get($criteria);
    if (count($results->collection) == 1){
        $rateAwareVenue = $results->collection[0];
        $upgradeReady = true;
        if ($rateAwareVenue->billingCode){
            $referralCodeRate = new ReferralCodeRate(
                $rateAwareVenue->billingCode,
                $rateAwareVenue->validFrom,
                $rateAwareVenue->validTo,
                ReferralCodeRate::getRateTypeFromString($rateAwareVenue->billingType),
                $rateAwareVenue->billingRate
            );
        } else {
            $referralCodeRate = ReferralCodeRate::getDefaultRate();
        }
        $upgradeReady = $referralCodeRate->isUpgradeReady();
    }
}
?>

<style type="text/css">
    #price-page .no-wrap{
        white-space:nowrap;
    }
    #price-page table.pricing-tables.secondary tbody tr th,
    #price-page table.pricing-tables.secondary .corner,
    #price-page table.pricing-tables.secondary col.features,
    #price-page table.pricing-tables caption
    {
        display:none;
    }
    #price-page table.pricing-tables .corner{
        border-top:none;
        border-left:none;
    }
    #price-page table.pricing-tables tr td,
    #price-page table.pricing-tables tr th{
        height: 20px;
        border:1px dotted #B8B8B8;
    }
    #price-page table.pricing-tables tr.on-hover{
        border-top:1px solid #B8B8B8;
        border-bottom:1px solid #B8B8B8;
    }
    #price-page table.pricing-tables tbody tr th{
        font-weight:normal;
        font-size:0.7em;
        text-align:left;
        padding-left:0.5em;
        font-family:tahoma;
    }
    #price-page table.pricing-tables td {
        text-align: center;
        font-size:0.7em;
        width:200px;
        margin:0 auto;
    }
    #price-page table.pricing-tables .checked{
        position:relative;
        vertical-align:top;
    }
    #price-page table.pricing-tables .checked span{
        position:absolute;
        left:45%;
        width: 20px;
        height: 20px;
        background: url(../images/check.png) no-repeat center center;
        text-indent: -9999px;
        background-size: 100% 100%;
    }
    #price-page table.pricing-tables .unchecked span{
        display:none;
    }
    #price-page table.pricing-tables thead th{
        height:180px;
    }
    #price-page table.pricing-tables thead th h4{
        font: bold 40px 'Yanone Kaffeesatz', sans-serif;
        color: #664b39;
    }
    #price-page table.pricing-tables thead th div{
        font-weight:normal;
        font-size:0.9em;
    }
    #price-page table.pricing-tables thead th div.target-audience{
        height:40px;
        font-size:0.7em;
        padding:0 10px;
        text-align:left;
    }
    #price-page table.pricing-tables thead th div.price-string{
        font: bold 25px 'Yanone Kaffeesatz', sans-serif;
    }
    #price-page table.pricing-tables thead th div.btn{
        margin-top:5px;
    }
    #price-page table.pricing-tables{
        margin-top:20px;
    }
    #price-page table.pricing-tables.best-value{
        margin-top:2px;
    }
    #price-page table.pricing-tables.best-value thead th{
        padding:12px 0 10px 0;
        background-color:#CFDCCB;
        border-top: 1px solid #6d9063;
        border-left:1px solid #6d9063;
        border-right:1px solid #6d9063;
    }
    #price-page table.pricing-tables.best-value thead th div{
        margin-top:6px;
    }
    #price-page table.pricing-tables.best-value thead th h4,
    #price-page table.pricing-tables.best-value thead th div.price-string{
        color: #f57d20;
    }
    #price-page table.pricing-tables.best-value tbody td{
        background-color:#CFDCCB;
        border-left:1px solid #6d9063;
        border-right:1px solid #6d9063;
    }
    #price-page table.pricing-tables.best-value tbody tr.last-row td{
        border-bottom: 1px solid #6d9063;
    }
    #price-page table.pricing-tables.best-value tbody tr.on-hover td{
        background-color:#B3C8AC;
    }
    #price-page .help {
        font-size:1em;
        font-style:normal;
        text-align: center;
    }
    #price-page .faq ul li span{
        display:block;
    }
    #price-page .faq .q{
        font: bold 25px 'Yanone Kaffeesatz', sans-serif;
        color: #664b39;
        margin-top:1em;
    }

    @-moz-document url-prefix() {
        #price-page table.pricing-tables .checked span{
        left:auto;
        }
    }

    @media only screen and (max-width: 959px) and (min-width: 768px) {
        #price-page table.pricing-tables thead th div.target-audience{
            height:50px;
        }
    }

    @media only screen and (max-width: 767px){
        #price-page table.pricing-tables.secondary tbody tr th,
        #price-page table.pricing-tables.secondary col.features,
        #price-page table.pricing-tables.secondary .corner
        {
            display:table-cell;
        }
        #price-page table.pricing-tables,
        #price-page table.pricing-tables.best-value{
            width:100%;
        }
        #price-page table.pricing-tables thead th{
            height:auto;
        }
        #price-page table.pricing-tables thead th div.target-audience{
            height:auto;
        }
    }
</style>
<span id="action-message" class="alert info"></span>
    <p>Upgrade to a subscription plan to save time and money. Premium members get extra help with their events and save nearly $200 per year by paying annually.</p>
<div class="twelve columns alpha omega">
<?php
    $firstTable = true;
    foreach ($pageModel->plans as $pricingPlan) :
    /* @var $pricingPlan PricingPlanModel */?>
<table class="pricing-tables <?php echo $firstTable? "six" : "three";?> columns alpha omega <?php echo $firstTable? "" : "secondary";?> <?php echo $pricingPlan->getTableClass();?>" summary="This table contains the <?php echo $pricingPlan->name;?> pricing plan available for merchants.">
    <caption><?php echo $pricingPlan->name;?> Plan</caption>
    <thead>
    <tr>
        <td class="corner"></td>
        <th>
            <h4><?php echo $pricingPlan->name;?></h4>
            <div class="target-audience"><?php echo $pricingPlan->targetAudience;?></div>
            <div class="price-string"><?php echo $pricingPlan->priceString;?></div>
            <?php if ($signUpReady || $upgradeReady && $pricingPlan->isUpgrade()) :?>
            <div class="btn"><a href="#" data-code="<?php echo $pricingPlan->billingCode;?>"><?php echo $upgradeReady ? "Upgrade Now" : "Sign Up";?></a></div>
            <div class="loading-spinner hidden"><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif"></div>
            <?php endif ?>
        </th>
    </tr>
    </thead>
    <tbody>
<?php
    $rowNumber = 1;
    $lastRow = count($pricingPlan->features);
        foreach($pricingPlan->features as $feature) :
        /* @var $feature PricingFeatureV1 */
            $lastRowClass = $lastRow == $rowNumber ? "last-row" : "";
            ?>
    <tr class="row_<?php echo $rowNumber;?> <?php echo $lastRowClass;?>" data-row-class="row_<?php echo $rowNumber;?>">
        <th scope="row"><?php echo $feature->name;?></th>
        <td class="<?php echo $feature->getCheckedClass();?>"><span><?php echo $feature->getValue();?></span></td>
    </tr>
<?php
        $rowNumber++;
        endforeach ?>
    </tbody>
</table>
<?php
    $firstTable = false;
    endforeach ?>
</div>
<?php if ($upgradeReady){?>
<script type="text/javascript">
$(function(){
    var pricingTables = $(".pricing-tables");

    pricingTables.on("click", "thead th .btn a", function(e){
        var button = $(this),
            billingCode = button.attr("data-code"),
            spinners = $(".loading-spinner"),
            buttons = pricingTables.find("thead th .btn"),
            message = $("#action-message");
        e.preventDefault();
        spinners.show();
        buttons.hide();
        message.html("");
        $.ajax({
            url: '<?php echo $CurrentServer;?>ApiMerchants',
            type: 'POST',
            data: {
                action:'upgrade_plan',
                billing_code: billingCode
            },
            success: function(data){
                spinners.hide();
                message.html("You have successfully upgraded!");
            },
            error: function(){
                buttons.show();
                message.html("There was an error upgrading. Please refresh the page and try again.");
            }
        });
    });

    pricingTables.on("mouseenter", "tbody tr", function(e){
        var row = $(this),
            rowClass = row.attr("data-row-class");
        $("." + rowClass).addClass("on-hover");
        row.addClass("on-hover");
    });

    pricingTables.on("mouseleave", "tbody tr", function(e){
        var row = $(this),
            rowClass = row.attr("data-row-class");
        $("." + rowClass).removeClass("on-hover");
    });
});
</script>
<?php } // end upgrade javascript ?>
<p class="help">Need help choosing? <span class="no-wrap">Call 855-MY-CAUSE</span></p>

<h3 class="hidden">Questions & Answers</h3>
<div class="faq hidden">
    <ul>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
        <li>
            <span class="q">This is a question?</span>
            <span class="a">This is an answer of some brevity. Perhaps a brief sentence or two.</span>
        </li>
    </ul>
</div>