<?php
/**
 * Embed this page as a widget is only allowed for pages that have some sort of "public" view that does not require a user to be logged in.
 */
if ($loginIsRequiredForThisPage){
    // present some kind of message and log if someone is trying to embed a page that is not allowed.
    trigger_error("_embedThis.php being called on a page that requires login: ".$_SERVER['REQUEST_URI'], E_USER_ERROR);
} else {
    $embedUrl = rtrim($CurrentServer,"/").$_SERVER['REQUEST_URI'];
    if (function_exists("embedThis_getEmbedUrl")){
        $embedUrl = embedThis_getEmbedUrl();
    }
?>
    <div style="display:none;">
        <style type="text/css">
            #embed-this-snippet{
                background-color: #f7f7f7;
                max-width: 760px;
                padding: 20px;
            }
            #embed-snippet-display{
                min-height:130px;
            }
            #embed-snippet-display fieldset{
                display:inline-block;
                border:2px groove #eee;
                padding:10px;
            }
            #embed-snippet-display pre{
                font-family:monospace;
                font-size:15px;
            }
        </style>
        <div id="embed-this-snippet">
            <h2>Embed this page</h2>
            <p>To include a version of this <?php echo $SiteName;?> page on your own website or blog just select a suitable size below.
                This will automatically generate an embed code to paste into your blog post or page.
                If you&rsquo;re unsure about the size to choose try the 680x550 option first.</p>
            <form>
                <input type="hidden" name="url" value="<?php echo $embedUrl;?>">
                <select name="size">
                    <option value="400x500">Mobile (400 x 500)</option>
                    <option value="500x300">Mobile Landscape (500 x 300)</option>
                    <option value="680x550" selected>Medium (680 x 550)</option>
                    <option value="950x700">Large (950 x 700)</option>
                </select>
                <input type="button" class="btn" value="submit">
            </form>
            <div id="embed-snippet-display"></div>
        </div>
    </div>
<script type='text/template' id='embed-this-snippet-template'>
<fieldset>
    <legend>Code Snippet</legend>
<pre>
&lt;iframe src="{url}"
width="{width}" height="{height}" style="border:none;" seamless>
&lt;/iframe>
</pre>
</fieldset>
</script>
    <script type="text/javascript">
        $(function(){
            var embedForm = $("#embed-this-snippet form"),
                templateHtml = $("#embed-this-snippet-template").html(),
                displayElement = $("#embed-snippet-display"),
                responsiveParent = embedForm.closest(".columns"),
                actualResponsiveWidth = responsiveParent.width();

            embedForm.on("click.causetown", "input[type='button']", function(e){
                var data = embedForm.serializeObject(),
                    dimensionsArray = data.size.split('x'),
                    height = dimensionsArray[1],
                    width = dimensionsArray[0],
                    snippetText = templateHtml.replace(/{url}/g, data.url).replace(/{height}/g, height).replace(/{width}/g, width);
                e.preventDefault();
                displayElement.html(snippetText);
            });

            $('body').on('click', '.embed-link', function(event){
                var responsiveWidth = actualResponsiveWidth < 700 ? actualResponsiveWidth : 700,
                width = responsiveWidth > 300 ? responsiveWidth - 40 : responsiveWidth,
                options = {
                    inline:true,
                    opacity: 0.8,
                    href: $(this).attr("href"),
                    width: (width) ? width : false,
                    onComplete: function(){
                        window.setTimeout('$.colorbox.resize()',10);
                    },
                    onClosed: function(){
                        displayElement.html("");
                    }
                };
                $.colorbox(options);
                event.preventDefault();
            }); // inline
        });
    </script>
<?php
}