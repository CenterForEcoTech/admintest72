<?php
/**
 * This is a front controller
 */
ob_start();
require_once('Global_Variables.php');
$FullSite = true;

//If doing an Archive then ignore the session variables to figure out which page to display
//the curl_init() in the archive page uses it's own session and needs to be circumvented
if(!$PageName){
	$PageName = $_GET['PageName'].'.php';
}

$displayTemplateFile = $PageName;
if (!file_exists($_SERVER['DOCUMENT_ROOT'].$Config_DevFolder.$PageName)){
	$displayTemplateFile = "_404Body.php";
	header("HTTP/1.0 404 Not Found");
}

if (GetParameter("isModal")){
	include_once("RenderModal.php");
} else if ($PageName == "Index_content.php") {
	include_once("RenderHomePage.php");
} else {
	include_once("RenderPage.php");
}
mysqli_close($conn);
$mysqli->close();
?>