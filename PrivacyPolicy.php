<?php
include_once("Global_Variables.php");

	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Privacy Policy";
		$PageHeader1 = $SiteName." Privacy Policy";

	}
if ($load_getContent){
	// Include WordPress
    include('_initializePrivateCms.php');
    $content = $sitePrivateCms->getPage("privacy_policy");
    echo "<div class='legal wordpress-content'>".$content."</div>";
}
?>