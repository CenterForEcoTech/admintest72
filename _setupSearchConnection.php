<?php

if (!isset($dbConfigSearch)){
    trigger_error("_setupSearchConnection.php This script requires the database configuration for the search database in siteconf.", E_USER_ERROR);
    die(); // no db config for the target db.
}

$searchConn = new mysqli($dbConfigSearch->serv,$dbConfigSearch->user,$dbConfigSearch->pass,$dbConfigSearch->inst);
if ($searchConn->connect_error) {
    trigger_error("_setupSearchConnection.php: Failed to connect to MySQL: ".$searchConn->connect_error, E_USER_ERROR);
}