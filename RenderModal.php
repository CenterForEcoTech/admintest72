<?php
require_once('Header.php');
?>
<div id="wrapper" class="form-modal">
	<div class="container">
		<div id="content">
			<div id="main" class="thirteen columns">
				<?php include($displayTemplateFile); ?>
			</div>
		</div> <!-- End content -->
	</div> <!--End Container-->
</div> <!-- End wrapper -->

<?php
require_once('Footer.php');
?>