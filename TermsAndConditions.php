<?php
include_once("Global_Variables.php");

$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
if ($load_getHeader){
	$PageTitle = "Terms and Conditions";
	$PageHeader1 = $SiteName." Terms &amp; Conditions";
	$pageSubtitle = "";
}
if ($load_getContent){
	// Include WordPress
    include('_initializePrivateCms.php');
    $content = $sitePrivateCms->getPage("terms_and_conditions");
    echo "<div class='legal wordpress-content'>".$content."</div>";
}
?>