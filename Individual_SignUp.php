<?php
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Sign up as an Individual";
		$PageHeader1 = "Shop, Dine, Do Good";
		$pageSubtitle = "It&rsquo;s easy, quick, and FREE to register.";
	}
if ($load_getContent){
	?>
<div id="sign-up-info" class="twelve columns" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
     xmlns="http://www.w3.org/1999/html">
		<div id="consumers" class="four columns user_types">
			<h2><span>Individuals</span></h2>

		</div>
		<div class="seven columns">
            <div id="signup-preamble">

                <p><?php echo $SiteName;?> is free for individuals.</p>

                <p>Shop or dine with local merchants running <?php echo $SiteName;?> events and then pick your favorite charity to receive a percentage of your purchase.</p>

                <p>We show you all the funds you&rsquo;ve generated and causes you&rsquo;ve helped, so you can pat yourself on the back for doing good when you spend.</p>

                <p>Register now for free!</p>
            </div>

            <?php
            $BaseLink = "mobile/mobile_";
            $FormSource = "Web";
            include($BaseLink."Individual_SignUp.php");
            //include_once("facebook_signin.php"); IF THIS IS INCLUDED AGAIN, CHECK THE INCLUDED FILE FOR SESSION HANDLING ISSUES?>
            <p class="help">Already registered? <a href="<?php echo $CurrentServer."Login";?>">Login Here!</a></p>
		</div>
	</div>
<?php }
?>