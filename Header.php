<!DOCTYPE HTML>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<?php
	if (!isset($ModalWindow)) { $ModalWindow = false; } // suppress page title
	if (!isset($PageTitle)) { $PageTitle = $SiteTitle; } // default page title
    if (!isset($PageFbTitle)){ $PageFbTitle = $SiteName;} // default title for facebook
?>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php if (!$ModalWindow) { ?>
	<title><?php echo $PageTitle;?></title>
<?php }?>
	<meta name="description" content="Think CraigsList for your Pets" />
	<meta name="keywords" content="pet supplies, pet food, pet boarding" />

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/base_ct.css">
    <?php if (!$suppressResponsive) {?>
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/skeleton.css">
    <?php } else { ?>
    <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/skeleton-base.css">
    <?php }?>

	<!-- jQuery - the core -->
	<link type="text/css" href="<?php echo $CurrentServer;?>css/jquery-ui-1.8.20.custom.css" rel="stylesheet" />

	<!-- General From Validation -->
	<link type="text/css" href="<?php echo $CurrentServer;?>css/formvalidate.css" rel="stylesheet" />

	<!--Modal Popup. -->
	<link type="text/css" href="<?php echo $CurrentServer;?>css/colorbox/colorbox.css" rel="stylesheet" />

    <?php if (isset($cssHeadInclude)){
        echo $cssHeadInclude;
    }?>

    <!-- Site css -->
    <?php if (!$suppressResponsive) {
    // TODO: modify the "build/deploy" process to concatenate css into single file.
    ?>
    <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/layout.css?x=13">
    <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/layout-responsive.css?x=6">
<?php } else { ?>
    <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/layout.css?x=13">
    <?php }?>


	<!-- fonts
	================================================== -->
    <!--<link href='http<?php echo $secureServer;?>://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200normal,300normal,400normal,700normal|Open+Sans:300normal,400normal' rel='stylesheet' type='text/css'>-->

	<!--[if lt IE 9]>
	<script src="http<?php echo $secureServer;?>://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CurrentServer;?>images/ct_icon3.ico">
	<link rel="img_src" href="<?php echo $CurrentServer;?>images/ctlogo_w200.jpg" />
    <?php if (isset($PageImageArray) && is_array($PageImageArray)) {
        foreach ($PageImageArray as $pageImageUrl){?>
            <link rel="image_src" type="image/jpeg" href="<?php echo $pageImageUrl;?>" />
        <?php }
    }?>

	<!--Facebook Open Graph markup-->
	<meta property="og:title" content="<?php echo $PageFbTitle;?>" />
	<meta property="og:type" content="website" />
    <?php if (isset($PageImageArray) && is_array($PageImageArray)) {
        foreach ($PageImageArray as $pageImageUrl){?>
            <meta property="og:image" content="<?php echo $pageImageUrl;?>" />
        <?php }
    }?>
    <meta property="og:image" content="<?php echo $CurrentServer;?>images/ctlogo_w200.jpg" />
	<meta property="og:site_name" content="<?php echo $SiteName;?>" />
	<?php if (isset($PageFbDescription)){?>
		<meta property="og:description" content="<?php echo htmlspecialchars($PageFbDescription);?>" />
	<?php }?>
	<meta property="og:url" content="<?php echo rtrim($CurrentServer,"/").$_SERVER['REQUEST_URI'];?>" />

    <?php if (isset($twitterMetaTagArray) && is_array($twitterMetaTagArray) && count($twitterMetaTagArray)){?>
        <!-- Twitter Card -->
        <?php foreach ($twitterMetaTagArray as $twitterMetaName => $twitterMetaContent) {?>
            <meta name="<?php echo $twitterMetaName;?>" content="<?php echo $twitterMetaContent;?>">
        <?php } ?>
    <?php } ?>

	<script src="<?php echo $CurrentServer;?>js/jquery-1.7.2.min.js"></script>
	<!--[if lt IE 8]>
	<script src="<?php echo $CurrentServer;?>js/json2.js"></script>
	<![endif]-->

    <?php if ($_GET['render'] == 'wide') { ?>
    <script type="text/javascript">
        var CETDASH = CETDASH || {};
        CETDASH.renderWide = true;
    </script>
    <?php } ?>

</head>