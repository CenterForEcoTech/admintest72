<?php
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Log in";
		$PageHeader1 = "Member Login";

	}
if ($load_getContent){
    $rememberedUsername = $_COOKIE["username"];
    if (!empty($PostArray['user_name'])){
        $rememberedUsername = $PostArray['user_name'];
    }
?>
<div><span style="color:red;" id="MobileAlert"><?php echo SessionManager::getTmpVar('LoginAlert');?>&nbsp;</span>
	<?php SessionManager::unsetTmpVar('LoginAlert'); ?>
	<form class="override_skel" action="<?php echo $CurrentServer;?>Login_Post" method="post" id="UserForm">
		<label>
			<div>Username (email or phone number):</div>
			<span class="highlight">
				<input id="username" class="with-help" type="text" value="<?php echo htmlspecialchars($rememberedUsername);?>" name="user_name" tabIndex="1">
			</span>
			<span class="help">You can log in with your email address or phone number &mdash; whichever one you registered with.</span>
		</label>
		<label class='no-lost-password'>
			<div>Password:</div>
			<span class="highlight">
				<input id="password" type="password" name="password" value="<?php echo htmlspecialchars($PostArray['password']);?>" tabIndex="2">
			</span>
		</label>
		
		<label class='no-lost-password'>
			<input id="remember-me" type="checkbox" name="MobileStaySignedIn" value="yes"<?php if ($_COOKIE["username"]){echo " checked";}?> tabIndex="3" >
			Remember Me
		</label>
		<div id="submit-button" class="btn">
			<input type="submit" id="submit-button-trigger" value="Log In" />
		</div>
        <a class="colorbox-inline" href="#forgot-password-form" style="text-decoration: underline;margin-left:25px;font-size:15px;font-style:italic;">Forgot password?</a>
	</form>
</div>
<div style="display:none;">
    <div id="forgot-password-form" class="form-modal">
        <h4>Forgot your password?</h4>
        <p>Just type in your email or phone number (whichever one you registered with) and we'll send you a link to the reset password page.</p>
        <form class="override_skel">
            <label>
                <div>Username (email or phone number):</div>
                <span class="highlight">
                    <input type="text" name="email" >
                </span>
                <span class="alert"></span>
            </label>
            <div class="btn">
                <a class="button do-not-navigate remind-password" href="#">Get Reset Link</a>
            </div>
            <div class="loading-spinner hidden"><img src="<?php echo $CurrentServer;?>images/loadingSearch.gif"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
	<?php if ($PostArray['postSource']=="User_activate" && $PostArray['user_name'] && $PostArray['password']){?>
		document.getElementById('UserForm').submit();
	<?php }?>
	$(function(){
		var loginfield = $("#username"),
			pwdField = $("#password"),
			lostpwdfield = $("#lostpassword"),
			lostpwdform = lostpwdfield.closest("form"),
			submitButtonTrigger = $("#submit-button-trigger"),
			submitButton = $("#submit-button"),
			validationTemplate = "<label for='username' class='error missing-data'>Please enter your username, then click 'Lost Your Password' link again.</label>",
			MobileAlert = $("#MobileAlert");
			;
			
		submitButtonTrigger.on('click',function(){
			submitButton.hide();
			submitButton.before('<span class=\'alert info\'>Logging in...</span>');
			$("#UserForm").submit();
			
		
			/*
			submitButtonTrigger.addClass('inactive');
			if (!submitButton.hasClass('inactive')){
				submitButton.append('&nbsp;Logging In...').addClass('inactive');
			}
			*/
		});

        $("#forgot-password-form").on("click", ".remind-password", function(e){
            var formElement = $(this).closest("form"),
                data = formElement.serialize(),
                alertElement = formElement.find(".alert"),
                spinner = formElement.find(".loading-spinner"),
                button = formElement.find(".btn");
            e.preventDefault();
            alertElement.html("");
            button.hide();
            spinner.show();
            $.ajax({
                url: "<?php echo $CurrentServer;?>AjaxResetPassword",
                type: "POST",
                data: data,
                success: function(data){
                    if (data.success){
                        alert(data.message);
                        CETDASH.colorbox.close();
                        loginfield.focus();
                    } else {
                        CETDASH.forms.placeError(data.message, formElement.find("input[name='email']"));
                    }
                    formElement.find("input[name='email']").val("");
                    spinner.hide();
                    button.show();
                }
            });
        });

		loginfield.keydown(function(e){
			if (e.keyCode == 13){
				pwdField.focus();
			}
		});

		loginfield.focus();
	});
</script>
<?php } ?>