<?php
$showNewsletterForm = false; // no need to keep this; we just put it here to document how to switch it on and off
$formCssClass = $showNewsletterForm ? "" : "hidden";
$columnTwoCssClass = $showNewsletterForm ? "" : "offset-by-four";
?>
<div id="footer" class="band">
    <div class="container">
        <div class="four columns">
			<!--
            <form id="newsletter-signup" action="" method="get" class="<?php echo $formCssClass;?>">
                <fieldset>
                    <legend>Sign up for our Newsletter</legend>
                    <p>
                        <input type="text" name="newsletter_fullname" id="newsletter_fullname" class="auto-watermarked" title="full name">
                        <input type="text" name="newsletter_email" id="newsletter_email" class="auto-watermarked" title="Email address">
                    </p>
                </fieldset>
                <p>
                    <input type="submit" class="btn" value="Submit" >
                </p>
            </form>
			-->
        </div>
        <div class="three columns list_stack <?php echo $columnTwoCssClass;?>">
		<!--
            <h2>How it Works</h2>
            <ul>
                <li><span>*</span><a href="<?php echo $CurrentServer; ?>FAQ">FAQ</a></li>
                <li><span>*</span><a href="https://cetonline.zendesk.com/account/dropboxes/220049508" onClick="script: Zenbox.show(); return false;" title="Submit a question">Support</a></li>
                <li><span>*</span><a href="<?php echo $CurrentServer;?>Campaigns">Campaigns</a></li>
            </ul>
		-->
        </div>
        <div class="three columns list_stack">
		<!--
            <h2>About Us</h2>
            <ul>
                <li><span>*</span><a href="<?php echo $CurrentServer; ?>OurTeam">Our Team</a></li>
                <li><span>*</span><a href="<?php echo $CurrentServer; ?>HowItWorks">Our Story</a></li>
                <li><span>*</span><a href="http://blog.cetonline.org/" target="_blank">Blog</a></li>
            </ul>
		-->
        </div>
        <div class="three columns list_stack">
		<!--
            <h2>Our Partners ROCK!</h2>
            <ul>
                <li><span>*</span><a href="http://shiftyourshopping.org/2013/charity/" target="partners">Shift Your Shopping</a></li>
                <li><span>*</span><a href="http://oldtownboutiquedistrict.com/2013/boutiques-give-back-to-the-charity-of-your-choice" target="partners">Old Town Boutique District</a></li>
                <li><span>*</span><a href="http://www.gysd.org/causetown" target="partners">Global Youth Service Day</a></li>
                <li><span>*</span><a href="http://actforalexandria.org" target="partners">ACT for Alexandria</a></li>
                <li><span>*</span><a href="http://thinklocalfirstdc.com" target="partners">Think Local First DC</a></li>
                <li><span>*</span><a href="http://www.makeadifferenceday.com" target="partners">Make A Difference Day</a></li>
                <li><span>*</span><a href="http://bclc.uschamber.com/smallbiz"  target="partners">U.S. Chamber BCLC</a></li>
                <li><span>*</span><a href="http://www.paolachamber.org" target="partners">Paola Chamber of Commerce</a></li>
            </ul>
		-->
        </div>
        <div id="feedback-stack" class="three columns">
		<!--
            <h2>We LOVE Feedback!</h2>
            <p>
                Share your ideas on how <?php echo $SiteName;?> can do more to help your community!
            </p>
            <p id="swoosh-contact-us">
                <a href="<?php echo $CurrentServer."ContactUs"; ?>" class="btn contact-us-link">Contact Us</a>
                <img src="<?php echo $CurrentServer."images/swoosh-arrow.png"?>" alt="#">
            </p>
		-->
        </div>
        <div class="fifteen columns">
		<!--
            <ul class="social_nav round_links">
                <li><a class="twitter" href="https://twitter.com/cetonline" target="_blank"><span>twitter</span></a></li>
                <li><a class="facebook" href="https://www.facebook.com/cetonline" target="_blank"><span>facebook</span></a></li>
                <li class="hidden"><a class="flickr" href="http://www.flickr.com/photos/cetonline" target="_blank"><span>flickr</span></a></li>
                <li><a class="pinterest" href="http://pinterest.com/cetonline" target="_blank"><span>pinterest</span></a></li>
                <li><a class="rss" href="http://feeds.feedburner.com/cetonline" target="_blank"><span>blog</span></a></li>
            </ul>
		-->
        </div>
        <div class="fifteen columns">
            <ul id="footer-links" class="<?php echo (!$loginIsRequiredForThisPage) ? "embed-included" : "";?>">
                <li><a href="<?php echo $CurrentServer; ?>Home?LoggedIn" title="Home Page">Home</a></li>
                <li><a href="<?php echo $CurrentServer."ContactUs"; ?>" title="Contact Us" class="contact-us-link">Contact Us</a></li>
                <li><a href="<?php echo $CurrentServer."FullPrivacyPolicy"?>" title="Privacy Policy">Privacy Policy</a></li>
                <li><a href="<?php echo $CurrentServer."FullTermsAndConditions"?>" title="Terms and Conditions">Terms and Conditions</a></li>
                <?php if (!$loginIsRequiredForThisPage) {?>
                <li><a href="#embed-this-snippet" class="embed-link do-not-navigate" title="Embed this page">Embed This</a></li>
                <?php } ?>
            </ul>
            <ul id="legal">
                <li>Copyright<span class="copyright">&copy;</span><?php echo date("Y");?>&nbsp;CET, Inc.</li>
                <li>All Rights Reserved.</li>
                <li>CET<span class="trademark">&trade;</span> is a trademark of CET, Inc.</li>
            </ul>
		</div>
    </div>
</div>