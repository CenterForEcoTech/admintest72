<?php
/**
 * This page must be include('LoginRequiredForThisPage.php), not include_once!!!
 */
$loginIsRequiredForThisPage = true;

$isAccessDenied = $isAccessDenied || false;
if (!isLoggedIn()){
	$isAccessDenied = true;
	if ($redirectToLogin){
		// this is for a case where someone goes directly to a page that is not intended to display anything (like Event/Accept_process)
		$accessDeniedMessage = "<h4>You are not logged in!</h4> This process has been aborted.";
		include_once("AccessDenied.php");
		//die();
	}
	if ($inLineMessage){
		// this means an ajax call is blocked due to user not being logged in.
		// we will selfishly conclude that this is a call from our website, and that the appropriate procedure is the reload the page
		// which will then call this code AGAIN and display the usual Oops message.
		// A more sophisticated approach may be needed at some point to handle json requests.
		?>
		<script type="text/javascript">
			//window.location.reload(true);
			window.history.go(0);
		</script>
		<?php
		die(); 
	}
	if ($ModalWindow){
	?>
	<div class="container form-modal">
		<h2>Oops! You're not logged in.</h2>
		<div>
			<p>You need to log in first to access this feature.</p>
			<p>Please close this window and refresh the page.</p>
		</div>
	</div>
	<?php } else {
        $lastPage = SessionManager::getLastPage();
		if ($lastPage != $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']){
            SessionManager::setLastPage($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
            SessionManager::setStartPage("LastPage");
		}
		$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
		$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
		if ($load_getHeader){
			$PageTitle = "Oops! Not Logged In!";
			$PageHeader1 = "Oops! You're not logged in.";

		}
		if ($load_getContent){
		?>
	<p>You need to log in first to access this page.</p>
	<div>
		<div class="btn">
			<a class="button" id="login-first" href="#">Log In</a>
		</div>
		<p>You'll be taken to the page you requested after you log in.</p>
	</div>
	<div id="login-form-hidden" style="display:none;">
		<?php include("Login_form.php"); ?>
	</div>
	<script type="text/javascript">
		$(function(){
			$("#login-first").click(function(e){
				e.preventDefault();
				$(this).parent().hide();
				$("#login-form-hidden").show();
			});
		});
	</script>
	<?php }
		} // not modal
} else {
	// initialize defaults
	$mustBeMerchant = $mustBeMerchant || false;
	$mustBeOrg = $mustBeOrg || false;
	$mustBeIndividual = $mustBeIndividual || false;
	if (!$isAccessDenied && $mustBeMerchant){
		$isAccessDenied = !isMerchant();
		//$accessDeniedMessage = "You must be a merchant to access this page.";
	}
	if (!$isAccessDenied && $mustBeOrg){
		$isAccessDenied = !isOrganization();
		//$accessDeniedMessage = "You must be an organization to access this page.";
	}
	if (!$isAccessDenied && $mustBeIndividual){
		$isAccessDenied = !isIndividual();
		//$accessDeniedMessage = "You must be an individual to access this page.";
	}
	if ($isAccessDenied){
		if ($mustBeMerchant){$acceptableLogin = " a Merchant";}
		if ($mustBeOrg){$acceptableLogin = " a Nonprofit";}
		if ($mustBeIndividual){$acceptableLogin = " an Individual";}
		include("_accessDenied.php");
	}	
}
?>