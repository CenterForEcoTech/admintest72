<?php
$IgnoreRedirects = true;
include "../Global_Variables.php";

//get the posted values
//removes sql injections from the data
//print_r($_POST);
$user_name= trim(strtolower(htmlspecialchars(addslashes($_POST['user_name']),ENT_QUOTES)));
$pass_word = trim($_POST['password']);
$staySignedIn = $_POST['MobileStaySignedIn']=="yes";
if ( isset($_SERVER["REMOTE_ADDR"]) ){
	$ip=$_SERVER["REMOTE_ADDR"];
} elseif (isset($_SERVER["SERVER_ADDR"])){
	$ip=$_SERVER["SERVER_ADDR"];
} else {
	$ip="Not Available";
}

// Call the AuthProvider
include_once($dbProviderFolder."AuthProvider.php");
$authProvider = new AuthProvider($mysqli);
$authAttempt = $authProvider->authenticate($user_name, $pass_word, $ip);
if ($authAttempt->message){
    SessionManager::setTmpVar('LoginAlert', $authAttempt->message);
	setcookie("username", "", time()-3600);
	header('location:Home');
	die();
}
$CreateSessionVariablesProvider = new CreateSessionVariablesProvider($mysqli);
$sessionData = $CreateSessionVariablesProvider->getSessionData($authAttempt);
SessionManager::login($authAttempt, $sessionData, $staySignedIn);

// redirect logic
if (isMerchant()){
    $MobileMenu = "LoggedIn";
} else {
    $MobileMenu = "notyetavailable";
}
//all is good then send to Menus
header('location:'.$MobileMenu);
?>