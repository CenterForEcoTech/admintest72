<?php
$BaseLink = "mobile_";
$HomeLink ="../";
$IgnoreRedirects = true;
$CleanURLFolder2 = "mobiles/";
require_once('../Global_Variables.php');

if ($PageName != "notyetavailable.php" && isLoggedIn() && !isMerchant()){
	header('location:notyetavailable');
	die();
}

//include incase Index.php from Mobile site accidentally overwrites main site
if ($PageName == "Index_content.php" || !isLoggedIn()){
	// TODO: find a better way to ungate open pages
	if ($PageName != "mobile_FullPrivacyPolicy.php" && $PageName != "mobile_ContactUs.php" && $PageName != "mobile_FullTermsAndConditions.php"){
		$PageName = "mobile_Index_content.php";
	}
	header('location:../Home');

}

if(!$PageName){
	$PageName = $_GET['PageName'].'.php';
	header('location:../Home');
}

$displayTemplateFile = $PageName;
if (!file_exists($PageName)){
	$displayTemplateFile = "../".$PageName;
	$PageName = $_SERVER['DOCUMENT_ROOT'].$Config_DevFolder.$PageName;
} 
// second check
if (!file_exists($PageName)){
	$displayTemplateFile = "../_404Body.php";
	header("HTTP/1.0 404 Not Found");
}

//header
require_once($BaseLink.'Header.php');
?>
<body>
	<div id="wrapper">
	<div id="custom-header">
		<div class="container">
			<?php include_once($BaseLink.'TopNav.php');?>
		</div>
	</div>
	<div class="container">
		<div id="content">
			<div class="six columns alpha-hook">
				<a class="icon-img" href="<?php echo $CurrentServer;?>mobile/LoggedIn" title="Home">Home</a>
				<?php include($displayTemplateFile); ?>
			</div>
		</div> <!-- End content -->
	</div> <!-- End container -->
	</div>
	<?php include_once($BaseLink.'Footer.php');?>
</body>
</html>
<?php 
mysqli_close($conn);
SessionManager::unsetTmpVar('LoginAlert');
?>