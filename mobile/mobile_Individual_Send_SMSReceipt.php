<?php
/**
 * Sends an SMS text message. Not used to send transaction receipt anymore
 * TODO: refactor templates that call this template.
 *
 * set in the external Twilio.php file: (probably needs to be a site config)
 *      $AccountSid
 *      $AuthToken
 *      $FromNumber
 *
 * set in the calling template mobile_Merchant_Transaction_SendReceipt.php:
 *      $ReceiptSMSMessage
 *      $Customer (phone number)
 *      $MemberID (in spite of the check in the code, this is ALWAYS set)
 */

// Include the PHP TwilioRest library
include_once("twilio/Twilio.php");
$SMSTo = $Customer;
$SMSReceipt = $ReceiptSMSMessage;
if (!strpos($CurrentServer,"localhost")){
	//break apart the message if longer than 160
	if (strlen($SMSReceipt) > 159){
		$SMSReceipt_array = explode("TO:",$SMSReceipt);
		//echo "<br>Message (".strlen($SMSReceipt_array[0]).") = ".$SMSReceipt_array[0]."<br>";
		$client2 = new Services_Twilio($AccountSid, $AuthToken);
		$Response2 = $client2->account->sms_messages->create($FromNumber,$SMSTo,$SMSReceipt_array[0]."...part 1 of 2");
		//echo "<br>Message (".strlen($SMSReceipt_array[1]).") = ".$SMSReceipt_array[1]."<br>";
		$client2 = new Services_Twilio($AccountSid, $AuthToken);
		$Response2 = $client2->account->sms_messages->create($FromNumber,$SMSTo,'part 2 of 2....TO:'.$SMSReceipt_array[1]);

	} else {
		$client2 = new Services_Twilio($AccountSid, $AuthToken);
		$Response2 = $client2->account->sms_messages->create($FromNumber,$SMSTo,$SMSReceipt);
	}
}//end if non-local server
if (!$MemberID || strlen($MemberID) == 0){
    $MemberID=getMemberId();
    if (strlen($MemberID) == 0){
        $MemberID = 0;
    }
}
$SQL_Email = "INSERT INTO email_history (EmailHistory_MemberID,EmailHistory_From,EmailHistory_To,EmailHistory_Subject,EmailHistory_Message,EmailHistory_OptOutEmails) VALUES (".$MemberID.",'".$FromNumber."','To:".$SMSTo."','".$subject."','".chkstring($SMSReceipt)."','')";
$SQLEmail = mysqli_query($SQLEmail, $SQL_Email);
if($Response2->IsError){
	$SMSError = $SMSError.$Response2->ErrorMessage;
	$ReceiptSent = false;
}else{
	$ReceiptSent = true;
}
?>