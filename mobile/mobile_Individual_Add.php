<?php
/**
 * Called by mobile_Individual_SignUp_Process.php and mobile_Merchant_Transaction_SendReceipt.php
 *
 * The following variables need to be set prior to calling this:
 *
 *      $IsEMail
 *      $IsPhone
 *      $Customer (phone or email)
 *
 * Optional variables:
 *      $event_isSalesTransaction (indicates that this template is called as part of sales transaction processing)
 *
 * If Successful, $MemberID will be set with a numeric value, and if not for a sales transaction,
 * activation link will be sent via email or SMS.
 *
 * If Unsuccessful, a session validation 'Alert' will be set with an error message.
 */
if (empty($event_isSalesTransaction)){
    $event_isSalesTransaction = false;
}

$PostEmail = '';
$PostPhone = '';
if ($IsEmail){
    $PostEmail = $Customer;
}
if ($IsPhone){
    $PostPhone = $Customer;
}
$StartPage = 'Individual_Start';

include_once($dbProviderFolder."MemberProvider.php");
$memberProvider = new IndividualMemberProvider($mysqli);
$memberRecord = new IndividualMemberRecord();
$memberRecord->email = htmlspecialchars($PostEmail);
$memberRecord->setPhone($PostPhone);
$memberRecord->startPage = $StartPage;
$memberRecord = $memberProvider->addMember($memberRecord);
$JustInsertedID = $memberRecord->id;
$MemberID = $JustInsertedID;

if (is_numeric($MemberID) && $MemberID > 0){
	$Success = true;
    if ($event_isSalesTransaction){
        // possibly log that the email was suppressed?
    } else {

        // 2012-05-07: the $isMobile is virtual untestable at this point as there are no links to the mobile
        // version.
        $isMobile = ($FormSource != "Web");
        if (!isset($authProvider)){
            include_once($dbProviderFolder."AuthProvider.php");
            $authProvider = new AuthProvider($mysqli);
        }
        $resetRequest = $authProvider->requestReset($Customer, $isMobile);
        $Email = $resetRequest->memberEmail;
        $Phone = $resetRequest->memberPhone;
        $MemberID = $resetRequest->memberId;
        $NonEmailLogin = ($Phone == $Customer || $MemberID==str_replace("ct","",strtolower($Customer)));
        $code = $resetRequest->verificationCode;

        if($IsEmail){

            //don't edit this, this is the link for there activication
            $link = $CurrentServer."User_activate-".$code;

            //sends the activation email to the person
            $to = $Customer;
            $subject = "Confirm Your ".$SiteTitle." Account!";
            if(!$NoReceipt){
                $IndividualWelcomeMessage = "We hope you enjoyed participating in your first event.<br><br>\r\n\r\n";
                $IndividualPSNote = "Please note: you will be receiving your donation receipt in a separate email.<br><br>\r\n\r\n";
            }
            $emailTemplateContext = GetWelcomeEmailContext($Customer,$link,$IndividualWelcomeMessage,$IndividualPSNote);
            include_once($dbProviderFolder."ContentProvider.php");
            $emailTemplateProvider = new IndividualRegistrationEmailTemplateProvider($mysqli);
            $body = $emailTemplateProvider->getRegistrationEmailBody($memberRecord, $emailTemplateContext);

            $EmailTo = $to;
            $EmailSubject = $subject;
            $EmailMsg = $body;
            $MemberID = $MemberID;
            include($_SERVER['DOCUMENT_ROOT'].$Config_DevFolder."Email_Send.php");
        }
        if ($IsPhone){
            // TODO: refactor to call TwilioProvider
            //SMS link is CzTwn.com
            //don't edit this, this is the link for there activication
            //$link = "http://CauzTwn.com/?v&c=".$code;
            $link = $CurrentServer."User_activate-".$code;
            $subject = $SiteTitle." Event Receipt";

            // Instantiate a new Twilio Rest Client
            include_once("twilio/Twilio.php");

            $ReceiptSent = true;
            if (!strpos($CurrentServer,"localhost")){
                $client = new Services_Twilio($AccountSid, $AuthToken);
                $SMSTo = $Customer;
                $SMSActivate = "Thank you for registering with ".$SiteTitle."!  Please confirm your account here: ".$link;
                //send activation link
                $Response = $client->account->sms_messages->create($FromNumber,$SMSTo,$SMSActivate);
                //send receipt
                if($Response->IsError){
                    $SMSError = $SMSError.$Response->ErrorMessage;
                    $ReceiptSent = false;
                }else{
                    $ReceiptSent = true;
                }
            }
        }
    }
} else {
    SessionManager::pushValidationMessage("Email or phone are already used by an existing member.");
}// end if successful
?>