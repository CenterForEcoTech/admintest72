<?php
/*
 * This is called by ApiTransactions process;
 * TODO: move into partial
 */

if ($eventRecord && $eventTransactionRecord){
    $event_isSalesTransaction = true;

    // get customerVerifiedId first
	$Customer = $eventTransactionRecord->customer;
	$CustomerEmail = $eventTransactionRecord->parsedEmail;
	$CustomerPhone = $eventTransactionRecord->parsedPhone;
	$CustomerVerifiedId = $eventTransactionRecord->verifiedMemberId;

    //check to see if email, CTID#, or phonenumber
    if ($CustomerEmail){
        $IsEmail = true;
    } else if ($CustomerPhone){
        $IsPhone = true;
    } else if ($CustomerVerifiedId){
        if (!isset($authProvider)){
            include_once($dbProviderFolder."AuthProvider.php");
            $authProvider = new AuthProvider($mysqli);
        }
        $user = (object) $authProvider->getMember($Customer);
        if ($user->Email){
            $IsEmail = true;
            $CustomerEmail = $user->Email;
        } else if ($user->Phone){
            $IsPhone = true;
            $CustomerPhone = trim($user->Phone);
        }
    }

    if ($CustomerVerifiedId){
        $Success = true; // this success just means we have a customer!
        $MemberID = $CustomerVerifiedId;
    } else if ($CustomerEmail || $CustomerPhone){
        $NewMember = true;
        // $MemberID is set in the following template
        include($siteRoot."mobile/mobile_Individual_Add.php");
        $eventTransactionProvider->setCustomerId($eventTransactionRecord->id, $MemberID);
        $CustomerVerifiedId = $MemberID;
    }

	$TransactionCode = $eventTransactionRecord->transCode;
	$TransactionID = $eventTransactionRecord->id;
    $EventID = $eventRecord->id;
	$EventTitle = $eventRecord->title;
	$EventDate = date("Y-m-d", $eventRecord->startDate);
	$EligibleAmount = money($eventTransactionRecord->eligibleAmount);
	$DonationGenerated = money($eventTransactionRecord->generatedAmount);
	$Beneficiary = $eventTransactionRecord->recipientText;
	$MerchantName = $eventRecord->venue->name;
	$EventTransaction_OrgID = $eventTransactionRecord->orgID;

    include_once($dbProviderFolder."MemberProvider.php");
    include_once($dbProviderFolder."PermaLinkProvider.php");

    $merchantProvider = new MerchantMemberProvider($mysqli);
    $merchantProfile = $merchantProvider->getPublicProfile($eventTransactionRecord->merchantId);
    if ($eventTransactionRecord->orgID){
        $orgProvider = new OrganizationMemberProvider($mysqli);
        $orgProfile = $orgProvider->getPublicProfile($eventTransactionRecord->orgID);
    }
    $CustomerChoiceValue =  false;
    $BeneficiaryText = $Beneficiary;
    if ($Beneficiary == "Customer Choice"){
        $BeneficiaryText = "any charity you choose";
        $CustomerChoiceValue =  true;
    }
    if (is_numeric($CustomerVerifiedId)){
        // this is the memberID
        $entity = MemberProvider::getEntityId($mysqli, $CustomerVerifiedId);
        if ($entity){
            $firstname = $entity->firstName;
            $lastname = $entity->lastName;
            $permalinkProvider = new PermaLinkProviderImpl($mysqli);
            $customerPidRecords = $permalinkProvider->getByEntity($entity->entity_name, $entity->entity_id);
            if (count($customerPidRecords)){
                // take the first one, as it will be the "preferred" one
                $customerPidRecord = $customerPidRecords[0];
                /* @var $customerPidRecord PermaLinkRecord */
                $customerPid = $customerPidRecord->pid;
                $chooseCharityUrl = $CurrentServer."CustomerChoice/".$customerPid."/".$TransactionCode;
            }
        }
    }

	if ($IsEmail){
        $AddCommonFooter = false; //turns off common footer in Email_Send.php
		$Customer = $CustomerEmail;
        $emailToName = $firstname." ".$lastname;
        include_once($dbProviderFolder."ContentProvider.php");
        $emailTemplateProvider = new TransactionReceiptEmailTemplateProvider($mysqli);
        $donationForSaleSnippet = "$".$EligibleAmount." generated a donation of $".$DonationGenerated;
        if ($eventTransactionRecord->isFlatDonation){
            $donationForSaleSnippet = $eventTransactionRecord->flatDonationUnits." ".$eventRecord->venue->unitDescriptor." generated a donation of $".$DonationGenerated;
        }
        $emailTemplateContext = (object)Array(
            "emailToName" => strlen(trim($emailToName)) ? $emailToName : "Friend",
            "eligibleAmount" => $EligibleAmount,
            "generatedAmount" => $DonationGenerated,
            "eventUrl" => $CurrentServer."event/".$EventID,
            "loginUrl" => $CurrentServer."Home",
            "chooseCharityUrl" => $chooseCharityUrl,
            "SiteName" => $SiteName,
            "GivingAccountURL" => $CurrentServer.$Config_GivingAccountURL,
            "GivingAccountName" => $givingBankName,
            "publicProfileURL" => $CurrentServer."ct/".$merchantProfile->pid,
            "donationForSaleSnippet" => $donationForSaleSnippet
        );
        $customTransactionEmailText = $merchantProvider->getCustomTransactionEmailText($eventTransactionRecord->merchantId);
        $merchantProfile->customTransactionEmailText = $customTransactionEmailText;
        $ReceiptMessage = $emailTemplateProvider->getTransactionEmailBody($merchantProfile, $emailTemplateContext, $merchantProfile, $orgProfile, $EventID);
        $ReceiptMessage .= "<a href='http://www.facebook.com/causetown'><img src='".$CurrentServer."images/Facebook_FindUs.png' alt='Join ".$SiteName." at Facebook'></a><br>";
        $ReceiptMessage .= "<a href='http://www.twitter.com/causetown'><img src='".$CurrentServer."images/Twitter_FollowUs.png' alt='Follow ".$SiteName." at Twitter'></a><br>";
        include($siteRoot.'mobile/mobile_Individual_Send_EmailReceipt.php');
	} else if ($IsPhone){
        include_once("repository/TwilioHelper.php");
        include_once("repository/mySqlProvider/MySqliEmailLogger.php");
        $smsMessage = new SMSMessage($CustomerPhone, "Causetown Donation Receipt", $MemberID);
        if ($CustomerChoiceValue){
            $smsMessage->addMessagePart("Sweet! ".$MerchantName." will give $".$DonationGenerated);
            $smsMessage->addMessagePart(" to any charity you choose: ");
            $smsMessage->addMessagePart($chooseCharityUrl);
        } else {
            $smsMessage->addMessagePart("Sweet! Because of you, ".$MerchantName." gave $".$DonationGenerated);
            $smsMessage->addMessagePart(" to ".$Beneficiary.".");
            $smsMessage->addMessagePart("Login at causetown.org for details.");
        }

        $emailLogger = new MySqliEmailLogger($mysqli);
        $smsProvider = new TwilioProvider($emailLogger, $AccountSid, $AuthToken, $FromNumber);
        $response = $smsProvider->send($smsMessage, $isDevMode_Email);
	}

	if (!$IsPhone && !$IsEmail){
		echo "The Customer contact information was not a valid phone or email<br>";
	} else {
		if ($Success && $ReceiptSent){
			echo $SiteName." sent a receipt";
			if ($NewMember){ echo " and activation link";}
			echo " to ".$Customer." for your records.<br>";
		} else {
			echo "There was an error sending the message:";
		}
	}
}
?>