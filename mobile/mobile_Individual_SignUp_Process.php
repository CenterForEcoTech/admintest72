<?php
$proceed = false;
$seconds = 60*1;
//echo '<h1>Testing:</h1><p>Cookie: '.$_COOKIE['token'].'<br />Timestamp: '. $_POST['ts'].'</p>';
if(isset($_POST['ts']) && isset($_COOKIE['token']) && $_COOKIE['token'] == md5('round house'.$_POST['ts'])) $proceed = true;

if(!$proceed) {
    // don't display this
    //$_SESSION["Alert"] = 'Form processing halted for suspicious activity';
    // Just return so the form is displayed
} else {
	#//As of PHP 5.1, when called with no arguments, mktime() throws an E_STRICT notice: use the time() function instead.
    //if(((int)$_POST['ts'] + $seconds) < mktime()) {
    if(((int)$_POST['ts'] + $seconds) < time()) {
        pushValidationMessage('Too much time elapsed. Please try again.');
    } else {

        // Include the PHP TwilioRest library
        include_once("twilio/Twilio.php");

        //$FormSource is embedded to know if from main web site or from mobil site
        //default value if left empty is the mobile site
        $FormSource = $_POST['source'];

        $Customer = $_POST['IndividualLogin'];
        //check to see if email or phonenumber
        if (strpos($Customer,"@") && strpos($Customer,".")){
            $IsEmail = true;
            $SignUpType = "Email";
        }
        if (is_Numeric(trim(str_replace("-","",str_replace("(","",str_replace(")","",str_replace(".","",str_replace(" ","",$Customer)))))))){
            $IsPhone = true;
            $SignUpType = "Phone Text Messages";
        }
        //Variable $NoReceipt will prevent a receit email from being sent
        $NoReceipt = true;

        include_once("mobile_Individual_Add.php");

    } // end check if too much time elapsed
}// end check if tokens matched up
?>