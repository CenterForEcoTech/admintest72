<?php
$FormValidated = $_POST['ts'];
if (strpos($BaseLink,"/")){
	$BaseSource = "/signup";
}else{
	$BaseSource = "mobile/signup";
}

$Success = false;
if ($FormValidated){
	include($BaseLink.'Individual_SignUp_Process.php');
    if ($Success){
    ?>
        <script type="text/javascript">
            $("#signup-preamble").hide();
        </script>
        <h3>Sign up Confirmation on the way!</h3>
        <p>Check your <?php echo $SignUpType;?> to confirm.</p>
        <p>You can use your <?php echo $SiteName; ?> ID# CT<?php echo $MemberID;?> at check out right now, but be sure to login later to update your profile.</p>
        <p>
            <span class="btn">
            <a href="<?php echo $CurrentServer.ltrim($BaseSource,"/");?>">Sign Up Another Phone or Email</a>
            </span>
        </p>
    <?php
    } // end if successful
}// end if form submitted

if (!$FormValidated || !$Success){
?>
    <form id="sign-up-form" method="post" class="override_skel" action="<?php echo $CurrentServer.$CleanURLFolder2;?>Individual/SignUp">
        <?php if (peekValidationMessage()){ ?>
            <span class="alert"><?php echo popValidationMessage();?></span>
        <?php
        }?>
		<label>
			<div>Email or Phone:</div>
			<span class="highlight">
				<input type="text" name="IndividualLogin" id="IndividualLogin">
			</span>
			<span class="alert"></span>
		</label>
		<label>
			<div>Confirm:</div>
			<span class="highlight">
				<input class="confirm" type="text" name="IndividualLoginConfirm" id="IndividualLoginConfirm">
			</span>
			<span class="alert"></span>
		</label>
		<input type="hidden" name="source" value="<?php echo $FormSource;?>">
		<div id="Proceed" class="btn">
			<input id="submit-button" type="submit" value="Submit" >
		</div>

        <?php echo render_template("TermsAgreement.snippet", array( "siteName" => $SiteName, "serverUrl" => $CurrentServer )); ?>

    </form>
	<p id="js-warning" class="warning">You must javascript enabled to register.</p>
	<script type="text/javascript">
		// show form
		$("#js-warning").hide();
		$("#sign-up-form").show();

		$(function(){
			var form = $("#sign-up-form"),
				submitButton = $("#submit-button"),
				username = $("#IndividualLogin"),
				usernameConfirm = $("#IndividualLoginConfirm"),
				loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
				tokenRequestUrl = "<?php echo $CurrentServer;?>MobileToken",
				tokenInputTemplate = '<input type="hidden" name="ts" value="{0}" />',

				// login availability
				loginAvailableString = "Login Available",
				checkLoginAvail = function(value,element){
					var alertElement = element.closest("label").find(".alert"),
						elementId = element.attr("id"),
						confirmIdTemplate = "#{0}Confirm",
						confirmElement = $(confirmIdTemplate.replace("{0}",elementId));
					alertElement.html("");
					$.get(loginAvailUrl,
						{
							IndividualLogin: element.val()
						},
						function(data){
							if (data !== loginAvailableString){
								alertElement.html(data);
								element.val("");
								confirmElement.val("");
								element.focus();
							}
						});
				},

				// email validation
				emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,
				checkEmail = function(value,element,alertElement){
					//determine if email
					if (emailReg.test(value)){
						checkLoginAvail(value,element);
						return true;
					}else{
						alertElement.html('Please use a valid Email.');
						element.focus();
						return false;
					}
				},

				// phone validation
				areacodes = Array(<?php echo $Config_areacodes;?>),
				checkPhone = function(value,element,alertElement){
					var areacodeInvalid = "Area Code is invalid",
						phoneTooShort = "Phone numbers must be 10 digits",
						phoneTooLong = "Phone numbers must be 10 digits.\r\nPhone number has been truncated.",
						formattedPhone = value.replace(/[^\d]/g, ""),
						areacode = Number(formattedPhone.substr(0,3));
					element.val(formattedPhone);

					if ($.inArray(areacode, areacodes) >= 0){
						if (formattedPhone.length === 10){
							checkLoginAvail(formattedPhone, element);
							return true;
						}
						if (formattedPhone.length < 10){
							alertElement.html(phoneTooShort);
							element.focus();
							return false;
						}
						alertElement.html(phoneTooLong);
						element.val(formattedPhone.substr(0,10));
						element.focus();
						return false;
					} else {
						alertElement.html(areacodeInvalid);
						element.focus();
						return false;
					}
				},

				// generic validation on confrm field
				validateConfirm = function(confirmelement){
					var confirmElId = confirmelement.attr("id"),
						elId = confirmElId.replace("Confirm",""),
						element = $("#"+elId),
						alertElement = confirmelement.closest("label").find(".alert"),
						validationMessage = "This value must match the previous value.",
						confirmValue = confirmelement.val(),
						value = element.val();
					alertElement.html("");
					if (confirmValue !== value){
						alertElement.html(validationMessage);
						return false;
					}

					return true;
				},

				// on submit validation
				isSubmittable = function(){
					var usernameValue = username.val(),
						hasUsername = (usernameValue);

					if (hasUsername && !validateConfirm(usernameConfirm)){
						return false;
					}

					if (!hasUsername){
						username.closest("label").find(".alert").html("You must provide an email or a phone number");
						username.focus();
						return false;
					}
					return true;
				};

			username.blur(function(){
				var el = $(this),
					value = el.val(),
					alertELement = el.closest("label").find(".alert");
				alertELement.html("");
				if (value){
					if (value.indexOf("@") > 0){
						return checkEmail(value, el, alertELement);
					} else {
						return checkPhone(value, el, alertELement);
					}
				} else {
					alertELement.html("You must provide an email or a phone number");
				}
			});

			$(".confirm").blur(function(){
				validateConfirm($(this));
			});

			form.submit(function(){
				return isSubmittable();
			});

			// load token
			$.get(tokenRequestUrl,function(txt){
				form.append(tokenInputTemplate.replace("{0}", txt));
			});

		});
	</script>
<?php
}//end if submitting form
?>