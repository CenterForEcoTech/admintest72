<!DOCTYPE HTML>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title><?php echo $SiteTitle;?></title>
	<meta name="description" content="What if every time you spent money - wherever you spent it - it triggered a donation to your favorite cause? What if it also sparked innovative local partnerships between business and community groups making your town a better place to live? That's our vision. <?php echo $SiteName; ?> aims to inject community into every transaction on the planet. Join <?php echo $SiteName; ?>." />
  	<meta name="keywords" content="cause marketing, nonprofit fundraising, restaurant night, restaurant fundraiser, new fundraising ideas, innovative fundraisers, local business partnerships, donate percentage sales, small business marketing, easy community relations, easy marketing, charity, donations, triple bottom line, how to organize fundraiser night, host business events, citizen engagement, build community, small town charm, small town feel, personal relationships, innovative partnerships, nonprofit social night, supper club, townie, town hall, town social, social media community promotion, national community events calendar, find charitable events, charitable sales promotion, help community, support local, shop local, support your favorite cause, charitable partnerships" />
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/base.css">
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/skeleton.css">
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/layout.css">
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/CauseTown.css">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CurrentServer; ?>images/ct_icon3.ico">
	<link rel="apple-touch-icon" href="<?php echo $CurrentServer;?>mobile/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $CurrentServer;?>mobile/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $CurrentServer;?>mobile/images/apple-touch-icon-114x114.png">

	<!-- JQuery-->
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>jquery/css/ui-lightness/jquery-ui-1.8.4.custom.css" type="text/css" />	
	<!-- Form Validation -->
	<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/formvalidate.css" type="text/css" />	
    <!-- jQuery - the core -->
		<script type="text/javascript" src="<?php echo $CurrentServer;?>jquery/js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?php echo $CurrentServer;?>jquery/js/jquery-ui-1.8.4.custom.min.js"></script>


	<!-- General From Validation -->
	<script src="<?php echo $CurrentServer;?>js/jquery.validate.js" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $CurrentServer;?>js/jquery.metadata.js" type="text/javascript" language="javascript"></script>
	
	<!--CauseTown scripts-->
	<script type="text/javascript" src="<?php echo $CurrentServer;?>js/<?php echo $BaseLink;?>CauseTown.js"></script>
</head>
<body>
