<?php
$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
if ($load_getHeader){
    $PageTitle = "503 - Site Temporarily Unavailable";
    $PageHeader1 = "Site Maintenance";

}
if ($load_getContent){
    ?>
<p>
    <?php echo $SiteName;?> via <?php echo $_SERVER['REMOTE_ADDR'];?> is temporarily down for maintenance. We&rsquo;re sorry for the inconvenience.
</p>
<p>
    Please check back soon.
</p>
    <h3>Contact Us</h3>
<div class="contact-us-row" class="clear">
    Please get in touch with us at any time if you need assistance or want to learn more about <?php echo $SiteName;?>!<Br>
    <br>
    Center for EcoTechnology.<br>
    112 Elm St<br>
    Pittsfield, MA 02101<br>
    <br>
</div>
<span class="btn">
    <a href="<?php echo $CurrentServer."ContactUs"; ?>" class="contact-us-link">Send Message</a>
</span>
<script type="text/javascript">
    $(function(){
        $(".contact-us-link").click(function(e){
            e.preventDefault();
            Zenbox.show();
        });
    });
</script>

<?php }?>