<?php

if (!isset($dbConfigData)){
    trigger_error("_setupDataConnection.php This script requires the database configuration for the data database in siteconf.", E_USER_ERROR);
    die(); // no db config for the target db.
}

$dataConn = new mysqli($dbConfigData->serv,$dbConfigData->user,$dbConfigData->pass,$dbConfigData->inst);
if ($dataConn->connect_error) {
    trigger_error("_setupDataConnection.php: Failed to connect to MySQL: ".$dataConn->connect_error, E_USER_ERROR);
}