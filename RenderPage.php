<?php

include_once($siteRoot."/repository/utils/UrlLinker.php");
if (isset($subdomain) && $subdomain){
    // set this early so that it can be overridden if necessary
    $brandingTemplatePath = $siteRoot."partners/branding/".$subdomain.".php";
    if (file_exists($brandingTemplatePath)){
        include_once($brandingTemplatePath);
        $customSideRailBottom = $brandingCardData;
    }
}

$customBodyTagId = $customBodyTagId ? $customBodyTagId : "";
$load_getHeader = true;
$load_getContent = false;
include($displayTemplateFile);
if (!isset($PageHeaderClass) && strlen(strip_tags($PageHeader1)) > 35){
	$PageHeaderClass = "no-wrap";
}
if ($customBodyTagId == ''){
    $customBodyTagId = 'interior-page';
}
$customBodyClass = $customBodyClass ? $customBodyClass : "";
if ($_GET['render'] == "wide"){
    $customBodyClass .= " wide";
}
$embedCssClass = ($_GET['isEmbed']) ? "clean-embed" : "";
$urlConverter = new UrlLinker();
$linkedSubtitle = nl2br($urlConverter->htmlEscapeAndLinkUrls($pageSubtitle, true));
if ($linkedSubtitle !== $pageSubtitle){
    $pageSubtitle = html_entity_decode($linkedSubtitle);
}
require_once('Header.php');
?>
<body id="<?php echo $customBodyTagId; ?>" class="<?php echo $customBodyClass." ".$embedCssClass;?>">
<div id="wrapper">
	<div id="header_band" class="band">
		<div id="header_nav" class="container">
			<?php include("TopNav.php"); ?>
		</div>
	</div><!-- end header_band -->
	<div id="banner_band" class="band">
		<div id="upper_stitch"></div>
		<div id="banner" class="container">
			<div class="content_container lower_banner_hook container"></div>
		</div>
		<div id="lower_stitch"></div>
	</div><!-- end banner_band -->
	<div id="featured_section_band" class="band">
		<div class="featured_section container">
			<?php if (isset($showHomeFeaturedSection) && $showHomeFeaturedSection) {?>
				<?php include("_home_featuredSection.php");?>
			<?php } else { ?>
				<div id="navigation" class="four columns">
					<?php // include("sideNav.php"); ?>
				</div>
				<div id="page_header" class="twelve columns <?php echo $PageHeaderClass;?>">
					<h1><?php echo $PageHeader1; ?></h1>
					<p><?php echo $pageSubtitle;?></p>
				</div>
<?php } ?>
		</div>
	</div><!-- end featured_section_band -->
	<div id="body_section_band" class="band">
		<div id="body_section" class="container">
			<div id="main" class="twelve columns">
				<?php
					$load_getHeader = false;
					$load_getContent = true;
					include($displayTemplateFile);
				?>
			</div><!-- end main -->
			<div id="right_rail" class="four columns">
				<?php include("_sideRail.php");?>
			</div><!-- end right_rail -->
		</div><!--end of body section-->
		<div class="bottom_body_section_stitch "></div>
	</div><!-- end body_section_band -->
	<?php include("_footer_band.php")?>
</div>

<?php
if (isset($outOfFlowContent)){
    echo $outOfFlowContent;
}
require_once('Footer.php');
?>