<?php
include_once("ServiceAPI.php");

class EventBriteApi {
    private static $API_URL_BASE = "http://www.eventbrite.com/json/";

    private $appKey;
    private $userKey;
    private $organizerId;
    private $locationProvider;

    public function __construct($apiKey, $apiUserKey, $eventOrganizerId, LocationProvider $locationProvider){
        $this->appKey = $apiKey;
        $this->userKey = $apiUserKey;
        $this->organizerId = $eventOrganizerId;
        $this->locationProvider = $locationProvider;
    }

    public function createTicket(EventRecord $eventRecord, $name = "Free", $price = "0.00", $quantity = 1000){
        if (isset($eventRecord->venue->eventBriteEventID)){
            $params = array(
                "event_id" => $eventRecord->venue->eventBriteEventID,
                "name" => $name,
                "price" => $price,
                "quantity" => $quantity
            );
            $url = $this->buildURL("ticket_new", $params);
            $rawData = $this->call($url);

            $data = json_decode($rawData, true);
            $ticketId = $data["process"]["id"];
            $error = $data["error"]["error_message"];
            if ($error){
                return $error;
            }

            return $ticketId;
        }
        return null;
    }

    public function createEvent(EventRecord $eventRecord, $asPrivate, $asDraft, $venueId = null){
        if ($venueId == null){
            $venueId = $this->getVenueId($eventRecord->venue);
        }
        $timezone = $this->locationProvider->getTimeZone($eventRecord->venue);
        $privacy = ($asPrivate) ? 0 : 1;
        $status = ($asDraft) ? "draft" : "live";
        $params = array(
            "title" => $eventRecord->title,
            "description" => $eventRecord->getFormattedDescription(),
            "start_date" => date("Y-m-d H:i:s", $eventRecord->startDate),
            "end_date" => date("Y-m-d H:i:s", $eventRecord->endDate),
            "timezone" => $timezone->getName(),
            "venue_id" => $venueId,
            "privacy" => $privacy,
            "status" => $status
        );
        $url = $this->buildURL("event_new", $params);
        $rawData = $this->call($url);

        $data = json_decode($rawData, true);
        $eventBriteEventId = $data["process"]["id"];
        $error = $data["error"]["error_message"];
        $response = new stdClass();
        $response->record = $eventRecord;
        if ($error){
            $response->message = $error;
        }
        $eventRecord->venue->eventBriteEventID = $eventBriteEventId;
        return $response;
    }

    public function getVenueById($ebVenueId){
        $params = array(
            "id" => $ebVenueId
        );
        $url = $this->buildURL("venue_get", $params);
        return $this->call($url);
    }

    public function getVenueId(Venue $venue, $debug = false){
        // unless $venue object grows an eventbrite venue id, we have to call their API
        $stateAbbr = $venue->region;
        if (strlen($stateAbbr) > 2){
            $stateAbbr = $this->locationProvider->getStateAbbr($venue->region);
        }
        // add default country code if it is missing
        if (strlen(trim($venue->country)) == 0){
            $venue->country = "US";
        }

        // add address to name if it's not already there (eventbrite requires name to be unique for the same organizer/user)
        $venueNameAndAddress = $venue->name;
        if (!strpos($venue->name, $venue->streetAddr)){
            $venueNameAndAddress .= ", ".$venue->streetAddr;
            $venueNameAndAddress .= ", ".$venue->city;
            $venueNameAndAddress .= ", ".$stateAbbr;
            $venueNameAndAddress .= " ".$venue->postalCode;
            $venueNameAndAddress .= " ".$venue->country;
        }
        $params = array(
            "venue" => $venueNameAndAddress,
            "address" => $venue->streetAddr,
            "city" => $venue->city,
            "region" => $stateAbbr,
            "postal_code" => $venue->postalCode,
            "country_code" => $venue->country
        );
        $url = $this->buildURL("venue_new", $params);
        $rawData = $this->call($url);

        // logiccopied from legacy code
        $data = json_decode($rawData, true);
        $candidateVenueId = $data["process"]["id"];
        if ($candidateVenueId){
            return $candidateVenueId;
        }
        // must have been an error; scrape the id out of the error
        $EventVenueError = explode("[",$data["error"]["error_message"]);
        $existingID = rtrim($EventVenueError[1],"]");
        if ($debug){
            return $rawData;
        }
        return $existingID;
    }

    private function buildURL($method, $paramsArray){
        $url = self::$API_URL_BASE.$method
            ."?app_key=".$this->appKey
            ."&user_key=".$this->userKey
            ."&organizer_id=".$this->organizerId;
        $arrayKeys = array_keys($paramsArray);
        for ($i = 0; $i < count($paramsArray); $i++){
            $key = $arrayKeys[$i];
            $url .= "&".$key."=".urlencode($paramsArray[$key]);
        }
        return $url;
    }

    protected function call($URL){
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}

class LoggingEventBriteApi extends EventBriteApi{
    private $eventId;
    private $memberId;

    public function setEventId($eventId){
        $this->eventId = $eventId;
    }

    public function setMemberId($memberId){
        $this->memberId = $memberId;
    }

    protected function call($url){
        $logText = "In Dev Mode EventBrite Post: URL = (".$url.")";
        getEventLogger()->log_action($this->eventId, $logText, $this->memberId);

        $response = array();

        // build some crap data
        if (strpos($url, "ticket_new")){
            $response["id"] = "inDevMode_ticket_".$this->eventId;
        } else if (strpos($url, "venue_new")){
            $response["process"] = array(
                "id" => "inDevMode_venue_".$this->eventId
            );
        } else if (strpos($url, "event_new")){
            $response["process"] = array(
                "id" => "inDevMode_event_".$this->eventId
            );
        }

        return json_encode($response);
    }
}