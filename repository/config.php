<?php
/**
 * some bootstrapping
 */
// generic service includes
include_once("ServiceAPI.php");
include_once("DataContracts.php");

//---------------------
// plug-in configuration
//---------------------
include_once("mySqlProvider/EventProvider.php");
include_once("mySqlProvider/GeoLookupProvider.php");
include_once("Indexing_API.php");
include_once($siteRoot."_setupGeolookupConnection.php");

// event provider
$geolookup = new GeoLookupProvider($geoConn);
$globalEventProvider = new EventProvider($mysqli, $geolookup);
function getEventProvider(){
    return $GLOBALS['globalEventProvider'];
}

// geolocation provider
function getLocationProvider(){
    global $geolookup;
    return $geolookup;
}

// event draft provider
include_once($dbProviderFolder."EventDraftRecordProvider.php");
function getEventDraftProvider(){
    global $mysqli;
    return new EventDraftRecordProvider($mysqli);
}

// event logger
include_once("mySqlProvider/Helper.php");
$globalEventLogger = new DbEventLogger($mysqli);
function getEventLogger(){
    return $GLOBALS['globalEventLogger'];
}

function getExternalEventManagerArray($saveAsDraftOverride){
    $eventManagers = array();

    // add event managers to post to after creation
    if (!$saveAsDraftOverride){
        //
        // Indexden
        //
        $eventManagers[] = getIndexDenEventManager();

        //
        // EventBrite
        //
        $eventManagers[] = getEventbriteEventManager();

        //
        // Facebook
        //
        $eventManagers[] = getFacebookEventManager();
    }
    return $eventManagers;
}

// event managers
include_once($dbProviderFolder."MySqliEventManager.php");
function getMySqliEventManager(){
    global $mysqli;
    return new MySqliEventManager($mysqli);
}

include_once($indexProviderFolder."IndexedEventManager.php");
function getIndexDenEventManager(){
    global $Config_IndexingURLPrivate, $Config_IndexingEventsIndex;
    return new IndexedEventManager($Config_IndexingURLPrivate, $Config_IndexingEventsIndex);
}

include_once($repositoryApiFolder."EventBriteEventManager.php");
function getEventbriteEventManager(){
    global $Config_EventBrite_EventPrivacyType, $isDevMode_Eventbrite, $geolookup,
           $Config_Eventbrite_APIKey, $Config_Eventbrite_Userkey,
           $Config_Eventbrite_OrganizerID;

    if (!isset($Config_EventBrite_EventPrivacyType)){
        $Config_EventBrite_EventPrivacyType = 1; // default to public
    }
    if (!$isDevMode_Eventbrite){
        return new EventBriteEventManager(
            $Config_Eventbrite_APIKey,
            $Config_Eventbrite_Userkey,
            $Config_Eventbrite_OrganizerID,
            $Config_EventBrite_EventPrivacyType,
            $geolookup,
            getMySqliEventManager()
        );
    } else {
        // initialize a "fake" that will log the eventbrite api call instead of calling eventbrite directly
        return new LoggingEventBriteEventManager(
            $Config_Eventbrite_APIKey,
            $Config_Eventbrite_Userkey,
            $Config_Eventbrite_OrganizerID,
            $Config_EventBrite_EventPrivacyType,
            $geolookup,
            getMySqliEventManager()
        );
    }
}

include_once($repositoryApiFolder."FacebookEventManager.php");
function getFacebookEventManager(){
    global $CurrentServer, $isDevMode_Facebook, $geolookup,
           $Config_FaceBook_Token, $Config_FaceBook_CTPageID, $ConfigIgnoreFaceBookTimeZone, $Config_FaceBook_EventPrivacyType;

    if (!strpos($CurrentServer,"localhost") && !$isDevMode_Facebook){
        return new FacebookEventManager(
            $Config_FaceBook_Token,
            $Config_FaceBook_CTPageID,
            $ConfigIgnoreFaceBookTimeZone,
            $Config_FaceBook_EventPrivacyType,
            $geolookup,
            getMySqliEventManager()
        );
    } else {
        // initialize a "fake" that will log the facebook api call instead of calling FB directly
        return new LoggingFacebookEventManager(
            $Config_FaceBook_Token,
            $Config_FaceBook_CTPageID,
            $ConfigIgnoreFaceBookTimeZone,
            $Config_FaceBook_EventPrivacyType,
            $geolookup,
            getMySqliEventManager()
        );
    }
}

// --------------------
// Error Logging
// --------------------

include_once("ErrorLoggingBootstrap.php");
?>