<?php
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/config.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once($rootFolder."/repository/ManagerAPI.php");
include_once($rootFolder."/repository/Indexing_API.php");
include_once("IndexedEventManager.php");
/**
 * The main purposes of this provider is to do the core select on events and create EventRecord objects. This is not a provider
 * that manages CRUD operations.
 */
class IndexedEventRecordProvider implements eventAPI{
    private $eventIndex;

    private $defaultMaximumDistance;
    private $defaultMaximumDistanceUnits;

    public function __construct($indexUrl, $indexName, $maximumDistance = -1, $maximumDistanceUnits = "M"){
        global $Config_IndexDistanceFromEvent;
        if ($maximumDistance < 0){
            $maximumDistance = $Config_IndexDistanceFromEvent ? $Config_IndexDistanceFromEvent : 100;
        }
        $indexProvider = new Indexing_Api($indexUrl);
        $this->eventIndex = new Indexing_Index($indexProvider, $indexName);
        $this->defaultMaximumDistance = $maximumDistance;
        $this->defaultMaximumDistanceUnits = (strtoupper($maximumDistanceUnits[0]) == "M") ? "M" : "K";
    }

    public function getSize(){
        return $this->eventIndex->get_size();
    }

    public function isReady(){
        $size = $this->eventIndex->get_size();
        return !empty($size);
    }

    /**
     * Implement Interface
     * This is for the RSS which lists items in descending chron order.
     * @see repository/eventAPI#getAll()
     */
    public function getAll() {
        $criteria = new EventByLocationCriteria();
        $criteria->upcomingOnly = true;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $paginatedResults = $this->get($criteria);
        return $paginatedResults->collection;
    }

    /**
     * Implement Interface
     * Returns paginated results based on start index and max records per page.
     * Default sort on date and time = 'ASC' and only 'ASC' or 'DESC' is valid.
     * @see repository/eventAPI#getNext($maxRecords)
     * @param $startIndex
     * @param int $maxRecords
     * @param string $dateSort
     * @return array|\PaginatedResult
     */
    public function getNext($startIndex, $maxRecords, $dateSort = "ASC"){
        if (!is_numeric($startIndex) || $startIndex < 0){
            $startIndex = 0;
        }
        if (!is_numeric($maxRecords) || $maxRecords <= 0){
            $maxRecords = 5;
        }
        if (strtolower($dateSort) != "desc"){
            $dateSort = "ASC";
        }

        $criteria = new EventByLocationCriteria();
        $criteria->upcomingOnly = false;
        $criteria->publishedOnly = true;
        $criteria->start = $startIndex;
        $criteria->size = $maxRecords;
        $criteria->multiColumnSort[] = new MultiColumnSort("startDate", $dateSort);
        $criteria->multiColumnSort[] = new MultiColumnSort("startTime", $dateSort);
        $paginatedResults = $this->get($criteria);
        return $paginatedResults;
    }

    private function buildSearchCriteria(EventByLocationCriteria $criteria, $fetch_fields = "*", $snippet_fields = null){
        $indexSearchCriteria = new IndexingSearchCriteria();
        $indexSearchCriteria->start = $criteria->start ? "$criteria->start" : null;
        $indexSearchCriteria->len = $criteria->size ? "$criteria->size" : $this->getSize();
        $indexSearchCriteria->function_filters = $this->getFunctionFilter($criteria);
        $indexSearchCriteria->docvar_filters = $this->getDocvarFilters($criteria);
        $indexSearchCriteria->query_variables = $this->getQueryVariables($criteria);
        $indexSearchCriteria->category_filters = $this->getCategoryFilters($criteria);
        $indexSearchCriteria->fetch_fields = array($fetch_fields);
        $indexSearchCriteria->snippet_fields = $snippet_fields;
        $indexSearchCriteria->scoring_function = $this->getScoringFunction($criteria);

        $queryString = "";
        $qualifierString = "";

        // tagname takes precedence
        if ($criteria->tagName){
            $queryString .= "TAG_LIST:TAG".urlencode($criteria->tagName)."TAG";
        }

        if ($criteria->eventId){
            $queryString .= " (eventId:".urlencode($criteria->eventId).")";
        }

        if ($criteria->orgId){
            $queryString .= " (causeId:".urlencode($criteria->orgId)." OR isCustomerChoice:1)";
        }

        // build keyword query
        if ($criteria->keyword){
            $queryString .= " (text:".urlencode($criteria->keyword)." OR venueStreetAddr:".urlencode($criteria->keyword)." OR causeName:".urlencode($criteria->keyword)." OR TAG_LIST:TAG".urlencode($criteria->keyword)."TAG)";
            $qualifierString = "&fetch_variables=true&fetch_categories=true&match_any_field=true";
        }

        // set a default
        if (empty($queryString)){
            $queryString = "text:Event";
        }
        $indexSearchCriteria->queryString = $queryString.$qualifierString;
        return $indexSearchCriteria;
    }

    /**
     * @param EventByLocationCriteria $criteria
     * @param $fetch_fields string comma-delimited string of field names
     * @param null $snippet_fields
     * @param $debug bool
     * @return PagedResult
     */
    public function get(EventByLocationCriteria $criteria, $fetch_fields = "*", $snippet_fields = null, $debug = false){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();
        if ($debug){
            $result->rawCollection = array();
        }

        $searchCriteria = $this->buildSearchCriteria($criteria, $fetch_fields, $snippet_fields);

        $pagedResults = $this->eventIndex->searchWithCriteria($searchCriteria);

        $result->totalRecords = $pagedResults->matches;

        foreach ($pagedResults->results as $record){
            $result->rawCollection[] = $record;
            $result->collection[] = EventRecord::convertFromIndexedDocument($record);
        }

        $result->setHasMore();
        return $result;
    }

    private function getFunctionFilter(EventByLocationCriteria $criteria){
        if ($criteria->limitByDistance && $criteria->userLat && $criteria->userLong && $criteria->maxRadiusMiles){
            // TODO: also handle when units is not miles
            return array(
                1 => array(array(-$criteria->maxRadiusMiles,NULL))
            );
        }
        return null;
    }

    private function getDocvarFilters(EventByLocationCriteria $criteria){
        $filters = array();
        if ($criteria->merchantId){
            $filters[IndexedEventVariables::DOCVAR_MERCHANT_ID] = array(array($criteria->merchantId,$criteria->merchantId));
        }
        if ($criteria->upcomingOnly){
            // use the beginning of the day, not this minute
            $filters[IndexedEventVariables::DOCVAR_ENDDATE] = array(array(strtotime('now'),NULL));
        }
        if ($criteria->onlineOnly > 0){
            $filters[IndexedEventVariables::DOCVAR_ONLINE_EVENT] = array(array(1,1)); // values from 1 to 1 inclusive
        } else if ($criteria->onlineOnly < 0){
            $filters[IndexedEventVariables::DOCVAR_ONLINE_EVENT] = array(array(0,0)); // values from 0 to 0 inclusive
        }
        return count($filters) ? $filters : null;
    }

    private function getQueryVariables(EventByLocationCriteria $criteria){
        if (count($criteria->multiColumnSort) && $criteria->multiColumnSort[0]->column == 'active'){
            $queryVars = array(
                IndexedEventVariables::QUERYVAR_CURRENTDATE => strtotime(date("Y-m-d", time()))
            );
            if ($criteria->userLat && $criteria->userLong){
                $queryVars[IndexedEventVariables::QUERYVAR_LAT] = $criteria->userLat;
                $queryVars[IndexedEventVariables::QUERYVAR_LONG] = $criteria->userLong;
            }
            return $queryVars;
        }
        if ($criteria->userLat && $criteria->userLong){
            return array(
                IndexedEventVariables::QUERYVAR_LAT => $criteria->userLat,
                IndexedEventVariables::QUERYVAR_LONG => $criteria->userLong
            );
        }
        return null;
    }

    private function getCategoryFilters(EventByLocationCriteria $criteria){
        return null;
    }

    private function getScoringFunction(EventByLocationCriteria $criteria){
        $function = null; // default sort
        if (count($criteria->multiColumnSort)){
            $sortColumns = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                //$sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : ""; // ignored
                switch ($multiColumnSort->column){
                    case "active":
                        $sortColumns[] = "active";
                        break;
                    case "timestamp":
                        $sortColumns[] = "timestamp";
                        break;
                    case "startDate":
                        $sortColumns[] = "startDate";
                        break;
                    case "endDate":
                        // ignored
                        break;
                    case "startTime":
                        // ignored
                        break;
                    case "endTime":
                        // ignored
                        break;
                    case "distance":
                        // sorting by distance requires a maximum distance
                        if (is_numeric($criteria->userLat) && is_numeric($criteria->userLong)){
                            $sortColumns[] = "distance";
                        }
                        break;
                }
            }
            if (count($sortColumns) == 1){
                if ($sortColumns[0] == "timestamp"){
                    $function = IndexedEventManager::SCORING_CREATEDATE;
                } else if ($sortColumns[0] == "distance"){
                    $function = IndexedEventManager::SCORING_DISTANCE_OR_NATIONAL;
                }
            } else if (count($sortColumns) > 1){
                $firstSort = $sortColumns[0];
                $secondSort = $sortColumns[1];
                if ($firstSort == "distance" && $secondSort == "startDate"){
                    $function = IndexedEventManager::SCORING_SORT_BY_DISTANCE_THEN_START_DATE;
                } else if ($firstSort == "active" && $secondSort == "distance"){
                    $function = IndexedEventManager::SCORING_SORT_BY_ACTIVE_THEN_DISTANCE;
                } else if (in_array("distance", $sortColumns)){
                    $function = IndexedEventManager::SCORING_SORT_BY_START_DATE_THEN_DISTANCE;
                }
            }
        }
        return $function;
    }

    private function getScoringFunctionOld(EventByLocationCriteria $criteria){
        $function = null; // default sort
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "timestamp":
                        $orderByStrings[] = "e.Event_ID ".$sortOrder;
                        break;
                    case "startDate":
                        $orderByStrings[] = "e.Event_Date ".$sortOrder;
                        break;
                    case "endDate":
                        $orderByStrings[] = "e.Event_EndDate ".$sortOrder;
                        break;
                    case "startTime":
                        $orderByStrings[] = "e.Event_StartTime ".$sortOrder;
                        break;
                    case "endTime":
                        $orderByStrings[] = "e.Event_EndTime ".$sortOrder;
                        break;
                    case "distance":
                        // sorting by distance requires a maximum distance
                        if (is_numeric($criteria->userLat) && is_numeric($criteria->userLong)){
                            $orderByStrings[] = "distance ".$sortOrder;
                        }
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
            // now assign scoring function if we have a match
            // FLAW: this logic does not account for descending sort order...
            if ($orderBy == " ORDER BY e.Event_ID"){
                // default
                $function = IndexedEventManager::SCORING_CREATEDATE;
            } else if (strpos($orderBy, "ORDER BY distance , e.Event_Date")){
                // distance then start date
                $function = IndexedEventManager::SCORING_SORT_BY_DISTANCE_THEN_START_DATE;
            } else if (strpos($orderBy, "ORDER BY distance")){
                // distance only
                $function = IndexedEventManager::SCORING_DISTANCE_OR_NATIONAL;
            } else if (strpos($orderBy, ", distance ")){
                // fall through default
                $function = IndexedEventManager::SCORING_SORT_BY_START_DATE_THEN_DISTANCE;
            }
        }
        return $function;
    }
}