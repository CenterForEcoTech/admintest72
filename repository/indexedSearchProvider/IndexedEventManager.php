<?php
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/config.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once($rootFolder."/repository/ManagerAPI.php");
include_once($rootFolder."/repository/Indexing_API.php");

/**
 * Class for managing events. CRUD operations, etc.
 */
class IndexedEventManager implements EventManager{
    private $eventIndex;

    public function __construct($indexUrl, $indexName){
        $indexProvider = new Indexing_Api($indexUrl);
        $this->eventIndex = new Indexing_Index($indexProvider, $indexName);
    }

    const SCORING_CREATEDATE = 0;
    const SCORING_DISTANCE_OR_NATIONAL = 1;
    const SCORING_SORT_BY_DISTANCE_THEN_START_DATE = 2;
    const SCORING_SORT_BY_START_DATE_THEN_DISTANCE = 3;
    const SCORING_SORT_BY_ACTIVE_THEN_DISTANCE = 4;

    // see DataContracts (IndexedEventVariables) for what the indexes mean in the functions
    private static $SCORING_FUNCTIONS = array(
        // ascending by start datetime
        0 => "-d[2]",

        // if national, zero miles, else distance from user lat/long
        1 => "if(d[5], 0, -miles(d[0], d[1], q[0], q[1]))",

        // sort by ascending distance, then by start datetime
        2 => "(if(d[5], -100, -miles(d[0], d[1], q[0], q[1])))-d[2]/10000000000",

        // sort by ascending start datetime, then by distance, putting national events at the bottom
        3 => "(if(d[5], -100, -miles(d[0], d[1], q[0], q[1])))-(d[2]/1200)",

        // sort by ascending active, then by distance
        4 => "(if(d[6] < q[2], -5000, 0)) + (if(d[5], -100, -miles(d[0], d[1], q[0], q[1])))"
    );

    private function addScoringFunction($index, $functionString){
        try {
            if (strlen($functionString)){
                $resultStatus = $this->eventIndex->add_function("$index", "$functionString");
            } else {
                $resultStatus = $this->eventIndex->delete_function("$index");
            }
            if ($resultStatus != 200){
                trigger_error("IndexedEventManager.addScoringFunction did not return successfully. Status = ".$resultStatus, E_USER_WARNING);
            }
        } catch (Exception $e) {
            trigger_error("IndexedEventManager.addScoringFunction error: ".$e->getMessage(), E_USER_ERROR);
        }
    }

    /**
     * Adds/Updates scoring functions
     */
    public function addScoringFunctions(){
        foreach (self::$SCORING_FUNCTIONS as $index => $functionString){
            $this->addScoringFunction($index, $functionString);
        }
    }

    private function areVenuesValid($arrayOfVenues){
        $isValid = count($arrayOfVenues) > 0;
        foreach($arrayOfVenues as $venue){
            if (!($venue instanceof Venue) || !($venue->id)){
                $isValid = false;
                break;
            }
        }
        return $isValid;
    }

    private function deleteDocument($eventId, $venueId, ExecuteResponseRecord $executeResponse, $memberId, $adminId){
        $docId = $eventId."_".$venueId;
        $httpStatusCode = $this->eventIndex->delete_document($docId);
        if ($httpStatusCode >= 200 && $httpStatusCode <= 299){
            $executeResponse->success = true;
            $executeResponse->affectedRows++;
            getEventLogger()->log_action($eventId, "Deleted from index service: ".$docId, $memberId, $adminId);
        } else {
            // don't turn a true to a false; true means at least one doc was removed. if it is important to know that some failed, then handle it
            //$executeResponse->success = false;
        }
    }

    /**
     * Helper utility to delete a specific document by the doc_id.
     * @param $docId
     * @return int httpStatusCode
     */
    public function delete($docId){
        return $this->eventIndex->delete_document($docId);
    }

    /**
     * Removes the associated documents from the index. Removes the document regardless of the existence of transactions.
     * @param EventRecord $eventRecord
     * @param $arrayOfVenues
     * @param int $memberId
     * @param Array of Venue $arrayOfVenues
     * @return ExecuteResponseRecord
     */
    public function deleteEvent(EventRecord $eventRecord, $arrayOfVenues, $memberId = 0, $adminId = null){
        $executeResponse = new ExecuteResponseRecord();
        if ($eventRecord->id && !$eventRecord->isDraft && ($eventRecord->venue->id || $this->areVenuesValid($arrayOfVenues))){
            $eventId = $eventRecord->id;
            if ($eventRecord->venue->id){
                $this->deleteDocument($eventId, $eventRecord->venue->id, $executeResponse, $memberId, $adminId);
            } else {
                foreach ($arrayOfVenues as $venue){
                    $this->deleteDocument($eventId, $venue->id, $executeResponse, $memberId, $adminId);
                }
            }
        } else {
            $executeResponse->error = "Invalid Operation: EventRecord must refer to a published event and arrayOfVenues must contain one or more Venue objects";
        }
        return $executeResponse;
    }

    public function updateEvent(EventRecord $eventRecord, $memberId = 0, $adminId = null){
        $executeResponse = new ExecuteResponseRecord();
        if ($eventRecord->venue && $eventRecord->venue->id){
            $document = $eventRecord->convertToIndexedDocument();
            $docId = $document['docid'];

            // turns out, the internal call always returns 200, so there is no way to tell if add_document fails or succeeds.
            // Therefore, we use add_documents that at least returns the response to us.
            $indexingResponse = (object)$this->eventIndex->add_documents(array($document));
            if (!empty($indexingResponse->error)){
                $executeResponse->error = "Error updating index for ".$docId.": ".$indexingResponse->error;
            } else {
                $executeResponse->success = true;
                $executeResponse->insertedId = $docId;
                getEventLogger()->log_action($eventRecord->id, 'Indexed docid='.$docId, $memberId, $adminId);
            }
        }
        return $executeResponse;
    }

    public function createEvent(EventRecord $eventRecord, $memberId = 0, $adminId = null){
        return $this->updateEvent($eventRecord, $memberId, $adminId);
    }
}