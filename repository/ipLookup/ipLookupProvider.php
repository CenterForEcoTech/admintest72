<?php
include("geoipcity.php");
class GeoLocationData{
    public $lat;
    public $long;
    public $city;
    public $region;
    public $postalCode;
}

class IpLookupProvider{
    public static function lookup($ip, GeoLocationData $dataRecord){
        global $GEOIP_REGION_NAME, $siteRoot;
        $dataFile = $siteRoot."repository/ipLookup/GeoLiteCity.dat";
        if ($ip && file_exists($dataFile)){
            $gi = geoip_open($dataFile,GEOIP_STANDARD);
            $record = geoip_record_by_addr($gi,$ip);
            $region = $GEOIP_REGION_NAME[$record->country_code][$record->region];
            if ($record->city && $region){
                $dataRecord->lat = $record->latitude;
                $dataRecord->long = $record->longitude;
                $dataRecord->city = $record->city;
                $dataRecord->region = $region;
                $dataRecord->postalCode = $record->postal_code;
            }
            geoip_close($gi);
        }
        return $dataRecord;
    }
}