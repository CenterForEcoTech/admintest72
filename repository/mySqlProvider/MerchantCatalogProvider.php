<?php

class MerchantCatalogProvider {
    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    //
    // Pagination queries
    //

    private static $PAGINATED_SELECT_COLUMNS = "
      SELECT
            c.id,
            c.name,
            c.title,
            c.subtitle,
            c.description,
            c.status,
            (
              SELECT count(*)
              FROM merchant_catalog_merchant cm
              WHERE cm.merchant_catalog_id = c.id
              AND cm.status = 'ACTIVE'
            ) as numMerchants";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM merchant_catalog c
        WHERE 1 = 1";

    private static $FILTER_ON_KEYWORD = "
        AND (
            c.name LIKE ?
            OR c.title LIKE ?
            OR c.subtitle LIKE ?
        )";

    private static $FILTER_ON_ID = "
        AND c.id = ?";

    private static $FILTER_ON_NAME = "
        AND c.name = ?";

    private static $FILTER_ON_IS_VISIBLE = "
        AND c.status = 'ACTIVE'";

    public function get(MerchantCatalogCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        $merchantListProvider = null;

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
        }

        if ($criteria->isVisible){
            $filterString .= self::$FILTER_ON_IS_VISIBLE;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $catalogRecord = $this->mapTo((object)$row);
                        $result->collection[] = $catalogRecord;
                    }
                } else {
                    trigger_error("MerchantCatalogProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("MerchantCatalogProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY c.id";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "name":
                        $orderByStrings[] = "c.name ".$sortOrder;
                        break;
                    case "title":
                        $orderByStrings[] = "c.title ".$sortOrder;
                        break;
                    case "subtitle":
                        $orderByStrings[] = "c.subtitle ".$sortOrder;
                        break;
                    case "numMerchants":
                        $orderByStrings[] = "numMerchants ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private function mapTo($dbRow){
        $record = new MerchantCatalogRecord();
        $record->id = $dbRow->id;
        $record->name = $dbRow->name;
        $record->title = $dbRow->title;
        $record->subtitle = $dbRow->subtitle;
        $record->description = $dbRow->description;
        $record->numMerchants = $dbRow->numMerchants;
        $record->status = $dbRow->status;
        return $record;
    }

    public function getCatalogMerchants(MerchantCatalogRecord $record){
        $merchantListProvider = new MerchantCatalogMerchantProvider($this->mysqli, $record->id);
        $merchantListResponse = $merchantListProvider->get(new PaginationCriteria());
        return $merchantListResponse->collection;
    }

    public function put(MerchantCatalogRecord $record, $userId){
        if ($record->id){
            return $this->update($record, $userId);
        } else {
            return $this->insert($record, $userId);
        }
    }

    private static $INSERT_SQL = "
        INSERT INTO merchant_catalog
        (
            name,
            title,
            subtitle,
            description,
            status,
            created_by
        )
        SELECT
            ?,?,?,?,?,?
        FROM dual";

    private static $INSERT_BINDING = "sssssi";

    private function insert(MerchantCatalogRecord $record, $userId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = self::$INSERT_BINDING;
        $sqlParams = array(
            $record->name,
            $record->title,
            $record->subtitle,
            $record->description,
            $record->status ? $record->status : MerchantCatalogRecord::ACTIVE_STATUS,
            $userId
        );
        $insertResponse = MySqliHelper::executeWithLogging("MerchantCatalogProvider.insert SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        if ($insertResponse->success){
            $record->id = $insertResponse->insertedId;
        }
        return $insertResponse;
    }

    private static $UPDATE_SQL = "
        UPDATE merchant_catalog
        SET name = ?,
            title = ?,
            subtitle = ?,
            description = ?,
            status = ?,
            updated_date = CURRENT_TIMESTAMP,
            updated_by = ?
        WHERE id = ?";

    private static $UPDATE_BINDING = "sssssii";

    private function update(MerchantCatalogRecord $record, $userId){
        $sqlString = self::$UPDATE_SQL;
        $sqlBinding = self::$UPDATE_BINDING;
        $sqlParams = array(
            $record->name,
            $record->title,
            $record->subtitle,
            $record->description,
            $record->status ? $record->status : MerchantCatalogRecord::ACTIVE_STATUS,
            $userId,
            $record->id
        );
        $updateResponse = MySqliHelper::executeWithLogging("MerchantCatalogProvider.update SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $updateResponse;
    }

    private static $ADD_MERCHANT_SQL = "
        INSERT INTO merchant_catalog_merchant
        (
            merchant_catalog_id,
            merchant_info_id,
            created_by
        )
        SELECT
            ?,?,?
        FROM merchant_info
        WHERE MerchantInfo_ID = ?
        ON DUPLICATE KEY UPDATE
            status = 'ACTIVE'";

    public function addMerchant(MerchantCatalogRecord $record, $merchantId, $userId){
        $sqlString = self::$ADD_MERCHANT_SQL;
        $sqlBinding = "iiii";
        $sqlParams = array(
            $record->id,
            $merchantId,
            $userId,
            $merchantId
        );
        return MySqliHelper::executeWithLogging("MerchantCatalogProvider.addMerchant SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $REMOVE_MERCHANT_SQL = "
        UPDATE merchant_catalog_merchant
        SET status = 'DELETED',
            updated_date = CURRENT_TIMESTAMP,
            updated_by = ?
        WHERE merchant_catalog_id = ?
        AND merchant_info_id = ?";

    public function removeMerchant(MerchantCatalogRecord $record, $merchantId, $userId){
        $sqlString = self::$REMOVE_MERCHANT_SQL;
        $sqlBinding = "iii";
        $sqlParams = array(
            $userId,
            $record->id,
            $merchantId
        );
        return MySqliHelper::executeWithLogging("MerchantCatalogProvider.removeMerchant SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}

class MerchantCatalogMerchantProvider{
    /* @var mysqli */
    private $mysqli;
    private $catalogId;

    public function __construct(mysqli $mysqli, $catalogId){
        $this->mysqli = $mysqli;
        $this->catalogId = $catalogId;
    }

    private static $PAGINATED_SELECT_COLUMNS = "
        SELECT
            mcm.id as id,
            MerchantInfo_ID as merchant_info_id,
            MerchantInfo_Name as name,
            MerchantInfo_Address as address,
            MerchantInfo_City as city,
            MerchantInfo_State as state,
            MerchantInfo_Zip as postalCode,

            MerchantInfo_ContactFirstName as contactFirstName,
            MerchantInfo_ContactLastName as contactLastName,
            MerchantInfo_ContactPhone as contactPhone,
            MerchantInfo_ContactEmail as contactEmail,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            (
                SELECT count(*)
                FROM merchant_images mi
                WHERE mi.merchant_info_id = MerchantInfo_ID
                AND mi.status = 'APPROVED'
            ) as numImages";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM merchant_catalog_merchant mcm
        JOIN merchant_info m ON mcm.merchant_info_id = m.MerchantInfo_ID
        WHERE mcm.merchant_catalog_id = ?
        AND mcm.status = 'ACTIVE'";

    private static $FILTER_ON_KEYWORD = "
        AND (
            MerchantInfo_Name LIKE ?
            OR MerchantInfo_City LIKE ?
            OR MerchantInfo_State LIKE ?
            OR MerchantInfo_Zip LIKE ?
            OR MerchantInfo_ContactFirstName LIKE ?
            OR MerchantInfo_ContactLastName LIKE ?
            OR MerchantInfo_ContactEmail LIKE ?
        )";

    private static $FILTER_ON_ID = "
        AND mcm.merchant_info_id = ?";

    public function get(PaginationCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "i";
        $filterParams = array(
            $this->catalogId
        );
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = $this->mapTo((object)$row);
                    }
                } else {
                    trigger_error("MerchantCatalogMerchantProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("MerchantCatalogMerchantProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY mcm.id";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "numImages":
                        $orderByStrings[] = "numImages ".$sortOrder;
                        break;
                    case "merchantIdDisplay":
                        $orderByStrings[] = "MerchantInfo_ID ".$sortOrder;
                        break;
                    case "contactInfo":
                        $orderByStrings[] = "MerchantInfo_Name ".$sortOrder;
                        break;

                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
    private function mapTo($dbRow){
        return self::mapToRecord($dbRow, $this->catalogId);
    }

    public static function mapToRecord($dbRow, $catalogId){
        $record = new MerchantCatalogMerchantRecord();
        $record->id = $dbRow->id;
        $record->catalogId = $catalogId;
        $record->merchantId = $dbRow->merchant_info_id;
        $record->name = $dbRow->name;
        $record->address = $dbRow->address;
        $record->city = $dbRow->city;
        $record->state = $dbRow->state;
        $record->zip = $dbRow->postalCode;
        $record->firstName = $dbRow->contactFirstName;
        $record->lastName = $dbRow->contactLastName;
        $record->email = $dbRow->contactEmail;
        $record->phone = $dbRow->contactPhone;
        $record->pid = $dbRow->pid;
        $record->numImages = $dbRow->numImages;
        $record->logoPath = $dbRow->logoPath;
        $record->logoFile = $dbRow->logoFile;
        return $record;
    }
}

class MerchantCatalogMerchantFinder {
    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    /**
     * Finds merchants involved in a specific campaign who are not also included in the specified catalog
     * @param $campaignId
     * @param $catalogId
     * @return array
     */
    public function findMerchantsByCampaign($campaignId, $catalogId){
        $sqlString = self::$MERCHANTS_BY_CAMPAIGN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $campaignId,
            $catalogId
        );
        $queryResponse = MySqliHelper::getResultWithLogging("MerchantCatalogMerchantFinder.findMerchantsByCampaign: SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        $collection = array();
        foreach ($queryResponse->resultArray as $dbRow){
            $collection[] = MerchantCatalogMerchantProvider::mapToRecord((object)$dbRow, $catalogId);
        }
        return $collection;
    }

    private static $MERCHANTS_BY_CAMPAIGN_SQL = "
        SELECT
            MerchantInfo_ID as merchant_info_id,
            MerchantInfo_Name as name,
            MerchantInfo_Address as address,
            MerchantInfo_City as city,
            MerchantInfo_State as state,
            MerchantInfo_Zip as postalCode,

            MerchantInfo_ContactFirstName as contactFirstName,
            MerchantInfo_ContactLastName as contactLastName,
            MerchantInfo_ContactPhone as contactPhone,
            MerchantInfo_ContactEmail as contactEmail,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            (
                SELECT count(*)
                FROM merchant_images mi
                WHERE mi.merchant_info_id = MerchantInfo_ID
                AND mi.status = 'APPROVED'
            ) as numImages,
            i.folder_url as logoPath,
            i.file_name as logoFile
        FROM merchant_info
        LEFT JOIN cms_image_library i ON MerchantInfo_ImageLibraryID = i.id AND i.status = 'ACTIVE'
        WHERE EXISTS (
            SELECT 1
            FROM event_info
            JOIN tag_association ON TagAssociation_SourceTable = 'event_info' AND TagAssociation_SourceID = Event_ID AND TagAssociation_Status IS NULL
            WHERE TagAssociation_TagListID = ?
            AND MerchantInfo_ID = Event_ProposedByMerchantID AND Event_Status = 'ACTIVE'
            LIMIT 1
        )
        AND NOT EXISTS (
            SELECT 1
            FROM merchant_catalog_merchant mcm
            WHERE mcm.merchant_catalog_id = ?
            AND mcm.merchant_info_id = MerchantInfo_ID
            AND mcm.status = 'ACTIVE'
        )
        ORDER BY MerchantInfo_Name";
}