<?php

class NominationQueryCriteria extends PaginationCriteria {
    public $date = null;
    public $orgName = null;
    public $businessName = null;
}

class NominatedMerchantRecord extends MerchantRecord {
    /**
     * The primary key of the nominated business record
     * @var $nominatedBusinessId int
     */
    public $nominatedBusinessId;

    /**
     * This is city grid's value "public_id" which presumably is a string
     * @var $cityGridPublicId string
     */
    public $cityGridPublicId;

    /**
     * This is city grid's value of "id" which is presumably an int
     * @var $cityGridProfileId int
     */
    public $cityGridProfileId;
}

class NonprofitPreferredBusiness {
    /**
     * The primary key of this record
     * @var $id int
     */
    public $id;
    /**
     * Timestamp when nomination occurred
     * @var $time int
     */
    public $time;
    /**
     * The organization that nominated this merchant
     * @var $orgRecord OrganizationRecord
     */
    public $orgRecord;
    /**
     * A real or manufactured NominatedMerchantRecord
     * @var $businessRecord NominatedMerchantRecord
     */
    public $businessRecord;
    /**
     * The original location used to search for the business
     * @var $searchLocation string
     */
    public $searchLocation;
    /**
     * The original string used to search for the business
     * @var $searchName string
     */
    public $searchName;
    /**
     * The comment entered by the nonprofit when nominating the merchant
     * @var $comments string
     */
    public $comments;

    /**
     * Some shortcuts to quickly get commonly referenced properties
     * @param $name
     * @return string
     */
    public function __get($name){
        switch ($name){
            case "date":
                return date("Y-m-d", $this->time);
            case "orgName":
                return $this->orgRecord->name;
            case "businessName":
                return $this->businessRecord->name;
            default:
                return $this->$name;
        }
    }
}

class AdminNonprofitPreferredBusiness extends NonprofitPreferredBusiness{
    /**
     * Comments entered by an administrator
     * @var $adminComments string
     */
    public $adminComments;

    /**
     * Returns true if record is deleted
     * @var $isDeleted boolean
     */
    public $isDeleted;
}

class NominatedBusinessProvider {
    /* @var $mysqli mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $NOMINATION_SELECT_COLUMNS = "
        SELECT
            business.id AS nominated_business_id,
            business.city_grid_public_id,
            business.city_grid_profile_id,
            business.name AS businessName,
            business.address AS businessAddress,
            business.city AS businessCity,
            business.state AS businessState,
            business.zip AS businessZip,
            business.phone AS businessPhone,
            business.website AS businessWebsite,
            business.description AS businessDescription,
            nomination.id AS nonprofit_preferred_business_id,
            nomination.organization_info_id,
            nomination.merchant_info_id,
            nomination.nomination_comment,
            nomination.admin_comment,
            nomination.nomination_name,
            nomination.nomination_location,
            nomination.created_date,
            nomination.status,
            org.OrganizationInfo_Name AS orgName,
            org.OrganizationInfo_Address AS orgAddress,
            org.OrganizationInfo_City AS orgCity,
            org.OrganizationInfo_State AS orgState,
            org.OrganizationInfo_Zip AS orgZip,
            org.OrganizationInfo_Website AS orgWebsite,
            org.OrganizationInfo_ContactPhone as orgContactPhone,
            org.OrganizationInfo_ContactEmail as orgContactEmail,
            org.OrganizationInfo_ContactFirstName as orgContactFirstName,
            org.OrganizationInfo_ContactLastName as orgContactLastName,
            (
                SELECT pid
                FROM permalinks
                JOIN merchant_info ON MerchantInfo_ID = entity_id AND entity_name = 'merchant'
                WHERE (
                    MerchantInfo_ID = nomination.merchant_info_id
                    OR MerchantInfo_CityGridID = business.city_grid_profile_id
                )
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
            ) AS businessPid";

    private static $FROM_TABLE_CLAUSE = "
        FROM nonprofit_preferred_businesses nomination
        JOIN organization_info org ON org.OrganizationInfo_ID = nomination.organization_info_id
        LEFT JOIN nominated_business business ON business.id = nomination.nominated_business_id
        WHERE 1 = 1";

    private static $FILTER_ON_ACTIVE = "
        AND nomination.status = 'ACTIVE'";

    private static $FILTER_ON_ORGID = "
        AND nomination.organization_info_id = ?";

    private static $FILTER_ON_DATE = "
        AND nomination.created_date > ?";

    private static $FILTER_ON_ORGNAME = "
        AND org.OrganizationInfo_Name LIKE ?";

    private static $FILTER_ON_BUSINESSNAME = "
        AND (
            business.name LIKE ?
            OR nomination.nomination_name LIKE ?
        )";

    private static $FILTER_ON_KEYWORD = "
        AND (
            business.name LIKE ?
            OR business.address LIKE ?
            OR business.city LIKE ?
            OR business.state LIKE ?
            OR business.zip LIKE ?
            OR business.phone LIKE ?
            OR business.website LIKE ?
            OR nomination.nomination_comment LIKE ?
            OR nomination.admin_comment LIKE ?
            OR nomination.nomination_name LIKE ?
            OR nomination.nomination_location LIKE ?
            OR org.OrganizationInfo_Name LIKE ?
            OR org.OrganizationInfo_Address LIKE ?
            OR org.OrganizationInfo_City LIKE ?
            OR org.OrganizationInfo_State LIKE ?
            OR org.OrganizationInfo_Zip LIKE ?
            OR org.OrganizationInfo_ContactPhone LIKE ?
            OR org.OrganizationInfo_ContactEmail LIKE ?
            OR org.OrganizationInfo_ContactFirstName LIKE ?
            OR org.OrganizationInfo_ContactLastName LIKE ?
        )";

    /**
     * Returns an array of NonprofitPreferredBusiness objects
     * @param $orgId int
     * @return array
     */
    public function getNonprofitPreferredBusinesses($orgId){
        $collection = array();
        $sqlString = self::$NOMINATION_SELECT_COLUMNS
                    .self::$FROM_TABLE_CLAUSE
                    .self::$FILTER_ON_ORGID
                    .self::$FILTER_ON_ACTIVE
                    .self::$ORDER_BY_BUSINESS;
        $sqlBinding = "i";
        $sqlParam = array($orgId);
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        foreach ($sqlResult as $row){
            $rowObject = (object)$row;

            // create the admin record
            $record = new NonprofitPreferredBusiness();
            $this->mapToRecord($rowObject, $record);
            $collection[] = $record;
        }
        return $collection;
    }

    /**
     * @param NominationQueryCriteria $criteria
     * @return PagedResult
     */
    public function getAdminNominations(NominationQueryCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$NOMINATION_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->orgName){
            $orgCriteria = "%".$criteria->orgName."%";
            $filterString .= self::$FILTER_ON_ORGNAME;
            $filterBinding .= "s";
            $filterParams[] = $orgCriteria;
        }
        if ($criteria->businessName){
            $businessCriteria = "%".$criteria->businessName."%";
            $filterString .= self::$FILTER_ON_BUSINESSNAME;
            $filterBinding .= "ss";
            $filterParams[] = $businessCriteria;
            $filterParams[] = $businessCriteria;
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $rowObject = (object)$row;

            // create the admin record
            $record = new AdminNonprofitPreferredBusiness();
            $record->adminComments = $rowObject->admin_comment;
            $record->isDeleted = $rowObject->status == "DELETED";
            $this->mapToRecord($rowObject, $record);
            $result->collection[] = $record;
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY nomination.sort_order";

    private static $ORDER_BY_ID = " ORDER BY nomination.id";

    private static $ORDER_BY_DATE = " ORDER BY nomination.created_date";

    private static $ORDER_BY_ORG = " ORDER BY org.OrganizationInfo_Name";

    private static $ORDER_BY_BUSINESS = " ORDER BY nomination.nomination_name";

    private static $ORDER_BY_COMMENTS = " ORDER BY nomination.nomination_comment";

    private static $ORDER_BY_ADMIN_COMMENTS = " ORDER BY nomination.admin_comment";

    private function getOrderByClause(NominationQueryCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "orgDisplay":
                    $orderBy = self::$ORDER_BY_ORG." ".$sortOrder;
                    break;
                case "businessDisplay":
                    $orderBy = self::$ORDER_BY_BUSINESS." ".$sortOrder;
                    break;
                case "comments":
                    $orderBy = self::$ORDER_BY_COMMENTS." ".$sortOrder;
                    break;
                case "adminComments":
                    $orderBy = self::$ORDER_BY_ADMIN_COMMENTS." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }

    private function mapToRecord($row, NonprofitPreferredBusiness $preferredBusinessRecord){
        // nominated business
        $businessRecord = new NominatedMerchantRecord();
        $businessRecord->nominatedBusinessId = $row->nominated_business_id;
        $this->mapToMerchantRecord($row, $businessRecord);

        // nominating org
        $orgRecord = new OrganizationRecord();
        $this->mapToOrganizationRecord($row, $orgRecord);
        $record = $preferredBusinessRecord;

        // nomination record
        $record->businessRecord = $businessRecord;
        $record->orgRecord = $orgRecord;
        $record->id = $row->nonprofit_preferred_business_id;
        $record->comments = $row->nomination_comment;
        $record->time = strtotime($row->created_date);
    }

    private function mapToMerchantRecord($row, MerchantRecord $record){
        if (empty($row->nominated_business_id)){
            // nothing found in external business lookup
            $record->name = $row->nomination_name;
            $record->address = $row->nomination_location;
            return;
        }
        $record->id = $row->merchant_info_id;
        $record->pid = $row->businessPid;
        $record->name = $row->businessName;
        $record->address = $row->businessAddress;
        $record->city = $row->businessCity;
        $record->state = $row->businessState;

        $record->postalCode = $row->businessZip;
        $record->phone = $row->businessPhone;
        $record->setURL($row->businessWebsite);
        $record->descriptionText = $row->businessDescription;
    }

    private function mapToOrganizationRecord($row, OrganizationRecord $record){
        $record->id = $row->organization_info_id;
        $record->name = $row->orgName;
        $record->address = $row->orgAddress;
        $record->city = $row->orgCity;
        $record->state = $row->orgState;

        $record->postalCode = $row->orgZip;
        $record->setURL($row->orgWebsite);

        $contactRecord = new ContactRecord();
        $contactRecord->firstname = $row->orgContactFirstName;
        $contactRecord->lastname = $row->orgContactLastName;

        $contactRecord->setPhone($row->orgContactPhone);
        $contactRecord->email = $row->orgContactEmail;
        $record->primaryContact = $contactRecord;
    }

    // -----------------
    // Add
    // -----------------

    private static $INSERT_PREFERRED_BUSINESS_SQL = "
        INSERT INTO nonprofit_preferred_businesses
        (
          organization_info_id,
          merchant_info_id,
          nominated_business_id,
          nomination_comment,
          nomination_location,
          nomination_name
        )
        SELECT
          ?,?,?,?,?,?
        FROM DUAL
        WHERE NOT EXISTS(
          SELECT 1
          FROM nonprofit_preferred_businesses
          WHERE organization_info_id = ?
          AND (
            0 != ? AND nominated_business_id = ?
            OR 0 = ? AND nomination_name = ? AND nomination_location = ?
          )
        ) ";

    public function add(NonprofitPreferredBusiness $preferredBusiness){
        // TODO: apply graceful validation
        $businessRecord = $preferredBusiness->businessRecord;
        $this->checkOrAdd($businessRecord);

        $orgId = $preferredBusiness->orgRecord->id;
        if (!is_numeric($orgId)){
            throw new Exception("NominatedBusinesProvider.add requires valid org id");
        }

        $sqlString = self::$INSERT_PREFERRED_BUSINESS_SQL;
        $sqlBinding = "iiisss"; // insert
        $sqlBinding .= "iiiiss"; // filter
        $sqlParam = array(
            $orgId,
            $businessRecord->id,
            $businessRecord->nominatedBusinessId,
            $preferredBusiness->comments,
            $preferredBusiness->searchLocation."",
            $preferredBusiness->searchName."",

            $orgId,
            $businessRecord->nominatedBusinessId,
            $businessRecord->nominatedBusinessId,
            $businessRecord->nominatedBusinessId,
            $preferredBusiness->searchName."",
            $preferredBusiness->searchLocation.""
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $preferredBusiness->id = $insertResponse->insertedId;
            return $insertResponse;
        } else {
            return $this->updateExistingRecord($preferredBusiness);
        }
    }

    private static $UPDATE_PREFERRED_BUSINESS = "
        UPDATE nonprofit_preferred_businesses
        SET
          merchant_info_id = ?,
          nominated_business_id = ?,
          nomination_comment = ?,
          nomination_location = ?,
          nomination_name = ?,
          status = 'ACTIVE',
          updated_date = CURRENT_TIMESTAMP
        WHERE id = ?";

    private function updateExistingRecord(NonprofitPreferredBusiness $preferredBusiness){
        $this->setExistingRecordData($preferredBusiness);
        $updateResponse = new ExecuteResponseRecord();
        if ($preferredBusiness->id){
            $sqlString = self::$UPDATE_PREFERRED_BUSINESS;
            $sqlBinding = "iisssi";
            $sqlParams = array(
                $preferredBusiness->businessRecord->id,
                $preferredBusiness->businessRecord->nominatedBusinessId,
                $preferredBusiness->comments,
                $preferredBusiness->searchLocation,
                $preferredBusiness->searchName,
                $preferredBusiness->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
        }
        return $updateResponse;
    }

    private static $GET_PREFERRED_BUSINESS = "
        SELECT
          id,
          merchant_info_id,
          nominated_business_id,
          nomination_name,
          nomination_location
        FROM nonprofit_preferred_businesses
        WHERE organization_info_id = ?
        AND ( 0 != ? AND nominated_business_id = ?
          OR 0 = ? AND nomination_name = ? AND nomination_location = ?)";

    private function setExistingRecordData(NonprofitPreferredBusiness $preferredBusiness){
        $sqlString = self::$GET_PREFERRED_BUSINESS;
        $sqlBinding = "iiiiss";
        $sqlParams = array(
            $preferredBusiness->orgRecord->id,
            $preferredBusiness->businessRecord->nominatedBusinessId,
            $preferredBusiness->businessRecord->nominatedBusinessId,
            $preferredBusiness->businessRecord->nominatedBusinessId,
            $preferredBusiness->searchName,
            $preferredBusiness->searchLocation
        );
        $getResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $getResponse);
        if (count($sqlResult)){
            $row = (object)$sqlResult[0];
            $preferredBusiness->id = $row->id;
            // set the associated record ids for update if they exist at this point; otherwise, we will update the db with new data
            if (empty($preferredBusiness->businessRecord->id)){
                $preferredBusiness->businessRecord->id = $row->merchant_info_id;
            }
            if (empty($preferredBusiness->businessRecord->nominatedBusinessId)){
                $preferredBusiness->businessRecord->nominatedBusinessId = $row->nominated_business_id;
            }
        }
    }

    private static $INSERT_BUSINESS_SQL = "
        INSERT INTO nominated_business
        (
          city_grid_public_id,
          city_grid_profile_id,
          `name`,
          address,
          city,

          state,
          zip,
          neighborhood,
          lat,
          `long`,

          phone,
          website,
          description
        )
        SELECT
          ?,?,?,?,?,
          ?,?,?,?,?,
          ?,?,?
        FROM DUAL
        WHERE NOT EXISTS (
          SELECT 1
          FROM nominated_business
          WHERE city_grid_profile_id = ?
        )";

    private static $GET_NOMINATED_BUSINESS_ID_SQL = "
        SELECT id
        FROM nominated_business
        WHERE city_grid_profile_id = ?";

    private function checkOrAdd(NominatedMerchantRecord $businessRecord){
        $businessRecord->nominatedBusinessId = 0;
        $businessRecord->id = 0;
        if (strlen($businessRecord->cityGridProfileId) == 0){
            return; // we don't insert these at this time
        }
        $sqlString = self::$INSERT_BUSINESS_SQL;
        $sqlBinding = "ssssssssssssss"; // even though we know public id is a number, the entire table is strings
        $sqlParams = array(
            $businessRecord->cityGridPublicId,
            $businessRecord->cityGridProfileId,
            $businessRecord->name,
            $businessRecord->address,
            $businessRecord->city,
            $businessRecord->state,
            $businessRecord->postalCode,
            $businessRecord->neighborhood,
            $businessRecord->lat,
            $businessRecord->long,
            $businessRecord->phone,
            $businessRecord->website,
            $businessRecord->rawDescriptionText,
            $businessRecord->cityGridProfileId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $businessRecord->nominatedBusinessId = $insertResponse->insertedId;
        } else {
            $sqlString = self::$GET_NOMINATED_BUSINESS_ID_SQL;
            $sqlBinding = "s";
            $sqlParams = array($businessRecord->cityGridProfileId);
            $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
            if (count($sqlResult)){
                $businessRecord->nominatedBusinessId = $sqlResult[0]["id"];
            }
        }
        $this->getRegisteredMerchantId($businessRecord);
    }

    private static $GET_REGISTERED_MERCHANT_ID = "
        SELECT
            MerchantInfo_ID as id
        FROM merchant_info
        WHERE MerchantInfo_CityGridID = ?";

    private function getRegisteredMerchantId(NominatedMerchantRecord $businessRecord){
        $sqlString = self::$GET_REGISTERED_MERCHANT_ID;
        $sqlBinding = "s";
        $sqlParam = array( $businessRecord->cityGridProfileId);
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        if (count($sqlResult)){
            $businessRecord->id = $sqlResult[0]["id"];
        }
    }

    // ------------------
    // Delete
    // ------------------

    private static $DELETE_NONPROFIT_PREFERRED_BUSINESS = "
        UPDATE nonprofit_preferred_businesses
        SET status = 'DELETED'
        WHERE id = ?";

    public function deletePreferredBusiness(NonprofitPreferredBusiness $preferredBusiness){
        if (!empty($preferredBusiness->id)){
            $sqlString = self::$DELETE_NONPROFIT_PREFERRED_BUSINESS;
            $sqlBinding = "i";
            $sqlParam = array($preferredBusiness->id);
            $deleteResponse = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $deleteResponse);
            return $deleteResponse;
        }
        return false;
    }

    // -----------------
    // Add Admin Comment
    // -----------------

    private static $ADD_ADMIN_COMMENT = "
        UPDATE nonprofit_preferred_businesses
        SET admin_comment = ?
        WHERE id = ?";

    public function addAdminComment($id, $comment){
        $sqlString = self::$ADD_ADMIN_COMMENT;
        $sqlBinding = "si";
        $sqlParam = array ($comment, $id);
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $response);
        return $response;
    }
}
?>