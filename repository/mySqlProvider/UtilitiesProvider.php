<?php
class UtilitiesProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
	private static $SELECT_COLUMNS = "
	SELECT
		t.TableIndex_Name as name,
		t.TableIndex_Description as description,
		t.TableIndex_OrderID as orderId";

    private static $FROM_TABLE_CLAUSE = "
        FROM table_index as t
        WHERE 1 = 1";

    private static $FILTER_ON_NAME = "
        AND t.TableIndex_Name = ?";
		
    private static $FILTER_ON_DESCRIPTION = "
        AND t.TableIndex_Description = ?";

		
    public function getTables($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
        }
		if ($criteria->description){
            $filterString .= self::$FILTER_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
        }
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			$limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("TableProvider.getTables failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("TableProvider.getTables failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    protected function mapToRecord($dataRow) {
        return TableRecord::castAs($dataRow);
    }

	private static $DEFAULT_ORDER_BY = "
        ORDER BY t.TableIndex_OrderID ASC";
		
    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;

        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "description":
                    $orderBy = " ORDER BY t.TableIndex_Description ".$sortOrder;
                    break;
                case "name":
                    $orderBy = " ORDER BY t.TableIndex_Name ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "description":
                        $orderByStrings[] = "t.TableIndex_Description ".$sortOrder;
                        break;
                    case "efi":
                        $orderByStrings[] = "t.TableIndex_Name ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
	
	public function createSelectPulldown($collection,$fieldName="TableName",$defaultSelect="",$excludedList = array(),$includeBlankDefault = false, $classList=""){
		foreach ($collection as $Table=>$TableInfo){
			$TableList[$TableInfo->name]["displayName"]=$TableInfo->description;
			$TableList[$TableInfo->name]["value"]=$TableInfo->name;
		}
		if (trim($classList)){
			$classList = " class=\"".$classList."\"";
		}
		$result = "<select name=\"".$fieldName."\" id=\"".$fieldName."\"".$classList.">";
		if ($includeBlankDefault){
			$result .= "<option value=\"\">Choose Any Table</option>";
		}
		foreach ($TableList as $Table=>$TableInfo){
			if (!in_array($TableInfo["value"],$excludedList)){
				$result .= "<option value=\"".$TableInfo["value"]."\"";
				$result .= ($defaultSelect == $TableInfo["value"]) ? " selected" : "";
				$result .= ">".$TableInfo["displayName"]."</option>";
			}
		}
		$result .= "</select>";
		//echo $result;
		return $result;
	}
	
	public function getTableData($tableName){
		$SqlString = "SELECT * FROM ".$tableName;
		$Binding = "";
		$Params = array();
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $Binding, $Params, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("TableProvider.getTableData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
}
?>