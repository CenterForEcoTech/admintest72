<?php
class HRTrainingsProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
			ti.HRTrainingInfo_ID as id,
			ti.HRTrainingInfo_Name as name,
			ti.HRTrainingInfo_Code as code,
			ti.HRTrainingInfo_Description as description,
			ti.HRTrainingInfo_CategoryId as categoryId,
			ti.HRTrainingInfo_Categories as categories,
			ti.HRTrainingInfo_Purposes as purposes,
			ti.HRTrainingInfo_Events as events,
			ti.HRTrainingInfo_DeliveryMethod as deliveryMethod,
			ti.HRTrainingInfo_DeliveryDetails as deliveryDetails,
			ti.HRTrainingInfo_DisplayOrderID as displayOrderId,
			ti.HRTrainingInfo_IsCertification as isCertification,
			ti.HRTrainingInfo_IsAnnuallyRequired as isAnnuallyRequired,
			ti.HRTrainingInfo_PartOfCertificationID as partOfCertificationId,
			ti.HRTrainingInfo_Cost as cost,
			ti.HRTrainingInfo_CostType as costType,
			ti.HRTrainingInfo_ActiveStatus as activeStatus";

    private static $FROM_TABLE_CLAUSE = "
        FROM hr_training_info as ti
        WHERE 1 = 1 AND ti.HRTrainingInfo_Status IS NULL";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND ti.HRTrainingInfo_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID = "
        AND ti.HRTrainingInfo_ID = ?";

    private static $FILTER_ON_CODE = "
        AND ti.HRTrainingInfo_Code = ?";
		
    private static $FILTER_ON_NAME = "
        AND ti.HRTrainingInfo_Name = ?";

    private static $FILTER_ON_CATEGORIES = "
        AND ti.HRTrainingInfo_Categories IN (?)";

	private static $FILTER_ON_CATEGORYID = "
        AND ti.HRTrainingInfo_CategoryId = ?";

    private static $FILTER_ON_CERTIFICATION = "
        AND ti.HRTrainingInfo_IsCertification = 1";
    private static $FILTER_ON_ANNUALLYREQUIRED = "
        AND ti.HRTrainingInfo_IsAnnuallyRequired = 1";

		
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->code){
            $filterString .= self::$FILTER_ON_CODE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->code;
		}
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
		}
		if ($criteria->categories){
            $filterString .= self::$FILTER_ON_CATEGORIES;
            $filterBinding .= "s";
            $filterParams[] = $criteria->categories;
		}
		if ($criteria->categoryId){
            $filterString .= self::$FILTER_ON_CATEGORYID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->categoryId;
		}
		if ($criteria->isCertification){
            $filterString .= self::$FILTER_ON_CERTIFICATION;
		}
		if ($criteria->isAnnuallyRequired){
            $filterString .= self::$FILTER_ON_ANNUALLYREQUIRED;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("HRTrainingsProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HRTrainingsProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY ti.HRTrainingInfo_DisplayOrderID ASC, ti.HRTrainingInfo_Name ASC";

    private function getOrderByClause( $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY ti.HRTrainingInfo_ID ".$sortOrder;
                    break;
                case "code":
                    $orderBy = " ORDER BY ti.HRTrainingInfo_Code ".$sortOrder;
                    break;
				case "name":
					$orderBy = " ORDER BY ti.HRTrainingInfo_Name ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "ti.HRTrainingInfo_ID ".$sortOrder;
                        break;
                    case "code":
                        $orderByStrings[] = "ti.HRTrainingInfo_Code ".$sortOrder;
                        break;
					case "name":
						$orderByStrings[] = "ti.HRTrainingInfo_Name ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return HRTrainingInfoRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL = "
		INSERT INTO hr_training_info
		(
			HRTrainingInfo_Name,
			HRTrainingInfo_Code,
			HRTrainingInfo_Description,
			HRTrainingInfo_CategoryId,
			HRTrainingInfo_Categories,
			HRTrainingInfo_Purposes,

			HRTrainingInfo_Events,
			HRTrainingInfo_DeliveryMethod,
			HRTrainingInfo_DeliveryDetails,
			HRTrainingInfo_DisplayOrderID,
			HRTrainingInfo_IsCertification,
			
			HRTrainingInfo_IsAnnuallyRequired,
			HRTrainingInfo_PartOfCertificationID,
			HRTrainingInfo_Cost,
			HRTrainingInfo_CostType
		) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?
		)";

    public function add(HRTrainingInfoRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "sssisssssiiiids";
        $sqlParam = array(
			$record->name,
			$record->code,
			$record->description,
			$record->categoryId,
			$record->categories,
			$record->purposes,
			$record->events,
			$record->deliveryMethod,
			$record->deliveryDetails,
			$record->displayOrderId,
			$record->isCertification,
			$record->isAnnuallyRequired,
			$record->partOfCertificationId,
			$record->cost,
			$record->costType
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HRTrainingsProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL = "
        UPDATE hr_training_info
        SET
			HRTrainingInfo_Name = ?,
			HRTrainingInfo_Code = ?,
			HRTrainingInfo_Description = ?,
			HRTrainingInfo_CategoryId = ?,
			HRTrainingInfo_Categories = ?,
			HRTrainingInfo_Purposes = ?,
			HRTrainingInfo_Events = ?,
			HRTrainingInfo_DeliveryMethod = ?,
			HRTrainingInfo_DeliveryDetails = ?,
			
			HRTrainingInfo_IsCertification = ?,
			HRTrainingInfo_IsAnnuallyRequired = ?,
			HRTrainingInfo_PartOfCertificationID = ?,
			HRTrainingInfo_Cost = ?,
			HRTrainingInfo_CostType = ?,
			HRTrainingInfo_ActiveStatus = ?
		WHERE HRTrainingInfo_ID = ?";

    public function update(HRTrainingInfoRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "sssisssssiiidssi";
            $sqlParams = array(
				$record->name,
				$record->code,
				$record->description,
				$record->categoryId,
				$record->categories,
				$record->purposes,
				$record->events,
				$record->deliveryMethod,
				$record->deliveryDetails,
				$record->isCertification,
				$record->isAnnuallyRequired,
				$record->partOfCertificationId,
				$record->cost,
				$record->costType,
				$record->activeStatus,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HRTrainingsProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }
    private static $INSERT_COPYSQL = "
		INSERT INTO hr_training_info
		(
			HRTrainingInfo_Name,
			HRTrainingInfo_Code,
			HRTrainingInfo_Description,
			HRTrainingInfo_Categories,
			HRTrainingInfo_Purposes,

			HRTrainingInfo_Events,
			HRTrainingInfo_DeliveryMethod,
			HRTrainingInfo_DeliveryDetails,
			HRTrainingInfo_DisplayOrderID,
			HRTrainingInfo_IsCertification,
			
			HRTrainingInfo_IsAnnuallyRequired,
			HRTrainingInfo_PartOfCertificationID,
			HRTrainingInfo_Cost,
			HRTrainingInfo_CostType
		) ";

    private static $INSERT_COPYTRAININGMATRIX_SQL = "
		INSERT INTO hr_training_matrix
		(
			HRTrainingMatrix_TrainingID,
			HRTrainingMatrix_TrainingName,
			HRTrainingMatrix_TrainingDisplayOrderID,
			HRTrainingMatrix_EmployeeID,
			HRTrainingMatrix_EmployeeFullName,
			
			HRTrainingMatrix_Status,
			HRTrainingMatrix_AffectiveDate,
			HRTrainingMatrix_AddedByAdmin
		) ";
	private static $SELECT_COPYMATRIXSQL = "
		SELECT
			?,
			?,
			HRTrainingMatrix_TrainingDisplayOrderID as trainingDisplayOrderId,
			HRTrainingMatrix_EmployeeID as employeeId,
			HRTrainingMatrix_EmployeeFullName as employeeFullName,
			?,?,?
		FROM hr_training_matrix WHERE HRTrainingMatrix_TrainingID = ?";

			
    public function copy($newRecord,$adminUserId){
		//training info
		$insertFrom  = self::$SELECT_COLUMNS;
		$insertFrom  = str_replace("ti.HRTrainingInfo_ID as id,","",$insertFrom);
		$insertFrom  = str_replace("ti.HRTrainingInfo_Name as name","?",$insertFrom);
		$insertFrom .= " FROM hr_training_info as ti WHERE HRTrainingInfo_ID = ?";
        $sqlString = self::$INSERT_COPYSQL;
        $sqlBinding = "si";
        $sqlParam = array($newRecord->name,$newRecord->id);
		$sqlString .= $insertFrom;
		
		//matrix info
		$matrixSqlString = self::$INSERT_COPYTRAININGMATRIX_SQL;
		$matrixFrom = self::$SELECT_COPYMATRIXSQL;
		$matrixSqlString .= $matrixFrom;
		$affectiveDate = $newRecord->affectiveDate;
		$affectiveDate = date("Y-m-d", strtotime($affectiveDate));
		$status = $newRecord->status;
		
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
			//copy over any assigned staff to new training
			if (!$newRecord->doNotCopyStaff){
				$matrixBinding = "issssi";
				$matrixParam = array($record->id,$newRecord->name,$status,$affectiveDate,$adminUserId,$newRecord->id);
				$insertResponse = new ExecuteResponseRecord();
				MySqliHelper::execute($this->mysqli, $matrixSqlString, $matrixBinding, $matrixParam, $insertResponse);
			}
        } else {
            trigger_error("HRTrainingsProvider.copy failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

		
		
        return $record->id;
    }
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	hr_training_info
        SET		HRTrainingInfo_DisplayOrderID = ?
		WHERE	HRTrainingInfo_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HRTrainingsProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	private static $DELETE_TRAINING_SQL = "
        UPDATE	hr_training_info
        SET		
			HRTrainingInfo_Status = ?
		WHERE	HRTrainingInfo_ID = ?";

    public function delete($record,$adminId){
        $sqlString = self::$DELETE_TRAINING_SQL;
        $sqlBinding = "si";
		$status = "Deleted By ".$adminId." on ".date("Y-m-d H:i:s");
        $sqlParams = array($status,$record->id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HRTrainingsProvider.delete failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }	
	
	
	
	//Training Matrix
    private static $SELECT_TRAININGMATRIX_COLUMNS = "
        SELECT
			tm.HRTrainingMatrix_ID as id,
			tm.HRTrainingMatrix_TrainingID as trainingId,
			tm.HRTrainingMatrix_TrainingName as trainingName,
			tm.HRTrainingMatrix_TrainingDisplayOrderID as trainingDisplayOrderId,
			tm.HRTrainingMatrix_EmployeeID as employeeId,
			tm.HRTrainingMatrix_EmployeeFullName as employeeFullName,
			tms.HRTrainingMatrixStatus_Status as status,
			tms.HRTrainingMatrixStatus_AffectiveDate as affectiveDate,
			tms.HRTrainingMatrixStatus_TimeStamp as timeStamp,
			tms.HRTrainingMatrixStatus_AddedByAdmin as addedByAdmin";

    private static $FROM_TABLE_MATRIX_CLAUSE = "
		FROM hr_training_matrix as tm 
			LEFT JOIN hr_training_matrixstatus as tms ON tms.HRTrainingMatrixStatus_MatrixID=tm.HRTrainingMatrix_ID
		WHERE tms.HRTrainingMatrixStatus_TimeStamp = (
				SELECT MAX(b.HRTrainingMatrixStatus_TimeStamp)
				FROM hr_training_matrixstatus as b
				WHERE b.HRTrainingMatrixStatus_MatrixID=tm.HRTrainingMatrix_ID
		)";
    private static $COUNTFROM_TABLE_MATRIX_CLAUSE = "
		FROM hr_training_matrix as tm";

    private static $FILTER_ON_MATRIX_ID = "
        AND tm.HRTrainingMatrix_ID = ?";

    private static $FILTER_ON_MATRIX_EMPLOYEEID = "
        AND tm.HRTrainingMatrix_EmployeeID = ?";

    private static $FILTER_ON_MATRIX_TRAININGID = "
        AND tm.HRTrainingMatrix_TrainingID = ?";
		
	private static $FILTER_ON_MATRIX_NOTSTATUS = "
        AND tm.HRTrainingMatrix_Status != ?";	
		
	private static $FILTER_ON_MATRIX_STATUS = "
        AND tm.HRTrainingMatrix_Status = ?";	
		
    public function getTrainingMatrix($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
		$result->collection = array();

        $paginationSelectString = self::$SELECT_TRAININGMATRIX_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_MATRIX_CLAUSE;
        $fromCountTableClause = self::$COUNTFROM_TABLE_MATRIX_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY HRTrainingMatrix_TrainingDisplayOrderID";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterStringCount .= str_replace("AND","WHERE",self::$FILTER_ON_MATRIX_ID);
            $filterString .= self::$FILTER_ON_MATRIX_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->employeeId){
            $filterString .= self::$FILTER_ON_MATRIX_EMPLOYEEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->employeeId;
		}
		if ($criteria->trainingId){
            $filterString .= self::$FILTER_ON_MATRIX_TRAININGID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->trainingId;
		}
		if ($criteria->statusNot){
            $filterString .= self::$FILTER_ON_MATRIX_NOTSTATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->statusNot;
		}
		if ($criteria->status){
            $filterString .= self::$FILTER_ON_MATRIX_STATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->status;
		}
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromCountTableClause.$filterStringCount.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToMatrixRecord((object)$row);
                    }
                } else {
                    trigger_error("HRTrainingsProvider.getTrainingMatrix failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HRTrainingsProvider.getTrainingMatrix failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

	
    private static $SELECT_TRAININGMATRIXSTATUS_COLUMNS = "
        SELECT
			tms.HRTrainingMatrixStatus_ID as id,
			tms.HRTrainingMatrixStatus_MatrixID as matrixId,
			tms.HRTrainingMatrixStatus_Status as status,
			tms.HRTrainingMatrixStatus_AffectiveDate as affectiveDate,
			tms.HRTrainingMatrixStatus_TimeStamp as timeStamp,
			tms.HRTrainingMatrixStatus_AddedByAdmin as addedByAdmin";

    private static $FROM_TABLE_MATRIXSTATUS_CLAUSE = "
		FROM hr_training_matrixstatus as tms
		WHERE 1=1";
		
    private static $FILTER_ON_MATRIXSTATUS_ID = "
        AND tms.HRTrainingMatrixStatus_MatrixID = ?";

    public function getTrainingMatrixStatusHistory($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
		$result->collection = array();

        $paginationSelectString = self::$SELECT_TRAININGMATRIXSTATUS_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_MATRIXSTATUS_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY HRTrainingMatrixStatus_TimeStamp DESC";
        // apply search
        if ($criteria->matrixId && is_numeric($criteria->matrixId)){
            $filterString .= self::$FILTER_ON_MATRIXSTATUS_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->matrixId;
        }
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToMatrixRecord((object)$row);
                    }
                } else {
                    trigger_error("HRTrainingsProvider.getTrainingMatrixStatusHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HRTrainingsProvider.getTrainingMatrixStatusHistory failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
	
    
    protected function mapToMatrixRecord($dataRow) {
        return HRTrainingMatrixRecord::castAs($dataRow);
    }

	
	
    private static $INSERT_TRAININGMATRIX_SQL = "
		INSERT INTO hr_training_matrix
		(
			HRTrainingMatrix_TrainingID,
			HRTrainingMatrix_TrainingName,
			HRTrainingMatrix_TrainingDisplayOrderID,
			HRTrainingMatrix_EmployeeID,
			HRTrainingMatrix_EmployeeFullName,
			
			HRTrainingMatrix_Status,
			HRTrainingMatrix_AffectiveDate,
			HRTrainingMatrix_AddedByAdmin
		) VALUES (
			?,?,?,?,?,
			?,?,?
		) ON DUPLICATE KEY UPDATE 
			HRTrainingMatrix_AffectiveDate = VALUES (HRTrainingMatrix_AffectiveDate)
		";

    public function addTrainingMatrix($record, $adminUserId){
		//first get training information
		if (!$record->trainingName){
			$criteria->id=$record->trainingId;
			$trainingInfo = $this->get($criteria);
			$trainingInfoRecord = $trainingInfo->collection;
			$training = $trainingInfoRecord[0];
		}else{
			$training = new stdClass();
			$training->name = $record->trainingName;
			$training->displayOrderId = $record->trainingDisplayOrderId;
			$employeeInfo = explode("_",$record->employeeInfo);
			$record->employeeId = $employeeInfo[0];
			$record->employeeFullName = urldecode($employeeInfo[1]);
		}
		$record->status = ($record->status ? $record->status : "Assigned");
		$record->affectiveDate = ($record->affectiveDate ? MySQLDateUpdate($record->affectiveDate) : MySQLDateUpdate(date("m/d/Y")));
		
        $sqlString = self::$INSERT_TRAININGMATRIX_SQL;
        $sqlBinding = "isiissss";
        $sqlParam = array(
			$record->trainingId,
			$training->name,
			$training->displayOrderId,
			$record->employeeId,
			$record->employeeFullName,
			$record->status,
			$record->affectiveDate,
			$adminUserId
		);
		//echo $sqlString.print_r($sqlParam,true)."<br>";
		//$insertResponse .= " TrainingID=".$record->trainingId." ".$training->name." DisplayOrderID ".$training->displayOrderId." EmployeeID ".$record->employeeId. " ".$record->employeeFullName." by ".$adminUserId;
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
			 if (!$insertResponse->success){
				trigger_error("HRTrainingsProvider.addTrainingMatrix failed to insert: ".print_r($insertResponse,true), E_USER_ERROR);
			 }else{
				$insertResponse->record = $record;
			 }
        }

        return $insertResponse;
    }
	
	private static $REMOVE_TRAININGMATRIX_SQL = "
        UPDATE	hr_training_matrix
        SET		HRTrainingMatrix_Status = 'Removed'
		WHERE	HRTrainingMatrix_ID = ?";
		
	private static $REMOVE_TRAININGEXPIRATION_SQL = "
        UPDATE	hr_training_activity
        SET		HRTrainingActivity_ExpirationDate = '0000-00-00'
		WHERE	HRTrainingActivity_MatrixID = ?";

    public function removeTrainingMatrix($id,$adminId){
        $sqlString = self::$REMOVE_TRAININGMATRIX_SQL;
        $sqlBinding = "i";
        $sqlParams = array($id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		//if deleted then also set expiration date to blank
        $sqlString = self::$REMOVE_TRAININGEXPIRATION_SQL;
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HRTrainingsProvider.removeTrainingMatrix failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }

	private static $UPDATE_TRAININGMATRIX_SQL = "
        UPDATE	hr_training_matrix
        SET		
			HRTrainingMatrix_Status = ?,
			HRTrainingMatrix_AffectiveDate = ?,
			HRTrainingMatrix_AddedByAdmin = ?
		WHERE	HRTrainingMatrix_ID = ?";

    public function updateTrainingMatrix($record,$adminId){
        $sqlString = self::$UPDATE_TRAININGMATRIX_SQL;
        $sqlBinding = "sssi";
        $sqlParams = array($record->status,MySQLDateUpdate($record->affectiveDate),$adminId,$record->id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HRTrainingsProvider.updateTrainingMatrix failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
    private static $INSERT_TRAININGSTATUS_SQL = "
		INSERT INTO hr_training_status
		(
			HRTrainingStatus_Name,
			HRTrainingStatus_AddedByAdmin
		) VALUES (
			?,?
		)";

    public function addTrainingStatus($status, $adminUserId){
        $sqlString = self::$INSERT_TRAININGSTATUS_SQL;
        $sqlBinding = "ss";
        $sqlParam = array($status,$adminUserId);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if (!$insertResponse->success && $insertResponse->affectedRows){
            trigger_error("HRTrainingsProvider.addTrainingStatus failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
    private static $SELECT_TRAININGSTATUS_COLUMNS = "
        SELECT
			ts.HRTrainingStatus_ID as id,
			ts.HRTrainingStatus_Name as name,
			ts.HRTrainingStatus_AddedByAdmin as addedByAdmin,
			ts.HRTrainingStatus_TimeStamp as timeStamp";

    private static $FROM_TABLE_STATUS_CLAUSE = "
		FROM hr_training_status as ts
		WHERE 1=1";
		
    public function getTrainingStatusList(){
        $paginationSelectString = self::$SELECT_TRAININGSTATUS_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_STATUS_CLAUSE;

        $orderBy = " ORDER BY HRTrainingStatus_Name";
		$pageSqlString = $paginationSelectString.$fromTableClause.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("HRTrainingsProvider.getTrainingMatrixStatusHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	//Training Activity
    private static $SELECT_ACTIVITYCOLUMNS = "
        SELECT
			ta.HRTrainingActivity_ID as id,
			ta.HRTrainingActivity_MatrixID as matrixId,
			ta.HRTrainingActivity_EmployeeID as employeeId,
			ta.HRTrainingActivity_TrainingID as trainingId,
			ta.HRTrainingActivity_Location as location,
			ta.HRTrainingActivity_Cost as cost,
			ta.HRTrainingActivity_CompanyPaid as companyPaid,
			ta.HRTrainingActivity_EmployeePaid as employeePaid,
			ta.HRTrainingActivity_ExpirationDate as expirationDate,
			ta.HRTrainingActivity_HoursApplied as hoursApplied,
			ta.HRTrainingActivity_Notes as notes,
			ta.HRTrainingActivity_UpdatedByAdmin as updatedByAdmin";

    private static $FROM_ACTIVITYTABLE_CLAUSE = "
        FROM hr_training_activity as ta
        WHERE 1 = 1";

    private static $FILTER_ON_ACTIVITYID = "
        AND ta.HRTrainingActivity_ID = ?";

    private static $FILTER_ON_TRAININGID = "
        AND ta.HRTrainingActivity_TrainingID = ?";

    private static $FILTER_ON_EMPLOYEEID = "
        AND ta.HRTrainingActivity_EmployeeID = ?";
    private static $FILTER_ON_MATRIXID = "
        AND ta.HRTrainingActivity_MatrixID = ?";

    public function getActivity($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_ACTIVITYCOLUMNS;
        $fromTableClause = self::$FROM_ACTIVITYTABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ACTIVITYID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->trainingId){
            $filterString .= self::$FILTER_ON_TRAININGID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->trainingId;
		}
		if ($criteria->employeeId){
            $filterString .= self::$FILTER_ON_EMPLOYEEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->employeeId;
		}
		if ($criteria->matrixId){
            $filterString .= self::$FILTER_ON_MATRIXID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->matrixId;
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToActivityRecord((object)$row);
                    }
                } else {
                    trigger_error("HRTrainingsProvider.getActivity failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HRTrainingsProvider.getActivity failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    protected function mapToActivityRecord($dataRow) {
        return HRTrainingActivityRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_ACTIVITYSQL = "
		INSERT INTO hr_training_activity
		(
			HRTrainingActivity_MatrixID,
			HRTrainingActivity_EmployeeID,
			HRTrainingActivity_EmployeeName,
			HRTrainingActivity_TrainingID,
			HRTrainingActivity_TrainingName,
			
			HRTrainingActivity_Location,
			HRTrainingActivity_Cost,
			HRTrainingActivity_CompanyPaid,
			HRTrainingActivity_EmployeePaid,
			HRTrainingActivity_ExpirationDate,

			HRTrainingActivity_HoursApplied,
			HRTrainingActivity_Notes,
			HRTrainingActivity_UpdatedByAdmin
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?
		) ON DUPLICATE KEY UPDATE 
			HRTrainingActivity_EmployeeName = VALUES (HRTrainingActivity_EmployeeName),
			HRTrainingActivity_TrainingName = VALUES (HRTrainingActivity_TrainingName),
			HRTrainingActivity_ExpirationDate = VALUES (HRTrainingActivity_ExpirationDate)
		";

    public function addActivity(HRTrainingActivityRecord $record, $adminUserId){
        $sqlString = self::$INSERT_ACTIVITYSQL;
        $sqlBinding = "iisissdddsdss";
		$location = ($record->location ? $record->location : NULL);
		$cost = ($record->cost ? $record->cost : 0.00);
		$companyPaid = ($record->companyPaid ? $record->companyPaid : 0.00);
		$employeePaid = ($record->employeePaid ? $record->employeePaid : 0.00);
		$hoursApplied = ($record->hoursApplied ? $record->hoursApplied : 0.00);
		$notes = ($record->notes ? $record->notes : NULL);
        $sqlParam = array(
			$record->matrixId,
			$record->employeeId,
			urldecode($record->employeeName),
			$record->trainingId,
			$record->trainingName,
			$location,
			$cost,
			$companyPaid,
			$employeePaid,
			MySQLDateUpdate($record->expirationDate),
			$hoursApplied,
			$notes,
			$adminUserId
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HRTrainingsProvider.addActivity failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_ACTIVITYSQL = "
        UPDATE hr_training_activity
        SET
			HRTrainingActivity_MatrixID = ?,
			HRTrainingActivity_Cost = ?,
			HRTrainingActivity_CompanyPaid = ?,
			HRTrainingActivity_EmployeePaid = ?,
			HRTrainingActivity_Location= ?,
			HRTrainingActivity_ExpirationDate = ?,
			HRTrainingActivity_HoursApplied = ?,
			HRTrainingActivity_Notes = ?,
			HRTrainingActivity_UpdatedByAdmin = ?
		WHERE HRTrainingActivity_ID = ?";

    public function updateActivity(HRTrainingActivityRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_ACTIVITYSQL;
            $sqlBinding = "idddssdssi";
            $sqlParams = array(
				$record->matrixId,
				$record->cost,
				$record->companyPaid,
				$record->employeePaid,
				$record->location,
				MySQLDateUpdate($record->expirationDate),
				$record->hoursApplied,
				$record->notes,
				$adminUserId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HRTrainingsProvider.updateActivity failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }
	
	
	
	
    private static $SELECT_TRAININGMATRIXBYTYPE_COLUMNS = "
        SELECT
			tmbt.HRTrainingMatrixByType_ID as id,
			tmbt.HRTrainingMatrixByType_TrainingID as trainingId,
			tmbt.HRTrainingMatrixByType_Type as type,
			tmbt.HRTrainingMatrixByType_TypeID as typeId,
			tmbt.HRTrainingMatrixByType_Status as status";

    private static $FROM_TABLE_MATRIXBYTYPE_CLAUSE = "
		FROM hr_training_matrix_bytype as tmbt WHERE 1=1";
		
    private static $FILTER_ON_MATRIXBYTYPE_ID = "
        AND tmbt.HRTrainingMatrixByType_ID = ?";

    private static $FILTER_ON_MATRIXBYTYPE_TRAININGID = "
        AND tmbt.HRTrainingMatrixByType_TrainingID = ?";
		
    private static $FILTER_ON_MATRIXBYTYPE_TYPE = "
        AND tmbt.HRTrainingMatrixByType_Type = ?";
		
    private static $FILTER_ON_MATRIXBYTYPE_TYPEID = "
        AND tmbt.HRTrainingMatrixByType_TypeID = ?";
		
	private static $FILTER_ON_MATRIXBYTYPE_NOTSTATUS = "
        AND tmbt.HRTrainingMatrixByType_Status != ?";	
		
	private static $FILTER_ON_MATRIXBYTYPE_STATUS = "
        AND tmbt.HRTrainingMatrixByType_Status = ?";	
		
    public function getTrainingMatrixByType($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
		$result->collection = array();

        $paginationSelectString = self::$SELECT_TRAININGMATRIXBYTYPE_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_MATRIXBYTYPE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

        $orderBy = "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->trainingId){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_TRAININGID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->trainingId;
		}
		if ($criteria->typeId){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_TYPEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->typeId;
		}
		if ($criteria->type){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_TYPE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->type;
		}
		if ($criteria->statusNot){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_NOTSTATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->statusNot;
		}
		if ($criteria->status){
            $filterString .= self::$FILTER_ON_MATRIXBYTYPE_STATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->status;
		}
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HRTrainingsProvider.getTrainingMatrixByType failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HRTrainingsProvider.getTrainingMatrixByType failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
	
    private static $INSERT_MATRIXBYTYPESQL = "
		INSERT INTO hr_training_matrix_bytype
		(
			HRTrainingMatrixByType_TrainingID,
			HRTrainingMatrixByType_Type,
			HRTrainingMatrixByType_TypeID
		) VALUES (
			?,?,?
		) ON DUPLICATE KEY UPDATE 
			HRTrainingMatrixByType_Status = 'Active'
		";

    public function insertTrainingMatrixByType($record){
        $sqlString = self::$INSERT_MATRIXBYTYPESQL;
        $sqlBinding = "isi";
        $sqlParam = array(
			$record->trainingId,
			$record->type,
			$record->typeId
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HRTrainingsProvider.insertTrainingMatrixByType failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	
	
}