<?php
class MerchantMemberProvider extends MemberProvider{
    private static $GET_MEMBERID_FROM_PERMALINK = "
        SELECT Members_ID as id
        FROM members
        JOIN merchant_info ON MerchantInfo_MemberID = Members_ID
        WHERE MerchantInfo_ID = ?";

    public function getMemberIdFromPermalink(PermaLinkRecord $permaLink){
        $memberId = 0;
        $sqlString = self::$GET_MEMBERID_FROM_PERMALINK;
        $sqlBinding = "i";
        $sqlParam = array($permaLink->entityId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        if (count($result)){
            $memberId = $result[0]["id"];
        }
        return $memberId;
    }

    private static $GET_REFERRED_BY_TYPES = "
        SELECT
            (SELECT MembersType_Name
             FROM members_type
             WHERE MembersType_ID = Members_TypeId) as Type,
             count(*) as num
        FROM merchant_info
        JOIN members ON MerchantInfo_ReferredByMemberId = Members_ID
        GROUP BY Members_TypeId";

    public function getReferredByTypes(){
        $sqlString = self::$GET_REFERRED_BY_TYPES;
        $result = MySqliHelper::get_result($this->mysqli, $sqlString);
        return $result;
    }

    private static $GET_REFERRERS_SQL = "
        SELECT
            referrer.Members_Id as id,
            (SELECT MembersType_Name
             FROM members_type
             WHERE MembersType_ID = referrer.Members_TypeId) as type,
            CASE referrer.Members_TypeID
                WHEN 3 THEN
                    (SELECT OrganizationInfo_Name FROM organization_info WHERE OrganizationInfo_MemberID = referrer.Members_ID)
                WHEN 2 THEN
                    (SELECT MerchantInfo_Name FROM merchant_info WHERE MerchantInfo_MemberID = referrer.Members_ID)
                ELSE
			        (SELECT CONCAT('ct', referrer.Members_Id))
            END as referrer,
            MerchantInfo_Name as name,
            merchant.Members_Timestamp as timestamp
        FROM merchant_info
        JOIN members referrer ON MerchantInfo_ReferredByMemberId = referrer.Members_ID
        JOIN members merchant on MerchantInfo_MemberID = merchant.Members_ID
        WHERE MerchantInfo_MemberID > 0
        ORDER BY merchant.Members_Timestamp DESC";

    public function getReferrers(){
        $sqlString = self::$GET_REFERRERS_SQL;
        $result = MySqliHelper::get_result($this->mysqli, $sqlString);
        return $result;
    }

    private static $GET_REFERREDBYMEMBER_SQL = "
        SELECT
            MerchantInfo_ID as id,
			MerchantInfo_Name as name,
			MerchantInfo_Address as address,
			MerchantInfo_City as city,
			MerchantInfo_State as state,

			MerchantInfo_Zip as postalCode,
			MerchantInfo_Phone as phone,
			MerchantInfo_Website as website,
			MerchantInfo_Description as description,
			MerchantInfo_DescriptionShow as descriptionshow,
			MerchantInfo_DonationShow as donationshow,

			MerchantInfo_ContactFirstName as contactFirstName,
			MerchantInfo_ContactLastName as contactLastName,

			MerchantInfo_ContactTitle as contactTitle,
			MerchantInfo_ContactPhone as contactPhone,
			MerchantInfo_ContactEmail as contactEmail,
			MerchantInfo_Country as country,

			MerchantInfo_Lat as lat,
			MerchantInfo_Long as `long`,
			MerchantInfo_CityGridIDName as cityGridIdName,
			MerchantInfo_CityGridID as cityGridProfileId,
			MerchantInfo_Neighborhood as neighborhood,
			MerchantInfo_Keywords as keywords,

			MerchantInfo_EventBriteVenueID as eventBriteVenueId,
			(
                SELECT 1
                FROM merchant_preferred_orgs
                WHERE MerchantPreferredOrgs_MerchantID = MerchantInfo_ID LIMIT 1
			) as hasPreferredOrgs,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid
        FROM merchant_info
        WHERE MerchantInfo_ReferredByMemberId = ?";

    public function getReferredByMember($memberId){
        $sqlString = self::$GET_REFERREDBYMEMBER_SQL;
        $sqlBinding = "i";
        $sqlParam = array ($memberId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        $collection = array();
        foreach ($result as $dbrow){
            $row = (object)$dbrow;
            $record = new MerchantRecord();
            $contactRecord = new ContactRecord();
            $record->primaryContact = $contactRecord;
            $this->mapToMerchantRecord($row, $record, $contactRecord);
            $collection[] = $record;
        }
        return $collection;
    }

    private static $INSERT_MERCHANT_INFO_SQL = "
		INSERT INTO merchant_info (
			MerchantInfo_MemberID,
			MerchantInfo_Name,
			MerchantInfo_Address,
			MerchantInfo_City,
			MerchantInfo_State,
			MerchantInfo_Country,

			MerchantInfo_Zip,
			MerchantInfo_Phone,
			MerchantInfo_Website,
			MerchantInfo_Description,
			MerchantInfo_DescriptionShow,
			MerchantInfo_DonationShow,

			MerchantInfo_ContactFirstName,
			MerchantInfo_ContactLastName,

			MerchantInfo_ContactTitle,
			MerchantInfo_ContactPhone,
			MerchantInfo_ContactEmail,
			MerchantInfo_ContactEmailConfirm,
			MerchantInfo_Lat,
			MerchantInfo_Long,
			MerchantInfo_EventBriteVenueID,

			MerchantInfo_RegisteredReferralID,
			MerchantInfo_CurrentReferralID,
			MerchantInfo_ReferralCodeText,
			MerchantInfo_ReferredByMemberID,

			MerchantInfo_CityGridIDName,
			MerchantInfo_CityGridID,
			MerchantInfo_Neighborhood,
			MerchantInfo_Keywords,
			MerchantInfo_OfficialName
		) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,?,
			?,?,
			?,?,?,?,?,?,?,
			?,?,?,?,
			?,?,?,?,?
		)";

    public function addMerchant(MerchantRecord &$companyRecord, MerchantMemberRecord $memberRecord = null){
        $memberId = 0;
        if (isset($memberRecord) && $memberRecord->id > 0){
            $memberId = $memberRecord->id;
        }
        $sqlString = self::$INSERT_MERCHANT_INFO_SQL;
        $sqlBindings = "isssss";
        $sqlBindings .= "ssssss";
        $sqlBindings .= "ss";
        $sqlBindings .= "sssssss";
        $sqlBindings .= "iisi";
        $sqlBindings .= "sssss";
        $descriptionShow = ($companyRecord->isDescriptionPublic) ? "yes" : "";
        $sqlParams = array(
            $memberId,
            $companyRecord->name,
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,
            $companyRecord->country."",

            $companyRecord->postalCode,
            $companyRecord->phone,
            $companyRecord->website,
            $companyRecord->rawDescriptionText."",
            $descriptionShow."",
            $companyRecord->donationshow."",

            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,
            $companyRecord->lat."",
            $companyRecord->long."",
            $companyRecord->eventBriteVenueId."",

            $companyRecord->registeredreferralid."",
            $companyRecord->currentreferralid."",
            $companyRecord->referralCodeText,
            $companyRecord->referredByMemberId."",

            $companyRecord->cityGridIdName."",
            $companyRecord->cityGridProfileId + 0,
            $companyRecord->neighborhood."",
            $companyRecord->keywords."",
            $companyRecord->officialname.""
        );
        $sqlResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.addMerchant: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if ($sqlResponse->success){
            if (is_numeric($sqlResponse->insertedId) && $sqlResponse->insertedId > 0){
                $result = $sqlResponse->insertedId;
                $permalinkProvider = new PermaLinkProviderImpl($this->mysqli);
                $addPermalinkRequest = new AddPermaLinkRequest();
                $addPermalinkRequest->setEntityId($sqlResponse->insertedId);
                $addPermalinkRequest->setEntityName(PermaLinkRecord::$MERCHANT_ENTITY_NAME);
                $permalinkProvider->tryAddPermalink($addPermalinkRequest, 10);
                $companyRecord->id = $sqlResponse->insertedId;
            }
        }

        return $sqlResponse;
    }

    public static $UPDATE_MERCHANT_INFO_AS_MEMBER_SQL = "
		UPDATE merchant_info
			SET
			    MerchantInfo_MemberID = ?,
				MerchantInfo_Name = ?,
				MerchantInfo_Address = ?,
				MerchantInfo_City = ?,
				MerchantInfo_State = ?,
				MerchantInfo_Country = ?,

				MerchantInfo_Zip = ?,
				MerchantInfo_Phone = ?,
				MerchantInfo_Website = ?,
				MerchantInfo_Description = ?,
				MerchantInfo_DescriptionShow = ?,
				MerchantInfo_DonationShow = ?,

				MerchantInfo_ContactFirstName = ?,
				MerchantInfo_ContactLastName = ?,

				MerchantInfo_ContactTitle = ?,
				MerchantInfo_ContactPhone = ?,
				MerchantInfo_ContactEmail = ?,
				MerchantInfo_ContactEmailConfirm = ?,
				MerchantInfo_Lat = ?,
				MerchantInfo_Long = ?,
				MerchantInfo_EventBriteVenueID = ?,

				MerchantInfo_RegisteredReferralID = ?,
				MerchantInfo_CurrentReferralID = ?,
                MerchantInfo_ReferralCodeText = ?,
                MerchantInfo_ReferredByMemberID = ?,
                MerchantInfo_Neighborhood = ?,
                MerchantInfo_Keywords = ?
		WHERE MerchantInfo_ID = ?";
    /*				MerchantInfo_Country = ?,

                    MerchantInfo_CityGridIDName = ?,
                    MerchantInfo_CityGridID = ?
    */

    public function updateMerchantAsMember(MerchantRecord $companyRecord, MerchantMemberRecord $memberRecord = null){
        $sqlString = self::$UPDATE_MERCHANT_INFO_AS_MEMBER_SQL;
        $sqlBindings = "isssss";
        $sqlBindings .= "ssssss";
        $sqlBindings .= "ss";
        $sqlBindings .= "sssssss";
        $sqlBindings .= "ssssssi";
        $descriptionShow = ($companyRecord->isDescriptionPublic) ? "yes" : "";
        $sqlParams = array(
            $memberRecord->id,
            $companyRecord->name,
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,
            $companyRecord->country,

            $companyRecord->postalCode,
            $companyRecord->phone,
            $companyRecord->website."",
            $companyRecord->rawDescriptionText."",
            $descriptionShow."",
            $companyRecord->donationshow."",

            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,
            $companyRecord->lat,
            $companyRecord->long,
            $companyRecord->eventBriteVenueId."",

            $companyRecord->registeredreferralid."",
            $companyRecord->currentreferralid."",
            $companyRecord->referralCodeText,
            $companyRecord->referredByMemberId."",
            $companyRecord->neighborhood,
            $companyRecord->keywords,

            /*
            $companyRecord->country,

            $companyRecord->cityGridIdName,
            $companyRecord->cityGridProfileId,
            */
            $companyRecord->id
        );
        $sqlResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.updateMerchantAsMember: Failure updating merchant_info: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);

        return $sqlResponse->success;
    }

    public function addMember(MerchantMemberRecord $memberRecord, MerchantRecord $companyRecord){
        $addMemberResult = $this->addMemberBase($memberRecord);
        if (is_numeric($addMemberResult) && $addMemberResult > 0){
            $memberRecord->id = $addMemberResult;
            $merchantAlreadyExists = false;
            $merchantPermalink = $this->getRegisteredMerchantPermalink($companyRecord);
            if ($merchantPermalink){
                $merchantAlreadyExists = true;
                $companyRecord->id = $merchantPermalink->entityId;
            }
            if ($merchantAlreadyExists){
                $this->updateMerchantAsMember($companyRecord, $memberRecord);
            } else {
                $this->addMerchant($companyRecord, $memberRecord);
            }
        }
        return $memberRecord;
    }

    public static $UPDATE_MERCHANT_INFO_SQL = "
		UPDATE merchant_info
			SET
				MerchantInfo_Name = ?,
				MerchantInfo_Address = ?,
				MerchantInfo_City = ?,
				MerchantInfo_State = ?,

				MerchantInfo_Zip = ?,
				MerchantInfo_Phone = ?,
				MerchantInfo_Website = ?,
				MerchantInfo_Description = ?,
				MerchantInfo_DescriptionShow = ?,
				MerchantInfo_DonationShow = ?,

				MerchantInfo_ContactFirstName = ?,
				MerchantInfo_ContactLastName = ?,

				MerchantInfo_ContactTitle = ?,
				MerchantInfo_ContactPhone = ?,
				MerchantInfo_ContactEmail = ?,
				MerchantInfo_ContactEmailConfirm = ?,

				MerchantInfo_Lat = ?,
				MerchantInfo_Long = ?
		WHERE MerchantInfo_MemberID = ?";
    /*				MerchantInfo_Country = ?,

                    MerchantInfo_CityGridIDName = ?,
                    MerchantInfo_CityGridID = ?,
				MerchantInfo_Neighborhood = ?,
				MerchantInfo_Keywords = ?
    */

    public function updateMember(MerchantMemberRecord $memberRecord, MerchantRecord $companyRecord){
        $updateResult = $this->updateMemberBase($memberRecord);

        $sqlString = self::$UPDATE_MERCHANT_INFO_SQL;
        $sqlBindings = "ssss";
        $sqlBindings .= "ssssss";
        $sqlBindings .= "ss";
        $sqlBindings .= "ssss";
        $sqlBindings .= "ssi";
        $descriptionShow = ($companyRecord->isDescriptionPublic) ? "yes" : "";
        $sqlParams = array(
            $companyRecord->name,
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,

            $companyRecord->postalCode,
            $companyRecord->phone,
            $companyRecord->website,
            $companyRecord->rawDescriptionText,
            $descriptionShow,
            $companyRecord->donationshow,

            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,

            $companyRecord->lat,
            $companyRecord->long,

            /*
            $companyRecord->country,

            $companyRecord->cityGridIdName,
            $companyRecord->cityGridProfileId,
            */
            $memberRecord->id
        );
        $sqlResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.updateMember: Failure updating merchant_info: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);

        return $updateResult || $sqlResponse->success;
    }

    private static $GET_CUSTOM_EMAIL_TEXT_SQL = "
        SELECT MerchantInfo_CustomTransactionEmailText as customTransactionEmailText
        FROM merchant_info
        WHERE MerchantInfo_ID = ?";

    public function getCustomTransactionEmailText($merchantID){
        $text = null;
        $result = MySqliHelper::get_result($this->mysqli, self::$GET_CUSTOM_EMAIL_TEXT_SQL, "i", array($merchantID));
        if (count($result) == 1){
            $text = $result[0]["customTransactionEmailText"];
        }
        return $text;
    }

    private static $SET_CUSTOM_EMAIL_TEXT_SQL = "
        UPDATE merchant_info
        SET MerchantInfo_CustomTransactionEmailText = ?
        WHERE MerchantInfo_ID = ?";

    public function updateCustomTransactionEmailText($merchantID, $text){
        if (!isset($text) || strlen(trim($text)) == 0){
            $text = null;
        }
        $sqlString = self::$SET_CUSTOM_EMAIL_TEXT_SQL;
        $sqlBinding = "si";
        $sqlParam = array($text, $merchantID);
        $executeResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.updateCustomTransactionEmailText: SQL Error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParam);

        return $executeResponse;
    }

    private static $GET_PUBLIC_PROFILE_SQL = "
        SELECT
            MerchantInfo_MemberID as memberId,
            MerchantInfo_ID as id,
			MerchantInfo_Name as name,
			MerchantInfo_Address as address,
			MerchantInfo_City as city,
			MerchantInfo_State as state,

			MerchantInfo_Zip as postalCode,
			MerchantInfo_Phone as phone,
			MerchantInfo_Website as website,
			MerchantInfo_Description as description,
			MerchantInfo_DescriptionShow as descriptionshow,
			MerchantInfo_DonationShow as donationshow,

			MerchantInfo_ContactFirstName as contactFirstName,
			MerchantInfo_ContactLastName as contactLastName,

			MerchantInfo_ContactTitle as contactTitle,
			MerchantInfo_ContactPhone as contactPhone,
			MerchantInfo_ContactEmail as contactEmail,
			MerchantInfo_Country as country,
			MerchantInfo_RegisteredReferralID as registeredReferralId,
			MerchantInfo_ReferralCodeText as referralCodeText,
			MerchantInfo_CurrentReferralID as currentReferralId,
			MerchantInfo_ReferredByMemberID as referredByMemberId,

			MerchantInfo_Lat as lat,
			MerchantInfo_Long as `long`,
			MerchantInfo_CityGridIDName as cityGridIdName,
			MerchantInfo_CityGridID as cityGridProfileId,
			MerchantInfo_Neighborhood as neighborhood,
			MerchantInfo_Keywords as keywords,
			i.folder_url as logoPath,
			i.file_name as logoFile,
			MerchantInfo_CustomTransactionEmailText as customTransactionEmailText,

			MerchantInfo_EventBriteVenueID as eventBriteVenueId,
			(
                SELECT 1
                FROM merchant_preferred_orgs
                WHERE MerchantPreferredOrgs_MerchantID = MerchantInfo_ID LIMIT 1
			) as hasPreferredOrgs,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid

		FROM
		    merchant_info
        LEFT JOIN
            cms_image_library i ON MerchantInfo_ImageLibraryID = i.id AND i.status = 'ACTIVE'
		WHERE
		    MerchantInfo_ID = ?
		OR
		    MerchantInfo_MemberID = ?";

    /**
     * @param $merchantId
     * @param $memberID
     * @return MerchantRecord
     */
    public function getPublicProfile($merchantId, $memberID = -1){
        $record = new MerchantRecord();
        $contactRecord = new ContactRecord();
        $record->primaryContact = $contactRecord;
        $sqlString = self::$GET_PUBLIC_PROFILE_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($merchantId, $memberID);
        $sqlResponse = MySqliHelper::getResultWithLogging("MemberProvider.getPublicProfile error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($sqlResponse->resultArray)){
            $row = (object)$sqlResponse->resultArray[0];
            $this->mapToMerchantRecord($row, $record, $contactRecord);

            $availableDates = $this->getAvailableDates($merchantId);

            if (count($availableDates)){
                $default = $availableDates[0];
                $record->defaultStartTime = $default->startTime;
                $record->defaultEndTime = $default->endTime;
                $record->defaultPercentage = $default->percentage;

                foreach($availableDates as $availableDateRecord){
                    $record->addAvailableDate($availableDateRecord);
                }
            } else {
                $record->defaultStartTime = "10:00:00";
                $record->defaultEndTime = "22:00:00";
                $record->defaultPercentage = 20;
            }
        }
        return $record;
    }

    private function mapToMerchantRecord($row, MerchantRecord $record, ContactRecord $contactRecord){
        $record->memberId = $row->memberId ? $row->memberId : "";
        $record->id = $row->id;
        $record->pid = $row->pid;
        $record->name = $row->name;
        $record->address = $row->address;
        $record->city = $row->city;
        $record->state = $row->state;

        $record->postalCode = $row->postalCode;
        $record->phone = $row->phone;
        $record->setURL($row->website);
        $record->descriptionText = $row->description;
        $record->isDescriptionPublic = $row->descriptionshow == "yes"; // TODO: change this column to an enum in the db
        $record->donationshow = $row->donationshow;
        //$record->website = $row->website;
        $contactRecord->firstname = $row->contactFirstName;
        $contactRecord->lastname = $row->contactLastName;

        $contactRecord->title = $row->contactTitle;
        $contactRecord->setPhone($row->contactPhone);
        $contactRecord->email = $row->contactEmail;
        $record->country = $row->country;

        $record->lat = $row->lat;
        $record->long = $row->long;
        $record->cityGridIdName = $row->cityGridIdName;
        $record->cityGridProfileId = $row->cityGridProfileId;
        $record->neighborhood = $row->neighborhood;
        $record->keywords = $row->keywords;

        $record->eventBriteVenueId = $row->eventBriteVenueId;
        $record->hasPreferredOrgs = $row->hasPreferredOrgs;
        $record->registeredreferralid = $row->registeredReferralId;
        $record->currentreferralid = $row->currentReferralId;
        $record->referralCodeText = $row->referralCodeText;
        $record->referredByMemberId = $row->referredByMemberId;
        if ($row->logoPath){
            $record->logoPath = $row->logoPath;
        }
        if ($row->logoFile){
            $record->logoFile = $row->logoFile;
        }
        $record->customTransactionEmailText = $row->customTransactionEmailText;
    }

    private static $GET_PREFERRED_ORGS ="
        SELECT
            MerchantPreferredOrgs_ID as id,
            MerchantPreferredOrgs_MerchantID as merchantID,
            MerchantPreferredOrgs_OrgID as orgID,
            MerchantPreferredOrgs_SortOrderID as sortOrder,
            OrganizationInfo_Name as orgName,
            OrganizationInfo_EIN as orgEin,
            OrganizationInfo_Address as orgAddress,
            OrganizationInfo_City as orgCity,
            OrganizationInfo_State as orgState,
            OrganizationInfo_Zip as orgZip
        FROM merchant_preferred_orgs
        JOIN organization_info ON MerchantPreferredOrgs_OrgID = OrganizationInfo_ID
        WHERE MerchantPreferredOrgs_MerchantID = ?
        AND MerchantPreferredOrgs_Status = 'ACTIVE'
        ORDER BY OrganizationInfo_Name";

    public function getPreferredOrgs($merchantID){
        $sqlString = self::$GET_PREFERRED_ORGS;
        $sqlBindings = "i";
        $sqlParams = array($merchantID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $collection = array();
        foreach ($result as $row){
            $collection[] = MerchantPreferredOrg::map((object)$row);
        }
        return $collection;
    }

    private static $GET_DEFAULT_CHARITY = "
        SELECT
            0 as id,
            m.MerchantInfo_ID as merchantID,
            o.OrganizationInfo_ID as orgID,
            0 as sortOrder,
            o.OrganizationInfo_Name as orgName,
            o.OrganizationInfo_EIN as orgEin,
            o.OrganizationInfo_Address as orgAddress,
            o.OrganizationInfo_City as orgCity,
            o.OrganizationInfo_State as orgState,
            o.OrganizationInfo_Zip as orgZip
        FROM organization_info o
        JOIN merchant_info m ON o.OrganizationInfo_MemberID = m.MerchantInfo_ReferredByMemberId AND o.OrganizationInfo_MemberID > 0
        WHERE m.MerchantInfo_ID = ?";

    public function getDefaultCharity($merchantId){
        $sqlString = self::$GET_DEFAULT_CHARITY;
        $sqlBinding = "i";
        $sqlParams = array($merchantId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        $collection = array();
        foreach ($result as $row){
            $collection[] = MerchantPreferredOrg::map((object)$row);
        }
        return $collection;
    }

    private static $GET_CHARITY_MAX_SORT_ORDER = "
        SELECT MAX(MerchantPreferredOrgs_SortOrderID) as lastSortOrder
        FROM merchant_preferred_orgs
        WHERE MerchantPreferredOrgs_MerchantID = ?";

    // NOTICE: The following relies on the existence of a unique index on merchantID + orgID
    private static $INSERT_OR_UPDATE_CHARITY = "
        INSERT INTO merchant_preferred_orgs
        (
            MerchantPreferredOrgs_MerchantID,
            MerchantPreferredOrgs_OrgID,
            MerchantPreferredOrgs_SortOrderID
        ) VALUES (
            ?,?,?
        )
        ON DUPLICATE KEY UPDATE
          MerchantPreferredOrgs_SortOrderID = ?,
          MerchantPreferredOrgs_Status = 'ACTIVE',
          MerchantPreferredOrgs_UpdatedDate = CURRENT_TIMESTAMP()";

    public function addPreferredOrg(MerchantPreferredOrg $preferredOrg){
        $executeResponse = new ExecuteResponseRecord();
        if ($preferredOrg && $preferredOrg->isValid()){
            $sortOrder = 0;
            $lastSortOrderResult = MySqliHelper::get_result($this->mysqli, self::$GET_CHARITY_MAX_SORT_ORDER, "i", array($preferredOrg->merchantID));
            if (count($lastSortOrderResult)){
                $sortOrder = $lastSortOrderResult[0]["lastSortOrder"];
                if (is_numeric($sortOrder)){
                    $sortOrder++;
                } else {
                    $sortOrder = 0;
                }
            }
            $sqlString = self::$INSERT_OR_UPDATE_CHARITY;
            $sqlBindings = "iiii";
            $sqlParams = array(
                $preferredOrg->merchantID,
                $preferredOrg->orgID,
                $sortOrder,
                $sortOrder
            );
            $executeResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.addPreferredOrg: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
        }
        return $executeResponse;
    }

    private static $DELETE_PREFERRED_ORG = "
        UPDATE merchant_preferred_orgs
        SET MerchantPreferredOrgs_Status = 'DELETED'
        WHERE MerchantPreferredOrgs_MerchantID = ?
        AND MerchantPreferredOrgs_OrgID = ?";

    public function deletePreferredOrg(MerchantPreferredOrg $preferredOrg){
        $executeResponse = new ExecuteResponseRecord();
        if ($preferredOrg && $preferredOrg->isValid()){
            $sqlString = self::$DELETE_PREFERRED_ORG;
            $sqlBindings = "ii";
            $sqlParams = array(
                $preferredOrg->merchantID,
                $preferredOrg->orgID
            );
            $executeResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.deletePreferredOrg: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
        }
        return $executeResponse;
    }

    private static $GET_AVAILABILITY_DATES_SQL = "
        SELECT
          MerchantAvailability_ID as availabilityId,
          MerchantAvailabilityDates_ID as dateId,
          MerchantAvailabilityDates_Date as date,
          MerchantAvailability_StartTime as startTime,
          MerchantAvailability_EndTime as endTime,
          MerchantAvailability_Percentage as percentage,
          (
          	SELECT 1 FROM event_info e
          	WHERE e.Event_ProposedByMerchantID = ?
          	AND Event_Status = 'ACTIVE'
          	AND Event_Date = MerchantAvailabilityDates_Date
          	AND Event_StartTime = MerchantAvailability_StartTime
          	AND Event_EndTime = MerchantAvailability_EndTime
          	AND (
          		Event_EndTime > Event_StartTime AND Event_Date = Event_EndDate
          	OR
          		Event_StartTime > Event_EndTime AND DateDiff(Event_EndDate,Event_Date) = 1
          	)
          	LIMIT 1
          ) As hasMatch
        FROM
          merchant_availability ma
        JOIN
          merchant_availability_dates mad ON ma.MerchantAvailability_ID = mad.MerchantAvailabilityDates_MerchantAvailabilityID
        WHERE
          ma.MerchantAvailability_MerchantID = ?
        AND
          mad.MerchantAvailabilityDates_Status = 'ACTIVE'
        AND
          MerchantAvailabilityDates_Date > curdate()
        ORDER BY
          MerchantAvailabilityDates_Date ASC";

    private function getAvailableDates($merchantId){
        $collection = array();
        $sqlString = self::$GET_AVAILABILITY_DATES_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($merchantId, $merchantId);
        $availabilityResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);

        foreach($availabilityResult as $availableDataArray){
            $availableDataRow = (object)$availableDataArray;
            $availableDateRecord = new AvailableDateRecord(
                $availableDataRow->date,
                $availableDataRow->percentage,
                $availableDataRow->startTime,
                $availableDataRow->endTime,
                $availableDataRow->dateId,
                $availableDataRow->availabilityId,
                $availableDataRow->hasMatch);
            $collection[] = $availableDateRecord;
        }
        return $collection;
    }

    private static $GET_AVAILABLE_DATE_SQL = "
        SELECT MerchantAvailabilityDates_ID
        FROM merchant_availability_dates
        WHERE MerchantAvailabilityDates_MerchantID = ?
        AND MerchantAvailabilityDates_Date = CAST(? AS DATE)";

    private static $ADD_AVAILABLE_DATE_SQL = "
        INSERT INTO merchant_availability_dates
        (
          MerchantAvailabilityDates_MerchantAvailabilityID,
          MerchantAvailabilityDates_MerchantID,
          MerchantAvailabilityDates_Date
        )
        VALUES ( ?,?,CAST(? AS DATE) )";

    private static $UPDATE_AVAILABLE_DATE_SQL = "
        UPDATE merchant_availability_dates
        SET MerchantAvailabilityDates_MerchantAvailabilityID = ?,
            MerchantAvailabilityDates_Status = 'ACTIVE'
        WHERE MerchantAvailabilityDates_ID = ?";

    public function addAvailableDate($merchantId, AvailableDateRecord $availableDateRecord){
        $response = new stdClass();
        $availabilityID = $this->getAvailabilityId($merchantId, $availableDateRecord);
        if (is_numeric($availabilityID)){
            $sqlString = self::$GET_AVAILABLE_DATE_SQL;
            $sqlBindings = "is";
            $sqlParams = array($merchantId, $availableDateRecord->date);
            $existingDate = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
            if (count($existingDate)){
                $existingRecord = (object)$existingDate[0];
                $sqlString = self::$UPDATE_AVAILABLE_DATE_SQL;
                $sqlBindings = "ii";
                $sqlParams = array($availabilityID, $existingRecord->MerchantAvailabilityDates_ID);
                $sqlResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.addAvailableDate: SQL Error on update: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
                if ($sqlResponse->success){
                    $this->touchAvailability($availabilityID);
                }
                $availableDateRecord->availabilityId = $availabilityID;
                $availableDateRecord->id = $existingRecord->MerchantAvailabilityDates_ID;
                $response->record = $availableDateRecord;
                return $response;
            }
            // no existing record
            $sqlString = self::$ADD_AVAILABLE_DATE_SQL;
            $sqlBindings = "iis";
            $sqlParams = array($availabilityID, $merchantId, $availableDateRecord->date);
            $insertResponse = MySqliHelper::executeWithLogging("MerchantMemberProvider.addAvailableDate: SQL Error on insert: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
            if ($insertResponse->success){
                $this->touchAvailability($availabilityID);
                $availableDateRecord->availabilityId = $availabilityID;
                $availableDateRecord->id = $insertResponse->insertedId;
                $response->record = $availableDateRecord;
                return $response;
            }
            $response->message = "Failure adding availability date";
        } else {
            $response->message = "Failure creating availability";
        }
        return $response;
    }

    private static $TOUCH_AVAILABILITY_SQL = "
        UPDATE merchant_availability
        SET MerchantAvailability_UpdateDate = CURRENT_TIMESTAMP
        WHERE MerchantAvailability_ID = ?";

    private function touchAvailability($availabilityId){
        $sqlString = self::$TOUCH_AVAILABILITY_SQL;
        $sqlBindings = "i";
        $sqlParams = array($availabilityId);
        return MySqliHelper::executeWithLogging("MerchantMemberProvider.touchAvailability: SQL Error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
    }

    private static $GET_AVAILAILITY_SQL = "
        SELECT
          MerchantAvailability_ID,
          (MerchantAvailability_ID = ?) AS matches
        FROM
          merchant_availability
        WHERE
          MerchantAvailability_MerchantID = ?
        AND
          MerchantAvailability_StartTime = CAST(? AS TIME)
        AND
          MerchantAvailability_EndTime = CAST(? AS TIME)
        AND
          MerchantAvailability_Percentage = ?
        ORDER BY matches desc";

    private static $INSERT_AVAILABILITY_SQL = "
        INSERT INTO merchant_availability
        (
          MerchantAvailability_MerchantID,
          MerchantAvailability_StartTime,
          MerchantAvailability_EndTime,
          MerchantAvailability_Percentage
        )
        VALUES
        (
            ?,
            CAST(? AS TIME),
            CAST(? AS TIME),
            ?
        )";

    private function getAvailabilityId($merchantId, AvailableDateRecord $availableDateRecord){
        $sqlString = self::$GET_AVAILAILITY_SQL;
        $sqlBindings = "iissi";
        $sqlParams = array(
            $availableDateRecord->availabilityId,
            $merchantId,
            $availableDateRecord->startTime,
            $availableDateRecord->endTime,
            $availableDateRecord->percentage
        );
        $existingMatches = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($existingMatches)){
            $bestMatch = (object)$existingMatches[0];
            return $bestMatch->MerchantAvailability_ID;
        }
        // no match found, so add a new one
        $sqlString = self::$INSERT_AVAILABILITY_SQL;
        $sqlBindings = "issi";
        $sqlParams = array(
            $merchantId,
            $availableDateRecord->startTime,
            $availableDateRecord->endTime,
            $availableDateRecord->percentage
        );
        $responseObject = MySqliHelper::executeWithLogging("MerchantMemberProvider.getAvailabilityId: Failure inserting availability: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if ($responseObject->insertedId){
            return $responseObject->insertedId;
        }
        return null;
    }

    private static $DELETE_AVAILABLE_DATE_SQL = "
        UPDATE merchant_availability_dates
        SET MerchantAvailabilityDates_Status = 'DELETED'
        WHERE
          MerchantAvailabilityDates_MerchantID = ?
        AND
          MerchantAvailabilityDates_Date = CAST(? AS DATE)";

    public function deleteAvailableDate($merchantId, $date){
        $sqlString = self::$DELETE_AVAILABLE_DATE_SQL;
        $sqlBindings = "is";
        $sqlParams = array($merchantId, $date);
        $updateResult = MySqliHelper::executeWithLogging("MerchantMemberProvider.deleteAvailableDate: SQL Error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $updateResult;
    }

    private static $MATCH_ON_CITYGRID_SQL = "
        SELECT
            MerchantInfo_ID as id,
        	permalinks.pid
        FROM merchant_info
        JOIN permalinks ON permalinks.entity_name = 'merchant' AND permalinks.entity_id = MerchantInfo_ID
        WHERE MerchantInfo_CityGridID = ?
        LIMIT 1";

    private static $MATCH_ON_CITYGRID_BINDING = "i";

    private static $MATCH_ON_PID_SQL = "
        SELECT pid
        FROM permalinks
        WHERE entity_name = ?
        AND pid = ?";

    private static $MATCH_ON_PID_BINDING = "ss";

    private static $MATCH_ON_NAME_AND_LOCATION_SQL = "
        SELECT
            MerchantInfo_ID as id,
        	permalinks.pid
        FROM merchant_info
        JOIN permalinks ON permalinks.entity_name = 'merchant' AND permalinks.entity_id = MerchantInfo_ID
        WHERE ( MerchantInfo_Name = ? OR MerchantInfo_OfficialName = ? )
        AND MerchantInfo_City = ?
        AND ( MerchantInfo_State = ? OR MerchantInfo_State = ? )
        LIMIT 1";

    private static $MATCH_ON_NAME_AND_LOCATION_BINDING = "sssss";

    public function getRegisteredMerchantPermalink(MerchantRecord $merchantRecord){

        if ($merchantRecord->cityGridProfileId){
            $sqlString = self::$MATCH_ON_CITYGRID_SQL;
            $sqlBinding = self::$MATCH_ON_CITYGRID_BINDING;
            $sqlParams = array( $merchantRecord->cityGridProfileId);
        } else if ($merchantRecord->pid){
            $sqlString = self::$MATCH_ON_PID_SQL;
            $sqlBinding = self::$MATCH_ON_PID_BINDING;
            $sqlParams = array( PermaLinkRecord::$MERCHANT_ENTITY_NAME, $merchantRecord->pid);
        } else {
            $sqlString = self::$MATCH_ON_NAME_AND_LOCATION_SQL;
            $sqlBinding = self::$MATCH_ON_NAME_AND_LOCATION_BINDING;
            $sqlParams = array(
                $merchantRecord->name,
                $merchantRecord->name,
                $merchantRecord->city,
                BillComStateFix($merchantRecord->state),
                StateAbbrToFull($merchantRecord->state)
            );
        }
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        if (count($result)){
            $matchingMerchantPID = $result[0]["pid"];
            $permalinkProvider = new PermaLinkProviderImpl($this->mysqli);
            $permalinkRecord = $permalinkProvider->get($matchingMerchantPID);
            return $permalinkRecord;
        }
        return null;
    }

    /**
     * overrides static sql in parent class in order to chech match on email or phone
     * @var string
     */
    public static $FIND_EXISTING_MATCHES_SQL = "
        SELECT
          Members_ID,
          Members_Email,
          Members_Phone,
          (Members_Email = ?) as emailMatch,
          (Members_Phone = ?) as phoneMatch
        FROM members
        WHERE (
          '' != ?
          AND Members_Email = ?
          OR '' != ?
          AND Members_Phone = ?)
        AND Members_ID > 0";

    public function checkUserExists(MerchantMemberRecord $memberRecord){
        if (!strlen($memberRecord->email) > 3 && !strlen($memberRecord->phone) > 3){
            // at least one item must have a value; assume entire update is invalid
            return false;
        }
        $sqlString = self::$FIND_EXISTING_MATCHES_SQL;
        $sqlBindings = "ssssss";
        $sqlParams = array(
            $memberRecord->email,
            $memberRecord->phone,
            $memberRecord->email,
            $memberRecord->email,
            $memberRecord->phone,
            $memberRecord->phone,
        );
        $existingMismatch = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($existingMismatch)){
            if ($existingMismatch[0]['emailMatch']){
                return 'email';
            } else {
                return 'phone';
            }
        } else {
            return false;
        }
    }

    public function getMissingGeoCodes(){
        $sqlString = "SELECT * FROM merchant_info WHERE !MerchantInfo_Lat OR !MerchantInfo_Long";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString);
        return $result;
    }

    public function updateMissingGeoCodes($getLat,$getLong,$merchantId){
        $sqlString = "UPDATE merchant_info SET MerchantInfo_Lat=?, MerchantInfo_Long=? WHERE MerchantInfo_ID=?";
        $sqlBindings = "ssi";
        $sqlParams = array($getLat,$getLong,$merchantId);
        $updateResult = MySqliHelper::executeWithLogging("MerchantMemberProvider.updateMissingGeoCodes: SQL Error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $updateResult->success;
    }

    private static $PAGINATED_SELECT_COLUMNS = "
        SELECT
            m.MerchantInfo_MemberID as memberId,
            m.MerchantInfo_ID as id,
            m.MerchantInfo_Name as name,
            m.MerchantInfo_Address as address,
            m.MerchantInfo_City as city,
            m.MerchantInfo_State as state,
            m.MerchantInfo_Zip as postalCode,
            m.MerchantInfo_Phone as phone,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = m.MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            (
                SELECT count(*)
                FROM event_transaction
                WHERE EventTransaction_MerchantID = m.MerchantInfo_ID
                AND EventTransaction_Status NOT LIKE '%Deleted%'
            ) as numTransactions,
            cc.CodesCampaign_ID as billingCodeId,
            cc.CodesCampaign_Code as billingCode,
            cc.CodesCampaign_ServiceRateType as billingType,
            cc.CodesCampaign_ServiceRateAmount as billingRate,
            IF (cc.CodesCampaign_ValidUseDate = '0000-00-00', NULL, cc.CodesCampaign_ValidUseDate) as validFrom,
            IF (cc.CodesCampaign_ValidUntilDate = '0000-00-00', NULL, cc.CodesCampaign_ValidUntilDate) as validTo,
            i.folder_url as logoPath,
            i.file_name as logoFile";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM merchant_info m
        LEFT JOIN cms_image_library i ON m.MerchantInfo_ImageLibraryID = i.id AND i.status = 'ACTIVE'
        LEFT JOIN codes_campaign cc ON m.MerchantInfo_CurrentReferralID = cc.CodesCampaign_ID
        WHERE 1 = 1";

    private static $FILTER_ON_KEYWORD = "
        AND (
            m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_Phone LIKE ?
            OR m.MerchantInfo_Website LIKE ?
            OR m.MerchantInfo_ContactFirstName LIKE ?
            OR m.MerchantInfo_ContactLastName LIKE ?
            OR m.MerchantInfo_ContactEmail LIKE ?
            OR cc.CodesCampaign_Code LIKE ?
            OR cc.CodesCampaign_ServiceRateType LIKE ?
        )";

    private static $FILTER_ON_ID = "
        AND m.MerchantInfo_ID = ?";

    private static $FILTER_ON_MEMBER_ID = "
        AND m.MerchantInfo_MemberID = ?";

    private static $FILTER_ON_STATE = "
        AND m.MerchantInfo_State = ?";

    /**
     * @param MerchantProfilesCriteria $criteria
     * @return PagedResult of Venue Object
     */
    public function get(MerchantProfilesCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        if (is_numeric($criteria->memberId)){
            $filterString .= self::$FILTER_ON_MEMBER_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->memberId;
        }

        if ($criteria->state){
            $filterString .= self::$FILTER_ON_STATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->state;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $rowArray){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $row = (object)$rowArray;
                        $record = new MerchantRecord();
                        $contactRecord = new ContactRecord();
                        $record->primaryContact = $contactRecord;
                        $this->mapToMerchantRecord($row, $record, $contactRecord);
                        $record->memberId = $row->memberId;
                        $record->billingCodeId = $row->billingCodeId;
                        $record->billingCode = $row->billingCode;
                        $record->billingType = $row->billingType;
                        $record->billingRate = $row->billingRate;
                        $record->validFrom = $row->validFrom;
                        $record->validTo = $row->validTo;
                        $record->numTransactions = $row->numTransactions;
                        $result->collection[] = $record;
                    }
                } else {
                    trigger_error("EventRecordProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventRecordProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }

    private static $ORDER_BY_ID = " ORDER BY m.MerchantInfo_ID";

    private static $ORDER_BY_BUSINESS = " ORDER BY m.MerchantInfo_Name";

    private static $ORDER_BY_BILLING = " ORDER BY cc.CodesCampaign_Code";

    private static $ORDER_BY_LOGOPATH = " ORDER BY i.file_name";

    private static $ORDER_BY_MEMBER_ID = " ORDER BY m.MerchantInfo_MemberID";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$ORDER_BY_ID;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "idLink":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "businessDisplay":
                    $orderBy = self::$ORDER_BY_BUSINESS." ".$sortOrder;
                    break;
                case "billingCodeDisplay":
                    $orderBy = self::$ORDER_BY_BILLING." ".$sortOrder;
                    break;
                case "logoPath":
                    $orderBy = self::$ORDER_BY_LOGOPATH." ".$sortOrder;
                    break;
                case "memberId":
                    $orderBy = self::$ORDER_BY_MEMBER_ID." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }

    private static $UPDATE_LOGO_IMAGE_SQL = "
        UPDATE merchant_info
        SET MerchantInfo_ImageLibraryID = (
            SELECT id
            FROM cms_image_library
            WHERE id = ?
            AND status = 'ACTIVE'
        )
        WHERE MerchantInfo_ID = ?";

    public function setImageId($merchantId, $imageId = 0){
        $updateResponse = new ExecuteResponseRecord();
        $sqlString = self::$UPDATE_LOGO_IMAGE_SQL;
        $sqlBinding = 'si';
        $sqlParams = array($imageId, $merchantId);
        MySqliHelper::executeWithLogging("MerchantMemberProvider.setImageId: SQL Error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
        return $updateResponse;
    }

    public function changeBillingPlan($merchantId, $billingCode, $memberId, $adminId){
        $referralCode = ReferralCodeHelper::getReferralCodeByName($this->mysqli, $billingCode);
        if ($referralCode && $referralCode->id){
            if (!is_numeric($memberId)){
                $memberId = 0;
            }
            if (!is_numeric($adminId)){
                $adminId = 0;
            }
            return $this->setBillingCodeId($merchantId, $referralCode->id, $memberId, $adminId);
        }
        return new ExecuteResponseRecord();
    }

    private static $UPDATE_BILLING_CODE_ID = "
        UPDATE merchant_info
        SET MerchantInfo_CurrentReferralID = ?
        WHERE MerchantInfo_ID = ?";

    public function setBillingCodeId($merchantId, $billingCodeId = 0, $memberId = 0, $adminId = 0){
        if (empty($billingCodeId)){
            $billingCodeId = 0;
        }
        $updateResponse = new ExecuteResponseRecord();
        $sqlString = self::$UPDATE_BILLING_CODE_ID;
        $sqlBinding = "ii";
        $sqlParams = array($billingCodeId, $merchantId);
        MySqliHelper::executeWithLogging("MerchantMemberProvider.setBillingCodeId: SQL Error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
        if ($updateResponse->affectedRows){
            // at least one row affected
            $this->logBillingCodeChange($merchantId, $billingCodeId, $memberId, $adminId);
        }
        return $updateResponse;
    }

    private static $LOG_BILLING_CODE_CHANGE = "
        INSERT INTO merchant_billing_plan
        (
          merchant_info_id,
          billing_code_id,
          member_id,
          admin_user_id
        ) VALUES (
          ?,?,?,?
        )";

    private function logBillingCodeChange($merchantId, $billingCodeId = 0, $memberId = 0, $adminId = 0){
        $sqlString = self::$LOG_BILLING_CODE_CHANGE;
        $sqlBinding = "iiii";
        $sqlParams = array($merchantId, $billingCodeId, $memberId, $adminId);
        MySqliHelper::executeWithLogging("MerchantMemberProvider.logBillingCodeChange: SQL Error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}