<?php
class ProductProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
			p.GHSInventoryProducts_ID as id,
			p.GHSInventoryProducts_EFI as efi,
			p.GHSInventoryProducts_Description as description,
			p.GHSInventoryProducts_CSGCode as csgCode,
			p.GHSInventoryProducts_BulbStyle as bulbStyle,
			p.GHSInventoryProducts_BulbType as bulbType,
			p.GHSInventoryProducts_BulbClass as bulbClass,
			p.GHSInventoryProducts_UnitsPerPack as unitsPerPack,
			p.GHSInventoryProducts_UnitsPerCase as unitsPerCase,
			p.GHSInventoryProducts_MinimumQty as minimumQty,
			p.GHSInventoryProducts_OrderQty as orderQty,
			p.GHSInventoryProducts_MinimumDays as minimumDays,
			p.GHSInventoryProducts_CurrentDaysOnHandQty as currentDaysOnHandQty,
			p.GHSInventoryProducts_CurrentDaysOnHandDays as currentDaysOnHandDays,
			p.GHSInventoryProducts_Cost as cost,
			p.GHSInventoryProducts_Retail as retail,
			p.GHSInventoryProducts_ActiveID as activeId,
			p.GHSInventoryProducts_CanBeOrderedID as canBeOrderedId,
			p.GHSInventoryProducts_DisplayOrderID as displayOrderId,
			p.salesForceID as salesForceID";

    private static $FROM_TABLE_CLAUSE = "
        FROM ghs_inventory_products as p
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND p.GHSInventoryProducts_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ACTIVEID = "
        AND p.GHSInventoryProducts_ActiveID = 1";
		
    private static $FILTER_ON_ID = "
        AND p.GHSInventoryProducts_ID = ?";

    private static $FILTER_ON_EFI = "
        AND p.GHSInventoryProducts_EFI = ?";
		
    private static $FILTER_ON_DESCRIPTION = "
        AND p.GHSInventoryProducts_Description = ?";

    private static $FILTER_ON_CSGCODE = "
        AND p.GHSInventoryProducts_CSGCode = ?";

    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->efi){
            $filterString .= self::$FILTER_ON_EFI;
            $filterBinding .= "s";
            $filterParams[] = $criteria->efi;
        }
		if ($criteria->description){
            $filterString .= self::$FILTER_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
        }
		if ($criteria->csgCode){
            $filterString .= self::$FILTER_ON_CSGCODE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->csgcode;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("ProductProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("ProductProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY p.GHSInventoryProducts_DisplayOrderID ASC, p.GHSInventoryProducts_EFI ASC";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY p.GHSInventoryProducts_ID ".$sortOrder;
                    break;
                case "description":
                    $orderBy = " ORDER BY p.GHSInventoryProducts_Description ".$sortOrder;
                    break;
                case "efi":
                    $orderBy = " ORDER BY p.GHSInventoryProducts_EFI ".$sortOrder;
                    break;
                case "csgcode":
                    $orderBy = " ORDER BY p.GHSInventoryProducts_CSGCode ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "p.GHSInventoryProducts_ID ".$sortOrder;
                        break;
                    case "description":
                        $orderByStrings[] = "p.GHSInventoryProducts_Description ".$sortOrder;
                        break;
                    case "efi":
                        $orderByStrings[] = "p.GHSInventoryProducts_EFI ".$sortOrder;
                        break;
                    case "csgcode":
                        $orderByStrings[] = "p.GHSInventoryProducts_CSGCode ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return ProductRecord::castAs($dataRow);
    }
	
    // add
    private static $INSERT_SQL = "
		INSERT INTO ghs_inventory_products
		(
			GHSInventoryProducts_EFI,
			GHSInventoryProducts_Description,
			GHSInventoryProducts_CSGCode,
			GHSInventoryProducts_BulbStyle,			
			GHSInventoryProducts_BulbType,
			GHSInventoryProducts_BulbClass,
			
			GHSInventoryProducts_UnitsPerPack,
			GHSInventoryProducts_UnitsPerCase,
			GHSInventoryProducts_MinimumQty,
			GHSInventoryProducts_OrderQty,			
			GHSInventoryProducts_MinimumDays,
			GHSInventoryProducts_Cost,
			
			GHSInventoryProducts_Retail,
			GHSInventoryProducts_ActiveID,
			GHSInventoryProducts_CanBeOrderedID,
			GHSInventoryProducts_DisplayOrderID
		) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,?,
			?,?,?,?
		)";

    public function add(ProductRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "ssssssiiiiiddiii";
        $sqlParam = array(
		
            $record->efi,
            $record->description,
            $record->csgCode,
            $record->bulbStyle,
            $record->bulbType,
            $record->bulbClass,

            $record->unitsPerPack,
            $record->unitsPerCase,
            $record->minimumQty,
            $record->orderQty,
            $record->minimumDays,
            $record->cost,

            $record->retail,
            $record->activeId,
            $record->canBeOrderedId,
            $record->displayOrderId
			
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ProductProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

    // transfer

    private static $TRANSFER_SQL = "
		INSERT INTO ghs_inventory_products_movementhistory
		(
			GHSInventoryProductsMovementHistory_ProductsEFI,
			GHSInventoryProductsMovementHistory_Date,
			GHSInventoryProductsMovementHistory_Units,
			GHSInventoryProductsMovementHistory_MovementType,
			
			GHSInventoryProductsMovementHistory_FromWarehouseLocationsName,
			GHSInventoryProductsMovementHistory_ToWarehouseLocationsName,
			GHSInventoryProductsMovementHistory_PurchaseOrder,
			GHSInventoryProductsMovementHistory_CreatedBy
		
		) VALUES (
			?,?,?,?,
			?,?,?,?
		)";

    public function transferAction(ProductTransferRecord $record, $adminUserId){
        $sqlString = self::$TRANSFER_SQL;
        $sqlBinding = "ssisssss";
        $sqlParam = array(
			$record->efi,
            $record->transferDate,
            $record->units,
            $record->movementType,
            $record->warehouseFrom,
            $record->warehouseTo,
            $record->purchaseOrder,
            $adminUserId

        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ProductProvider.transfer failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	public function transfer($transferInfo, $adminUserId){
		//Currently used in cetadmin/inventory/_productTransfer.php
		
		$TransferDate = MySQLDateUpdate($transferInfo["TransferDate"]);
		$WarehouseFrom = $transferInfo["WarehouseFromLocation"];
		$WarehouseTo = $transferInfo["WarehouseToLocation"];
		$PurchaseOrder = $transferInfo["PurchaseOrder"];
		$MovementType = ($transferInfo["MovementType"]) ? $transferInfo["MovementType"] : "Transfer";
		$i = 0;
		foreach ($transferInfo as $key=>$val){
			if (strpos(" ".$key,"Total_") && $val > 0){
				$key = str_replace("Total_","",$key);
				$key = str_replace("_",".",$key);
				$record->efi = $key;
				$record->transferDate = $TransferDate;
				$record->units = $val;
				$record->movementType = $MovementType;
				$record->warehouseFrom = $WarehouseFrom;
				$record->warehouseTo = $WarehouseTo;
				$record->purchaseOrder = $PurchaseOrder;
				$results = $this->transferAction($record, $adminUserId);
				if ($results->success){
					$resultInfo[$i]["efi"] = $results->record->efi;
					$resultInfo[$i]["units"] = $results->record->units;
					$resultInfo[$i]["warehouseFrom"] = $results->record->warehouseFrom;
					$resultInfo[$i]["warehouseTo"] = $results->record->warehouseTo;
					$resultInfo[$i]["transferDate"] = MySQLDate($results->record->transferDate);
					$resultInfo[$i]["purchaseOrder"] = $results->record->purchaseOrder;
					$resultInfo[$i]["movementType"] = $results->record->movementType;
					$resultInfo[$i]["id"] = $results->insertedId;					
				}

				$i++;
			}
		}
		if ($i < 1){
			$resultInfo["NoProduct"] = true;
		}
		return $resultInfo;
	
	}
	public function transferItem($transferInfo, $adminUserId){
		//Currently used in cetadmin/inventory/_productTransfer.php
		
		$TransferDate = MySQLDateUpdate($transferInfo["TransferDate"]);
		$WarehouseFrom = $transferInfo["WarehouseFromLocation"];
		$WarehouseTo = $transferInfo["WarehouseToLocation"];
		$PurchaseOrder = $transferInfo["PurchaseOrder"];
		$MovementType = ($transferInfo["MovementType"]) ? $transferInfo["MovementType"] : "Transfer";
		$EFI = $transferInfo["EFI"];
		$Qty = $transferInfo["Qty"];
		$SiteID = $transferInfo["SiteID"];
		$i = 0;
		if (trim($EFI) != ""){
			$record->efi = $EFI;
			$record->transferDate = $TransferDate;
			$record->units = $Qty;
			$record->movementType = $MovementType;
			$record->warehouseFrom = $WarehouseFrom;
			$record->warehouseTo = $WarehouseTo;
			$record->purchaseOrder = $PurchaseOrder;
			$results = $this->transferAction($record, $adminUserId);
			if ($results->success){
				$resultInfo[$i]["efi"] = $results->record->efi;
				$resultInfo[$i]["units"] = $results->record->units;
				$resultInfo[$i]["warehouseFrom"] = $results->record->warehouseFrom;
				$resultInfo[$i]["warehouseTo"] = $results->record->warehouseTo;
				$resultInfo[$i]["transferDate"] = MySQLDate($results->record->transferDate);
				$resultInfo[$i]["purchaseOrder"] = $results->record->purchaseOrder;
				$resultInfo[$i]["movementType"] = $results->record->movementType;
				$resultInfo[$i]["siteId"] = $SiteID;
				$resultInfo[$i]["byAdmin"] = $adminUserId;
				$resultInfo[$i]["id"] = $results->insertedId;					
			}
			$i++;
		}else{
			$resultInfo["NoProduct"] = $transferInfo["EFI"];
		}
		return $resultInfo;
	
	}
	public function countadjust($transferInfo, $adminUserId){
		$i = 0;
		foreach($transferInfo as $record){

			//get efi description
			$sqlString = "SELECT GHSInventoryProducts_Description as description FROM ghs_inventory_products WHERE 1 = 1 AND GHSInventoryProducts_EFI = ?";
			$filterBinding = "s";
			$filterParams = array($record->efi);
			$queryResponse = new QueryResponseRecord();
			$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$description = $row["description"];
			}
			
			
			$results = $this->transferAction($record, $adminUserId);
			if ($results->success){
				$resultInfo[$i]["efi"] = $results->record->efi;
				$resultInfo[$i]["description"] = $description;
				$resultInfo[$i]["units"] = $results->record->units;
				$resultInfo[$i]["warehouseFrom"] = $results->record->warehouseFrom;
				$resultInfo[$i]["warehouseTo"] = $results->record->warehouseTo;
				$resultInfo[$i]["transferDate"] = MySQLDate($results->record->transferDate);
				$resultInfo[$i]["purchaseOrder"] = $results->record->purchaseOrder;
				$resultInfo[$i]["movementType"] = $results->record->movementType;
				$resultInfo[$i]["adjustcount"] = $record->adjustcount;
				$resultInfo[$i]["siteId"] = "";
				$resultInfo[$i]["byAdmin"] = $adminUserId;
				$resultInfo[$i]["id"] = $results->insertedId;					
			}
			$i++;
		}
		return $resultInfo;
	
	}
	public function transferMultiple($transferCollection, $adminUserId){
		$recordCollection = array();
		foreach ($transferCollection as $transferInfo){
			$TransferDate = MySQLDateUpdate($transferInfo["TransferDate"]);
			$WarehouseFrom = $transferInfo["WarehouseFromLocation"];
			$WarehouseTo = $transferInfo["WarehouseToLocation"];
			$PurchaseOrder = $transferInfo["PurchaseOrder"];
			$MovementType = ($transferInfo["MovementType"]) ? $transferInfo["MovementType"] : "Transfer";
			foreach ($transferInfo as $key=>$val){
				if (strpos(" ".$key,"Total_") && $val > 0){
					$record = new stdClass();
					$key = str_replace("Total_","",$key);
					$key = str_replace("_",".",$key);
					$record->efi = $key;
					$record->transferDate = $TransferDate;
					$record->units = $val;
					$record->movementType = $MovementType;
					$record->warehouseFrom = $WarehouseFrom;
					$record->warehouseTo = $WarehouseTo;
					$record->purchaseOrder = $PurchaseOrder;
					$recordCollection[] = $record;
				}
			}
		}
		$resultsCollection = $this->transferMultipleAction($recordCollection, $adminUserId);
		$i = 0;
		foreach ($resultsCollection as $results){
			if ($results->success){
				$resultInfo[$i]["efi"] = $results->record->efi;
				$resultInfo[$i]["units"] = $results->record->units;
				$resultInfo[$i]["warehouseFrom"] = $results->record->warehouseFrom;
				$resultInfo[$i]["warehouseTo"] = $results->record->warehouseTo;
				$resultInfo[$i]["transferDate"] = MySQLDate($results->record->transferDate);
				$resultInfo[$i]["purchaseOrder"] = $results->record->purchaseOrder;
				$resultInfo[$i]["movementType"] = $results->record->movementType;
				$resultInfo[$i]["id"] = ($results->insertedId+$i);					
				$i++;
			}
		}
		if ($i < 1){
			$resultInfo["NoProduct"] = true;
		}
		return $resultsCollection;
	}
	
    private static $TRANSFERMULTIPLE_SQL = "
		INSERT INTO ghs_inventory_products_movementhistory
		(
			GHSInventoryProductsMovementHistory_ProductsEFI,
			GHSInventoryProductsMovementHistory_Date,
			GHSInventoryProductsMovementHistory_Units,
			GHSInventoryProductsMovementHistory_MovementType,
			
			GHSInventoryProductsMovementHistory_FromWarehouseLocationsName,
			GHSInventoryProductsMovementHistory_ToWarehouseLocationsName,
			GHSInventoryProductsMovementHistory_PurchaseOrder,
			GHSInventoryProductsMovementHistory_CreatedBy
		
		) VALUES ";
		
	private static $TRANSFERMULTIPLE_VALUES ="	
		(
			?,?,?,?,
			?,?,?,?
		)";
    public function transferMultipleAction($recordCollection, $adminUserId){
        $sqlString = self::$TRANSFERMULTIPLE_SQL;
		$sqlParam = array();
		$multipleResponseCollection = array();
		$multipleResponseRecords = new stdClass();
		foreach ($recordCollection as $record){
			$sqlStringArray[] = self::$TRANSFERMULTIPLE_VALUES;
			$sqlBinding .= "ssisssss";
			array_push($sqlParam,
				$record->efi,
				$record->transferDate,
				$record->units,
				$record->movementType,
				$record->warehouseFrom,
				$record->warehouseTo,
				$record->purchaseOrder,
				$adminUserId
			);
			$multipleResponseCollection[] = $record;
		}
		$multipleResponseRecords->records = $multipleResponseCollection;
		
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString.implode(",",$sqlStringArray), $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
			$multipleResponseRecords->success = true;
			$multipleResponseRecords->affectedRows = $insertResponse->affectedRows;
			$multipleResponseRecords->insertedId = $insertResponse->insertedId;
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ProductProvider.transfer failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $multipleResponseRecords;
    }
	
	
    private static $UPDATE_SQL = "
        UPDATE ghs_inventory_products
        SET
			GHSInventoryProducts_EFI = ?,
			GHSInventoryProducts_Description = ?,
			GHSInventoryProducts_CSGCode = ?,
			GHSInventoryProducts_BulbStyle = ?,			
			GHSInventoryProducts_BulbType = ?,
			GHSInventoryProducts_BulbClass = ?,
			
			GHSInventoryProducts_UnitsPerPack = ?,
			GHSInventoryProducts_UnitsPerCase = ?,
			GHSInventoryProducts_MinimumQty = ?,
			GHSInventoryProducts_OrderQty = ?,
			GHSInventoryProducts_MinimumDays = ?,
			GHSInventoryProducts_Cost = ?,
			
			GHSInventoryProducts_Retail = ?,
			GHSInventoryProducts_ActiveID = ?,
			GHSInventoryProducts_CanBeOrderedID = ?,
			GHSInventoryProducts_DisplayOrderID = ?
		WHERE GHSInventoryProducts_ID = ?";

    public function update(ProductRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "ssssssiiiiiddiiii";
            $sqlParams = array(
				$record->efi,
				$record->description,
				$record->csgCode,
				$record->bulbStyle,
				$record->bulbType,
				$record->bulbClass,

				$record->unitsPerPack,
				$record->unitsPerCase,
				$record->minimumQty,
				$record->orderQty,
				$record->minimumDays,
				$record->cost,

				$record->retail,
				$record->activeId,
				$record->canBeOrderedId,
				$record->displayOrderId,
				
                $record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("ProductProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
		
        return $updateResponse;
    }

    private static $UPDATE_MINIMUM_SQL = "
        UPDATE	ghs_inventory_products
        SET		GHSInventoryProducts_MinimumQty = ?
		WHERE	GHSInventoryProducts_EFI = ? AND GHSInventoryProducts_DisplayOrderID > 0";

    public function updateMinimum($data){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_MINIMUM_SQL;
        $sqlBinding = "is";
        $sqlParams = array(
			$record->minimum,
			$record->efi
        );
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("ProductProvider.updateMinimum failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }
	
    private static $UPDATE_DAYSCOUNT_SQL = "
        UPDATE	ghs_inventory_products
        SET		GHSInventoryProducts_CurrentDaysOnHandQty = ?
		WHERE	GHSInventoryProducts_EFI = ? AND GHSInventoryProducts_DisplayOrderID > 0";

    public function updateDaysOnHand($data){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DAYSCOUNT_SQL;
        $sqlBinding = "is";
        $sqlParams = array(
			$record->qty,
			$record->efi
        );
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("ProductProvider.updateDaysOnHand failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }
	
	private static $INSERT_DAYSONHANDHISTORY_SQL = "
		INSERT INTO ghs_inventory_products_daysonhandhistory (
			GHSInventoryProductsDaysOnHandHistory_EFI, 
			GHSInventoryProductsDaysOnHandHistory_Qty, 
			GHSInventoryProductsDaysOnHandHistory_PerDays
		) VALUES (?,?,?)";

	public function insertDaysOnHandHistory($data){
		$sqlString = self::$INSERT_MINHISTORY_SQL;
		$sqlBinding = "sii";
		$sqlParam = array(
			$data->efi,
			$data->qty,
			$data->days
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ProductProvider.insertDaysOnHandHistory failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;

	}
	
	private static $INSERT_MINIMUMHISTORY_SQL ="
		INSERT INTO ghs_inventory_products_minimumhistory (
			GHSInventoryProductsMinimumHistory_EFI, 
			GHSInventoryProductsMinimumHistory_Qty, 
			GHSInventoryProductsMinimumHistory_PerDays
		) VALUES (?,?,?)";
		
	public function insertMinimumHistory($data){
		$sqlString = self::$INSERT_MINIMUMHISTORY_SQL;
		$sqlBinding = "sii";
		$sqlParam = array(
			$data->efi,
			$data->qty,
			$data->days
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ProductProvider.insertMinimumHistory failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;

	}

	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	ghs_inventory_products
        SET		GHSInventoryProducts_DisplayOrderID = ?
		WHERE	GHSInventoryProducts_EFI = ?";

    public function updateDisplayOrder($efi,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "is";
        $sqlParams = array($displayOrderId,$efi);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("ProductProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
    private static $SELECT_MOVEMENTHISTORY_COLUMNS = "
        SELECT
			mh.GHSInventoryProductsMovementHistory_ID as id,
			mh.GHSInventoryProductsMovementHistory_ProductsEFI as efi,
			mh.GHSInventoryProductsMovementHistory_Date as transferDate,
			mh.GHSInventoryProductsMovementHistory_Units as units,
			mh.GHSInventoryProductsMovementHistory_MovementType as movementType,
			mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName as warehouseFrom,
			mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName as warehouseTo,
			mh.GHSInventoryProductsMovementHistory_PurchaseOrder as purchaseOrder,
			mh.GHSInventoryProductsMovementHistory_CreatedBy as createdBy,
			mh.GHSInventoryProductsMovementHistory_CreatedOnTimeStamp as timeStamp";

    private static $FROM_MOVEMENTHISTORY_TABLE_CLAUSE = "
        FROM ghs_inventory_products_movementhistory as mh
        WHERE 1 = 1"; //also used in getProductMovmentLeakage

    private static $FILTER_MOVEMENTHISTORY_ON_WAREHOUSEFROM = "
        AND mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName = ?"; //also used in getProductMovmentLeakage
    private static $FILTER_MOVEMENTHISTORY_ON_NOTWAREHOUSEFROM = "
        AND mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName != ?"; //also used in getProductMovmentLeakage
		
    private static $FILTER_MOVEMENTHISTORY_ON_WAREHOUSETO = "
        AND mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName = ?";
	private static $FILTER_MOVEMENTHISTORY_ON_NOTWAREHOUSETO = "
        AND mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName != ?";
		
    private static $FILTER_MOVEMENTHISTORY_ON_WAREHOUSEANY = "
        AND (mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName = ? OR mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName = ?)";
		
    private static $FILTER_MOVEMENTHISTORY_NOTRETURNED = "
        AND (mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName != 'Returned For Credit' OR mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName != 'EFI Shipment Received' OR mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName != 'Defective' OR mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName != 'Discontinued')";
		
    private static $FILTER_MOVEMENTHISTORY_ON_TRANSFERDATE = "
        AND mh.GHSInventoryProductsMovementHistory_Date = ?";
		
    private static $FILTER_MOVEMENTHISTORY_ON_TRANSFERGREATERTHANDATE = "
        AND mh.GHSInventoryProductsMovementHistory_Date >= ?";
	
	private static $FILTER_MOVEMENTHISTORY_ON_TRANSFERUPTODATE = "
		AND mh.GHSInventoryProductsMovementHistory_Date <= ?";
		
    private static $FILTER_MOVEMENTHISTORY_ON_DATERANGE = "
        AND mh.GHSInventoryProductsMovementHistory_Date BETWEEN ? AND ?"; //also used in getProductMovmentLeakage

	private static $FILTER_MOVEMENTHISTORY_ON_MOVEMENTTYPE = "
        AND mh.GHSInventoryProductsMovementHistory_MovementType LIKE ?"; //also used in getProductMovmentLeakage
		
	private static $FILTER_MOVEMENTHISTORY_ON_EFI = "
        AND mh.GHSInventoryProductsMovementHistory_EFI = ?";

    public function getProductMovmentHistory($criteria){
		$result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_MOVEMENTHISTORY_COLUMNS;
        $fromTableClause = self::$FROM_MOVEMENTHISTORY_TABLE_CLAUSE;
        $filterString = ""; // get all records default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getMovementHistoryOrderByClause($criteria);
        //$limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : ""; //No Limit
        // apply search
		if ($criteria->warehouseFrom){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_WAREHOUSEFROM;
            $filterBinding .= "s";
            $filterParams[] = $criteria->warehouseFrom;
        }
		if ($criteria->notFrom){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_NOTWAREHOUSEFROM;
            $filterBinding .= "s";
            $filterParams[] = $criteria->notFrom;
        }
		if ($criteria->warehouseTo){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_WAREHOUSETO;
            $filterBinding .= "s";
            $filterParams[] = $criteria->warehouseTo;
        }
		if ($criteria->notTo){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_NOTWAREHOUSETO;
            $filterBinding .= "s";
            $filterParams[] = $criteria->notTo;
        }
		if ($criteria->warehouseAny){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_WAREHOUSEANY;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->warehouseAny;
            $filterParams[] = $criteria->warehouseAny;
        }
		if ($criteria->transferDate){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_TRANSFERDATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->transferDate;
        }
		if ($criteria->transferDateGreaterThan){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_TRANSFERGREATERTHANDATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->transferDateGreaterThan;
        }
		if ($criteria->transferDateUpTo){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_TRANSFERUPTODATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->transferDateUpTo;
        }
		if ($criteria->endDate){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_DATERANGE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
        }

		if ($criteria->movementType){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_MOVEMENTTYPE;
            $filterBinding .= "s";
            $filterParams[] = "%{$criteria->movementType}%";
        }
		if ($criteria->efi){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_EFI;
            $filterBinding .= "s";
            $filterParams[] = $criteria->efi;
        }
		if ($criteria->notReturned){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_NOTRETURNED;
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
				//print_pre($criteria);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("ProductProvider.getMovementHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("ProductProvider.getMovementHistory failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;

    }	
	
    private static $DEFAULT_PRODUCTMOVEMENT_ORDER_BY = "
        ORDER BY mh.GHSInventoryProductsMovementHistory_Date DESC, mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName";
		
    private function getMovementHistoryOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_PRODUCTMOVEMENT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "movementType":
                    $orderBy = " ORDER BY mh.GHSInventoryProductsMovementHistory_MovementType ".$sortOrder;
                    break;
                case "transferDate":
                    $orderBy = " ORDER BY mh.GHSInventoryProductsMovementHistory_Date ".$sortOrder;
                    break;
                case "warehouseFrom":
                    $orderBy = " ORDER BY mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName ".$sortOrder;
                    break;
                case "warehouseTo":
                    $orderBy = " ORDER BY mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "movementType":
                        $orderByStrings[] = "mh.GHSInventoryProductsMovementHistory_MovementType ".$sortOrder;
                        break;
                    case "transferDate":
                        $orderByStrings[] = "mh.GHSInventoryProductsMovementHistory_Date ".$sortOrder;
                        break;
                    case "warehouseFrom":
                        $orderByStrings[] = "mh.GHSInventoryProductsMovementHistory_FromWarehouseLocationsName ".$sortOrder;
                        break;
                    case "warehouseTo":
                        $orderByStrings[] = "mh.GHSInventoryProductsMovementHistory_ToWarehouseLocationsName ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
	
    private static $SELECT_MOVEMENTLEAKAGE_COLUMNS = "
		SELECT 
			GHSInventoryProductsMovementHistory_ProductsEFI as efi, 
			SUM(GHSInventoryProductsMovementHistory_Units) as units";
			
		
	private static $GROUP_BY_MOVEMENTLEAKAGE = "
		GROUP BY GHSInventoryProductsMovementHistory_ProductsEFI
	";
			
    public function getProductMovmentLeakage($criteria){
		$result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_MOVEMENTLEAKAGE_COLUMNS;
        $fromTableClause = self::$FROM_MOVEMENTHISTORY_TABLE_CLAUSE;
        $filterString = ""; // get all records default
        $filterBinding = "";
        $filterParams = array();

		$groupBy = self::$GROUP_BY_MOVEMENTLEAKAGE;
        $orderBy = $this->getMovementHistoryOrderByClause($criteria);
        //$limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : ""; //No Limit
        // apply search
		if ($criteria->warehouseFrom){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_WAREHOUSEFROM;
            $filterBinding .= "s";
            $filterParams[] = $criteria->warehouseFrom;
        }
		if ($criteria->endDate){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_DATERANGE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
        }

		if ($criteria->movementType){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_MOVEMENTTYPE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->movementType;
        }
		if ($criteria->efi){
            $filterString .= self::$FILTER_MOVEMENTHISTORY_ON_EFI;
            $filterBinding .= "s";
            $filterParams[] = $criteria->efi;
        }
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$groupBy.$orderBy;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("ProductProvider.getMovementLeakge failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("ProductProvider.getMovementLeakage failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;

    }	
	
	public function createSelectPulldown($collection,$fieldName="ProductEFI",$defaultSelect="",$excludedList = array()){
		foreach ($collection as $Product=>$ProductInfo){
			$displayName = $ProductInfo->efi."&nbsp;&nbsp;&nbsp;".$ProductInfo->description;
			$ProductList[$ProductInfo->efi]["displayName"]=$displayName;
			$ProductList[$ProductInfo->efi]["value"]=$ProductInfo->efi;
		}
		
		$result = "<select name=\"".$fieldName."\" id=\"".$fieldName."\"><option value=''></option>";
		foreach ($ProductList as $Product=>$ProductInfo){
			if (!in_array($ProductInfo["value"],$excludedList)){
				$result .= "<option value=\"".$ProductInfo["value"]."\"";
				$result .= ($defaultSelect == $ProductInfo["value"]) ? " selected" : "";
				$result .= ">".$ProductInfo["displayName"]."</option>";
			}
		}
		$result .= "</select>";
		//echo $result;
		return $result;
	}
	
	public function getMinimumHistory($efi){
		$SqlString = "SELECT * FROM ghs_inventory_products_minimumhistory WHERE GHSInventoryProductsMinimumHistory_EFI = ?";
		$Binding = "s";
		$Params = array($efi);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $Binding, $Params, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ProductProvider.getMinimumHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;

	}
	
	
	private static $INSERT_MOVEMENTRECORD_SQL = "
        INSERT INTO ghs_inventory_products_movementhistory_record
        (
            GHSInventoryProductsMovementHistoryRecord_Text,
            GHSInventoryProductsMovementHistoryRecord_To,
			GHSInventoryProductsMovementHistoryRecord_Headers,
            GHSInventoryProductsMovementHistoryRecord_From,
            GHSInventoryProductsMovementHistoryRecord_FromAuth
        ) VALUES (
            ?,?,?,?,?
        )";
	public function movementRecordEmail($body,$to,$headers,$from,$fromAuth){
        $sqlString = self::$INSERT_MOVEMENTRECORD_SQL;
        $sqlBindings = "sssss";
        $sqlParams = array($body,$to,print_r($headers,true),$from,$fromAuth);
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse->insertedId;
	}
	
	private static $UPDATE_MOVEMENTRECORDMAIL_SQL = "
        UPDATE	ghs_inventory_products_movementhistory_record
        SET		GHSInventoryProductsMovementHistoryRecord_MailStatus = ?
		WHERE	GHSInventoryProductsMovementHistoryRecord_ID = ?";

    public function movementRecordEmailUpdate($id,$mail){
        $sqlString = self::$UPDATE_MOVEMENTRECORDMAIL_SQL;
        $sqlBinding = "si";
		$mailMessage = print_r($mail,true);
        $sqlParams = array($mailMessage,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("ProductProvider.movementRecordEmailUpdate failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }

	private static $UPDATE_MOVEMENTRECORD_SQL = "
        UPDATE	ghs_inventory_products_movementhistory_record
        SET		GHSInventoryProductsMovementHistoryRecord_AcknowledgedOnDate = ?,
        		GHSInventoryProductsMovementHistoryRecord_AcknowledgedByIP = ?,
        		GHSInventoryProductsMovementHistoryRecord_DiscrepancyNote = ?
		WHERE	GHSInventoryProductsMovementHistoryRecord_ID = ?";

    public function movementRecordReceived($criteria){
        $sqlString = self::$UPDATE_MOVEMENTRECORD_SQL;
        $sqlBinding = "sssi";
		$todaysDate = date("Y-m-d");
		$discrepancyNote = ($criteria->discrepancy ? $criteria->discrepancy : NULL);
        $sqlParams = array($todaysDate,$criteria->ipAddress,$discrepancyNote,$criteria->token);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("ProductProvider.movementRecordUpdate failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }
	
	public function getOneMovementRecordEmail($id){
		$SqlString = "SELECT * FROM ghs_inventory_products_movementhistory_record WHERE GHSInventoryProductsMovementHistoryRecord_ID = ?";
		$Binding = "i";
		$Params = array($id);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $Binding, $Params, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result = $row;
			}
		} else {
			trigger_error("ProductProvider.getMovementRecordEmail failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;

	}
	private static $SELECT_UNACKNOWLEDGEDMOVEMENT_SQL = "
		SELECT 
			*,  DATEDIFF(CURDATE(),`GHSInventoryProductsMovementHistoryRecord_SentOnDate`) as Days 
		FROM 
			ghs_inventory_products_movementhistory_record
		";
			
	private static $WHERE_UNACKNOWLEDGEDMOVEMENT = "
		WHERE 
			GHSInventoryProductsMovementHistoryRecord_AcknowledgedOnDate = '0000-00-00'
		";

	private static $WHERE_UNACKNOWLEDGEDMOVEMENT_DEFAULT="
		AND	DATEDIFF( CURDATE(),`GHSInventoryProductsMovementHistoryRecord_SentOnDate`) >= 7";
	
	private static $ORDERBY_UNACKNOWLEDGEDMOVEMENT = "
		ORDER BY Days DESC";	
	
	public function getUnAcknowledgedMovementRecordEmail($criteria){
		$SqlString = self::$SELECT_UNACKNOWLEDGEDMOVEMENT_SQL;
		$SqlWhere = self::$WHERE_UNACKNOWLEDGEDMOVEMENT;
		$OrderBy = self::$ORDERBY_UNACKNOWLEDGEDMOVEMENT;
		
		if ($criteria->warehouseName){
			$SqlWhere .=" AND GHSInventoryProductsMovementHistoryRecord_Text LIKE '".$criteria->warehouseName.":%'";
		}
		if ($criteria->didNotSend){
			$SqlWhere .=" AND GHSInventoryProductsMovementHistoryRecord_MailStatus !='1'";
		}
		if ($SqlWhere == self::$WHERE_UNACKNOWLEDGEDMOVEMENT){
			$SqlWhere .= self::$WHERE_UNACKNOWLEDGEDMOVEMENT_DEFAULT;
		}
		$SqlString .= $SqlWhere.$OrderBy;
		$Binding = "";
		$Params = array();
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $Binding, $Params, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result[] = $row;
			}
		} else {
			trigger_error("ProductProvider.getUnAcknowledgedMovementRecordEmail failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;

	}
	
	
	
	
	
	
	
	
	
	
	
	
    private static $CLONE_CAMPAIGN_SQL = "
        INSERT INTO tag_list
        (
            TagList_Name,
            TagList_Description,
            TagList_CampaignHeading,
            TagList_CampaignSubHeading,
            TagList_CampaignDescription,
            TagList_CampaignDescriptionMarkdown,
            TagList_CampaignDescriptionHtml,
            TagList_URL,
            TagList_EventURLOverride,
            TagList_AddWrap,
            TagList_MapZoomLevel,
            TagList_MapCenterLat,
            TagList_MapCenterLong,
            TagList_PosterImageUrl,
            TagList_PosterUrl,
			TagList_ShowMap,
			TagList_ShowBizList,
			TagList_NotShowingBizListBlurb,
            TagList_CreatedByAdminID
        )
        SELECT
            CONCAT('Copy of ', TagList_Name, ' - ', CURRENT_TIMESTAMP),
            TagList_Description,
            TagList_CampaignHeading,
            TagList_CampaignSubHeading,
            TagList_CampaignDescription,
            TagList_CampaignDescriptionMarkdown,
            TagList_CampaignDescriptionHtml,
            TagList_URL,
            TagList_EventURLOverride,
            TagList_AddWrap,
            TagList_MapZoomLevel,
            TagList_MapCenterLat,
            TagList_MapCenterLong,
            TagList_PosterImageUrl,
            TagLIst_PosterUrl,
			TagList_ShowMap,
			TagList_ShowBizList,
			TagList_NotShowingBizListBlurb,
            ?
        FROM tag_list
        WHERE TagList_ID = ?";

    public function cloneProduct(ProductCloneCriteria $criteria){
        $sqlString = self::$CLONE_CAMPAIGN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            getAdminId(),
            $criteria->record->id
        );
        $executeResponse = MySqliHelper::executeWithLogging("ProductProvider.cloneCampaign error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);

        if ($executeResponse->success && $executeResponse->insertedId){
            $newRecord = new ProductRecord();
            $newRecord->id = $executeResponse->insertedId;
            if ($criteria->cloneReferences){
                $this->cloneReferences($criteria, $newRecord);
            }
            if ($criteria->cloneSocialMessages){
                $this->cloneSocialMessages($criteria, $newRecord);
            }
            $executeResponse->record = $newRecord;
        }
        return $executeResponse;
    }

    private static $CLONE_CAMPAIGN_REFERENCES_SQL = "
        INSERT INTO tag_references
        (
            TagReference_TagListID,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Status,
            TagReference_Page
        )
        SELECT
            ?,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Status,
            TagReference_Page
        FROM tag_references
        WHERE TagReference_TagListID = ?
        AND TagReference_Status = 'ACTIVE'";

    private function cloneReferences(ProductCloneCriteria $criteria, ProductRecord $newRecord){
        $sqlString = self::$CLONE_CAMPAIGN_REFERENCES_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $newRecord->id,
            $criteria->record->id
        );
        return MySqliHelper::executeWithLogging("ProductProvider.cloneReferences error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $CLONE_SOCIAL_MESSAGES_SQL = "
        INSERT INTO social_messaging_samples
        (
            type,
            message,
            relevant_entity,
            relevant_entity_id,
            `status`,
            sort_order,
            created_by
        )
        SELECT
            type,
            message,
            relevant_entity,
            ?,
            `status`,
            sort_order,
            ?
        FROM social_messaging_samples
        WHERE relevant_entity = 'campaign'
        AND relevant_entity_id = ?";

    private function cloneSocialMessages(ProductCloneCriteria $criteria, ProductRecord $newRecord){
        $sqlString = self::$CLONE_SOCIAL_MESSAGES_SQL;
        $sqlBinding = "iii";
        $sqlParams = array(
            $newRecord->id,
            getAdminId(),
            $criteria->record->id
        );
        return MySqliHelper::executeWithLogging("ProductProvider.cloneSocialMessages error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
	
	
	
}

class ProductCloneCriteria {

    public $record;
    public $cloneReferences;
    public $cloneSocialMessages;
}

class ProductResourceCriteria extends PaginationCriteria{
    public $tagId;
}

class ProductResourceProvider{
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    const CAMPAIGN_PAGE = 'Campaign';
    const EVENT_TEMPLATE_PAGE = 'Event Template';
    const CAMPAIGN_PAGE_RELATED = 'Campaign - Related';

    public static $PAGES_ARRAY = array(
        self::EVENT_TEMPLATE_PAGE,
        self::CAMPAIGN_PAGE,
        self::CAMPAIGN_PAGE_RELATED
    );

    private static $SELECT_COLUMNS = "
        SELECT
            TagReference_ID as id,
            TagReference_TagListID as tagID,
            TagReference_SortOrder as sortOrder,
            TagReference_DisplayText as displayText,
            TagReference_HoverText as hoverText,
            TagReference_URL as url,
            TagReference_TargetID as targetID,
            TagReference_Status as status,
            TagReference_Page as page";

    private static $FROM_TABLE_CLAUSE = "
        FROM tag_references
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND TagReference_ID = ?";

    private static $FILTER_ON_TAG_ID = "
        AND TagReference_TagListID = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            TagReference_DisplayText LIKE ?
            OR TagReference_DisplayText LIKE ?
            OR TagReference_HoverText LIKE ?
            OR TagReference_URL LIKE ?
            OR TagReference_TargetID LIKE ?
            OR TagReference_Page LIKE ?
        )";

    public function get(ProductResourceCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else {

            if ($criteria->tagId){
                $filterString .= self::$FILTER_ON_TAG_ID;
                $filterBinding .= "i";
                $filterParams[] = $criteria->tagId;
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToTagRefRecord((object)$row);
                    }
                } else {
                    trigger_error("ProductResourceProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("ProductResourceProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY TagReference_Page ASC, TagReference_SortOrder ASC, TagReference_DisplayText ASC";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY TagReference_ID ".$sortOrder;
                    break;
                case "page":
                    $orderBy = " ORDER BY TagReference_Page ".$sortOrder;
                    break;
                case "sortOrder":
                    $orderBy = " ORDER BY TagReference_SortOrder ".$sortOrder;
                    break;
                case "displayText":
                    $orderBy = " ORDER BY TagReference_DisplayText ".$sortOrder;
                    break;
                case "hoverText":
                    $orderBy = " ORDER BY TagReference_HoverText ".$sortOrder;
                    break;
                case "url":
                    $orderBy = " ORDER BY TagReference_URL ".$sortOrder;
                    break;
                case "targetID":
                    $orderBy = " ORDER BY TagReference_TargetID ".$sortOrder;
                    break;
                case "status":
                    $orderBy = " ORDER BY TagReference_Status ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = " TagReference_ID ".$sortOrder;
                        break;
                    case "page":
                        $orderByStrings[] = " TagReference_Page ".$sortOrder;
                        break;
                    case "sortOrder":
                        $orderByStrings[] = " TagReference_SortOrder ".$sortOrder;
                        break;
                    case "displayText":
                        $orderByStrings[] = " TagReference_DisplayText ".$sortOrder;
                        break;
                    case "hoverText":
                        $orderByStrings[] = " TagReference_HoverText ".$sortOrder;
                        break;
                    case "url":
                        $orderByStrings[] = " TagReference_URL ".$sortOrder;
                        break;
                    case "targetID":
                        $orderByStrings[] = " TagReference_TargetID ".$sortOrder;
                        break;
                    case "status":
                        $orderByStrings[] = " TagReference_Status ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private static $GET_TAG_REFERENCES_SQL = "
        SELECT
          TagReference_ID as id,
          TagReference_TagListID as tagID,
          TagReference_SortOrder as sortOrder,
          TagReference_DisplayText as displayText,
          TagReference_HoverText as hoverText,
          TagReference_URL as url,
          TagReference_TargetID as targetID,
          TagReference_Status as status,
          TagReference_Page as page
        FROM tag_references
        JOIN tag_list ON tag_references.TagReference_TagListID = tag_list.TagList_ID
        WHERE TagReference_TagListID = ?
        AND (TagReference_Status = 'ACTIVE' OR 'retrieveAll' = ? )
        AND ('' = ? OR TagReference_Page = ?)
        ORDER BY TagReference_Page, TagReference_SortOrder, TagReference_DisplayText";

    public function getTagReferences($tagID, $activeOnly = true, $page = ''){
        $sqlString = self::$GET_TAG_REFERENCES_SQL;
        $sqlBindings = "isss";
        $sqlParams = array(
            $tagID,
            ($activeOnly) ? "activeOnly" : "retrieveAll",
            $page,
            $page
        );
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToTagRefCollection($result);
    }

    private function mapToTagRefCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToTagRefRecord($row);
        }
        return $collection;
    }

    private static $GET_TAG_REFERENCE_SQL = "
       SELECT
          TagReference_ID as id,
          TagReference_TagListID as tagID,
          TagReference_SortOrder as sortOrder,
          TagReference_DisplayText as displayText,
          TagReference_HoverText as hoverText,
          TagReference_URL as url,
          TagReference_TargetID as targetID,
          TagReference_Status as status,
          TagReference_Page as page
        FROM tag_references
        WHERE TagReference_ID = ?";

    public function getTagReference($id){
        $result = MySqliHelper::get_result($this->mysqli, self::$GET_TAG_REFERENCE_SQL, array($id));
        if (count($result)){
            return $this->mapToTagRefRecord($result[0]);
        }
        return null;
    }

    private function mapToTagRefRecord($datarow) {
        $record = new TagReference();
        $record->id = $datarow->id;
        $record->tagID = $datarow->tagID;
        $record->sortOrder = $datarow->sortOrder;
        $record->displayText = $datarow->displayText;
        $record->hoverText = $datarow->hoverText;
        $record->url = $datarow->url;
        $record->targetID = $datarow->targetID;
        $record->isActive = $datarow->status == "ACTIVE";
        $record->page = $datarow->page;

        return $record;
    }

    private static $FIND_MATCHING_TAG_REFERENCE_SQL = "
        SELECT *
        FROM tag_references
        WHERE TagReference_TagListID = ?
        AND TagReference_Page = ?
        AND TagReference_DisplayText = ?";

    public function saveTagReference(TagReference $tagReference){
        $saveResponse = new stdClass();
        $saveResponse->message = "";
        if (!is_numeric($tagReference->tagID) || $tagReference->tagID <= 0){
            $saveResponse->message = "Invalid TagList_ID";
            return $saveResponse;
        }
        if (!$tagReference->displayText){
            $saveResponse->message = "DisplayText is a required field.";
            return $saveResponse;
        }
        if ($tagReference->isNew()){
            // first check to see if it's a dupe
            $dupes = MySqliHelper::get_result($this->mysqli, self::$FIND_MATCHING_TAG_REFERENCE_SQL, "iss", array($tagReference->tagID, $tagReference->page, $tagReference->displayText));
            if (count($dupes) > 1){
                $saveResponse->message = "There are already one or more references on this TagList_ID with the same displayText";
                return $saveResponse;
            } else if (count($dupes)){
                // make this an update
                $tagReference->id = $dupes[0]["TagReference_ID"];
            }
        }
        if ($tagReference->isNew()){
            $insertResponse = $this->insertTagReference($tagReference);
            if ($insertResponse->success){
                $saveResponse->record = $tagReference;
                $saveResponse->record->id = $insertResponse->insertedId;
            } else {
                $saveResponse->message = ($insertResponse->error) ? $insertResponse->error : "Error saving Tag Reference";
            }
        } else {
            $updateResponse = $this->updateTagReference($tagReference);
            if (!$updateResponse->success){
                $saveResponse->message = ($updateResponse->error) ? $updateResponse->error : "Error saving Tag Reference";
            } else {
                $saveResponse->record = $tagReference;
            }
        }
        return $saveResponse;
    }

    private static $INSERT_TAG_REFERENCE_SQL = "
        INSERT INTO tag_references
        (
            TagReference_TagListID,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Page
        ) VALUES (
            ?,?,?,?,?,?,?
        )";

    private static $INSERT_TAG_REFERENCE_BINDINGS = "iisssss";

    private function insertTagReference(TagReference $tagReference){
        $sqlString = self::$INSERT_TAG_REFERENCE_SQL;
        $sqlBindings = self::$INSERT_TAG_REFERENCE_BINDINGS;
        $sqlParams = array(
            $tagReference->tagID,
            $tagReference->sortOrder,
            $tagReference->displayText,
            $tagReference->hoverText,
            $tagReference->url,
            $tagReference->targetID,
            $tagReference->page
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }

    private static $UPDATE_TAG_REFERENCE_SQL = "
        UPDATE tag_references
        SET
            TagReference_SortOrder = ?,
            TagReference_DisplayText = ?,
            TagReference_HoverText = ?,
            TagReference_URL = ?,
            TagReference_TargetID = ?,
            TagReference_Status = ?,
            TagReference_Page = ?
        WHERE TagReference_ID = ?";

    private static $UPDATE_TAG_REFERENCE_BINDINGS = "issssssi";

    private function updateTagReference(TagReference $tagReference){
        $sqlString = self::$UPDATE_TAG_REFERENCE_SQL;
        $sqlBindings = self::$UPDATE_TAG_REFERENCE_BINDINGS;
        $sqlParams = array(
            $tagReference->sortOrder,
            $tagReference->displayText,
            $tagReference->hoverText,
            $tagReference->url,
            $tagReference->targetID,
            ($tagReference->isActive) ? "ACTIVE" : "DELETED",
            $tagReference->page,
            $tagReference->id
        );
        $updateResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
        return $updateResponse;
    }

}