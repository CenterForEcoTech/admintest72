<?php

class HaversineCalculator{
    public static $EARTH_RADIUS_MILES = 3959;
    public static $LATITUDE_MILES = 69.11;
    private static $KM_MULTIPLIER = 1.609344;

    public static function getUnitsPerLatitude($unitType = "M"){
        if (strtoupper($unitType) == "M"){
            return self::$LATITUDE_MILES;
        }
        return self::$LATITUDE_MILES * self::$KM_MULTIPLIER;
    }

    public static function getEarthRadius($unitType = "M"){
        if (strtoupper($unitType) == "M"){
            return self::$EARTH_RADIUS_MILES;
        }
        return self::$EARTH_RADIUS_MILES * self::$KM_MULTIPLIER;
    }

    /**
     * The following example groups the variables at the top:
     *  - user lat & long
     *  - number of units (miles or km) in one degree of latitude (at the equator one presumes)
     * @var string
     */
    private static $HAVERSINE_FORMULA_EXAMPLE_SQL = "
        SELECT
          @userLat := (38.88360000) AS userlat,
          @userLong := (-77.13950000) AS userlong,
          @unitsLat := (69.11) AS unitsLat,
          @maxDistance := (40) AS maxDistance,
          @earthRadius := (3959) AS earthRadius,
          @unitsLong := (cos(radians(MerchantInfo_Lat))*@unitsLat) AS unitsLong,
          @boxLat1 := (@userLat - (@maxDistance/@unitsLat)) as lat1,
          @boxLat2 := (@userLat + (@maxDistance/@unitsLat)) as lat2,
          @boxLong1 := (@userLong - (@maxDistance/abs(@unitsLong))) as long1,
          @boxLong2 := (@userLong + (@maxDistance/abs(@unitsLong))) as long2,
          MerchantInfo_Lat,
          MerchantInfo_Long,
          (
            @earthRadius * acos(
              cos( radians(MerchantInfo_Lat) ) *
              cos( radians( @userLat ) ) *
              cos( radians( @userLong ) - radians(MerchantInfo_Long) ) +
              sin( radians(MerchantInfo_Lat) ) *
              sin( radians( @userLat ) )
            )
          ) AS distance
        FROM merchant_info
        WHERE MerchantInfo_Lat between @boxLat1 and @boxLat2
        AND MerchantInfo_Long between @boxLong1 and @boxLong2
        ORDER BY distance";
}

class EventByLocationProvider extends EventProvider {

    private $defaultMaximumDistance;
    private $defaultMaximumDistanceUnits;

    public function __construct(mysqli $mysqli, $maximumDistance = -1, $maximumDistanceUnits = "M"){
        parent::__construct($mysqli);
        global $Config_IndexDistanceFromEvent;
        if ($maximumDistance < 0){
            $maximumDistance = $Config_IndexDistanceFromEvent ? $Config_IndexDistanceFromEvent : 100;
        }
        $this->defaultMaximumDistance = $maximumDistance;
        $this->defaultMaximumDistanceUnits = (strtoupper($maximumDistanceUnits[0]) == "M") ? "M" : "K";
    }

    protected static $GET_SELECT_COLUMNS = "
        SELECT
            e.Event_ID,
            e.Event_Title,
            e.Event_Description,
            e.Event_Date,
            e.Event_StartTime,
            e.Event_EndDate,
            e.Event_EndTime,
            e.Event_IsNational,
            e.Event_IsOnline,
            e.Event_IsFcOnly as isFcOnly,
            e.Event_OnlineLocationLabel as onlineLocationLabel,
            e.Event_OnlineLocationLink as onlineLocationLink,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = m.MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            m.MerchantInfo_ID,
            m.MerchantInfo_Name,
            m.MerchantInfo_Address,
            m.MerchantInfo_City,
            m.MerchantInfo_State,
            m.MerchantInfo_Zip,
            m.MerchantInfo_Country,
            m.MerchantInfo_Phone,
            m.MerchantInfo_Lat,
            m.MerchantInfo_Long,
            m.MerchantInfo_Website,
			m.MerchantInfo_Neighborhood as venueNeighborhood,
			m.MerchantInfo_Keywords as venueKeywords,
            e.Event_ProposedByOrgID,
            o.OrganizationInfo_ID,
            o.OrganizationInfo_Name,
			o.OrganizationInfo_Website,
			o.OrganizationInfo_EIN,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'organization'
                AND entity_id = o.OrganizationInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as orgPid,
            active_events.distance,
			active_events.featured_cause_percentage as Event_MerchantPercentage,
			active_events.customer_choice_percentage as Event_MerchantSecondaryPercentage,
			active_events.isFlatDonation as isFlatDonation,
			active_events.donationPerUnit as donationPerUnit,
			active_events.unitDescriptor as unitDescriptor,
			active_events.flatDonationActionPhrase as flatDonationActionPhrase,
            (CASE active_events.Event_LastEditedDate WHEN 0 THEN active_events.Event_CreatedDate ELSE active_events.Event_LastEditedDate END) as Event_EditedDate,

            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = e.Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames";

    private static $FROM_TABLE_CLAUSE = "
        FROM
        (
        SELECT
            ev.Event_ID AS event_id,
            (SELECT EventLog_Timestamp FROM event_log WHERE EventLog_EventID = ev.Event_ID AND EventLog_Action = 'Created' ORDER BY EventLog_ID limit 1) AS Event_CreatedDate,
            ev.Event_LastEditedDate,
            if(mp.MerchantInfo_ID, mp.MerchantInfo_ID, mm.MerchantInfo_ID) AS merchant_id,
            if(mp.MerchantInfo_ID, ev.Event_MerchantPercentage, em.EventMatch_MerchantPercentage) AS featured_cause_percentage,
            if(mp.MerchantInfo_ID, ev.Event_MerchantSecondaryPercentage, em.EventMatch_MerchantSecondaryPercentage) AS customer_choice_percentage,
            if(mp.MerchantInfo_ID, ev.Event_IsFlatDonation, em.EventMatch_IsFlatDonation) AS isFlatDonation,
            if(mp.MerchantInfo_ID, ev.Event_DonationPerUnit, em.EventMatch_DonationPerUnit) AS donationPerUnit,
            if(mp.MerchantInfo_ID, ev.Event_UnitDescriptor, em.EventMatch_UnitDescriptor) AS unitDescriptor,
            if(mp.MerchantInfo_ID, ev.Event_FlatDonationActionPhrase, em.EventMatch_FlatDonationActionPhrase) AS flatDonationActionPhrase,
            if (op.OrganizationInfo_ID, op.OrganizationInfo_ID, 0) AS org_id,
            @userLat := (?) AS userlat,
            @userLong := (?) AS userlong,
            @earthRadius := (?) AS earthRadius,

            if(mp.MerchantInfo_ID, mp.MerchantInfo_Lat, mm.MerchantInfo_Lat) AS MerchantInfo_Lat,
            if(mp.MerchantInfo_ID, mp.MerchantInfo_Long, mm.MerchantInfo_Long) AS MerchantInfo_Long,

            (
                @earthRadius * acos(
                    cos( radians(if(mp.MerchantInfo_ID, mp.MerchantInfo_Lat, mm.MerchantInfo_Lat)) ) *
                    cos( radians( @userLat ) ) *
                    cos( radians( @userLong ) - radians(if(mp.MerchantInfo_ID, mp.MerchantInfo_Long, mm.MerchantInfo_Long)) ) +
                    sin( radians(if(mp.MerchantInfo_ID, mp.MerchantInfo_Lat, mm.MerchantInfo_Lat)) ) *
                    sin( radians( @userLat ) )
                )
            ) AS distance
            FROM event_info ev
            LEFT JOIN merchant_info mp ON ev.Event_ProposedByMerchantID = mp.MerchantInfo_ID AND ev.Event_ProposedByMerchantID > 0
            LEFT JOIN organization_info op ON ev.Event_ProposedByOrgID = op.OrganizationInfo_ID
            LEFT JOIN event_match em ON em.EventMatch_EventID = ev.Event_ID AND ev.Event_ProposedByOrgID > 0 AND EventMatch_Status = 'Accepted'
            LEFT JOIN merchant_info mm ON em.EventMatch_MerchantID = mm.MerchantInfo_ID
            WHERE ev.Event_Status = 'ACTIVE'
            AND ( ev.Event_IsNational = 1 OR 0 = ?
                OR if(mp.MerchantInfo_ID, mp.MerchantInfo_Lat, mm.MerchantInfo_Lat) BETWEEN ? AND ?
                AND if(mp.MerchantInfo_ID, mp.MerchantInfo_Long, mm.MerchantInfo_Long) BETWEEN ? AND ?
            )
            AND (mm.MerchantInfo_ID > 0 OR mp.MerchantInfo_ID > 0)
            AND (
                0 = ?
                OR ev.Event_EndDate >= CURRENT_DATE)
            AND (
                0 = ?
                OR EXISTS(
                    SELECT 1
                    FROM tag_association
                    JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                    WHERE TagAssociation_SourceID = Event_ID
                    AND TagAssociation_SourceTable = 'event_info'
                    AND TagAssociation_Status IS NULL
                    AND TagList_Name = ?
                )
            )
        ) AS active_events
        JOIN event_info e ON active_events.event_id = e.Event_ID
        JOIN merchant_info m ON active_events.merchant_id = m.MerchantInfo_ID
        LEFT JOIN organization_info o ON active_events.org_id = o.OrganizationInfo_ID
        WHERE ( e.Event_IsNational = 1 OR 0 = ? OR active_events.distance <= ?)";

    private static $BASIC_QUERY_BINDING = "dddiddddiisid";

    private static $FILTER_ON_IS_ONLINE = "
        AND e.Event_IsOnline = 1";

    private static $FILTER_ON_NOT_IS_ONLINE = "
        AND e.Event_IsOnline = 0";

    private static $FILTER_ON_ORG_ID_OR_CC = "
       AND (o.OrganizationInfo_ID = ? OR active_events.isFlatDonation = 1 OR active_events.customer_choice_percentage > 0)";

    private static $FILTER_ON_KEYWORD = "
        AND (
            e.Event_Title LIKE ?
            OR e.Event_Category LIKE ?
            OR e.Event_Description LIKE ?
            OR m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR o.OrganizationInfo_Name LIKE ?
            OR m.MerchantInfo_Keywords LIKE ?
            OR m.MerchantInfo_Neighborhood LIKE ?
        )";

    private function getBoundingBoxValues($centerLat, $centerLong){
        $unitsLat = HaversineCalculator::getUnitsPerLatitude($this->defaultMaximumDistanceUnits); // units (e.g., miles) per latitude
        $unitsLong = abs( cos( deg2rad( $unitsLat ) ) * $unitsLat ); // units (e.g., miles) per longitude at that latitude
        $maximumDistance = $this->defaultMaximumDistance;
        $latDegreesFromCenter = $maximumDistance/$unitsLat;
        $longDegreesFromCenter = $maximumDistance/$unitsLong;

        $northLat = $centerLat + $latDegreesFromCenter;
        $southLat = $centerLat - $latDegreesFromCenter;
        $eastLong = $centerLong + $longDegreesFromCenter;
        $westLong = $centerLong - $longDegreesFromCenter;

        return (object)array(
            "north" => $northLat,
            "south" => $southLat,
            "east" => $eastLong,
            "west" => $westLong
        );
    }

    /**
     * Returns a PagedResult that contains the paginated results as well as more general data about
     * the available records that match the criteria.
     * @param EventByLocationCriteria $criteria
     * @param bool $getCountOnly
     * @return PagedResult
     */
    public function get(EventByLocationCriteria $criteria, $getCountOnly = false){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$GET_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = self::$BASIC_QUERY_BINDING;
            $userLat = (is_numeric($criteria->userLat)) ? $criteria->userLat : 0;
            $userLong = (is_numeric($criteria->userLong)) ? $criteria->userLong : 0;
            $boundingBox = $this->getBoundingBoxValues($userLat, $userLong);
            $maximumDistance = $this->defaultMaximumDistance;
            $earthRadius = HaversineCalculator::getEarthRadius($this->defaultMaximumDistanceUnits);
            $upcomingOnly = ($criteria->upcomingOnly) ? 1 : 0;
            $limitByDistance = $criteria->limitByDistance;
        if ($criteria->tagName){
            $limitByTagName = 1;
            $tagNameParam = $criteria->queryTagName;
        } else {
            $limitByTagName = 0;
            $tagNameParam = "";
        }
        $filterParams = array(
            $userLat,
            $userLong,
            $earthRadius,
            $limitByDistance ? 1 : 0,
            $boundingBox->south,
            $boundingBox->north,
            $boundingBox->west,
            $boundingBox->east,
            $upcomingOnly,
            $limitByTagName,
            $tagNameParam,
            $limitByDistance ? 1 : 0,
            $maximumDistance
        );
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // org or cc
        if ($criteria->orgId && is_numeric($criteria->orgId)){
            $filterString .= self::$FILTER_ON_ORG_ID_OR_CC;
            $filterBinding .= "i";
            $filterParams[] = $criteria->orgId;
        }

        // apply search
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->onlineOnly > 0){
            $filterString .= self::$FILTER_ON_IS_ONLINE;
        } else if ($criteria->onlineOnly < 0){
            $filterString .= self::$FILTER_ON_NOT_IS_ONLINE;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            if ($queryResponse->success){
                $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;
            }

            if (!$getCountOnly && $result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        $result->collection[] = $this->mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("EventByLocationProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventByLocationProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.Event_ID desc";

    private function getOrderByClause(EventByLocationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "active":
                        $orderByStrings[] = "e.Event_EndDate > CURRENT_DATE ".$sortOrder;
                        break;
                    case "timestamp":
                        $orderByStrings[] = "e.Event_ID ".$sortOrder;
                        break;
                    case "startDate":
                        $orderByStrings[] = "e.Event_Date ".$sortOrder;
                        break;
                    case "endDate":
                        $orderByStrings[] = "e.Event_EndDate ".$sortOrder;
                        break;
                    case "startTime":
                        $orderByStrings[] = "e.Event_StartTime ".$sortOrder;
                        break;
                    case "endTime":
                        $orderByStrings[] = "e.Event_EndTime ".$sortOrder;
                        break;
                    case "distance":
                        // sorting by distance requires a maximum distance
                        if (is_numeric($criteria->userLat) && is_numeric($criteria->userLong)){
                            $orderByStrings[] = "distance ".$sortOrder;
                        }
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
}