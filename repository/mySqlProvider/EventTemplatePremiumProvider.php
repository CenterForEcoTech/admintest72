<?php

class EventTemplatePremiumProvider{
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            id,
            event_template_id,
            sort_order,
            status,
            name,
            price,
            display_text,
            display_text_markdown,
            is_default";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_template_premiums
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND id = ?";

    private static $FILTER_ON_EVENT_TEMPLATE_ID = "
        AND event_template_id = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            name LIKE ?
            OR status LIKE ?
            OR display_text_markdown LIKE ?
        )";

    public function get(EventTemplatePremiumCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else {

            if ($criteria->templateID){
                $filterString .= self::$FILTER_ON_EVENT_TEMPLATE_ID;
                $filterBinding .= "i";
                $filterParams[] = $criteria->templateID;
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToContract((object)$row);
                    }
                } else {
                    trigger_error("EventTemplatePremiumProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventTemplatePremiumProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY sort_order ASC, price ASC";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY id ".$sortOrder;
                    break;
                case "name":
                    $orderBy = " ORDER BY name ".$sortOrder;
                    break;
                case "sortOrder":
                    $orderBy = " ORDER BY sort_order ".$sortOrder;
                    break;
                case "status":
                    $orderBy = " ORDER BY status ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = " id ".$sortOrder;
                        break;
                    case "name":
                        $orderByStrings[] = " name ".$sortOrder;
                        break;
                    case "sortOrder":
                        $orderByStrings[] = " sort_order ".$sortOrder;
                        break;
                    case "status":
                        $orderByStrings[] = " status ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private static $GET_PREMIUMS_SQL = "
        SELECT
          id,
          event_template_id,
          sort_order,
          name,
          price,
          display_text,
          display_text_markdown,
          status,
          is_default
        FROM event_template_premiums
        WHERE event_template_id = ?
        AND (status = 'ACTIVE' OR 'retrieveAll' = ? )
        ORDER BY sort_order, price";

    public function getPremiums($eventTemplateId, $activeOnly = true){
        $sqlString = self::$GET_PREMIUMS_SQL;
        $sqlBindings = "is";
        $sqlParams = array(
            $eventTemplateId,
            ($activeOnly) ? "activeOnly" : "retrieveAll"
        );
        $queryResponse = MySqliHelper::getResultWithLogging("EventTemplatePremiumProvider.getPremiums error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToPremiumsCollection($queryResponse->resultArray);
    }

    private static $GET_PREMIUMS_BY_PREMIUM_SQL = "
        SELECT
            id,
            event_template_id,
            sort_order,
            name,
            price,
            display_text,
            display_text_markdown,
            status,
            is_default
        FROM event_template_premiums
        WHERE event_template_id = (
            SELECT event_template_id
            FROM event_template_premiums related
            WHERE related.id = ?
        )
        AND status = 'ACTIVE'
        ORDER BY sort_order, price";

    /**
     * Given a premiumID, returns all other premiums on the same event_template
     */
    public function getPremiumsByPremiumId($premiumId){
        $sqlString = self::$GET_PREMIUMS_BY_PREMIUM_SQL;
        $sqlBinding = "i";
        $sqlParams = array( $premiumId );
        $response = MySqliHelper::getResultWithLogging("EventTemplatePremiumProvider.getPremiumsByPremiumId error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $this->mapToPremiumsCollection($response->resultArray);
    }

    private function mapToPremiumsCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToContract($row);
        }
        return $collection;
    }

    private function mapToContract($datarow) {
        $record = new EventTemplatePremium(null);
        $record->id = $datarow->id;
        $record->eventTemplateId = $datarow->event_template_id;
        $record->name = $datarow->name;
        $record->price = $datarow->price;
        $record->sortOrder = $datarow->sort_order;
        $record->displayText = $datarow->display_text;
        $record->displayTextMarkdownSource = $datarow->display_text_markdown;
        $record->isActive = $datarow->status == "ACTIVE";
        $record->isDefault = $datarow->is_default == 1;

        return $record;
    }

    public function save(EventTemplatePremium $record){
        $saveResponse = new stdClass();
        $saveResponse->message = "";
        if (!is_numeric($record->eventTemplateId) || $record->eventTemplateId <= 0){
            $saveResponse->message = "Invalid event_template_id: ".$record->eventTemplateId." ". !is_numeric($record->eventTemplateId);
            return $saveResponse;
        }
        if (!$record->displayText){
            $saveResponse->message = "DisplayText is a required field.";
            return $saveResponse;
        }
        if (!$record->name){
            $saveResponse->message = "Name is a required field.";
            return $saveResponse;
        }
        if (!strlen($record->price) || !is_numeric($record->price)){
            $saveResponse->message = "Price is a required field, and it must be numeric.";
            return $saveResponse;
        }
        if ($record->isNew()){
            $insertResponse = $this->insert($record);
            if ($insertResponse->success){
                $saveResponse->record = $record;
                $saveResponse->record->id = $insertResponse->insertedId;
            } else {
                $saveResponse->message = ($insertResponse->error) ? $insertResponse->error : "Error saving Event Template Premium";
            }
        } else {
            $updateResponse = $this->update($record);
            if (!$updateResponse->success){
                $saveResponse->message = ($updateResponse->error) ? $updateResponse->error : "Error saving Event Template Premium";
            } else {
                $saveResponse->record = $record;
            }
        }
        if ($saveResponse->record){
            $this->setDefault($saveResponse->record);
        }
        return $saveResponse;
    }

    private static $INSERT_PREMIUM_SQL = "
        INSERT INTO event_template_premiums
        (
            event_template_id,
            sort_order,
            display_text,
            display_text_markdown,
            name,
            price,
            is_default
        ) VALUES (
            ?,?,?,?,?,?,?
        )";

    private static $INSERT_PREMIUM_BINDINGS = "iisssdi";

    private function insert(EventTemplatePremium $record){
        $sqlString = self::$INSERT_PREMIUM_SQL;
        $sqlBindings = self::$INSERT_PREMIUM_BINDINGS;
        $sqlParams = array(
            $record->eventTemplateId,
            $record->sortOrder,
            $record->displayText,
            $record->displayTextMarkdownSource,
            $record->name,
            $record->price,
            ($record->isDefault) ? 1 : 0
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::executeWithLogging("EventTemplatePremiumProvider.insert:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }

    private static $UPDATE_PREMIUM_SQL = "
        UPDATE event_template_premiums
        SET
            sort_order = ?,
            display_text = ?,
            display_text_markdown = ?,
            name = ?,
            price = ?,
            status = ?,
            is_default = ?
        WHERE id = ?";

    private static $UPDATE_PREMIUM_BINDINGS = "isssdsii";

    private function update(EventTemplatePremium $record){
        $sqlString = self::$UPDATE_PREMIUM_SQL;
        $sqlBindings = self::$UPDATE_PREMIUM_BINDINGS;
        $sqlParams = array(
            $record->sortOrder,
            $record->displayText,
            $record->displayTextMarkdownSource,
            $record->name,
            $record->price,
            ($record->isActive) ? "ACTIVE" : "DELETED",
            ($record->isDefault) ? 1 : 0,
            $record->id
        );
        $updateResponse = new ExecuteResponseRecord();
        MySqliHelper::executeWithLogging("EventTemplatePremiumProvider.update:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
        return $updateResponse;
    }

    private static $SET_DEFAULT_SQL = "
        UPDATE event_template_premiums
        SET is_default = 0
        WHERE id <> ?
        AND event_template_id = ?
        AND is_default = 1";

    private function setDefault(EventTemplatePremium $record){
        if ($record->isDefault){
            $sqlString = self::$SET_DEFAULT_SQL;
            $sqlBinding = "ii";
            $sqlParams = array(
                $record->id,
                $record->eventTemplateId
            );
            $updateResponse = new ExecuteResponseRecord();
            MySqliHelper::executeWithLogging("EventTemplatePremiumProvider.setDefault: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            return $updateResponse->success;
        }
        return false;
    }
}