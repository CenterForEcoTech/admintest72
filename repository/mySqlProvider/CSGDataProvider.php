<?php
class CSGDataProvider {
	private $mysqli;
	
    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
	private static $SELECT_CSGEXTRACTDATA_SQL = "
		SELECT * ";
			
	private static $FROM_TABLE_CSGEXTRACTDATA_CLAUSE="
		FROM csg_extract_data WHERE 1=1";
	private static $FILTER_CSGEXTRACTDATA_ON_DESCRIPTION = "
		AND Description = ?";
	private static $FILTER_CSGEXTRACTDATA_ON_PROGCODE = "
		AND Progcode = ?";
	
    public function getExtractData($criteria){
        $paginationSelectString = self::$SELECT_CSGEXTRACTDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGEXTRACTDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY Date";

		if ($criteria->description){
            $filterString .= self::$FILTER_CSGEXTRACTDATA_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
		}
		if ($criteria->progcode){
            $filterString .= self::$FILTER_CSGEXTRACTDATA_ON_PROGCODE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->progcode;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("CSGDataProvider.getExtractData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		

	private static $SELECT_CSGHUBBGEXTRACTDATA_SQL = "
		SELECT * ";
			
	private static $FROM_TABLE_CSGHUBBGEXTRACTDATA_CLAUSE="
		FROM csg_hub_bgextract_data WHERE 1=1";
	private static $FILTER_CSGHUBBGEXTRACTDATA_ON_DESCRIPTION = "
		AND Description = ?";
	
    public function getHUBBGExtractData($criteria){
        $paginationSelectString = self::$SELECT_CSGHUBBGEXTRACTDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGHUBBGEXTRACTDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY INSTALL_DT";

		if ($criteria->description){
            $filterString .= self::$FILTER_CSGHUBBGEXTRACTDATA_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("CSGDataProvider.getHUBBGExtractData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	public function loadExternalReportData($fileLocation,$tableName=Null){
		$tableName = ($tableName ? $tableName : "csg_hub_schedulereport_data");
		$sqlResult = MySqliHelper::loadExternalData($this->mysqli, $fileLocation,$tableName);		
		return $sqlResult;
	}
	public function cleanReportData(){
		$SQLTestForDuplicates = "SELECT `Site ID`,`Appt Date`,`Appt Type`, Count(`Site ID`) as X FROM `csg_hub_schedulereport_data` WHERE (`Site ID` !='' AND `Site ID` IS NOT NULL) GROUP BY `Site ID`,`Appt Date`,`Appt Type` HAVING X >1 ORDER BY `csg_hub_schedulereport_data`.`Appt Date`  DESC";

		$sqlString = "DELETE n1 FROM csg_hub_schedulereport_data n1, csg_hub_schedulereport_data n2 WHERE (n1.CSGHUBScheduleReportData_ID > n2.CSGHUBScheduleReportData_ID) AND (n1.`Site ID` = n2.`Site ID` AND n1.`Appt Date` = n2.`Appt Date` AND n1.`Appt Type` = n2.`Appt Type`)";
		$sqlBindings = "";
		$sqlParams = array();
        $Response = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $Response);
		return $result;
	}
	
	private static $SELECT_CSGHUBSCHEDULEREPORTDATA_SQL = "
		SELECT * ";
			
	private static $FROM_TABLE_CSGHUBSCHEDULEREPORTDATA_CLAUSE="
		FROM csg_hub_schedulereport_data WHERE 1=1";
	private static $FILTER_CSGHUBSCHEDULEREPORTDATA_ON_DESCRIPTION = "
		AND Description = ?";
	private static $FILTER_CSGHUBSCHEDULEREPORTDATA_ON_DATE = "
		AND `Appt Date` BETWEEN ? AND ?";
	
    public function getHUBScheduleReportData($criteria){
        $paginationSelectString = self::$SELECT_CSGHUBSCHEDULEREPORTDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGHUBSCHEDULEREPORTDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY `Appt Date`";

		if ($criteria->description){
            $filterString .= self::$FILTER_CSGHUBSCHEDULEREPORTDATA_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_CSGHUBSCHEDULEREPORTDATA_ON_DATE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
			
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CSGDataProvider.getHUBScheduleReportData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	
	private static $SELECT_CSGHUBAPPTANALYSIS_SQL = "
		SELECT 
			CSGHubScheduleReportAnalysis_YearMonth as yearMonth,
			CSGHubScheduleReportAnalysis_ApptType as apptType,
			CSGHubScheduleReportAnalysis_Count as count,
			CSGHubScheduleReportAnalysis_AsOfDate as asOfDate";
	private static $FROM_TABLE_CSGHUBAPPTANALYSIS_CLAUSE = "
		FROM csg_hub_schedulereport_analysis WHERE 1=1";
	
	private static $FILTER_CSGHUBAPPTANALYSIS_ON_DATE = "
		AND CSGHubScheduleReportAnalysis_YearMonth BETWEEN ? and ?";
		
	private static $SELECT_CSGHUBAPPTANALYSIS_INSPECTION_SQL = "
		SELECT 
			MAX(CSGHubScheduleReportAnalysis_Count) as maxCount,
			CSGHubScheduleReportAnalysis_YearMonth as yearMonth,
			CSGHubScheduleReportAnalysis_ApptType as apptType";
	private static $FILTER_CSGHUBAPPTANALYSIS_INSPECTION = "
		AND CSGHubScheduleReportAnalysis_ApptType LIKE '%inspection%' OR CSGHubScheduleReportAnalysis_ApptType LIKE '%mech%'
		AND CSGHubScheduleReportAnalysis_YearMonth >= ?
		GROUP BY CSGHubScheduleReportAnalysis_YearMonth, CSGHubScheduleReportAnalysis_ApptType";		
		
    public function getApptTypeAnlysis($criteria){
        $paginationSelectString = self::$SELECT_CSGHUBAPPTANALYSIS_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGHUBAPPTANALYSIS_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
//		$OrderBy = " ORDER BY `CSGHubScheduleReportAnalysis_AsOfDate`,`CSGHubScheduleReportAnalysis_YearMonth`,`CSGHubScheduleReportAnalysis_ApptType`";
		$OrderBy = " ORDER BY CSGHubScheduleReportAnalysis_ApptType,CSGHubScheduleReportAnalysis_AsOfDate,CSGHubScheduleReportAnalysis_YearMonth";

		if ($criteria->endDate){
            $filterString .= self::$FILTER_CSGHUBAPPTANALYSIS_ON_DATE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
			
		}
		if ($criteria->inspections){
			$paginationSelectString = self::$SELECT_CSGHUBAPPTANALYSIS_INSPECTION_SQL;
            $filterString .= self::$FILTER_CSGHUBAPPTANALYSIS_INSPECTION;
			$filterBinding .="s";
			$filterParams[] = $criteria->startDate;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
//		echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CSGDataProvider.getApptTypeAnlysis failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	
	private static $SELECT_CSGMRVDATA_SQL = "
		SELECT * ";
			
	private static $FROM_TABLE_CSGMRVDATA_CLAUSE="
		FROM csg_mrv_data WHERE 1=1";
	private static $FILTER_CSGMRVDATA_ON_DATE = "
		AND CSGMRVData_Date = ?";
	
    public function getMRVData($criteria){
        $paginationSelectString = self::$SELECT_CSGMRVDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGMRVDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY CSGMRVData_Date";

		if ($criteria->mrvdate){
            $filterString .= self::$FILTER_CSGMRVDATA_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->mrvdate;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CSGDataProvider.getMRVData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }	
	
    private static $INSERT_CSGSCHEDULEANALYSIS_SQL = "
        INSERT INTO csg_hub_schedulereport_analysis
        (
			CSGHubScheduleReportAnalysis_YearMonth,
			CSGHubScheduleReportAnalysis_ApptType, 
			CSGHubScheduleReportAnalysis_Count, 
			CSGHubScheduleReportAnalysis_AsOfDate
		) VALUES (
            ?,?,?,?
        )ON DUPLICATE KEY UPDATE CSGHubScheduleReportAnalysis_ID = CSGHubScheduleReportAnalysis_ID";

    private static  $INSERT_CSGSCHEDULEANALYSIS_BINDINGS = "ssis";
	
    public function insertScheduleReportAnalysis($ApptType,$value,$YearMonth){
        $sqlString = self::$INSERT_CSGSCHEDULEANALYSIS_SQL;
        $sqlBindings = self::$INSERT_CSGSCHEDULEANALYSIS_BINDINGS;
		$AsOfDate = date("Y-m-d");
        $sqlParams = array(
			$YearMonth,
			$ApptType,
			$value,
			$AsOfDate
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	
	private static $SELECT_CSGINSPECTIONREFUSED_SQL = "
		SELECT 
			CSGHUBInspectionRefused_ID as Id,
			CSGHUBInspectionRefused_Status as status,
			CSGHUBInspectionRefused_SiteID as siteId,
			CSGHUBInspectionRefused_ByAdminID as byAdminId,
			CSGHUBInspectionRefused_TimeStamp as timeStamp
		";
			
	private static $FROM_TABLE_CSGINSPECTIONREFUSED_CLAUSE="
		FROM csg_hub_inspection_refused WHERE 1=1";
	
    public function getInspectionRefused($criteria){
        $sqlSelectString = self::$SELECT_CSGINSPECTIONREFUSED_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGINSPECTIONREFUSED_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY CSGHUBInspectionRefused_SiteID";

		$pageSqlString = $sqlSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CSGDataProvider.getInspectionRefused failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }	
	
    private static $INSERT_CSGINSPECTIONREFUSED_SQL = "
        INSERT INTO csg_hub_inspection_refused
        (
			CSGHUBInspectionRefused_Status,
			CSGHUBInspectionRefused_SiteID,
			CSGHUBInspectionRefused_ByAdminID
		) VALUES (
            ?,?,?
        ) ON DUPLICATE KEY UPDATE CSGHUBInspectionRefused_Status = ?";

	public function insertInspectionRefused($siteId,$status,$adminId){
        $sqlString = self::$INSERT_CSGINSPECTIONREFUSED_SQL;
        $sqlBindings = "ssss";
        $sqlParams = array(
			$status,
			$siteId,
			$adminId,
			$status
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	private static $SELECT_CSGINSPECTIONDATA_SQL = "
		SELECT * FROM csg_hub_inspectionreport_data WHERE 1=1";
	
    public function getInspectionData($criteria){
        $sqlSelectString = self::$SELECT_CSGINSPECTIONDATA_SQL;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY SITEID";

		$pageSqlString = $sqlSelectString.$filterString.$OrderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CSGDataProvider.getInspectionData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }	
	
}
?>