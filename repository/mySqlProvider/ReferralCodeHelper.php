<?php
/**
 * Static Helper Class.
 */
include_once("Helper.php");
class ReferralCodeHelper{
    private static $GET_ALL_REFERRAL_CODES_SQL = "
        SELECT
		  CodesCampaign_ID as id,
		  CodesCampaign_Code as code,
		  CodesCampaign_Comments as comments
		FROM codes_campaign";

    public static function getAllReferralCodes($mysqli){
        return MySqliHelper::get_result($mysqli, self::$GET_ALL_REFERRAL_CODES_SQL);
    }

    private static $GET_ALL_BILLING_CODES_SQL = "
        SELECT
            CodesCampaign_ID as id,
            CodesCampaign_Code as billing_code,
            CodesCampaign_Comments as comments,
            IF (CodesCampaign_ValidUseDate = '0000-00-00', NULL, CodesCampaign_ValidUseDate) as valid_on_date,
            IF (CodesCampaign_ValidUntilDate = '0000-00-00', NULL, CodesCampaign_ValidUntilDate) as valid_until_date,
            CodesCampaign_ServiceRateType as rate_type,
            CodesCampaign_ServiceRateAmount as rate_amount
        FROM codes_campaign
        WHERE (CodesCampaign_ValidUntilDate = '0000-00-00' OR CURRENT_DATE < CodesCampaign_ValidUntilDate)";

    public static function getAllBillingCodes($mysqli){
        $arrayOfResults = MySqliHelper::get_result($mysqli, self::$GET_ALL_BILLING_CODES_SQL);
        $results = array(
            (object)array(
                "id" => 0,
                "billing_code" => "(none)",
                "valid_on_date" => "",
                "valid_until_date" => "",
                "rate_type" => "percentage",
                "rate_amount" => 3.00
            ) // default rate
        );
        foreach ($arrayOfResults as $row){
            $results[] = (object)$row;
        }
        return $results;
    }

    public static $GET_REFERRAL_CODE_BY_NAME_SQL ="
		SELECT
		  CodesCampaign_ID as id,
		  CodesCampaign_Code as code,
		  CodesCampaign_Comments as comments,
		  CodesCampaign_ValidUntilDate as validUntilDate,
		  CodesCampaign_ValidUseDate as validOnDate,
		  CodesCampaign_ServiceRateType as serviceRateType,
		  CodesCampaign_ServiceRateAmount as serviceRateAmount
		FROM codes_campaign
		WHERE LOWER(CodesCampaign_Code)= ?
		LIMIT 1";

    /**
     * Returns the matching referral code object, if any. Otherwise, returns null. ContentProvider has a dependency on this.
     * @static
     * @param $mysqli
     * @param $referralCode
     * @return object|null
     */
    public static function getReferralCodeByName($mysqli, $referralCode){
        $result = MySqliHelper::get_result($mysqli, self::$GET_REFERRAL_CODE_BY_NAME_SQL, "s", array(strtolower($referralCode)));
        if (count($result)){
            return (object)$result[0];
        }
        return null;
    }
}
?>