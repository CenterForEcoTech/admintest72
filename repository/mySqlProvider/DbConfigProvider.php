<?php


class DbConfigProvider implements SiteConfigProvider{
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $INSERT_CONFIG = "
      INSERT INTO `configs`
      (
        `Config_Name`,
        `Config_Description`,
        `Config_Type`,
        `Config_Options`,
        `Config_DefaultValue`,
        `Config_OrderID`
      )
      SELECT
        ?,
        ?,
        'text',
        NULL,
        ?,
        ?
      FROM dual
      WHERE NOT EXISTS (
          SELECT 1
          FROM configs
          WHERE Config_Name = ?
      )";

    public function setConfig($name, $value, $createIfNotExists = false, $description = "", $order = 0){
        $sqlString = "UPDATE configs SET Config_DefaultValue = ? WHERE Config_Name = ?";
        $sqlBindings = "ss";
        $sqlParams = array($value, $name);
        $response = new ExecuteResponseRecord();
        $updateResult = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $response);
        if ($createIfNotExists && $response->affectedRows == 0){
            $sqlString = self::$INSERT_CONFIG;
            $sqlBindings = "sssis";
            $sqlParams = array(
                $name,
                $description,
                $value,
                $order,
                $name
            );
            $response = new ExecuteResponseRecord();
            $insertResult = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $response);
        }
        return $response;
    }
}