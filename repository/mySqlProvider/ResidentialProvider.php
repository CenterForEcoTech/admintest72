<?php
class ResidentialProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
    public function getResAuditFields($criteria){
        $sqlString = "SELECT ResidentialAuditFields_Id as id, ResidentialAuditFields_Sheet as sheet, ResidentialAuditFields_Cell as cell, ResidentialAuditFields_FieldName as fieldName, ResidentialAuditFields_FieldSource as fieldSource";
		$fromTableClause = " FROM residential_auditfields WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY ResidentialAuditFields_Cell";
        // apply search
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getResAuditFields failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
    private static $INSERT_RESAUDIT_SQL = "
		INSERT INTO residential_auditfields
		(
			ResidentialAuditFields_Cell,
			ResidentialAuditFields_FieldName,
			ResidentialAuditFields_FieldSource,
			ResidentialAuditFields_Sheet
		) VALUES (
			?,?,?,?
		)";

    public function insertResAuditCell($record){
        $sqlString = self::$INSERT_RESAUDIT_SQL;
        $sqlBinding = "ssss";
        $sqlParam = array(
			$record->cell,
			$record->fieldName,
			$record->fieldSource,
			$record->sheet
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.insertResAuditCell failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	public function removeResAuditCell($record){
		$sqlString = "DELETE FROM residential_auditfields WHERE ResidentialAuditFields_Id=?";
		$sqlBinding="i";
		$sqlParams = array($record->id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	
    private static $INSERT_RESEZDOCDATA_SQL = "
		INSERT INTO residential_ezdocdata
		(
			ResidentialEZDocData_DocumentID,
			ResidentialEZDocData_SiteID,
			ResidentialEZDocData_ProjectID,
			ResidentialEZDocData_CreatedBy,
			ResidentialEZDocData_Status,

			ResidentialEZDocData_CheckedOutStatus,
			ResidentialEZDocData_LastUpdatedBy,
			ResidentialEZDocData_LastUpdatedTimeStamp,
			ResidentialEZDocData_DataByFieldName,
			ResidentialEZDocData_DataBySheet,

			ResidentialEZDocData_DataByMeasure,
			ResidentialEZDocData_EFIFlagged,
			ResidentialEZDocData_EFIExtractedDate,
			ResidentialEZDocData_ISMExtractedDate,
			ResidentialEZDocData_CreateCustomerDocumentsStatus,

			ResidentialEZDocData_CreateCustomerDocumentsStatusText,
			ResidentialEZDocData_EZDocVersionDate
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?
		)";

    public function insertResEZDocData($record,$adminId){
		
		//first mark all other documents as 'Revised'
		$revisedStatus = $this->reviseEZDocRecord($record->documentId);
		
        $sqlString = self::$INSERT_RESEZDOCDATA_SQL;
        $sqlBinding = "sssssssssssississ";
        $sqlParam = array(
			$record->documentId,
			$record->siteId,
			$record->projectId,
			$record->createdBy,
			$record->status,
			$record->checkedOutStatus,
			$adminId,
			$record->lastUpdatedTimeStamp,
			$record->dataByFieldName,
			$record->dataBySheet,
			$record->dataByMeasure,
			$record->efiFlag,
			$record->efiExtractedDate,
			$record->ismExtractedDate,
			$record->createCustomerDocumentsStatus,
			$record->createCustomerDocumentsStatusText,
			$record->ezDocVersionDate
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.insertResEZDocData failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

		//now update schedule data for uploaded date
		$criteria = new stdClass();
		$criteria->siteId = $record->siteId;
		$criteria->projectId = $record->projectId;
		$scheduledDataResults = $this->getScheduledData($criteria);
		$uploadedDate = $scheduledDataResults->collection[0]->ezDocUploadedDate;
		$scheduleId = $scheduledDataResults->collection[0]->id;
		if ($uploadedDate == "0000-00-00"){
			$scheduledDataResults = $this->updateScheduledUploadedDate($uploadedDate,$scheduleId);
		}

		
        return $insertResponse;
    }
	
	public function reviseEZDocRecord($documentId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_Status=? WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="ss";
		$sqlParams = array("Revised",$documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	public function ismExtractDateSet($documentId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_ISMExtractedDate=? WHERE ResidentialEZDocData_ID=?";
		$sqlBinding="ss";
		$dateToday= date("Y-m-d");
		$sqlParams = array($dateToday,$documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	public function ismNoElectricSet($documentId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_NoElectricISMs=1 WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="s";
		$sqlParams = array($documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	public function checkOutEZDoc($documentId,$checkedOutBy,$downloadType="Downloadable"){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_CheckedOutStatus=? WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="ss";
		$statusText = ($downloadType == "Downloadable" ? "Checked Out By " : "Currently Cloned By ");
		$sqlParams = array($statusText.$checkedOutBy,$documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	public function isCheckedOut($documentId){
		$sqlString = "SELECT ResidentialEZDocData_CheckedOutStatus FROM residential_ezdocdata WHERE ResidentialEZDocData_Status='Current' AND ResidentialEZDocData_DocumentID = ? LIMIT 0,1";
		$filterBinding = "s";
		$filterParams = array($documentId);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if (count($sqlResult)){
			$isCheckedOutResult = $sqlResult[0]["ResidentialEZDocData_CheckedOutStatus"];
		}else{
			$isCheckedOutResult = False;
		}
		return $isCheckedOutResult;
		
	}
	
	public function bgasFlagged($documentId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_BGASFlagged=1 WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="s";
		$sqlParams = array($documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}	
	public function contractorFlagged($documentId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_ContractorFlagged=1 WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="s";
		$sqlParams = array($documentId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}	
	
	public function ismExtract($docIds){
		foreach ($docIds as $docId){
			
			$criteria = new stdClass();
			$criteria->documentId = $docId;
			$criteria->status = "Current";
			$criteria->ismExtractedDate = "0000-00-00";
			$thisrecord = $this->getEZDocs($criteria);
			$thiscollection = $thisrecord->collection[0];
			$dataByFieldName = json_decode($thiscollection->dataByFieldName);
			$dataByMeasure = json_decode($thiscollection->dataByMeasure);
			$thisId= $thiscollection->id;
			$ismExtractResult=$this->ismExtractDateSet($thisId);
			$ismExtractResults[$docId]["ismExtract"]=$ismExtractResult;
			$ismExtractResults[$docId]["lookup"]=$thiscollection;
			
			//return $thiscollection;
		}
		return $ismExtractResults;
	}
	
	public function bgasISMExtract($docIds){
		foreach ($docIds as $docId){
			//check to ismExtractDate is 0000-00-00
			$criteria = new stdClass();
			$criteria->documentId = $docId;
			$criteria->status = "Current";
//			$criteria->ismExtractedDate = "0000-00-00";
//			$thisrecord = $this->getEZDocs($criteria);
			if (count($thisrecord->collection)){
				$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_ISMExtractedDate=? ,ResidentialEZDocData_BGASISMExtractedDate=? WHERE ResidentialEZDocData_DocumentID=?";
				$sqlBinding="sss";
				$dateToday= date("Y-m-d");
				$sqlParams = array($dateToday,$dateToday,$docId);
			}else{
				$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_BGASISMExtractedDate=? WHERE ResidentialEZDocData_DocumentID=?";
				$sqlBinding="ss";
				$dateToday= date("Y-m-d");
				$sqlParams = array($dateToday,$docId);
			}
			$updateResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
			$bgasResponses["criteria"] = $criteria;
			$bgasResponses["data"][] = $updateResponse;
			$bgasResponses["sql"] = $sqlString.print_r($sqlParams,true);
		}
		return $bgasResponses;
	}
	public function bgasMeasureExtract($docIds){
		foreach ($docIds as $docId){
			$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_BGASExtractedDate=? WHERE ResidentialEZDocData_DocumentID=?";
			$sqlBinding="ss";
			$dateToday= date("Y-m-d");
			$sqlParams = array($dateToday,$docId);
			$updateResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
			$bgasResponses[] = $updateResponse;
		}
		return $bgasResponses;
	}
	
	public function markEFIExtractDate($docIds){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_EFIExtractedDate=? WHERE ResidentialEZDocData_DocumentID IN (".$docIds.")";
		$sqlBinding="s";
		$extractDate = date("Y-m-d");
		$sqlParams = array($extractDate);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
    public function getEZDocs($criteria){
        $sqlString = "SELECT 
			ResidentialEZDocData_ID as id,
			ResidentialEZDocData_DocumentID as documentId,
			ResidentialEZDocData_SiteID as siteId,
			ResidentialEZDocData_ProjectID as projectId,
			ResidentialEZDocData_CreatedDate as createdDate,
			ResidentialEZDocData_CreatedBy as createdBy,
			ResidentialEZDocData_Status as status,
			ResidentialEZDocData_CheckedOutStatus as checkedOutStatus,
			ResidentialEZDocData_LastUpdatedBy as lastUpdatedBy,
			ResidentialEZDocData_LastUpdatedTimeStamp as lastUpdatedTimeStamp,
			ResidentialEZDocData_DataByFieldName as dataByFieldName,
			ResidentialEZDocData_DataBySheet as dataBySheet,
			ResidentialEZDocData_DataByMeasure as dataByMeasure,
			ResidentialEZDocData_EFIFlagged as efiFlagged,
			ResidentialEZDocData_EFIExtractedDate as efiExtractedDate,
			ResidentialEZDocData_ISMExtractedDate as ismExtractedDate,
			ResidentialEZDocData_NoElectricISMs as noElectricIsms,
			ResidentialEZDocData_CreateCustomerDocumentsStatus as createCustomerDocumentsStatus,
			ResidentialEZDocData_CreateCustomerDocumentsStatusText as createCustomerDocumentsStatusText,
			ResidentialEZDocData_EZDocVersionDate as ezDocVersionDate,
			ResidentialEZDocData_BGASFlagged as bgasFlagged,
			ResidentialEZDocData_BGASISMExtractedDate as bgasISMExtractedDate,
			ResidentialEZDocData_BGASExtractedDate as bgasExtractedDate,
			ResidentialEZDocData_ContractorFlagged as contractorFlagged
			";	
		$fromTableClause = " FROM residential_ezdocdata WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->documentId){
			$filterString .= " AND ResidentialEZDocData_DocumentID=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->documentId;
		}
		//is the below line filtering for a recommendation? will adding 'or ResidentialEZDocData_EFIFlagged=0' pull all?
		if ($criteria->efiFlagged){
			$filterString .= " AND ResidentialEZDocData_EFIFlagged=1"; 
		}
		if ($criteria->bgasFlagged){
			$filterString .= " AND ResidentialEZDocData_BGASFlagged=1"; 
		}
		if ($criteria->status){
			$filterString .= " AND ResidentialEZDocData_Status=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->efiExtractedDate){
			$filterString .= " AND ResidentialEZDocData_EFIExtractedDate=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->efiExtractedDate;
		}
		if ($criteria->bgasExtractedDate){
			$filterString .= " AND ResidentialEZDocData_BGASExtractedDate=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->efiExtractedDate;
		}
		if ($criteria->ismExtractedDate){
			$filterString .= " AND ResidentialEZDocData_ISMExtractedDate=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->ismExtractedDate;
		}
		
		$orderBy = " ORDER BY ResidentialEZDocData_CreatedDate ASC";
        // apply search
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getResAuditFields failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
    private static $INSERT_RESSECHEDULEDATA_SQL = "
		INSERT INTO residential_scheduled
		(
			ResidentialScheduled_EnergySpecialist,
			ResidentialScheduled_Date,
			ResidentialScheduled_EndDate,
			ResidentialScheduled_SiteID,
			ResidentialScheduled_ProjectID,
			ResidentialScheduled_HESID,

			ResidentialScheduled_ElectricProvider,
			ResidentialScheduled_CustomerName,
			ResidentialScheduled_Email,
			ResidentialScheduled_Phone,
			ResidentialScheduled_Address,
			ResidentialScheduled_VisitType,
			ResidentialScheduled_AuditNotes
		) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,?,?
		) ON DUPLICATE KEY UPDATE 
			ResidentialScheduled_EnergySpecialist = ?,
			ResidentialScheduled_Date = ?,
			ResidentialScheduled_EndDate = ?,
			ResidentialScheduled_Email = ?,
			ResidentialScheduled_Phone = ?,
			ResidentialScheduled_Address = ?,
			ResidentialScheduled_Status = ?,
			ResidentialScheduled_VisitType = ?,
			ResidentialScheduled_AuditNotes = ?,
			ResidentialScheduled_CustomerName = ?,
			ResidentialScheduled_ElectricProvider = ?,
			ResidentialScheduled_HESID = ?
			
		";

    public function insertResScheduleData($record){
		$customerName = str_replace(array("'", "\n", "\t", "\r"),"",$record->customerName);
        $sqlString = self::$INSERT_RESSECHEDULEDATA_SQL;
        $sqlBinding = "sssssssssssssssssssssssss";
		$phone =($record->phone ? $record->phone : null);
		$auditNotes =($record->auditNotes ? $record->auditNotes : null);
        $sqlParam = array(
			$record->energySpecialist,
			$record->apptDate,
			$record->endDate,
			$record->siteId,
			$record->projectId,
			$record->hesId,

			$record->electricProvider,
			$customerName,
			$record->email,
			$phone,
			$record->address,
			$record->visitType,
			$auditNotes,
			$record->energySpecialist,
			$record->apptDate,
			$record->endDate,
			$record->email,
			$phone,
			$record->address,
			$record->status,
			$record->visitType,
			$auditNotes,
			$customerName,
			$record->electricProvider,
			$record->hesId
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
			//could possible have success but not affect any rows if no new information
			if ($insertResponse->error){
				trigger_error("ResidentialProvider.insertResScheduleData failed to insert: ".print_r($sqlParam,true).$insertResponse->error, E_USER_ERROR);
			}
        }
        return $insertResponse;
    }
	
	
    public function getScheduledData($criteria){
		//print_pre($criteria);
		//ERCW and Building Category added here?
        $sqlString = "SELECT 
			ResidentialScheduled_ID as id,
			ResidentialScheduled_EnergySpecialist as energySpecialist,
			ResidentialScheduled_Date as apptDate,
			ResidentialScheduled_EndDate as endDate,
			ResidentialScheduled_SiteID as siteId,
			ResidentialScheduled_ProjectID as projectId,
			ResidentialScheduled_HESID as hesId,

			ResidentialScheduled_ElectricProvider as electricProvider,
			ResidentialScheduled_CustomerName as customerName,
			ResidentialScheduled_Email as email,
			ResidentialScheduled_Phone as phone,
			ResidentialScheduled_Address as address,
			ResidentialScheduled_VisitType as visitType,
			ResidentialScheduled_AuditNotes as auditNotes,
			ResidentialScheduled_EZDocUploadedDate as ezDocUploadedDate,
			ResidentialScheduled_Status as status,
			ResidentialScheduled_StatusDate as statusDate
			";	
		$fromTableClause = " FROM residential_scheduled WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if (count($criteria->electricProviderNot)){
			$filterString .= " AND (ResidentialScheduled_ElectricProvider IS NULL"; 
			if (count($criteria->electricProviderNot) > 1){
				$filterString .= " OR ("; 
				$thisFilter = array();
				foreach ($criteria->electricProviderNot as $eprovider){
					$thisFilter[] = "ResidentialScheduled_ElectricProvider !=?";
					$filterBinding .= "s";
					$filterParams[] = $eprovider;
				}
				$filterString .= implode(" AND ",$thisFilter); 
				$filterString .= ")"; 

			}else{
				foreach ($criteria->electricProviderNot as $eprovider){
					$filterString .= " OR ResidentialScheduled_ElectricProvider !=?"; 
					$filterBinding .= "s";
					$filterParams[] = $eprovider;
				}
			}
			$filterString .= ")"; 
		}
		if ($criteria->apptDateOn){
			$filterString .= " AND ResidentialScheduled_Date BETWEEN ? AND ?"; 
			$filterBinding .= "ss";
			$filterParams[] = $criteria->apptDateOn." 00:00:00";
			$filterParams[] = $criteria->apptDateOn." 23:59:59";
		}
		if ($criteria->apptDateBefore){
			$filterString .= " AND ResidentialScheduled_Date < ?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->apptDateBefore;
		}
		if ($criteria->apptDate){
			$filterString .= " AND ResidentialScheduled_Date > ?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->apptDate;
		}
		
		if ($criteria->endDate){
			$filterString .= " AND ResidentialScheduled_Date BETWEEN ? AND ?"; 
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}
		if ($criteria->uploadedDate){
			$filterString .= " AND ResidentialScheduled_EZDocUploadedDate=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->uploadedDate;
		}
		if ($criteria->siteId){
			$filterString .= " AND ResidentialScheduled_SiteID=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->siteId;
		}
		if ($criteria->projectId){
			$filterString .= " AND ResidentialScheduled_ProjectID=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->projectId;
		}
		if ($criteria->energySpecialist){
			$es = explode(",",$criteria->energySpecialist);
			if (count($es) > 1){
				$filterString .= " AND ("; 
				foreach ($es as $EnergySpecialist){
					$filterString .= "ResidentialScheduled_EnergySpecialist=? OR "; 
					$filterBinding .= "s";
					$filterParams[] = $EnergySpecialist;
				}
				$filterString = rtrim($filterString," OR "); 
				$filterString .= ")"; 
			}else{
				$filterString .= " AND ResidentialScheduled_EnergySpecialist=?"; 
				$filterBinding .= "s";
				$filterParams[] = $es[0];
			}
		}
		if ($criteria->energySpecialistNot){
			$filterString .= " AND ResidentialScheduled_EnergySpecialist!=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->energySpecialistNot;
		}
		if ($criteria->visitType){
			$filterString .= " AND ResidentialScheduled_VisitType=?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->visitType;
		}
		if ($criteria->visitTypes){
			$filterString .= " AND (";
			$thisFilter = array();
			foreach ($criteria->visitTypes as $visitType){
				$thisFilter[] = "ResidentialScheduled_VisitType=?"; 
				$filterBinding .= "s";
				$filterParams[] = $visitType;
			}
			$filterString .= implode(" OR ",$thisFilter);
			$filterString .= ")";

		}
		if ($criteria->city){
			$filterString .= " AND ResidentialScheduled_Address like '%".$criteria->city."%'"; 
		}
		if ($criteria->status){
			$filterString .= " AND ResidentialScheduled_Status =?"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->statusNot){
			$filterString .= " AND (ResidentialScheduled_Status IS NULL OR ResidentialScheduled_Status !=?)"; 
			$filterBinding .= "s";
			$filterParams[] = $criteria->statusNot;
		}
		$orderBy = " ORDER BY ResidentialScheduled_EnergySpecialist, ResidentialScheduled_Date ASC";
        // apply search
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString."<br>";
		//print_pre($filterParams);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getScheduledData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }

	public function updateScheduledUploadedDate($uploadedDate,$scheduleId){
		$sqlString = "UPDATE residential_scheduled SET ResidentialScheduled_EZDocUploadedDate=? WHERE ResidentialScheduled_ID=?";
		$sqlBinding="si";
		$dateToday= date("Y-m-d");
		$sqlParams = array($dateToday,$scheduleId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	public function updateTravelScheduleData($recordId){
		$sqlString = "UPDATE residential_scheduled SET ResidentialScheduled_SiteID=?,ResidentialScheduled_ProjectID=?  WHERE ResidentialScheduled_ID=?";
		$sqlBinding="ssi";
		$siteId = "TravelID".$recordId;
		$projectId = "TravelID".$recordId;
		$sqlParams = array($siteId,$projectId,$recordId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
    private static $INSERT_EZDOCERROR_SQL = "
		INSERT INTO residential_ezdocuploaderror
		(
			ResidentialEZDocUploadError_DocumentID,
			ResidentialEZDocUploadError_Message,
			ResidentialEZDocUploadError_UploadedBy
		) VALUES (
			?,?,?
		)
		";

    public function insertEZDocUploadError($docId,$message,$adminId){
        $sqlString = self::$INSERT_EZDOCERROR_SQL;
        $sqlBinding = "sss";
        $sqlParam = array($docId,$message,$adminId);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.insertEZDocUploadError failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	public function getEZDocUploadErrors($criteria){
        $sqlString = "SELECT * FROM residential_ezdocuploaderror WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		$orderBy = "";
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getEZDocUploadErrors failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}
	
	public function cancelApt($aptId){
		$sqlString = "UPDATE residential_scheduled SET ResidentialScheduled_Status='Cancelled' WHERE ResidentialScheduled_ID=?";
		$sqlBinding="i";
		$sqlParams = array($aptId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	public function cancelSharePointApt($criteria){
		$sqlString = "UPDATE residential_scheduled SET ResidentialScheduled_Status='Cancelled', ResidentialScheduled_StatusDate=? WHERE ResidentialScheduled_SiteID=? AND ResidentialScheduled_ProjectID=?";
		$thisDate = date("Y-m-d");
		$sqlBinding="sss";
		$sqlParams = array($thisDate,$criteria->siteId,$criteria->projectId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	public function confirmApt($aptId){
		$sqlString = "UPDATE residential_scheduled SET ResidentialScheduled_Status='Confirmed', ResidentialScheduled_StatusDate=? WHERE ResidentialScheduled_ID=?";
		$sqlBinding="si";
		$thisDate = date("Y-m-d");
		$sqlParams = array($thisDate,$aptId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	public function mergeRecords($record){
		$sqlString = "UPDATE residential_ezdocdata SET 
			ResidentialEZDocData_DocumentID=?, 
			ResidentialEZDocData_Status='Revised' 
			WHERE 
			ResidentialEZDocData_SiteID=? AND ResidentialEZDocData_ProjectID=? AND ResidentialEZDocData_ID!=?";
		$sqlBinding="sssi";
		$sqlParams = array($record->docId,$record->siteId,$record->projectId,$record->lastId);
		
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
		//return $sqlString.print_r($sqlParams,true);
	}
	
    private static $INSERT_BLOCKED_SQL = "
		INSERT INTO residential_scheduledblocked
		(
			ResidentialScheduledBlocked_ES,
			ResidentialScheduledBlocked_StartTimeStamp,
			ResidentialScheduledBlocked_EndTimeStamp,
			ResidentialScheduledBlocked_Reason,
			ResidentialScheduledBlocked_Note,
			ResidentialScheduledBlocked_CreatedBy
		) VALUES (
			?,?,?,?,?,?
		)
		";

    public function addScheduleBlock($record,$adminId){
        $sqlString = self::$INSERT_BLOCKED_SQL;
        $sqlBinding = "ssssss";
        $sqlParam = array(
			$record->es,
			$record->startTimeStamp,
			$record->endTimeStamp,
			$record->reason,
			$record->note,
			$adminId);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.insertScheduleBlock failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
	public function getScheduleBlocked($criteria){
        $sqlString = "SELECT * FROM residential_scheduledblocked WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->future){
			$filterString .= " AND ResidentialScheduledBlocked_StartTimeStamp >= ?";
			$filterBinding .= "s";
			$filterParams[] = date("Y-m-d");
		}
		if ($criteria->status){
			$filterString .= " AND ResidentialScheduledBlocked_Status = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->orderBy){
			$orderBy = $criteria->orderBy;
		}
		$orderBy = " ORDER BY ResidentialScheduledBlocked_StartTimeStamp";
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getScheduleBlocked failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}
	public function cancelBlocked($blockedId){
		$sqlString = "UPDATE residential_scheduledblocked SET ResidentialScheduledBlocked_Status='Cancelled' WHERE ResidentialScheduledBlocked_ID=?";
		$sqlBinding="i";
		$sqlParams = array($blockedId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}

	public function removeBgasExtractedDate($docId){
		$sqlString = "UPDATE residential_ezdocdata SET ResidentialEZDocData_BGASExtractedDate='0000-00-00' WHERE ResidentialEZDocData_DocumentID=?";
		$sqlBinding="s";
		$sqlParams = array($docId);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	public function getLeads($criteria){
        $sqlString = "SELECT * FROM residential_leads WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->future){
			$filterString .= " AND ResidentialScheduledBlocked_StartTimeStamp >= ?";
			$filterBinding .= "s";
			$filterParams[] = date("Y-m-d");
		}
		if ($criteria->status){
			$filterString .= " AND ResidentialScheduledBlocked_Status = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->orderBy){
			$orderBy = $criteria->orderBy;
		}
		$orderBy = " ORDER BY createdTimeStamp";
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getLeads failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}
    public function addLeads($records,$adminId){
		$sqlStarter = "INSERT INTO residential_leads (";
		$sqlEnder = ") ON DUPLICATE KEY UPDATE UpdatedLastBy = ?";
		$successCount = 0;
		$duplicateCount = 0;
		foreach ($records as $record){
			$fields = array();
			$vals = array();
			$Qs = array();
			$sqlBinding = "";
			foreach ($record as $key=>$val){
				if ($key == "floor_area_residential-surveys" || $key == "year_built_residential-surveys"){$binding = "i";}else{$binding = "s";}
				if ($key == "landlord_zip_enrollments" || $key == "postal_code_people" || $key == "postal_code_premises"){
					if (strlen($val) == 4){$val = "0".$val;}
				}
				$fields[] = "`".$key."`";
				$vals[] = $val;
				$Qs[] = "?";
				$sqlBinding .= $binding;
			}
			$sqlBinding .= "s";
			$vals[] = $adminId."_".date("Y-m-d"); //add admin for duplicate key update
			$sqlFields = implode(",",$fields);
			$sqlQs = implode(",",$Qs);
			
			$sqlString  = $sqlStarter.$sqlFields.") VALUES (".$sqlQs.$sqlEnder;
			$sqlParam = $vals;
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$successCount++;
			} else {
				if ($insertResponse->success){
					$duplicateCount++;
				}else{
					$errorMessage = $insertResponse->error;
					trigger_error("ResidentialProvider.addLeads failed to insert: ".$insertResponse->error, E_USER_ERROR);
				}
			}
			$responses[] = $insertResponse;
			$counts["success"] = $successCount;
			$counts["duplicates"] = $duplicateCount;
			$counts["error"] = $errorMessage;
			
		}
		return $counts;
    }
	public function leadNotInterested($id){
		$sqlString = "UPDATE residential_leads SET SiteId='Not Interested', SiteIdAdded=? WHERE id=?";
		$sqlBinding="si";
		$todaysDate = date("Y-m-d");
		$sqlParams = array($todaysDate,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}

    private static $INSERT_AVAILABLE_SQL = "
		INSERT INTO residential_scheduledavailable
		(
			ResidentialScheduledAvailable_ES,
			ResidentialScheduledAvailable_Days,
			ResidentialScheduledAvailable_Date,
			ResidentialScheduledAvailable_StartTime,
			ResidentialScheduledAvailable_EndTime,
			ResidentialScheduledAvailable_TotalHours,
			ResidentialScheduledAvailable_Types,
			ResidentialScheduledAvailable_CreatedBy
		) VALUES (
			?,?,?,?,?,?,?,?
		)
		";

    public function addAvailability($record,$adminId){
        $sqlString = self::$INSERT_AVAILABLE_SQL;
        $sqlBinding = "ssssssss";
		$days = implode(",",$record->days);
		$types = implode(",",$record->types);
        $sqlParam = array(
			$record->es,
			$days,
			$record->adate,
			$record->startTime,
			$record->endTime,
			$record->totalHours,
			$types,
			$adminId);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.addAvailability failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
	public function getScheduleAvailable($criteria){
        $sqlString = "SELECT * FROM residential_scheduledavailable WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->status){
			$filterString .= " AND ResidentialScheduledAvailable_Status = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->orderBy){
			$orderBy = $criteria->orderBy;
		}
		$orderBy = " ORDER BY ResidentialScheduledAvailable_ES";
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getScheduleAvailable failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}
	public function availabilityStatus($status,$id){
		$sqlString = "UPDATE residential_scheduledavailable SET ResidentialScheduledAvailable_Status=? WHERE ResidentialScheduledAvailable_ID=?";
		$sqlBinding="si";
		$sqlParams = array($status,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
    private static $UPDATE_AVAILABLE_SQL = "
		UPDATE residential_scheduledavailable
		SET
			ResidentialScheduledAvailable_Days = ?,
			ResidentialScheduledAvailable_Date = ?,
			ResidentialScheduledAvailable_StartTime = ?,
			ResidentialScheduledAvailable_EndTime = ?,
			ResidentialScheduledAvailable_TotalHours = ?,
			ResidentialScheduledAvailable_Types = ?,
			ResidentialScheduledAvailable_Status = ?,
			ResidentialScheduledAvailable_LastUpdatedBy = ?
		WHERE ResidentialScheduledAvailable_ID = ?
		";
	
	public function updateAvailability($record,$adminId){
        $sqlString = self::$UPDATE_AVAILABLE_SQL;
        $sqlBinding = "ssssssssi";
		$days = implode(",",$record->days);
		$types = implode(",",$record->types);
        $sqlParams = array(
			$days,
			$record->adate,
			$record->startTime,
			$record->endTime,
			$record->totalHours,
			$types,
			$record->status,
			$adminId,
			$record->id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	public function getPiggyBacks($criteria){
        $sqlString = "SELECT * FROM residential_piggybacks WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->siteId){
			$filterString .= " AND SITEID = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->siteId;
		}
		$orderBy = "";
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getPiggyBacks failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}
	
    private static $INSERT_PIGGYBACK_SQL = "
		INSERT INTO residential_piggybacks
		(
			SITEID,
			EVENTDATE,
			CONTRACT
		) VALUES (
			?,?,?
		)
		";
	public function addPiggyBack($siteId,$thisDate,$contract){
		//echo $siteId.",".$thisDate.",".$contract."<br>";
        $sqlString = self::$INSERT_PIGGYBACK_SQL;
        $sqlBinding = "sss";
        $sqlParam = array(
			$siteId,
			$thisDate,
			$contract);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.addPiggyBack failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
			
	}
	public function pdfFormInsert($emailedTo,$recordType,$record,$adminId){
		$sqlString = "INSERT INTO pdf_form_data
		(
			formType,
			fieldData,
			createdBy,
			emailedTo
		)	VALUES
		(
			?,?,?,?
		)";
		$sqlBinding = "ssss";
		$sqlParam =  array($recordType,$record,$adminId,$emailedTo);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
           // $record->id = $insertResponse->insertedId;
           // $insertResponse->record = $record;
        } else {
            trigger_error("ResidentialProvider.addPiggyBack failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
	}

		/*
	function getsf270data($criteria){
	   $sql = "SELECT * FROM sf270 WHERE 1=1";
	   [$bindings = "", $params = array(), $filter = ""]

	 if ($criteria->periodStartDate){
	   $filter .= " AND periodStartDate = ?";
	   $bindings .= "s";
	   $params[] = $criteria->periodStartDate;

	  }
	 $sqlStmt  = $sql.$filter;
	Then run query response like other examples.
	}	
	*/
	public function getSF270Data($criteria){
        $sqlString = "SELECT * FROM sf270 WHERE 1=1";
		$filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->periodEndDate){
			$filterString .= " AND PeriodStartDate BETWEEN ? AND ?";
			$filterBinding .= "ss";
			$filterParams[] = $criteria->periodStartDate;
			$filterParams[] = $criteria->periodEndDate;
		}
		if ($criteria->source){

			$filterString .= " AND Source = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->source;
		}
		$orderBy = "";
		$pageSqlString = $sqlString.$filterString.$orderBy;
		// echo $pageSqlString;
		// print_pre($criteria);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("ResidentialProvider.getsf270data failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
		
	}


	private static $INSERT_SF270_DATA = "
		INSERT INTO sf270
		(
			AccountType,
			PeriodStartDate,
			PaymentsAmount,
			ExpenseAmount,
			Source
		) VALUES (
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE 
			PaymentsAmount = ?,
			ExpenseAmount = ?
		";

	public function insertSF270Data($record,$source){
		$sqlString = self::$INSERT_SF270_DATA;
        $sqlBinding = "ssddsdd";
        $startDate = date("Y-m-d",strtotime($record["StartDate"]));
        $personell = str_replace(",","", $record["Personell"]);
        $expenses = str_replace(",","", $record["Expenses"]);
        $sqlParam = array(
			$record["SF270Name"],
			$startDate,
			$personell,
			$expenses,
			$source,
			$personell,
			$expenses	
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;	
        } else {
			//could possible have success but not affect any rows if no new information
			if ($insertResponse->error){
				trigger_error("ResidentialProvider.insertSF270Data failed to insert: ".print_r($sqlParam,true).$insertResponse->error, E_USER_ERROR);
			}
        }

        return $insertResponse;



	}
	
}