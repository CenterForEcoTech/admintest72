<?php
include_once("ContentProvider.php");

class MerchantImageProvider {
    /* @var mysqli */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    //
    // Pagination queries
    //

    private static $PAGINATED_SELECT_COLUMNS = "
      SELECT
            m.id,
            m.merchant_info_id as merchantId,
            m.custom_caption as imageCaption,
            m.price_range_low as priceRangeLow,
            m.price_range_high as priceRangeHigh,
            m.is_one_price as isOnePrice,
            m.status,
            m.external_link_url as externalLinkUrl,
            m.expiration_date as expirationDate,
            m.description_markdown as descriptionMarkDownSource,
            m.description_html as description,
            m.sort_order as sortOrder,
            c.id as imageId,
            c.file_name as fileName,
            c.folder_url as folderUrl,
            c.file_size as fileSize,
            c.file_type as fileType,
            c.width,
            c.height,
            c.error,
            c.title,
            c.description as imageDescription,
            c.keywords,
            c.status as imageStatus";

    private static $INCLUDE_MERCHANT_METADATA_COLUMNS = "
            ,mi.MerchantInfo_Name as merchantName,
            mi.MerchantInfo_Address as address,
            mi.MerchantInfo_City as city,
            mi.MerchantInfo_State as state,
            mi.MerchantInfo_Zip as zip,
            mi.MerchantInfo_Website as website";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM cms_image_library c
        JOIN merchant_images m ON m.cms_image_library_id = c.id AND c.status <> 'DELETED'
        JOIN merchant_info mi ON m.merchant_info_id = mi.MerchantInfo_ID
        WHERE 1 = 1";

    private static $FILTER_ON_KEYWORD = "
        AND (
            c.file_name LIKE ?
            OR c.title LIKE ?
            OR c.description LIKE ?
            OR c.keywords LIKE ?
            OR m.description_markdown LIKE ?
            OR m.custom_caption LIKE ?
            OR mi.MerchantInfo_Name LIKE ?
            OR mi.MerchantInfo_Address LIKE ?
            OR mi.MerchantInfo_City LIKE ?
            OR mi.MerchantInfo_State LIKE ?
            OR mi.MerchantInfo_Zip LIKE ?
            OR mi.MerchantInfo_Description LIKE ?
        )";

    private static $FILTER_ON_ID = "
        AND m.id = ?";

    private static $FILTER_ON_MERCHANT_ID = "
        AND m.merchant_info_id = ?";

    private static $FILTER_ON_CATALOG_ID = "
        AND m.merchant_info_id IN (
            SELECT mcm.merchant_info_id
            FROM merchant_catalog_merchant mcm
            WHERE mcm.merchant_catalog_id = ?
            AND mcm.status = 'ACTIVE'
        )";

    public function get(MerchantImageCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        if ($criteria->includeMerchantMetadata){
            $paginationSelectString .= self::$INCLUDE_MERCHANT_METADATA_COLUMNS;
        }

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        if (is_numeric($criteria->merchantId)){
            $filterString .= self::$FILTER_ON_MERCHANT_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->merchantId;
        }

        if ($criteria->catalogId && is_numeric($criteria->catalogId)){
            $filterString .= self::$FILTER_ON_CATALOG_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->catalogId;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $merchantImageRecord = $this->mapTo((object)$row);
                        if ($criteria->isVisible){
                            if ($merchantImageRecord->isVisible()){
                                $result->collection[] = $merchantImageRecord;
                            }
                        } else {
                            $result->collection[] = $merchantImageRecord;
                        }
                    }
                } else {
                    trigger_error("MerchantImageProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("MerchantImageProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY m.sort_order, m.id desc";

    private static $RANDOM_ORDER_BY = " ORDER BY rand()";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->randomize){
            $orderBy = self::$RANDOM_ORDER_BY;
        } else {
            if (count($criteria->multiColumnSort)){
                $orderByStrings = array();
                foreach($criteria->multiColumnSort as $multiColumnSort){
                    /* @var $multiColumnSort MultiColumnSort */
                    $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                    switch ($multiColumnSort->column){
                        case "image":
                            $orderByStrings[] = "c.title ".$sortOrder;
                            break;
                        case "caption":
                            $orderByStrings[] = "c.title ".$sortOrder;
                            break;
                        case "status":
                            $orderByStrings[] = "m.status ".$sortOrder;
                            break;
                        case "sortOrder":
                            $orderByStrings[] = "m.sort_order ".$sortOrder;
                            break;
                    }
                }
                $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
            }
        }
        return $orderBy;
    }

    private function mapTo($dbRow){
        $merchantImageRecord = new MerchantImageRecord();
        $imageRecord = new ImageRecord();
        $merchantImageRecord->imageRecord = $imageRecord;

        $merchantImageRecord->id = $dbRow->id;
        $merchantImageRecord->imageCaption = $dbRow->imageCaption;
        $merchantImageRecord->priceRangeLow = $dbRow->priceRangeLow;
        $merchantImageRecord->priceRangeHigh = $dbRow->priceRangeHigh;
        $merchantImageRecord->isOnePrice = $dbRow->isOnePrice;
        $merchantImageRecord->descriptionMarkDownSource = $dbRow->descriptionMarkDownSource;
        $merchantImageRecord->description = $dbRow->description;
        $merchantImageRecord->status = $dbRow->status;
        $merchantImageRecord->setExternalLinkUrl($dbRow->externalLinkUrl);
        $merchantImageRecord->expirationDate = $dbRow->expirationDate;
        $merchantImageRecord->merchantId = $dbRow->merchantId;
        $merchantImageRecord->sortOrder = $dbRow->sortOrder;

        // merchant meta columns
        $merchantImageRecord->merchantName = $dbRow->merchantName;
        $merchantImageRecord->address = $dbRow->address;
        $merchantImageRecord->city = $dbRow->city;
        $merchantImageRecord->state = $dbRow->state;
        $merchantImageRecord->zip = $dbRow->zip;
        $merchantImageRecord->setWebsite($dbRow->website);

        // image record
        $imageRecord->id = $dbRow->imageId;
        $imageRecord->fileName = $dbRow->fileName;
        $imageRecord->folderUrl = $dbRow->folderUrl;
        $imageRecord->fileType = $dbRow->fileType;
        $imageRecord->width = $dbRow->width;
        $imageRecord->height = $dbRow->height;
        $imageRecord->error = $dbRow->error;
        $imageRecord->title = $dbRow->title;
        $imageRecord->description = $dbRow->imageDescription;
        $imageRecord->keywords = $dbRow->keywords;
        $imageRecord->status = $dbRow->imageStatus;
        return $merchantImageRecord;
    }
}

class MerchantImageManager extends MerchantImageProvider{
    /* @var ImageLibraryProvider */
    private $imageProvider;

    public function __construct(mysqli $mysqli, ImageLibraryProvider $imageProvider){
        parent::__construct($mysqli);
        $this->imageProvider = $imageProvider;
    }

    private static $INSERT_USER_DATA_SQL = "
        INSERT INTO merchant_images
        (
            custom_caption,
            price_range_low,
            price_range_high,
            is_one_price,
            external_link_url,

            expiration_date,
            description_markdown,
            description_html,
            created_by_member_id,
            merchant_info_id,

            cms_image_library_id,
            status
        )
        SELECT
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?
        FROM dual";

    private static $INSERT_USER_DATA_BINDING = "sddissssiiis";

    public function insertUserData(MerchantImageRecord $record, $memberId){
        $imageRecord = $record->imageRecord;
        $response = $this->imageProvider->updateUserData($imageRecord->id, $imageRecord->title, $imageRecord->description, $imageRecord->keywords);
        if ($response->success){
            $sqlString = self::$INSERT_USER_DATA_SQL;
            $sqlBinding = self::$INSERT_USER_DATA_BINDING;
            $sqlParams = array(
                $record->imageCaption,
                $record->priceRangeLow ? $record->priceRangeLow : 0.00,
                $record->priceRangeHigh ? $record->priceRangeHigh : 0.00,
                $record->isOnePrice ? 1 : 0,
                $record->externalLinkUrl,

                strtotime($record->expirationDate) ? date("Y-m-d", strtotime($record->expirationDate)) : null,
                $record->descriptionMarkDownSource,
                $record->description,
                $memberId,
                $record->merchantId,

                $imageRecord->id,
                $record->status
            );
            $insertResponse = MySqliHelper::executeWithLogging("MerchantImageManager.insertUserData SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
            if ($insertResponse->success){
                $record->id = $insertResponse->insertedId;
            }
            return $insertResponse;
        }
        return $response;
    }

    private static $UPDATE_USER_DATA_SQL = "
        UPDATE merchant_images
        SET custom_caption = ?,
            price_range_low = ?,
            price_range_high = ?,
            is_one_price = ?,
            status = ?,

            external_link_url = ?,
            expiration_date = ?,
            description_markdown = ?,
            description_html = ?,
            updated_date = current_date,

            updated_by_member_id = ?
        WHERE id = ?
        AND merchant_info_id = ?";

    private static $UPDATE_USER_DATA_BINDING = "sddisssssiii";

    public function updateUserData(MerchantImageRecord $record, $memberId){
        $imageRecord = $record->imageRecord;
        $response = $this->imageProvider->updateUserData($imageRecord->id, $imageRecord->title, $imageRecord->description, $imageRecord->keywords);
        if ($response->success){
            $sqlString = self::$UPDATE_USER_DATA_SQL;
            $sqlBinding = self::$UPDATE_USER_DATA_BINDING;
            $sqlParams = array(
                $record->imageCaption,
                $record->priceRangeLow ? $record->priceRangeLow : 0.00,
                $record->priceRangeHigh ? $record->priceRangeHigh : 0.00,
                $record->isOnePrice ? 1 : 0,
                $record->status,

                $record->externalLinkUrl,
                strtotime($record->expirationDate) ? date("Y-m-d", strtotime($record->expirationDate)) : null,
                $record->descriptionMarkDownSource,
                $record->description,

                $memberId,
                $record->id,
                $record->merchantId
            );
            $updateResponse = MySqliHelper::executeWithLogging("MerchantImageManager.updateUserData SQL error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
            return $updateResponse;
        }
        return $response;
    }

    private static $SET_SORT_ORDER_SQL = "
        UPDATE merchant_images
        SET sort_order = ?
        WHERE id = ?
        AND merchant_info_id = ?";

    public function setSortOrder(MerchantImageRecord $record){
        $sqlString = self::$SET_SORT_ORDER_SQL;
        $sqlBinding = "iii";
        $sqlParams = array(
            $record->sortOrder,
            $record->id,
            $record->merchantId
        );
        $response = MySqliHelper::executeWithLogging("MerchantImageManager.setSortOrder SQL Error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $response;
    }
}