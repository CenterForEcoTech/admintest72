<?php
/**
 * This file contains read-only providers to service the monitoring and logs part of the mayor console.
 */


class LoggingCriteriaBase extends PaginationCriteria{
    public $date = null;
}
class EventLoggingCriteria extends LoggingCriteriaBase{
    public $event_id = null;
}

class EventLoggingProvider {
    /** @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            EventLog_ID as id,
            EventLog_EventID as event_id,
            EventLog_Action as message,
            EventLog_MemberID as member_id,
            EventLog_AdminID as admin_id,
            EventLog_TimeStamp as timestamp";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_log
        WHERE 1=1";

    private static $FILTER_ON_DATE = "
        AND EventLog_TimeStamp > ?";

    private static $FILTER_ON_EVENT_ID = "
        AND EventLog_EventID = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            EventLog_Action LIKE ?
            OR EventLog_ID LIKE ?
            OR EventLog_EventID LIKE ?
        )";

    /**
     * @param EventLoggingCriteria $criteria
     * @return PagedResult
     */
    public function get(EventLoggingCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->event_id){
            $filterString .= self::$FILTER_ON_EVENT_ID;
            $filterBinding .= "s"; // treat as string
            $filterParams[] = $criteria->event_id;
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $result->collection[] = (object)$row;
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY EventLog_ID desc";

    private static $ORDER_BY_ID = " ORDER BY EventLog_ID";

    private static $ORDER_BY_EVENT = " ORDER BY EventLog_EventID";

    private static $ORDER_BY_DATE = " ORDER BY EventLog_TimeStamp";

    private static $ORDER_BY_MESSAGE = " ORDER BY EventLog_Action";

    private static $ORDER_BY_MEMBER = " ORDER BY EventLog_MemberID";

    private static $ORDER_BY_ADMIN = " ORDER BY EventLog_AdminID";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "event_id":
                    $orderBy = self::$ORDER_BY_EVENT." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "message":
                    $orderBy = self::$ORDER_BY_MESSAGE." ".$sortOrder;
                    break;
                case "member_id":
                    $orderBy = self::$ORDER_BY_MEMBER." ".$sortOrder;
                    break;
                case "admin_id":
                    $orderBy = self::$ORDER_BY_ADMIN." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}


class AdminLoggingCriteria extends LoggingCriteriaBase{
    public $page = null;
}

class AdminLoggingProvider {
    /** @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            AdminErrorLog_ID as id,
            AdminErrorLog_Message as message,
            AdminErrorLog_Page as page,
            AdminErrorLog_TimeStamp as timestamp";

    private static $FROM_TABLE_CLAUSE = "
        FROM admin_errorlog
        WHERE 1=1";

    private static $FILTER_ON_DATE = "
        AND AdminErrorLog_TimeStamp > ?";

    private static $FILTER_ON_PAGE = "
        AND AdminErrorLog_Page LIKE ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            AdminErrorLog_Message LIKE ?
            OR AdminErrorLog_Page LIKE ?
        )";

    /**
     * @param AdminLoggingCriteria $criteria
     * @return PagedResult
     */
    public function get(AdminLoggingCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->page){
            $filterString .= self::$FILTER_ON_PAGE;
            $filterBinding .= "s"; // treat as string
            $filterParams[] = $criteria->page;
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $result->collection[] = (object)$row;
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY AdminErrorLog_ID desc";

    private static $ORDER_BY_ID = " ORDER BY AdminErrorLog_ID";

    private static $ORDER_BY_PAGE = " ORDER BY AdminErrorLog_Page";

    private static $ORDER_BY_DATE = " ORDER BY AdminErrorLog_TimeStamp";

    private static $ORDER_BY_MESSAGE = " ORDER BY AdminErrorLog_Message";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "page":
                    $orderBy = self::$ORDER_BY_PAGE." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "message":
                    $orderBy = self::$ORDER_BY_MESSAGE." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}




class EmailLoggingCriteria extends LoggingCriteriaBase{
    public $status = null;
}

class EmailLoggingProvider {
    /** @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            EmailHistory_ID as id,
            EmailHistory_Date as date,
            EmailHistory_MemberID as member_id,
            EmailHistory_From as email_from,
            EmailHistory_To as email_to,
            EmailHistory_Subject as subject,
            EmailHistory_Message as message,
            EmailHistory_Status as status,
            EmailHistory_Date as timestamp,
            EmailHistory_Errors as errors,
            EmailHistory_UpdatedDate as updated";

    private static $FROM_TABLE_CLAUSE = "
        FROM email_history
        WHERE 1=1";

    private static $FILTER_ON_DATE = "
        AND EmailHistory_Date > ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            EmailHistory_Message LIKE ?
            OR EmailHistory_From LIKE ?
            OR EmailHistory_To LIKE ?
            OR EmailHistory_Subject LIKE ?
            OR EmailHistory_Status LIKE ?
            OR EmailHistory_Errors LIKE ?
        )";

    /**
     * @param AdminLoggingCriteria $criteria
     * @return PagedResult
     */
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $result->collection[] = (object)$row;
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY EmailHistory_ID desc";

    private static $ORDER_BY_ID = " ORDER BY EmailHistory_ID";

    private static $ORDER_BY_DATE = " ORDER BY EmailHistory_Date";

    private static $ORDER_BY_MEMBER = " ORDER BY EmailHistory_MemberID";

    private static $ORDER_BY_FROM = " ORDER BY EmailHistory_From";

    private static $ORDER_BY_TO = " ORDER BY EmailHistory_To";

    private static $ORDER_BY_SUBJECT = " ORDER BY EmailHistory_Subject";

    private static $ORDER_BY_MESSAGE = " ORDER BY EmailHistory_Message";

    private static $ORDER_BY_STATUS = " ORDER BY EmailHistory_Status";

    private static $ORDER_BY_ERRORS = " ORDER BY EmailHistory_Errors";

    private static $ORDER_BY_UPDATED = " ORDER BY EmailHistory_UpdatedDate";

    private function getOrderByClause(ErrorLoggingCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "member)id":
                    $orderBy = self::$ORDER_BY_MEMBER." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "message":
                    $orderBy = self::$ORDER_BY_MESSAGE." ".$sortOrder;
                    break;
                case "email_from":
                    $orderBy = self::$ORDER_BY_FROM." ".$sortOrder;
                    break;
                case "email_to":
                    $orderBy = self::$ORDER_BY_TO." ".$sortOrder;
                    break;
                case "subject":
                    $orderBy = self::$ORDER_BY_SUBJECT." ".$sortOrder;
                    break;
                case "status":
                    $orderBy = self::$ORDER_BY_STATUS." ".$sortOrder;
                    break;
                case "errors":
                    $orderBy = self::$ORDER_BY_ERRORS." ".$sortOrder;
                    break;
                case "updated":
                    $orderBy = self::$ORDER_BY_UPDATED." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}

class LoginHistoryCriteria extends LoggingCriteriaBase{

}

class LoginHistoryProvider {
    /** @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    /**
     * @param LoginHistoryCriteria $criteria
     * @return PagedResult
     */
    public function get(LoginHistoryCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter; //echo $pageSqlString; var_dump($filterParams);die();
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $result->collection[] = (object)$row;
        }

        return $result;
    }

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "member)id":
                    $orderBy = self::$ORDER_BY_MEMBER." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }

    private static $SELECT_COLUMNS = "
        SELECT lh.LoginHistory_ID as id,
            lh.LoginHistory_MembersID as member_id,
            lh.LoginHistory_IP as ip,
            lh.LoginHistory_ImpersonatedByAdminID as admin_id,
            lh.LoginHistory_TimeStamp as timestamp,
            m.Members_Email as contact_email,
            m.Members_Phone as contact_phone,
            CONCAT(m.Members_FirstName, ' ', m.Members_LastName) as contact_name,
            m.Members_Timestamp as create_timestamp,
            CASE
                WHEN m.Members_TypeID = 1 THEN 'Individual'
                WHEN m.Members_TypeID = 2 THEN 'Merchant'
                WHEN m.Members_TypeID = 3 THEN 'Organization'
                ELSE 'Unknown'
            END as member_type,
            IF (m.Members_TypeID = 2, mi.MerchantInfo_Name, o.OrganizationInfo_Name) as name,
            IF (m.Members_TypeID = 2, mi.MerchantInfo_City, o.OrganizationInfo_City) as city,
            IF (m.Members_TypeID = 2, mi.MerchantInfo_State, o.OrganizationInfo_State) as state,
            IF (m.Members_TypeID = 2, mi.MerchantInfo_Zip, o.OrganizationInfo_Zip) as zip,
            IF (m.Members_TypeID = 2, mi.MerchantInfo_Phone, o.OrganizationInfo_Phone) as phone,
            o.OrganizationInfo_EIN as ein,
            o.OrganizationInfo_FiscalSponsorEIN as parent_ein";

    private static $FROM_TABLE_CLAUSE = "
        FROM login_history lh
        JOIN members m ON lh.LoginHistory_MembersID = m.Members_ID
        LEFT JOIN merchant_info mi ON m.Members_TypeID = 2 AND m.Members_ID = mi.MerchantInfo_MemberID
        LEFT JOIN organization_info o ON m.Members_TypeID = 3 AND m.Members_ID = o.OrganizationInfo_MemberID
        WHERE 1 = 1";

    private static $FILTER_ON_DATE = "
        AND m.Members_Timestamp > ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            concat(m.Members_FirstName, ' ', m.Members_LastName) LIKE ?
            OR m.Members_TypeID = 2 AND mi.MerchantInfo_Name LIKE ?
            OR m.Members_TypeID = 3 AND o.OrganizationInfo_Name LIKE ?
            OR m.Members_Email LIKE ?
            OR m.Members_Phone LIKE ?
            OR lh.LoginHistory_IP LIKE ?
            OR m.Members_TypeID = 2 AND mi.MerchantInfo_City LIKE ?
            OR m.Members_TypeID = 3 AND o.OrganizationInfo_City LIKE ?
            OR m.Members_TypeID = 2 AND mi.MerchantInfo_Zip LIKE ?
            OR m.Members_TypeID = 3 AND o.OrganizationInfo_Zip LIKE ?
            OR m.Members_TypeID = 2 AND mi.MerchantInfo_Phone LIKE ?
            OR m.Members_TypeID = 3 AND o.OrganizationInfo_Phone LIKE ?
            OR lh.LoginHistory_MembersID LIKE ?
        )";

    private static $DEFAULT_ORDER_BY = "
        ORDER BY lh.LoginHistory_ID desc";

    private static $ORDER_BY_ID = " ORDER BY lh.LoginHistory_ID";

    private static $ORDER_BY_DATE = " ORDER BY lh.LoginHistory_TimeStamp";

    private static $ORDER_BY_MEMBER = " ORDER BY lh.LoginHistory_MembersID";
}