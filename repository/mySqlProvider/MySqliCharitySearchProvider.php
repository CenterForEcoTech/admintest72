<?php
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/ServiceAPI.php");

class CharitySearchObject extends CharityBase{
    public $id;
    public $orgId;
    public $display;
    public $isChildOrg;
}

abstract class MySqliCharitySearchBase {
    protected static $SEARCH_SELECT_COLUMNS = "
        SELECT
            docs.id AS id,
            docs.displayname AS display,
            docs.orgname AS name,
            docs.officialname AS officialname,
            docs.org_id AS orgId,
            docs.ein AS ein,
            docs.address AS address,
            docs.city AS city,
            docs.state AS region,
            docs.zipcode AS postalCode,
            docs.nteecode AS NTEECode,
            docs.childorg AS isChildOrg";

    protected static $FILTER_ON_STATE = "
        AND docs.state = ?";

    protected static $FILTER_ON_IS_REGISTERED = "
        AND docs.org_id > 0";

    protected function mapTo($dbQueryObject){
        $charity = new CharitySearchObject();
        $charity->id = $dbQueryObject->id;
        $charity->orgId = $dbQueryObject->orgId;
        $charity->ein = $dbQueryObject->ein;
        $charity->name = $dbQueryObject->name ? $dbQueryObject->name : ucwords(strtolower($dbQueryObject->officialname));
        $charity->display = ucwords(strtolower($dbQueryObject->display));
        $charity->address1 = $dbQueryObject->address;
        $charity->city = $dbQueryObject->city;
        $charity->region = $dbQueryObject->region;
        $charity->postalCode = $dbQueryObject->postalCode;
        $charity->NTEECode = $dbQueryObject->NTEECode;
        $charity->isChildOrg = $dbQueryObject->isChildOrg;
        return $charity;
    }

    /**
     * Returns a string with hyphenated EIN replaced with a non-hyphenated EIN.
     * @param string $searchTerm
     * @return mixed|null|string
     */
    protected function getCleanSearchTerm($searchTerm){
        if (strlen($searchTerm)){
            // find all ein-like strings with hyphens and add them without hyphens
            $cleanQuery = preg_replace('/\b(\d{2})[-](\d{7})\b/', "$1$2", $searchTerm);

            // return raw search string as best relevancy indicator
            return $cleanQuery;
        }
        return null;
    }
}

class MySqliFullTextBooleanCharitySearchProvider extends MySqliCharitySearchBase implements CharitySearchEngine{
    const FT_MIN_WORD_LEN = 3;

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SEARCH_BODY = "
            ,MATCH(searchterm) AGAINST(?) AS score
        FROM combined_irs_orgs docs
        WHERE MATCH(searchterm) AGAINST(? IN BOOLEAN MODE) ";

    /**
     * Given a CharitySearchCriteria, returns a PaginatedResult object.
     * @param CharitySearchCriteria $criteria
     * @return PaginatedResult containing a collection of CharityBase
     */
    public function search(CharitySearchCriteria $criteria)
    {
        $relevanceTerm = $this->getCleanSearchTerm($criteria->term);
        $searchTerm = $this->getFormattedSearchTerm($relevanceTerm);
        $sqlString = self::$SEARCH_SELECT_COLUMNS.self::$SEARCH_BODY;
        $limiter = ($criteria->size) ? " LIMIT ".$criteria->start.",".($criteria->size + 1) : "";
        $sqlBinding = "ss";
        $sqlParams = array(
            $relevanceTerm,
            $searchTerm
        );

        if ($criteria->state && strlen($criteria->state)){
            $sqlString .= self::$FILTER_ON_STATE;
            $sqlBinding .= "s";
            $sqlParams[] = $criteria->state;
        }

        if ($criteria->isRegistered){
            $sqlString .= self::$FILTER_ON_IS_REGISTERED;
        }

        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $sqlString.$limiter, $sqlBinding, $sqlParams, $queryResponse);
        $result = new PaginatedResult();
        $result->hasMore = count($queryResponse->resultArray) > $criteria->size;
        $result->maxLength = $criteria->size;
        $result->startIndex = $criteria->start;
        $index = 0;
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $dbRow){
                if ($index < $criteria->size){
                    $result->collection[] = $this->mapTo((object)$dbRow);
                }
                $index++;
            }
        } else {
            trigger_error("MySqliFullTextBooleanCharitySearchProvider.search: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }

    private function getFormattedSearchTerm($cleanSearchTerm){
        if ($cleanSearchTerm){
            // check if a complete ein
            if (preg_match('/^\d{9}$/', $cleanSearchTerm)){
                return "+".$cleanSearchTerm;
            }

            // replace hyphens, which count as "not" criteria
            $cleanQuery = preg_replace("/[-]/", " ", $cleanSearchTerm);

            // force "and" and wildcards
            $wordArray = explode(" ", $cleanQuery);
            $resultString = "";
            $isFirst = true;
            foreach($wordArray as $word){
                if (strlen($word) >= 3){
                    if ($isFirst){
                        $isFirst = false;
                    } else {
                        $resultString .= " ";
                    }
                    $resultString .= "+".$word."*";
                } else {
                    // do nothing with short words; they don't get indexed anyway and forcing them into the query here will eliminate valid results
                }
            }
            return $resultString;
        }
        return null;
    }
}

class MySqliFullTextCharitySearchProvider extends MySqliCharitySearchBase implements CharitySearchEngine{

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SEARCH_BODY = "
            ,MATCH(searchterm) AGAINST(?) AS score
        FROM combined_irs_orgs docs
        WHERE MATCH(searchterm) AGAINST(?) ";

    /**
     * Given a CharitySearchCriteria, returns a PaginatedResult object.
     * @param CharitySearchCriteria $criteria
     * @return PaginatedResult containing a collection of CharityBase
     */
    public function search(CharitySearchCriteria $criteria)
    {
        $result = new PaginatedResult();
        $result->maxLength = $criteria->size ? $criteria->size : 1000;
        $result->startIndex = $criteria->start;

        $relevanceTerm = $this->getCleanSearchTerm($criteria->term);
        $sqlString = self::$SEARCH_SELECT_COLUMNS.self::$SEARCH_BODY;
        $limiter = ($result->maxLength) ? " LIMIT ".$criteria->start.",".($result->maxLength + 1) : "";
        $sqlBinding = "ss";
        $sqlParams = array(
            $relevanceTerm,
            $relevanceTerm
        );

        if ($criteria->state && strlen($criteria->state)){
            $sqlString .= self::$FILTER_ON_STATE;
            $sqlBinding .= "s";
            $sqlParams[] = $criteria->state;
        }

        if ($criteria->isRegistered){
            $sqlString .= self::$FILTER_ON_IS_REGISTERED;
        }
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $sqlString.$limiter, $sqlBinding, $sqlParams, $queryResponse);
        $result->hasMore = count($queryResponse->resultArray) > $result->maxLength;
        $index = 0;
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $dbRow){
                if ($index < $result->maxLength){
                    $result->collection[] = $this->mapTo((object)$dbRow);
                }
                $index++;
            }
        } else {
            trigger_error("MySqliFullTextCharitySearchProvider.search: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
}

class MySqliSphinxCharitySearchProvider extends MySqliCharitySearchBase implements CharitySearchEngine{

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SEARCH_BODY_PRIORITY_ORGS = "
		FROM 
			combined_irs_orgs docs
		JOIN 
			searchdataindexwithorgid sd ON (docs.id=sd.id)
		WHERE 
			query = ?";

    private static $SEARCH_BODY_UNREG_ORGS = "
		FROM 
			combined_irs_orgs docs
		JOIN 
			searchdataindexwithoutorgid sd ON (docs.id=sd.id)
		WHERE 
			query = ?";

    /**
     * Given a CharitySearchCriteria, returns a PaginatedResult object.
     * @param CharitySearchCriteria $criteria
     * @return PaginatedResult containing a collection of CharityBase
     */
    public function search(CharitySearchCriteria $criteria)
    {
        $result = new PaginatedResult();
        $result->maxLength = $criteria->size ? $criteria->size : 1000;
        $result->startIndex = $criteria->start;

        $searchTerm = $this->getCleanSearchTerm($criteria->term);

        $sqlPriorityOrgs = self::$SEARCH_SELECT_COLUMNS.self::$SEARCH_BODY_PRIORITY_ORGS;
        $sqlPriorityBinding = "s";
        $sqlPriorityParams = array($searchTerm.";mode=any");

        $sqlUnregOrgs = self::$SEARCH_SELECT_COLUMNS.self::$SEARCH_BODY_UNREG_ORGS;
        $sqlUnregBinding = "s";
        $sqlUnregParams = array($searchTerm.";mode=any");

        if ($criteria->state && strlen($criteria->state)){
            $sqlPriorityOrgs .= self::$FILTER_ON_STATE;
            $sqlPriorityBinding .= "s";
            $sqlPriorityParams[] = $criteria->state;
            $sqlUnregOrgs .= self::$FILTER_ON_STATE;
            $sqlUnregBinding .= "s";
            $sqlUnregParams[] = $criteria->state;
        }

        if ($criteria->isRegistered){
            $sqlString = $sqlPriorityOrgs;
            $sqlBinding = $sqlPriorityBinding;
            $sqlParams = $sqlPriorityParams;
        } else {
            $sqlString = $sqlPriorityOrgs." UNION ALL ".$sqlUnregOrgs;
            $sqlBinding = $sqlPriorityBinding.$sqlUnregBinding;
            $sqlParams = array_merge($sqlPriorityParams, $sqlUnregParams);
        }

        $limiter = ($result->maxLength) ? " LIMIT ".$criteria->start.",".($result->maxLength + 1) : "";
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::getResultWithLogging("MySqliSphinxCharitySearchProvider.search: SQL error:", $this->mysqli, $sqlString.$limiter, $sqlBinding, $sqlParams, $queryResponse);
        $result->hasMore = count($queryResponse->resultArray) > $result->maxLength;
        $index = 0;
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $dbRow){
                if ($index < $result->maxLength){
                    $result->collection[] = $this->mapTo((object)$dbRow);
                }
                $index++;
            }
        } else {
            trigger_error("MySqliSphinxCharitySearchProvider.search: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
}

class MySqliCharityLookupProvider extends MySqliCharitySearchBase implements CharityLookup{

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $IRS_SELECT_SQL_FOR_TEST = "
        SELECT
          irs_eobmf_ein as 1_ein,
          irs_eobmf_name as 1_primary_name,
          irs_eobmf_sortname as 1_sort_name,
          irs_eobmf_incareofname as 21_in_care_of_name,
          irs_eobmf_address as 22_address,
          irs_eobmf_city as 23_city,
          irs_eobmf_state as 24_state,
          irs_eobmf_zipcode as 25_zip_code,
          irs_eobmf_groupexemptionnumber as 31_group_exemption_number,
          irs_eobmf_subsectioncode as 32_subsection_code,
          irs_eobmf_affiliationcode as 33_affiliation_code,
          irs_eobmf_classificationcode as 34_classification_code,
          irs_eobmf_rulingdate as 35_ruling_date,
          irs_eobmf_deductabilitycode as 36_deductibility_code,
          irs_eobmf_foundationcode as 37_foundation_code,
          irs_eobmf_activitycode as 38_activity_codes,
          irs_eobmf_organizationcode as 39_organization_code,
          irs_eobmf_exemptstatuscode as 40_exempt_organization_status_code,
          irs_eobmf_taxperiod as 41_tax_period,
          irs_eobmf_assetcode as 42_asset_code,
          irs_eobmf_incomecode as 43_income_code,
          irs_eobmf_filingrequirementcode as 44_filing_requirement_code,
          irs_eobmf_pffilingrequirementcode as 45_pf_filing_requirement_code,
          irs_eobmf_accountingperiod as 46_accounting_period,
          irs_eobmf_assetamount as 47_asset_amount,
          irs_eobmf_incomeamount as 48_income_amount,
          irs_eobmf_negativeincome as 49_is_income_amount_negative,
          irs_eobmf_form990revenueamount as 50_form_990_revenue_amount,
          irs_eobmf_negativeform990 as 51_is_form_990_revenue_amount_negative,
          irs_eobmf_nteecode as 52_ntee_code
        FROM irs_eobmf
        WHERE irs_eobmf_ein = ?";

    private static $IRS_SELECT_SQL = "
        SELECT
          trim(irs_eobmf_ein) as id,
          0 as orgId,
          trim(irs_eobmf_ein) as ein,
          trim(irs_eobmf_name) as name,
          trim(irs_eobmf_address) as address,
          trim(irs_eobmf_city) as city,
          trim(irs_eobmf_state) as region,
          trim(irs_eobmf_zipcode) as postalCode,
          trim(irs_eobmf_nteecode) as NTEECode
        FROM irs_eobmf
        WHERE irs_eobmf_ein = ?";

    /**
     * Also called "reverse lookup," given a tax id (or EIN) lookup the public charity that matches.
     * Expectations:
     *      1) It returns zero or one match (never returns fiscally sponsored charities)
     *      2) When possible, it returns no match for a charity that has lost its IRS status.
     * @param $ein
     * @param boolean $isTest true to return a sorted iterable array of values
     * @return CharityBase
     */
    public function getPublicCharity($ein, $isTest = false)
    {
        $cleanEin = $this->getCleanSearchTerm($ein);
        $sqlString = $isTest ? self::$IRS_SELECT_SQL_FOR_TEST : self::$IRS_SELECT_SQL;
        $sqlBinding = "s";
        $sqlParam = array($cleanEin);
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $queryResponse);
        if ($queryResponse->success){
            if (count($queryResponse->resultArray)){
                if ($isTest){
                    return $queryResponse->resultArray[0];
                }
                return $this->mapTo((object)$queryResponse->resultArray[0]);
            }
        } else {
            trigger_error("MySqliCharityLookupProvider.getPublicCharity: ".$queryResponse->error, E_USER_ERROR);
        }
        return null;
    }
}