<?php
include_once("_getRootFolder.php");
include_once($rootFolder."repository/ManagerAPI.php");

class MockInvoiceIntegration implements InvoiceIntegration{
    private $mysqli;
    private $numberOfWeeksUntilDue = 2;
    private $paymentUrlPrefix;

    public function __construct(mysqli $mysqli, $paymentUrlPrefix, $numberOfWeeksUntilDue = 2){
        $this->mysqli = $mysqli;
        $this->paymentUrlPrefix = $paymentUrlPrefix;
        $this->numberOfWeeksUntilDue = $numberOfWeeksUntilDue;
    }

    /**
     * Returns the name of the integration that matches the registered InvoiceProvider
     * @return string name value
     */
    public function getProviderName()
    {
        return "mock";
    }

    /**
     * Returns the number of weeks until an invoice is due.
     * @return int
     */
    public function getWeeksUntilDue()
    {
        return $this->numberOfWeeksUntilDue;
    }

    /**
     * Creates a customer in the integration system and returns that external system's identifier for this customer.
     * @param MerchantInfo $merchantInfo
     * @return string identifier
     */
    public function createCustomer(MerchantInfo $merchantInfo)
    {
        return "MockBillingId".$merchantInfo->merchantID;
    }

    /**
     * Given an array of invoice records, creates invoices in the integration system and updates the invoice records
     * with the external system's identifier for each invoice.
     * @param array $invoiceRecords
     * @return array $invoiceRecords
     */
    public function createInvoice(array $invoiceRecords)
    {
        $getInvoiceProviderSql = "
            SELECT InvoiceProvider_ID, InvoiceProvider_NextInvoiceID, InvoiceProvider_InvoiceIdPrefix
            FROM invoice_providers
            WHERE InvoiceProvider_Name = ?";

        $invoiceProviderArray = MySqliHelper::get_result($this->mysqli, $getInvoiceProviderSql, "s", array($this->getProviderName()));
        if (count($invoiceProviderArray) != 1){
            //output_json(array( "message" => "error initializing invoice provider."));
            return array(); // fail!!
        }

        // by this time, we have a valid invoice provider and a valid merchant id
        $invoiceProvider = (object)$invoiceProviderArray[0];
        $invoiceProviderId = $invoiceProvider->InvoiceProvider_ID;
        $invoiceIdPrefix = $invoiceProvider->InvoiceProvider_InvoiceIdPrefix;

        foreach ($invoiceRecords as $invoiceRecord){
            $merchantInfo = $invoiceRecord->getMerchantInfo();
            $referralCodeRate = $merchantInfo->referralCodeRate;
            /* @var $invoiceRecord InvoiceRecord */

            // we are SOOO fake. We don't create a request, nor do we create a response. But it is an opportunity for us
            // to log messages
            $invoiceRecord->setInvoiceDate(date("Y-m-d", time()));
            $fakePayload = (object)array(
                "isActive"=> "1",
                "customerId" => $invoiceRecord->getCustomerId(),
                "invoiceNumber" => $invoiceRecord->getInvoiceNumber(),
                "invoiceDate" => $invoiceRecord->getInvoiceDate(),
                "dueDate" => $invoiceRecord->getDueDate(),
                "description" => $invoiceRecord->getDescription($this->getPaymentUrl($invoiceRecord->getMerchantInfo())),
                "isToBePrinted" => "0",
                "isToBeEmailed" => "1",
                "invoiceLineItems" => array(),
                "merchantInfo" => $merchantInfo,
                "referralRate" => $referralCodeRate ? $referralCodeRate->jsonSerialize() : null
            );
            if ($invoiceRecord->getPoNumber() != null){
                $fakePayload->poNumber = $invoiceRecord->getPoNumber();
            }
            foreach ($invoiceRecord->getInvoiceLineItems() as $lineItem){
                /* @var $lineItem InvoiceLineItemBase */
                $fakePayload->invoiceLineItems[] = (object)array(
                    "itemId" => $lineItem->getItemType(),
                    "quantity" => "1",
                    "price" => $lineItem->getAmount(),
                    "description" => $lineItem->getDescription(),
                    "taxable" => "0"
                );
            }
            $invoiceRecord->setApiRequest("*MockRequest* ".json_encode($fakePayload));

            // now make the stored procedure call to get the next invoice id
            $nextInvoiceIdResult = $this->mysqli->query("CALL increment_invoice_id(".$invoiceProviderId.")");
            $nextInvoiceIdData = null;
            if ($nextInvoiceIdResult){
                $debugDescription = $invoiceRecord->getDescription($this->getPaymentUrl($invoiceRecord->getMerchantInfo()));
                $invoiceRecord->setApiResponse("*MockResponse: Success incrementing a new ID. ".$debugDescription);

                //use the data in the resultset
                $nextInvoiceIdData = $nextInvoiceIdResult->fetch_assoc();

                //free the resultset
                $nextInvoiceIdResult->free();

                //clear the other result(s) from buffer
                //loop through each result using the next_result() method
                while ($this->mysqli->next_result()) {
                    //free each result.
                    $result = $this->mysqli->use_result();
                    if ($result instanceof mysqli_result) {
                        $result->free();
                    }
                }

                $nextInvoiceId = $invoiceIdPrefix.$nextInvoiceIdData["NextInvoiceID"];

                // for now, the mock only "pretends" to have an invoice pushed to it and it fulfils the primary contract
                // of updating the externalInvoiceId.
                $invoiceRecord->setExternalInvoiceId($nextInvoiceId);
            } else {
                //output_json(array( "message" => "error calling increment_invoice_id."));
                $invoiceRecord->setApiResponse("*MockResponse: error calling increment_invoice_id.");
            }
        }
        return $invoiceRecords;
    }

    /**
     * Given a MerchantInfo object, this integration returns the url that merchant can follow to pay the bill. May return
     * an empty string or null if no url access to payment is supported. Should not throw an exception.
     * @param MerchantInfo $merchantInfo
     * @return string
     */
    public function getPaymentUrl(MerchantInfo $merchantInfo)
    {
        if (strlen($this->paymentUrlPrefix)){
            $separator = "?";
            if (strpos($this->paymentUrlPrefix, "?")){
                $separator = "&";
            }
            return $this->paymentUrlPrefix.$separator."email=".$merchantInfo->contactEmail;
        }
        return "";
    }

    /**
     * Given an external customer ID returns all payments made by this customer in the last x months
     * @param $externalCustomerId
     * @param $months int number of months (default = 2)
     * @return array $invoicePayment InvoicePayment
     */
    public function getInvoicePayments($externalCustomerId, $months = 2)
    {
        // TODO: Implement getInvoicePayments() method.
    }
}