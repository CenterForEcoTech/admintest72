<?php
include_once("Helper.php");
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/config.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once("EventRecordProvider.php");

class MerchantBillingLogProvider{

    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $POST_EVENT_PREMIUM_SQL = "
        INSERT INTO merchant_billing_log
        (
            merchant_info_id,
            type,
            amount,
            description,
            event_id,
            ref_type,
            ref_id,
            member_id,
            admin_id
        )
        SELECT
          event_info.Event_ProposedByMerchantID,
          'fee',
          event_template_premiums.price,
          CONCAT_WS(' ', event_templates.EventTemplate_Name, event_template_premiums.name),
          event_info.Event_ID,
          'event_template_premium',
          event_template_premiums.id,
          ?,
          ?
        FROM event_info
        JOIN event_template_premiums ON event_info.Event_EventTemplatePremiumID = event_template_premiums.id
        JOIN event_templates ON event_template_premiums.event_template_id = event_templates.EventTemplate_ID
        WHERE event_info.Event_ID = ?
        AND NOT EXISTS (
          SELECT 1
          FROM merchant_billing_log
          WHERE merchant_info_id = event_info.Event_ProposedByMerchantID
          AND event_id = event_info.Event_ID
          AND ref_type = 'event_template_premium'
          AND ref_id = event_template_premiums.id
        )";

    private static $POST_EVENT_PREMIUM_BINDINGS = "iii";

    /**
     * Given an eventId that has an event template premium on it, post it for billing if it has not already been posted
     * @param $eventId
     * @param $memberId
     * @param $adminId
     */
    public function postEventTemplatePremium($eventId, $memberId = 0, $adminId = 0){
        $postPremiumParams = array(
            $memberId,
            $adminId,
            $eventId
        );
        $result = MySqliHelper::executeWithLogging("MySqliEventManager.postEventTemplatePremium SQL error posting billing: ", $this->mysqli, self::$POST_EVENT_PREMIUM_SQL, self::$POST_EVENT_PREMIUM_BINDINGS, $postPremiumParams);
    }
}