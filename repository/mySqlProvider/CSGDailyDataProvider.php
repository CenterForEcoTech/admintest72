<?php
class CSGDailyDataProvider {
	private $mysqli;
	
    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
    private static $INSERT_CSGDAILYDATA_SQL = "
        INSERT INTO csg_dailydata
        (
			CSGDailyData_FileName,
			SITEID, 
			PROJECTID, 
			MEASTABLE, 
			ID0, 
			ID1, 
			INSTALLED_DT, 
			
			APPTDATE, 
			AUDITOR_NAME, 
			CREW, 
			PARTID, 
			QTY, 
			EFIPART
		) VALUES (
            ?,?,?,?,?,?,?,
			?,?,?,?,?,?
        )
		ON DUPLICATE KEY UPDATE 
		CSGDailyData_DuplicateFileName=?,
		DuplicateID0=?,
		DuplicateInstallDate=?,
		INSTALLED_DT=INSTALLED_DT";

    private static  $INSERT_CSGDAILYDATA_BINDINGS = "sssssssssssdssss";
	
    public function insertDailyDataSingleRow($csgDailyData,$fileLocation){
        $sqlString = self::$INSERT_CSGDAILYDATA_SQL;
        $sqlBindings = self::$INSERT_CSGDAILYDATA_BINDINGS;

        $sqlParams = array(
			$fileLocation,
			$csgDailyData->siteId,
			$csgDailyData->projectId,
			$csgDailyData->meastable,
			$csgDailyData->id0,
			$csgDailyData->id1,
			$csgDailyData->installed_dt,
			$csgDailyData->apptDate,
			$csgDailyData->auditor_name,
			$csgDailyData->crew,
			$csgDailyData->partId,
			$csgDailyData->qty,
			$csgDailyData->efiPart,
			$fileLocation,
			$csgDailyData->id0,
			$csgDailyData->installed_dt			
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		$resultCollection["insertResults"][]=$insertResponse;
		
		$resultCollection["fileHistory"] = $this->insertDailyDataFileHistory($fileLocation);		
        return $resultCollection;
    }	
	
    public function insertDailyDataMulitpleRows($csgDailyData,$fileLocation){
        $sqlString = self::$INSERT_CSGDAILYDATA_SQL;
        $sqlBindings = self::$INSERT_CSGDAILYDATA_BINDINGS;
		$CSGDailyRecord = new CSGDailyDataRecord();
		foreach ($csgDailyData as $Id=>$row){
			$convertData = $CSGDailyRecord::castAs($row);
			$sqlParams = array(
				$fileLocation,
				$convertData->siteId,
				$convertData->projectId,
				$convertData->meastable,
				$convertData->id0,
				$convertData->id1,
				$convertData->installed_dt,
				$convertData->apptDate,
				$convertData->auditor_name,
				$convertData->crew,
				$convertData->partId,
				$convertData->qty,
				$convertData->efiPart,
				$fileLocation,
				$convertData->id0,
				$convertData->installed_dt
			);
			$insertResponse = new ExecuteResponseRecord();
			$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
			$resultCollection["insertResults"][] = $insertResponse;
			
			//prepare update count format that has SiteID and EFI Part
			if ($convertData->siteId && trim($convertData->efiPart)){
				$transferInfo = array();
				$transferInfo["WarehouseFromLocation"] = strtoupper($convertData->crew);
				$transferInfo["WarehouseToLocation"] = "Installed";
				$transferInfo["PurchaseOrder"] = "SiteID: ".$convertData->siteId.",ProjectID: ".$convertData->projectId.",Measure: ".$convertData->meastable;
				$efi = trim($convertData->efiPart);
				$units = $convertData->qty;
				$rawdate = explode(" ",$convertData->installed_dt);
				$transferInfo["TransferDate"] = MySQLDate($rawdate[0]);
				$transferInfo["MovementType"] = "Installed";
				$transferInfo["Total_".$efi] = $units;
				$resultCollection["updateCountInfo"][] = $transferInfo;
			}
		}
		$resultCollection["fileHistory"] = $this->insertDailyDataFileHistory($fileLocation);		
		return $resultCollection;
    }	
	
    private static $INSERT_CSGDAILYDATAFILEHISTORY_SQL = "
        INSERT INTO csg_dailydata_filehistory
        (
			CSGDailyDataFileHistory_FileName
		) VALUES (
            ?
        )";

    private static  $INSERT_CSGDAILYDATAFILEHISTORY_BINDINGS = "s";
	
    public function insertDailyDataFileHistory($fileLocation){
        $sqlString = self::$INSERT_CSGDAILYDATAFILEHISTORY_SQL;
        $sqlBindings = self::$INSERT_CSGDAILYDATAFILEHISTORY_BINDINGS;
        $sqlParams = array($fileLocation);
		//echo $sqlString.$sqlBindings.$sqlParams;
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	

    public function insertMonthlyData($csgMonthlyData,$fileLocation){
        $sqlString = self::$INSERT_CSGDAILYDATA_SQL;
        $sqlBindings = self::$INSERT_CSGDAILYDATA_BINDINGS;
		$sqlParams = array(
			$fileLocation,
			$csgMonthlyData->siteId,
			$csgMonthlyData->projectId,
			$csgMonthlyData->meastable,
			$csgMonthlyData->id0,
			$csgMonthlyData->id1,
			$csgMonthlyData->installed_dt,
			$csgMonthlyData->apptDate,
			$csgMonthlyData->auditor_name,
			$csgMonthlyData->crew,
			$csgMonthlyData->partId,
			$csgMonthlyData->qty,
			$csgMonthlyData->efiPart,
			$fileLocation,
			$csgMonthlyData->id0,
			$csgMonthlyData->installed_dt
		);
		$insertResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		$resultCollection["insertResults"][] = $insertResponse;
		return $resultCollection;
    }	

    public function prepareMonthlyDataMulitpleRows($csgDailyData,$fileLocation){
		//create EFI array to match on partId
		$PartIDByEFI = $this->getEFIByPartID();
		$CSGDailyRecord = new CSGDailyDataRecord();
		foreach ($csgDailyData as $Id=>$row){
			if ($row["siteId"]){
				$convertData = new stdClass();
				$convertData = $CSGDailyRecord::castAs($row);
				$convertData->efiPart = $PartIDByEFI[trim(strtoupper($convertData->partId))];
				$convertData->crew = trim(strtoupper($convertData->crew));
				$sqlParams = array(
					$fileLocation,
					$convertData->siteId,
					$convertData->projectId,
					$convertData->meastable,
					$convertData->id0,
					$convertData->id1,
					$convertData->installed_dt,
					$convertData->apptDate,
					$convertData->auditor_name,
					$convertData->crew,
					$convertData->partId,
					$convertData->qty,
					$convertData->efiPart
				);
				
				//First search on id1 to see if record was already added
				$critera = new stdClass();
				$criteria->id1 = $convertData->id1;
				$searchResults = $this->getDailyData($criteria);
				if ($searchResults->siteId && trim(strtoupper($searchResults->crew)) != trim(strtoupper($convertData->crew))){
					$resultCollection["misMatch"][] = $searchResults;
				}
				if (!$searchResults->siteId){
					//Add these records like a normal import
					$resultCollection["notFound"][] = $convertData;
					//separate the differnt types of rows for counting purposes
					if (!strpos(strtoupper($convertData->partId),"VISIT")){
						$resultCollection["notFoundISM"][] = $convertData;
					}else{
						$resultCollection["notFoundVisit"][] = $convertData;
					}
				}
			}//end if siteId
		}
		return $resultCollection;
    }	
    public function prepareMonthlyDataRows($csgDailyData,$fileLocation){
		//create EFI array to match on partId
		$PartIDByEFI = $this->getEFIByPartID();
		$CSGDailyRecord = new CSGDailyDataRecord();
		$visitCount = 0;
		foreach ($csgDailyData as $Id=>$row){
			if ($row["siteId"]){
				$convertData = new stdClass();
				$convertData = $CSGDailyRecord::castAs($row);
				$convertData->efiPart = $PartIDByEFI[trim(strtoupper($convertData->partId))];
				$convertData->crew = trim(strtoupper($convertData->crew));
				$StartDate = date("Y-m-01",strtotime($convertData->installed_dt));
				$sqlParams = array(
					$fileLocation,
					$convertData->siteId,
					$convertData->projectId,
					$convertData->meastable,
					$convertData->id0,
					$convertData->id1,
					$convertData->installed_dt,
					$convertData->apptDate,
					$convertData->auditor_name,
					$convertData->crew,
					$convertData->partId,
					$convertData->qty,
					$convertData->efiPart
				);
				if (!$convertData->efiPart){$visitCount++;}				

				$resultCollection["notFound"][] = $convertData;				
			}//end if siteId
		}
		//Delete daily record was already added
		$critera = new stdClass();
		$criteria->startDate = $StartDate;
		$criteria->endDate = date("Y-m-t",strtotime($StartDate));
		$deleteResults = $this->deleteDailyData($criteria);
		if (count($resultCollection)){
			$sqlString = self::$INSERT_CSGMONTHLYDATAFILEHISTORY_SQL;
			$sqlBindings = self::$INSERT_CSGMONTHLYDATAFILEHISTORY_BINDINGS;
			$sqlParams = array($fileLocation,0,count($resultCollection),$visitCount);
			//echo $sqlString.$sqlBindings.$sqlParams;
			$insertResponse = new ExecuteResponseRecord();
			$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
			
		}
		$resultCollection["deletedResults"] = $deleteResults;
		$resultCollection["processedMonth"] = date("Y-m",strtotime($StartDate));
		return $resultCollection;
    }	
	private static $MOVE_CSGDAILYDATA_SQL = "
		INSERT INTO csg_dailydata_dailyrecords SELECT * FROM csg_dailydata WHERE INSTALLED_DT BETWEEN ? AND ?";
	private static $DELETE_CSGDAILYDATA_SQL = "
		DELETE FROM csg_dailydata WHERE INSTALLED_DT BETWEEN ? AND ?";
    public function deleteDailyData($criteria){
        $movesqlString = self::$MOVE_CSGDAILYDATA_SQL;
        $deletesqlString = self::$DELETE_CSGDAILYDATA_SQL;
        $sqlBindings = "ss";
        $sqlParams = array($criteria->startDate,$criteria->endDate);

		//FirstMove data from one table to the next
        $moveResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $movesqlString, $sqlBindings, $sqlParams, $moveResponse);
		$responses["move"] = $moveResponse;
		
		//Now delete data from csg_dailydata
        $deleteResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $deletesqlString, $sqlBindings, $sqlParams, $deleteResponse);
		$responses["delete"] = $deleteResponse;
		return $responses;
    }	
		
    private static $INSERT_CSGMONTHLYDATAFILEHISTORY_SQL = "
        INSERT INTO csg_monthlydata_filehistory
        (
			CSGMonthlyDataFileHistory_FileName,
			CSGMonthlyDataFileHistory_MisMatchCount,
			CSGMonthlyDataFileHistory_NotFoundISMCount,
			CSGMonthlyDataFileHistory_NotFoundVisitCount
		) VALUES (
            ?,?,?,?
        )";

    private static  $INSERT_CSGMONTHLYDATAFILEHISTORY_BINDINGS = "siii";
	
    public function insertMonthlyDataFileHistory($fileLocation){
		//FirstGet counts
		$monthlycounts = $this->getMonthlyDataCounts($fileLocation);
		foreach ($monthlycounts as $count){
			$MisMatchCount = $count["MisMatchCount"];
			$NotFoundISMCount = $count["NotFoundISMCount"];
			$NotFoundVisitCount = $count["NotFoundVisitCount"];
		}
	
        $sqlString = self::$INSERT_CSGMONTHLYDATAFILEHISTORY_SQL;
        $sqlBindings = self::$INSERT_CSGMONTHLYDATAFILEHISTORY_BINDINGS;
        $sqlParams = array($fileLocation,$MisMatchCount,$NotFoundISMCount,$NotFoundVisitCount);
		//echo $sqlString.$sqlBindings.$sqlParams;
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	
    public function monthlyDataCounts($fileLocation,$misMatchCount,$notFoundISMCount,$notFoundVisitCount){
        $sqlString = "INSERT INTO temp_monthlycount (FileLocation,MisMatchCount,NotFoundISMCount,NotFoundVisitCount) VALUES (?,?,?,?)";
        $sqlBindings = "siii";
        $sqlParams = array($fileLocation,$misMatchCount,$notFoundISMCount,$notFoundVisitCount);
		//echo $sqlString.$sqlBindings.$sqlParams;
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	
    private static $SELECT_MONTHLYDATACOUNT_COLUMNS = "
        SELECT MisMatchCount, NotFoundISMCount, NotFoundVisitCount FROM temp_monthlycount WHERE FileLocation = ? ORDER BY ID LIMIT 0,1";
	
    public function getMonthlyDataCounts($fileLocation){
        $sqlString = self::$SELECT_MONTHLYDATACOUNT_COLUMNS;
        $sqlBindings = "s";
        $sqlParams = array($fileLocation);
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			$result = $sqlResult;
        } else {
            trigger_error("CSGDailyDataProvider.getEFIByPartID failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }		
	
	
	
	
	
	
	
	
    private static $SELECT_EFI_BYPARTID_COLUMNS = "
        SELECT GHSInventoryProducts_EFI as efi, GHSInventoryProducts_CSGCode partIds FROM ghs_inventory_products WHERE GHSInventoryProducts_CSGCode !=''";
	
    public function getEFIByPartID(){
        $sqlString = self::$SELECT_EFI_BYPARTID_COLUMNS;
        $sqlBindings = "";
        $sqlParams = array();
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$partIds = explode(",",$row["partIds"]);
				foreach ($partIds as $partId){
					$result[trim(strtoupper($partId))] = $row["efi"];
				}
			}
        } else {
            trigger_error("CSGDailyDataProvider.getEFIByPartID failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }		
	
	
	
	
    private static $SELECT_CSGDAILYDATAFILEHISTORY_COLUMNS = "
        SELECT COUNT(*) AS count FROM csg_dailydata_filehistory WHERE CSGDailyDataFileHistory_FileName=?";

    private static  $SELECT_CSGDAILYDATAFILEHISTORY_BINDINGS = "s";
	
    public function searchDailyDataFileHistory($fileLocation){
        $sqlString = self::$SELECT_CSGDAILYDATAFILEHISTORY_COLUMNS;
        $sqlBindings = self::$SELECT_CSGDAILYDATAFILEHISTORY_BINDINGS;
        $sqlParams = array($fileLocation);
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

        } else {
            trigger_error("CSGDailyDataProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }		

    private static $SELECT_CSGMONTHLYDATAFILEHISTORY_COLUMNS = "
        SELECT COUNT(*) AS count FROM csg_monthlydata_filehistory WHERE CSGMonthlyDataFileHistory_FileName=?";

    private static  $SELECT_CSGMONTHLYDATAFILEHISTORY_BINDINGS = "s";
	
    public function searchMonthlyDataFileHistory($fileLocation){
        $sqlString = self::$SELECT_CSGMONTHLYDATAFILEHISTORY_COLUMNS;
        $sqlBindings = self::$SELECT_CSGMONTHLYDATAFILEHISTORY_BINDINGS;
        $sqlParams = array($fileLocation);
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		$result->monthProcessed = str_replace(".csv","",str_replace("bulbreport","",$fileLocation));
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

        } else {
            trigger_error("CSGMonthlyDataProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }		
	
	
	private static $SELECT_CSGDAILYDATA_PRIOR_SQL = "
		SELECT 
			INSTALLED_DT, 
			QTY, 
			EFIPART
		FROM
			csg_dailydata
		WHERE 
			INSTALLED_DT BETWEEN ? AND ?
		";
    private static  $SELECT_CSGDAILYDATA_PRIOR_BINDINGS = "ss";
	
    public function getPriorDailyData($startDate,$endDate){
        $sqlString = self::$SELECT_CSGDAILYDATA_PRIOR_SQL;
        $sqlBindings = self::$SELECT_CSGDAILYDATA_PRIOR_BINDINGS ;
        $sqlParams = array($startDate,$endDate);
		//echo $sqlString;
		//print_pre($sqlParams);
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = self::mapToRecord((object)$row);
			}
		} else {
			trigger_error("CSGDailyDataProvider.getPriorDailyData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }		

	private static $SELECT_CSGDAILYDATA_SUM_SQL = "
		SELECT 
			EFIPART as efiPart,
			SUM(QTY) AS qty
		FROM
			csg_dailydata
		WHERE 
			INSTALLED_DT BETWEEN ? AND ?
		GROUP BY EFIPART
		";
    private static  $SELECT_CSGDAILYDATA_SUM_BINDINGS = "ss";

    public function getPriorDailyDataSum($startDate,$endDate){
        $sqlString = self::$SELECT_CSGDAILYDATA_SUM_SQL;
        $sqlBindings = self::$SELECT_CSGDAILYDATA_SUM_BINDINGS ;
        $sqlParams = array($startDate,$endDate);
		//echo $sqlString;
		//print_pre($sqlParams);
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = self::mapToRecord((object)$row);
			}
		} else {
			trigger_error("CSGDailyDataProvider.getPriorDailyData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }		

	protected function mapToRecord($dataRow) {
        return CSGDailyDataRecord::castAs($dataRow);
    }
	
    private static $SELECT_CSGDAILYDATA_DISTINCTEFI = "
        SELECT DISTINCT(EFIPART)as efiPart, CREW as crew FROM csg_dailydata";
		
	private static $GROUP_CSGDAILYDATA_DISTINCT = "
		GROUP BY CREW, EFIPART";
		
	private static $FILTER_CSGDAILYDATA_DISTINCTEFI = "
		WHERE INSTALLED_DT BETWEEN ? AND ? AND EFIMisMatch IS NULL";

	public function getDistinctEFIByCrew($StartDate, $EndDate){
        $sqlString = self::$SELECT_CSGDAILYDATA_DISTINCTEFI;
        $sqlBindings = "ss";
        $sqlParams = array(MySQLDateUpdate($StartDate),MySQLDateUpdate($EndDate));
		$sqlGroupBy = self::$GROUP_CSGDAILYDATA_DISTINCT;
		$sqlFilter = self::$FILTER_CSGDAILYDATA_DISTINCTEFI;
		
		$sqlSelectString = $sqlString.$sqlFilter.$sqlGroupBy;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlSelectString, $sqlBindings, $sqlParams, $queryResponse);		
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = self::mapToRecord((object)$row);
			}
		} else {
			trigger_error("CSGDailyDataProvider.getPriorDailyData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
 	
	}
    private static $SELECT_CSGDAILYDATA_EFIQTY = "
		SELECT CREW as crew,AUDITOR_NAME as auditor_name,INSTALLED_DT as installed_dt, PARTID as partId ,EFIPART as efiPart,SUM(QTY)as qty, SITEID as siteId FROM csg_dailydata";
		
	private static $GROUP_CSGDAILYDATA_EFIQTY = "
		GROUP BY CREW, SITEID, EFIPART";
		
	private static $FILTER_CSGDAILYDATA_EFIQTY = "
		WHERE (CREW !='' AND CREW IS NOT NULL) AND INSTALLED_DT BETWEEN ? AND ?";

	public function getEFIQtyByCrew($StartDate, $EndDate){
        $sqlString = self::$SELECT_CSGDAILYDATA_EFIQTY;
        $sqlBindings = "ss";
        $sqlParams = array(MySQLDateUpdate($StartDate),MySQLDateUpdate($EndDate));
		$sqlGroupBy = self::$GROUP_CSGDAILYDATA_EFIQTY;
		$sqlFilter = self::$FILTER_CSGDAILYDATA_EFIQTY;
		
		$sqlSelectString = $sqlString.$sqlFilter.$sqlGroupBy;
		//echo $sqlSelectString.print_r($sqlParams,true);
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlSelectString, $sqlBindings, $sqlParams, $queryResponse);		
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = self::mapToRecord((object)$row);
			}
		} else {
			trigger_error("CSGDailyDataProvider.getPriorDailyData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
 	
	}
	
	private static $SELECT_CSGDAILYDATA_BYEFI_SQL = "
		SELECT 
			SITEID as siteId,
			PROJECTID as projectId,
			MEASTABLE as meastable,
			ID0 as id0,
			ID1 as id1,
			INSTALLED_DT as installed_dt, 
			APPTDATE as apptDate,
			AUDITOR_NAME as auditor_name,
			CREW as crew,
			PARTID as partId,
			QTY as qty, 
			EFIPART as efiPart
		FROM
			csg_dailydata
		WHERE 1=1 AND CSGDailyData_FileName LIKE '%BulbReport%'";
	private static $FILTER_CSGDAILYDATA_ON_EFI = "
		AND	EFIPART LIKE ?";
	private static $FILTER_CSGDAILYDATA_ON_CREW = "
		AND	UPPER(CREW)= ?";
	private static $FILTER_CSGDAILYDATA_ON_MISMATCH = "
		AND	EFIMisMatch IS NULL";
		
    private static  $SELECT_CSGDAILYDATA_BYEFI_BINDINGS = "s";
	
    public function getDailyDataByEFI($criteria){
        $sqlString = self::$SELECT_CSGDAILYDATA_BYEFI_SQL;
		$sqlFilter = "";
        $sqlBindings = "";
        $sqlParams = array();
		if ($criteria->efi){
			$sqlFilter .= self::$FILTER_CSGDAILYDATA_ON_EFI;
			$sqlBindings .= "s";
			$sqlParams[] = $criteria->efi;
		}
		if ($criteria->crew){
			$sqlFilter .= self::$FILTER_CSGDAILYDATA_ON_CREW;
			$sqlBindings .= "s";
			$sqlParams[] = strtoupper($criteria->crew);
		}
		if ($criteria->efiMisMatch){
			$sqlFilter .= self::$FILTER_CSGDAILYDATA_ON_MISMATCH;
		}
		$sqlString =$sqlString.$sqlFilter;
		//echo $sqlString;
		//print_pre($sqlParams);
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = self::mapToRecord((object)$row);
			}
		} else {
			trigger_error("CSGDailyDataProvider.getDailyDataByEFI failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }		
	
	private static $SELECT_CSGDAILYDATA_SQL = "
		SELECT 
			CSGDailyData_ID as id,
			CSGDailyData_FileName as fileName,
			SITEID as siteId,
			PROJECTID as projectId,
			MEASTABLE as meastable,
			ID0 as id0,
			ID1 as id1,
			INSTALLED_DT as installed_dt, 
			APPTDATE as apptDate,
			AUDITOR_NAME as auditor_name,
			CREW as crew,
			PARTID as partId,
			QTY as qty, 
			EFIPART as efiPart";
			
	private static $FROM_TABLE_CSGDAILYDATA_CLAUSE="
		FROM csg_dailydata WHERE 1=1";
	private static $FILTER_CSGDAILYDATA_ON_ID1 = "
		AND ID1 = ?";
	private static $FILTER_CSGDAILYDATA_ON_DATE = "
		AND INSTALLED_DT BETWEEN ? AND ?";
	
    public function getDailyData($criteria){
        $paginationSelectString = self::$SELECT_CSGDAILYDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CSGDAILYDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

		if ($criteria->id1){
            $filterString .= self::$FILTER_CSGDAILYDATA_ON_ID1;
            $filterBinding .= "s";
            $filterParams[] = $criteria->id1;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_CSGDAILYDATA_ON_DATE;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->startDate;
            $filterParams[] = $criteria->endDate;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				if ($criteria->endDate){
					$result[] = self::mapToRecord((object)$row);
				}else{
					$result = self::mapToRecord((object)$row);
				}
			}
		} else {
			trigger_error("CSGDailyDataProvider.getDailyData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		

	private static $FIX_CSGDATA_CREW ="
		UPDATE csg_dailydata
		SET CREW = ?
		WHERE CSGDailyData_ID = ?";
	public function updateCSGData($crew,$id){
        $sqlString = self::$FIX_CSGDATA_CREW;
        $sqlBinding = "si";
        $sqlParams = array($crew,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("CSGDailyDataProvider.updateCSGData failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
	
	}
	private static $FIX_CSGDATA_EFIMISMATCH ="
		UPDATE csg_dailydata
		SET EFIMisMatch = ?
		WHERE CSGDailyData_ID = ?";
	public function updateCSGDataEFIMisMatch($efi,$id){
        $sqlString = self::$FIX_CSGDATA_EFIMISMATCH;
        $sqlBinding = "si";
        $sqlParams = array($efi,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("CSGDailyDataProvider.updateCSGDataEFIMisMatch failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
	
	}
	
}
?>