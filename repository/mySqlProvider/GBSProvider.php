<?php
class GBSProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT 
			GBSHoursCodes_ID as id,
			GBSHoursCodes_Name as name,
			GBSHoursCodes_Description as description,
			GBSHoursCodes_NonBillable as nonbillable,
			GBSHoursCodes_GroupID as groupId,
			GBSHoursCodes_DisplayOrderID as displayOrderId";
	
    private static $FROM_TABLE_CLAUSE = "
        FROM gbs_hours_codes
        WHERE 1 = 1";

    private static $FILTER_ON_CODESGROUP_ID = "
        AND GBSHoursCodes_GroupID = ?";
				
    public function getCodes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodes_Name";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->groupId){
			$filterString = self::$FILTER_ON_CODESGROUP_ID;
			$filterBinding = "i";
			$filterParams = array($criteria->groupId);
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getCodes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    private static $FROM_EMPLOYEECODE_CLAUSE = "
        FROM gbs_hours_codes_matrix LEFT JOIN gbs_hours_codes ON GBSHoursCodes_ID=GBSHoursCodesMatrix_CodesID
        WHERE 1=1";
		
	private static $FILTER_EMPLOYEECODE_EMPLOYEEID = "
		AND GBSHoursCodesMatrix_EmployeeID = ?";
		
    public function getEmployeeCodes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_EMPLOYEECODE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodes_DisplayOrderID";
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->employeeID){
			$filterBinding .= "i";
			$filterString = self::$FILTER_EMPLOYEECODE_EMPLOYEEID;
			$filterParams[] = $criteria->employeeID;
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getCodes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    // add
    private static $INSERT_CODES_SQL = "
		INSERT INTO gbs_hours_codes
		(
			GBSHoursCodes_Name,
			GBSHoursCodes_Description,
			GBSHoursCodes_NonBillable,
			GBSHoursCodes_GroupID,
			GBSHoursCodes_DisplayOrderID,
			GBSHoursCodes_AddedByUserID
		) VALUES (
			?,?,?,?,?,?
		)";

    public function addCodes($record, $adminUserId){
        $sqlString = self::$INSERT_CODES_SQL;
        $sqlBinding = "ssiiis";
        $sqlParam = array(
			$record->name,
			$record->description,
			$record->nonbillable,
			$record->groupId,
			$record->displayOrderId,
			$adminUserId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("GBSProvider.addCodes failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_CODES_SQL = "
        UPDATE gbs_hours_codes
        SET
			GBSHoursCodes_Name = ?,
			GBSHoursCodes_Description = ?,
			GBSHoursCodes_NonBillable = ?,
			GBSHoursCodes_GroupID = ?,
			GBSHoursCodes_DisplayOrderID = ?
		WHERE GBSHoursCodes_ID = ?";

    public function updateCodes($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_CODES_SQL;
            $sqlBinding = "ssiiii";
            $sqlParams = array(
				$record->name,
				$record->description,
				$record->nonbillable,
				$record->groupId,
				$record->displayOrderId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.updateCodes failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_CODESDISPLAYORDER_SQL = "
        UPDATE	gbs_hours_codes
        SET		GBSHoursCodes_DisplayOrderID = ?
		WHERE	GBSHoursCodes_ID = ?";

    public function updateCodesDisplayOrder($id,$displayOrderId){
        $sqlString = self::$UPDATE_CODESDISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("GBSProvider.updateCodesDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		$responses[] = $updateResponse;
		$responses[] = $sqlString.print_r($sqlParams,true);
        return $responses;
    }	

	//Code Matrix
    private static $SELECT_CODEMATRIX_COLUMNS = "
        SELECT
			GBSHoursCodesMatrix_ID as id,
			GBSHoursCodesMatrix_CodesID as codesId,
			GBSHoursCodesMatrix_EmployeeID as employeeId,
			GBSHoursCodesMatrix_AddTimeStamp as addTimeStamp,
			GBSHoursCodesMatrix_AddedByAdmin as addedByAdmin";

    private static $FROM_TABLE_CODEMATRIX_CLAUSE = "
		FROM gbs_hours_codes_matrix
		WHERE 1=1";
		
    private static $FILTER_ON_CODESMATRIX_ID = "
        AND GBSHoursCodesMatrix_ID = ?";

    private static $FILTER_ON_CODESMATRIX_EMPLOYEEID = "
        AND GBSHoursCodesMatrix_EmployeeID = ?";

    private static $FILTER_ON_CODESMATRIX_CODESID = "
        AND GBSHoursCodesMatrix_CodesID = ?";
		
	private static $FILTER_ON_CODESMATRIX_NOTSTATUS = "
        AND GBSHoursCodesMatrix_Status != ?";	
		
    public function getCodesMatrix($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
		$result->collection = array();

        $paginationSelectString = self::$SELECT_CODEMATRIX_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CODEMATRIX_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

        $orderBy = "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_CODESMATRIX_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->employeeId){
            $filterString .= self::$FILTER_ON_CODESMATRIX_EMPLOYEEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->employeeId;
		}
		if ($criteria->codesId){
            $filterString .= self::$FILTER_ON_CODESMATRIX_CODESID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->codesId;
		}
		if ($criteria->statusNot){
            $filterString .= self::$FILTER_ON_CODESMATRIX_NOTSTATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->statusNot;
		}
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromCountTableClause.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getCodesMatrix failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getCodesMatrix failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $INSERT_CODESMATRIX_SQL = "
		INSERT INTO gbs_hours_codes_matrix
		(
			GBSHoursCodesMatrix_CodesID,
			GBSHoursCodesMatrix_EmployeeID,
			GBSHoursCodesMatrix_AddedByAdmin
		) VALUES (
			?,?,?
		)";

    public function addCodesMatrix($record, $adminUserId){
		//first get training information
	
        $sqlString = self::$INSERT_CODESMATRIX_SQL;
        $sqlBinding = "iis";
        $sqlParam = array(
			$record->codesId,
			$record->employeeId,
			$adminUserId
		);
		//$insertResponse .= " CodesID=".$record->trainingId." ".$training->name." DisplayOrderID ".$training->displayOrderId." EmployeeID ".$record->employeeId. " ".$record->employeeFullName." by ".$adminUserId;
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("GBSProvider.addCodesMatrix failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	private static $REMOVE_CODESMATRIX_SQL = "
        DELETE FROM	gbs_hours_codes_matrix
		WHERE	GBSHoursCodesMatrix_ID = ?";

    public function removeCodesMatrix($id){
        $sqlString = self::$REMOVE_CODESMATRIX_SQL;
        $sqlBinding = "i";
        $sqlParams = array($id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("GBSProvider.removeCodesMatrix failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
	public function validateHours($record){
		$DataObj = new stdClass();
		foreach ($record as $type=>$value){
			if ($type =="employeeID"){
				$DataObj->employeeID = $value;
			}
			if ($type != "action" && $type !="employeeID"){
				$typeData = explode("_",$type);
				if ($typeData[2]){
					$Data[$typeData[0]][$typeData[2]][$typeData[1]] = $value;
					$DataObj->$typeData[0]->$typeData[2]->$typeData[1] = $value;
//					$Data[$typeData[0]]["date"][$typeData[2]]["value"]=$value;
				}else{
					$Data[$typeData[0]][$typeData[1]]=$value;
					$DataObj->$typeData[0]->$typeData[1] = $value;
//					$Data[$typeData[0]]["value"]=$value;
				}
			}
		}
		//validate the dateRows
		if (count($Data["DateRow"])){
			foreach ($Data["DateRow"] as $Date=>$RowInfo){
				foreach ($RowInfo as $RowID=>$Value){
					if (trim($Data["CodeRow"][$RowID]) == ''){
						$Errors[] = "Row ".$RowID." is missing Billing Code";
					}
					if (!is_numeric($Value)){
						$Errors[] = "The value '".$Value."' for Row ".$RowID." ".date("D m/d/Y",strtotime($Date))." is not a valid number";
					}
				}
			}
		}else{
			$Errors[] = "You must enter a value for each row you indicated a Billing Code";
		}
		if (!$DataObj->employeeID){
			$Errors[] = "Please indicate the Staff Member";
		}
		$UniqueErrors = array_unique($Errors);
		$Data["Errors"] = $UniqueErrors;
		foreach ($UniqueErrors as $Error){
			$dataErrors .= $Error."<bR>";
		}
		$DataObj->Errors = $dataErrors;
			
		return $DataObj;
	}
	
    private static $INSERT_CODEHOURS_SQL = "
		INSERT INTO gbs_hours
		(
			GBSHours_EmployeeID,
			GBSHours_Date,
			GBSHours_CodeID,
			GBSHours_Hours,
			GBSHours_AddedByAdmin
		) VALUES (
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          GBSHours_Hours = ?
		 ";
	
	public function insertHours($record,$adminUserID){
        $sqlString = self::$INSERT_CODEHOURS_SQL;
        $sqlBinding = "isidsd";
		
		foreach ($record->DateRow as $Date=>$DateInfo){
			foreach ($DateInfo as $code=>$hour){
				$results .= $Date.": Code=".$record->CodeRow->$code." hour=".$hour;
				$sqlParam = array(
					$record->employeeID,
					$Date,
					$record->CodeRow->$code,
					$hour,
					$adminUserID,
					$hour
				);
				$sqlParams[] = $sqlParam;
				$insertResponse = new ExecuteResponseRecord();
				MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
				if ($insertResponse->success && $insertResponse->affectedRows){
					$record->id = $insertResponse->insertedId;
					$insertResponse->record = $record;
				} else {
					trigger_error("GBSProvider.insertHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
				}
			}
			
		}
		return $insertResponse;
	}
	private static $SELECT_NOTES_SQL = "
		SELECT 
			GBSHoursNotes_EmployeeID as employeeId,
			GBSHoursNotes_Date as noteDate,
			GBSHoursNotes_Note as note,
			GBSHoursNotes_AddedByAdmin as addedByAdmin
		";
    private static $FROM_NOTESTABLE_CLAUSE = "
        FROM gbs_hours_notes
        WHERE 1 = 1";

    private static $FILTER_ON_NOTES_EMPLOYEEID = "
        AND GBSHoursNotes_EmployeeID = ?";
    private static $FILTER_ON_NOTES_DATE = "
        AND GBSHoursNotes_Date = ?";
				
    public function getNotes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_NOTES_SQL;
        $fromTableClause = self::$FROM_NOTESTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursNotes_Date";
        $limiter = "";
        // apply search
		if ($criteria->employeeId){
			$filterString .= self::$FILTER_ON_NOTES_EMPLOYEEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->employeeId;
		}
		if ($criteria->noteDate){
			$filterString .= self::$FILTER_ON_NOTES_DATE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->noteDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getNotes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getNotes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
		
    private static $INSERT_NOTES_SQL = "
		INSERT INTO gbs_hours_notes
		(
			GBSHoursNotes_EmployeeID,
			GBSHoursNotes_Date,
			GBSHoursNotes_Note,
			GBSHoursNotes_AddedByAdmin
		) VALUES (
			?,?,?,?
		) ON DUPLICATE KEY UPDATE
          GBSHoursNotes_Note = ?
		 ";
	
	public function insertNotes($record,$adminUserID){
        $sqlString = self::$INSERT_NOTES_SQL;
        $sqlBinding = "issss";
		$sqlParam = array(
			$record->employeeId,
			$record->noteDate,
			$record->note,
			$adminUserID,
			$record->note
		);
		$sqlParams[] = $sqlParam;
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("GBSProvider.insertNotes failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $DELETE_CODEHOURS_SQL = "DELETE FROM gbs_hours WHERE GBSHours_ID IN (XXX)";
		
	public function deleteHours($record,$adminUserID){
        $sqlString = self::$DELETE_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		$IDS = $record->hourIDs;
		$sqlString = str_replace("XXX",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("GBSProvider.deleteHours failed to delete: ".$sqlString.$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $DELETE_CODEHOURID_SQL = "DELETE FROM gbs_hours WHERE GBSHours_ID = ? AND GBSHours_AddedByAdmin = ? AND GBSHours_Status='Saved'";
    private static $SELECT_CODEHOURID_SQL = "SELECT GBSHours_AddedByAdmin FROM gbs_hours WHERE GBSHours_ID = ?";
		
	public function deleteHourId($record,$adminUserID){
        $sqlString = self::$DELETE_CODEHOURID_SQL;
        $sqlBinding = "is";
		$sqlParam = array($record->hourid,$adminUserID);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success){
			if (!$insertResponse->affectedRows){
				$sqlString = self::$SELECT_CODEHOURID_SQL;
				$filterBinding = "i";
				$filterParams = array($record->hourid);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
					foreach ($sqlResult as $row){
						$insertResponse->AddedByAdmin = $row["GBSHours_AddedByAdmin"];
					}
                }
			}
			//now process the information into the only_submitted table
		} else {
			trigger_error("GBSProvider.deleteHours failed to delete: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $SUBMIT_CODEHOURS_SQL = "
		UPDATE gbs_hours
		SET 
			GBSHours_Status = 'Submitted',
			GBSHours_SubmittedDate = NOW(),
			GBSHours_SubmittedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function submitHours($record,$adminUserID){
        $sqlString = self::$SUBMIT_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("GBSProvider.submitHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
    private static $APPROVE_CODEHOURS_SQL = "
		UPDATE gbs_hours
		SET 
			GBSHours_Status = 'Approved',
			GBSHours_ApprovedDate = NOW(),
			GBSHours_ApprovedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function approveHours($record,$adminUserID){
        $sqlString = self::$APPROVE_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("GBSProvider.approveHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
    private static $SUBMIT_REMOVE_CODEHOURS_SQL = "
		UPDATE gbs_hours
		SET 
			GBSHours_Status = 'Saved',
			GBSHours_SubmittedDate = NOW(),
			GBSHours_SubmittedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function submitRemove($record,$adminUserID){
        $sqlString = self::$SUBMIT_REMOVE_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("GBSProvider.submitRemove failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $SELECT_CODEHOURS_COLUMNS = "
		SELECT
			GBSHours_ID as id,
			GBSHours_Date as hoursDate,
			GBSHours_CodeID as codeId,
			GBSHours_EmployeeID as employeeId,
			GBSHours_Hours as hours,
			GBSHours_AddedByAdmin as addedByAdmin,
			GBSHours_TimeStamp as timeStamp,
			GBSHours_Status as status,
			GBSHours_SubmittedDate as submittedDate,
			GBSHours_SubmittedByAdmin as submittedByAdmin,
			GBSHours_ApprovedDate as approvedDate,
			GBSHours_ApprovedByAdmin as approvedByAdmin";
			
    private static $FROM_HOURSTABLE_CLAUSE = "
        FROM gbs_hours
        WHERE 1 = 1";
	private static $FILTER_ON_HOURS_EMPLOYEEID = "
		AND GBSHours_EmployeeID = ?";
	private static $FILTER_ON_HOURS_ID = "
		AND GBSHours_ID IN (?)";
	private static $FILTER_ON_HOURS_CODEID = "
		AND GBSHours_CodeID = ?";
	private static $FILTER_ON_HOURS_STATUS = "
		AND GBSHours_Status = ?";
	private static $FILTER_ON_HOURS_STATUS_OR = "
		AND (GBSHours_Status = ? OR GBSHours_Status = ?)";
	private static $FILTER_ON_HOURS_DATE = "
		AND GBSHours_Date BETWEEN ? AND ?";
				
    public function getHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_CODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_HOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHours_Date";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->employeeId){
			$filterString .= self::$FILTER_ON_HOURS_EMPLOYEEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->employeeId;
		}
		if ($criteria->id){
			foreach ($criteria->id as $HourID){
				$HourIDS[] = $HourID;
			}
			$IDS = implode(",",$HourIDS);
			$thisFilterString = self::$FILTER_ON_HOURS_ID;
			$thisFilterString = str_replace("?",$IDS,$thisFilterString);
			$filterString .= $thisFilterString;
		}
		if ($criteria->codeId){
			$filterString .= self::$FILTER_ON_HOURS_CODEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->codeId;
		}
		if ($criteria->status){
			$filterString .= self::$FILTER_ON_HOURS_STATUS;
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->statusOr){
			$filterString .= self::$FILTER_ON_HOURS_STATUS_OR;
			$filterBinding .= "ss";
			$splitStatus = explode("OR",$criteria->statusOr);
			$filterParams[] = $splitStatus[0];
			$filterParams[] = $splitStatus[1];
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_ON_HOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("GBSProvider.getHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
	
    private static $SELECT_APPROVEDCODEHOURS_COLUMNS = "
		SELECT
			GBSHoursOnlySubmitted_GBSHoursID as id,
			GBSHoursOnlySubmitted_GBSHoursID as gbsHoursId,
			Dateworked as dateWorked,
			Cc1 as codeName,
			CONCAT(LastName,'_',FirstName) as employeeName,
			Trxvalue as hours";
			
    private static $FROM_APPROVEDHOURSTABLE_CLAUSE = "
        FROM gbs_hours_only_submitted
        WHERE 1 = 1";
	private static $FILTER_ON_APPROVEDHOURS_LASTNAME = "
		AND LastName = ?";
	private static $FILTER_ON_APPROVEDHOURS_CODENAME = "
		AND Cc1 = ?";
	private static $FILTER_ON_APPROVEDHOURS_DATE = "
		AND Dateworked BETWEEN ? AND ?";
				
    public function getApprovedHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_APPROVEDCODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_APPROVEDHOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY Dateworked";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->lastName){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_LASTNAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->lastName;
		}
		if ($criteria->codeName){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_CODENAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->codeName;
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getApprovedHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("GBSProvider.getApprovedHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }	
	
	public function CodeDisplayRow($rowObj,$CodesActive,$CodeHours = NULL){
		$DisplayMonthYear = $rowObj->DisplayMonthYear;
		$trCount = $rowObj->trCount;
		$trHeaderCount = 0;
		$trCountEnd = count($CodesActive);
		$WeekStartOrg = $rowObj->WeekStartOrg;
		$WeekEnd = $rowObj->WeekEnd;
		$WeekSubmitted = $rowObj->Submitted;
		$RowDisplayNotSubmitted = count($CodeHours)+1;
		$RowDisplaySubmitted = count($CodeHours);
		$displayRow = "";
		
		if (count($CodeHours)){
			foreach ($CodeHours as $CodeID=>$CodeInfo){
				$CodesWithData[] = $CodeID;
			}
			
		}
		
		foreach ($CodesActive as $CodeID=>$CodesInfo){
			$WeekStart = $WeekStartOrg;
			$displayRowData = "";
			$deleteRow[$trCount] = "";
			$headerRowData = "";
			$thisHeaderDate = "";
			while ($WeekStart <=  $WeekEnd){
				$thisDate = "DateRow_".$trCount."_".date("Y-m-d",$WeekStart);
				$thisValue = "";
				$thisHourID = "";
				$thisStatus = ($WeekSubmitted ? " SubmittedValue" : "");
				$thisHeaderDate .= "<td class=\"ui-state-default headerRow\">".date("D m/d",$WeekStart)."</td>";
				foreach ($CodeHours[$CodeID] as $Ccount=>$CodeHoursInfo){
					if ($WeekStart == strtotime($CodeHoursInfo->hoursDate)){
						$thisValue = $CodeHoursInfo->hours;
						$thisHourID = $CodeHoursInfo->id;
						$thisStatus = " ".$CodeHoursInfo->status."ValueData";
						$thisSubmittedDate = date("m/d/Y g:i a",strtotime($CodeHoursInfo->submittedDate));
						if ($thisValue > 0){
							$CodeTotals[$CodeID] = bcadd($CodeTotals[$CodeID],$thisValue,2);
							$DayTotals[$WeekStart] = bcadd($DayTotals[$WeekStart],$thisValue,2);
						}
						if ($CodeHoursInfo->status == "Saved"){
							$selectDivDisplay[$trCount] = "none";
							$displayDivStyle[$trCount] = "";
							$displayDivClassAndTitle[$trCount] = " class=\"codeNameDisplay\"";
							$thisHoursIDs[$trCount][] = $thisHourID;
							$deleteRow[$trCount] = "<div class=\"deleteRow\" id=\"".implode(",",$thisHoursIDs[$trCount])."\" data-row=\"row".$DisplayMonthYear.$trCount."\">&nbsp;</div>";
						}
					}
				}
				$displayRowData .="<td class=\"dayInput".$thisStatus."\">";
				if ($thisStatus == " SubmittedValue" || $thisStatus == " SubmittedValueData" || $thisStatus == " ApprovedValueData"){
					$WeekHasBeenSubmitted = true;
					$displayRowData .="<span class=\"".$thisStatus."Span\" data-hourid=\"".$thisHourID."\" title=\"Submitted ".$thisSubmittedDate."\">".$thisValue."</span>";
				}else{
					$displayRowData .="<input class=\"dayInputValue columndateInput".date("Y-m-d",$WeekStart)." rowCountInput".$trCount."\" data-rowCount=\"".$trCount."\" data-columndate=\"".date("Y-m-d",$WeekStart)."\" type=\"text\" style=\"height:25px;width:100px;\" data-hourid=\"".$thisHourID."\" id=\"".$thisDate."\" name=\"".$thisDate."\" value=\"".$thisValue."\"></td>";
				}
				$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
			}
			
			
			$RowDisplayCount = (!$WeekHasBeenSubmitted ? $RowDisplayNotSubmitted : $RowDisplaySubmitted);
			$headerRow = "<tr><td class=\"ui-state-default headerRow\">Billing Code</td>".$thisHeaderDate."<td class=\"totals\">&nbsp;</td></tr>";
			if ($trHeaderCount == 20){
				$trHeaderCount = 0;
				$displayRow .= $headerRow;
			}
			$displayRow .= "<tr id=\"row".$DisplayMonthYear.$trCount."\">
								<td>";
			if (!$WeekHasBeenSubmitted){
					$displayRow .= 	"<input type=\"hidden\" class=\"dayInputValue\" id=\"CodeRow_".$trCount."\" name=\"CodeRow_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\" value=\"".$CodesInfo->id."\">";
					$displayRow .="<div id=\"display".$DisplayMonthYear.$trCount."\" class=\"codeNameDisplay\">".$CodesInfo->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesInfo->name,"",str_replace("-","",$CodesInfo->description)))."</span></div>";
					$displayRow .= $deleteRow[$trCount];
			}else{
				$displayRow .=$CodesInfo->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesInfo->name,"",str_replace("-","",$CodesInfo->description)))."</span>";
			}			
			$displayRow .=	"</td>";
			$displayRow .= $displayRowData;
//			if (count($CodeHours)){
				$displayRow .="<td class=\"totals\" id=\"rowTotal".$trCount."\" style=\"font-size:8pt;\">".$CodeTotals[$CodeID]."</td>";
//			}
			$displayRow .="</tr>";
			$trCount++;
			$trHeaderCount++;
		}//end while
		
		$results["displayRow"] = $displayRow;
		if (count($DayTotals)){
			$results["dayTotals"] = $DayTotals;
		}

		return $results;
		//$displayRow;
		
	}
	public function CodePickerRow($rowObj,$CodesActive,$CodeHours = NULL){
		$DisplayMonthYear = $rowObj->DisplayMonthYear;
		$trCount = $rowObj->trCount;
		$trCountEnd = $rowObj->trCountEnd+10;
		$WeekStartOrg = $rowObj->WeekStartOrg;
		$WeekEnd = $rowObj->WeekEnd;
		$WeekSubmitted = $rowObj->Submitted;
		$RowDisplayNotSubmitted = count($CodeHours)+1;
		$RowDisplaySubmitted = count($CodeHours);
		$displayRow = "";
		
		if (count($CodeHours)){
			foreach ($CodeHours as $CodeID=>$CodeInfo){
				$CodesWithData[] = $CodeID;
			}
			
		}
		
		while ($trCount <=$trCountEnd){
			$WeekStart = $WeekStartOrg;
			$displayRowData = "";
			$selectDivDisplay[$trCount] = "block";
			$displayDivStyle[$trCount] = " style=\"display:none;cursor:pointer\"";
			$displayDivClassAndTitle[$trCount] = " class=\"editDisplay\" title=\"click to edit list\"";
			$deleteRow[$trCount] = "";
			while ($WeekStart <=  $WeekEnd){
				$thisDate = "DateRow_".$trCount."_".date("Y-m-d",$WeekStart);
				$thisValue = "";
				$thisHourID = "";
				$thisStatus = ($WeekSubmitted ? " SubmittedValue" : "");
				foreach ($CodeHours[$CodesWithData[$trCount-1]] as $Ccount=>$CodeHoursInfo){
					if ($WeekStart == strtotime($CodeHoursInfo->hoursDate)){
						$thisValue = $CodeHoursInfo->hours;
						$thisHourID = $CodeHoursInfo->id;
						$thisStatus = " ".$CodeHoursInfo->status."ValueData";
						$thisSubmittedDate = date("m/d/Y g:i a",strtotime($CodeHoursInfo->submittedDate));
						if ($thisValue > 0){
							$CodeTotals[$CodesWithData[$trCount-1]] = bcadd($CodeTotals[$CodesWithData[$trCount-1]],$thisValue,2);
							$DayTotals[$WeekStart] = bcadd($DayTotals[$WeekStart],$thisValue,2);
						}
						if ($CodeHoursInfo->status == "Saved"){
							$selectDivDisplay[$trCount] = "none";
							$displayDivStyle[$trCount] = "";
							$displayDivClassAndTitle[$trCount] = " class=\"codeNameDisplay\"";
							$thisHoursIDs[$trCount][] = $thisHourID;
							$deleteRow[$trCount] = "<div class=\"deleteRow\" id=\"".implode(",",$thisHoursIDs[$trCount])."\" data-row=\"row".$DisplayMonthYear.$trCount."\">&nbsp;</div>";
						}
					}
				}
				$displayRowData .="<td class=\"dayInput".$thisStatus."\">";
				if ($thisStatus == " SubmittedValue" || $thisStatus == " SubmittedValueData" || $thisStatus == " ApprovedValueData"){
					$WeekHasBeenSubmitted = true;
					$displayRowData .="<span class=\"".$thisStatus."Span\" data-hourid=\"".$thisHourID."\" title=\"Submitted ".$thisSubmittedDate."\">".$thisValue."</span>";
				}else{
					$displayRowData .="<input class=\"dayInputValue columndateInput".date("Y-m-d",$WeekStart)." rowCountInput".$trCount."\" data-rowCount=\"".$trCount."\" data-columndate=\"".date("Y-m-d",$WeekStart)."\" type=\"text\" style=\"height:25px;width:100px;\" data-hourid=\"".$thisHourID."\" id=\"".$thisDate."\" name=\"".$thisDate."\" value=\"".$thisValue."\"></td>";
				}
				$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
			}
			
			
			$RowDisplayCount = (!$WeekHasBeenSubmitted ? $RowDisplayNotSubmitted : $RowDisplaySubmitted);
			$displayRow .= "<tr id=\"row".$DisplayMonthYear.$trCount."\"".($trCount > $RowDisplayCount ? " style=\"display:none;\"" : "")."\">
								<td>";
			if (!$WeekHasBeenSubmitted){
					$displayRow .= 	"<div id=\"select".$DisplayMonthYear.$trCount."\" style=\"display:".$selectDivDisplay[$trCount]."\">
										<select class=\"comboboxCodes dayInputValue\" id=\"CodeRow_".$trCount."\" name=\"CodeRow_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\">
											<option value=\"\">Select one...</option>";
											foreach ($CodesActive as $code=>$codeInfo){
												$displayRow .= "<option value=\"".$codeInfo->id."\"".($CodesWithData[$trCount-1] == $codeInfo->id ? " selected" : "").">".$codeInfo->description."</option>";
											}
					$displayRow .="		</select>
								</div>";
					$displayRow .="<div id=\"display".$DisplayMonthYear.$trCount."\"".$displayDivClassAndTitle[$trCount].$displayDivStyle[$trCount]."\">".$CodesActive[$CodesWithData[$trCount-1]]->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesActive[$CodesWithData[$trCount-1]]->name,"",str_replace("-","",$CodesActive[$CodesWithData[$trCount-1]]->description)))."</span></div>";
					$displayRow .= $deleteRow[$trCount];
			}else{
				$displayRow .=$CodesActive[$CodesWithData[$trCount-1]]->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesActive[$CodesWithData[$trCount-1]]->name,"",str_replace("-","",$CodesActive[$CodesWithData[$trCount-1]]->description)))."</span>";
			}			
			$displayRow .=	"</td>";
			$displayRow .= $displayRowData;
//			if (count($CodeHours)){
				$displayRow .="<td class=\"totals\" id=\"rowTotal".$trCount."\" style=\"font-size:8pt;\">".$CodeTotals[$CodesWithData[$trCount-1]]."</td>";
//			}
			$displayRow .="</tr>";
			$trCount++;
		}//end while
		
		$results["displayRow"] = $displayRow;
		if (count($DayTotals)){
			$results["dayTotals"] = $DayTotals;
		}

		return $results;
		//$displayRow;
		
	}
	
	// Groups
	    private static $SELECT_GROUP_COLUMNS = "
        SELECT 
			GBSHoursCodesGroups_ID as id,
			GBSHoursCodesGroups_Name as name";
	
    private static $FROM_TABLE_GROUP_CLAUSE = "
        FROM gbs_hours_codes_groups
        WHERE 1 = 1";

    public function getGroups($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_GROUP_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_GROUP_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodesGroups_Name";
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getGroups failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getGroups failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $INSERT_GROUPS_SQL = "
		INSERT INTO gbs_hours_codes_groups
		(
			GBSHoursCodesGroups_Name,
			GBSHoursCodesGroups_AddedByUserID
		) VALUES (
			?,?
		)";

    public function addGroups($record, $adminUserId){
        $sqlString = self::$INSERT_GROUPS_SQL;
        $sqlBinding = "ss";
        $sqlParam = array(
			$record->name,
			$adminUserId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("GBSProvider.addGroups failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_GROUPS_SQL = "
        UPDATE gbs_hours_codes_groups
        SET
			GBSHoursCodesGroups_Name = ?
		WHERE GBSHoursCodesGroups_ID = ?";

    public function updateGroups($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_GROUPS_SQL;
            $sqlBinding = "si";
            $sqlParams = array(
				$record->name,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.updateGroups failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }
	
    private static $SELECT_COLUMNS_SUPERVISEDBY = "
        SELECT 
			GBSHoursSupervised_ID as id,
			GBSHoursSupervised_EmployeeEmail as employeeEmail,
			GBSHoursSupervised_SupervisorName as supervisorName";
	
    private static $FROM_TABLE_CLAUSE_SUPERVISEDBY = "
        FROM gbs_hours_supervised
        WHERE 1 = 1";
		
    public function getSupervisedBy($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS_SUPERVISEDBY;
        $fromTableClause = self::$FROM_TABLE_CLAUSE_SUPERVISEDBY;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = "";
        $limiter = "";
		$filterString = "";

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getSupervisedBy failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getSupervisedBy failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }


    private static $SELECT_AEPTEMPLATES_COLUMNS = "
        SELECT 
			GBSEAPTemplates_ID as id,
			GBSEAPTemplates_Measure as measure,
			GBSEAPTemplates_PrimaryUtility as primaryUtility,
			GBSEAPTemplates_SecondaryUtility as secondaryUtility,
			GBSEAPTemplates_IsMFEP as isMfep,
			GBSEAPTemplates_DisplayText as displayText,
			GBSEAPTemplates_LastModifiedTimeStamp as lastModifiedTimeStamp,
			GBSEAPTemplates_LastModifiedBy as lastModifiedBy";
	
    private static $FROM_TABLE_AEPTEMPLATES_CLAUSE = "
        FROM gbs_eap_templates
        WHERE 1 = 1";
		
    private static $FROM_TABLE_AEPMFEPTEMPLATES_CLAUSE = "
        FROM gbs_eap_mfeptemplates
        WHERE 1 = 1";

    private static $FILTER_ON_EAP_MEASURE = "
        AND GBSEAPTemplates_Measure = ?";
		
    private static $FILTER_ON_EAP_UTILITY_PRIMARY = "
        AND GBSEAPTemplates_PrimaryUtility = ?";

    private static $FILTER_ON_EAP_UTILITY_SECONDARY = "
        AND GBSEAPTemplates_SecondaryUtility = ?";
		
    private static $FILTER_ON_EAP_ISMFEP = "
        AND GBSEAPTemplates_IsMFEP = 1";
		
    private static $FILTER_ON_EAP_TEXT = "
        AND GBSEAPTemplates_DisplayText = ?";
		
    public function getEAPTemplates($criteria){
        $paginationSelectString = self::$SELECT_AEPTEMPLATES_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_AEPTEMPLATES_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSEAPTemplates_Measure";
        $limiter = "";
        // apply search
 		if ($criteria->measure){
			$filterString .= self::$FILTER_ON_EAP_MEASURE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->measure;
		}
 		if ($criteria->primaryUtility){
			$filterString .= self::$FILTER_ON_EAP_UTILITY_PRIMARY;
			$filterBinding .= "s";
			$filterParams[] = $criteria->primaryUtility;
		}
 		if ($criteria->secondaryUtility){
			$filterString .= self::$FILTER_ON_EAP_UTILITY_SECONDARY;
			$filterBinding .= "s";
			$filterParams[] = $criteria->secondaryUtility;
		}
 		if ($criteria->displayText){
			$filterString .= self::$FILTER_ON_EAP_TEXT;
			$filterBinding .= "s";
			$filterParams[] = $criteria->displayText;
		}
 		if ($criteria->isMfep){
			$filterString .= self::$FILTER_ON_EAP_ISMFEP;
			$fromTableClause = self::$FROM_TABLE_AEPMFEPTEMPLATES_CLAUSE;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("GBSProvider.getAEPTemplates failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("GBSProvider.getAEPTemplates failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $INSERT_EAPTEMPLATE_SQL = "
		INSERT INTO gbs_eap_templates
		(
			GBSEAPTemplates_Measure,
			GBSEAPTemplates_PrimaryUtility,
			GBSEAPTemplates_SecondaryUtility,
			GBSEAPTemplates_IsMFEP,
			GBSEAPTemplates_DisplayText,
			GBSEAPTemplates_LastModifiedTimeStamp,
			GBSEAPTemplates_LastModifiedBy
		) VALUES (
			?,?,?,?,?,?,?
		)";

    public function addEAPTemplate($record, $adminUserId){
        $sqlString = self::$INSERT_EAPTEMPLATE_SQL;
        $sqlBinding = "sssisss";
        $sqlParam = array(
			$record->measure,
			$record->primaryUtility,
			$record->secondaryUtility,
			$record->isMfep,
			$record->displayText,
			date("Y-m-d H:i:s"),
			$adminUserId
        );
		if ($record->isMfep){
			$sqlString = str_replace("gbs_eap_templates","gbs_eap_mfeptemplates",$sqlString);
		}

        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("GBSProvider.addEAPTemplate failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_EAPTEMPLATE_SQL = "
        UPDATE gbs_eap_templates
        SET
			GBSEAPTemplates_Measure = ?,
			GBSEAPTemplates_PrimaryUtility = ?,
			GBSEAPTemplates_SecondaryUtility = ?,
			GBSEAPTemplates_IsMFEP = ?,
			GBSEAPTemplates_DisplayText = ?,
			GBSEAPTemplates_LastModifiedTimeStamp = ?,
			GBSEAPTemplates_LastModifiedBy = ?
		WHERE GBSEAPTemplates_ID = ?";

    public function updateEAPTemplate($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlString = self::$UPDATE_EAPTEMPLATE_SQL;
            $sqlBinding = "sssisssi";
            $sqlParams = array(
				trim($record->measure),
				trim($record->primaryUtility),
				trim($record->secondaryUtility),
				trim($record->isMfep),
				trim($record->displayText),
				$thisTimeStamp,
				$adminUserId,
				$record->id
            );
			
			if ($record->isMfep){
				$sqlString = str_replace("gbs_eap_templates","gbs_eap_mfeptemplates",$sqlString);
			}
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.updateEAPTemplate failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
	public function invoiceTool_loadExternalData($fileLocation,$tableName=Null){
		$tableName = ($tableName ? $tableName : "gbs_eversource_customer_account");
		
		//delete contents of table
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = "DELETE FROM ".$tableName." WHERE 1";
		$sqlBinding = "";
		$sqlParam = array();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);

		$sqlResult = MySqliHelper::loadExternalData($this->mysqli, $fileLocation,$tableName);		
		return $sqlResult;
	}
	
	public function invoiceTool_getEversourceAccounts($addressRecord){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT gbs_eversource_customer_account.*, CONCAT(`STREET_NAME`,' ',`STREET_SUFFIX`) as FULL_STREETNAME FROM gbs_eversource_customer_account WHERE CONCAT(`STREET_NAME`,' ',`STREET_SUFFIX`) LIKE '%FULLSTREETADDRESS%' AND SERVICE_TOWN=?";
		$SqlString = str_replace("FULLSTREETADDRESS",$addressRecord["STREET_NAME"],$SqlString);
		$filterBinding = "s";
		$filterParams = array($addressRecord["SERVICE_TOWN"]);
		
		if ($addressRecord["STREET_NUMBER_start"] > 0){
			if ($addressRecord["STREET_NUMBER_start"] == $addressRecord["STREET_NUMBER_end"]){
				$SqlString .= " AND STREET_NUMBER = ?";
				$filterBinding .= "i";
				$filterParams[] = $addressRecord["STREET_NUMBER_start"];
			}else{
				$SqlString .= " AND STREET_NUMBER >= ? AND STREET_NUMBER <= ?";
				$filterBinding .= "ii";
				$filterParams[] = $addressRecord["STREET_NUMBER_start"];
				$filterParams[] = $addressRecord["STREET_NUMBER_end"];
			}
			
		}else{
			$SqlString .= " AND STREET_NUMBER >= ?";
			$filterBinding .= "i";
			$filterParams[] = $addressRecord["STREET_NUMBER_start"];
		}
		//echo $SqlString.print_r($filterParams,true);
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getEversourceAccounts failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	public function invoiceTool_getEversourceMasterPricelist(){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_eversource_masterpricelist";
		$filterBinding = "";
		$filterParams = array();
		
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getEversourceMasterPricelist failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}

	public function invoiceTool_getMFEPAllocations(){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_invoicing_mfep_allocations";
		$filterBinding = "";
		$filterParams = array();
		
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getMFEPAllocations failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
    private static $UPDATE_MFEPALLOCATIONS_SQL = "
        UPDATE gbs_invoicing_mfep_allocations
        SET
			MFEPAllocations_Name = ?,
			MFEPAllocations_Multiplier = ?,
			MFEPAllocations_AverageHourlyRate = ?,
			MFEPAllocations_LastUpdatedBy = ?
		WHERE MFEPAllocations_ID = ?";

    public function invoiceTool_updateMFEPAllocations($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlString = self::$UPDATE_MFEPALLOCATIONS_SQL;
            $sqlBinding = "ssssi";
            $sqlParams = array(
				trim($record->name),
				trim($record->multiplier),
				trim($record->averageHourlyRate),
				$adminUserId,
				$record->id
            );
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.invoiceTool_updateMFEPAllocations failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
	
	
	public function invoiceTool_getBillingRates($criteria){
		  
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_billingrates WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		
		if ($criteria->id){
			$SqlString .= " AND GBSBillingRate_ID=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->id;
		}
		if ($criteria->invoiceName){
			$SqlString .= " AND GBSBillingRate_InvoiceName=?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->invoiceName;
		}
		$orderBy = " ORDER BY GBSBillingRate_InvoiceName, GBSBillingRate_Rate DESC";
		$SqlString .= $orderBy;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getBillingRates failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	
		
	}
	
    private static $UPDATE_BILLINGRATES_SQL = "
        UPDATE gbs_billingrates
        SET
			GBSBillingRate_Name = ?,
			GBSBillingRate_Rate = ?,
			GBSBillingRate_LastUpdatedBy = ?
		WHERE GBSBillingRate_ID = ?";

    public function invoiceTool_updateBillingRates($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlString = self::$UPDATE_BILLINGRATES_SQL;
            $sqlBinding = "sdsi";
            $sqlParams = array(
				trim($record->name),
				trim($record->rate),
				$adminUserId,
				$record->id
            );
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.invoiceTool_updateBillingRates failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
    public function invoiceTool_removeEmployeeFromBillingRate($record, $adminUserId){
		//first get all the employees currently in the list
		$criteria = new stdClass();
		$criteria->id = $record->id;
		$employeeResults = self::invoiceTool_getBillingRates($criteria);
		foreach ($employeeResults->collection as $id=>$result){
			$employees = explode(",",$result->GBSBillingRate_EmployeeMembers);
		}
		if(($key = array_search($record->employeeName, $employees)) !== false) {
			unset($employees[$key]);
		}
		$newEmployeeMembers = implode(",",$employees);
		$sqlString = "UPDATE gbs_billingrates SET GBSBillingRate_EmployeeMembers=?,GBSBillingRate_LastUpdatedBy = ? WHERE GBSBillingRate_ID=?";
		
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlBinding = "ssi";
            $sqlParams = array(
				trim($newEmployeeMembers),
				$adminUserId,
				$record->id
            );
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.invoiceTool_removeEmployeeFromBillingRate failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	

    public function invoiceTool_addEmployeeFromBillingRate($record, $adminUserId){
		//first get all the employees currently in the list and remove this name from all list
		$criteria = new stdClass();
		$criteria->invoiceName = $record->invoiceName;
		$employeeResults = self::invoiceTool_getBillingRates($criteria);
		foreach ($employeeResults->collection as $id=>$result){
			$employees = explode(",",$result->GBSBillingRate_EmployeeMembers);
			$id = $result->GBSBillingRate_ID;
			
			if(($key = array_search($record->employeeName, $employees)) !== false) {
				unset($employees[$key]);
			}
			$newEmployeeMembers = implode(",",$employees);
			$sqlString = "UPDATE gbs_billingrates SET GBSBillingRate_EmployeeMembers=?,GBSBillingRate_LastUpdatedBy = ? WHERE GBSBillingRate_ID=?";
			
			$updateResponse = new ExecuteResponseRecord();
			$thisTimeStamp = date("Y-m-d H:i:s");
			if ($record->id){
				$sqlBinding = "ssi";
				$sqlParams = array(
					trim($newEmployeeMembers),
					$adminUserId,
					$id
				);
				
				MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
				if ($updateResponse->error){
					trigger_error("GBSProvider.invoiceTool_removeEmployeeFromBillingRate failed to update: ".$updateResponse->error, E_USER_ERROR);
				}
			}
			
		}
		//now update the current id

		$criteria = new stdClass();
		$criteria->id = $record->id;
		$employeeResults2 = self::invoiceTool_getBillingRates($criteria);
		foreach ($employeeResults2->collection as $id=>$result){
			$employees2 = explode(",",$result->GBSBillingRate_EmployeeMembers);
		}
		if(($key = array_search($record->employeeName, $employees2)) !== false) {
			unset($employees2[$key]);
		}
		$employees2[] = $record->employeeName;
		$newEmployeeMembers2 = implode(",",$employees2);
		$sqlString = "UPDATE gbs_billingrates SET GBSBillingRate_EmployeeMembers=?,GBSBillingRate_LastUpdatedBy = ? WHERE GBSBillingRate_ID=?";
		
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlBinding = "ssi";
            $sqlParams = array(
				trim($newEmployeeMembers2),
				$adminUserId,
				$record->id
            );
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.invoiceTool_removeEmployeeFromBillingRate failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
	
	    private static $INSERT_BGASCUSTOMERS_SQL = "
		INSERT INTO gbs_bgas_customer_account (
			BusinessPartner, Phone, Account, Premise, Rate, 
			FirstName, LastName, StreetAddress, Unit, City, 
			HouseNumber, StreetName, State, ZipCode, Meter, 
			Device, BillingPortion, BillingPeriodBegin, BillingPeriodEnd, Amount, 
			Therms,AddedByAdmin)
		VALUES (
			?,?,?,?,?, 
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?
		) ON DUPLICATE KEY UPDATE
          Rate = ?
		 ";
		 
    public function addBGASCustomer($record, $adminUserId){
        $sqlString = self::$INSERT_BGASCUSTOMERS_SQL;
        $sqlBinding = "sssssssssssssssssssssss";
        $sqlParam = array(
			$record["BusinessPartner"], $record["Phone"], $record["Account"], $record["Premise"], $record["Rate"], 
			$record["FirstName"], $record["LastName"], $record["StreetAddress"], $record["Unit"], $record["City"], 
			$record["HouseNumber"], $record["StreetName"], $record["State"], $record["ZipCode"], $record["Meter"], 
			$record["Device"], $record["BillingPortion"], $record["BillingPeriodBegin"], $record["BillingPeriodEnd"], $record["Amount"], 
			$record["Therms"], $adminUserId, $record["Rate"]
        );
		//$insertResponse =  $sqlString.print_r($sqlParam,true)."<br>";
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $insertResponse->successcount = $insertResponse->successcount+1;
        } else {
            trigger_error("GBSProvider.addBGASCustomer failed to insert: ".$insertResponse->error, E_USER_ERROR);
            $insertResponse->failcount = $insertResponse->failcount+1;
        }

        return $insertResponse;
    }
	public function deleteBGASCustomers(){
		//delete contents of table
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = "DELETE FROM gbs_bgas_customer_accounts WHERE 1";
		$sqlBinding = "";
		$sqlParam = array();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		return $updateResponse;
	}
	
	
    public function addBGASCustomerSQL($sqlValues, $header){
        $sqlString = "INSERT INTO gbs_bgas_customer_accounts (".implode(", ",$header).")	VALUES ";
		
		$sqlString .= implode(",",$sqlValues);

        $sqlBinding = "";
        $sqlParam = array();
		//echo $sqlString.";<hr>";
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $insertResponse->successcount = $insertResponse->successcount+1;
        } else {
            trigger_error("GBSProvider.addBGASCustomer failed to insert: ".$insertResponse->error, E_USER_ERROR);
            $insertResponse->failcount = $insertResponse->failcount+1;
        }
        return $insertResponse;
    }
    public function getBGASCustomer($accountId,$locationId){
		$queryResponse = new QueryResponseRecord();
        $SqlString = "SELECT * FROM gbs_bgas_customer_accounts WHERE Account=? AND Premise=?";
        $filterBinding = "ss";
        $filterParams = array($accountId,$locationId);
		//echo $SqlString.print_r($filterParams,true)."<hr>";
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		$result = new stdClass();
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.getBGASCustomer failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
    }
    public function getBGASCustomers($criteria){
		$queryResponse = new QueryResponseRecord();
        $SqlString = "SELECT * FROM gbs_bgas_customer_accounts WHERE 1=1";
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->selectFields){
			$SqlString = str_replace("*",$criteria->selectFields,$SqlString);
		}
		
		if ($criteria->inPremise){
			$filterString = " AND Premise IN ('".implode("','",$criteria->inPremise)."')";
			
		}
		
		$SqlString .=$filterString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		//print_pre($sqlResult);

		if (!$queryResponse->success){
			trigger_error("GBSProvider.getBGASCustomer failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $sqlResult;
    }
	

	public function invoiceTool_getGPTypes($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_sf_gptypes WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		
		$orderBy = " ORDER BY GPType_Name";
		$SqlString .= $orderBy;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getGPTypes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
	
    private static $INSERT_PASSTHROUGH_SAGE_SQL = "
		INSERT INTO gbs_passthrough_sage
		(
			AccountID,AccountDescription,Date,Reference,Jrnl,
			TransDescription,DebitAmt,CreditAmt,Balance,JobID,
			UploadedBy
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?,
			?
		) ON DUPLICATE KEY UPDATE
          DebitAmt = ?, JobID = ?, UploadedBy = ?, UpdatedLastDate = ?
		 ";
    private static $INSERT_PASSTHROUGH_SQL = "
		INSERT INTO gbs_passthrough
		(
			Trans,Type,Date,Num,Name,
			SourceName,Memo,Debit,JobID,UpdatedBy
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          Debit = ?, JobID = ?, UpdatedBy = ?
		 ";
	
	public function insertPassthrough($record,$adminUserID){
        $sqlString = self::$INSERT_PASSTHROUGH_SQL;
        $sqlBinding = "issssssdssdss";
		//echo $sqlString;
		foreach ($record as $passThroughRecord){
			$sqlParam = array(
				$passThroughRecord["Trans"],
				$passThroughRecord["Type"],
				$passThroughRecord["Date"],
				$passThroughRecord["Num"],
				$passThroughRecord["Name"],
				$passThroughRecord["SourceName"],
				$passThroughRecord["Memo"],
				$passThroughRecord["Debit"],
				$passThroughRecord["JobID"],
				$adminUserID,
				$passThroughRecord["Debit"],
				$passThroughRecord["JobID"],
				$adminUserID
			);
			//print_pre($sqlParam);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$insertResponse->recordCount = $insertResponse->recordCount+1;
			} else {
				trigger_error("GBSProvider.insertPassthrough failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
			
		}
		return $insertResponse;
	}
	public function insertPassthroughSage($record,$adminUserID){
        $sqlString = self::$INSERT_PASSTHROUGH_SAGE_SQL;
        $sqlBinding = "ssssssdddssdsss";
		
		foreach ($record as $passThroughRecord){
			$sqlParam = array(
				$passThroughRecord["AccountID"],
				$passThroughRecord["AccountDescription"],
				$passThroughRecord["Date"],
				$passThroughRecord["Reference"],
				$passThroughRecord["Jrnl"],
				$passThroughRecord["TransDescription"],
				$passThroughRecord["DebitAmt"],
				$passThroughRecord["CreditAmt"],
				$passThroughRecord["Balance"],
				$passThroughRecord["JobID"],
				$adminUserID,
				$passThroughRecord["DebitAmt"],
				$passThroughRecord["JobID"],
				$adminUserID,
				date("Y-m-d h:m:s")
			);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$insertResponse->recordCount = $insertResponse->recordCount+1;
			} else {
				trigger_error("GBSProvider.insertPassthrough failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
			
		}
		return $insertResponse;
	}
	
	public function invoiceTool_getPassthrough($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_passthrough WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		
		$orderBy = " ORDER BY Date, JobID";
		if ($criteria->accountID){
			$SqlString .= " AND AccountID=?";
			$filterParams[] = $criteria->accountID;
			$filterBinding .= "s";
		}
		if ($criteria->recordedDate){
			$SqlString .= " AND UpdatedLastDate BETWEEN ? AND ?";
			$filterParams[] = date("Y-m-01 00:00:00",strtotime($criteria->recordedDate));
			$filterParams[] = date("Y-m-t 23:59:59",strtotime($criteria->recordedDate));
			$filterBinding .= "ss";
		}
		
		$SqlString .= $orderBy;
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getPassthrough failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
	public function invoiceTool_getStaticBullets($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_bullets WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		
		$orderBy = " ORDER BY GBSBullet_ID";
		if ($criteria->reportName){
			$SqlString .= " AND GBSBullet_Report=?";
			$filterParams[] = $criteria->reportName;
			$filterBinding .= "s";
		}
		if (!$criteria->includeAllStatus){
			$SqlString .= " AND GBSBullet_Status=?";
			$filterParams[] = 'Active';
			$filterBinding .= "s";
		}
	
		$SqlString .= $orderBy;
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getStaticBullets failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
    private static $INSERT_STATICBULLET_SQL = "
		INSERT INTO gbs_bullets
		(
				GBSBullet_Report,GBSBullet_InvoiceCode,GBSBullet_Content,GBSBullet_Status,GBSBullet_AddedByAdminID
		) VALUES (
			?,?,?,?,?
		)";
		 
	public function addStaticBullet($record,$adminUserID){
        $sqlString = self::$INSERT_STATICBULLET_SQL;
        $sqlBinding = "sssss";
		
		$sqlParam = array(
			$record->reportName,
			$record->invoiceCode,
			$record->displayText,
			$record->status,
			$adminUserID
		);
		
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$insertResponse->recordCount = $insertResponse->recordCount+1;
			} else {
				trigger_error("GBSProvider.addStaticBullet failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
		return $insertResponse;
	}

    private static $UPDATE_STATICBULLETS_SQL = "
        UPDATE gbs_bullets
        SET
			GBSBullet_Report = ?,
			GBSBullet_InvoiceCode = ?,
			GBSBullet_Content = ?,
			GBSBullet_Status = ?
		WHERE GBSBullet_ID = ?";

    public function updateStaticBullet($record, $adminUserId){
        if ($record->id){
			$updateResponse = new ExecuteResponseRecord();
            $sqlString = self::$UPDATE_STATICBULLETS_SQL;
            $sqlBinding = "ssssi";
            $sqlParams = array(
				trim($record->reportName),
				trim($record->invoiceCode),
				trim($record->displayText),
				trim($record->status),
				$record->id
            );

            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("GBSProvider.updateStaticBullet failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
    private static $INSERT_PREINVOICED_SQL = "
		INSERT INTO gbs_preinvoiced
		(
			GBSPreInvoiced_SalesforceID,GBSPreInvoiced_AccountName,GBSPreInvoiced_InvoiceDate,GBSPreInvoiced_Type,GBSPreInvoiced_IncentiveAmount
		) VALUES (
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          GBSPreInvoiced_UpdatedLastDate = ?
		 ";
		 
	public function insertPreInvoiced($record){
        $sqlString = self::$INSERT_PREINVOICED_SQL;
        $sqlBinding = "ssssss";
		
		$sqlParam = array(
			$record["SalesforceID"],
			$record["AccountName"],
			$record["InvoiceDate"],
			$record["Type"],
			$record["IncentiveAmount"],
			date("Y-m-d h:m:s")
			);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$insertResponse->recordCount = $insertResponse->recordCount+1;
			} else {
				trigger_error("GBSProvider.insertPreInvoiced failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
		return $insertResponse;
	}

	public function invoiceTool_getPreInvoiced($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_preinvoiced WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		
		$orderBy = " ORDER BY GBSPreInvoiced_AccountName";
		if ($criteria->salesForceID){
			$filterBinding .= "s";
			$filterParams[] = $criteria->salesForceID;
			$SqlString .= " AND GBSPreInvoiced_SalesforceID = ?";
		}
	
		$SqlString .= $orderBy;
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getPreInvoiced failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	public function invoiceTool_getCorridor(){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_corridor WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getCorridor failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	public function invoiceTool_getGeoBilling(){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM geobilling_definitions WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getGeoBilling failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	public function invoiceTool_getProductInfo(){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT GHSInventoryProducts_Description as Description,GHSInventoryProducts_EFI as EFI,GHSInventoryProducts_MeasureName as MeasureName, GHSInventoryProducts_BGasName as BGasName, GHSInventoryProducts_Cost as Cost FROM ghs_inventory_products WHERE 1=1";
		$filterBinding = "";
		$filterParams = array();
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.invoiceTool_getPreInvoiced failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	public function pos_getItems($criteria,$extraSFID){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT ID, Description, ItemLookupCode, SubDescription2, SubDescription3, ParentItem FROM pos_item WHERE 1=1";
		$filterString = "";
		$filterBinding = "";
		$filterParams = array();
		
		if ($criteria->SFOPID){
			$filterString .= " AND SubDescription3=?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->SFOPID;
		}
		if ($criteria->posItemID){
			$filterString .= " AND ID=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->posItemID;
		}
		if ($criteria->posParentID){
			$filterString .= " AND ParentItem=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->posParentID;
		}
		if ($criteria->SFItemID){
			$filterString .= " AND SubDescription2=?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->SFItemID;
		}
		if ($criteria->SFItemIDs){
			$itemList = "'".implode("','",$criteria->SFItemIDs)."'";
			$itemListAlt = str_replace($extraSFID,"",$itemList);
			$filterString .= " AND (SubDescription2 IN (".$itemList.") OR SubDescription2 IN (".$itemListAlt."))";
		}
		if ($criteria->posItemIDs){
			$itemList = "'".implode("','",$criteria->posItemIDs)."'";
			$itemListAlt = str_replace($extraSFID,"",$itemList);
			$filterString .= " OR ID IN (".$itemList.")";
		}
				
		$SqlString .=$filterString;
		
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
//				print_pre($row);
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.pos_getItems failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
	public function pos_checkSoldItems($criteria){
		$queryResponse = new QueryResponseRecord();
		
		$SqlString = "SELECT 
			pos_transactionentry.ID, FullPrice, pos_transactionentry.Price, pos_transactionentry.Quantity, SalesTax, 
			ComputedQuantity, TransactionNumber, TransactionTime, VoucherID, pos_transactionentry.ItemID, 
			pos_item.ItemLookupCode, pos_item.SubDescription2, pos_item.SubDescription3, pos_item.ParentItem 
			FROM pos_transactionentry JOIN pos_item ON pos_item.ID=pos_transactionentry.ItemID WHERE 1=1";
			
			
		$filterString = "";
		$filterBinding = "";
		$filterParams = array();
		if ($criteria->withParentIdorSubDescription2){
			$filterString .= "  AND (pos_item.ParentItem > 0 OR pos_item.SubDescription2 !='')";
		}
		
		if ($criteria->posItemIDs){
			$itemList = implode(",",$criteria->posItemIDs);
			$filterString .= " AND ItemID IN (".$itemList.")";
		}
		if ($criteria->startDate){
			//			TransactionTime BETWEEN '2017-02-15 12:22' AND '2017-02-15 13:00'";

			$filterString .= " AND TransactionTime BETWEEN '".$criteria->startDate."' AND '".$criteria->endDate."'";
			$filterBinding = "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
			
		}
		$SqlString .=$filterString;
		//echo $SqlString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.pos_checkSoldItems failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
    private static $INSERT_ECOBBCOMMISSION_SQL = "
		INSERT INTO ecobb_commissions
		(
			Owner, FiscalYear, Month, Type, Amt, LockedByID
		) VALUES (
			?,?,?,?,?,?
		)";
		 
	public function insertEcoBBCommissions($record,$adminId){
        $sqlString = self::$INSERT_ECOBBCOMMISSION_SQL;
        $sqlBinding = "ssssds";
		
		$sqlParam = array(
			$record["Owner"],
			$record["FiscalYear"],
			$record["Month"],
			$record["Type"],
			$record["Amt"],
			$adminId
			);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
				$insertResponse->recordCount = $insertResponse->recordCount+1;
			} else {
				trigger_error("GBSProvider.insertEcoBBCommissions failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
		return $insertResponse;
	}

	public function getLockedEcoBBCommissions($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM ecobb_commissions WHERE 1=1";
		$filterString = "";
		$filterBinding = "";
		$filterParams = array();
		
		if ($criteria->FiscalMonth){
			$filterString .= " AND FiscalYear = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->FiscalYear;
		}
		
		$SqlString .= $filterString;
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		$result->counted = 0;
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->counted = $result->counted+1;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.pos_checkSoldItems failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
    private static $INSERT_RWTRACKER_SQL = "
		INSERT INTO gbs_rw_reporttracker
		(
			Type,
			YearMonth,
			ItemCount,
			Items,
			CurrentYearMonth,
			FiscalYear
		) VALUES (
			?,?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          ItemCount = ?,
		  Items = ?
		 ";
	
	public function insertRWTracker($record){
        $sqlString = self::$INSERT_RWTRACKER_SQL;
        $sqlBinding = "ssisssis";
		
		$sqlParam = array(
			$record->Type,
			$record->YearMonth,
			$record->ItemCount,
			$record->Items,
			$record->CurrentYearMonth,
			$record->FiscalYear,
			$record->ItemCount,
			$record->Items
		);
		$sqlParams[] = $sqlParam;
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("GBSProvider.insertRWTracker failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
	public function getRWTracker($criteria){
		$queryResponse = new QueryResponseRecord();
		$SqlString = "SELECT * FROM gbs_rw_reporttracker WHERE 1=1";
		$filterString = "";
		$filterBinding = "";
		$filterParams = array();
		
		if ($criteria->FiscalYear){
			$filterString .= " AND FiscalYear = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->FiscalYear;
		}
		
		$SqlString .= $filterString;
		//echo $SqlString.print_r($filterParams,true);
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		$result->counted = 0;
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result->counted = $result->counted+1;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("GBSProvider.getRWTracker failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		return $result;
	}
	
}