<?php
/**
 * For the CMS
 */
 
include_once("Helper.php");
include_once("ReferralCodeHelper.php");
include_once("_getRootFolder.php");
include_once($rootFolder."repository/files/ImageFileManager.php");

include_once("EmailTemplateProvider.php");
include_once("_paginatedDataProvider.php");

class FileLibraryProvider implements UploadedFileMetadataHandler{
    protected static $SAVED = 'ACTIVE';
    protected static $UPLOADING = 'PENDING';
    protected static $DELETED = 'DELETED';
    protected $mysqli;
    protected $currentUserId;

    public function __construct($mysqli, $adminUserId){
        $this->mysqli = $mysqli;
        $this->currentUserId = $adminUserId; // this is actually bad form, but since we know we don't use cached object instances, we'll cheat
    }

    private static $GET_SQL = "
        SELECT
            id,
            file_name,
            folder_url,
            file_size,
            file_type,
            width,
            height,
            error,
            title,
            description,
            keywords,
            status,
            created_date,
            created_by,
            updated_date,
            updated_by
        FROM cms_image_library
        WHERE id = ? OR 0 = ?
        AND status != 'DELETED'
        AND file_type NOT LIKE 'image/%'
        ORDER BY id desc";

    /**
     * If id is not supplied, this returns all files, including images from the image library
     * @param int $id
     * @return array
     */
    public function getFiles($id = 0){
        if (!is_numeric($id)){
            $id = 0;
        }
        $sqlString = self::$GET_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($id, $id);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $collection = array();
        foreach ($result as $row){
            $collection[] = (object)$row;
        }
        return $collection;
    }

    private static $INSERT_SQL = "
        INSERT INTO cms_image_library
        (
            file_name,
            folder_url,
            file_size,
            file_type,
            width,
            height,
            error,

            title,
            description,
            keywords,
            status,
            created_by
        )
        VALUES(
            ?,?,?,?,?,?,?,
            ?,?,?,?,?
        )";

    private function insert(MediaLibraryFile $fileInfo, UploadFileManagerOptions $options){
        $formData = $fileInfo->formValuesArray;
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "ssssiissssss";
        $sqlParams = array(
            $fileInfo->fileName,
            $options->url,
            $fileInfo->size,
            $fileInfo->type,
            $fileInfo->width,
            $fileInfo->height,
            $fileInfo->error,
            $formData['title']."",
            $formData['description']."",
            $formData['keywords']."",
            self::$UPLOADING,
            $this->currentUserId ? $this->currentUserId : 0
        );
        $response = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("FileLibraryProvider.insert error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        return $response;
    }

    private static $UPDATE_SQL = "
        UPDATE cms_image_library
        SET
            file_name = ?,
            file_size = ?,
            file_type = ?,
            width = ?,
            height = ?,
            error = ?,
            title = ?,

            description = ?,
            keywords = ?,
            status = ?,
            updated_date = CURRENT_TIMESTAMP,
            updated_by = ?
        WHERE id = ?";

    private function update(MediaLibraryFile $fileInfo){
        $formData = $fileInfo->formValuesArray;
        $sqlString = self::$UPDATE_SQL;
        $sqlBinding = "ssssiisssssi";
        $sqlParams = array(
            $fileInfo->fileName,
            $fileInfo->size,
            $fileInfo->type,
            $fileInfo->width,
            $fileInfo->height,
            $fileInfo->error,
            $formData['title']."",

            $formData['description']."",
            $formData['keywords']."",
            self::$SAVED,
            $this->currentUserId,
            $fileInfo->formValuesArray['id']
        );
        $response = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("FileLibraryProvider.update error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        return $response;
    }

    private static $UPDATE_USER_DATA_SQL = "
        UPDATE cms_image_library
        SET
            title = ?,
            description = ?,
            keywords = ?,
            updated_date = CURRENT_TIMESTAMP,
            updated_by = ?
        WHERE id = ?";

    //TODO: roll up user data into an object type
    public function updateUserData($id, $title, $description, $keywords){
        $sqlString = self::$UPDATE_USER_DATA_SQL;
        $sqlBinding = "ssssi";
        $sqlParams = array($title, $description, $keywords, $this->currentUserId, $id);
        $response = new ExecuteResponseRecord();
        MySqliHelper::executeWithLogging("FileLibraryProvider.updateUserData error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        return $response;
    }

    private static $EXISTS_BY_NAME_SQL = "
        SELECT 1
        FROM cms_image_library
        WHERE lower(folder_url) = lower(?)
        AND lower(file_name) = lower(?)
        AND id != ?
        AND file_type = ?
        AND status != 'DELETED'";

    private function isDuplicate($id, $folderUrl, $filename, $fileType){
        $sqlString = self::$EXISTS_BY_NAME_SQL;
        $sqlBinding = "ssis";
        $sqlParams = array($folderUrl, $filename, $id, $fileType);
        $result = MySqliHelper::getResultWithLogging("FileLibraryProvider.isDuplicate error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return count($result->resultArray) > 0;
    }

    private function doPreUploadCallback(MediaLibraryFile $fileInfo, UploadFileManagerOptions $options){
        // we check to make sure the filename remains the same if the file in question already has a record in the cms
        $formData = $fileInfo->formValuesArray;
        $id = 0; // default for new record
        $origFileName = $fileInfo->fileName; // before we attempt to change it
        $defaultFileName = $origFileName; // for a new file
        $title = $formData['title'];
        if (!empty($title)){
            $defaultFileName = PendingUploadFile::sanitizedFilenameFromText($title, $origFileName);
        }
        // look for existing record
        if (!empty($formData['id']) && is_numeric($formData['id'])){
            $id = (int)$formData['id'];
            $matchingRecords = $this->getFiles($id);
            if (count($matchingRecords) == 1){
                // record exists, update the filename
                // TODO: handle case where folder_url is changing? In theory, if a folderURL is changing, this should be treated as a new record.
                // for now, we don't deal with this comprehensively
                //$folderUrl = $matchingRecords[0]['folder_url'];
                $fileInfo->fileName = $matchingRecords[0]->file_name;
            }
        }
        // now do the dupe check
        $filename = $fileInfo->fileName;
        if (!isset($folderUrl)){
            $folderUrl = $options->url;
        }
        $firstCheck = true;
        while ($this->isDuplicate($id, $folderUrl, $filename, $fileInfo->type)){
            if ($firstCheck && $filename != $defaultFileName){
                $firstCheck = true;
                $filename = $defaultFileName;
            } else {
                $filename .= "_"; // HACK: append a single underscore to de-dupe so the name remains largely the same.
            }
        }
        $fileInfo->fileName = $filename;
        // now save a pending row in the database and set the id on the fileinfo
        if ($id == 0){
            $response = $this->insert($fileInfo, $options);
            if ($response->success){
                $fileInfo->formValuesArray['id'] = $response->insertedId;
            } else {
                throw new Exception($response->error);
            }
        }
    }

    public function preUploadCallback(PendingUploadFile $fileInfo, UploadFileManagerOptions $options)
    {
        if ($fileInfo instanceof MediaLibraryFile){
            try {
                $this->doPreUploadCallback($fileInfo, $options);
            } catch (Exception $e) {
            }
        }
    }

    private function doPostUploadCallback(MediaLibraryFile $fileInfo, UploadFileManagerOptions $options){
        $response = $this->update($fileInfo);
        if (!$response->success){
            throw new Exception($response->error);
        }
    }

    public function postUploadCallback(PendingUploadFile $fileInfo, UploadFileManagerOptions $options)
    {
        if ($fileInfo instanceof MediaLibraryFile){
            try {
                $this->doPostUploadCallback($fileInfo, $options);
            } catch (Exception $e) {
            }
        }
    }

    private static $DELETE_SQL = "
        UPDATE cms_image_library
        SET status = 'DELETED',
          updated_by = ?
        WHERE lower(file_name) = lower(?)
        AND lower(folder_url) = lower(?)";

    public function preDeleteCallback($filename, UploadFileManagerOptions $options){
        $sqlString = self::$DELETE_SQL;
        $sqlBinding = "sss";
        $sqlParams = array($this->currentUserId, $filename, rtrim($options->url,"/"));
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        if (!$response->success){
            throw new Exception($response->error);
        }
		//return print_r($response,true);
    }

    public function postDeleteCallback($filename, UploadFileManagerOptions $options){
        return; // nothing to do
    }

    //
    // Pagination queries
    //

    private static $PAGINATED_SELECT_COLUMNS = "
      SELECT
            id,
            file_name,
            folder_url,
            file_size,
            file_type,
            width,
            height,
            error,
            title,
            description,
            keywords,
            status,
            created_date,
            created_by,
            updated_date,
            updated_by";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM cms_image_library
        WHERE status != 'DELETED'";

    private static $FILTER_ON_KEYWORD = "
        AND (
            file_name LIKE ?
            OR title LIKE ?
            OR description LIKE ?
            OR keywords LIKE ?
        )";
    private static $FILTER_ON_KEYWORD_EQUAL = "
        AND keywords = ?";
		
	private static $FILTER_ON_LOCATION_HR = "
		AND folder_url LIKE '%hr/uploads%'";

    private static $FILTER_ON_ID = "
        AND id = ?";

    private static $FILTER_ON_IMAGES = "
        AND file_type like 'image/%'";

    private static $FILTER_ON_NO_IMAGES = "
        AND file_type not like 'image/%'";

    public function get(PaginationCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }
        if ($criteria->keywordEqual){
			$filterString .= self::$FILTER_ON_KEYWORD_EQUAL;
            $filterBinding .= "s";
            $filterParams[] = $criteria->keywordEqual;
        }
        if ($criteria->location){
			if (str_replace(";","",$criteria->location) == "hr"){
				$filterString .= self::$FILTER_ON_LOCATION_HR;
			}
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        if ($criteria instanceof MediaCriteria){
            if ($criteria->isImage){
                $filterString .= self::$FILTER_ON_IMAGES;
            } else {
                $filterString .= self::$FILTER_ON_NO_IMAGES;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = $this->mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("FileLibraryProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("FileLibraryProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }

    protected function mapToRecord($dbRow, MediaRecord &$record = null){
        if ($record == null){
            $record = new MediaRecord();
        }
        $record->id = $dbRow->id;
        $record->fileName = $dbRow->file_name;
        $record->folderUrl = rtrim($dbRow->folder_url, '/') . '/';
        $record->fileSize = $dbRow->file_size;
        $record->fileType = $dbRow->file_type;
        $record->error = $dbRow->error;
        $record->title = $dbRow->title;
        $record->description = $dbRow->description;
        $record->keywords = $dbRow->keywords;
        $record->status = $dbRow->status;
        $record->createdDate = $dbRow->created_date;
        $record->createdBy = $dbRow->created_by;
        $record->updatedDate = $dbRow->updated_date;
        $record->updatedBy = $dbRow->updated_by;
        return $record;
    }

    private static $ORDER_BY_ID = " ORDER BY id";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$ORDER_BY_ID." desc";
        return $orderBy;
    }
}

class ImageLibraryProvider extends FileLibraryProvider{

    private static $GET_SQL = "
        SELECT
            id,
            file_name,
            folder_url,
            file_size,
            file_type,
            width,
            height,
            error,
            title,
            description,
            keywords,
            status,
            created_date,
            created_by,
            updated_date,
            updated_by
        FROM cms_image_library
        WHERE id = ? OR 0 = ?
        AND status != 'DELETED'
        AND file_type LIKE 'image/%'
        ORDER BY id desc";

    public function getFiles($id = 0){
        return $this->getImages($id);
    }

    public function getImages($id = 0){
        if (!is_numeric($id)){
            $id = 0;
        }
        $sqlString = self::$GET_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($id, $id);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $collection = array();
        foreach ($result as $row){
            $collection[] = (object)$row;
        }
        return $collection;
    }

    public function get(PaginationCriteria $criteria){
        $mediaCriteria = $criteria;
        if (!($criteria instanceof MediaCriteria)){
            $mediaCriteria = new MediaCriteria($criteria);
        }
        $mediaCriteria->isImage = true;
        return parent::get($mediaCriteria);
    }

    protected function  mapToRecord($dbRow, MediaRecord &$record = null){
        $record = new ImageRecord();
        parent::mapToRecord($dbRow, $record);
        $record->width = $dbRow->width;
        $record->height = $dbRow->height;
        return $record;
    }
}

class SocialMessageCriteria extends PaginationCriteria{
    public $id;
    public $relevantEntity;
    public $relevantEntityId = 0;
    public $eventId;
    public $campaignId;
    public $isPublicQuery;

    public function prepForPublicQuery(){
        $this->isPublicQuery = true;
        $this->multiColumnSort[] = new MultiColumnSort("sortOrder", "asc");
        $this->multiColumnSort[] = new MultiColumnSort("type", "asc");
        $this->multiColumnSort[] = new MultiColumnSort("relevant_id", "desc");
        if ($this->relevantEntity == SocialMessageSample::EVENT_ENTITY && $this->relevantEntityId > 0){
            $this->eventId = $this->relevantEntityId;
            return;
        }
        if ($this->relevantEntity == SocialMessageSample::CAMPAIGN_ENTITY && $this->relevantEntityId > 0){
            $this->campaignId = $this->relevantEntityId;
            return;
        }
        $this->relevantEntity = null;
    }
}

class SocialMessagingSampleProvider{
    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            sm.id,
            sm.type,
            sm.message,
            sm.relevant_entity,
            sm.relevant_entity_id,
            sm.status,
            sm.sort_order,
            sm.created_date,
            sm.created_by,
            sm.updated_date,
            sm.updated_by";

    private static $FROM_TABLE_CLAUSE = "
        FROM social_messaging_samples sm
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND sm.id = ?";

    private static $FILTER_ON_ENTITY = "
        AND sm.relevant_entity = ?
        AND sm.relevant_entity_id = 0";

    private static $FILTER_ON_ENTITY_ID = "
        AND sm.relevant_entity = ?
        AND sm.relevant_entity_id = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            sm.type LIKE ?
            OR sm.message LIKE ?
            OR sm.status LIKE ?
        )";

    private static $PUBLIC_FROM_TABLE_CLAUSE = "
        FROM social_messaging_samples sm
        WHERE sm.status = 'ACTIVE'";

    private static $PUBLIC_FILTER_ON_EVENT_ID = "
        AND ( 1 = 0
        OR sm.relevant_entity = ?
        AND ( sm.relevant_entity_id = 0 OR sm.relevant_entity_id = ? ) )";

    private static $PUBLIC_FILTER_ON_CAMPAIGN_ID = "
        AND ( 1 = 0
        OR sm.relevant_entity = ?
        AND sm.relevant_entity_id = ? )";

    public function get(SocialMessageCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else if ($criteria->isPublicQuery){
            $fromTableClause = self::$PUBLIC_FROM_TABLE_CLAUSE;
            if (is_numeric($criteria->eventId) && $criteria->eventId > 0){
                $filterString .= self::$PUBLIC_FILTER_ON_EVENT_ID;
                $filterBinding = "si";
                $filterParams[] = SocialMessageSample::EVENT_ENTITY;
                $filterParams[] = $criteria->eventId;
            } else if (is_numeric($criteria->campaignId) && $criteria->campaignId > 0){
                $filterString .= self::$PUBLIC_FILTER_ON_CAMPAIGN_ID;
                $filterBinding = "si";
                $filterParams[] = SocialMessageSample::CAMPAIGN_ENTITY;
                $filterParams[] = $criteria->campaignId;
            }
        } else {// datatables support

            if ($criteria->relevantEntityId && is_numeric($criteria->relevantEntityId)){
                $filterString .= self::$FILTER_ON_ENTITY_ID;
                $filterBinding .= "si";
                $filterParams[] = $criteria->relevantEntity;
                $filterParams[] = $criteria->relevantEntityId;
            } else if ($criteria->relevantEntity){
                $filterString .= self::$FILTER_ON_ENTITY;
                $filterBinding .= "s";
                $filterParams[] = $criteria->relevantEntity;
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("SocialMessagingSampleProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("SocialMessagingSampleProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY sm.type ASC, sm.sort_order ASC";

    private function getOrderByClause(SocialMessageCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY sm.id ".$sortOrder;
                    break;
                case "type":
                    $orderBy = " ORDER BY sm.type ".$sortOrder;
                    break;
                case "message":
                    $orderBy = " ORDER BY sm.message ".$sortOrder;
                    break;
                case "status":
                    $orderBy = " ORDER BY sm.status ".$sortOrder;
                    break;
                case "sortOrder":
                    $orderBy = " ORDER BY sm.sort_order ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "sm.id ".$sortOrder;
                        break;
                    case "relevant_id":
                        $orderByStrings[] = "sm.relevant_entity_id ".$sortOrder;
                        break;
                    case "type":
                        $orderByStrings[] = "sm.type ".$sortOrder;
                        break;
                    case "message":
                        $orderByStrings[] = "sm.message ".$sortOrder;
                        break;
                    case "status":
                        $orderByStrings[] = "sm.status ".$sortOrder;
                        break;
                    case "sortOrder":
                        $orderByStrings[] = "sm.sort_order ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return SocialMessageSampleFactory::buildFrom($dataRow);
    }

    // -----------------
    // Add
    // -----------------

    private static $INSERT_SOCIAL_MESSAGE_SQL = "
        INSERT INTO social_messaging_samples
        (
          type,
          message,
          relevant_entity,
          relevant_entity_id,
          status,
          sort_order,
          created_by
        )
        SELECT
          ?,?,?,?,?,?,?
        FROM DUAL";

    public function add(SocialMessageSample $record, $adminUserId){
        // save despite validity
        $validationMessage = $record->isValid() ? "" : "Message is too long.";

        $sqlString = self::$INSERT_SOCIAL_MESSAGE_SQL;
        $sqlBinding = "sssisii"; // insert
        $sqlParam = array(
            $record->type,
            $record->message,
            $record->relevantEntity,
            $record->relevantEntityId,
            $record->status,
            $record->sortOrder,
            $adminUserId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
        } else {
            trigger_error("SocialMessagingSampleProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        $insertResponse->message = $validationMessage;
        $insertResponse->record = $record;

        return $insertResponse;
    }

    private static $UPDATE_SOCIAL_MESSAGE = "
        UPDATE social_messaging_samples
        SET
          type = ?,
          message = ?,
          status = ?,
          sort_order = ?,
          updated_date = CURRENT_TIMESTAMP,
          updated_by = ?
        WHERE id = ?";

    public function update(SocialMessageSample $record, $adminUserId){
        $validationMessage = $record->isValid() ? "" : "Message is too long.";
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SOCIAL_MESSAGE;
            $sqlBinding = "sssiii";
            $sqlParams = array(
                $record->type,
                $record->message,
                $record->status,
                $record->sortOrder,
                $adminUserId,
                $record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("SocialMessagingSampleProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        $updateResponse->message = $validationMessage;
        return $updateResponse;
    }
}

class WordPressContentScrapeConfig{
    public $id;
    public $urlName;
    public $urlToScrape;
    public $pageTitle;
    public $pageHeader;
    public $pageSubheader;
    public $useCETDashCmsCredentials;

    public $createdBy;
    public $createdDate;
    public $updatedBy;
    public $updatedDate;

    public function isValid(){
        return strlen($this->urlName) > 0 && strlen($this->urlToScrape) > 0 && strlen($this->pageTitle) > 0 && strlen($this->pageHeader) > 0;
    }

    public function getUrlToScrape($cmsCredentials){
        if ($this->useCETDashCmsCredentials){
            $credentialsParts = explode(":", $cmsCredentials);
            if (count($credentialsParts) == 2){
                $urlParts = explode("://", $this->urlToScrape);
                $newUrl = $urlParts[0]."://".$cmsCredentials."@".$urlParts[1];
                return $newUrl;
            }
        }
        return $this->urlToScrape;
    }
}

class WordPressContentScrapeCriteria extends PaginationCriteria{
    public $urlName;
}

class WordPressContentScrapeConfigProvider extends PaginatedDataProvider{

    private static $PAGINATED_SELECT_COLUMNS = "
        SELECT
            id,
            url_name as urlName,
            url_to_scrape as urlToScrape,
            page_title as pageTitle,
            page_header as pageHeader,
            page_subheader as pageSubheader,
            use_ct_cms_credentials as useCETDashCmsCredentials,
            created_date as createdDate,
            updated_date as updatedDate";

    private static $PAGINATED_FROM_CLAUSE = "
        FROM cms_wordpress_scrape_config";

    /**
     * Returns a PaginatedQueryParameters object ready to be executed
     * @param PaginationCriteria $criteria
     * @return PaginatedQueryParameters
     */
    protected function getQueryParameters(PaginationCriteria $criteria)
    {
        $queryParameters = new PaginatedQueryParameters();
        $queryParameters->selectColumnsClause = self::$PAGINATED_SELECT_COLUMNS;
        $queryParameters->fromClause = self::$PAGINATED_FROM_CLAUSE;
        $queryParameters->filterString .= " WHERE 1 = 1";
        if (is_numeric($criteria->id)){
            $queryParameters->filterString .= " AND id = ?";
            $queryParameters->addFilterParameter("i", $criteria->id);
        } else if ($criteria instanceof WordPressContentScrapeCriteria){
            if ($criteria->urlName){
                $queryParameters->filterString .= " AND url_name = ?";
                $queryParameters->addFilterParameter("s", $criteria->urlName);
            }
        }
        return $queryParameters;
    }

    private static $KEYWORD_FILTER_CLAUSE = "
        AND (
            url_name LIKE ?
            OR url_to_scrape LIKE ?
            OR page_title LIKE ?
            OR page_header LIKE ?
            OR page_subheader LIKE ?
        )";

    /**
     * Returns a string that resembles the following:
     *
     *  AND (
     *      t.TagList_Name LIKE ?
     *      OR t.TagList_Description LIKE ?
     *      OR t.TagList_CampaignHeading LIKE ?
     *      OR t.TagList_CampaignSubHeading LIKE ?
     *      OR t.TagList_CampaignDescription LIKE ?
     *      OR t.TagList_URL LIKE ?
     *      OR t.TagList_CallToAction LIKE ?
     *  )
     * @return string
     */
    protected function getKeywordFilterClause()
    {
        return self::$KEYWORD_FILTER_CLAUSE;
    }

    /**
     * This is where the provider can map the ui field names to the db columns. A field name is what the ui passes back in reference to
     * a datatable column, and the db columns refer to a comma-delimited string of database column names that should be used in the sort.
     * Although in theory, multiple columns might be used to sort a single datatable column, this can have unexpected side effects with respect
     * to the ASC and DESC modifiers.
     *
     * EXAMPLE:
     *   return array(
     *      "id" => "id",
     *      "pageTitle" -> "page_title"
     *   )
     *
     * Be sure to use the db alias if your FROM clause requires it:
     *
     *   return array(
     *      "id" => "myTable.id",
     *      "pageTitle" => "myJoinTable.pageTitle"
     *   )
     *
     * @return array mapping UIFIeldNames to dbColumns
     */
    protected function getOrderByMapping()
    {
        return array(
            "id" => "id",
            "urlName" => "url_name",
            "urlToScrape" => "url_to_scrape",
            "pageTitle" => "page_title",
            "pageHeader" => "page_header",
            "pageSubheader" => "page_subheader",
            "createdDate" => "created_date",
            "updatedDate" => "updated_date"
        );
    }

    /**
     * Returns an object that represents the desired output object for the collection (e.g., contract record).
     * @param array $rowArray
     * @return mixed
     */
    protected function mapTo(array $rowArray)
    {
        $record = new WordPressContentScrapeConfig();
        foreach($rowArray as $dbField => $value){
            if (property_exists($record, $dbField)){
                $record->$dbField = $value;
            }
        }
        return $record;
    }

    //
    // Save
    //
    public function save(WordPressContentScrapeConfig $record){
        if ($record->id){
            return $this->update($record);
        }
        return $this->add($record);
    }

    private static $UPDATE_SQL = "
        UPDATE cms_wordpress_scrape_config
        SET url_name = ?,
            url_to_scrape = ?,
            page_title = ?,
            page_header = ?,
            page_subheader = ?,
            use_ct_cms_credentials = ?,
            updated_by_admin_id = ?,
            updated_date = CURRENT_TIMESTAMP
        WHERE
            id = ?";

    private function update(WordPressContentScrapeConfig $record){
        $sqlString = self::$UPDATE_SQL;
        $sqlBinding = "sssssiii";
        $sqlParams = array(
            $record->urlName."",
            $record->urlToScrape."",
            $record->pageTitle."",
            $record->pageHeader."",
            $record->pageSubheader."",
            $record->useCETDashCmsCredentials ? 1 : 0,
            getAdminId(),
            $record->id
        );
        return MySqliHelper::executeWithLogging(get_class($this).".update (".$sqlString.") error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $INSERT_SQL = "
        INSERT INTO cms_wordpress_scrape_config
        (
            url_name,
            url_to_scrape,
            page_title,
            page_header,
            page_subheader,
            use_ct_cms_credentials,
            created_by_admin_id
        )
        VALUES
        ( ?,?,?,?,?,?,? )";

    private function add(WordPressContentScrapeConfig $record){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "sssssii";
        $sqlParams = array(
            $record->urlName."",
            $record->urlToScrape."",
            $record->pageTitle."",
            $record->pageHeader."",
            $record->pageSubheader."",
            $record->useCETDashCmsCredentials ? 1 : 0,
            getAdminId()
        );
        return MySqliHelper::executeWithLogging(get_class($this).".add (".$sqlString.") error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}
?>