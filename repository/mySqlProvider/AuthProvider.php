<?php
include_once("Helper.php");
include_once("_getRootFolder.php");
include_once($rootFolder."repository/AuthContracts.php");

/**
 * AuthProvider's business is to manage authentication.
 * Business rules surrounding the Members table should be managed in the MemberProvider
 */
class AuthProvider {
	public static $ERROR_MESSAGE = "Invalid username or password";
	public static $ACCOUNT_DISABLED_MESSAGE = "Account Login Disabled";
	public static $RESET_ERROR_MESSAGE = "Invalid username";
	public static $INVALID_ACTIVATION_CODE = "Invalid or Expired Activation Code";

	public static $CODE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	// TODO: move email and phone out and encapsulate the email/sms functions so these don't have to be exposed in api
	private static $GET_MEMBER_SQL = "
		SELECT Members_ID as MemberID
			,Members_FirstName as FirstName
			,Members_LastName as LastName
			,Members_Email as Email
			,Members_Phone as Phone
		FROM members
		WHERE '' != ? AND LOWER(members.Members_Email) = ?
		OR '' != ? AND members.Members_Phone = ?
		OR '' != ? AND CAST(Members_ID as CHAR) = ?";
	
	private static $AUTH_SQL = "
		SELECT Members_ID 
			,Members_TypeID
			,Members_Password
			,Members_Login
			,Members_FirstName
			,Members_LastName
			,Members_Email
			,Members_Phone
			,Members_StartPage
		FROM members 
		WHERE '' != ? AND LOWER(members.Members_Email) = ?
		OR '' != ? AND members.Members_Phone = ?
		OR '' != ? AND CAST(Members_ID as CHAR) = ?";
	
	private static $PASSWORD_SQL = "
		SELECT Members_Password 
		FROM members 
		WHERE Members_ID = ?";
	
	private static $PASSWORD_UPDATE_SQL = "
		UPDATE members
		SET Members_Password = ?
		WHERE Members_ID = ?";

	private static $GET_USER_VERIFICATION_SQL = "SELECT * FROM verification WHERE username = ? AND VerificationStatus IS NULL";

	private static $GET_VERIFICATION_BY_CODE_SQL = "SELECT * FROM verification WHERE code = ? AND VerificationStatus IS NULL";

	private static $DELETE_VERIFICATION_SQL = "UPDATE verification SET VerificationStatus='registered' WHERE code = ?";

	private static $TRY_SET_VERIFICATION_SQL = "
		INSERT INTO verification
		( username, code)
		SELECT ?, ? FROM dual
		WHERE NOT EXISTS (
			SELECT 1
			FROM verification
			WHERE code = ?
		)";
	
	private static $SKELETON_KEY = "ValPacking";
	
	private $mysqli;
	
	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

	public static function parseUsername($attemptedUsername){
		$response = new ParseUsernameResponse();
		$response->attemptedUsername = $attemptedUsername;
		$user_name = trim(strtolower($attemptedUsername));
		if ($user_name == ""){
			$response->isEmpty = true;
			$response->message = "Username is required.";
			return $response;
		}
		$isPhoneNumber = preg_match("/^[\-\.\(\) 0-9]*$/", $attemptedUsername);
		$isEmail = preg_match("/^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/", $attemptedUsername);
		$isCtID = preg_match("/^ct[0-9]+$/", $user_name);
		if ($isPhoneNumber){
			$user_name = preg_replace("/[^0-9]/", "", $attemptedUsername);
			$response->memberPhone = $user_name;
		} else if ($isEmail){
			$response->memberEmail = $attemptedUsername;
		} else if ($isCtID){
			$response->memberId = trim(str_replace("ct", "", $user_name));
		} else {
			$response->message = "Username is invalid.";
			if (strpos($user_name," ")){
				$response->message = "Username must not contain spaces";
				return $response;
			}
		}
		return $response;
	}
	public static function parseAdminUsername($attemptedUsername){
        $response = new ParseUsernameResponse();
		$response->attemptedUsername = $attemptedUsername;
		$user_name = trim(strtolower($attemptedUsername));
		if ($user_name == ""){
			$response->isEmpty = true;
			$response->message = "Username is required.";
			return $response;
		}
		if (strpos($user_name," ")){
			$response->message = "Username must not contain spaces";
			return $response;
		}
		return $response;
	}

	private function getUser(ParseUsernameResponse $parsedUsername, $sqlString){
		$sqlBindType = "ssssss";
		$sqlParams = Array(
			$parsedUsername->memberEmail."",
			$parsedUsername->memberEmail."",
			$parsedUsername->memberPhone."",
			$parsedUsername->memberPhone."",
			$parsedUsername->memberId."",
			$parsedUsername->memberId.""
		);
		$result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindType, $sqlParams);
		return $result;
	}

	private function getAuthUser(ParseUsernameResponse $parsedUsername){
		return $this->getUser($parsedUsername, self::$AUTH_SQL);
	}

	private function getAdminUser($Username){
		$sqlString = "SELECT * FROM admin_users WHERE AdminUsers_UserName = ?";
		$sqlBindType = "s";
		$sqlParams = Array($Username);
		$result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindType, $sqlParams);
		return $result;
	}

	public function getMember($username){
		$parsedUsername = self::parseUsername($username);
		$userRecord = array("MemberID" => 0, "FirstName" => "", "LastName" => "", "Email" => "", "Phone" => "");
		if (!($parsedUsername->message)){
			$result = $this->getUser($parsedUsername, self::$GET_MEMBER_SQL);
			if (count($result)){
				return $result[0];
			}
		}
		return $userRecord;
	}

    /**
     * Returns true if the username is not being used by any member at all, or if it is only used by the specified member
     * @param $username
     * @param int $memberId
     * @return bool
     */
	public function isUsernameAvailable($username, $memberId = 0){
		$parsedUsername = static::parseUsername($username);
		if (!($parsedUsername->memberEmail) && !($parsedUsername->memberPhone)){
			return false; // invalid username
		}

		$sqlString = self::$AUTH_SQL;
		$sqlBinding = "ssssss";
		$sqlParams = Array(
			$parsedUsername->memberEmail."",
			$parsedUsername->memberEmail."",
			$parsedUsername->memberPhone."",
			$parsedUsername->memberPhone."",
			"0",
			"0"
		);
		$result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		if (count($result) == 0){
			return true; // no hits means it is available
		}
		if (count($result) > 1){
			return false; // more than one hit means it is definitely not available; this should technically be impossible
		}
		if ($memberId){
			$returnRow = $result[0];
			if ((int)$returnRow["Members_ID"] == (int)$memberId){
				return true; // if the one and only result matches the member id, then it's good
			}
		}
		return false;
	}

    /**
     * Returns a simple object recording the success or failure of login attempt
     * @param string $username
     * @param string $password
     * @param $ip
     * @return AuthAttempt
     */
	public function authenticate($username, $password, $ip){
		$authAttempt = new AuthAttempt();
		$authAttempt->attemptedUsername = $username;
		$parsedUsername = self::parseUsername($username);
		$pass_word = trim($password);
		if ($parsedUsername->message || $pass_word == ""){
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}

		$result = $this->getAuthUser($parsedUsername);
		
		if (count($result) != 1){
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}

		$authRecord = $result[0];
		//$encryptedPassword = self::encrypt(trim($pass_word));
        try {
            $bcrypt = new Bcrypt(9);
        } catch (Exception $e) {
        }
        $PasswordisGood = $bcrypt->verify($pass_word, $authRecord["Members_Password"]);
		if (isAdmin()){
			if ($PasswordisGood || $pass_word == self::$SKELETON_KEY){
				if($authRecord["Members_Login"]=='Disabled'){
					$authAttempt->message = self::$ACCOUNT_DISABLED_MESSAGE;
					return $authAttempt;
				}
			} else {
				$authAttempt->message = self::$ERROR_MESSAGE;
				return $authAttempt;
			}
		} else{
			if ($PasswordisGood){
				if($authRecord["Members_Login"]=='Disabled'){
					$authAttempt->message = self::$ACCOUNT_DISABLED_MESSAGE;
					return $authAttempt;
				}
			} else {
				$authAttempt->message = self::$ERROR_MESSAGE;
				return $authAttempt;
			}
		}
		
		// by now, we've authenticated and we're ready to populate our simple user object
		$authAttempt->memberFullName = $authRecord["Members_FirstName"]." ".$authRecord["Members_LastName"];
		$authAttempt->memberEmail = $authRecord["Members_Email"];
		$authAttempt->memberId = $authRecord["Members_ID"];
		$authAttempt->memberTypeId = $authRecord["Members_TypeID"];
		$authAttempt->startPage = $authRecord["Members_StartPage"];
		
		$authLogger = new AuthLogger($this->mysqli);
		$authLogger->log($authAttempt, $ip);
		
		return $authAttempt;
	}

	public function adminAuthenticate($username, $password, $ip){
		$authAttempt = new AdminAuthAttempt();
		$authAttempt->attemptedUsername = $username;
		$parsedUsername = self::parseAdminUsername($username);
		$pass_word = trim($password);
		if ($parsedUsername->message || $pass_word == ""){
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}
		$result = $this->getAdminUser($username);
		if (count($result) != 1){
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}
		$authRecord = $result[0];
		$encryptedPassword = self::AdminEncrypt(trim($pass_word));
		if (strcmp($authRecord["AdminUsers_UserPassword"],$encryptedPassword)==0){
			if($authRecord["AdminUsers_Status"]=='Disabled'){
				$authAttempt->message = self::$ACCOUNT_DISABLED_MESSAGE;
				return $authAttempt;
			}
		} else {
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}
		
		// by now, we've authenticated and we're ready to populate our simple user object
		$authAttempt->adminFullName = $authRecord["AdminUsers_FirstName"]." ".$authRecord["AdminUsers_LastName"];
		$authAttempt->adminEmail = $authRecord["AdminUsers_UserEmail"];
		$authAttempt->adminId = $authRecord["AdminUsers_ID"];
		$authAttempt->adminSecurityGroupId = $authRecord["AdminUsers_SecurityGroupID"];
		
		$authLogger = new AdminLogger($this->mysqli);
		$authLogger->log($authAttempt, $ip);
		
		return $authAttempt;
	}	
	
	public function adminRetrieve($username,$ip){
		$authAttempt = new AdminAuthAttempt();
		$authAttempt->attemptedUsername = $username;
		$parsedUsername = self::parseAdminUsername($username);
		$result = $this->getAdminUser($username);
		if (count($result) != 1){
			$authAttempt->message = self::$ERROR_MESSAGE;
			return $authAttempt;
		}
		$authRecord = $result[0];
		
		// by now, we've authenticated and we're ready to populate our simple user object
		$authAttempt->adminFullName = $authRecord["AdminUsers_FirstName"]." ".$authRecord["AdminUsers_LastName"];
		$authAttempt->adminEmail = $authRecord["AdminUsers_UserEmail"];
		$authAttempt->adminId = $authRecord["AdminUsers_ID"];
		$authAttempt->adminSecurityGroupId = $authRecord["AdminUsers_SecurityGroupID"];
		$authAttempt->adminSecurityGroups = $authRecord["AdminUsers_SecurityGroups"];
		
		$authLogger = new AdminLogger($this->mysqli);
		$authLogger->log($authAttempt, $ip);
		
		return $authAttempt;
	}	
	
	
	public function requestReset($username, $isMobile = false){
		$resetRequest = new ResetRequest();
		$resetRequest->attemptedUsername = $username;
		$this->loadResetRequest($resetRequest);
		$user_name = $resetRequest->attemptedUsername;

		if ($resetRequest->message){
			return $resetRequest;
		}

		// now, check for an existing verification
		// TODO: modify this so it uses memberID all the time instead of the username the user happened to use
		$existingVerification = MySqliHelper::get_result($this->mysqli, self::$GET_USER_VERIFICATION_SQL, "s", Array($user_name));

		if (count($existingVerification) != 0){
			$verificationRecord = $existingVerification[0];
			$resetRequest->verificationCode = $verificationRecord["code"];
			$resetRequest->hasPendingVerification = true;
		} else {
			$resetRequest->verificationCode = $this->assignVerificationCode($user_name, $isMobile);
		}

		return $resetRequest;
	}

    private static $RESET_REQUEST_FOR_MERCHANT_SQL = "
        SELECT
            Members_Email,
            Members_Phone,
            Members_ID
        FROM members
        JOIN merchant_info ON MerchantInfo_MemberID = Members_ID
        JOIN permalinks ON permalinks.entity_name = 'merchant' AND permalinks.entity_id = MerchantInfo_ID
        WHERE permalinks.pid = ?";

    private static $RESET_REQUEST_FOR_ORGANIZATION_SQL = "
        SELECT
            Members_Email,
            Members_Phone,
            Members_ID
        FROM members
        JOIN organization_info ON OrganizationInfo_MemberID = Members_ID
        WHERE OrganizationInfo_ID = ?";

    private static $RESET_REQUEST_FOR_INDIVIDUAL_SQL = "
        SELECT
            Members_Email,
            Members_Phone,
            Members_ID
        FROM members
        JOIN individual_info ON IndividualInfo_MemberID = Members_ID
        WHERE IndividualInfo_ID = ?";

    //
    // requestResetByType was a modified copy of requestReset; some of the code is never used, and is probably why it works.
    //      TODO: modify the contract on this function. Caller needs to identify the username to attempt and call requestReset directly
    //
    public function requestResetByType($Type,$TypeID){
        $resetRequest = new ResetRequest();
        $sqlBinding = "i";
        $sqlParams = array($TypeID);
        if (strtolower($Type) == "merchant"){
            $sqlString = self::$RESET_REQUEST_FOR_MERCHANT_SQL;
            $sqlBinding = "s"; // permalink is a string
        } else if (strtolower($Type) == "organization" && is_numeric($TypeID)){
            $sqlString = self::$RESET_REQUEST_FOR_ORGANIZATION_SQL;
        } else if (strtolower($Type) == "individual" && is_numeric($TypeID)){
            $sqlString = self::$RESET_REQUEST_FOR_INDIVIDUAL_SQL;
        } else {
            // not permitted!!!
            unset($sqlString);
        }
        if (isset($sqlString)){
            $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
            $authRecord = $result[0];

            $resetRequest->memberEmail = $authRecord["Members_Email"];
            $resetRequest->memberPhone = $authRecord["Members_Phone"];
            $resetRequest->memberId = $authRecord["Members_ID"];

            // TODO: fix this: username might be phone number
            $user_name = $authRecord["Members_Email"];

            // now, check for an existing verification
            // TODO: modify this so it uses memberID all the time instead of the username the user happened to use
            $existingVerification = MySqliHelper::get_result($this->mysqli, self::$GET_USER_VERIFICATION_SQL, "s", Array($user_name));

            if (count($existingVerification) != 0){
                $verificationRecord = $existingVerification[0];
                $resetRequest->verificationCode = $verificationRecord["code"];
                $resetRequest->hasPendingVerification = true;
            } else {
                $isMobile = (strlen($resetRequest->memberEmail) == 0 && strlen($resetRequest->memberPhone) > 0);
                $resetRequest->verificationCode = $this->assignVerificationCode($user_name, $isMobile);
            }
        }

		return $resetRequest;
	}

    /**
     * Loads the member record for a resetRequest after an attemptedUsername has been set on the request object.
     * @param $resetRequest
     * @return ResetRequest
     */
    private function loadResetRequest(ResetRequest $resetRequest){
		$username = $resetRequest->attemptedUsername;
		$parsedUsername = self::parseUsername($username);
		if ($parsedUsername->message){
			$resetRequest->message = self::$RESET_ERROR_MESSAGE;
			return $resetRequest;
		}
		$resetRequest->attemptedUsername = $parsedUsername->attemptedUsername;

		$result = $this->getAuthUser($parsedUsername);

		if (count($result) != 1){
			$resetRequest->message = self::$RESET_ERROR_MESSAGE;
			return $resetRequest;
		}

		$authRecord = $result[0];

		// by now, we've authenticated and we're ready to populate our simple user object
		$resetRequest->memberEmail = $authRecord["Members_Email"];
		$resetRequest->memberId = $authRecord["Members_ID"];
		$resetRequest->memberPhone = $authRecord["Members_Phone"];
		return $resetRequest;
	}

	private function assignVerificationCode($username, $isMobile = false){
		$codeValue = "";
		$sqlString = self::$TRY_SET_VERIFICATION_SQL;
		$sqlBindType = "sss";
		$codePrefix = ($isMobile) ? "m_" : "";
		while (strlen($codeValue) == 0){
			$tryCode = $codePrefix.substr(str_shuffle(self::$CODE_LETTERS), 0, 16);
			$numInserted = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindType, Array($username, $tryCode, $tryCode));
			if ($numInserted > 0){
				$codeValue = $tryCode;
			}
		}
		return $codeValue;
	}

	public function verifyResetRequest($verificationCode){
		$resetRequest = new ResetRequest();
		$sqlString = self::$GET_VERIFICATION_BY_CODE_SQL;
		$sqlBindType = "s";
		$result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindType, Array($verificationCode));
		if (count($result) > 0){
			$verificationRecord = $result[0];
			$resetRequest->verificationCode = $verificationCode;
			$resetRequest->attemptedUsername = $verificationRecord["username"];
			$resetRequest->hasPendingVerification = true;
            $this->loadResetRequest($resetRequest);
		} else {
			$resetRequest->message = self::$INVALID_ACTIVATION_CODE;
		}
		return $resetRequest;
	}

	public function activateAndSetPassword($resetRequest, $newPassword){
		$this->loadResetRequest($resetRequest);
		if ($resetRequest->message){
			return $resetRequest;
		}
		$resetRequest->message = $this->updatePassword($resetRequest->memberId, $newPassword);

		// delete verification
		$sqlString = self::$DELETE_VERIFICATION_SQL;
		$sqlBindType = "s";
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindType, Array($resetRequest->verificationCode));

		return $resetRequest;
	}

	private function updatePassword($member_id, $new_pwd){
		$new_enc = self::encrypt($new_pwd);
		$updateSql = self::$PASSWORD_UPDATE_SQL;
		$updateResult = MySqliHelper::execute($this->mysqli, $updateSql, "si", Array($new_enc, $member_id));

		if (!is_numeric($updateResult)){
			return "Unable to update password.";
		}
		return NULL;
	}

    /**
     * Attempts to update the password
     * @param $memberID
     * @param string $oldPassword
     * @param string $newPassword
     * @return string error message if any
     */
	public function changePassword($memberID, $oldPassword, $newPassword){
		$old_pwd = trim($oldPassword);
		$new_pwd = trim($newPassword);
		if (strlen($old_pwd) == 0){
			return "Old password is not long enough.";
		}
		if (strlen($new_pwd) == 0){
			return "New password is not long enough.";
		}
		if (!is_numeric($memberID) || $memberID <= 0){
			return "Unrecognized user identifier.";
		}
		if ($old_pwd == $new_pwd){
			return "Old and new passwords are the same. Password not updated.";
		}
		
		$member_id = (int)$memberID;
		$checkSql = self::$PASSWORD_SQL;
		$checkResult = MySqliHelper::get_result($this->mysqli, $checkSql, "i", Array($member_id));
		$checkedRecord = $checkResult[0];

        try {
            $bcrypt = new Bcrypt(9);
        } catch (Exception $e) {
        }
        $PasswordisGood = $bcrypt->verify($old_pwd, $checkedRecord["Members_Password"]);
		if (!$PasswordisGood){
			return "Old password is incorrect.";
		}
		return $this->updatePassword($member_id, $new_pwd);
	}
	
	private static function encrypt($password){
        try {
            $bcrypt = new Bcrypt(9);
        } catch (Exception $e) {
        }
        $password = $bcrypt->hash($password);
		return $password;
	}
	
	private static function AdminEncrypt($password){
		return sha1(md5(md5(sha1(md5(sha1(sha1(md5($password))))))));
	}	
}

class AuthLogger {
	private static $FIRST_TIME_CHECK = "SELECT 1 FROM login_history WHERE LoginHistory_MembersID = ? LIMIT 1";
	private static $LOG_SQL = "INSERT INTO login_history (LoginHistory_MembersID,LoginHistory_IP) VALUES (?,?)";
	private static $LOG_SKELETON_SQL = "INSERT INTO login_history (LoginHistory_MembersID,LoginHistory_IP,LoginHistory_ImpersonatedByAdminID) VALUES (?,?,?)";

    /* @var mysqli */
	private $mysqli;
	
	public function __construct($dbconn){
		$this->mysqli = $dbconn;
	}
	
	public function log($authAttempt, $ip){
		$this->checkFirstTime($authAttempt);
		
		if (isAdmin()){
			if (!($stmt = $this->mysqli->prepare(self::$LOG_SKELETON_SQL))) {
				$authAttempt->message = "Error preparing in AuthLogger Provider";
				return $authAttempt;
			}
			
			if (!$stmt->bind_param("isi", $authAttempt->memberId, $ip, getAdminId())) {
				$authAttempt->message = "Error binding in AuthLogger Provider";
				$stmt->close();
				return $authAttempt;
			}		
		
		}else{
		
			if (!($stmt = $this->mysqli->prepare(self::$LOG_SQL))) {
				$authAttempt->message = "Error preparing in AuthLogger Provider";
				return $authAttempt;
			}
			
			if (!$stmt->bind_param("is", $authAttempt->memberId, $ip)) {
				$authAttempt->message = "Error binding in AuthLogger Provider";
				$stmt->close();
				return $authAttempt;
			}
		}
		
		if (!$stmt->execute()) {
			$authAttempt->message = "Error executing in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		$stmt->close();
	}
	
	private function checkFirstTime($authAttempt){
		if (!($stmt = $this->mysqli->prepare(self::$FIRST_TIME_CHECK))) {
			$authAttempt->message = "Error preparing in AuthLogger Provider";
			return $authAttempt;
		}
		
		if (!$stmt->bind_param("i", $authAttempt->memberId)) {
			$authAttempt->message = "Error binding in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		if (!$stmt->execute()) {
			$authAttempt->message = "Error executing in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		$stmt->store_result();
		
		$authAttempt->isFirstTime = ($stmt->num_rows() == 0);
		
		$stmt->close();
	}
}

class AdminLogger {
	private static $FIRST_TIME_CHECK = "SELECT 1 FROM admin_loginhistory WHERE AdminLoginHistory_AdminUserID = ? LIMIT 1";
	private static $LOG_SQL = "INSERT INTO admin_loginhistory (AdminLoginHistory_AdminUserID,AdminLoginHistory_IP) VALUES (?,?)";

    /* @var mysqli */
	private $mysqli;
	
	public function __construct($dbconn){
		$this->mysqli = $dbconn;
	}
	
	public function log(AdminAuthAttempt $authAttempt, $ip){
		$this->checkFirstTime($authAttempt);
		
		if (!($stmt = $this->mysqli->prepare(self::$LOG_SQL))) {
			$authAttempt->message = "Error preparing in AuthLogger Provider";
			return $authAttempt;
		}
		
		if (!$stmt->bind_param("is", $authAttempt->adminId, $ip)) {
			$authAttempt->message = "Error binding in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		if (!$stmt->execute()) {
			$authAttempt->message = "Error executing in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		$stmt->close();
	}
	
	private function checkFirstTime(AdminAuthAttempt $authAttempt){
		if (!($stmt = $this->mysqli->prepare(self::$FIRST_TIME_CHECK))) {
			$authAttempt->message = "Error preparing in AuthLogger Provider";
			return $authAttempt;
		}

		if (!$stmt->bind_param("i", $authAttempt->adminId)) {
			$authAttempt->message = "Error binding in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		if (!$stmt->execute()) {
			$authAttempt->message = "Error executing in AuthLogger Provider";
			$stmt->close();
			return $authAttempt;
		}
		
		$stmt->store_result();
		
		$authAttempt->isFirstTime = ($stmt->num_rows() == 0);
		
		$stmt->close();
	}
}

class Bcrypt {
  private $rounds;
  public function __construct($rounds = 12) {
    if(CRYPT_BLOWFISH != 1) {
      throw new Exception("bcrypt not supported in this installation. See http://php.net/crypt");
    }

    $this->rounds = $rounds;
  }

  public function hash($input) {
    $hash = crypt($input, $this->getSalt());

    if(strlen($hash) > 13)
      return $hash;

    return false;
  }

  public function verify($input, $existingHash) {
    $hash = crypt($input, $existingHash);

    return $hash === $existingHash;
  }

  private function getSalt() {
    $salt = sprintf('$2a$%02d$', $this->rounds);

    $bytes = $this->getRandomBytes(16);

    $salt .= $this->encodeBytes($bytes);

    return $salt;
  }

  private $randomState;
  private function getRandomBytes($count) {
    $bytes = '';

    if(function_exists('openssl_random_pseudo_bytes') && (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')) { // OpenSSL slow on Win
      $bytes = openssl_random_pseudo_bytes($count);
    }
    if(function_exists('mt_rand') && (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')) { // OpenSSL slow on Win
      $bytes = mt_rand($count);
    }

    if($bytes === '' && is_readable('/dev/urandom') && ($hRand = @fopen('/dev/urandom', 'rb')) !== FALSE) {
      $bytes = fread($hRand, $count);
      fclose($hRand);
    }

    if(strlen($bytes) < $count) {
      $bytes = '';

      if($this->randomState === null) {
        $this->randomState = microtime();
        if(function_exists('getmypid')) {
          $this->randomState .= getmypid();
        }
      }

      for($i = 0; $i < $count; $i += 16) {
        $this->randomState = md5(microtime() . $this->randomState);

        if (PHP_VERSION >= '5') {
          $bytes .= md5($this->randomState, true);
        } else {
          $bytes .= pack('H*', md5($this->randomState));
        }
      }

      $bytes = substr($bytes, 0, $count);
    }

    return $bytes;
  }

  private function encodeBytes($input) {
    // The following is code from the PHP Password Hashing Framework
    $itoa64 = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    $output = '';
    $i = 0;
    do {
      $c1 = ord($input[$i++]);
      $output .= $itoa64[$c1 >> 2];
      $c1 = ($c1 & 0x03) << 4;
      if ($i >= 16) {
        $output .= $itoa64[$c1];
        break;
      }

      $c2 = ord($input[$i++]);
      $c1 |= $c2 >> 4;
      $output .= $itoa64[$c1];
      $c1 = ($c2 & 0x0f) << 2;

      $c2 = ord($input[$i++]);
      $c1 |= $c2 >> 6;
      $output .= $itoa64[$c1];
      $output .= $itoa64[$c2 & 0x3f];
    } while (1);

    return $output;
  }
}

class CreateSessionVariablesProvider
{
	protected $mysqli;

	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

    public function getSessionData(AuthAttempt $authAttempt){
        switch ($authAttempt->memberTypeId){
            case AuthAttempt::INDIVIDUAL_MEMBER_TYPE:
                return $this->getIndividualSessionValues($authAttempt->memberId);
            case AuthAttempt::MERCHANT_MEMBER_TYPE:
                return $this->getMerchantSessionValues($authAttempt->memberId);
            case AuthAttempt::ORGANIZATION_MEMBER_TYPE:
                return $this->getOrganizationSessionValues($authAttempt->memberId);
            default:
        }
        return null;
    }

    private static $GET_SESSION_MERCHANT_SQL = "
        SELECT
			MerchantInfo_ID as id,
			MerchantInfo_Name as name,
			MerchantInfo_City as city,
			MerchantInfo_State as state,
			MerchantInfo_Zip as zip,

			MerchantInfo_Lat as lat,
			MerchantInfo_Long as `long`,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
				SELECT 1
				FROM codes_campaign
				WHERE CodesCampaign_ID = MerchantInfo_CurrentReferralID
				AND (CodesCampaign_ValidUntilDate = '0000-00-00' OR CURRENT_DATE < CodesCampaign_ValidUntilDate)
			) as isPlanSubscriber
			
		FROM
		    merchant_info
		WHERE
		    MerchantInfo_MemberID = ?";

    private function getMerchantSessionValues($memberId){
        $sqlString = self::$GET_SESSION_MERCHANT_SQL;
        $sqlBindings = "i";
        $sqlParams = array($memberId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $record = new MerchantSession();
        if (count($result)){
            $row = (object)$result[0];

            $record->id = $row->id;
            $record->pid = $row->pid;
            $record->name = $row->name;
            $record->city = $row->city;
            $record->state = $row->state;
            $record->zip = $row->zip;

            $record->lat = $row->lat;
            $record->long = $row->long;
            $record->isPlanSubscriber = $row->isPlanSubscriber ? true : false;
        }
        return $record;
    }
	
    private static $GET_SESSION_INDIVIDUAL_SQL = "
		SELECT 
			IndividualInfo_ID as id,
			IndividualInfo_Email as email,
			IndividualInfo_Zip as zip,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'individual'
	            AND entity_id = IndividualInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid
		FROM individual_info WHERE IndividualInfo_MemberID=?";

	private function getIndividualSessionValues($memberId){
        $sqlString = self::$GET_SESSION_INDIVIDUAL_SQL;
        $sqlBindings = "i";
        $sqlParams = array($memberId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $record = new IndividualSession();
        if (count($result)){
            $row = (object)$result[0];

            $record->id = $row->id;
            $record->email = $row->email;
            $record->zip = $row->zip;
            $record->pid = $row->pid;
        }
        return $record;
    }
    private static $GET_SESSION_ORGANIZATION_SQL = "
		SELECT 
			OrganizationInfo_EIN as ein, 
			OrganizationInfo_Name as name,
			OrganizationInfo_ID as id,

			OrganizationInfo_City as city,
			OrganizationInfo_State as state,
			OrganizationInfo_Zip as zip,
			OrganizationInfo_Lat as lat,
			OrganizationInfo_Long as `long`,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid			
			
		FROM organization_info 
		WHERE OrganizationInfo_MemberID=?";
		
	private function getOrganizationSessionValues($memberId){
        $sqlString = self::$GET_SESSION_ORGANIZATION_SQL;
        $sqlBindings = "i";
        $sqlParams = array($memberId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $record = new OrganizationSession();
        if (count($result)){
            $row = (object)$result[0];

            $record->ein = $row->ein;
            $record->id = $row->id;
            $record->name = $row->name;

            $record->city = $row->city;
            $record->state = $row->state;
            $record->zip = $row->zip;

            $record->lat = $row->lat;
            $record->long = $row->long;
            $record->pid = $row->pid;
        }
        return $record;
    }
}

?>