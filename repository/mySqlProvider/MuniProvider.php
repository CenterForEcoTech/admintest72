<?php
ini_set('memory_limit', '7048M');
//ini_set('max_execution_time', 120000); //1200 seconds = 20 minutes
class MuniProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
    private static $INSERT_SQL_RES = "
		INSERT INTO muni_res_customers
		( INSERTFIELDSGOHERE) VALUES ( VALUESGOHERE )";
	private static $INSERT_SQL_RES_INSERTFIELDSGOHERE = "
		MuniCustomer_AccountHolderFirstName,MuniCustomer_AccountHolderLastName,MuniCustomer_InstalledAddressOwnedOrLeased,MuniCustomer_InstalledAddress,MuniCustomer_InstalledCity,MuniCustomer_InstalledState,MuniCustomer_InstalledZip,
		MuniCustomer_InstalledPhone,MuniCustomer_InstalledWorkPhone,MuniCustomer_InstalledFax,MuniCustomer_InstalledEmail,MuniCustomer_UtilityName,MuniCustomer_UtilityAccountNumber,MuniCustomer_UtilityAccountNumberPerson,MuniCustomer_UtilityAccountNumberLocation,MuniCustomer_RebateMadePayableTo,
		MuniCustomer_MailingAddress,MuniCustomer_MailingCity,MuniCustomer_MailingState,MuniCustomer_MailingZip,MuniCustomer_OwnerFirstName,MuniCustomer_OwnerLastName,MuniCustomer_OwnerTaxID,
		MuniCustomer_OwnerAddress,MuniCustomer_OwnerCity,MuniCustomer_OwnerState,MuniCustomer_OwnerZip,MuniCustomer_OwnerPhone,MuniCustomer_OwnerFax,MuniCustomer_OwnerEmail,
		MuniCustomer_AuditFloorArea,MuniCustomer_AuditHouseWidth,MuniCustomer_AuditHouseLength,MuniCustomer_AuditConditionedStories,MuniCustomer_AuditHeatingEquipmentType,MuniCustomer_AuditHeatingFuel,
		MuniCustomer_CustomerNotes,MuniCustomer_AddedByAdmin";
	private static $INSERT_SQL_RES_VALUESGOHERE = "
		?,?,?,?,?,?,?,
		?,?,?,?,?,?,?,?,?,
		?,?,?,?,?,?,?,
		?,?,?,?,?,?,?,
		?,?,?,?,?,?,
		?,?";
			
    public function addResCustomer($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES;
		$sqlInsertFields = self::$INSERT_SQL_RES_INSERTFIELDSGOHERE;
		$sqlValues = self::$INSERT_SQL_RES_VALUESGOHERE;
        $sqlBinding = "ssssssssssssssssssssssssssssssssssssss";
		/*
		//break apart name
			if (!$record->accountHolderLastName){
				echo "add";
				print_pre($record);
				$accountName = explode(" ",$record->accountHolderFirstName);
				$record->accountHolderFirstName = "";
				if (count($accountName) > 1){
					$accountLastName = array_pop($accountName); //remove last element
					$accountFirstName = implode(" ",$accountName);
				}else{
					$accountLastName = $accountFirstName = $record->accountHolderFirstName;
				}
				$record->accountHolderFirstName = trim(ucwords(strtolower($accountFirstName)));
				$record->accountHolderLastName = ucwords(strtolower($accountLastName));
			}
		*/

		//breakapart AccountNumber
		$record->utilityAccountNumber = ($record->utilityAccountNumber ? $record->utilityAccountNumber : $record->utilityAccountNumberSource);
		$utilityAccountNumberLocation = ($record->utilityAccountNumberLocation ? $record->utilityAccountNumberLocation : substr($record->utilityAccountNumber,0,6));
		$utilityAccountNumberPerson = ($record->utilityAccountNumberPerson ? $record->utilityAccountNumberPerson : substr($record->utilityAccountNumber,6,11));
		
        $sqlParam = array(
			$record->accountHolderFirstName,$record->accountHolderLastName,$record->installedAddressOwnedOrLeased,$record->installedAddress,$record->installedCity,$record->installedState,$record->installedZip,
			$record->installedPhone,$record->installedWorkPhone,$record->installedFax,$record->installedEmail,$record->utilityName,$record->utilityAccountNumber,$utilityAccountNumberPerson,$utilityAccountNumberLocation,$record->rebateMadePayableTo,
			$record->mailingAddress,$record->mailingCity,$record->mailingState,$record->mailingZip,$record->ownerFirstName,$record->ownerLastName,$record->ownerTaxId,
			$record->ownerAddress,$record->ownerCity,$record->ownerState,$record->ownerZip,$record->ownerPhone,$record->ownerFax,$record->ownerEmail,
			$record->auditFloorArea,$record->auditHouseWidth,$record->auditHouseLength,$record->auditConditionedStories,$record->auditHeatingEquipmentType,$record->auditHeatingFuel,
			$record->customerNotes,$adminUserId
        );
		$rebateType = $record->rebateType;
		if ($rebateType){
			$sqlInsertFields .=",MuniCustomer_Rebate".$rebateType;
			$sqlValues .=",?";
			$sqlBinding .="i";
			$sqlParam[] = 1;
		}
		$customerType = $record->customerType;
		if ($customerType == "commercial"){
			$sqlInsertFields .=",MuniCustomer_CustomerType";
			$sqlValues .=",?";
			$sqlBinding .="s";
			$sqlParam[] = "commercial";
		}
		if ($record->auditStatus){
			$sqlInsertFields .=",MuniCustomer_AuditStatus";
			$sqlValues .=",?";
			$sqlBinding .="i";
			$sqlParam[] = "1";
			
		}
		$sqlString = str_replace("INSERTFIELDSGOHERE",$sqlInsertFields,$sqlString);
		$sqlString = str_replace("VALUESGOHERE",$sqlValues,$sqlString);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addResCustomer failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
    private static $SELECT_RES_COLUMNS = "
        SELECT
			m.MuniCustomer_ID as id,
			m.MuniCustomer_ID as customerId,
			m.MuniCustomer_ID as customerID,
			m.MuniCustomer_CustomerType as customerType,
			m.MuniCustomer_AccountHolderFirstName as accountHolderFirstName,
			m.MuniCustomer_AccountHolderLastName as accountHolderLastName,
			CONCAT(m.MuniCustomer_AccountHolderFirstName,' ',m.MuniCustomer_AccountHolderLastName) as accountHolderFullName,
			m.MuniCustomer_InstalledAddressOwnedOrLeased as installedAddressOwnedOrLeased,
			m.MuniCustomer_InstalledAddress as installedAddress,
			m.MuniCustomer_InstalledCity as installedCity,
			m.MuniCustomer_InstalledState as installedState,
			m.MuniCustomer_InstalledZip as installedZip,
			m.MuniCustomer_InstalledPhone as installedPhone,
			m.MuniCustomer_InstalledWorkPhone as installedWorkPhone,
			m.MuniCustomer_InstalledFax as installedFax,
			m.MuniCustomer_InstalledEmail as installedEmail,
			m.MuniCustomer_UtilityName as utilityName,
			m.MuniCustomer_UtilityAccountNumber as utilityAccountNumber,
			m.MuniCustomer_UtilityAccountNumberPerson as utilityAccountNumberPerson,
			m.MuniCustomer_UtilityAccountNumberLocation as utilityAccountNumberLocation,
			m.MuniCustomer_RebateMadePayableTo as rebateMadePayableTo,
			m.MuniCustomer_MailingAddress as mailingAddress,
			m.MuniCustomer_MailingCity as mailingCity,
			m.MuniCustomer_MailingState as mailingState,
			m.MuniCustomer_MailingZip as mailingZip,
			m.MuniCustomer_OwnerFirstName as ownerFirstName,
			m.MuniCustomer_OwnerLastName as ownerLastName,
			m.MuniCustomer_OwnerTaxID as ownerTaxId,
			m.MuniCustomer_OwnerAddress as ownerAddress,
			m.MuniCustomer_OwnerCity as ownerCity,
			m.MuniCustomer_OwnerState as ownerState,
			m.MuniCustomer_OwnerZip as ownerZip,
			m.MuniCustomer_OwnerPhone as ownerPhone,
			m.MuniCustomer_OwnerFax as ownerFax,
			m.MuniCustomer_OwnerEmail as ownerEmail,
			m.MuniCustomer_CustomerNotes as customerNotes,
			m.MuniCustomer_AuditFloorArea as auditFloorArea,
			m.MuniCustomer_AuditHouseWidth as auditHouseWidth,
			m.MuniCustomer_AuditHouseLength as auditHouseLength,
			m.MuniCustomer_AuditConditionedStories as auditConditionedStories,
			m.MuniCustomer_AuditHeatingEquipmentType as auditHeatingEquipmentType,
			m.MuniCustomer_AuditHeatingFuel as auditHeatingFuel,
			m.MuniCustomer_RebateAppliance as rebateAppliance,
			m.MuniCustomer_RebateCoolHome as rebateCoolHome,
			m.MuniCustomer_RebateEfficiency as rebateEfficiency,
			m.MuniCustomer_RebateHeatHotwater as rebateHeatHotwater,
			m.MuniCustomer_RebatePoolPump as rebatePoolPump,
			m.MuniCustomer_RebateCommercialHeat as rebateCommercialHeat,
			m.MuniCustomer_RebateWiFi as rebateWiFi,
			m.MuniCustomer_GasCredit as gasCredit,
			m.MuniCustomer_AuditStatus as auditStatus,
			m.MuniCustomer_Auditor as auditor,
			m.MuniCustomer_AuditScheduledDate as auditScheduledDate,
			m.MuniCustomer_AuditScheduledStatus as auditScheduledStatus,
			m.MuniCustomer_AuditScheduledTime as auditScheduledTime,
			m.MuniCustomer_AuditScheduledBy as auditScheduledBy,
			m.MuniCustomer_AddedTimeStamp as addedTimeStamp,
			m.MuniCustomer_AddedByAdmin as addedByAdmin,
			m.MuniCustomer_UpdatedByAdmin as updatedByAdmin,
			m.MuniCustomer_UpdatedTimeStamp as updatedTimeStamp,
			m.MuniCustomer_CustomerStatus as customerStatus,
			m.MuniCustomer_CustomerStatusSetBy as customerStatusSetBy";

    private static $FROM_RES_TABLE_CLAUSE = "
        FROM muni_res_customers as m
		WHERE 1 = 1";
	
	private static $FILTER_RES_ON_CUSTOMERSTATUS_NOTDELETED = "
		AND (m.MuniCustomer_CustomerStatus !='Deleted' OR m.MuniCustomer_CustomerStatus IS NULL)";
		 
	private static $FILTER_RES_ON_CUSTOMERSTATUS = "
		 AND m.MuniCustomer_CustomerStatus = ?";
		
    private static $FILTER_RES_ON_UTILITYACCOUNTNUMBER = "
        AND (m.MuniCustomer_UtilityAccountNumber = ? OR m.MuniCustomer_UtilityAccountNumber = ?)";
		
    private static $FILTER_RES_ON_ID = "
        AND m.MuniCustomer_ID = ?";
				
    public function getResCustomer($criteria){
        $sqlString = self::$SELECT_RES_COLUMNS;
		$fromTableClause = self::$FROM_RES_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
        // apply search
		if (!$criteria->customerStatus){
            $filterString .= self::$FILTER_RES_ON_CUSTOMERSTATUS_NOTDELETED;
		}
		if ($criteria->customerStatus){
            $filterString .= self::$FILTER_RES_ON_CUSTOMERSTATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->customerStatus;
		}
		if ($criteria->utilityAccountNumber){
            $filterString .= self::$FILTER_RES_ON_UTILITYACCOUNTNUMBER;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->utilityAccountNumber;
            $filterParams[] = trim(str_replace("-","",$criteria->utilityAccountNumber));
		}
		if ($criteria->id){
            $filterString .= self::$FILTER_RES_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
		}
		if ($criteria->rebateType){
            $filterString .= " AND MuniCustomer_".ucfirst($criteria->rebateType)." > 0";
		}
		if ($criteria->rebateApproved){
            $filterString .= " AND (MuniCustomer_RebateAppliance > 0 OR MuniCustomer_RebateCoolHome > 0 OR MuniCustomer_RebateCommercialHeat > 0 OR MuniCustomer_RebateEfficiency > 0 OR MuniCustomer_RebateHeatHotwater > 0 OR MuniCustomer_RebatePoolPump > 0 OR MuniCustomer_RebateWiFi > 0)";
		}
		if ($criteria->auditScheduledFuture){
            $filterString .= " AND MuniCustomer_AuditScheduledDate >= ?";
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d");
		}
		if ($criteria->auditStatus){
            $filterString .= " AND MuniCustomer_AuditStatus = ?";
            $filterBinding .= "i";
            $filterParams[] = $criteria->auditStatus;
		}
		if ($criteria->auditDateNotSet){
            $filterString .= " AND MuniCustomer_AuditScheduledDate = '0000-00-00'";
		}
		if ($criteria->orderBy){
			$orderBy = " ORDER BY ".$criteria->orderBy;
		}
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->success = true;
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getResCustomer failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	private function checkforchanges($record,$functionName,$fieldExclusionList = NULL){
			$changedFields = array();
			$criteria = new stdClass();
			$criteria->id = $record->id;
			$criteria->rebateId = $record->id;
			$criteria->rebateName = $record->formName;
			$currentDataResult = $this->{$functionName}($criteria);
			$currentCollection = $currentDataResult->collection;
			foreach ($currentCollection as $result){
				foreach ($result as $field=>$value){
					if (!in_array($field,$fieldExclusionList)){
						$fieldName = $field;
						if (strpos($field,"_")){
							$fieldParts = explode("_",$field);
							$fieldName = lcfirst($fieldParts[1]).($fieldParts[2] ? "_".$fieldParts[2] : "");
						}
						$newValue = $record->{$fieldName};
						//clean up newValue
						$newValue = str_replace("$","",$newValue);
						if (substr($field,-4) == "Date"){
							if ($newValue){
								$newValue = date("Y-m-d",strtotime($value));
							}else{
								$newValue = "0000-00-00";
							}
						}
						if (is_array($newValue)){$newValue = implode(",",$newValue);}
						
						if ($newValue != $value){
							$changedFields[$field]["From"] = $value;
							$changedFields[$field]["To"] = $newValue;
						}
					}
				}
			}
		return $changedFields;
		
	}
    private static $UPDATE_RES_SQL = "
        UPDATE muni_res_customers SET UPDATEFIELDSGOHERE WHERE MuniCustomer_ID = ?";
	private static $UPDATE_RES_FIELDS= "
		MuniCustomer_CustomerType = ?,
		MuniCustomer_AccountHolderFirstName = ?,MuniCustomer_AccountHolderLastName = ?,MuniCustomer_InstalledAddressOwnedOrLeased=?,MuniCustomer_InstalledAddress = ?,MuniCustomer_InstalledCity = ?,MuniCustomer_InstalledState = ?,MuniCustomer_InstalledZip = ?,
		MuniCustomer_InstalledPhone = ?,MuniCustomer_InstalledWorkPhone = ?,MuniCustomer_InstalledFax = ?,MuniCustomer_InstalledEmail = ?,MuniCustomer_UtilityName = ?,MuniCustomer_UtilityAccountNumber = ?,MuniCustomer_RebateMadePayableTo = ?,
		MuniCustomer_MailingAddress = ?,MuniCustomer_MailingCity = ?,MuniCustomer_MailingState = ?,MuniCustomer_MailingZip = ?,MuniCustomer_OwnerFirstName = ?,MuniCustomer_OwnerLastName = ?,MuniCustomer_OwnerTaxID = ?,
		MuniCustomer_OwnerAddress = ?,MuniCustomer_OwnerCity = ?,MuniCustomer_OwnerState = ?,MuniCustomer_OwnerZip = ?,MuniCustomer_OwnerPhone = ?,MuniCustomer_OwnerFax = ?,MuniCustomer_OwnerEmail = ?,
		MuniCustomer_AuditFloorArea = ?,MuniCustomer_AuditHouseWidth = ?,MuniCustomer_AuditHouseLength = ?,MuniCustomer_AuditConditionedStories = ?,MuniCustomer_AuditHeatingEquipmentType = ?,MuniCustomer_AuditHeatingFuel=?,
		MuniCustomer_CustomerNotes = ?, MuniCustomer_UpdatedByAdmin = ?,MuniCustomer_UpdatedTimeStamp=?";
    public function updateResCustomer($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusionList = array("addedByAdmin","addedTimeStamp","updatedByAdmin","updatedTimeStamp","rebateAppliance","rebateCommercialHeat","rebateEfficiency","rebateCoolHome","rebatePoolPump","rebateHeatHotwater","utilityAccountNumberPerson","utilityAccountNumberLocation");
			$changedFields = $this->checkforchanges($record,"getResCustomer",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_SQL;
			$sqlUpdateFields = self::$UPDATE_RES_FIELDS;
            $sqlBinding = "ssssssssssssssssssssssssssssssssssssss";
				if ((!$record->accountHolderLastName || !$record->accountHolderFirstName) && $record->accountHolderFullName){
					$accountName = explode(" ",$record->accountHolderFullName);
					$record->accountHolderFirstName = "";
					if (count($accountName) > 1){
						$accountLastName = array_pop($accountName); //remove last element
						$accountFirstName = implode(" ",$accountName);
					}else{
						$accountLastName = $accountFirstName = $record->accountHolderFirstName;
					}
					$record->accountHolderFirstName = trim(ucwords(strtolower($accountFirstName)));
					$record->accountHolderLastName = ucwords(strtolower($accountLastName));
				}
			
            $sqlParams = array(
			$record->customerType,
			$record->accountHolderFirstName,$record->accountHolderLastName,$record->installedAddressOwnedOrLeased,$record->installedAddress,$record->installedCity,$record->installedState,$record->installedZip,
			$record->installedPhone,$record->installedWorkPhone,$record->installedFax,$record->installedEmail,$record->utilityName,$record->utilityAccountNumber,$record->rebateMadePayableTo,
			$record->mailingAddress,$record->mailingCity,$record->mailingState,$record->mailingZip,$record->ownerFirstName,$record->ownerLastName,$record->ownerTaxId,
			$record->ownerAddress,$record->ownerCity,$record->ownerState,$record->ownerZip,$record->ownerPhone,$record->ownerFax,$record->ownerEmail,
			$record->auditFloorArea,$record->auditHouseWidth,$record->auditHouseLength,$record->auditConditionedStories,$record->auditHeatingEquipmentType,$record->auditHeatingFuel,
			$record->customerNotes,$adminUserId,$record->updatedTimeStamp
            );
			if ($record->rebateType){
				$sqlUpdateFields .=",MuniCustomer_Rebate".$record->rebateType."=?";
				$sqlBinding .= "i";
				$sqlParams[] = 1;
			}
			if ($record->auditStatus){
				$sqlUpdateFields .=",MuniCustomer_AuditStatus=?";
				$sqlBinding .= "i";
				$sqlParams[] = $record->auditStatus;
			}
			if ($record->singleField == true){
				$sqlUpdateFields ="MuniCustomer_".$record->fieldName."=?";
				$sqlBinding = "s";
				$sqlParams = array($record->value);
			}
			$sqlString = str_replace("UPDATEFIELDSGOHERE",$sqlUpdateFields,$sqlString);
			$sqlBinding .="i";
			$sqlParams[] = $record->id;
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						if ($fieldName !="accountHolderFullName"){
							$changedResults[] = $adminUserId;
							$changedResults[] = $this->trackChanges("muni_customers",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
						}
					}
					if ($changedFields["utilityAccountNumber"]){
						$thisSQL = "UPDATE muni_com_rebate_heathotwater SET MuniComRebateHeatHotwater_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniComRebateHeatHotwater_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_efficiency SET MuniResRebateEfficiency_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebateEfficiency_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_appliance SET MuniResRebateAppliance_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebateAppliance_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_coolhome SET MuniResRebateCoolHome_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebateCoolHome_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_heathotwater SET MuniResRebateHeatHotwater_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebateHeatHotwater_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_poolpump SET MuniResRebatePoolPump_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebatePoolPump_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
						$thisSQL = "UPDATE muni_res_rebate_wifi SET MuniResRebateWiFi_UtilityAccountNumber='".$record->utilityAccountNumber."' WHERE MuniResRebateWiFi_CustomerID=".$record->id;
						MySqliHelper::execute($this->mysqli, $thisSQL,'',array());
					}
				}

			}
        }
        return $updateResponse;
    }
	
    private static $UPDATE_RES_CUSTOMERSTATUS_SQL = "
        UPDATE muni_res_customers SET MuniCustomer_CustomerStatus = ?, MuniCustomer_CustomerStatusSetBy = ? WHERE MuniCustomer_ID = ?";
	
    public function updateResCustomerStatus($recordId, $status, $adminId){
        $updateResponse = new ExecuteResponseRecord();
        $sqlString = self::$UPDATE_RES_CUSTOMERSTATUS_SQL;
        $sqlBinding = "ssi";
		$statusSetBy = date("Y-m-d H:i:s")."|".$adminId;
		$sqlParams = array($status,$statusSetBy,$recordId);
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
        if ($updateResponse->error){
            trigger_error("MuniProvider.updateCustomerStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
        }
        return $updateResponse;
    }
	
	private static $INSERT_SQL_TRACKCHANGES = "
		INSERT INTO muni_trackchanges 
			(
				MuniTrackChanges_Table,
				MuniTrackChanges_TableID,
				MuniTrackChanges_FieldName,
				MuniTrackChanges_From,
				MuniTrackChanges_To,
				MuniTrackChanges_ChangedBy
			)
			VALUES (
				?,?,?,?,?,?
			)";
    public function trackChanges($table,$tableId,$fieldName,$fieldFrom,$fieldTo,$adminUserId){
        $sqlString = self::$INSERT_SQL_TRACKCHANGES;
        $sqlBinding = "sissss";
        $sqlParam = array($table,$tableId,$fieldName,$fieldFrom,$fieldTo,$adminUserId);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.trackChanges failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	private function commonRebateExclusionFields($exclusionFields){
		array_push($exclusionFields,"Notes","NotesUpdatedDate","addedBy","oid_status","receivedTimeStamp","CurrentStatus","CurrentStatusSetBy","ApprovedDate","ApprovedDateBy","DeniedDate","DeniedDateBy","RebateAmountApproved","RebateAmountApprovedBy");
		return $exclusionFields;
	}
	
	private static $INSERT_SQL_RES_POOL ="
		INSERT INTO muni_res_rebate_poolpump 
			(
			MuniResRebatePoolPump_CustomerID, MuniResRebatePoolPump_UtilityAccountNumber, MuniResRebatePoolPump_RebateMadePayableTo, 
			MuniResRebatePoolPump_ContractorName, MuniResRebatePoolPump_ContractorContactName, MuniResRebatePoolPump_ContractorLicenseNumber, MuniResRebatePoolPump_ContractorTaxID, 
			MuniResRebatePoolPump_ContractorAddress, MuniResRebatePoolPump_ContractorCity, MuniResRebatePoolPump_ContractorState, 
			MuniResRebatePoolPump_ContractorZip, MuniResRebatePoolPump_ContractorPhone, MuniResRebatePoolPump_ContractorEmail, 
			MuniResRebatePoolPump_ReplacedManufacturer, MuniResRebatePoolPump_ReplacedModelNumber, MuniResRebatePoolPump_ReplacedHorsepower, 
			MuniResRebatePoolPump_ReplacedType, MuniResRebatePoolPump_NewManufacturer, MuniResRebatePoolPump_NewModelNumber, 
			MuniResRebatePoolPump_NewHorsepower, MuniResRebatePoolPump_NewType, MuniResRebatePoolPump_NewControllerManufacturer, 
			MuniResRebatePoolPump_NewControllerModelNumber, MuniResRebatePoolPump_NewPrice, MuniResRebatePoolPump_NewPurchaseDate, 
			MuniResRebatePoolPump_NewInstalledDate, MuniResRebatePoolPump_SupportingFile, MuniResRebatePoolPump_CustomerInitials, 
			MuniResRebatePoolPump_StaffInitials, MuniResRebatePoolPump_CustomerAcknowledgementDate, 
			MuniResRebatePoolPump_AddedByAdmin,MuniResRebatePoolPump_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,?
			)";
			
    public function addRebatePoolPump($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_POOL;
        $sqlBinding = "ssssssssssssssssssssssssssssssssss";
        $sqlParam = array(
			$record->customerID, $record->utilityAccountNumber, $record->rebateMadePayableTo, 
			$record->contractorName, $record->contractorContactName, $record->contractorLicenseNumber, $record->contractorTaxID, 
			$record->contractorAddress, $record->contractorCity, $record->contractorState, 
			$record->contractorZip, $record->contractorPhone, $record->contractorEmail, 
			$record->replacedManufacturer, $record->replacedModelNumber, $record->replacedHorsepower, 
			$record->replacedType, $record->newManufacturer, $record->newModelNumber, 
			$record->newHorsepower, $record->newType, $record->newControllerManufacturer, 
			$record->newControllerModelNumber, toInt($record->newPrice), MySQLDateUpdate($record->newPurchaseDate), 
			MySQLDateUpdate($record->newInstalledDate), $record->supportingFile, $record->customerInitials, 
			$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate), 
			$adminUserId,$record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebatePoolPump failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
    private static $UPDATE_RES_POOLPUMP_SQL = "
        UPDATE muni_res_rebate_poolpump SET 
			MuniResRebatePoolPump_RebateMadePayableTo=?, 
			MuniResRebatePoolPump_ContractorName=?, MuniResRebatePoolPump_ContractorContactName=?, MuniResRebatePoolPump_ContractorLicenseNumber=?, MuniResRebatePoolPump_ContractorTaxID=?, 
			MuniResRebatePoolPump_ContractorAddress=?, MuniResRebatePoolPump_ContractorCity=?, MuniResRebatePoolPump_ContractorState=?, 
			MuniResRebatePoolPump_ContractorZip=?, MuniResRebatePoolPump_ContractorPhone=?, MuniResRebatePoolPump_ContractorEmail=?, 
			MuniResRebatePoolPump_ReplacedManufacturer=?, MuniResRebatePoolPump_ReplacedModelNumber=?, MuniResRebatePoolPump_ReplacedHorsepower=?, 
			MuniResRebatePoolPump_ReplacedType=?, MuniResRebatePoolPump_NewManufacturer=?, MuniResRebatePoolPump_NewModelNumber=?, 
			MuniResRebatePoolPump_NewHorsepower=?, MuniResRebatePoolPump_NewType=?, MuniResRebatePoolPump_NewControllerManufacturer=?, 
			MuniResRebatePoolPump_NewControllerModelNumber=?, MuniResRebatePoolPump_NewPrice=?, MuniResRebatePoolPump_NewPurchaseDate=?, 
			MuniResRebatePoolPump_NewInstalledDate=?, MuniResRebatePoolPump_SupportingFile=?, MuniResRebatePoolPump_CustomerInitials=?, 
			MuniResRebatePoolPump_StaffInitials=?, MuniResRebatePoolPump_CustomerAcknowledgementDate=?
		WHERE MuniResRebatePoolPump_ID = ?";
    public function updateRebatePoolPump($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebatePoolPump_AddedByAdmin","MuniResRebatePoolPump_ID","MuniResRebatePoolPump_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_POOLPUMP_SQL;
            $sqlBinding = "ssssssssssssssssssssssssssssi";
            $sqlParams = array(
				$record->rebateMadePayableTo, 
				$record->contractorName, $record->contractorContactName, $record->contractorLicenseNumber, $record->contractorTaxID, 
				$record->contractorAddress, $record->contractorCity, $record->contractorState, 
				$record->contractorZip, $record->contractorPhone, $record->contractorEmail, 
				$record->replacedManufacturer, $record->replacedModelNumber, $record->replacedHorsepower, 
				$record->replacedType, $record->newManufacturer, $record->newModelNumber, 
				$record->newHorsepower, $record->newType, $record->newControllerManufacturer, 
				$record->newControllerModelNumber, toInt($record->newPrice), MySQLDateUpdate($record->newPurchaseDate), 
				MySQLDateUpdate($record->newInstalledDate), $record->supportingFile, $record->customerInitials, 
				$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate), 
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebatePoolPump failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $adminUserId;
						$changedResults[] = $this->trackChanges("muni_res_rebate_poolpump",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $updateResponse;
    }
	
	private static $INSERT_SQL_RES_WIFI ="
		INSERT INTO muni_res_rebate_wifi 
			(
			MuniResRebateWiFi_CustomerID, MuniResRebateWiFi_UtilityAccountNumber, MuniResRebateWiFi_RebateMadePayableTo, 
			MuniResRebateWiFi_Measure_WiFiDateInstalled, MuniResRebateWiFi_Measure_WiFiManufacturer, MuniResRebateWiFi_Measure_WiFiModelNumber, 
			MuniResRebateWiFi_Measure_WiFiPurchasePrice, MuniResRebateWiFi_Measure_WiFiQuantity, MuniResRebateWiFi_Measure_WiFiTotalAmount,
			MuniResRebateWiFi_Measure_WiFiApproved, MuniResRebateWiFi_SupportingFile, MuniResRebateWiFi_CustomerInitials, 
			MuniResRebateWiFi_StaffInitials, MuniResRebateWiFi_CustomerAcknowledgementDate, MuniResRebateWiFi_AddedByAdmin,
			MuniResRebateWiFi_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?
			)";
			
    public function addRebateWiFi($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_WIFI;
        $sqlBinding = "ssssssssssssssssss";
        $sqlParam = array(
			$record->customerID, $record->utilityAccountNumber, $record->rebateMadePayableTo, 
			MySQLDateUpdate($record->measure_WiFiDateInstalled), $record->measure_WiFiManufacturer, $record->measure_WiFiModelNumber,
			toInt($record->measure_WiFiPurchasePrice), toInt($record->measure_WiFiQuantity), toInt($record->measure_WiFiTotalAmount), 
			$record->measure_WiFiApproved, $record->supportingFile, $record->customerInitials, 
			$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate),$adminUserId,
			$record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateWiFi failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
    private static $UPDATE_RES_WIFI_SQL = "
        UPDATE muni_res_rebate_wifi SET 
			MuniResRebateWiFi_RebateMadePayableTo = ?, 
			MuniResRebateWiFi_Measure_WiFiDateInstalled = ?, MuniResRebateWiFi_Measure_WiFiManufacturer = ?, MuniResRebateWiFi_Measure_WiFiModelNumber = ?, 
			MuniResRebateWiFi_Measure_WiFiPurchasePrice = ?, MuniResRebateWiFi_Measure_WiFiQuantity = ?, MuniResRebateWiFi_Measure_WiFiTotalAmount = ?,
			MuniResRebateWiFi_Measure_WiFiApproved = ?, MuniResRebateWiFi_SupportingFile = ?, MuniResRebateWiFi_CustomerInitials = ?, 
			MuniResRebateWiFi_StaffInitials = ?, MuniResRebateWiFi_CustomerAcknowledgementDate = ?
			WHERE MuniResRebateWiFi_ID = ?";
    public function updateRebateWiFi($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebateWiFi_AddedByAdmin","MuniResRebateWiFi_ID","MuniResRebateWiFi_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_WIFI_SQL;
            $sqlBinding = "ssssssssssssi";
            $sqlParams = array(
				$record->rebateMadePayableTo, 
				MySQLDateUpdate($record->measure_WiFiDateInstalled), $record->measure_WiFiManufacturer, $record->measure_WiFiModelNumber,
				toInt($record->measure_WiFiPurchasePrice), toInt($record->measure_WiFiQuantity), toInt($record->measure_WiFiTotalAmount), 
				$record->measure_WiFiApproved,$record->supportingFile, $record->customerInitials, 
				$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate), 
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateWiFi failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $adminUserId;
						$changedResults[] = $this->trackChanges("muni_res_rebate_wifi",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $updateResponse;
    }
	
	private static $INSERT_SQL_RES_HEATHOTWATER ="
		INSERT INTO muni_res_rebate_heathotwater 
			(
				MuniResRebateHeatHotwater_CustomerID, MuniResRebateHeatHotwater_UtilityAccountNumber, MuniResRebateHeatHotwater_RebateMadePayableTo,
				MuniResRebateHeatHotwater_ContractorName, MuniResRebateHeatHotwater_ContractorContactName, MuniResRebateHeatHotwater_ContractorLicenseNumber, MuniResRebateHeatHotwater_ContractorTaxID,
				MuniResRebateHeatHotwater_ContractorAddress, MuniResRebateHeatHotwater_ContractorCity, MuniResRebateHeatHotwater_ContractorState,
				MuniResRebateHeatHotwater_ContractorZip, MuniResRebateHeatHotwater_ContractorPhone, MuniResRebateHeatHotwater_ContractorEmail,
				
				MuniResRebateHeatHotwater_Measure_Furnace95Manufacturer, MuniResRebateHeatHotwater_Measure_Furnace95ModelNumber, MuniResRebateHeatHotwater_Measure_Furnace95EfficiencyRating,
				MuniResRebateHeatHotwater_Measure_Furnace95InputSize, MuniResRebateHeatHotwater_Measure_Furnace95RebateAmount, MuniResRebateHeatHotwater_Measure_Furnace95QuantityInstalled,
				MuniResRebateHeatHotwater_Measure_Furnace95InstalledCost, MuniResRebateHeatHotwater_Measure_Furnace95Approved, 
				MuniResRebateHeatHotwater_Measure_Furnace97Manufacturer, MuniResRebateHeatHotwater_Measure_Furnace97ModelNumber, MuniResRebateHeatHotwater_Measure_Furnace97EfficiencyRating,
				MuniResRebateHeatHotwater_Measure_Furnace97InputSize, MuniResRebateHeatHotwater_Measure_Furnace97RebateAmount, MuniResRebateHeatHotwater_Measure_Furnace97QuantityInstalled,
				MuniResRebateHeatHotwater_Measure_Furnace97InstalledCost, MuniResRebateHeatHotwater_Measure_Furnace97Approved, 
				MuniResRebateHeatHotwater_Measure_Boiler85Manufacturer, MuniResRebateHeatHotwater_Measure_Boiler85ModelNumber,MuniResRebateHeatHotwater_Measure_Boiler85EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Boiler85InputSize, MuniResRebateHeatHotwater_Measure_Boiler85RebateAmount,MuniResRebateHeatHotwater_Measure_Boiler85QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Boiler85InstalledCost, MuniResRebateHeatHotwater_Measure_Boiler85Approved, 
				MuniResRebateHeatHotwater_Measure_Boiler90Manufacturer, MuniResRebateHeatHotwater_Measure_Boiler90ModelNumber,MuniResRebateHeatHotwater_Measure_Boiler90EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Boiler90InputSize, MuniResRebateHeatHotwater_Measure_Boiler90RebateAmount,MuniResRebateHeatHotwater_Measure_Boiler90QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Boiler90InstalledCost, MuniResRebateHeatHotwater_Measure_Boiler90Approved, 
				MuniResRebateHeatHotwater_Measure_Boiler95Manufacturer, MuniResRebateHeatHotwater_Measure_Boiler95ModelNumber,MuniResRebateHeatHotwater_Measure_Boiler95EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Boiler95InputSize, MuniResRebateHeatHotwater_Measure_Boiler95RebateAmount,MuniResRebateHeatHotwater_Measure_Boiler95QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Boiler95InstalledCost,MuniResRebateHeatHotwater_Measure_Boiler95Approved, 
				MuniResRebateHeatHotwater_Measure_Integrated90Manufacturer, MuniResRebateHeatHotwater_Measure_Integrated90ModelNumber,MuniResRebateHeatHotwater_Measure_Integrated90EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Integrated90InputSize, MuniResRebateHeatHotwater_Measure_Integrated90RebateAmount,MuniResRebateHeatHotwater_Measure_Integrated90QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Integrated90InstalledCost,MuniResRebateHeatHotwater_Measure_Integrated90Approved, 
				MuniResRebateHeatHotwater_Measure_Integrated95Manufacturer, MuniResRebateHeatHotwater_Measure_Integrated95ModelNumber,MuniResRebateHeatHotwater_Measure_Integrated95EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Integrated95InputSize, MuniResRebateHeatHotwater_Measure_Integrated95RebateAmount,MuniResRebateHeatHotwater_Measure_Integrated95QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Integrated95InstalledCost,MuniResRebateHeatHotwater_Measure_Integrated95Approved, 
				MuniResRebateHeatHotwater_Measure_IndirectWaterManufacturer,MuniResRebateHeatHotwater_Measure_IndirectWaterModelNumber, MuniResRebateHeatHotwater_Measure_IndirectWaterEfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_IndirectWaterInputSize,MuniResRebateHeatHotwater_Measure_IndirectWaterRebateAmount, MuniResRebateHeatHotwater_Measure_IndirectWaterQuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_IndirectWaterInstalledCost, MuniResRebateHeatHotwater_Measure_IndirectWaterApproved,
				MuniResRebateHeatHotwater_Measure_H2OStorage62Manufacturer, MuniResRebateHeatHotwater_Measure_H2OStorage62ModelNumber, MuniResRebateHeatHotwater_Measure_H2OStorage62EfficiencyRating,
				MuniResRebateHeatHotwater_Measure_H2OStorage62InputSize, MuniResRebateHeatHotwater_Measure_H2OStorage62RebateAmount, MuniResRebateHeatHotwater_Measure_H2OStorage62QuantityInstalled,
				MuniResRebateHeatHotwater_Measure_H2OStorage62InstalledCost, MuniResRebateHeatHotwater_Measure_H2OStorage62Approved, 
				MuniResRebateHeatHotwater_Measure_H2OStorage67Manufacturer, MuniResRebateHeatHotwater_Measure_H2OStorage67ModelNumber, MuniResRebateHeatHotwater_Measure_H2OStorage67EfficiencyRating,
				MuniResRebateHeatHotwater_Measure_H2OStorage67InputSize, MuniResRebateHeatHotwater_Measure_H2OStorage67RebateAmount, MuniResRebateHeatHotwater_Measure_H2OStorage67QuantityInstalled,
				MuniResRebateHeatHotwater_Measure_H2OStorage67InstalledCost, MuniResRebateHeatHotwater_Measure_H2OStorage67Approved, 
				MuniResRebateHeatHotwater_Measure_Tankless82Manufacturer, MuniResRebateHeatHotwater_Measure_Tankless82ModelNumber,MuniResRebateHeatHotwater_Measure_Tankless82EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Tankless82InputSize, MuniResRebateHeatHotwater_Measure_Tankless82RebateAmount,MuniResRebateHeatHotwater_Measure_Tankless82QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Tankless82InstalledCost, MuniResRebateHeatHotwater_Measure_Tankless82Approved, 
				MuniResRebateHeatHotwater_Measure_Tankless94Manufacturer, MuniResRebateHeatHotwater_Measure_Tankless94ModelNumber,MuniResRebateHeatHotwater_Measure_Tankless94EfficiencyRating, 
				MuniResRebateHeatHotwater_Measure_Tankless94InputSize, MuniResRebateHeatHotwater_Measure_Tankless94RebateAmount,MuniResRebateHeatHotwater_Measure_Tankless94QuantityInstalled, 
				MuniResRebateHeatHotwater_Measure_Tankless94InstalledCost, MuniResRebateHeatHotwater_Measure_Tankless94Approved, 

				MuniResRebateHeatHotwater_TotalAnticipatedRebate, MuniResRebateHeatHotwater_InstalledCost,
				MuniResRebateHeatHotwater_InstalledDate, MuniResRebateHeatHotwater_SupportingFile, MuniResRebateHeatHotwater_CustomerInitials,
				MuniResRebateHeatHotwater_StaffInitials, MuniResRebateHeatHotwater_CustomerAcknowledgementDate,
				MuniResRebateHeatHotwater_AddedByAdmin, MuniResRebateHeatHotwater_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,

			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,

			?,?,
			?,?,?,
			?,?,
			?,?,?,?
			)";
			
    public function addRebateHeatHotwater($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_HEATHOTWATER;
        $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        $sqlParam = array(
				$record->customerID, $record->utilityAccountNumber, $record->rebateMadePayableTo,
				$record->contractorName, $record->contractorContactName,$record->contractorLicenseNumber, $record->contractorTaxID,
				$record->contractorAddress, $record->contractorCity, $record->contractorState,
				$record->contractorZip, $record->contractorPhone, $record->contractorEmail,
				$record->measure_Furnace95Manufacturer, $record->measure_Furnace95ModelNumber, $record->measure_Furnace95EfficiencyRating,
				$record->measure_Furnace95InputSize, toInt($record->measure_Furnace95RebateAmount), $record->measure_Furnace95QuantityInstalled,
				toInt($record->measure_Furnace95InstalledCost),$record->measure_Furnace95Approved, 
				$record->measure_Furnace97Manufacturer, $record->measure_Furnace97ModelNumber, $record->measure_Furnace97EfficiencyRating,
				$record->measure_Furnace97InputSize, toInt($record->measure_Furnace97RebateAmount), $record->measure_Furnace97QuantityInstalled,
				toInt($record->measure_Furnace97InstalledCost), $record->measure_Furnace97Approved, 
				$record->measure_Boiler85Manufacturer, $record->measure_Boiler85ModelNumber,$record->measure_Boiler85EfficiencyRating, 
				$record->measure_Boiler85InputSize, toInt($record->measure_Boiler85RebateAmount),$record->measure_Boiler85QuantityInstalled, 
				toInt($record->measure_Boiler85InstalledCost), $record->measure_Boiler85Approved,
				$record->measure_Boiler90Manufacturer, $record->measure_Boiler90ModelNumber,$record->measure_Boiler90EfficiencyRating, 
				$record->measure_Boiler90InputSize, toInt($record->measure_Boiler90RebateAmount),$record->measure_Boiler90QuantityInstalled, 
				toInt($record->measure_Boiler90InstalledCost), $record->measure_Boiler90Approved,
				$record->measure_Boiler95Manufacturer, $record->measure_Boiler95ModelNumber,$record->measure_Boiler95EfficiencyRating, 
				$record->measure_Boiler95InputSize, toInt($record->measure_Boiler95RebateAmount),$record->measure_Boiler95QuantityInstalled, 
				toInt($record->measure_Boiler95InstalledCost), $record->measure_Boiler95Approved,
				$record->measure_Integrated90Manufacturer, $record->measure_Integrated90ModelNumber,$record->measure_Integrated90EfficiencyRating, 
				$record->measure_Integrated90InputSize, toInt($record->measure_Integrated90RebateAmount),$record->measure_Integrated90QuantityInstalled, 
				toInt($record->measure_Integrated90InstalledCost), $record->measure_Integrated90Approved,
				$record->measure_Integrated95Manufacturer, $record->measure_Integrated95ModelNumber,$record->measure_Integrated95EfficiencyRating, 
				$record->measure_Integrated95InputSize, toInt($record->measure_Integrated95RebateAmount),$record->measure_Integrated95QuantityInstalled, 
				toInt($record->measure_Integrated95InstalledCost), $record->measure_Integrated95Approved,
				$record->measure_IndirectWaterManufacturer,$record->measure_IndirectWaterModelNumber, $record->measure_IndirectWaterEfficiencyRating, 
				$record->measure_IndirectWaterInputSize,toInt($record->measure_IndirectWaterRebateAmount), $record->measure_IndirectWaterQuantityInstalled, 
				toInt($record->measure_IndirectWaterInstalledCost), $record->measure_IndirectWaterApproved,
				$record->measure_H2OStorage62Manufacturer, $record->measure_H2OStorage62ModelNumber, $record->measure_H2OStorage62EfficiencyRating,
				$record->measure_H2OStorage62InputSize, toInt($record->measure_H2OStorage62RebateAmount), $record->measure_H2OStorage62QuantityInstalled,
				toInt($record->measure_H2OStorage62InstalledCost), $record->measure_H2OStorage62Approved,
				$record->measure_H2OStorage67Manufacturer, $record->measure_H2OStorage67ModelNumber, $record->measure_H2OStorage67EfficiencyRating,
				$record->measure_H2OStorage67InputSize, toInt($record->measure_H2OStorage67RebateAmount), $record->measure_H2OStorage67QuantityInstalled,
				toInt($record->measure_H2OStorage67InstalledCost), $record->measure_H2OStorage67Approved,
				$record->measure_Tankless82Manufacturer, $record->measure_Tankless82ModelNumber,$record->measure_Tankless82EfficiencyRating, 
				$record->measure_Tankless82InputSize, toInt($record->measure_Tankless82RebateAmount),$record->measure_Tankless82QuantityInstalled, 
				toInt($record->measure_Tankless82InstalledCost), $record->measure_Tankless82Approved,
				$record->measure_Tankless94Manufacturer, $record->measure_Tankless94ModelNumber,$record->measure_Tankless94EfficiencyRating, 
				$record->measure_Tankless94InputSize, toInt($record->measure_Tankless94RebateAmount),$record->measure_Tankless94QuantityInstalled, 
				toInt($record->measure_Tankless94InstalledCost), $record->measure_Tankless94Approved,
				toInt($record->totalAnticipatedRebate), toInt($record->installedCost),
				MySQLDateUpdate($record->installedDate), $record->supportingFile, $record->customerInitials,
				$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate),
				$adminUserId, $record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateHeatHotwater failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }

    private static $UPDATE_RES_HEATHOTWATER_SQL = "
        UPDATE muni_res_rebate_heathotwater SET 
			MuniResRebateHeatHotwater_RebateMadePayableTo=?,
			MuniResRebateHeatHotwater_ContractorName=?, MuniResRebateHeatHotwater_ContractorContactName=?, MuniResRebateHeatHotwater_ContractorLicenseNumber=?, MuniResRebateHeatHotwater_ContractorTaxID=?,
			MuniResRebateHeatHotwater_ContractorAddress=?, MuniResRebateHeatHotwater_ContractorCity=?, MuniResRebateHeatHotwater_ContractorState=?,
			MuniResRebateHeatHotwater_ContractorZip=?, MuniResRebateHeatHotwater_ContractorPhone=?, MuniResRebateHeatHotwater_ContractorEmail=?,
			MuniResRebateHeatHotwater_Measure_Furnace95Manufacturer=?, MuniResRebateHeatHotwater_Measure_Furnace95ModelNumber=?, MuniResRebateHeatHotwater_Measure_Furnace95EfficiencyRating=?,
			MuniResRebateHeatHotwater_Measure_Furnace95InputSize=?, MuniResRebateHeatHotwater_Measure_Furnace95RebateAmount=?, MuniResRebateHeatHotwater_Measure_Furnace95QuantityInstalled=?,
			MuniResRebateHeatHotwater_Measure_Furnace95InstalledCost=?, MuniResRebateHeatHotwater_Measure_Furnace95Approved=?, 
			MuniResRebateHeatHotwater_Measure_Furnace97Manufacturer=?, MuniResRebateHeatHotwater_Measure_Furnace97ModelNumber=?, MuniResRebateHeatHotwater_Measure_Furnace97EfficiencyRating=?,
			MuniResRebateHeatHotwater_Measure_Furnace97InputSize=?, MuniResRebateHeatHotwater_Measure_Furnace97RebateAmount=?, MuniResRebateHeatHotwater_Measure_Furnace97QuantityInstalled=?,
			MuniResRebateHeatHotwater_Measure_Furnace97InstalledCost=?, MuniResRebateHeatHotwater_Measure_Furnace97Approved=?, 
			MuniResRebateHeatHotwater_Measure_Boiler85Manufacturer=?, MuniResRebateHeatHotwater_Measure_Boiler85ModelNumber=?,MuniResRebateHeatHotwater_Measure_Boiler85EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Boiler85InputSize=?, MuniResRebateHeatHotwater_Measure_Boiler85RebateAmount=?,MuniResRebateHeatHotwater_Measure_Boiler85QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Boiler85InstalledCost=?, MuniResRebateHeatHotwater_Measure_Boiler85Approved=?, 
			MuniResRebateHeatHotwater_Measure_Boiler90Manufacturer=?, MuniResRebateHeatHotwater_Measure_Boiler90ModelNumber=?,MuniResRebateHeatHotwater_Measure_Boiler90EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Boiler90InputSize=?, MuniResRebateHeatHotwater_Measure_Boiler90RebateAmount=?,MuniResRebateHeatHotwater_Measure_Boiler90QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Boiler90InstalledCost=?, MuniResRebateHeatHotwater_Measure_Boiler90Approved=?, 
			MuniResRebateHeatHotwater_Measure_Boiler95Manufacturer=?, MuniResRebateHeatHotwater_Measure_Boiler95ModelNumber=?,MuniResRebateHeatHotwater_Measure_Boiler95EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Boiler95InputSize=?, MuniResRebateHeatHotwater_Measure_Boiler95RebateAmount=?,MuniResRebateHeatHotwater_Measure_Boiler95QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Boiler95InstalledCost=?, MuniResRebateHeatHotwater_Measure_Boiler95Approved=?, 
			MuniResRebateHeatHotwater_Measure_Integrated90Manufacturer=?, MuniResRebateHeatHotwater_Measure_Integrated90ModelNumber=?,MuniResRebateHeatHotwater_Measure_Integrated90EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Integrated90InputSize=?, MuniResRebateHeatHotwater_Measure_Integrated90RebateAmount=?,MuniResRebateHeatHotwater_Measure_Integrated90QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Integrated90InstalledCost=?, MuniResRebateHeatHotwater_Measure_Integrated90Approved=?, 
			MuniResRebateHeatHotwater_Measure_Integrated95Manufacturer=?, MuniResRebateHeatHotwater_Measure_Integrated95ModelNumber=?,MuniResRebateHeatHotwater_Measure_Integrated95EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Integrated95InputSize=?, MuniResRebateHeatHotwater_Measure_Integrated95RebateAmount=?,MuniResRebateHeatHotwater_Measure_Integrated95QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Integrated95InstalledCost=?, MuniResRebateHeatHotwater_Measure_Integrated95Approved=?, 
			MuniResRebateHeatHotwater_Measure_IndirectWaterManufacturer=?, MuniResRebateHeatHotwater_Measure_IndirectWaterModelNumber=?, MuniResRebateHeatHotwater_Measure_IndirectWaterEfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_IndirectWaterInputSize=?, MuniResRebateHeatHotwater_Measure_IndirectWaterRebateAmount=?, MuniResRebateHeatHotwater_Measure_IndirectWaterQuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_IndirectWaterInstalledCost=?, MuniResRebateHeatHotwater_Measure_IndirectWaterApproved=?, 
			MuniResRebateHeatHotwater_Measure_H2OStorage62Manufacturer=?, MuniResRebateHeatHotwater_Measure_H2OStorage62ModelNumber=?, MuniResRebateHeatHotwater_Measure_H2OStorage62EfficiencyRating=?,
			MuniResRebateHeatHotwater_Measure_H2OStorage62InputSize=?, MuniResRebateHeatHotwater_Measure_H2OStorage62RebateAmount=?, MuniResRebateHeatHotwater_Measure_H2OStorage62QuantityInstalled=?,
			MuniResRebateHeatHotwater_Measure_H2OStorage62InstalledCost=?, MuniResRebateHeatHotwater_Measure_H2OStorage62Approved=?, 
			MuniResRebateHeatHotwater_Measure_H2OStorage67Manufacturer=?, MuniResRebateHeatHotwater_Measure_H2OStorage67ModelNumber=?, MuniResRebateHeatHotwater_Measure_H2OStorage67EfficiencyRating=?,
			MuniResRebateHeatHotwater_Measure_H2OStorage67InputSize=?, MuniResRebateHeatHotwater_Measure_H2OStorage67RebateAmount=?, MuniResRebateHeatHotwater_Measure_H2OStorage67QuantityInstalled=?,
			MuniResRebateHeatHotwater_Measure_H2OStorage67InstalledCost=?, MuniResRebateHeatHotwater_Measure_H2OStorage67Approved=?, 
			MuniResRebateHeatHotwater_Measure_Tankless82Manufacturer=?, MuniResRebateHeatHotwater_Measure_Tankless82ModelNumber=?,MuniResRebateHeatHotwater_Measure_Tankless82EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Tankless82InputSize=?, MuniResRebateHeatHotwater_Measure_Tankless82RebateAmount=?,MuniResRebateHeatHotwater_Measure_Tankless82QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Tankless82InstalledCost=?, MuniResRebateHeatHotwater_Measure_Tankless82Approved=?, 
			MuniResRebateHeatHotwater_Measure_Tankless94Manufacturer=?, MuniResRebateHeatHotwater_Measure_Tankless94ModelNumber=?,MuniResRebateHeatHotwater_Measure_Tankless94EfficiencyRating=?, 
			MuniResRebateHeatHotwater_Measure_Tankless94InputSize=?, MuniResRebateHeatHotwater_Measure_Tankless94RebateAmount=?,MuniResRebateHeatHotwater_Measure_Tankless94QuantityInstalled=?, 
			MuniResRebateHeatHotwater_Measure_Tankless94InstalledCost=?, MuniResRebateHeatHotwater_Measure_Tankless94Approved=?, 
			MuniResRebateHeatHotwater_TotalAnticipatedRebate=?, MuniResRebateHeatHotwater_InstalledCost=?,
			MuniResRebateHeatHotwater_InstalledDate=?, MuniResRebateHeatHotwater_SupportingFile=?, MuniResRebateHeatHotwater_CustomerInitials=?,
			MuniResRebateHeatHotwater_StaffInitials=?, MuniResRebateHeatHotwater_CustomerAcknowledgementDate=?
		WHERE MuniResRebateHeatHotwater_ID = ?";
    public function updateRebateHeatHotwater($record,$adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebateHeatHotwater_AddedByAdmin","MuniResRebateHeatHotwater_ID","MuniResRebateHeatHotwater_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_HEATHOTWATER_SQL;
            $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssi";
            $sqlParams = array(
				$record->rebateMadePayableTo,
				$record->contractorName, $record->contractorContactName, $record->contractorLicenseNumber, $record->contractorTaxID,
				$record->contractorAddress, $record->contractorCity, $record->contractorState,
				$record->contractorZip, $record->contractorPhone, $record->contractorEmail,
				$record->measure_Furnace95Manufacturer, $record->measure_Furnace95ModelNumber, $record->measure_Furnace95EfficiencyRating,
				$record->measure_Furnace95InputSize, toInt($record->measure_Furnace95RebateAmount), $record->measure_Furnace95QuantityInstalled,
				toInt($record->measure_Furnace95InstalledCost), $record->measure_Furnace95Approved,
				$record->measure_Furnace97Manufacturer, $record->measure_Furnace97ModelNumber, $record->measure_Furnace97EfficiencyRating,
				$record->measure_Furnace97InputSize, toInt($record->measure_Furnace97RebateAmount), $record->measure_Furnace97QuantityInstalled,
				toInt($record->measure_Furnace97InstalledCost), $record->measure_Furnace97Approved,
				$record->measure_Boiler85Manufacturer, $record->measure_Boiler85ModelNumber,$record->measure_Boiler85EfficiencyRating, 
				$record->measure_Boiler85InputSize, toInt($record->measure_Boiler85RebateAmount),$record->measure_Boiler85QuantityInstalled,
				toInt($record->measure_Boiler85InstalledCost), $record->measure_Boiler85Approved,
				$record->measure_Boiler90Manufacturer, $record->measure_Boiler90ModelNumber,$record->measure_Boiler90EfficiencyRating, 
				$record->measure_Boiler90InputSize, toInt($record->measure_Boiler90RebateAmount),$record->measure_Boiler90QuantityInstalled,
				toInt($record->measure_Boiler90InstalledCost), $record->measure_Boiler90Approved,
				$record->measure_Boiler95Manufacturer, $record->measure_Boiler95ModelNumber,$record->measure_Boiler95EfficiencyRating, 
				$record->measure_Boiler95InputSize, toInt($record->measure_Boiler95RebateAmount),$record->measure_Boiler95QuantityInstalled,
				toInt($record->measure_Boiler95InstalledCost), $record->measure_Boiler95Approved,
				$record->measure_Integrated90Manufacturer, $record->measure_Integrated90ModelNumber,$record->measure_Integrated90EfficiencyRating, 
				$record->measure_Integrated90InputSize, toInt($record->measure_Integrated90RebateAmount),$record->measure_Integrated90QuantityInstalled,
				toInt($record->measure_Integrated90InstalledCost), $record->measure_Integrated90Approved,
				$record->measure_Integrated95Manufacturer, $record->measure_Integrated95ModelNumber,$record->measure_Integrated95EfficiencyRating, 
				$record->measure_Integrated95InputSize, toInt($record->measure_Integrated95RebateAmount),$record->measure_Integrated95QuantityInstalled,
				toInt($record->measure_Integrated95InstalledCost), $record->measure_Integrated95Approved,
				$record->measure_IndirectWaterManufacturer,$record->measure_IndirectWaterModelNumber, $record->measure_IndirectWaterEfficiencyRating, 
				$record->measure_IndirectWaterInputSize, toInt($record->measure_IndirectWaterRebateAmount), $record->measure_IndirectWaterQuantityInstalled, 
				toInt($record->measure_IndirectWaterInstalledCost), $record->measure_IndirectWaterApproved,
				$record->measure_H2OStorage62Manufacturer, $record->measure_H2OStorage62ModelNumber, $record->measure_H2OStorage62EfficiencyRating,
				$record->measure_H2OStorage62InputSize, toInt($record->measure_H2OStorage62RebateAmount), $record->measure_H2OStorage62QuantityInstalled,
				toInt($record->measure_H2OStorage62InstalledCost), $record->measure_H2OStorage62Approved,
				$record->measure_H2OStorage67Manufacturer, $record->measure_H2OStorage67ModelNumber, $record->measure_H2OStorage67EfficiencyRating,
				$record->measure_H2OStorage67InputSize, toInt($record->measure_H2OStorage67RebateAmount), $record->measure_H2OStorage67QuantityInstalled,
				toInt($record->measure_H2OStorage67InstalledCost), $record->measure_H2OStorage67Approved, 
				$record->measure_Tankless82Manufacturer, $record->measure_Tankless82ModelNumber,$record->measure_Tankless82EfficiencyRating, 
				$record->measure_Tankless82InputSize, toInt($record->measure_Tankless82RebateAmount),$record->measure_Tankless82QuantityInstalled, 
				toInt($record->measure_Tankless82InstalledCost), $record->measure_Tankless82Approved,
				$record->measure_Tankless94Manufacturer, $record->measure_Tankless94ModelNumber,$record->measure_Tankless94EfficiencyRating, 
				$record->measure_Tankless94InputSize, toInt($record->measure_Tankless94RebateAmount),$record->measure_Tankless94QuantityInstalled, 
				toInt($record->measure_Tankless94InstalledCost), $record->measure_Tankless94Approved,
				toInt($record->totalAnticipatedRebate), toInt($record->installedCost),
				MySQLDateUpdate($record->installedDate), $record->supportingFile, $record->customerInitials,
				$record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate),
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
			$updateResponse->sql = $sqlString;
			$updateResponse->binding = $sqlBinding;
			$updateResponse->params = print_r($sqlParams,true);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateHeatHotwater failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $this->trackChanges("muni_res_rebate_heathotwater",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $updateResponse;
    }
	
	private static $INSERT_SQL_RES_EFFICIENCY ="
		INSERT INTO muni_res_rebate_efficiency 
			(
				MuniResRebateEfficiency_CustomerID, MuniResRebateEfficiency_UtilityAccountNumber, MuniResRebateEfficiency_RebateMadePayableTo,
				MuniResRebateEfficiency_ContractorName, MuniResRebateEfficiency_ContractorContactName, MuniResRebateEfficiency_ContractorLicenseNumber, MuniResRebateEfficiency_ContractorTaxID,
				MuniResRebateEfficiency_ContractorAddress, MuniResRebateEfficiency_ContractorCity, MuniResRebateEfficiency_ContractorState,
				MuniResRebateEfficiency_ContractorZip, MuniResRebateEfficiency_ContractorPhone, MuniResRebateEfficiency_ContractorEmail,MuniResRebateEfficiency_EligibleMeasures,
				MuniResRebateEfficiency_AirSealingApprovedAmount, MuniResRebateEfficiency_InsulationApprovedAmount, MuniResRebateEfficiency_HeatingApprovedAmount,
				MuniResRebateEfficiency_MeasuresInstalled, MuniResRebateEfficiency_MeasuresInspectionFile, MuniResRebateEfficiency_MeasuresInvoiceFile,
				MuniResRebateEfficiency_CustomerInitials, MuniResRebateEfficiency_StaffInitials, MuniResRebateEfficiency_CustomerAcknowledgementDate,
				MuniResRebateEfficiency_AddedByAdmin, MuniResRebateEfficiency_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,?
			)";
			
    public function addRebateEfficiency($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_EFFICIENCY;
        $sqlBinding = "sssssssssssssssssssssssssss";
		$eligibleMeasuresValues = (count($record->eligibleMeasures) > 1 ? implode(",",$record->eligibleMeasures) : $record->eligibleMeasures);
        $sqlParam = array(
				$record->customerID, $record->utilityAccountNumber, $record->rebateMadePayableTo,
				$record->contractorName, $record->contractorContactName,$record->contractorLicenseNumber, $record->contractorTaxID,
				$record->contractorAddress, $record->contractorCity, $record->contractorState,
				$record->contractorZip, $record->contractorPhone, $record->contractorEmail,$eligibleMeasuresValues,
				toInt($record->airSealingApprovedAmount), toInt($record->insulationApprovedAmount), toInt($record->heatingApprovedAmount),
				$record->measuresInstalled, $record->measuresInspectionFile, $record->measuresInvoiceFile,
				$record->customerInitials, $record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate),
				$adminUserId, $record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateEfficiency failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }
	
    private static $UPDATE_RES_EFFICIENCY_SQL = "
        UPDATE muni_res_rebate_efficiency SET 
			MuniResRebateEfficiency_UtilityAccountNumber=?, MuniResRebateEfficiency_RebateMadePayableTo=?,
			MuniResRebateEfficiency_ContractorName=?, MuniResRebateEfficiency_ContractorContactName=?, MuniResRebateEfficiency_ContractorLicenseNumber=?, MuniResRebateEfficiency_ContractorTaxID=?,
			MuniResRebateEfficiency_ContractorAddress=?, MuniResRebateEfficiency_ContractorCity=?, MuniResRebateEfficiency_ContractorState=?,
			MuniResRebateEfficiency_ContractorZip=?, MuniResRebateEfficiency_ContractorPhone=?, MuniResRebateEfficiency_ContractorEmail=?,MuniResRebateEfficiency_EligibleMeasures=?,
			MuniResRebateEfficiency_AirSealingApprovedAmount=?, MuniResRebateEfficiency_InsulationApprovedAmount=?, MuniResRebateEfficiency_HeatingApprovedAmount=?,
			MuniResRebateEfficiency_MeasuresInstalled=?, MuniResRebateEfficiency_MeasuresInspectionFile=?, MuniResRebateEfficiency_MeasuresInvoiceFile=?,MuniResRebateEfficiency_SupportingFile=?,
			MuniResRebateEfficiency_CustomerInitials=?, MuniResRebateEfficiency_StaffInitials=?, MuniResRebateEfficiency_CustomerAcknowledgementDate=?
		WHERE MuniResRebateEfficiency_ID = ?";
    public function updateRebateEfficiency($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebateEfficiency_AddedByAdmin","MuniResRebateEfficiency_ID","MuniResRebateEfficiency_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			$eligibleMeasuresValues = (count($record->eligibleMeasures) > 1 ? implode(",",$record->eligibleMeasures) : $record->eligibleMeasures);
            $sqlString = self::$UPDATE_RES_EFFICIENCY_SQL;
            $sqlBinding = "sssssssssssssssssssssssi";
            $sqlParams = array(
				$record->utilityAccountNumber, $record->rebateMadePayableTo,
				$record->contractorName, $record->contractorContactName,$record->contractorLicenseNumber, $record->contractorTaxID,
				$record->contractorAddress, $record->contractorCity, $record->contractorState,
				$record->contractorZip, $record->contractorPhone, $record->contractorEmail,$eligibleMeasuresValues,
				toInt($record->airSealingApprovedAmount), toInt($record->insulationApprovedAmount), toInt($record->heatingApprovedAmount),
				$record->measuresInstalled, $record->measuresInspectionFile, $record->measuresInvoiceFile,$record->supportingFile,
				$record->customerInitials, $record->staffInitials, MySQLDateUpdate($record->customerAcknowledgementDate),
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateEfficiency failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $this->trackChanges("muni_res_rebate_efficiency",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $changedFields;
    }
	
	private static $INSERT_SQL_RES_COOLHOME ="
		INSERT INTO muni_res_rebate_coolhome 
			(
				MuniResRebateCoolHome_CustomerID,MuniResRebateCoolHome_UtilityAccountNumber,MuniResRebateCoolHome_RebateMadePayableTo,
				MuniResRebateCoolHome_ContractorName,MuniResRebateCoolHome_ContractorContactName,MuniResRebateCoolHome_ContractorLicenseNumber,MuniResRebateCoolHome_ContractorTaxID,
				MuniResRebateCoolHome_ContractorAddress,MuniResRebateCoolHome_ContractorCity,MuniResRebateCoolHome_ContractorState,
				MuniResRebateCoolHome_ContractorZip,MuniResRebateCoolHome_ContractorPhone,MuniResRebateCoolHome_ContractorEmail,
				MuniResRebateCoolHome_ContractorNATECertified,MuniResRebateCoolHome_EquipmentInstalledType,MuniResRebateCoolHome_EquipmentNewConstruction,MuniResRebateCoolHome_EquipmentReplacement,
				MuniResRebateCoolHome_EquipmentAdding,MuniResRebateCoolHome_EquipmentNewOrAdd,MuniResRebateCoolHome_EquipmentInstalledDate,
				MuniResRebateCoolHome_EquipmentInstalledCost,MuniResRebateCoolHome_EquipmentAHRIRef,MuniResRebateCoolHome_EquipmentManufacturer,
				MuniResRebateCoolHome_EquipmentTXVorEXV,MuniResRebateCoolHome_EquipmentCondensorModelNumber,MuniResRebateCoolHome_EquipmentCoilModelNumber,
				MuniResRebateCoolHome_EquipmentAHRIRatedSEER,MuniResRebateCoolHome_EquipmentAHRIRatedEER,MuniResRebateCoolHome_EquipmentHSPF,
				MuniResRebateCoolHome_EquipmentNewUnitSize,MuniResRebateCoolHome_EquipmentMiniSplit,MuniResRebateCoolHome_EquipmentRebateAmount,

				MuniResRebateCoolHome_Measure_CentralAC16Manufacturer,MuniResRebateCoolHome_Measure_CentralAC16CondenserModelNumber,MuniResRebateCoolHome_Measure_CentralAC16CoilModelNumber,
				MuniResRebateCoolHome_Measure_CentralAC16SEER,MuniResRebateCoolHome_Measure_CentralAC16EER,MuniResRebateCoolHome_Measure_CentralAC16HSPF,
				MuniResRebateCoolHome_Measure_CentralAC16SizeTons,MuniResRebateCoolHome_Measure_CentralAC16InstallCost,MuniResRebateCoolHome_Measure_CentralAC16Rebate,
				MuniResRebateCoolHome_Measure_CentralAC16QtyInstalled,MuniResRebateCoolHome_Measure_CentralAC16TotalRebate,MuniResRebateCoolHome_Measure_CentralAC16Approved,

				MuniResRebateCoolHome_Measure_CentralAC18Manufacturer,MuniResRebateCoolHome_Measure_CentralAC18CondenserModelNumber,MuniResRebateCoolHome_Measure_CentralAC18CoilModelNumber,
				MuniResRebateCoolHome_Measure_CentralAC18SEER,MuniResRebateCoolHome_Measure_CentralAC18EER,MuniResRebateCoolHome_Measure_CentralAC18HSPF,
				MuniResRebateCoolHome_Measure_CentralAC18SizeTons,MuniResRebateCoolHome_Measure_CentralAC18InstallCost,MuniResRebateCoolHome_Measure_CentralAC18Rebate,
				MuniResRebateCoolHome_Measure_CentralAC18QtyInstalled,MuniResRebateCoolHome_Measure_CentralAC18TotalRebate,MuniResRebateCoolHome_Measure_CentralAC18Approved,

				MuniResRebateCoolHome_Measure_Ductless18Manufacturer,MuniResRebateCoolHome_Measure_Ductless18CondenserModelNumber,MuniResRebateCoolHome_Measure_Ductless18CoilModelNumber,
				MuniResRebateCoolHome_Measure_Ductless18SEER,MuniResRebateCoolHome_Measure_Ductless18EER,MuniResRebateCoolHome_Measure_Ductless18HSPF,
				MuniResRebateCoolHome_Measure_Ductless18SizeTons,MuniResRebateCoolHome_Measure_Ductless18InstallCost,MuniResRebateCoolHome_Measure_Ductless18Rebate,
				MuniResRebateCoolHome_Measure_Ductless18QtyInstalled,MuniResRebateCoolHome_Measure_Ductless18TotalRebate,MuniResRebateCoolHome_Measure_Ductless18Approved,

				MuniResRebateCoolHome_Measure_Ductless20Manufacturer,MuniResRebateCoolHome_Measure_Ductless20CondenserModelNumber,MuniResRebateCoolHome_Measure_Ductless20CoilModelNumber,
				MuniResRebateCoolHome_Measure_Ductless20SEER,MuniResRebateCoolHome_Measure_Ductless20EER,MuniResRebateCoolHome_Measure_Ductless20HSPF,
				MuniResRebateCoolHome_Measure_Ductless20SizeTons,MuniResRebateCoolHome_Measure_Ductless20InstallCost,MuniResRebateCoolHome_Measure_Ductless20Rebate,
				MuniResRebateCoolHome_Measure_Ductless20QtyInstalled,MuniResRebateCoolHome_Measure_Ductless20TotalRebate,MuniResRebateCoolHome_Measure_Ductless20Approved,
			
				MuniResRebateCoolHome_SupportingFile,MuniResRebateCoolHome_CustomerInitials,MuniResRebateCoolHome_StaffInitials,
				MuniResRebateCoolHome_CustomerAcknowledgementDate,MuniResRebateCoolHome_AddedByAdmin,MuniResRebateCoolHome_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,

			?,?,?,
			?,?,?,?,?
			)";
			
			
    public function addRebateCoolHome($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_COOLHOME;
        $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
		$equipmentTypeArrayValues = (count($record->equipmentInstalledType) > 1 ? implode(",",$record->equipmentInstalledType) : $record->equipmentInstalledType);
        $sqlParam = array(
				$record->customerID,$record->utilityAccountNumber,$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->contractorNATECertified,$equipmentTypeArrayValues,$record->equipmentNewConstruction,$record->equipmentReplacement,
				$record->equipmentAdding,$record->equipmentNewOrAdd,MySQLDateUpdate($record->equipmentInstalledDate),
				toInt($record->equipmentInstalledCost),$record->equipmentAHRIRef,$record->equipmentManufacturer,
				$record->equipmentTXVorEXV,$record->equipmentCondensorModelNumber,$record->equipmentCoilModelNumber,
				$record->equipmentAHRIRatedSEER,$record->equipmentAHRIRatedEER,$record->equipmentHSPF,
				$record->equipmentNewUnitSize,$record->equipmentMiniSplit,toInt($record->equipmentRebateAmount),
				
				$record->measure_CentralAC16Manufacturer,$record->measure_CentralAC16CondenserModelNumber,$record->measure_CentralAC16CoilModelNumber,
				$record->measure_CentralAC16SEER,$record->measure_CentralAC16EER,$record->measure_CentralAC16HSPF,
				$record->measure_CentralAC16SizeTons,toInt($record->measure_CentralAC16InstallCost),toInt($record->measure_CentralAC16Rebate),
				$record->measure_CentralAC16QtyInstalled,toInt($record->measure_CentralAC16TotalRebate),$record->measure_CentralAC16Approved,

				$record->measure_CentralAC18Manufacturer,$record->measure_CentralAC18CondenserModelNumber,$record->measure_CentralAC18CoilModelNumber,
				$record->measure_CentralAC18SEER,$record->measure_CentralAC18EER,$record->measure_CentralAC18HSPF,
				$record->measure_CentralAC18SizeTons,toInt($record->measure_CentralAC18InstallCost),toInt($record->measure_CentralAC18Rebate),
				$record->measure_CentralAC18QtyInstalled,toInt($record->measure_CentralAC18TotalRebate),$record->measure_CentralAC18Approved,

				$record->measure_Ductless18Manufacturer,$record->measure_Ductless18CondenserModelNumber,$record->measure_Ductless18CoilModelNumber,
				$record->measure_Ductless18SEER,$record->measure_Ductless18EER,$record->measure_Ductless18HSPF,
				$record->measure_Ductless18SizeTons,toInt($record->measure_Ductless18InstallCost),toInt($record->measure_Ductless18Rebate),
				$record->measure_Ductless18QtyInstalled,toInt($record->measure_Ductless18TotalRebate),$record->measure_Ductless18Approved,

				$record->measure_Ductless20Manufacturer,$record->measure_Ductless20CondenserModelNumber,$record->measure_Ductless20CoilModelNumber,
				$record->measure_Ductless20SEER,$record->measure_Ductless20EER,$record->measure_Ductless20HSPF,
				$record->measure_Ductless20SizeTons,toInt($record->measure_Ductless20InstallCost),toInt($record->measure_Ductless20Rebate),
				$record->measure_Ductless20QtyInstalled,toInt($record->measure_Ductless20TotalRebate),$record->measure_Ductless20Approved,
				
				$record->supportingFile,$record->customerInitials,$record->staffInitials,
				MySQLDateUpdate($record->customerAcknowledgementDate),$adminUserId, $record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateCoolHome failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }	
	
    private static $UPDATE_RES_COOLHOME_SQL = "
        UPDATE muni_res_rebate_coolhome SET 
			MuniResRebateCoolHome_RebateMadePayableTo=?,
			MuniResRebateCoolHome_ContractorName=?,MuniResRebateCoolHome_ContractorContactName=?,MuniResRebateCoolHome_ContractorLicenseNumber=?,MuniResRebateCoolHome_ContractorTaxID=?,
			MuniResRebateCoolHome_ContractorAddress=?,MuniResRebateCoolHome_ContractorCity=?,MuniResRebateCoolHome_ContractorState=?,
			MuniResRebateCoolHome_ContractorZip=?,MuniResRebateCoolHome_ContractorPhone=?,MuniResRebateCoolHome_ContractorEmail=?,
			MuniResRebateCoolHome_ContractorNATECertified=?,MuniResRebateCoolHome_EquipmentInstalledType=?,MuniResRebateCoolHome_EquipmentNewConstruction=?,MuniResRebateCoolHome_EquipmentReplacement=?,
			MuniResRebateCoolHome_EquipmentAdding=?,MuniResRebateCoolHome_EquipmentNewOrAdd=?,MuniResRebateCoolHome_EquipmentInstalledDate=?,
			MuniResRebateCoolHome_EquipmentInstalledCost=?,MuniResRebateCoolHome_EquipmentAHRIRef=?,MuniResRebateCoolHome_EquipmentManufacturer=?,
			MuniResRebateCoolHome_EquipmentTXVorEXV=?,MuniResRebateCoolHome_EquipmentCondensorModelNumber=?,MuniResRebateCoolHome_EquipmentCoilModelNumber=?,
			MuniResRebateCoolHome_EquipmentAHRIRatedSEER=?,MuniResRebateCoolHome_EquipmentAHRIRatedEER=?,MuniResRebateCoolHome_EquipmentHSPF=?,
			MuniResRebateCoolHome_EquipmentNewUnitSize=?,MuniResRebateCoolHome_EquipmentMiniSplit=?,MuniResRebateCoolHome_EquipmentRebateAmount=?,

			MuniResRebateCoolHome_Measure_CentralAC16Manufacturer=?,MuniResRebateCoolHome_Measure_CentralAC16CondenserModelNumber=?,MuniResRebateCoolHome_Measure_CentralAC16CoilModelNumber=?,
			MuniResRebateCoolHome_Measure_CentralAC16SEER=?,MuniResRebateCoolHome_Measure_CentralAC16EER=?,MuniResRebateCoolHome_Measure_CentralAC16HSPF=?,
			MuniResRebateCoolHome_Measure_CentralAC16SizeTons=?,MuniResRebateCoolHome_Measure_CentralAC16InstallCost=?,MuniResRebateCoolHome_Measure_CentralAC16Rebate=?,
			MuniResRebateCoolHome_Measure_CentralAC16QtyInstalled=?,MuniResRebateCoolHome_Measure_CentralAC16TotalRebate=?,MuniResRebateCoolHome_Measure_CentralAC16Approved=?,

			MuniResRebateCoolHome_Measure_CentralAC18Manufacturer=?,MuniResRebateCoolHome_Measure_CentralAC18CondenserModelNumber=?,MuniResRebateCoolHome_Measure_CentralAC18CoilModelNumber=?,
			MuniResRebateCoolHome_Measure_CentralAC18SEER=?,MuniResRebateCoolHome_Measure_CentralAC18EER=?,MuniResRebateCoolHome_Measure_CentralAC18HSPF=?,
			MuniResRebateCoolHome_Measure_CentralAC18SizeTons=?,MuniResRebateCoolHome_Measure_CentralAC18InstallCost=?,MuniResRebateCoolHome_Measure_CentralAC18Rebate=?,
			MuniResRebateCoolHome_Measure_CentralAC18QtyInstalled=?,MuniResRebateCoolHome_Measure_CentralAC18TotalRebate=?,MuniResRebateCoolHome_Measure_CentralAC18Approved=?,

			MuniResRebateCoolHome_Measure_Ductless18Manufacturer=?,MuniResRebateCoolHome_Measure_Ductless18CondenserModelNumber=?,MuniResRebateCoolHome_Measure_Ductless18CoilModelNumber=?,
			MuniResRebateCoolHome_Measure_Ductless18SEER=?,MuniResRebateCoolHome_Measure_Ductless18EER=?,MuniResRebateCoolHome_Measure_Ductless18HSPF=?,
			MuniResRebateCoolHome_Measure_Ductless18SizeTons=?,MuniResRebateCoolHome_Measure_Ductless18InstallCost=?,MuniResRebateCoolHome_Measure_Ductless18Rebate=?,
			MuniResRebateCoolHome_Measure_Ductless18QtyInstalled=?,MuniResRebateCoolHome_Measure_Ductless18TotalRebate=?,MuniResRebateCoolHome_Measure_Ductless18Approved=?,

			MuniResRebateCoolHome_Measure_Ductless20Manufacturer=?,MuniResRebateCoolHome_Measure_Ductless20CondenserModelNumber=?,MuniResRebateCoolHome_Measure_Ductless20CoilModelNumber=?,
			MuniResRebateCoolHome_Measure_Ductless20SEER=?,MuniResRebateCoolHome_Measure_Ductless20EER=?,MuniResRebateCoolHome_Measure_Ductless20HSPF=?,
			MuniResRebateCoolHome_Measure_Ductless20SizeTons=?,MuniResRebateCoolHome_Measure_Ductless20InstallCost=?,MuniResRebateCoolHome_Measure_Ductless20Rebate=?,
			MuniResRebateCoolHome_Measure_Ductless20QtyInstalled=?,MuniResRebateCoolHome_Measure_Ductless20TotalRebate=?,MuniResRebateCoolHome_Measure_Ductless20Approved=?,
			
			MuniResRebateCoolHome_SupportingFile=?,MuniResRebateCoolHome_CustomerInitials=?,MuniResRebateCoolHome_StaffInitials=?,
			MuniResRebateCoolHome_CustomerAcknowledgementDate=?
		WHERE MuniResRebateCoolHome_ID = ?";
    public function updateRebateCoolHome($record,$adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebateCoolHome_AddedByAdmin","MuniResRebateCoolHome_ID","MuniResRebateCoolHome_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_COOLHOME_SQL;
            $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssi";
			$equipmentTypeArrayValues = (count($record->equipmentInstalledType) > 1 ? implode(",",$record->equipmentInstalledType) : $record->equipmentInstalledType);
            $sqlParams = array(
				$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->contractorNATECertified,$equipmentTypeArrayValues,$record->equipmentNewConstruction,$record->equipmentReplacement,
				$record->equipmentAdding,$record->equipmentNewOrAdd,MySQLDateUpdate($record->equipmentInstalledDate),
				toInt($record->equipmentInstalledCost),$record->equipmentAHRIRef,$record->equipmentManufacturer,
				$record->equipmentTXVorEXV,$record->equipmentCondensorModelNumber,$record->equipmentCoilModelNumber,
				$record->equipmentAHRIRatedSEER,$record->equipmentAHRIRatedEER,$record->equipmentHSPF,
				$record->equipmentNewUnitSize,$record->equipmentMiniSplit,toInt($record->equipmentRebateAmount),
				
				$record->measure_CentralAC16Manufacturer,$record->measure_CentralAC16CondenserModelNumber,$record->measure_CentralAC16CoilModelNumber,
				$record->measure_CentralAC16SEER,$record->measure_CentralAC16EER,$record->measure_CentralAC16HSPF,
				$record->measure_CentralAC16SizeTons,toInt($record->measure_CentralAC16InstallCost),toInt($record->measure_CentralAC16Rebate),
				$record->measure_CentralAC16QtyInstalled,toInt($record->measure_CentralAC16TotalRebate),$record->measure_CentralAC16Approved,

				$record->measure_CentralAC18Manufacturer,$record->measure_CentralAC18CondenserModelNumber,$record->measure_CentralAC18CoilModelNumber,
				$record->measure_CentralAC18SEER,$record->measure_CentralAC18EER,$record->measure_CentralAC18HSPF,
				$record->measure_CentralAC18SizeTons,toInt($record->measure_CentralAC18InstallCost),toInt($record->measure_CentralAC18Rebate),
				$record->measure_CentralAC18QtyInstalled,toInt($record->measure_CentralAC18TotalRebate),$record->measure_CentralAC18Approved,

				$record->measure_Ductless18Manufacturer,$record->measure_Ductless18CondenserModelNumber,$record->measure_Ductless18CoilModelNumber,
				$record->measure_Ductless18SEER,$record->measure_Ductless18EER,$record->measure_Ductless18HSPF,
				$record->measure_Ductless18SizeTons,toInt($record->measure_Ductless18InstallCost),toInt($record->measure_Ductless18Rebate),
				$record->measure_Ductless18QtyInstalled,toInt($record->measure_Ductless18TotalRebate),$record->measure_Ductless18Approved,

				$record->measure_Ductless20Manufacturer,$record->measure_Ductless20CondenserModelNumber,$record->measure_Ductless20CoilModelNumber,
				$record->measure_Ductless20SEER,$record->measure_Ductless20EER,$record->measure_Ductless20HSPF,
				$record->measure_Ductless20SizeTons,toInt($record->measure_Ductless20InstallCost),toInt($record->measure_Ductless20Rebate),
				$record->measure_Ductless20QtyInstalled,toInt($record->measure_Ductless20TotalRebate),$record->measure_Ductless20Approved,
				
				$record->supportingFile,$record->customerInitials,$record->staffInitials,
				MySQLDateUpdate($record->customerAcknowledgementDate),
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateCoolHome failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $this->trackChanges("muni_res_rebate_coolhome",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $changedFields;
    }
	
	private static $INSERT_SQL_RES_APPLIANCE ="
		INSERT INTO muni_res_rebate_appliance
			(
				MuniResRebateAppliance_CustomerID,MuniResRebateAppliance_UtilityAccountNumber,MuniResRebateAppliance_RebateMadePayableTo,
				MuniResRebateAppliance_ContractorName,MuniResRebateAppliance_ContractorContactName,MuniResRebateAppliance_ContractorLicenseNumber,MuniResRebateAppliance_ContractorTaxID,
				MuniResRebateAppliance_ContractorAddress,MuniResRebateAppliance_ContractorCity,MuniResRebateAppliance_ContractorState,
				MuniResRebateAppliance_ContractorZip,MuniResRebateAppliance_ContractorPhone,MuniResRebateAppliance_ContractorEmail,
				MuniResRebateAppliance_ApplianceType,MuniResRebateAppliance_ApplianceBrand,MuniResRebateAppliance_ApplianceModelNumber,
				MuniResRebateAppliance_ApplianceSerialNumber,MuniResRebateAppliance_StoreName,MuniResRebateAppliance_StoreAddress,
				MuniResRebateAppliance_StoreCity,MuniResRebateAppliance_StoreState,MuniResRebateAppliance_StoreZipCode,
				MuniResRebateAppliance_StorePurchasePrice,MuniResRebateAppliance_StorePurchaseDate,MuniResRebateAppliance_SupportingFile,
				MuniResRebateAppliance_CustomerInitials,MuniResRebateAppliance_StaffInitials,MuniResRebateAppliance_CustomerAcknowledgementDate,
				MuniResRebateAppliance_AddedByAdmin,MuniResRebateAppliance_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,?
			)";
			
    public function addRebateAppliance($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_RES_APPLIANCE;
        $sqlBinding = "ssssssssssssssssssssssssssssssss";
        $sqlParam = array(
				$record->customerID,$record->utilityAccountNumber,$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->applianceType,$record->applianceBrand,$record->applianceModelNumber,
				$record->applianceSerialNumber,$record->storeName,$record->storeAddress,
				$record->storeCity,$record->storeState,$record->storeZipCode,
				toInt($record->storePurchasePrice),MySQLDateUpdate($record->storePurchaseDate),$record->supportingFile,
				$record->customerInitials,$record->staffInitials,MySQLDateUpdate($record->customerAcknowledgementDate),
				$adminUserId,$record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateAppliance failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }	
    private static $UPDATE_RES_APPLIANCE_SQL = "
        UPDATE muni_res_rebate_appliance SET 
				MuniResRebateAppliance_RebateMadePayableTo=?,
				MuniResRebateAppliance_ContractorName=?,MuniResRebateAppliance_ContractorContactName=?,MuniResRebateAppliance_ContractorLicenseNumber=?,MuniResRebateAppliance_ContractorTaxID=?,
				MuniResRebateAppliance_ContractorAddress=?,MuniResRebateAppliance_ContractorCity=?,MuniResRebateAppliance_ContractorState=?,
				MuniResRebateAppliance_ContractorZip=?,MuniResRebateAppliance_ContractorPhone=?,MuniResRebateAppliance_ContractorEmail=?,
				MuniResRebateAppliance_ApplianceType=?,MuniResRebateAppliance_ApplianceBrand=?,MuniResRebateAppliance_ApplianceModelNumber=?,
				MuniResRebateAppliance_ApplianceSerialNumber=?,MuniResRebateAppliance_StoreName=?,MuniResRebateAppliance_StoreAddress=?,
				MuniResRebateAppliance_StoreCity=?,MuniResRebateAppliance_StoreState=?,MuniResRebateAppliance_StoreZipCode=?,
				MuniResRebateAppliance_StorePurchasePrice=?,MuniResRebateAppliance_StorePurchaseDate=?,MuniResRebateAppliance_SupportingFile=?,
				MuniResRebateAppliance_CustomerInitials=?,MuniResRebateAppliance_StaffInitials=?,MuniResRebateAppliance_CustomerAcknowledgementDate=?
		WHERE MuniResRebateAppliance_ID = ?";
    public function updateRebateAppliance($record,$adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniResRebateAppliance_AddedByAdmin","MuniResRebateAppliance_ID","MuniResRebateAppliance_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_RES_APPLIANCE_SQL;
            $sqlBinding = "ssssssssssssssssssssssssssi";
            $sqlParams = array(
				$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->applianceType,$record->applianceBrand,$record->applianceModelNumber,
				$record->applianceSerialNumber,$record->storeName,$record->storeAddress,
				$record->storeCity,$record->storeState,$record->storeZipCode,
				toInt($record->storePurchasePrice),MySQLDateUpdate($record->storePurchaseDate),$record->supportingFile,
				$record->customerInitials,$record->staffInitials,MySQLDateUpdate($record->customerAcknowledgementDate),
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateAppliance failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $adminUserId;
						$changedResults[] = $this->trackChanges("muni_res_rebate_appliance",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $updateResponse;
    }
	
	private static $INSERT_SQL_COM_HEATHOTWATER ="
		INSERT INTO muni_com_rebate_heathotwater
			(
				MuniComRebateHeatHotwater_CustomerID,MuniComRebateHeatHotwater_UtilityAccountNumber,MuniComRebateHeatHotwater_RebateMadePayableTo,
				MuniComRebateHeatHotwater_ContractorName,MuniComRebateHeatHotwater_ContractorContactName,MuniComRebateHeatHotwater_ContractorLicenseNumber,MuniComRebateHeatHotwater_ContractorTaxID,
				MuniComRebateHeatHotwater_ContractorAddress,MuniComRebateHeatHotwater_ContractorCity,MuniComRebateHeatHotwater_ContractorState,
				MuniComRebateHeatHotwater_ContractorZip,MuniComRebateHeatHotwater_ContractorPhone,MuniComRebateHeatHotwater_ContractorEmail,
				MuniComRebateHeatHotwater_OwnedOrLeased,MuniComRebateHeatHotwater_FacilityName,MuniComRebateHeatHotwater_FacilityContact,
				MuniComRebateHeatHotwater_FacilityEmail,MuniComRebateHeatHotwater_FacilityPhone,MuniComRebateHeatHotwater_FacilityInstalledAddress,
				MuniComRebateHeatHotwater_FacilityInstalledCity,MuniComRebateHeatHotwater_FacilityInstalledState,MuniComRebateHeatHotwater_FacilityInstalledZip,
				MuniComRebateHeatHotwater_FacilitySqrFtMainBuilding,MuniComRebateHeatHotwater_FacilitySqrFtOtherBuildings,MuniComRebateHeatHotwater_FacilityApproximateAgeInYears,
				MuniComRebateHeatHotwater_FacilityType,MuniComRebateHeatHotwater_FacilityOverviewOfSpace,
				MuniComRebateHeatHotwater_Measure_Furnace95Manufacturer,MuniComRebateHeatHotwater_Measure_Furnace95ModelNumber,MuniComRebateHeatHotwater_Measure_Furnace95EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Furnace95InputSize,MuniComRebateHeatHotwater_Measure_Furnace95RebateAmount,MuniComRebateHeatHotwater_Measure_Furnace95QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Furnace95InstalledCost, MuniComRebateHeatHotwater_Measure_Furnace95Approved, 
				MuniComRebateHeatHotwater_Measure_Furnace97Manufacturer,MuniComRebateHeatHotwater_Measure_Furnace97ModelNumber,MuniComRebateHeatHotwater_Measure_Furnace97EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Furnace97InputSize,MuniComRebateHeatHotwater_Measure_Furnace97RebateAmount,MuniComRebateHeatHotwater_Measure_Furnace97QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Furnace97InstalledCost, MuniComRebateHeatHotwater_Measure_Furnace97Approved,
				MuniComRebateHeatHotwater_Measure_Boiler85Manufacturer,MuniComRebateHeatHotwater_Measure_Boiler85ModelNumber,MuniComRebateHeatHotwater_Measure_Boiler85EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Boiler85InputSize,MuniComRebateHeatHotwater_Measure_Boiler85RebateAmount,MuniComRebateHeatHotwater_Measure_Boiler85QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Boiler85InstalledCost, MuniComRebateHeatHotwater_Measure_Boiler85Approved,
				MuniComRebateHeatHotwater_Measure_Boiler90Manufacturer,MuniComRebateHeatHotwater_Measure_Boiler90ModelNumber,MuniComRebateHeatHotwater_Measure_Boiler90EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Boiler90InputSize,MuniComRebateHeatHotwater_Measure_Boiler90RebateAmount,MuniComRebateHeatHotwater_Measure_Boiler90QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Boiler90InstalledCost, MuniComRebateHeatHotwater_Measure_Boiler90Approved,
				MuniComRebateHeatHotwater_Measure_Boiler95Manufacturer,MuniComRebateHeatHotwater_Measure_Boiler95ModelNumber,MuniComRebateHeatHotwater_Measure_Boiler95EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Boiler95InputSize,MuniComRebateHeatHotwater_Measure_Boiler95RebateAmount,MuniComRebateHeatHotwater_Measure_Boiler95QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Boiler95InstalledCost, MuniComRebateHeatHotwater_Measure_Boiler95Approved,
				MuniComRebateHeatHotwater_Measure_Integrated90Manufacturer,MuniComRebateHeatHotwater_Measure_Integrated90ModelNumber,MuniComRebateHeatHotwater_Measure_Integrated90EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Integrated90InputSize,MuniComRebateHeatHotwater_Measure_Integrated90RebateAmount,MuniComRebateHeatHotwater_Measure_Integrated90QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Integrated90InstalledCost, MuniComRebateHeatHotwater_Measure_Integrated90Approved,
				MuniComRebateHeatHotwater_Measure_Integrated95Manufacturer,MuniComRebateHeatHotwater_Measure_Integrated95ModelNumber,MuniComRebateHeatHotwater_Measure_Integrated95EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Integrated95InputSize,MuniComRebateHeatHotwater_Measure_Integrated95RebateAmount,MuniComRebateHeatHotwater_Measure_Integrated95QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Integrated95InstalledCost, MuniComRebateHeatHotwater_Measure_Integrated95Approved,
				MuniComRebateHeatHotwater_Measure_IndirectWaterManufacturer,MuniComRebateHeatHotwater_Measure_IndirectWaterModelNumber,MuniComRebateHeatHotwater_Measure_IndirectWaterEfficiencyRating,
				MuniComRebateHeatHotwater_Measure_IndirectWaterInputSize,MuniComRebateHeatHotwater_Measure_IndirectWaterRebateAmount,MuniComRebateHeatHotwater_Measure_IndirectWaterQuantityInstalled,
				MuniComRebateHeatHotwater_Measure_IndirectWaterInstalledCost, MuniComRebateHeatHotwater_Measure_IndirectWaterApproved,
				MuniComRebateHeatHotwater_Measure_H2OStorage62Manufacturer,MuniComRebateHeatHotwater_Measure_H2OStorage62ModelNumber,MuniComRebateHeatHotwater_Measure_H2OStorage62EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_H2OStorage62InputSize,MuniComRebateHeatHotwater_Measure_H2OStorage62RebateAmount,MuniComRebateHeatHotwater_Measure_H2OStorage62QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_H2OStorage62InstalledCost, MuniComRebateHeatHotwater_Measure_H2OStorage62Approved,
				MuniComRebateHeatHotwater_Measure_H2OStorage67Manufacturer,MuniComRebateHeatHotwater_Measure_H2OStorage67ModelNumber,MuniComRebateHeatHotwater_Measure_H2OStorage67EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_H2OStorage67InputSize,MuniComRebateHeatHotwater_Measure_H2OStorage67RebateAmount,MuniComRebateHeatHotwater_Measure_H2OStorage67QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_H2OStorage67InstalledCost, MuniComRebateHeatHotwater_Measure_H2OStorage67Approved,
				MuniComRebateHeatHotwater_Measure_Tankless82Manufacturer,MuniComRebateHeatHotwater_Measure_Tankless82ModelNumber,MuniComRebateHeatHotwater_Measure_Tankless82EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Tankless82InputSize,MuniComRebateHeatHotwater_Measure_Tankless82RebateAmount,MuniComRebateHeatHotwater_Measure_Tankless82QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Tankless82InstalledCost, MuniComRebateHeatHotwater_Measure_Tankless82Approved,
				MuniComRebateHeatHotwater_Measure_Tankless94Manufacturer,MuniComRebateHeatHotwater_Measure_Tankless94ModelNumber,MuniComRebateHeatHotwater_Measure_Tankless94EfficiencyRating,
				MuniComRebateHeatHotwater_Measure_Tankless94InputSize,MuniComRebateHeatHotwater_Measure_Tankless94RebateAmount,MuniComRebateHeatHotwater_Measure_Tankless94QuantityInstalled,
				MuniComRebateHeatHotwater_Measure_Tankless94InstalledCost, MuniComRebateHeatHotwater_Measure_Tankless94Approved,
				
				MuniComRebateHeatHotwater_TotalAnticipatedRebate,MuniComRebateHeatHotwater_InstalledCost,MuniComRebateHeatHotwater_InstalledDate,
				MuniComRebateHeatHotwater_SupportingFile,MuniComRebateHeatHotwater_CustomerInitials,MuniComRebateHeatHotwater_StaffInitials,
				MuniComRebateHeatHotwater_CustomerAcknowledgementDate,MuniComRebateHeatHotwater_AddedByAdmin,MuniComRebateHeatHotwater_RawFormData,ipAddress,oid
			) 
			VALUES (
			?,?,?,
			?,?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,?,
			?,?,
			
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			?,?,?,
			?,?,?,
			?,?,
			
			?,?,?,
			?,?,?,
			?,?,?,?,?
			)";
			
    public function addRebateCommercialHeat($record, $adminUserId){
        $sqlString = self::$INSERT_SQL_COM_HEATHOTWATER;
        $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
		$facilityTypeArrayValues = (count($record->facilityType) > 1 ? implode(",",$record->facilityType) : $record->facilityType);
        $sqlParam = array(
				$record->customerID,$record->utilityAccountNumber,$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->ownedOrLeased,$record->facilityName,$record->facilityContact,
				$record->facilityEmail,$record->facilityPhone,$record->facilityInstalledAddress,
				$record->facilityInstalledCity,$record->facilityInstalledState,$record->facilityInstalledZip,
				$record->facilitySqrFtMainBuilding,$record->facilitySqrFtOtherBuildings,$record->facilityApproximateAgeInYears,
				$facilityTypeArrayValues,$record->facilityOverviewOfSpace,
				$record->measure_Furnace95Manufacturer,$record->measure_Furnace95ModelNumber,$record->measure_Furnace95EfficiencyRating,
				$record->measure_Furnace95InputSize,toInt($record->measure_Furnace95RebateAmount),$record->measure_Furnace95QuantityInstalled,
				toInt($record->measure_Furnace95InstalledCost), $record->measure_Furnace95Approved,
				$record->measure_Furnace97Manufacturer,$record->measure_Furnace97ModelNumber,$record->measure_Furnace97EfficiencyRating,
				$record->measure_Furnace97InputSize,toInt($record->measure_Furnace97RebateAmount),$record->measure_Furnace97QuantityInstalled,
				toInt($record->measure_Furnace97InstalledCost), $record->measure_Furnace97Approved,
				$record->measure_Boiler85Manufacturer,$record->measure_Boiler85ModelNumber,$record->measure_Boiler85EfficiencyRating,
				$record->measure_Boiler85InputSize,toInt($record->measure_Boiler85RebateAmount),$record->measure_Boiler85QuantityInstalled,
				toInt($record->measure_Boiler85InstalledCost), $record->measure_Boiler85Approved,
				$record->measure_Boiler90Manufacturer,$record->measure_Boiler90ModelNumber,$record->measure_Boiler90EfficiencyRating,
				$record->measure_Boiler90InputSize,toInt($record->measure_Boiler90RebateAmount),$record->measure_Boiler90QuantityInstalled,
				toInt($record->measure_Boiler90InstalledCost), $record->measure_Boiler90Approved,
				$record->measure_Boiler95Manufacturer,$record->measure_Boiler95ModelNumber,$record->measure_Boiler95EfficiencyRating,
				$record->measure_Boiler95InputSize,toInt($record->measure_Boiler95RebateAmount),$record->measure_Boiler95QuantityInstalled,
				toInt($record->measure_Boiler95InstalledCost), $record->measure_Boiler95Approved,
				$record->measure_Integrated90Manufacturer,$record->measure_Integrated90ModelNumber,$record->measure_Integrated90EfficiencyRating,
				$record->measure_Integrated90InputSize,toInt($record->measure_Integrated90RebateAmount),$record->measure_Integrated90QuantityInstalled,
				toInt($record->measure_Integrated90InstalledCost), $record->measure_Integrated90Approved,
				$record->measure_Integrated95Manufacturer,$record->measure_Integrated95ModelNumber,$record->measure_Integrated95EfficiencyRating,
				$record->measure_Integrated95InputSize,toInt($record->measure_Integrated95RebateAmount),$record->measure_Integrated95QuantityInstalled,
				toInt($record->measure_Integrated95InstalledCost), $record->measure_Integrated95Approved,
				$record->measure_IndirectWaterManufacturer,$record->measure_IndirectWaterModelNumber,$record->measure_IndirectWaterEfficiencyRating,
				$record->measure_IndirectWaterInputSize,toInt($record->measure_IndirectWaterRebateAmount),$record->measure_IndirectWaterQuantityInstalled,
				toInt($record->measure_IndirectWaterInstalledCost), $record->measure_IndirectWaterApproved,
				$record->measure_H2OStorage62Manufacturer,$record->measure_H2OStorage62ModelNumber,$record->measure_H2OStorage62EfficiencyRating,
				$record->measure_H2OStorage62InputSize,toInt($record->measure_H2OStorage62RebateAmount),$record->measure_H2OStorage62QuantityInstalled,
				toInt($record->measure_H2OStorage62InstalledCost), $record->measure_H2OStorage62Approved,
				$record->measure_H2OStorage67Manufacturer,$record->measure_H2OStorage67ModelNumber,$record->measure_H2OStorage67EfficiencyRating,
				$record->measure_H2OStorage67InputSize,toInt($record->measure_H2OStorage67RebateAmount),$record->measure_H2OStorage67QuantityInstalled,
				toInt($record->measure_H2OStorage67InstalledCost), $record->measure_H2OStorage67Approved,
				$record->measure_Tankless82Manufacturer,$record->measure_Tankless82ModelNumber,$record->measure_Tankless82EfficiencyRating,
				$record->measure_Tankless82InputSize,toInt($record->measure_Tankless82RebateAmount),$record->measure_Tankless82QuantityInstalled,
				toInt($record->measure_Tankless82InstalledCost), $record->measure_Tankless82Approved,
				$record->measure_Tankless94Manufacturer,$record->measure_Tankless94ModelNumber,$record->measure_Tankless94EfficiencyRating,
				$record->measure_Tankless94InputSize,toInt($record->measure_Tankless94RebateAmount),$record->measure_Tankless94QuantityInstalled,
				toInt($record->measure_Tankless94InstalledCost), $record->measure_Tankless94Approved,
				toInt($record->totalAnticipatedRebate),toInt($record->installedCost),MySQLDateUpdate($record->installedDate),
				$record->supportingFile,$record->customerInitials,$record->staffInitials,
				MySQLDateUpdate($record->customerAcknowledgementDate),$adminUserId,$record->rawFormData,$record->ipAddress,$record->oid
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("MuniProvider.addRebateCommercialHeat failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }	
    private static $UPDATE_COM_HEATHOTWATER_SQL = "
        UPDATE muni_com_rebate_heathotwater SET 
			MuniComRebateHeatHotwater_RebateMadePayableTo=?,
			MuniComRebateHeatHotwater_ContractorName=?,MuniComRebateHeatHotwater_ContractorContactName=?,MuniComRebateHeatHotwater_ContractorLicenseNumber=?,MuniComRebateHeatHotwater_ContractorTaxID=?,
			MuniComRebateHeatHotwater_ContractorAddress=?,MuniComRebateHeatHotwater_ContractorCity=?,MuniComRebateHeatHotwater_ContractorState=?,
			MuniComRebateHeatHotwater_ContractorZip=?,MuniComRebateHeatHotwater_ContractorPhone=?,MuniComRebateHeatHotwater_ContractorEmail=?,
			MuniComRebateHeatHotwater_OwnedOrLeased=?,MuniComRebateHeatHotwater_FacilityName=?,MuniComRebateHeatHotwater_FacilityContact=?,
			MuniComRebateHeatHotwater_FacilityEmail=?,MuniComRebateHeatHotwater_FacilityPhone=?,MuniComRebateHeatHotwater_FacilityInstalledAddress=?,
			MuniComRebateHeatHotwater_FacilityInstalledCity=?,MuniComRebateHeatHotwater_FacilityInstalledState=?,MuniComRebateHeatHotwater_FacilityInstalledZip=?,
			MuniComRebateHeatHotwater_FacilitySqrFtMainBuilding=?,MuniComRebateHeatHotwater_FacilitySqrFtOtherBuildings=?,MuniComRebateHeatHotwater_FacilityApproximateAgeInYears=?,
			MuniComRebateHeatHotwater_FacilityType=?,MuniComRebateHeatHotwater_FacilityOverviewOfSpace=?,
			MuniComRebateHeatHotwater_Measure_Furnace95Manufacturer=?,MuniComRebateHeatHotwater_Measure_Furnace95ModelNumber=?,MuniComRebateHeatHotwater_Measure_Furnace95EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Furnace95InputSize=?,MuniComRebateHeatHotwater_Measure_Furnace95RebateAmount=?,MuniComRebateHeatHotwater_Measure_Furnace95QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Furnace95InstalledCost=?, MuniComRebateHeatHotwater_Measure_Furnace95Approved=?,
			MuniComRebateHeatHotwater_Measure_Furnace97Manufacturer=?,MuniComRebateHeatHotwater_Measure_Furnace97ModelNumber=?,MuniComRebateHeatHotwater_Measure_Furnace97EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Furnace97InputSize=?,MuniComRebateHeatHotwater_Measure_Furnace97RebateAmount=?,MuniComRebateHeatHotwater_Measure_Furnace97QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Furnace97InstalledCost=?, MuniComRebateHeatHotwater_Measure_Furnace97Approved=?, 
			MuniComRebateHeatHotwater_Measure_Boiler85Manufacturer=?,MuniComRebateHeatHotwater_Measure_Boiler85ModelNumber=?,MuniComRebateHeatHotwater_Measure_Boiler85EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Boiler85InputSize=?,MuniComRebateHeatHotwater_Measure_Boiler85RebateAmount=?,MuniComRebateHeatHotwater_Measure_Boiler85QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Boiler85InstalledCost=?, MuniComRebateHeatHotwater_Measure_Boiler85Approved=?, 
			MuniComRebateHeatHotwater_Measure_Boiler90Manufacturer=?,MuniComRebateHeatHotwater_Measure_Boiler90ModelNumber=?,MuniComRebateHeatHotwater_Measure_Boiler90EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Boiler90InputSize=?,MuniComRebateHeatHotwater_Measure_Boiler90RebateAmount=?,MuniComRebateHeatHotwater_Measure_Boiler90QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Boiler90InstalledCost=?, MuniComRebateHeatHotwater_Measure_Boiler90Approved=?, 
			MuniComRebateHeatHotwater_Measure_Boiler95Manufacturer=?,MuniComRebateHeatHotwater_Measure_Boiler95ModelNumber=?,MuniComRebateHeatHotwater_Measure_Boiler95EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Boiler95InputSize=?,MuniComRebateHeatHotwater_Measure_Boiler95RebateAmount=?,MuniComRebateHeatHotwater_Measure_Boiler95QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Boiler95InstalledCost=?, MuniComRebateHeatHotwater_Measure_Boiler95Approved=?, 
			MuniComRebateHeatHotwater_Measure_Integrated90Manufacturer=?,MuniComRebateHeatHotwater_Measure_Integrated90ModelNumber=?,MuniComRebateHeatHotwater_Measure_Integrated90EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Integrated90InputSize=?,MuniComRebateHeatHotwater_Measure_Integrated90RebateAmount=?,MuniComRebateHeatHotwater_Measure_Integrated90QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Integrated90InstalledCost=?, MuniComRebateHeatHotwater_Measure_Integrated90Approved=?, 
			MuniComRebateHeatHotwater_Measure_Integrated95Manufacturer=?,MuniComRebateHeatHotwater_Measure_Integrated95ModelNumber=?,MuniComRebateHeatHotwater_Measure_Integrated95EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Integrated95InputSize=?,MuniComRebateHeatHotwater_Measure_Integrated95RebateAmount=?,MuniComRebateHeatHotwater_Measure_Integrated95QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Integrated95InstalledCost=?, MuniComRebateHeatHotwater_Measure_Integrated95Approved=?, 
			MuniComRebateHeatHotwater_Measure_IndirectWaterManufacturer=?,MuniComRebateHeatHotwater_Measure_IndirectWaterModelNumber=?,MuniComRebateHeatHotwater_Measure_IndirectWaterEfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_IndirectWaterInputSize=?,MuniComRebateHeatHotwater_Measure_IndirectWaterRebateAmount=?,MuniComRebateHeatHotwater_Measure_IndirectWaterQuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_IndirectWaterInstalledCost=?, MuniComRebateHeatHotwater_Measure_IndirectWaterApproved=?, 
			MuniComRebateHeatHotwater_Measure_H2OStorage62Manufacturer=?,MuniComRebateHeatHotwater_Measure_H2OStorage62ModelNumber=?,MuniComRebateHeatHotwater_Measure_H2OStorage62EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_H2OStorage62InputSize=?,MuniComRebateHeatHotwater_Measure_H2OStorage62RebateAmount=?,MuniComRebateHeatHotwater_Measure_H2OStorage62QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_H2OStorage62InstalledCost=?, MuniComRebateHeatHotwater_Measure_H2OStorage62Approved=?, 
			MuniComRebateHeatHotwater_Measure_H2OStorage67Manufacturer=?,MuniComRebateHeatHotwater_Measure_H2OStorage67ModelNumber=?,MuniComRebateHeatHotwater_Measure_H2OStorage67EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_H2OStorage67InputSize=?,MuniComRebateHeatHotwater_Measure_H2OStorage67RebateAmount=?,MuniComRebateHeatHotwater_Measure_H2OStorage67QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_H2OStorage67InstalledCost=?, MuniComRebateHeatHotwater_Measure_H2OStorage67Approved=?, 
			MuniComRebateHeatHotwater_Measure_Tankless82Manufacturer=?,MuniComRebateHeatHotwater_Measure_Tankless82ModelNumber=?,MuniComRebateHeatHotwater_Measure_Tankless82EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Tankless82InputSize=?,MuniComRebateHeatHotwater_Measure_Tankless82RebateAmount=?,MuniComRebateHeatHotwater_Measure_Tankless82QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Tankless82InstalledCost=?, MuniComRebateHeatHotwater_Measure_Tankless82Approved=?, 
			MuniComRebateHeatHotwater_Measure_Tankless94Manufacturer=?,MuniComRebateHeatHotwater_Measure_Tankless94ModelNumber=?,MuniComRebateHeatHotwater_Measure_Tankless94EfficiencyRating=?,
			MuniComRebateHeatHotwater_Measure_Tankless94InputSize=?,MuniComRebateHeatHotwater_Measure_Tankless94RebateAmount=?,MuniComRebateHeatHotwater_Measure_Tankless94QuantityInstalled=?,
			MuniComRebateHeatHotwater_Measure_Tankless94InstalledCost=?, MuniComRebateHeatHotwater_Measure_Tankless94Approved=?, 
			MuniComRebateHeatHotwater_TotalAnticipatedRebate=?,MuniComRebateHeatHotwater_InstalledCost=?,MuniComRebateHeatHotwater_InstalledDate=?,
			MuniComRebateHeatHotwater_SupportingFile=?,MuniComRebateHeatHotwater_CustomerInitials=?,MuniComRebateHeatHotwater_StaffInitials=?,
			MuniComRebateHeatHotwater_CustomerAcknowledgementDate=?
		WHERE MuniComRebateHeatHotwater_ID = ?";
    public function updateRebateCommercialHeat($record,$adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			//before doing an update get current values to check for changes
			$changedFields = array();
			$fieldExclusions = array("MuniComRebateHeatHotwater_AddedByAdmin","MuniComRebateHeatHotwater_ID","MuniComRebateHeatHotwater_ReceivedTimeStamp");
			$fieldExclusionList = $this->commonRebateExclusionFields($fieldExclusions);
			$changedFields = $this->checkforchanges($record,"getRebateInfo",$fieldExclusionList);
			
            $sqlString = self::$UPDATE_COM_HEATHOTWATER_SQL;
            $sqlBinding = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssi";
			$facilityTypeArrayValues = (count($record->facilityType) > 1 ? implode(",",$record->facilityType) : $record->facilityType);
            $sqlParams = array(
				$record->rebateMadePayableTo,
				$record->contractorName,$record->contractorContactName,$record->contractorLicenseNumber,$record->contractorTaxID,
				$record->contractorAddress,$record->contractorCity,$record->contractorState,
				$record->contractorZip,$record->contractorPhone,$record->contractorEmail,
				$record->ownedOrLeased,$record->facilityName,$record->facilityContact,
				$record->facilityEmail,$record->facilityPhone,$record->facilityInstalledAddress,
				$record->facilityInstalledCity,$record->facilityInstalledState,$record->facilityInstalledZip,
				$record->facilitySqrFtMainBuilding,$record->facilitySqrFtOtherBuildings,$record->facilityApproximateAgeInYears,
				$facilityTypeArrayValues,$record->facilityOverviewOfSpace,
				$record->measure_Furnace95Manufacturer,$record->measure_Furnace95ModelNumber,$record->measure_Furnace95EfficiencyRating,
				$record->measure_Furnace95InputSize,toInt($record->measure_Furnace95RebateAmount),$record->measure_Furnace95QuantityInstalled,
				toInt($record->measure_Furnace95InstalledCost), $record->measure_Furnace95Approved,
				$record->measure_Furnace97Manufacturer,$record->measure_Furnace97ModelNumber,$record->measure_Furnace97EfficiencyRating,
				$record->measure_Furnace97InputSize,toInt($record->measure_Furnace97RebateAmount),$record->measure_Furnace97QuantityInstalled,
				toInt($record->measure_Furnace97InstalledCost), $record->measure_Furnace97Approved,
				$record->measure_Boiler85Manufacturer,$record->measure_Boiler85ModelNumber,$record->measure_Boiler85EfficiencyRating,
				$record->measure_Boiler85InputSize,toInt($record->measure_Boiler85RebateAmount),$record->measure_Boiler85QuantityInstalled,
				toInt($record->measure_Boiler85InstalledCost), $record->measure_Boiler85Approved,
				$record->measure_Boiler90Manufacturer,$record->measure_Boiler90ModelNumber,$record->measure_Boiler90EfficiencyRating,
				$record->measure_Boiler90InputSize,toInt($record->measure_Boiler90RebateAmount),$record->measure_Boiler90QuantityInstalled,
				toInt($record->measure_Boiler90InstalledCost), $record->measure_Boiler90Approved,
				$record->measure_Boiler95Manufacturer,$record->measure_Boiler95ModelNumber,$record->measure_Boiler95EfficiencyRating,
				$record->measure_Boiler95InputSize,toInt($record->measure_Boiler95RebateAmount),$record->measure_Boiler95QuantityInstalled,
				toInt($record->measure_Boiler95InstalledCost), $record->measure_Boiler95Approved,
				$record->measure_Integrated90Manufacturer,$record->measure_Integrated90ModelNumber,$record->measure_Integrated90EfficiencyRating,
				$record->measure_Integrated90InputSize,toInt($record->measure_Integrated90RebateAmount),$record->measure_Integrated90QuantityInstalled,
				toInt($record->measure_Integrated90InstalledCost), $record->measure_Integrated90Approved,
				$record->measure_Integrated95Manufacturer,$record->measure_Integrated95ModelNumber,$record->measure_Integrated95EfficiencyRating,
				$record->measure_Integrated95InputSize,toInt($record->measure_Integrated95RebateAmount),$record->measure_Integrated95QuantityInstalled,
				toInt($record->measure_Integrated95InstalledCost), $record->measure_Integrated95Approved,
				$record->measure_IndirectWaterManufacturer,$record->measure_IndirectWaterModelNumber,$record->measure_IndirectWaterEfficiencyRating,
				$record->measure_IndirectWaterInputSize,toInt($record->measure_IndirectWaterRebateAmount),$record->measure_IndirectWaterQuantityInstalled,
				toInt($record->measure_IndirectWaterInstalledCost), $record->measure_IndirectWaterApproved,
				$record->measure_H2OStorage62Manufacturer,$record->measure_H2OStorage62ModelNumber,$record->measure_H2OStorage62EfficiencyRating,
				$record->measure_H2OStorage62InputSize,toInt($record->measure_H2OStorage62RebateAmount),$record->measure_H2OStorage62QuantityInstalled,
				toInt($record->measure_H2OStorage62InstalledCost), $record->measure_H2OStorage62Approved,
				$record->measure_H2OStorage67Manufacturer,$record->measure_H2OStorage67ModelNumber,$record->measure_H2OStorage67EfficiencyRating,
				$record->measure_H2OStorage67InputSize,toInt($record->measure_H2OStorage67RebateAmount),$record->measure_H2OStorage67QuantityInstalled,
				toInt($record->measure_H2OStorage67InstalledCost), $record->measure_H2OStorage67Approved,
				$record->measure_Tankless82Manufacturer,$record->measure_Tankless82ModelNumber,$record->measure_Tankless82EfficiencyRating,
				$record->measure_Tankless82InputSize,toInt($record->measure_Tankless82RebateAmount),$record->measure_Tankless82QuantityInstalled,
				toInt($record->measure_Tankless82InstalledCost), $record->measure_Tankless82Approved,
				$record->measure_Tankless94Manufacturer,$record->measure_Tankless94ModelNumber,$record->measure_Tankless94EfficiencyRating,
				$record->measure_Tankless94InputSize,toInt($record->measure_Tankless94RebateAmount),$record->measure_Tankless94QuantityInstalled,
				toInt($record->measure_Tankless94InstalledCost), $record->measure_Tankless94Approved,
				toInt($record->totalAnticipatedRebate),toInt($record->installedCost),MySQLDateUpdate($record->installedDate),
				$record->supportingFile,$record->customerInitials,$record->staffInitials,
				MySQLDateUpdate($record->customerAcknowledgementDate),
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
			$updateResponse->sql = $sqlString;
			$updateResponse->binding = $sqlBinding;
			$updateResponse->params = print_r($sqlParams,true);
			
            if ($updateResponse->error){
                trigger_error("MuniProvider.updateRebateCommercialHeat failed to update: ".$updateResponse->error, E_USER_ERROR);
            }else{
				//since update was successful now log the changes
				if (count($changedFields)){
					foreach ($changedFields as $fieldName=>$fieldStatus){
						$changedResults[] = $adminUserId;
						$changedResults[] = $this->trackChanges("muni_com_rebate_heathotwater",$record->id,$fieldName,$fieldStatus["From"],$fieldStatus["To"],$adminUserId);
					}
				}
			}
        }
        return $updateResponse;
    }
	
    private static $SELECT_REBATE_STATUS_COLUMNS = "
        SELECT
			MuniRebateStatus_ID as id,
			MuniRebateStatus_Name as name,
			MuniRebateStatus_BackgroundColor as backgroundColor,
			MuniRebateStatus_BorderColor as borderColor,
			MuniRebateStatus_TextColor as textColor";

    private static $FROM_REBATE_STATUS_TABLE_CLAUSE = "
        FROM muni_rebate_status
        WHERE 1 = 1";

    public function getRebateStatusNames($criteria){
        $sqlString = self::$SELECT_REBATE_STATUS_COLUMNS;
        $fromTableClause = self::$FROM_REBATE_STATUS_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
		
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getRebateStatusNames failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
    private static $SELECT_REBATE_INFO_COLUMNS = "
        SELECT *";

    private static $FROM_REBATE_INFO_TABLE_CLAUSE = "
        FROM muni_res_rebate_REBATENAME WHERE 1 = 1";
		
    private static $FROM_COM_REBATE_INFO_TABLE_CLAUSE = "
        FROM muni_com_rebate_heathotwater WHERE 1 = 1";
	private static $FILTER_REBATE_INFO_CUSTOMERID = "
		AND MuniResRebateREBATENAME_CustomerID = ?";
	private static $FILTER_COM_REBATE_INFO_CUSTOMERID = "
		AND MuniComRebateHeatHotwater_CustomerID = ?";
	private static $FILTER_REBATE_INFO_REBATEID = "
		AND MuniResRebateREBATENAME_ID = ?";
	private static $FILTER_COM_REBATE_INFO_REBATEID = "
		AND MuniComRebateHeatHotwater_ID = ?";
	private static $FILTER_ACTIVEREBATES_ON_CURRENTSTATUS = "
		AND CurrentStatus > 0";
	private static $FILTER_ANYREBATES_ON_CURRENTSTATUS = "
		AND CurrentStatus = ?";

		
    public function getRebateInfo($criteria){
        $sqlString = self::$SELECT_REBATE_INFO_COLUMNS;
        $fromTableClause = self::$FROM_REBATE_INFO_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
		
		$rebateName=$criteria->rebateName;
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName=="CommercialHeat"){
			$fromTableClause = self::$FROM_COM_REBATE_INFO_TABLE_CLAUSE;
			$orderByDate = " ORDER BY MuniComRebateHeatHotwater_ReceivedTimeStamp DESC";
			$sqlString .= ", MuniComRebateHeatHotwater_ID as id, MuniComRebateHeatHotwater_ReceivedTimeStamp as receivedTimeStamp, MuniComRebateHeatHotwater_AddedByAdmin as addedBy, MuniComRebateHeatHotwater_SupportingFile as supportingFile";
		}else{
			$fromTableClause = str_replace("REBATENAME",strtolower($rebateName),$fromTableClause);
			$orderByDate = " ORDER BY MuniResRebate".$rebateName."_ReceivedTimeStamp DESC";
			$sqlString .= ", MuniResRebate".$rebateName."_ID as id, MuniResRebate".$rebateName."_ReceivedTimeStamp as receivedTimeStamp, MuniResRebate".$rebateName."_AddedByAdmin as addedBy, MuniResRebate".$rebateName."_SupportingFile as supportingFile";
		}
		
		if ($criteria->customerId){
			$thisFilterString = self::$FILTER_REBATE_INFO_CUSTOMERID;
			if ($rebateName=="CommercialHeat"){
				$thisFilterString = self::$FILTER_COM_REBATE_INFO_CUSTOMERID;
			}else{
				$thisFilterString = str_replace("REBATENAME",$rebateName,$thisFilterString);
			}
            $filterString .= $thisFilterString;
            $filterBinding .= "i";
            $filterParams[] = $criteria->customerId;
		}
		if ($criteria->rebateId){
			$thisFilterString = self::$FILTER_REBATE_INFO_REBATEID;
			if ($rebateName=="CommercialHeat"){
				$thisFilterString = self::$FILTER_COM_REBATE_INFO_REBATEID;
			}else{
				$thisFilterString = str_replace("REBATENAME",$rebateName,$thisFilterString);
			}
            $filterString .= $thisFilterString;
            $filterBinding .= "i";
            $filterParams[] = $criteria->rebateId;
		}
		if (!$criteria->currentStatus){
			$filterString .= self::$FILTER_ACTIVEREBATES_ON_CURRENTSTATUS;
		}else{
			$filterString .= self::$FILTER_ANYREBATES_ON_CURRENTSTATUS;
			$filterBinding .="i";
			$filterParams[] = $criteria->currentStatus;
		}
		
		if ($criteria->orderByDateReceived){
			$orderBy = $orderByDate;
		}

		
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getRebateInfo failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	private static $UPDATE_REBATE_NOTES_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET Notes=?, NotesUpdatedBy=?, NotesUpdatedDate=? WHERE MuniComRebateHeatHotwater_ID=?";
	private static $UPDATE_REBATE_NOTES_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET Notes=?, NotesUpdatedBy=?, NotesUpdatedDate=? WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	public function updateRebateNotes($record,$adminId){
		$recordId = $record->rebateId;
		$rebateName = str_replace("rebate","",$record->rebateName);
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$UPDATE_REBATE_NOTES_COM_SQL;
		}else{
			$sqlString = self::$UPDATE_REBATE_NOTES_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "sss";
		$todaysDate = date("Y-m-d H:i:s");
		$sqlParams = array($record->notes,$adminId,$todaysDate);
		
		$sqlBinding .= "i";
        $sqlParams[] = $recordId;
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateNotes failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
	private static $UPDATE_REBATE_STATUS_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET CurrentStatus=?, CurrentStatusSetBy=? ADDITIONALSTATUS WHERE MuniComRebateHeatHotwater_ID=?";
	private static $UPDATE_REBATE_STATUS_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET CurrentStatus=?, CurrentStatusSetBy=? ADDITIONALSTATUS WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	public function updateRebateStatus($record,$adminId){
		$recordId = $record->rebateId;
		$customerId = $record->customerId;
		$currentStatus = $record->currentStatus;
		$currentText = $record->currentText;
		$rebateName = str_replace("rebate","",$record->rebateName);
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$UPDATE_REBATE_STATUS_COM_SQL;
		}else{
			$sqlString = self::$UPDATE_REBATE_STATUS_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "is";
		$todaysDate = date("Y-m-d H:i:s")."|".$adminId;
		$sqlParams = array($currentStatus,$todaysDate);
		
		if ($currentText == "Approved"){
			$sqlString = str_replace("ADDITIONALSTATUS",", ApprovedDate=?, ApprovedDateBy=?",$sqlString);
			$sqlBinding .= "ss";
			$sqlParams[] = date("Y-m-d");
			$sqlParams[] = $adminId;
		}elseif ($currentText == "Denied"){
			$sqlString = str_replace("ADDITIONALSTATUS",", DeniedDate=?, DeniedDateBy=?",$sqlString);
			$sqlBinding .= "ss";
			$sqlParams[] = date("Y-m-d");
			$sqlParams[] = $adminId;
		}elseif ($currentText == "DispersedToFiscal"){
			$sqlString = str_replace("ADDITIONALSTATUS",", DispersedToFiscalDate=?, DispersedToFiscalDateBy=?",$sqlString);
			$sqlBinding .= "ss";
			$sqlParams[] = date("Y-m-d");
			$sqlParams[] = $adminId;
		}else{
			$sqlString = str_replace("ADDITIONALSTATUS","",$sqlString);
		}
		
		$sqlBinding .= "i";
        $sqlParams[] = $recordId;
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}else{
			if ($currentText != "Deleted"){
				//update customer status
				$updateResponse2 = new ExecuteResponseRecord();
				$sqlString = "UPDATE muni_res_customers SET MuniCustomer_Rebate".$rebateName."=? WHERE MuniCustomer_ID=?";
				$sqlBinding = "ii";
				$sqlParams = array($record->currentStatus,$customerId);
				MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse2);
			}
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
	private static $UPDATE_REBATE_VERIFIED_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET VerifiedBy=? WHERE MuniComRebateHeatHotwater_ID=?";
	private static $UPDATE_REBATE_VERIFIED_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET VerifiedBy=? WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	public function updateRebateVerifiedDate($record,$adminId){
		$recordId = $record->rebateId;
		$customerId = $record->customerId;
		$verifiedDate = $record->verifiedDate;
		$rebateName = str_replace("rebate","",$record->rebateName);
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$UPDATE_REBATE_VERIFIED_COM_SQL;
		}else{
			$sqlString = self::$UPDATE_REBATE_VERIFIED_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "si";
		$todaysDate = MySQLDateUpdate($verifiedDate)."|".$adminId;
		$sqlParams = array($todaysDate,$recordId);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}


	private static $UPDATE_REBATE_INVOICED_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET InvoicedBy=? WHERE MuniComRebateHeatHotwater_ID IN ";
	private static $UPDATE_REBATE_INVOICED_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET InvoicedBy=? WHERE MuniResRebateREBATENAMEGOESHERE_ID IN ";
	public function updateRebateInvoicedDate($rebateName,$rebateList,$adminId){
		$rebateName = $rebateName;
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$UPDATE_REBATE_INVOICED_COM_SQL;
		}else{
			$sqlString = self::$UPDATE_REBATE_INVOICED_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "s";
		$sqlString .= "(".$rebateList.")";
		$todaysDate = date("Y-m-d")."|".$adminId;
		$sqlParams = array($todaysDate);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateInvoicedDate failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//echo $sqlString.$sqlBinding.print_r($sqlParams,true);
	}


	
	private static $UPDATE_REBATE_APPROVEDAMOUNT_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET RebateAmountApproved=?, RebateAmountApprovedBy=? WHERE MuniComRebateHeatHotwater_ID=?";
	private static $UPDATE_REBATE_APPROVEDAMOUNT_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET RebateAmountApproved=?, RebateAmountApprovedBy=? WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	public function updateRebateApprovedAmount($record,$adminId){
		$recordId = $record->rebateId;
		$rebateName = str_replace("rebate","",$record->rebateName);
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$UPDATE_REBATE_APPROVEDAMOUNT_COM_SQL;
		}else{
			$sqlString = self::$UPDATE_REBATE_APPROVEDAMOUNT_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "ss";
		$todaysDate = date("Y-m-d H:i:s")."|".$adminId;
		$sqlParams = array($record->approvedAmount,$todaysDate);
		
		$sqlBinding .= "i";
        $sqlParams[] = $recordId;
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateApprovedAmount failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
//Appliance_567447ac43037b1c1384f2f9_applebee.pdf

	private static $SELECT_REBATE_DOCUMENT_COM_SQL = "SELECT MuniComRebateHeatHotwater_SupportingFile as SupportingFile FROM muni_com_rebate_heathotwater WHERE MuniComRebateHeatHotwater_ID=?";
	private static $SELECT_REBATE_DOCUMENT_RES_SQL = "SELECT MuniResRebateREBATENAMEGOESHERE_SupportingFile as SupportingFile FROM muni_res_rebate_LCASEREBATENAMEGOESHERE WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	private static $DELETE_REBATE_DOCUMENT_COM_SQL = "UPDATE muni_com_rebate_heathotwater SET MuniComRebateHeatHotwater_SupportingFile=? WHERE MuniComRebateHeatHotwater_ID=?";
	private static $DELETE_REBATE_DOCUMENT_RES_SQL = "UPDATE muni_res_rebate_LCASEREBATENAMEGOESHERE SET MuniResRebateREBATENAMEGOESHERE_SupportingFile=? WHERE MuniResRebateREBATENAMEGOESHERE_ID=?";
	public function updateDocument($record,$adminId){
		$recordId = $record->rebateId;
		$rebateName = str_replace("rebate","",$record->rebateName);
		$documentName = $record->documentName;
        $updateResponse = new ExecuteResponseRecord();
		$rebateName = str_replace(" ","",$rebateName);
		
		//Get Current DocumentList
		if ($rebateName == "CommercialHeat"){
			$pageSqlString = self::$SELECT_REBATE_DOCUMENT_COM_SQL;
		}else{
			$pageSqlString = self::$SELECT_REBATE_DOCUMENT_RES_SQL;
			$pageSqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$pageSqlString);
			$pageSqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$pageSqlString);
		}
		$filterBinding = "i";
		$filterParams = array($recordId);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		foreach ($sqlResult as $row){
			$supportingFileRow = (object)$row;
		}		
		$supportingFile = $supportingFileRow->SupportingFile;
		$supportingFiles = explode(",",$supportingFile);
		if ($record->deleteDocument){
			if(($key = array_search($documentName, $supportingFiles)) !== false) {
				unset($supportingFiles[$key]);
			}
		}
		if ($record->addDocument){
			$supportingFiles[] = $documentName;
		}
		
		$newSupportingFiles = implode(",",$supportingFiles);
//		return $newSupportingFiles;
	
		//update new document list
		if ($rebateName == "CommercialHeat"){
			$sqlString = self::$DELETE_REBATE_DOCUMENT_COM_SQL;
		}else{
			$sqlString = self::$DELETE_REBATE_DOCUMENT_RES_SQL;
			$sqlString = str_replace("LCASEREBATENAMEGOESHERE",strtolower($rebateName),$sqlString);
			$sqlString = str_replace("REBATENAMEGOESHERE",$rebateName,$sqlString);
		}
		$sqlBinding = "si";
		$sqlParams = array($newSupportingFiles,$recordId);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateRebateNotes failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $newSupportingFiles;
		//return $sqlString.print_r($sqlParams,true);
	}

    private static $SELECT_ZIPCODES_COLUMNS = "
        SELECT * FROM muni_zipcodes WHERE 1=1";

    public function getZipCodes($criteria){
        $sqlString = self::$SELECT_ZIPCODES_COLUMNS;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
		
		// get the page
		$pageSqlString = $sqlString.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getZipCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	public function updateOIDStatus($formName,$oid){
        $updateResponse = new ExecuteResponseRecord();
		$rebateTable = ($formName == "CommercialHeat" ? "muni_com_rebate_heathotwater" : "muni_res_rebate_".strtolower($formName));
		$sqlString = "UPDATE ".$rebateTable." SET oid_status='confirmed' WHERE oid=?";
		$sqlBinding = "s";
        $sqlParams = array($oid);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateOIDStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		
	}
	public function updateFileStatus($formName,$destination,$oid){
        $updateResponse = new ExecuteResponseRecord();
		$rebateTable = ($formName == "CommercialHeat" ? "muni_com_rebate_heathotwater" : "muni_res_rebate_".strtolower($formName));
		$rebateField = ($formName == "CommercialHeat" ? "MuniComRebateHeatHotwater_SupportingFile" : "MuniResRebate".$formName."_SupportingFile");
		$sqlString = "UPDATE ".$rebateTable." SET ".$rebateField."=? WHERE oid=?";
		$sqlBinding = "ss";
        $sqlParams = array($destination,$oid);
		//echo $sqlString.print_r($sqlParams,true);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateFileStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		
	}

	private static $INSERT_SQL_PHONELOG = "
		INSERT INTO muni_phonelog (
			MuniPhoneLog_ReasonForCall, 
			MuniPhoneLog_HowTheyHeard, 
			MuniPhoneLog_ActionTaken, 
			MuniPhoneLog_Notes, 
			MuniPhoneLog_FirstName, 
			
			MuniPhoneLog_LastName, 
			MuniPhoneLog_MunicipalName, 
			MuniPhoneLog_AccountNumber,
			MuniPhoneLog_PhoneNumber,			
			MuniPhoneLog_AddedByAdmin
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?
		)";	
    public function addPhoneLog($record,$adminUserId){
        $sqlString = self::$INSERT_SQL_PHONELOG;
        $sqlBinding = "ssssssssss";
        $sqlParam = array(
			$record->reasonForCall,
			$record->howTheyHeard,
			$record->actionTaken,
			$record->notes,
			$record->firstName,
			
			$record->lastName,
			$record->municipalName,
			$record->accountNumber,
			$record->phoneNumber,
			$adminUserId
			);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if (!$insertResponse->success){
            trigger_error("MuniProvider.addPhoneLog failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }else{
			//save session information for last call
			$_SESSION['PhoneLog'] = $record;
		}
        return $insertResponse;
    }

    private static $SELECT_PHONELOG_COLUMNS = "
        SELECT
			MuniPhoneLog_ReasonForCall as reasonForCall, 
			MuniPhoneLog_HowTheyHeard as howTheyHeard, 
			MuniPhoneLog_ActionTaken as actionTaken, 
			MuniPhoneLog_Notes as notes, 
			MuniPhoneLog_FirstName as firstName, 
			
			MuniPhoneLog_LastName as lastName, 
			MuniPhoneLog_MunicipalName as municipalName, 
			MuniPhoneLog_AccountNumber as accountNumber, 
			MuniPhoneLog_PhoneNumber as phoneNumber,
			MuniPhoneLog_AddedByAdmin as addedByAdmin,
			MuniPhoneLog_TimeStamp as timeStamp
		";

    private static $FROM_PHONELOG_TABLE_CLAUSE = "
        FROM muni_phonelog
        WHERE 1 = 1 ORDER BY MuniPhoneLog_TimeStamp DESC";

    public function getPhoneLog($criteria){
        $sqlString = self::$SELECT_PHONELOG_COLUMNS;
        $fromTableClause = self::$FROM_PHONELOG_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
		
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getPhoneLog failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }	
	
	private static $INSERT_SQL_AUDIT ="
		INSERT INTO muni_audit 
			(
			MuniAudit_CustomerID, MuniAudit_RawFormData,MuniAudit_AddedByAdmin, SupportingFile, CurrentStatusSetBy
			) 
			VALUES (
			?,?,?,?,?
			)";
			
    public function addAudit($record, $adminUserId){
		$todaysDate = date("Y-m-d H:i:s")."|".$adminUserId;
        $sqlString = self::$INSERT_SQL_AUDIT;
        $sqlBinding = "sssss";
        $sqlParam = array(
			$record->auditClientID, json_encode($record),$adminUserId,$record->supportingFile,$todaysDate
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $insertResponse->record = $record;
			//update customer status
			$updateResponse2 = new ExecuteResponseRecord();
			$sqlString = "UPDATE muni_res_customers SET MuniCustomer_AuditStatus=2 WHERE MuniCustomer_ID=?";
			$sqlBinding = "i";
			$sqlParams = array($record->auditClientID);
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse2);
			
			
        } else {
            trigger_error("MuniProvider.addAudit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
        //return $record;
    }
	
    private static $SELECT_AUDIT_COLUMNS = "SELECT * FROM muni_audit WHERE 1 = 1";
	public function getAuditInfo($criteria){
        $sqlString = self::$SELECT_AUDIT_COLUMNS;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY MuniAudit_AddedTimeStamp DESC";
		if ($criteria->customerId){
			$filterString .= " AND MuniAudit_CustomerID=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->customerId;
		}
		if ($criteria->currentStatus){
			$filterString .= " AND CurrentStatus=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->currentStatus;
		}		
		$pageSqlString = $sqlString.$filterString.$orderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				if ($criteria->customerId){
					$result = (object)$row;
				}else{
					$result[] = (object)$row;
				}
			}
		} else {
			trigger_error("MuniProvider.getAuditInfo failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;

	}
	
	private static $SELECT_AUDIT_DOCUMENT_SQL = "SELECT SupportingFile FROM muni_audit WHERE MuniAudit_ID=?";
	private static $DELETE_AUDIT_DOCUMENT_SQL = "UPDATE muni_audit SET SupportingFile=? WHERE MuniAudit_ID=?";
	public function updateAuditDocument($record,$adminId){
		$recordId = $record->auditId;
		$documentName = $record->documentName;
        $updateResponse = new ExecuteResponseRecord();
		
		//Get Current DocumentList
		$pageSqlString = self::$SELECT_AUDIT_DOCUMENT_SQL;
		$filterBinding = "i";
		$filterParams = array($recordId);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		foreach ($sqlResult as $row){
			$supportingFileRow = (object)$row;
		}		
		$supportingFile = $supportingFileRow->SupportingFile;
		$supportingFiles = explode(",",$supportingFile);
		if ($record->deleteDocument){
			if(($key = array_search($documentName, $supportingFiles)) !== false) {
				unset($supportingFiles[$key]);
			}
		}
		if ($record->addDocument){
			$supportingFiles[] = $documentName;
		}
		$newSupportingFiles = (count($supportingFiles) ? implode(",",$supportingFiles) : "");
//		return $newSupportingFiles;
		//update new document list
		$sqlString = self::$DELETE_AUDIT_DOCUMENT_SQL;
		$sqlBinding = "si";
		$sqlParams = array($newSupportingFiles,$recordId);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditDocument failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $newSupportingFiles;
	}
	
	
	private static $UPDATE_AUDIT_NOTES_SQL = "UPDATE muni_audit SET Notes=?, NotesUpdatedBy=?, NotesUpdatedDate=? WHERE MuniAudit_ID=?";
	public function updateAuditNotes($record,$adminId){
		$recordId = $record->auditId;
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_AUDIT_NOTES_SQL;
		$sqlBinding = "sss";
		$todaysDate = date("Y-m-d H:i:s");
		$sqlParams = array($record->notes,$adminId,$todaysDate);
		
		$sqlBinding .= "i";
        $sqlParams[] = $recordId;
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditNotes failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
	private static $UPDATE_AUDITOR_SQL = "UPDATE muni_res_customers SET MuniCustomer_Auditor=?, MuniCustomer_AuditScheduledDate=?, MuniCustomer_AuditScheduledTime=?, MuniCustomer_AuditScheduledBy=? WHERE MuniCustomer_ID=?";
	public function updateAuditor($record,$adminId){
		$customerId = $record->customerId;
		$auditId = $record->auditId;
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_AUDITOR_SQL;
		$sqlBinding = "ssssi";
		$auditor = $record->auditor;
		$todaysDate = date("Y-m-d H:i:s")."|".$adminId;
		$scheduledDate = MySQLDateUpdate($record->scheduledDate);
		$scheduledTime = $record->scheduledTime;
		$sqlParams = array($auditor,$scheduledDate,$scheduledTime,$todaysDate,$customerId);
		
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditor failed to update: ".$updateResponse->error, E_USER_ERROR);
		}else{
			//update Audit Record if id included
			if ($auditId){
				$updateResponse = new ExecuteResponseRecord();
				$sqlString = "UPDATE muni_audit SET Auditor=?, ScheduledDate=?, ScheduledTime=?, ScheduledBy=? WHERE MuniAudit_ID=?";
				$sqlBinding = "ssssi";
				$sqlParams = array($auditor,$scheduledDate,$scheduledTime,$todaysDate,$auditId);
				MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
			}
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
	private static $UPDATE_AUDIT_STATUS_SQL = "UPDATE muni_audit SET CurrentStatus=?, CurrentStatusSetBy=? ADDITIONALSTATUS WHERE MuniAudit_ID=?";
	public function updateAuditStatus($record,$adminId){
		$recordId = $record->auditId;
		$customerId = $record->customerId;
		$currentStatus = $record->currentStatus;
		$currentText = $record->currentText;
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_AUDIT_STATUS_SQL;
		$sqlBinding = "is";
		$todaysDate = date("Y-m-d H:i:s")."|".$adminId;
		$sqlParams = array($currentStatus,$todaysDate);
		
		if ($currentText == "Approved"){
			$sqlString = str_replace("ADDITIONALSTATUS",", ApprovedDate=?, ApprovedDateBy=?",$sqlString);
			$sqlBinding .= "ss";
			$sqlParams[] = date("Y-m-d");
			$sqlParams[] = $adminId;
		}elseif ($currentText == "Denied"){
			$sqlString = str_replace("ADDITIONALSTATUS",", DeniedDate=?, DeniedDateBy=?",$sqlString);
			$sqlBinding .= "ss";
			$sqlParams[] = date("Y-m-d");
			$sqlParams[] = $adminId;
		}else{
			$sqlString = str_replace("ADDITIONALSTATUS","",$sqlString);
		}
		
		$sqlBinding .= "i";
        $sqlParams[] = $recordId;
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}else{
			//update customer status
			$updateResponse2 = new ExecuteResponseRecord();
			$sqlString = "UPDATE muni_res_customers SET MuniCustomer_AuditStatus=? WHERE MuniCustomer_ID=?";
			$sqlBinding = "ii";
			$sqlParams = array($record->currentStatus,$customerId);
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse2);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	private static $UPDATE_AUDIT_VERIFIED_SQL = "UPDATE muni_audit SET VerifiedBy=? WHERE MuniAudit_ID=?";
	public function updateAuditVerifiedDate($record,$adminId){
		$recordId = $record->auditId;
		$verifiedDate = $record->verifiedDate;
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_AUDIT_VERIFIED_SQL;
		$sqlBinding = "si";
		$todaysDate = MySQLDateUpdate($verifiedDate)."|".$adminId;
		$sqlParams = array($todaysDate,$recordId);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditStatus failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
		//return $sqlString.$sqlBinding.print_r($sqlParams,true);
	}
	
	private static $UPDATE_AUDIT_INVOICED_SQL = "UPDATE muni_audit SET InvoicedBy=? WHERE MuniAudit_ID IN ";
	public function updateAuditInvoiced($auditList,$adminId){
		$auditList = $auditList;
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_AUDIT_INVOICED_SQL;
		$sqlString .= " (".$auditList.")";
		$sqlBinding = "s";
		$todaysDate = date("Y-m-d")."|".$adminId;
		$sqlParams = array($todaysDate);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.updateAuditInvoiced failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
	}
	
    private static $SELECT_AUDIT_STATUS_COLUMNS = "
        SELECT
			MuniAuditStatus_ID as id,
			MuniAuditStatus_Name as name,
			MuniAuditStatus_BackgroundColor as backgroundColor,
			MuniAuditStatus_BorderColor as borderColor,
			MuniAuditStatus_TextColor as textColor";

    private static $FROM_AUDIT_STATUS_TABLE_CLAUSE = "
        FROM muni_audit_status
        WHERE 1 = 1";

    public function getAuditStatusNames($criteria){
        $sqlString = self::$SELECT_AUDIT_STATUS_COLUMNS;
        $fromTableClause = self::$FROM_AUDIT_STATUS_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY MuniAuditStatus_AddedTimeStamp";
		
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getAuditStatusNames failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	public function exporttofiscal($record,$adminId){
		$exportRecords = $record->exportRecords;
		foreach ($exportRecords as $exportRecord){
			$updateRecord = new stdClass();
			$updateRecord->rebateId = $exportRecord->rebateId;
			$updateRecord->customerId = $exportRecord->customerId;
			$updateRecord->currentStatus = 7;
			$updateRecord->currentText = "DispersedToFiscal";
			$updateRecord->rebateName = $exportRecord->rebateType;
			$updateResponses[] = $this->updateRebateStatus($updateRecord,$adminId);
		}
		return $updateResponses;
	}
	
	//Boiler Plate text for Customer Report
	private static $SELECT_BOILERPLATE_COLUMNS = "
        SELECT
			MuniReportBoilerPlate_ID as id,
			MuniReportBoilerPlate_Category as category,
			MuniReportBoilerPlate_Message as message,
			MuniReportBoilerPlate_Restrictions as restrictions,
			MuniReportBoilerPlate_Rebate as rebate,
			MuniReportBoilerPlate_Note as note";

    private static $FROM_BOILERPLATE_TABLE_CLAUSE = "
        FROM muni_report_boilerplate
        WHERE 1 = 1";

    public function getBoilerPlate($criteria){
        $sqlString = self::$SELECT_BOILERPLATE_COLUMNS;
        $fromTableClause = self::$FROM_BOILERPLATE_TABLE_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "";
		
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getBoilerPlate failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }	
	
	private static $SELECT_AUDIT_SIR_SQL = "SELECT SIRData FROM muni_audit WHERE MuniAudit_ID=?";
	private static $UPDATE_AUDIT_SIR_SQL = "UPDATE muni_audit SET SIRData=? WHERE MuniAudit_ID=?";
	public function addSir($record,$adminId){
		$auditId = $record->auditId;
		$sirItem = json_encode($record->sirItem);
        $updateResponse = new ExecuteResponseRecord();
		//Get Current SIRData
		$pageSqlString = self::$SELECT_AUDIT_SIR_SQL;
		$filterBinding = "i";
		$filterParams = array($auditId);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		foreach ($sqlResult as $row){
			$sirDataRow = (object)$row;
		}		
		$sirData = trim($sirDataRow->SIRData);
		if ($sirData){
			$sirDatas = explode("|",$sirData);
			if ($record->deleteSIR){
				if(($key = array_search($sirItem, $sirDatas)) !== false) {
					unset($sirDatas[$key]);
				}
			}
		}
		if ($record->addSIR){
			$sirDatas[] = $sirItem;
		}
		
		$newSirDatas = implode("|",$sirDatas);
//		return $newSirDatas;
	
		//update new sirData
		$sqlString = self::$UPDATE_AUDIT_SIR_SQL;
		$sqlBinding = "si";
		$sqlParams = array($newSirDatas,$auditId);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.addSIR failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $newSirDatas;
		//return $sqlString.print_r($sqlParams,true);
	}
	
	private static $SELECT_REBATE_CODES_SQL = "SELECT * FROM muni_rebate_codes";
	
    public function getRebateCodes(){
        $sqlString = self::$SELECT_REBATE_CODES_SQL;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		// get the page
		$pageSqlString = $sqlString.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.getRebateCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }	
	public function checkContractorLicense($criteria){
        $filterString = ""; 
        $filterBinding = "s";
        $filterParams = array($criteria->license);
		
		$tableNames = array("muni_com_rebate_heathotwater"=>"MuniComRebateHeatHotwater_","muni_res_rebate_appliance"=>"MuniResRebateAppliance_","muni_res_rebate_coolhome"=>"MuniResRebateCoolHome_","muni_res_rebate_efficiency"=>"MuniResRebateEfficiency_","muni_res_rebate_heathotwater"=>"MuniResRebateHeatHotwater_","muni_res_rebate_poolpump"=>"MuniResRebatePoolPump_","muni_res_rebate_wifi"=>"MuniResRebateWiFi_");
		$response["criteria"] = print_r($criteria,true);
		$response["params"] = print_r($filterParams,true);
		
		foreach ($tableNames as $tableName=>$fieldPrefix){
			$sqlString = "SELECT * FROM ".$tableName." WHERE 1=1 AND TRIM(".$fieldPrefix."ContractorLicenseNumber) = ? AND ApprovedDate != '0000-00-00'";
			$pageSqlString = $sqlString.$filterString.$orderBy;
			$response["sql"][] = $pageSqlString;
			$queryResponse = new QueryResponseRecord();
			$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
			if (count($queryResponse->resultArray)){
				$count = count($queryResponse->resultArray);
				$response["count"] = $response["count"]+$count;
			} else {
				trigger_error("MuniProvider.getRebateCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
			}
				
		}
        return $response;
        //return $count;
		
		
	}
    public function checkApprovedRes($criteria){
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$tableName = "muni_res_".$criteria->rebateName;
		
		switch ($criteria->rebateName){
			case "rebate_wifi":
				$fieldPrefix = "MuniResRebateWiFi_";
				break;
			case "rebate_coolhome":
				$fieldPrefix = "MuniResRebateCoolHome_";
				break;
			case "rebate_heathotwater":
				$fieldPrefix = "MuniResRebateHeatHotwater_";
				break;
			case "rebate_appliance":
				$fieldPrefix = "MuniResRebateAppliance_";
				break;
			case "rebate_commercialheat":
				$fieldPrefix = "MuniComRebateHeatHotwater_";
				$tableName = "muni_com_rebate_heathotwater";
				break;
			default:
				$fieldPrefix = "";
				break;
		}
        $sqlString = "SELECT * FROM ".$tableName." WHERE 1=1";
		
		foreach ($criteria->fields as $fieldName=>$fieldValue){
			if(strpos($fieldName,"Manufacturer")){
				$manufacturerFieldName = $fieldName;
			}
			$filterString .= " AND ".$fieldPrefix.ucfirst($fieldName)."=?"; 
			$filterBinding .= "s";
			$filterParams[] = $fieldValue;
		}
		if ($fieldPrefix != "MuniResRebateAppliance_"){
			//add approved field
			$approvedField = str_replace("Manufacturer","Approved",$manufacturerFieldName);
			$filterString .= " AND ".$fieldPrefix.ucfirst($approvedField)."=?"; 
			$filterBinding .= "s";
			$filterParams[] = "Yes";
		}
		
		
		
		$pageSqlString = $sqlString.$filterString.$orderBy;
		$response["criteria"] = print_r($criteria,true);
		$response["sql"] = $pageSqlString;
		$response["params"] = print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if (count($queryResponse->resultArray)){
			$count = count($queryResponse->resultArray);
			$response["count"] = $count;
		} else {
			if ($queryResponse->error){
				trigger_error("MuniProvider.checkApprovedRes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
			}
		}
        return $response;
        //return $count;
    }	
	
	public function revertApproval($database,$recordId,$customerId){
		$databaseName = strtolower(str_replace("rebate","muni_res_rebate_",$database));
		$databaseId = "MuniResRebate".str_replace("rebate","",$database)."_ID";
		$customerDatabase = "MuniCustomer_Rebate".str_replace("rebate","",$database);
		if ($database == "rebateCommercialHeat"){
			$databaseName = "muni_com_rebate_heathotwater";
			$databaseId = "MuniComRebateHeatHotwater_ID";
			$customerDatabase = "MuniCustomer_RebateCommercialHeat";
		}
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = "UPDATE ".$databaseName." SET RebateAmountApproved=NULL,RebateAmountApprovedBy=NULL,ApprovedDate='0000-00-00',ApprovedDateBy=NULL, CurrentStatus=1 WHERE ".$databaseId."=?";
		$sqlParams = array($recordId);
		$sqlBinding = "i";
//		return $sqlString.print_r($sqlParams,true);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.revertApproval failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		$sqlString2 = "UPDATE muni_res_customers SET ".$customerDatabase."=1 WHERE MuniCustomer_ID=?";
		$sqlParams2 = array($customerId);
		$sqlBinding2 = "i";
//		return $sqlString.print_r($sqlParams,true);
		MySqliHelper::execute($this->mysqli, $sqlString2, $sqlBinding2, $sqlParams2, $updateResponse);
		if ($updateResponse->error){
			trigger_error("MuniProvider.revertApproval failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		return $updateResponse;
	}
	
	
}