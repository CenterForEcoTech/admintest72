<?php
include_once("Helper.php");
class ReportingProvider
{
	protected $mysqli;

	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

	public function registrationReport($Filters = null){
		if ($Filters){
			$StartDate = $Filters["StartDate"];
			$EndDate = $Filters["EndDate"];
			$ReferralCode = $Filters["ReferralCode"];
			$UserType = $Filters["UserType"];
			$UserTypeConnector = " WHERE ";
		}
		if ($StartDate && $EndDate){
			$ReferralDateWhere  = " WHERE CAST(members.Members_TimeStamp AS DATE) BETWEEN '".MySQLDateUpdate($StartDate)."' AND '".MySQLDateUpdate($EndDate)."'";
			$UserTypeConnector = " AND ";
		}
		if ($ReferralCode){
			$ReferralCodeWhere = " AND CodesCampaign_Code='".$ReferralCode."'";
		}
		if ($UserType){
			$UserTypeWhere = $UserTypeConnector."members.Members_TypeID=".$UserType;
		}
        $sqlString = "
			SELECT
				members.Members_ID AS memberId,
				members.Members_FirstName AS FirstName,
				members.Members_LastName AS LastName,
				members.Members_Phone AS Phone,
				members.Members_Email AS Email,
				members.Members_TimeStamp As RegistrationDate,
			   (SELECT members_type.MembersType_Name FROM members_type WHERE MembersType_ID= Members_TypeID) AS UserType,
				merchant_info.MerchantInfo_Name as Company,
			   (SELECT CodesCampaign_Code FROM codes_campaign WHERE MerchantInfo_RegisteredReferralID = CodesCampaign_ID".$ReferralCodeWhere.") as ReferralCode
				FROM members
				JOIN merchant_info ON members.Members_ID = merchant_info.MerchantInfo_MemberID".$ReferralDateWhere."
				".$UserTypeWhere."
				UNION
					SELECT
						members.Members_ID AS memberId,
						members.Members_FirstName AS FirstName,
						members.Members_LastName AS LastName,
						members.Members_Phone AS Phone,
						members.Members_Email AS Email,
						members.Members_TimeStamp As RegistrationDate,
						(SELECT members_type.MembersType_Name FROM members_type WHERE MembersType_ID= Members_TypeID) AS UserType,
						organization_info.OrganizationInfo_Name as Company,
						(SELECT CodesCampaign_Code FROM codes_campaign WHERE OrganizationInfo_RegisteredReferralID = CodesCampaign_ID".$ReferralCodeWhere.") as ReferralCode
						FROM members
						JOIN organization_info ON members.Members_ID = organization_info.OrganizationInfo_MemberID".$ReferralDateWhere."
						".$UserTypeWhere."
					UNION
						SELECT
							members.Members_ID AS memberId,
							members.Members_FirstName AS FirstName,
							members.Members_LastName AS LastName,
							members.Members_Phone AS Phone,
							members.Members_Email AS Email,
							members.Members_TimeStamp As RegistrationDate,
						   (SELECT members_type.MembersType_Name FROM members_type WHERE MembersType_ID= Members_TypeID) AS UserType,
							'' as Company,
							'' as ReferralCode
							FROM members
							JOIN individual_info ON members.Members_ID = individual_info.IndividualInfo_MemberID".$ReferralDateWhere."
				".$UserTypeWhere."
				ORDER BY registrationDate DESC";
		//echo $sqlString;
        $sqlParams = array();
        $sqlBinding = "";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);

		return($result);
	}
	
	public function getReferralCodes(){
        $sqlString = "SELECT * FROM  codes_campaign ORDER BY CodesCampaign_Code";
	    $sqlParams = array();
        $sqlBinding = "";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return($result);
	}
	
	public function transactionReport($Filters = null,$GroupBy = null){
		if ($Filters){
			$StartDate = $Filters["StartDate"];
			$EndDate = $Filters["EndDate"];
			$MerchantID = $Filters["MerchantID"];
			$CauseSelected = $Filters["CauseSelected"];
			$SelectedOrgID = $Filters["SelectedOrgID"];
		}
		if ($GroupBy){
			$GroupByStmt = " GROUP BY Business, EventTransaction_OrgID";
			$GroupSumStmt = " SUM(EventTransaction_GeneratedAmount) as Total_Donation,";
		}
		if ($StartDate && $EndDate){
			$EventDateWhere  = " AND CAST(EventTransaction_EventDate AS DATE) BETWEEN '".MySQLDateUpdate($StartDate)."' AND '".MySQLDateUpdate($EndDate)."'";
		}
		if ($MerchantID){
			$MerchantIDWhere = " AND MerchantInfo_ID=".$MerchantID;
		}
		if ($CauseSelected || $SelectedOrgID){
			if ($SelectedOrgID) {
				$CauseSelectedOrgID = " OR EventTransaction_OrgID=".$SelectedOrgID;
			}
			$CauseSelectedWhere = " AND (EventTransaction_RecipientText='".$CauseSelected."'".$CauseSelectedOrgID.")";
		}
		$sqlString = "
			SELECT 
				EventTransaction_ID as Id, 
				EventTransaction_EligibleAmount as Sale_Amount, 
				EventTransaction_PercentageAmount as Percent_Donated,
				EventTransaction_GeneratedAmount as Donation_Amount,".$GroupSumStmt."
				EventTransaction_EventDate as Event_Date, 
				EventTransaction_TimeStamp as Transaction_Date,
				EventTransaction_RecipientText as Cause_Selected,
				EventTransaction_OrgEIN as EIN,
				MerchantInfo_Name as Business,
				MerchantInfo_ContactFirstName as Merchant_FirstName,
				MerchantInfo_ContactLastName as Merchant_LastName,
				MerchantInfo_ContactEmail as Merchant_Email,
				MerchantInfo_ContactPhone as Merchant_Phone,
				Members_FirstName as FirstName,
				Members_LastName as LastName,
				Members_Email as Email,
				Members_Phone as Phone,
				(CASE WHEN EventTransaction_OrgID > 1 THEN (SELECT OrganizationInfo_Name FROM organization_info WHERE OrganizationInfo_ID=EventTransaction_OrgID) END) AS Selected_Cause
			FROM (event_transaction 
			LEFT JOIN merchant_info ON MerchantInfo_ID=EventTransaction_MerchantID)
			LEFT JOIN members ON Members_ID=EventTransaction_CustomerID 
			WHERE EventTransaction_Status!='Deleted'".$EventDateWhere.$MerchantIDWhere.$CauseSelectedWhere.$GroupByStmt;
		//echo $sqlString;
	    $sqlParams = array();
        $sqlBinding = "";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return($result);
	}
	
	public function getBusinessNames(){
		$sqlString = "SELECT MerchantInfo_Name, MerchantInfo_ID FROM merchant_info WHERE MerchantInfo_Name !='' ORDER BY MerchantInfo_Name";
	    $sqlParams = array();
        $sqlBinding = "";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return($result);
	
	}
	public function getDistinctCauses(){
		$sqlString = "SELECT DISTINCT EventTransaction_RecipientText as Cause_Selected, EventTransaction_OrgID as OrgID FROM event_transaction GROUP BY EventTransaction_RecipientText ORDER BY EventTransaction_RecipientText";
		$sqlString = "
			SELECT 
				DISTINCT 
					EventTransaction_RecipientText as Cause_Selected, 
					EventTransaction_OrgID as OrgID, 
					(CASE 
						EventTransaction_RecipientAction 
					WHEN 
						'Customer Selected'
					THEN 
						(SELECT 
							OrganizationInfo_Name 
						FROM 
							organization_info 
						WHERE 
							OrganizationInfo_ID=OrgID
						) 
					ELSE 
						EventTransaction_RecipientText 
					END) as Selected_Cause
			FROM 
				event_transaction
			GROUP BY 
				Selected_Cause
			ORDER BY 
				Selected_Cause";
		//$sqlString = "SELECT DISTINCT EventTransaction_RecipientText as Cause_Selected FROM event_transaction ORDER BY EventTransaction_RecipientText";
	    $sqlParams = array();
        $sqlBinding = "";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return($result);
	
	}
	
}

class RegistrationQueryCriteria extends PaginationCriteria {
    public $date = null;
    public $memberType = null;
    public $referralCode = null;
}

class MemberRegistrationRecord {
    public $id;
    public $timestamp;
    public $firstName;
    public $lastName;
    public $phone;
    public $email;
    public $memberTypeId;
    public $memberType;
    public $merchantId;
    public $organizationId;
    public $companyName;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $orgEIN;
    public $referralId;
    public $referralCode;
    public $referredByMemberId;
    public $referralCodeText;
    public $pid;

}

class EventTransactionReportProvider{
    /* @var $mysqli mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            EventTransaction_ID as ID,
            MerchantInfo_Name as \"Business Name\",
            EventTransaction_EventID as \"Event ID\",
            Event_Title as \"Event Title\",
            Event_Date as \"Event Start Date\",
            EventTransaction_TimeStamp as timestamp,
            EventTransaction_EligibleAmount as \"Sale Amount\",
            EventTransaction_PercentageAmount as \"Percentage of Donation\",
            EventTransaction_FlatDonationUnits as \"Flat Donation Units\",
            EventTransaction_GeneratedAmount as \"Donation Amount\",
            EventTransaction_Customer as \"Customer\",
            EventTransaction_CustomerID as \"Customer ID\",
            OrganizationInfo_Name as \"Charity Name\",
            EventTransaction_Status as \"Status\",
            EventTransaction_UpdatedDate as \"Updated Timestamp\",
            EventTransaction_InvoiceID as \"Invoice ID\",
            EventTransaction_InvoiceID = 0 as canDelete,
            MerchantInfo_MemberID as memberId,
            EventTransaction_EventID as eventId";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_transaction
        JOIN merchant_info ON EventTransaction_MerchantID = MerchantInfo_ID
        LEFT JOIN event_info ON EventTransaction_EventID = Event_ID
        LEFT JOIN organization_info ON EventTransaction_OrgID = OrganizationInfo_ID
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND EventTransaction_ID = ?";

    /**
     * @param PaginationCriteria $criteria
     * @return PagedResult
     */
    public function get(PaginationCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if (is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];

            // get the page
            $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
            $queryResponse = new QueryResponseRecord();
            $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
            foreach ($sqlResult as $row){
                $rowObject = (object)$row;
                $result->collection[] = $this->mapToRecord($rowObject);
            }
        }

        return $result;
    }

    private function mapToRecord($rowObject){
        $record = new stdClass();
        foreach($rowObject as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY EventTransaction_ID";

    private static $ORDER_BY_ID = " ORDER BY EventTransaction_ID";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}

class MemberRegistrationProvider {
    /* @var $mysqli mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            m.Members_ID as id,
            m.Members_FirstName as firstName,
            m.Members_LastName as lastName,
            m.Members_Phone as phone,
            m.Members_Email as email,
            m.Members_TimeStamp as timestamp,
            m.Members_TypeID as memberTypeId,
	        t.MembersType_Name as memberType,
            a.merchantId,
            a.organizationId,
            a.companyName,
            a.address,
            a.city,
            a.state,
            a.zip,
            a.orgEIN,
            a.referralId,
            r.CodesCampaign_Code as referralCode,
            a.referredByMemberId,
            a.referralCodeText,
            a.pid";

    private static $FROM_TABLE_CLAUSE = "
        FROM (
            SELECT
                a.Members_ID as id,
                if (m.MerchantInfo_ID, m.MerchantInfo_ID, 0) as merchantId,
                if (o.OrganizationInfo_ID, o.OrganizationInfo_ID, 0) as organizationId,
                if (m.MerchantInfo_ID, m.MerchantInfo_Name, if(o.OrganizationInfo_ID, o.OrganizationInfo_Name, if(a.Members_Email, a.Members_Email, a.Members_Phone))) as companyName,
                if (m.MerchantInfo_ID, m.MerchantInfo_Address, if(o.OrganizationInfo_ID, o.OrganizationInfo_Address, '')) as address,
                if (m.MerchantInfo_ID, m.MerchantInfo_City, if(o.OrganizationInfo_ID, o.OrganizationInfo_City, '')) as city,
                if (m.MerchantInfo_ID, m.MerchantInfo_State, if(o.OrganizationInfo_ID, o.OrganizationInfo_State, '')) as state,
                if (m.MerchantInfo_ID, m.MerchantInfo_Zip, if(o.OrganizationInfo_ID, o.OrganizationInfo_Zip, '')) as zip,
                if(o.OrganizationInfo_ID, o.OrganizationInfo_EIN, '') as orgEIN,
                if (m.MerchantInfo_ID, m.MerchantInfo_RegisteredReferralID, if(o.OrganizationInfo_ID, o.OrganizationInfo_RegisteredReferralID, 0)) as referralId,
                if (m.MerchantInfo_ID, m.MerchantInfo_ReferredByMemberID, 0) as referredByMemberId,
                if (m.MerchantInfo_ID, m.MerchantInfo_ReferralCodeText, null) as referralCodeText,
                if (m.MerchantInfo_ID,
                    (
                        SELECT pid
                        FROM permalinks
                        WHERE entity_name = 'merchant'
                        AND entity_id = m.MerchantInfo_ID
                        AND status = 'ACTIVE'
                        ORDER BY weight DESC
                        LIMIT 1
                    ),
                    if (o.OrganizationInfo_ID,
                    (
                        SELECT pid
                        FROM permalinks
                        WHERE entity_name = 'organization'
                        AND entity_id = o.OrganizationInfo_ID
                        AND status = 'ACTIVE'
                        ORDER BY weight DESC
                        LIMIT 1
                    ),
                    ''
                    )
                ) as pid
            FROM members a
            LEFT JOIN organization_info o ON o.OrganizationInfo_MemberID = a.Members_ID
            LEFT JOIN merchant_info m ON m.MerchantInfo_MemberID = a.Members_ID
        ) as a
        JOIN members m ON m.Members_ID = a.id
        JOIN members_type t ON t.MembersType_ID = m.Members_TypeID
        LEFT JOIN codes_campaign r ON r.CodesCampaign_ID = a.referralId
        WHERE 1 = 1";

    private static $FILTER_ON_DATE = "
        AND m.Members_TimeStamp > ?";

    private static $FILTER_ON_TYPE = "
        AND t.MembersType_Name LIKE ?";

    private static $FILTER_ON_REFERRAL_CODE = "
        AND (
            r.CodesCampaign_Code LIKE ?
        )";

    private static $FILTER_ON_KEYWORD = "
        AND (
            a.companyName LIKE ?
            OR a.address LIKE ?
            OR a.city LIKE ?
            OR a.state LIKE ?
            OR a.zip LIKE ?
            OR m.Members_Phone LIKE ?
            OR m.Members_Email LIKE ?
            OR m.Members_FirstName LIKE ?
            OR m.Members_LastName LIKE ?
            OR t.MembersType_Name LIKE ?
            OR a.orgEIN LIKE ?
            OR a.referralCodeText LIKE ?
            OR a.pid LIKE ?
        )";

    /**
     * @param RegistrationQueryCriteria $criteria
     * @return PagedResult
     */
    public function get(RegistrationQueryCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->memberType){
            $orgCriteria = "%".$criteria->memberType."%";
            $filterString .= self::$FILTER_ON_TYPE;
            $filterBinding .= "s";
            $filterParams[] = $orgCriteria;
        }
        if ($criteria->referralCode){
            $businessCriteria = "%".$criteria->referralCode."%";
            $filterString .= self::$FILTER_ON_REFERRAL_CODE;
            $filterBinding .= "s";
            $filterParams[] = $businessCriteria;
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];

            // get the page
            $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
            $queryResponse = new QueryResponseRecord();
            $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
            foreach ($sqlResult as $row){
                $rowObject = (object)$row;
                $result->collection[] = $this->mapToRecord($rowObject);
            }
        }

        return $result;
    }

    private function mapToRecord($rowObject){
        $record = new MemberRegistrationRecord();
        foreach($rowObject as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY m.Members_TimeStamp";

    private static $ORDER_BY_ID = " ORDER BY m.Members_ID";

    private static $ORDER_BY_DATE = " ORDER BY m.Members_TimeStamp";

    private static $ORDER_BY_TYPE = " ORDER BY t.MembersType_Name";

    private static $ORDER_BY_CONTACT_INFO = " ORDER BY a.companyName";

    private static $ORDER_BY_FIRST_NAME = " ORDER BY m.Members_FirstName";

    private static $ORDER_BY_LAST_NAME = " ORDER BY m.Members_LastName";

    private static $ORDER_BY_REFERRAL_CODE = " ORDER BY a.referralCodeText";

    private static $ORDER_BY_ENTITY_ID = " ORDER BY GREATEST(a.merchantId, a.organizationId)";

    private function getOrderByClause(RegistrationQueryCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "memberType":
                    $orderBy = self::$ORDER_BY_TYPE." ".$sortOrder;
                    break;
                case "contactInfo":
                    $orderBy = self::$ORDER_BY_CONTACT_INFO." ".$sortOrder;
                    break;
                case "referralCode":
                    $orderBy = self::$ORDER_BY_REFERRAL_CODE." ".$sortOrder;
                    break;
                case "firstName":
                    $orderBy = self::$ORDER_BY_FIRST_NAME." ".$sortOrder;
                    break;
                case "lastName":
                    $orderBy = self::$ORDER_BY_LAST_NAME." ".$sortOrder;
                    break;
                case "entityIdDisplay":
                    $orderBy = self::$ORDER_BY_ENTITY_ID." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}

class EventPremiumQueryCriteria extends PaginationCriteria {
    public $date = null;
    public $hasPremium = true;
}

class EventPremiumRecord {
    // event
    public $eventId;
    public $draftEventId;
    public $eventName;
    public $eventStart;
    public $eventEnd;
    public $createdDate;
    public $createdByAdminId;

    // premium
    public $premiumId;
    public $templateId;
    public $premiumName;
    public $premiumPrice;

    // merchant
    public $merchantId;
    public $merchantName;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $pid;
    public $firstName;
    public $lastName;
    public $phone;
    public $email;

    // assets
    public $numAssets;
    public $pendingAssets;
}

class EventPremiumReportProvider{
    /* @var $mysqli mysqli */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    public function getSelectColumnString(){
        return self::$SELECT_COLUMNS;
    }

    public function getFromTableClause(){
        return self::$FROM_TABLE_CLAUSE;
    }

    public function applyFilter(PaginationCriteria $criteria, &$filterString, &$filterBinding, array &$filterParams){
        /* @var $criteria EventPremiumQueryCriteria */
        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }
        if ($criteria->hasPremium){
            $filterString.= self::$FILTER_ON_HAS_PREMIUM;
        }
    }

    /**
     * @param EventPremiumQueryCriteria $criteria
     * @return PagedResult
     */
    public function get(EventPremiumQueryCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = $this->getSelectColumnString();
        $fromTableClause = $this->getFromTableClause();
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }
        if ($criteria->hasPremium){
            $filterString.= self::$FILTER_ON_HAS_PREMIUM;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];

            // get the page
            $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
            $queryResponse = new QueryResponseRecord();
            $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
            foreach ($sqlResult as $row){
                $rowObject = (object)$row;
                $result->collection[] = $this->mapToRecord($rowObject);
            }
        }

        return $result;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            ev.Event_ID as eventId,
            0 as draftEventId,
            ev.Event_Title as eventName,
            ev.Event_Date as eventStart,
            ev.Event_EndDate as eventEnd,
            ev.Event_CreatedDate AS createdDate,
            ev.Event_CreatedByAdminID as createdByAdminId,

            ev.Event_EventTemplatePremiumID as premiumId,
            etp.event_template_id as templateId,
            etp.name as premiumName,
            etp.price as premiumPrice,

            m.MerchantInfo_Id as merchantId,
            m.MerchantInfo_Name as merchantName,
            m.MerchantInfo_Address as address,
            m.MerchantInfo_City as city,
            m.MerchantInfo_State as state,
            m.MerchantInfo_Zip as zip,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = m.MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            m.MerchantInfo_ContactFirstName as firstName,
            m.MerchantInfo_ContactLastName as lastName,
            m.MerchantInfo_ContactPhone as phone,
            m.MerchantInfo_ContactEmail as email,
            (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
            ) as numAssets,
            (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
                AND merchant_images.status = 'PENDING'
            ) as pendingAssets";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_info ev
        JOIN merchant_info m ON ev.Event_ProposedByMerchantID = m.MerchantInfo_ID
        LEFT JOIN event_template_premiums etp ON ev.Event_EventTemplatePremiumID = etp.id
        WHERE ev.Event_Status = 'ACTIVE'";

    private static $FILTER_ON_DATE = "
        AND ev.Event_CreatedDate > ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_ContactPhone LIKE ?
            OR m.MerchantInfo_ContactEmail LIKE ?
            OR m.MerchantInfo_ContactFirstName LIKE ?
            OR m.MerchantInfo_ContactLastName LIKE ?
            OR etp.name LIKE ?
        )";

    private static $FILTER_ON_HAS_PREMIUM = "
        AND etp.id > 0";

    protected function mapToRecord($rowObject){
        $record = new EventPremiumRecord();
        foreach($rowObject as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY ev.Event_CreatedDate DESC";

    private static $ORDER_BY_ID = " ORDER BY ev.Event_ID";
    private static $ORDER_BY_DATE = " ORDER BY ev.Event_CreatedDate";
    private static $ORDER_BY_START_DATE = " ORDER BY ev.Event_Date";
    private static $ORDER_BY_END_DATE = " ORDER BY ev.Event_EndDate";
    private static $ORDER_BY_EVENT_NAME = " ORDER BY ev.Event_Title";
    private static $ORDER_BY_MERCHANT_ID = " ORDER BY m.MerchantInfo_Id";
    private static $ORDER_BY_MERCHANT = " ORDER BY m.MerchantInfo_Name";
    private static $ORDER_BY_PREMIUM = " ORDER BY etp.name";
    private static $ORDER_BY_NUM_ASSETS = " ORDER BY (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
            )";
    private static $ORDER_BY_ADMIN_ID = " ORDER BY ev.Event_CreatedByAdminID";

    protected function getOrderByClause(EventPremiumQueryCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "eventId":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "startDate":
                    $orderBy = self::$ORDER_BY_START_DATE." ".$sortOrder;
                    break;
                case "endDate":
                    $orderBy = self::$ORDER_BY_END_DATE." ".$sortOrder;
                    break;
                case "eventName":
                    $orderBy = self::$ORDER_BY_EVENT_DATE." ".$sortOrder;
                    break;
                case "entityIdDisplay":
                    $orderBy = self::$ORDER_BY_MERCHANT_ID." ".$sortOrder;
                    break;
                case "contactInfo":
                    $orderBy = self::$ORDER_BY_MERCHANT." ".$sortOrder;
                    break;
                case "premiumInfo":
                    $orderBy = self::$ORDER_BY_PREMIUM." ".$sortOrder;
                    break;
                case "numAssets":
                    $orderBy = self::$ORDER_BY_NUM_ASSETS." ".$sortOrder;
                    break;
                case "createdByAdmin":
                    $orderBy = self::$ORDER_BY_ADMIN_ID." ".$sortOrder;
                    break;

            }
        }
        return $orderBy;
    }
}

class DraftEventPremiumReportProvider extends EventPremiumReportProvider{

    public function getSelectColumnString(){
        return self::$SELECT_COLUMNS;
    }

    public function getFromTableClause(){
        return self::$FROM_TABLE_CLAUSE;
    }

    public function applyFilter(PaginationCriteria $criteria, &$filterString, &$filterBinding, array &$filterParams){
        /* @var $criteria EventPremiumQueryCriteria */
        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }
        if ($criteria->hasPremium){
            $filterString.= self::$FILTER_ON_HAS_PREMIUM;
        }
    }

    private static $SELECT_COLUMNS = "
        SELECT
            0 as eventId,
            ev.EventDraft_ID as draftEventId,
            ev.EventDraft_Title as eventName,
            ev.EventDraft_StartDate as eventStart,
            ev.EventDraft_EndDate as eventEnd,
            ev.EventDraft_CreatedDate AS createdDate,
            0 as createdByAdminId,

            ev.EventDraft_EventTemplatePremiumID as premiumId,
            etp.event_template_id as templateId,
            etp.name as premiumName,
            etp.price as premiumPrice,

            m.MerchantInfo_Id as merchantId,
            m.MerchantInfo_Name as merchantName,
            m.MerchantInfo_Address as address,
            m.MerchantInfo_City as city,
            m.MerchantInfo_State as state,
            m.MerchantInfo_Zip as zip,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = m.MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            m.MerchantInfo_ContactFirstName as firstName,
            m.MerchantInfo_ContactLastName as lastName,
            m.MerchantInfo_ContactPhone as phone,
            m.MerchantInfo_ContactEmail as email,
            (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
            ) as numAssets,
            (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
                AND merchant_images.status = 'PENDING'
            ) as pendingAssets";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_draft ev
        JOIN merchant_info m ON ev.EventDraft_ProposedByMerchantID = m.MerchantInfo_ID
        LEFT JOIN event_template_premiums etp ON ev.EventDraft_EventTemplatePremiumID = etp.id
        WHERE ev.EventDraft_Status = 'ACTIVE'
        AND ev.EventDraft_EventID = 0";

    private static $FILTER_ON_DATE = "
        AND ev.EventDraft_CreatedDate > ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_ContactPhone LIKE ?
            OR m.MerchantInfo_ContactEmail LIKE ?
            OR m.MerchantInfo_ContactFirstName LIKE ?
            OR m.MerchantInfo_ContactLastName LIKE ?
            OR etp.name LIKE ?
        )";

    private static $FILTER_ON_HAS_PREMIUM = "
        AND etp.id > 0";

    private static $DEFAULT_ORDER_BY = "
        ORDER BY ev.EventDraft_CreatedDate DESC";

    private static $ORDER_BY_ID = " ORDER BY ev.EventDraft_ID";
    private static $ORDER_BY_DATE = " ORDER BY ev.EventDraft_CreatedDate";
    private static $ORDER_BY_MERCHANT_ID = " ORDER BY m.MerchantInfo_Id";
    private static $ORDER_BY_MERCHANT = " ORDER BY m.MerchantInfo_Name";
    private static $ORDER_BY_PREMIUM = " ORDER BY etp.name";
    private static $ORDER_BY_NUM_ASSETS = " ORDER BY (
                SELECT count(*)
                FROM merchant_images
                WHERE merchant_images.merchant_info_id = m.MerchantInfo_ID
            )";
    private static $ORDER_BY_ADMIN_ID = " ORDER BY ev.EventDraft_ID";

    protected function getOrderByClause(EventPremiumQueryCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "eventId":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "entityIdDisplay":
                    $orderBy = self::$ORDER_BY_MERCHANT_ID." ".$sortOrder;
                    break;
                case "contactInfo":
                    $orderBy = self::$ORDER_BY_MERCHANT." ".$sortOrder;
                    break;
                case "premiumInfo":
                    $orderBy = self::$ORDER_BY_PREMIUM." ".$sortOrder;
                    break;
                case "numAssets":
                    $orderBy = self::$ORDER_BY_NUM_ASSETS." ".$sortOrder;
                    break;
                case "createdByAdmin":
                    $orderBy = self::$ORDER_BY_ADMIN_ID." ".$sortOrder;
                    break;

            }
        }
        return $orderBy;
    }
}
?>
