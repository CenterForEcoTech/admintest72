<?php
include_once("Helper.php");
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once($rootFolder."/repository/ServiceAPI.php");

class GeoLookupProvider implements LocationProvider {

    private $mysqli;

    public function __construct(mysqli $mygeosqli){
        $this->mysqli = $mygeosqli;
    }

    private static $GET_STATE_ABBR_SQL = "
        SELECT StateAbr
        FROM us
        WHERE StateFull = ?
        AND StateAbr != ''
        LIMIT 1";

    public function getStateAbbr($stateName)
    {
        $result = MySqliHelper::get_result($this->mysqli, self::$GET_STATE_ABBR_SQL, "s", array($stateName));
        if (count($result)){
            return $result[0]["StateAbr"];
        }
        return null;
    }

    private static $TIMEZONE_BY_ZIP = "
        SELECT timezoneId
        FROM us
        WHERE Zip = ?
        AND timezoneId != ''
        LIMIT 1";

    /**
     * For a specific venue that has a zipcode, returns the DateTimeZone object. To get the id value, do timezone->getName();
     * This method implements the method on the LocationProvider interface, which is expected to return a DateTimeZone object
     * @param Venue $venue
     * @return DateTimeZone
     */
    public function getTimeZone(Venue $venue)
    {
        $result = MySqliHelper::get_result($this->mysqli, self::$TIMEZONE_BY_ZIP, "s", array($venue->postalCode));
        if (count($result)){
            $timezoneId = $result[0]["timezoneId"];
			return new DateTimeZone($timezoneId);
        }
        // lacking any TZ, we always return EST
		return new DateTimeZone("America/New_York");
    }

    private static $FIPS_CODE_BY_ZIP = "
      SELECT DistrictCode as value
      FROM `us`
      WHERE Zip = ?
      LIMIT 1";

    /**
     * Given a Venue data object (see DataContracts), returns the relevant FIPS code (based on zip code)
     * @param Venue $venue
     * @return string
     */
    public function getFIPSCode(Venue $venue)
    {
        $result = MySqliHelper::get_result($this->mysqli, self::$FIPS_CODE_BY_ZIP, "s", array($venue->postalCode));
        if (count($result)){
            return $result[0]["value"];
        }
        return "";
    }

    private static $CITY_LOOKUP_BY_ZIP = "
        SELECT
            City,
            StateFull,
            StateAbr,
            Country,
            Zip,
            Lat,
            `Long`
        FROM us
        WHERE Zip LIKE ?";

    private static $CITY_LOOKUP_BY_CITY = "
        SELECT
            City,
            StateFull,
            StateAbr,
            Country,
            Zip,
            Lat,
            `Long`
        FROM us
        WHERE City LIKE ?";

    private static $CITY_LOOKUP_FILTER_STATE_ABBR = "
        AND StateAbr = ?";

    private static $CITY_LOOKUP_FILTER_STATE_NAME = "
        AND StateFull LIKE ?";

    private static $CITY_LOOKUP_GROUP_BY = "
        GROUP BY City, StateAbr";

    /**
     * Given some free form text submitted by the user, search for a city.
     * @param $freeFormText string
     * @param $limit int
     * @return array
     */
    public function lookupCity($freeFormText, $limit = 12){
        $criteria = GeoLookupCriteria::parse($freeFormText);
        $collection = array();
        if (!is_numeric($limit)){
            $limit = 12;
        }
        if ($criteria->zip){
            $sqlString = self::$CITY_LOOKUP_BY_ZIP." LIMIT ".$limit;
            $sqlBinding = "s";
            $sqlParam = array($criteria->zip."%");
            $queryResponse = new QueryResponseRecord();
            MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $queryResponse);
            if ($queryResponse->success){
                foreach ($queryResponse->resultArray as $row){
                    $collection[] = $this->mapCities((object)$row);
                }
            } else {
                trigger_error("GeoLookupProvider.lookupCity error searching by ZIP: ".$queryResponse->error, E_USER_ERROR);
            }
        } else {
            $sqlString = self::$CITY_LOOKUP_BY_CITY;
            $sqlBinding = "s";
            $sqlParam = array("%".$criteria->city."%");

            if ($criteria->stateAbbr){
                $sqlString .= self::$CITY_LOOKUP_FILTER_STATE_ABBR;
                $sqlBinding .= "s";
                $sqlParam[] = strtoupper($criteria->stateAbbr);
            }

            if ($criteria->stateName){
                $sqlString .= self::$CITY_LOOKUP_FILTER_STATE_NAME;
                $sqlBinding .= "s";
                $sqlParam[] = "%".$criteria->stateName."%";
            }

            $sqlString .= self::$CITY_LOOKUP_GROUP_BY." LIMIT ".$limit;
            $queryResponse = new QueryResponseRecord();
            MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $queryResponse);
            if ($queryResponse->success){
                foreach ($queryResponse->resultArray as $row){
                    $collection[] = $this->mapCities((object)$row);
                }
            } else {
                trigger_error("GeoLookupProvider.lookupCity error searching by City: ".$queryResponse->error, E_USER_ERROR);
            }
        }
        return $collection;
    }

    private function mapCities($row){
        return array(
            "value" => $row->City.", ".$row->StateFull." (".$row->Zip.")",
            "City" => $row->City,
            "State" => $row->StateAbr,
            "Country" => $row->Country,
            "Zip" => $row->Zip,
            "Lat" => $row->Lat,
            "Long" => $row->Long
        );
    }
}

class GeoLookupCriteria{
    public $zip;
    public $city;
    public $stateAbbr;
    public $stateName;

    /**
     * Parses a string into four value array:
     *   - complete matching portion of string
     *   - optional city
     *   - optional state
     *   - optional zipcode
     * @var string
     */
    private static $REGEX_PATTERN = "/([a-zA-Z][^,]*)*[^a-zA-Z]*([a-zA-Z][^\d\(]*)*[^0-9]*([0-9]*).*/";

    /**
     * Parses a string into two value array:
     *   - complete matching portion
     *   - optional zip
     * @var string
     */
    private static $EXTRACT_ZIP_PATTERN = "/[^0-9]*([0-9]*).*/";

    /**
     * @static
     * @param $freeFormText
     * @return GeoLookupCriteria
     */
    public static function parse($freeFormText){
        $criteria = new GeoLookupCriteria();
        if (is_numeric(trim($freeFormText))){
            $criteria->zip = trim($freeFormText);
            return $criteria;
        }
        preg_match(self::$EXTRACT_ZIP_PATTERN, $freeFormText, $zipMatches);
        if (is_numeric($zipMatches[1])){
            $criteria->zip = $zipMatches[1];
            if (strlen($criteria->zip) == 5){
                return $criteria; // all we need is zip!
            }
        }
        // we can't do htmlspecialchars because there are valid ampersands in state names
        // see unit tests for supported cases
        preg_match(self::$REGEX_PATTERN, $freeFormText, $matches);
        $criteria->city = $matches[1];
        $criteria->zip = $matches[3];
        if (strlen(trim($matches[2])) == 2){
            $criteria->stateAbbr = trim($matches[2]);
        } else {
            $criteria->stateName = trim($matches[2]);
        }
        return $criteria;
    }
}