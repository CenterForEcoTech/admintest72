<?php
/**
 * Retrieves information about customer and nonprofit "accounts"
 * which refers to donation funds that are in-hand and are either
 * in "pots of money" (undesignated funds customers can assign to charities
 * or in "nonprofit accounts" (funds disbursed or ready to disburse to
 * specific nonprofits).
 */

/**
 * API for this provider, for documentation purposes
 */
interface DonationAccountProvider {

    /**
     * Returns an array of totals for each calendar year.
     * @abstract
     * @param int $memberId
     * @return array of year => total values.
     */
    public function getDonationTotals($memberId);

    /**
     * Returns the specified member's PotOfMoney object
     * @abstract
     * @param $memberId
     * @return PotOfMoney object
     */
    public function getPotOfMoney($memberId);

    /**
     * Returns the updated/created record if successful
     * @abstract
     * @param $memberId
     * @param $allocationRecord
     * @return $planResponse
     */
    public function put($memberId, $allocationRecord);
}

class PotOfMoney{
    private $memberId;
    private $unallocatedTotal;
    protected $plans; // array of CustomerAllocationPlan

    public function __construct($memberId, $unallocatedTotal, $plans){
        $this->memberId = $memberId;
        $this->unallocatedTotal = $unallocatedTotal;
        $this->plans = $plans;
    }

    public function memberId(){
        return $this->memberId;
    }

    public function unallocatedTotal(){
        return $this->unallocatedTotal;
    }

    public function getPlans(){
        return $this->plans;
    }
}

class CustomerAllocationPlan{
    public $id;
    public $orgId;
    public $orgEin;
    public $isFiscallySponsored;
    public $orgName;
    public $amount;
    public $status;
    public $priority;
    public $spent_amount;

    public function isDeleted(){
        return $this->status == "DELETED";
    }

    public static function convert($plan){
        return self::map(new CustomerAllocationPlan(), $plan);
    }

    /**
     * Converts an anonymous object into the target
     * @static
     * @param CustomerAllocationPlan $target
     * @param object $plan (expecting an object with the same properties)
     * @return \CustomerAllocationPlan
     */
    protected static function map(CustomerAllocationPlan $target, $plan){
        $target->id = $plan->id;
        $target->orgId = $plan->orgId;
        $target->orgEin = $plan->orgEin;
        $target->isFiscallySponsored = ($plan->isFiscallySponsored) ? true : false;
        $target->orgName = $plan->orgName;
        $target->amount = $plan->amount;
        $target->status = $plan->status;
        $target->priority = $plan->priority;
        $target->spent_amount = $plan->spent_amount;
        return $target;
    }
}

class MutableCustomerAllocationPlan extends CustomerAllocationPlan{
    private $isDirty;
    public $disbursementOrgId;
    public $memberId;
    public $accountId;

    /**
     * Converts a stdClass to a MutableCustomerAllocationPlan with the same properties
     * @static
     * @param $plan (expecting an object with same properties)
     * @return CustomerAllocationPlan
     */
    public static function convert($plan){
        return self::map(new MutableCustomerAllocationPlan(), $plan);
    }

    public function isDirty(){
        return $this->isDirty;
    }

    public function update($incomingRecord){
        // validate
        if ($incomingRecord->id != $this->id && $incomingRecord->id != 0){
            return false;
        }
        // only amount and priority are allowed to be updated using this api
        if ($incomingRecord->amount != $this->amount){
            $this->amount = $incomingRecord->amount;
            $this->isDirty = true;
        }
        if ($incomingRecord->priority != $this->priority){
            $this->priority = $incomingRecord->priority;
            $this->isDirty = true;
        }
        return $this;
    }

    public function delete(){
        $this->status = "DELETED";
        $this->isDirty = true;
    }
}

class DonationAccountException extends Exception {

}
/*
 * =============
 * Begin provider implementation
 * =============
 */
include_once("Helper.php");
include_once("MemberProvider.php");

class PlanResponse{
    public $message;
    public $record;
}

class OrgInfoRecord{
    public $id;
    public $ein;
    public $isFiscallySponsored;
    public $name;

    public function getIsFiscallySponsoredText(){
        return ($this->isFiscallySponsored) ? "true" : "";
    }

    public function getIsFiscallySponsoredBit(){
        return ($this->isFiscallySponsored) ? 1 : 0;
    }
}

class DonationAccountProviderImpl implements DonationAccountProvider{

    private $causetownConn;
    private $disbursementConn;
    private $charityLookup;

    public function __construct(mysqli $mysqli, mysqli $mysqli_disb, CharityLookup $charityLookup){
        $this->causetownConn = $mysqli;
        $this->disbursementConn = $mysqli_disb;
        $this->charityLookup = $charityLookup;
    }

    private static $GET_DONATION_TOTALS_SQL = "
        SELECT
            sum(EventTransaction_GeneratedAmount) AS total,
            YEAR(EventTransaction_Timestamp) AS year
        FROM event_transaction etrans
        WHERE etrans.EventTransaction_CustomerID = ?
        AND EventTransaction_Status != 'Deleted'
        GROUP BY YEAR(EventTransaction_Timestamp)";

    /**
     * Returns an array of totals for each calendar year.
     * @param int $memberId
     * @return array of year => total values (unsorted).
     */
    public function getDonationTotals($memberId)
    {
        $totals = array();

        // get donations not in disbursement pipeline yet
        $sqlString = self::$GET_DONATION_TOTALS_SQL;
        $sqlBinding = "i";
        $sqlParams = Array($memberId);
        $sqlResult = MySqliHelper::get_result($this->causetownConn, $sqlString, $sqlBinding, $sqlParams);
        foreach($sqlResult as $totalByYear){
            $year = $totalByYear["year"];
            if (is_numeric($year)){
                $amount = $totalByYear["total"];
                if ($totals[$year]){
                    $totals[$year] = $totals[$year] + $amount;
                } else {
                    $totals[$year] = $amount;
                }
            }
        }

        // return unsorted
        return $totals;
    }

    private static $UNALLOCATED_AMOUNT_SQL = "
        SELECT
        sum(trans.generated_amount) AS generated_amount,
        (
            SELECT sum(alloc.amount)
            FROM customer_allocations alloc
            WHERE alloc.event_transaction_id = trans.event_transaction_id
        ) AS allocated_amount
        FROM customer_account acct
        JOIN customer_account_transactions trans ON acct.id = trans.customer_account_id
        WHERE trans.status IN ('NEW', 'ALLOCATING')
        AND acct.member_id = ?";

    private static $ALLOCATION_PLANS_SQL = "
        SELECT
            plan.id,
            org.organization_info_id AS orgId,
            org.ein AS orgEIN,
            org.is_fiscally_sponsored AS isFiscallySponsored,
            org.name AS orgName,
            plan.min_amount AS amount,
            plan.status,
            plan.priority,
            (
                SELECT sum(alloc.amount)
                FROM customer_allocations alloc
                WHERE alloc.customer_allocation_plan_id = plan.id
            ) AS spent_amount
        FROM customer_allocation_plans plan
        JOIN disbursement_orgs org ON plan.disbursement_org_id = org.id
        JOIN customer_account acct ON plan.customer_account_id = acct.id
        WHERE plan.status IN ('NEW', 'APPLYING')
        AND acct.member_id = ?";

    /**
     * Returns the specified member's PotOfMoney object
     * @param $memberId
     * @return PotOfMoney object
     */
    public function getPotOfMoney($memberId)
    {
        // defaults
        $unallocatedAmount = 0;
        $plans = array();

        // retrieve unallocated sum total
        $sqlString = self::$UNALLOCATED_AMOUNT_SQL;
        $sqlBinding = "i";
        $sqlParams = Array($memberId);
        $sqlResult = MySqliHelper::get_result($this->disbursementConn, $sqlString, $sqlBinding, $sqlParams);
        if (count($sqlResult)){
            $generatedAmount = $sqlResult[0]['generated_amount'];
            $allocatedAmount = $sqlResult[0]['allocated_amount'];
            $unallocatedAmount = $generatedAmount - $allocatedAmount;
        }

        // retrieve existing plans and spent status
        $sqlString = self::$ALLOCATION_PLANS_SQL;
        $sqlBinding = "i";
        $sqlParams = Array($memberId);
        $sqlResult = MySqliHelper::get_result($this->disbursementConn, $sqlString, $sqlBinding, $sqlParams);
        foreach ($sqlResult as $planRow){
            $plans[] = CustomerAllocationPlan::convert((object)$planRow);
        }

        // create pot of money and return
        $potOfMoney = new PotOfMoney($memberId, $unallocatedAmount, $plans);
        return $potOfMoney;
    }

    /**
     * Returns the updated/created record if successful
     * @param $memberId
     * @param $allocationRecord
     * @throws DonationAccountException if attempting to update (update is not supported)
     * @return \PlanResponse $planResponse
     */
    public function put($memberId, $allocationRecord)
    {
        /* @var $mutableRecord MutableCustomerAllocationPlan */
        $mutableRecord = MutableCustomerAllocationPlan::convert($allocationRecord);
        $mutableRecord->memberId = $memberId;
        if ($allocationRecord->id == 0){
            return $this->add($mutableRecord);
        } else {
            throw new DonationAccountException("Updating customer allocation is not supported.");
        }
    }

    private function add(MutableCustomerAllocationPlan $record){
        $response = new PlanResponse();
        // validation
        $hasOrgId = ($record->orgId) ? true : false;
        $hasOrgEIN = ($record->orgEin) ? true : false;
        if (strlen($record->orgName) == 0){
            $response->message = "A charity is required.";
        } else if (!$hasOrgId && !$hasOrgEIN){
            $response->message = "A charity is required to be registered or have an EIN.";
        } else if (!$hasOrgId && $hasOrgEIN){
            // TODO: charity lookup should not be here, it should be done prior to calling this object
            $orgMemberProvider = new OrganizationMemberProvider($this->causetownConn);
            $record->orgId = $orgMemberProvider->checkAndAdd($record->orgEin, $this->charityLookup);
        }
        if (!($response->message)){
            $insertedRecord = $this->insertPlan($record);
            $response->record = CustomerAllocationPlan::convert($insertedRecord);
        }
        return $response;
    }

    private static $ADD_ALLOCATION_PLAN_SQL = "
        INSERT INTO customer_allocation_plans
        (
          customer_account_id,
          disbursement_org_id,
          min_amount,
          status,
          priority
        )
        SELECT ?,?,?,?,
          (
            SELECT IFNULL((
                SELECT max(priority)
                FROM customer_allocation_plans
                WHERE customer_account_id = ?
            ),0) + 1
            FROM DUAL
          )";

    private static $GET_ALLOCATION_PLAN_PRIORITY_SQL = "
        SELECT
          priority
        FROM customer_allocation_plans
        WHERE id = ?";

    private function insertPlan(MutableCustomerAllocationPlan $record){
        try {
            $disbursementOrgId = $this->getDisbursementOrgId($record);
        } catch (DonationAccountException $e) {
        }
        $customerAccountId = $this->getCustomerAccountId($record);

        // add the plan
        $sqlString = self::$ADD_ALLOCATION_PLAN_SQL;
        $sqlBindings = "iidsi";
        $sqlParams = array($customerAccountId, $disbursementOrgId, $record->amount, $record->status, $customerAccountId);
        $result = MySqliHelper::execute($this->disbursementConn, $sqlString, $sqlBindings, $sqlParams);
        $record->id = $result;

        // retrieve and refresh record priority
        $sqlString = self::$GET_ALLOCATION_PLAN_PRIORITY_SQL;
        $sqlBindings = "i";
        $sqlParams = array($record->id);
        $result = MySqliHelper::get_result($this->disbursementConn, $sqlString, $sqlBindings, $sqlParams);
        $record->priority = $result[0]['priority'];

        return $record;
    }

    private static $GET_CUSTOMER_ACCOUNT_SQL = "
        SELECT id
        FROM customer_account
        WHERE member_id = ?";

    private static $ADD_CUSTOMER_ACCOUNT_SQL = "
        INSERT INTO
            customer_account (member_id)
        SELECT ?
        FROM dual
        WHERE NOT EXISTS (
          SELECT 1 FROM customer_account WHERE member_id = ?
        )";

    private function getCustomerAccountId(MutableCustomerAllocationPlan $record){
        $sqlString = self::$GET_CUSTOMER_ACCOUNT_SQL;
        $sqlBindings = "i";
        $sqlParams = array($record->memberId);
        $sqlResult = MySqliHelper::get_result($this->disbursementConn, $sqlString, $sqlBindings, $sqlParams);
        if (count($sqlResult)){
            return $sqlResult[0]["id"];
        }

        // insert it
        $sqlString = self::$ADD_CUSTOMER_ACCOUNT_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($record->memberId, $record->memberId);
        return MySqliHelper::execute($this->disbursementConn, $sqlString, $sqlBindings, $sqlParams);
    }

    private static $GET_ORG_INFO_BY_ID_SQL = "
        SELECT
            OrganizationInfo_ID as orgID,
            OrganizationInfo_Name as orgName,
            OrganizationInfo_DifferentNameThanFiscalSponsor as isFiscallySponsoredText,
            OrganizationInfo_EIN as orgEin
        FROM organization_info
        WHERE OrganizationInfo_ID = ?";

    private function getDisbursementOrgId(MutableCustomerAllocationPlan $record){
        $orgRecord = new OrgInfoRecord();
        $orgRecord->id = $record->orgId;
        $orgRecord->ein = $record->orgEin;
        $orgRecord->name = $record->orgName;
        $orgRecord->isFiscallySponsored = ($record->isFiscallySponsored) ? true : false;
        if (is_numeric($orgRecord->id) && ((int)$orgRecord->id) > 0){
            // orgId lookup (this trumps everything)
            $sqlString = self::$GET_ORG_INFO_BY_ID_SQL;
            $sqlBinding = "i";
            $sqlParams = Array($orgRecord->id);
            $sqlResult = MySqliHelper::get_result($this->causetownConn, $sqlString, $sqlBinding, $sqlParams);
            if (count($sqlResult)){
                $row = $sqlResult[0];
                $orgRecord->ein = $row["orgEin"];
                $orgRecord->name = $row["orgName"];
                $orgRecord->isFiscallySponsored = (strlen($row["isFiscallySponsoredText"])) ? true : false;
            } else {
                $orgRecord->id = 0; // reset this value since we can't find the record in the system.
            }
        } else {
            throw new DonationAccountException("Charity designations require a valid organization id.");
        }
        $disbursementOrgId = $this->getDisbursementOrg($orgRecord);
        if ($disbursementOrgId <= 0){
            try {
                $disbursementOrgId = $this->insertDisbursementOrg($orgRecord);
            } catch (DonationAccountException $e) {
            }
        }
        return $disbursementOrgId;
    }

    private static $GET_DISBURSEMENT_ORG_SQL = "
        SELECT
            id,
            organization_info_id as orgId,
            ein as orgEin,
            name as orgName
        FROM
            disbursement_orgs
        WHERE organization_info_id > 0
        AND organization_info_id = ?";

    private static $UPDATE_DISBURSEMENT_ORG_SQL = "
        UPDATE disbursement_orgs
        SET organization_info_id = ?,
          name = ?,
          ein = ?
        WHERE id = ?";

    private function getDisbursementOrg(OrgInfoRecord $orgRecord)
    {
        $disbursementOrgId = 0;
        $sqlString = self::$GET_DISBURSEMENT_ORG_SQL;
        $sqlBinding = "i";
        $sqlParams = Array($orgRecord->id);
        $sqlResult = MySqliHelper::get_result($this->disbursementConn, $sqlString, $sqlBinding, $sqlParams);
        if (count($sqlResult)) {
            // now, take the opportunity to update the disbursement org record as needed
            $row = (object)$sqlResult[0];
            $disbursementOrgId = $row->id;
            $needsUpdate = false;
            if ($row->orgEin != $orgRecord->ein) {
                if (strlen($row->ein) == 0) {
                    $needsUpdate = true;
                } else {
                    // we always take the data from our table
                    $orgRecord->ein = $row->ein;
                }
            }
            if ($row->orgName != $orgRecord->name) {
                $needsUpdate = true;
            }
            if ($needsUpdate) {
                $sqlString = self::$UPDATE_DISBURSEMENT_ORG_SQL;
                $sqlBinding = "issi";
                $sqlParams = array($orgRecord->id, $orgRecord->name, $orgRecord->ein, $disbursementOrgId);
                MySqliHelper::execute($this->disbursementConn, $sqlString, $sqlBinding, $sqlParams);
            }
        }
        return $disbursementOrgId;
    }

    /**
     * ADD org SQL is designed to add a row only if a match does not already exist.
     * sqlbindings: "iissisi"
     * sqlparams: orgID,isFiscallySponsored,ein,name,orgId,orgId
     *
     * match assumptions:
     * 1) an orgID is always expected to be specified
     *
     * This SQL will FAIL TO INSERT a row that has no match AND is missing the orgID.
     */
    private static $ADD_DISBURSEMENT_ORG_SQL = "
        INSERT INTO
          disbursement_orgs
        (
          organization_info_id,
          is_fiscally_sponsored,
          ein,
          name
        )
        SELECT
          ?,?,?,?
        FROM dual
        WHERE (
          0 != ? -- orgId check
        )
        AND NOT EXISTS (
            SELECT 1 FROM disbursement_orgs
            WHERE organization_info_id = ?
        )";

    private function insertDisbursementOrg(OrgInfoRecord $orgRecord){
        if (empty($orgRecord->id)){
            throw new DonationAccountException("Charity designation requires an Organization ID.");
        }
        $sqlString = self::$ADD_DISBURSEMENT_ORG_SQL;
        $sqlBindings = "iissii";
        $sqlParams = array(
            $orgRecord->id,
            $orgRecord->getIsFiscallySponsoredBit(),
            $orgRecord->ein,
            $orgRecord->name,
            $orgRecord->id,
            $orgRecord->id
        );
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($this->disbursementConn, $sqlString, $sqlBindings, $sqlParams, $response);
        if ($response->success){
            return $response->insertedId;
        }
        trigger_error("DonationAccountProvider.insertDisbursementOrg error: ".$response->error, E_USER_ERROR);
        return null;
    }
}
?>