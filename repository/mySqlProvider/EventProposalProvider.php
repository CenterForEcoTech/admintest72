<?php
//
// Future Refactoring: Eliminate event_info table dependency
// TODO: implement or remove commented out code (as deemed necessary)
//

class EventProposalCriteria extends EventCriteria{
    public $pendingOnly = false;
    public $versionId;
}

class EventProposalProvider {
    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $GET_SELECT_COLUMNS = "
        SELECT
            e.Event_ID,
            e.VersionID,
            e.Event_Title,
            e.Event_Description,
            e.Event_CreatedDate,
            e.Event_EditedDate,
            e.Event_Date,
            e.Event_StartTime,
            e.Event_EndDate,
            e.Event_EndTime,
            e.Event_MerchantPercentage,
            e.Event_OrgAnticipatedCount,
            e.Event_OrgWhyYou,
            e.VersionStatus,
            e.Event_MerchantSecondaryPercentage,
            e.MerchantInfo_ID,
            e.OrganizationInfo_ID,
            organization_info.OrganizationInfo_Name,
            organization_info.OrganizationInfo_Website,
            organization_info.OrganizationInfo_EIN,
            merchant_info.MerchantInfo_Name,
            merchant_info.MerchantInfo_WebSite,
            merchant_info.MerchantInfo_Description,
            merchant_info.MerchantInfo_Lat,
            merchant_info.MerchantInfo_Long,
            merchant_info.MerchantInfo_Address,
            merchant_info.MerchantInfo_City,
            merchant_info.MerchantInfo_State,
            merchant_info.MerchantInfo_Zip,
            merchant_info.MerchantInfo_Country,
            merchant_info.MerchantInfo_Phone,
			merchant_info.MerchantInfo_Neighborhood as venueNeighborhood,
			merchant_info.MerchantInfo_Keywords as venueKeywords,
                    (
                        SELECT pid
                        FROM permalinks
                        WHERE entity_name = 'merchant'
                        AND entity_id = e.MerchantInfo_ID
                        AND status = 'ACTIVE'
                        ORDER BY weight DESC
                        LIMIT 1
                    ) as pid,
                    (
                        SELECT pid
                        FROM permalinks
                        WHERE entity_name = 'organization'
                        AND entity_id = e.OrganizationInfo_ID
                        AND status = 'ACTIVE'
                        ORDER BY weight DESC
                        LIMIT 1
                    ) as orgPid";

    private static $FROM_TABLE_CLAUSE = "
        FROM (
        SELECT
                    0 as Event_ID,
                    v1.EventProposalVersions_ID as VersionID,
                    v1.EventProposalVersions_Title as Event_Title,
                    v1.EventProposalVersions_Description as Event_Description,
                    EventProposals_CreatedDate as Event_CreatedDate,
                    v1.EventProposalVersions_CreatedDate as Event_EditedDate,
                    v1.EventProposalVersions_StartDate as Event_Date,
                    v1.EventProposalVersions_StartTime as Event_StartTime,
                    v1.EventProposalVersions_EndDate as Event_EndDate,
                    v1.EventProposalVersions_EndTime as Event_EndTime,
                    v1.EventProposalVersions_Percentage as Event_MerchantPercentage,
                    v1.EventProposalVersions_ExpectedAttendance as Event_OrgAnticipatedCount,
                    v1.EventProposalVersions_OrgWhyYou as Event_OrgWhyYou,
                    v1.EventProposalVersions_Status as VersionStatus,
                    0 as Event_MerchantSecondaryPercentage,
                    EventProposals_MerchantID as MerchantInfo_ID,
                    EventProposals_OrgID as OrganizationInfo_ID
                FROM event_proposals
                JOIN event_proposal_versions v1 ON EventProposals_ID = v1.EventProposalVersions_EventProposalsID
                LEFT JOIN event_proposal_versions v2 ON (
                    v1.EventProposalVersions_EventProposalsID = v2.EventProposalVersions_EventProposalsID
                    AND v1.EventProposalVersions_ID < v2.EventProposalVersions_ID
                )
                WHERE v2.EventProposalVersions_ID IS NULL
                AND ( 0 = ? OR v1.EventProposalVersions_StartDate >= CURRENT_DATE)
                AND ( 0 = ? OR v1.EventProposalVersions_Status = 'PENDING')
                AND ( 0 = ? OR v1.EventProposalVersions_ID = ? )
                AND ( 0 = ? OR '1' = 'event-specific' )
                UNION
                SELECT
                    Event_ID,
                    0 as VersionID,
                    Event_Title,
                    Event_Description,
                    EventLog_TimeStamp as Event_CreatedDate,
                    (CASE Event_LastEditedDate WHEN 0 THEN EventLog_TimeStamp ELSE Event_LastEditedDate END) as Event_EditedDate,
                    Event_Date,
                    Event_StartTime,
                    Event_EndDate,
                    Event_EndTime,
                    EventMatch_MerchantPercentage as Event_MerchantPercentage,
                    Event_OrgAnticipatedCount,
                    Event_OrgWhyYou,
                    CASE COALESCE( merchant_info.MerchantInfo_ID, 0)
                        WHEN '0' THEN 'Not Yet Hosted'
                        ELSE 'ACCEPTED'
                    END as VersionStatus,
                    EventMatch_MerchantSecondaryPercentage as Event_MerchantSecondaryPercentage,
                    EventMatch_MerchantID as MerchantInfo_ID,
                    Event_ProposedByOrgID as OrganizationInfo_ID
                FROM event_info
                JOIN event_log ON Event_ID = EventLog_EventID AND EventLog_Action = 'Created'
                LEFT JOIN event_match ON EventMatch_OrgID = Event_ProposedByOrgID AND EventMatch_EventID = Event_ID
                LEFT JOIN merchant_info ON MerchantInfo_ID = EventMatch_MerchantID
                WHERE Event_Status = 'ACTIVE'
                AND Event_ProposedByMerchantID = 0
                AND NOT EXISTS(
                    SELECT 1
                    FROM event_proposals
                    WHERE EventProposals_EventID = Event_ID
                    LIMIT 1
                )
                AND ( 0 = ? OR event_info.Event_EndDate >= CURRENT_DATE)
                AND ( 0 = ? OR EventMatch_MerchantID IS NULL)
                AND ( 0 = ? OR '1' = 'version-specific' )
                AND ( 0 = ? OR event_info.Event_ID = ? )
        ) as e
        JOIN organization_info ON e.OrganizationInfo_ID = organization_info.OrganizationInfo_ID
        LEFT JOIN merchant_info ON e.MerchantInfo_ID > 0 AND merchant_info.MerchantInfo_ID = e.MerchantInfo_ID
        WHERE 1 = 1";

    private static $BASIC_QUERY_BINDING = "iiiiiiiiii";

    private static $FILTER_ON_ORG = "
        AND organization_info.OrganizationInfo_ID = ?";

    // TODO: fix the SQL for version specific and event specific (if no match, returns all records)

    public function get(EventProposalCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$GET_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = self::$BASIC_QUERY_BINDING;
        $upcomingOnly = ($criteria->upcomingOnly) ? 1 : 0;
        $pendingOnly = ($criteria->pendingOnly) ? 1 : 0;
        $versionSpecific = (is_numeric($criteria->versionId) && $criteria->versionId > 0) ? 1 : 0;
        $versionId = $versionSpecific ? $criteria->versionId : 0;
        $eventSpecific = (is_numeric($criteria->eventId) && $criteria->eventId > 0) ? 1 : 0;
        $eventId = $eventSpecific ? $criteria->eventId : 0;
        $filterParams = array(
            $upcomingOnly,
            $pendingOnly,
            $versionSpecific, $versionId,
            $eventSpecific,
            // union
            $upcomingOnly,
            $pendingOnly,
            $versionSpecific,
            $eventSpecific, $eventId
        );
        /*
        $upcomingOnly = ($criteria->upcomingOnly) ? 1 : 0;
        $publishedOnly = ($criteria->publishedOnly) ? 1 : 0;
        $eventSpecific = (is_numeric($criteria->eventId) && $criteria->eventId > 0) ? 1 : 0;
        $eventId = $eventSpecific ? $criteria->eventId : 0;
        $filterParams = array(
            $eventSpecific,
            $eventId,
            $publishedOnly,
            $upcomingOnly
        );
        */

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        /*
        if ($criteria->merchantId && is_numeric($criteria->merchantId)){
            $filterString .= self::$FILTER_ON_MERCHANT;
            $filterBinding .= "i";
            $filterParams[] = $criteria->merchantId;
        }
        */

        if ($criteria->orgId && is_numeric($criteria->orgId)){
            $filterString .= self::$FILTER_ON_ORG;
            $filterBinding .= "i";
            $filterParams[] = $criteria->orgId;
        }

        /*
        if ($criteria->eventId && is_numeric($criteria->eventId)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->eventId;
        }
        */

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("EventProposalProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventProposalProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.Event_Date desc, e.Event_StartTime desc";

    private function getOrderByClause(EventByLocationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "timestamp":
                        $orderByStrings[] = "e.Event_ID ".$sortOrder;
                        break;
                    case "startDate":
                        $orderByStrings[] = "e.Event_Date ".$sortOrder;
                        break;
                    case "startTime":
                        $orderByStrings[] = "e.Event_StartTime ".$sortOrder;
                        break;
                    case "endDate":
                        $orderByStrings[] = "e.Event_EndDate ".$sortOrder;
                        break;
                    case "endTime":
                        $orderByStrings[] = "e.Event_EndTime ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($datarow) {
        $record = new EventRecord();
        $record->id = $datarow->Event_ID;
        $record->title = $datarow->Event_Title;
        $record->description = $datarow->Event_Description;
        $record->createdDate = strtotime($datarow->Event_CreatedDate);
        $record->lastUpdatedDate = strtotime($datarow->Event_EditedDate);
        $record->setStartDate($datarow->Event_Date, $datarow->Event_StartTime);
        $record->setEndDate($datarow->Event_EndDate, $datarow->Event_EndTime);
        $record->isMultiDay = $datarow->Event_Date != $datarow->Event_EndDate;

        //$record->lastEditedByMemberID = $datarow->Event_LastEditedByMemberID;
        //$record->commaDelimitedTagNames = $datarow->tagNames;
        //$record->customAttendLink = $datarow->customAttendLink;
        //$record->isNationalEvent = $datarow->Event_IsNational;

        if (isset($datarow->VersionID)){
            $record->versionId = $datarow->VersionID;
        }
        if (isset($datarow->Event_OrgWhyYou)){
            $record->whyHostThisDescription = $datarow->Event_OrgWhyYou;
        }
        if (isset($datarow->Event_OrgAnticipatedCount)){
            $record->expectedAttendance = $datarow->Event_OrgAnticipatedCount;
        }
        if (isset($datarow->VersionStatus)){
            $record->versionStatus = $datarow->VersionStatus;
        }

        $record->venue = $this->mapToVenue($datarow);

        $record->cause = new Cause();
        if (is_numeric($datarow->OrganizationInfo_ID)){
            $record->cause->id = $datarow->OrganizationInfo_ID;
            $record->cause->name = $datarow->OrganizationInfo_Name;
            $record->cause->ein = $datarow->OrganizationInfo_EIN;
            $record->cause->setURL($datarow->OrganizationInfo_Website);
            $record->cause->pid = $datarow->orgPid;
        }
        return $record;
    }

    private function mapToVenue($datarow){
        $venue = new Venue();
        $venue->id = $datarow->MerchantInfo_ID;
        $venue->pid = $datarow->pid;
        $venue->name = $datarow->MerchantInfo_Name;
        $venue->url = $venue->setURL($datarow->MerchantInfo_Website);
        $venue->description = $datarow->MerchantInfo_Description;
        //$venue->location = $this->getEventLocation($venue->url, $venue->name); // only used for rss
        if (is_numeric($datarow->MerchantInfo_Lat)){
            $venue->lat = $datarow->MerchantInfo_Lat;
        }
        if (is_numeric($datarow->MerchantInfo_Long)){
            $venue->long = $datarow->MerchantInfo_Long;
        }
        $venue->streetAddr = $datarow->MerchantInfo_Address;
        $venue->city = $datarow->MerchantInfo_City;
        $venue->region = $datarow->MerchantInfo_State;
        $venue->postalCode = $datarow->MerchantInfo_Zip;
        $venue->country = $datarow->MerchantInfo_Country;
        $venue->phone = $this->getPhone($datarow->MerchantInfo_Phone);
        if (is_numeric($datarow->Event_MerchantPercentage)){
            $venue->featuredCausePercentage = $datarow->Event_MerchantPercentage;
        }
        //overide if event_match data used during migration scripts
        //if ($datarow->EventMatch_MerchantPercentage){
        //    $venue->featuredCausePercentage = $datarow->EventMatch_MerchantPercentage;
        //}

        if (is_numeric($datarow->Event_MerchantSecondaryPercentage)){
            $venue->customerChoicePercentage = $datarow->Event_MerchantSecondaryPercentage;
        }
        //overide if event_match data used during migration scripts
        //if ($datarow->EventMatch_MerchantSecondaryPercentage){
        //    $venue->customerChoicePercentage = $datarow->EventMatch_MerchantSecondaryPercentage;
        //}

        //$venue->facebookEventID = $datarow->Event_FacebookEventID;
        //overide if event_match data used during migration scripts
        //if ($datarow->EventMatch_FacebookEventID){
        //    $venue->facebookEventID = $datarow->EventMatch_FacebookEventID;
        //}

        //$venue->eventBriteEventID = $datarow->Event_EventBriteEventID;
        //overide if event_match data used during migration scripts
        //if ($datarow->EventMatch_EventBriteEventID){
        //    $venue->eventBriteEventID = $datarow->EventMatch_EventBriteEventID;
        //}

        $venue->eventBriteVenueID = $datarow->MerchantInfo_EventBriteVenueID;
        //$venue->fipsCounty = $datarow->FIPS_County;

        $venue->isFlatDonation = $datarow->isFlatDonation;
        $venue->donationPerUnit = $datarow->donationPerUnit;
        $venue->unitDescriptor = $datarow->unitDescriptor;
        $venue->flatDonationActionPhrase = $datarow->flatDonationActionPhrase;
        $venue->keywords = $datarow->venueKeywords;
        $venue->neighborhood = $datarow->neighborhood;

        return $venue;
    }

    private function getPhone($phoneString) {
        $cleanPhone = preg_replace("/[^0-9]/", "", $phoneString);
        if (strlen($cleanPhone) > 6){
            return preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $cleanPhone);

            //return "(".substr($cleanPhone, 0, 3).") ".substr($cleanPhone, 3, 3)."-".substr($cleanPhone,6);
        }
        return $cleanPhone;
    }
}