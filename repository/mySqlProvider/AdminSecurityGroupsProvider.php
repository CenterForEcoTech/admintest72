<?php
class AdminSecurityGroupsProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
			asg.AdminSecurityGroups_ID as id,
			asg.AdminSecurityGroups_Name as name,
			asg.AdminSecurityGroups_Description as description,
			asg.AdminSecurityGroups_DisplayOrderID as displayOrderId";

    private static $FROM_TABLE_CLAUSE = "
        FROM admin_securitygroups as asg
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND asg.AdminSecurityGroups_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID = "
        AND asg.AdminSecurityGroups_ID = ?";

    private static $FILTER_ON_DESCRIPTION = "
        AND asg.AdminSecurityGroups_DESCRIPTION = ?";
		
    private static $FILTER_ON_NAME = "
        AND asg.AdminSecurityGroups_Name = ?";
		
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->description){
            $filterString .= self::$FILTER_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
		}
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("AdminSecurityGroupsProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("AdminSecurityGroupsProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY asg.AdminSecurityGroups_DisplayOrderID ASC, asg.AdminSecurityGroups_Name ASC";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY asg.AdminSecurityGroups_ID ".$sortOrder;
                    break;
                case "description":
                    $orderBy = " ORDER BY asg.AdminSecurityGroups_Description ".$sortOrder;
                    break;
				case "name":
					$orderBy = " ORDER BY asg.AdminSecurityGroups_Name ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "asg.AdminSecurityGroups_ID ".$sortOrder;
                        break;
                    case "description":
                        $orderByStrings[] = "asg.AdminSecurityGroups_Description ".$sortOrder;
                        break;
					case "name":
						$orderByStrings[] = "asg.AdminSecurityGroups_Name ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return AdminSecurityGroupsRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL = "
		INSERT INTO admin_securitygroups
		(
			AdminSecurityGroups_Name,
			AdminSecurityGroups_Description,
			AdminSecurityGroups_DisplayOrderID
		) VALUES (
			?,?,?
		)";

    public function add(AdminSecurityGroupsRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "ssi";
        $sqlParam = array(
			$record->name,
			$record->description,
			$record->displayOrderId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("AdminSecurityGroupsProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL = "
        UPDATE admin_securitygroups
        SET
			AdminSecurityGroups_Name = ?,
			AdminSecurityGroups_Description = ?
		WHERE AdminSecurityGroups_ID = ?";

    public function update(AdminSecurityGroupsRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "ssi";
            $sqlParams = array(
				$record->name,
				$record->description,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("AdminSecurityGroupsProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	admin_securitygroups
        SET		AdminSecurityGroups_DisplayOrderID = ?
		WHERE	AdminSecurityGroups_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("AdminSecurityGroupsProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
}
