<?php
class ErrorContext{
    /** @var $postVars Array */
    public $postVars;

    /** @var $getVars Array */
    public $getVars;

    /** @var $cookies Array */
    public $cookies;

    /** @var $server Array */
    public $server;

    /** @var $files Array */
    public $files;

    /** @var $request Array */
    public $request;

    public $ip;

    /**
     * Takes the GLOBALS array and parses it
     * @static
     * @param $errorContext Array
     */
    public function __construct($errorContext = null){
        if (is_array($errorContext) && count($errorContext)){
            $this->postVars = (array)$errorContext["_POST"];
            $this->getVars = (array)$errorContext["_GET"];
            $this->cookies = (array)$errorContext["_COOKIE"];
            $this->server = (array)$errorContext["_SERVER"];
            $this->files = (array)$errorContext["_FILES"];
            $this->request = (array)$errorContext["_REQUEST"];
            $this->ip = $this->server["SERVER_ADDR"];
        }
    }

    /**
     * @param $jsonString
     * @return ErrorContext
     */
    public static function parse($jsonString){
        $stdObject = json_decode($jsonString);
        $contextObject = new ErrorContext();
        $contextObject->postVars = (array)$stdObject->postVars;
        $contextObject->getVars = (array)$stdObject->getVars;
        $contextObject->cookies = (array)$stdObject->cookies;
        $contextObject->server = (array)$stdObject->server;
        $contextObject->files = (array)$stdObject->files;
        $contextObject->request = (array)$stdObject->request;
        return $contextObject;
    }

    public static function extract($globalContext){
        $coreContext = array(
            "_POST" => $globalContext["_POST"],
            "_GET" => $globalContext["_GET"],
            "_COOKIE" => $globalContext["_COOKIE"],
            "_SERVER" => $globalContext["_SERVER"],
            "_FILES" => $globalContext["_FILES"],
            "_REQUEST" => $globalContext["_REQUEST"],
        );
        return $coreContext;
    }
}

class LoggedError{
    public $id;
    public $errorLevel;
    public $message;
    public $file;
    public $line;
    /** @var $context ErrorContext */
    public $context;
    /** @var $timestamp int */
    public $timestamp;
    public $count;
    public $firstOccurrenceDate;

    public function __construct($errorLevel, $message, $context, $file = null, $line = null, $timeString = null){
        $this->errorLevel = $errorLevel;
        $this->message = $message;
        $this->context = new ErrorContext($context);
        $this->file = $file;
        $this->line = $line;
        $this->timestamp = $timeString == null ? time() : strtotime($timeString);

        if ($this->context && !$this->file){
            $this->file = $this->context->server["SCRIPT_NAME"];
        }
        if ($context["line"]){
            $this->line = $context["line"];
        }
    }
}

include_once($rootFolder."repository/mySqlProvider/Helper.php");

class ErrorLoggingCriteria extends PaginationCriteria{
    public $date = null;
}

class ErrorLoggingProvider extends \Monolog\Handler\AbstractProcessingHandler implements \Monolog\Formatter\FormatterInterface {
    /** @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli, $level = \Monolog\Logger::DEBUG, $bubble = true)
    {
        $this->mysqli = $mysqli;
        parent::__construct($level, $bubble);
        parent::setFormatter($this);
    }

    protected function write(array $record)
    {
        $loggedError = new LoggedError($record["level"], $record["message"], $record["context"]);

        $this->put($loggedError);
    }

    public function format(array $record){
        return ""; // do nothing!
    }

    public function formatBatch(array $records){
        return ""; // do nothing!
    }

    private static $INSERT_SQL = "
        INSERT INTO error_logging
        (
            error_level,
            error_level_text,
            message,
            orig_message,
            file,
            line,
            ip,
            context_serialized,
            updated_date
        )
        SELECT
            ?,?,?,?,?,?,?,?,
            CURRENT_TIMESTAMP()
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT 1
            FROM error_logging
            WHERE error_level = ?
            AND message = ?
            AND file = ?
            AND line = ?
            AND ip = ?
        )";

    private static $INSERT_BINDING = "isssssssissss";

    private function insert(LoggedError $error){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = self::$INSERT_BINDING;
        $sqlParams = array(
            $error->errorLevel,
            translateErrorType($error->errorLevel),
            $error->message."", // truncated if too long
            $error->message."",
            $error->file."",
            $error->line."",
            $error->context->ip."",
            json_encode($error->context)."",

            $error->errorLevel,
            $error->message."",
            $error->file."",
            $error->line."",
            $error->context->ip.""
        );
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        return $response;
    }

    private static $SELECT_SQL = "
        SELECT id
        FROM error_logging
        WHERE error_level = ?
        AND message = ?
        AND file = ?
        AND line = ?
        AND ip = ?";

    private static $UPDATE_SQL = "
        UPDATE error_logging
        SET count = count + 1,
            updated_date = CURRENT_TIMESTAMP(),
            status = 'ACTIVE'
        WHERE id = ?";

    private function update(LoggedError $error){
        $selectString = self::$SELECT_SQL;
        $selectBinding = "issss";
        $selectParams = array(
            $error->errorLevel,
            $error->message."",
            $error->file."",
            $error->line."",
            $error->context->ip.""
        );
        $selectResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $selectString, $selectBinding, $selectParams, $selectResponse);
        if ($selectResponse->success && count($selectResponse->resultArray)){
            $id = $selectResponse->resultArray[0]["id"];
            $updateString = self::$UPDATE_SQL;
            $updateBinding = "i";
            $updateParams = array($id);
            $updateResponse = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, $updateString, $updateBinding, $updateParams, $updateResponse);
            return $updateResponse;
        }
        return false;
    }

    public function put(LoggedError $error){
        $insertAttempt = $this->insert($error);
        if ($insertAttempt->success && $insertAttempt->insertedId){
            return true;
        }
        $updateAttempt = $this->update($error);
        if ($updateAttempt === false){
            // something failed, but we really cannot do anything about it here, can we?
            return false;
        }
        return $updateAttempt->success;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            id,
            error_level,
            error_level_text,
            message,
            orig_message,
            file,
            line,
            ip,
            context_serialized,
            created_date,
            updated_date,
            status,
            count";

    private static $FROM_TABLE_CLAUSE = "
        FROM error_logging
        WHERE status = 'ACTIVE'";

    private static $FILTER_ON_ID = "
        AND id = ?";

    private static $FILTER_ON_DATE = "
        AND created_date > ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            error_level_text LIKE ?
            OR message LIKE ?
            OR file LIKE ?
            OR context_serialized LIKE ?
            or LINE LIKE ?
        )";

    /**
     * @param ErrorLoggingCriteria $criteria
     * @return PagedResult
     */
    public function get(ErrorLoggingCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->date){
            $startDate = strtotime($criteria->date);
            $filterString .= self::$FILTER_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = date("Y-m-d", $startDate);
        }
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $rowObject = (object)$row;
            $record = $this->mapToRecord($rowObject);
            $result->collection[] = $record;
        }

        return $result;
    }

    public function getRecord($id){
        if (is_numeric($id)){
            $sqlString = self::$SELECT_COLUMNS.self::$FROM_TABLE_CLAUSE.self::$FILTER_ON_ID;
            $sqlBinding = "i";
            $sqlParam = array($id);
            $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
            if (count($result)){
                $rowObject = (object)$result[0];
                return $this->mapToRecord($rowObject);
            }
        }
        return null;
    }

    private function mapToRecord($rowObject){
        $record = new LoggedError(
            $rowObject->error_level,
            $rowObject->orig_message,
            null,
            $rowObject->file,
            $rowObject->line,
            $rowObject->updated_date
        );
        $record->id = $rowObject->id;
        $record->count = $rowObject->count;
        $record->context = ErrorContext::parse($rowObject->context_serialized);
        $record->context->ip = $rowObject->ip;
        $record->firstOccurrenceDate = $rowObject->created_date;
        return $record;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY id desc";

    private static $ORDER_BY_ID = " ORDER BY id";

    private static $ORDER_BY_DATE = " ORDER BY updated_date";

    private static $ORDER_BY_TYPE = " ORDER BY error_level_text";

    private static $ORDER_BY_MESSAGE = " ORDER BY message";

    private static $ORDER_BY_FILE = " ORDER BY file";

    private static $ORDER_BY_LINE = " ORDER BY line";

    private static $ORDER_BY_IP = " ORDER BY ip";

    private static $ORDER_BY_COUNT = " ORDER BY count";

    private function getOrderByClause(ErrorLoggingCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "dateString":
                    $orderBy = self::$ORDER_BY_DATE." ".$sortOrder;
                    break;
                case "errorLevel":
                    $orderBy = self::$ORDER_BY_TYPE." ".$sortOrder;
                    break;
                case "message":
                    $orderBy = self::$ORDER_BY_MESSAGE." ".$sortOrder;
                    break;
                case "file":
                    $orderBy = self::$ORDER_BY_FILE." ".$sortOrder;
                    break;
                case "line":
                    $orderBy = self::$ORDER_BY_LINE." ".$sortOrder;
                    break;
                case "ip":
                    $orderBy = self::$ORDER_BY_IP." ".$sortOrder;
                    break;
                case "count":
                    $orderBy = self::$ORDER_BY_COUNT." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}