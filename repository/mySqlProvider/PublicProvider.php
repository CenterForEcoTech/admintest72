<?php
class PublicProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $INSERT_EEOFORM_SQL = "
		INSERT INTO hr_eeo
		(
			HREEO_Ethnicity,
			HREEO_EthnicityOther,
			HREEO_Gender,
			HREEO_Participation,
			HREEO_Title,
			HREEO_IPAddress,
			HREEO_Source
		) VALUES (
			?,?,?,?,?,?,?
		)";

    public function eeoSubmit($record,$title,$ipAddress){
		$ethnicity = (count($record->ethnicity) > 1 ? implode(",",$record->ethnicity) : $record->ethnicity);
		$gender = (count($record->gender) > 1 ? implode(",",$record->gender) : $record->gender);
        $sqlString = self::$INSERT_EEOFORM_SQL;
        $sqlBinding = "sssssss";
        $sqlParam = array(
			$ethnicity,
			$record->ethnicityOther,
			$gender,
			$record->participation,
			$title,
			$ipAddress,
			$record->source
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("PublicProvider.eeoSubmit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
    private static $SELECT_COLUMNS = "
        SELECT
			e.HREEO_ID as id,
			e.HREEO_Ethnicity as ethnicity,
			e.HREEO_EthnicityOther as ethnicityOther,
			e.HREEO_Gender as gender,
			e.HREEO_Participation as participation,
			e.HREEO_Title as title,
			e.HREEO_Source as source,
			e.HREEO_TimeStamp as timeStamp";

    private static $FROM_TABLE_CLAUSE = "
        FROM hr_eeo as e
        WHERE 1 = 1";
				
    public function get(){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY e.HREEO_TimeStamp DESC";
        $limiter = "";

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("PublicProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("PublicProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
	
    private static $INSERT_EEOLINK_SQL = "
		INSERT INTO hr_employee_recruitment_eeolinks
		(
			HREmployeeRecruitmentEEOLinks_Token,
			HREmployeeRecruitmentEEOLinks_IPAddress
		) VALUES (
			?,?
		)";

    public function eeoLink($criteria){
        $sqlString = self::$INSERT_EEOLINK_SQL;
        $sqlBinding = "ss";
        $sqlParam = array(
			$criteria->token,
			$criteria->ipAddress
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        }

        return $insertResponse;
    }
	
    private static $UPDATE_EEOLINK_SQL = "
		UPDATE hr_employee_recruitment_eeolinks
		SET 
			HREmployeeRecruitmentEEOLinks_Title = ?
		WHERE
			HREmployeeRecruitmentEEOLinks_Token = ?
			AND	HREmployeeRecruitmentEEOLinks_IPAddress = ?";

    public function eeoLinkUpdate($criteria){
        $sqlString = self::$UPDATE_EEOLINK_SQL;
        $sqlBinding = "sss";
        $sqlParams = array(
			$criteria->title,
			$criteria->token,
			$criteria->ipAddress
		);
        $updateResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
        return $updateResponse;
    }
	
    private static $SELECT_COLUMNS_SEARCH = "
        SELECT
			HREmployeeRecruitmentEEOLinks_ID as id,
			HREmployeeRecruitmentEEOLinks_Token as token,
			HREmployeeRecruitmentEEOLinks_IPAddress as ipAddress,
			HREmployeeRecruitmentEEOLinks_Title as title,
			HREmployeeRecruitmentEEOLinks_TimeStamp as timeStamp";

    private static $FROM_TABLE_SEARCH_CLAUSE = "
        FROM hr_employee_recruitment_eeolinks
        WHERE 1 = 1";
		
	private static $FILTER_EEOLINK_BY_TOKEN = "
		AND HREmployeeRecruitmentEEOLinks_Token = ?";
	
	private static $FILTER_EEOLINK_BY_IP = "
		AND HREmployeeRecruitmentEEOLinks_IPAddress = ?";
		
    public function eeoLinkSearch($criteria){
        $sqlString = self::$SELECT_COLUMNS_SEARCH;
        $fromTableClause = self::$FROM_TABLE_SEARCH_CLAUSE;
		$filterString = "";
		$filterBinding = "";
        $filterParams = array();
		if ($criteria->token){
			$filterString .= self::$FILTER_EEOLINK_BY_TOKEN;
			$filterBinding .= "s";
			$filterParams[] = $criteria->token;
		}
		if ($criteria->ipAddress){
			$filterString .= self::$FILTER_EEOLINK_BY_IP;
			$filterBinding .= "s";
			$filterParams[] = $criteria->ipAddress;
		}
		$SqlString = $sqlString.$fromTableClause.$filterString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("PublicProvider.eeoLinkSearch failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}

        return $result;
    }
	
    private static $INSERT_RECRUITMENTTITLES_SQL = "
		INSERT INTO hr_employee_recruitment_titles
		(
			HREmployeeRecruitmentTitle_Title,
			HREmployeeRecruitmentTitle_Token,
			HREmployeeRecruitmentTitle_AddedByAdmin
		) VALUES (
			?,?,?
		)";

    public function eeoRecruitmentLink($criteria,$adminId){
        $sqlString = self::$INSERT_RECRUITMENTTITLES_SQL;
        $sqlBinding = "sss";
        $sqlParam = array(
			$criteria->title,
			$criteria->token,
			$adminId
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        }

        return $insertResponse;
    }	
	public function insertEZDocData($data){
		$sqlString = "INSERT INTO ezdoc_online_data
		(
			Data
		) Values
		(
			?
		)";
		$sqlBinding = "s";
		$sqlParam = array($data);
		$insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        return $insertResponse;
	}
    private static $SELECT_COLUMNS_TITLETOKEN = "
        SELECT
			HREmployeeRecruitmentTitle_ID as id,
			HREmployeeRecruitmentTitle_Title as title,
			HREmployeeRecruitmentTitle_Token as token,
			HREmployeeRecruitmentTitle_TimeStamp as timeStamp,
			HREmployeeRecruitmentTitle_AddedByAdmin as addedByAdmin";

    private static $FROM_TABLE_TITLETOKEN_CLAUSE = "
        FROM hr_employee_recruitment_titles
        WHERE 1 = 1";
		
	private static $FILTER_TITLETOKEN_BY_TOKEN = "
		AND HREmployeeRecruitmentTitle_Token = ?";
	
	private static $FILTER_TITLETOKEN_BY_TITLE = "
		AND HREmployeeRecruitmentTitle_Title = ?";
		
    public function eeoTitleTokenSearch($criteria){
        $sqlString = self::$SELECT_COLUMNS_TITLETOKEN;
        $fromTableClause = self::$FROM_TABLE_TITLETOKEN_CLAUSE;
		$filterString = "";
		$filterBinding = "";
        $filterParams = array();
		if ($criteria->token){
			$filterString .= self::$FILTER_TITLETOKEN_BY_TOKEN;
			$filterBinding .= "s";
			$filterParams[] = $criteria->token;
		}
		if ($criteria->title){
			$filterString .= self::$FILTER_TITLETOKEN_BY_TITLE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->title;
		}
		$SqlString = $sqlString.$fromTableClause.$filterString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("PublicProvider.eeoTitleTokenSearch failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}

        return $result;
    }
	
}