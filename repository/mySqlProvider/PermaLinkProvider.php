<?php
include_once("Helper.php");

interface PermaLinkProvider{

    /**
     * Returns a PermaLinkRecord denoting the location parameters of a specific page on the site
     * @abstract
     * @param $pid
     * @return PermaLinkRecord
     */
    public function get($pid);

    /**
     * Returns all the permalinks for the specified entity instance
     * @abstract
     * @param $entityName
     * @param $entityId
     * @return array of PermaLinkRecord
     */
    public function getByEntity($entityName, $entityId);

    /**
     * Attempts to add a permalink for an entity, and returns the updated record upon success. If unsuccessful, returns
     * @abstract
     * @param AddPermaLinkRequest $request
     * @return AddPermalinkResponse
     */
    public function put(AddPermaLinkRequest $request);

    /**
     * Returns true if the pid is not currently used
     * @abstract
     * @param $pid
     * @return true if available
     */
    public function isAvailable($pid);
}

class PermaLinkProviderImpl implements PermaLinkProvider{

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private function mapRowToRecord($row, $record = null){
        if ($record == null){
            $record = new PermaLinkRecord();
        }
        $record->pid = $row->pid;
        $record->entityName = $row->entity_name;
        $record->entityId = $row->entity_id;
        $record->weight = $row->weight;
        $record->currentPid = $row->currentPid;
        return $record;
    }

    private static $GET_PERMALINK_SQL = "
        SELECT
            pid,
            entity_name,
            entity_id,
            weight,
			(
	            SELECT current.pid
	            FROM permalinks current
	            WHERE current.entity_name = orig.entity_name
	            AND current.entity_id = orig.entity_id
                AND current.status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as currentPid
        FROM permalinks orig
        WHERE pid = ?
        AND status = 'ACTIVE'
        ORDER BY weight DESC";

    /**
     * Returns a PermaLinkRecord denoting the location parameters of a specific page on the site
     * @param $pid
     * @return PermaLinkRecord
     */
    public function get($pid)
    {
        $record = new PermaLinkRecord();
        $sqlString = self::$GET_PERMALINK_SQL;
        $sqlBindings = "s";
        $sqlParams = array($pid);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            $row = $result[0];
            $this->mapRowToRecord((object)$row, $record);
        }
        return $record;
    }

    public function getMerchantId($pid){
        $record = $this->get($pid);
        if ($record->entityName == PermaLinkRecord::$MERCHANT_ENTITY_NAME){
            return $record->entityId;
        }
        return 0;
    }

    public function getOrganizationId($pid){
        $record = $this->get($pid);
        if ($record->entityName == PermaLinkRecord::$ORG_ENTITY_NAME){
            return $record->entityId;
        }
        return 0;
    }

    private static $GET_PERMALINK_BY_ENTITY_SQL = "
        SELECT
            pid,
            entity_name,
            entity_id,
            weight,
            status
        FROM permalinks
        WHERE entity_name = ?
        AND entity_id = ?
        ORDER BY status, weight DESC, id DESC";

    /**
     * Returns all the permalinks for the specified entity instance
     * @param $entityName
     * @param $entityId
     * @return array of PermaLinkRecord
     */
    public function getByEntity($entityName, $entityId)
    {
        $collection = array();
        $sqlString = self::$GET_PERMALINK_BY_ENTITY_SQL;
        $sqlBindings = "si";
        $sqlParams = array($entityName, $entityId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        foreach ($result as $row){
            $record = $this->mapRowToRecord((object)$row);
            $collection[] = $record;
        }

        return $collection;
    }

    private static $INSERT_PERMALINK_SQL = "
        INSERT INTO permalinks
        (
            pid,
            entity_name,
            entity_id,
            weight
        )
        SELECT
          ?,?,?,?
        FROM dual
        WHERE NOT EXISTS(
          SELECT 1
          FROM permalinks
          WHERE pid = ?
        )";

    /**
     * Attempts to add a permalink for an entity, and returns the updated record upon success. If unsuccessful, returns
     * @param AddPermaLinkRequest $request
     * @return AddPermalinkResponse
     */
    public function put(AddPermaLinkRequest $request)
    {
        $response = new AddPermaLinkResponse();
        if ($request != null){
            if ($request->isValid()){
                $pid = $request->getPid();
                $weight = $request->getWeight();
                $entityName = $request->getEntityName();
                $entityId = $request->getEntityId();
                $sqlString = self::$INSERT_PERMALINK_SQL;
                $sqlBindings = "ssiis";
                $sqlParams = array($pid, $entityName, $entityId, $weight, $pid);
                $sqlResponse = new ExecuteResponseRecord();
                $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $sqlResponse);
                if ($result){
                    $response->success = $result;
                    $record = $this->get($pid);
                    $response->record = $record;
                } else {
                    $response->success = false;
                    $response->message = $sqlResponse->error;
                }
            } else {
                $response->message = $request->getValidationMessage();
            }
        } else {
            $response->message = "Invalid Request";
        }
        return $response;
    }

    /**
     * Returns true if the pid is not currently used
     * @param $pid
     * @return true if available
     */
    public function isAvailable($pid)
    {
        $existing = $this->get($pid);
        return !($existing->entityId > 0);
    }

    public function tryAddPermalink(AddPermaLinkRequest $request, $maxAttempts = 3){
        $attempts = 0;
        $success = false;
        $response = new AddPermaLinkResponse();
        while(!$success && $attempts < $maxAttempts){
            $request->generatePid();
            $response = $this->put($request);
            $success = $response->success;
            $attempts++;
        }
        return $response;
    }
}

class MockPermaLinkProvider implements PermaLinkProvider{

    /**
     * Returns a PermaLinkRecord denoting the location parameters of a specific page on the site
     * @param $pid
     * @return PermaLinkRecord
     */
    public function get($pid)
    {
        $record = new PermaLinkRecord();
        $record->entityName = PermaLinkRecord::$MERCHANT_ENTITY_NAME;
        $record->entityId = 2;
        $record->pid = $pid;
        $record->weight = 0;
        return $record;
    }

    /**
     * Attempts to add a permalink for an entity, and returns the updated record upon success. If unsuccessful, returns
     * @param AddPermaLinkRequest $request
     * @param $pid
     * @return AddPermalinkResponse
     */
    public function put(AddPermaLinkRequest $request)
    {
        $response = new AddPermaLinkResponse();
        $response->message = "Not Yet Implemented";
        return $response;
    }

    /**
     * Returns true if the pid is not currently used
     * @param $pid (unique and case-INSENSITIVE)
     * @return true if available
     */
    public function isAvailable($pid)
    {
        return true;
    }

    /**
     * Returns all the permalinks for the specified entity instance
     * @param $entityName
     * @param $entityId
     * @return array of PermaLinkRecord
     */
    public function getByEntity($entityName, $entityId)
    {
        $record = $this->get("blah");
        $record->entityId = $entityId;
        $record->entityName = $entityName;
        return array($record);
    }
}

class PermaLinkRecord{
    public $pid; // the permalink id used to retrieve this PermaLinkRecord; or the id to be added for this entity
    public $entityName; // e.g., Merchant, Organization
    public $entityId; // numeric identifier for the entity
    public $weight;
    public $currentPid; // optional, to hold the current pid in case the entity has more than one

    public static $MERCHANT_ENTITY_NAME = "merchant";
    public static $ORG_ENTITY_NAME = "organization";
    public static $INDIVIDUAL_ENTITY_NAME = "individual";
    public static $ENTITY_NAME_INVALID = "Invalid Entity.";

    public static function isValidEntityName($name){
        return $name == self::$MERCHANT_ENTITY_NAME ||
                $name == self::$ORG_ENTITY_NAME ||
                $name == self::$INDIVIDUAL_ENTITY_NAME;
    }
}

class AddPermaLinkRequest{
    private $pid; // the desired new pid
    private $entityName; // the specific entity name
    private $entityId; // the specific entity id to associate with the pid
    private $validationMessages;
    private $weight = 0; // the priority: the higher the number the higher the priority, but at this time, zero means it was auto-assigned, one means it is custom

    public function __construct(){
        $this->validationMessages = array();
        $this->generatePid();
        $this->validate();
    }

    private static $CODE_LETTERS = "abcdefghijklmnopqrstuvwxyz0123456789";

    public function generatePid(){
        $candidatePid = substr(str_shuffle(self::$CODE_LETTERS), 0, 5);
        $candidatePid .= "-".substr(str_shuffle(self::$CODE_LETTERS), 0, 9);
        $candidatePid .= "-".substr(str_shuffle(self::$CODE_LETTERS), 0, 7);
        $this->setPid($candidatePid);
        $this->weight = 0;
    }

    private function validate(){
        $this->validatePid();
        $this->validateEntityName();
        $this->validateEntityId();
    }

    public function getValidationMessage(){
        return join("; ", $this->validationMessages);
    }

    public function getWeight(){
        return $this->weight;
    }

    public function isValid(){
        return (count($this->validationMessages) > 0) ? false : true;
    }

    private function resetValidation($validationKey){
        if (isset($this->validationMessages[$validationKey])){
            unset($this->validationMessages[$validationKey]);
        }
    }

    private function isInvalid($result, $validationKey, $message){
        if ($result){
            $this->validationMessages[$validationKey] = $message;
            return true;
        }
        return false;
    }

    // ------------------------------------
    // PID
    // ------------------------------------

    public function getPid(){
        return $this->pid;
    }

    public function setPid($pid){
        $this->pid = $pid;
        $this->validatePid();
        $this->weight++;
    }

    private static $PID_TOO_SHORT = "Must be at least 6 characters long.";
    private static $PID_TOO_LONG = "Must be 128 characters or shorter.";
    private static $PID_INVALID_CHARS = "Must only contain letters, numbers, or a hyphen character.";

    /**
     * Permalink pid rules:
     *
     * Can be a minimum of 8 characters and a maximum of 128 characters
     * Can only contain numbers, letters, or hyphens (and nothing else)
     */
    private function validatePid(){
        $validationKey = "pID";
        $this->resetValidation($validationKey);
        if (call_user_func(array($this, 'isInvalid'),
                    strlen($this->pid) < 6,
                    $validationKey,
                    self::$PID_TOO_SHORT) ||
            call_user_func(array($this, 'isInvalid'),
                    strlen($this->pid) > 128,
                    $validationKey,
                    self::$PID_TOO_LONG) ||
            call_user_func(array($this, "isInvalid"),
                    !preg_match("/^[\d\w-]*$/", $this->pid),
                    $validationKey,
                    self::$PID_INVALID_CHARS)){
            return false;
        }
        return true;
    }

    // ------------------------------------
    // Entity Name
    // ------------------------------------

    public function getEntityName(){
        return $this->entityName;
    }

    public function setEntityName($entityName){
        $this->entityName = strtolower($entityName);
        $this->validateEntityName();
    }

    private static $ENTITY_NAME_REQUIRED = "Entity Name is required.";

    private function validateEntityName(){
        $validationKey = "entityName";
        $this->resetValidation($validationKey);
        if (call_user_func(array($this, "isInvalid"),
                strlen($this->entityName) < 1,
                $validationKey,
                self::$ENTITY_NAME_REQUIRED) ||
            call_user_func(array($this, "isInvalid"),
                !PermaLinkRecord::isValidEntityName($this->entityName),
                $validationKey,
                PermaLinkRecord::$ENTITY_NAME_INVALID)){
            return false;
        }
        return true;
    }

    // ------------------------------------
    // Entity Id
    // ------------------------------------

    public function getEntityId(){
        return $this->entityId;
    }

    public function setEntityId($entityId){
        $this->entityId = $entityId;
        $this->validateEntityId();
    }

    private static $ENTITY_ID_REQUIRED = "Entity Id is required.";

    private function validateEntityId(){
        $validationKey = "entityId";
        $this->resetValidation($validationKey);
        if (call_user_func(array($this, "isInvalid"),
                (strlen($this->entityId) < 1),
                $validationKey,
                self::$ENTITY_ID_REQUIRED)){
            return false;
        }
        return true;
    }
}

class AddPermaLinkResponse{
    public $record; // PermaLinkRecord
    public $message; // string error message
    public $success; // true if action successful

    public function __construct(){
        $this->success = false;
    }
}
?>