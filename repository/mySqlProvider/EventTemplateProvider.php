<?php
include_once("Helper.php");
include_once("CampaignProvider.php");
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once("EventTemplatePremiumProvider.php");
include_once("CampaignAssociationProvider.php");

/**
 * EventTemplateProvider's business is to manage Event Templates.
 */
class EventTemplateProvider {

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    public function saveTemplate(EventTemplate $template, $adminUserId){
        if ($template->id){
            return $this->updateTemplate($template, $adminUserId);
        }
        return $this->addTemplate($template, $adminUserId);
    }

    private static $UPDATE_EVENT_TEMPLATE_SQL = "
		UPDATE event_templates SET
			EventTemplate_Name = ?,
			EventTemplate_URL = ?,
			EventTemplate_PageHeader = ?,
			EventTemplate_PageSubtitle = ?,
			EventTemplate_InstructionsMarkdown = ?,
			EventTemplate_InstructionsHtml = ?,

			EventTemplate_EventName = ?,
			EventTemplate_EventStartDate = ?,
			EventTemplate_EventStartTime = ?,
			EventTemplate_EventEndDate = ?,
			EventTemplate_EventEndTime = ?,

			EventTemplate_EventDescription = ?,
			EventTemplate_EventPercentageID = ?,
			EventTemplate_LastEditedByAdminID = ?,
			EventTemplate_LastEditedDate = curdate()
			WHERE EventTemplate_ID = ?";

    private function updateTemplate(EventTemplate $template, $adminUserId){
        $response = new SaveEventTemplateResponse();
        $response->record = $template;
        $sqlString = self::$UPDATE_EVENT_TEMPLATE_SQL;
        $sqlBindings = "ssssssssssssiii";
        $sqlParams = array(
            $template->name,
            $template->url,
            $template->pageHeader,
            $template->pageSubtitle,
            $template->instructionsMarkdownSource,
            $template->instructionsHtmlSource,

            $template->eventName,
            $template->startDate,
            $template->startTime,
            $template->endDate,
            $template->endTime,

            $template->description,
            $template->percentageID,
            $adminUserId,
            $template->id
        );
        $updateResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
        $response->success = $updateResponse->success;
        if (!$updateResponse->success){
            $response->message = $updateResponse->error;
        } else {
            $copyOfCodes = $template->tagNamesArray;
            // by this time, we've handled the codes we want to delete in the system, we just need to insert the remaining codes
            $this->insertTags($template->id, $copyOfCodes);
            $response->record = $this->getTemplateByID($template->id, false);
        }
        return $response;
    }

    private static $INSERT_EVENT_TEMPLATE_SQL = "
		INSERT INTO event_templates (
			EventTemplate_Name,
			EventTemplate_URL,
			EventTemplate_PageHeader,
			EventTemplate_PageSubtitle,
			EventTemplate_InstructionsMarkdown,
			EventTemplate_InstructionsHtml,

			EventTemplate_EventName,
			EventTemplate_EventStartDate,
			EventTemplate_EventStartTime,
			EventTemplate_EventEndDate,
			EventTemplate_EventEndTime,

			EventTemplate_EventDescription,
			EventTemplate_EventPercentageID,
			EventTemplate_CreatedByAdminID,
			EventTemplate_LastEditedByAdminID
			) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?
			)";

    private static $INSERT_EVENT_TEMPLATE_BINDINGS = "ssssssssssssiii";

    private function addTemplate(EventTemplate $template, $adminUserId){
        $response = new SaveEventTemplateResponse();
        $response->record = $template;
        $sqlString = self::$INSERT_EVENT_TEMPLATE_SQL;
        $sqlParams = array(
            $template->name,
            $template->url,
            $template->pageHeader."",
            $template->pageSubtitle,
            $template->instructionsMarkdownSource,
            $template->instructionsHtmlSource,

            $template->eventName,
            $template->startDate,
            $template->startTime,
            $template->endDate,
            $template->endTime,

            $template->description,
            $template->percentageID,
            $adminUserId,
            $adminUserId
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("EventTemplateProvider.addTemplate: ", $this->mysqli, $sqlString, self::$INSERT_EVENT_TEMPLATE_BINDINGS, $sqlParams, $insertResponse);
        $response->record->id = $insertResponse->insertedId;
        $response->success = $insertResponse->success;
        if (!$insertResponse->success || $insertResponse->affectedRows == 0){
            $response->success = false;
            $response->message = $insertResponse->error;
            if ($insertResponse->affectedRows == 0){
                $response->message .= " Unique Constraint Violation: Name must be unique for each Template URL value";
            }
            return $response;
        } else {
            if (count($template->tagNamesArray)){
                $this->insertTags($insertResponse->insertedId, $template->tagNamesArray);
            }
            $response->record = $this->getTemplateByID($insertResponse->insertedId, false);
        }
        return $response;
    }

    private function insertTags($templateId, $arrayOfTagNames){
        $associationProvider = new CampaignAssociationProvider($this->mysqli);
        return $associationProvider->setEventTemplateTags($arrayOfTagNames, $templateId);
    }

    private function getTagsFor($templateId){
        $associationProvider = new CampaignAssociationProvider($this->mysqli);
        return $associationProvider->getEventTemplateTags($templateId);
    }

// TODO: get rid of this sql injection point
	public function checkUnique($WhereField) {
		$ThisSQL = "SELECT * FROM event_templates WHERE ".$WhereField;
		//return $ThisSQL.$result_Events;
        $result_Events = MySqliHelper::get_result($this->mysqli, $ThisSQL);
		return $result_Events;
	}

    private static $UPDATE_LOGO_IMAGE_SQL = "
        UPDATE event_templates
        SET EventTemplate_CalendarIconImageID = (
            SELECT id
            FROM cms_image_library
            WHERE id = ?
            AND status = 'ACTIVE'
        )
        WHERE EventTemplate_ID = ?";

    public function setImageId($templateId, $imageId = 0){
        $sqlString = self::$UPDATE_LOGO_IMAGE_SQL;
        $sqlBinding = 'si';
        $sqlParams = array($imageId, $templateId);
        return MySqliHelper::executeWithLogging("EventTemplateProvider.setImageId: SQL Error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $SQL_GET_TEMPLATES = "
		SELECT
			et.*,
				(SELECT GROUP_CONCAT(CAST(TagAssociation_TagListID AS CHAR)) Tags2 FROM tag_association ta
				WHERE ta.TagAssociation_SourceID=et.EventTemplate_ID AND ta.TagAssociation_SourceTable='event_templates' AND ta.TagAssociation_Status IS NULL
				GROUP BY TagAssociation_SourceID ) AS Tags
		FROM event_templates et ORDER BY EventTemplate_CreatedDate DESC";

    /**
     * Returns all event templates for the admin console. TagNames are not guaranteed to be in desired order.
     * @return array
     */
    public function getAll() {
        $result_Events = MySqliHelper::get_result($this->mysqli, self::$SQL_GET_TEMPLATES);
        return $this->mapToCollection($result_Events);
    }
    private static $SQL_GET_TEMPLATES_BY_TAG = "
		SELECT
			event_templates.*, tag_association.* 
		FROM event_templates 
			LEFT JOIN tag_association ON TagAssociation_SourceID=EventTemplate_ID 
		WHERE TagAssociation_SourceTable='event_templates' AND (TagAssociation_Status IS NULL OR TagAssociation_Status != 'Deleted') AND TagAssociation_TagListID=? 
		ORDER BY EventTemplate_CreatedDate DESC";

    /**
     * Old function used by old campaign page. Get rid of this once the old page is removed from code base.
     * @param $tagId
     * @return array
     * @deprecated use getTemplatesForCampaign. This gets way too much data for what it is used for.
     */
    public function getAllTemplatesByTag($tagId) {
		$sqlString = self::$SQL_GET_TEMPLATES_BY_TAG;
		$sqlBindings = "i";
		$sqlParams = array($tagId);
        $result_Templates = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToCollection($result_Templates);
    }

    private static $GET_TEMPLATES_FOR_CAMPAIGN = "
        SELECT
            et.EventTemplate_Name as name,
            et.EventTemplate_URL as url,
            (
                SELECT GROUP_CONCAT(TagList_Name ORDER BY ta.TagAssociation_SortOrder, t.TagList_Name)
                FROM tag_association ta
                JOIN tag_list t ON ta.TagAssociation_TagListID = t.TagList_ID
                WHERE ta.TagAssociation_SourceID = et.EventTemplate_ID
                AND ta.TagAssociation_SourceTable = 'event_templates'
                AND ta.TagAssociation_Status IS NULL
                GROUP BY TagAssociation_SourceID
            ) AS commaDelimitedTagNames
        FROM event_templates et
        JOIN tag_association ta
            ON ta.TagAssociation_SourceID = et.EventTemplate_ID
            AND ta.TagAssociation_SourceTable = 'event_templates'
            AND ta.TagAssociation_TagListID = ?
        WHERE ta.TagAssociation_Status IS NULL
        ";

    /**
     * Returns an array of event template records pared down to just the name, the URL, and the associated tag names.
     * @param $campaignId
     * @return array of EventTemplate
     */
    public function getTemplatesForCampaign($campaignId){
        $sqlString = self::$GET_TEMPLATES_FOR_CAMPAIGN;
        $sqlBinding = "i";
        $sqlParams = array($campaignId);
        $templateArray = array();
        $response = MySqliHelper::getResultWithLogging("EventTemplateProvider.getTemplatesForCampaign error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        if ($response->success && count($response->resultArray)){
            foreach ($response->resultArray as $dataRow){
                $dataObject = (object)$dataRow;
                $record = new EventTemplate();
                $record->name = $dataObject->name;
                $record->url = $dataObject->url;
                $record->tagNamesArray = explode(",", $dataObject->commaDelimitedTagNames);
                $templateArray[] = $record;
            }
        }
        return $templateArray;
    }
	
	private static $GET_EVENT_TEMPLATE_SQL = "
		SELECT 
			et.*,
				(SELECT GROUP_CONCAT(TagList_Name ORDER BY ta.TagAssociation_SortOrder, t.TagList_Name)
				FROM tag_association ta
				JOIN tag_list t on ta.TagAssociation_TagListID = t.TagList_ID
				WHERE ta.TagAssociation_SourceID=et.EventTemplate_ID AND ta.TagAssociation_SourceTable='event_templates' AND ta.TagAssociation_Status IS NULL
				GROUP BY TagAssociation_SourceID) AS commaDelimitedTagNames,

            i.folder_url as calendarIconPath,
            i.file_name as calendarIconFile
		FROM event_templates et
        LEFT JOIN cms_image_library i ON EventTemplate_CalendarIconImageID = i.id AND i.status = 'ACTIVE'
		WHERE EventTemplate_URL=? ORDER BY EventTemplate_CreatedDate DESC";

	public function getTemplate($TemplateName) {
		$sqlString = self::$GET_EVENT_TEMPLATE_SQL;
		$sqlBindings = "s";
		$sqlParams = array($TemplateName);
		$result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result) == 0){
            return new stdClass(); // empty object
        }
        $datarow = (object)$result[0];
        $record = $this->mapToRecord($datarow,  true);
        $premiumProvider = new EventTemplatePremiumProvider($this->mysqli);
        $premiums = $premiumProvider->getPremiums($record->id);
        if (is_array($premiums) && count($premiums)){
            $record->attachPremiums($premiums);
        }

        return $record;
	}

    private static $GET_EVENT_TEMPLATE_BY_ID_SQL = "
		SELECT
			et.*,
				(SELECT GROUP_CONCAT(TagList_Name ORDER BY ta.TagAssociation_SortOrder, t.TagList_Name)
				FROM tag_association ta
				JOIN tag_list t on ta.TagAssociation_TagListID = t.TagList_ID
				WHERE ta.TagAssociation_SourceID=et.EventTemplate_ID AND ta.TagAssociation_SourceTable='event_templates' AND ta.TagAssociation_Status IS NULL
				GROUP BY TagAssociation_SourceID) AS commaDelimitedTagNames,

            i.folder_url as calendarIconPath,
            i.file_name as calendarIconFile
		FROM event_templates et
        LEFT JOIN cms_image_library i ON EventTemplate_CalendarIconImageID = i.id AND i.status = 'ACTIVE'
		WHERE EventTemplate_ID=? ORDER BY EventTemplate_CreatedDate DESC";

    public function getTemplateByID($templateID, $getReferences = true) {
        $sqlString = self::$GET_EVENT_TEMPLATE_BY_ID_SQL;
        $sqlBindings = "i";
        $sqlParams = array($templateID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result) == 0){
            return new stdClass(); // empty object
        }
        $datarow = (object)$result[0];
        return $this->mapToRecord($datarow,  $getReferences);
    }

	private function mapToCollection($query_results) {
		$collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToRecord($row);
        }
		return $collection;
	}

	private function mapToRecord($datarow, $getReferences = false) {
        $campaignRecords = $this->getTagsFor($datarow->EventTemplate_ID);
        $record = EventTemplate::build($datarow, $campaignRecords);

        if ($getReferences & $campaignRecords && count($campaignRecords)){
            // we attach these to the template
            $tagReferences = array();
            foreach($campaignRecords as $campaignRecord){
                $references = $this->getTagReferences($campaignRecord->id, true, CampaignResourceProvider::EVENT_TEMPLATE_PAGE);
                foreach ($references as $refRecord){
                    $tagReferences[] = $refRecord;
                }
            }
            if (count($tagReferences)){
                $record->attachTagReferences($tagReferences);
            }
        }

		return $record;
	}

    public function getTagReferences($tagID, $activeOnly = true, $page = ''){
        $campaignResourceProvider = new CampaignResourceProvider($this->mysqli);
        return $campaignResourceProvider->getTagReferences($tagID, $activeOnly, $page);
    }

    private static $CLONE_TEMPLATE_SQL = "
        INSERT INTO event_templates
        (
            EventTemplate_Name,
            EventTemplate_URL,
            EventTemplate_PageHeader,
            EventTemplate_PageSubtitle,
            EventTemplate_Instructions,
            EventTemplate_InstructionsMarkdown,
            EventTemplate_InstructionsHtml,
            EventTemplate_EventName,
            EventTemplate_EventStartDate,
            EventTemplate_EventEndDate,
            EventTemplate_EventStartTime,
            EventTemplate_EventEndTime,
            EventTemplate_EventDescription,
            EventTemplate_EventPercentageID,
            EventTemplate_CalendarIconImageID,
            EventTemplate_CreatedByAdminID
        )
        SELECT
            CONCAT('Copy of ', EventTemplate_Name, ' - ', CURRENT_TIMESTAMP),
            CONCAT('copy_of_', EventTemplate_URL, '_', CURRENT_TIMESTAMP),
            EventTemplate_PageHeader,
            EventTemplate_PageSubtitle,
            EventTemplate_Instructions,
            EventTemplate_InstructionsMarkdown,
            EventTemplate_InstructionsHtml,
            EventTemplate_EventName,
            EventTemplate_EventStartDate,
            EventTemplate_EventEndDate,
            EventTemplate_EventStartTime,
            EventTemplate_EventEndTime,
            EventTemplate_EventDescription,
            EventTemplate_EventPercentageID,
            EventTemplate_CalendarIconImageID,
            ?
        FROM event_templates
        WHERE EventTemplate_ID = ?";

    public function cloneTemplate(EventTemplateCloneCriteria $criteria){
        $sqlString = self::$CLONE_TEMPLATE_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            getAdminId(),
            $criteria->record->id
        );
        $executeResponse = MySqliHelper::executeWithLogging("EventTemplateProvider.cloneTemplate error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);

        if ($executeResponse->success && $executeResponse->insertedId){
            $newRecord = new EventTemplate();
            $newRecord->id = $executeResponse->insertedId;
            if ($criteria->clonePremiums){
                $this->clonePremiums($criteria, $newRecord);
            }
            $executeResponse->record = $newRecord;
        }
        return $executeResponse;
    }

    private static $CLONE_TEMPLATE_PREMIUMS_SQL = "
        INSERT INTO event_template_premiums
        (
            event_template_id,
            status,
            name,
            price,
            sort_order,
            display_text,
            display_text_markdown,
            is_default
        )
        SELECT
            ?,
            status,
            name,
            price,
            sort_order,
            display_text,
            display_text_markdown,
            is_default
        FROM event_template_premiums
        WHERE event_template_id = ?
        AND status = 'ACTIVE'";

    private function clonePremiums(EventTemplateCloneCriteria $criteria, EventTemplate $newRecord){
        $sqlString = self::$CLONE_TEMPLATE_PREMIUMS_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $newRecord->id,
            $criteria->record->id
        );
        return MySqliHelper::executeWithLogging("EventTemplateProvider.clonePremiums error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}

/**
 * Class TagProvider
 * @deprecated use CampaignProvider
 */
class TagProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $GET_BY_NAME_SQL = "SELECT * FROM tag_list WHERE TagList_Name = ?";

    /**
     * Business rule states that this query will return only one TagRecord, but it will be returned in an array.
     * @param $tagName
     * @return array TagRecord
     */
    public function getByName($tagName){
        $sqlString = self::$GET_BY_NAME_SQL;
        $sqlBinding = "s";
        $sqlParams = array(TagRecord::getQueryName($tagName));
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return EventTemplate::mapToTagCollection($result);
    }

    public function getAllTags() {
        $sqlString = "SELECT * FROM tag_list ORDER BY TagList_Name";
        $result_Events = MySqliHelper::get_result($this->mysqli, $sqlString);
        return EventTemplate::mapToTagCollection($result_Events);
    }
}
class EventTemplate extends EventTemplateBase{
    public $createdByAdminID;
    public $lastEditedByAdminID;

    /**
     * Given a standard object (expected to be rehydrated from json), convert to strongly typed EventTemplate
     * @param null $stdObject
     */
    public function __construct($stdObject = null){
        if ($stdObject != null){
            $this->id = $stdObject->id;
            $this->name = htmlspecialchars($stdObject->name);
            $this->pageHeader = htmlspecialchars($stdObject->pageHeader);
            $this->pageSubtitle = htmlspecialchars($stdObject->pageSubtitle);
            $this->instructions = htmlspecialchars($stdObject->instructions);
            $this->instructionsMarkdownSource = $stdObject->instructionsMarkdownSource;
            $this->instructionsHtmlSource = $stdObject->instructionsHtmlSource;
            $this->url = $stdObject->url;
            $this->eventName = htmlspecialchars($stdObject->eventName);
            $this->description = htmlspecialchars($stdObject->description);
            $this->startDate = $stdObject->startDate;
            $this->endDate = $stdObject->endDate;
            $this->startTime = $stdObject->startTime;
            $this->endTime = $stdObject->endTime;
            $this->percentageID = $stdObject->percentageID;

            if (isset($stdObject->tagNamesArray) && !is_array($stdObject->tagNamesArray) && strlen($stdObject->tagNamesArray)){
                $stdObject->tagNamesArray = explode(",", $stdObject->tagNamesArray);
            }

            if (isset($stdObject->tagNamesArray) && is_array($stdObject->tagNamesArray)){
                foreach($stdObject->tagNamesArray as $tag){
                    $this->tagNamesArray[] = $tag;
                }
            }
        }
    }

    public function attachTagReferences($tagReferences){
        $this->tagReferences = $tagReferences;
    }

    public function attachPremiums($premiums){
        $this->premiums = $premiums;
    }

    public static function build($datarow, $campaignRecords = null) {
        $record = new EventTemplate();
        $record->pageHeader = $datarow->EventTemplate_PageHeader;
        $record->pageSubtitle = $datarow->EventTemplate_PageSubtitle;
        $record->instructions = $datarow->EventTemplate_Instructions;
        $record->instructionsMarkdownSource = $datarow->EventTemplate_InstructionsMarkdown;
        $record->instructionsHtmlSource = $datarow->EventTemplate_InstructionsHtml;
        $record->id = $datarow->EventTemplate_ID;
        $record->name = $datarow->EventTemplate_Name;
        $record->url = $datarow->EventTemplate_URL;
        $record->eventName = $datarow->EventTemplate_EventName;
        $record->description = $datarow->EventTemplate_EventDescription;
        $record->startDate = $datarow->EventTemplate_EventStartDate;
        $record->startTime = $datarow->EventTemplate_EventStartTime;
        $record->endDate = $datarow->EventTemplate_EventEndDate;
        $record->endTime = $datarow->EventTemplate_EventEndTime;
        $record->percentageID = $datarow->EventTemplate_EventPercentageID;
        $record->calendarIconFile = $datarow->calendarIconFile;
        $record->calendarIconPath = $datarow->calendarIconPath;
        $record->createdByAdminID = $datarow->EventTemplate_CreatedByAdminID;
        $record->createdDate = $datarow->EventTemplate_CreatedDate;
        $record->lastEditedDate = $datarow->EventTemplate_LastEditedDate;
        $record->lastEditedByAdminID = $datarow->EventTemplate_LastEditedByAdminID;
        $record->tagsArray = $campaignRecords;
        if ($datarow->commaDelimitedTagNames){
            $record->tagNamesArray = explode(",", $datarow->commaDelimitedTagNames);
        } else {
            if ($campaignRecords && is_array($campaignRecords) && count($campaignRecords)){
                foreach ($campaignRecords as $campaignRecord){
                    /* @var $campaignRecord CampaignRecord */
                    $record->tagNamesArray[] = $campaignRecord->name;
                }
            }
        }
        $record->tagReferences = array();

        return $record;
    }

    public static function mapToTagCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = self::mapToTagRecord($row);
        }
        return $collection;
    }

    private static function mapToTagRecord($datarow) {
        $record = new TagRecord();
        $record->TagList_ID = $datarow->TagList_ID;
        $record->TagList_Name = $datarow->TagList_Name;
        $record->TagList_Description = $datarow->TagList_Description;
        $record->TagList_CampaignHeading = $datarow->TagList_CampaignHeading;
        $record->TagList_CampaignSubHeading = $datarow->TagList_CampaignSubHeading;
        $record->TagList_CampaignDescription = $datarow->TagList_CampaignDescription;
        $record->TagList_Image = $datarow->TagList_Image;
        $record->TagList_URL = $datarow->TagList_URL;
        $record->TagList_EventURLOverride = $datarow->TagList_EventURLOverride;
        $record->TagList_AddWrap = $datarow->TagList_AddWrap;
        $record->map_zoom_level = $datarow->TagList_MapZoomLevel;
        $record->map_center_lat = $datarow->TagList_MapCenterLat;
        $record->map_center_long = $datarow->TagList_MapCenterLong;

        return $record;
    }
}
class SaveEventTemplateResponse{
    public $record;
    public $message;
    public $success = false;
}
?>