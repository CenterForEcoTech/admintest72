<?php
// hacky security
if (!isAdmin()){
    trigger_error("AdminEventProvider accessed without being logged in as admin.", E_USER_ERROR);
    exit;
}
include_once("EventRecordProvider.php");
include_once("MySqliEventManager.php");

class AdminEventProvider extends EventRecordProvider{

    public function get(EventCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = $this->getSelectColumnString();
        $fromTableClause = $this->getFromTableClause();
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $this->applyFilter($criteria, $filterString, $filterBinding, $filterParams);

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        $rowObject = (object)$row;
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $eventRecord = self::mapToRecord($rowObject);
                        $eventRecord->isDraft = $rowObject->isDraft ? true : false;
                        $eventRecord->status = $rowObject->status;
                        $eventRecord->eventTemplateId = $rowObject->eventTemplateId;
                        $result->collection[] = $eventRecord;
                    }
                } else {
                    trigger_error("AdminEventProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("AdminEventProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    protected static $GET_SELECT_COLUMNS = "
        SELECT
            0 as isDraft,
            e.Event_EventTemplateID as eventTemplateId,
			e.Event_ID as id,
			e.Event_Status as status,
			e.Event_Title as title,
			e.Event_Description as description,
			e.Event_Date as startDate,
			e.Event_StartTime as startTime,
			e.Event_EndDate as endDate,
			e.Event_EndTime as endTime,
			e.Event_CustomAttendLink as customAttendLink,
			e.Event_IsNational as isNationalEvent,
			e.Event_IsFcOnly as isFcOnly,
			e.Event_IsCausetownEvent as isCausetownEvent,
			e.Event_IsOnline as isOnlineEvent,
			e.Event_OnlineLocationLabel as onlineLocationLabel,
			e.Event_OnlineLocationLink as onlineLocationLink,
			e.Event_SuppressCharitySelectionForm as suppressCharitySelectionForm,
			e.Event_SuppressPrintFlier as suppressPrintFlier,
			ti.folder_url as calendarIconPath,
			ti.file_name as calendarIconFile,

			/* venue (merchant) info (for merchant created events only) */
			1 as isOwnedByMerchant,
			e.Event_EventBriteEventID as eventBriteEventId,
			e.Event_FacebookEventID as facebookEventId,
			e.Event_MerchantPercentage as featuredCausePercentage,
			e.Event_MerchantSecondaryPercentage as customerChoicePercentage,
			e.Event_IsFlatDonation as isFlatDonation,
			e.Event_DonationPerUnit as donationPerUnit,
			e.Event_UnitDescriptor as unitDescriptor,
			e.Event_FlatDonationActionPhrase as flatDonationActionPhrase,
			e.Event_EventTemplatePremiumId as eventTemplatePremiumId,
			m.MerchantInfo_ID as venueId,
			m.MerchantInfo_Name as venueName,
			m.MerchantInfo_Website as venueUrl,
			m.MerchantInfo_Description as venueDescription,
			m.MerchantInfo_Lat as venueLat,
			m.MerchantInfo_Long as venueLong,
			m.MerchantInfo_Address as venueAddress,
			m.MerchantInfo_City as venueCity,
			m.MerchantInfo_State as venueState,
			m.MerchantInfo_Zip as venueZip,
			m.MerchantInfo_Country as venueCountry,
			m.MerchantInfo_Phone as venuePhone,
			m.MerchantInfo_EventBriteVenueID as venueEventBriteId,
			m.MerchantInfo_Neighborhood as venueNeighborhood,
			m.MerchantInfo_Keywords as venueKeywords,

			/* nonprofit info (for nonprofit create events only) */
			o.OrganizationInfo_ID as causeId,
			o.OrganizationInfo_Name as causeName,
			o.OrganizationInfo_Website as causeUrl,
			o.OrganizationInfo_EIN as causeEin,
			e.Event_CreatedDate as createdDate,
			(CASE e.Event_LastEditedDate WHEN 0 THEN e.Event_CreatedDate ELSE e.Event_LastEditedDate END) as updatedDate,
			(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as venuePid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as causePid,
            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_EventTemplateID
                AND TagAssociation_SourceTable = 'event_templates'
                AND TagAssociation_Status IS NULL) as templateTagNames";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_info e
        JOIN merchant_info m ON e.Event_ProposedByMerchantID = m.MerchantInfo_ID
        LEFT JOIN organization_info o ON e.Event_ProposedByOrgID = o.OrganizationInfo_ID
        LEFT JOIN event_templates et ON e.Event_EventTemplateID = et.EventTemplate_ID
        LEFT JOIN cms_image_library ti ON et.EventTemplate_CalendarIconImageID = ti.id AND ti.status = 'ACTIVE'
        WHERE 1 = 1";

    private static $FILTER_ON_MERCHANT_EVENT = "
        AND e.Event_ProposedByMerchantID > 0";

    private static $FILTER_ON_ID = "
        AND e.Event_ID = ?";

    private static $FILTER_ON_UPCOMING_ONLY = "
        AND e.Event_EndDate >= CURRENT_DATE";

    private static $FILTER_ON_PUBLISHED_ONLY = "
        AND e.Event_Status = 'ACTIVE'";

    private static $FILTER_ON_MERCHANT = "
        AND e.Event_ProposedByMerchantID = ?";

    private static $FILTER_ON_ORG = "
        AND e.Event_ProposedByOrgID = ?";

    private static $FILTER_ON_TAGNAME = "
       AND tagNames REGEXP ?";

    private static $FILTER_ON_IS_ONLINE = "
        AND e.Event_IsOnline = 1";

    private static $FILTER_ON_NOT_IS_ONLINE = "
        AND e.Event_IsOnline = 0";

    private static $FILTER_ON_EVENT_TEMPLATE = "
        AND e.Event_EventTemplateID = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            e.Event_ID LIKE ?
            OR e.Event_Title LIKE ?
            OR m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_Keywords LIKE ?
            OR m.MerchantInfo_Neighborhood LIKE ?
            OR o.OrganizationInfo_Name LIKE ?
            OR EXISTS (
                SELECT 1
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL
                AND TagList_Name LIKE ?
            )
        )";

    protected function getSelectColumnString(){
        return self::$GET_SELECT_COLUMNS;
    }

    protected function getFromTableClause(){
        return self::$FROM_TABLE_CLAUSE;
    }

    protected function applyFilter(AdminEventCriteria $criteria, &$filterString, &$filterBinding, &$filterParams){
        // apply search
        if ($criteria->isMerchantEvent){
            $filterString .= self::$FILTER_ON_MERCHANT_EVENT;
        }
        if ($criteria->eventId && $criteria->eventId > 0){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->eventId;
        } else {
            if ($criteria->upcomingOnly){
                $filterString .= self::$FILTER_ON_UPCOMING_ONLY;
            }

            if ($criteria->publishedOnly){
                $filterString .= self::$FILTER_ON_PUBLISHED_ONLY;
            }

            if ($criteria->onlineOnly > 0){
                $filterString .= self::$FILTER_ON_IS_ONLINE;
            } else if ($criteria->onlineOnly < 0){
                $filterString .= self::$FILTER_ON_NOT_IS_ONLINE;
            }

            if ($criteria->merchantId && is_numeric($criteria->merchantId)){
                $filterString .= self::$FILTER_ON_MERCHANT;
                $filterBinding .= "i";
                $filterParams[] = $criteria->merchantId;
            }

            if ($criteria->orgId && is_numeric($criteria->orgId)){
                $filterString .= self::$FILTER_ON_ORG;
                $filterBinding .= "i";
                $filterParams[] = $criteria->orgId;
            }

            if ($criteria->eventTemplateId && is_numeric($criteria->eventTemplateId)){
                $filterString .= self::$FILTER_ON_EVENT_TEMPLATE;
                $filterBinding .= "i";
                $filterParams[] = $criteria->eventTemplateId;
            }

            // tagname
            if ($criteria->tagName){
                $filterString.= self::$FILTER_ON_TAGNAME;
                $filterBinding .= "s";
                $filterParams[] = "(^|,)".$criteria->tagName."(,|$)";
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.Event_Date desc, e.Event_StartTime desc";

    protected function getOrderByClause(EventCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "e.Event_ID ".$sortOrder;
                        break;
                    case "name":
                        $orderByStrings[] = "e.Event_Title ".$sortOrder;
                        break;
                    case "dates":
                        $orderByStrings[] = "e.Event_Date ".$sortOrder.", e.Event_StartTime ".$sortOrder;
                        break;
                    case "merchant":
                        $orderByStrings[] = "m.MerchantInfo_Name ".$sortOrder;
                        break;
                    case "cause":
                        $orderByStrings[] = "o.OrganizationInfo_Name ".$sortOrder;
                        break;
                    case "campaigns":
                        $orderByStrings[] =
                            "(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                                FROM tag_association
                                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                                WHERE TagAssociation_SourceID = Event_ID
                                AND TagAssociation_SourceTable = 'event_info'
                                AND TagAssociation_Status IS NULL) ".$sortOrder;
                        break;
                    case "isOnline":
                        $orderByStrings[] = "e.Event_IsOnline ".$sortOrder;
                        break;
                    case "isNational":
                        $orderByStrings[] = "e.Event_IsNational ".$sortOrder;
                        break;
                    case "isCtEvent":
                        $orderByStrings[] = "e.Event_IsCausetownEvent ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
}

class AdminEventDraftProvider extends AdminEventProvider{
    protected static $GET_SELECT_COLUMNS = "
        SELECT
            1 as isDraft,
            e.EventDraft_EventTemplateID as eventTemplateId,
			e.EventDraft_ID as id,
			e.EventDraft_Status as status,
			e.EventDraft_Title as title,
			e.EventDraft_Description as description,
			e.EventDraft_StartDate as startDate,
			e.EventDraft_StartTime as startTime,
			e.EventDraft_EndDate as endDate,
			e.EventDraft_EndTime as endTime,
			e.EventDraft_CustomAttendLink as customAttendLink,
			e.EventDraft_IsNational as isNationalEvent,
			e.EventDraft_IsFcOnly as isFcOnly,
			e.EventDraft_IsCausetownEvent as isCausetownEvent,
			e.EventDraft_IsOnline as isOnlineEvent,
			e.EventDraft_OnlineLocationLabel as onlineLocationLabel,
			e.EventDraft_OnlineLocationLink as onlineLocationLink,
			e.EventDraft_SuppressCharitySelectionForm as suppressCharitySelectionForm,
			e.EventDraft_SuppressPrintFlier as suppressPrintFlier,

			/* venue (merchant) info (for merchant created events only) */
			1 as isOwnedByMerchant,
			e.EventDraft_FeaturedCausePercentage as featuredCausePercentage,
			e.EventDraft_CustomerChoicePercentage as customerChoicePercentage,
			e.EventDraft_IsFlatDonation as isFlatDonation,
			e.EventDraft_DonationPerUnit as donationPerUnit,
			e.EventDraft_UnitDescriptor as unitDescriptor,
			e.EventDraft_FlatDonationActionPhrase as flatDonationActionPhrase,
			e.EventDraft_EventTemplatePremiumId as eventTemplatePremiumId,
			m.MerchantInfo_ID as venueId,
			m.MerchantInfo_Name as venueName,
			m.MerchantInfo_Website as venueUrl,
			m.MerchantInfo_Description as venueDescription,
			m.MerchantInfo_Lat as venueLat,
			m.MerchantInfo_Long as venueLong,
			m.MerchantInfo_Address as venueAddress,
			m.MerchantInfo_City as venueCity,
			m.MerchantInfo_State as venueState,
			m.MerchantInfo_Zip as venueZip,
			m.MerchantInfo_Country as venueCountry,
			m.MerchantInfo_Phone as venuePhone,
			m.MerchantInfo_EventBriteVenueID as venueEventBriteId,
			m.MerchantInfo_Neighborhood as venueNeighborhood,
			m.MerchantInfo_Keywords as venueKeywords,

			/* nonprofit info (for nonprofit create events only) */
			o.OrganizationInfo_ID as causeId,
			o.OrganizationInfo_Name as causeName,
			o.OrganizationInfo_Website as causeUrl,
			o.OrganizationInfo_EIN as causeEin,
			e.EventDraft_CreatedDate as createdDate,
			(CASE e.EventDraft_LastEditedDate WHEN 0 THEN e.EventDraft_CreatedDate ELSE e.EventDraft_LastEditedDate END) as updatedDate,
			(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = EventDraft_EventTemplateID
                AND TagAssociation_SourceTable = 'event_templates'
                AND TagAssociation_Status IS NULL) as tagNames,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as venuePid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as causePid";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_draft e
        JOIN merchant_info m ON e.EventDraft_ProposedByMerchantID = m.MerchantInfo_ID
        LEFT JOIN organization_info o ON e.EventDraft_ProposedByOrgID = o.OrganizationInfo_ID
        WHERE e.EventDraft_EventID = 0";

    private static $FILTER_ON_ID = "
        AND e.EventDraft_ID = ?";

    private static $FILTER_ON_UPCOMING_ONLY = "
        AND e.EventDraft_EndDate >= CURRENT_DATE";

    private static $FILTER_ON_PUBLISHED_ONLY = "
        AND e.EventDraft_Status = 'ACTIVE'";

    private static $FILTER_ON_IS_ONLINE = "
        AND e.EventDraft_IsOnline = 1";

    private static $FILTER_ON_NOT_IS_ONLINE = "
        AND e.EventDraft_IsOnline = 0";

    private static $FILTER_ON_MERCHANT = "
        AND e.EventDraft_ProposedByMerchantID = ?";

    private static $FILTER_ON_ORG = "
        AND e.EventDraft_ProposedByOrgID = ?";

    private static $FILTER_ON_TAGNAME = "
       AND tagNames REGEXP ?";

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.EventDraft_StartDate desc, e.EventDraft_StartTime desc";

    private static $FILTER_ON_KEYWORD = "
        AND (
            e.EventDraft_ID LIKE ?
            OR e.EventDraft_Title LIKE ?
            OR m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_Keywords LIKE ?
            OR m.MerchantInfo_Neighborhood LIKE ?
            OR o.OrganizationInfo_Name LIKE ?
            OR EXISTS (
                SELECT 1
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = EventDraft_EventTemplateID
                AND TagAssociation_SourceTable = 'event_templates'
                AND TagAssociation_Status IS NULL
                AND TagList_Name LIKE ?
            )
        )";

    protected function getSelectColumnString(){
        return self::$GET_SELECT_COLUMNS;
    }

    protected function getFromTableClause(){
        return self::$FROM_TABLE_CLAUSE;
    }

    protected function applyFilter(EventCriteria $criteria, &$filterString, &$filterBinding, &$filterParams){
        // apply search
        if ($criteria->eventId && $criteria->eventId > 0){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->eventId;
        } else {
            if ($criteria->upcomingOnly){
                $filterString .= self::$FILTER_ON_UPCOMING_ONLY;
            }

            if ($criteria->publishedOnly){
                $filterString .= self::$FILTER_ON_PUBLISHED_ONLY;
            }

            if ($criteria->onlineOnly > 0){
                $filterString .= self::$FILTER_ON_IS_ONLINE;
            } else if ($criteria->onlineOnly < 0){
                $filterString .= self::$FILTER_ON_NOT_IS_ONLINE;
            }
            if ($criteria->merchantId && is_numeric($criteria->merchantId)){
                $filterString .= self::$FILTER_ON_MERCHANT;
                $filterBinding .= "i";
                $filterParams[] = $criteria->merchantId;
            }

            if ($criteria->orgId && is_numeric($criteria->orgId)){
                $filterString .= self::$FILTER_ON_ORG;
                $filterBinding .= "i";
                $filterParams[] = $criteria->orgId;
            }

            // tagname
            if ($criteria->tagName){
                $filterString.= self::$FILTER_ON_TAGNAME;
                $filterBinding .= "s";
                $filterParams[] = "(^|,)".$criteria->tagName."(,|$)";
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }
    }

    protected function getOrderByClause(EventCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "e.EventDraft_ID ".$sortOrder;
                        break;
                    case "name":
                        $orderByStrings[] = "e.EventDraft_Title ".$sortOrder;
                        break;
                    case "dates":
                        $orderByStrings[] = "e.EventDraft_StartDate ".$sortOrder.", e.EventDraft_StartTime ".$sortOrder;
                        break;
                    case "merchant":
                        $orderByStrings[] = "m.MerchantInfo_Name ".$sortOrder;
                        break;
                    case "cause":
                        $orderByStrings[] = "o.OrganizationInfo_Name ".$sortOrder;
                        break;
                    case "campaigns":
                        $orderByStrings[] =
                            "(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                                FROM tag_association
                                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                                WHERE TagAssociation_SourceID = EventDraft_ID
                                AND TagAssociation_SourceTable = 'event_info'
                                AND TagAssociation_Status IS NULL) ".$sortOrder;
                        break;
                    case "isOnline":
                        $orderByStrings[] = "e.EventDraft_IsOnline ".$sortOrder;
                        break;
                    case "isNational":
                        $orderByStrings[] = "e.EventDraft_IsNational ".$sortOrder;
                        break;
                    case "isCtEvent":
                        $orderByStrings[] = "e.EventDraft_IsCausetownEvent ".$sortOrder;
                        break;
                }
            }
            $orderBy = count($orderByStrings) ? " ORDER BY ".implode(", ", $orderByStrings) : "";
        }
        return $orderBy;
    }
}