<?php
include_once("Helper.php");

/**
 * AdminProvider's business is to manage administrative functions.
 */
class AdminProvider {

	private static $ADMIN_LOOKUP_SQL = "SELECT * FROM admin_users";
	
	private $mysqli;
	
	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

	public function ErrorLog($Page, $Message){
        $response = DbAdminLogger::log_message($this->mysqli, $Page, $Message);
	}
	
	public function getAllAdmins() {
        $result_Events = MySqliHelper::get_result($this->mysqli, self::$ADMIN_LOOKUP_SQL);
		return $this->mapToAdminCollection($result_Events);
	}

	private function mapToAdminCollection($query_results) {
		$collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToAdminRecord($row);
        }
		return $collection;
	}
	
	private function mapToAdminRecord($datarow) {
					
	
		$record = new stdClass();
		$record->AdminUsers_ID = $datarow->AdminUsers_ID;
		$record->AdminUsers_UserName = $datarow->AdminUsers_UserName;
		$record->AdminUsers_UserEmail = $datarow->AdminUsers_UserEmail;
		$record->AdminUsers_FirstName = $datarow->AdminUsers_FirstName;
		$record->AdminUsers_LastName = $datarow->AdminUsers_LastName;
		$record->AdminUsers_Name = $datarow->AdminUsers_FirstName." ".$datarow->AdminUsers_LastName;
		$record->AdminUsers_SecurityGroupID = $datarow->AdminUsers_SecurityGroupID;
		$record->AdminUsers_Status = $datarow->AdminUsers_Status;
		$record->AdminUsers_AddedTimeStamp = $datarow->AdminUsers_AddedTimeStamp;
		$record->endTime = $datarow->EventTemplate_EventEndTime;
		$record->AdminUsers_AddedByAdminID = $datarow->AdminUsers_AddedByAdminID;
		return $record;
	}
	
    private static $SELECT_CRONJOBS_COLUMNS = "
        SELECT
			CronJob_ID as id,
			CronJob_Title as title,
			CronJob_Recipients as recipients,
			CronJob_StartDate as startDate,
			CronJob_Schedule as schedule,
			CronJob_Message as message,
			CronJob_LastRun as lastRun,
			CronJob_LastRunStatus as lastRunStatus,
			CronJob_Active as active";

    private static $FROM_TABLE_CLAUSE = "
        FROM cronjobs WHERE 1 = 1";

    public function getCronJobs($criteria){
		$sqlString = self::$SELECT_CRONJOBS_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
		$filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY CronJob_Title";

		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("AdminProvider.getCronJobs failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	private static $UPDATE_CRONJOBS_SQL = "
        UPDATE	cronjobs
        SET		CronJob_LastRun = ?, CronJob_LastRunStatus = ?
		WHERE	CronJob_ID = ?";

    public function updateCronJobs($id,$mail){
        $sqlString = self::$UPDATE_CRONJOBS_SQL;
        $sqlBinding = "ssi";
		$now = date("Y-m-d H:i:s");
		$mailMessage = print_r($mail,true);
        $sqlParams = array($now,$mailMessage,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("AdminProvider.updateCronJobs failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }

	private static $INSERT_SQL_PAGETRACKER ="
		INSERT INTO pagetracker 
			(page, user) 
			VALUES 
			(?,?)";
			
    public function pageTracker($page,$user){
        $sqlString = self::$INSERT_SQL_PAGETRACKER;
        $sqlBinding = "ss";
        $sqlParam = array($page, $user);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if (!$insertResponse->success){
            trigger_error("MuniProvider.addAudit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
        //return $record;
    }


	
}
?>