<?php

/**
 * The main purposes of this provider is to do the core select on events and create EventRecord objects. This is not a provider
 * that manages CRUD operations.
 */
class EventRecordProvider implements eventAPI{

    /**
     * @var mysqli
     */
    protected $mysqli;

    /**
     * @var LocationProvider
     */
    private $geoLookupProvider;

    public function __construct(mysqli $mysqli, LocationProvider $geoLookupProvider = null){
        $this->mysqli = $mysqli;
        $this->geoLookupProvider = $geoLookupProvider;
    }

    /**
     * Implement Interface
     * This is for the RSS which lists items in descending chron order.
     * @see repository/eventAPI#getAll()
     */
    public function getAll() {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = true;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $criteria->multiColumnSort[] = new MultiColumnSort("timestamp", "desc");
        $paginatedResults = $this->get($criteria);
        return $paginatedResults->collection;
    }

    /**
     * Implement Interface
     * Returns paginated results based on start index and max records per page.
     * Default sort on date and time = 'ASC' and only 'ASC' or 'DESC' is valid.
     * @see repository/eventAPI#getNext($maxRecords)
     * @param $startIndex
     * @param int $maxRecords
     * @param string $dateSort
     * @return array|\PaginatedResult
     */
    public function getNext($startIndex, $maxRecords, $dateSort = "ASC"){
        if (!is_numeric($startIndex) || $startIndex < 0){
            $startIndex = 0;
        }
        if (!is_numeric($maxRecords) || $maxRecords <= 0){
            $maxRecords = 5;
        }
        if (strtolower($dateSort) != "desc"){
            $dateSort = "ASC";
        }

        $criteria = new EventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->publishedOnly = true;
        $criteria->start = $startIndex;
        $criteria->size = $maxRecords;
        $criteria->multiColumnSort[] = new MultiColumnSort("startDate", $dateSort);
        $criteria->multiColumnSort[] = new MultiColumnSort("startTime", $dateSort);
        $paginatedResults = $this->get($criteria);
        return $paginatedResults;
    }

    protected static $GET_SELECT_COLUMNS = "
        SELECT
			e.Event_ID as id,
			e.Event_Title as title,
			e.Event_Description as description,
			e.Event_Date as startDate,
			e.Event_StartTime as startTime,
			e.Event_EndDate as endDate,
			e.Event_EndTime as endTime,
			e.Event_CustomAttendLink as customAttendLink,
			e.Event_IsNational as isNationalEvent,
			e.Event_IsFcOnly as isFcOnly,
			e.Event_IsCausetownEvent as isCausetownEvent,
			e.Event_IsOnline as isOnlineEvent,
			e.Event_OnlineLocationLabel as onlineLocationLabel,
			e.Event_OnlineLocationLink as onlineLocationLink,
			e.Event_SuppressCharitySelectionForm as suppressCharitySelectionForm,
			e.Event_SuppressPrintFlier as suppressPrintFlier,

			/* venue (merchant) info (for merchant created events only) */
			active_events.owned_by_merchant as isOwnedByMerchant,
			active_events.event_brite_event_id as eventBriteEventId,
			active_events.facebook_event_id as facebookEventId,
			active_events.featured_cause_percentage as featuredCausePercentage,
			active_events.customer_choice_percentage as customerChoicePercentage,
			active_events.isFlatDonation as isFlatDonation,
			active_events.donationPerUnit as donationPerUnit,
			active_events.unitDescriptor as unitDescriptor,
			active_events.flatDonationActionPhrase as flatDonationActionPhrase,
			e.Event_EventTemplatePremiumId as eventTemplatePremiumId,
			ti.folder_url as calendarIconPath,
			ti.file_name as calendarIconFile,
			m.MerchantInfo_ID as venueId,
			m.MerchantInfo_Name as venueName,
			m.MerchantInfo_Website as venueUrl,
			i.folder_url as logoPath,
			i.file_name as logoFile,
			m.MerchantInfo_Description as venueDescription,
			m.MerchantInfo_Lat as venueLat,
			m.MerchantInfo_Long as venueLong,
			m.MerchantInfo_Address as venueAddress,
			m.MerchantInfo_City as venueCity,
			m.MerchantInfo_State as venueState,
			m.MerchantInfo_Zip as venueZip,
			m.MerchantInfo_Country as venueCountry,
			m.MerchantInfo_Phone as venuePhone,
			m.MerchantInfo_EventBriteVenueID as venueEventBriteId,
			m.MerchantInfo_Neighborhood as venueNeighborhood,
			m.MerchantInfo_Keywords as venueKeywords,

			/* nonprofit info (for nonprofit create events only) */
			o.OrganizationInfo_ID as causeId,
			o.OrganizationInfo_Name as causeName,
			o.OrganizationInfo_Website as causeUrl,
			o.OrganizationInfo_EIN as causeEin,
			active_events.Event_CreatedDate as createdDate,
			(CASE active_events.Event_LastEditedDate WHEN 0 THEN active_events.Event_CreatedDate ELSE active_events.Event_LastEditedDate END) as updatedDate,
			active_events.tagNames,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as venuePid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as causePid,
            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_EventTemplateID
                AND TagAssociation_SourceTable = 'event_templates'
                AND TagAssociation_Status IS NULL) as templateTagNames";

    private static $FROM_TABLE_CLAUSE = "
        FROM
        (
        SELECT
            ev.Event_ID AS event_id,
            if (ev.Event_ProposedByMerchantID, 1, 0) AS owned_by_merchant,
            if(mp.MerchantInfo_ID, mp.MerchantInfo_ID, mm.MerchantInfo_ID) AS merchant_id,
            if(mp.MerchantInfo_ID, ev.Event_EventBriteEventID, em.EventMatch_EventBriteEventID) AS event_brite_event_id,
            if(mp.MerchantInfo_ID, ev.Event_FacebookEventID, em.EventMatch_FacebookEventID) AS facebook_event_id,
            if(mp.MerchantInfo_ID, ev.Event_MerchantPercentage, em.EventMatch_MerchantPercentage) AS featured_cause_percentage,
            if(mp.MerchantInfo_ID, ev.Event_MerchantSecondaryPercentage, em.EventMatch_MerchantSecondaryPercentage) AS customer_choice_percentage,
            if(mp.MerchantInfo_ID, ev.Event_IsFlatDonation, em.EventMatch_IsFlatDonation) AS isFlatDonation,
            if(mp.MerchantInfo_ID, ev.Event_DonationPerUnit, em.EventMatch_DonationPerUnit) AS donationPerUnit,
            if(mp.MerchantInfo_ID, ev.Event_UnitDescriptor, em.EventMatch_UnitDescriptor) AS unitDescriptor,
            if(mp.MerchantInfo_ID, ev.Event_FlatDonationActionPhrase, em.EventMatch_FlatDonationActionPhrase) AS flatDonationActionPhrase,
            if (op.OrganizationInfo_ID, op.OrganizationInfo_ID, 0) AS org_id,
            ev.Event_CreatedDate,
            ev.Event_LastEditedDate,
            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames
            FROM event_info ev
            LEFT JOIN merchant_info mp ON ev.Event_ProposedByMerchantID = mp.MerchantInfo_ID AND ev.Event_ProposedByMerchantID > 0
            LEFT JOIN organization_info op ON ev.Event_ProposedByOrgID = op.OrganizationInfo_ID
            LEFT JOIN event_match em ON em.EventMatch_EventID = ev.Event_ID AND ev.Event_ProposedByOrgID > 0 AND EventMatch_Status = 'Accepted'
            LEFT JOIN merchant_info mm ON em.EventMatch_MerchantID = mm.MerchantInfo_ID
            WHERE ev.Event_Status = 'ACTIVE'
            AND (0 = ? OR ev.Event_ID = ?)
            AND (0 = ? OR mm.MerchantInfo_ID > 0 OR mp.MerchantInfo_ID > 0)
            AND ( 0 = ? OR ev.Event_EndDate >= CURRENT_DATE)
        ) AS active_events
        JOIN event_info e ON active_events.event_id = e.Event_ID
        LEFT JOIN merchant_info m ON active_events.merchant_id = m.MerchantInfo_ID
        LEFT JOIN cms_image_library i ON m.MerchantInfo_ImageLibraryID = i.id AND i.status = 'ACTIVE'
        LEFT JOIN event_templates et ON e.Event_EventTemplateID = et.EventTemplate_ID
        LEFT JOIN cms_image_library ti ON et.EventTemplate_CalendarIconImageID = ti.id AND ti.status = 'ACTIVE'
        LEFT JOIN organization_info o ON active_events.org_id = o.OrganizationInfo_ID
        WHERE 1 = 1";

    private static $BASIC_QUERY_BINDING = "iiii";

    private static $FILTER_ON_MERCHANT = "
        AND m.MerchantInfo_ID = ?";

    private static $FILTER_ON_ORG = "
        AND o.OrganizationInfo_ID = ?";

    private static $FILTER_ON_ID = "
        AND e.Event_ID = ?";

    private static $FILTER_ON_TAGNAME = "
       AND tagNames REGEXP ?";

    private static $FILTER_ON_IS_ONLINE = "
        AND e.Event_IsOnline = 1";

    public function get(EventCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$GET_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = self::$BASIC_QUERY_BINDING;
        $upcomingOnly = ($criteria->upcomingOnly) ? 1 : 0;
        $publishedOnly = ($criteria->publishedOnly) ? 1 : 0;
        $eventSpecific = (is_numeric($criteria->eventId) && $criteria->eventId > 0) ? 1 : 0;
        $eventId = $eventSpecific ? $criteria->eventId : 0;
        $filterParams = array(
            $eventSpecific,
            $eventId,
            $publishedOnly,
            $upcomingOnly
        );
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->merchantId && is_numeric($criteria->merchantId)){
            $filterString .= self::$FILTER_ON_MERCHANT;
            $filterBinding .= "i";
            $filterParams[] = $criteria->merchantId;
        }

        if ($criteria->orgId && is_numeric($criteria->orgId)){
            $filterString .= self::$FILTER_ON_ORG;
            $filterBinding .= "i";
            $filterParams[] = $criteria->orgId;
        }

        if ($criteria->eventId && is_numeric($criteria->eventId)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->eventId;
        }

        // tagname
        if ($criteria->tagName){
            $filterString.= self::$FILTER_ON_TAGNAME;
            $filterBinding .= "s";
            $filterParams[] = "(^|,)".$criteria->tagName."(,|$)";
        }

        if ($criteria->onlineOnly){
            $filterString .= self::$FILTER_ON_IS_ONLINE;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("EventRecordProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventRecordProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.Event_Date desc, e.Event_StartTime desc";

    private function getOrderByClause(EventByLocationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "active":
                        $orderByStrings[] = "e.Event_EndDate > CURRENT_DATE ".$sortOrder;
                        break;
                    case "timestamp":
                        $orderByStrings[] = "e.Event_ID ".$sortOrder;
                        break;
                    case "startDate":
                        $orderByStrings[] = "e.Event_Date ".$sortOrder;
                        break;
                    case "startTime":
                        $orderByStrings[] = "e.Event_StartTime ".$sortOrder;
                        break;
                    case "endDate":
                        $orderByStrings[] = "e.Event_EndDate ".$sortOrder;
                        break;
                    case "endTime":
                        $orderByStrings[] = "e.Event_EndTime ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private function mapToCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToRecord($row);
        }
        return $collection;
    }

    protected function mapToRecord($dataRow) {
        $record = new EventRecord();
        $record->isOwnedByMerchant = $dataRow->isOwnedByMerchant;
        $record->id = $dataRow->id;
        $record->title = $dataRow->title;
        $record->description = $dataRow->description;
        $record->createdDate = strtotime($dataRow->createdDate);
        $record->lastUpdatedDate = strtotime($dataRow->updatedDate);
        $record->setStartDate($dataRow->startDate, $dataRow->startTime);
        $record->setEndDate($dataRow->endDate, $dataRow->endTime);
        $record->isMultiDay = $dataRow->startDate != $dataRow->endDate;
        $record->commaDelimitedTagNames = $dataRow->tagNames;
        $record->customAttendLink = $dataRow->customAttendLink;
        $record->isNationalEvent = $dataRow->isNationalEvent;
        $record->isCausetownEvent = $dataRow->isCausetownEvent;
        $record->isOnlineEvent = $dataRow->isOnlineEvent;
        $record->onlineLocationLabel = $dataRow->onlineLocationLabel;
        $record->onlineLocationLink = $dataRow->onlineLocationLink;
        $record->suppressCharitySelectionForm = $dataRow->suppressCharitySelectionForm;
        $record->suppressPrintFlier = $dataRow->suppressPrintFlier;
        $record->calendarIconFile = $dataRow->calendarIconFile;
        $record->calendarIconPath = $dataRow->calendarIconPath;
        $record->templateTagNames = $dataRow->templateTagNames;

        $record->venue = $this->mapToVenue($dataRow);

        if (is_numeric($dataRow->causeId)){
            $record->cause = $this->mapToCause($dataRow);
        }
        return $record;
    }

    private function mapToCause($dataRow){
        $cause = new Cause();
        $cause->id = $dataRow->causeId;
        $cause->name = $dataRow->causeName;
        $cause->ein = $dataRow->causeEin;
        $cause->setURL($dataRow->causeUrl);
        $cause->pid = $dataRow->causePid;
        return $cause;
    }

    private function mapToVenue($dataRow){
        $venue = new Venue();
        $venue->id = $dataRow->venueId;
        $venue->pid = $dataRow->venuePid;
        $venue->name = $dataRow->venueName;
        $venue->url = $venue->setURL($dataRow->venueUrl);
        $venue->logoPath = $dataRow->logoPath;
        $venue->logoFile = $dataRow->logoFile;
        $venue->description = $dataRow->venueDescription;
        $venue->location = $this->getEventLocation($venue->url, $venue->name);
        if (is_numeric($dataRow->venueLat)){
            $venue->lat = $dataRow->venueLat;
        }
        if (is_numeric($dataRow->venueLong)){
            $venue->long = $dataRow->venueLong;
        }
        $venue->streetAddr = $dataRow->venueAddress;
        $venue->city = $dataRow->venueCity;
        $venue->region = $dataRow->venueState;
        $venue->postalCode = $dataRow->venueZip;
        $venue->country = $dataRow->venueCountry;
        $venue->phone = $this->getPhone($dataRow->venuePhone);
        if (is_numeric($dataRow->featuredCausePercentage)){
            $venue->featuredCausePercentage = $dataRow->featuredCausePercentage;
        }

        if (is_numeric($dataRow->customerChoicePercentage)){
            $venue->customerChoicePercentage = $dataRow->customerChoicePercentage;
        }
        if ($dataRow->isFlatDonation){
            $venue->isFlatDonation = true;
            $venue->donationPerUnit = $dataRow->donationPerUnit;
            $venue->unitDescriptor = $dataRow->unitDescriptor;
            $venue->flatDonationActionPhrase = $dataRow->flatDonationActionPhrase;
        }

        $venue->facebookEventID = $dataRow->facebookEventId;
        $venue->eventBriteEventID = $dataRow->eventBriteEventId;
        $venue->eventBriteVenueID = $dataRow->venueEventBriteId;

        if ($this->geoLookupProvider){
            $venue->fipsCounty = $this->geoLookupProvider->getFIPSCode($venue);
        }

        $venue->eventTemplatePremiumId = $dataRow->eventTemplatePremiumId;
        $venue->neighborhood = $dataRow->venueNeighborhood;
        $venue->keywords = $dataRow->venueKeywords;
        $venue->isFcOnly = $dataRow->isFcOnly;
        return $venue;
    }

    private function getFullDate($dateString, $timeString) {
        if (substr_compare($dateString, "0000", 0, 4) == 0){
            return NULL;
        }
        return strtotime($dateString." ".$timeString);
    }

    private function getEventLocation($merchantWebsite, $merchantName) {
        return ($merchantWebsite) ? $merchantWebsite : $merchantName;
    }

    private function getPhone($phoneString) {
        $cleanPhone = preg_replace("/[^0-9]/", "", $phoneString);
        if (strlen($cleanPhone) > 6){
            return preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $cleanPhone);

            //return "(".substr($cleanPhone, 0, 3).") ".substr($cleanPhone, 3, 3)."-".substr($cleanPhone,6);
        }
        return $cleanPhone;
    }
}

/**
 * Specifically used to retrieve an event for editing purposes
 */
class MerchantEventProvider extends EventRecordProvider{

    private static $FROM_TABLE_CLAUSE = "
        FROM
        (
        SELECT
            ev.Event_ID AS event_id,
            if (ev.Event_ProposedByMerchantID, 1, 0) AS owned_by_merchant,
            ev.Event_ProposedByMerchantID AS merchant_id,
            if(em.EventMatch_EventBriteEventID, em.EventMatch_EventBriteEventID, ev.Event_EventBriteEventID) AS event_brite_event_id,
            if(em.EventMatch_FacebookEventID, em.EventMatch_FacebookEventID, ev.Event_FacebookEventID) AS facebook_event_id,
            if(em.EventMatch_MerchantPercentage, em.EventMatch_MerchantPercentage, ev.Event_MerchantPercentage) AS featured_cause_percentage,
            if(em.EventMatch_MerchantSecondaryPercentage, em.EventMatch_MerchantSecondaryPercentage, ev.Event_MerchantSecondaryPercentage) AS customer_choice_percentage,
            if(em.EventMatch_IsFlatDonation, em.EventMatch_IsFlatDonation, ev.Event_IsFlatDonation) AS isFlatDonation,
            if(em.EventMatch_DonationPerUnit, em.EventMatch_DonationPerUnit, ev.Event_DonationPerUnit) AS donationPerUnit,
            if(em.EventMatch_UnitDescriptor, em.EventMatch_UnitDescriptor, ev.Event_UnitDescriptor) AS unitDescriptor,
            if(em.EventMatch_FlatDonationActionPhrase, em.EventMatch_FlatDonationActionPhrase, ev.Event_FlatDonationActionPhrase) AS flatDonationActionPhrase,
            if (op.OrganizationInfo_ID, op.OrganizationInfo_ID, 0) AS org_id,
            (SELECT EventLog_Timestamp FROM event_log WHERE EventLog_EventID = ev.Event_ID AND EventLog_Action = 'Created' ORDER BY EventLog_ID limit 1) AS Event_CreatedDate,
            ev.Event_LastEditedDate,
            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames
            FROM event_info ev
            LEFT JOIN merchant_info mp ON ev.Event_ProposedByMerchantID = mp.MerchantInfo_ID AND ev.Event_ProposedByMerchantID > 0
            LEFT JOIN organization_info op ON ev.Event_ProposedByOrgID = op.OrganizationInfo_ID
            LEFT JOIN event_match em ON em.EventMatch_EventID = ev.Event_ID AND em.EventMatch_MerchantID = ev.Event_ProposedByMerchantID AND EventMatch_Status = 'Accepted'
            WHERE ev.Event_Status = 'ACTIVE'
            AND ev.Event_ID = ?
            AND mp.MerchantInfo_ID = ?
            AND ( ev.Event_Date > CURRENT_DATE OR (ev.Event_Date = CURRENT_DATE AND ev.Event_StartTime > CURRENT_TIME) )
        ) AS active_events
        JOIN event_info e ON active_events.event_id = e.Event_ID
        LEFT JOIN merchant_info m ON active_events.merchant_id = m.MerchantInfo_ID
        LEFT JOIN cms_image_library i ON m.MerchantInfo_ImageLibraryID = i.id AND i.status = 'ACTIVE'
        LEFT JOIN organization_info o ON active_events.org_id = o.OrganizationInfo_ID
        LEFT JOIN event_templates et ON e.Event_EventTemplateID = et.EventTemplate_ID
        LEFT JOIN cms_image_library ti ON et.EventTemplate_CalendarIconImageID = ti.id AND ti.status = 'ACTIVE'
        WHERE 1 = 1";

    private static $BASIC_QUERY_BINDING = "ii";

    public function get(EventCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = parent::$GET_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = self::$BASIC_QUERY_BINDING;
        $filterParams = array(
            $criteria->eventId,
            $criteria->merchantId
        );

        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = count($sqlResult);
            foreach ($sqlResult as $row){
                $result->collection[] = $this->mapToRecord((object)$row);
            }
        } else {
            trigger_error("MerchantEventProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
}