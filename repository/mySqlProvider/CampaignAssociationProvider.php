<?php
class CampaignAssociationProvider{
    const EVENT_ASSOCIATION = "event_info";
    const EVENT_DRAFT_ASSOCIATION = "event_draft";
    const EVENT_TEMPLATE_ASSOCIATION = 'event_templates';
    const ORG_ASSOCIATION = 'organization_info';

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    // ---------------
    // Organization
    // ---------------

    public function hasOrganization(CampaignRecord $campaign, OrganizationRecord $source){
        if ($campaign->id && $source->id){
            $queryResponse = $this->hasAssociation(self::ORG_ASSOCIATION, $source->id, $campaign->id);
            if (count($queryResponse->resultArray)){
                return true;
            }
        }
        return false;
    }

    public function addOrganization(CampaignRecord $campaign, OrganizationRecord $source){
        if ($campaign->id && $source->id){
            return $this->addAssociation(self::ORG_ASSOCIATION, $source->id, $campaign->id);
        }
        return new ExecuteResponseRecord();
    }

    public function removeOrganization($campaignId, $sourceId){
        if ($campaignId && $sourceId){
            return $this->removeAssociation(self::ORG_ASSOCIATION, $sourceId, $campaignId);
        }
        return new ExecuteResponseRecord();
    }

    // ---------------
    // Event Template
    // ---------------

    public function setEventTemplateTags($commaDelimitedCampaignNames, $sourceId){
        return $this->setManyByName(self::EVENT_TEMPLATE_ASSOCIATION, $sourceId, $commaDelimitedCampaignNames);
    }

    public function getEventTemplateTags($sourceId){
        return $this->getAssociations(self::EVENT_TEMPLATE_ASSOCIATION, $sourceId, null);
    }

    // ---------------
    // Event
    // ---------------

    public function setEventTags($commaDelimitedCampaignNames, $sourceId){
        return $this->setManyByName(self::EVENT_ASSOCIATION, $sourceId, $commaDelimitedCampaignNames);
    }

    public function getEventTags($sourceId, $campaignImageUrlPath = null){
        return $this->getAssociations(self::EVENT_ASSOCIATION, $sourceId, $campaignImageUrlPath);
    }

    // -----------------------
    // Private generic methods
    // -----------------------

    private function getAssociations($sourceTable, $sourceId, $campaignImageUrlPath){
        $sqlString = self::$GET_ASSOCIATIONS_SQL;
        $sqlBinding = "si";
        $sqlParams = array(
            $sourceTable,
            $sourceId
        );
        $campaignRecords = array();
        $response = MySqliHelper::getResultWithLogging("CampaignAssociationProvider.getAssociations SQL error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        foreach ($response->resultArray as $record){
            $campaignRecord = CampaignRecord::castAs($record);
            if (!empty($campaignImageUrlPath)){
                $campaignRecord->setImageUrl($campaignImageUrlPath);
            }
            $campaignRecords[] = $campaignRecord;
        }
        return $campaignRecords;
    }

    private static $GET_ASSOCIATIONS_SQL = "
        SELECT
            TagList_ID as id,
            TagList_Name as name,
            TagList_Description as snippet,
            TagList_CampaignHeading as heading,
            TagList_CampaignSubHeading as subHeading,
            TagList_CampaignDescription as description,
            TagList_Image as imageFileName,
            TagList_URL as url,
            TagList_EventURLOverride as eventUrlOverride,
            TagList_AddWrap as addWrap,
            TagList_MapZoomLevel as mapZoomlevel,
            TagList_MapCenterLat as mapCenterLat,
            TagList_MapCenterLong as mapCenterLong
        FROM tag_list
        JOIN tag_association ON TagList_ID = TagAssociation_TagListID
        WHERE TagAssociation_SourceTable = ?
        AND TagAssociation_SourceID = ?
        AND TagAssociation_Status IS NULL
        ORDER BY TagAssociation_SortOrder, TagList_Name";

    private function hasAssociation($sourceTable, $sourceId, $campaignId){
        $sqlString = self::$HAS_ASSOCIATION_SQL;
        $sqlBinding = "ssi";
        $sqlParams =
        $sqlParams = array(
            $sourceTable,
            $sourceId,
            $campaignId
        );
        return MySqliHelper::getResultWithLogging("CampaignAssociationProvider.hasAssociation: SQL Error: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $HAS_ASSOCIATION_SQL = "
        SELECT 1
        FROM tag_association
        WHERE TagAssociation_SourceTable = ?
        AND TagAssociation_SourceID = ?
        AND TagAssociation_TagListID = ?
        AND TagAssociation_Status IS NULL";

    private function addAssociation($sourceTable, $sourceId, $campaignId, $sortOrder = 0){
        $sqlString = self::$ADD_ASSOCIATION_SQL;
        $sqlBinding = 'siiii';
        $sqlParams = array(
            $sourceTable,
            $sourceId,
            $campaignId,
            $sortOrder,
            $sortOrder
        );
        return MySqliHelper::executeWithLogging("CampaignAssociationProvider.addAssociation: SQL error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $ADD_ASSOCIATION_SQL = "
        INSERT INTO tag_association
        (
            TagAssociation_SourceTable,
            TagAssociation_SourceID,
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        )
        VALUES
        (
            ?,?,?,?
        )
        ON DUPLICATE KEY UPDATE
          TagAssociation_Status = NULL,
          TagAssociation_SortOrder = ?
        ";

    private function removeAssociation($sourceTable, $sourceId, $campaignId){
        $sqlString = self::$REMOVE_ASSOCIATION_SQL;
        $sqlBinding = "sii";
        $sqlParams = array(
            $sourceTable,
            $sourceId,
            $campaignId
        );
        return MySqliHelper::executeWithLogging("CampaignAssociationProvider.removeAssociation: SQL error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $REMOVE_ASSOCIATION_SQL = "
        UPDATE tag_association
        SET TagAssociation_Status = 'Deleted'
        WHERE TagAssociation_SourceTable = ?
        AND TagAssociation_SourceID = ?
        AND TagAssociation_TagListID = ?";

    private function setManyByName($sourceTable, $sourceId, $commaDelimitedCampaignNames){
        if (is_array($commaDelimitedCampaignNames)){
            $campaignNames = $commaDelimitedCampaignNames;
        } else {
            $campaignNames = explode(",", $commaDelimitedCampaignNames);
        }
        $addResponse = $this->addManyByName($sourceTable, $sourceId, $campaignNames);
        $removeResponse = $this->removeNotInList($sourceTable, $sourceId, $campaignNames);
        return $addResponse->success ? $addResponse : $removeResponse;
    }

    private function addManyByName($sourceTable, $sourceId, array $campaignNames){
        $addResponse = new ExecuteResponseRecord();
        if (count($campaignNames)){
            $sqlString = self::$ADD_BY_NAME_SELECT;
            $sqlBinding = "siisi";
            $orderCounter = 0;
            foreach ($campaignNames as $campaignName){
                $sqlParams = array(
                    $sourceTable,
                    $sourceId,
                    $orderCounter,
                    $campaignName,
                    $orderCounter
                );
                $orderCounter++;
                MySqliHelper::executeWithLogging("CampaignAssociationProvider.addManyByName:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams, $addResponse);
            }

        }
        return $addResponse;
    }

    private static $ADD_BY_NAME_SELECT = "
        INSERT INTO tag_association
        (
            TagAssociation_SourceTable,
            TagAssociation_SourceID,
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        )
        SELECT ?, ?, tag_list.TagList_ID, ?
        FROM tag_list
        WHERE tag_list.TagList_Name = ?
        ON DUPLICATE KEY UPDATE
            TagAssociation_Status = NULL,
            TagAssociation_SortOrder = ?";

    private function removeNotInList($sourceTable, $sourceId, array $campaignNames){
        // run this regardless of the count of items
        $sqlString = self::$REMOVE_NOT_IN_LIST_SELECT;
        $sqlBinding = "si";
        $sqlParams = array(
            $sourceTable,
            $sourceId
        );
        if (count($campaignNames)){
            $sqlString .= " AND tag_list.TagList_Name NOT IN (";
            $isFirst = true;
            foreach ($campaignNames as $campaignName){
                if ($isFirst){
                    $isFirst = false;
                } else {
                    $sqlString .= ",";
                }
                $sqlString .= "?";
                $sqlBinding .= "s";
                $sqlParams[] = $campaignName;
            }
            $sqlString .= ") ";
        }

        return MySqliHelper::executeWithLogging("CampaignAssociationProvider.removeNotInList:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $REMOVE_NOT_IN_LIST_SELECT = "
        UPDATE tag_association JOIN tag_list ON tag_association.TagAssociation_TagListID = tag_list.TagList_ID
        SET tag_association.TagAssociation_Status = 'Deleted'
        WHERE tag_association.TagAssociation_SourceTable = ?
        AND tag_association.TagAssociation_SourceID = ?";
}