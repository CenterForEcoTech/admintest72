<?php

if (empty($rootFolder)){
    include_once("_getRootFolder.php");
}
include_once($rootFolder."DataContracts.php");

class NonprofitReceiptsReportCriteria implements ReportCriteria{
    /**
     * Required: identifies the nonprofit whose data is desired
     * @var int
     */
    public $orgId;

    /**
     * Optional merchant ID for detail report
     * @var int
     */
    public $merchantId;

    /**
     * This is the last run time and is used to determine how far back to query to update the cache
     * @var int
     * */
    public $fromDateTime = null; // default

    /**
     * The start date (inclusive) of the report. If empty or invalid, defaults to current calendar year.
     * @var string
     */
    public $startDate;

    /**
     * The end date (inclusive) of the report. If empty or invalid, defaults to current date
     * @var string
     */
    public $endDate;

    public function getQueryStartDate(){
        if ($this->startDate && strtotime($this->startDate)){
            return $this->startDate;
        }
        return date("Y-m-d", strtotime(date("Y")."-01-01")); // First day of the current year
    }

    public function getQueryEndDate(){
        if ($this->endDate && strtotime($this->endDate)){
            return $this->endDate;
        }
        return date("Y-m-d", strtotime(date("Y-m-d")." + 1 day")); // Today
    }
}

class NonprofitAllTimeReceiptsReportCriteria extends NonprofitReceiptsReportCriteria{
    public function getQueryStartDate(){
        if ($this->startDate && strtotime($this->startDate)){
            return $this->startDate;
        }
        return "2012-01-01"; // First possible day of all possible transactions
    }
}

class NonprofitDisbursementCheckCriteria extends NonprofitAllTimeReceiptsReportCriteria{
    public $batchId;
}

class NonprofitReceiptsReportRunner implements CachedReportRunner {
    CONST REPORT_NAME = "Nonprofit Receipts";
    protected $reportsConn;
    protected $disbursementConn;
    private $reportId;

    public function __construct(mysqli $reportsConn, mysqli $disbursementConn){
        $this->reportsConn = $reportsConn;
        $this->disbursementConn = $disbursementConn;
    }

    private static $GET_REPORT_DEFINITION_SQL = "
        SELECT id
        FROM report_definition
        WHERE name = ?";

    protected function getReportId(){
        if (!$this->reportId){
            $sqlString = self::$GET_REPORT_DEFINITION_SQL;
            $sqlBinding = "s";
            $sqlParam = array(self::REPORT_NAME);
            $queryResponse = new QueryResponseRecord();
            $result = MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParam, $queryResponse);

            if ($queryResponse->success){
                if (count($result)){
                    $this->reportId = $result[0]["id"];
                }
            } else {
                trigger_error("NonprofitReceiptsReportRunner.getReportId: ".$queryResponse->error, E_USER_ERROR);
                return null;
            }
        }

        return $this->reportId;
    }

    private static $CHECK_IF_CACHE_EXPIRED_SQL = "
        SELECT 1
        FROM report_definition rd
        WHERE rd.id = ?
        AND NOT EXISTS (
            SELECT 1
            FROM report_merchant_donations_lastrun rdl
            WHERE rdl.report_definition_id = rd.id
            AND rdl.organization_info_id = ?
            AND rdl.timestamp + INTERVAL rd.cache_life_minutes MINUTE > CURRENT_TIMESTAMP
        )";

    /**
     * Returns true if the cache can be refreshed.
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function canRun(ReportCriteria $criteria)
    {
        /* @var $criteria NonprofitReceiptsReportCriteria */
        if (is_numeric($criteria->fromDateTime) && $criteria->orgId){
            return true; // always allowable if datetime has been specified.
        }
        $reportId = $this->getReportId();
        $orgId = $criteria->orgId;
        if ($reportId && $orgId){
            $sqlString = self::$CHECK_IF_CACHE_EXPIRED_SQL;
            $sqlBinding = "ii";
            $sqlParams = array($reportId, $orgId);
            $queryResponse = new QueryResponseRecord();
            $result = MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
            if ($queryResponse->success){
                if (count($result)){
                    return true;
                }
            } else {
                trigger_error("NonprofitReceiptsReportRunner.canRun (check expired): ".$queryResponse->error, E_USER_ERROR);
                return false;
            }
        }
        return false;
    }

    /**
     * Attempts to refresh the cache for a report with the given parameters. Returns true if successful.
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function run(ReportCriteria $criteria)
    {
        $success = false;
        /* @var $criteria NonprofitReceiptsReportCriteria */
        if ($this->canRun($criteria)){
            $reportId = $this->getReportId();
            $orgId = $criteria->orgId;
            $lastRunTime = $this->getLastRuntime($reportId, $orgId);

            if ($criteria->fromDateTime && is_numeric($criteria->fromDateTime) && $criteria->fromDateTime < $lastRunTime){
                $lastRunTime = $criteria->fromDateTime;
            }
            $success = $this->logRunTime($reportId, $orgId);

            // from event_transaction
            $success = $success && $this->updatePendingTransactions($reportId, $orgId);
            $success = $success && $this->insertPendingTransactions($reportId, $orgId);

            // from disbursement
            $success = $success && $this->updateAllocationData($reportId, $orgId, $lastRunTime);
            if ($this->updateDisbursementChecks($reportId, $orgId, $lastRunTime)){
                $success = $success && $this->updateDisbursementCheckData($reportId, $orgId, $lastRunTime);
            }
        }
        return $success;
    }

    private static $GET_LAST_RUN_SQL = "
        SELECT timestamp
        FROM report_merchant_donations_lastrun
        WHERE report_definition_id = ?
        AND organization_info_id = ?";

    private function getLastRunTime($reportId, $orgId){
        $sqlString = self::$GET_LAST_RUN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $orgId
        );
        $result = MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams);
        if (count($result)){
            return $result[0]["timestamp"];
        }
        return 0;
    }

    private static $LOG_RUN_TIME_SQL = "
        INSERT INTO report_merchant_donations_lastrun
        (report_definition_id, organization_info_id)
        values (?,?)
        ON DUPLICATE KEY UPDATE timestamp = CURRENT_TIMESTAMP";

    private function logRunTime($reportId, $orgId){
        $sqlString = self::$LOG_RUN_TIME_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $orgId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("NonprofitReceiptsReportRunner.logRunTime: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $UPDATE_PENDING_TRANSACTIONS = "
        UPDATE report_merchant_donations
        JOIN event_transaction e ON transaction_id = e.EventTransaction_ID
            AND customer_account_id = 0
        LEFT JOIN invoices_matrix i
            ON e.EventTransaction_InvoiceID = i.InvoicesMatrix_InvoiceID
            AND e.EventTransaction_ID = i.InvoicesMatrix_EventTransactionID
        SET cache_status = if (e.EventTransaction_Status LIKE '%Deleted%', 'DELETED', 'ACTIVE'),
            cache_timestamp = CURRENT_TIMESTAMP()
        WHERE report_definition_id = ?
        AND organization_info_id = ?
        AND (
            e.EventTransaction_InvoiceID = 0
            OR ( e.EventTransaction_InvoiceID <> 0
                AND i.InvoicesMatrix_IncomingQueueID IS NULL )
            )";

    /**
     * Purpose: find transactions that have not yet hit disbursement and update the status if the merchant deleted it.
     * This does not operate on transactions that are in the cache, but have moved into disbursement.
     * @param $reportId
     * @param $orgId
     * @return bool
     */
    private function updatePendingTransactions($reportId, $orgId){
        $sqlString = self::$UPDATE_PENDING_TRANSACTIONS;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $orgId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("NonprofitReceiptsReportProvider.updatePendingTransactions: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $INSERT_PENDING_TRANSACTIONS = "
        INSERT INTO report_merchant_donations
        (
            report_definition_id,
            merchant_info_id,
            organization_info_id,
            customer_member_id,

            transaction_id,
            transaction_date,
            transaction_year,
            transaction_month,
            transaction_week,
            transaction_sale,
            transaction_donation,
            is_flat_donation,
            flat_donation_units
        )
        SELECT
            ?,
            EventTransaction_MerchantID as merchant_info_id,
            EventTransaction_OrgID as organization_info_id,
            EventTransaction_CustomerID as customer_member_id,

            EventTransaction_ID as transaction_id,
            EventTransaction_EventDate as transaction_date,
            YEAR(EventTransaction_EventDate) as transaction_year,
            MONTH(EventTransaction_EventDate) as transaction_month,
            WEEK(EventTransaction_EventDate, 3) as transaction_week,
            EventTransaction_EligibleAmount as transaction_sale,
            EventTransaction_GeneratedAmount as transaction_donation,
            EventTransaction_IsFlatDonation as is_flat_donation,
            EventTransaction_FlatDonationUnits as flat_donation_units
        FROM event_transaction e
        WHERE EventTransaction_OrgID = ?
        AND e.EventTransaction_Status NOT LIKE '%Deleted%'
        AND NOT EXISTS (
            SELECT 1
            FROM report_merchant_donations
            WHERE report_definition_id = ?
            AND organization_info_id = ?
            AND transaction_id = EventTransaction_ID
        )";

    /**
     * Inserts new transactions that have not been previously cached, and that have not yet entered disbursement.
     * @param $reportId
     * @param $orgId
     * @return bool
     */
    private function insertPendingTransactions($reportId, $orgId){
        $sqlString = self::$INSERT_PENDING_TRANSACTIONS;
        $sqlBinding = "iiii";
        $sqlParams = array(
            $reportId,
            $orgId,
            $reportId,
            $orgId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("NonprofitReceiptsReportRunner.insertPendingTransactions: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $GET_UPDATED_ALLOCATION_DATA = "
        SELECT
            at.merchant_info_id,
            orgs.organization_info_id,
            a.member_id AS customer_member_id,
            at.customer_account_id,
            al.id customer_allocation_id,
            al.amount AS donation_amount,
            if (ap.id, ap.timestamp, al.event_transaction_timestamp) as allocation_timestamp,
            at.status AS allocation_status,

            at.event_transaction_id as transaction_id,
            trans.event_date as transaction_date,
            year(trans.event_date) as transaction_year,
            month(trans.event_date) as transaction_month,
            week(trans.event_date, 3) as transaction_week,
            trans.sale_amount AS transaction_sale,
            trans.is_flat_donation as is_flat_donation,
            trans.flat_donation_units as flat_donation_units,
            at.generated_amount as transaction_donation,
            CASE at.status
                WHEN 'NEW' THEN 0
                ELSE (
                        SELECT sum(amount)
                        FROM customer_allocations al2
                        WHERE al2.customer_account_id = at.customer_account_id
                        AND al2.event_transaction_id = at.event_transaction_id
                    )
            END as 'already_allocated'
        FROM customer_account_transactions at
        JOIN customer_account a ON at.customer_account_id = a.id
        JOIN incoming_data trans ON at.event_transaction_id = trans.event_transaction_id
        JOIN customer_allocations al ON at.event_transaction_id = al.event_transaction_id
        LEFT JOIN customer_allocation_plans ap ON al.customer_allocation_plan_id = ap.id
        JOIN disbursement_orgs orgs ON al.disbursement_org_id = orgs.id
        WHERE orgs.organization_info_id = ?
        AND al.timestamp > ?";

    private static $INSERT_ALLOCATION_TRANSACTION_DATA = "
        INSERT INTO report_merchant_donations
        (
            report_definition_id,
            merchant_info_id,
            customer_member_id,
            customer_account_id,

            allocation_status,
            transaction_id,
            transaction_date,

            transaction_year,
            transaction_month,
            transaction_week,
            transaction_sale,
            transaction_donation,

            is_flat_donation,
            flat_donation_units
        )
        VALUES (
            ?,?,?,?,
            ?,?,?,
            ?,?,?,?,?,
            ?,?
        )
        ON DUPLICATE KEY UPDATE
            cache_timestamp = CURRENT_TIMESTAMP,
            customer_account_id = ?,
            allocation_status = ?";

    private static $INSERT_ALLOCATION_TRANSACTION_BINDING = "iiiisisiiiddiiis";

    public static $INSERT_ALLOCATION_DATA = "
        INSERT INTO report_donation_recipients
        (
            report_definition_id,
            organization_info_id,
            customer_allocation_id,
            transaction_id,
            donation_amount,
            allocation_timestamp
        )
        VALUES
        (
          ?,?,?,?,?,?
        )
        ON DUPLICATE KEY UPDATE
          cache_timestamp = CURRENT_TIMESTAMP";

    /**
     * Adds newly allocated amounts from disbursement. This includes whole transactions that were already allocated while
     * in event_transaction, as well as all partial allocations done via the Giving Bank.
     * @param $reportId
     * @param $orgId
     * @param $lastRunTime
     * @return bool
     */
    private function updateAllocationData($reportId, $orgId, $lastRunTime){
        $selectString = self::$GET_UPDATED_ALLOCATION_DATA;
        $selectBinding = "is";
        $selectParams = array(
            $orgId,
            date("Y-m-d H:i:s", $lastRunTime)
        );
        $queryResponse = new QueryResponseRecord();
        $updatedDisbursementData = MySqliHelper::get_result($this->disbursementConn, $selectString, $selectBinding, $selectParams, $queryResponse);
        if ($queryResponse->success){
            $insertString1 = self::$INSERT_ALLOCATION_TRANSACTION_DATA;
            $insertBinding1 = self::$INSERT_ALLOCATION_TRANSACTION_BINDING;
            $insertString2 = self::$INSERT_ALLOCATION_DATA;
            $insertBinding2 = "iiiids";
            $numErrors = 0;
            foreach ($updatedDisbursementData as $disbursementData){
                $row = (object)$disbursementData;
                $insertParams1 = array(
                    $reportId,
                    $row->merchant_info_id,
                    $row->customer_member_id,
                    $row->customer_account_id,

                    $row->allocation_status,
                    $row->transaction_id,
                    $row->transaction_date,

                    $row->transaction_year,
                    $row->transaction_month,
                    $row->transaction_week,
                    $row->transaction_sale,
                    $row->transaction_donation,

                    $row->is_flat_donation,
                    $row->flat_donation_units,

                    $row->customer_account_id,
                    $row->allocation_status
                );
                $insertParams2 = array(
                    $reportId,
                    $row->organization_info_id,
                    $row->customer_allocation_id,
                    $row->transaction_id,
                    $row->donation_amount,
                    $row->allocation_timestamp
                );
                $executeResponse1 = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString1, $insertBinding1, $insertParams1, $executeResponse1);
                if (!$executeResponse1->success){
                    trigger_error("NonprofitReceiptsReportRunner.updateAllocationData (insert 1): ".$executeResponse1->error, E_USER_ERROR);
                    $numErrors++;
                }
                $executeResponse2 = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString2, $insertBinding2, $insertParams2, $executeResponse2);
                if (!$executeResponse2->success){
                    trigger_error("NonprofitReceiptsReportRunner.updateAllocationData (insert 2): ".$executeResponse2->error, E_USER_ERROR);
                    $numErrors++;
                }
            }
            return $numErrors == 0;
        } else {
            trigger_error("NonprofitReceiptsReportRunner.updateAllocationData (query): ".$queryResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $GET_UPDATED_CHECKS = "
        SELECT
            o.organization_info_id,
            d.id AS disbursement_id,
            d.batch_id AS batch_id,
            d.amount AS check_amount,
            b.timestamp AS check_timestamp,
            d.status AS check_status
        FROM disbursements d
        JOIN disbursement_batches b ON d.batch_id = b.id
        JOIN disbursement_orgs o ON d.disbursement_org_id = o.id
        WHERE o.organization_info_id = ?
        AND b.timestamp > ?";

    private static $INSERT_UPDATED_CHECKS = "
        INSERT INTO report_disbursement_checks
        (
          report_definition_id,
          organization_info_id,
          disbursement_id,
          batch_id,
          check_amount,
          check_timestamp,
          check_status
        )
        VALUES
        (
          ?,?,?,?,?,?,?
        )
        ON DUPLICATE KEY UPDATE
          cache_timestamp = CURRENT_TIMESTAMP";

    private static $INSERT_UPDATED_CHECKS_BINDINGS = "iiiidss";

    /**
     * Adds newly disbursed checks to the report cache for the specified report and org.
     * @param $reportId
     * @param $orgId
     * @param $lastRunTime
     * @return bool
     */
    private function updateDisbursementChecks($reportId, $orgId, $lastRunTime){
        $selectString = self::$GET_UPDATED_CHECKS;
        $selectBinding = "is";
        $selectParams = array(
            $orgId,
            date("Y-m-d H:i:s", $lastRunTime)
        );
        $queryResponse = new QueryResponseRecord();
        $updatedDisbursementData = MySqliHelper::get_result($this->disbursementConn, $selectString, $selectBinding, $selectParams, $queryResponse);
        if ($queryResponse->success){
            $insertString = self::$INSERT_UPDATED_CHECKS;
            $insertBinding = self::$INSERT_UPDATED_CHECKS_BINDINGS;
            $numErrors = 0;
            foreach ($updatedDisbursementData as $disbursementData){
                $row = (object)$disbursementData;
                $insertParams = array(
                    $reportId,
                    $row->organization_info_id,
                    $row->disbursement_id,
                    $row->batch_id,
                    $row->check_amount,
                    $row->check_timestamp,
                    $row->check_status
                );
                $executeResponse = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString, $insertBinding, $insertParams, $executeResponse);
                if (!$executeResponse->success){
                    trigger_error("NonprofitReceiptsReportRunner.updateDisbursementChecks: ".$executeResponse->error, E_USER_ERROR);
                    $numErrors++;
                }
            }
            return $numErrors == 0;
        } else {
            trigger_error("NonprofitReceiptsReportRunner.updateDisbursementChecks (query): ".$queryResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $GET_UPDATED_CHECK_DATA = "
        SELECT
            SUM(nd.amount) AS merchant_amount,
            b.id AS batch_id,
            d.id AS disbursement_id,
	        o.organization_info_id,
            trans.merchant_info_id
        FROM nonprofit_disbursements nd
        JOIN disbursements d ON nd.disbursement_id = d.id
        JOIN disbursement_batches b ON d.batch_id = b.id
        JOIN disbursement_orgs o ON nd.disbursement_org_id = o.id
        JOIN incoming_data trans ON nd.event_transaction_id = trans.event_transaction_id
        WHERE o.organization_info_id = ?
        AND b.timestamp > ?
        GROUP BY b.id, d.id, o.organization_info_id, trans.merchant_info_id";

    private static $INSERT_UPDATED_CHECK_DATA = "
        INSERT INTO report_disbursement_check_data
        (
          report_definition_id,
          organization_info_id,
          merchant_info_id,
          disbursement_id,
          batch_id,
          merchant_amount
        )
        VALUES
        (
          ?,?,?,?,?,?
        )
        ON DUPLICATE KEY UPDATE
          cache_timestamp = CURRENT_TIMESTAMP";

    private static $INSERT_UPDATED_CHECK_DATA_BINDINGS = "iiiiid";

    /**
     * Adds newly disbursed check detail (merchant amounts) to cache.
     * @param $reportId
     * @param $orgId
     * @param $lastRunTime
     * @return bool
     */
    private function updateDisbursementCheckData($reportId, $orgId, $lastRunTime){
        $selectString = self::$GET_UPDATED_CHECK_DATA;
        $selectBinding = "is";
        $selectParams = array(
            $orgId,
            date("Y-m-d H:i:s", $lastRunTime)
        );
        $queryResponse = new QueryResponseRecord();
        $updatedDisbursementData = MySqliHelper::get_result($this->disbursementConn, $selectString, $selectBinding, $selectParams, $queryResponse);
        if ($queryResponse->success){
            $insertString = self::$INSERT_UPDATED_CHECK_DATA;
            $insertBinding = self::$INSERT_UPDATED_CHECK_DATA_BINDINGS;
            $numErrors = 0;
            foreach ($updatedDisbursementData as $disbursementData){
                $row = (object)$disbursementData;
                $insertParams = array(
                    $reportId,
                    $row->organization_info_id,
                    $row->merchant_info_id,
                    $row->disbursement_id,
                    $row->batch_id,
                    $row->merchant_amount
                );
                $executeResponse = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString, $insertBinding, $insertParams, $executeResponse);
                if (!$executeResponse->success){
                    trigger_error("NonprofitReceiptsReportRunner.updateDisbursementCheckData: ".$executeResponse->error, E_USER_ERROR);
                    $numErrors++;
                }
            }
            return $numErrors == 0;
        } else {
            trigger_error("NonprofitReceiptsReportRunner.updateDisbursementCheckData (query): ".$queryResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $MERCHANT_BREAKDOWN_SQL = "
        SELECT allocation_date, merchantId, merchantName, merchantCity, merchantState, merchantPid, SUM(donations) AS donations FROM
        (
            SELECT
                IF (r.transaction_id, DATE(r.allocation_timestamp), t.transaction_date) AS allocation_date,
                m.MerchantInfo_ID as 'merchantId',
                m.MerchantInfo_Name as 'merchantName',
                m.MerchantInfo_City as 'merchantCity',
                m.MerchantInfo_State as 'merchantState',
                (
                    SELECT pid
                    FROM permalinks
                    WHERE entity_name = 'merchant'
                    AND entity_id = m.MerchantInfo_ID
                    AND status = 'ACTIVE'
                    ORDER BY weight DESC
                    LIMIT 1
                ) as merchantPid,
                IF (r.transaction_id, r.donation_amount, t.transaction_donation) as 'donations'
            FROM report_merchant_donations t
            JOIN merchant_info m ON t.merchant_info_id = m.MerchantInfo_ID
            LEFT JOIN report_donation_recipients r
                ON t.transaction_id = r.transaction_id AND r.report_definition_id = t.report_definition_id
            WHERE t.cache_status = 'ACTIVE'
            AND t.report_definition_id = ?
            AND (t.organization_info_id = ? OR r.organization_info_id = ?)
        ) AS transactions
        WHERE transactions.allocation_date >= ? AND transactions.allocation_date <= ?
        GROUP BY merchantId WITH ROLLUP";

    protected function getMerchantBreakdown(NonprofitReceiptsReportCriteria $criteria){
        $reportId = $this->getReportId();
        $reportWrapper = new stdClass();
        $collection = array();
        $sqlString = self::$MERCHANT_BREAKDOWN_SQL;
        $sqlBinding = "iiiss";
        $sqlParams = array( $reportId, $criteria->orgId, $criteria->orgId, $criteria->getQueryStartDate(), $criteria->getQueryEndDate());
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $summaryRow = new NonprofitReceiptsSummaryRow();
                $summaryRow->year = $rowObject->transaction_year;
                $summaryRow->donationsGenerated = $rowObject->donations;
                if ($rowObject->merchantId){
                    $summaryRow->breakdownValue = $rowObject->merchantName;
                    $summaryRow->merchantName = $rowObject->merchantName;
                    $summaryRow->merchantCity = $rowObject->merchantCity;
                    $summaryRow->merchantState = $rowObject->merchantState;
                    $summaryRow->merchantPid = $rowObject->merchantPid;
                } else {
                    // this is a full report aggregate
                    $summaryRow->breakdownValue = "Total Donations";
                    $summaryRow->isGrandTotal = true;
                }
                $collection[] = $summaryRow;
            }
        } else {
            trigger_error("NonprofitReceiptsReportRunner.getMerchantBreakdown: ".$queryResponse->error, E_USER_ERROR);
        }
        $reportWrapper->summaryReport = $collection;
        return $reportWrapper;
    }

    /**
     * Returns data from the cache. This function does not ensure the cache is up to date. If it is desirable to update the cache,
     * the "run" function must be executed.
     * @param ReportCriteria $criteria
     * @return mixed
     */
    public function get(ReportCriteria $criteria)
    {
        /* @var $criteria NonprofitReceiptsReportCriteria */
        return $this->getMerchantBreakdown($criteria);
    }

    private static $CUSTOMER_DETAIL_REPORT = "
        SELECT
            tr.transaction_id,
            tr.allocation_date,
            tr.customer_member_id,
            sum(tr.donation_amount) as donation_amount,
            tr.merchant_info_id,
	        if (ind.IndividualInfo_FirstName IS NOT NULL,
	            ind.IndividualInfo_FirstName,
	            ''
	        ) as first_name,
            ind.IndividualInfo_LastName as last_name,
            ind.IndividualInfo_Email as email
        FROM (
            SELECT
                t.id,
                t.report_definition_id,
                t.merchant_info_id,
                t.organization_info_id,
                t.transaction_id,
                if (r.transaction_id, DATE(r.allocation_timestamp), t.transaction_date) AS allocation_date,
                if (r.transaction_id, r.donation_amount, t.transaction_donation) AS donation_amount,
                t.transaction_donation as orig_donation_amount,
                t.customer_member_id
            FROM report_merchant_donations t
            JOIN merchant_info m ON t.merchant_info_id = m.MerchantInfo_ID
            LEFT JOIN report_donation_recipients r ON t.transaction_id = r.transaction_id AND t.report_definition_id = r.report_definition_id
            WHERE t.cache_status = 'ACTIVE'
            AND t.report_definition_id = ?
            AND (t.organization_info_id = ? OR r.organization_info_id = ?)
            AND (0 = ? OR t.merchant_info_id = ?)
        ) as tr
        LEFT JOIN  member_preferences mp ON tr.customer_member_id = mp.members_id AND mp.shareWithOrgs = 1
        LEFT JOIN individual_info ind ON mp.members_id = ind.IndividualInfo_MemberID
        WHERE tr.allocation_date >= ? AND tr.allocation_date <= ?
        GROUP BY tr.allocation_date, tr.transaction_id WITH ROLLUP";

    public function getDetail(NonprofitReceiptsReportCriteria $criteria){
        $reportId = $this->getReportId();
        $filterByMerchant = ($criteria->merchantId) ? 1 : 0;
        $merchantId = ($criteria->merchantId) ? $criteria->merchantId : 0;
        $reportWrapper = new stdClass();
        $collection = array();
        $sqlString = self::$CUSTOMER_DETAIL_REPORT;
        $sqlBinding = "iiiiiss";
        $sqlParams = array(
            $reportId,
            $criteria->orgId,
            $criteria->orgId,
            $filterByMerchant,
            $merchantId,
            $criteria->getQueryStartDate(),
            $criteria->getQueryEndDate()
        );
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $detailRow = new NonprofitCustomerDetailRow();
                $detailRow->allocationDate = $rowObject->allocation_date;
                $detailRow->donationAmount = $rowObject->donation_amount;
                if ($rowObject->transaction_id){
                    $detailRow->breakdownValue = $rowObject->allocation_date;
                    $detailRow->firstName = $rowObject->first_name;
                    $detailRow->lastName = $rowObject->last_name;
                    $detailRow->email = $rowObject->email;
                    $collection[] = $detailRow;
                } else if ($rowObject->allocation_date){
                    // ignore these, this grouping exists only to make items sort by date
                } else {
                    // this is a full report aggregate
                    $detailRow->breakdownValue = "Total Donations";
                    $detailRow->isGrandTotal = true;
                    $collection[] = $detailRow;
                }
            }
        } else {
            trigger_error("NonprofitReceiptsReportRunner.getDetail: ".$queryResponse->error, E_USER_ERROR);
        }
        $reportWrapper->detailReport = $collection;
        return $reportWrapper;
    }

    private static $GET_DISBURSED_CHECKS_REPORT = "
        SELECT
            check_timestamp,
            check_amount,
            batch_id
        FROM report_disbursement_checks
        WHERE report_definition_id = ?
        AND organization_info_id = ?
        AND cache_status = 'ACTIVE'
        ORDER BY check_timestamp DESC";

    public function getDisbursedChecks(NonprofitReceiptsReportCriteria $criteria){
        $reportId = $this->getReportId();
        $reportWrapper = new stdClass();
        $runningTotal = 0;
        $collection = array();
        $sqlString = self::$GET_DISBURSED_CHECKS_REPORT;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $criteria->orgId
        );
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $rowObject->check_date = date("n/j/Y", strtotime($rowObject->check_timestamp));
                $runningTotal += $rowObject->check_amount;
                $collection[] = $rowObject;
            }
        } else {
            trigger_error("NonprofitReceiptsReportRunner.getDisbursedChecks: ".$queryResponse->error, E_USER_ERROR);
        }
        $reportWrapper->checks = $collection;
        $reportWrapper->total = $runningTotal;
        $reportWrapper->fundsPending = $this->getFundsPendingAmount($criteria);
        return $reportWrapper;
    }

    private static $GET_DISBURSED_CHECK_MERCHANT_DETAIL = "
        SELECT
            r.merchant_info_id,
            r.merchant_amount,
            m.MerchantInfo_Name as merchant_name,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'merchant'
                AND entity_id = m.MerchantInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as merchantPid
        FROM report_disbursement_check_data r
        JOIN merchant_info m ON r.merchant_info_id = m.MerchantInfo_ID
        WHERE r.report_definition_id = ?
        AND r.organization_info_id = ?
        AND r.batch_id = ?
        AND cache_status = 'ACTIVE'
        ORDER BY m.MerchantInfo_Name ASC";

    public function getDisbursedCheckData(NonprofitDisbursementCheckCriteria $criteria){
        $reportId = $this->getReportId();
        $reportWrapper = new stdClass();
        $collection = array();
        $sqlString = self::$GET_DISBURSED_CHECK_MERCHANT_DETAIL;
        $sqlBinding = "iii";
        $sqlParams = array(
            $reportId,
            $criteria->orgId,
            $criteria->batchId
        );
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $collection[] = $rowObject;
            }
        } else {
            trigger_error("NonprofitReceiptsReportRunner.getDisbursedCheckData: ".$queryResponse->error, E_USER_ERROR);
        }
        $reportWrapper->checkData = $collection;
        return $reportWrapper;
    }

    private static $GET_FUNDS_PENDING_AMOUNT = "
        SELECT
            COALESCE(SUM(donations.transaction_donation),0) AS amount
        FROM report_merchant_donations donations
        WHERE donations.cache_status = 'ACTIVE'
        AND donations.report_definition_id = ?
        AND donations.organization_info_id = ?
        AND NOT EXISTS(
            SELECT 1
            FROM report_donation_recipients allocations
            WHERE allocations.transaction_id = donations.transaction_id
            AND allocations.report_definition_id = donations.report_definition_id
        )";

    /**
     * Returns the sum total in the report cache that represents donations designated for this org that have not yet entered
     * the disbursement pipeline. Regardless of invoice payment status, we call these "funds pending" (i.e., awaiting payment from merchant).
     * @param NonprofitReceiptsReportCriteria $criteria
     * @return float
     */
    private function getFundsPendingAmount(NonprofitReceiptsReportCriteria $criteria){
        $reportId = $this->getReportId();
        $sqlString = self::$GET_FUNDS_PENDING_AMOUNT;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $criteria->orgId
        );
        $result = MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams);
        if (count($result)){
            return $result[0]['amount'];
        }
        return 0.00;
    }
}

class NonprofitReceiptsSummaryRow extends DonationsSummaryRow{
    public $year;
    public $merchantName;
    public $merchantCity;
    public $merchantState;
    public $merchantPid;
}

class NonprofitCustomerDetailRow {
    public $allocationDate;
    public $donationAmount;
    public $firstName;
    public $lastName;
    public $email;
    public $isGrandTotal;
    public $breakdownValue;
}