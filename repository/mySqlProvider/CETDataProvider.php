<?php
class CETDataProvider {
	private $mysqli;
	
    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
	private static $SELECT_CETSHAREPOINTDATA_SQL = "
		SELECT * ";
			
	private static $FROM_TABLE_CETSHAREPOINTDATA_CLAUSE="
		FROM cet_sharepoint WHERE 1=1";
	private static $FILTER_CETSHAREPOINT_ON_SITEID = "
		AND SiteID = ?";
	private static $FILTER_CETSHAREPOINT_ON_PROJECTID = "
		AND ProjectID = ?";
		
	private static $FILTER_CETSHAREPOINT_ON_HEADATE = "
		AND HEADate BETWEEN ? AND ?";
		
	private static $FILTER_CETSHAREPOINT_ON_SCHEDULED = "
		AND InstallDate BETWEEN ? AND ?";
		
	private static $FILTER_CETSHAREPOINT_ON_NOTBILLED = "
		AND (AirSealing_BillingDate ='0000-00-00' AND Insulation_BillingDate ='0000-00-00')";
	
    public function getSharePointData($criteria){
        $paginationSelectString = self::$SELECT_CETSHAREPOINTDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_CETSHAREPOINTDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY HEADate DESC";

		if ($criteria->siteID){
            $filterString .= self::$FILTER_CETSHAREPOINT_ON_SITEID;
            $filterBinding .= "s";
            $filterParams[] = $criteria->siteID;
		}
		if ($criteria->projectID){
            $filterString .= self::$FILTER_CETSHAREPOINT_ON_PROJECTID;
            $filterBinding .= "s";
            $filterParams[] = $criteria->projectID;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_CETSHAREPOINT_ON_HEADATE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
		}
		if ($criteria->scheduledEndMonth){
            $filterString .= self::$FILTER_CETSHAREPOINT_ON_SCHEDULED;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->scheduledStartMonth);
            $filterParams[] = MySQLDateUpdate($criteria->scheduledEndMonth);
		}
		if ($criteria->notBilled){
            $filterString .= self::$FILTER_CETSHAREPOINT_ON_NOTBILLED;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CETDataProvider.getSharePointData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	
	
    private static $INSERT_SHAREPOINTDATA_SQL = "
        INSERT INTO cet_sharepoint_slim
        (
			ID, ContentType, SiteID, ProjectID,	LastName,
			FirstName, Status, PhoneNumber, Town, Utility,
			EnergySpecialist,HEADate,InspectionDate,AirSealing_BillingDate,AirSealing_FinalContractAmount,
			Insulation_BillingDate,Insulation_FinalContractAmount,TStat_BillingDate,TStat_FinalContractAmount
		) VALUES (
            ?,?,?,?,?,
            ?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?
        )"; //Use of Replace Into instead of INSERT INTO so that if a record matches index (currently set at SiteIdProjectId) it deletes the old record and adds a new record

	private function spdate($rawDate){
		$cleanDate = (date("Y",strtotime($rawDate)) == 1969 ? "0000-00-00" : date("Y-m-d",strtotime($rawDate)));
		return $cleanDate;
	}
	public function insertSharePointData($spData){
        $sqlString = self::$INSERT_SHAREPOINTDATA_SQL;
        $sqlBindings = "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "ssss";
		$heaDate = $this->spdate($spData->hea_x0020_date);
		$inspectionDate = $this->spdate($spData->inspection_x0020_date);
		$airsealingBillingDate = $this->spdate($spData->utility_x0020_billing_x0020_date);
		$insulationBillingDate = $this->spdate($spData->insulation_x0020_billing_x0020_d);
		$tstatBillingDate = $this->spdate($spData->tstat_x0020_billing_x0020_date);
        $sqlParams = array(
			$spData->id,$spData->contenttype,$spData->site_x0020_id,$spData->project_x0020_id,$spData->last_x0020_name,
			$spData->first_x0020_name,$spData->status,$spData->phone_x0020_number,$spData->town,$spData->utility,
			$spData->energy_x0020_specialist,$heaDate,$inspectionDate,$airsealingBillingDate,$spData->final_x0020__x0024_,
			$insulationBillingDate,$spData->insulation_x0020_final_x0020_con,$tstatBillingDate,$spData->tstat_x0020_final_x0020_contract
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	
	public static $SELECT_CRIEXTRACTDATABGAS_SQL = "SELECT * ";
	private static $FROM_TABLE_CRIEXTRACTDATABGAS_CLAUSE="
		FROM cri_extract_bgas WHERE 1=1";
	private static $FILTER_CRIEXTRACTDATABGAS_ON_SITEID = "
		AND SITEID = ?";
	private static $FILTER_CRIEXTRACTDATABGAS_ON_INSTDATE = "
		AND INSTALL_DT BETWEEN ? AND ?";
	private static $FILTER_CRIEXTRACTDATABGAS_ON_YEARMONTH = "
		AND YearMonth = ?";
		
    public function getCRIExtractDataBGAS($criteria){
        $paginationSelectString = self::$SELECT_CRIEXTRACTDATABGAS_SQL;
        $fromTableClause = self::$FROM_TABLE_CRIEXTRACTDATABGAS_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = "";

		if ($criteria->siteID){
            $filterString .= self::$FILTER_CRIEXTRACTDATABGAS_ON_SITEID;
            $filterBinding .= "s";
            $filterParams[] = $criteria->siteID;
		}
		if ($criteria->yearMonth){
            $filterString .= self::$FILTER_CRIEXTRACTDATABGAS_ON_YEARMONTH;
            $filterBinding .= "s";
            $filterParams[] = $criteria->yearMonth;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_CRIEXTRACTDATABGAS_ON_INSTDATE;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->startDate;
            $filterParams[] = $criteria->endDate;
		}
		
		if ($criteria->orderBy){
			$OrderBy = " ORDER BY ".$criteria->orderBy;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CETDataProvider.getCRIExtractDataBGAS failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		

	public static $SELECT_CRIOCP_SQL = "SELECT * ";
	private static $FROM_TABLE_CRIOCP_CLAUSE="
		FROM cri_ocp WHERE 1=1";
	private static $FILTER_CRIOCP_ON_SITEID = "
		AND SITEID = ?";
	private static $FILTER_CRIOCP_ON_PROPOSEDDATE = "
		AND PROPOSED_DT BETWEEN ? AND ?";
	private static $FILTER_CRIOCP_ON_PRIMARYPROVIDER ="
		AND PRIMARY_PROVIDER = ?";
    public function getCRIOCP($criteria){
        $paginationSelectString = self::$SELECT_CRIOCP_SQL;
        $fromTableClause = self::$FROM_TABLE_CRIOCP_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = " ORDER BY SITEID";

		if ($criteria->siteID){
            $filterString .= self::$FILTER_CRIOCP_ON_SITEID;
            $filterBinding .= "s";
            $filterParams[] = $criteria->siteID;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_CRIOCP_ON_PROPOSEDDATE;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->startDate;
            $filterParams[] = $criteria->endDate;
		}
		if ($criteria->primaryProvider){
            $filterString .= self::$FILTER_CRIOCP_ON_PRIMARYPROVIDER;
            $filterBinding .= "s";
            $filterParams[] = $criteria->primaryProvider;
		}
		if ($criteria->crewIn){
            $filterString .= " AND CREW IN ('".$criteria->crewIn."')";
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CETDataProvider.getCRIOCP failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	
	public function deleteExtractDataBGAS($criteria){
        $sqlString = "DELETE FROM cri_extract_bgas WHERE YearMonth=?";
        $sqlBindings = "s";
        $sqlParams = array($criteria->yearMonth);
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	
	
    private static $INSERT_EXTRACTBGASDATA_SQL = "
        INSERT INTO cri_extract_bgas
        (
			SITEID,CUST_FIRST_NAME,CUST_LAST_NAME,ADDRESS,CITY,
			STATE,ZIP,PLUS4,NUM_UNITS,PRIMARY_PRVD,
			PROVIDERID,PROVIDERCODE,ACCTCUST,LOCATIONID,SECONDARY_PRVD,
			PROVIDERID2,PROVIDERCODE2,ACCTCUST2,HEATFUEL,DHWFUEL,
			CUST_TYPE,APPTDATE,APPTTYPE,AUDITOR_FIRST_NAME,AUDITOR_LAST_NAME,
			CONTRACT,MEASTABLE,ID0,ID1,DESCRIPTION,
			PROP_PARTID,PROP_QTY,INS_PARTID,INS_QTY,MBTU_SAVINGS,
			DEPOSIT,CUST_PRICE,UTIL_PRICE,PAYEE,ISSUED_DT,
			SIGNED_DT,INSTALL_DT,INV_STATUS,MEAS_STATUS,INSTALLER_NAME,
			MEASURELIFE,ICTV_AWARE_SIR,ICTV_AWARE_SIMPLEPAYBACK,Therms,YearMonth

		) VALUES (
            ?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?,
            ?,?,?,?,?
        )"; 

		
		/*  
		
		
		
			
		*/

		
	public function insertExtractDataBGAS($records,$criteria){
//		print_pre($records);
//		echo "<hr>YOSH<hr>";
        $sqlString = self::$INSERT_EXTRACTBGASDATA_SQL;
        $sqlBindings = "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "ssiis";
        $sqlBindings .= "ssiss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sisid";
        $sqlBindings .= "dddss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "dddds";
		$coutner = 0;
		foreach ($records as $data){
//		if ($counter < 1){
			$sqlParams = array(
				$data["SITEID"],$data["CUST_FIRST_NAME"],$data["CUST_LAST_NAME"],$data["ADDRESS"],$data["CITY"],
				$data["STATE"],$data["ZIP"],$data["PLUS4"],$data["NUM_UNITS"],$data["PRIMARY_PRVD"],
				$data["PROVIDERID"],$data["PROVIDERCODE"],$data["ACCTCUST"],$data["LOCATIONID"],$data["SECONDARY_PRVD"],
				$data["PROVIDERID2"],$data["PROVIDERCODE2"],$data["ACCTCUST2"],$data["HEATFUEL"],$data["DHWFUEL"],
				$data["CUST_TYPE"],$data["APPTDATE"],$data["APPTTYPE"],$data["AUDITOR_FIRST_NAME"],$data["AUDITOR_LAST_NAME"],
				$data["CONTRACT"],$data["MEASTABLE"],$data["ID0"],$data["ID1"],$data["DESCRIPTION"],
				$data["PROP_PARTID"],$data["PROP_QTY"],$data["INS_PARTID"],$data["INS_QTY"],$data["MBTU_SAVINGS"],
				$data["DEPOSIT"],$data["CUST_PRICE"],$data["UTIL_PRICE"],$data["PAYEE"],$data["ISSUED_DT"],
				$data["SIGNED_DT"],$data["INSTALL_DT"],$data["INV_STATUS"],$data["MEAS_STATUS"],$data["INSTALLER_NAME"],
				$data["MEASURELIFE"],$data["ICTV_AWARE_SIR"],$data["ICTV_AWARE_SIMPLEPAYBACK"],$data["Therms"],$criteria->yearMonth	
			);
				//echo $data["SIGNED_DT"]." ".$data["INSTALL_DT"]."<br>";
				//print_pre($sqlParams);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->records = $insertResponse->records+1;
				$insertResponses['successes'][$counter] = $insertResponse;
			} else {
				$insertResponses['errors'][$counter] = $insertResponse;
				trigger_error("CETDataProvider.insertExtractDataBGAS failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
			$counter++;
//		}
		}
        return $insertResponses;
    }	
	public static $SELECT_PROPOSEDEZDOCDATA_SQL = "SELECT * ";
	private static $FROM_TABLE_PROPOSEDEZDOCDATA_CLAUSE="
		FROM residential_ezdocdata WHERE 1=1 AND ResidentialEZDocData_Status = 'Current' AND ResidentialEZDocData_BGASExtractedDate='0000-00-00'";
	private static $FILTER_PROPOSEDEZDOCDATA_ON_SITEID = "
		AND ResidentialEZDocData_SiteID = ?";
	private static $FILTER_PROPOSEDEZDOCDATA_ON_PROPOSEDDATE = "
		AND ResidentialEZDocData_CreatedDate BETWEEN ? AND ?";
    public function getProposedEZDocData($criteria){
        $paginationSelectString = self::$SELECT_PROPOSEDEZDOCDATA_SQL;
        $fromTableClause = self::$FROM_TABLE_PROPOSEDEZDOCDATA_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		$OrderBy = "";

		if ($criteria->siteID){
            $filterString .= self::$FILTER_PROPOSEDEZDOCDATA_ON_SITEID;
            $filterBinding .= "s";
            $filterParams[] = $criteria->siteID;
		}
		if ($criteria->proposedEndDate){
            $filterString .= self::$FILTER_PROPOSEDEZDOCDATA_ON_PROPOSEDDATE;
            $filterBinding .= "ss";
            $filterParams[] = $criteria->proposedStartDate;
            $filterParams[] = $criteria->proposedEndDate;
		}
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$OrderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("CETDataProvider.getProposedEZDocData failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }		
	
	
}
?>