<?php
/*
 * Some common methods for paginated queries.
 */

class PaginatedQueryParameters {

    /**
     * This string is pre-pended to the FROM clause, and is required to identify the column data to be returned.
     * e.g., SELECT field1, field2, field2
     * @var string
     */
    public $selectColumnsClause;

    /**
     * This string is the FROM clause that will be appended to the column string for selecting rows, and also to the
     * count select which retrieves the full count.
     * Include all joins in this clause.
     * e.g., FROM table1
     * @var string
     */
    public $fromClause;

    /**
     * This string is the complete WHERE clause.
     * @var string
     */
    public $filterString;

    private $filterBinding = array();
    private $filterParams = array();

    /**
     * Adds bindings and parameters for filters.
     * @param string $typeLetter
     * @param mixed $value
     */
    public function addFilterParameter($typeLetter, $value){
        $this->filterBinding[] = $typeLetter;
        $this->filterParams[] = $value;
    }

    public function getFilterBindings(){
        return implode("", $this->filterBinding);
    }

    public function getFilterParams(){
        return $this->filterParams;
    }

    public $defaultOrderByClause;

    private $orderByFieldToColumnMapping = array();

    /**
     * Maps names in the sortColumn or multiSortColumn properties to db columns that will be used to construct an order by clause
     * @param array $mapping
     * @internal param string $uiFieldName
     * @internal param string $dbColumns
     */
    public function addOrderByMapping(array $mapping){
        if (is_array($mapping)){
            foreach ($mapping as $uiFieldName => $dbColumns){
                if ($uiFieldName && $dbColumns){
                    $this->orderByFieldToColumnMapping[$uiFieldName] = $dbColumns;
                }
            }
        }
    }

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = $this->defaultOrderByClause ? $this->defaultOrderByClause : "";
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            $mappedColumns = array_key_exists($criteria->sortColumn, $this->orderByFieldToColumnMapping) ? $this->orderByFieldToColumnMapping[$criteria->sortColumn] : null;
            if ($mappedColumns){
                $orderBy = " ORDER BY ".$mappedColumns." ".$sortOrder;
            }
        }
        // multi-column always trumps single column if it exists
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                $mappedColumns = array_key_exists($multiColumnSort->column, $this->orderByFieldToColumnMapping) ? $this->orderByFieldToColumnMapping[$multiColumnSort->column] : null;
                if ($mappedColumns){
                    $orderByStrings[] = $mappedColumns." ".$sortOrder;
                }
            }
            if (count($orderByStrings)){
                $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
            }
        }
        return $orderBy;
    }

    public function getCountSqlString(){
        return "SELECT COUNT(*) AS count ".$this->fromClause.$this->filterString;
    }

    public function getFullSqlString(PaginationCriteria $criteria){
        return $this->selectColumnsClause.$this->fromClause.$this->filterString.$this->getOrderByClause($criteria);
    }
}

/**
 * Class PaginatedDataProvider
 * This class handles all the generic actions that are always done for a paginated query.
 */
abstract class PaginatedDataProvider {

    /* @var mysqli */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    /**
     * Returns a PaginatedQueryParameters object ready to be executed
     * @param PaginationCriteria $criteria
     * @return PaginatedQueryParameters
     */
    abstract protected function getQueryParameters(PaginationCriteria $criteria);

    /**
     * Returns a string that resembles the following:
     *
     *  AND (
     *      t.TagList_Name LIKE ?
     *      OR t.TagList_Description LIKE ?
     *      OR t.TagList_CampaignHeading LIKE ?
     *      OR t.TagList_CampaignSubHeading LIKE ?
     *      OR t.TagList_CampaignDescription LIKE ?
     *      OR t.TagList_URL LIKE ?
     *      OR t.TagList_CallToAction LIKE ?
     *  )
     * @return string
     */
    abstract protected function getKeywordFilterClause();

    /**
     * This is where the provider can map the ui field names to the db columns. A field name is what the ui passes back in reference to
     * a datatable column, and the db columns refer to a comma-delimited string of database column names that should be used in the sort.
     * Although in theory, multiple columns might be used to sort a single datatable column, this can have unexpected side effects with respect
     * to the ASC and DESC modifiers.
     *
     * EXAMPLE:
     *   return array(
     *      "id" => "id",
     *      "pageTitle" -> "page_title"
     *   )
     *
     * Be sure to use the db alias if your FROM clause requires it:
     *
     *   return array(
     *      "id" => "myTable.id",
     *      "pageTitle" => "myJoinTable.pageTitle"
     *   )
     *
     * @return array mapping UIFIeldNames to dbColumns
     */
    abstract protected function getOrderByMapping();

    /**
     * Returns an object that represents the desired output object for the collection (e.g., contract record).
     * @param array $rowArray
     * @return mixed
     */
    abstract protected function mapTo(array $rowArray);

    public function get(PaginationCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $queryParameters = $this->getQueryParameters($criteria);
        $queryParameters->addOrderByMapping($this->getOrderByMapping());

        $keywordFilterClause = $this->getKeywordFilterClause();
        if ($criteria->keyword && $keywordFilterClause){
            $keywordCriteria = "%".$criteria->keyword."%";
            $queryParameters->filterString .= $keywordFilterClause;
            $numParams = substr_count($keywordFilterClause, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $queryParameters->addFilterParameter("s", $keywordCriteria);
            }
        }
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // get the total count
        $countSqlString = $queryParameters->getCountSqlString();
        $queryResponse = MySqliHelper::getResultWithLogging(get_class($this).".get while getting record count: error: ", $this->mysqli, $countSqlString, $queryParameters->getFilterBindings(), $queryParameters->getFilterParams());

        if ($queryResponse->success){
            $result->totalRecords = $queryResponse->resultArray[0]['count'] ? $queryResponse->resultArray[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $queryParameters->getFullSqlString($criteria).$limiter;
                $queryResponse = MySqliHelper::getResultWithLogging(get_class($this).".get while getting full data: error: ", $this->mysqli, $pageSqlString, $queryParameters->getFilterBindings(), $queryParameters->getFilterParams());

                if ($queryResponse->success){
                    foreach ($queryResponse->resultArray as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = $this->mapTo($row);
                    }
                } else {
                    trigger_error(get_class($this).".get failed to get paginated results: (".$pageSqlString.") error: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error(get_class($this).".get failed to get count of results: (".$countSqlString.") error:".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }
}