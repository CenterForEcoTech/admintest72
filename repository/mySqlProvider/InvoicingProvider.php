<?php
class InvoicingProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT 
			InvoicingCodes_ID as id,
			InvoicingCodes_DisplayName as displayName,
			InvoicingCodes_Codes as codes,
			InvoicingCodes_InvoicingNumberName as invoicingNumberName,
			InvoicingCodes_FiscalYearStart as fiscalYearStart,
			InvoicingCodes_FiscalYearEnd as fiscalYearEnd,
			InvoicingCodes_Budget as budget,
			InvoicingCodes_BudgetNonCodeRelated as budgetNonCodeRelated,
			InvoicingCodes_BillingRate as billingRate,
			InvoicingCodes_FIN as fin,
			InvoicingCodes_SF270Name as sf270Name,
			InvoicingCodes_ProcessRequirements as processRequirements,
			InvoicingCodes_ProcessTrigger as processTrigger,
			InvoicingCodes_ProcessFile as processFile,
			InvoicingCodes_ProcessRecipient as processRecipient,
			InvoicingCodes_Status as status,
			InvoicingCodes_LastUpdatedBy as lastUpdatedBy,
			InvoicingCodes_LastUpdatedDate as lastUpdatedDate,
			InvoicingCodes_Notes as notes";
	
    private static $FROM_TABLE_CLAUSE = "
        FROM invoicing_codes
        WHERE 1 = 1";

    public function getInvoicingCodes($criteria){
		
        $sqlSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
		$filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY InvoicingCodes_DisplayName";

		if ($criteria->displayName){
			$filterString .= " AND InvoicingCodes_DisplayName = ?"; // show only those with display order by default
			$filterBinding .= "s";
			$filterParams[] = $criteria->displayName;
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
				$sqlString = $sqlSelectString.$fromTableClause.$filterString.$orderBy;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("InvoicingProvider.getInvoicingCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("InvoicingProvider.getInvoicingCodes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
	
    private static $UPDATE_INVOCINGNOTES_SQL = "
		UPDATE invoicing_codes SET InvoicingCodes_Notes = ?, InvoicingCodes_LastUpdatedBy = ? WHERE InvoicingCodes_DisplayName = ?";
	
	public function updateInvoicingNotes($record,$adminId){
        $sqlString = self::$UPDATE_INVOCINGNOTES_SQL;
        $sqlBinding = "sss";
		$sqlParam = array($record->note,$adminId,$record->invoicingCodeName);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("InvoicingProvider.insertInvoicingNotes failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}	
	
	
    private static $SELECT_STATUSHISTORY_SQL = "
        SELECT * FROM invoicing_statushistory WHERE 1 = 1";

    public function getInvoicingStatusHistory($criteria){
		
        $sqlSelectString = self::$SELECT_STATUSHISTORY_SQL;
		$filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY InvoicingStatusHistory_TimeStamp DESC";

		if ($criteria->displayName){
			$filterString .= " AND InvoicingStatusHistory_DisplayName = ?"; // show only those with display order by default
			$filterBinding .= "s";
			$filterParams[] = $criteria->displayName;
		}
		if ($criteria->type){
			$filterString .= " AND InvoicingStatusHistory_Type = ?"; // show only those with display order by default
			$filterBinding .= "s";
			$filterParams[] = $criteria->type;
		}
		
		$sqlString = $sqlSelectString.$fromTableClause.$filterString.$orderBy;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("InvoicingProvider.getInvoicingStatusHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	

    public function getInvoicingFileConverterFields($criteria){
		
        $sqlSelectString = "SELECT * FROM invoicing_fileconverter WHERE 1 = 1";
		$filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();
		$orderBy = " ORDER BY OrderID";

		if ($criteria->recordType){
			$filterString .= " AND RecordType = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->recordType;
		}
		
		if ($criteria->destination){
			$filterString .= " AND Destination = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->destination;
		}
		
		$sqlString = $sqlSelectString.$filterString.$orderBy;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("InvoicingProvider.getInvoicingFileConverterFields failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	
	public function insertInvoicingAmount($record,$adminId){
		$sqlString = "
			INSERT INTO invoicing_amounthistory
			(
				invoiceDate,invoiceCode,invoiceNumber,invoiceAmount,nonStaffAmount,sourcePage,addedBy
			) VALUES (
				?,?,?,?,?,?,?
			)";
        $sqlBinding = "sssddss";
        $sqlParam = array(
			$record->invoiceDate,
			$record->invoiceCode,
			$record->invoiceNumber,
			$record->invoiceAmount,
			$record->nonStaffAmount,
			$record->sourcePage,
			$adminId
			);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("InvoiceProvider.insertInvoicingAmount failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
	}
	
    private static $SELECT_AMOUNTHISTORY_SQL = "
		SELECT * FROM invoicing_amounthistory as a where id in (
			SELECT max(id) FROM invoicing_amounthistory as b WHERE b.invoiceNumber != ? AND b.invoiceCode = ? AND b.invoiceDate >= ? GROUP BY invoiceNumber 
		)";
    public function getInvoicingAmountHistory($criteria){
        $sqlSelectString = self::$SELECT_AMOUNTHISTORY_SQL;
		//echo $sqlSelectString;
		$filterString = ""; // show only those with display order by default
        $filterBinding = "sss";
        $filterParams = array($criteria->invoiceNumber,$criteria->invoiceCode,$criteria->startDate);
		$orderBy = "  ORDER BY timestamp DESC";
		
		$sqlString = $sqlSelectString.$fromTableClause.$filterString.$orderBy;
		//echo $sqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("InvoicingProvider.getInvoicingAmountHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
    private static $SELECT_HISTORY_SQL = "
		SELECT * FROM invoicing_amounthistory WHERE 1=1";
    public function getInvoicingHistory($criteria){
        $sqlSelectString = self::$SELECT_HISTORY_SQL;
		$filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "  ORDER BY timestamp DESC";
		
		if ($criteria->invoiceCode){
			$filterString .= " AND invoiceCode = ?";
			$filterBinding .= "s";
			$filterParams[] = $criteria->invoiceCode;
		}
		if ($criteria->invoiceCodeStartsWith){
			$filterString .= " AND invoiceCode LIKE '".$criteria->invoiceCodeStartsWith."%'";
		}
		if ($criteria->invoiceDateStart){
			$filterString .= " AND invoiceDate BETWEEN ? AND ?";
			$filterBinding .= "ss";
			$filterParams[] = $criteria->invoiceDateStart;
			$filterParams[] = $criteria->invoiceDateEnd;
		}
		
		$sqlString = $sqlSelectString.$fromTableClause.$filterString.$orderBy;
		//echo $sqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("InvoicingProvider.getInvoicingHistory failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
    private static $UPDATE_INVOICINGCODES_SQL = "
        UPDATE invoicing_codes
        SET
			InvoicingCodes_DisplayName = ?,
			InvoicingCodes_Codes = ?,
			InvoicingCodes_InvoicingNumberName = ?,
			InvoicingCodes_FiscalYearStart = ?,
			InvoicingCodes_FiscalYearEnd = ?,
			InvoicingCodes_Budget = ?,
			InvoicingCodes_BudgetNonCodeRelated = ?,
			InvoicingCodes_BillingRate = ?,
			InvoicingCodes_LastUpdatedBy =?
		WHERE InvoicingCodes_ID = ?";

    public function updateInvoicingCodes($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_INVOICINGCODES_SQL;
            $sqlBinding = "sssssddssi";
			$budget = str_replace(",","",$record->budget);
			$budget = str_replace("\$","",$budget);
			$budgetNonCodeRelated = str_replace(",","",$record->budgetNonCodeRelated);
			$budgetNonCodeRelated = str_replace("\$","",$budgetNonCodeRelated);
            $sqlParams = array(
				trim($record->displayName),
				trim($record->codes),
				trim($record->invoicingNumberName),
				MySQLDateUpdate($record->fiscalYearStart),
				MySQLDateUpdate($record->fiscalYearEnd),
				trim($budget),
				trim($budgetNonCodeRelated),
				trim($record->billingRate),
				$adminUserId,
				$record->id
            );
			
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("InvoicingProvider.updateInvoicingCodes failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
    public function updateInvoicingCodes_justCodes($CodesInThisInvoice,$id){
        $updateResponse = new ExecuteResponseRecord();
			$CodesInThisInvoice = implode(",",$CodesInThisInvoice);
            $sqlString = "UPDATE invoicing_codes SET InvoicingCodes_Codes = ? WHERE InvoicingCodes_ID = ?";
            $sqlBinding = "si";
            $sqlParams = array($CodesInThisInvoice,$id);
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("InvoicingProvider.updateInvoicingCodes_justCodes failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        return $updateResponse;
    }	
	
    private static $UPDATE_HISTORYNONSTAFFAMOUNT_SQL = "
		UPDATE invoicing_amounthistory SET nonStaffAmount = ? WHERE id = ?";
	
	public function updateInvoiceHistoryNonStaffAmount($record){
        $sqlString = self::$UPDATE_HISTORYNONSTAFFAMOUNT_SQL;
        $sqlBinding = "di";
		$sqlParam = array($record->nonStaffAmount,$record->invoiceId);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("InvoicingProvider.updateInvoiceHistoryNonStaffAmount failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}	
	public function addInvoicingCodes($record,$adminId){
		$sqlString = "INSERT INTO invoicing_codes
			(
				InvoicingCodes_DisplayName,
				InvoicingCodes_InvoicingNumberName,
				InvoicingCodes_FiscalYearStart,
				InvoicingCodes_FiscalYearEnd,
				InvoicingCodes_Budget,
				InvoicingCodes_BudgetNonCodeRelated,
				InvoicingCodes_LastUpdatedBy
			) VALUES (
				?,?,?,?,?,?,?
			)";
        $sqlBinding = "ssssdds";
			$budget = str_replace(",","",$record->budget);
			$budget = str_replace("\$","",$budget);
			$budgetNonCodeRelated = str_replace(",","",$record->budgetNonCodeRelated);
			$budgetNonCodeRelated = str_replace("\$","",$budgetNonCodeRelated);
        $sqlParam = array(
			$record->displayName,
			$record->invoicingNumberName,
			MySQLDateUpdate($record->fiscalYearStart),
			MySQLDateUpdate($record->fiscalYearEnd),
			$budget,
			$budgetNonCodeRelated,
			$adminId
			);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("InvoiceProvider.addInvoicingCodes failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;

	}
	
	public function insertInvoiceHistoryCodeAdjust($record,$adminId){
		$sqlString = "
			INSERT INTO invoicing_amounthistory_codeshift
			(
				invoiceDate,invoiceCode,invoiceNumber,invoiceAmount,invoiceCodeName,invoiceNote,addedBy
			) VALUES (
				?,?,?,?,?,?,?
			)";
        $sqlBinding = "sssdsss";
        $sqlParam = array(
			MySQLDateUpdate($record->invoiceDate),
			$record->invoiceCode,
			$record->invoiceNumber,
			$record->invoiceAmount,
			$record->invoiceCodeName,
			$record->invoiceNote,
			$adminId
			);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("InvoiceProvider.insertInvoiceHistoryCodeAdjust failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
	}
	public function getInvoiceHistoryCodeAdjust($criteria){
		$sqlSelectString = "SELECT * FROM invoicing_amounthistory_codeshift WHERE 1=1";
		$filterString = " AND (status != 'Deleted' || status IS NULL)" ; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();
		$orderBy = "  ORDER BY invoiceDate DESC";
		
		if ($criteria->invoiceCode){
			$filterString .= " AND invoiceCode=?"; // show only those with display order by default
			$filterBinding = "s";
			$filterParams[] = $criteria->invoiceCode;
		}
		if ($criteria->invoiceCodeStartsWith){
			$filterString .= " AND invoiceCode LIKE '".$criteria->invoiceCodeStartsWith."%'"; // show only those with display order by default
		}
		if ($criteria->endDate){
			$filterString .= " AND invoiceDate BETWEEN ? AND ?";
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}
		
		
		$sqlString = $sqlSelectString.$filterString.$orderBy;
		//echo $sqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("InvoicingProvider.getInvoiceHistoryCodeAdjust failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
	}
	
	public function deleteCodeAdjustment($record,$adminId){
        $sqlString = "UPDATE invoicing_amounthistory_codeshift SET status='Deleted', statusUpdatedBy=? WHERE id=?";
        $sqlBinding = "si";
		$sqlParam = array($adminId,$record->id);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("InvoicingProvider.deleteCodeAdjustment failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
		
	}	
}
?>