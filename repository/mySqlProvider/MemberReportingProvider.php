<?php
include_once("Helper.php");
class MemberReportingProvider
{
	protected $mysqli;

	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

    // TODO: move this into the MerchantMemberProvider -- it is only ever used for public merchant profile and merchant profile edit
	public function getTotals($merchantId){
		$sqlString = "SELECT SUM(EventTransaction_GeneratedAmount) AS TotalDonation, SUM(EventTransaction_EligibleAmount) AS TotalSales FROM event_transaction WHERE EventTransaction_Status !='Deleted' AND EventTransaction_MerchantID=?";
	    $sqlParams = array($merchantId);
        $sqlBinding = "i";
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return($result);
	}
}
?>
