<?php
class HREmployeeProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
			e.HREmployeeName_ID as id,
			e.HREmployeeName_FirstName as firstName,
			e.HREmployeeName_LastName as lastName,
			CONCAT(e.HREmployeeName_FirstName,' ',e.HREmployeeName_LastName) as fullName,
			e.HREmployeeName_Email as email,
			e.HREmployeeName_HireDate as hireDate,
			e.HREmployeeName_EndDate as endDate,
			e.HREmployeeName_OfficeLocation as officeLocation,
			e.HREmployeeName_DisplayOrderID as displayOrderId,
			e.HREmployeeName_Department as department,
			e.HREmployeeName_DepartmentID as departmentId,
			e.HREmployeeName_Title as title,
			e.HREmployeeName_TitleID as titleId,
			e.HREmployeeName_CategoryID as categoryId,
			e.HREmployeeName_Manager as manager,
			e.HREmployeeName_IsManager as isManager,
			e.HREmployeeName_TrainingCodes as trainingCodes,
			e.HREmployeeName_HoursTrackingGBS as hoursTrackingGBS,
			e.HREmployeeName_HoursTrackingGHS as hoursTrackingGHS,
			e.HREmployeeName_RevenueCode as revenueCode,
			e.HREmployeeName_ExcludeFromHoursEmail as excludeFromHoursEmail,
			e.HREmployeeName_ExcludeFromProjectionSummary as excludeFromProjectionSummary,
			e.HREmployeeName_HES as hes";

    private static $FROM_TABLE_CLAUSE = "
        FROM hr_employee_name as e
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND e.HREmployeeName_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID = "
        AND e.HREmployeeName_ID = ?";

    private static $FILTER_ON_LOCATION = "
        AND e.HREmployeeName_OfficeLocation = ?";
		
    private static $FILTER_ON_ISMANAGER = "
        AND e.HREmployeeName_IsManager = ?";
		
    private static $FILTER_ON_DEPARTMENT = "
        AND e.HREmployeeName_Department = ?";
		
    private static $FILTER_ON_DEPARTMENTID = "
        AND e.HREmployeeName_DepartmentID = ?";
		
    private static $FILTER_ON_TITLEID = "
        AND e.HREmployeeName_TitleID = ?";
    private static $FILTER_ON_CATEGORYID = "
        AND e.HREmployeeName_CategoryID = ?";
		
/*
    private static $FILTER_ON_HOURSTRACKINGGBS = "
        AND (e.HREmployeeName_Department = ? OR e.HREmployeeName_HoursTrackingGBS = ?)";
		
    private static $FILTER_ON_HOURSTRACKINGGHS = "
        AND (e.HREmployeeName_Department = ? OR e.HREmployeeName_HoursTrackingGHS = ?)";
*/
    private static $FILTER_ON_HOURSTRACKINGGBS = "
        AND (e.HREmployeeName_DepartmentID = ? OR e.HREmployeeName_HoursTrackingGBS = ?)";
		
    private static $FILTER_ON_HOURSTRACKINGGHS = "
        AND (e.HREmployeeName_DepartmentID = ? OR e.HREmployeeName_HoursTrackingGHS = ?)";
		
    private static $FILTER_ON_MANAGER = "
        AND e.HREmployeeName_Manager = ?";
		
    private static $FILTER_ON_EMAIL = "
        AND e.HREmployeeName_Email = ?";
		
    private static $FILTER_ON_HES = "
        AND e.HREmployeeName_HES = ?";
				
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->officeLocation){
            $filterString .= self::$FILTER_ON_LOCATION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->officeLocation;
		}
		if ($criteria->email){
            $filterString .= self::$FILTER_ON_EMAIL;
            $filterBinding .= "s";
            $filterParams[] = $criteria->email;
		}
		if ($criteria->isManager){
            $filterString .= self::$FILTER_ON_ISMANAGER;
            $filterBinding .= "i";
            $filterParams[] = 1;
		}
		if ($criteria->hoursTrackingGBS){
            $filterString .= self::$FILTER_ON_HOURSTRACKINGGBS;
            $filterBinding .= "ii";
            $filterParams[] = $criteria->departmentId;
            $filterParams[] = 1;
			$criteria->department = false;
		}
		if ($criteria->hoursTrackingGHS){
            $filterString .= self::$FILTER_ON_HOURSTRACKINGGHS;
            $filterBinding .= "ii";
            $filterParams[] = $criteria->departmentId;
            $filterParams[] = 1;
			$criteria->department = false;
		}
		if ($criteria->notExcludedFromHourEmail){
            $filterString .= " AND e.HREmployeeName_ExcludeFromHoursEmail < 1";
		}
		
		if ($criteria->isHES){
            $filterString .= " AND e.HREmployeeName_HES > 0";
		}
		
		if ($criteria->department){
            $filterString .= self::$FILTER_ON_DEPARTMENT;
            $filterBinding .= "s";
            $filterParams[] = $criteria->department;
		}
		if ($criteria->departmentId){
            $filterString .= self::$FILTER_ON_DEPARTMENTID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->departmentId;
		}
		if ($criteria->titleId){
            $filterString .= self::$FILTER_ON_TITLEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->titleId;
		}
		if ($criteria->categoryId){
            $filterString .= self::$FILTER_ON_CATEGORYID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->categoryId;
		}
		if ($criteria->manager){
            $filterString .= self::$FILTER_ON_MANAGER;
            $filterBinding .= "s";
            $filterParams[] = $criteria->manager;
		}
		if ($criteria->trainingCodes){
            $filterString .= " AND (";
			$trainingCodes = explode(",",$criteria->trainingCodes);
			foreach ($trainingCodes as $trainingCode){
				$filterString .= " e.HREmployeeName_TrainingCodes LIKE ? OR";
				$filterBinding .= "s";
				$filterParams[] = $trainingCode;
			}
			$filterString = rtrim($filterString,"OR");
            $filterString .= ")";
		}
		if ($criteria->withIDs){
	          $filterString .= " AND e.HREmployeeName_ID IN (".$criteria->withIDs.")";
  		}
		if ($criteria->revenueCode){
            $filterString .= " AND e.HREmployeeName_RevenueCode = ?";
            $filterBinding .= "s";
            $filterParams[] = $criteria->revenueCode;
		}
		if ($criteria->isActive){
            $filterString .= " AND e.HREmployeeName_EndDate = '0000-00-00'";
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("HREmployeeProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.HREmployeeName_FirstName ASC, e.HREmployeeName_LastName ASC";

    private function getOrderByClause( $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY e.HREmployeeName_ID ".$sortOrder;
                    break;
                case "officeLocation":
                    $orderBy = " ORDER BY e.HREmployeeName_OfficeLocation ".$sortOrder;
                    break;
				case "isManager":
					$orderBy = " ORDER BY e.HREmployeeName_IsManager ".$sortOrder;
					break;
				case "manager":
					$orderBy = " ORDER BY e.HREmployeeName_Manager ".$sortOrder;
					break;
				case "department":
					$orderBy = " ORDER BY e.HREmployeeName_Department ".$sortOrder;
					break;
				case "departmentId":
					$orderBy = " ORDER BY e.HREmployeeName_DepartmentID ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "e.HREmployeeName_ID ".$sortOrder;
                        break;
                    case "officeLocation":
                        $orderByStrings[] = "e.HREmployeeName_OfficeLocation ".$sortOrder;
                        break;
					case "isManager":
						$orderByStrings[] = "e.HREmployeeName_IsManager ".$sortOrder;
						break;
					case "manager":
						$orderByStrings[] = "e.HREmployeeName_Manager ".$sortOrder;
						break;
					case "department":
						$orderByStrings[] = "e.HREmployeeName_Department ".$sortOrder;
						break;
					case "departmentId":
						$orderByStrings[] = "e.HREmployeeName_DepartmentID ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return HREmployeeRecord::castAs($dataRow);
    }

    public function getOrgChart($criteria,$managerCount){
        $SelectString = "SELECT ";
		$FromString = " FROM hr_employee_name as e1 ";
		$WhereString = " WHERE 1";
		$OrderByString = " ORDER BY";

        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        // apply search
		$thisDisplayID = "";
		if ($criteria->department){
			$sqlWhereString[] = $thisDisplayID." AND e2.HREmployeeName_Department =?";
			
			$filterBinding .= "s";
			$filterParams[] = $criteria->department;
/*		
			for ($c=1;$c<=2;$c++){
				$sqlWhereString[] = " AND e".$c.".HREmployeeName_Department =?";
				$filterBinding .= "s";
				$filterParams[] = $criteria->department;
			}
*/
		}
		if ($criteria->departmentId){
			$sqlWhereString[] = $thisDisplayID." AND e2.HREmployeeName_DepartmentID =?";
			
			$filterBinding .= "i";
			$filterParams[] = $criteria->departmentId;
		}
		if ($criteria->titleId){
			$sqlWhereString[] = $thisDisplayID." AND e2.HREmployeeName_TitleID =?";
			
			$filterBinding .= "i";
			$filterParams[] = $criteria->titleId;
		}
		if ($criteria->categoryId){
			$sqlWhereString[] = $thisDisplayID." AND e2.HREmployeeName_CategoryID =?";
			
			$filterBinding .= "i";
			$filterParams[] = $criteria->categoryId;
		}
		for ($c=1;$c<=$managerCount;$c++){
			$sqlSelectString[] = " CONCAT(e".$c.".HREmployeeName_FirstName,' ', e".$c.".HREmployeeName_LastName) AS ManagerLevel".$c;
			$sqlFromString[] = " LEFT JOIN hr_employee_name as e".($c+1)." ON e".($c+1).".HREmployeeName_Manager = CONCAT(e".$c.".HREmployeeName_FirstName,' ', e".$c.".HREmployeeName_LastName)";
			$orderByString[] = " ManagerLevel".$c." DESC";
		}
	
		$pageSqlString = $SelectString.implode(",",$sqlSelectString).$FromString.implode(" ",$sqlFromString).$WhereString.implode(" ",$sqlWhereString).$OrderByString.implode(",",array_reverse($orderByString));
		//echo $pageSqlString.$criteria->department;
		//echo $filterBinding;
		//print_pre($filterParams);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("HREmployeeProvider.getOrgChart failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
		
        return $result;
    }
	
    // add
    private static $INSERT_SQL = "
		INSERT INTO hr_employee_name
		(
			HREmployeeName_FirstName,
			HREmployeeName_LastName,
			HREmployeeName_Email,

			HREmployeeName_HireDate,
			HREmployeeName_EndDate,
			HREmployeeName_OfficeLocation,
			HREmployeeName_DisplayOrderID,

			HREmployeeName_Department,
			HREmployeeName_DepartmentID,
			HREmployeeName_Title,
			HREmployeeName_TitleID,
			HREmployeeName_CategoryID,
			HREmployeeName_Manager,
			HREmployeeName_IsManager,
			HREmployeeName_HoursTrackingGBS,
			HREmployeeName_HoursTrackingGHS,
			
			HREmployeeName_RevenueCode,
			HREmployeeName_ExcludeFromHoursEmail,
			HREmployeeName_ExcludeFromProjectionSummary,
			HREmployeeName_HES
			
		) VALUES (
			?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,?,?,?,
			?,?,?,?
		)";

    public function add(HREmployeeRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "ssssssisisiisiiisiii";
        $sqlParam = array(
			$record->firstName,
			$record->lastName,
			$record->email,

			MySQLDateUpdate($record->hireDate),
			MySQLDateUpdate($record->endDate),
			$record->officeLocation,
			$record->displayOrderId,
			
			$record->department,
			$record->departmentId,
			$record->title,
			$record->titleId,
			$record->categoryId,
			$record->manager,
			$record->isManager,
			$record->hoursTrackingGBS,
			$record->hoursTrackingGHS,
			
			$record->revenueCode,
			$record->excludeFromHoursEmail,
			$record->excludeFromProjectionSummary,
			$record->hes

        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL = "
        UPDATE hr_employee_name
        SET
			HREmployeeName_FirstName = ?,
			HREmployeeName_LastName = ?,
			HREmployeeName_Email = ?,

			HREmployeeName_HireDate = ?,
			HREmployeeName_EndDate = ?,
			HREmployeeName_OfficeLocation = ?,
			
			HREmployeeName_Department = ?,
			HREmployeeName_DepartmentID = ?,
			HREmployeeName_Title = ?,
			HREmployeeName_TitleID = ?,
			HREmployeeName_CategoryID = ?,
			HREmployeeName_Manager = ?,
			HREmployeeName_IsManager = ?,
			HREmployeeName_HoursTrackingGBS = ?,
			HREmployeeName_HoursTrackingGHS = ?,
			
			HREmployeeName_RevenueCode = ?,
			HREmployeeName_ExcludeFromHoursEmail = ?,
			HREmployeeName_ExcludeFromProjectionSummary = ?,
			HREmployeeName_HES = ?
			
		WHERE HREmployeeName_ID = ?";

    public function update(HREmployeeRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "sssssssisiisiiisiiii";
            $sqlParams = array(
				$record->firstName,
				$record->lastName,
				$record->email,

				MySQLDateUpdate($record->hireDate),
				MySQLDateUpdate($record->endDate),
				$record->officeLocation,
				
				$record->department,
				$record->departmentId,
				$record->title,
				$record->titleId,
				$record->categoryId,
				
				$record->manager,
				$record->isManager,
				$record->hoursTrackingGBS,
				$record->hoursTrackingGHS,
				
				$record->revenueCode,
				$record->excludeFromHoursEmail,
				$record->excludeFromProjectionSummary,
				$record->hes,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
			
			//Now update any manager references
			$pageSqlString = "SELECT * FROM temp_employeename WHERE employeeNameNew=?";
			$filterBinding = "s";
			$filterParams = array($record->firstName." ".$record->lastName);
			$queryResponse = new QueryResponseRecord();
            $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
						$sqlString = "UPDATE hr_employee_name SET HREmployeeName_Manager=? WHERE HREmployeeName_Manager=?";
						$sqlBinding = "ss";
						$sqlParams = array($row["employeeNameNew"],$row["employeeNameOld"]);
						$updateResponseNew = new ExecuteResponseRecord();
						MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponseNew);
						
						//Now Trucate table
						 $sqlString = "DELETE FROM temp_employeename WHERE employeeNameNew=?";
						 $sqlBinding="s";
						 $sqlParams = array($row["employeeNameNew"]);
						MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponseNew);

                    }
                }			
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	hr_employee_name
        SET		HREmployeeName_DisplayOrderID = ?
		WHERE	HREmployeeName_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HREmployeeProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }
	
	public function createOrgChartLayout($orgChartResults,$managerCount,$departmentName,$departmentDirector){
		//resort the managers by layer
		for ($m=1;$m <$managerCount;$m++){
			$ManagerLevels[] = "ManagerLevel".$m;
		}
		if ($managerCount < 2){
			$ManagerLevels[] = "ManagerLevel1";
			$OrgChartLevel[0][$orgChartResults[0]->ManagerLevel1][] = $orgChartResults[0]->ManagerLevel1;
			
		}
		//print_pre($ManagerLevels);
		foreach ($orgChartResults as $id=>$obj){
			$howManyLevels = 0;
			foreach (array_reverse($ManagerLevels) as $ManagerLevel){
				if ($obj->$ManagerLevel){
					$howManyLevels++;
				}
			}
			$OrgChartCount[$howManyLevels][] = $obj;
		}
		$MinLevels = 0;
		$orgLevelMoveUp = 1;
		if ($howManyLevels < 2){
			$MinLevels = 0;
			$orgLevelMoveUp = 1;
		}
		$i=count($OrgChartCount);
		$orgLevel = 0;
		//print_pre($OrgChartCount);
		foreach ($OrgChartCount as $count=>$countObj){
			$i=$count;
				if (count($OrgChartCount[$i])){
					foreach($OrgChartCount[$i] as $OrgLevel){	
						if ($ManagerLevels[$orgLevel+$orgLevelMoveUp]){
							$ThisNextManager = trim($OrgLevel->$ManagerLevels[$orgLevel+$orgLevelMoveUp]);
						}else{
							$ThisNextManager = trim($OrgLevel->ManagerLevel2);
						}
						
						if ($ThisNextManager){
							if (!in_array($ThisNextManager,$OrgChartLevel[$orgLevel][$OrgLevel->$ManagerLevels[$orgLevel]])){
								$OrgChartLevel[$orgLevel][$OrgLevel->$ManagerLevels[$orgLevel]][] = $ThisNextManager;
							}
						}
						
						//echo "hi ".$orgLevel."<br>";
						if ($orgLevel>0){
							$ThisPreviousManager = trim($OrgLevel->$ManagerLevels[$orgLevel-1]);
							if ($ThisPreviousManager){
								//check to see if this is in the previous Manager was in a level update
								if (array_key_exists($ThisPreviousManager,$OrgChartLevel[$orgLevel-1])){
									if (!in_array($OrgLevel->$ManagerLevels[$orgLevel],$OrgChartLevel[$orgLevel-1][$ThisPreviousManager])){
										$OrgChartLevel[$orgLevel-1][$ThisPreviousManager][] = $OrgLevel->$ManagerLevels[$orgLevel];
									}
								}else{
									if (!in_array($ThisPreviousManager,$OrgChartLevel[$orgLevel][$OrgLevel->$ManagerLevels[$orgLevel]])){
										$OrgChartLevel[$orgLevel][$ThisPreviousManager][] = $OrgLevel->$ManagerLevels[$orgLevel];
									}
								}
							}
						}
					} //end foreach OrgLevel
				}//end if count
				$i--;
				$orgLevel++;
		}//end foreach OrgChartCount
		//print_pre($OrgChartLevel);
		//remove dups	
		$OrgChartLevel = super_unique($OrgChartLevel);
		//remove John from departments
		if ($departmentName !="Executive" && $OrgChartLevel[0]["John Majercak"][0] != "John Majercak"){
			unset($OrgChartLevel[0]["John Majercak"]);
			if (!count($OrgChartLevel[0])){
				unset($OrgChartLevel[0]);
			}
		}
		//reset any missing keys
		$OrgChartLevel = array_values($OrgChartLevel);
		$OrgChartLevel[0] = array_map('array_values', $OrgChartLevel[0]);

		//move managers from top into lower levels if appropriate
		$ThisOrgLevel = 0;
		$NextOrgLevel = $ThisOrgLevel+1;
		foreach ($OrgChartLevel[$ThisOrgLevel] as $Manager=>$Managees){
			//move them out of top level with director
			if (in_array($Manager,$OrgChartLevel[$ThisOrgLevel][$departmentDirector])){
				$OrgChartLevel[$NextOrgLevel][$Manager]=$OrgChartLevel[$ThisOrgLevel][$Manager];
				unset($OrgChartLevel[$ThisOrgLevel][$Manager]);
			}else{
				if ($Manager != $departmentDirector){
					foreach ($OrgChartLevel[$ThisOrgLevel] as $ManagerLevel=>$ManagerLevelItems){
						if ($Manager==$ManagerLevel){
							$OrgChartLevel[$NextOrgLevel][$Manager]=$OrgChartLevel[$ThisOrgLevel][$Manager];
							unset($OrgChartLevel[$ThisOrgLevel][$Manager]);
						}
					}
				}
			}
		}
		
		//check next levels
		for ($m=1;$m <$managerCount;$m++){

			$ThisOrgLevel = $m;
			$NextOrgLevel = $ThisOrgLevel+1;
			foreach ($OrgChartLevel[$ThisOrgLevel] as $Manager=>$Managees){
				foreach ($OrgChartLevel[$ThisOrgLevel] as $ManagerLevel=>$ManagerLevelItems){
					if (in_array($Manager,$ManagerLevelItems)){
						$OrgChartLevel[$NextOrgLevel][$Manager]=$OrgChartLevel[$ThisOrgLevel][$Manager];
						unset($OrgChartLevel[$ThisOrgLevel][$Manager]);
					}
				
				}		
			}
		}
		return $OrgChartLevel;
	}
	
    public function getOfficeLocations(){
		$sqlSelectString = "SELECT eo.HREmployeeOfficeLocations_Name as name ";
		$sqlFromString = "FROM hr_employee_officelocations as eo WHERE 1 = 1";
		$sqlBinding = "";
		$sqlParams = array();

        $SqlString = $sqlSelectString.$sqlFromString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $sqlBinding, $sqlParams, $queryResponse);
		if ($queryResponse->success){
			$result = $sqlResult;
		} else {
			trigger_error("HREmployeeProvider.getOfficeLocations failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	private static $UPDATE_OFFICELOCATIONS_SQL = "
		UPDATE hr_employee_officelocations
		SET
			HREmployeeOfficeLocations_Name = ?
		WHERE
			HREmployeeOfficeLocations_Name = ?";
			
	private static $UPDATE_EMPLOYEE_OFFICELOCATIONS_SQL = "
		UPDATE hr_employee_name
		SET
			HREmployeeName_OfficeLocation = ?
		WHERE
			HREmployeeName_OfficeLocation = ?";
	
	private static $INSERT_OFFICELOCATIONS_SQL = "
		INSERT INTO hr_employee_officelocations
			(HREmployeeOfficeLocations_Name)
		VALUES
			(?)";
			
	public function updateOfficeLocations($location_old,$location_new){
		//update if $location_old
        if ($location_old){
            $sqlStringLocation = self::$UPDATE_OFFICELOCATIONS_SQL;
            $sqlStringEmployee = self::$UPDATE_EMPLOYEE_OFFICELOCATIONS_SQL;
            $sqlBinding = "ss";
            $sqlParams = array($location_new,$location_old);

			//first update location table
			$updateResponse = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, $sqlStringLocation, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeProvider.updateOfficeLocations_LocationsTable failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
			//now update employee table
            MySqliHelper::execute($this->mysqli, $sqlStringEmployee, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeProvider.updateOfficeLocations_EmployeesTable failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
			
			return $updateResponse;	

		//add if !$location_old
		}else{
			$sqlString = self::$INSERT_OFFICELOCATIONS_SQL;
			$sqlBinding = "s";
			$sqlParam = array($location_new);
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$record->id = $insertResponse->insertedId;
				$insertResponse->record = $record;
			} else {
				trigger_error("HREmployeeProvider.updateOfficeLocations_LocationsTable failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
			return $insertResponse;
		}
	}	
	
    private static $INSERT_EEOSTAFFFORM_SQL = "
		INSERT INTO hr_eeo_staff
		(
			HREEO_Ethnicity,
			HREEO_EthnicityOther,
			HREEO_Gender,
			HREEO_Participation,
			HREEO_EmailAddress,
			HREEO_IPAddress
		) VALUES (
			?,?,?,?,?,?
		)";

    public function eeoSubmit($record){
		$ipAddress  = $_SERVER['REMOTE_ADDR'];
		$ethnicity = (count($record->ethnicity) > 1 ? implode(",",$record->ethnicity) : $record->ethnicity);
		$gender = (count($record->gender) > 1 ? implode(",",$record->gender) : $record->gender);
		$emailAddress = $record->email;
        $sqlString = self::$INSERT_EEOSTAFFFORM_SQL;
        $sqlBinding = "ssssss";
        $sqlParam = array(
			$ethnicity,
			$record->ethnicityOther,
			$gender,
			$record->participation,
			$emailAddress,
			$ipAddress
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeProvider.eeoSubmit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
    private static $SELECT_STAFFEEOCOLUMNS = "
        SELECT
			e.HREEO_ID as id,
			e.HREEO_Ethnicity as ethnicity,
			e.HREEO_EthnicityOther as ethnicityOther,
			e.HREEO_Gender as gender,
			e.HREEO_Participation as participation,
			e.HREEO_EmailAddress as email,
			e.HREEO_TimeStamp as timeStamp";

    private static $FROM_STAFFEEOTABLE_CLAUSE = "
        FROM hr_eeo_staff as e
        WHERE 1 = 1";
    private static $FILTER_STAFFEEOTABLE_BYEMAIL = "
        AND e.HREEO_EmailAddress = ?";
				
    public function getEEOSurvey($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_STAFFEEOCOLUMNS;
        $fromTableClause = self::$FROM_STAFFEEOTABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY e.HREEO_TimeStamp DESC";
        $limiter = "";
		if ($criteria->email){
			$filterString .= self::$FILTER_STAFFEEOTABLE_BYEMAIL;
			$filterBinding = "s";
			$filterParams = array($criteria->email);
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HREmployeeProvider.getEEOSurvey failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeProvider.getEEOSurvey failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $SELECT_EEOTITLESCOLUMNS = "
		SELECT 
			DISTINCT(HREmployeeName_Title) AS Title FROM hr_employee_name
		UNION
			SELECT 
				DISTINCT(HREmployeeRecruitmentTitle_Title) AS TITLE FROM hr_employee_recruitment_titles ORDER BY Title";

    public function getEEOTitles(){
        $pageSqlString = self::$SELECT_EEOTITLESCOLUMNS;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();
	
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row["Title"];
			}
		} else {
			trigger_error("HREmployeeProvider.getEEOTitles failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
    private static $SELECT_STAFFMANUALCOLUMNS = "
        SELECT
			e.HRStaffManual_ID as id,
			e.HRStaffManual_Version as version,
			e.HRStaffManual_TimeStamp as timeStamp,
			e.HRStaffManual_Email as email,
			e.HRStaffManual_IPAddress as ipAddress";

    private static $FROM_STAFFMANUALTABLE_CLAUSE = "
        FROM hr_staffmanual as e
        WHERE 1 = 1";
    private static $FILTER_STAFFMANUALTABLE_BYEMAIL = "
        AND e.HRStaffManual_Email = ?";
    private static $FILTER_STAFFMANUALTABLE_BYVERSION = "
        AND e.HRStaffManual_Version = ?";
    private static $FILTER_STAFFMANUALTABLE_BYVERSIONHIRE = "
        AND e.HRStaffManual_Version > ?";
				
    public function getStaffManual($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_STAFFMANUALCOLUMNS;
        $fromTableClause = self::$FROM_STAFFMANUALTABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();

        $orderBy = " ORDER BY e.HRStaffManual_TimeStamp DESC";
        $limiter = "";
		if ($criteria->email){
			$filterString .= self::$FILTER_STAFFMANUALTABLE_BYEMAIL;
			$filterBinding .= "s";
			$filterParams[] = $criteria->email;
		}
		if ($criteria->version){
			$filterString .= self::$FILTER_STAFFMANUALTABLE_BYVERSION;
			$filterBinding .= "s";
			$filterParams[] = $criteria->version;
		}
		if ($criteria->currentVersion){
			$filterString .= self::$FILTER_STAFFMANUALTABLE_BYVERSIONHIRE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->currentVersion;
		}
		if ($criteria->hireDate){
			$filterString .= self::$FILTER_STAFFMANUALTABLE_BYVERSIONHIRE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->hireDate;
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
		//print_pre($filterParams);
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HREmployeeProvider.getStaffManual failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeProvider.getEEOSurvey failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $INSERT_STAFFMANUALFORM_SQL = "
		INSERT INTO hr_staffmanual
		(
			HRStaffManual_Version,
			HRStaffManual_Email,
			HRStaffManual_IPAddress
		) VALUES (
			?,?,?
		)";

    public function staffManualSubmit($record){
		$ipAddress  = $_SERVER['REMOTE_ADDR'];
		$emailAddress = $record->email;
		$version = $record->version;
        $sqlString = self::$INSERT_STAFFMANUALFORM_SQL;
        $sqlBinding = "sss";
        $sqlParam = array(
			$version,
			$emailAddress,
			$ipAddress
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeProvider.staffManualSubmit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
    public function getStaffHoursMatrixEmployeeIDs($criteria){
		$sqlString = "SELECT DISTINCT GBSHoursCodesMatrix_EmployeeID as EmployeeIDs FROM hr_hours_codes_matrix ORDER BY GBSHoursCodesMatrix_EmployeeID";
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();
		
		$result = new stdClass();
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("HREmployeeProvider.getStaffManual failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }

    public function getRevenueCodes(){
		$sqlSelectString = "SELECT * FROM hr_revenuecodes WHERE 1 = 1 ORDER BY HRRevenueCode_Code";
		$sqlBinding = "";
		$sqlParams = array();

        $SqlString = $sqlSelectString.$sqlFromString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $sqlBinding, $sqlParams, $queryResponse);
		if ($queryResponse->success){
			$result = $sqlResult;
		} else {
			trigger_error("HREmployeeProvider.getRevenueCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
	    private static $INSERT_REVENUECODE_SQL = "
		INSERT INTO hr_revenuecodes
		(
			HRRevenueCode_Code,
			HRRevenueCode_Name,
			HRRevenueCode_LastEditedBy
		) VALUES (
			?,?,?
		)";

    public function addRevenueCodes($record,$adminId){
		$code = $record->HRRevenueCode_Code;
		$name = $record->HRRevenueCode_Name;
		$lastEditedBy = date("Y-m-d H:i:s")." ".$adminId;
        $sqlString = self::$INSERT_REVENUECODE_SQL;
        $sqlBinding = "sss";
        $sqlParam = array(
			$code,
			$name,
			$lastEditedBy
		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeProvider.staffManualSubmit failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	private static $UPDATE_REVENUECODES_SQL = "
		UPDATE hr_revenuecodes
		SET
			HRRevenueCode_Code = ?,
			HRRevenueCode_Name = ?,
			HRRevenueCode_LastEditedBy = ?
		WHERE
			HRRevenueCode_ID = ?";
	
		
	public function updateRevenueCodes($record,$adminId){
		//update if $location_old
		$lastEditedBy = date("Y-m-d H:i:s")." ".$adminId;
         $sqlString = self::$UPDATE_REVENUECODES_SQL;
            $sqlBinding = "sssi";
            $sqlParams = array($record->HRRevenueCode_Code,$record->HRRevenueCode_Name,$lastEditedBy,$record->HRRevenueCode_ID);

			//first update location table
			$updateResponse = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeProvider.updateRevenueCodes failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
			
			return $updateResponse;	
	}	

	
}