<?php
class ForecastingProvider {
	private $mysqli;
	
    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
    private static $SELECT_FORECASTVARIABLES_COLUMNS = "
        SELECT 
			ExecForecastVariable_Name as name,
			ExecForecastVariable_DefaultValue as defaultValue,
			ExecForecastVariable_Description as description,
			ExecForecastVariable_Type as type,
			ExecForecastVariable_Options as options,
			ExecForecastVariable_Category as category
		FROM 
			exec_forecast_variables ORDER BY ExecForecastVariable_Category, ExecForecastVariable_Name";
	
    public function getVariables(){
        $sqlString = self::$SELECT_FORECASTVARIABLES_COLUMNS;
        $sqlBindings = "";
        $sqlParams = array();
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			$result = new stdClass();
			foreach ($sqlResult as $row){
				$result->$row["name"] = $row["defaultValue"];
			}
        } else {
            trigger_error("ForecastingProvider.getVariables failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }		
    public function getVariableDetails(){
        $sqlString = self::$SELECT_FORECASTVARIABLES_COLUMNS;
        $sqlBindings = "";
        $sqlParams = array();
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result[] = (object)$row;
			}
        } else {
            trigger_error("ForecastingProvider.getVariables failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }		

	public function updateVariables($post){
		foreach ($post as $key => $val) {
			$sqlString = "UPDATE exec_forecast_variables SET ExecForecastVariable_DefaultValue = ? WHERE ExecForecastVariable_Name = ?";
			$sqlBindings = "ss";
			$sqlParams = array($val, $key);
			$updateResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
			if ($updateResponse->error){
				trigger_error("ForecastingProvider.updateVariables failed to update: ".$updateResponse->error, E_USER_ERROR);
			}
			return $updateResponse;
		}
	}
	
    private static $INSERT_FORECASTVARIABLES_SQL = "
        INSERT INTO exec_forecast_variables
        (
			ExecForecastVariable_Name,
			ExecForecastVariable_DefaultValue,
			ExecForecastVariable_Description,
			ExecForecastVariable_Type,
			ExecForecastVariable_Options,
			ExecForecastVariable_Category
		) VALUES (
			?,?,?,?,?,?
        )";

    private static  $INSERT_FORECASTVARIABLES_BINDINGS = "ssssss";
	
    public function insertVariables($post){
        $sqlString = self::$INSERT_FORECASTVARIABLES_SQL;
        $sqlBindings = self::$INSERT_FORECASTVARIABLES_BINDINGS;

        $sqlParams = array(
			$post["name"],
			$post["defaultValue"],
			$post["description"],
			$post["type"],
			$post["options"],
			$post["category"]
        );
		//echo $sqlString.print_r($sqlParams,true);
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }	

	public function getPLCategoryTypes(){
        $sqlString = "SELECT DISTINCT(ExecPL_Type) as type ,ExecPL_Category as category FROM `exec_pl` WHERE 1";
        $sqlBindings = "";
        $sqlParams = array();
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result[] = $row;
			}
        } else {
            trigger_error("ForecastingProvider.getPLCategoryTypes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
	}

	private static $SELECT_EXECPL_SQL = "
		SELECT 
			epl.ExecPL_Date as date,
			epl.ExecPL_Department as department,
			epl.ExecPL_Category as category,
			epl.ExecPL_Type as type,
			epl.ExecPL_Actual as actual,
			epl.ExecPL_Budget as budget,
			epl.ExecPL_Forecast as forecast,
			epl.ExecPL_ForecastAsOfDate as forecastAsOfDate";
	
	private static $FROM_TABLE_EXECPL_CLAUSE = "
		FROM exec_pl as epl WHERE 1=1";
		
	private static $FILTER_EXECPL_ON_DEPARTMENT = "
		AND epl.ExecPL_Department = ?";
		
	private static $FILTER_EXECPL_ON_CATEGORY = "
		AND epl.ExecPL_Category = ?";
		
	private static $FILTER_EXECPL_ON_TYPE = "
		AND epl.ExecPL_Type = ?";
	private static $FILTER_EXECPL_ON_DATE = "
		AND epl.ExecPL_Date BETWEEN ? AND ?";
	
    public function getPL($criteria){
        $paginationSelectString = self::$SELECT_EXECPL_SQL;
        $fromTableClause = self::$FROM_TABLE_EXECPL_CLAUSE;
		
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		$orderBy = " ORDER BY epl.ExecPL_Date, epl.ExecPL_Department, epl.ExecPL_Category, epl.ExecPL_Type";

		if ($criteria->department){
            $filterString .= self::$FILTER_EXECPL_ON_DEPARTMENT;
            $filterBinding .= "s";
            $filterParams[] = $criteria->department;
		}
		if ($criteria->category){
            $filterString .= self::$FILTER_EXECPL_ON_CATEGORY;
            $filterBinding .= "s";
            $filterParams[] = $criteria->category;
		}
		if ($criteria->type){
            $filterString .= self::$FILTER_EXECPL_ON_TYPE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->type;
		}
		if ($criteria->endDate){
            $filterString .= self::$FILTER_EXECPL_ON_DATE;
            $filterBinding .= "ss";
            $filterParams[] = MySQLDateUpdate($criteria->startDate);
            $filterParams[] = MySQLDateUpdate($criteria->endDate);
		}
		
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("ForecastingProvider.getPL failed to get results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
    }			
	
	private static $INSERT_PL_SQL="
		INSERT INTO exec_pl
		(
			ExecPL_Date,
			ExecPL_Department,
			ExecPL_Category,
			ExecPL_Type,
			
			ExecPL_Actual,
			ExecPL_Budget,
			ExecPL_Forecast,
			ExecPL_ForeCastAsOfDate,
			ExecPL_ForeCastVariables
		) 
		VALUES 
		(
			?,?,?,?,
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE ExecPL_Forecast=VALUES(ExecPL_Forecast), ExecPL_ForeCastAsOfDate=VALUES(ExecPL_ForecastAsOfDate), ExecPL_ForeCastVariables=VALUES(ExecPL_ForecastVariables)";
    private static  $INSERT_PL_BINDINGS = "ssssdddss";
	
	
    public function insertPL($plObject){
        $sqlString = self::$INSERT_PL_SQL;
        $sqlBindings = self::$INSERT_PL_BINDINGS;

        $sqlParams = array(
			$plObject->plDate,
			$plObject->department,
			$plObject->category,
			$plObject->type,
			
			$plObject->actual,
			$plObject->budget,
			$plObject->forecast,
			$plObject->forecastAsOfDate,
			$plObject->forecastVariables
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
	
	private static $INSERT_MRVDATA_SQL="
		INSERT INTO csg_mrv_data
		(
			CSGMRVData_Utility,
			CSGMRVData_Date,
			CSGMRVData_Count,
			CSGMRVData_AddedByAdmin
		) 
		VALUES 
		(
			?,?,?,?
		)";
    private static  $INSERT_MRVDATA_BINDINGS = "ssis";
	
	
    public function mrvadd($utility,$countDate,$countQty,$adminId){
        $sqlString = self::$INSERT_MRVDATA_SQL;
        $sqlBindings = self::$INSERT_MRVDATA_BINDINGS;

        $sqlParams = array(
			$utility,
			$countDate,
			$countQty,
			$adminId
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
	
    private static $INSERT_FORECASTAVERAGESES_SQL = "
        INSERT INTO exec_forecast_averages_es_isms
        (
			ExecForecastAveragesES_Crew,
			ExecForecastAveragesES_Type,
			ExecForecastAveragesES_Qty,
			ExecForecastAveragesES_AsOfDate
		) VALUES (
			?,?,?,?
        )";

    private static  $INSERT_FORECASTAVERAGESES_BINDINGS = "ssis";
	
    public function insertAveragesES($crew,$ism,$qty){
        $sqlString = self::$INSERT_FORECASTAVERAGESES_SQL;
        $sqlBindings = self::$INSERT_FORECASTAVERAGESES_BINDINGS;
		$asOfDate = date("Y-m-d");
		$sqlParams = array($crew,$ism,$qty,$asOfDate);
		$insertResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
	
	private static $SELECT_LASTAVERAGESES_SQL = "
		SELECT 
			ExecForecastAveragesES_Crew as crew, 
			ExecForecastAveragesES_Type as type, 
			ExecForecastAveragesES_Qty as qty,
			ExecForecastAveragesES_AsOfDate as asOfDate";
		
	private static $FROM_TABLE_LASTAVERAGESES_CLAUSE = "
		FROM exec_forecast_averages_es_isms WHERE 1=1";
	
	private static $FILTER_LASTAVERAGESES_ON_DATE = "
		AND ExecForecastAveragesES_AsOfDate=?";
	private static $FILTER_LASTAVERAGES_ON_MOSTRECENT = "
		AND ExecForecastAveragesES_AsOfDate = (SELECT MAX(ExecForecastAveragesES_AsOfDate) FROM exec_forecast_averages_es_isms)";
	
	public function lastAveragesES($criteria){
        $paginationSelectString = self::$SELECT_LASTAVERAGESES_SQL;
        $fromTableClause = self::$FROM_TABLE_LASTAVERAGESES_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->date){
            $filterString .= self::$FILTER_LASTAVERAGESES_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->date;
		}
		
		if ($criteria->mostRecent){
			$filterString .= self::$FILTER_LASTAVERAGES_ON_MOSTRECENT;
		}
		
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = $row;
			}
		} else {
			trigger_error("ForecastingProvider.lastAveragesES failed to get results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
	}
    private static $INSERT_FORECASTAVERAGESES_CONTRACTS_SQL = "
        INSERT INTO exec_forecast_averages_es_contracts
        (
			ExecForecastAveragesESContracts_Crew,
			ExecForecastAveragesESContracts_SiteVisits,
			ExecForecastAveragesESContracts_NoWork,
			ExecForecastAveragesESContracts_RoadBlocked,
			ExecForecastAveragesESContracts_Pending,

			ExecForecastAveragesESContracts_Signed,
			ExecForecastAveragesESContracts_SignedRate,
			ExecForecastAveragesESContracts_ConversionRate,
			ExecForecastAveragesESContracts_DaysToIssue,
			ExecForecastAveragesESContracts_DaysToSign,

			ExecForecastAveragesESContracts_DaysToBill,
			ExecForecastAveragesESContracts_ContractAmount,
			ExecForecastAveragesESContracts_BilledAmount,
			ExecForecastAveragesESContracts_Commissions,
			ExecForecastAveragesESContracts_NATGRIDBilledCount,
			
			ExecForecastAveragesESContracts_WMECOBilledCount,
			ExecForecastAveragesESContracts_BGASBilledCount,
			ExecForecastAveragesESContracts_BGASBilledAmount,
			ExecForecastAveragesESContracts_AsOfDate			
			) VALUES (
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?
        )";

    private static  $INSERT_FORECASTAVERAGESES_CONTRACTS_BINDINGS = "siiiiiiiiiiiiiiiiis";
	
    public function insertAveragesContractsES($ContractObj){
        $sqlString = self::$INSERT_FORECASTAVERAGESES_CONTRACTS_SQL;
        $sqlBindings = self::$INSERT_FORECASTAVERAGESES_CONTRACTS_BINDINGS;
		$asOfDate = date("Y-m-d");
		$sqlParams = array(
			$ContractObj->crew,
			$ContractObj->siteVisits,
			$ContractObj->noWork,
			$ContractObj->roadBlocked,
			$ContractObj->pending,
			
			$ContractObj->signed,
			$ContractObj->signedRate,
			$ContractObj->conversionRate,
			$ContractObj->daysToIssue,
			$ContractObj->daysToSign,
			
			$ContractObj->daysToBill,
			$ContractObj->contractAmount,
			$ContractObj->billedAmount,
			$ContractObj->commissions,

			$ContractObj->NATGRID_BilledCount,
			$ContractObj->WMECO_BilledCount,
			$ContractObj->BGAS_BilledCount,
			$ContractObj->BGAS_BilledAmount,
			$asOfDate
			);
		$insertResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
    private static $SELECT_FORECASTAVERAGESES_CONTRACTS_SQL = "
		SELECT 
			ExecForecastAveragesESContracts_Crew as crew, 
			ExecForecastAveragesESContracts_SiteVisits as siteVisits, 
			ExecForecastAveragesESContracts_NoWork as noWork, 
			ExecForecastAveragesESContracts_RoadBlocked as roadBlocked, 
			ExecForecastAveragesESContracts_Pending as pending, 

			ExecForecastAveragesESContracts_Signed as signed, 
			ExecForecastAveragesESContracts_SignedRate as signedRate, 
			ExecForecastAveragesESContracts_ConversionRate as conversionRate,
			ExecForecastAveragesESContracts_DaysToIssue as daysToIssue,
			ExecForecastAveragesESContracts_DaysToSign as daysToSign,

			ExecForecastAveragesESContracts_DaysToBill as daysToBill,
			ExecForecastAveragesESContracts_ContractAmount as contractAmount,
			ExecForecastAveragesESContracts_BilledAmount as billedAmount,
			ExecForecastAveragesESContracts_Commissions as commissions,
			ExecForecastAveragesESContracts_NATGRIDBilledCount as NATGRIDBilledCount,
			ExecForecastAveragesESContracts_WMECOBilledCount as WMECOBilledCount,
			ExecForecastAveragesESContracts_BGASBilledCount as BGASBilledCount,
			ExecForecastAveragesESContracts_BGASBilledAmount as BGASBilledAmount,
			ExecForecastAveragesESContracts_AsOfDate as asOfDate";

	private static $FROM_FORECASTAVERAGESES_CONTRACTS_SQL = "
		FROM exec_forecast_averages_es_contracts WHERE 1=1";

	private static $FILTER_LASTAVERAGESES_CONTRACTS_ON_DATE = "
		AND ExecForecastAveragesESContracts_AsOfDate=?";
	private static $FILTER_LASTAVERAGES_CONTRACTS_ON_MOSTRECENT = "
		AND ExecForecastAveragesESContracts_AsOfDate = (SELECT MAX(ExecForecastAveragesESContracts_AsOfDate) FROM exec_forecast_averages_es_contracts)";
	
	public function lastAveragesContractsES($criteria){
        $paginationSelectString = self::$SELECT_FORECASTAVERAGESES_CONTRACTS_SQL;
        $fromTableClause = self::$FROM_FORECASTAVERAGESES_CONTRACTS_SQL;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->date){
            $filterString .= self::$FILTER_LASTAVERAGESES_CONTRACTS_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->date;
		}
		
		if ($criteria->mostRecent){
			$filterString .= self::$FILTER_LASTAVERAGES_CONTRACTS_ON_MOSTRECENT;
		}
		
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli,$pageSqlString , $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("ForecastingProvider.lastAveragesContractsES failed to get results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
	}
	
    private static $INSERT_FORECASTAVERAGESES_CONTRACTORS_SQL = "
        INSERT INTO exec_forecast_averages_es_contractors
        (
			ExecForecastAveragesESContractors_Contractor,
			ExecForecastAveragesESContractors_DaysToAccept,
			ExecForecastAveragesESContractors_DaysToInstall,
			ExecForecastAveragesESContractors_DaysToBill,
			ExecForecastAveragesESContractors_ContractAmount,
			ExecForecastAveragesESContractors_AsOfDate			
			) VALUES (
			?,?,?,?,?,?
        )";

    private static  $INSERT_FORECASTAVERAGESES_CONTRACTORS_BINDINGS = "siiiis";
	
    public function insertAveragesContractorsES($ContractorObj){
        $sqlString = self::$INSERT_FORECASTAVERAGESES_CONTRACTORS_SQL;
        $sqlBindings = self::$INSERT_FORECASTAVERAGESES_CONTRACTORS_BINDINGS;
		$asOfDate = date("Y-m-d");
		$sqlParams = array(
			$ContractorObj->contractor,
			$ContractorObj->daysToAccept,
			$ContractorObj->daysToInstall,
			$ContractorObj->daysToBill,
			$ContractorObj->contractAmount,
			$asOfDate
			);
		$insertResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
    private static $SELECT_FORECASTAVERAGESES_CONTRACTORS_SQL = "
		SELECT 
			ExecForecastAveragesESContractors_Contractor as contractor, 
			ExecForecastAveragesESContractors_DaysToAccept as daysToAccept, 
			ExecForecastAveragesESContractors_DaysToInstall as daysToInstall, 
			ExecForecastAveragesESContractors_DaysToBill as daysToBill, 
			ExecForecastAveragesESContractors_ContractAmount as contractAmount,
			ExecForecastAveragesESContractors_AsOfDate as asOfDate";

	private static $FROM_FORECASTAVERAGESES_CONTRACTORS_SQL = "
		FROM exec_forecast_averages_es_contractors WHERE 1=1";

	private static $FILTER_LASTAVERAGESES_CONTRACTORS_ON_DATE = "
		AND ExecForecastAveragesESContractors_AsOfDate=?";
	private static $FILTER_LASTAVERAGES_CONTRACTORS_ON_MOSTRECENT = "
		AND ExecForecastAveragesESContractors_AsOfDate = (SELECT MAX(ExecForecastAveragesESContractors_AsOfDate) FROM exec_forecast_averages_es_contractors)";
	
	public function lastAveragesContractorsES($criteria){
        $paginationSelectString = self::$SELECT_FORECASTAVERAGESES_CONTRACTORS_SQL;
        $fromTableClause = self::$FROM_FORECASTAVERAGESES_CONTRACTORS_SQL;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->date){
            $filterString .= self::$FILTER_LASTAVERAGESES_CONTRACTORS_ON_DATE;
            $filterBinding .= "s";
            $filterParams[] = $criteria->date;
		}
		
		if ($criteria->mostRecent){
			$filterString .= self::$FILTER_LASTAVERAGES_CONTRACTORS_ON_MOSTRECENT;
		}
		
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
		//echo $pageSqlString.print_r($filterParams,true);
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli,$pageSqlString , $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("ForecastingProvider.lastAveragesContractorsES failed to get results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;	
	}
	public function dropAveragesES(){
        $sqlString = "DELETE FROM exec_forecast_averages_es_contractors WHERE ExecForecastAveragesESContractors_AsOfDate = ?";
		$asOfDate = date("Y-m-d");
        $sqlBindings = "s";
		$sqlParams = array(
			$asOfDate
		);
        $sqlString = "DELETE FROM exec_forecast_averages_es_contractors WHERE ExecForecastAveragesESContractors_AsOfDate = ?";
		$deleteResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $deleteResponse);
		$deleteResponses["Contractors"]=$deleteResponse;

        $sqlString = "DELETE FROM exec_forecast_averages_es_contracts WHERE ExecForecastAveragesESContracts_AsOfDate = ?";
		$deleteResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $deleteResponse);
		$deleteResponses["Contracts"]=$deleteResponse;

        $sqlString = "DELETE FROM exec_forecast_averages_es_isms WHERE ExecForecastAveragesES_AsOfDate = ?";
		$deleteResponse = new ExecuteResponseRecord();
		$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $deleteResponse);
		$deleteResponses["ISMS"]=$deleteResponse;
		
		return $deleteResponses;
		
	}
	
    private static $SELECT_PLCOMMENTS_COLUMNS = "
        SELECT 
			ExecPLComments_ID as id,
			ExecPLComments_Date as commentDate,
			ExecPLComments_Comment as comment,
			ExecPLComments_LastEditedBy as lastEditedBy
		FROM 
			exec_pl_comments WHERE 1=1 AND ExecPLComments_Date = ?";
	
    public function getPLComments($commentDate){
        $sqlString = self::$SELECT_PLCOMMENTS_COLUMNS;
        $sqlBindings = "s";
        $sqlParams = array($commentDate);
		//echo $sqlString.$sqlBindings.$sqlParams;
		$queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $queryResponse);		
		
        if ($queryResponse->success){
			foreach ($sqlResult as $row){
				$result[$row["commentDate"]]["comment"] = $row["comment"];
				$result[$row["commentDate"]]["lastEditedBy"] = $row["lastEditedBy"];
			}
        } else {
            trigger_error("ForecastingProvider.getPLComments failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }		
	
	private static $INSERT_PLCOMMENT_SQL="
		INSERT INTO exec_pl_comments
		(
			ExecPLComments_Date,
			ExecPLComments_Comment
		) 
		VALUES 
		(
			?,?
		) ON DUPLICATE KEY UPDATE ExecPLComments_Comment=VALUES(ExecPLComments_Comment), ExecPLComments_LastEditedBy=?";

    private static  $INSERT_PLCOMMENT_BINDINGS = "sss";
	
	
    public function addPLComment($record,$adminId){
        $sqlString = self::$INSERT_PLCOMMENT_SQL;
        $sqlBindings = self::$INSERT_PLCOMMENT_BINDINGS;
		$asOfText = $adminId." at ".date("m/d/Y g:ia");

        $sqlParams = array(
			$record->commentDate,
			$record->comment,
			$asOfText
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
	
	private static $INSERT_ACTUALBUDGET_SQL="
		INSERT INTO exec_pl
		(
			ExecPL_Date,
			ExecPL_Department,
			ExecPL_Category,
			ExecPL_Type,
			ExecPL_Actual
		) 
		VALUES 
		(
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE ExecPL_Actual=VALUES(ExecPL_Actual)";

    private static  $INSERT_PLENTRY_BINDINGS = "ssssd";
	
    public function enterActualBudget($record){
		$sqlString = self::$INSERT_ACTUALBUDGET_SQL;
		$value = $record["Actual"];
        if($record["TypeValue"] == "Budget"){
			$sqlString = str_replace("Actual","Budget",$sqlString);
			$value = $record["Budget"];
		}
        $sqlBindings = self::$INSERT_PLENTRY_BINDINGS;
        $sqlParams = array(
			$record["Date"],
			$record["Department"],
			$record["Category"],
			$record["Type"],
			$value
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		return $insertResponse;
    }	
	
}
?>