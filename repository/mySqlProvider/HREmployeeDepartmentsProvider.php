<?php
class HREmployeeDepartmentsProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

/*DEPARTMENT INFO */
    private static $SELECT_COLUMNS = "
        SELECT
			ed.HREmployeeDepartments_ID as id,
			ed.HREmployeeDepartments_Name as name,
			ed.HREmployeeDepartments_Director as director,
			ed.HREmployeeDepartments_DisplayOrderID as displayOrderId,
			ed.HREmployeeDepartments_Color as color";

    private static $FROM_TABLE_CLAUSE = "
        FROM hr_employee_departments as ed
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND ed.HREmployeeDepartments_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID = "
        AND ed.HREmployeeDepartments_ID = ?";

    private static $FILTER_ON_DIRECTOR = "
        AND ed.HREmployeeDepartments_Director = ?";
		
    private static $FILTER_ON_NAME = "
        AND ed.HREmployeeDepartments_Name = ?";
		
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->director){
            $filterString .= self::$FILTER_ON_DIRECTOR;
            $filterBinding .= "s";
            $filterParams[] = $criteria->director;
		}
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("HREmployeeDepartmentsProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeDepartmentsProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY ed.HREmployeeDepartments_DisplayOrderID ASC, ed.HREmployeeDepartments_Name ASC";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY ed.HREmployeeDepartments_ID ".$sortOrder;
                    break;
                case "director":
                    $orderBy = " ORDER BY ed.HREmployeeDepartments_Director ".$sortOrder;
                    break;
				case "name":
					$orderBy = " ORDER BY ed.HREmployeeDepartments_Name ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "ed.HREmployeeDepartments_ID ".$sortOrder;
                        break;
                    case "director":
                        $orderByStrings[] = "ed.HREmployeeDepartments_Director ".$sortOrder;
                        break;
					case "name":
						$orderByStrings[] = "ed.HREmployeeDepartments_Name ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return HREmployeeDepartmentsRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL = "
		INSERT INTO hr_employee_departments
		(
			HREmployeeDepartments_Name,
			HREmployeeDepartments_Director,
			HREmployeeDepartments_DisplayOrderID,
			HREmployeeDepartments_Color
		) VALUES (
			?,?,?,?
		)";

    public function add(HREmployeeDepartmentsRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "ssis";
        $sqlParam = array(
			$record->name,
			$record->director,
			$record->displayOrderId,
			$record->color
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeDepartmentsProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL = "
        UPDATE hr_employee_departments
        SET
			HREmployeeDepartments_Name = ?,
			HREmployeeDepartments_Director = ?,
			HREmployeeDepartments_Color = ?
		WHERE HREmployeeDepartments_ID = ?";

    public function update(HREmployeeDepartmentsRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "sssi";
            $sqlParams = array(
				$record->name,
				$record->director,
				$record->color,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeDepartmentsProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	hr_employee_departments
        SET		HREmployeeDepartments_DisplayOrderID = ?
		WHERE	HREmployeeDepartments_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HREmployeeDepartmentsProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
    private static $UPDATE_EMPLOYEEDEPARTMENTID_SQL = "
        UPDATE	hr_employee_name
        SET		HREmployeeName_DepartmentID = ?
		WHERE	HREmployeeName_Department = ?";

    public function updateEmployeeDepartmentID($id,$departmentName){
        $sqlString = self::$UPDATE_EMPLOYEEDEPARTMENTID_SQL;
        $sqlBinding = "is";
        $sqlParams = array($id,$departmentName);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HREmployeeDepartmentsProvider.updateEmployeeDepartmentID failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
/*END DEPARTMENT INFO */

/*TITLE INFO */
    private static $SELECT_COLUMNS_TITLE = "
        SELECT
			et.HRTitles_ID as id,
			et.HRTitles_Name as name,
			et.HRTitles_DisplayOrderID as displayOrderId,
			et.HRTitles_activeID as activeId";

    private static $FROM_TABLE_CLAUSE_TITLE = "
        FROM hr_titles as et
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID_TITLES = "
        AND et.HRTitles_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID_TITLES = "
        AND et.HRTitles_ID = ?";

    private static $FILTER_ON_NAME_TITLES = "
        AND et.HRTitles_Name = ?";
		
    public function getTitles($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS_TITLE;
        $fromTableClause = self::$FROM_TABLE_CLAUSE_TITLE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID_TITLES; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClauseTitle($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID_TITLES;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME_TITLES;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("HREmployeeDepartmentsProvider.getTitles failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeDepartmentsProvider.getTitles failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY_TITLE = "
        ORDER BY et.HRTitles_DisplayOrderID ASC, et.HRTitles_Name ASC";

    private function getOrderByClauseTitle($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY_TITLE;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY et.HRTitles_ID ".$sortOrder;
                    break;
				case "name":
					$orderBy = " ORDER BY et.HRTitles_Name ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "et.HRTitles_ID ".$sortOrder;
                        break;
					case "name":
						$orderByStrings[] = "et.HRTitles_Name ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecordTitle($dataRow) {
        return HREmployeeTitlesRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL_TITLES = "
		INSERT INTO hr_titles
		(
			HRTitles_Name,
			HRTitles_DisplayOrderID
		) VALUES (
			?,?
		)";

    public function addTitle(HREmployeeTitlesRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL_TITLES;
        $sqlBinding = "si";
        $sqlParam = array(
			$record->name,
			$record->displayOrderId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeDepartmentsProvider.addTitle failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL_TITLE = "
        UPDATE hr_titles
        SET
			HRTitles_Name = ?,
			HRTitles_ActiveID = ?
		WHERE HRTitles_ID = ?";

    public function updateTitle(HREmployeeTitlesRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL_TITLE;
            $sqlBinding = "sii";
            $sqlParams = array(
				$record->name,
				$record->activeId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeDepartmentsProvider.updateTitle failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL_TITLE = "
        UPDATE	hr_titles
        SET		HRTitles_DisplayOrderID = ?
		WHERE	HRTitles_ID = ?";

    public function updateDisplayOrderTitle($id,$displayOrderId,$adminId){
		$record = $this->mapToRecordTitle($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL_TITLE;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HREmployeeDepartmentsProvider.updateDisplayOrderTitle failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
/*END TITLE INFO */



/*CATEGORY INFO */
    private static $SELECT_COLUMNS_CATEGORY = "
        SELECT
			ec.HRCategories_ID as id,
			ec.HRCategories_Name as name,
			ec.HRCategories_DisplayOrderID as displayOrderId,
			ec.HRCategories_activeID as activeId";

    private static $FROM_TABLE_CLAUSE_CATEGORY = "
        FROM hr_categories as ec
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID_CATEGORIES = "
        AND ec.HRCategories_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID_CATEGORIES = "
        AND ec.HRCategories_ID = ?";

    private static $FILTER_ON_NAME_CATEGORIES = "
        AND ec.HRCategories_Name = ?";
		
    public function getCategories($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS_CATEGORY;
        $fromTableClause = self::$FROM_TABLE_CLAUSE_CATEGORY;
        $filterString = self::$FILTER_ON_DISPLAYORDERID_CATEGORIES; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClauseCategory($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID_CATEGORIES;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME_CATEGORIES;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("HREmployeeDepartmentsProvider.getCategories failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HREmployeeDepartmentsProvider.getCategories failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY_CATEGORY = "
        ORDER BY ec.HRCategories_DisplayOrderID ASC, ec.HRCategories_Name ASC";

    private function getOrderByClauseCategory($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY_CATEGORY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY ec.HRCategories_ID ".$sortOrder;
                    break;
				case "name":
					$orderBy = " ORDER BY ec.HRCategories_Name ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "ec.HRCategories_ID ".$sortOrder;
                        break;
					case "name":
						$orderByStrings[] = "ec.HRCategories_Name ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecordCategory($dataRow) {
        return HREmployeeCategoriesRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL_CATEGORIES = "
		INSERT INTO hr_categories
		(
			HRCategories_Name,
			HRCategories_DisplayOrderID
		) VALUES (
			?,?
		)";

    public function addCategory(HREmployeeCategoriesRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL_CATEGORIES;
        $sqlBinding = "si";
        $sqlParam = array(
			$record->name,
			$record->displayOrderId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HREmployeeDepartmentsProvider.addCategory failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL_CATEGORY = "
        UPDATE hr_categories
        SET
			HRCategories_Name = ?,
			HRCategories_ActiveID = ?
		WHERE HRCategories_ID = ?";

    public function updateCategory(HREmployeeCategoriesRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL_CATEGORY;
            $sqlBinding = "sii";
            $sqlParams = array(
				$record->name,
				$record->activeId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HREmployeeDepartmentsProvider.updateCategories failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL_CATEGORY = "
        UPDATE	hr_categories
        SET		HRCategories_DisplayOrderID = ?
		WHERE	HRCategories_ID = ?";

    public function updateDisplayOrderCategory($id,$displayOrderId,$adminId){
		$record = $this->mapToRecordCategory($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL_CATEGORY;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HREmployeeDepartmentsProvider.updateDisplayOrderCategory failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
/*END CATEGORY INFO */


	
}
