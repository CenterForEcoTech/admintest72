<?php
/**
 * Hack: utility class to run some sql that probably needs to be refactored into something else. This is a mishmosh that
 * happens to work because all our data are currently in the same database and nothing real fancy.
 */
include_once("EventTransactionProvider.php");
include_once("PermaLinkProvider.php");
include_once("MemberProvider.php");
class EventTransactionHelper {


    /**
     * To support a page whose query parameters include only the "public" transaction code (transCode) and some way
     * to identify the user (either their PID or their verification code), this returns enough information for that page
     * to do its job. The logic here should be in the front end layer, but it is here in the back end because it is faster here.
     * @static
     * @param mysqli $mysqli
     * @param $transCode
     * @param $permaLink
     * @return object
     */
    public static function getTransactionRequestStatus(mysqli $mysqli, $transCode, $permaLink){
        $response = new stdClass();
        $response->isReady = false; // means a recipient cannot be assigned
        $transactionRequest = new EventTransactionRequest();

        // look up the user
        $permalinkProvider = new PermaLinkProviderImpl($mysqli);
        $permaLink = $permalinkProvider->get($permaLink);
        $customerId = MemberProvider::getMemberId($mysqli, $permaLink);

        if (is_numeric($customerId) && $customerId > 0){

            $transactionProvider = new EventTransactionProvider($mysqli);
            $transactionRequest = new EventTransactionRequest();
            $transactionRequest->transCode = $transCode;
            $transactionRequest->id = $transactionRequest->getTransactionId();
            $transactionRequest->customerId = $customerId;
            $mustHaveCustomer = true;

            // isValid ensures that the transactionId is valid. No need to muck with the transcode anymore
            if ($transactionProvider->isValid($transactionRequest, $mustHaveCustomer)){

                // now make sure the transaction is "ready" for a charity to be chosen
                // verifies the following:
                // - has the expected customerID,
                // - not deleted
                // - not invoiced
                // - no charity already picked
                $sqlString = self::$IS_TRANSACTION_CHARITY_PICKER_READY_SQL;
                $sqlBinding = "ii";
                $sqlParam = array(
                    $transactionRequest->id,
                    $transactionRequest->customerId
                );
                $result = MySqliHelper::get_result($mysqli, $sqlString, $sqlBinding, $sqlParam);
                if (count($result) == 0){
                    // invalid!
                    $transactionRequest = new EventTransactionRequest(); // empty it out. it is invalid
                } else {
                    $transactionRequest->merchantId = $result[0]["EventTransaction_MerchantID"];
                    $transactionRequest->orgId = $result[0]["EventTransaction_OrgID"];
                    $transactionRequest->merchantName = $result[0]["merchantName"];
                    $transactionRequest->generatedAmount = $result[0]["EventTransaction_GeneratedAmount"];
                    $response->isReady = !($result[0]["hasRecipient"] || $result[0]["isInPotOfMoney"]);
                }
            }
        }

        $response->transactionRequest = $transactionRequest;
        return $response;
    }

    private static $IS_TRANSACTION_CHARITY_PICKER_READY_SQL = "
        SELECT
          EventTransaction_MerchantID,
          EventTransaction_OrgID,
          EventTransaction_GeneratedAmount,
          (EventTransaction_OrgID != 0) as hasRecipient,
		  (
		    SELECT 1
		    FROM invoices_matrix
		    WHERE InvoicesMatrix_EventTransactionId = EventTransaction_ID
		    AND InvoicesMatrix_IncomingQueueId > 0
		  ) AS 'isInPotOfMoney',
		  (
		    SELECT MerchantInfo_Name
		    FROM merchant_info
		    WHERE MerchantInfo_ID = EventTransaction_MerchantID
		  ) AS merchantName
        FROM event_transaction
        WHERE EventTransaction_ID = ?
        AND EventTransaction_CustomerID = ?
        AND EventTransaction_Status != 'Deleted'";

    public static function setRecipient(mysqli $mysqli, CharityLookup $charityLookup, EventTransactionRequest $req){
        $provider = new EventTransactionProvider($mysqli);
        return $provider->chooseRecipient($req, $charityLookup);
    }

}