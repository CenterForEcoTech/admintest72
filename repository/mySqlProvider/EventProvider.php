<?php
include_once("EventRecordProvider.php");
include_once("CampaignAssociationProvider.php");
include_once($repositoryApiFolder."Indexing_API.php"); //for indexing services

class EventProvider extends EventRecordProvider {

    public function __construct(mysqli $mysqli, LocationProvider $geolookupProvider = null){
        parent::__construct($mysqli, $geolookupProvider);
    }

	private static $GET_EVENT_BY_ID = "
		SELECT
			event_info.Event_ID,
			event_info.Event_Title,
			event_info.Event_Description,
			event_info.Event_Date,
			event_info.Event_StartTime,
			event_info.Event_EndDate,
			event_info.Event_EndTime,
			event_info.Event_OrgWhyYou,
			event_info.Event_OrgAnticipatedCount,
			event_info.Event_LastEditedByMemberID,
			event_info.Event_CustomAttendLink as customAttendLink,
			event_info.Event_IsNational,
			event_info.Event_IsFcOnly as isFcOnly,
			event_info.Event_IsOnline,
			event_info.Event_OnlineLocationLabel as onlineLocationLabel,
			event_info.Event_OnlineLocationLink as onlineLocationLink,
			event_info.Event_SuppressCharitySelectionForm,
			event_info.Event_SuppressPrintFlier,
			event_info.Event_IsCausetownEvent,
			ti.folder_url as calendarIconPath,
			ti.file_name as calendarIconFile,

			/* venue (merchant) info (for merchant created events only) */
			event_info.Event_EventBriteEventID,
			event_info.Event_FacebookEventID,
			event_info.Event_MerchantPercentage,
			event_info.Event_MerchantSecondaryPercentage,
			event_info.Event_IsFlatDonation as isFlatDonation,
			event_info.Event_DonationPerUnit as donationPerUnit,
			event_info.Event_UnitDescriptor as unitDescriptor,
			event_info.Event_FlatDonationActionPhrase as flatDonationActionPhrase,
			event_info.Event_EventTemplatePremiumId as eventTemplatePremiumId,
			merchant_info.MerchantInfo_ID,
			merchant_info.MerchantInfo_Name,
			merchant_info.MerchantInfo_Website,
			merchant_info.MerchantInfo_Description,
			merchant_info.MerchantInfo_Lat,
			merchant_info.MerchantInfo_Long,
			merchant_info.MerchantInfo_Address,
			merchant_info.MerchantInfo_City,
			merchant_info.MerchantInfo_State,
			merchant_info.MerchantInfo_Zip,
			merchant_info.MerchantInfo_Country,
			merchant_info.MerchantInfo_Phone,
			merchant_info.MerchantInfo_EventBriteVenueID,
			merchant_info.MerchantInfo_Neighborhood as venueNeighborhood,
			merchant_info.MerchantInfo_Keywords as venueKeywords,

			/* nonprofit info (for nonprofit create events only) */
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_Website,
			organization_info.OrganizationInfo_EIN,
			EventLog_TimeStamp as Event_CreatedDate,
			(CASE Event_LastEditedDate WHEN 0 THEN EventLog_TimeStamp ELSE Event_LastEditedDate END) as Event_EditedDate,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid,
			(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association 
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID 
                AND TagAssociation_SourceTable = 'event_info' 
                AND TagAssociation_Status IS NULL) as tagNames,
            (SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_EventTemplateID
                AND TagAssociation_SourceTable = 'event_templates'
                AND TagAssociation_Status IS NULL) as templateTagNames
		FROM event_info
		JOIN event_log ON Event_ID = EventLog_EventID AND EventLog_Action = 'Created'
		LEFT JOIN merchant_info ON MerchantInfo_ID = event_ProposedByMerchantID
		LEFT JOIN organization_info ON OrganizationInfo_ID = event_ProposedByOrgID
        LEFT JOIN event_templates et ON Event_EventTemplateID = et.EventTemplate_ID
        LEFT JOIN cms_image_library ti ON et.EventTemplate_CalendarIconImageID = ti.id AND ti.status = 'ACTIVE'
		WHERE Event_ID = ?
		AND Event_Status = 'ACTIVE'";

    /**
     * Retrieves the core event and the merchant who created the event, if any.
     * @param $eventID
     * @return EventRecord
     */
    public function getEventById($eventID){
        $sqlString = self::$GET_EVENT_BY_ID;
        $sqlBindings = "i";
        $sqlParams = array($eventID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToRecord((object)$result[0]);
    }

    public function getEventByIdExploded($eventID){
        $collection = array();
        $coreEventRecord = $this->getEventById($eventID);
        if (!empty($coreEventRecord->cause->id)){
            $merchants = $this->getMerchantsForEvent($eventID);
            if (count($merchants)){
                foreach($merchants as $venue){
                    /* @var $venue Venue */
                    $eventRecord = clone $coreEventRecord; // shallow clone is fine
                    $eventRecord->venue = $venue;
                    $collection[] = $eventRecord;
                }
            } else {
                $collection[] = $coreEventRecord;
            }
        } else {
            $collection[] = $coreEventRecord;
        }
        return $collection;
    }

	private static $GET_MERCHANTS_FOR_EVENT_SQL = "
		SELECT
			merchant_info.MerchantInfo_ID,
			merchant_info.MerchantInfo_Name,
			merchant_info.MerchantInfo_Address,
			merchant_info.MerchantInfo_City,
			merchant_info.MerchantInfo_State,
			merchant_info.MerchantInfo_Zip,
			merchant_info.MerchantInfo_Country,
			merchant_info.MerchantInfo_Lat,
			merchant_info.MerchantInfo_Long,
			merchant_info.MerchantInfo_EventBriteVenueID,
			merchant_info.MerchantInfo_Website,
			merchant_info.MerchantInfo_Description,
			merchant_info.MerchantInfo_Neighborhood as venueNeighborhood,
			merchant_info.MerchantInfo_Keywords as venueKeywords,
			event_match.EventMatch_MerchantPercentage as Event_MerchantPercentage,
			event_match.EventMatch_MerchantSecondaryPercentage as Event_MerchantSecondaryPercentage,
			event_match.EventMatch_IsFlatDonation as isFlatDonation,
			event_match.EventMatch_DonationPerUnit as donationPerUnit,
			event_match.EventMatch_UnitDescriptor as unitDescriptor,
			event_match.EventMatch_FlatDonationActionPhrase as flatDonationActionPhrase,
			event_match.EventMatch_EventBriteEventID as Event_EventBriteEventID,
			event_match.EventMatch_FacebookEventID as Event_FacebookEventID,
			(
					SELECT pid
					FROM permalinks
					WHERE entity_name = 'merchant'
					AND entity_id = MerchantInfo_ID
					  AND status = 'ACTIVE'
					ORDER BY weight DESC
					LIMIT 1
			) as pid,
			(
					SELECT pid
					FROM permalinks
					WHERE entity_name = 'organization'
					AND entity_id = OrganizationInfo_ID
					  AND status = 'ACTIVE'
					ORDER BY weight DESC
					LIMIT 1
			) as orgPid
		FROM (event_match
		JOIN merchant_info ON MerchantInfo_ID = EventMatch_MerchantID
		JOIN organization_info ON OrganizationInfo_ID = EventMatch_OrgID)
		where EventMatch_EventID = ? AND EventMatch_Status = 'Accepted'";

    /**
     * For the specified EventID, returns an array of Venue object.
     * @param $eventID
     * @return array of Venue
     */
    public function getMerchantsForEvent($eventID){
        $sqlString = self::$GET_MERCHANTS_FOR_EVENT_SQL;
        $sqlBindings = "i";
        $sqlParams = array($eventID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $collection = array();
        foreach ($result as $row){
            $collection[] = $this->mapToVenue((object)$row);
        }
        return $collection;
    }

    /**
     * Called by Merchant Event Accept Process, which independently updates the indexden repository.
     * @param $EventID
     * @param $merchantID
     * @return array of EventRecord
     */
    public function getAllByEventAndMerchant($EventID,$merchantID) {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = true;
        $criteria->eventId = $EventID;
        $criteria->merchantId = $merchantID;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $criteria->multiColumnSort[] = new MultiColumnSort("timestamp", "desc");
        $paginatedResults = parent::get($criteria);
        return $paginatedResults->collection;
	}

    /**
     * Used by internal methods that independently update indexden repository.
     * @param $EventID
     * @param $merchantID
     * @return array
     */
    private function getAnyByEventAndMerchant($EventID,$merchantID) {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->eventId = $EventID;
        $criteria->merchantId = $merchantID;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $paginatedResults = parent::get($criteria);
        return $paginatedResults->collection;
	}

    private static $MERCHANT_EVENT_AUTOCOMPLETE = "
        SELECT
          ev.Event_Title as eventName,
          ev.Event_Description as eventDescription
        FROM event_info ev
        LEFT JOIN event_match em ON ev.Event_ID = em.EventMatch_EventID AND ev.Event_ProposedByOrgID > 0 AND em.EventMatch_Status = 'Accepted'
        WHERE ev.Event_Status = 'ACTIVE'
        AND ( ev.Event_ProposedByMerchantID = ? or em.EventMatch_MerchantID = ? )
        GROUP BY ev.Event_Title";

    /**
     * For a given merchant, return an array of event names and descriptions, reduced to unique names
     * @param $merchantId
     * @return array
     */
    public function getMerchantEventAutocomplete($merchantId){
        $sqlString = self::$MERCHANT_EVENT_AUTOCOMPLETE;
        $sqlBinding = "ii";
        $sqlParam = array($merchantId, $merchantId);
        return MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
    }

    private static $ORG_UPDATE_SQL = "
        UPDATE event_info SET
            Event_Title = ?,
            Event_Date = ?,
            Event_StartTime = ?,
            Event_EndDate = ?,
            Event_EndTime = ?,

            Event_OrgAnticipatedCount = ?,
            Event_OrgWhyYou = ?,
            Event_Description = ?,
            Event_LastEditedByMemberID = ?,
            Event_LastEditedDate = CURRENT_TIMESTAMP,
            Event_UpdatedByAdminID = ?
        WHERE Event_ID = ?";

    private static $ORG_UPDATE_BINDINGS = "ssssssssiii";

    // TODO: change this to use $EventRecord
    public function updateOldStyleProposedEvent($EventUpdateInfo, $memberId, $adminId = 0){
        $eventId = (int)$EventUpdateInfo["EventID"];
        // set up $sqlParams for all but the last two fields
        $sqlParams = Array(
            $EventUpdateInfo["Event_Title"],
            MySqlDateUpdate($EventUpdateInfo["Event_Date"]),
            $EventUpdateInfo["Event_StartTime"],
            MySqlDateUpdate($EventUpdateInfo["Event_EndDate"]),
            $EventUpdateInfo["Event_EndTime"],

            $EventUpdateInfo["Event_OrgAnticipatedCount"],
            $EventUpdateInfo["Event_OrgWhyYou"],
            $EventUpdateInfo["Event_Description"],
            $memberId,
            $adminId,

            $eventId,
        );
        $sqlEventUpdate = self::$ORG_UPDATE_SQL;
        $sqlParamTypes = self::$ORG_UPDATE_BINDINGS;

        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::executeWithLogging("EventProvider.updateOldStyleProposedEvent: SQL error:", $this->mysqli, $sqlEventUpdate, $sqlParamTypes, $sqlParams, $executeResponse);
        return $executeResponse;
    }

    /**
     * Used by the indexing migration.
     * @param $eventID
     * @return array
     */
    public function getAllByEvent($eventID) {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = true;
        $criteria->publishedOnly = true;
        $criteria->eventId = $eventID;
        $criteria->size = 0;
        $paginatedResults = parent::get($criteria);
        return $paginatedResults->collection;
	}

    /**
     * Used by the indexing migration.
     * @param null $Limit
     * @return array
     */
    public function getAllHistoric($Limit = NULL) {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->publishedOnly = true;
        $criteria->start = $Limit["Start"];
        $criteria->size = $Limit["End"];
        $paginatedResults = parent::get($criteria);
        return $paginatedResults->collection;
	}	
	
	private static $GET_ALL_HISTORIC_COUNT = "
		SELECT count(event_info.Event_ID) As Events
		FROM event_info
		JOIN event_log ON Event_ID = EventLog_EventID AND EventLog_Action = 'Created'
		JOIN event_match ON EventMatch_EventID = Event_ID AND EventMatch_Status = 'Accepted'
		WHERE Event_Status = 'ACTIVE'
		AND Event_ProposedByMerchantID = 0
		UNION ALL
		SELECT count(event_info.Event_ID) As Events
		FROM event_info
		JOIN event_log ON Event_ID = EventLog_EventID AND EventLog_Action = 'Created'
		JOIN merchant_info ON MerchantInfo_ID = event_ProposedByMerchantID
		WHERE Event_Status = 'ACTIVE'";

    /**
     * Used by the indexing migration
     * @return array
     */
    public function getAllHistoricCount() {
        $result_Events = MySqliHelper::get_result($this->mysqli, self::$GET_ALL_HISTORIC_COUNT);
		return $result_Events;
	}

    /**
     * Built specifically for the Merchant's Enter Sales page, this function returns an object with three arrays
     * of event data:
     *  - today's events
     *  - all past events
     *  - all future events
     *
     * @param $merchantID
     * @return object
     */
    public function getEventBreakdownForEnterSalesPage($merchantID){
        $sqlString = "
			SELECT
				event_info.Event_ID as id,
				event_info.Event_Title as title,
				event_info.Event_Date as startDate,
				event_info.Event_EndDate as endDate,
				event_info.Event_StartTime as startTime,
				event_info.Event_ProposedByMerchantID as ProposedByMerchantID,
				event_match.EventMatch_ID as EventMatchID,
				event_match.EventMatch_MerchantPercentage as featuredCausePercentage,
				event_match.EventMatch_MerchantSecondaryPercentage as customerChoicePercentage,
				event_match.EventMatch_IsFlatDonation as isFlatDonation,
				event_match.EventMatch_DonationPerUnit as donationPerUnit,
				event_match.EventMatch_UnitDescriptor as unitDescriptor,
				event_match.EventMatch_OrgID as orgID,
				organization_info.OrganizationInfo_Name as orgName,
				(event_info.Event_Date <= curdate() AND event_info.Event_EndDate >= curdate()) as IsToday,
				(event_info.Event_EndDate < curdate()) as IsHistory,
				(event_info.Event_Date > curdate()) as IsFuture,
				1 as IsFeaturedCause
			FROM event_info
			JOIN event_match ON EventMatch_EventID = Event_ID AND EventMatch_Status = 'Accepted'
			JOIN organization_info ON EventMatch_OrgID = OrganizationInfo_ID
			WHERE event_match.EventMatch_MerchantID = ?
			AND event_info.Event_Status = 'ACTIVE'
			UNION ALL
			SELECT
				event_info.Event_ID as id,
				event_info.Event_Title as title,
				event_info.Event_Date as startDate,
				event_info.Event_EndDate as endDate,
				event_info.Event_StartTime as startTime,
				event_info.Event_ProposedByMerchantID as ProposedByMerchantID,
				null as EventMatchID,
				event_info.Event_MerchantPercentage as featuredCausePercentage,
				event_info.Event_MerchantSecondaryPercentage as customerChoicePercentage,
				event_info.Event_IsFlatDonation as isFlatDonation,
				event_info.Event_DonationPerUnit as donationPerUnit,
				event_info.Event_UnitDescriptor as unitDescriptor,
				event_info.Event_ProposedByOrgID as orgID,
				organization_info.OrganizationInfo_Name as orgName,
				(event_info.Event_Date <= curdate() AND event_info.Event_EndDate >= curdate()) as IsToday,
				(event_info.Event_EndDate < curdate()) as IsHistory,
				(event_info.Event_Date > curdate()) as IsFuture,
				if (Event_ProposedByOrgID, 1 , 0) as IsFeaturedCause
			FROM event_info
			LEFT JOIN organization_info ON Event_ProposedByOrgID = OrganizationInfo_ID
			WHERE event_info.Event_ProposedByMerchantID = ?
			AND event_info.Event_Status = 'ACTIVE'
			ORDER BY startDate DESC, startTime DESC";
        $sqlBinding = "ii";
        $sqlParams = array($merchantID, $merchantID);
        $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        $pastEventsArray = array();
        $todaysEventsArray = array();
        $futureEventsArray = array();
        $mostRecentEvent = null;
        foreach ($sqlResult as $eventRow){
            $eventObject = (object)$eventRow;
            $featuredCause = null;
            if ($eventObject->IsFeaturedCause){
                $featuredCause = (object)array(
                    "id" => $eventObject->orgID,
                    "name" => $eventObject->orgName,
                    "percentage" => $eventObject->featuredCausePercentage
                );
            }
            $dateText = date("m/d/Y", strtotime($eventObject->startDate));
            if (strtotime($eventObject->endDate) > strtotime($eventObject->startDate)){
                $dateText .= " through ".date("m/d/Y", strtotime($eventObject->endDate));
            }
            $recordObject = (object)array(
                "id" => $eventObject->id,
                "title" => $eventObject->title,
                "dateDisplay" => $dateText,
                "endDate" => $eventObject->endDate,
                "customerChoicePercentage" => $eventObject->customerChoicePercentage,
                "featuredCause" => $featuredCause,
                "isFlatDonation" => $eventObject->isFlatDonation,
                "donationPerUnit" => $eventObject->donationPerUnit,
                "unitDescriptor" => $eventObject->unitDescriptor
            );
            if ($eventObject->IsToday){
                $todaysEventsArray[] = $recordObject;
            } else if ($eventObject->IsHistory){
                $pastEventsArray[] = $recordObject;
            } else if ($eventObject->IsFuture){
                // prepend so future events are sorted in ASC
                array_unshift($futureEventsArray, $recordObject);
            }
        }
        if (!count($todaysEventsArray) && count($pastEventsArray)){
            // add the most recent past event to today's events if within the last 7 days
            $mostRecentEvent = $pastEventsArray[0];
            if (strtotime($mostRecentEvent->endDate) < strtotime('-7 days')){
                $mostRecentEvent = null;
            }
        }
        return (object)array(
            "todaysEvents" => $todaysEventsArray,
            "pastEvents" => $pastEventsArray,
            "futureEvents" => $futureEventsArray,
            "mostRecentEvent" => $mostRecentEvent
        );
    }

    private static $GET_ALL_PROPOSALS_FOR_MERCHANT_SQL = "
		SELECT
			0 as Event_ID,
			EventProposalVersions_ID as VersionID,
			EventProposalVersions_Title as Event_Title,
			EventProposalVersions_Description as Event_Description,
			EventProposals_CreatedDate as Event_CreatedDate,
			EventProposalVersions_CreatedDate as Event_EditedDate,
			EventProposalVersions_StartDate as Event_Date,
			EventProposalVersions_StartTime as Event_StartTime,
			EventProposalVersions_EndDate as Event_EndDate,
			EventProposalVersions_EndTime as Event_EndTime,
			EventProposalVersions_Percentage as Event_MerchantPercentage,
			EventProposalVersions_ExpectedAttendance as Event_OrgAnticipatedCount,
			EventProposalVersions_OrgWhyYou as Event_OrgWhyYou,
			0 as Event_MerchantSecondaryPercentage,
			'' as Event_FacebookEventID,
			'' as Event_EventBriteEventID,
			merchant_info.*,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_Website,
			organization_info.OrganizationInfo_EIN,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid
		FROM event_proposals
		JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
		JOIN merchant_info ON MerchantInfo_ID = EventProposals_MerchantID
		JOIN organization_info ON OrganizationInfo_ID = EventProposals_OrgID
		WHERE MerchantInfo_ID = ?
		AND EventProposalVersions_Status = 'PENDING'
		AND EventProposalVersions_StartDate > CURRENT_DATE()

		UNION

		SELECT
			Event_ID,
			0 as VersionID,
			Event_Title,
			Event_Description,
			(SELECT EventLog_Timestamp FROM event_log WHERE EventLog_EventID = Event_ID AND EventLog_Action = 'Created' ORDER BY EventLog_ID limit 1) as Event_CreatedDate,
			Event_LastEditedDate as Event_EditedDate,
			Event_Date,
			Event_StartTime,
			Event_EndDate,
			Event_EndTime,
			Event_MerchantPercentage,
			Event_OrgAnticipatedCount,
			Event_OrgWhyYou,
			Event_MerchantSecondaryPercentage,
			Event_FacebookEventID,
			Event_EventBriteEventID,
			merchant_info.*,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_Website,
			organization_info.OrganizationInfo_EIN,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid
	    FROM event_info
		JOIN organization_info ON OrganizationInfo_ID = Event_ProposedByOrgID
		LEFT JOIN merchant_info ON MerchantInfo_ID = 0
		WHERE Event_ProposedByMerchantID = 0
		AND Event_Status = 'ACTIVE'
		AND Event_Date > CURRENT_DATE()
		AND NOT EXISTS (
		    SELECT 1
		    FROM event_match
		    WHERE EventMatch_EventID = Event_ID
		)";

    public function getNextMerchantProposedEvents($merchantID, $startIndex, $maxRecords, $dateSort = "ASC"){
        if (strtolower($dateSort) != "desc"){
            $dateSort = "ASC";
        }
        $sqlString = self::$GET_ALL_PROPOSALS_FOR_MERCHANT_SQL." ORDER BY Event_Date ".$dateSort.", Event_StartTime ".$dateSort;
        $sqlBindings = "i";
        $sqlParams = array($merchantID);
        return $this->getPaginatedEvents($sqlString, $startIndex, $maxRecords, $sqlBindings, $sqlParams);
    }

    private static $GET_NONPROFIT_PROPOSED_EVENTS = "
        SELECT 1
        FROM event_info
        WHERE Event_Status = 'ACTIVE'
        AND  Event_EndDate >= CURRENT_DATE
        AND Event_ProposedByMerchantID = 0
        ORDER BY Event_Date, Event_StartTime";

    public function hasNonprofitProposedEvents(){
        $sqlString = self::$GET_NONPROFIT_PROPOSED_EVENTS;
        $result = MySqliHelper::get_result($this->mysqli, $sqlString);
        return count($result) > 0;
    }

    private static $GET_PENDING_MERCHANT_BY_VERSION_SQL = "
		SELECT
			0 as Event_ID,
			EventProposalVersions_ID as VersionID,
			EventProposalVersions_Title as Event_Title,
			EventProposalVersions_Description as Event_Description,
			EventProposals_CreatedDate as Event_CreatedDate,
			EventProposalVersions_CreatedDate as Event_EditedDate,
			EventProposalVersions_StartDate as Event_Date,
			EventProposalVersions_StartTime as Event_StartTime,
			EventProposalVersions_EndDate as Event_EndDate,
			EventProposalVersions_EndTime as Event_EndTime,
			EventProposalVersions_Percentage as Event_MerchantPercentage,
			EventProposalVersions_ExpectedAttendance as Event_OrgAnticipatedCount,
			EventProposalVersions_OrgWhyYou as Event_OrgWhyYou,
			EventProposalVersions_Status as VersionStatus,
			0 as Event_MerchantSecondaryPercentage,
			'' as Event_FacebookEventID,
			'' as Event_EventBriteEventID,
			merchant_info.*,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_Website,
			organization_info.OrganizationInfo_EIN,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid
		FROM event_proposals
		JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
		JOIN merchant_info ON MerchantInfo_ID = EventProposals_MerchantID
		JOIN organization_info ON OrganizationInfo_ID = EventProposals_OrgID
		WHERE MerchantInfo_ID = ?
		AND EventProposalVersions_ID = ?";

    public function getMerchantProposedEvent($merchantId, $versionId){
        $sqlString = self::$GET_PENDING_MERCHANT_BY_VERSION_SQL;
        $sqlBindings = "ii";
        $sqlParams = array( $merchantId, $versionId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            $datarow = (object)$result[0];
            $record = $this->mapToRecord($datarow);
            return $record;
        } else {
            return new EventRecord();
        }
    }

    private static $GET_PENDING_ORG_BY_VERSION_SQL = "
		SELECT
			EventProposals_EventID as Event_ID,
			EventProposalVersions_ID as VersionID,
			EventProposalVersions_Title as Event_Title,
			EventProposalVersions_Description as Event_Description,
			EventProposals_CreatedDate as Event_CreatedDate,
			EventProposalVersions_CreatedDate as Event_EditedDate,
			EventProposalVersions_StartDate as Event_Date,
			EventProposalVersions_StartTime as Event_StartTime,
			EventProposalVersions_EndDate as Event_EndDate,
			EventProposalVersions_EndTime as Event_EndTime,
			EventProposalVersions_Percentage as Event_MerchantPercentage,
			EventProposalVersions_ExpectedAttendance as Event_OrgAnticipatedCount,
			EventProposalVersions_OrgWhyYou as Event_OrgWhyYou,
			EventProposalVersions_Status as VersionStatus,
			0 as Event_MerchantSecondaryPercentage,
			'' as Event_FacebookEventID,
			'' as Event_EventBriteEventID,
			merchant_info.*,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_Website,
			organization_info.OrganizationInfo_EIN,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid
		FROM event_proposals
		JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
		JOIN merchant_info ON MerchantInfo_ID = EventProposals_MerchantID
		JOIN organization_info ON OrganizationInfo_ID = EventProposals_OrgID
		WHERE OrganizationInfo_ID = ?
		AND EventProposalVersions_ID = ?";

    public function getOrganizationProposedEvent($orgId, $versionId){
        $sqlString = self::$GET_PENDING_ORG_BY_VERSION_SQL;
        $sqlBindings = "ii";
        $sqlParams = array( $orgId, $versionId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            $datarow = (object)$result[0];
            $record = $this->mapToRecord($datarow);
            return $record;
        } else {
            return new EventRecord();
        }
    }

    private static $GET_EVENT_FOR_MERCHANT_SQL = "
		SELECT
			event_info.Event_ID as ID,
			event_info.Event_Title as Title,
			event_info.Event_Date as Date,
			event_info.Event_EndDate as EndDate,
			event_info.Event_StartTime,
			event_info.Event_ProposedByMerchantID as ProposedByMerchantID,
			event_info.Event_PreferredOrgEIN,
			null as customAttendLink,
			event_info.Event_IsNational,
			event_info.Event_IsFcOnly as isFcOnly,
			event_info.Event_OnlineLocationLabel as onlineLocationLabel,
			event_info.Event_OnlineLocationLink as onlineLocationLink,
			event_match.EventMatch_ID as EventMatchID,
			event_match.EventMatch_MerchantPercentage as MerchantPercentage,
			event_match.EventMatch_MerchantSecondaryPercentage as MerchantSecondaryPercentage,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_EIN,
			merchant_info.MerchantInfo_ID,
			merchant_info.MerchantInfo_Name,
			merchant_info.MerchantInfo_Address,
			merchant_info.MerchantInfo_City,
			merchant_info.MerchantInfo_State,
			merchant_info.MerchantInfo_Zip,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid,
			(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames
		FROM event_info
		JOIN event_match ON EventMatch_EventID = Event_ID AND EventMatch_Status = 'Accepted'
		JOIN organization_info ON OrganizationInfo_ID = EventMatch_OrgID
		JOIN merchant_info ON MerchantInfo_ID = EventMatch_MerchantID
		WHERE event_match.EventMatch_MerchantID = ?
		AND event_info.Event_Status = 'ACTIVE'
		AND event_info.Event_ID = ?
		UNION ALL
		SELECT
			event_info.Event_ID as ID,
			event_info.Event_Title as Title,
			event_info.Event_Date as Date,
			event_info.Event_EndDate as EndDate,
			event_info.Event_StartTime,
			event_info.Event_ProposedByMerchantID as ProposedByMerchantID,
			event_info.Event_PreferredOrgEIN,
			event_info.Event_CustomAttendLink as customAttendLink,
			event_info.Event_IsNational,
			event_info.Event_IsFcOnly as isFcOnly,
			event_info.Event_OnlineLocationLabel as onlineLocationLabel,
			event_info.Event_OnlineLocationLink as onlineLocationLink,
			null as EventMatchID,
			event_info.Event_MerchantPercentage as MerchantPercentage,
			event_info.Event_MerchantSecondaryPercentage as MerchantSecondaryPercentage,
			organization_info.OrganizationInfo_ID,
			organization_info.OrganizationInfo_Name,
			organization_info.OrganizationInfo_EIN,
			merchant_info.MerchantInfo_ID,
			merchant_info.MerchantInfo_Name,
			merchant_info.MerchantInfo_Address,
			merchant_info.MerchantInfo_City,
			merchant_info.MerchantInfo_State,
			merchant_info.MerchantInfo_zip,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as orgPid,
			(SELECT group_concat(TagList_Name ORDER BY TagAssociation_SortOrder, TagList_Name)
                FROM tag_association
                JOIN tag_list ON TagAssociation_TagListID = TagList_ID
                WHERE TagAssociation_SourceID = Event_ID
                AND TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_Status IS NULL) as tagNames
		FROM event_info
		JOIN merchant_info ON MerchantInfo_ID = event_info.Event_ProposedByMerchantID
		LEFT JOIN organization_info ON OrganizationInfo_ID = Event_PreferredOrgID
		WHERE event_info.Event_ProposedByMerchantID = ?
		AND event_info.Event_Status = 'ACTIVE'
		AND event_info.Event_ID = ?
		ORDER BY Date DESC, Event_StartTime DESC";

    /**
     * @deprecated use getMerchantEvent
     * @param $EventID
     * @param $merchantID
     * @return array
     */
    public function getEventForMerchant($EventID, $merchantID){
		$sqlString = self::$GET_EVENT_FOR_MERCHANT_SQL;
		$sqlBindings = "iiii";
		$sqlParams = array($merchantID, $EventID, $merchantID, $EventID);
		return MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
	}

    public function getMerchantEvent($eventId, $merchantId){
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->eventId = $eventId;
        $criteria->merchantId = $merchantId;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $paginatedResults = parent::get($criteria);
        return $paginatedResults->collection;
    }

	private function getPaginatedEvents($sqlString, $startIndex, $maxRecords, $sqlBindings = null, $sqlParams = null){
		if (!is_numeric($startIndex) || $startIndex < 0){
			$startIndex = 0;
		}
		if (!is_numeric($maxRecords) || $maxRecords <= 0){
			$maxRecords = 5;
		}
        $result_Events = MySqliHelper::get_result($this->mysqli, $sqlString." LIMIT ".$startIndex.",".($maxRecords + 1), $sqlBindings, $sqlParams);
		$paginatedResult = new PaginatedResult();
		$records = $this->mapToCollection($result_Events);
		$paginatedResult->collection = array_slice($records, 0, $maxRecords);
		$paginatedResult->startIndex = $startIndex;
		$paginatedResult->maxLength = $maxRecords;
		$paginatedResult->hasMore = count($records) > $maxRecords;
		return $paginatedResult;
	}

	private function mapToCollection($query_results) {
		$collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToRecord($row);
        }
		return $collection;
	}
	
	protected function mapToRecord($datarow) {
		$record = new EventRecord();
		$record->id = $datarow->Event_ID;
		$record->title = $datarow->Event_Title;
		$record->description = $datarow->Event_Description;
		$record->createdDate = strtotime($datarow->Event_CreatedDate);
		$record->lastUpdatedDate = strtotime($datarow->Event_EditedDate);
        $record->setStartDate($datarow->Event_Date, $datarow->Event_StartTime);
        $record->setEndDate($datarow->Event_EndDate, $datarow->Event_EndTime);
		$record->isMultiDay = $datarow->Event_Date != $datarow->Event_EndDate;
		$record->lastEditedByMemberID = $datarow->Event_LastEditedByMemberID;
        $record->commaDelimitedTagNames = $datarow->tagNames;
        $record->customAttendLink = $datarow->customAttendLink;
        $record->isNationalEvent = $datarow->Event_IsNational;
        $record->isOnlineEvent = $datarow->Event_IsOnline;
        $record->onlineLocationLabel = $datarow->onlineLocationLabel;
        $record->onlineLocationLink = $datarow->onlineLocationLink;
        $record->suppressCharitySelectionForm = $datarow->Event_SuppressCharitySelectionForm;
        $record->suppressPrintFlier = $datarow->Event_SuppressPrintFlier;
        $record->isCausetownEvent = $datarow->Event_IsCausetownEvent;
        $record->calendarIconFile = $datarow->calendarIconFile;
        $record->calendarIconPath = $datarow->calendarIconPath;
        $record->templateTagNames = $datarow->templateTagNames;

        if (isset($datarow->VersionID)){
            $record->versionId = $datarow->VersionID;
        }
        if (isset($datarow->Event_OrgWhyYou)){
            $record->whyHostThisDescription = $datarow->Event_OrgWhyYou;
        }
        if (isset($datarow->Event_OrgAnticipatedCount)){
            $record->expectedAttendance = $datarow->Event_OrgAnticipatedCount;
        }
        if (isset($datarow->VersionStatus)){
            $record->versionStatus = $datarow->VersionStatus;
        }

		$record->venue = $this->mapToVenue($datarow);

        $record->cause = new Cause();
		if (is_numeric($datarow->OrganizationInfo_ID)){
			$record->cause->id = $datarow->OrganizationInfo_ID;
			$record->cause->name = $datarow->OrganizationInfo_Name;
			$record->cause->ein = $datarow->OrganizationInfo_EIN;
			$record->cause->setURL($datarow->OrganizationInfo_Website);
            $record->cause->pid = $datarow->orgPid;
		}
		return $record;
	}

	private function mapToVenue($datarow){
		$venue = new Venue();
		$venue->id = $datarow->MerchantInfo_ID;
        $venue->pid = $datarow->pid;
		$venue->name = $datarow->MerchantInfo_Name;
		$venue->url = $venue->setURL($datarow->MerchantInfo_Website);
		$venue->description = $datarow->MerchantInfo_Description;
		$venue->location = $this->getEventLocation($venue->url, $venue->name);
		if (is_numeric($datarow->MerchantInfo_Lat)){
			$venue->lat = $datarow->MerchantInfo_Lat;
		}
		if (is_numeric($datarow->MerchantInfo_Long)){
			$venue->long = $datarow->MerchantInfo_Long;
		}
		$venue->streetAddr = $datarow->MerchantInfo_Address;
		$venue->city = $datarow->MerchantInfo_City;
		$venue->region = $datarow->MerchantInfo_State;
		$venue->postalCode = $datarow->MerchantInfo_Zip;
		$venue->country = $datarow->MerchantInfo_Country;
		$venue->phone = $this->getPhone($datarow->MerchantInfo_Phone);
		if (is_numeric($datarow->Event_MerchantPercentage)){
			$venue->featuredCausePercentage = $datarow->Event_MerchantPercentage;
		}
		//overide if event_match data used during migration scripts
		if ($datarow->EventMatch_MerchantPercentage){
			$venue->featuredCausePercentage = $datarow->EventMatch_MerchantPercentage;
		}
		
		if (is_numeric($datarow->Event_MerchantSecondaryPercentage)){
			$venue->customerChoicePercentage = $datarow->Event_MerchantSecondaryPercentage;
		}
		//overide if event_match data used during migration scripts
		if ($datarow->EventMatch_MerchantSecondaryPercentage){
			$venue->customerChoicePercentage = $datarow->EventMatch_MerchantSecondaryPercentage;
		}
		
		$venue->facebookEventID = $datarow->Event_FacebookEventID;
		//overide if event_match data used during migration scripts
		if ($datarow->EventMatch_FacebookEventID){
			$venue->facebookEventID = $datarow->EventMatch_FacebookEventID;
		}
		
		$venue->eventBriteEventID = $datarow->Event_EventBriteEventID;
		//overide if event_match data used during migration scripts
		if ($datarow->EventMatch_EventBriteEventID){
			$venue->eventBriteEventID = $datarow->EventMatch_EventBriteEventID;
		}
		
        $venue->eventBriteVenueID = $datarow->MerchantInfo_EventBriteVenueID;
        $venue->fipsCounty = $datarow->FIPS_County;

        $venue->isFlatDonation = $datarow->isFlatDonation;
        $venue->donationPerUnit = $datarow->donationPerUnit;
        $venue->unitDescriptor = $datarow->unitDescriptor;
        $venue->flatDonationActionPhrase = $datarow->flatDonationActionPhrase;
        $venue->eventTemplatePremiumId = $datarow->eventTemplatePremiumId;
        $venue->keywords = $datarow->venueKeywords;
        $venue->neighborhood = $datarow->venueNeighborhood;

        $venue->isFcOnly = $datarow->isFcOnly;
		return $venue;
	}
	
	private function getFullDate($dateString, $timeString) {
		if (substr_compare($dateString, "0000", 0, 4) == 0){
			return NULL;
		}
		return strtotime($dateString." ".$timeString);
	}
	
	private function getEventLocation($merchantWebsite, $merchantName) {
		return ($merchantWebsite) ? $merchantWebsite : $merchantName;
	}
	
	private function getPhone($phoneString) {
        $cleanPhone = preg_replace("/[^0-9]/", "", $phoneString);
		if (strlen($cleanPhone) > 6){
            return preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $cleanPhone);

			//return "(".substr($cleanPhone, 0, 3).") ".substr($cleanPhone, 3, 3)."-".substr($cleanPhone,6);
		}
		return $cleanPhone;
	}

    // ------------------
    // PENDING EVENTS
    // ------------------

    private static $INSERT_EVENT_PROPOSAL_SQL = "
        INSERT INTO
            event_proposals
        (
            EventProposals_MerchantID,
            EventProposals_OrgID,
            EventProposals_EventStartDate,
            EventProposals_CreatedByMemberID
        )
        SELECT ?,?,?,?
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT 1
            FROM event_proposals
            WHERE EventProposals_MerchantID = ?
            AND EventProposals_OrgID = ?
            AND EventProposals_EventStartDate = ?
            AND EventProposals_EventID IS NULL
        )";

    private static $GET_EVENT_PROPOSAL_SQL = "
        SELECT
            EventProposals_ID as id,
            EventProposals_EventID as event_id,
            EventProposals_MerchantID as merchant_id,
            EventProposals_EventStartDate as start_date,
            EventProposals_OrgID as org_id,
            EventProposals_Status as status,
            EventProposals_CreatedDate as created_date,
            EventProposals_CreatedByMemberID as created_by
        FROM event_proposals
        WHERE EventProposals_MerchantID = ?
        AND EventProposals_OrgID = ?
        AND EventProposals_EventStartDate = ?
        ORDER BY EventProposals_ID DESC";

    private function getOrCreateEventProposal($merchantID, $orgID, $startDate, $memberID){
        $sqlString = self::$INSERT_EVENT_PROPOSAL_SQL;
        $sqlBindings = "iisiiis";
        $sqlParams = array($merchantID, $orgID, $startDate, $memberID, $merchantID, $orgID, $startDate);
        $response = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $response);
        if ($result){
            // successfully inserted
            return $response->insertedId;
        } else {
            // must have already existed
            $sqlString = self::$GET_EVENT_PROPOSAL_SQL;
            $sqlBindings = "iis";
            $sqlParams = array($merchantID, $orgID, $startDate);
            $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
            if (count($result)){
                $row = (object)$result[0]; // TODO: get
                return $row->id;
            }
        }
        return null;
    }

    private static $INSERT_EVENT_PROPOSAL_VERSION_SQL = "
        INSERT INTO
            event_proposal_versions
        (
            EventProposalVersions_EventProposalsID,
            EventProposalVersions_Title,
            EventProposalVersions_Description,
            EventProposalVersions_OrgWhyYou,
            EventProposalVersions_ExpectedAttendance,

            EventProposalVersions_StartDate,
            EventProposalVersions_StartTime,
            EventProposalVersions_EndDate,
            EventProposalVersions_EndTime,

            EventProposalVersions_Percentage,
            EventProposalVersions_CreatedByMemberID
        )
        SELECT
            ?,?,?,?,?,
            ?,?,?,?,
            ?,?
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT 1
            FROM event_proposal_versions
            WHERE EventProposalVersions_EventProposalsID = ?
            AND EventProposalVersions_Status = 'PENDING'
        )";

    public function addPendingEvent(EventRecord $eventRecord, $memberID, $availableDateRecord = null){
        $response = new stdClass();
        // TODO: move validation to back end
        // validate merchantID and orgID are set
        $eventProposalStartDate = ($availableDateRecord != null) ? $availableDateRecord->date : date("Y-m-d H:i", $eventRecord->startDate);
        $merchantId = (isset($eventRecord->venue->id)) ? $eventRecord->venue->id : 0;
        $orgId = (isset($eventRecord->cause->id)) ? $eventRecord->cause->id : 0;
        $eventProposalId = $this->getOrCreateEventProposal($merchantId, $orgId, $eventProposalStartDate, $memberID);
        if ($eventProposalId){
            $startDateAsTime = $eventRecord->startDate;
            $endDateAsTime = $eventRecord->endDate;
            $percentage = (isset($eventRecord->venue->featuredCausePercentage)) ? $eventRecord->venue->featuredCausePercentage : 0;

            $sqlString = self::$INSERT_EVENT_PROPOSAL_VERSION_SQL;
            $sqlBindings = "issssssssiii";
            $sqlParams = array(
                $eventProposalId,
                $eventRecord->title,
                $eventRecord->description."",
                $eventRecord->whyHostThisDescription."",
                $eventRecord->expectedAttendance."",
                date("Y-m-d", $startDateAsTime),
                date("H:i", $startDateAsTime),
                date("Y-m-d", $endDateAsTime),
                date("H:i", $endDateAsTime),
                $percentage,
                $memberID,
                $eventProposalId
            );
            $executeResponse = new ExecuteResponseRecord();
            $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
            if ($result){
                $record = new stdClass();
                $record->id = $executeResponse->insertedId;
                $response->record = $record;
            } else {
                if ($executeResponse->success && $executeResponse->affectedRows == 0){
                    $response->message = "There is already a pending event from this nonprofit to this merchant on this date.";
                } else {
                    $response->message = $executeResponse->error;
                }
            }
        } else {
            $response->message = "Error adding pending event.";
        }
        return $response;
    }

    private static $GET_PROPOSAL_BY_VERSION_ID_SQL = "
        SELECT
            EventProposals_ID as proposalId,
            EventProposals_EventID as eventId,
            EventProposals_MerchantID as merchantId,
            EventProposals_OrgID as orgId,
            EventProposals_Status as proposalStatus,
            EventProposalVersions_ID as versionId,
            EventProposalVersions_Percentage as percentage,
            EventProposalVersions_Status as versionStatus
        FROM event_proposals
        JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
        WHERE EventProposalVersions_ID = ?";

    public function acceptProposedEvent(EventRecord $eventRecord, $memberID, $adminId){
        $response = new stdClass();
        // TODO: move validation to back end
        // validate merchantID and orgID are set
        $versionId = $eventRecord->versionId;
        if ($versionId > 0){
            $versionResult = MySqliHelper::get_result($this->mysqli, self::$GET_PROPOSAL_BY_VERSION_ID_SQL, "i", array($versionId));
            $merchantId = $eventRecord->venue->id;
            $orgId = $eventRecord->cause->id;
            if (count($versionResult)){
                $versionRow = (object)$versionResult[0];
                // validate that this is the merchant and org
                if ($versionRow->merchantId != $merchantId){
                    $response->message = "Access Denied.";
                } else if ($versionRow->orgId != $orgId){
                    $response->message = "This proposal appears to be from a different organization. Please check the request.";
                } else if ($versionRow->percentage > (int)$eventRecord->venue->featuredCausePercentage){
                    $response->message = "Featured Cause Percentage must be the same or higher than in the original proposal.";
                } else {
                    // validation passed
                    $eventInsertResponse = $this->createNonprofitProposedEvent($eventRecord, $memberID, $adminId);
                    if ($eventInsertResponse->success){
                        $eventID = $eventInsertResponse->insertedId;
                        $eventRecord->id = $eventID;
                        $this->markProposalAccepted($versionId, $eventID, $versionRow->proposalId, $memberID);
                        $this->addMerchantAsHost($eventRecord, $memberID);
                        $response->record = $eventRecord;
                    }
                }
            } else {
                $response->message = "Record not found.";
            }
        } else {
            // TODO: possibly add validation. At this point, the assumption is that we already know by this point that this merchant isn't already hosting this event
            $this->addMerchantAsHost($eventRecord, $memberID);
        }
        return $response;
    }

    private static $ACCEPT_PROPOSAL_VERSION = "
        UPDATE event_proposals
        JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
        SET EventProposals_Status = 'ACCEPTED',
            EventProposals_EventID = ?,
            EventProposalVersions_Status = 'ACCEPTED',
            EventProposalVersions_UpdatedDate = CURRENT_TIMESTAMP(),
            EventProposalVersions_UpdatedByMemberID = ?
        WHERE EventProposalVersions_ID = ?";

    private static $CANCEL_PENDING_VERSIONS = "
        UPDATE event_proposals
        JOIN event_proposal_versions ON EventProposals_ID = EventProposalVersions_EventProposalsID
        SET EventProposalVersions_Status = 'DECLINED',
            EventProposalVersions_UpdatedDate = CURRENT_TIMESTAMP(),
            EventProposalVersions_UpdatedByMemberID = ?
        WHERE EventProposals_ID = ?
        AND EventProposals_Status != 'PENDING'
        AND EventProposalVersions_Status = 'PENDING'";

    private function markProposalAccepted($versionID, $eventID, $proposalID, $memberID){
        $result = MySqliHelper::execute($this->mysqli, self::$ACCEPT_PROPOSAL_VERSION, "iii", array($eventID, $memberID, $versionID));
        if ($result){
            $result = MySqliHelper::execute($this->mysqli, self::$CANCEL_PENDING_VERSIONS, "ii", array($memberID, $proposalID));
        }
    }

    private static $DECLINE_PROPOSAL_VERSION = "
        UPDATE event_proposal_versions
        SET EventProposalVersions_Status = 'DECLINED',
            EventProposalVersions_Comment = ?,
            EventProposalVersions_UpdatedDate = CURRENT_TIMESTAMP(),
            EventProposalVersions_UpdatedByMemberID = ?
        WHERE EventProposalVersions_ID = ?";

    public function markProposalVersionDeclined($versionId, $comment, $memberID){
        $sqlString = self::$DECLINE_PROPOSAL_VERSION;
        $sqlBindings = "sii";
        $sqlParams = array($comment, $memberID, $versionId);
        $executeResult = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResult);
        return $executeResult;
    }

    private static $INSERT_EVENT_INFO_PROPOSEDBYORG_SQL = "
        INSERT INTO event_info
        (
            Event_SubmittedByMemberID,
            Event_ProposedByOrgID,
            Event_Date,
            Event_EndDate,
            Event_StartTime,
            Event_EndTime,

            Event_Title,
            Event_OrgAnticipatedCount,
            Event_Description,
            Event_OrgWhyYou,
            Event_LastEditedByMemberID,

            Event_LastEditedDate,
            Event_CreatedByMemberID,
            Event_CreatedByAdminID
        ) VALUES (
            ?,?,?,?,?,?,
            ?,?,?,?,?,
            CURRENT_TIMESTAMP,?,?
        )";

    public function createNonprofitProposedEvent(EventRecord $eventRecord, $memberID, $adminID = 0){
        // skipping validation -- assuming calling program has already ensured this
        $sqlString = self::$INSERT_EVENT_INFO_PROPOSEDBYORG_SQL;
        $sqlBinding = "iissssssssiii";
        $sqlParams = array(
            $memberID,
            $eventRecord->cause->id,
            date("Y-m-d", $eventRecord->startDate),
            date("Y-m-d", $eventRecord->endDate),
            date("H:i", $eventRecord->startDate),
            date("H:i", $eventRecord->endDate),

            $eventRecord->title,
            $eventRecord->expectedAttendance,
            $eventRecord->description,
            $eventRecord->whyHostThisDescription,
            $memberID,

            $memberID,
            $adminID
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $insertResponse);

        if ($insertResponse->success){
            // add action to event_log
            $logged = MySqliHelper::log_event_action($this->mysqli, $insertResponse->insertedId, 'Created', $memberID);
        }

        return $insertResponse;
    }

    private static $ADD_MERCHANT_AS_HOST = "
        INSERT INTO event_match
        (
            EventMatch_EventID,
            EventMatch_OrgID,
            EventMatch_OrgEIN,
            EventMatch_MerchantID,

            EventMatch_MerchantPercentage,
            EventMatch_MerchantSecondaryPercentage,
            EventMatch_Source,
            EventMatch_Status
        )
        VALUES
        (
            ?,?,?,?,
            ?,?,'Merchant','Accepted'
        )";

	private function addMerchantAsHost(EventRecord $eventRecord, $memberID){
        // no validation -- assuming calling program has already ensured that this merchant isn't already hosting
        $sqlString = self::$ADD_MERCHANT_AS_HOST;
        $sqlBindings = "iisiii";
        $sqlParams = array(
            $eventRecord->id,
            $eventRecord->cause->id,
            $eventRecord->cause->ein,
            $eventRecord->venue->id,
            $eventRecord->venue->featuredCausePercentage,
            $eventRecord->venue->customerChoicePercentage
        );
        $executeResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);

        // add action to event_log
        $logged = MySqliHelper::log_event_action($this->mysqli, $eventRecord->id, 'Matched to Merchant', $memberID);
		
		$indexResult = $this->getAllByEventAndMerchant($eventRecord->id, $eventRecord->venue->id);
		$x = 0;
		foreach ($indexResult as $uEvents=>$upcomingEvent){
            $documents[$x] = $upcomingEvent->convertToIndexedDocument();
			$x++;
		}
		$IndexingProvider = new Indexing_Api($GLOBALS['IndexingProviderURL']);
		$IndexingThisIndex = new Indexing_Index($IndexingProvider,$GLOBALS['IndexName']);
		$IndexAddDocuments = $IndexingThisIndex->add_documents($documents);
		$logged = MySqliHelper::log_event_action($this->mysqli, $eventRecord->id,'Indexed docid='.$eventRecord->id."_".$eventRecord->venue->id, $memberID);

    }

    private static $INSERT_EVENT_INFO_PROPOSEDBYMERCHANT_SQL = "
        INSERT INTO event_info
        (
            Event_SubmittedByMemberID,
            Event_ProposedByMerchantID,
            Event_Date,
            Event_EndDate,
            Event_StartTime,
            Event_EndTime,

            Event_Title,
            Event_Description,
            Event_MerchantSecondaryPercentage,
            Event_LastEditedByMemberID,

            Event_IsFlatDonation,
            Event_DonationPerUnit,
            Event_UnitDescriptor,
            Event_FlatDonationActionPhrase,

            Event_LastEditedDate,
            Event_CreatedByMemberID,
            Event_CreatedByAdminID,
            Event_EventTemplateID
        ) VALUES (
            ?,?,?,?,?,?,
            ?,?,?,?,
            ?,?,?,?,
            CURRENT_TIMESTAMP,?,?,?
        )";

    private static $ADD_EVENTID_TO_DRAFT_SQL = "
        UPDATE event_draft
        SET EventDraft_EventID = ?
        WHERE EventDraft_ID = ?";

    private static $ADD_TEMPLATE_TAGS_TO_EVENT_SQL = "
        INSERT INTO tag_association
        (
            TagAssociation_SourceTable,
            TagAssociation_SourceID,
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        )
        SELECT
            'event_info',
            ?, -- event id
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        FROM tag_association input
        WHERE input.TagAssociation_SourceTable = 'event_templates'
        AND input.TagAssociation_SourceID = ? -- templateID
        AND input.TagAssociation_Status IS NULL
        AND NOT EXISTS (
            SELECT 1
            FROM tag_association dupe
            WHERE dupe.TagAssociation_SourceTable = 'event_info'
            AND dupe.TagAssociation_SourceID = ? -- event ID
            AND dupe.TagAssociation_TagListID = input.TagAssociation_TagListID
            AND dupe.TagAssociation_Status IS NULL
        )";

    /**
     * @param EventRecord $eventRecord
     * @param $memberID
     * @param null $adminId
     * @return ExecuteResponseRecord
     * @deprecated use MySqliEventManager->createEvent
     */
    public function createCustomerChoiceEvent(EventRecord $eventRecord, $memberID, $adminId = null){
        // skipping validation -- assuming calling program has already ensured this
        $sqlString = self::$INSERT_EVENT_INFO_PROPOSEDBYMERCHANT_SQL;
        $sqlBinding = "iissssssiiidssiii";
        $sqlParams = array(
            $memberID,
            $eventRecord->venue->id,
            date("Y-m-d", $eventRecord->startDate),
            date("Y-m-d", $eventRecord->endDate),
            date("H:i", $eventRecord->startDate),
            date("H:i", $eventRecord->endDate),

            $eventRecord->title,
            $eventRecord->description,
            $eventRecord->venue->customerChoicePercentage,
            $memberID,

            $eventRecord->venue->isFlatDonation,
            $eventRecord->venue->donationPerUnit,
            $eventRecord->venue->unitDescriptor,
            $eventRecord->venue->flatDonationActionPhrase,

            $memberID,
            $adminId,
            $eventRecord->eventTemplateId
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $insertResponse);

        if ($insertResponse->success){
            // add action to event_log
			// eventInfo_id = $insertResponse->insertedId
            $logged = MySqliHelper::log_event_action($this->mysqli, $insertResponse->insertedId, 'Created', $memberID, $adminId);
			//Push up Event to Indexing Server
			if ($insertResponse->insertedId){
                if ($eventRecord->id){
                    // add new event id to draft
                    MySqliHelper::execute($this->mysqli, self::$ADD_EVENTID_TO_DRAFT_SQL, "ii", array($insertResponse->insertedId, $eventRecord->id));
                }

                if ($eventRecord->eventTemplateId > 0){
                    // copy the templates tags to the event
                    MySqliHelper::execute($this->mysqli, self::$ADD_TEMPLATE_TAGS_TO_EVENT_SQL, "iii", array($insertResponse->insertedId,$eventRecord->eventTemplateId,$insertResponse->insertedId));
                }
				
				$indexResult = $this->getAllByEventAndMerchant($insertResponse->insertedId, $eventRecord->venue->id);
				$x = 0;
				foreach ($indexResult as $uEvents=>$upcomingEvent){
                    /* @var $upcomingEvent EventRecord */
                    $documents[$x] = $upcomingEvent->convertToIndexedDocument();
					$x++;
				}
				$IndexingProvider = new Indexing_Api($GLOBALS['IndexingProviderURL']);
				$IndexingThisIndex = new Indexing_Index($IndexingProvider,$GLOBALS['IndexName']);
				$IndexAddDocuments = $IndexingThisIndex->add_documents($documents);
				$logged = MySqliHelper::log_event_action($this->mysqli, $insertResponse->insertedId,'Indexed docid='.$insertResponse->insertedId."_".$eventRecord->venue->id, $memberID);
			}
        }

        return $insertResponse;
    }

    private static $FIND_DUPLICATE_MERCHANT_EVENT_SQL = "
        SELECT
          Event_ID,
          Event_StartTime,
          Event_EndTime,
          Event_Title,
          Event_MerchantSecondaryPercentage
        FROM event_info
        WHERE Event_ProposedByMerchantID = ?
        AND Event_Status = 'ACTIVE'
        AND Event_Date = ?
        AND Event_EndDate = ?
        AND IFNULL(Event_MerchantSecondaryPercentage,0) > 0
        AND ((Event_StartTime BETWEEN ? AND ?) OR (Event_EndTime BETWEEN ? AND ?))
        AND Event_ID <> ?";

    private static $FIND_DUPLICATE_ORG_EVENT_SQL = "
        SELECT
          Event_ID,
          Event_StartTime,
          Event_EndTime,
          Event_Title
        FROM event_info
        WHERE Event_ProposedByOrgID = ?
        AND Event_Status = 'ACTIVE'
        AND Event_Date = ?
        AND Event_EndDate = ?
        AND ((Event_StartTime BETWEEN ? AND ?) OR (Event_EndTime BETWEEN ? AND ?))
        AND Event_ID <> ?";

    public function isDuplicate(EventRecord $eventRecord, $allowedToOverlap = true){
        // we only care about duplicates by proposer
        $isValid = false;
        $sqlString = self::$FIND_DUPLICATE_MERCHANT_EVENT_SQL;
        $proposerId = $eventRecord->venue->id;
        if (isset($eventRecord->venue->id)){
            $isValid = true;
        } else if (isset($eventRecord->cause->id)){
            $isValid = true;
            $sqlString = self::$FIND_DUPLICATE_ORG_EVENT_SQL;
            $proposerId = $eventRecord->cause->id;
        }
        if (!$isValid){
            return false;
        }

        $startDate = date("Y-m-d", $eventRecord->startDate);
        $endDate = date("Y-m-d", $eventRecord->endDate);
        $startTime = date("H:i", $eventRecord->startDate);
        $endTime = date("H:i", $eventRecord->endDate);
        $sqlBindings = "issssssi";
        $sqlParams = array(
            $proposerId,
            $startDate,
            $endDate,
            $startTime,
            $endTime,
            $startTime,
            $endTime,
            $eventRecord->id ? $eventRecord->id : 0
        );
        $candidateDupes = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($candidateDupes)){
            if (!$allowedToOverlap){
                return true;
            }
            // return true only if start and end time match
            $matchFound = false;
            foreach ($candidateDupes as $candidate){
                $candidateStartTime = date("H:i", strtotime($startDate." ".$candidate["Event_StartTime"]));
                $candidateEndTime = date("H:i", strtotime($endDate." ".$candidate["Event_EndTime"]));
                if ($candidateStartTime === $startTime && $candidateEndTime === $endTime){
                    return true;
                }
            }
        }
        return false;
    }

	public function addTagAssociationByTagName(EventRecord $eventRecord) {
        $associationProvider = new CampaignAssociationProvider($this->mysqli);
        $associationProvider->setEventTags($eventRecord->commaDelimitedTagNames, $eventRecord->id);
	}

	public function getTagAssociationByEventID($eventID, $campaignImageUrlPath) {
        $associationProvider = new CampaignAssociationProvider($this->mysqli);
        return $associationProvider->getEventTags($eventID, $campaignImageUrlPath);
	}
	
	public function getEventMatches($EventID){
		$sqlGetEvent = "
			SELECT 
				(
					SELECT count(*)
					FROM event_match
					WHERE EventMatch_EventID = Event_ID
					AND EventMatch_Status = 'Accepted'
				) AS hasHosts,
				(
					Event_Date >= curdate()
				) as isFutureEvent,
				event_info.*
			FROM event_info
			WHERE Event_ID = ?
			AND Event_Status = 'ACTIVE'";
		
		$resultEvent = MySqliHelper::get_result($this->mysqli, $sqlGetEvent, "i", Array((int)$EventID));
		return $resultEvent;
	
	}
	
	public function eventMemberWatch($EventID){
		$sqlWatch = "SELECT DISTINCT event_watch.MemberID, members.Members_Email, members.Members_Phone 
				FROM event_watch LEFT JOIN members ON Members_ID=MemberID WHERE EventID=?";
		$sqlParams = Array((int)$EventID);
		$sqlParamTypes = "i";
		$resultWatch = MySqliHelper::get_result($this->mysqli, $sqlWatch, $sqlParamTypes, $sqlParams);
		return $resultWatch;
	}
}
?>