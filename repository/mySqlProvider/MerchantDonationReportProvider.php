<?php
if (empty($rootFolder)){
    include_once("_getRootFolder.php");
}
include_once($rootFolder."repository/DataContracts.php");
class MerchantDonationReportCriteria implements ReportCriteria{
    /** @var int */
    public $merchantId;

    /** @var int */
    public $fromDateTime = null; // default
}

class MerchantDonationReportRunner implements CachedReportRunner {
    CONST REPORT_NAME = "Merchant Donations";
    protected $reportsConn;
    protected $disbursementConn;
    private $reportId;

    public function __construct(mysqli $reportsConn, mysqli $disbursementConn){
        $this->reportsConn = $reportsConn;
        $this->disbursementConn = $disbursementConn;
    }

    private static $GET_REPORT_DEFINITION_SQL = "
        SELECT id
        FROM report_definition
        WHERE name = ?";

    protected function getReportId(){
        if (!$this->reportId){
            $sqlString = self::$GET_REPORT_DEFINITION_SQL;
            $sqlBinding = "s";
            $sqlParam = array(self::REPORT_NAME);
            $queryResponse = new QueryResponseRecord();
            $result = MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParam, $queryResponse);

            if ($queryResponse->success){
                if (count($result)){
                    $this->reportId = $result[0]["id"];
                }
            } else {
                trigger_error("MerchantDonationReportProvider.getReportId: ".$queryResponse->error, E_USER_ERROR);
                return null;
            }
        }

        return $this->reportId;
    }

    private static $CHECK_IF_CACHE_EXPIRED_SQL = "
        SELECT 1
        FROM report_definition rd
        WHERE rd.id = ?
        AND NOT EXISTS (
            SELECT 1
            FROM report_merchant_donations_lastrun rdl
            WHERE rdl.report_definition_id = rd.id
            AND rdl.merchant_info_id = ?
            AND rdl.timestamp + INTERVAL rd.cache_life_minutes MINUTE > CURRENT_TIMESTAMP
        )";

    /**
     * Returns true if the cache can be refreshed.
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function canRun(ReportCriteria $criteria)
    {
        /* @var $criteria MerchantDonationReportCriteria */
        if (is_numeric($criteria->fromDateTime) && $criteria->merchantId){
            return true; // always allowable if datetime has been specified.
        }
        $reportId = $this->getReportId();
        $merchantId = $criteria->merchantId;
        if ($reportId && $merchantId){
            $sqlString = self::$CHECK_IF_CACHE_EXPIRED_SQL;
            $sqlBinding = "ii";
            $sqlParams = array($reportId, $merchantId);
            $queryResponse = new QueryResponseRecord();
            $result = MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
            if ($queryResponse->success){
                if (count($result)){
                    return true;
                }
            } else {
                trigger_error("MerchantDonationReportProvider.canRun (check expired): ".$queryResponse->error, E_USER_ERROR);
                return false;
            }
        }
        return false;
    }

    /**
     * Attempts to refresh the cache for a report with the given parameters. Returns true if successful.
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function run(ReportCriteria $criteria)
    {
        $success = false;
        /* @var $criteria MerchantDonationReportCriteria */
        if ($this->canRun($criteria)){
            $reportId = $this->getReportId();
            $merchantId = $criteria->merchantId;
            $lastRunTime = $this->getLastRuntime($reportId, $merchantId);

            if ($criteria->fromDateTime && is_numeric($criteria->fromDateTime) && $criteria->fromDateTime < $lastRunTime){
                $lastRunTime = $criteria->fromDateTime;
            }
            $success = $this->logRunTime($reportId, $merchantId);

            // from event_transaction
            $success = $success && $this->updatePendingTransactions($reportId, $merchantId);
            $success = $success && $this->insertPendingTransactions($reportId, $merchantId);

            // from disbursement
            $success = $success && $this->updateAllocationData($reportId, $merchantId, $lastRunTime);

            // update fully and partial allocations (from giving bank activity)
            $success = $success && $this->updatePendingAllocations($reportId, $merchantId);
        }
        return $success;
    }

    private static $GET_LAST_RUN_SQL = "
        SELECT timestamp
        FROM report_merchant_donations_lastrun
        WHERE report_definition_id = ?
        AND merchant_info_id = ?";

    private function getLastRunTime($reportId, $merchantId){
        $sqlString = self::$GET_LAST_RUN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $merchantId
        );
        $result = MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams);
        if (count($result)){
            return $result[0]["timestamp"];
        }
        return 0;
    }

    private static $LOG_RUN_TIME_SQL = "
        INSERT INTO report_merchant_donations_lastrun
        (report_definition_id, merchant_info_id)
        values (?,?)
        ON DUPLICATE KEY UPDATE timestamp = CURRENT_TIMESTAMP";

    private function logRunTime($reportId, $merchantId){
        $sqlString = self::$LOG_RUN_TIME_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $merchantId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("MerchantDonationReportProvider.logRunTime: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $UPDATE_PENDING_TRANSACTIONS = "
        UPDATE report_merchant_donations
        JOIN event_transaction e ON transaction_id = e.EventTransaction_ID
            AND customer_account_id = 0
        LEFT JOIN invoices_matrix i
            ON e.EventTransaction_InvoiceID = i.InvoicesMatrix_InvoiceID
            AND e.EventTransaction_ID = i.InvoicesMatrix_EventTransactionID
        SET organization_info_id = e.EventTransaction_OrgID,
            cache_status = if (e.EventTransaction_Status LIKE '%Deleted%', 'DELETED', 'ACTIVE'),
            cache_timestamp = CURRENT_TIMESTAMP()
        WHERE report_definition_id = ?
        AND merchant_info_id = ?
        AND (
            e.EventTransaction_InvoiceID = 0
            OR ( e.EventTransaction_InvoiceID <> 0
                AND i.InvoicesMatrix_IncomingQueueID IS NULL )
            )";

    /**
     * Purpose: find transactions that have not yet hit disbursement and update the org if the user picked one, and the status
     * in case it has been deleted. This does not operate on transactions that are in the cache, but have moved into disbursement.
     * @param $reportId
     * @param $merchantId
     * @return bool
     */
    private function updatePendingTransactions($reportId, $merchantId){
        $sqlString = self::$UPDATE_PENDING_TRANSACTIONS;
        $sqlBinding = "ii";
        $sqlParams = array(
            $reportId,
            $merchantId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("MerchantDonationReportProvider.updatePendingTransactions: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $INSERT_PENDING_TRANSACTIONS = "
        INSERT INTO report_merchant_donations
        (
            report_definition_id,
            merchant_info_id,
            organization_info_id,
            customer_member_id,

            transaction_id,
            transaction_date,
            transaction_year,
            transaction_month,
            transaction_week,
            transaction_sale,
            transaction_donation,
            is_flat_donation,
            flat_donation_units
        )
        SELECT
            ?,
            EventTransaction_MerchantID as merchant_info_id,
            EventTransaction_OrgID as organization_info_id,
            EventTransaction_CustomerID as customer_member_id,

            EventTransaction_ID as transaction_id,
            EventTransaction_EventDate as transaction_date,
            YEAR(EventTransaction_EventDate) as transaction_year,
            MONTH(EventTransaction_EventDate) as transaction_month,
            WEEK(EventTransaction_EventDate, 3) as transaction_week,
            EventTransaction_EligibleAmount as transaction_sale,
            EventTransaction_GeneratedAmount as transaction_donation,
            EventTransaction_IsFlatDonation as is_flat_donation,
            EventTransaction_FlatDonationUnits as flat_donation_units
        FROM event_transaction e
        LEFT JOIN invoices_matrix i
            ON e.EventTransaction_InvoiceID = i.InvoicesMatrix_InvoiceID
            AND e.EventTransaction_ID = i.InvoicesMatrix_EventTransactionID
        WHERE EventTransaction_MerchantID = ?
        AND (
            e.EventTransaction_InvoiceID = 0
            OR ( e.EventTransaction_InvoiceID <> 0
                AND i.InvoicesMatrix_IncomingQueueID is null )
            )
        AND e.EventTransaction_Status NOT LIKE '%Deleted%'
        AND NOT EXISTS (
            SELECT 1
            FROM report_merchant_donations
            WHERE report_definition_id = ?
            AND merchant_info_id = ?
            AND transaction_id = EventTransaction_ID
        )";

    /**
     * Inserts new transactions that have not been previously cached, and that have not yet entered disbursement.
     * @param $reportId
     * @param $merchantId
     * @return bool
     */
    private function insertPendingTransactions($reportId, $merchantId){
        $sqlString = self::$INSERT_PENDING_TRANSACTIONS;
        $sqlBinding = "iiii";
        $sqlParams = array(
            $reportId,
            $merchantId,
            $reportId,
            $merchantId
        );
        $executeResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $executeResponse);
        if ($executeResponse->success){
            return true;
        } else {
            trigger_error("MerchantDonationReportProvider.insertPendingTransactions: ".$executeResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $GET_UPDATED_ALLOCATION_DATA = "
        SELECT
            at.merchant_info_id,
            orgs.organization_info_id,
            a.member_id AS customer_member_id,
            at.customer_account_id,
            al.id customer_allocation_id,
            al.amount AS donation_amount,
            if (ap.id, ap.timestamp, al.event_transaction_timestamp) as allocation_timestamp,
            at.status AS allocation_status,

            at.event_transaction_id as transaction_id,
            trans.event_date as transaction_date,
            year(trans.event_date) as transaction_year,
            month(trans.event_date) as transaction_month,
            week(trans.event_date, 3) as transaction_week,
            trans.sale_amount AS transaction_sale,
            trans.is_flat_donation as is_flat_donation,
            trans.flat_donation_units as flat_donation_units,
            at.generated_amount as transaction_donation,
            CASE at.status
                WHEN 'NEW' THEN 0
                ELSE (
                        SELECT sum(amount)
                        FROM customer_allocations al2
                        WHERE al2.customer_account_id = at.customer_account_id
                        AND al2.event_transaction_id = at.event_transaction_id
                    )
            END as 'already_allocated'
        FROM customer_account_transactions at
        JOIN customer_account a ON at.customer_account_id = a.id
        JOIN incoming_data trans ON at.event_transaction_id = trans.event_transaction_id
        JOIN customer_allocations al ON at.event_transaction_id = al.event_transaction_id
        LEFT JOIN customer_allocation_plans ap ON al.customer_allocation_plan_id = ap.id
        JOIN disbursement_orgs orgs ON al.disbursement_org_id = orgs.id
        WHERE at.merchant_info_id = ?
        AND al.timestamp > ?";

    private static $INSERT_ALLOCATION_TRANSACTION_DATA = "
        INSERT INTO report_merchant_donations
        (
            report_definition_id,
            merchant_info_id,
            customer_member_id,
            customer_account_id,

            allocation_status,
            transaction_id,
            transaction_date,

            transaction_year,
            transaction_month,
            transaction_week,
            transaction_sale,
            transaction_donation,

            is_flat_donation,
            flat_donation_units
        )
        VALUES (
            ?,?,?,?,
            ?,?,?,
            ?,?,?,?,?,
            ?,?
        )
        ON DUPLICATE KEY UPDATE
            cache_timestamp = CURRENT_TIMESTAMP,
            customer_account_id = ?,
            allocation_status = ?";

    private static $INSERT_ALLOCATION_TRANSACTION_BINDING = "iiiisisiiiddiiis";

    public static $INSERT_ALLOCATION_DATA = "
        INSERT INTO report_donation_recipients
        (
            report_definition_id,
            organization_info_id,
            customer_allocation_id,
            transaction_id,
            donation_amount,
            allocation_timestamp
        )
        VALUES
        (
          ?,?,?,?,?,?
        )
        ON DUPLICATE KEY UPDATE
          cache_timestamp = CURRENT_TIMESTAMP";

    /**
     * Adds newly allocated amounts from disbursement. This includes whole transactions that were already allocated while
     * in event_transaction, as well as all partial allocations done via the Giving Bank.
     * @param $reportId
     * @param $merchantId
     * @param $lastRunTime
     * @return bool
     */
    private function updateAllocationData($reportId, $merchantId, $lastRunTime){
        $selectString = self::$GET_UPDATED_ALLOCATION_DATA;
        $selectBinding = "is";
        $selectParams = array(
            $merchantId,
            date("Y-m-d H:i:s", $lastRunTime)
        );
        $queryResponse = new QueryResponseRecord();
        $updatedDisbursementData = MySqliHelper::get_result($this->disbursementConn, $selectString, $selectBinding, $selectParams, $queryResponse);
        if ($queryResponse->success){
            $insertString1 = self::$INSERT_ALLOCATION_TRANSACTION_DATA;
            $insertBinding1 = self::$INSERT_ALLOCATION_TRANSACTION_BINDING;
            $insertString2 = self::$INSERT_ALLOCATION_DATA;
            $insertBinding2 = "iiiids";
            $numErrors = 0;
            foreach ($updatedDisbursementData as $disbursementData){
                $row = (object)$disbursementData;
                $insertParams1 = array(
                    $reportId,
                    $merchantId,
                    $row->customer_member_id,
                    $row->customer_account_id,

                    $row->allocation_status,
                    $row->transaction_id,
                    $row->transaction_date,

                    $row->transaction_year,
                    $row->transaction_month,
                    $row->transaction_week,
                    $row->transaction_sale,
                    $row->transaction_donation,

                    $row->is_flat_donation,
                    $row->flat_donation_units,

                    $row->customer_account_id,
                    $row->allocation_status
                );
                $insertParams2 = array(
                    $reportId,
                    $row->organization_info_id,
                    $row->customer_allocation_id,
                    $row->transaction_id,
                    $row->donation_amount,
                    $row->allocation_timestamp
                );
                $executeResponse1 = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString1, $insertBinding1, $insertParams1, $executeResponse1);
                if (!$executeResponse1->success){
                    trigger_error("MerchantDonationReportProvider.updateAllocationData (insert 1): ".$executeResponse1->error, E_USER_ERROR);
                    $numErrors++;
                }
                $executeResponse2 = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString2, $insertBinding2, $insertParams2, $executeResponse2);
                if (!$executeResponse2->success){
                    trigger_error("MerchantDonationReportProvider.updateAllocationData (insert 2): ".$executeResponse2->error, E_USER_ERROR);
                    $numErrors++;
                }
            }
            return $numErrors == 0;
        } else {
            trigger_error("MerchantDonationReportProvider.updateAllocationData (query): ".$queryResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $GET_PENDING_ALLOCATIONS_SQL = "
        SELECT
            at.merchant_info_id,
            0 as organization_info_id,
            a.member_id AS customer_member_id,
            at.customer_account_id,
            0 as customer_allocation_id,
            at.status AS allocation_status,

            at.event_transaction_id as transaction_id,
            trans.event_date as transaction_date,
            year(trans.event_date) as transaction_year,
            month(trans.event_date) as transaction_month,
            week(trans.event_date, 3) as transaction_week,
            trans.sale_amount AS transaction_sale,
            trans.is_flat_donation AS is_flat_donation,
            trans.flat_donation_units AS flat_donation_units,
            at.generated_amount as transaction_donation,
            CASE at.status
                WHEN 'NEW' THEN 0
                ELSE (
                        SELECT sum(amount)
                        FROM customer_allocations al2
                        WHERE al2.customer_account_id = at.customer_account_id
                        AND al2.event_transaction_id = at.event_transaction_id
                    )
            END as 'already_allocated'
        FROM customer_account_transactions at
        JOIN customer_account a ON at.customer_account_id = a.id
        JOIN incoming_data trans ON at.event_transaction_id = trans.event_transaction_id
        WHERE at.merchant_info_id = ?
        AND at.status <> 'ALLOCATED'";

    private static $INSERT_OR_UPDATE_PENDING_ALLOCATION_SQL = "
        INSERT INTO report_merchant_donations
        (
            report_definition_id,
            merchant_info_id,
            customer_member_id,
            customer_account_id,

            allocation_status,
            transaction_id,
            transaction_date,

            transaction_year,
            transaction_month,
            transaction_week,
            transaction_sale,
            transaction_donation,

            is_flat_donation,
            flat_donation_units
        )
        VALUES (
            ?,?,?,?,
            ?,?,?,
            ?,?,?,?,?,
            ?,?
        )
        ON DUPLICATE KEY UPDATE
            cache_timestamp = CURRENT_TIMESTAMP,
            cache_status = 'ACTIVE',
            customer_account_id = ?,
            allocation_status = ?";

    /**
     * This updates and/or adds pending disbursement data into the cache. This captures all the partial pieces left over
     * from Giving Bank activity. It does not cancel out previously added partial data that have since been "fully allocated."
     * @param $reportId
     * @param $merchantId
     * @return bool
     */
    private function updatePendingAllocations($reportId, $merchantId){
        $selectString = self::$GET_PENDING_ALLOCATIONS_SQL;
        $selectBinding = "i";
        $selectParam = array( $merchantId);
        $queryResponse = new QueryResponseRecord();
        $pendingAllocations = MySqliHelper::get_result($this->disbursementConn, $selectString, $selectBinding, $selectParam, $queryResponse);
        if ($queryResponse->success){
            $insertString = self::$INSERT_OR_UPDATE_PENDING_ALLOCATION_SQL;
            $insertBinding = "iiiisisiiiddiiis";
            $numErrors = 0;
            foreach ($pendingAllocations as $pendingAllocation){
                $row = (object)$pendingAllocation;
                $donationAmountRemaining = $row->transaction_donation - $row->already_allocated;
                $insertParams = array(
                    $reportId,
                    $merchantId,
                    $row->customer_member_id,
                    $row->customer_account_id,

                    $row->allocation_status,
                    $row->transaction_id,
                    $row->transaction_date,

                    $row->transaction_year,
                    $row->transaction_month,
                    $row->transaction_week,
                    $row->transaction_sale,
                    $row->transaction_donation,

                    $row->is_flat_donation,
                    $row->flat_donation_units,

                    $row->customer_account_id,
                    $row->allocation_status
                );
                $executeResponse = new ExecuteResponseRecord();
                MySqliHelper::execute($this->reportsConn, $insertString, $insertBinding, $insertParams, $executeResponse);
                if (!$executeResponse->success){
                    trigger_error("MerchantDonationReportProvider.updatePendingAllocations (insert): ".$executeResponse->error, E_USER_ERROR);
                    $numErrors++;
                }
            }
            return $numErrors == 0;
        } else {
            trigger_error("MerchantDonationReportProvider.updatePendingAllocations (query): ".$queryResponse->error, E_USER_ERROR);
            return false;
        }
    }

    private static $WEEKLY_BREAKDOWN_SQL = "
        SELECT transaction_year, transaction_week, breakdown, SUM(sales) AS sales, SUM(donations) AS donations FROM
        (
            SELECT
                transaction_year,
                transaction_week,
                STR_TO_DATE(CONCAT(YEARWEEK(transaction_date, 3), ' Monday'), '%x%v %W') AS 'breakdown',
                transaction_sale AS sales,
                transaction_donation AS donations
            FROM report_merchant_donations
            WHERE cache_status = 'ACTIVE'
            AND report_definition_id = ?
            AND merchant_info_id = ?
        ) AS transactions
        GROUP BY transaction_year, transaction_week WITH ROLLUP";

    protected function getWeeklyBreakdown($reportId, $merchantId){
        $collection = array();
        $sqlString = self::$WEEKLY_BREAKDOWN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array( $reportId, $merchantId);
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $summaryRow = new DonationsSummaryRow();
                $summaryRow->eligibleSales = $rowObject->sales;
                $summaryRow->donationsGenerated = $rowObject->donations;
                if ($rowObject->transaction_year && $rowObject->transaction_week){
                    // this is a basic row object for the week
                    $summaryRow->breakdownValue = date("n/j/Y", strtotime($rowObject->breakdown));
                } else if ($rowObject->transaction_year){
                    // this is a year aggregate
                    $summaryRow->breakdownValue = $rowObject->transaction_year." Total";
                    $summaryRow->isYearAggregate = true;
                } else {
                    // this is a full report aggregate
                    $summaryRow->breakdownValue = "Total";
                    $summaryRow->isGrandTotal = true;
                }
                $collection[] = $summaryRow;
            }
        } else {
            trigger_error("MerchantDonationReportProvider.getWeeklyBreakdown: ".$queryResponse->error, E_USER_ERROR);
        }
        return $collection;
    }

    private static $MONTHLY_BREAKDOWN_SQL = "
        SELECT
            transaction_year,
            transaction_month,
            MONTHNAME(transaction_date) AS 'date',
            SUM(transaction_sale) AS sales,
            SUM(transaction_donation) AS donations
        FROM report_merchant_donations
        WHERE cache_status = 'ACTIVE'
        AND report_definition_id = ?
        AND merchant_info_id = ?
        GROUP BY transaction_year, transaction_month WITH ROLLUP";

    protected function getMonthlyBreakdown($reportId, $merchantId){
        $collection = array();
        $sqlString = self::$MONTHLY_BREAKDOWN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array( $reportId, $merchantId);
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $summaryRow = new DonationsSummaryRow();
                $summaryRow->eligibleSales = $rowObject->sales;
                $summaryRow->donationsGenerated = $rowObject->donations;
                if ($rowObject->transaction_year && $rowObject->transaction_month){
                    // this is a basic row object for the week
                    $summaryRow->breakdownValue = $rowObject->date." ".$rowObject->transaction_year;
                } else if ($rowObject->transaction_year){
                    // this is a year aggregate
                    $summaryRow->breakdownValue = $rowObject->transaction_year." Total";
                    $summaryRow->isYearAggregate = true;
                } else {
                    // this is a full report aggregate
                    $summaryRow->breakdownValue = "Total";
                    $summaryRow->isGrandTotal = true;
                }
                $collection[] = $summaryRow;
            }
        } else {
            trigger_error("MerchantDonationReportProvider.getMonthlyBreakdown: ".$queryResponse->error, E_USER_ERROR);
        }
        return $collection;
    }

    private static $RECIPIENT_BREAKDOWN_SQL = "
        SELECT transaction_year, recipient, sum(donations) AS donations FROM
        (
            SELECT
                transaction_year,
                IF (r.organization_info_id, ro.OrganizationInfo_Name, if(t.organization_info_id, ot.OrganizationInfo_Name, 'z-Undesignated')) as 'recipient',
                IF (r.transaction_id, r.donation_amount, t.transaction_donation) as 'donations'
            FROM report_merchant_donations t
            LEFT JOIN report_donation_recipients r
                ON t.transaction_id = r.transaction_id
                AND t.report_definition_id = r.report_definition_id
            LEFT JOIN organization_info ot
                ON t.organization_info_id = ot.OrganizationInfo_ID
            LEFT JOIN organization_info ro
                ON r.organization_info_id = ro.OrganizationInfo_ID
            WHERE t.cache_status = 'ACTIVE'
            AND t.report_definition_id = ?
            AND t.merchant_info_id = ?

            UNION ALL

            SELECT
                transaction_year,
                'z-Undesignated' as 'recipient',
                transaction_donation - (
                    SELECT SUM(rdr.donation_amount)
                    FROM report_donation_recipients rdr
                    WHERE rdr.transaction_id = t.transaction_id
                    AND rdr.report_definition_id = t.report_definition_id
                  ) as 'donations'
            FROM report_merchant_donations t
            WHERE t.cache_status = 'ACTIVE'
            AND t.report_definition_id = ?
            AND t.merchant_info_id = ?
            AND t.allocation_status = 'ALLOCATING'
        ) as allocations
        GROUP BY transaction_year, recipient WITH ROLLUP";

    private static function getSqlRecipientBreakdownByDonation(){
        $prefix = "
        SELECT
          transaction_year,
          recipient,
          donations,
          if(transaction_year, transaction_year, 9999) as transaction_year_sorter,
          if(recipient IS NULL, 'zzzzz', '    ') as recipient_sorter
        FROM (";

        $suffix = ") as rollup
        ORDER BY transaction_year_sorter, recipient_sorter, donations DESC";

        return $prefix.self::$RECIPIENT_BREAKDOWN_SQL.$suffix;
    }

    protected function getRecipientBreakdown($reportId, $merchantId, $sortByDonation = false){
        $collection = array();
        $sqlString = $sortByDonation ? self::getSqlRecipientBreakdownByDonation() : self::$RECIPIENT_BREAKDOWN_SQL;
        $sqlBinding = "iiii";
        $sqlParams = array( $reportId, $merchantId, $reportId, $merchantId);
        $queryResponse = new QueryResponseRecord();
        MySqliHelper::get_result($this->reportsConn, $sqlString, $sqlBinding, $sqlParams, $queryResponse);
        if ($queryResponse->success){
            foreach ($queryResponse->resultArray as $sqlRow){
                $rowObject = (object)$sqlRow;
                $summaryRow = new DonationsSummaryRow();
                //$summaryRow->eligibleSales = $rowObject->sales;
                $summaryRow->donationsGenerated = $rowObject->donations;
                if ($rowObject->transaction_year && $rowObject->recipient){
                    // this is a basic row object for the week
                    $recipient = $rowObject->recipient;
                    if ($recipient == 'z-Undesignated'){
                        $recipient = "Currently Undesignated";
                    }
                    $summaryRow->breakdownValue = $recipient;
                } else if ($rowObject->transaction_year){
                    // this is a year aggregate
                    $summaryRow->breakdownValue = $rowObject->transaction_year." Total";
                    $summaryRow->isYearAggregate = true;
                } else {
                    // this is a full report aggregate
                    $summaryRow->breakdownValue = "Total";
                    $summaryRow->isGrandTotal = true;
                }
                $collection[] = $summaryRow;
            }
        } else {
            trigger_error("MerchantDonationReportProvider.getMonthlyBreakdown: ".$queryResponse->error, E_USER_ERROR);
        }
        return $collection;
    }

    /**
     * Returns data from the cache. This function does not ensure the cache is up to date. If it is desirable to update the cache,
     * the "run" function must be executed.
     * @param ReportCriteria $criteria
     * @return mixed
     */
    public function get(ReportCriteria $criteria)
    {
        /* @var $criteria MerchantDonationReportCriteria */
        $reportId = $this->getReportId();
        $merchantId = $criteria->merchantId;
        $weeklyReport = $this->getWeeklyBreakdown($reportId, $merchantId);
        $monthlyReport = $this->getMonthlyBreakdown($reportId, $merchantId);
        $recipientReport = $this->getRecipientBreakdown($reportId, $merchantId);

        $reportWrapper = new stdClass();
        $reportWrapper->weeklyReport = $weeklyReport;
        $reportWrapper->monthlyReport = $monthlyReport;
        $reportWrapper->recipientReport = $recipientReport;

        return $reportWrapper;
    }
}

class MerchantDonationReportProvider extends MerchantDonationReportRunner{

}

class MerchantDonationReportStatus{
    public $merchantId;
    public $name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $minutes_since_last_run;

    public function getBusinessDisplay($newline = "\n"){
        $outputString = $this->name;
        if (strlen($this->address)){
            $outputString .= $newline.$this->address;
        }
        if ($this->city || $this->state || $this->zip){
            $outputString .= $newline.$this->city.", ".$this->state." ".$this->zip;
        }
        return trim($outputString);
    }

    public function getLastRunDisplay($default = "Never"){
        $minutes = $this->minutes_since_last_run;
        if ($minutes === null){
            return $default;
        }
        if ($minutes < 60){
            return $minutes." minutes";
        }
        if ($minutes < 60*24){
            return floor($minutes/60)." hours";
        }
        return floor($minutes/(60*24))." days";
    }
}

class AdminMerchantDonationReportProvider extends MerchantDonationReportProvider{

    private static $GET_MERCHANTS_SELECT_COLUMNS = "
        SELECT
            m.MerchantInfo_ID as merchant_info_id,
            m.MerchantInfo_Name as name,
            m.MerchantInfo_Address as address,
            m.MerchantInfo_City as city,
            m.MerchantInfo_State as state,
            m.MerchantInfo_Zip as zip,
            floor(time_to_sec(timediff(CURRENT_TIMESTAMP, r.timestamp))/60) as minutes_since_last_run";

    private static $FROM_TABLE_CLAUSE = "
        FROM merchant_info m
        LEFT JOIN report_merchant_donations_lastrun r
            ON m.MerchantInfo_ID = r.merchant_info_id
            AND r.report_definition_id = 1
        WHERE EXISTS (
            SELECT 1
            FROM event_transaction
            WHERE EventTransaction_MerchantID = m.MerchantInfo_ID
        )";

    private static $FILTER_ON_KEYWORD = "
        AND (
            m.MerchantInfo_Name LIKE ?
            OR m.MerchantInfo_Address LIKE ?
            OR m.MerchantInfo_City LIKE ?
            OR m.MerchantInfo_State LIKE ?
            OR m.MerchantInfo_Zip LIKE ?
            OR m.MerchantInfo_Phone LIKE ?
            OR m.MerchantInfo_Website LIKE ?
            OR m.MerchantInfo_ContactFirstName LIKE ?
            OR m.MerchantInfo_ContactLastName LIKE ?
            OR m.MerchantInfo_ContactEmail LIKE ?
        )";

    public function getMerchants(PaginationCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$GET_MERCHANTS_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->reportsConn, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'];
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->reportsConn, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        foreach ($sqlResult as $row){
            $rowObject = (object)$row;
            $record = new MerchantDonationReportStatus();
            $record->merchantId = $rowObject->merchant_info_id;
            $record->name = $rowObject->name;
            $record->address = $rowObject->address;
            $record->city = $rowObject->city;
            $record->state = $rowObject->state;
            $record->zip = $rowObject->zip;
            $record->minutes_since_last_run = $rowObject->minutes_since_last_run;
            $result->collection[] = $record;
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY r.timestamp desc";

    private static $ORDER_BY_ID = " ORDER BY m.MerchantInfo_ID";

    private static $ORDER_BY_BUSINESS = " ORDER BY m.MerchantInfo_Name";

    private static $ORDER_BY_LASTRUN = " ORDER BY r.timestamp";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "idLink":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "businessDisplay":
                    $orderBy = self::$ORDER_BY_BUSINESS." ".$sortOrder;
                    break;
                case "lastRun":
                    $orderBy = self::$ORDER_BY_LASTRUN." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}