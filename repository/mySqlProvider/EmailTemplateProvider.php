<?php

use Cassandra\Value;

class EmailTemplateProvider {

    protected static $REFERRAL_DELETED = "DELETED";
    protected static $REFERRAL_ACTIVE = "ACTIVE";

    /* @var mysqli */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $EMAIL_TYPES_SQL = "
        SELECT id, name
        FROM cms_email_type
        WHERE '' = ?
        OR name = ?";

    /**
     * Retrieves the list of email types for a drop down.
     * @param string $typeName
     * @return array
     */
    public function getTypes($typeName = ''){
        $collection = array();
        $result = MySqliHelper::get_result($this->mysqli, self::$EMAIL_TYPES_SQL, "ss", array($typeName, $typeName));
        foreach($result as $record){
            $emailType = (object)$record;
            $collection[] = $emailType;
        }
        return $collection;
    }

    //
    // Basic get pattern
    //
    private static $SELECT_COLUMNS = "
        SELECT
            templates.id,
            templates.email_type_id,
            cms_email_type.name as email_type,
            templates.is_default,
            templates.name,
            templates.email_subject,
            templates.markdown_source,
            templates.html_source,
            templates.field_array_json";

    private static $FROM_TABLE_CLAUSE = "
        FROM cms_email_templates templates
        JOIN cms_email_type ON templates.email_type_id = cms_email_type.id
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND templates.id = ?";

    private static $FILTER_ON_ACTIVE = "
        AND templates.status = 'ACTIVE'";

    private static $FILTER_ON_EMAIL_TYPE = "
        AND templates.email_type_id = ?";

    private static $FILTER_ON_REFERRAL_CODE = "
        AND (EXISTS (
                SELECT 1
                FROM cms_email_referral_codes
                WHERE cms_email_template_id = templates.id
                AND codes_campaign_id = ?
                AND status = 'ACTIVE')
            OR templates.is_default = 1
            )";

    private static $FILTER_ON_IS_DEFAULT = "
        AND templates.is_default = 1";

    /**
     * @param GetTemplateRequest $criteria
     * @return PagedResult
     */
    public function get(GetTemplateRequest $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        if ($criteria->getAll){
            $result->maxLength = 0; // get them all
        }
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->id){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else {
            $filterString .= self::$FILTER_ON_ACTIVE;

            if ($criteria->emailTypeId){
                $filterString .= self::$FILTER_ON_EMAIL_TYPE;
                $filterBinding .= "i";
                $filterParams[] = $criteria->emailTypeId;
            }

            if ($criteria->getReferralCode()){
                $filterString .= self::$FILTER_ON_REFERRAL_CODE;
                $filterBinding .= "i";
                $filterParams[] = $criteria->getReferralCode();
            } else if ($criteria->useDefault()){
                $filterString .= self::$FILTER_ON_IS_DEFAULT;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = MySqliHelper::getResultWithLogging("EmailTemplateProvider.get error:", $this->mysqli, $countSqlString, $filterBinding, $filterParams);
        if ($queryResponse->success){
            $result->totalRecords = count($queryResponse->resultArray);
        }

        // get the page
        $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
        $queryResponse = MySqliHelper::getResultWithLogging("EmailTemplateProvider.get error:", $this->mysqli, $pageSqlString, $filterBinding, $filterParams);
        foreach ($queryResponse->resultArray as $row){
            $result->collection[] = $this->mapToRecord($row);
        }

        return $result;
    }

    private static $DEFAULT_ORDER_BY = " ORDER BY templates.is_default, templates.name";

    private function getOrderByClause(GetTemplateRequest $criteria){
        return self::$DEFAULT_ORDER_BY;
    }

    private static $GET_EMAIL_REFERRAL_CODE_SQL = "
        SELECT
            codes.id,
            codes.codes_campaign_id,
            codes.status,
            domain.CodesCampaign_Code as code
        FROM cms_email_referral_codes codes
        JOIN codes_campaign domain ON codes.codes_campaign_id = domain.CodesCampaign_ID
        WHERE codes.cms_email_template_id = ?
        AND (
            1 = ?
          OR
            codes.status = ?
          )";

    private function mapToRecord($dbRowArray){
        $dbRowObject = (object)$dbRowArray;
        $template = new EmailTemplate();
        $template->id = $dbRowObject->id;
        $template->emailTypeId = $dbRowObject->email_type_id;
        $template->emailType = $dbRowObject->email_type;
        $template->name = $dbRowObject->name;
        $template->emailSubject = $dbRowObject->email_subject;
        $template->markdownSource = $dbRowObject->markdown_source;
        $template->htmlSource = $dbRowObject->html_source;
        $decodedFieldArray = json_decode($dbRowObject->field_array_json);
        foreach($decodedFieldArray as $field){
            $varName = $field->variableName;
            $sampleValue = $field->sampleValue;
            $fieldObj = new TemplateField($varName, $sampleValue);
            $template->addField($fieldObj);
        }
        // add referral codes
        if ($dbRowObject->is_default == 0){
            $result = $this->getReferralCodesFor($template->id);
            foreach ($result as $referralCodeArray){
                $referralCodeObject = (object)$referralCodeArray;
                $template->referralCodesArray[] = $referralCodeObject->code;
            }
        } else {
            $template->isDefault = true;
        }
        return $template;
    }

    protected function getReferralCodesFor($templateId, $activeOnly = true){
        $matchAll = $activeOnly ? 0 : 1;
        $sqlString = self::$GET_EMAIL_REFERRAL_CODE_SQL;
        $sqlBindings = "iis";
        $sqlParams = array(
            $templateId,
            $matchAll,
            self::$REFERRAL_ACTIVE
        );
        $queryResponse = MySqliHelper::getResultWithLogging("EmailTemplateProvider.getReferralCodesFor error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $queryResponse->resultArray;
    }
}
//
// Registration email template
//
abstract class RegistrationEmailTemplateProvider extends EmailTemplateProvider {
    private static $DEFAULT_EMAIL_TYPE = "Registration";

    public function getRegistrationEmailBody($memberRecord, $emailTemplateContext){
        $templateRequest = new GetTemplateRequest();
        $regTypes = $this->getTypes($this->getEmailType());
        if (!count($regTypes)){
            $regTypes = $this->getTypes(self::$DEFAULT_EMAIL_TYPE);
        }
        $templateRequest->emailTypeId = $regTypes[0]->id;
        $templateRequest->memberRecord = $memberRecord;
        $emailTemplateResponse = $this->get($templateRequest);
        $emailTemplate = $emailTemplateResponse->collection[0];
        /* @var $emailTemplate EmailTemplate */
        return $emailTemplate->merge($emailTemplateContext);
    }

    abstract protected function getEmailType();
}

class IndividualRegistrationEmailTemplateProvider extends RegistrationEmailTemplateProvider {
    private static $EMAIL_TYPE = "IndividualRegistration";

    protected function getEmailType(){
        return self::$EMAIL_TYPE;
    }
}
class MerchantRegistrationEmailTemplateProvider extends RegistrationEmailTemplateProvider {
    private static $EMAIL_TYPE = "MerchantRegistration";

    protected function getEmailType(){
        return self::$EMAIL_TYPE;
    }
}

class OrganizationRegistrationEmailTemplateProvider extends RegistrationEmailTemplateProvider {
    private static $EMAIL_TYPE = "OrganizationRegistration";

    protected function getEmailType(){
        return self::$EMAIL_TYPE;
    }
}

//
// Transaction Receipts
//
class TransactionReceiptEmailTemplateProvider extends EmailTemplateProvider{

    public static $RECEIPT_TMPL_CC = "TransactionReceiptCC";
    public static $RECEIPT_TMPL_FC = "TransactionReceiptFC";
    public static $RECEIPT_TMPL_OTF_CC = "TransactionReceiptNoEvent";
    public static $RECEIPT_TMPL_OTF_FC = "TransactionReceiptFCNoEvent";

    public static function getTemplateType($cause, $eventId){
        return
            ($cause == null)
                ? (($eventId > 0) ? self::$RECEIPT_TMPL_CC : self::$RECEIPT_TMPL_OTF_CC)
                : (($eventId > 0) ? self::$RECEIPT_TMPL_FC : self::$RECEIPT_TMPL_OTF_FC);
    }

    public function getTransactionEmailBody($memberRecord, $emailTemplateContext, $venue, $cause, $eventId){
        $templateType = self::getTemplateType($cause, $eventId);
        if (!isset($emailTemplateContext->venue)){
            $emailTemplateContext->venue = $venue;
        }
        if (!isset($emailTemplateContext->cause)){
            $emailTemplateContext->cause = $cause;
        }
        $templateRequest = new GetTemplateRequest();
        $regTypes = $this->getTypes($templateType);
        $templateRequest->emailTypeId = $regTypes[0]->id;
        $templateRequest->memberRecord = $memberRecord;
        $emailTemplateResponse = $this->get($templateRequest);
        $emailTemplate = $emailTemplateResponse->collection[0];
        /* @var $emailTemplate EmailTemplate */

        return $emailTemplate->merge($emailTemplateContext);
    }
}

//
// EventPublishedEmails
//
class MerchantEventPublishedEmailTemplateProvider extends EmailTemplateProvider{

    public static $MerchantEventPublished = "MerchantEventPublished";

    /* @var EmailTemplate */
    private $cachedEmailTemplate;

    private function getTemplate(EventTemplate $eventTemplate = null){
        if (!$this->cachedEmailTemplate){
            $types = $this->getTypes(self::$MerchantEventPublished);
            $criteria = new GetTemplateRequest();
            $criteria->emailTypeId = $types[0]->id;
            $criteria->eventTemplate = $eventTemplate;
            $emailTemplateResponse = $this->get($criteria);
            if (count($emailTemplateResponse->collection)){
                $this->cachedEmailTemplate = $emailTemplateResponse->collection[0];
            } else {
                trigger_error("MerchantEventPublishedEmailTemplateProvider.getTemplate: no email template available", E_USER_ERROR);
            }
        }
        return $this->cachedEmailTemplate;
    }

    public function getEmailBody($emailTemplateContext, EventTemplate $eventTemplate = null){
        $template = $this->getTemplate($eventTemplate);
        if ($template){
            return $template->merge($emailTemplateContext);
        }
        return null;
    }

    public function getEmailSubject($emailTemplateContext, $defaultSubject, EventTemplate $eventTemplate = null){
        $template = $this->getTemplate($eventTemplate);
        if ($template){
            return $template->getEmailSubject($emailTemplateContext, $defaultSubject);
        }
        return null;
    }
}

//
// CRUD operations
//
class AdminEmailTemplateProvider extends EmailTemplateProvider{

    private static $FIND_MATCHING_DEFAULT_TEMPLATE_SQL = "
        SELECT id
        FROM cms_email_templates
        WHERE is_default = 1
        AND email_type_id = ?
        AND status = 'ACTIVE'";

    public function save(EmailTemplate $template, $username){
        if (!(count($template->referralCodesArray))&& !($template->id)){
            // any template that has no referral codes must be the default template; if it does not have an ID, get it now
            $sqlString = self::$FIND_MATCHING_DEFAULT_TEMPLATE_SQL;
            $sqlBindings = "i";
            $sqlParams = array($template->emailTypeId);
            $response = MySqliHelper::getResultWithLogging("AdminEmailTemplateProvider.save sql error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
            if (count($response->resultArray)){
                $template->id = $response->resultArray[0]["id"];
                $response = new SaveTemplateResponse();
                $response->record = $template;
                $response->message = "There is already an existing default template (with no referral codes). Please add at least one referral code.";
                return $response;
            }
        }
        if ($template->id){
            return $this->updateTemplate($template, $username);
        }
        return $this->addTemplate($template, $username);
    }

    private static $INSERT_EMAIL_TEMPLATE_SQL = "
        INSERT INTO cms_email_templates
        (
            email_type_id,
            is_default,
            name,
            email_subject,

            markdown_source,
            html_source,
            field_array_json,
            created_by
        )
        SELECT
            ?,?,?,?,
            ?,?,?,?
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT 1
            FROM cms_email_templates
            WHERE email_type_id = ?
            AND name = ?
            AND status = 'ACTIVE'
        )
        ";
    private static $INSERT_EMAIL_TEMPLATE_BINDINGS = "iissssssis";

    private static $INSERT_EMAIL_REFERRAL_CODE_SQL = "
        INSERT INTO cms_email_referral_codes
        (
            cms_email_template_id,
            codes_campaign_id,
            created_by
        )
        SELECT ?,?,?
        FROM dual
        WHERE NOT EXISTS (
            SELECT 1
            FROM cms_email_referral_codes
            WHERE cms_email_template_id = ?
            AND codes_campaign_id = ?
        )";

    private function addTemplate(EmailTemplate $template, $username){
        $response = new SaveTemplateResponse();
        $response->record = $template;
        $isDefault = (count($template->referralCodesArray) == 0) ? 1 : 0;
        $sqlString = self::$INSERT_EMAIL_TEMPLATE_SQL;
        $sqlParams = array(
            $template->emailTypeId,
            $isDefault,
            $template->name,
            $template->emailSubject,

            $template->markdownSource,
            $template->htmlSource,
            json_encode($template->getFields()),
            $username,

            $template->emailTypeId,
            $template->name
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("AdminEmailTemplateProvider.addTemplate error:", $this->mysqli, $sqlString, self::$INSERT_EMAIL_TEMPLATE_BINDINGS, $sqlParams, $insertResponse);
        $response->record->id = $insertResponse->insertedId;
        $response->success = $insertResponse->success;
        if (!$insertResponse->success || $insertResponse->affectedRows == 0){
            $response->success = false;
            $response->message = $insertResponse->error;
            if ($insertResponse->affectedRows == 0){
                $response->message = "Unique Constraint Violation: Name must be unique for each Email Type";
            }
            return $response;
        }
        // by this time, we've inserted the main template
        if (!$isDefault){
            $templateId = $response->record->id;
            $this->insertReferralCodes($templateId, $template->referralCodesArray, $username);
        }
        $template->init();
        return $response;
    }

    private function insertReferralCodes($templateId, $referralCodesArray, $username){
        $referralId = 0;
        // now add the referral codes
        $stmt = $this->mysqli->prepare(self::$INSERT_EMAIL_REFERRAL_CODE_SQL);
        $stmt->bind_param("iisii", $templateId, $referralId, $username, $templateId, $referralId);
        foreach($referralCodesArray as $code){
            $referralCode = ReferralCodeHelper::getReferralCodeByName($this->mysqli, $code);
            if ($referralCode != null){
                $referralId = $referralCode->id;
                $stmt->execute();
            }
        }
    }

    private static $UPDATE_EMAIL_TEMPLATE_SQL = "
        UPDATE cms_email_templates
        SET
          name = ?,
          email_subject = ?,
          markdown_source = ?,
          html_source = ?,
          field_array_json = ?,
          updated_date = CURRENT_TIMESTAMP(),
          updated_by = ?
        WHERE id = ?";

    private static $UPDATE_EMAIL_TEMPLATE_BINDINGS = "ssssssi";

    private static $UPDATE_EMAIL_REFERRAL_CODE_SQL = "
        UPDATE cms_email_referral_codes
        SET status = ?,
            updated_date = CURRENT_TIMESTAMP(),
            updated_by = ?
        WHERE id = ?";

    private function setCodeStatus($emailReferralCodeId, $status, $username){
        $sqlString = self::$UPDATE_EMAIL_REFERRAL_CODE_SQL;
        $sqlBindings = "ssi";
        $sqlParams = array(
            $status,
            $username,
            $emailReferralCodeId
        );
        MySqliHelper::executeWithLogging("AdminEmailTemplateProvider.setCodeStatus error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
    }

    private function enableCode($emailReferralCodeId, $username){
        $this->setCodeStatus($emailReferralCodeId, self::$REFERRAL_ACTIVE, $username);
    }

    private function disableCode($emailReferralCodeId, $username){
        $this->setCodeStatus($emailReferralCodeId, self::$REFERRAL_DELETED, $username);
    }

    private function updateTemplate(EmailTemplate $template, $username){
        $response = new SaveTemplateResponse();
        $response->record = $template;
        $sqlString = self::$UPDATE_EMAIL_TEMPLATE_SQL;
        $sqlBindings = self::$UPDATE_EMAIL_TEMPLATE_BINDINGS;
        $sqlParams = array(
            $template->name,
            $template->emailSubject,
            $template->markdownSource,
            $template->htmlSource,
            json_encode($template->getFields()),
            $username,
            $template->id
        );
        $updateResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("AdminEmailTemplateProvider.updateTemplate error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
        $response->success = $updateResponse->success;
        if ($updateResponse->affectedRows == 0){
            $response->message = "Unique Constraint Violation: Name must be unique for each Email Type";
        } else if (count($template->referralCodesArray)){
            $copyOfCodes = $template->referralCodesArray;
            $currentCodes = $this->getReferralCodesFor($template->id, false);
            foreach($currentCodes as $referralCodeRecordArray){
                $referralCodeRecord = (object)$referralCodeRecordArray;
                $codeValue = $referralCodeRecord->code;
                $currentStatus = $referralCodeRecord->status;
                $emailReferralCodeId = $referralCodeRecord->id;
                $indexInCopy = array_search($codeValue, $copyOfCodes);
                if (is_numeric($indexInCopy)){
                    // we want to keep this, AND it's already in the db
                    if ($currentStatus !== self::$REFERRAL_ACTIVE){
                        $this->enableCode($emailReferralCodeId, $username);
                    }
                    // now remove it from the array so we don't run another insert on it
                    unset($copyOfCodes[$indexInCopy]);
                } else {
                    // we do not want to keep this, AND it's already in the db
                    if ($currentStatus === self::$REFERRAL_ACTIVE){
                        $this->disableCode($emailReferralCodeId, $username);
                    }
                }
            }
            // by this time, we've handled the codes in the system, we just need to insert the remaining codes
            $this->insertReferralCodes($template->id, $copyOfCodes, $username);
        }
        $template->init();
        return $response;
    }

    private static $DELETE_EMAIL_TEMPLATE_SQL = "
        UPDATE cms_email_templates
        SET status = 'DELETED'
        WHERE id = ?";

    public function delete($templateID){
        $deleteResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::executeWithLogging("AdminEmailTemplateProvider.delete error:", $this->mysqli, self::$DELETE_EMAIL_TEMPLATE_SQL, "i", array($templateID), $deleteResponse);
        return $deleteResponse;
    }
}
class GetTemplateRequest extends PaginationCriteria{
    public $emailTypeId;
    public $memberRecord; // if the member type matters
    public $getAll = false;
    public $eventTemplate;

    public function getReferralCode(){
        if ($this->memberRecord && isset($this->memberRecord->registeredreferralid) && $this->memberRecord->registeredreferralid){
            return $this->memberRecord->registeredreferralid;
        }
        return null;
    }

    public function useDefault(){
        return $this->memberRecord ? true : false;
    }
}
class SaveTemplateResponse{
    public $record;
    public $message;
    public $success = false;
}
class EmailTemplate{
    public $id;
    public $name; // e.g., BCLC Registration Welcome Email
    public $emailTypeId;
    public $emailType;
    public $emailSubject;
    public $markdownSource;
    public $htmlSource;
    private $fieldArray = array(); // array of TemplateField which is helper content for the UI
    public $isDefault;
    public $isActive;

    /**
     * referralCodesArray is an array of string
     */
    public $referralCodesArray = array(); // e.g., BCLC, Jason

    /**
     * Given a standard object (expected to be rehydrated from json), convert to strongly typed EmailTemplate
     * @param null $stdObject
     */
    public function __construct($stdObject = null){
        if ($stdObject != null){
            $this->id = $stdObject->id;
            $this->name = $stdObject->name;
            $this->emailSubject = $stdObject->emailSubject;
            $this->emailTypeId = $stdObject->emailTypeId;
            $this->markdownSource = $stdObject->markdownSource;
            $this->htmlSource = $stdObject->htmlSource;
            $this->isActive = $stdObject->isActive;
            if (isset($stdObject->referralCodesArray) && is_array($stdObject->referralCodesArray)){
                foreach($stdObject->referralCodesArray as $code){
                    $this->referralCodesArray[] = $code;
                }
            }
            if (isset($stdObject->fieldArray) && is_array($stdObject->fieldArray)){
                foreach($stdObject->fieldArray as $fieldObject){
                    $templateField = new TemplateField($fieldObject->variableName, $fieldObject->sampleValue);
                    $this->addField($templateField);
                }
            }
        }
    }

    /**
     * Initialize for edit by grabbing all fieldnames in the htmlsource and adding to field array for the user's convenience
     */
    public function init(){
        preg_match_all("/{([^}]*)}/", $this->htmlSource, $matches);
        if (count($matches) > 1){
            $variableNamesArray = $matches[1];
            foreach ($variableNamesArray as $varName){
                if (!$this->hasField($varName)){
                    $field = new TemplateField($varName);
                    $this->addField($field);
                }
            }
        }

        preg_match_all("/{([^}]*)}/", $this->emailSubject, $matches);
        if (count($matches) > 1){
            $variableNamesArray = $matches[1];
            foreach ($variableNamesArray as $varName){
                if (!$this->hasField($varName)){
                    $field = new TemplateField($varName);
                    $this->addField($field);
                }
            }
        }
    }

    public function addField(TemplateField $field){
        $this->fieldArray[$field->variableName] = $field;
    }

    public function hasField($varName){
        return isset($this->fieldArray[$varName]);
    }

    public function getFields(){
        $collection = array();
        foreach($this->fieldArray as $varName => $field){
            $collection[] = $field;
        }
        return $collection;
    }

    /**
     * Using the sample fields, builds the sample context, presumably for a test email
     */
    public function getSampleContext(){
        $fields = $this->getFields();
        $context = array();
        foreach($fields as $field){
            $context[$field->variableName] = $field->getSampleValue();
        }
        return (object)$context;
    }

    /**
     * Given an object, attempts to get values and update them on the template
     * @param $object the object whose properties are used to merge into the template
     * @param null $templateText optional template that has already had one object merged on it
     * @return mixed|null
     */
    public function merge($object, $templateText = null){
        $this->init();
        $text = $templateText;
        if ($templateText == null){
            $text = $this->htmlSource;
        }
        $patternArray = array();
        $replacementArray = array();
        foreach($this->fieldArray as $templateField){
            $variableName = $templateField->variableName;
            $extractedValue = $this->extractValue($object, $variableName);
            if (isset($extractedValue)){
                $patternArray[] = $this->getReplacementPattern($text, $variableName, $extractedValue);
                $replacementArray[] = preg_quote($extractedValue);
            }
        }
        $result = $text; // default
        if (strlen($text) && count($patternArray)){
            $result = preg_replace($patternArray, $replacementArray, $text);
            // now eliminate the backslashes
            $result = preg_replace('/\\\\/', '', $result);
        }
        return $result;
    }

    public function getEmailSubject($object, $defaultText){
        $emailSubject = $this->emailSubject ? $this->emailSubject : $defaultText;
        return $this->merge($object, $emailSubject);
    }

    private function getReplacementPattern($templateText, $variableName, $extractedValue){
        $pattern = "{".$variableName."}";
        if (startsWith($extractedValue, "http://") or startsWith($extractedValue, "https://")){
            // first test for default pattern
            $defaultHttpPattern = "http://{".$variableName."}";
            $sslPattern = "https://{".$variableName."}";
            if (strpos($templateText, $defaultHttpPattern) !== false){
                // found
                $pattern = str_replace("/", "\/", $defaultHttpPattern);
            } else if (strpos($templateText, $sslPattern) !== false) {
                $pattern = str_replace("/", "\/", $sslPattern);
            }
        }
        return "/".$pattern."/";
    }

    /**
     * Given an object that has properties, and a variable name in dot notation (like venue.id), gets the value on the object.
     * For example, if the object is an $eventRecord, and variable name is venue.id, this retrieves $eventRecord->venue->id.
     * @param $object
     * @param $variableName
     * @return value|null
     */
    private function extractValue($object, $variableName){
        if (isset($object->$variableName)){
            return $object->$variableName;
        }
        $propertyPath = explode(".", $variableName);
        $value = $object;
        foreach($propertyPath as $path){
            if ($value != null && property_exists($value, $path)){
                $value = $value->$path;
                if ($value == null){
                    $value = ''; // don't allow null values to be null
                }
            } else {
                $value = $value->$path;
            }
        }
        return $value;
    }
}

class TemplateField{
    public $variableName; // uses object dot notation. $eventRecord->title = "title"; $eventRecord->venue->id = "venue.id"
    public $sampleValue; // public for serialization
    // TODO: add additional properties for optional formatting

    public function __construct($varName, $value = null){
        $this->variableName = $varName;
        if ($value != null){
            $this->setSampleValue($value);
        } else {
            $this->setSampleValue("");
        }
    }

    public function setSampleValue($sampleValue){
        $this->sampleValue = $sampleValue;
    }

    public function getSampleValue(){
        return $this->sampleValue;
    }
}