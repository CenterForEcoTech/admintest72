<?php
require_once("Helper.php");
class WebhooksProvider
{
			
	private $mysqli;

	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

    public static $SQL_PAYLOAD = "
		INSERT INTO webhooks_payload 
			(
				WebhooksPayload_Source,
				WebhooksPayload_MerchantID,
				WebhooksPayload_Payload,
				WebhooksPayload_Status
			)
			VALUES
			(
				?,?,?,'Pending'
			)
		";

	public function vend_storePayload($source, $merchantID, $payload){
		$sqlString = self::$SQL_PAYLOAD;
		$sqlBindType = "sss";
		$sqlParams = Array($source,$merchantID,$payload);
		$payloadStored = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindType, $sqlParams);
		return $payloadStored;
	}

    public static $SQL_DATA = "
		INSERT INTO webhooks_data 
			(
				WebhooksData_PayloadID,
				WebhooksData_Source,
				WebhooksData_MerchantID,
				
				WebhooksData_Customer,
				WebhooksData_Amount,
				WebhooksData_Status
			)
			VALUES
			(
				?,?,?,
				?,?,?
			)
		";
	public function vend_storeData($payloadID, $source, $merchantID, $data){
		$customer = $data["customer"];
		$amount = $data["amount"];
		$status = ($data["status"]) ? $data["status"] : 'Pending';
		$sqlString = self::$SQL_DATA;
		$sqlBindType = "isisds";
		$sqlParams = Array($payloadID,$source,$merchantID,$customer,$amount,$status);
		$dataStored = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindType, $sqlParams);
		return $dataStored;
	}
}
?>