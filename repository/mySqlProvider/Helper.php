<?php
include_once("_getRootFolder.php");
include_once($rootFolder."repository/ManagerAPI.php");

class MySqliHelper {

    public static function getResultWithLogging($errorPrefix, $mysqli, $sql, $types = null, $params = null, QueryResponseRecord $responseRecord = null){
        if ($responseRecord == null){
            $responseRecord = new QueryResponseRecord();
        }
        if (!$errorPrefix){
            $errorPrefix = "Unspecified caller: SQL Error:";
        }
        self::get_result($mysqli, $sql, $types, $params, $responseRecord);
        if (!$responseRecord->success){
            trigger_error($errorPrefix." ".$responseRecord->error, E_USER_ERROR);
        }
        return $responseRecord;
    }

    /**
     * Returns a fully loaded result set into an array of arrays. Don't do this with large datasets!
     * @param mysqli $mysqli
     * @param string $sql
     * @param string $types (bind types, like 's' for string, 'i' for int)
     * @param array $params (bind values)
     * @param QueryResponseRecord $responseRecord
     * @return array
     */
	public static function get_result($mysqli, $sql, $types = null, $params = null, QueryResponseRecord $responseRecord = null)
	{
        if ($responseRecord == null){
            $responseRecord = new QueryResponseRecord();
        }

        try {
            # create a prepared statement
            $stmt = $mysqli->prepare($sql);
            if ($stmt === false){
                // something went wrong!
                $error = "Helper.php: Error preparing statement [".$sql."]".$mysqli->error;
                trigger_error($error, E_USER_ERROR);
                $responseRecord->error = $error;
                return array();
            }

            if($types && $params)
            {
                $bind_names[] = $types;
                for ($i=0; $i<count($params);$i++)
                {
                    $bind_name = 'bind' . $i;
                    $$bind_name = $params[$i];
                    $bind_names[] = &$$bind_name;
                }
                $return = call_user_func_array(array($stmt,'bind_param'),$bind_names);
            }

            # execute query
            $stmt->execute();

            # these lines of code below return one dimentional array, similar to mysqli::fetch_assoc()
            $meta = $stmt->result_metadata();

            while ($field = $meta->fetch_field()) {
                $var = $field->name;
                $$var = null;
                $parameters[$field->name] = &$$var;
            }

            call_user_func_array(array($stmt, 'bind_result'), $parameters);

            $resultsetarray = array();
            while($stmt->fetch())
            {
                $newRecord = array();
                foreach($parameters as $key => $value){
                    $newRecord[$key] = $value;
                }
                $resultsetarray[] = $newRecord;
            }
            $responseRecord->success = true;
            $responseRecord->resultArray = $resultsetarray;

            # close statement
            $stmt->close();
            return $resultsetarray;
        } catch (Exception $e){
            trigger_error("MySqliHelper.get_result exception: ".$e->getMessage(), E_USER_ERROR);
        }
	}

    /**
     * Executes sql on the database and returns an ExecuteResponseRecord. This method wraps the standard "execute" method and
     * standardizes error handling in case the execution fails. An error prefix is required.
     * @param $errorPrefix
     * @param $mysqli
     * @param $sql
     * @param null $types
     * @param null $params
     * @param ExecuteResponseRecord $responseRecord
     * @return ExecuteResponseRecord
     */
    public static function executeWithLogging($errorPrefix, $mysqli, $sql, $types = null, $params = null, ExecuteResponseRecord $responseRecord = null){
        if ($responseRecord == null){
            $responseRecord = new ExecuteResponseRecord();
        }
        if (!$errorPrefix){
            $errorPrefix = "Unspecified caller: SQL Error:";
        }
        self::execute($mysqli, $sql, $types, $params, $responseRecord);
        if (!$responseRecord->success){
            trigger_error($errorPrefix." ".$responseRecord->error, E_USER_ERROR);
        }
        return $responseRecord;
    }
	
	public static function loadExternalData($mysqli,$fileLocation,$tableName){
		$sql = "LOAD DATA LOCAL INFILE '".$fileLocation."'
				REPLACE
				INTO TABLE ".$tableName."
				FIELDS TERMINATED BY ','
				OPTIONALLY ENCLOSED BY '\"'
				LINES TERMINATED BY '\n'";

		//Try to execute query (not stmt) and catch mysqli error from engine and php error
		if (!($stmt = $mysqli->query($sql))) {
			$response["error"] =  "\nQuery execute failed: ERRNO: (" . $mysqli->errno . ") " . $mysqli->error;
		}else{
			$response["success"] = true;
		};				
		return $response;
	}

    /**
     * Executes sql on the database and returns true/false depending on success
     * @param mysqli $mysqli
     * @param string $sql
     * @param string $types
     * @param array $params
     * @param ExecuteResponseRecord $responseRecord
     * @return number of affected rows if successful, false otherwise
     */
	public static function execute($mysqli, $sql, $types = null, $params = null, ExecuteResponseRecord $responseRecord = null)
	{
        if ($responseRecord == null){
            $responseRecord = new ExecuteResponseRecord();
        }

        try {
            # create a prepared statement
            $stmt = $mysqli->prepare($sql);
            if ($stmt === false){
                // something went wrong!
                $responseRecord->error = "Helper.php: Error preparing statement [".$sql."]:".$mysqli->error;
                return false;
            }

            if($types && $params)
            {
                $bind_names[] = $types;
                for ($i=0; $i<count($params);$i++)
                {
                    $bind_name = 'bind' . $i;
                    $$bind_name = $params[$i];
                    $bind_names[] = &$$bind_name;
                }
                $return = call_user_func_array(array($stmt,'bind_param'),$bind_names);
            }

            # execute query
            $success = $stmt->execute();
            $responseRecord->success = $success;
            $responseRecord->affectedRows = $mysqli->affected_rows;
            $responseRecord->insertedId = $mysqli->insert_id;
            if ($success){
                $success = $mysqli->affected_rows;
                if (strpos(strtolower($sql), "insert ") !== false){
                    // insert stmt, therefore return the insertID instead
                    $success = $mysqli->insert_id;
                }
            } else {
                $responseRecord->error = $stmt->error;
            }

            # close statement
            $stmt->close();
            return $success;
        } catch (Exception $e){
            trigger_error("MySqliHelper.execute exception: ".$e->getMessage(), E_USER_ERROR);
        }
	}

    /**
     * Helper to log action on an event
     * @param $mysqli object $mysqli
     * @param integer $eventId
     * @param string $logAction
     * @param integer $memberId
     * @param null $adminId
     * @return bool|\ExecuteResponseRecord
     */
	public static function log_event_action($mysqli, $eventId, $logAction, $memberId, $adminId = null){
		return DbEventLogger::log_event_action($mysqli, $eventId, $logAction, $memberId, $adminId);
	}

    public static function append_log_action($mysqli, $logId, $messageToAppend){
        return DbEventLogger::append_log_action($mysqli, $logId, $messageToAppend);
    }
}
class QueryResponseRecord{
    public $success = false;
    public $resultArray = null;
    public $error;
}
class ExecuteResponseRecord{
    public $success = false;
    public $affectedRows = 0;
    public $insertedId = 0;
    public $error;
}
class DbEventLogger implements EventLogger{
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    public function log_action($eventId, $message, $memberId = 0, $adminId = null){
        return self::log($eventId, $message, $memberId, $adminId);
    }

    public function log($eventId, $logAction, $memberId, $adminId = null){
        return self::log_event_action($this->mysqli, $eventId, $logAction, $memberId, $adminId);
    }

    private static $EVENT_LOG_SQL = "
			INSERT INTO event_log
			(
				EventLog_EventID,
				EventLog_Action,
				EventLog_MemberID,
				EventLog_AdminID
			)
			VALUES
			(
				?,
				?,
				?,
				?
			)";

    /**
     * Helper to log action on an event
     * @param $mysqli object $mysqli
     * @param integer $eventId
     * @param string $logAction
     * @param integer $memberId
     * @param null $adminId
     * @return bool|\ExecuteResponseRecord
     */
    public static function log_event_action($mysqli, $eventId, $logAction, $memberId, $adminId = null){
        if (!is_numeric($eventId) || !is_numeric($memberId)){
            return false;
        }
        if (!isset($adminId)){
            $adminId = getAdminId();
        }
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($mysqli, self::$EVENT_LOG_SQL, "isii", array((int)$eventId, $logAction, (int)$memberId,(int)$adminId), $insertResponse);
        return $insertResponse;
    }

    public static function append_log_action($mysqli, $logId, $messageToAppend){
        $sqlUpdateString = "
            UPDATE event_log
            SET EventLog_Action = CONCAT_WS(',',EventLog_Action, ? )
            WHERE EventLog_ID = ?";
        $updateResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($mysqli, $sqlUpdateString, "si", array($messageToAppend, $logId), $updateResponse);
        return $updateResponse;
    }
}
class DbAdminLogger implements SystemLogger{

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $ERROR_LOG_SQL = "
		INSERT INTO admin_errorlog
		(AdminErrorLog_Page, AdminErrorLog_Message)
		VALUES
		(?,?)
		";

    public function log($sourceLocation, $message){
        return self::log_message($this->mysqli, $sourceLocation, $message);
    }

    public static function log_message($mysqli, $sourceLocation, $message){
        $sqlString = self::$ERROR_LOG_SQL;
        $sqlBindings = "ss";
        $sqlParams = array($sourceLocation,$message);
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $response;
    }
}
?>