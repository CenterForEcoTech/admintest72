<?php
class SocialMediaHandleProvider{

    /* @var mysqli */
    private $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            sma.id,
            sma.entity_type as entityType,
            sma.entity_id as entityId,
            sma.status,
            sn.name as socialNetworkName,
            sma.social_handle as socialHandle,
            sma.original_identifier_text as originalIdentifierText";

    private static $FROM_TABLE_CLAUSE = "
        FROM social_media_associations sma
        JOIN social_networks sn ON sma.social_network_id = sn.id
        WHERE 1 = 1";

    private static $FILTER_ON_ORGANIZATION_ID = "
        AND sma.entity_type = 'organization'
        AND sma.entity_id = ?";

    private static $FILTER_ON_MERCHANT_ID = "
        AND sma.entity_type = 'merchant'
        AND sma.entity_id = ?";

    private static $FILTER_ON_INDIVIDUAL_ID = "
        AND sma.entity_type = 'individual'
        AND sma.entity_id = ?";

    private static $FILTER_ON_NETWORK_NAME = "
        AND sn.name = ?";

    private static $FILTER_ON_ACTIVE_ONLY = "
        AND sma.status = 'ACTIVE'";

    public function get(SocialMediaHandleCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->orgId && is_numeric($criteria->orgId)){
            $filterString .= self::$FILTER_ON_ORGANIZATION_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->orgId;
        }
        if ($criteria->merchantId && is_numeric($criteria->merchantId)){
            $filterString .= self::$FILTER_ON_MERCHANT_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->merchantId;
        }
        if ($criteria->individualId && is_numeric($criteria->individualId)){
            $filterString .= self::$FILTER_ON_INDIVIDUAL_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->individualId;
        }
        if ($criteria->socialNetworkName){
            $filterString .= self::$FILTER_ON_NETWORK_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->socialNetworkName;
        }
        if ($criteria->activeOnly){
            $filterString .= self::$FILTER_ON_ACTIVE_ONLY;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("CampaignProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("CampaignProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY sn.name";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        $record = new SocialMediaHandle();

        // iterate over object
        foreach($dataRow as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    /**
     * Enforces a rule that an entity may only have one handle of a particular social network.
     * @param SocialMediaHandle $newHandle
     * @param int memberId
     * @return null|\SocialMediaHandle
     */
    public function setSingleHandle(SocialMediaHandle $newHandle, $memberId = 0){
        // first find existing handle(s) if any
        $criteria = new SocialMediaHandleCriteria();
        $criteria->merchantId = $newHandle->getMerchantId();
        $criteria->orgId = $newHandle->getOrganizationId();
        $criteria->individualId = $newHandle->getIndividualId();
        $criteria->socialNetworkName = $newHandle->socialNetworkName;
        $existingHandles = $this->get($criteria);

        $createNew = strlen($newHandle->socialHandle) > 0;
        $savedRecord = null;
        if (count($existingHandles->collection)){
            foreach ($existingHandles->collection as $handle){
                /* @var $handle SocialMediaHandle */
                if (!$createNew){
                    // we are already going to eliminate all handles
                    $this->delete($handle);
                } else if ($handle->socialHandle === $newHandle->socialHandle && $handle->status == SocialMediaHandle::$ACTIVE_STATUS){
                    // exists! don't do anything!
                    $createNew = false;
                    $savedRecord = $newHandle;
                } else if ($handle->socialHandle !== $newHandle->socialHandle){
                    // delete the existing one!
                    $this->delete($handle);
                }
            }
        }
        if ($createNew){
            $this->save($newHandle);
            $savedRecord = $newHandle;
        }
        return $savedRecord;
    }

    /**
     * Given a handle that identifies the entity, the social network, and the handle, will save the handle as a new handle.
     * If the handle's status is deleted, this will delete the handle.
     * @param SocialMediaHandle $handle
     * @return ExecuteResponseRecord
     */
    public function save(SocialMediaHandle $handle){
        if ($handle->status == SocialMediaHandle::$DELETED_STATUS){
            return $this->update($handle);
        }
        return $this->add($handle);
    }

    /**
     * Helper function that takes a handle, sets its status to deleted, then calls save on it.
     * @param SocialMediaHandle $handle
     * @return ExecuteResponseRecord
     */
    public function delete(SocialMediaHandle $handle){
        $handle->status = SocialMediaHandle::$DELETED_STATUS;
        return $this->save($handle);
    }

    private static $ADD_SQL = "
        INSERT INTO social_media_associations
        (
            entity_type,
            entity_id,
            status,

            social_network_id,
            social_handle,
            original_identifier_text,
            created_by_member_id,
            created_by_admin_id
        )
        SELECT
            ?,?,?,
            (SELECT sn.id FROM social_networks sn WHERE sn.name = ?),
            ?,?,?,?
        FROM dual
        ON DUPLICATE KEY UPDATE
            original_identifier_text = ?,
            status = 'ACTIVE',
            updated_date = CURRENT_TIMESTAMP,
            updated_by_member_id = ?,
            updated_by_admin_id = ?";

    private static $ADD_BINDING = "sissssiisii";

    private function add(SocialMediaHandle $handle){
        $sqlString = self::$ADD_SQL;
        $sqlBinding = self::$ADD_BINDING;
        $sqlParams = array(
            $handle->entityType,
            $handle->entityId,
            $handle->status,

            $handle->socialNetworkName,
            $handle->socialHandle,
            $handle->originalIdentifierText,
            getMemberId(),
            getAdminId(),

            $handle->originalIdentifierText,
            getMemberId(),
            getAdminId()
        );
        return MySqliHelper::executeWithLogging("SocialMediaHandleProvider.add: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $UPDATE_SQL = "
        UPDATE social_media_associations
        SET
            status = ?,
            original_identifier_text = ?,
            updated_date = CURRENT_TIMESTAMP,
            updated_by_member_id = ?,
            updated_by_admin_id = ?
        WHERE entity_type = ?
        AND entity_id = ?
        AND social_network_id = (SELECT sn.id FROM social_networks sn WHERE sn.name = ?)
        AND social_handle = ?";

    private function update(SocialMediaHandle $handle){
        $sqlString = self::$UPDATE_SQL;
        $sqlBinding = "ssiisiss";
        $sqlParams = array(
            $handle->status,
            $handle->originalIdentifierText,
            getMemberid(),
            getAdminId(),

            $handle->entityType,
            $handle->entityId,
            $handle->socialNetworkName,
            $handle->socialHandle
        );

        return MySqliHelper::executeWithLogging("SocialMediaHandleProvider.update:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}