<?php
if (!isset($rootFolder)){
    include_once("_getRootFolder.php");
}
include_once($rootFolder."facebook/facebook.php");
include_once($rootFolder."repository/TwitterAPIExchange.php");

// provides caching
class SocialMediaReachProvider{

    /* @var mysqli */
    private $mysqli;

    /* @var array */
    private $fbSettings;

    /* @var array */
    private $twitterSettings;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    public function getCounts(SocialMediaHandle $handle){
        if ($handle->socialNetworkName == SocialMediaHandle::$FACEBOOK_NAME && isset($this->fbSettings)){
            return $this->getFacebookLikeCount($handle->socialHandle);
        } else if ($handle->socialNetworkName == 'twitter' && isset($this->twitterSettings)){
            return $this->getTwitterFollowerCount($handle->socialHandle);
        }
        return (object)array(
            'count' => null,
            'message' => "bad request"
        );
    }

    public function setFacebookCredentials($appId, $appSecret){
        $this->fbSettings = array(
            'appId' => $appId,
            'secret' => $appSecret
        );
    }

    private function getFacebookLikeCount($facebookHandle){
// TODO: HANDLE CACHING
        $facebook = new Facebook($this->fbSettings);
        $query = "/".$facebookHandle."?fields=likes";
        $response = (object)array(
            'count' => null,
            'message' => null
        );
        try {
            $fbResponse = $facebook->api($query);
            if (is_array($fbResponse)){
                if ($fbResponse['likes']){
                    $response->count = $fbResponse['likes'];
                } else {
                    $response->message = "This page has not authorized anonymous access to their likes count";
                    trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$facebookHandle.": ".$response->message, E_USER_ERROR);
                }
            } else {
                $response->message = "No response";
                trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$facebookHandle.": ".$response->message, E_USER_ERROR);
            }
        } catch (Exception $e){
            $response->message = $e->getMessage();
            trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$facebookHandle.": ".$response->message, E_USER_ERROR);
        }
        return $response;
    }

    public function setTwitterCredentials($oauth_access_token, $oauth_access_token_secret, $consumer_key, $consumer_secret){
        $this->twitterSettings = array(
            'oauth_access_token' => $oauth_access_token,
            'oauth_access_token_secret' => $oauth_access_token_secret,
            'consumer_key' => $consumer_key,
            'consumer_secret' => $consumer_secret
        );
    }

    private function getTwitterFollowerCount($twitterHandle){
// TODO: HANDLE CACHING
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        $getField = "?screen_name=".preg_replace("/@?(.*)/", "$1", $twitterHandle)."&count=1";
        $requestMethod = "GET";
        $twitter = new TwitterAPIExchange($this->twitterSettings);
        $userTimelineJSON =
            $twitter
                ->setGetfield($getField)
                ->buildOauth($url, $requestMethod)
                ->performRequest();
        $userTimeline = json_decode($userTimelineJSON, true);

        $response = (object)array(
            'count' => null,
            'message' => null
        );

        if (is_array($userTimeline) && count($userTimeline)){
            // all responses are arrays anyway
            if ($userTimeline['errors']){
                // too bad
                $output = "";
                foreach ($userTimeline['errors'] as $error){
                    $output .= $error['message'].";";
                }
                $response->message = $output;
                trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$twitterHandle.": ".$response->message, E_USER_ERROR);
            } else if ($userTimeline[0]['user'] && $userTimeline[0]['user']['followers_count']){
                $response->count = $userTimeline[0]['user']['followers_count'];
            } else {
                $response->message = "Unexpected response.";
                trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$twitterHandle.": ".$response->message, E_USER_ERROR);
            }
        } else {
            $response->message = "No response";
            trigger_error("SocialMediaReachProvider.getTwitterFollowerCount: error with handle ".$twitterHandle.": ".$response->message, E_USER_ERROR);
        }
        return $response;
    }
}