<?php
class EventDraftRecordProvider implements eventAPI{

    /**
     * @var mysqli
     */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    /**
     * Implement Interface
     * This is for the RSS which lists items in descending chron order.
     * @see repository/eventAPI#getAll()
     */
    public function getAll() {
        $criteria = new EventCriteria();
        $criteria->upcomingOnly = true;
        $criteria->publishedOnly = true;
        $criteria->size = 0;
        $paginatedResults = $this->get($criteria);
        return $paginatedResults->collection;
    }

    /**
     * Implement Interface
     * Returns paginated results based on start index and max records per page.
     * Default sort on date and time = 'ASC' and only 'ASC' or 'DESC' is valid.
     * @see repository/eventAPI#getNext($maxRecords)
     * @param $startIndex
     * @param int $maxRecords
     * @param string $dateSort
     * @return array|\PaginatedResult
     */
    public function getNext($startIndex, $maxRecords, $dateSort = "ASC"){
        if (!is_numeric($startIndex) || $startIndex < 0){
            $startIndex = 0;
        }
        if (!is_numeric($maxRecords) || $maxRecords <= 0){
            $maxRecords = 5;
        }
        if (strtolower($dateSort) != "desc"){
            $dateSort = "ASC";
        }

        $criteria = new EventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->publishedOnly = true;
        $criteria->start = $startIndex;
        $criteria->size = $maxRecords;
        $criteria->multiColumnSort[] = new MultiColumnSort("startDate", $dateSort);
        $criteria->multiColumnSort[] = new MultiColumnSort("startTime", $dateSort);
        $paginatedResults = $this->get($criteria);
        return $paginatedResults;
    }

    private static $GET_SELECT_COLUMNS = "
        SELECT
            1 as isDraft,
            e.EventDraft_EventID as publishedEventId,
            e.EventDraft_EventTemplateID as eventTemplateID,
            ( SELECT EventTemplate_URL FROM event_templates WHERE EventTemplate_ID = e.EventDraft_EventTemplateID) as eventTemplateUrl,
			e.EventDraft_ID as id,
			e.EventDraft_Title as title,
			e.EventDraft_Description as description,
			e.EventDraft_StartDate as startDate,
			e.EventDraft_StartTime as startTime,
			e.EventDraft_EndDate as endDate,
			e.EventDraft_EndTime as endTime,
			e.EventDraft_IsNational as isNationalEvent,
			e.EventDraft_IsOnline as isOnlineEvent,
			e.EventDraft_OnlineLocationLabel as onlineLocationLabel,
			e.EventDraft_OnlineLocationLink as onlineLocationLink,

			/* venue (merchant) info (for merchant created events only) */
			e.EventDraft_FeaturedCausePercentage as featuredCausePercentage,
			e.EventDraft_CustomerChoicePercentage as customerChoicePercentage,
			e.EventDraft_IsFlatDonation as isFlatDonation,
			e.EventDraft_DonationPerUnit as donationPerUnit,
			e.EventDraft_UnitDescriptor as unitDescriptor,
			e.EventDraft_FlatDonationActionPhrase as flatDonationActionPhrase,
			e.EventDraft_EventTemplatePremiumID as eventTemplatePremiumId,
			m.MerchantInfo_ID as venueId,
			m.MerchantInfo_Name as venueName,
			m.MerchantInfo_Website as venueUrl,
			m.MerchantInfo_Description as venueDescription,
			m.MerchantInfo_Lat as venueLat,
			m.MerchantInfo_Long as venueLong,
			m.MerchantInfo_Address as venueAddress,
			m.MerchantInfo_City as venueCity,
			m.MerchantInfo_State as venueState,
			m.MerchantInfo_Zip as venueZip,
			m.MerchantInfo_Country as venueCountry,
			m.MerchantInfo_Phone as venuePhone,
			m.MerchantInfo_EventBriteVenueID as venueEventBriteId,
			m.MerchantInfo_Neighborhood as venueNeighborhood,
			m.MerchantInfo_Keywords as venueKeywords,

			/* cause (nonprofit) info (for merchant created events only) */
			o.OrganizationInfo_ID as causeId,
			o.OrganizationInfo_Name as causeName,
			o.OrganizationInfo_EIN as causeEIN,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = o.OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as causePid,

			e.EventDraft_CreatedDate as createdDate,
			e.EventDraft_LastEditedDate as updatedDate,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'merchant'
	            AND entity_id = MerchantInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as venuePid";

    private static $FROM_TABLE_CLAUSE = "
        FROM event_draft e
        JOIN merchant_info m ON e.EventDraft_ProposedByMerchantID = m.MerchantInfo_ID
        LEFT JOIN organization_info o ON e.EventDraft_ProposedByOrgID = o.OrganizationInfo_ID
        WHERE EventDraft_EventID = 0
		AND EventDraft_Status = 'ACTIVE'";

    private static $BASIC_QUERY_BINDING = "";

    private static $FILTER_ON_MERCHANT = "
        AND m.MerchantInfo_ID = ?";

    private static $FILTER_ON_ID = "
        AND e.EventDraft_ID = ?";

    /**
     * @param EventCriteria $criteria
     * @param bool $getCountOnly
     * @return PagedResult
     */
    public function get(EventCriteria $criteria, $getCountOnly = false){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$GET_SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = self::$BASIC_QUERY_BINDING;
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->merchantId && is_numeric($criteria->merchantId) && $criteria->merchantId > 0){
            $filterString .= self::$FILTER_ON_MERCHANT;
            $filterBinding .= "i";
            $filterParams[] = $criteria->merchantId;
        }

        if ($criteria->eventId && is_numeric($criteria->eventId) && $criteria->eventId > 0){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->eventId;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if (!$getCountOnly && $result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        $result->collection[] = $this->mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("EventDraftRecordProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventDraftRecordProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY e.EventDraft_StartDate desc, e.EventDraft_StartTime desc";

    private function getOrderByClause(EventByLocationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "startDate":
                        $orderByStrings[] = "e.EventDraft_StartDate ".$sortOrder;
                        break;
                    case "startTime":
                        $orderByStrings[] = "e.EventDraft_StartTime ".$sortOrder;
                        break;
                    case "endDate":
                        $orderByStrings[] = "e.EventDraft_EndDate ".$sortOrder;
                        break;
                    case "endTime":
                        $orderByStrings[] = "e.EventDraft_EndTime ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private function mapToCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToRecord($row);
        }
        return $collection;
    }

    private function mapToRecord($dataRow) {
        $record = new EventRecord();
        $record->id = $dataRow->id;
        $record->title = $dataRow->title;
        $record->description = $dataRow->description;
        $record->createdDate = strtotime($dataRow->createdDate);
        $record->lastUpdatedDate = strtotime($dataRow->updatedDate);
        $record->setStartDate($dataRow->startDate, $dataRow->startTime);
        $record->setEndDate($dataRow->endDate, $dataRow->endTime);
        $record->isMultiDay = $dataRow->startDate != $dataRow->endDate;
        $record->commaDelimitedTagNames = $dataRow->tagNames;
        $record->customAttendLink = $dataRow->customAttendLink;
        $record->isNationalEvent = $dataRow->isNationalEvent;
        $record->isOnlineEvent = $dataRow->isOnlineEvent;
        $record->onlineLocationLabel = $dataRow->onlineLocationLabel;
        $record->onlineLocationLink = $dataRow->onlineLocationLink;
        $record->isDraft = $dataRow->isDraft;
        $record->publishedEventId = $dataRow->publishedEventID;

        if (isset($dataRow->eventTemplateID)){
            $record->eventTemplateId = $dataRow->eventTemplateID;
            $record->eventTemplateUrl = $dataRow->eventTemplateUrl;
        }

        $record->venue = $this->mapToVenue($dataRow);

        if (is_numeric($dataRow->causeId)){
            $record->cause = $this->mapToCause($dataRow);
        }
        return $record;
    }

    private function mapToCause($dataRow){
        $cause = new Cause();
        $cause->id = $dataRow->causeId;
        $cause->name = $dataRow->causeName;
        $cause->ein = $dataRow->causeEin;
        $cause->setURL($dataRow->causeUrl);
        $cause->pid = $dataRow->causePid;
        return $cause;
    }

    private function mapToVenue($dataRow){
        $venue = new Venue();
        $venue->id = $dataRow->venueId;
        $venue->pid = $dataRow->venuePid;
        $venue->name = $dataRow->venueName;
        $venue->url = $venue->setURL($dataRow->venueUrl);
        $venue->description = $dataRow->venueDescription;
        $venue->location = $this->getEventLocation($venue->url, $venue->name);
        if (is_numeric($dataRow->venueLat)){
            $venue->lat = $dataRow->venueLat;
        }
        if (is_numeric($dataRow->venueLong)){
            $venue->long = $dataRow->venueLong;
        }
        $venue->streetAddr = $dataRow->venueAddress;
        $venue->city = $dataRow->venueCity;
        $venue->region = $dataRow->venueState;
        $venue->postalCode = $dataRow->venueZip;
        $venue->country = $dataRow->venueCountry;
        $venue->phone = $this->getPhone($dataRow->venuePhone);
        if (is_numeric($dataRow->featuredCausePercentage)){
            $venue->featuredCausePercentage = $dataRow->featuredCausePercentage;
        }

        if (is_numeric($dataRow->customerChoicePercentage)){
            $venue->customerChoicePercentage = $dataRow->customerChoicePercentage;
        }
        if ($dataRow->isFlatDonation){
            $venue->isFlatDonation = true;
            $venue->donationPerUnit = $dataRow->donationPerUnit;
            $venue->unitDescriptor = $dataRow->unitDescriptor;
            $venue->flatDonationActionPhrase = $dataRow->flatDonationActionPhrase;
        }

        $venue->facebookEventID = $dataRow->facebookEventId;
        $venue->eventBriteEventID = $dataRow->eventBriteEventId;
        $venue->eventBriteVenueID = $dataRow->venueEventBriteId;
        $venue->eventTemplatePremiumId = $dataRow->eventTemplatePremiumId;
        $venue->keywords = $dataRow->venueKeywords;
        $venue->neighborhood = $dataRow->venueNeighborhood;

        return $venue;
    }

    private function getFullDate($dateString, $timeString) {
        if (substr_compare($dateString, "0000", 0, 4) == 0){
            return NULL;
        }
        return strtotime($dateString." ".$timeString);
    }

    private function getEventLocation($merchantWebsite, $merchantName) {
        return ($merchantWebsite) ? $merchantWebsite : $merchantName;
    }

    private function getPhone($phoneString) {
        $cleanPhone = preg_replace("/[^0-9]/", "", $phoneString);
        if (strlen($cleanPhone) > 6){
            return preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $cleanPhone);

            //return "(".substr($cleanPhone, 0, 3).") ".substr($cleanPhone, 3, 3)."-".substr($cleanPhone,6);
        }
        return $cleanPhone;
    }

    // --------------------
    // CRUD operations
    // --------------------

    public function put(EventRecord $eventRecord, $memberID){
        if ($eventRecord->id){
            return $this->updateDraftCustomerChoiceEvent($eventRecord, $memberID);
        } else {
            return $this->createDraftCustomerChoiceEvent($eventRecord, $memberID);
        }
    }

    private function createDraftCustomerChoiceEvent(EventRecord $eventRecord, $memberID){
        $sqlString = "";
        $sqlBinding = "";
        $sqlParams = array();
        $insertHelper = new InsertEventDraftHelper($eventRecord, $memberID, getAdminId());
        $insertHelper->setupInsert($sqlString, $sqlBinding, $sqlParams);
        $insertResponse = MySqliHelper::executeWithLogging("MySqliEventManager.createEvent: SQL: [".$sqlString."] Binding:".$sqlBinding." Param Count:".count($sqlParams)." :", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);

        if ($insertResponse->success){
            // add action to event_log
            $logged = MySqliHelper::log_event_action($this->mysqli, $insertResponse->insertedId, 'Created Draft Event', $memberID);
        }

        return $insertResponse;
    }

    private function updateDraftCustomerChoiceEvent(EventRecord $eventRecord, $memberID){
        $sqlString = "";
        $sqlBinding = "";
        $sqlParams = array();
        $insertHelper = new UpdateEventDraftHelper($eventRecord, $memberID, getAdminId());
        $insertHelper->setupUpdate($sqlString, $sqlBinding, $sqlParams);
        //echo $sqlString; var_dump($sqlParams);die();
        return MySqliHelper::executeWithLogging("MySqliEventManager.createEvent: SQL: [".$sqlString."] Binding:".$sqlBinding." Param Count:".count($sqlParams)." :", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}



/**
 * Class InsertEventDraftHelper
 * This helper just constructs the insert SQL, handles the bindings, and assigns the parameters for the bindings.
 * This is easier to debug since there are so many columns being inserted, and it is also easier to conditionally add sets of columns;
 */
class InsertEventDraftHelper{

    /* @var EventRecord */
    private $eventRecord;
    private $sqlBindings = "";
    private $sqlParams = array();
    private $memberId;
    private $adminId;

    private $sqlFieldsString = "";
    private $sqlParametersString = "";

    public function __construct(EventRecord $record, $memberId, $adminId){
        $this->eventRecord = $record;
        $this->memberId = $memberId ? $memberId : 0;
        $this->adminId = $adminId ? $adminId : 0;
    }

    /**
     * Sets up the insert sqlString, sqlBindings, and sqlParams. The passed in variables will be overwritten.
     * @param $sqlString
     * @param $sqlBindings
     * @param $sqlParams
     */
    public function setupInsert(&$sqlString, &$sqlBindings, &$sqlParams){
        $this->setupBasicFields();
        $this->setupAuditFields();
        $this->setupOnlineFields();
        $this->setupCausetownEventFields();
        if ($this->eventRecord->venue->isFlatDonation){
            $this->setupFlatDonationFields();
        } else {
            $this->setupPercentageFields();
        }
        $this->setupMiscFields();

        $sqlString = $this->getSqlString();
        $sqlBindings = $this->sqlBindings;
        $sqlParams = $this->sqlParams;
    }

    private function getSqlString(){
        return "INSERT INTO event_draft (".$this->sqlFieldsString.") VALUES (".$this->sqlParametersString.")";
    }

    private function appendInsertFields($string){
        $this->sqlFieldsString .= (strlen($this->sqlFieldsString)) ? "," : "";
        $this->sqlFieldsString .= $string;
    }

    private function appendParametersString($string){
        $this->sqlParametersString .= (strlen($this->sqlParametersString)) ? "," : "";
        $this->sqlParametersString .= $string;
    }

    private function appendBindings($string){
        $this->sqlBindings .= $string;
    }

    private function appendParamValues(array $paramValues){
        $this->sqlParams = array_merge($this->sqlParams, $paramValues);
    }

    private static $BASIC_INSERT_FIELDS = "
        EventDraft_ProposedByMerchantID,
        EventDraft_Title,
        EventDraft_Description,
        EventDraft_StartDate,
        EventDraft_EndDate,
        EventDraft_StartTime,
        EventDraft_EndTime";

    private function setupBasicFields(){
        $this->appendInsertFields(self::$BASIC_INSERT_FIELDS);
        $this->appendBindings("issssss");
        $this->appendParametersString("?,?,?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->id,
                $this->eventRecord->title,
                $this->eventRecord->description,
                date("Y-m-d", $this->eventRecord->startDate),
                date("Y-m-d", $this->eventRecord->endDate),
                date("H:i", $this->eventRecord->startDate),
                date("H:i", $this->eventRecord->endDate)
            )
        );
    }

    private static $AUDIT_INSERT_FIELDS = "
        EventDraft_SubmittedByMemberID,
        EventDraft_CreatedDate,
        EventDraft_LastEditedDate,
        EventDraft_LastEditedByMemberID,
        EventDraft_CreatedByMemberID,
        EventDraft_CreatedByAdminID,
        EventDraft_UpdatedByAdminID";

    private function setupAuditFields(){
        $this->appendInsertFields(self::$AUDIT_INSERT_FIELDS);
        $this->appendBindings("iiiii");
        $this->appendParametersString("?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?,?,?,?");
        $this->appendParamValues(array(
                $this->memberId,
                $this->memberId,
                $this->memberId,
                $this->adminId,
                $this->adminId
            )
        );
    }

    private static $ONLINE_INSERT_FIELDS = "
        EventDraft_IsNational,
        EventDraft_IsOnline,
        EventDraft_OnlineLocationLink,
        EventDraft_OnlineLocationLabel";

    private function setupOnlineFields(){
        $this->appendInsertFields(self::$ONLINE_INSERT_FIELDS);
        $this->appendBindings("iiss");
        $this->appendParametersString("?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->isNationalEvent ? 1 : 0,
                $this->eventRecord->isOnlineEvent ? 1 : 0,
                $this->eventRecord->onlineLocationLink."",
                $this->eventRecord->onlineLocationLabel.""
            )
        );
    }

    private static $CAUSETOWN_EventDraft_INSERT_FIELDS = "
        EventDraft_IsCausetownEvent,
        EventDraft_SuppressCharitySelectionForm,
        EventDraft_SuppressPrintFlier";

    private function setupCausetownEventFields(){
        $this->appendInsertFields(self::$CAUSETOWN_EventDraft_INSERT_FIELDS);
        $this->appendBindings("iii");
        $this->appendParametersString("?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->isCausetownEvent ? 1 : 0,
                $this->eventRecord->suppressCharitySelectionForm ? 1 : 0,
                $this->eventRecord->suppressPrintFlier ? 1 : 0
            )
        );
    }

    private static $PERCENTAGE_INSERT_FIELDS = "
        EventDraft_FeaturedCausePercentage,
        EventDraft_CustomerChoicePercentage";

    private function setupPercentageFields(){
        $this->appendInsertFields(self::$PERCENTAGE_INSERT_FIELDS);
        $this->appendBindings("ii");
        $this->appendParametersString("?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->featuredCausePercentage,
                $this->eventRecord->venue->customerChoicePercentage
            )
        );
    }

    private static $FLAT_DONATION_INSERT_FIELDS = "
        EventDraft_IsFlatDonation,
        EventDraft_DonationPerUnit,
        EventDraft_UnitDescriptor,
        EventDraft_FlatDonationActionPhrase,
        EventDraft_IsFcOnly";

    private function setupFlatDonationFields(){
        $this->appendInsertFields(self::$FLAT_DONATION_INSERT_FIELDS);
        $this->appendBindings("idssi");
        $this->appendParametersString("?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->isFlatDonation ? 1 : 0,
                $this->eventRecord->venue->donationPerUnit,
                $this->eventRecord->venue->unitDescriptor,
                $this->eventRecord->venue->flatDonationActionPhrase,
                $this->eventRecord->venue->isFcOnly ? 1 : 0
            )
        );
    }

    private static $MISC_INSERT_FIELDS = "
        EventDraft_ProposedByOrgID,
        EventDraft_EventTemplateID,
        EventDraft_EventTemplatePremiumID,
        EventDraft_OrgWhyYou,
        EventDraft_CustomAttendLink";

    private function setupMiscFields(){
        $this->appendInsertFields(self::$MISC_INSERT_FIELDS);
        $this->appendBindings("iiiss");
        $this->appendParametersString("?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->cause->id ? $this->eventRecord->cause->id : 0,
                $this->eventRecord->eventTemplateId ? $this->eventRecord->eventTemplateId : 0,
                $this->eventRecord->venue->eventTemplatePremiumId ? $this->eventRecord->venue->eventTemplatePremiumId : 0,
                $this->eventRecord->whyHostThisDescription."",
                $this->eventRecord->customAttendLink.""
            )
        );
    }
}

/**
 * Class UpdateEventDraftHelper
 * This helper just constructs the insert SQL, handles the bindings, and assigns the parameters for the bindings.
 * This is easier to debug since there are so many columns being inserted, and it is also easier to conditionally add sets of columns;
 */
class UpdateEventDraftHelper{

    /* @var EventRecord */
    private $eventRecord;
    private $sqlUpdateString = "";
    private $sqlWhereString = "";
    private $sqlBindings = "";
    private $sqlParams = array();
    private $memberId;
    private $adminId;

    public function __construct(EventRecord $record, $memberId, $adminId){
        $this->eventRecord = $record;
        $this->memberId = $memberId ? $memberId : 0;
        $this->adminId = $adminId ? $adminId : 0;
    }

    /**
     * Sets up the insert sqlString, sqlBindings, and sqlParams. The passed in variables will be overwritten.
     * @param $sqlString
     * @param $sqlBindings
     * @param $sqlParams
     */
    public function setupUpdate(&$sqlString, &$sqlBindings, &$sqlParams){
        $this->setupBasicFields();
        $this->setupAuditFields();
        $this->setupOnlineFields();
        $this->setupCausetownEventFields();
        if ($this->eventRecord->venue->isFlatDonation){
            $this->setupFlatDonationFields();
        } else {
            $this->setupPercentageFields();
        }
        $this->setupMiscFields();
        $this->setupWhereClause();

        $sqlString = $this->getSqlString();
        $sqlBindings = $this->sqlBindings;
        $sqlParams = $this->sqlParams;
    }

    private function getSqlString(){
        return "UPDATE event_draft SET ".$this->sqlUpdateString." WHERE ".$this->sqlWhereString;
    }

    private function appendUpdateFields($string){
        $this->sqlUpdateString .= (strlen($this->sqlUpdateString)) ? "," : "";
        $this->sqlUpdateString .= $string;
    }

    private function appendBindings($string){
        $this->sqlBindings .= $string;
    }

    private function appendParamValues(array $paramValues){
        $this->sqlParams = array_merge($this->sqlParams, $paramValues);
    }

    private function setupWhereClause(){
        $this->sqlWhereString = " EventDraft_ID = ? AND EventDraft_ProposedByMerchantID = ?";
        $this->appendParamValues(array(
            $this->eventRecord->id,
            $this->eventRecord->venue->id
        ));
        $this->appendBindings("ii");
    }

    private static $BASIC_UPDATE_FIELDS = "
        EventDraft_Title = ?,
        EventDraft_Description = ?,
        EventDraft_StartDate = ?,
        EventDraft_EndDate = ?,
        EventDraft_StartTime = ?,
        EventDraft_EndTime = ?";

    private function setupBasicFields(){
        $this->appendUpdateFields(self::$BASIC_UPDATE_FIELDS);
        $this->appendBindings("ssssss");
        $this->appendParamValues(array(
                $this->eventRecord->title,
                $this->eventRecord->description,
                date("Y-m-d", $this->eventRecord->startDate),
                date("Y-m-d", $this->eventRecord->endDate),
                date("H:i", $this->eventRecord->startDate),
                date("H:i", $this->eventRecord->endDate)
            )
        );
    }

    private static $AUDIT_UPDATE_FIELDS = "
        EventDraft_LastEditedDate = CURRENT_TIMESTAMP,
        EventDraft_LastEditedByMemberID = ?,
        EventDraft_UpdatedByAdminID = ?";

    private function setupAuditFields(){
        $this->appendUpdateFields(self::$AUDIT_UPDATE_FIELDS);
        $this->appendBindings("ii");
        $this->appendParamValues(array(
                $this->memberId,
                $this->adminId
            )
        );
    }

    private static $ONLINE_UPDATE_FIELDS = "
        EventDraft_IsNational = ?,
        EventDraft_isOnline = ?,
        EventDraft_OnlineLocationLink = ?,
        EventDraft_OnlineLocationLabel = ?";

    private function setupOnlineFields(){
        $this->appendUpdateFields(self::$ONLINE_UPDATE_FIELDS);
        $this->appendBindings("iiss");
        $this->appendParamValues(array(
                $this->eventRecord->isNationalEvent ? 1 : 0,
                $this->eventRecord->isOnlineEvent ? 1 : 0,
                $this->eventRecord->onlineLocationLink."",
                $this->eventRecord->onlineLocationLabel.""
            )
        );
    }

    private static $CAUSETOWN_EVENT_UPDATE_FIELDS = "
        EventDraft_IsCausetownEvent = ?,
        EventDraft_SuppressCharitySelectionForm = ?,
        EventDraft_SuppressPrintFlier = ?";

    private function setupCausetownEventFields(){
        $this->appendUpdateFields(self::$CAUSETOWN_EVENT_UPDATE_FIELDS);
        $this->appendBindings("iii");
        $this->appendParamValues(array(
                $this->eventRecord->isCausetownEvent ? 1 : 0,
                $this->eventRecord->suppressCharitySelectionForm ? 1 : 0,
                $this->eventRecord->suppressPrintFlier ? 1 : 0
            )
        );
    }

    private static $PERCENTAGE_UPDATE_FIELDS = "
        EventDraft_IsFlatDonation = 0,
        EventDraft_DonationPerUnit = 0,
        EventDraft_UnitDescriptor = '',
        EventDraft_FlatDonationActionPhrase = '',
        EventDraft_FeaturedCausePercentage = ?,
        EventDraft_CustomerChoicePercentage = ?,
        EventDraft_IsFcOnly = 0";

    private function setupPercentageFields(){
        $this->appendUpdateFields(self::$PERCENTAGE_UPDATE_FIELDS);
        $this->appendBindings("ii");
        $this->appendParamValues(array(
                $this->eventRecord->venue->featuredCausePercentage,
                $this->eventRecord->venue->customerChoicePercentage
            )
        );
    }

    private static $FLAT_DONATION_UPDATE_FIELDS = "
        EventDraft_FeaturedCausePercentage = 0,
        EventDraft_CustomerChoicePercentage = 0,
        EventDraft_IsFlatDonation = ?,
        EventDraft_DonationPerUnit = ?,
        EventDraft_UnitDescriptor = ?,
        EventDraft_FlatDonationActionPhrase = ?,
        EventDraft_IsFcOnly = ?";

    private function setupFlatDonationFields(){
        $this->appendUpdateFields(self::$FLAT_DONATION_UPDATE_FIELDS);
        $this->appendBindings("idssi");
        $this->appendParamValues(array(
                $this->eventRecord->venue->isFlatDonation ? 1 : 0,
                $this->eventRecord->venue->donationPerUnit,
                $this->eventRecord->venue->unitDescriptor,
                $this->eventRecord->venue->flatDonationActionPhrase,
                $this->eventRecord->venue->isFcOnly ? 1 : 0
            )
        );
    }

    private static $MISC_UPDATE_FIELDS = "
        EventDraft_ProposedByOrgID = ?,
        EventDraft_EventTemplateID = ?,
        EventDraft_EventTemplatePremiumID = ?,
        EventDraft_OrgWhyYou = ?,
        EventDraft_CustomAttendLink = ?";

    private function setupMiscFields(){
        $this->appendUpdateFields(self::$MISC_UPDATE_FIELDS);
        $this->appendBindings("iiiss");
        $this->appendParamValues(array(
                $this->eventRecord->cause->id ? $this->eventRecord->cause->id : 0,
                $this->eventRecord->eventTemplateId ? $this->eventRecord->eventTemplateId : 0,
                $this->eventRecord->venue->eventTemplatePremiumId ? $this->eventRecord->venue->eventTemplatePremiumId : 0,
                $this->eventRecord->whyHostThisDescription."",
                $this->eventRecord->customAttendLink.""
            )
        );
    }
}