<?php
class ApiAuthProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT 
			APIAuth_ID as id,
			APIAuth_IPAddress as ipAddress,
			APIAuth_Secret as secret,
			APIAuth_Status as status
		";

    private static $FROM_TABLE_CLAUSE = "
        FROM api_auth WHERE 1 = 1";

    private static $FILTER_ON_IP = "
        AND APIAuth_IPAddress = ?";
    private static $FILTER_ON_SECRET = "
        AND APIAuth_Secret = ?";
    private static $FILTER_ON_STATUS = "
        AND APIAuth_Status = ?";
				
    public function get($criteria){
        $sqlString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_IP; 
        $filterBinding = "s";
        $filterParams = array($criteria->ip);
        // apply search
		if ($criteria->secret){
            $filterString .= self::$FILTER_ON_SECRET;
            $filterBinding .= "s";
            $filterParams[] = $criteria->secret;
		}
		if ($criteria->status){
            $filterString .= self::$FILTER_ON_STATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->status;
		}
		// get the page
		$pageSqlString = $sqlString.$fromTableClause.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("MuniProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    } 	
	
    private static $INSERT_LOG_SQL = "
		INSERT INTO api_accesslog
		(
			APIAccessLog_ApiAuthID,
			APIAccessLog_IP	
		) VALUES (?,?)";

    public function addAccessLog($id,$ip){
        $sqlString = self::$INSERT_LOG_SQL;
        $sqlBinding = "is";
        $sqlParam = array($id,$ip);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("APIAuthProvider.addAccessLog failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }
        return $insertResponse;
    }

	
	
}