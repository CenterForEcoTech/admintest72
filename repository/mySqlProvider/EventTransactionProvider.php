<?php
include_once("Helper.php");
include_once("AuthProvider.php");
include_once("MemberProvider.php");
class EventTransactionProvider implements TransactionProvider
{
	private static $INITIAL_INSERT_STATUS = "Entered By Merchant";

	private static $GET_MEMBER_TRANSACTIONS_SQL = "
		SELECT
		  event_transaction.*,
		  organization_info.OrganizationInfo_Name,
		  (
		    SELECT 1
		    FROM invoices_matrix
		    WHERE InvoicesMatrix_EventTransactionId = EventTransaction_ID
		    AND InvoicesMatrix_IncomingQueueId > 0
		  ) AS 'isInPotOfMoney'
		FROM event_transaction
		LEFT OUTER JOIN organization_info ON event_transaction.EventTransaction_OrgID = organization_info.OrganizationInfo_ID
		WHERE EventTransaction_CustomerID = ?
		AND (0 != ? AND EventTransaction_EventID = ? OR 0 = ?)
		AND (
			0 != ?
				AND DATEDIFF(CURRENT_TIMESTAMP, EventTransaction_TimeStamp) <= ?
				OR NOT EXISTS (
                    SELECT 1
                        FROM invoices_matrix
                        WHERE InvoicesMatrix_EventTransactionId = EventTransaction_ID
                        AND InvoicesMatrix_IncomingQueueId > 0
				)
			OR 0 = ?)
		AND EventTransaction_Status NOT LIKE '%Deleted%'
		ORDER BY EventTransaction_TimeStamp DESC";

	private static $GET_ORG_TRANSACTIONS_SQL = "
		SELECT
		 	event_transaction.EventTransaction_ID,
		 	event_transaction.EventTransaction_Code,
		 	event_transaction.EventTransaction_RecipientText,
		 	event_transaction.EventTransaction_RecipientAction,
		 	event_transaction.EventTransaction_OrgID,
		 	event_transaction.EventTransaction_OrgEIN,
		 	event_transaction.EventTransaction_TimeStamp,
		 	event_transaction.EventTransaction_MerchantName,
		 	event_transaction.EventTransaction_EligibleAmount,
		 	event_transaction.EventTransaction_PercentageAmount,
		 	event_transaction.EventTransaction_GeneratedAmount,
		 	event_transaction.EventTransaction_IsFlatDonation,
		 	event_transaction.EventTransaction_FlatDonationUnits,
		 	event_transaction.EventTransaction_CustomerID,
		 	event_transaction.EventTransaction_Customer,
		 	event_transaction.EventTransaction_EventID,
		 	event_transaction.EventTransaction_InvoiceID,
		 	event_info.Event_PreferredOrgID,
		 	event_info.Event_PreferredOrgEIN,
		 	event_info.Event_ProposedByOrgID,
		 	event_info.Event_Title as EventTransaction_EventTitle,
		 	event_info.Event_Date as EventTransaction_EventDate,
		 	merchant_info.MerchantInfo_Address,
		 	merchant_info.MerchantInfo_City,
		 	merchant_info.MerchantInfo_State,
		 	merchant_info.MerchantInfo_Zip,
		 	(
		 		SELECT OrganizationInfo_Name
		 		FROM organization_info
		 		WHERE OrganizationInfo_ID = ? -- orgId
		 	) as OrganizationInfo_Name
		FROM event_info
		JOIN event_transaction ON EventTransaction_EventID=Event_ID
		JOIN merchant_info ON MerchantInfo_ID=EventTransaction_MerchantID
		WHERE Event_Status = 'ACTIVE'
		AND (
		      Event_ProposedByOrgID = ? -- orgId
			  OR event_transaction.EventTransaction_OrgID = ? -- orgId
			)
			AND Event_ID = ? -- eventId
			AND event_transaction.EventTransaction_Status != 'Deleted'
		GROUP BY event_transaction.EventTransaction_ID
		ORDER BY EventTransaction_MerchantName, EventTransaction_TimeStamp";

	private $mysqli;

	public function __construct(mysqli $mysqli){
		$this->mysqli = $mysqli;
	}

	public function getMemberDonations($memberID, $eventID = 0, $numberOfDays = 0){
		if (!is_numeric($eventID)){
			$eventID = 0;
		}
		$sqlString = self::$GET_MEMBER_TRANSACTIONS_SQL;
		$sqlBinding = "iiiiiii";
		$sqlParams = Array($memberID, $eventID, $eventID, $eventID, $numberOfDays, $numberOfDays, $numberOfDays);
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		return $this->mapToCollection($sqlResult);
	}

    private static $GET_MERCHANT_TRANSACTIONS_SQL = "
		SELECT
		 	event_transaction.EventTransaction_ID,
		 	event_transaction.EventTransaction_Code,
		 	event_transaction.EventTransaction_RecipientText,
		 	event_transaction.EventTransaction_RecipientAction,
		 	event_transaction.EventTransaction_OrgID,
		 	event_transaction.EventTransaction_OrgEIN,
		 	event_transaction.EventTransaction_TimeStamp,
		 	event_transaction.EventTransaction_MerchantName,
		 	event_transaction.EventTransaction_EligibleAmount,
		 	event_transaction.EventTransaction_PercentageAmount,
		 	event_transaction.EventTransaction_IsFlatDonation,
		 	event_transaction.EventTransaction_FlatDonationUnits,
		 	event_transaction.EventTransaction_GeneratedAmount,
		 	event_transaction.EventTransaction_CustomerID,
		 	event_transaction.EventTransaction_Customer,
		 	event_transaction.EventTransaction_EventID,
		 	event_info.Event_ProposedByOrgId,
		 	event_transaction.EventTransaction_EventTitle,
		 	event_transaction.EventTransaction_EventDate,
		 	event_transaction.EventTransaction_InvoiceID,
		 	(
		 		SELECT OrganizationInfo_Name
		 		FROM organization_info
		 		WHERE OrganizationInfo_ID = EventTransaction_OrgID
		 	) as OrganizationInfo_Name
		FROM event_transaction
		LEFT JOIN event_info ON EventTransaction_EventID=Event_ID
		WHERE event_transaction.EventTransaction_MerchantID = ?
			AND (0 = ? AND EventTransaction_EventID = ? OR Event_ID = ? AND Event_Status = 'ACTIVE')
			AND event_transaction.EventTransaction_Status != 'Deleted'
			AND (0 = ? OR event_transaction.EventTransaction_ID = ?)
			AND (0 = ? OR event_transaction.EventTransaction_InvoiceID = 0 )
		GROUP BY event_transaction.EventTransaction_ID
		ORDER BY EventTransaction_MerchantName, EventTransaction_TimeStamp";

    /**
     * @param $merchantID
     * @param $eventID (event to limit list, or zero to get all events)
     * @param bool $uninvoiced (true to return only uninvoiced transactions; default = false)
     * @param int $transactionID (specific transaction, or zero to get all transactions)
     * @return ___PHPSTORM_HELPERS\object
     */
    public function getMerchantTransactions($merchantID, $eventID, $uninvoiced = false, $transactionID = 0){
		$collection = array();
		$eventTransactionInfo = (object) array(
			"merchantID" => $merchantID,
			"eventID" => $eventID,
			"totalSales" => 0,
			"totalBenefit" => 0,
            "uninvoicedItemCount" => 0,
			"transactions" => $collection
		);
		if (!is_numeric($eventID)){
			$eventID = 0;
		}
		$sqlString = self::$GET_MERCHANT_TRANSACTIONS_SQL;
		$sqlBinding = "iiiiiii";
		$sqlParams = Array(
            $merchantID,
            $eventID,
            $eventID,
            $eventID,
            $transactionID,
            $transactionID,
            ($uninvoiced) ? 1 : 0
        );
		$sqlResults = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);

		foreach($sqlResults as $row){
			$record = $this->mapToRecord($row);
            /* @var $record EventTransactionRecord */
			$eventTransactionInfo->organizationName = $record->orgName;
			$eventTransactionInfo->totalSales += $record->eligibleAmount;
			$eventTransactionInfo->totalBenefit += $record->generatedAmount;
            if (!$record->invoiceId){
                $eventTransactionInfo->uninvoicedItemCount++;
            }
			$collection[] = $record;
		}
		$eventTransactionInfo->transactions = $collection;
		return $eventTransactionInfo;
	}

    public function getMerchantTransaction($merchantId, $eventID, EventTransactionRequest $request){
        if ($this->isValid($request)){
            $results = $this->getMerchantTransactions($merchantId, $eventID, true, $request->id);
            if (count($results->transactions)){
                return $results->transactions[0];
            }
            return null;
        }
        return null;
    }

    private static $GET_INVOICE_TRANSACTIONS_SQL = "
		SELECT
		 	event_transaction.EventTransaction_ID,
		 	event_transaction.EventTransaction_Code,
		 	event_transaction.EventTransaction_RecipientText,
		 	event_transaction.EventTransaction_RecipientAction,
		 	event_transaction.EventTransaction_OrgID,
		 	event_transaction.EventTransaction_OrgEIN,
		 	event_transaction.EventTransaction_TimeStamp,
		 	event_transaction.EventTransaction_MerchantName,
		 	event_transaction.EventTransaction_EligibleAmount,
		 	event_transaction.EventTransaction_PercentageAmount,
		 	event_transaction.EventTransaction_GeneratedAmount,
		 	event_transaction.EventTransaction_IsFlatDonation,
		 	event_transaction.EventTransaction_FlatDonationUnits,
		 	event_transaction.EventTransaction_CustomerID,
		 	event_transaction.EventTransaction_Customer,
		 	event_transaction.EventTransaction_EventID,
		 	event_transaction.EventTransaction_EventTitle,
		 	event_transaction.EventTransaction_EventDate,
		 	event_info.Event_ProposedByOrgID,
		 	event_transaction.EventTransaction_InvoiceID,
		 	(
		 		SELECT OrganizationInfo_Name
		 		FROM organization_info
		 		WHERE OrganizationInfo_ID = EventTransaction_OrgID
		 	) as OrganizationInfo_Name
		FROM event_transaction
		LEFT JOIN event_info ON EventTransaction_EventID=Event_ID
		WHERE (EventTransaction_EventID = 0 OR Event_Status = 'ACTIVE')
			AND event_transaction.EventTransaction_Status != 'Deleted'
			AND event_transaction.EventTransaction_InvoiceID = ?
		ORDER BY EventTransaction_MerchantName, EventTransaction_TimeStamp";

    /**
     * Implements TransactionProvider->getInvoiceTransactions($invoiceId). Returns an array of EventTransactionRecord.
     * @param $invoiceId
     * @return array|EventTransactionRecord
     */
    public function getInvoiceTransactions($invoiceId){
        $collection = array();
        $sqlString = self::$GET_INVOICE_TRANSACTIONS_SQL;
        $sqlBinding = "i";
        $sqlParams = array($invoiceId);
        $response = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        if ($response->success){
            $results = $response->resultArray;
            foreach($results as $row){
                $record = $this->mapToRecord($row);
                $collection[] = $record;
            }
        } else {
            DbAdminLogger::log_message($this->mysqli, "EventTransactionProvider.php getInvoiceTransactions", $response->error);
        }
        return $collection;
    }

	public function getOrganizationDonations($orgId, $eventId){
		$collection = array();
		$eventDonationInfo = (object) array(
			"organizationName" => "",
			"proposedByThisOrg" => false,
			"totalToThisOrg" => 0,
			"totalToOtherOrg" => 0,
			"totalNotAssigned" => 0,
			"eventTitle" => "",
			"eventDate" => "",
			"transactions" => $collection);
		// must specify the orgID
		if (!is_numeric($orgId) || $orgId < 1 ){
			return $eventDonationInfo; // empty array
		}
		// must specify the eventID
		if (!is_numeric($eventId) || $eventId < 1){
			return $eventDonationInfo; // empty array
		}

		$sqlString = self::$GET_ORG_TRANSACTIONS_SQL;
		$sqlBinding = "iiii";
		$sqlParams = Array($orgId, $orgId, $orgId, $eventId);
		$sqlResults = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		foreach($sqlResults as $row){
			$record = $this->mapToExtendedRecord($row);
			$eventDonationInfo->eventTitle = $record->eventTitle;
			$eventDonationInfo->eventDate = $record->eventDate;
			if ($row["Event_PreferredOrgID"] == $orgId ||
				$row["Event_ProposedByOrgID"] == $orgId){
				$eventDonationInfo->proposedByThisOrg = true;
			}
            if ($record->recipientNotAssigned()){
                $eventDonationInfo->totalNotAssigned += $record->generatedAmount;
            } else if ($record->donationsNotFor($orgId)){
				$eventDonationInfo->totalToOtherOrg += $record->generatedAmount;
			} else {
				$eventDonationInfo->organizationName = $record->orgName;
				$eventDonationInfo->totalToThisOrg += $record->generatedAmount;
				$collection[] = $record;
			}
		}
		$eventDonationInfo->transactions = $collection;
		return $eventDonationInfo;
	}

	private function mapToCollection($query_results) {
		$collection = array();
		foreach ($query_results as $row){
			$collection[] = $this->mapToRecord($row);
		}
		return $collection;
	}

	private function mapToRecord($datarow) {
		$record = new EventTransactionRecord($datarow["EventTransaction_ID"], $datarow["EventTransaction_Code"]);
		return $this->mapDataToRecord($datarow, $record);
	}

	private function mapToExtendedRecord($datarow){
		$record = new ExtendedEventTransactionRecord($datarow["EventTransaction_ID"], $datarow["EventTransaction_Code"]);
		$record = $this->mapDataToRecord($datarow, $record);
		$record->address = $datarow["MerchantInfo_Address"];
		$record->city = $datarow["MerchantInfo_City"];
		$record->state = $datarow["MerchantInfo_State"];
		$record->zip = $datarow["MerchantInfo_Zip"];
		return $record;
	}

	private function mapDataToRecord($datarow, EventTransactionRecord $record){
		$record->recipientText = $datarow["EventTransaction_RecipientText"];
		$record->recipientAction = $datarow["EventTransaction_RecipientAction"];
		$record->orgID = $datarow["EventTransaction_OrgID"];
		$record->orgEin = $datarow["EventTransaction_OrgEIN"];
		$record->orgName = $datarow["OrganizationInfo_Name"];
		$record->eventId = $datarow["EventTransaction_EventID"];
        $record->featuredCauseOrgId = $datarow["Event_ProposedByOrgID"];
		$record->eventDate = $datarow["EventTransaction_EventDate"];
		$record->eventTitle = $datarow["EventTransaction_EventTitle"];
		$record->eligibleAmount = $datarow["EventTransaction_EligibleAmount"];
		$record->percentageAmount = $datarow["EventTransaction_PercentageAmount"];
        $record->isFlatDonation = $datarow["EventTransaction_IsFlatDonation"];
        $record->flatDonationUnits = $datarow["EventTransaction_FlatDonationUnits"];
		$record->generatedAmount = $datarow["EventTransaction_GeneratedAmount"];
		$record->timestamp = $datarow["EventTransaction_TimeStamp"];
		$record->merchantName = $datarow["EventTransaction_MerchantName"];
		$record->setCustomer($datarow["EventTransaction_CustomerID"], $datarow["EventTransaction_Customer"]);
		$record->customerId = $datarow["EventTransaction_CustomerID"];
		$record->invoiceId = $datarow["EventTransaction_InvoiceID"];
		$record->transactionId = $datarow["EventTransaction_ID"];
        $record->isInPotOfMoney = $datarow["isInPotOfMoney"];
		return $record;
	}

    private static $SET_RECIPIENT_SQL = "
			UPDATE event_transaction
			SET EventTransaction_OrgEIN = ?,
				EventTransaction_OrgID = ?,
				EventTransaction_RecipientAction = 'Customer Selected'
			WHERE EventTransaction_ID = ?
			AND EventTransaction_CustomerID = ?";

	public function chooseRecipient(EventTransactionRequest $req, CharityLookup $charityLookup){
        $mustHaveCustomer = true;
        $response = new ExecuteResponseRecord();
		if ($this->isValid($req, $mustHaveCustomer)){
			if (!is_numeric($req->orgId)){
				$req->orgId = 0;
			}
            if ($req->orgId == 0 && strlen(trim($req->orgEin)) != 9){
                return $response;
            }
            if ($req->orgId == 0){
                $memberProvider = new OrganizationMemberProvider($this->mysqli);
                $req->orgId = $memberProvider->checkAndAdd($req->orgEin, $charityLookup);
            }
			$sqlString = self::$SET_RECIPIENT_SQL;
			$sqlBindings = "siii";
			$sqlParams = Array($req->orgEin, $req->orgId, $req->id, $req->customerId);
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $response);
		}
		return $response;
	}

    private static $VALIDATE_TRANSCODE_SQL = "
		SELECT 1
		FROM event_transaction
		WHERE EventTransaction_ID = ?
		AND EventTransaction_Code = ?
		AND (EventTransaction_CustomerID = ? OR 0 = ?)";

	public function isValid(EventTransactionRequest $req, $mustHaveCustomer = false){
		if (strlen($req->transCode) < 8){
			return false;
		}
		if (!is_numeric($req->id) || ((int)$req->id) <= 0){
			return false;
		}
        if ($mustHaveCustomer){
            if (!is_numeric($req->customerId) || ((int)$req->customerId) <= 0){
                return false;
            }
        }
		$sqlString = self::$VALIDATE_TRANSCODE_SQL;
		$sqlBinding = "isii";
		$sqlParams = array(
            $req->id,
            $req->getPrivateTranscode(),
            $req->customerId,
            ($mustHaveCustomer) ? 1 : 0
        );
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
		if (count($sqlResult)){
			return true;
		}
		return false;
	}

    private static $DELETE_MERCHANT_TRANSACTION_SQL = "
		UPDATE event_transaction
		SET EventTransaction_Status='Deleted',
			EventTransaction_UpdatedDate = CURRENT_TIMESTAMP,
			EventTransaction_UpdatedByMemberID = ?
		WHERE EventTransaction_ID = ?
		AND EventTransaction_InvoiceID = 0";

	public function deleteMerchantTransaction(EventTransactionRecord $eventTransactionRecord, $deletedBy, $deletedByUsername, $deletedByPassword, $ip){
		// TODO: this is bad coding; we should not assume the AuthProvider uses the same database; figure out how to inject this?
		$authProvider = new AuthProvider($this->mysqli);

		$authAttempt = $authProvider->authenticate($deletedByUsername, $deletedByPassword, $ip);
		if ($authAttempt->message){
			return "Incorrect Password.";
		} else {
			$sqlString = self::$DELETE_MERCHANT_TRANSACTION_SQL;
			$sqlBindings = "ii";
			$sqlParams = array($deletedBy, $eventTransactionRecord->id);
            $executeResponse = new ExecuteResponseRecord();
			$sqlResult = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
			if ($sqlResult){
				return ""; // success is no error message
			} else {
				return "Unable to remove transaction.";
			}
		}
		return "";
	}

    private static $ADD_MERCHANT_TRANSACTION_SQL = "
		INSERT INTO event_transaction (

			EventTransaction_Code,
			EventTransaction_MerchantID,
			EventTransaction_MerchantName,
			EventTransaction_RecipientAction,
			EventTransaction_RecipientText,

			EventTransaction_OrgID,
			EventTransaction_OrgEIN,
			EventTransaction_Customer,
			EventTransaction_CustomerID,
			EventTransaction_EligibleAmount,

			EventTransaction_PercentageAmount,
			EventTransaction_GeneratedAmount,
			EventTransaction_EventID,
			EventTransaction_EventTitle,
			EventTransaction_EventDate,

			EventTransaction_EventMatchID,
			EventTransaction_Status,
			EventTransaction_EnteredByID,
			EventTransaction_IP,

			EventTransaction_IsFlatDonation,
			EventTransaction_FlatDonationUnits,

			EventTransaction_TimeStamp
		)
		SELECT
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,
			?,?,
			CURRENT_TIMESTAMP
		FROM event_info
		WHERE Event_ID = ?
		AND Event_Status = 'ACTIVE'
		OR 0 = ? LIMIT 1";

	public function addMerchantTransaction(EventTransactionRecord $eventTransactionRecord, $enteredBy, $validAreaCodes){
		$response = new AddEventTransactionResponse();
		$response->eventTransactionRecord = $eventTransactionRecord;
		$response->enteredBy = $enteredBy;
		if (is_array($validAreaCodes)){
			$response->validAreaCodes = $validAreaCodes;
		} else {
			$response->validAreaCodes = explode(",", str_replace(" ", "", $validAreaCodes));
		}

		if (!isset($authProvider)){
			$authProvider = new AuthProvider($this->mysqli);
		}
		$parsedUsername = AuthProvider::parseUsername(trim($eventTransactionRecord->customer));
		if (!($parsedUsername->message) || $parsedUsername->isEmpty){
			// customerID is the "ct___" value that was attempted
			$eventTransactionRecord->customerId = (is_numeric($parsedUsername->memberId)) ? $parsedUsername->memberId : 0;
			$eventTransactionRecord->parsedEmail = $parsedUsername->memberEmail;
			$eventTransactionRecord->parsedPhone = $parsedUsername->memberPhone;
			// look up member
			$memberRecord = $authProvider->getMember($eventTransactionRecord->customer);
			// verifiedMemberID is the actual member id found in the database
			if (count($memberRecord)){
				$eventTransactionRecord->verifiedMemberId = $memberRecord["MemberID"];
			}
		} else {
			$response->addValidationError("customer", $parsedUsername->message);
		}

		if ($response->isValid()){
			$sqlString = self::$ADD_MERCHANT_TRANSACTION_SQL;
			$sqlBindings = "sisss";
			$sqlBindings .= "issid";
			$sqlBindings .= "ddiss";
			$sqlBindings .= "isisiiii";
			$sqlParams = Array(
				$eventTransactionRecord->getPrivateTranscode(),
				$eventTransactionRecord->merchantId,
				$eventTransactionRecord->merchantName,
				$eventTransactionRecord->recipientAction,
				$eventTransactionRecord->recipientText,

				$eventTransactionRecord->orgID,
				$eventTransactionRecord->orgEin,
				$eventTransactionRecord->customer,
				$eventTransactionRecord->verifiedMemberId,
				$eventTransactionRecord->eligibleAmount,

				$eventTransactionRecord->percentageAmount,
				$eventTransactionRecord->generatedAmount,
				$eventTransactionRecord->eventId,
				$eventTransactionRecord->eventTitle,
				$eventTransactionRecord->eventDate,

				(is_numeric($eventTransactionRecord->eventMatchId)) ? $eventTransactionRecord->eventMatchId : 0,
				self::$INITIAL_INSERT_STATUS,
				$enteredBy,
                $eventTransactionRecord->ip,

                $eventTransactionRecord->isFlatDonation ? 1 : 0,
                $eventTransactionRecord->flatDonationUnits,

                $eventTransactionRecord->eventId,
                $eventTransactionRecord->eventId
			);
            $executeResponse = new ExecuteResponseRecord();
			$sqlResult = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
			$eventTransactionRecord->setId($executeResponse->insertedId);
			$response->eventTransactionRecord = $eventTransactionRecord;
		}
		return $response;
	}

    private static $SET_CUSTOMER_ID_SQL = "
        UPDATE event_transaction
        SET EventTransaction_CustomerID = ?
        WHERE EventTransaction_ID = ?";

    public function setCustomerId($transactionId, $customerId){
        // HACK: this should try to be more secure than this
        $sqlString = self::$SET_CUSTOMER_ID_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $customerId,
            $transactionId
        );
        $response = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        return $response;
    }

    public function checkBillingInfo($MerchantID){
		$sqlBillingInfo = "
			SELECT merchant_billinginfo.*, merchant_info.* 
			FROM merchant_info 
			LEFT JOIN merchant_billinginfo ON MerchantBillingInfo_MerchantID=MerchantInfo_ID 
			WHERE MerchantInfo_ID = ?";
		$sqlBinding = "i";
		$sqlParams = Array($MerchantID);
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlBillingInfo, $sqlBinding, $sqlParams);
		return $sqlResult;
	}

	// TODO: eliminate $BillingType: we should not be constructing a database columname using an input parameter without checking it in any case.
	public function updateBillingId($MerchantID,$Billing_ID,$BillingType){
        $SQLMerchant_Update = "INSERT INTO merchant_billinginfo (MerchantBillingInfo_MerchantID, MerchantBillingInfo_".$BillingType."ID) VALUES (?,?)";
        $insertResult = MySqliHelper::execute($this->mysqli, $SQLMerchant_Update, "is", Array($MerchantID,$Billing_ID));
		return $insertResult;
	}

    public function hasTransactions($eventID){
        $sqlString = "SELECT 1 FROM event_transaction WHERE EventTransaction_EventID = ? AND EventTransaction_Status NOT LIKE '%Deleted%' LIMIT 1";
        $sqlBinding = "i";
        $sqlParam = array($eventID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        return (count($result) > 0);
    }

}

class EventTransactionRequest{

	private static $CODE_PREFIX_LENGTH = 5;
	private static $CODE_SUFFIX_START = -2;
	public $id = 0;
	public $transCode;
	public $orgId = 0;
	public $orgEin;
    public $customerId = 0;
    public $merchantId = 0;
    public $merchantName;
    public $generatedAmount;

    public function getTransactionId(){
        $frontStripped = substr($this->transCode, self::$CODE_PREFIX_LENGTH);
        $transactionId = substr($frontStripped, 0, strlen($frontStripped) - abs(self::$CODE_SUFFIX_START));
        if (is_numeric($transactionId)){
            return $transactionId;
        }
        return null;
    }

	public function getPrivateTranscode(){
        if (strlen($this->transCode) <= 7){
            return null;
        }
        if ($this->getTransactionId() == null){
            return null;
        }
		return substr($this->transCode,0,self::$CODE_PREFIX_LENGTH)."_".substr($this->transCode,self::$CODE_SUFFIX_START);
	}
}

class ValidationError{
	public $property;
	public $message;
	public function __construct($propertyValue, $messageValue){
		$this->property = $propertyValue;
		$this->message = $messageValue;
	}
}

class AddEventTransactionResponse{
    /* @var $eventTransactionRecord EventTransactionRecord */
	public $eventTransactionRecord;
	public $enteredBy = 0;
	public $validAreaCodes = array();
	public $validationErrors = array();

	public function addValidationError($propertyName, $message){
		$this->validationErrors[] = new ValidationError($propertyName, $message);
	}

	public function isValid(){
		if (!is_numeric($this->enteredBy)){
			$this->addValidationError("enteredBy", "Must specify a logged in member.");
		}
		$record = $this->eventTransactionRecord;

        if ($record->isFlatDonation){
            if (!(is_numeric($record->flatDonationUnits) && $record->flatDonationUnits > 0)){
                $this->addValidationError("flat_units", "Please enter Number of Units.");
            }
        } else {
            if (!(is_numeric($record->eligibleAmount) && $record->eligibleAmount > 0)){
                $this->addValidationError("eligibleAmount", "Please enter Eligible Amount.");
            }
        }
		if ($record->recipientText){
			// if customer choice, customer must be specified
			if (EventTransactionRecord::isCharityAssigned($record) && !EventTransactionRecord::isCustomerAssigned($record)){
				$this->addValidationError("customer", "Please enter a valid email, phone, or customer id.");
			}
		} else {
			// Recipient is required. If this occurs, there is a programming error or a form hack.
			$this->addValidationError("recipientText", "Field is required.");
		}
		if ($record->parsedPhone){
			if (strlen($record->parsedPhone) != 10){
				$this->addValidationError("customer", "Phone numbers must be exactly 10 digits.");
			} else {
				$areacode = substr($record->parsedPhone,0,3);
				if (!in_array($areacode, $this->validAreaCodes)){
					$this->addValidationError("customer", "Area code is invalid.");
				}
			}
		}
		if ($record->customerId && !$record->verifiedMemberId){
			// we don't verify email or phone here because this may be an unregistered user, which is valid.
			$this->addValidationError("customer", "Please enter a valid email, phone, or customer id.");
		}

		return count($this->validationErrors) == 0;
	}
}

class ExtendedEventTransactionRecord extends EventTransactionRecord{
	public $address;
	public $city;
	public $state;
	public $zip;

}
?>