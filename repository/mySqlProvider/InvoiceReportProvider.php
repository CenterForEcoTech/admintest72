<?php

/**
 * The main purposes of this provider is to do the read-only operations on the invoices (and related) tables.
 */
class InvoiceReportProvider{

    /**
     * @var mysqli
     */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    protected static $SELECT_COLUMNS = "
        SELECT
			i.Invoices_ID as id,
			i.Invoices_MerchantID as merchant_id,
			i.Invoices_DonationAmount as donation_amount,
			i.Invoices_FeeAmount as fee_amount,
			i.Invoices_Amount as amount,
			i.Invoices_AmountPaid as amount_paid,
			i.Invoices_Status as status,
			DATE(i.Invoices_Timestamp) as invoice_date,
			DATE_ADD(DATE(i.Invoices_Timestamp), INTERVAL ? WEEK) as due_date";

    private static $FROM_TABLE_CLAUSE = "
        FROM invoices i
        WHERE 1 = 1";

    private static $BASIC_QUERY_BINDING = "i";

    private static $FILTER_ON_MERCHANT = "
        AND i.Invoices_MerchantID = ?";

    public function get(InvoiceReportCriteria $criteria){
        $result = new InvoiceReportData();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();
        $result->hasOverdueInvoices = $this->hasOverdueInvoices($criteria);

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $paginationFilterBinding = self::$BASIC_QUERY_BINDING;
        $countFilterBinding = "";
        $paginationFilterParams = array(
            $criteria->weeksUntilDue
        );
        $countFilterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->merchantId && is_numeric($criteria->merchantId)){
            $filterString .= self::$FILTER_ON_MERCHANT;
            $paginationFilterBinding .= "i";
            $paginationFilterParams[] = $criteria->merchantId;
            $countFilterBinding .= "i";
            $countFilterParams[] = $criteria->merchantId;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $countFilterBinding, $countFilterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $paginationFilterBinding, $paginationFilterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("InvoiceReportProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("InvoiceReportProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY i.Invoices_Timestamp desc";

    private function getOrderByClause(EventByLocationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    // TODO: implement if we ever decide it's important
                }
            }
            if (count($orderByStrings)){
                $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
            }
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        $record = new InvoiceReportRow();
        $record->id = $dataRow->id;
        $record->merchantId = $dataRow->merchant_id;
        $record->donationAmount = $dataRow->donation_amount;
        $record->feeAmount = $dataRow->fee_amount;
        $record->totalAmount = $dataRow->amount;
        $record->amountPaid = $dataRow->amount_paid;
        $record->status = $dataRow->status;
        $record->invoiceDate = $dataRow->invoice_date;
        $record->dueDate = $dataRow->due_date;
        return $record;
    }

    private static $HAS_OVERDUE_INVOICES_SQL = "
        SELECT 1
        FROM invoices
        WHERE Invoices_MerchantID = ?
        AND Invoices_Status NOT IN ('Voided', 'Creating', 'Funds Received')
        AND Invoices_Amount > Invoices_AmountPaid
        AND DATE_ADD(DATE(Invoices_Timestamp), INTERVAL ? WEEK) < CURRENT_DATE
        LIMIT 1";

    public function hasOverdueInvoices(InvoiceReportCriteria $criteria){
        $sqlString = self::$HAS_OVERDUE_INVOICES_SQL;
        $sqlBinding = "ii";
        $sqlParams = array( $criteria->merchantId, $criteria->weeksUntilDue );
        $response = MySqliHelper::getResultWithLogging("InvoiceReportProvider.hasOverdueInvoices", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $response->success && count($response->resultArray);
    }

    private static $HAS_UNPAID_INVOICES_SQL = "
        SELECT 1
        FROM invoices
        WHERE Invoices_MerchantID = ?
        AND Invoices_Status NOT IN ('Voided', 'Creating', 'Funds Received')
        AND Invoices_Amount > Invoices_AmountPaid
        LIMIT 1";

    public function hasUnpaidInvoices(InvoiceReportCriteria $criteria){
        $sqlString = self::$HAS_UNPAID_INVOICES_SQL;
        $sqlBinding = "i";
        $sqlParams = array( $criteria->merchantId );
        $response = MySqliHelper::getResultWithLogging("InvoiceReportProvider.hasUnpaidInvoices", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $response->success && count($response->resultArray);
    }

    private static $HAS_INVOICES_SQL = "
        SELECT 1
        FROM invoices
        WHERE Invoices_MerchantID = ?
        AND Invoices_Status NOT IN ('Voided', 'Creating')
        LIMIT 1";

    public function hasInvoices(InvoiceReportCriteria $criteria){
        $sqlString = self::$HAS_INVOICES_SQL;
        $sqlBinding = "i";
        $sqlParams = array( $criteria->merchantId );
        $response = MySqliHelper::getResultWithLogging("InvoiceReportProvider.hasInvoices", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        return $response->success && count($response->resultArray);
    }
}