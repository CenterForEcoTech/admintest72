<?php
class AdminUsersProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
			au.AdminUsers_ID as id,
			au.AdminUsers_UserEmail as userEmail,
			au.AdminUsers_UserName as userName,
			au.AdminUsers_UserPassword as userPassword,
			au.AdminUsers_FirstName as firstName,
			au.AdminUsers_LastName as lastName,
			CONCAT(au.AdminUsers_FirstName,' ',au.AdminUsers_LastName) as fullName,
			au.AdminUsers_SecurityGroupID as securityGroupId,
			au.AdminUsers_SecurityGroups as securityGroups,
			au.AdminUsers_Status as status,
			au.AdminUsers_AddedByAdminID as addedByAdminId,
			au.AdminUsers_DisplayOrderID as displayOrderId";

    private static $FROM_TABLE_CLAUSE = "
        FROM admin_users as au
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND au.AdminUsers_DisplayOrderID >= 1";
		
    private static $FILTER_ON_ID = "
        AND au.AdminUsers_ID = ?";

    private static $FILTER_ON_SECURITYGROUP = "
        AND au.AdminUsers_SecurityGroupID = ?";
		
    private static $FILTER_ON_STATUS = "
        AND au.AdminUsers_Status = ?";
		
    public function get($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_DISPLAYORDERID; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->securityGroupId){
            $filterString .= self::$FILTER_ON_SECURITYGROUP;
            $filterBinding .= "s";
            $filterParams[] = $criteria->securityGroupId;
		}
		if ($criteria->status){
            $filterString .= self::$FILTER_ON_STATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->status;
		}
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("AdminUsersProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("AdminUsersProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY au.AdminUsers_DisplayOrderID ASC, au.AdminUsers_FirstName ASC";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY au.AdminUsers_ID ".$sortOrder;
                    break;
                case "securityGroupId":
                    $orderBy = " ORDER BY au.AdminUsers_SecurityGroupID ".$sortOrder;
                    break;
				case "status":
					$orderBy = " ORDER BY au.AdminUsers_Status ".$sortOrder;
					break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "au.AdminUsers_ID ".$sortOrder;
                        break;
                    case "securityGroupId":
                        $orderByStrings[] = "au.AdminUsers_SecurityGroupID ".$sortOrder;
                        break;
					case "status":
						$orderByStrings[] = "au.AdminUsers_Status ".$sortOrder;
						break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return AdminUserRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL = "
		INSERT INTO admin_users
		(
			AdminUsers_UserName,
			AdminUsers_UserEmail,
			AdminUsers_FirstName,
			AdminUsers_LastName,

			AdminUsers_SecurityGroupID,
			AdminUsers_SecurityGroups,
			AdminUsers_Status,
			AdminUsers_AddedByAdminID,
			AdminUsers_DisplayOrderID
		) VALUES (
			?,?,?,?,
			?,?,?,?,?
		)";

    public function add(AdminUserRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
		$securityGroups = (is_array($record->securityGroups) ? implode(",",$record->securityGroups) : $record->securityGroups);
        $sqlBinding = "ssssissii";
        $sqlParam = array(
			$record->userEmail,
			$record->userEmail,
			$record->firstName,
			$record->lastName,

			$record->securityGroupId,
			$securityGroups,
			$record->status,
			$adminUserId,
			$record->displayOrderId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("AdminUsersProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_SQL = "
        UPDATE admin_users
        SET
			AdminUsers_UserName = ?,
			AdminUsers_UserEmail = ?,
			AdminUsers_FirstName = ?,
			AdminUsers_LastName = ?,

			AdminUsers_SecurityGroupID = ?,
			AdminUsers_SecurityGroups = ?,
			AdminUsers_Status = ?
			
		WHERE AdminUsers_ID = ?";

    public function update(AdminUserRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
			$securityGroups = (is_array($record->securityGroups) ? implode(",",$record->securityGroups) : $record->securityGroups);
            $sqlBinding = "ssssissi";
            $sqlParams = array(
				$record->userEmail,
				$record->userEmail,
				$record->firstName,
				$record->lastName,

				$record->securityGroupId,
				$securityGroups,
				$record->status,
				
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("AdminUsersProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
			
        }
        return $updateResponse;
    }

	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	admin_users
        SET		AdminUsers_DisplayOrderID = ?
		WHERE	AdminUsers_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("AdminUsersProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }	
}