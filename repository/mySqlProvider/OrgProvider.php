<?php
require_once("Helper.php");
class OrgProvider
{
			
	private $mysqli;

	public function __construct($mysqli){
		$this->mysqli = $mysqli;
	}

    private static $SQL_AUTOCOMPLETE = "
        SELECT
            OrganizationInfo_ID as OrgID,
            OrganizationInfo_Address as Address,
            OrganizationInfo_City as City,
            OrganizationInfo_State as State,
            OrganizationInfo_Zip as Zip,
            OrganizationInfo_Name as OrgName,
            OrganizationInfo_EIN as OrgEIN,
            OrganizationInfo_MemberID,
            IF(LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) > 0, 1, 0) AS IsFiscallySponsored
        FROM organization_info
        WHERE (
            OrganizationInfo_Name LIKE ?
            OR OrganizationInfo_EIN LIKE ?
            OR OrganizationInfo_FiscalSponsorEin LIKE ?
        )
        LIMIT ?";

    public function getAutoCompleteResults($searchString, $limit = 10){
        if (!is_numeric($limit)){
            $limit = 10;
        }
        $nameSearchString = "%".$searchString."%";
        $einSearchString = $searchString."%";
        $sqlString = self::$SQL_AUTOCOMPLETE;
        $sqlBindings = "sssi";
        $sqlParams = array($nameSearchString, $einSearchString, $einSearchString, $limit);
        return MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
    }

    private static $SQL_PARENT_CONTACT_BY_EIN = "
			SELECT
				OrganizationInfo_ID,
				OrganizationInfo_EIN,
				OrganizationInfo_NCESSchoolID,
				OrganizationInfo_ContactEmail,
                IF(LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) > 0, 1, 0) AS IsFiscallySponsored,
                OrganizationInfo_MemberID,
                OrganizationInfo_Name,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid
			FROM organization_info
			WHERE OrganizationInfo_EIN IS NOT NULL
			AND OrganizationInfo_EIN = ?
			AND IF(LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) > 0, 1, 0)  = 0";

    /**
     * Returns the existing OrgRecord (if any) for a nonprofit with this matching EIN. This lookup specifically
     * excludes fiscally sponsored charities. To include Fiscally Sponsored Charities, use the getAutoCompleteResults.
     * @param $ein
     * @return OrgRecord
     */
    public function getByEIN($ein){
        $response = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, self::$SQL_PARENT_CONTACT_BY_EIN, "s", array($ein), $response);
		return $this->mapToRecord($sqlResult);
	}

    private static $SQL_PARENT_CONTACT_BY_ORGID = "
			SELECT
				OrganizationInfo_ID,
				OrganizationInfo_EIN,
				OrganizationInfo_NCESSchoolID,
				OrganizationInfo_ContactEmail,
                IF(LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) > 0, 1, 0) AS IsFiscallySponsored,
                OrganizationInfo_MemberID,
                OrganizationInfo_Name,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid
			FROM organization_info
			WHERE OrganizationInfo_ID = ?";
    /**
     * Returns the existing OrgRecord (if any) for a nonprofit with this matching OrgID.
     * @param $orgID
     * @return OrgRecord
     */
    public function getByOrgID($orgID){
		$sqlResult = MySqliHelper::get_result($this->mysqli, self::$SQL_PARENT_CONTACT_BY_ORGID, "i", Array($orgID));
		return $this->mapToRecord($sqlResult);
	}
	
	
    /**
     * Returns the matching orgRecord. If the $ein value is specified, it is assumed that it refers to a charity's own
     * ein and not that of its fiscal sponsor.
     * @param $ein
     * @param $name
     * @param $city
     * @param $state
     * @return null|OrgRecord
     */
    public function getMatch($ein, $name, $city, $state){
        $orgRecord = ($ein) ? $this->getByEIN($ein) : $this->getByNameAndCityAndState($name, $city, $state);
        if ($orgRecord->memberId > 0){
            // exists
            return $orgRecord;
        }
        // doesn't exist; return NULL
        return NULL;
    }

    private static $SQL_CONTACT_BY_NAME_CITY_STATE = "
			SELECT
				OrganizationInfo_ID,
				OrganizationInfo_EIN,
				OrganizationInfo_NCESSchoolID,
				OrganizationInfo_ContactEmail,
				OrganizationInfo_Name,
                IF(LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) > 0, 1, 0) AS IsFiscallySponsored,
                OrganizationInfo_MemberID,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid
			FROM organization_info
			WHERE OrganizationInfo_MemberID > 0
 			AND (
                    UPPER(OrganizationInfo_Name) = ?
                    OR
                      UPPER(OrganizationInfo_OfficialName) = ?
                    AND
                      LENGTH(OrganizationInfo_DifferentNameThanFiscalSponsor) = 0
				)
			AND UPPER(OrganizationInfo_City) = ?
			AND UPPER(OrganizationInfo_State) = ?";

    /**
     * Checks to see if there is a registered organization with the specified name, city, and state
     * @param $name
     * @param $city
     * @param $state
     * @return mixed
     */
    public function getByNameAndCityAndState($name, $city, $state){
        $sqlString = self::$SQL_CONTACT_BY_NAME_CITY_STATE;
        $sqlBindings = "ssss";
        $likeName = strtoupper($name);
        $sqlParams = Array($likeName, $likeName, strtoupper($city), strtoupper($state));
		$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToRecord($sqlResult);
	}

	private function mapToRecord($sqlResult){
		$dataRecord = new OrgRecord();
		if (count($sqlResult) > 0){
			$sqlRow = $sqlResult[0];
            $dataRecord->id = $sqlRow["OrganizationInfo_ID"];
            $dataRecord->ein = $sqlRow["OrganizationInfo_EIN"];
            $dataRecord->contactEmail = $sqlRow["OrganizationInfo_ContactEmail"];
            $dataRecord->isFiscallySponsored = $sqlRow["IsFiscallySponsored"];
            $dataRecord->ncesSchoolId = $sqlRow["OrganizationInfo_NCESSchoolID"];
            $dataRecord->memberId = $sqlRow["OrganizationInfo_MemberID"];
            $dataRecord->name = $sqlRow["OrganizationInfo_Name"];
            $dataRecord->pid = $sqlRow['pid'];
		}
		return $dataRecord;
	}
}

// TODO: move to data contracts when API is defined
class OrgRecord {
	public $id;
	public $ein;
	public $contactEmail;
    public $isFiscallySponsored;
    public $memberId;
    public $name;
    public $pid;
}

?>