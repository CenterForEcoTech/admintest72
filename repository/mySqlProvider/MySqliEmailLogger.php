<?php
include_once("_getRootFolder.php");
include_once("Helper.php");
include_once($rootFolder."/repository/EmailHelper.php");

class MySqliEmailLogger implements EmailLogger{

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $INSERT_LOG_SQL = "
        INSERT INTO email_history
        (
            EmailHistory_MemberID,
            EmailHistory_From,
            EmailHistory_To,
            EmailHistory_Subject,
            EmailHistory_Message,
            EmailHistory_OptOutEmails,
            EmailHistory_Status
        )
        VALUES
        (
          ?,?,?,?,?,?,'PENDING'
        )";
    private static $INSERT_LOG_BINDINGS = "isssss";

    private static $UPDATE_LOG_SQL = "
        UPDATE email_history
        SET EmailHistory_Status = ?,
            EmailHistory_Errors = ?,
            EmailHistory_UpdatedDate = CURRENT_TIMESTAMP
        WHERE EmailHistory_ID = ?";
    private static $UPDATE_LOG_BINDINGS = "ssi";

    public function log(EmailLoggerMessage $message, EmailResponse $response = null, $isDevMode = false){
        if (empty($response)){
            // new attempt
            $memberId = $message->getMemberId();
            if (!is_numeric($memberId)){
                $memberId = 0;
            }
            $sqlString = self::$INSERT_LOG_SQL;
            $sqlBindings = self::$INSERT_LOG_BINDINGS;
            $sqlParams = array(
                $memberId,
                $message->getFromText()."",
                $message->getToText()."",
                $message->getSubject()."",
                $message->getMessage()."",
                ''
            );
            $executeResponse = new ExecuteResponseRecord();
            $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
            return $executeResponse;
        } else {
            $status = ($response->emailSent|| $isDevMode) ? "SENT" : "ERROR";
            $sqlString = self::$UPDATE_LOG_SQL;
            $sqlBindings = self::$UPDATE_LOG_BINDINGS;
            $sqlParams = array(
                $status,
                $response->getErrorForLog(),
                $response->logId
            );
            $executeResponse = new ExecuteResponseRecord();
            $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
            return $executeResponse;
        }
    }

    private static $INSERT_DEFERRED_LOG_SQL = "
        INSERT INTO email_deferred_log
        (
            EmailDeferredLog_Payload
        )
        VALUES (?)";

    public function logDeferred(EmailLoggerMessage $message){
        $sqlString = self::$INSERT_DEFERRED_LOG_SQL;
        $sqlBindings = "s";
        $sqlParams = array( json_encode($message) );
        $executeResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
        return $executeResponse;
    }

    private static $UPDATE_DEFERRED_LOG_SQL = "
        UPDATE email_deferred_log
        SET EmailDeferredLog_EmailHistoryID = ?
        WHERE EmailDeferredLog_ID = ?";

    public function logDeferredSent(EmailLoggerMessage $message, $historyId){
        $executeResponse = new ExecuteResponseRecord();
        if (is_numeric($historyId) && $historyId > 0 && is_numeric($message->getDeferredEmailLogId()) && $message->getDeferredEmailLogId() > 0){
            $sqlString = self::$UPDATE_DEFERRED_LOG_SQL;
            $sqlBindings = "ii";
            $sqlParams = array( $historyId, $message->getDeferredEmailLogId());
            $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $executeResponse);
        }
        return $executeResponse;
    }
}