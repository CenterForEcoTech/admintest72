<?php
class ResourcesProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_CALCULATIONSCOLUMNS = "
        SELECT 
			ExecForecastConversion_ID as id,
			ExecForecastConversion_DisplayOrderID as displayOrderId,
			ExecForecastConversion_FromUnit as fromUnit,
			ExecForecastConversion_Calculation as calculations,
			ExecForecastConversion_ToUnit as toUnit,
			ExecForecastConversion_UsedFor as usedFor,
			ExecForecastConversion_InfoSource as infoSource,
			ExecForecastConversion_InfoLink as infoLink,
			ExecForecastConversion_Notes as notes,
			ExecForecastConversion_NotUsed as notUsed,
			ExecForecastConversion_ValidThru as validThru,
			ExecForecastConversion_LastUpdated as lastUpdated";
	
    private static $FROM_CALCULATIONS_TABLE_CLAUSE = "
        FROM exec_forecast_conversion
        WHERE 1 = 1";

    public function getCalculations(){
		$sqlString = self::$SELECT_CALCULATIONSCOLUMNS;
        $fromTableClause = self::$FROM_CALCULATIONS_TABLE_CLAUSE;
		$filterBinding = "";
		$filterParams = array();
		
		$SqlString = $sqlString.$fromTableClause." ORDER BY ExecForecastConversion_DisplayOrderID ASC";
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $SqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result[] = (object)$row;
			}
		} else {
			trigger_error("ResourcesProvider.getCalculations failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
}