<?php
class HoursProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT 
			GBSHoursCodes_ID as id,
			GBSHoursCodes_Name as name,
			GBSHoursCodes_Description as description,
			GBSHoursCodes_NonBillable as nonbillable,
			GBSHoursCodes_Dormant as dormant,
			GBSHoursCodes_GroupID as groupId,
			GBSHoursCodes_BillingRateID as billingRateId,
			GBSHoursCodes_DisplayOrderID as displayOrderId,
			GBSHoursCodes_Department as department,
			GBSHoursCodes_Rate as rate,
			GBSHoursCodes_Budget as budget,
			GBSHoursCodes_ExcludeFromProjections as excludeFromProjections";
	
    private static $FROM_TABLE_CLAUSE = "
        FROM hr_hours_codes
        WHERE 1 = 1";

    private static $FILTER_ON_CODESGROUP_ID = "
        AND GBSHoursCodes_GroupID = ?";
		
    private static $FILTER_ON_CODESGROUP_DEPARTMENT = "
        AND GBSHoursCodes_Department = ?";
				
    public function getCodes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodes_Name";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->groupId){
			$filterString .= self::$FILTER_ON_CODESGROUP_ID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->groupId;
		}
		if ($criteria->department){
			if (!$criteria->department2){
				$filterBinding .= "s";
				$filterString .= self::$FILTER_ON_CODESGROUP_DEPARTMENT;
				$filterParams[] = $criteria->department;
			}else{
				$filterBinding .= "ss";
				$filterString .= " AND (".str_replace("AND","",self::$FILTER_ON_CODESGROUP_DEPARTMENT)." OR ".str_replace("AND","",self::$FILTER_ON_CODESGROUP_DEPARTMENT).")";
				$filterParams[] = $criteria->department;				
				$filterParams[] = $criteria->department2;				
			}
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getCodes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    private static $FROM_EMPLOYEECODE_CLAUSE = "
        FROM hr_hours_codes_matrix LEFT JOIN hr_hours_codes ON GBSHoursCodes_ID=GBSHoursCodesMatrix_CodesID
        WHERE 1=1";
		
	private static $FILTER_EMPLOYEECODE_EMPLOYEEID = "
		AND GBSHoursCodesMatrix_EmployeeID = ?";
		
    public function getEmployeeCodes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_EMPLOYEECODE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodes_DisplayOrderID";
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->employeeID){
			$filterBinding .= "i";
			$filterString .= self::$FILTER_EMPLOYEECODE_EMPLOYEEID;
			$filterParams[] = $criteria->employeeID;
		}
		if ($criteria->department){
//			$filterBinding .= "s";
//			$filterString .= " 		AND GBSHoursCodesMatrix_Department LIKE '%".$criteria->department."%'";
//			$filterParams[] = $criteria->department;
		}
		if ($criteria->notdormant){
			$filterString .= " 		AND (GBSHoursCodes_Dormant IS NULL OR GBSHoursCodes_Dormant<1)";
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
				//print_pre($filterParams);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getCodes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getCodes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    // add
    private static $INSERT_CODES_SQL = "
		INSERT INTO hr_hours_codes
		(
			GBSHoursCodes_Name,
			GBSHoursCodes_Description,
			GBSHoursCodes_NonBillable,
			GBSHoursCodes_Dormant,
			GBSHoursCodes_GroupID,
			GBSHoursCodes_BillingRateID,
			GBSHoursCodes_Rate,
			GBSHoursCodes_DisplayOrderID,
			GBSHoursCodes_AddedByUserID,
			GBSHoursCodes_Department
		) VALUES (
			?,?,?,?,?,?,?,?,?,?
		)";

    public function addCodes($record, $adminUserId){
        $sqlString = self::$INSERT_CODES_SQL;
        $sqlBinding = "ssiiiidiss";
		$rate = ($record->rate ? : 0.00);
		$department = ($record->department ? : "ALL");
        $sqlParam = array(
			$record->name,
			$record->description,
			$record->nonbillable,
			$record->dormant,
			$record->groupId,
			$record->billingRateId,
			$rate,
			$record->displayOrderId,
			$adminUserId,
			$department
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.addCodes failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_CODES_SQL = "
        UPDATE hr_hours_codes
        SET
			GBSHoursCodes_Name = ?,
			GBSHoursCodes_Description = ?,
			GBSHoursCodes_NonBillable = ?,
			GBSHoursCodes_Dormant = ?,
			GBSHoursCodes_GroupID = ?,
			GBSHoursCodes_BillingRateID = ?,
			GBSHoursCodes_Rate = ?,
			GBSHoursCodes_Budget = ?,
			GBSHoursCodes_ExcludeFromProjections = ?,
			GBSHoursCodes_DisplayOrderID = ?
		WHERE GBSHoursCodes_ID = ?";

    public function updateCodes($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			$rate = ($record->rate ? : 0.00);
			$budget = str_replace(",","",$record->budget);
			$budget = str_replace("\$","",$budget);
            $sqlString = self::$UPDATE_CODES_SQL;
            $sqlBinding = "ssiiididiii";
            $sqlParams = array(
				$record->name,
				$record->description,
				$record->nonbillable,
				$record->dormant,
				$record->groupId,
				$record->billingRateId,
				$rate,
				$budget,
				$record->excludeFromProjections,
				$record->displayOrderId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HoursProvider.updateCodes failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

	
    private static $UPDATE_CODESDISPLAYORDER_SQL = "
        UPDATE	hr_hours_codes
        SET		GBSHoursCodes_DisplayOrderID = ?
		WHERE	GBSHoursCodes_ID = ?";

    public function updateCodesDisplayOrder($id,$displayOrderId){
        $sqlString = self::$UPDATE_CODESDISPLAYORDER_SQL;
        $sqlBinding = "ii";
        $sqlParams = array($displayOrderId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HoursProvider.updateCodesDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		$responses[] = $updateResponse;
		$responses[] = $sqlString.print_r($sqlParams,true);
        return $responses;
    }	

	//Code Matrix
    private static $SELECT_CODEMATRIX_COLUMNS = "
        SELECT
			GBSHoursCodesMatrix_ID as id,
			GBSHoursCodesMatrix_CodesID as codesId,
			GBSHoursCodesMatrix_EmployeeID as employeeId,
			GBSHoursCodesMatrix_AddTimeStamp as addTimeStamp,
			GBSHoursCodesMatrix_AddedByAdmin as addedByAdmin";

    private static $FROM_TABLE_CODEMATRIX_CLAUSE = "
		FROM hr_hours_codes_matrix
		WHERE 1=1";
		
    private static $FILTER_ON_CODESMATRIX_ID = "
        AND GBSHoursCodesMatrix_ID = ?";

    private static $FILTER_ON_CODESMATRIX_EMPLOYEEID = "
        AND GBSHoursCodesMatrix_EmployeeID = ?";

    private static $FILTER_ON_CODESMATRIX_CODESID = "
        AND GBSHoursCodesMatrix_CodesID = ?";
		
	private static $FILTER_ON_CODESMATRIX_NOTSTATUS = "
        AND GBSHoursCodesMatrix_Status != ?";	
		
	private static $FILTER_ON_CODESMATRIX_DEPARTMENT = "
		AND GBSHoursCodesMatrix_Department = ?";
		
    public function getCodesMatrix($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
		$result->collection = array();

        $paginationSelectString = self::$SELECT_CODEMATRIX_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CODEMATRIX_CLAUSE;
        $filterString = ""; 
        $filterBinding = "";
        $filterParams = array();

        $orderBy = "";
        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_CODESMATRIX_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->employeeId){
            $filterString .= self::$FILTER_ON_CODESMATRIX_EMPLOYEEID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->employeeId;
		}
		if ($criteria->codesId){
            $filterString .= self::$FILTER_ON_CODESMATRIX_CODESID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->codesId;
		}
		if ($criteria->department){
			$filterBinding .= "s";
			$filterString .= self::$FILTER_ON_CODESMATRIX_DEPARTMENT;
			$filterParams[] = $criteria->department;
		}
		if ($criteria->statusNot){
            $filterString .= self::$FILTER_ON_CODESMATRIX_NOTSTATUS;
            $filterBinding .= "s";
            $filterParams[] = $criteria->statusNot;
		}
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromCountTableClause.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getCodesMatrix failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getCodesMatrix failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $INSERT_CODESMATRIX_SQL = "
		INSERT INTO hr_hours_codes_matrix
		(
			GBSHoursCodesMatrix_CodesID,
			GBSHoursCodesMatrix_EmployeeID,
			GBSHoursCodesMatrix_AddedByAdmin,
			GBSHoursCodesMatrix_Department
		) VALUES (
			?,?,?,?
		)";

    public function addCodesMatrix($record, $adminUserId){
		//first get training information
	
        $sqlString = self::$INSERT_CODESMATRIX_SQL;
        $sqlBinding = "iiss";
        $sqlParam = array(
			$record->codesId,
			$record->employeeId,
			$adminUserId,
			$record->department
		);
		//$insertResponse .= " CodesID=".$record->trainingId." ".$training->name." DisplayOrderID ".$training->displayOrderId." EmployeeID ".$record->employeeId. " ".$record->employeeFullName." by ".$adminUserId;
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.addCodesMatrix failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
	private static $REMOVE_CODESMATRIX_SQL = "
        DELETE FROM	hr_hours_codes_matrix
		WHERE	GBSHoursCodesMatrix_ID = ?";

    public function removeCodesMatrix($id){
        $sqlString = self::$REMOVE_CODESMATRIX_SQL;
        $sqlBinding = "i";
        $sqlParams = array($id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HoursProvider.removeCodesMatrix failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }
	
	public function validateHours($record){
		$DataObj = new stdClass();
		foreach ($record as $type=>$value){
			if ($type =="employeeID"){
				$DataObj->employeeID = $value;
			}
			if ($type =="department"){
				$DataObj->department = $value;
			}
			if ($type != "action" && $type !="employeeID" && $type !="department"){
				$typeData = explode("_",$type);
				if ($typeData[2]){
					$Data[$typeData[0]][$typeData[2]][$typeData[1]] = $value;
					$DataObj->$typeData[0]->$typeData[2]->$typeData[1] = $value;
//					$Data[$typeData[0]]["date"][$typeData[2]]["value"]=$value;
				}else{
					$Data[$typeData[0]][$typeData[1]]=$value;
					$DataObj->$typeData[0]->$typeData[1] = $value;
//					$Data[$typeData[0]]["value"]=$value;
				}
			}
		}
		//validate the dateRows
		if (count($Data["DateRow"])){
			foreach ($Data["DateRow"] as $Date=>$RowInfo){
				foreach ($RowInfo as $RowID=>$Value){
					if (trim($Data["CodeRow"][$RowID]) == ''){
						$Errors[] = "Row ".$RowID." is missing Billing Code";
					}
					if (!is_numeric($Value)){
						$Errors[] = "The value '".$Value."' for Row ".$RowID." ".date("D m/d/Y",strtotime($Date))." is not a valid number";
					}
				}
			}
		}else{
			$Errors[] = "You must enter a value for each row you indicated a Billing Code";
		}
		if (!$DataObj->employeeID){
			$Errors[] = "Please indicate the Staff Member";
		}
		$UniqueErrors = array_unique($Errors);
		$Data["Errors"] = $UniqueErrors;
		foreach ($UniqueErrors as $Error){
			$dataErrors .= $Error."<bR>";
		}
		$DataObj->Errors = $dataErrors;
			
		return $DataObj;
	}
	
    private static $INSERT_CODEHOURS_SQL = "
		INSERT INTO hr_hours
		(
			GBSHours_EmployeeID,
			GBSHours_Date,
			GBSHours_CodeID,
			GBSHours_Hours,
			GBSHours_AddedByAdmin,
			GBSHours_Department
		) VALUES (
			?,?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          GBSHours_Hours = ?
		 ";
	
	public function insertHours($record,$adminUserID){
        $sqlString = self::$INSERT_CODEHOURS_SQL;
        $sqlBinding = "isidssd";
		
		foreach ($record->DateRow as $Date=>$DateInfo){
			foreach ($DateInfo as $code=>$hour){
				$results .= $Date.": Code=".$record->CodeRow->$code." hour=".$hour;
				$sqlParam = array(
					$record->employeeID,
					$Date,
					$record->CodeRow->$code,
					$hour,
					$adminUserID,
					$record->CodeRowDepartment->$code,
					$hour
				);
				$sqlParams[] = $sqlParam;
				$insertResponse = new ExecuteResponseRecord();
				MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
				if ($insertResponse->success && $insertResponse->affectedRows){
					$record->id = $insertResponse->insertedId;
					$insertResponse->record = $record;
				} else {
					if (!$insertResponse->success){
						trigger_error("HoursProvider.insertHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
					}
				}
			}
			
		}
		return $insertResponse;
	}
	private static $SELECT_NOTES_SQL = "
		SELECT 
			GBSHoursNotes_EmployeeID as employeeId,
			GBSHoursNotes_Date as noteDate,
			GBSHoursNotes_Note as note,
			GBSHoursNotes_AddedByAdmin as addedByAdmin
		";
    private static $FROM_NOTESTABLE_CLAUSE = "
        FROM hr_hours_notes
        WHERE 1 = 1";

    private static $FILTER_ON_NOTES_EMPLOYEEID = "
        AND GBSHoursNotes_EmployeeID = ?";
    private static $FILTER_ON_NOTES_DATE = "
        AND GBSHoursNotes_Date = ?";
    private static $FILTER_ON_NOTES_DEPARTMENT = "
        AND GBSHoursNotes_Department = ?";
				
    public function getNotes($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_NOTES_SQL;
        $fromTableClause = self::$FROM_NOTESTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursNotes_Date";
        $limiter = "";
        // apply search
		if ($criteria->employeeId){
			$filterString .= self::$FILTER_ON_NOTES_EMPLOYEEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->employeeId;
		}
		if ($criteria->noteDate){
			$filterString .= self::$FILTER_ON_NOTES_DATE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->noteDate;
		}
		if ($criteria->department){
			$filterString .= self::$FILTER_ON_NOTES_DEPARTMENT;
			$filterBinding .= "s";
			$filterParams[] = $criteria->department;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getNotes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getNotes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
		
    private static $INSERT_NOTES_SQL = "
		INSERT INTO hr_hours_notes
		(
			GBSHoursNotes_EmployeeID,
			GBSHoursNotes_Date,
			GBSHoursNotes_Note,
			GBSHoursNotes_AddedByAdmin,
			GBSHoursNotes_Department
		) VALUES (
			?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          GBSHoursNotes_Note = ?
		 ";
	
	public function insertNotes($record,$adminUserID){
        $sqlString = self::$INSERT_NOTES_SQL;
        $sqlBinding = "isssss";
		$department = ($record->department ? : "ALL");
		$sqlParam = array(
			$record->employeeId,
			$record->noteDate,
			$record->note,
			$adminUserID,
			$department,
			$record->note
		);
		$sqlParams[] = $sqlParam;
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
		} else {
			trigger_error("HoursProvider.insertNotes failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $DELETE_CODEHOURS_SQL = "DELETE FROM hr_hours WHERE GBSHours_ID IN (XXX)";
		
	public function deleteHours($record,$adminUserID){
        $sqlString = self::$DELETE_CODEHOURS_SQL;
        $sqlBinding = "s";
		$sqlParam = array($adminUserID);
		$IDS = $record->hourIDs;
		$sqlString = str_replace("XXX",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("HoursProvider.deleteHours failed to delete: ".$sqlString.$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $DELETE_CODEHOURID_SQL = "DELETE FROM hr_hours WHERE GBSHours_ID = ? AND GBSHours_Status='Saved'";
    private static $SELECT_CODEHOURID_SQL = "SELECT GBSHours_AddedByAdmin FROM hr_hours WHERE GBSHours_ID = ?";
		
	public function deleteHourId($record,$adminUserID){
        $sqlString = self::$DELETE_CODEHOURID_SQL;
        $sqlBinding = "i";
		$sqlParam = array($record->hourid);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success){
			if (!$insertResponse->affectedRows){
				$sqlString = self::$SELECT_CODEHOURID_SQL;
				$filterBinding = "i";
				$filterParams = array($record->hourid);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
					foreach ($sqlResult as $row){
						$insertResponse->AddedByAdmin = $row["GBSHours_AddedByAdmin"];
					}
                }
			}
			//now process the information into the only_submitted table
		} else {
			trigger_error("HoursProvider.deleteHours failed to delete: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $SUBMIT_CODEHOURS_SQL = "
		UPDATE hr_hours
		SET 
			GBSHours_Status = 'Submitted',
			GBSHours_SubmittedDate = NOW(),
			GBSHours_SubmittedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function submitHours($record,$adminUserID){
        $sqlString = self::$SUBMIT_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("HoursProvider.submitHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
    private static $APPROVE_CODEHOURS_SQL = "
		UPDATE hr_hours
		SET 
			GBSHours_Status = 'Approved',
			GBSHours_ApprovedDate = NOW(),
			GBSHours_ApprovedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function approveHours($record,$adminUserID){
        $sqlString = self::$APPROVE_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("HoursProvider.approveHours failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
    private static $SUBMIT_REMOVE_CODEHOURS_SQL = "
		UPDATE hr_hours
		SET 
			GBSHours_Status = 'Saved',
			GBSHours_SubmittedDate = NOW(),
			GBSHours_SubmittedByAdmin = adminUserID
		WHERE GBSHours_ID IN (?)";
		
	public function submitRemove($record,$adminUserID){
        $sqlString = self::$SUBMIT_REMOVE_CODEHOURS_SQL;
        $sqlBinding = "";
		$sqlParam = array();
		
		foreach ($record->hourIds as $HourID){
			$HourIDS[] = $HourID;
		}
		$IDS = implode(",",$HourIDS);
		$sqlString = str_replace("adminUserID","'".$adminUserID."'",$sqlString);
		$sqlString = str_replace("?",$IDS,$sqlString);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$record->id = $insertResponse->insertedId;
			$insertResponse->record = $record;
			//now process the information into the only_submitted table
		} else {
			trigger_error("HoursProvider.submitRemove failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponse;
	}
	
    private static $SELECT_CODEHOURS_COLUMNS = "
		SELECT
			GBSHours_ID as id,
			GBSHours_Date as hoursDate,
			GBSHours_CodeID as codeId,
			GBSHours_EmployeeID as employeeId,
			GBSHours_Hours as hours,
			GBSHours_AddedByAdmin as addedByAdmin,
			GBSHours_TimeStamp as timeStamp,
			GBSHours_Status as status,
			GBSHours_SubmittedDate as submittedDate,
			GBSHours_SubmittedByAdmin as submittedByAdmin,
			GBSHours_ApprovedDate as approvedDate,
			GBSHours_ApprovedByAdmin as approvedByAdmin";
			
    private static $FROM_HOURSTABLE_CLAUSE = "
        FROM hr_hours
        WHERE 1 = 1";
	private static $FILTER_ON_HOURS_EMPLOYEEID = "
		AND GBSHours_EmployeeID = ?";
	private static $FILTER_ON_HOURS_ID = "
		AND GBSHours_ID IN (?)";
	private static $FILTER_ON_HOURS_CODEID = "
		AND GBSHours_CodeID = ?";
	private static $FILTER_ON_HOURS_STATUS = "
		AND GBSHours_Status = ?";
	private static $FILTER_ON_HOURS_STATUS_OR = "
		AND (GBSHours_Status = ? OR GBSHours_Status = ?)";
	private static $FILTER_ON_HOURS_DATE = "
		AND GBSHours_Date BETWEEN ? AND ?";
	private static $FILTER_ON_HOURS_DEPARTMENT = "
		AND GBSHours_Department = ?";
				
    public function getHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_CODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_HOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHours_Date";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->employeeId){
			$filterString .= self::$FILTER_ON_HOURS_EMPLOYEEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->employeeId;
		}
		if ($criteria->id){
			foreach ($criteria->id as $HourID){
				$HourIDS[] = $HourID;
			}
			$IDS = implode(",",$HourIDS);
			$thisFilterString = self::$FILTER_ON_HOURS_ID;
			$thisFilterString = str_replace("?",$IDS,$thisFilterString);
			$filterString .= $thisFilterString;
		}
		if ($criteria->codeId){
			$filterString .= self::$FILTER_ON_HOURS_CODEID;
			$filterBinding .= "i";
			$filterParams[] = $criteria->codeId;
		}
		if ($criteria->status){
			$filterString .= self::$FILTER_ON_HOURS_STATUS;
			$filterBinding .= "s";
			$filterParams[] = $criteria->status;
		}
		if ($criteria->statusOr){
			$filterString .= self::$FILTER_ON_HOURS_STATUS_OR;
			$filterBinding .= "ss";
			$splitStatus = explode("OR",$criteria->statusOr);
			$filterParams[] = $splitStatus[0];
			$filterParams[] = $splitStatus[1];
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_ON_HOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}
		if ($criteria->department){
			$filterString .= " AND GBSHours_Department LIKE '%".$criteria->department."%'"; //self::$FILTER_ON_HOURS_DEPARTMENT;
			//$filterBinding .= "s";
			//$filterParams[] = $criteria->department;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("HoursProvider.getHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $SELECT_ALLAPPROVEDCODEHOURS_COLUMNS = "
		SELECT
			HoursSubmitted_ID as id,
			HoursSubmitted_GBSHoursID as gbsHoursId,
			Dateworked as dateWorked,
			Cc1 as codeName,
			CONCAT(LastName,'_',FirstName) as employeeName,
			Trxvalue as hours";
			
    private static $FROM_ALLAPPROVEDHOURSTABLE_CLAUSE = "
        FROM hr_hours_submitted
        WHERE 1 = 1";
	private static $FILTER_ON_ALLAPPROVEDHOURS_LASTNAME = "
		AND LastName = ?";
	private static $FILTER_ON_ALLAPPROVEDHOURS_CODENAME = "
		AND Cc1 = ?";
	private static $FILTER_ON_ALLAPPROVEDHOURS_DATE = "
		AND Dateworked BETWEEN ? AND ?";
				
    public function getALLApprovedHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_ALLAPPROVEDCODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_ALLAPPROVEDHOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY Dateworked";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->lastName){
			$filterString .= self::$FILTER_ON_ALLAPPROVEDHOURS_LASTNAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->lastName;
		}
		if ($criteria->codeName){
			$filterString .= self::$FILTER_ON_ALLAPPROVEDHOURS_CODENAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->codeName;
		}
		if ($criteria->codes){
			$filterString .= " AND Cc1 IN ('".str_replace(",","','",$criteria->codes)."')";
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_ON_ALLAPPROVEDHOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getALLApprovedHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("HoursProvider.getALLApprovedHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }	
	
    private static $SELECT_APPROVEDCODEHOURS_COLUMNS = "
		SELECT
			GBSHoursOnlySubmitted_GBSHoursID as id,
			GBSHoursOnlySubmitted_GBSHoursID as gbsHoursId,
			Dateworked as dateWorked,
			Cc1 as codeName,
			CONCAT(LastName,'_',FirstName) as employeeName,
			Trxvalue as hours";
			
    private static $FROM_APPROVEDHOURSTABLE_CLAUSE = "
        FROM hr_hours_gbs_only_submitted
        WHERE 1 = 1";
	private static $FILTER_ON_APPROVEDHOURS_LASTNAME = "
		AND LastName = ?";
	private static $FILTER_ON_APPROVEDHOURS_CODENAME = "
		AND Cc1 = ?";
	private static $FILTER_ON_APPROVEDHOURS_DATE = "
		AND Dateworked BETWEEN ? AND ?";
				
    public function getGBSApprovedHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_APPROVEDCODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_APPROVEDHOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY Dateworked";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->lastName){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_LASTNAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->lastName;
		}
		if ($criteria->codeName){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_CODENAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->codeName;
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_ON_APPROVEDHOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getGBSApprovedHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("HoursProvider.getGBSApprovedHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }	
    private static $SELECT_GHSAPPROVEDCODEHOURS_COLUMNS = "
		SELECT
			GBSHoursOnlySubmitted_GBSHoursID as id,
			GBSHoursOnlySubmitted_GBSHoursID as gbsHoursId,
			Dateworked as dateWorked,
			Cc1 as codeName,
			CONCAT(LastName,'_',FirstName) as employeeName,
			Trxvalue as hours";
			
    private static $FROM_GHSAPPROVEDHOURSTABLE_CLAUSE = "
        FROM hr_hours_ghs_only_submitted
        WHERE 1 = 1";
	private static $FILTER_GHSON_APPROVEDHOURS_LASTNAME = "
		AND LastName = ?";
	private static $FILTER_GHSON_APPROVEDHOURS_CODENAME = "
		AND Cc1 = ?";
	private static $FILTER_GHSON_APPROVEDHOURS_DATE = "
		AND Dateworked BETWEEN ? AND ?";
				
    public function getGHSApprovedHours($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_GHSAPPROVEDCODEHOURS_COLUMNS;
        $fromTableClause = self::$FROM_GHSAPPROVEDHOURSTABLE_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY Dateworked";
        $limiter = "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->lastName){
			$filterString .= self::$FILTER_GHSON_APPROVEDHOURS_LASTNAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->lastName;
		}
		if ($criteria->codeName){
			$filterString .= self::$FILTER_GHSON_APPROVEDHOURS_CODENAME;
			$filterBinding .= "s";
			$filterParams[] = $criteria->codeName;
		}
		if ($criteria->endDate){
			$filterString .= self::$FILTER_GHSON_APPROVEDHOURS_DATE;
			$filterBinding .= "ss";
			$filterParams[] = $criteria->startDate;
			$filterParams[] = $criteria->endDate;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString.print_r($filterParams,true);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getGHSApprovedHours failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
        } else {
            trigger_error("HoursProvider.getGHSApprovedHours failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }	
	
	public function CodeDisplayRow($rowObj,$CodesActive,$CodeHours = NULL){
		$DisplayMonthYear = $rowObj->DisplayMonthYear;
		$trCount = $rowObj->trCount;
		$trHeaderCount = 0;
		$trCountEnd = count($CodesActive);
		$WeekStartOrg = $rowObj->WeekStartOrg;
		$WeekEnd = $rowObj->WeekEnd;
		$WeekSubmitted = $rowObj->Submitted;
		$RowDisplayNotSubmitted = count($CodeHours)+1;
		$RowDisplaySubmitted = count($CodeHours);
		$displayRow = "";
		
		if (count($CodeHours)){
			foreach ($CodeHours as $CodeID=>$CodeInfo){
				$CodesWithData[] = $CodeID;
			}
			
		}
		
		foreach ($CodesActive as $CodeID=>$CodesInfo){
			$WeekStart = $WeekStartOrg;
			$displayRowData = "";
			$deleteRow[$trCount] = "";
			$headerRowData = "";
			$thisHeaderDate = "";
			while ($WeekStart <=  $WeekEnd){
				$thisDate = "DateRow_".$trCount."_".date("Y-m-d",$WeekStart);
				$thisValue = "";
				$thisHourID = "";
				$thisStatus = ($WeekSubmitted ? " SubmittedValue" : "");
				$thisHeaderDate .= "<td class=\"ui-state-default headerRow\">".date("D m/d",$WeekStart)."</td>";
				foreach ($CodeHours[$CodeID] as $Ccount=>$CodeHoursInfo){
					if ($WeekStart == strtotime($CodeHoursInfo->hoursDate)){
						$thisValue = $CodeHoursInfo->hours;
						$thisHourID = $CodeHoursInfo->id;
						$thisStatus = " ".$CodeHoursInfo->status."ValueData";
						$thisSubmittedDate = date("m/d/Y g:i a",strtotime($CodeHoursInfo->submittedDate));
						if ($thisValue > 0){
							$CodeTotals[$CodeID] = bcadd($CodeTotals[$CodeID],$thisValue,2);
							$DayTotals[$WeekStart] = bcadd($DayTotals[$WeekStart],$thisValue,2);
						}
						if ($CodeHoursInfo->status == "Saved"){
							$selectDivDisplay[$trCount] = "none";
							$displayDivStyle[$trCount] = "";
							$displayDivClassAndTitle[$trCount] = " class=\"codeNameDisplay\"";
							$thisHoursIDs[$trCount][] = $thisHourID;
							$deleteRow[$trCount] = "<div class=\"deleteRow\" id=\"".implode(",",$thisHoursIDs[$trCount])."\" data-row=\"row".$DisplayMonthYear.$trCount."\">&nbsp;</div>";
						}
					}
				}
				$displayRowData .="<td class=\"dayInput".$thisStatus."\">";
				if ($thisStatus == " SubmittedValue" || $thisStatus == " SubmittedValueData" || $thisStatus == " ApprovedValueData"){
					$WeekHasBeenSubmitted = true;
					$displayRowData .="<span class=\"".$thisStatus."Span\" data-hourid=\"".$thisHourID."\" title=\"Submitted ".$thisSubmittedDate."\">".$thisValue."</span>";
				}else{
					$displayRowData .="<input class=\"dayInputValue columndateInput".date("Y-m-d",$WeekStart)." rowCountInput".$trCount."\" data-rowCount=\"".$trCount."\" data-columndate=\"".date("Y-m-d",$WeekStart)."\" type=\"text\" style=\"height:25px;width:100px;\" data-hourid=\"".$thisHourID."\" id=\"".$thisDate."\" name=\"".$thisDate."\" value=\"".$thisValue."\"></td>";
				}
				$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
			}
			
			
			$RowDisplayCount = (!$WeekHasBeenSubmitted ? $RowDisplayNotSubmitted : $RowDisplaySubmitted);
			$headerRow = "<tr><td class=\"ui-state-default headerRow\">Billing Code</td>".$thisHeaderDate."<td class=\"totals\">&nbsp;</td></tr>";
			if ($trHeaderCount == 20){
				$trHeaderCount = 0;
				$displayRow .= $headerRow;
			}
			$displayRow .= "<tr id=\"row".$DisplayMonthYear.$trCount."\">
								<td>";
			if (!$WeekHasBeenSubmitted){
					$displayRow .= 	"<input type=\"hidden\" class=\"dayInputValue\" id=\"CodeRow_".$trCount."\" name=\"CodeRow_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\" value=\"".$CodesInfo->id."\"><input type=\"hidden\" class=\"dayInputValue\" id=\"CodeRowDepartment_".$trCount."\" name=\"CodeRowDepartment_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\" value=\"".$CodesInfo->department."\">";
					$displayRow .="<div id=\"display".$DisplayMonthYear.$trCount."\" class=\"codeNameDisplay\">".$CodesInfo->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesInfo->name,"",str_replace("-","",$CodesInfo->description)))."</span></div>";
					$displayRow .= $deleteRow[$trCount];
			}else{
				$displayRow .=$CodesInfo->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesInfo->name,"",str_replace("-","",$CodesInfo->description)))."</span>";
			}			
			$displayRow .=	"</td>";
			$displayRow .= $displayRowData;
//			if (count($CodeHours)){
				$displayRow .="<td class=\"totals\" id=\"rowTotal".$trCount."\" style=\"font-size:8pt;\">".$CodeTotals[$CodeID]."</td>";
//			}
			$displayRow .="</tr>";
			$trCount++;
			$trHeaderCount++;
		}//end while
		
		$results["displayRow"] = $displayRow;
		if (count($DayTotals)){
			$results["dayTotals"] = $DayTotals;
		}

		return $results;
		//$displayRow;
		
	}
	public function CodePickerRow($rowObj,$CodesActive,$CodeHours = NULL){
		$DisplayMonthYear = $rowObj->DisplayMonthYear;
		$trCount = $rowObj->trCount;
		$trCountEnd = $rowObj->trCountEnd+10;
		$WeekStartOrg = $rowObj->WeekStartOrg;
		$WeekEnd = $rowObj->WeekEnd;
		$WeekSubmitted = $rowObj->Submitted;
		$RowDisplayNotSubmitted = count($CodeHours)+1;
		$RowDisplaySubmitted = count($CodeHours);
		$displayRow = "";
		
		if (count($CodeHours)){
			foreach ($CodeHours as $CodeID=>$CodeInfo){
				$CodesWithData[] = $CodeID;
			}
			
		}
		
		while ($trCount <=$trCountEnd){
			$WeekStart = $WeekStartOrg;
			$displayRowData = "";
			$selectDivDisplay[$trCount] = "block";
			$displayDivStyle[$trCount] = " style=\"display:none;cursor:pointer\"";
			$displayDivClassAndTitle[$trCount] = " class=\"editDisplay\" title=\"click to edit list\"";
			$deleteRow[$trCount] = "";
			while ($WeekStart <=  $WeekEnd){
				$thisDate = "DateRow_".$trCount."_".date("Y-m-d",$WeekStart);
				$thisValue = "";
				$thisHourID = "";
				$thisStatus = ($WeekSubmitted ? " SubmittedValue" : "");
				foreach ($CodeHours[$CodesWithData[$trCount-1]] as $Ccount=>$CodeHoursInfo){
					if ($WeekStart == strtotime($CodeHoursInfo->hoursDate)){
						$thisValue = $CodeHoursInfo->hours;
						$thisHourID = $CodeHoursInfo->id;
						$thisStatus = " ".$CodeHoursInfo->status."ValueData";
						$thisSubmittedDate = date("m/d/Y g:i a",strtotime($CodeHoursInfo->submittedDate));
						if ($thisValue > 0){
							$CodeTotals[$CodesWithData[$trCount-1]] = bcadd($CodeTotals[$CodesWithData[$trCount-1]],$thisValue,2);
							$DayTotals[$WeekStart] = bcadd($DayTotals[$WeekStart],$thisValue,2);
						}
						if ($CodeHoursInfo->status == "Saved"){
							$selectDivDisplay[$trCount] = "none";
							$displayDivStyle[$trCount] = "";
							$displayDivClassAndTitle[$trCount] = " class=\"codeNameDisplay\"";
							$thisHoursIDs[$trCount][] = $thisHourID;
							$deleteRow[$trCount] = "<div class=\"deleteRow\" id=\"".implode(",",$thisHoursIDs[$trCount])."\" data-row=\"row".$DisplayMonthYear.$trCount."\">&nbsp;</div>";
						}
					}
				}
				$displayRowData .="<td class=\"dayInput".$thisStatus."\">";
				if ($thisStatus == " SubmittedValue" || $thisStatus == " SubmittedValueData" || $thisStatus == " ApprovedValueData"){
					$WeekHasBeenSubmitted = true;
					$displayRowData .="<span class=\"".$thisStatus."Span\" data-hourid=\"".$thisHourID."\" title=\"Submitted ".$thisSubmittedDate."\">".$thisValue."</span>";
				}else{
					$displayRowData .="<input class=\"dayInputValue columndateInput".date("Y-m-d",$WeekStart)." rowCountInput".$trCount."\" data-rowCount=\"".$trCount."\" data-columndate=\"".date("Y-m-d",$WeekStart)."\" type=\"text\" style=\"height:25px;width:100px;\" data-hourid=\"".$thisHourID."\" id=\"".$thisDate."\" name=\"".$thisDate."\" value=\"".$thisValue."\"></td>";
				}
				$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
			}
			
			
			$RowDisplayCount = (!$WeekHasBeenSubmitted ? $RowDisplayNotSubmitted : $RowDisplaySubmitted);
			$displayRow .= "<tr id=\"row".$DisplayMonthYear.$trCount."\"".($trCount > $RowDisplayCount ? " style=\"display:none;\"" : "")."\">
								<td>";
			if (!$WeekHasBeenSubmitted){
					$displayRow .= 	"<div id=\"select".$DisplayMonthYear.$trCount."\" style=\"display:".$selectDivDisplay[$trCount]."\">
										<select class=\"comboboxCodes dayInputValue\" id=\"CodeRow_".$trCount."\" name=\"CodeRow_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\">
											<option value=\"\">Select one...</option>";
											foreach ($CodesActive as $code=>$codeInfo){
												$displayRow .= "<option value=\"".$codeInfo->id."\"".($CodesWithData[$trCount-1] == $codeInfo->id ? " selected" : "").">".$codeInfo->description."</option>";
											}
					$displayRow .="		</select><input type=\"hidden\" class=\"dayInputValue\" id=\"CodeRowDepartment_".$trCount."\" name=\"CodeRowDepartment_".$trCount."\" data-monthyear=\"".$DisplayMonthYear."\" data-count=\"".$trCount."\" value=\"".$codeInfo->department."\">
								</div>";
					$displayRow .="<div id=\"display".$DisplayMonthYear.$trCount."\"".$displayDivClassAndTitle[$trCount].$displayDivStyle[$trCount]."\">".$CodesActive[$CodesWithData[$trCount-1]]->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesActive[$CodesWithData[$trCount-1]]->name,"",str_replace("-","",$CodesActive[$CodesWithData[$trCount-1]]->description)))."</span></div>";
					$displayRow .= $deleteRow[$trCount];
			}else{
				$displayRow .=$CodesActive[$CodesWithData[$trCount-1]]->name." <span style=\"font-size:8pt;\">".trim(str_replace($CodesActive[$CodesWithData[$trCount-1]]->name,"",str_replace("-","",$CodesActive[$CodesWithData[$trCount-1]]->description)))."</span>";
			}			
			$displayRow .=	"</td>";
			$displayRow .= $displayRowData;
//			if (count($CodeHours)){
				$displayRow .="<td class=\"totals\" id=\"rowTotal".$trCount."\" style=\"font-size:8pt;\">".$CodeTotals[$CodesWithData[$trCount-1]]."</td>";
//			}
			$displayRow .="</tr>";
			$trCount++;
		}//end while
		
		$results["displayRow"] = $displayRow;
		if (count($DayTotals)){
			$results["dayTotals"] = $DayTotals;
		}

		return $results;
		//$displayRow;
		
	}
	
	// Groups
	    private static $SELECT_GROUP_COLUMNS = "
        SELECT 
			GBSHoursCodesGroups_ID as id,
			GBSHoursCodesGroups_Name as name,
			GBSHoursCodesGroups_ColorBackground as colorBackground,
			GBSHoursCodesGroups_ColorBorder as colorBorder,
			GBSHoursCodesGroups_DisplayInProjections as displayInProjections,
			GBSHoursCodesGroups_InvoiceID as invoiceId,
			GBSHoursCodesGroups_Department as department";
	
    private static $FROM_TABLE_GROUP_CLAUSE = "
        FROM hr_hours_codes_groups
        WHERE 1 = 1";
	private static $FILTER_ON_GROUP_DEPARTMENT = "
		AND GBSHoursCodesGroups_Department = ?";

    public function getGroups($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_GROUP_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_GROUP_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSHoursCodesGroups_Name";
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->department){
			if (!$criteria->department2){
				$filterString .= self::$FILTER_ON_GROUP_DEPARTMENT;
				$filterBinding .= "s";
				$filterParams[] = $criteria->department;
			}else{
				$filterString .= " AND (".str_replace("AND","",self::$FILTER_ON_GROUP_DEPARTMENT)." OR ".str_replace("AND","",self::$FILTER_ON_GROUP_DEPARTMENT).")";
				$filterBinding .= "ss";
				$filterParams[] = $criteria->department;
				$filterParams[] = $criteria->department2;
			}
		}
		
		if ($criteria->invoiceId){
			$filterString .= " AND GBSHoursCodesGroups_InvoiceID=?";
			$filterBinding .= "i";
			$filterParams[] = $criteria->invoiceId;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getGroups failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getGroups failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $INSERT_GROUPS_SQL = "
		INSERT INTO hr_hours_codes_groups
		(
			GBSHoursCodesGroups_Name,
			GBSHoursCodesGroups_ColorBackground,
			GBSHoursCodesGroups_ColorBorder,
			GBSHoursCodesGroups_DisplayInProjections,
			GBSHoursCodesGroups_InvoiceID,
			GBSHoursCodesGroups_AddedByUserID,
			GBSHoursCodesGroups_Department
		) VALUES (
			?,?,?,?,?,?,?
		)";

    public function addGroups($record, $adminUserId){
        $sqlString = self::$INSERT_GROUPS_SQL;
        $sqlBinding = "ssssiss";
		$department = ($record->department ? : "ALL");
        $sqlParam = array(
			$record->name,
			$record->colorBackground,
			$record->colorBorder,
			$record->displayInProjections,
			$record->invoiceId,
			$adminUserId,
			$department
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.addGroups failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_GROUPS_SQL = "
        UPDATE hr_hours_codes_groups
        SET
			GBSHoursCodesGroups_Name = ?,
			GBSHoursCodesGroups_ColorBackground = ?,
			GBSHoursCodesGroups_ColorBorder = ?,
			GBSHoursCodesGroups_DisplayInProjections = ?,
			GBSHoursCodesGroups_InvoiceID = ?
		WHERE GBSHoursCodesGroups_ID = ?";

    public function updateGroups($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_GROUPS_SQL;
            $sqlBinding = "ssssii";
            $sqlParams = array(
				$record->name,
				$record->colorBackground,
				$record->colorBorder,
				$record->displayInProjections,
				$record->invoiceId,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HoursProvider.updateGroups failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }
	
    private static $SELECT_COLUMNS_SUPERVISEDBY = "
        SELECT 
			GBSHoursSupervised_ID as id,
			GBSHoursSupervised_EmployeeEmail as employeeEmail,
			GBSHoursSupervised_SupervisorName as supervisorName";
	
    private static $FROM_TABLE_CLAUSE_SUPERVISEDBY = "
        FROM hr_hours_supervised
        WHERE 1 = 1";
		
    private static $FILTER_SUPERVISEDBY_DEPARTMENT = "
        AND GBSHoursSupervised_Department = ?";
		
    public function getSupervisedBy($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS_SUPERVISEDBY;
        $fromTableClause = self::$FROM_TABLE_CLAUSE_SUPERVISEDBY;
        $filterString = self::$FILTER_SUPERVISEDBY_DEPARTMENT;
        $filterBinding = "s";
        $filterParams = array($criteria->department);

        $orderBy = "";
        $limiter = "";

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getSupervisedBy failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getSupervisedBy failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }


    private static $SELECT_AEPTEMPLATES_COLUMNS = "
        SELECT 
			GBSEAPTemplates_ID as id,
			GBSEAPTemplates_Measure as measure,
			GBSEAPTemplates_PrimaryUtility as primaryUtility,
			GBSEAPTemplates_SecondaryUtility as secondaryUtility,
			GBSEAPTemplates_IsMFEP as isMfep,
			GBSEAPTemplates_DisplayText as displayText,
			GBSEAPTemplates_LastModifiedTimeStamp as lastModifiedTimeStamp,
			GBSEAPTemplates_LastModifiedBy as lastModifiedBy";
	
    private static $FROM_TABLE_AEPTEMPLATES_CLAUSE = "
        FROM gbs_eap_templates
        WHERE 1 = 1";
		
    private static $FROM_TABLE_AEPMFEPTEMPLATES_CLAUSE = "
        FROM gbs_eap_mfeptemplates
        WHERE 1 = 1";

    private static $FILTER_ON_EAP_MEASURE = "
        AND GBSEAPTemplates_Measure = ?";
		
    private static $FILTER_ON_EAP_UTILITY_PRIMARY = "
        AND GBSEAPTemplates_PrimaryUtility = ?";

    private static $FILTER_ON_EAP_UTILITY_SECONDARY = "
        AND GBSEAPTemplates_SecondaryUtility = ?";
		
    private static $FILTER_ON_EAP_ISMFEP = "
        AND GBSEAPTemplates_IsMFEP = 1";
		
    private static $FILTER_ON_EAP_TEXT = "
        AND GBSEAPTemplates_DisplayText = ?";
		
    public function getEAPTemplates($criteria){
        $paginationSelectString = self::$SELECT_AEPTEMPLATES_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_AEPTEMPLATES_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY GBSEAPTemplates_Measure";
        $limiter = "";
        // apply search
 		if ($criteria->measure){
			$filterString .= self::$FILTER_ON_EAP_MEASURE;
			$filterBinding .= "s";
			$filterParams[] = $criteria->measure;
		}
 		if ($criteria->primaryUtility){
			$filterString .= self::$FILTER_ON_EAP_UTILITY_PRIMARY;
			$filterBinding .= "s";
			$filterParams[] = $criteria->primaryUtility;
		}
 		if ($criteria->secondaryUtility){
			$filterString .= self::$FILTER_ON_EAP_UTILITY_SECONDARY;
			$filterBinding .= "s";
			$filterParams[] = $criteria->secondaryUtility;
		}
 		if ($criteria->displayText){
			$filterString .= self::$FILTER_ON_EAP_TEXT;
			$filterBinding .= "s";
			$filterParams[] = $criteria->displayText;
		}
 		if ($criteria->isMfep){
			$filterString .= self::$FILTER_ON_EAP_ISMFEP;
			$fromTableClause = self::$FROM_TABLE_AEPMFEPTEMPLATES_CLAUSE;
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getAEPTemplates failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getAEPTemplates failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $INSERT_EAPTEMPLATE_SQL = "
		INSERT INTO gbs_eap_templates
		(
			GBSEAPTemplates_Measure,
			GBSEAPTemplates_PrimaryUtility,
			GBSEAPTemplates_SecondaryUtility,
			GBSEAPTemplates_IsMFEP,
			GBSEAPTemplates_DisplayText,
			GBSEAPTemplates_LastModifiedTimeStamp,
			GBSEAPTemplates_LastModifiedBy
		) VALUES (
			?,?,?,?,?,?,?
		)";

    public function addEAPTemplate($record, $adminUserId){
        $sqlString = self::$INSERT_EAPTEMPLATE_SQL;
        $sqlBinding = "sssisss";
        $sqlParam = array(
			$record->measure,
			$record->primaryUtility,
			$record->secondaryUtility,
			$record->isMfep,
			$record->displayText,
			date("Y-m-d H:i:s"),
			$adminUserId
        );
		if ($record->isMfep){
			$sqlString = str_replace("gbs_eap_templates","gbs_eap_mfeptemplates",$sqlString);
		}

        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.addEAPTemplate failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_EAPTEMPLATE_SQL = "
        UPDATE gbs_eap_templates
        SET
			GBSEAPTemplates_Measure = ?,
			GBSEAPTemplates_PrimaryUtility = ?,
			GBSEAPTemplates_SecondaryUtility = ?,
			GBSEAPTemplates_IsMFEP = ?,
			GBSEAPTemplates_DisplayText = ?,
			GBSEAPTemplates_LastModifiedTimeStamp = ?,
			GBSEAPTemplates_LastModifiedBy = ?
		WHERE GBSEAPTemplates_ID = ?";

    public function updateEAPTemplate($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
		$thisTimeStamp = date("Y-m-d H:i:s");
        if ($record->id){
            $sqlString = self::$UPDATE_EAPTEMPLATE_SQL;
            $sqlBinding = "sssisssi";
            $sqlParams = array(
				trim($record->measure),
				trim($record->primaryUtility),
				trim($record->secondaryUtility),
				trim($record->isMfep),
				trim($record->displayText),
				$thisTimeStamp,
				$adminUserId,
				$record->id
            );
			
			if ($record->isMfep){
				$sqlString = str_replace("gbs_eap_templates","gbs_eap_mfeptemplates",$sqlString);
			}
			
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HoursProvider.updateEAPTemplate failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }	
	
	// Farms
	    private static $SELECT_FARM_COLUMNS = "
        SELECT 
			HoursFarm_ID as id,
			HoursFarm_Name as name,
			HoursFarm_Description as description,
			HoursFarm_Town as town,
			HoursFarm_Status as status,
			HoursFarm_Department as department";
	
    private static $FROM_TABLE_FARM_CLAUSE = "
        FROM hr_hours_gbs_farms
        WHERE 1 = 1";
	private static $FILTER_ON_FARM_DEPARTMENT = "
		AND HoursFarm_Department = ?";

    public function getFarms($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_FARM_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_FARM_CLAUSE;
        $filterString = ""; // show only those with display order by default
        $filterBinding = "";
        $filterParams = array();

//        $orderBy = $this->getOrderByClause($criteria);
        $orderBy = " ORDER BY HoursFarm_Name";
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
 		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			 $limiter = "";
		}
		if ($criteria->department){
			if (!$criteria->department2){
				$filterString .= self::$FILTER_ON_FARM_DEPARTMENT;
				$filterBinding .= "s";
				$filterParams[] = $criteria->department;
			}else{
				$filterString .= " AND (".str_replace("AND","",self::$FILTER_ON_FARM_DEPARTMENT)." OR ".str_replace("AND","",self::$FILTER_ON_FARM_DEPARTMENT).")";
				$filterBinding .= "ss";
				$filterParams[] = $criteria->department;
				$filterParams[] = $criteria->department2;
			}
		}
		

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = (object)$row;
                    }
                } else {
                    trigger_error("HoursProvider.getFarms failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("HoursProvider.getFarms failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $INSERT_FARMS_SQL = "
		INSERT INTO hr_hours_gbs_farms
		(
			HoursFarm_Name,
			HoursFarm_Description,
			HoursFarm_Town,
			HoursFarm_Status,
			HoursFarm_Department,
			HoursFarm_AddedByAdmin
		) VALUES (
			?,?,?,?,?,?
		)";

    public function addFarms($record, $adminUserId){
        $sqlString = self::$INSERT_FARMS_SQL;
        $sqlBinding = "ssssss";
        $sqlParam = array(
			$record->name,
			$record->description,
			$record->town,
			$record->status,
			$record->department,
			$adminUserId
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.addFarms failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

	
    private static $UPDATE_FARMS_SQL = "
        UPDATE hr_hours_gbs_farms
        SET
			HoursFarm_Name = ?,
			HoursFarm_Description = ?,
			HoursFarm_Town = ?,
			HoursFarm_Department = ?,
			HoursFarm_Status = ?,
			HoursFarm_EditedByAdmin =?,
			HoursFarm_LastEditedByTimeStamp =?
		WHERE HoursFarm_ID = ?";

    public function updateFarms($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
			$currentTimeStamp = date("Y-m-d H:i:s");
			$sqlString = self::$UPDATE_FARMS_SQL;
            $sqlBinding = "sssssssi";
            $sqlParams = array(
				$record->name,
				$record->description,
				$record->town,
				$record->department,
				$record->status,
				$adminUserId,
				$currentTimeStamp,
				$record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("HoursProvider.updateFarms failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }
	
    private static $INSERT_PROJECTIONS_SQL = "
		INSERT INTO hr_hours_projections
		(
			adjustmentTimeStamp,
			adjustmentByEmployee,
			adjustmentByGroup,
			adjustmentQueryString,
			createdBy,
			adjustmentNotes,
			adjustmentInvoiceGroupsToHide
		) VALUES (
			?,?,?,?,?,?,?
		) ON DUPLICATE KEY UPDATE
          adjustmentByEmployee = ?,
          adjustmentByGroup = ?,
          adjustmentNotes = ?,
		  adjustmentInvoiceGroupsToHide = ?";

    public function saveProjectionAdjustments($record, $adminUserId){
        $sqlString = self::$INSERT_PROJECTIONS_SQL;
        $sqlBinding = "issssssssss";
		$adjustmentByEmployee = json_encode($record->adjustmentByEmployee);
		$adjustmentByGroup = json_encode($record->adjustmentByGroup);
        $sqlParam = array(
			$record->adjustmentTimeStamp,
			$adjustmentByEmployee,
			$adjustmentByGroup,
			$record->adjustmentQueryString,
			$adminUserId,
			$record->adjustmentNotes,
			$record->adjustmentInvoiceGroupsToHide,
			$adjustmentByEmployee,
			$adjustmentByGroup,
			$record->adjustmentNotes,
			$record->adjustmentInvoiceGroupsToHide

		);
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("HoursProvider.saveProjectionAdjustments failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }
	
    private static $UPDATE_PROJECTIONS_SQL = "
        UPDATE hr_hours_projections
        SET
			adjustmentName = ?,
			adjustmentNotes = ?,
			adjustmentInvoiceGroupsToHide = ?
		WHERE adjustmentTimeStamp = ?";

    public function nameProjectionAdjustments($record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
		$sqlString = self::$UPDATE_PROJECTIONS_SQL;
		$sqlBinding = "sssi";
		$sqlParams = array(
			$record->adjustmentName,
			$record->adjustmentNotes,
			$record->adjustmentInvoiceGroupsToHide,
			$record->adjustmentTimeStamp
		);
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("HoursProvider.nameProjectionAdjustments failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
        return $updateResponse;
    }
	
    public function getProjections($criteria){
        $result = new PagedResult();
        $SqlString = "SELECT * FROM hr_hours_projections WHERE 1=1";
        $filterString = " AND adjustmentName IS NOT NULL"; 
        $filterBinding = "";
        $filterParams = array();

        if ($criteria->adjustmentTimeStamp){
            $filterString .= " AND adjustmentTimeStamp=?";
            $filterBinding .= "i";
            $filterParams[] = $criteria->adjustmentTimeStamp;
        }
		$orderBy = " ORDER BY adjustmentTimeStamp";
		$pageSqlString = $SqlString.$filterString.$orderBy;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("HoursProvider.getProjections failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}

        $result->setHasMore();
        return $result;
    }
	
	

}