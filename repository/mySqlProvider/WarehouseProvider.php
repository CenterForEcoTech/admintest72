<?php
class WarehouseProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }
	
	private static $SELECT_COLUMNS = "
	SELECT
		w.GHSInventoryWareHouseLocations_ID as id,
		w.GHSInventoryWareHouseLocations_DisplayOrderID as displayOrderId,
		w.GHSInventoryWareHouseLocations_Name as name,
		w.GHSInventoryWareHouseLocations_Description as description,
		w.GHSInventoryWareHouseLocations_LinkedToEmployeeID as linkedToEmployeeId,
		w.GHSInventoryWareHouseLocations_LinkedToSecondEmployeeID as linkedToSecondEmployeeId,
		w.GHSInventoryWareHouseLocations_ActiveID as activeId,
		w.GHSInventoryWareHouseLocations_ExcludeFromCountID as excludeFromCountId,
		w.GHSInventoryWareHouseLocations_StartDate as startDate,
		w.GHSInventoryWareHouseLocations_EndDate as endDate,
		w.GHSInventoryWareHouseLocations_Lead as lead,
		w.GHSInventoryWareHouseLocations_BulbRateNotTiedToClosingRate as bulbRateNotTiedToClosingRate,
		w.GHSInventoryWareHouseLocations_OutsideGHS as outsideGHS";

    private static $FROM_TABLE_CLAUSE = "
        FROM ghs_inventory_warehouse_locations as w
        WHERE 1 = 1";

    private static $FILTER_ON_DISPLAYORDERID = "
        AND w.GHSInventoryWareHouseLocations_DisplayOrderID >= 1";
		
    private static $FILTER_ON_NAME = "
        AND w.GHSInventoryWareHouseLocations_Name = ?";
		
    private static $FILTER_ON_DESCRIPTION = "
        AND w.GHSInventoryWareHouseLocations_Description = ?";
		
    private static $FILTER_ON_ID = "
        AND w.GHSInventoryWareHouseLocations_ID = ?";
		
    private static $FILTER_ON_ACTIVEID = "
        AND w.GHSInventoryWareHouseLocations_ActiveID = 1";
	
		

    public function get($criteria){
        
		$result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = self::$FILTER_ON_ACTIVEID; // show only those active by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
		if ($criteria->name){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
        }
		if ($criteria->description){
            $filterString .= self::$FILTER_ON_DESCRIPTION;
            $filterBinding .= "s";
            $filterParams[] = $criteria->description;
        }
		if ($criteria->id){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }
		if ($criteria->showAll){
			$filterString = "";
		}
		if ($criteria->noLimit){
			$limiter = "";
		}

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("WarehouseProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("WarehouseProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
	
    }

    protected function mapToRecord($dataRow) {
        return WarehouseRecord::castAs($dataRow);
    }
	

	
    private static $DEFAULT_ORDER_BY = "
        ORDER BY w.GHSInventoryWareHouseLocations_DisplayOrderID ASC";

    private function getOrderByClause($criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;

        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "description":
                    $orderBy = " ORDER BY w.GHSInventoryWareHouseLocations_Description ".$sortOrder;
                    break;
                case "name":
                    $orderBy = " ORDER BY w.GHSInventoryWareHouseLocations_Name ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "description":
                        $orderByStrings[] = "w.GHSInventoryWareHouseLocations_Description ".$sortOrder;
                        break;
                    case "efi":
                        $orderByStrings[] = "w.GHSInventoryWareHouseLocations_Name ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }
	//some change
	public function createSelectPulldown($collection,$fieldName="WarehouseName",$defaultSelect="",$excludedList = array(),$includeBlankDefault = false, $classList=""){
		foreach ($collection as $Warehouse=>$WarehouseInfo){
			$displayName = ($WarehouseInfo->name == $WarehouseInfo->description) ? $WarehouseInfo->name : $WarehouseInfo->name. " ".$WarehouseInfo->description;
			$WarehouseList[$WarehouseInfo->name]["displayName"]=$displayName;
			$WarehouseList[$WarehouseInfo->name]["value"]=$WarehouseInfo->name;
		}
		if (trim($classList)){
			$classList = " class=\"".$classList."\"";
		}
		$result = "<select name=\"".$fieldName."\" id=\"".$fieldName."\"".$classList.">";
		if ($includeBlankDefault){
			$result .= "<option value=\"\">All Warehouse Movements</option>";
		}
		foreach ($WarehouseList as $Warehouse=>$WarehouseInfo){
			if (!in_array($WarehouseInfo["value"],$excludedList)){
				$result .= "<option value=\"".$WarehouseInfo["value"]."\"";
				$result .= ($defaultSelect == $WarehouseInfo["value"]) ? " selected" : "";
				$result .= ">".$WarehouseInfo["displayName"]."</option>";
			}
		}
		$result .= "</select>";
		//echo $result;
		return $result;
	}
	
	/* Warehouse Count */
	
	private static $SELECT_WAREHOUSECOUNT_COLUMNS = "	
	SELECT
		wc.GHSInventoryWarehouseProductCount_ID as id,
		wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName as warehouseName,
		wc.GHSInventoryWarehouseProductCount_ProductsEFI as efi,
		wc.GHSInventoryWarehouseProductCount_Qty as qty,
		wc.GHSInventoryWarehouseProductCount_AsOfDate as asOfDate,
		wc.GHSInventoryWarehouseProductCount_TransactionDate as transactionDate
		";

    private static $WAREHOUSECOUNT_MOSTRECENT = "
		, MAX(wc.GHSInventoryWarehouseProductCount_AsOfDate) as mostRecent";

	private static $FROM_WAREHOUSECOUNT_CLAUSE = "
		FROM ghs_inventory_warehouse_productcount as wc
        WHERE 1 = 1";

    private static $FILTER_WAREHOUSECOUNT_ON_NAME = "
        AND wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName = ?";
    private static $FILTER_WAREHOUSECOUNT_NOTRECEIVED = "
        AND wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName NOT LIKE '%Received%'";
		
    private static $FILTER_WAREHOUSECOUNT_ON_EFI = "
        AND wc.GHSInventoryWarehouseProductCount_ProductsEFI = ?";
		
	private static $FILTER_WAREHOUSECOUNT_ON_TRANSACTIONDATE = "
		AND wc.GHSInventoryWarehouseProductCount_TransactionDate > ?";

    private static $WAREHOUSECOUNT_MOSTRECENT_FILTER = "
		AND GHSInventoryWarehouseProductCount_AsOfDate = (
			SELECT MAX(GHSInventoryWarehouseProductCount_AsOfDate)
			FROM ghs_inventory_warehouse_productcount AS b
			WHERE wc.GHSInventoryWarehouseProductCount_ProductsEFI = b.GHSInventoryWarehouseProductCount_ProductsEFI
			  AND wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName = b.GHSInventoryWarehouseProductCount_WarehouseLocationsName
		)";
		
		
    public function getWarehouseCount($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 50;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_WAREHOUSECOUNT_COLUMNS;
		$mostRecentString = ""; //pull in all records by default
        $fromTableClause = self::$FROM_WAREHOUSECOUNT_CLAUSE;
        $filterString = ""; // no filters by default
        $filterBinding = "";
        $filterParams = array();
		$criteria->warehouseCount = true;
//        $orderBy = $this->getOrderByClause($criteria);
//		echo $orderBy;
		//Do not limit these results
        //$limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
		if ($criteria->name){
            $filterString .= self::$FILTER_WAREHOUSECOUNT_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
        }
		if ($criteria->efi){
            $filterString .= self::$FILTER_WAREHOUSECOUNT_ON_EFI;
            $filterBinding .= "s";
            $filterParams[] = $criteria->efi;
        }
		if ($criteria->mostrecent){
			$mostRecentString = self::$WAREHOUSECOUNT_MOSTRECENT;
			$filterString .= self::$WAREHOUSECOUNT_MOSTRECENT_FILTER;
		}
		if ($criteria->efihistory){
			$mostRecentString = "";
			$filterString .= self::$FILTER_WAREHOUSECOUNT_NOTRECEIVED;
			$orderBy = " GROUP BY warehouseName, asOfDate";
		}
		if ($criteria->forChart){
			$filterString .= self::$FILTER_WAREHOUSECOUNT_ON_TRANSACTIONDATE;
			$filterBinding .= "s";
            $filterParams[] = date("Y-m-d", strtotime(date()." -6 months"));
			$orderBy = " ORDER BY warehouseName, transactionDate";
		}
		
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
				//Group By Criteria has to happen after record count
                // get the page
                $pageSqlString = $paginationSelectString.$mostRecentString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("WarehouseProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("WarehouseProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	private static $SELECT_WAREHOUSECOUNT_FAST_COLUMNS = "	
	SELECT
		GHSInventoryWarehouseProductCount_Qty as qty
		FROM ghs_inventory_warehouse_productcount
		WHERE GHSInventoryWarehouseProductCount_WarehouseLocationsName = ? 
		AND GHSInventoryWarehouseProductCount_ProductsEFI = ?
		ORDER BY GHSInventoryWarehouseProductCount_ID DESC LIMIT 0,1";
	
    public function getWarehouseCountFast($criteria){
        $paginationSelectString = self::$SELECT_WAREHOUSECOUNT_FAST_COLUMNS;
        $filterBinding = "ss";
        $filterParams = array($criteria->name,$criteria->efi);
		
        // get the total count
		$pageSqlString = $paginationSelectString;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            return $sqlResult[0]["qty"];
        } else {
            trigger_error("WarehouseProvider.getWarehouseCountFast failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
    }

		
	
	private static $SELECT_ALL_WAREHOUSECOUNT_COLUMNS = "	
	SELECT
		wc.GHSInventoryWarehouseProductCount_ID as id,
		wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName as warehouseName,
		wc.GHSInventoryWarehouseProductCount_ProductsEFI as efi,
		wc.GHSInventoryWarehouseProductCount_Qty as qty,
		wc.GHSInventoryWarehouseProductCount_TransactionDate as mostRecentTransaction";
	
	private static $FILTER_ALL_WAREHOUSECOUNT_EXCLUDELIST = "
        AND FIND_IN_SET(wc.GHSInventoryWarehouseProductCount_WarehouseLocationsName,?)=0";

	private static $FROM_WAREHOUSECOUNT_CURRENT_CLAUSE = "
		FROM ghs_inventory_warehouse_productcount_current as wc
        WHERE 1 = 1";
		

    public function getAllWarehouseCount($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_ALL_WAREHOUSECOUNT_COLUMNS;
        $fromTableClause = self::$FROM_WAREHOUSECOUNT_CURRENT_CLAUSE;

		//$filterString .= self::$WAREHOUSECOUNT_MOSTRECENT_CURRENT_FILTER;
        $filterBinding = "";
        $filterParams = array();

		//Do not limit these results
        //$limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";
        // apply search
		if ($criteria->name){
            $filterString .= self::$FILTER_WAREHOUSECOUNT_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->name;
        }
		if ($criteria->efi){
            $filterString .= self::$FILTER_WAREHOUSECOUNT_ON_EFI;
            $filterBinding .= "s";
            $filterParams[] = $criteria->efi;
        }
		if ($criteria->excludelist){
            $filterString .= self::$FILTER_ALL_WAREHOUSECOUNT_EXCLUDELIST;
            $filterBinding .= "s";
            $filterParams[] = implode(",",$criteria->excludelist);
        }
        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$mostRecentString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
				//print_pre($filterParams);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("WarehouseProvider.getAllWarehouseCount failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("WarehouseProvider.getAllWarehouseCount failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }
	
    private static $INSERT_WAREHOUSECOUNT_SQL = "
        INSERT INTO ghs_inventory_warehouse_productcount
        (
            GHSInventoryWarehouseProductCount_WarehouseLocationsName,
            GHSInventoryWarehouseProductCount_ProductsEFI,
            GHSInventoryWarehouseProductCount_Qty,
            GHSInventoryWarehouseProductCount_TransactionDate,
            GHSInventoryWarehouseProductCount_MovementHistoryID
		) VALUES (
            ?,?,?,?,?
        )";
    private static $INSERT_WAREHOUSECOUNT_CURRENT_SQL = "
        INSERT INTO ghs_inventory_warehouse_productcount_current
        (
            GHSInventoryWarehouseProductCount_WarehouseLocationsName,
            GHSInventoryWarehouseProductCount_ProductsEFI,
            GHSInventoryWarehouseProductCount_Qty,
            GHSInventoryWarehouseProductCount_TransactionDate,
            GHSInventoryWarehouseProductCount_MovementHistoryID
		) VALUES (
            ?,?,?,?,?
        ) ON DUPLICATE KEY UPDATE
            GHSInventoryWarehouseProductCount_WarehouseLocationsName=VALUES(GHSInventoryWarehouseProductCount_WarehouseLocationsName),
            GHSInventoryWarehouseProductCount_ProductsEFI=VALUES(GHSInventoryWarehouseProductCount_ProductsEFI),
            GHSInventoryWarehouseProductCount_Qty=VALUES(GHSInventoryWarehouseProductCount_Qty),
            GHSInventoryWarehouseProductCount_TransactionDate=VALUES(GHSInventoryWarehouseProductCount_TransactionDate),
            GHSInventoryWarehouseProductCount_MovementHistoryID=VALUES(GHSInventoryWarehouseProductCount_MovementHistoryID)
		";

    private static $INSERT_WAREHOUSECOUNT_BINDINGS = "ssisi";

    public function insertWarehouseCount($warehouseCountData){
        $sqlString = self::$INSERT_WAREHOUSECOUNT_SQL;
        $sqlBindings = self::$INSERT_WAREHOUSECOUNT_BINDINGS;
        $sqlParams = array(
            $warehouseCountData->warehouseName,
            $warehouseCountData->efi,
            $warehouseCountData->qty,
            $warehouseCountData->transactionDate,
            $warehouseCountData->movementHistoryID
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		//Now update current warehouse
		$sqlString = self::$INSERT_WAREHOUSECOUNT_CURRENT_SQL;
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);

        return $insertResponse;
    }
    private static $INSERT_WAREHOUSECOUNT_REBUILD_SQL = "
        INSERT INTO ghs_inventory_warehouse_productcount
        (
            GHSInventoryWarehouseProductCount_WarehouseLocationsName,
            GHSInventoryWarehouseProductCount_ProductsEFI,
            GHSInventoryWarehouseProductCount_Qty,
            GHSInventoryWarehouseProductCount_TransactionDate,
            GHSInventoryWarehouseProductCount_MovementHistoryID,
            GHSInventoryWarehouseProductCount_AsOfDate
		) VALUES (
            ?,?,?,?,?,?
        )";
    private static $INSERT_WAREHOUSECOUNT_REBUILD_CURRENT_SQL = "
        INSERT INTO ghs_inventory_warehouse_productcount_current
        (
            GHSInventoryWarehouseProductCount_WarehouseLocationsName,
            GHSInventoryWarehouseProductCount_ProductsEFI,
            GHSInventoryWarehouseProductCount_Qty,
            GHSInventoryWarehouseProductCount_TransactionDate,
            GHSInventoryWarehouseProductCount_MovementHistoryID,
            GHSInventoryWarehouseProductCount_AsOfDate
		) VALUES (
            ?,?,?,?,?,?
        ) ON DUPLICATE KEY UPDATE
            GHSInventoryWarehouseProductCount_WarehouseLocationsName=VALUES(GHSInventoryWarehouseProductCount_WarehouseLocationsName),
            GHSInventoryWarehouseProductCount_ProductsEFI=VALUES(GHSInventoryWarehouseProductCount_ProductsEFI),
            GHSInventoryWarehouseProductCount_Qty=VALUES(GHSInventoryWarehouseProductCount_Qty),
            GHSInventoryWarehouseProductCount_TransactionDate=VALUES(GHSInventoryWarehouseProductCount_TransactionDate),
            GHSInventoryWarehouseProductCount_MovementHistoryID=VALUES(GHSInventoryWarehouseProductCount_MovementHistoryID),
            GHSInventoryWarehouseProductCount_AsOfDate=VALUES(GHSInventoryWarehouseProductCount_AsOfDate)
		";

    private static $INSERT_WAREHOUSECOUNT_REBUILD_BINDINGS = "ssisis";
	
    public function insertWarehouseCountRebuild($warehouseCountData){
        $sqlString = self::$INSERT_WAREHOUSECOUNT_REBUILD_SQL;
        $sqlBindings = self::$INSERT_WAREHOUSECOUNT_REBUILD_BINDINGS;
		$AsOfDate = $warehouseCountData->transactionDate." ".date("H:i:s");

        $sqlParams = array(
            $warehouseCountData->warehouseName,
            $warehouseCountData->efi,
            $warehouseCountData->qty,
            $warehouseCountData->transactionDate,
            $warehouseCountData->movementHistoryID,
            $AsOfDate
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
		//Now update current warehouse
		$sqlString = self::$INSERT_WAREHOUSECOUNT_REBUILD_CURRENT_SQL;
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);

        return $insertResponse;
    }
	
	public function updateWarehouseCount($TransferResults){
		foreach ($TransferResults as $Obj){
			$efi = $Obj["efi"];
			$id = $Obj["id"];
			$transferDate = $Obj["transferDate"];
			$units = (int)$Obj["units"];
			$warehouseFrom = $Obj["warehouseFrom"];
			$warehouseTo = $Obj["warehouseTo"];
			$purchaseOrder = $Obj["purchaseOrder"];

			//Move count from
			$criteria->name = $warehouseFrom;
			$criteria->efi = $efi ;
			$warehouseCount = $this->getWarehouseCountFast($criteria);
			$CurrentUnitQty = (int)($warehouseCount ? $warehouseCount : 0);
			
			$warehouseCountData->warehouseName = $warehouseFrom;
			$warehouseCountData->efi = $efi;
			$warehouseCountData->currentQty = $CurrentUnitQty;
			$warehouseCountData->currentUnits = $units;
			$warehouseCountData->qty = (($CurrentUnitQty)-($units));
			$warehouseCountData->transactionDate = MySQLDateUpdate($transferDate);
			$warehouseCountData->movementHistoryID = $id;
			$resultsFrom = $this->insertWarehouseCount($warehouseCountData);
			if ($resultsFrom->success){
				$results["From"] .= $efi." Total Count ".(($CurrentUnitQty)-($units))."<br>";
				$results["FromWarehouseChange"] .= $warehouseFrom." ".$efi." From ".$CurrentUnitQty." units to ".(($CurrentUnitQty)-($units))." units";
			}
 
			//Move count to
			$criteria->name = $warehouseTo;
			$criteria->efi = $efi ;
			$warehouseCount = $this->getWarehouseCountFast($criteria);
			$CurrentUnitQty = (int)($warehouseCount ? $warehouseCount : 0);
			
			$warehouseCountData->warehouseName = $warehouseTo;
			$warehouseCountData->efi = $efi;
			$warehouseCountData->currentQty = $CurrentUnitQty;
			$warehouseCountData->currentUnits = $units;
			$warehouseCountData->qty = (($CurrentUnitQty)+($units));
			$warehouseCountData->transactionDate = MySQLDateUpdate($transferDate);
			$warehouseCountData->movementHistoryID = $id;
			$resultsTo = $this->insertWarehouseCount($warehouseCountData);
			if ($resultsTo->success){
				$results["To"] .= $efi." Total Count ".(($CurrentUnitQty)+($units))."<br>";
				$results["ToWarehouseChange"] .= $warehouseTo." ".$efi." From ".$CurrentUnitQty." units to ".(($CurrentUnitQty)+($units))." units";
				$results["EFI"][$efi] = " received ".$units." units";
			}
		}
		return $results;
	}
	public function resetWarehouseCount($TransferResults){
		foreach ($TransferResults as $Obj){
			$efi = $Obj["efi"];
			$description = $Obj["description"];
			$id = $Obj["id"];
			$transferDate = date("Y-m-d", strtotime($Obj["transferDate"]));
			$units = (int)$Obj["units"];
			$warehouseFrom = $Obj["warehouseFrom"];
			$warehouseTo = $Obj["warehouseTo"];
			$purchaseOrder = $Obj["purchaseOrder"];
			$adjustCount = $Obj["adjustcount"];

			//Move count from
			if ($warehouseFrom == "Adjust Manual Count"){
				$criteria->name = $warehouseFrom;
				$criteria->efi = $efi ;
				$warehouseCount = $this->getWarehouseCountFast($criteria);
				$CurrentUnitQty = (int)($warehouseCount ? $warehouseCount : 0);
				
				$warehouseCountData->warehouseName = $warehouseFrom;
				$warehouseCountData->efi = $efi;
				$warehouseCountData->qty = (($CurrentUnitQty)-($units));
				$warehouseCountData->transactionDate = $transferDate;
				$warehouseCountData->movementHistoryID = $id;
				$resultsFrom = $this->insertWarehouseCount($warehouseCountData);
				if ($resultsFrom->success){
					$results["AMC"] .= $efi." Total Count ".(($CurrentUnitQty)-($units))."<br>";
				}
			}else{
				$warehouseCountData->warehouseName = $warehouseFrom;
				$warehouseCountData->efi = $efi;
				$warehouseCountData->qty = $adjustCount;
				$warehouseCountData->transactionDate = $transferDate;
				$warehouseCountData->movementHistoryID = $id;
				$resultsFrom = $this->insertWarehouseCount($warehouseCountData);
				if ($resultsFrom->success){
					$results["Warehouse"] .= $warehouseFrom." ".$efi." Total Count Now Set To ".$adjustCount."<br>";
				}
				$results["Summary"] .= $warehouseFrom."|".$efi."|".$description."|-".$units."|".$transferDate.",";
			}
			
 
			//Move count to
			if ($warehouseTo == "Adjust Manual Count"){
				$criteria->name = $warehouseTo;
				$criteria->efi = $efi;
				$warehouseCount = $this->getWarehouseCountFast($criteria);
				$CurrentUnitQty = (int)($warehouseCount ? $warehouseCount : 0);
				
				$warehouseCountData->warehouseName = $warehouseTo;
				$warehouseCountData->efi = $efi;
				$warehouseCountData->currentQty = $CurrentUnitQty;
				$warehouseCountData->currentUnits = $units;
				$warehouseCountData->qty = (($CurrentUnitQty)+($units));
				$warehouseCountData->transactionDate = $transferDate;
				$warehouseCountData->movementHistoryID = $id;
				$resultsTo = $this->insertWarehouseCount($warehouseCountData);
				if ($resultsTo->success){
					$results["AMC"] .= $efi." Total Count ".(($CurrentUnitQty)+($units))."<br>";
				}
			}else{
				$warehouseCountData->warehouseName = $warehouseTo;
				$warehouseCountData->efi = $efi;
				$warehouseCountData->qty = $adjustCount;
				$warehouseCountData->transactionDate = $transferDate;
				$warehouseCountData->movementHistoryID = $id;
				$resultsTo = $this->insertWarehouseCount($warehouseCountData);
				if ($resultsTo->success){
					$results["Warehouse"] .= $warehouseTo." ".$efi." Total Count Now Set To ".$adjustCount."<br>";
				}
				$results["Summary"] .= $warehouseTo."|".$efi."|".$description."|".$units."|".$transferDate.",";
			}
		}
		$results["Summary"] = rtrim($results["Summary"],",");
		return $results;
	}
	public function updateSingleWarehouseCount($TransferResults){
		foreach ($TransferResults as $Obj){
			$efi = $Obj["efi"];
			$id = $Obj["id"];
			$transferDate = $Obj["transferDate"];
			$units = (int)$Obj["units"];
			$warehouseFrom = $Obj["warehouseFrom"];
			$warehouseTo = $Obj["warehouseTo"];
			$purchaseOrder = $Obj["purchaseOrder"];
			$siteId = $Obj["siteId"];
			$byAdmin = $Obj["byAdmin"];
			$warehouseCount = 0;
			//Move count from
			$criteria->name = $warehouseFrom;
			$criteria->efi = $efi ;
			$criteria->mostrecent = true;
			$criteria->asOfDate = MySQLDateUpdate($transferDate);
			$warehouseCount = $this->getWarehouseCountFast($criteria);
			$CurrentUnitQty = $warehouseCount;
			
			$warehouseCountData->warehouseName = $warehouseFrom;
			$warehouseCountData->efi = $efi;
			$warehouseCountData->currentQty = $CurrentUnitQty;
			$warehouseCountData->currentUnits = $units;
			$warehouseCountData->qty = (($CurrentUnitQty)-($units));
			$warehouseCountData->transactionDate = MySQLDateUpdate($transferDate);
			$warehouseCountData->movementHistoryID = $id;
			$resultsFrom = $this->insertWarehouseCountRebuild($warehouseCountData);
			if ($resultsFrom->success){
				$results["siteId"] = $siteId;
				$results["installDate"] = $transferDate;
				$results["efi"] = $efi;
				$results["units"] = $units;
				$results["crewId"] = $warehouseFrom;
				$results["remainingUnits"] = (($CurrentUnitQty)-($units));
				$results["toWarehouse"] = $warehouseTo;
				$results["byAdmin"] = $byAdmin;
				$results["summary"] = $siteId." ".$transferDate. " ".$warehouseFrom." installed ".$units." units of ".$efi." from ".$CurrentUnitQty." to ".(($CurrentUnitQty)-($units))." units";
				$results["From"] .= $efi." Total Count ".(($CurrentUnitQty)-($units));
				$results["FromWarehouseChange"] .= $warehouseFrom." ".$efi." From ".$CurrentUnitQty." units to ".(($CurrentUnitQty)-($units))." units";
			}
 
			//Move count to
			$criteria->name = $warehouseTo;
			$criteria->efi = $efi ;
			$criteria->mostrecent = true;
			$criteria->asOfDate = MySQLDateUpdate($transferDate);
			$warehouseCount = $this->getWarehouseCountFast($criteria);
			$CurrentUnitQty = (int)($warehouseCount ? $warehouseCount : 0);
			
			$warehouseCountData->warehouseName = $warehouseTo;
			$warehouseCountData->efi = $efi;
			$warehouseCountData->currentQty = $CurrentUnitQty;
			$warehouseCountData->currentUnits = $units;
			$warehouseCountData->qty = (($CurrentUnitQty)+($units));
			$warehouseCountData->transactionDate = MySQLDateUpdate($transferDate);
			$warehouseCountData->movementHistoryID = $id;
			$resultsTo = $this->insertWarehouseCountRebuild($warehouseCountData);
			if ($resultsTo->success){
				$results["To"] .= $efi." Total Count ".(($CurrentUnitQty)+($units));
				$results["ToWarehouseChange"] .= $warehouseTo." ".$efi." From ".$CurrentUnitQty." units to ".(($CurrentUnitQty)+($units))." units";
			}
		}
		return $results;
	}
	
    private static $UPDATE_DISPLAYORDER_SQL = "
        UPDATE	ghs_inventory_warehouse_locations
        SET		GHSInventoryWareHouseLocations_DisplayOrderID = ?,
				GHSInventoryWareHouseLocations_ActiveID = ?
		WHERE	GHSInventoryWareHouseLocations_ID = ?";

    public function updateDisplayOrder($id,$displayOrderId,$activeId,$adminId){
		$record = $this->mapToRecord($data);
        $sqlString = self::$UPDATE_DISPLAYORDER_SQL;
        $sqlBinding = "iii";
        $sqlParams = array($displayOrderId,$activeId,$id);
		$updateResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
		if ($updateResponse->error){
			trigger_error("WarehouseProvider.updateDisplayOrder failed to update: ".$updateResponse->error, E_USER_ERROR);
		}
		//return $sqlString.$displayOrderId.$efi;
        return $updateResponse;
    }

    private static $UPDATE_SQL = "
        UPDATE ghs_inventory_warehouse_locations
        SET
			GHSInventoryWareHouseLocations_DisplayOrderID = ?,
			GHSInventoryWareHouseLocations_Name = ?,
			GHSInventoryWareHouseLocations_Description = ?,
			GHSInventoryWareHouseLocations_LinkedToEmployeeID = ?,
			GHSInventoryWareHouseLocations_LinkedToSecondEmployeeID = ?,
			GHSInventoryWareHouseLocations_ActiveID = ?,			
			GHSInventoryWareHouseLocations_ExcludeFromCountID = ?,
			GHSInventoryWareHouseLocations_StartDate = ?,
			GHSInventoryWareHouseLocations_EndDate = ?,
			GHSInventoryWareHouseLocations_Lead = ?,
			GHSInventoryWareHouseLocations_BulbRateNotTiedToClosingRate  = ?,
			GHSInventoryWareHouseLocations_OutsideGHS  = ?
		WHERE GHSInventoryWareHouseLocations_ID = ?";

    public function update(WarehouseRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "issiiiissiiii";
            $sqlParams = array(
				$record->displayOrderId,
				$record->name,
				$record->description,
				$record->linkedToEmployeeId,
				$record->linkedToSecondEmployeeId,
				$record->activeId,
				$record->excludeFromCountId,
				MySQLDateUpdate($record->startDate),
				MySQLDateUpdate($record->endDate),
				$record->lead,
				$record->bulbRateNotTiedToClosingRate,
				$record->outsideGHS,
                $record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("WarehouseProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

    private static $INSERT_SQL = "
		INSERT INTO ghs_inventory_warehouse_locations
		(
			GHSInventoryWareHouseLocations_DisplayOrderID,
			GHSInventoryWareHouseLocations_Name,
			GHSInventoryWareHouseLocations_Description,
			GHSInventoryWareHouseLocations_LinkedToEmployeeID,
			GHSInventoryWareHouseLocations_LinkedToSecondEmployeeID,
			GHSInventoryWareHouseLocations_ActiveID,			
			GHSInventoryWareHouseLocations_ExcludeFromCountID,
			GHSInventoryWareHouseLocations_StartDate,
			GHSInventoryWareHouseLocations_EndDate,
			GHSInventoryWareHouseLocations_Lead,
			GHSInventoryWareHouseLocations_BulbRateNotTiedToClosingRate,
			GHSInventoryWareHouseLocations_OutsideGHS
		) VALUES (
			?,?,?,?,?,?,?,?,?,?,?,?
		)";

    public function add(ProductRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "issiiiissiii";
        $sqlParam = array(
		
            $record->displayOrderId,
            $record->name,
            $record->description,
            $record->linkedToEmployeeId,
            $record->linkedToSecondEmployeeId,
            $record->activeId,
            $record->excludeFromCountId,
            MySQLDateUpdate($record->startDate),
            MySQLDateUpdate($record->endDate),
            $record->lead,
			$record->bulbRateNotTiedToClosingRate,
			$record->outsideGHS
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("WarehouseProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

    private static $INSERT_COMMISSION_SQL = "
		INSERT INTO ghs_inventory_warehouse_commissions
		(
			GHSInventoryWareHouseCommissions_LockedDate,
			GHSInventoryWareHouseCommissions_CrewID,
			GHSInventoryWareHouseCommissions_ES,
			GHSInventoryWareHouseCommissions_HEACount,
			GHSInventoryWareHouseCommissions_YTDHEACount,
			GHSInventoryWareHouseCommissions_BilledContracts,
			GHSInventoryWareHouseCommissions_YTDBilledContracts,
			GHSInventoryWareHouseCommissions_ConvRate,
			
			GHSInventoryWareHouseCommissions_YTDClosingRate,
			GHSInventoryWareHouseCommissions_ContractsOriginal,
			GHSInventoryWareHouseCommissions_ContractsFinal,
			GHSInventoryWareHouseCommissions_ContractsCommission,
			GHSInventoryWareHouseCommissions_ContractsCommissionPrior,
			GHSInventoryWareHouseCommissions_NatGridBilled,
			GHSInventoryWareHouseCommissions_WMECOBilled,
			
			GHSInventoryWareHouseCommissions_BGASBilled,
			GHSInventoryWareHouseCommissions_BGASAmount,
			GHSInventoryWareHouseCommissions_BulbVisit,
			GHSInventoryWareHouseCommissions_BulbCount,
			GHSInventoryWareHouseCommissions_BulbAvg,			

			GHSInventoryWareHouseCommissions_BulbFactor,
			GHSInventoryWareHouseCommissions_BulbCommission,
			GHSInventoryWareHouseCommissions_YTDCommissionEligibilty,
			GHSInventoryWareHouseCommissions_YTDCommissionEligibleByDate,
			GHSInventoryWareHouseCommissions_YTDBulbCommissionEligibleByBulb,
			
			GHSInventoryWareHouseCommissions_YTDBulbCount,
			GHSInventoryWareHouseCommissions_YTDBulbVisit,
			GHSInventoryWareHouseCommissions_YTDBulbPerVisit,
			GHSInventoryWareHouseCommissions_BulbBonus,
			GHSInventoryWareHouseCommissions_BulbFactorProposed,
			
			GHSInventoryWareHouseCommissions_BulbCommissionProposed,
			GHSInventoryWareHouseCommissions_BulbFactorPrior,
			GHSInventoryWareHouseCommissions_BulbCommissionPrior,
			GHSInventoryWareHouseCommissions_TotalCommission,
			GHSInventoryWareHouseCommissions_AddedByAdmin
		) VALUES (
			?,?,?,?,?,?,?,?,
			?,?,?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?
		)";
	public function commissionLock($recordData, $adminUserId){
        $sqlString = self::$INSERT_COMMISSION_SQL;
        $sqlBinding = "sssiiiiidddddiiidiiiddsiiiiiiddddds";
			
		$record->CrewID = $recordData->CrewID;
		$record->ES = $recordData->ES;
		foreach ($recordData->DateData as $MonthYear=>$MonthYearInfo){
			$sqlParam = null;
			$record->LockedDate = $MonthYear."-01";
			$sqlParam = array(
				$record->LockedDate,
				$record->CrewID,
				$record->ES
			);
			foreach ($MonthYearInfo as $FieldName=>$FieldValue){
				$sqlParam[] = $FieldValue;
			}
			$sqlParam[] = $adminUserId;
			$insertResponse = new ExecuteResponseRecord();
			MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
			if ($insertResponse->success && $insertResponse->affectedRows){
				$insertResponses["success"] = $insertResponse;
			} else {
				$insertResponses["error"] = $insertResponse;
				trigger_error("WarehouseProvider.commissionLock failed to insert: ".$insertResponse->error, E_USER_ERROR);
			}
		}
		return $insertResponses;
	}
	
	private static $SELECT_COMMISSION_COLUMNS = "	
		SELECT * ";
	private static $FROM_COMMISSION_CLAUSE = "
		FROM ghs_inventory_warehouse_commissions WHERE GHSInventoryWareHouseCommissions_LockedDate BETWEEN ? AND ? ORDER BY GHSInventoryWareHouseCommissions_ES, GHSInventoryWareHouseCommissions_LockedDate";
	
    public function getCommissions($criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COMMISSION_COLUMNS;
        $fromTableClause = self::$FROM_COMMISSION_CLAUSE;

		$filterString = "";
        $filterBinding = "ss";
        $filterParams = array($criteria->startDate,$criteria->endDate);

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString.print_r($filterParams,true);
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$mostRecentString.$fromTableClause.$filterString.$orderBy.$limiter;
				//echo $pageSqlString;
				//print_pre($filterParams);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = $row;
                    }
                } else {
                    trigger_error("WarehouseProvider.getCommissions failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("WarehouseProvider.getCommissions failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
    private static $INSERT_COMMISSIONNOTE_SQL = "
		INSERT INTO ghs_inventory_warehouse_commissions_notes
		(
			GHSInventoryWareHouseCommissionsNotes_FiscalYear,
			GHSInventoryWareHouseCommissionsNotes_Note,
			GHSInventoryWareHouseCommissionsNotes_AddedByAdmin
		) VALUES (
			?,?,?
		) ON DUPLICATE KEY UPDATE GHSInventoryWareHouseCommissionsNotes_Note=VALUES(GHSInventoryWareHouseCommissionsNotes_Note), GHSInventoryWareHouseCommissionsNotes_AddedByAdmin=?";
	public function commissionNoteInsert($NoteDate,$Note,$adminUserId){
        $sqlString = self::$INSERT_COMMISSIONNOTE_SQL;
        $sqlBinding = "ssss";
		$asOfText = $adminUserId." at ".date("m/d/Y g:ia");

		$sqlParam = array($NoteDate,$Note,$asOfText,$asOfText);
		$insertResponse = new ExecuteResponseRecord();
		MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
		if ($insertResponse->success && $insertResponse->affectedRows){
			$insertResponses["success"] = $insertResponse;
		} else {
			$insertResponses["error"] = $insertResponse;
			trigger_error("WarehouseProvider.commissionNoteInsert failed to insert: ".$insertResponse->error, E_USER_ERROR);
		}
		return $insertResponses;
	}
	
	private static $SELECT_COMMISSIONNOTES_COLUMNS = "	
		SELECT * ";
	private static $FROM_COMMISSIONNOTES_CLAUSE = "
		FROM ghs_inventory_warehouse_commissions_notes WHERE GHSInventoryWareHouseCommissionsNotes_FiscalYear=?";
	
    public function getCommissionNotes($NoteDate){
        $paginationSelectString = self::$SELECT_COMMISSIONNOTES_COLUMNS;
        $fromTableClause = self::$FROM_COMMISSIONNOTES_CLAUSE;

        $filterBinding = "s";
        $filterParams = array($NoteDate);

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
		//echo $countSqlString.print_r($filterParams,true);
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);
        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause;
				//echo $pageSqlString;
				//print_pre($filterParams);
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = $row;
                    }
                } else {
                    trigger_error("WarehouseProvider.getCommissionsNotes failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("WarehouseProvider.getCommissionsNotes failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }
        return $result;
    }
	public function countadjust($transferInfo, $adminUserId){
		
		$TransferDate = MySQLDateUpdate($transferInfo["TransferDate"]);
		$WarehouseStart = $transferInfo["WarehouseFromLocation"];
		$MovementType = "Transfer";
		$i = 0;
		foreach ($transferInfo as $key=>$val){
			if (substr($key,0,6) == "Total_"){
				$key = str_replace("Total_","",$key);
				$key = str_replace("_",".",$key);
				
				//Get Current inventory level to decide if moving in or out of Adjust Manual Count
				$criteria = new stdClass();
				$criteria->name = $WarehouseStart;
				$criteria->efi = $key;
				$warehouseCount = $this->getWarehouseCountFast($criteria);
				$CurrentUnitQty = (int)$warehouseCount;
				if ($CurrentUnitQty == $val){
					$units = 0;
					$WarehouseFrom = $WarehouseStart;
					$WarehouseTo = $WarehouseStart;
				}else{
					if ($CurrentUnitQty < $val){
						$units = ($val-$CurrentUnitQty);
						$WarehouseFrom = "Adjust Manual Count";
						$WarehouseTo = $WarehouseStart;
					}
					if ($CurrentUnitQty > $val){
						$units = ($CurrentUnitQty-$val);
						$WarehouseFrom = $WarehouseStart;
						$WarehouseTo = "Adjust Manual Count";
					}
					
				}
				if (!$units || $units === 0){
					$resultInfo[$i]["narrative"] = $WarehouseStart." ".$key." Started at ".$CurrentUnitQty." and need to be ".$val." so we did not move any units To or From ".$WarehouseFrom;
				}else{
					$record = new stdClass();
					$resultInfo[$i]["narrative"] = $WarehouseStart." ".$key." Started at ".$CurrentUnitQty." and need to be ".$val." so we move ".$units." From ".$WarehouseFrom." To ".$WarehouseTo;
					$record->efi = $key;
					$record->transferDate = $TransferDate;
					$record->units = $units;
					$record->movementType = $MovementType;
					$record->warehouseFrom = $WarehouseFrom;
					$record->warehouseTo = $WarehouseTo;
					$record->purchaseOrder = "";
					$record->adjustcount = (int)$val;
					$resultInfo[$i]["transferRecord"] = $record;
				}

				$i++;
			}
		}
		if ($i < 1){
			$resultInfo["NoProduct"] = true;
		}
		return $resultInfo;
	
	}
	
	
    private static $INSERT_WAREHOUSEFILLQTY_SQL = "
        INSERT INTO ghs_inventory_warehouse_fillqty
        (
            GHSInventoryWarehouseFillQty_WarehouseName,
            GHSInventoryWarehouseFillQty_EFI,
            GHSInventoryWarehouseFillQty_Qty,
            GHSInventoryWarehouseFillQty_AsOfDate,
            GHSInventoryWarehouseFillQty_ByAdmin
		) VALUES (
            ?,?,?,?,?
        ) ON DUPLICATE KEY UPDATE
            GHSInventoryWarehouseFillQty_Qty=VALUES(GHSInventoryWarehouseFillQty_Qty),
			GHSInventoryWarehouseFillQty_AsOfDate=VALUES(GHSInventoryWarehouseFillQty_AsOfDate)
		";
	
	public function setfillqty($setfillqtyInfo, $adminUserId){
		
		$asOfDate = MySQLDateUpdate($setfillqtyInfo["TransferDate"]);
		$WarehouseName = $setfillqtyInfo["WarehouseFromLocation"];
		$i = 0;
		foreach ($setfillqtyInfo as $key=>$val){
			if (substr($key,0,6) == "Total_"){
				$key = str_replace("Total_","",$key);
				$key = str_replace("_",".",$key);
				
				//get efi description
				$sqlString = "SELECT GHSInventoryProducts_Description as description FROM ghs_inventory_products WHERE 1 = 1 AND GHSInventoryProducts_EFI = ?";
				$filterBinding = "s";
				$filterParams = array($key);
				$queryResponse = new QueryResponseRecord();
				$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
				foreach ($sqlResult as $row){
					// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
					$description = $row["description"];
				}
				$record = new stdClass();
				$record->efi = $key;
				$record->description = $description;
				$record->asOfDate = $asOfDate;
				$record->qty = (int)$val;
				$record->warehouseName = $WarehouseName;
				$resultInfo[$i]["setFillQtyRecord"] = $record;
				$sqlString = self::$INSERT_WAREHOUSEFILLQTY_SQL;
				$sqlBindings = "ssiss";
				$sqlParams = array(
					$record->warehouseName,
					$record->efi,
					$record->qty,
					$record->asOfDate = $asOfDate,
					$adminUserId
				);

				$insertResponse = new ExecuteResponseRecord();
				$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
				$resultInfo[$i]["insertResponse"] = $insertResponse;
				
				//get efi description
				$sqlString = "SELECT GHSInventoryProducts_Description as description FROM ghs_inventory_products WHERE 1 = 1 AND GHSInventoryProducts_EFI = ?";
				$filterBinding = "s";
				$filterParams = array($record->efi);
				$queryResponse = new QueryResponseRecord();
				$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
				foreach ($sqlResult as $row){
					// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
					$description = $row["description"];
				}

				
				$resultInfo["warehouse"] = str_replace("_","",$record->warehouseName);
				$resultInfo["summary"] = $resultInfo["summary"].$record->efi." set to ".$record->qty."<br>";
				$resultInfo["details"] = $resultInfo["details"].$record->efi."|".$description."|".$record->qty.",";
				$i++;
			}
		}
		$resultInfo["details"] = rtrim($resultInfo["details"],",");
		return $resultInfo;
	}
	private static $SELECT_AUTOFILL_COLUMNS = "
	SELECT
            GHSInventoryWarehouseFillQty_WarehouseName as warehouseName,
            GHSInventoryWarehouseFillQty_EFI as efi,
            GHSInventoryWarehouseFillQty_Qty as qty,
            GHSInventoryWarehouseFillQty_AsOfDate as asOfDate,
            GHSInventoryWarehouseFillQty_ByAdmin as byAdmin";

    private static $FROM_AUTOFILLTABLE_CLAUSE = "
        FROM ghs_inventory_warehouse_fillqty WHERE 1 = 1";

    private static $FILTER_ON_AUTOFILLNAME = "
        AND GHSInventoryWarehouseFillQty_WarehouseName = ?";
		
    public function getfillqty($criteria){

		$paginationSelectString = self::$SELECT_AUTOFILL_COLUMNS;
        $fromTableClause = self::$FROM_AUTOFILLTABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();
		
		if ($criteria->warehouseName){
			$filterString = self::$FILTER_ON_AUTOFILLNAME;
			$filterBinding = "s";
			$filterParams[] = $criteria->warehouseName;
		}
		
		// get the page
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString;
		//echo $pageSqlString;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("WarehouseProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }
	
    private static $INSERT_WAREHOUSEPHYSICALCOUNT_SQL = "
        INSERT INTO ghs_inventory_warehouse_physicalcount
        (
            GHSInventoryWarehousePhysicalCount_CrewID,
            GHSInventoryWarehousePhysicalCount_EFI,
            GHSInventoryWarehousePhysicalCount_Description,
            GHSInventoryWarehousePhysicalCount_QTY,
            GHSInventoryWarehousePhysicalCount_CalculatedCount,
            GHSInventoryWarehousePhysicalCount_AsOfDate,
            GHSInventoryWarehousePhysicalCount_ByAdmin
		) VALUES (
            ?,?,?,?,?,?,?
        )";
	public function physicalcount($phsyicalcountInfo,$adminId){
		$asOfDate = MySQLDateUpdate($phsyicalcountInfo["TransferDate"]);
		$WarehouseName = $phsyicalcountInfo["WarehouseFromLocation"];
		$i = 0;
		foreach ($phsyicalcountInfo as $thiskey=>$val){
			if (substr($thiskey,0,6) == "Total_"){
				$thiskey = str_replace("Total_","",$thiskey);
				$thiskey = str_replace("_",".",$thiskey);
				$keyandcount = explode("QQQ",$thiskey);
				$key = $keyandcount[0];
				$priorCount = ($keyandcount[1] ? $keyandcount[1] : 0);
				//get efi description
				$sqlString = "SELECT GHSInventoryProducts_Description as description FROM ghs_inventory_products WHERE 1 = 1 AND GHSInventoryProducts_EFI = ?";
				$filterBinding = "s";
				$filterParams = array($key);
				$queryResponse = new QueryResponseRecord();
				$sqlResult = MySqliHelper::get_result($this->mysqli, $sqlString, $filterBinding, $filterParams, $queryResponse);
				foreach ($sqlResult as $row){
					// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
					$description = $row["description"];
				}
				$record = new stdClass();
				$record->efi = $key;
				$record->description = $description;
				$record->asOfDate = $asOfDate;
				$record->qty = (int)$val;
				$record->priorCount = $priorCount;
				$record->warehouseName = $WarehouseName;
				$resultInfo[$i]["physicalCountRecord"] = $record;
				$sqlString = self::$INSERT_WAREHOUSEPHYSICALCOUNT_SQL;
				$sqlBindings = "sssiiss";
				$sqlParams = array(
					$record->warehouseName,
					$record->efi,
					$record->description,
					$record->qty,
					$record->priorCount,
					$record->asOfDate = $asOfDate,
					$adminId
				);

				$insertResponse = new ExecuteResponseRecord();
				$result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
				$resultInfo[$i]["insertResponse"] = $insertResponse;
				
				$resultInfo["warehouse"] = str_replace("_","",$record->warehouseName);
				$resultInfo["summary"] = $resultInfo["summary"].$record->efi." physical count ".$record->qty."<br>";
				$resultInfo["details"] = $resultInfo["details"].$record->efi."|".$description."|".$record->qty."|".($priorCount-(int)$val).",";
				$i++;
			}
		}
		$resultInfo["details"] = rtrim($resultInfo["details"],",");
		return $resultInfo;
	}
	private static $SELECT_PHYSICALCOUNT_COLUMNS = "
	SELECT
            GHSInventoryWarehousePhysicalCount_CrewID as warehouseName,
            GHSInventoryWarehousePhysicalCount_EFI as efi,
            GHSInventoryWarehousePhysicalCount_QTY as qty,
            GHSInventoryWarehousePhysicalCount_CalculatedCount as calculatedCount,
            GHSInventoryWarehousePhysicalCount_Description as description,
            GHSInventoryWarehousePhysicalCount_AsOfDate as asOfDate,
            GHSInventoryWarehousePhysicalCount_ByAdmin as byAdmin";

    private static $FROM_PHYSICALCOUNTTABLE_CLAUSE = "
        FROM ghs_inventory_warehouse_physicalcount p1 WHERE 1 = 1";
		
    private static $FROM_PHYSICALCOUNTTABLE_CLAUSEMAX = "
		AND
		GHSInventoryWarehousePhysicalCount_AsOfDate =
			(
				SELECT max(GHSInventoryWarehousePhysicalCount_AsOfDate) 
				FROM ghs_inventory_warehouse_physicalcount p2
				WHERE 
					p2.GHSInventoryWarehousePhysicalCount_CrewID = p1.GHSInventoryWarehousePhysicalCount_CrewID
					AND p2.GHSInventoryWarehousePhysicalCount_EFI = p1.GHSInventoryWarehousePhysicalCount_EFI
			)
		";

    private static $FILTER_ON_PHYSICALCOUNTNAME = "
        AND GHSInventoryWarehousePhysicalCount_CrewID = ?";
	private static $GROUP_BY_PHYSICALCOUNTNAME = " GROUP BY `GHSInventoryWarehousePhysicalCount_EFI`,`GHSInventoryWarehousePhysicalCount_CrewID`";	
	
    public function getlastphysicalcount($criteria){

		$paginationSelectString = self::$SELECT_PHYSICALCOUNT_COLUMNS;
        $fromTableClause = self::$FROM_PHYSICALCOUNTTABLE_CLAUSE;
        $filterString = "";
        $filterBinding = "";
        $filterParams = array();
		$groupBy = self::$GROUP_BY_PHYSICALCOUNTNAME;
		$orderBy = " ORDER BY GHSInventoryWarehousePhysicalCount_EFI";
		if ($criteria->warehouseName){
			$filterString .= self::$FILTER_ON_PHYSICALCOUNTNAME;
			$filterBinding = "s";
			$filterParams[] = $criteria->warehouseName;
		}
		$filterString .= self::$FROM_PHYSICALCOUNTTABLE_CLAUSEMAX;		
		// get the page
		$pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$groupBy.$orderBy;
		$queryResponse = new QueryResponseRecord();
		$sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);
		if ($queryResponse->success){
			foreach ($sqlResult as $row){
				// since the select columns is done here, call the mapToRecord in this class rather than in subclasses
				$result->collection[] = (object)$row;
			}
		} else {
			trigger_error("WarehouseProvider.getLastPhysicalCount failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
		}
        return $result;
    }

}
?>