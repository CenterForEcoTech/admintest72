<?php
class OrganizationMemberProvider extends MemberProvider{

    private static $GET_MEMBERID_FROM_PERMALINK = "
        SELECT Members_ID as id
        FROM members
        JOIN organization_info ON OrganizationInfo_MemberID = Members_ID
        WHERE OrganizationInfo_ID = ?";

    public function getMemberIdFromPermalink(PermaLinkRecord $permaLink){
        $memberId = 0;
        $sqlString = self::$GET_MEMBERID_FROM_PERMALINK;
        $sqlBinding = "i";
        $sqlParam = array($permaLink->entityId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        if (count($result)){
            $memberId = $result[0]["id"];
        }
        return $memberId;
    }

    public function getReferredByTypes(){
        return array(); // organizations don't get referred at this time
    }

    public function getReferrers(){
        return array(); // organizations don't get referred at this time
    }

    public function getReferredByMember($memberId){
        return array(); // oganizations don't get referred at this time
    }

    private static $NCES_MATCH_SQL = "
        SELECT OrganizationInfo_ID as id
        FROM organization_info
        WHERE OrganizationInfo_NCESSchoolID = ?";

    private static $NAME_AND_ADDRESS_MATCH_SQL = "
        SELECT OrganizationInfo_ID as id
        FROM organization_info
        WHERE OrganizationInfo_Name = ?
        AND OrganizationInfo_Address = ?
        AND OrganizationInfo_City = ?
        AND OrganizationInfo_State = ?
        AND OrganizationInfo_Zip = ?";

    public function getExistingOrg(OrganizationRecord $orgRecord){
        if ($orgRecord->ein && strlen($orgRecord->ein) == 9){
            // check if parent IRS org exists
            $orgProvider = new OrgProvider($this->mysqli);
            $existingOrg = $orgProvider->getByEIN($orgRecord->ein);
            if ($existingOrg->id){
                return $this->getPublicProfile($existingOrg->id);
            }
        }
        if ($orgRecord->ncesSchoolId && strlen($orgRecord->ncesSchoolId) > 11){
            // check if NCES school exists
            $response = MySqliHelper::getResultWithLogging("OrganizationMemberProvider.getExistingOrg sql error:", $this->mysqli, self::$NCES_MATCH_SQL, "s", array($orgRecord->ncesSchoolId));
            if (count($response->resultArray)){
                return $this->getPublicProfile($response->resultArray[0]["id"]);
            }
        }
        $sqlString = self::$NAME_AND_ADDRESS_MATCH_SQL;
        $sqlBinding = "sssss";
        $sqlParams = array(
            $orgRecord->name,
            $orgRecord->address,
            $orgRecord->city,
            $orgRecord->state,
            $orgRecord->postalCode
        );
        $response = MySqliHelper::getResultWithLogging("OrganizationMemberProvider.getExistingOrg sql error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        if (count($response->resultArray)){
            return $this->getPublicProfile($response->resultArray[0]["id"]);
        }
        return $orgRecord;
    }

    public static $INSERT_ORGANIZATION_INFO_SQL = "
		INSERT INTO organization_info (
			OrganizationInfo_MemberID,
			OrganizationInfo_Name,
			OrganizationInfo_Address,
			OrganizationInfo_City,
			OrganizationInfo_State,

			OrganizationInfo_Zip,
			OrganizationInfo_Phone,
			OrganizationInfo_Website,
			OrganizationInfo_ContactFirstName,
			OrganizationInfo_ContactLastName,

			OrganizationInfo_ContactTitle,
			OrganizationInfo_ContactPhone,
			OrganizationInfo_ContactEmail,
			OrganizationInfo_ContactEmailConfirm,
			OrganizationInfo_Country,

			OrganizationInfo_Lat,
			OrganizationInfo_Long,
			OrganizationInfo_CityGridIDName,
			OrganizationInfo_CityGridID,
			OrganizationInfo_Neighborhood,

			OrganizationInfo_OfficialName,
			OrganizationInfo_RegisteredReferralID,
			OrganizationInfo_CurrentReferralID,

			OrganizationInfo_DifferentNameThanFiscalSponsor,
			OrganizationInfo_EIN,
			OrganizationInfo_FiscalSponsorEIN,
			OrganizationInfo_NCESSchoolID,

			OrganizationInfo_CreatedBy,
			OrganizationInfo_CreatedByAdminID
		) VALUES (
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,
			?,?,?,?,
			?,?
		)";

    public function addOrganization(CharityLookup $charityLookup, OrganizationRecord $companyRecord, OrganizationMemberRecord $memberRecord = null){
        $memberId = 0;
        if (isset($memberRecord) && $memberRecord->id > 0){
            $memberId = $memberRecord->id;
        }
        $sqlString = self::$INSERT_ORGANIZATION_INFO_SQL;
        $sqlBindings = "issss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sssis";
        $sqlBindings .= "sii";
        $sqlBindings .= "ssss";
        $sqlBindings .= "ii";
        $nullableEin = strlen($companyRecord->ein) ? $companyRecord->ein : null;
        $sqlParams = array(
            $memberId,
            $companyRecord->name,
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,

            $companyRecord->postalCode,
            $companyRecord->phone,
            $companyRecord->website,
            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,
            $companyRecord->country."",

            $companyRecord->lat."",
            $companyRecord->long."",
            $companyRecord->cityGridIdName."",
            $companyRecord->cityGridProfileId + 0,
            $companyRecord->neighborhood."",

            $companyRecord->officialname."",
            $companyRecord->registeredreferralid."",
            $companyRecord->currentreferralid."",

            $companyRecord->differentnamethanfiscalsponsor,
            $nullableEin,
            $companyRecord->fiscalSponsorEin,
            $companyRecord->ncesSchoolId."",

            getMemberId(),
            getAdminId()
        );
        $sqlResponse = MySqliHelper::executeWithLogging("OrganizationMemberProvider.addOrganization: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if ($sqlResponse->success){
            if (is_numeric($sqlResponse->insertedId) && $sqlResponse->insertedId > 0){
                $companyRecord->id = $sqlResponse->insertedId;
                $result = $sqlResponse->insertedId;
                $permalinkProvider = new PermaLinkProviderImpl($this->mysqli);
                $addPermalinkRequest = new AddPermaLinkRequest();
                $addPermalinkRequest->setEntityId($sqlResponse->insertedId);
                $addPermalinkRequest->setEntityName(PermaLinkRecord::$ORG_ENTITY_NAME);
                $permalinkProvider->tryAddPermalink($addPermalinkRequest, 10);
            }

            if (strlen($companyRecord->fiscalSponsorEin) > 0){
                $this->checkAndAdd($companyRecord->fiscalSponsorEin, $charityLookup);
            }
        }

        return $sqlResponse;
    }

    /**
     * Checks to see if we have an org whose ein is this value, and if not, adds it to our system.
     * This method only adds parent orgs. To add a fiscally sponsored org, use addOrganization.
     * @param $einValue
     * @param $charityLookup
     * @return orgId
     */
    public function checkAndAdd($einValue, CharityLookup $charityLookup){

        if (strlen($einValue) > 0){
            // make sure parent org exists, if not, create it
            $orgProvider = new OrgProvider($this->mysqli);
            $existingOrg = $orgProvider->getByEIN($einValue);
            if (!is_numeric($existingOrg->id) || $existingOrg->id <= 0){
                // parent org doesn't exist. Add it now
                $results = $charityLookup->getPublicCharity($einValue);
                /* @var $results CharitySearchObject */
                if ($results->name){
                    $fiscalSponsorRecord = new OrganizationRecord();
                    $results->mapToOrgRecord($fiscalSponsorRecord);
                    $fiscalSponsorRecord->primaryContact = new ContactRecord();
                    $result = $this->addOrganization($charityLookup, $fiscalSponsorRecord);
                    if ($result->success){
                        return $result->insertedId;
                    }
                }
            }
            return $existingOrg->id;
        }
    }

    public static $UPDATE_ORGANIZATION_INFO_AS_MEMBER_SQL = "
		UPDATE organization_info
        SET
            OrganizationInfo_MemberID = ?,
            OrganizationInfo_Name = ?,
            OrganizationInfo_Address = ?,
            OrganizationInfo_City = ?,
            OrganizationInfo_State = ?,

            OrganizationInfo_Zip = ?,
            OrganizationInfo_Phone = ?,
            OrganizationInfo_Website = ?,
            OrganizationInfo_ContactFirstName = ?,
            OrganizationInfo_ContactLastName = ?,

            OrganizationInfo_ContactTitle = ?,
            OrganizationInfo_ContactPhone = ?,
            OrganizationInfo_ContactEmail = ?,
            OrganizationInfo_ContactEmailConfirm = ?,
            OrganizationInfo_Country = ?,

            OrganizationInfo_Lat = ?,
            OrganizationInfo_Long = ?,
            OrganizationInfo_RegisteredReferralID = ?,
            OrganizationInfo_CurrentReferralID = ?,

            OrganizationInfo_UpdatedBy = ?,
            OrganizationInfo_UpdatedDate = CURRENT_TIMESTAMP,
            OrganizationInfo_UpdatedByAdminID = ?
        WHERE OrganizationInfo_ID = ?";

    public function updateOrganizationAsMember(OrganizationRecord $companyRecord, OrganizationMemberRecord $memberRecord = null){
        $sqlString = self::$UPDATE_ORGANIZATION_INFO_AS_MEMBER_SQL;
        $sqlBindings = "issss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "sssss";
        $sqlBindings .= "ssii";
        $sqlBindings .= "iii";
        $sqlParams = array(
            $memberRecord->id,
            $companyRecord->name,
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,

            $companyRecord->postalCode,
            $companyRecord->phone,
            $companyRecord->website,
            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,
            $companyRecord->country."",

            $companyRecord->lat."",
            $companyRecord->long."",
            $companyRecord->registeredreferralid."",
            $companyRecord->currentreferralid."",

            getMemberId(),
            getAdminId(),
            $companyRecord->id
        );
        $sqlResponse = new ExecuteResponseRecord();
        $sqlResponse = MySqliHelper::executeWithLogging("OrganizationMemberProvider.updateOrganizationAsMember: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $sqlResponse->success;
    }

    public static $UPDATE_ORGANIZATION_DESCRIPTION_SQL = "
		UPDATE organization_info
			SET
			    OrganizationInfo_Description = ?,
				OrganizationInfo_DescriptionShow = ?,
				OrganizationInfo_UpdatedBy = ?,
				OrganizationInfo_UpdatedDate = CURRENT_TIMESTAMP,
				OrganizationInfo_UpdatedByAdminID = ?
			WHERE OrganizationInfo_ID = ?";

    public function updateOrganizationDescription(OrganizationRecord $companyRecord){
        $descriptionShow = $companyRecord->isDescriptionPublic ? "yes" : null;
        $sqlString = self::$UPDATE_ORGANIZATION_DESCRIPTION_SQL;
        $sqlBindings = "ssiii";
        $sqlParams = array(
            $companyRecord->rawDescriptionText,
            $descriptionShow,
            getMemberId(),
            getAdminId(),
            $companyRecord->id
        );
        $sqlResponse = new ExecuteResponseRecord();
        $sqlResponse = MySqliHelper::executeWithLogging("OrganizationMemberProvider.updateOrganizationDescription: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $sqlResponse->success;
    }

    public function addMember(OrganizationMemberRecord $memberRecord, OrganizationRecord $companyRecord, CharityLookup $charityLookup){
        $addMemberResult = $this->addMemberBase($memberRecord);
        if (is_numeric($addMemberResult) && $addMemberResult > 0){
            $memberRecord->id = $addMemberResult;
            $orgAlreadyExists = $companyRecord->id > 0;
            if (!$orgAlreadyExists && strlen($companyRecord->ein) > 0 && strlen($companyRecord->differentnamethanfiscalsponsor) == 0){
                // not fiscally sponsored
                $orgProvider = new OrgProvider($this->mysqli);
                $existingOrg = $orgProvider->getByEIN($companyRecord->ein);
                if (is_numeric($existingOrg->id) && $existingOrg->id > 0){
                    $orgAlreadyExists = true;
                    $companyRecord->id = $existingOrg->id;
                }
            }
            if ($orgAlreadyExists){
                $this->updateOrganizationAsMember($companyRecord, $memberRecord);
            } else {
                $result = $this->addOrganization($charityLookup, $companyRecord, $memberRecord);
            }
        }

        return $memberRecord;
    }

    public static $UPDATE_ORGANIZATION_INFO_SQL = "
		UPDATE organization_info
			SET
				OrganizationInfo_Website = ?,
				OrganizationInfo_ContactFirstName = ?,
				OrganizationInfo_ContactLastName = ?,

				OrganizationInfo_ContactTitle = ?,
				OrganizationInfo_ContactPhone = ?,
				OrganizationInfo_ContactEmail = ?,
				OrganizationInfo_ContactEmailConfirm = ?,

				OrganizationInfo_Description = ?,
				OrganizationInfo_DescriptionShow = ?,
				OrganizationInfo_UpdatedBy = ?,
				OrganizationInfo_UpdatedDate = CURRENT_TIMESTAMP,
				OrganizationInfo_UpdatedByAdminID = ?
		WHERE OrganizationInfo_MemberID = ?";
    /*
                 OrganizationInfo_Address = ?,
                 OrganizationInfo_City = ?,
                 OrganizationInfo_State = ?,

                 OrganizationInfo_Zip = ?,
                 OrganizationInfo_Phone = ?,
                 */
    /*				OrganizationInfo_Country = ?,

                    OrganizationInfo_Lat = ?,
                    OrganizationInfo_Long = ?,
                    OrganizationInfo_CityGridIDName = ?,
                    OrganizationInfo_CityGridID = ?,
                    OrganizationInfo_Neighborhood = ?
    */

    public function updateMember(MemberRecord $memberRecord, OrganizationRecord $companyRecord = null){
        $updateResult = $this->updateMemberBase($memberRecord);

        $sqlString = self::$UPDATE_ORGANIZATION_INFO_SQL;
        $sqlBindings = "sss";
        $sqlBindings .= "ssss";
        $sqlBindings .= "ssiii";
        $descriptionShow = $companyRecord->isDescriptionPublic ? "yes" : null;
        $sqlParams = array(
            /* not allowed to change address
            $companyRecord->address,
            $companyRecord->city,
            $companyRecord->state,

            $companyRecord->postalCode,
            $companyRecord->phone,
            */
            $companyRecord->website,
            $companyRecord->primaryContact->firstname,
            $companyRecord->primaryContact->lastname,

            $companyRecord->primaryContact->title,
            $companyRecord->primaryContact->phone,
            $companyRecord->primaryContact->email,
            $companyRecord->primaryContact->emailConfirm,
            $companyRecord->rawDescriptionText,
            $descriptionShow,
            getMemberId(),
            getAdminId(),
            $memberRecord->id
        );
        /*
                            $companyRecord->country,

                            $companyRecord->lat,
                            $companyRecord->long,
                            $companyRecord->cityGridIdName,
                            $companyRecord->cityGridProfileId,
                            $companyRecord->neighborhood,
        */
        $sqlResponse = MySqliHelper::executeWithLogging("OrganizationMemberProvider.updateMember: SQL Error: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);

        return $updateResult || $sqlResponse->success;
    }

    private static $GET_PUBLIC_ORG_PROFILE_SQL = "
        SELECT
            OrganizationInfo_MemberID as memberId,
            OrganizationInfo_ID as id,
			OrganizationInfo_Name as name,
			OrganizationInfo_Address as address,
			OrganizationInfo_City as city,
			OrganizationInfo_State as state,

			OrganizationInfo_Zip as postalCode,
			OrganizationInfo_Phone as phone,
			OrganizationInfo_Website as website,
			OrganizationInfo_ContactFirstName as contactFirstName,
			OrganizationInfo_ContactLastName as contactLastName,

			OrganizationInfo_ContactTitle as contactTitle,
			OrganizationInfo_ContactPhone as contactPhone,
			OrganizationInfo_ContactEmail as contactEmail,
			OrganizationInfo_Country as country,

			OrganizationInfo_DifferentNameThanFiscalSponsor as differentNameThanFiscalSponsor,
			OrganizationInfo_OfficialName as officialName,
			OrganizationInfo_OfficialName as fiscalSponsorName,
			OrganizationInfo_Description as description,
			OrganizationInfo_DescriptionShow as descriptionShow,

			OrganizationInfo_Lat as lat,
			OrganizationInfo_Long as `long`,
			OrganizationInfo_CityGridIDName as cityGridIdName,
			OrganizationInfo_CityGridID as cityGridProfileId,
			OrganizationInfo_Neighborhood as neighborhood,

			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			OrganizationInfo_EIN as ein,
			OrganizationInfo_FiscalSponsorEIN as fiscalSponsorEin,
			OrganizationInfo_NCESSchoolID as ncesSchoolId,
			(
			    SELECT OrganizationInfo_ID
			    FROM organization_info parent
			    WHERE parent.OrganizationInfo_EIN = o.OrganizationInfo_FiscalSponsorEIN
			    LIMIT 1
			) as parentOrgId,
			(
			    SELECT count(*)
			    FROM organization_info child
			    WHERE o.OrganizationInfo_EIN <> ''
			    AND child.OrganizationInfo_FiscalSponsorEIN = o.OrganizationInfo_EIN
			) as hasChildren,
			OrganizationInfo_RegisteredReferralID as registeredReferralId,
			OrganizationInfo_CurrentReferralID as currentReferralId
		FROM
		    organization_info o
		WHERE
		    OrganizationInfo_ID = ?";

    public function getPublicProfile($orgID){
        $record = new OrganizationRecord();
        $contactRecord = new ContactRecord();
        $record->primaryContact = $contactRecord;
        $sqlString = self::$GET_PUBLIC_ORG_PROFILE_SQL;
        $sqlBindings = "i";
        $sqlParams = array($orgID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            $row = (object)$result[0];
            $this->mapToOrganizationRecord($row, $record, $contactRecord);
            $record->memberId = $row->memberId;
            $record->parentOrgId = $row->parentOrgId;
            $record->hasChildren = $row->hasChildren;
            $record->currentreferralid = $row->currentReferralId;
            $record->registeredreferralid = $row->registeredReferralId;
        }
        return $record;
    }

    private function mapToOrganizationRecord($row, OrganizationRecord $record, ContactRecord $contactRecord){
        $record->id = $row->id;
        $record->name = $row->name;
        $record->address = $row->address;
        $record->city = $row->city;
        $record->state = $row->state;

        $record->postalCode = $row->postalCode;
        $record->phone = $row->phone;
        $record->setURL($row->website);
        //$record->website = $row->website;
        $contactRecord->firstname = $row->contactFirstName;
        $contactRecord->lastname = $row->contactLastName;

        $contactRecord->title = $row->contactTitle;
        $contactRecord->setPhone($row->contactPhone);
        $contactRecord->email = $row->contactEmail;
        $record->country = $row->country;
        $record->officialname = $row->officialName;
        $record->differentNameThanFiscalSponsor = $row->differentNameThanFiscalSponsor;
        $record->fiscalSponsor = ucwords(strtolower($row->fiscalSponsorName));
        $record->isDescriptionPublic = $row->descriptionShow == "yes";
        $record->descriptionText = $row->description;

        $record->lat = $row->lat;
        $record->long = $row->long;
        $record->cityGridIdName = $row->cityGridIdName;
        $record->cityGridProfileId = $row->cityGridProfileId;
        $record->neighborhood = $row->neighborhood;

        $record->pid = $row->pid;
        $record->ein = $row->ein;
        $record->fiscalSponsorEin = $row->fiscalSponsorEin;
        $record->ncesSchoolId = $row->ncesSchoolId;
    }

    private static $PAGINATED_SELECT_COLUMNS = "
        SELECT
            m.OrganizationInfo_MemberID as memberId,
            m.OrganizationInfo_ID as id,
            m.OrganizationInfo_Name as name,
            m.OrganizationInfo_Address as address,
            m.OrganizationInfo_City as city,
            m.OrganizationInfo_State as state,
            m.OrganizationInfo_Zip as postalCode,
            m.OrganizationInfo_Phone as phone,
            m.OrganizationInfo_ContactFirstName as contactFirstName,
            m.OrganizationInfo_ContactLastName as contactLastName,
            m.OrganizationInfo_ContactPhone as contactPhone,
            m.OrganizationInfo_ContactEmail as contactEmail,
            (
                SELECT pid
                FROM permalinks
                WHERE entity_name = 'organization'
                AND entity_id = m.OrganizationInfo_ID
                AND status = 'ACTIVE'
                ORDER BY weight DESC
                LIMIT 1
            ) as pid,
            (
                SELECT count(*)
                FROM event_transaction
                WHERE EventTransaction_OrgID = m.OrganizationInfo_ID
                AND EventTransaction_Status NOT LIKE '%Deleted%'
            ) as numTransactions";

    private static $PAGINATED_FROM_TABLE_CLAUSE = "
        FROM organization_info m
        WHERE 1 = 1";

    private static $FILTER_ON_KEYWORD = "
        AND (
            m.OrganizationInfo_Name LIKE ?
            OR m.OrganizationInfo_Address LIKE ?
            OR m.OrganizationInfo_City LIKE ?
            OR m.OrganizationInfo_State LIKE ?
            OR m.OrganizationInfo_Zip LIKE ?
            OR m.OrganizationInfo_Phone LIKE ?
            OR m.OrganizationInfo_Website LIKE ?
            OR m.OrganizationInfo_ContactFirstName LIKE ?
            OR m.OrganizationInfo_ContactLastName LIKE ?
            OR m.OrganizationInfo_ContactEmail LIKE ?
            OR m.OrganizationInfo_NCESSchoolID LIKE ?
            OR m.OrganizationInfo_EIN LIKE ?
            OR m.OrganizationInfo_FiscalSponsorEin LIKE ?
        )";

    private static $FILTER_ON_ID = "
        AND m.OrganizationInfo_ID = ?";

    private static $FILTER_ON_MEMBER_ID = "
        AND m.OrganizationInfo_MemberID = ?";

    private static $FILTER_ON_CAMPAIGN_ID = "
        AND EXISTS (
            SELECT 1
            FROM tag_association
            WHERE TagAssociation_SourceTable = 'organization_info'
            AND TagAssociation_SourceID = m.OrganizationInfo_ID
            AND TagAssociation_TagListID = ?
            AND TagAssociation_Status IS NULL
        )";

    private static $FILTER_ON_FISCAL_SPONSOR_EIN = "
        AND m.OrganizationInfo_FiscalSponsorEIN = ?";

    /**
     * @param OrganizationProfilesCriteria $criteria
     * @return PagedResult of Venue Object
     */
    public function get(OrganizationProfilesCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$PAGINATED_SELECT_COLUMNS;
        $fromTableClause = self::$PAGINATED_FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();
        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search and pagination
        if ($criteria->keyword){
            $keywordCriteria = "%".$criteria->keyword."%";
            $filterString .= self::$FILTER_ON_KEYWORD;
            $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
            for ($i = 0; $i < $numParams; $i++){
                $filterBinding .= "s";
                $filterParams[] = $keywordCriteria;
            }
        }

        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        }

        if (is_numeric($criteria->memberId)){
            $filterString .= self::$FILTER_ON_MEMBER_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->memberId;
        }

        if (is_numeric($criteria->campaignId)){
            $filterString .= self::$FILTER_ON_CAMPAIGN_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->campaignId;
        }

        if (is_numeric($criteria->fiscalSponsorEin) && strlen($criteria->fiscalSponsorEin) == 9){
            $filterString .= self::$FILTER_ON_FISCAL_SPONSOR_EIN;
            $filterBinding .= "s";
            $filterParams[] = $criteria->fiscalSponsorEin;
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $rowArray){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $row = (object)$rowArray;
                        $record = new OrganizationRecord();
                        $contactRecord = new ContactRecord();
                        $record->primaryContact = $contactRecord;
                        $this->mapToOrganizationRecord($row, $record, $contactRecord);
                        $record->memberId = $row->memberId;
                        $record->numTransactions = $row->numTransactions;
                        $result->collection[] = $record;
                    }
                } else {
                    trigger_error("EventRecordProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("EventRecordProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        return $result;
    }

    private static $ORDER_BY_ID = " ORDER BY m.OrganizationInfo_ID";

    private static $ORDER_BY_BUSINESS = " ORDER BY m.OrganizationInfo_Name";

    private static $ORDER_BY_MEMBER_ID = " ORDER BY m.OrganizationInfo_MemberID";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$ORDER_BY_ID;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "orgId":
                    $orderBy = self::$ORDER_BY_ID." ".$sortOrder;
                    break;
                case "contactInfo":
                    $orderBy = self::$ORDER_BY_BUSINESS." ".$sortOrder;
                    break;
                case "memberId":
                    $orderBy = self::$ORDER_BY_MEMBER_ID." ".$sortOrder;
                    break;
            }
        }
        return $orderBy;
    }
}