<?php
include_once("Helper.php");
include_once("OrgProvider.php");
include_once("PermaLinkProvider.php");
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once("ReferralCodeHelper.php");
include_once($rootFolder."/repository/utils/UrlLinker.php");
include_once($rootFolder."/repository/ServiceAPI.php");

abstract class MemberProvider
{
    /* @var mysqli */
    protected $mysqli;

    public function __construct(mysqli $mysqli){
        $this->mysqli = $mysqli;
    }

    abstract public function getMemberIdFromPermalink(PermaLinkRecord $permaLink);

    /**
     * Given the code string, returns the associated ID if any. Otherwise, returns zero.
     * @param $ThisReferralCode
     * @return int
     */
    public function GetReferralCodeID($ThisReferralCode){
        $referralCode = ReferralCodeHelper::getReferralCodeByName($this->mysqli, $ThisReferralCode);
        if ($referralCode != null){
            return $referralCode->id;
        }
        return 0;
    }

    public function getReferredByMemberId($ThisReferralCode){
        $permaLinkProvider = new PermaLinkProviderImpl($this->mysqli);
        $permaLink = $permaLinkProvider->get($ThisReferralCode);
        return self::getMemberId($this->mysqli, $permaLink);
    }

    public static function getMemberId(mysqli $mysqli, PermaLinkRecord $permaLink){
        $memberId = 0;
        if ($permaLink->entityId){
            switch($permaLink->entityName){
                case PermaLinkRecord::$ORG_ENTITY_NAME:
                    $memberProvider = new OrganizationMemberProvider($mysqli);
                    $memberId = $memberProvider->getMemberIdFromPermalink($permaLink);
                    break;
                case PermaLinkRecord::$INDIVIDUAL_ENTITY_NAME:
                    $memberProvider = new IndividualMemberProvider($mysqli);
                    $memberId = $memberProvider->getMemberIdFromPermalink($permaLink);
                    break;
                case PermaLinkRecord::$MERCHANT_ENTITY_NAME:
                    $memberProvider = new MerchantMemberProvider($mysqli);
                    $memberId = $memberProvider->getMemberIdFromPermalink($permaLink);
                    break;
                default:
                    break;
            }
        }
        return $memberId;
    }

    abstract public function getReferredByTypes();

    abstract public function getReferrers();

    // TODO: remove this function and its implementations if we never use it.
    abstract public function getReferredByMember($memberId);

    public static $INSERT_SQL = "
		INSERT INTO members
		(
			Members_FirstName,
			Members_LastName,
			Members_Email,
			Members_Phone,
			Members_TypeID,
			Members_StartPage)
		SELECT ?,?,?,?,?,?
		FROM DUAL
		WHERE NOT EXISTS (
		  SELECT 1
		  FROM members
		  WHERE '' != ? AND Members_Email = ?
		  OR '' != ? AND Members_Phone = ?
		)";

    protected function addMemberBase(MemberRecord $memberRecord){
        if (!strlen($memberRecord->email) > 3 && !strlen($memberRecord->phone) > 3){
            // at least one item must have a value
            return false;
        }
        $sqlString = self::$INSERT_SQL;
        $sqlBindings = "ssssis";
        $sqlBindings .= "ssss";
        $sqlParams = Array(
            $memberRecord->firstname,
            $memberRecord->lastname,
            $memberRecord->email,
            $memberRecord->phone,
            $memberRecord->typeId,
            $memberRecord->startPage,
            $memberRecord->email,
            $memberRecord->email,
            $memberRecord->phone,
            $memberRecord->phone
        );
        $sqlResponse = MySqliHelper::executeWithLogging("MemberProvider.addMemberBase: Failure inserting members: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        $insertResult = $sqlResponse->insertedId;
        if (is_numeric($insertResult) && $insertResult > 0){
            return $insertResult;
        }

        return false;
    }

    public static $FIND_EXISTING_MATCHES_SQL = "
        SELECT
          Members_ID,
          Members_Email,
          Members_Phone
        FROM members
        WHERE (
          '' != ?
          AND Members_Email = ?
          OR '' != ?
          AND Members_Phone = ?)
        AND Members_ID != ?";

    public static $UPDATE_SQL = "
		UPDATE members
		SET Members_Email = ?,
			Members_FirstName = ?,
			Members_LastName = ?,
			Members_Phone = ?
		WHERE Members_ID = ?";

    protected function updateMemberBase(MemberRecord $memberRecord){
        if (!strlen($memberRecord->email) > 3 && !strlen($memberRecord->phone) > 3){
            // at least one item must have a value; assume entire update is invalid
            return false;
        }
        $sqlString = self::$FIND_EXISTING_MATCHES_SQL;
        $sqlBindings = "ssssi";
        $sqlParams = array(
            $memberRecord->email,
            $memberRecord->email,
            $memberRecord->phone,
            $memberRecord->phone,
            $memberRecord->id
        );
        $existingMismatch = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($existingMismatch)){
            return false; // the update is trying to set either email or phone to one that is used by a different member
        }

        $updateSql = self::$UPDATE_SQL;
        $updateParams = Array(
            $memberRecord->email,
            $memberRecord->firstname,
            $memberRecord->lastname,
            $memberRecord->phone,
            $memberRecord->id
        );
        $updateBindTypes = "ssssi";
        $updateResponse = MySqliHelper::executeWithLogging("MemberProvider.updateMemberBase: Failure updating members: ", $this->mysqli, $updateSql, $updateBindTypes, $updateParams);

        return $updateResponse->success;
    }

    private static $GET_ENTITY_ID_SQL = "
        SELECT
            LOWER(MembersType_Name) as entity_name,
            CASE
                WHEN MembersType_Name = 'Individual' THEN IndividualInfo_ID
                WHEN MembersType_Name = 'Organization' THEN OrganizationInfo_ID
                When MembersType_Name = 'Merchant' THEN MerchantInfo_ID
            END as entity_id,
            CASE
                WHEN MembersType_Name = 'Individual' THEN IndividualInfo_FirstName
                WHEN MembersType_Name = 'Organization' THEN OrganizationInfo_contactFirstName
                When MembersType_Name = 'Merchant' THEN MerchantInfo_contactFirstName
            END as firstName,
            CASE
                WHEN MembersType_Name = 'Individual' THEN IndividualInfo_LastName
                WHEN MembersType_Name = 'Organization' THEN OrganizationInfo_contactLastName
                When MembersType_Name = 'Merchant' THEN MerchantInfo_contactLastName
            END as lastName
        FROM members
        JOIN members_type on Members_TypeID = MembersType_ID
        LEFT JOIN individual_info ON MembersType_Name = 'Individual' AND IndividualInfo_MemberID = Members_ID
        LEFT JOIN organization_info ON MembersType_Name = 'Organization' AND OrganizationInfo_MemberID = Members_ID
        LEFT JOIN merchant_info ON MembersType_Name = 'Merchant' AND MerchantInfo_MemberID = Members_ID
        WHERE Members_ID = ?";

    /**
     * Returns entity_id and entity_name for the specific member
     * @static
     * @param mysqli $mysqli
     * @param $memberId int
     * @return object
     */
    public static function getEntityId(mysqli $mysqli, $memberId){
        $sqlString = self::$GET_ENTITY_ID_SQL;
        $sqlBinding = "i";
        $sqlParam = array( $memberId );
        $sqlResult = MySqliHelper::get_result($mysqli, $sqlString, $sqlBinding, $sqlParam);
        if (count($sqlResult)){
            return (object)$sqlResult[0];
        }
        return null;
    }

}

class IndividualMemberProvider extends MemberProvider{
    private static $GET_MEMBERID_FROM_PERMALINK = "
        SELECT Members_ID as id
        FROM members
        JOIN individual_info ON IndividualInfo_MemberID = Members_ID
        WHERE IndividualInfo_ID = ?";

    public function getMemberIdFromPermalink(PermaLinkRecord $permaLink){
        $memberId = 0;
        $sqlString = self::$GET_MEMBERID_FROM_PERMALINK;
        $sqlBinding = "i";
        $sqlParam = array($permaLink->entityId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        if (count($result)){
            $memberId = $result[0]["id"];
        }
        return $memberId;
    }

    public function getReferredByTypes(){
        return array(); // individuals don't get referred at this time
    }

    public function getReferrers(){
        return array(); // individuals don't get referrred at this time
    }

    public function getReferredByMember($memberId){
        return array(); // individuals don't get referred at this time
    }

    private static $INSERT_MEMBER_PREFERENCES = "
        INSERT INTO member_preferences (
          members_id,
          shareWithOrgs,
          notifyNewEvents,
          createdBy
        ) VALUES (
          ?,?,?,?
        )";

    private static $INSERT_INDIVIDUAL_INFO_SQL = "
		INSERT INTO individual_info (
			IndividualInfo_MemberID,
			IndividualInfo_Email,
			IndividualInfo_Phone)
		VALUES (?,?,?)";

    public function addMember(IndividualMemberRecord $memberRecord, $originatingMemberId = 0){
        $addMemberResult = $this->addMemberBase($memberRecord);
        if (is_numeric($addMemberResult) && $addMemberResult > 0){
            $memberRecord->id = $addMemberResult;

            if (empty($originatingMemberId)){
                $originatingMemberId = $memberRecord->id;
            }

            // add user preferences
            $sqlString = self::$INSERT_MEMBER_PREFERENCES;
            $sqlBindings = "iiii";
            $sqlParams = array(
                $memberRecord->id,
                $memberRecord->shareWithNonprofit ? 1 : 0,
                $memberRecord->notifyNewEvents ? 1 : 0,
                $originatingMemberId
            );
            $sqlResponse = MySqliHelper::executeWithLogging("IndividualMemberProvider.addMember: Failure inserting member_preferences: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
            if ($sqlResponse->success){
                // add basics to individual_info
                $sqlString = self::$INSERT_INDIVIDUAL_INFO_SQL;
                $sqlBindings = "iss";
                $sqlParams = array($memberRecord->id, $memberRecord->email."", $memberRecord->phone."");
                $sqlResponse = MySqliHelper::executeWithLogging("IndividualMemberProvider.addMember: Failure inserting individual_info: ", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
                if ($sqlResponse->success && is_numeric($sqlResponse->insertedId) && $sqlResponse->insertedId > 0){
                    $permalinkProvider = new PermaLinkProviderImpl($this->mysqli);
                    $addPermalinkRequest = new AddPermaLinkRequest();
                    $addPermalinkRequest->setEntityId($sqlResponse->insertedId);
                    $addPermalinkRequest->setEntityName(PermaLinkRecord::$INDIVIDUAL_ENTITY_NAME);
                    $permalinkProvider->tryAddPermalink($addPermalinkRequest, 10);
                }
            }
        }

        return $memberRecord;
    }

    public static $UPDATE_INDIVIDUAL_INFO_SQL = "
		UPDATE individual_info, member_preferences
			SET IndividualInfo_FirstName = ?,
				IndividualInfo_LastName = ?,
				IndividualInfo_Email = ?,
				IndividualInfo_Phone = ?,
				member_preferences.shareWithOrgs = ?,
				member_preferences.notifyNewEvents = ?,
				member_preferences.updatedDate = CURRENT_TIMESTAMP,
				member_preferences.updatedBy = ?
		WHERE IndividualInfo_MemberID = member_preferences.members_id
		AND IndividualInfo_MemberID = ?";

    public function updateMember(IndividualMemberRecord $memberRecord, $updatingMemberId = 0){
        $updateResult = $this->updateMemberBase($memberRecord);
        if (empty($updatingMemberId)){
            $updatingMemberId = $memberRecord->id;
        }

        // update basics to individual_info
        $sqlString = self::$UPDATE_INDIVIDUAL_INFO_SQL;
        $sqlBindings = "ssssiiii";
        $sqlParams = array(
            $memberRecord->firstname,
            $memberRecord->lastname,
            $memberRecord->email."",
            $memberRecord->phone."",
            $memberRecord->shareWithNonprofit ? 1 : 0,
            $memberRecord->notifyNewEvents ? 1 : 0,
            $updatingMemberId,
            $memberRecord->id
        );
        $response = MySqliHelper::executeWithLogging("IndividualMemberProvider.updateMember SQL error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);

        return $updateResult || $response->success;
    }

    private static $GET_INDIVIDUAL_INFO_SQL = "
        SELECT
            IndividualInfo_ID as Id,
            IndividualInfo_FirstName as FirstName,
            IndividualInfo_LastName as LastName,
            IndividualInfo_Email as Email,
            IndividualInfo_Phone as Phone
        FROM individual_info
        WHERE IndividualInfo_MemberID = ?";

    /**
     * TODO: delete this
     * @deprecated
     * @param $memberId
     * @return object|null
     */
    public function getMember($memberId){
        trigger_error("IndividualMemberProvider.getMember being called although it is deprecated.", E_USER_WARNING);
        $sqlString = self::$GET_INDIVIDUAL_INFO_SQL;
        $sqlBindings = "i";
        $sqlParams = array($memberId);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return (count($result)) ? (object)$result[0] : null;
    }

    private static $GET_PUBLIC_PROFILE_SQL = "
        SELECT
            IndividualInfo_ID as id,
            IndividualInfo_FirstName as firstname,
            IndividualInfo_LastName as lastname,
            IndividualInfo_Email as email,
            IndividualInfo_Phone as phone,
			(
	            SELECT pid
	            FROM permalinks
	            WHERE entity_name = 'individual'
	            AND entity_id = IndividualInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
			member_preferences.shareWithOrgs as shareWithNonprofit,
			member_preferences.notifyNewEvents
        FROM individual_info
        JOIN member_preferences ON individual_info.IndividualInfo_MemberID = member_preferences.members_id
		WHERE
		    IndividualInfo_ID = ?
		OR
		    IndividualInfo_MemberID = ?";

    public function getPublicProfile($individualId, $memberID = -1){
        $record = new IndividualMemberRecord();
        $sqlString = self::$GET_PUBLIC_PROFILE_SQL;
        $sqlBindings = "ii";
        $sqlParams = array($individualId, $memberID);
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            $row = (object)$result[0];

            $record->id = $row->id;
            $record->pid = $row->pid;
            $record->firstname = $row->firstname;
            $record->lastname = $row->lastname;
            $record->setPhone($row->phone);
            $record->email = $row->email;
            $record->shareWithNonprofit = $row->shareWithNonprofit ? true : false;
            $record->notifyNewEvents = $row->notifyNewEvents ? true : false;
        }
        return $record;
    }

    private static $UPDATE_SHARE_WITH_NONPROFIT = "
        UPDATE member_preferences, individual_info
        SET member_preferences.shareWithOrgs = ?,
            member_preferences.updatedBy = ?,
            member_preferences.updatedDate = CURRENT_TIMESTAMP
        WHERE member_preferences.members_id = individual_info.IndividualInfo_MemberID
        AND individual_info.IndividualInfo_ID = ?";

    /**
     * Given the member record, set the shareWithNonprofit setting ONLY.
     * @param IndividualMemberRecord $memberRecord
     * @param int $updatingMemberId
     * @return ExecuteResponseRecord
     */
    public function setShareWithNonprofit(IndividualMemberRecord $memberRecord, $updatingMemberId = 0){
        $sqlString = self::$UPDATE_SHARE_WITH_NONPROFIT;
        $sqlBinding = "iii";
        $sqlParam = array(
            $memberRecord->shareWithNonprofit ? 1 : 0,
            $updatingMemberId,
            $memberRecord->id
        );
        $sqlResponse = MySqliHelper::executeWithLogging("IndividualMemberProvider.setShareWithNonprofit: Failure updating member_preferences: ", $this->mysqli, $sqlString, $sqlBinding, $sqlParam);

        return $sqlResponse;
    }
}

include_once("MerchantMemberProvider.php");
include_once("OrganizationMemberProvider.php");
?>