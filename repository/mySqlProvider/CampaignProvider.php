<?php
class CampaignProvider {
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $SELECT_COLUMNS = "
        SELECT
            t.TagList_ID as id,
            t.TagList_Name as name,
            t.TagList_Description as snippet,
            t.TagList_CampaignHeading as heading,
            t.TagList_CampaignSubHeading as subHeading,
            t.TagList_CampaignDescription as description,
            t.TagList_CampaignDescriptionMarkdown as descriptionMarkdownSource,
            t.TagLIst_CampaignDescriptionHtml as descriptionHtmlSource,
            t.TagList_Image as imageFileName,
            t.TagList_URL as url,
            t.TagList_EventURLOverride as eventUrlOverride,
            t.TagList_AddWrap as addWrap,
            t.TagList_MapZoomLevel as mapZoomLevel,
            t.TagList_MapCenterLat as mapCenterLat,
            t.TagList_MapCenterLong as mapCenterLong,
            t.TagList_CallToAction as callToAction,
            t.TagList_PosterImageUrl as posterImageUrl,
            t.TagList_PosterUrl as posterUrl,
            t.TagList_ShowMap as showMap,
            t.TagList_ShowBizList as showBizList,
            t.TagList_NotShowingBizListBlurb as notShowingBizListBlurb";

    private static $FROM_TABLE_CLAUSE = "
        FROM tag_list as t
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND t.TagList_ID = ?";

    private static $FILTER_ON_NAME = "
        AND t.TagList_Name = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            t.TagList_Name LIKE ?
            OR t.TagList_Description LIKE ?
            OR t.TagList_CampaignHeading LIKE ?
            OR t.TagList_CampaignSubHeading LIKE ?
            OR t.TagList_CampaignDescription LIKE ?
            OR t.TagList_URL LIKE ?
            OR t.TagList_CallToAction LIKE ?
        )";

    private static $FILTER_ON_HAS_EVENTS = "
        AND EXISTS (
            SELECT 1
            FROM event_info
            JOIN tag_association
                ON TagAssociation_SourceTable = 'event_info'
                AND TagAssociation_SourceID = Event_ID
                AND TagAssociation_Status IS NULL
            WHERE Event_Status = 'ACTIVE'
            AND TagAssociation_TagListID = t.TagList_ID
            LIMIT 1
        )";

    public function get(CampaignCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else if ($criteria->getCampaignName()){
            $filterString .= self::$FILTER_ON_NAME;
            $filterBinding .= "s";
            $filterParams[] = $criteria->getCampaignName();
        } else {

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
            if ($criteria->hasEvents){
                $filterString .= self::$FILTER_ON_HAS_EVENTS;
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToRecord((object)$row);
                    }
                } else {
                    trigger_error("CampaignProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("CampaignProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY t.TagList_Name ASC";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY t.TagList_ID ".$sortOrder;
                    break;
                case "name":
                    $orderBy = " ORDER BY t.TagList_Name ".$sortOrder;
                    break;
                case "snippet":
                    $orderBy = " ORDER BY t.TagList_Description ".$sortOrder;
                    break;
                case "imageUrl":
                    $orderBy = " ORDER BY t.TagList_Image ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = "t.TagList_ID ".$sortOrder;
                        break;
                    case "name":
                        $orderByStrings[] = "t.TagList_Name ".$sortOrder;
                        break;
                    case "snippet":
                        $orderByStrings[] = "t.TagList_Description ".$sortOrder;
                        break;
                    case "imageUrl":
                        $orderByStrings[] = "t.TagList_Image ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    protected function mapToRecord($dataRow) {
        return CampaignRecord::castAs($dataRow);
    }

    // add

    private static $INSERT_SQL = "
		INSERT INTO tag_list
		(
			TagList_Name,
			TagList_Description,
			TagList_CampaignHeading,
			TagList_CampaignSubHeading,
			TagList_CampaignDescriptionMarkdown,
			TagList_CampaignDescriptionHtml,

			TagList_Image,
			TagList_URL,
		    TagList_EventURLOverride,
			TagList_AddWrap,
			TagList_MapZoomLevel,

			TagList_MapCenterLat,
			TagList_MapCenterLong,
			TagList_CreatedByAdminID,
			TagList_CallToAction,
			TagList_PosterImageUrl,
			TagList_PosterUrl,
			
			TagList_ShowMap,
			TagList_ShowBizList,
			TagList_NotShowingBizListBlurb
		) VALUES (
			?,?,?,?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,?,
			?,?,?
		)";

    public function add(CampaignRecord $record, $adminUserId){
        $sqlString = self::$INSERT_SQL;
        $sqlBinding = "sssssssssiiddissssss";
        $sqlParam = array(
            TagRecord::getQueryName($record->name),
            $record->snippet,
            $record->heading,
            $record->subHeading,
            $record->descriptionMarkdownSource,
            $record->descriptionHtmlSource,

            $record->imageFileName,
            $record->url,
            $record->eventUrlOverride,
            $record->addWrap,
            $record->mapZoomLevel,

            $record->mapCenterLat,
            $record->mapCenterLong,
            $adminUserId,
            $record->callToAction,
            $record->posterImageUrl,
            $record->posterUrl,
			
			$record->showMap,
			$record->showBizList,
			$record->notShowingBizListBlurb
        );
        $insertResponse = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $insertResponse);
        if ($insertResponse->success && $insertResponse->affectedRows){
            $record->id = $insertResponse->insertedId;
            $insertResponse->record = $record;
        } else {
            trigger_error("CampaignProvider.add failed to insert: ".$insertResponse->error, E_USER_ERROR);
        }

        return $insertResponse;
    }

    private static $UPDATE_SQL = "
        UPDATE tag_list
        SET
			TagList_Name = ?,
			TagList_Description = ?,
			TagList_CampaignHeading = ?,
			TagList_CampaignSubHeading = ?,
			TagList_CampaignDescriptionMarkdown = ?,
			TagLIst_CampaignDescriptionHtml = ?,

			TagList_Image = ?,
			TagList_URL = ?,
			TagList_EventURLOverride = ?,
			TagList_AddWrap = ?,
			TagList_MapZoomLevel = ?,

			TagList_MapCenterLat = ?,
			TagList_MapCenterLong = ?,
			TagList_CallToAction = ?,
			TagList_PosterImageUrl = ?,
			TagList_PosterUrl = ?,
			
			TagList_ShowMap = ?,
			TagList_ShowBizList = ?,
			TagList_NotShowingBizListBlurb = ?
		WHERE TagList_ID = ?";

    public function update(CampaignRecord $record, $adminUserId){
        $updateResponse = new ExecuteResponseRecord();
        if ($record->id){
            $sqlString = self::$UPDATE_SQL;
            $sqlBinding = "sssssssssiiddssssssi";
            $sqlParams = array(
                TagRecord::getQueryName($record->name),
                $record->snippet,
                $record->heading,
                $record->subHeading,
                $record->descriptionMarkdownSource,
                $record->descriptionHtmlSource,

                $record->imageFileName,
                $record->url,
                $record->eventUrlOverride,
                $record->addWrap,
                $record->mapZoomLevel,

                $record->mapCenterLat,
                $record->mapCenterLong,
                $record->callToAction,
                $record->posterImageUrl,
                $record->posterUrl,
				
				$record->showMap,
				$record->showBizList,
				$record->notShowingBizListBlurb,
                $record->id
            );
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $updateResponse);
            if ($updateResponse->error){
                trigger_error("CampaignProvider.update failed to update: ".$updateResponse->error, E_USER_ERROR);
            }
        }
        return $updateResponse;
    }

    private static $CLONE_CAMPAIGN_SQL = "
        INSERT INTO tag_list
        (
            TagList_Name,
            TagList_Description,
            TagList_CampaignHeading,
            TagList_CampaignSubHeading,
            TagList_CampaignDescription,
            TagList_CampaignDescriptionMarkdown,
            TagList_CampaignDescriptionHtml,
            TagList_URL,
            TagList_EventURLOverride,
            TagList_AddWrap,
            TagList_MapZoomLevel,
            TagList_MapCenterLat,
            TagList_MapCenterLong,
            TagList_PosterImageUrl,
            TagList_PosterUrl,
			TagList_ShowMap,
			TagList_ShowBizList,
			TagList_NotShowingBizListBlurb,
            TagList_CreatedByAdminID
        )
        SELECT
            CONCAT('Copy of ', TagList_Name, ' - ', CURRENT_TIMESTAMP),
            TagList_Description,
            TagList_CampaignHeading,
            TagList_CampaignSubHeading,
            TagList_CampaignDescription,
            TagList_CampaignDescriptionMarkdown,
            TagList_CampaignDescriptionHtml,
            TagList_URL,
            TagList_EventURLOverride,
            TagList_AddWrap,
            TagList_MapZoomLevel,
            TagList_MapCenterLat,
            TagList_MapCenterLong,
            TagList_PosterImageUrl,
            TagLIst_PosterUrl,
			TagList_ShowMap,
			TagList_ShowBizList,
			TagList_NotShowingBizListBlurb,
            ?
        FROM tag_list
        WHERE TagList_ID = ?";

    public function cloneCampaign(CampaignCloneCriteria $criteria){
        $sqlString = self::$CLONE_CAMPAIGN_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            getAdminId(),
            $criteria->record->id
        );
        $executeResponse = MySqliHelper::executeWithLogging("CampaignProvider.cloneCampaign error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);

        if ($executeResponse->success && $executeResponse->insertedId){
            $newRecord = new CampaignRecord();
            $newRecord->id = $executeResponse->insertedId;
            if ($criteria->cloneReferences){
                $this->cloneReferences($criteria, $newRecord);
            }
            if ($criteria->cloneSocialMessages){
                $this->cloneSocialMessages($criteria, $newRecord);
            }
            $executeResponse->record = $newRecord;
        }
        return $executeResponse;
    }

    private static $CLONE_CAMPAIGN_REFERENCES_SQL = "
        INSERT INTO tag_references
        (
            TagReference_TagListID,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Status,
            TagReference_Page
        )
        SELECT
            ?,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Status,
            TagReference_Page
        FROM tag_references
        WHERE TagReference_TagListID = ?
        AND TagReference_Status = 'ACTIVE'";

    private function cloneReferences(CampaignCloneCriteria $criteria, CampaignRecord $newRecord){
        $sqlString = self::$CLONE_CAMPAIGN_REFERENCES_SQL;
        $sqlBinding = "ii";
        $sqlParams = array(
            $newRecord->id,
            $criteria->record->id
        );
        return MySqliHelper::executeWithLogging("CampaignProvider.cloneReferences error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $CLONE_SOCIAL_MESSAGES_SQL = "
        INSERT INTO social_messaging_samples
        (
            type,
            message,
            relevant_entity,
            relevant_entity_id,
            `status`,
            sort_order,
            created_by
        )
        SELECT
            type,
            message,
            relevant_entity,
            ?,
            `status`,
            sort_order,
            ?
        FROM social_messaging_samples
        WHERE relevant_entity = 'campaign'
        AND relevant_entity_id = ?";

    private function cloneSocialMessages(CampaignCloneCriteria $criteria, CampaignRecord $newRecord){
        $sqlString = self::$CLONE_SOCIAL_MESSAGES_SQL;
        $sqlBinding = "iii";
        $sqlParams = array(
            $newRecord->id,
            getAdminId(),
            $criteria->record->id
        );
        return MySqliHelper::executeWithLogging("CampaignProvider.cloneSocialMessages error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }
}

class CampaignCloneCriteria {

    public $record;
    public $cloneReferences;
    public $cloneSocialMessages;
}

class CampaignResourceCriteria extends PaginationCriteria{
    public $tagId;
}

class CampaignResourceProvider{
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    const CAMPAIGN_PAGE = 'Campaign';
    const EVENT_TEMPLATE_PAGE = 'Event Template';
    const CAMPAIGN_PAGE_RELATED = 'Campaign - Related';

    public static $PAGES_ARRAY = array(
        self::EVENT_TEMPLATE_PAGE,
        self::CAMPAIGN_PAGE,
        self::CAMPAIGN_PAGE_RELATED
    );

    private static $SELECT_COLUMNS = "
        SELECT
            TagReference_ID as id,
            TagReference_TagListID as tagID,
            TagReference_SortOrder as sortOrder,
            TagReference_DisplayText as displayText,
            TagReference_HoverText as hoverText,
            TagReference_URL as url,
            TagReference_TargetID as targetID,
            TagReference_Status as status,
            TagReference_Page as page";

    private static $FROM_TABLE_CLAUSE = "
        FROM tag_references
        WHERE 1 = 1";

    private static $FILTER_ON_ID = "
        AND TagReference_ID = ?";

    private static $FILTER_ON_TAG_ID = "
        AND TagReference_TagListID = ?";

    private static $FILTER_ON_KEYWORD = "
        AND (
            TagReference_DisplayText LIKE ?
            OR TagReference_DisplayText LIKE ?
            OR TagReference_HoverText LIKE ?
            OR TagReference_URL LIKE ?
            OR TagReference_TargetID LIKE ?
            OR TagReference_Page LIKE ?
        )";

    public function get(CampaignResourceCriteria $criteria){
        $result = new PagedResult();
        $result->totalRecords = 0;
        $result->startIndex = is_numeric($criteria->start) ? $criteria->start : 0;
        $result->maxLength = is_numeric($criteria->size) ? $criteria->size : 10;
        $result->collection = array();

        $paginationSelectString = self::$SELECT_COLUMNS;
        $fromTableClause = self::$FROM_TABLE_CLAUSE;
        $filterString = ""; // nothing by default
        $filterBinding = "";
        $filterParams = array();

        $orderBy = $this->getOrderByClause($criteria);
        $limiter = ($result->maxLength) ? " LIMIT ".$result->startIndex.", ".$result->maxLength : "";

        // apply search
        if ($criteria->id && is_numeric($criteria->id)){
            $filterString .= self::$FILTER_ON_ID;
            $filterBinding .= "i";
            $filterParams[] = $criteria->id;
        } else {

            if ($criteria->tagId){
                $filterString .= self::$FILTER_ON_TAG_ID;
                $filterBinding .= "i";
                $filterParams[] = $criteria->tagId;
            }

            if ($criteria->keyword){
                $keywordCriteria = "%".$criteria->keyword."%";
                $filterString .= self::$FILTER_ON_KEYWORD;
                $numParams = substr_count(self::$FILTER_ON_KEYWORD, " ?");
                for ($i = 0; $i < $numParams; $i++){
                    $filterBinding .= "s";
                    $filterParams[] = $keywordCriteria;
                }
            }
        }

        // get the total count
        $countSqlString = "SELECT COUNT(*) AS count ".$fromTableClause.$filterString.$orderBy;
        $queryResponse = new QueryResponseRecord();
        $sqlResult = MySqliHelper::get_result($this->mysqli, $countSqlString, $filterBinding, $filterParams, $queryResponse);

        if ($queryResponse->success){
            $result->totalRecords = $sqlResult[0]['count'] ? $sqlResult[0]['count']: 0;

            if ($result->totalRecords){
                // get the page
                $pageSqlString = $paginationSelectString.$fromTableClause.$filterString.$orderBy.$limiter;
                $queryResponse = new QueryResponseRecord();
                $sqlResult = MySqliHelper::get_result($this->mysqli, $pageSqlString, $filterBinding, $filterParams, $queryResponse);

                if ($queryResponse->success){
                    foreach ($sqlResult as $row){
                        // since the select columns is done here, call the mapToRecord in this class rather than in subclasses
                        $result->collection[] = self::mapToTagRefRecord((object)$row);
                    }
                } else {
                    trigger_error("CampaignResourceProvider.get failed to get paginated results: ".$queryResponse->error, E_USER_ERROR);
                }
            }
        } else {
            trigger_error("CampaignResourceProvider.get failed to get count of results: ".$queryResponse->error, E_USER_ERROR);
        }

        $result->setHasMore();
        return $result;
    }

    private static $DEFAULT_ORDER_BY = "
        ORDER BY TagReference_Page ASC, TagReference_SortOrder ASC, TagReference_DisplayText ASC";

    private function getOrderByClause(PaginationCriteria $criteria){
        $orderBy = self::$DEFAULT_ORDER_BY;
        if ($criteria->sortColumn){
            $sortOrder = strtolower($criteria->sortOrder) == "desc" ? "desc" : "";
            switch ($criteria->sortColumn){
                case "id":
                    $orderBy = " ORDER BY TagReference_ID ".$sortOrder;
                    break;
                case "page":
                    $orderBy = " ORDER BY TagReference_Page ".$sortOrder;
                    break;
                case "sortOrder":
                    $orderBy = " ORDER BY TagReference_SortOrder ".$sortOrder;
                    break;
                case "displayText":
                    $orderBy = " ORDER BY TagReference_DisplayText ".$sortOrder;
                    break;
                case "hoverText":
                    $orderBy = " ORDER BY TagReference_HoverText ".$sortOrder;
                    break;
                case "url":
                    $orderBy = " ORDER BY TagReference_URL ".$sortOrder;
                    break;
                case "targetID":
                    $orderBy = " ORDER BY TagReference_TargetID ".$sortOrder;
                    break;
                case "status":
                    $orderBy = " ORDER BY TagReference_Status ".$sortOrder;
                    break;
            }
        }
        if (count($criteria->multiColumnSort)){
            $orderByStrings = array();
            foreach($criteria->multiColumnSort as $multiColumnSort){
                /* @var $multiColumnSort MultiColumnSort */
                $sortOrder = strtolower($multiColumnSort->order) == "desc" ? "desc" : "";
                switch ($multiColumnSort->column){
                    case "id":
                        $orderByStrings[] = " TagReference_ID ".$sortOrder;
                        break;
                    case "page":
                        $orderByStrings[] = " TagReference_Page ".$sortOrder;
                        break;
                    case "sortOrder":
                        $orderByStrings[] = " TagReference_SortOrder ".$sortOrder;
                        break;
                    case "displayText":
                        $orderByStrings[] = " TagReference_DisplayText ".$sortOrder;
                        break;
                    case "hoverText":
                        $orderByStrings[] = " TagReference_HoverText ".$sortOrder;
                        break;
                    case "url":
                        $orderByStrings[] = " TagReference_URL ".$sortOrder;
                        break;
                    case "targetID":
                        $orderByStrings[] = " TagReference_TargetID ".$sortOrder;
                        break;
                    case "status":
                        $orderByStrings[] = " TagReference_Status ".$sortOrder;
                        break;
                }
            }
            $orderBy = " ORDER BY ".implode(", ", $orderByStrings);
        }
        return $orderBy;
    }

    private static $GET_TAG_REFERENCES_SQL = "
        SELECT
          TagReference_ID as id,
          TagReference_TagListID as tagID,
          TagReference_SortOrder as sortOrder,
          TagReference_DisplayText as displayText,
          TagReference_HoverText as hoverText,
          TagReference_URL as url,
          TagReference_TargetID as targetID,
          TagReference_Status as status,
          TagReference_Page as page
        FROM tag_references
        JOIN tag_list ON tag_references.TagReference_TagListID = tag_list.TagList_ID
        WHERE TagReference_TagListID = ?
        AND (TagReference_Status = 'ACTIVE' OR 'retrieveAll' = ? )
        AND ('' = ? OR TagReference_Page = ?)
        ORDER BY TagReference_Page, TagReference_SortOrder, TagReference_DisplayText";

    public function getTagReferences($tagID, $activeOnly = true, $page = ''){
        $sqlString = self::$GET_TAG_REFERENCES_SQL;
        $sqlBindings = "isss";
        $sqlParams = array(
            $tagID,
            ($activeOnly) ? "activeOnly" : "retrieveAll",
            $page,
            $page
        );
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        return $this->mapToTagRefCollection($result);
    }

    private function mapToTagRefCollection($query_results) {
        $collection = array();
        foreach ($query_results as $rowArray){
            $row = (object)$rowArray;
            $collection[] = $this->mapToTagRefRecord($row);
        }
        return $collection;
    }

    private static $GET_TAG_REFERENCE_SQL = "
       SELECT
          TagReference_ID as id,
          TagReference_TagListID as tagID,
          TagReference_SortOrder as sortOrder,
          TagReference_DisplayText as displayText,
          TagReference_HoverText as hoverText,
          TagReference_URL as url,
          TagReference_TargetID as targetID,
          TagReference_Status as status,
          TagReference_Page as page
        FROM tag_references
        WHERE TagReference_ID = ?";

    public function getTagReference($id){
        $result = MySqliHelper::get_result($this->mysqli, self::$GET_TAG_REFERENCE_SQL, array($id));
        if (count($result)){
            return $this->mapToTagRefRecord($result[0]);
        }
        return null;
    }

    private function mapToTagRefRecord($datarow) {
        $record = new TagReference();
        $record->id = $datarow->id;
        $record->tagID = $datarow->tagID;
        $record->sortOrder = $datarow->sortOrder;
        $record->displayText = $datarow->displayText;
        $record->hoverText = $datarow->hoverText;
        $record->url = $datarow->url;
        $record->targetID = $datarow->targetID;
        $record->isActive = $datarow->status == "ACTIVE";
        $record->page = $datarow->page;

        return $record;
    }

    private static $FIND_MATCHING_TAG_REFERENCE_SQL = "
        SELECT *
        FROM tag_references
        WHERE TagReference_TagListID = ?
        AND TagReference_Page = ?
        AND TagReference_DisplayText = ?";

    public function saveTagReference(TagReference $tagReference){
        $saveResponse = new stdClass();
        $saveResponse->message = "";
        if (!is_numeric($tagReference->tagID) || $tagReference->tagID <= 0){
            $saveResponse->message = "Invalid TagList_ID";
            return $saveResponse;
        }
        if (!$tagReference->displayText){
            $saveResponse->message = "DisplayText is a required field.";
            return $saveResponse;
        }
        if ($tagReference->isNew()){
            // first check to see if it's a dupe
            $dupes = MySqliHelper::get_result($this->mysqli, self::$FIND_MATCHING_TAG_REFERENCE_SQL, "iss", array($tagReference->tagID, $tagReference->page, $tagReference->displayText));
            if (count($dupes) > 1){
                $saveResponse->message = "There are already one or more references on this TagList_ID with the same displayText";
                return $saveResponse;
            } else if (count($dupes)){
                // make this an update
                $tagReference->id = $dupes[0]["TagReference_ID"];
            }
        }
        if ($tagReference->isNew()){
            $insertResponse = $this->insertTagReference($tagReference);
            if ($insertResponse->success){
                $saveResponse->record = $tagReference;
                $saveResponse->record->id = $insertResponse->insertedId;
            } else {
                $saveResponse->message = ($insertResponse->error) ? $insertResponse->error : "Error saving Tag Reference";
            }
        } else {
            $updateResponse = $this->updateTagReference($tagReference);
            if (!$updateResponse->success){
                $saveResponse->message = ($updateResponse->error) ? $updateResponse->error : "Error saving Tag Reference";
            } else {
                $saveResponse->record = $tagReference;
            }
        }
        return $saveResponse;
    }

    private static $INSERT_TAG_REFERENCE_SQL = "
        INSERT INTO tag_references
        (
            TagReference_TagListID,
            TagReference_SortOrder,
            TagReference_DisplayText,
            TagReference_HoverText,
            TagReference_URL,
            TagReference_TargetID,
            TagReference_Page
        ) VALUES (
            ?,?,?,?,?,?,?
        )";

    private static $INSERT_TAG_REFERENCE_BINDINGS = "iisssss";

    private function insertTagReference(TagReference $tagReference){
        $sqlString = self::$INSERT_TAG_REFERENCE_SQL;
        $sqlBindings = self::$INSERT_TAG_REFERENCE_BINDINGS;
        $sqlParams = array(
            $tagReference->tagID,
            $tagReference->sortOrder,
            $tagReference->displayText,
            $tagReference->hoverText,
            $tagReference->url,
            $tagReference->targetID,
            $tagReference->page
        );
        $insertResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $insertResponse);
        return $insertResponse;
    }

    private static $UPDATE_TAG_REFERENCE_SQL = "
        UPDATE tag_references
        SET
            TagReference_SortOrder = ?,
            TagReference_DisplayText = ?,
            TagReference_HoverText = ?,
            TagReference_URL = ?,
            TagReference_TargetID = ?,
            TagReference_Status = ?,
            TagReference_Page = ?
        WHERE TagReference_ID = ?";

    private static $UPDATE_TAG_REFERENCE_BINDINGS = "issssssi";

    private function updateTagReference(TagReference $tagReference){
        $sqlString = self::$UPDATE_TAG_REFERENCE_SQL;
        $sqlBindings = self::$UPDATE_TAG_REFERENCE_BINDINGS;
        $sqlParams = array(
            $tagReference->sortOrder,
            $tagReference->displayText,
            $tagReference->hoverText,
            $tagReference->url,
            $tagReference->targetID,
            ($tagReference->isActive) ? "ACTIVE" : "DELETED",
            $tagReference->page,
            $tagReference->id
        );
        $updateResponse = new ExecuteResponseRecord();
        $result = MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams, $updateResponse);
        return $updateResponse;
    }
}