<?php
include_once("Helper.php");
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/config.php");
include_once($rootFolder."/repository/DataContracts.php");
include_once($rootFolder."/repository/ManagerAPI.php");
include_once("MerchantBillingLogProvider.php");

/**
 * Class for managing events. CRUD operations, etc.
 */
class MySqliEventManager implements EventManager, ExternalIdXwalk{
    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    private static $DELETE_EVENT_SQL = "
        UPDATE event_info
        SET Event_Status = 'DELETED'
        WHERE Event_ID = ?
        AND NOT EXISTS (
          SELECT 1
          FROM event_transaction
          WHERE EventTransaction_EventID = Event_ID
          AND EventTransaction_Status NOT LIKE '%Deleted%'
          LIMIT 1
        )";

    /**
     * Marks an event deleted regardless of how many venues are attached. Will not mark an event deleted if
     * there are any event transactions associated with it.
     * @param EventRecord $eventRecord
     * @param $arrayOfVenues
     * @param int $memberId
     * @param Array of Venue $arrayOfVenues
     * @return ExecuteResponseRecord
     */
    public function deleteEvent(EventRecord $eventRecord, $arrayOfVenues, $memberId = 0, $adminId = null){
        $executeResponse = new ExecuteResponseRecord();
        if ($eventRecord->id && !$eventRecord->isDraft){
            $sqlString = self::$DELETE_EVENT_SQL;
            $sqlBinding = "i";
            $sqlParam = array($eventRecord->id);
            MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $executeResponse);
            getEventLogger()->log_action($eventRecord->id, "Deleted from database", $memberId, $adminId);
        } else {
            $executeResponse->error = "Invalid Operation: EventRecord must refer to a published event.";
        }
        return $executeResponse;
    }

    public function updateEvent(EventREcord $eventRecord, $memberId = 0, $adminId = null){
        $sqlString = "";
        $sqlBinding = "";
        $sqlParams = array();
        $insertHelper = new UpdateEventHelper($eventRecord, $memberId, $adminId);
        $insertHelper->setupUpdate($sqlString, $sqlBinding, $sqlParams);
        return MySqliHelper::executeWithLogging("MySqliEventManager.updateEvent: SQL: [".$sqlString."] Binding:".$sqlBinding." Param Count:".count($sqlParams)." :", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);
    }

    private static $ADD_EVENTID_TO_DRAFT_SQL = "
        UPDATE event_draft
        SET EventDraft_EventID = ?
        WHERE EventDraft_ID = ?";

    private static $ADD_TEMPLATE_TAGS_TO_EVENT_SQL = "
        INSERT INTO tag_association
        (
            TagAssociation_SourceTable,
            TagAssociation_SourceID,
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        )
        SELECT
            'event_info',
            ?, -- event id
            TagAssociation_TagListID,
            TagAssociation_SortOrder
        FROM tag_association input
        WHERE input.TagAssociation_SourceTable = 'event_templates'
        AND input.TagAssociation_SourceID = ? -- templateID
        AND input.TagAssociation_Status IS NULL
        AND NOT EXISTS (
            SELECT 1
            FROM tag_association dupe
            WHERE dupe.TagAssociation_SourceTable = 'event_info'
            AND dupe.TagAssociation_SourceID = ? -- event ID
            AND dupe.TagAssociation_TagListID = input.TagAssociation_TagListID
            AND dupe.TagAssociation_Status IS NULL
        )";

    public function createEvent(EventRecord $eventRecord, $memberId = 0, $adminId = null){
        $sqlString = "";
        $sqlBinding = "";
        $sqlParams = array();
        $insertHelper = new InsertEventHelper($eventRecord, $memberId, $adminId);
        $insertHelper->setupInsert($sqlString, $sqlBinding, $sqlParams);
        $insertResponse = MySqliHelper::executeWithLogging("MySqliEventManager.createEvent: SQL: [".$sqlString."] Binding:".$sqlBinding." Param Count:".count($sqlParams)." :", $this->mysqli, $sqlString, $sqlBinding, $sqlParams);

        if ($insertResponse->success){
            // add action to event_log
            // eventInfo_id = $insertResponse->insertedId
            $logged = MySqliHelper::log_event_action($this->mysqli, $insertResponse->insertedId, 'Created', $memberId, $adminId);
            //Push up Event to Indexing Server
            if ($insertResponse->insertedId){
                if ($eventRecord->id){
                    // add new event id to draft
                    MySqliHelper::executeWithLogging("MySqliEventManager.createEvent SQL error updating draft: ", $this->mysqli, self::$ADD_EVENTID_TO_DRAFT_SQL, "ii", array($insertResponse->insertedId, $eventRecord->id));
                }

                if ($eventRecord->eventTemplateId > 0){
                    // copy the templates tags to the event
                    MySqliHelper::executeWithLogging("MySqliEventManager.createEvent SQL error adding template tags: ", $this->mysqli, self::$ADD_TEMPLATE_TAGS_TO_EVENT_SQL, "iii", array($insertResponse->insertedId,$eventRecord->eventTemplateId,$insertResponse->insertedId));
                }
                if ($eventRecord->venue->eventTemplatePremiumId){
                    // add billing item
                    $this->postEventTemplatePremium($insertResponse->insertedId, $memberId, $adminId);
                }
            }
        }

        return $insertResponse;
    }

    private function postEventTemplatePremium($eventId, $memberId, $adminId){
        if ($eventId){
            $billingProvider = new MerchantBillingLogProvider($this->mysqli);
            $billingProvider->postEventTemplatePremium($eventId, $memberId, $adminId);
        }
    }

    private static $UPDATE_EVENTBRITE_EVENTID_SQL = "
        UPDATE event_info
        SET Event_EventBriteEventID = ?
        WHERE Event_ID = ?
        AND Event_ProposedByMerchantID = ?";

    private static $UPDATE_EVENTBRITE_EVENTID_MATCH_SQL = "
        UPDATE event_match
        SET EventMatch_EventBriteEventID = ?
        WHERE EventMatch_EventID = ?
        AND EventMatch_MerchantID = ?";

    public function setEventBriteID(EventRecord $eventRecord){
        if (isset($eventRecord->venue->eventBriteEventID) && $eventRecord->id){
            $sqlBindings = "sii";
            $sqlParams = array(
                $eventRecord->venue->eventBriteEventID,
                $eventRecord->id,
                $eventRecord->venue->id
            );
            $executeResponse = new ExecuteResponseRecord();
            $result = MySqliHelper::execute($this->mysqli, self::$UPDATE_EVENTBRITE_EVENTID_SQL, $sqlBindings, $sqlParams, $executeResponse);
            if (!$result){
                $result = MySqliHelper::execute($this->mysqli, self::$UPDATE_EVENTBRITE_EVENTID_MATCH_SQL, $sqlBindings, $sqlParams, $executeResponse);
            }
            return $result;
        }
        return false;
    }

    private static $SET_EVENTBRITE_VENUEID_SQL = "
        UPDATE merchant_info
        SET MerchantInfo_EventBriteVenueID = ?
        WHERE MerchantInfo_ID = ?";

    public function setEventBriteVenueID($merchantID, $venueID){
        if ($merchantID && $venueID){
            $sqlString = self::$SET_EVENTBRITE_VENUEID_SQL;
            $sqlBindings = "si";
            $sqlParams = array(
                $venueID,
                $merchantID
            );
            return MySqliHelper::execute($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        }
    }

    private static $UPDATE_FACEBOOK_EVENTID_SQL = "
        UPDATE event_info
        SET Event_FacebookEventID = ?
        WHERE Event_ID = ?
        AND Event_ProposedByMerchantID = ?";

    private static $UPDATE_FACEBOOK_EVENTID_MATCH_SQL = "
        UPDATE event_match
        SET EventMatch_FacebookEventID = ?
        WHERE EventMatch_EventID = ?
        AND EventMatch_MerchantID = ?";

    public function setFacebookID(EventRecord $eventRecord){
        if ($eventRecord->venue->facebookEventID && $eventRecord->id){
            $sqlBindings = "sii";
            $sqlParams = array(
                $eventRecord->venue->facebookEventID,
                $eventRecord->id,
                $eventRecord->venue->id
            );
            $executeResponse = new ExecuteResponseRecord();
            $result = MySqliHelper::execute($this->mysqli, self::$UPDATE_FACEBOOK_EVENTID_SQL, $sqlBindings, $sqlParams, $executeResponse);
            if (!$result){
                $result = MySqliHelper::execute($this->mysqli, self::$UPDATE_FACEBOOK_EVENTID_MATCH_SQL, $sqlBindings, $sqlParams, $executeResponse);
            }
            return $result;
        }
        return false;
    }
}

/**
 * Class InsertEventHelper
 * This helper just constructs the insert SQL, handles the bindings, and assigns the parameters for the bindings.
 * This is easier to debug since there are so many columns being inserted, and it is also easier to conditionally add sets of columns;
 */
class InsertEventHelper{

    /* @var EventRecord */
    private $eventRecord;
    private $sqlBindings = "";
    private $sqlParams = array();
    private $memberId;
    private $adminId;

    private $sqlFieldsString = "";
    private $sqlParametersString = "";

    public function __construct(EventRecord $record, $memberId, $adminId){
        $this->eventRecord = $record;
        $this->memberId = $memberId ? $memberId : 0;
        $this->adminId = $adminId ? $adminId : 0;
    }

    /**
     * Sets up the insert sqlString, sqlBindings, and sqlParams. The passed in variables will be overwritten.
     * @param $sqlString
     * @param $sqlBindings
     * @param $sqlParams
     */
    public function setupInsert(&$sqlString, &$sqlBindings, &$sqlParams){
        $this->setupBasicFields();
        $this->setupAuditFields();
        $this->setupOnlineFields();
        $this->setupCausetownEventFields();
        if ($this->eventRecord->venue->isFlatDonation){
            $this->setupFlatDonationFields();
        } else {
            $this->setupPercentageFields();
        }
        $this->setupMiscFields();

        $sqlString = $this->getSqlString();
        $sqlBindings = $this->sqlBindings;
        $sqlParams = $this->sqlParams;
    }

    private function getSqlString(){
        return "INSERT INTO event_info (".$this->sqlFieldsString.") VALUES (".$this->sqlParametersString.")";
    }

    private function appendInsertFields($string){
        $this->sqlFieldsString .= (strlen($this->sqlFieldsString)) ? "," : "";
        $this->sqlFieldsString .= $string;
    }

    private function appendParametersString($string){
        $this->sqlParametersString .= (strlen($this->sqlParametersString)) ? "," : "";
        $this->sqlParametersString .= $string;
    }

    private function appendBindings($string){
        $this->sqlBindings .= $string;
    }

    private function appendParamValues(array $paramValues){
        $this->sqlParams = array_merge($this->sqlParams, $paramValues);
    }
    
    private static $BASIC_INSERT_FIELDS = "
        Event_ProposedByMerchantID,
        Event_Title,
        Event_Description,
        Event_Date,
        Event_EndDate,
        Event_StartTime,
        Event_EndTime";

    private function setupBasicFields(){
        $this->appendInsertFields(self::$BASIC_INSERT_FIELDS);
        $this->appendBindings("issssss");
        $this->appendParametersString("?,?,?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->id,
                $this->eventRecord->title,
                $this->eventRecord->description,
                date("Y-m-d", $this->eventRecord->startDate),
                date("Y-m-d", $this->eventRecord->endDate),
                date("H:i", $this->eventRecord->startDate),
                date("H:i", $this->eventRecord->endDate)
            )
        );
    }

    private static $AUDIT_INSERT_FIELDS = "
        Event_SubmittedByMemberID,
        Event_LastEditedDate,
        Event_LastEditedByMemberID,
        Event_CreatedByMemberID,
        Event_CreatedByAdminID,
        Event_UpdatedByAdminID";

    private function setupAuditFields(){
        $this->appendInsertFields(self::$AUDIT_INSERT_FIELDS);
        $this->appendBindings("iiiii");
        $this->appendParametersString("?,CURRENT_TIMESTAMP,?,?,?,?");
        $this->appendParamValues(array(
                $this->memberId,
                $this->memberId,
                $this->memberId,
                $this->adminId,
                $this->adminId
            )
        );
    }

    private static $ONLINE_INSERT_FIELDS = "
        Event_IsNational,
        Event_IsOnline,
        Event_OnlineLocationLink,
        Event_OnlineLocationLabel";

    private function setupOnlineFields(){
        $this->appendInsertFields(self::$ONLINE_INSERT_FIELDS);
        $this->appendBindings("iiss");
        $this->appendParametersString("?,?,?,?");
        if ($this->eventRecord->isOnlineEvent){
            $this->appendParamValues(array(
                    $this->eventRecord->isNationalEvent ? 1 : 0,
                    $this->eventRecord->isOnlineEvent ? 1 : 0,
                    $this->eventRecord->onlineLocationLink."",
                    $this->eventRecord->onlineLocationLabel.""
                )
            );
        } else {
            $this->appendParamValues(array(
                    0,
                    0,
                    "",
                    ""
                )
            );
        }
    }

    private static $CAUSETOWN_EVENT_INSERT_FIELDS = "
        Event_IsCausetownEvent,
        Event_SuppressCharitySelectionForm,
        Event_SuppressPrintFlier";

    private function setupCausetownEventFields(){
        $this->appendInsertFields(self::$CAUSETOWN_EVENT_INSERT_FIELDS);
        $this->appendBindings("iii");
        $this->appendParametersString("?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->isCausetownEvent ? 1 : 0,
                $this->eventRecord->suppressCharitySelectionForm ? 1 : 0,
                $this->eventRecord->suppressPrintFlier ? 1 : 0
            )
        );
    }

    private static $PERCENTAGE_INSERT_FIELDS = "
        Event_MerchantPercentage,
        Event_MerchantSecondaryPercentage";

    private function setupPercentageFields(){
        $this->appendInsertFields(self::$PERCENTAGE_INSERT_FIELDS);
        $this->appendBindings("ii");
        $this->appendParametersString("?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->featuredCausePercentage,
                $this->eventRecord->venue->customerChoicePercentage
            )
        );
    }

    private static $FLAT_DONATION_INSERT_FIELDS = "
        Event_IsFlatDonation,
        Event_DonationPerUnit,
        Event_UnitDescriptor,
        Event_FlatDonationActionPhrase,
        Event_IsFcOnly";

    private function setupFlatDonationFields(){
        $this->appendInsertFields(self::$FLAT_DONATION_INSERT_FIELDS);
        $this->appendBindings("idssi");
        $this->appendParametersString("?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->venue->isFlatDonation ? 1 : 0,
                $this->eventRecord->venue->donationPerUnit,
                $this->eventRecord->venue->unitDescriptor,
                $this->eventRecord->venue->flatDonationActionPhrase,
                $this->eventRecord->venue->isFcOnly ? 1 : 0
            )
        );
    }

    private static $MISC_INSERT_FIELDS = "
        Event_ProposedByOrgID,
        Event_EventTemplateID,
        Event_EventTemplatePremiumID,
        Event_OrgWhyYou,
        Event_CustomAttendLink";

    private function setupMiscFields(){
        $this->appendInsertFields(self::$MISC_INSERT_FIELDS);
        $this->appendBindings("iiiss");
        $this->appendParametersString("?,?,?,?,?");
        $this->appendParamValues(array(
                $this->eventRecord->cause->id ? $this->eventRecord->cause->id : 0,
                $this->eventRecord->eventTemplateId,
                $this->eventRecord->venue->eventTemplatePremiumId ? $this->eventRecord->venue->eventTemplatePremiumId : 0,
                $this->eventRecord->whyHostThisDescription."",
                $this->eventRecord->customAttendLink.""
            )
        );
    }
}

/**
 * Class UpdateEventHelper
 * This helper just constructs the insert SQL, handles the bindings, and assigns the parameters for the bindings.
 * This is easier to debug since there are so many columns being inserted, and it is also easier to conditionally add sets of columns;
 */
class UpdateEventHelper{

    /* @var EventRecord */
    private $eventRecord;
    private $sqlUpdateString = "";
    private $sqlWhereString = "";
    private $sqlBindings = "";
    private $sqlParams = array();
    private $memberId;
    private $adminId;

    public function __construct(EventRecord $record, $memberId, $adminId){
        $this->eventRecord = $record;
        $this->memberId = $memberId ? $memberId : 0;
        $this->adminId = $adminId ? $adminId : 0;
    }

    /**
     * Sets up the insert sqlString, sqlBindings, and sqlParams. The passed in variables will be overwritten.
     * @param $sqlString
     * @param $sqlBindings
     * @param $sqlParams
     */
    public function setupUpdate(&$sqlString, &$sqlBindings, &$sqlParams){
        $this->setupBasicFields();
        $this->setupAuditFields();
        $this->setupOnlineFields();
        $this->setupCausetownEventFields();
        if ($this->eventRecord->venue->isFlatDonation){
            $this->setupFlatDonationFields();
        } else {
            $this->setupPercentageFields();
        }
        $this->setupMiscFields();
        $this->setupWhereClause();

        $sqlString = $this->getSqlString();
        $sqlBindings = $this->sqlBindings;
        $sqlParams = $this->sqlParams;
    }

    private function getSqlString(){
        return "UPDATE event_info SET ".$this->sqlUpdateString." WHERE ".$this->sqlWhereString;
    }

    private function appendUpdateFields($string){
        $this->sqlUpdateString .= (strlen($this->sqlUpdateString)) ? "," : "";
        $this->sqlUpdateString .= $string;
    }

    private function appendBindings($string){
        $this->sqlBindings .= $string;
    }

    private function appendParamValues(array $paramValues){
        $this->sqlParams = array_merge($this->sqlParams, $paramValues);
    }

    private function setupWhereClause(){
        $this->sqlWhereString = " Event_ID = ? AND Event_ProposedByMerchantID = ?";
        $this->appendParamValues(array(
            $this->eventRecord->id,
            $this->eventRecord->venue->id
        ));
        $this->appendBindings("ii");
    }

    private static $BASIC_UPDATE_FIELDS = "
        Event_Title = ?,
        Event_Description = ?,
        Event_Date = ?,
        Event_EndDate = ?,
        Event_StartTime = ?,
        Event_EndTime = ?";

    private function setupBasicFields(){
        $this->appendUpdateFields(self::$BASIC_UPDATE_FIELDS);
        $this->appendBindings("ssssss");
        $this->appendParamValues(array(
                $this->eventRecord->title,
                $this->eventRecord->description,
                date("Y-m-d", $this->eventRecord->startDate),
                date("Y-m-d", $this->eventRecord->endDate),
                date("H:i", $this->eventRecord->startDate),
                date("H:i", $this->eventRecord->endDate)
            )
        );
    }

    private static $AUDIT_UPDATE_FIELDS = "
        Event_LastEditedDate = CURRENT_TIMESTAMP,
        Event_LastEditedByMemberID = ?,
        Event_UpdatedByAdminID = ?";

    private function setupAuditFields(){
        $this->appendUpdateFields(self::$AUDIT_UPDATE_FIELDS);
        $this->appendBindings("ii");
        $this->appendParamValues(array(
                $this->memberId,
                $this->adminId
            )
        );
    }

    private static $ONLINE_UPDATE_FIELDS = "
        Event_IsNational = ?,
        Event_IsOnline = ?,
        Event_OnlineLocationLink = ?,
        Event_OnlineLocationLabel = ?";

    private function setupOnlineFields(){
        $this->appendUpdateFields(self::$ONLINE_UPDATE_FIELDS);
        $this->appendBindings("iiss");
        if ($this->eventRecord->isOnlineEvent){
            $this->appendParamValues(array(
                    $this->eventRecord->isNationalEvent ? 1 : 0,
                    $this->eventRecord->isOnlineEvent ? 1 : 0,
                    $this->eventRecord->onlineLocationLink."",
                    $this->eventRecord->onlineLocationLabel.""
                )
            );
        } else {
            $this->appendParamValues(array(
                    0,
                    0,
                    "",
                    ""
                )
            );
        }
    }

    private static $CAUSETOWN_EVENT_UPDATE_FIELDS = "
        Event_IsCausetownEvent = ?,
        Event_SuppressCharitySelectionForm = ?,
        Event_SuppressPrintFlier = ?";

    private function setupCausetownEventFields(){
        $this->appendUpdateFields(self::$CAUSETOWN_EVENT_UPDATE_FIELDS);
        $this->appendBindings("iii");
        $this->appendParamValues(array(
                $this->eventRecord->isCausetownEvent ? 1 : 0,
                $this->eventRecord->suppressCharitySelectionForm ? 1 : 0,
                $this->eventRecord->suppressPrintFlier ? 1 : 0
            )
        );
    }

    private static $PERCENTAGE_UPDATE_FIELDS = "
        Event_IsFlatDonation = 0,
        Event_DonationPerUnit = 0,
        Event_UnitDescriptor = '',
        Event_FlatDonationActionPhrase = '',
        Event_MerchantPercentage = ?,
        Event_MerchantSecondaryPercentage = ?,
        Event_IsFcOnly = 0";

    private function setupPercentageFields(){
        $this->appendUpdateFields(self::$PERCENTAGE_UPDATE_FIELDS);
        $this->appendBindings("ii");
        $this->appendParamValues(array(
                $this->eventRecord->venue->featuredCausePercentage,
                $this->eventRecord->venue->customerChoicePercentage
            )
        );
    }

    private static $FLAT_DONATION_UPDATE_FIELDS = "
        Event_MerchantPercentage = 0,
        Event_MerchantSecondaryPercentage = 0,
        Event_IsFlatDonation = ?,
        Event_DonationPerUnit = ?,
        Event_UnitDescriptor = ?,
        Event_FlatDonationActionPhrase = ?,
        Event_IsFcOnly = ?";

    private function setupFlatDonationFields(){
        $this->appendUpdateFields(self::$FLAT_DONATION_UPDATE_FIELDS);
        $this->appendBindings("idssi");
        $this->appendParamValues(array(
                $this->eventRecord->venue->isFlatDonation ? 1 : 0,
                $this->eventRecord->venue->donationPerUnit,
                $this->eventRecord->venue->unitDescriptor,
                $this->eventRecord->venue->flatDonationActionPhrase,
                $this->eventRecord->venue->isFcOnly ? 1 : 0
            )
        );
    }

    private static $MISC_UPDATE_FIELDS = "
        Event_ProposedByOrgID = ?,
        Event_EventTemplatePremiumID = ?,
        Event_OrgWhyYou = ?,
        Event_CustomAttendLink = ?";

    private function setupMiscFields(){
        $this->appendUpdateFields(self::$MISC_UPDATE_FIELDS);
        $this->appendBindings("iiss");
        $this->appendParamValues(array(
                $this->eventRecord->cause->id ? $this->eventRecord->cause->id : 0,
                $this->eventRecord->venue->eventTemplatePremiumId ? $this->eventRecord->venue->eventTemplatePremiumId : 0,
                $this->eventRecord->whyHostThisDescription."",
                $this->eventRecord->customAttendLink.""
            )
        );
    }
}