<?php
/**
 * Unfortunately, this provider will be tied to the db table event_transaction until a future refactoring.
 */
include_once("_getRootFolder.php");
include_once("Helper.php");
include_once($rootFolder."repository/ManagerAPI.php");
class InvoiceProvider{
    private $mysqli;
    private $integration;
    private $transactionProvider;
    private $logger;
    private $siteName;

    public function __construct(mysqli $mysqli, InvoiceIntegration $integration, TransactionProvider $transactionProvider, SystemLogger $logger, $siteName = "Causetown"){
        $this->mysqli = $mysqli;
        $this->integration = $integration;
        $this->transactionProvider = $transactionProvider;
        $this->logger = $logger;
        $this->siteName = $siteName;
    }

    /**
     * Pass through to support unit testing
     * @param $sqlString
     * @param $sqlBinding
     * @param $sqlParam
     * @param ExecuteResponseRecord $responseObject
     * @return number
     */
    protected function executeSql($sqlString, $sqlBinding, $sqlParam, ExecuteResponseRecord $responseObject = null){
        return MySqliHelper::execute($this->mysqli, $sqlString, $sqlBinding, $sqlParam, $responseObject);
    }

    public function log($sourceLocation, $message, $throwable = null){
        if ($this->logger){
            $this->logger->log($sourceLocation, $message);
        } else if ($throwable != null && is_a($throwable, "Exception")) {
            /* @var $throwable Exception */
            throw $throwable;
        }
    }

    /**
     * Optimistically creates a draft invoice with associated uninvoiced event transaction records.
     * If an eventId is specified, the invoice is limited to that event only. Otherwise, this will
     * add all uninvoiced transactions to the new invoice.
     *
     * Returns false if for any reason the invoice cannot be made (e.g., no pending transactions).
     * @param string $siteName
     * @param ReferralCodeRate $defaultRate
     * @param int $merchantId
     * @param int $eventId
     * @return InvoiceRecord|boolean
     */
    public function createDraftInvoice($siteName, ReferralCodeRate $defaultRate, $merchantId, $eventId = null){
        if (empty($eventId) || !is_numeric($eventId)){
            $eventId = 0;
        }
        try {
            $invoiceProviderId = $this->getInvoiceProviderId();
            $sp_query = "CALL create_draft_invoice(".$merchantId.",".$eventId.",".$invoiceProviderId.")";
            $generateInvoiceResult = $this->mysqli->query($sp_query);
            if (!$generateInvoiceResult){
                $message = "error calling create_draft_invoice. ".$this->mysqli->error;
                $this->log("InvoiceProvider.php createDraftInvoice", $message, new Exception("InvoiceProvider.php createInvoice: ".$message));
            } else {
                $result = $generateInvoiceResult->fetch_array();
                $invoiceId =  $result["invoice_id"];
                $this->log("InvoiceProvider.php createDraftInvoice", "Draft invoice created with id=".$invoiceId, null);

                //free the resultset
                $generateInvoiceResult->free();

                //clear the other result(s) from buffer
                //loop through each result using the next_result() method
                while ($this->mysqli->next_result()) {
                    //free each result.
                    $result = $this->mysqli->use_result();
                    if ($result instanceof mysqli_result) {
                        $result->free();
                    }
                }

                $invoiceObject = $this->getInvoiceRecord($merchantId, $invoiceId, $siteName, $defaultRate);
                return $invoiceObject;
            }
        } catch (Exception $e){
            try {
                $this->log("InvoiceProvider.php createDraftInvoice", $e->getMessage(), $e);
            } catch (Exception $e) {
            }
        }
        return false;
    }

    private static $SET_EXTERNAL_DATA_SQL = "
		UPDATE invoices
		SET Invoices_BillComInvoiceID = ?,
		    Invoices_Amount = ?,
		    Invoices_DonationAmount = ?,
		    Invoices_FeeAmount = ?,
			Invoices_Status = 'Created'
		WHERE Invoices_ID = ?";

    private static $VOID_FAILED_INVOICE_SQL = "
		UPDATE invoices
		SET Invoices_Status = 'Voided'
		WHERE Invoices_ID = ?";

    public function pushInvoices(array $invoiceRecords){
        $this->verifyInvoices($invoiceRecords);
        $resultRecords = $this->integration->createInvoice($invoiceRecords);
        $sqlString = self::$SET_EXTERNAL_DATA_SQL;
        $sqlBinding = "sdddi";
        foreach ($resultRecords as $invoiceRecord){
            /* @var $invoiceRecord InvoiceRecord */
            $externalInvoiceId = $invoiceRecord->getExternalInvoiceId();
            $invoiceId = $invoiceRecord->getInvoiceNumber();
            $totalAmount = $invoiceRecord->getAmountDue();
            if (isset($externalInvoiceId) && strlen($externalInvoiceId)){
                $sqlParams = array(
                    $externalInvoiceId,
                    $totalAmount,
                    $invoiceRecord->getDonationAmount(),
                    $invoiceRecord->getFeeAmount(),
                    $invoiceId
                );
                $response = new ExecuteResponseRecord();
                $this->executeSql($sqlString, $sqlBinding, $sqlParams, $response);
                if (!$response->success){
                    $message = "Not updating invoiceID ".$invoiceId." with External Id ".$externalInvoiceId." or Total Amount ".$totalAmount."(".$response->error.") ".$invoiceRecord->getApiRequest()." ".$invoiceRecord->getApiResponse();
                    try {
                        $this->log("InvoiceProvider.php pushInvoices", $message, new Exception($message));
                    } catch (Exception $e) {
                    }
                } else {
                    $message = "Success Creating ".$invoiceId." ".$invoiceRecord->getApiRequest()." ".$invoiceRecord->getApiResponse();
                    try {
                        $this->log("InvoiceProvider.php pushInvoices", $message, null);
                    } catch (Exception $e) {
                    }
                }
            } else {
                $response = new ExecuteResponseRecord();
                $this->executeSql(self::$VOID_FAILED_INVOICE_SQL, "i", array($invoiceRecord->getInvoiceNumber()), $response);
                if (!$response->success){
                    $message = "Not able to void invoice ".$invoiceId."(".$response->error."). ".$invoiceRecord->getApiRequest()." ".$invoiceRecord->getApiResponse();
                    try {
                        $this->log("InvoiceProvider.php pushInvoices", $message, new Exception($message));
                    } catch (Exception $e) {
                    }
                } else {
                    $message = "No External Invoice for ".$invoiceId." ".$invoiceRecord->getApiRequest()." ".$invoiceRecord->getApiResponse();
                    try {
                        $this->log("InvoiceProvider.php pushInvoices", $message, null);
                    } catch (Exception $e) {
                    }
                }
            }
        }
        return $resultRecords;
    }

    public function verifyInvoices(array $invoiceRecords){
        foreach($invoiceRecords as $invoiceRecord){
            /* @var $invoiceRecord InvoiceRecord */
            if (!($invoiceRecord->getCustomerId())){
                $merchantInfo = $invoiceRecord->getMerchantInfo();
                $customerId = $this->integration->createCustomer($invoiceRecord->getMerchantInfo());
                $merchantInfo->billingId = $customerId;
                $this->saveBillingId($merchantInfo);
            }
        }
    }

    private static $SAVE_BILLING_ID_SQL = "
        INSERT INTO merchant_billinginfo
        (
            MerchantBillingInfo_MerchantID,
            MerchantBillingInfo_BillComID
        )
        VALUES
        (?,?)";

    private function saveBillingId(MerchantInfo $merchantInfo){
        $sqlString = self::$SAVE_BILLING_ID_SQL;
        $sqlBinding = "is";
        $sqlParam = array($merchantInfo->merchantID, $merchantInfo->billingId);
        $response = new ExecuteResponseRecord();
        $this->executeSql($sqlString, $sqlBinding, $sqlParam, $response);
        return $response;
    }

    private static $GET_PROVIDER_SQL = "
        SELECT InvoiceProvider_ID as id
		FROM invoice_providers
		WHERE InvoiceProvider_Name = ?";

    private function getInvoiceProviderId(){
        $sqlString = self::$GET_PROVIDER_SQL;
        $sqlBinding = "s";
        $sqlParams = array($this->integration->getProviderName());
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams);
        if (count($result)){
            return $result[0]["id"];
        }
        return 0;
    }

    private static $GET_MERCHANT_BILLINGINFO_SQL = "
		SELECT
		  MerchantInfo_Name as Name,
		  MerchantInfo_OfficialName as CompanyName,
		  MerchantInfo_ContactFirstName as FirstName,
		  MerchantInfo_ContactLastName as LastName,
		  MerchantInfo_ContactPhone as Phone,
		  MerchantInfo_ContactEmail as Email,
		  MerchantInfo_Address as Address,
		  MerchantInfo_City as City,
		  MerchantInfo_State as State,
		  MerchantInfo_Zip as Zip,
		  MerchantInfo_Country as Country,
		  (
		    SELECT CodesCampaign_Code
		    FROM codes_campaign
		    WHERE CodesCampaign_id = MerchantInfo_CurrentReferralID
		  ) as ReferralCode,
		  (
		    SELECT MerchantBillingInfo_BillComId
		    FROM merchant_billinginfo
		    WHERE MerchantBillingInfo_MerchantID = MerchantInfo_ID
		  ) as customerId
		FROM merchant_info
		WHERE MerchantInfo_ID = ?";

    public function getMerchantBillingInfo($merchantId){
        $sqlString = self::$GET_MERCHANT_BILLINGINFO_SQL;
        $sqlBinding = "i";
        $sqlParams = array($merchantId);
        $response = new QueryResponseRecord();
        MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBinding, $sqlParams, $response);
        $record = new MerchantInfo();
        if ($response->success){
            $sqlResult = $response->resultArray;
            foreach($sqlResult as $row){
                if (!empty($row["ReferralCode"])){
                    $referralCodeData = ReferralCodeHelper::getReferralCodeByName($this->mysqli, $row["ReferralCode"]);
                    if (!empty($referralCodeData)){
                        $rateType = ReferralCodeRate::getRateTypeFromString($referralCodeData->serviceRateType);
                        $referralCodeRate = new ReferralCodeRate($row["ReferralCode"], $referralCodeData->validOnDate, $referralCodeData->validUntilDate, $rateType, $referralCodeData->serviceRateAmount);
                        $record->referralCodeRate = $referralCodeRate;
                    }
                }
                $record->merchantID = $merchantId;
                $record->billingId = $row["customerId"];
                $record->contactFirstName = $row["FirstName"];
                $record->contactLastName = $row["LastName"];
                $record->contactPhone = $row["Phone"];
                $record->contactEmail = $row["Email"];
                $record->addressLine1 = $row["Address"];
                $record->city = $row["City"];
                $record->state = StateNameAndAbbrConverter::nameToAbbr($row["State"]);
                $record->postalCode = $row["Zip"];
                $record->country = $row["Country"];
                $record->name = $row["Name"];
                $record->officialName = $row["CompanyName"];
                $record->billingCode = $row["ReferralCode"];
            }
        } else {
            $response = DbAdminLogger::log_message($this->mysqli, "InvoiceProvider.php getMerchantBillingInfo", "Unable to execute query ".$sqlString);
        }
        return $record;
    }

    /**
     * @param $merchantId int
     * @param $invoiceId int
     * @param $siteName string (e.g., "Causetown")
     * @param ReferralCodeRate $defaultRate
     * @return InvoiceRecord
     */
    public function getInvoiceRecord($merchantId, $invoiceId, $siteName, ReferralCodeRate $defaultRate){
        $merchantInfo = $this->getMerchantBillingInfo($merchantId);
        $invoiceRecord = new InvoiceRecord($invoiceId, $merchantInfo, $siteName, $this->integration->getWeeksUntilDue());

        // add invoice lines for donations
        $transactions = $this->transactionProvider->getInvoiceTransactions($invoiceId);
        foreach ($transactions as $transaction){
            /* @var $transaction EventTransactionRecord */
            $lineItem = DonationLineItemBase::buildLineItem($transaction, $merchantInfo, $defaultRate);
            $invoiceRecord->addDonationLineItem($lineItem);
        }

        // add invoice lines for fees
        $invoiceRecord->generateFeeLineItems($this->siteName);

        // TODO: add invoice line for periodic fee if due (e.g., monthly fee)

        return $invoiceRecord;
    }

    private static $MARK_INVOICE_PAID_SQL = "
        UPDATE invoices
        SET Invoices_Status = 'Funds Received'
        WHERE Invoices_ID = ?
        AND Invoices_Status = 'Created'";

    public function markAsPaid(InvoiceRecord $invoiceRecord){
        // at this time, we only allow this for mock invoice integration
        if ($this->integration->getProviderName() == "mock"){
            $sqlString = self::$MARK_INVOICE_PAID_SQL;
            $sqlBinding = "i";
            $sqlParam = array($invoiceRecord->getInvoiceNumber());
            $response = new ExecuteResponseRecord();
            $this->executeSql($sqlString, $sqlBinding, $sqlParam, $response);
            if ($response->success){
                try {
                    $this->log("InvoiceProvider.php markAsPaid", "Marked invoice as paid in db: " . $invoiceRecord->getInvoiceNumber() . ". ");
                } catch (Exception $e) {
                }
                return true;
            } else {
                try {
                    $this->log("InvoiceProvider.php markAsPaid", "Unable to mark invoice as paid in db: " . $invoiceRecord->getInvoiceNumber() . " " . $response->error);
                } catch (Exception $e) {
                }
            }
        } else {
            try {
                $this->log("InvoiceProvider.php markAsPaid", "Only the mock integration supports invoices marked paid (manually) at this time.");
            } catch (Exception $e) {
            }
        }
        return false;
    }

    private static $DELETE_VOIDED_INVOICE_SQL = "
        UPDATE event_transaction, invoices
            SET EventTransaction_InvoiceID = 0
        WHERE EventTransaction_InvoiceID = Invoices_ID
        AND Invoices_ID = ?
        AND Invoices_Status IN ('Voided', 'Creating')";

    /**
     * Inactivates a previously voided invoice and releases the event transactions so that they may be invoiced again.
     * @param InvoiceRecord $invoiceRecord
     * @return ExecuteResponseRecord
     */
    public function deleteDraftInvoice(InvoiceRecord $invoiceRecord){
        $sqlString = self::$DELETE_VOIDED_INVOICE_SQL;
        $sqlBinding = "i";
        $sqlParam = array( $invoiceRecord->getInvoiceNumber());
        $response = new ExecuteResponseRecord();
        $this->executeSql($sqlString, $sqlBinding, $sqlParam, $response);
        return $response;
    }

    private static $GET_UNPAID_INVOICE_SQL = "
		SELECT
		  Invoices_ID AS id,
		  Invoices_Amount AS amount,
		  Invoices_AmountPaid AS amountPaid,
		  Invoices_Status AS status,
		  Invoices_BillComInvoiceID AS externalId,
		  MerchantBillingInfo_BillComID AS externalCustomerId
		FROM invoices
		JOIN merchant_billinginfo ON Invoices_MerchantID = MerchantBillingInfo_MerchantID
		JOIN invoice_providers ON Invoices_ProviderID = InvoiceProvider_ID
		WHERE Invoices_AmountPaid < Invoices_Amount
		AND Invoices_Status != 'Funds Received'
		AND InvoiceProvider_Name = ?";

    public function getUnpaidInvoices($invoiceProviderName = "BillCom"){
        $sqlString = self::$GET_UNPAID_INVOICE_SQL;
        $sqlParam = array($invoiceProviderName);
        $sqlBinding = "s";
        $sqlResult = MySqliHelper::getResultWithLogging("InvoiceProvider.getUnpaidInvoices sql error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParam);
        $invoices = array();
        foreach ($sqlResult->resultArray as $dbRow){
            $invoices[] = SimpleInvoiceRow::mapTo($dbRow);
        }

        return $invoices;
    }

    public function applyPayments(array $invoicePayments){
        $numUpdated = 0;
        foreach ($invoicePayments as $invoicePayment){
            $result = $this->addPayment($invoicePayment);
            $numUpdated += $result->affectedRows;
        }
        if ($numUpdated){
            $this->updateInvoiceStatus();
        }
    }

    private static $ADD_INVOICE_PAYMENT_SQL = "
		INSERT INTO invoices_payments
		(
			InvoicesPayments_BillComInvoiceID,
			InvoicesPayments_PaymentID,
			InvoicesPayments_PaymentDate,
			InvoicesPayments_Amount
		) VALUES (
				?,?,?,?
		)
		ON DUPLICATE KEY UPDATE InvoicesPayments_PaymentID = InvoicesPayments_PaymentID";

    private function addPayment(InvoicePayment $invoicePayment){
        $sqlString = self::$ADD_INVOICE_PAYMENT_SQL;
        $sqlBindings = "sssd";
        $sqlParams = array(
            $invoicePayment->externalInvoiceId,
            $invoicePayment->externalPaymentId,
            $invoicePayment->paymentDate,
            $invoicePayment->amountPaid
        );
        return MySqliHelper::executeWithLogging("InvoiceProvider.addPayment error:", $this->mysqli, $sqlString, $sqlBindings, $sqlParams);
    }

    private static $UPDATE_INVOICE_PAIDAMOUNT_STATUS_SQL = "
		UPDATE invoices dest, (
            SELECT
                (SELECT SUM(InvoicesPayments_Amount) FROM invoices_payments WHERE InvoicesPayments_BillComInvoiceID = i.Invoices_BillComInvoiceID) as paidAmount,
                i.Invoices_BillComInvoiceID
            FROM invoices i
            JOIN invoice_providers ON Invoices_ProviderID = InvoiceProvider_ID
            WHERE i.Invoices_Status <> 'Funds Received'
            AND InvoiceProvider_Name = ?
            AND EXISTS (
                SELECT 1
                FROM invoices_payments
                WHERE InvoicesPayments_BillComInvoiceID = Invoices_BillComInvoiceID
            )
        ) as src
        SET Invoices_Status = CASE
            WHEN src.paidAmount < Invoices_Amount THEN 'Partial Payment'
            WHEN src.paidAmount >= Invoices_Amount THEN 'Funds Received'
            ELSE dest.Invoices_status
            END,
            Invoices_AmountPaid = src.paidAmount
        WHERE src.Invoices_BillComInvoiceID = dest.Invoices_BillComInvoiceID";

    public function updateInvoiceStatus($invoiceProviderName = "BillCom"){
        $sqlString = self::$UPDATE_INVOICE_PAIDAMOUNT_STATUS_SQL;
        $sqlBinding = "s";
        $sqlParam = array($invoiceProviderName);
        return MySqliHelper::executeWithLogging("InvoiceProvider.updateInvoiceStatus error:", $this->mysqli, $sqlString, $sqlBinding, $sqlParam);
    }
}