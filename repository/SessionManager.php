<?php
include_once("AuthContracts.php");

class SessionManagerBase {
    static function sessionStart($name, $limit = 0, $path = '/', $domain = null, $secure = null)
    {
        // Set the cookie name before we start.
        session_name($name . '_Session');


        // Set the domain to default to the current domain.
        $domain = isset($domain) ? $domain : $_SERVER['SERVER_NAME'];

        // NOTE: do not set the domain to "localhost" because chrome has disabled secure session cookies on localhost
        if (strtolower($domain) == 'localhost'){
            $domain = null;
        }

        // Set the default secure value to whether the site is being accessed with SSL
        $https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);

        // Set the cookie settings and start the session
        session_set_cookie_params($limit, $path, $domain, $https, true);
        session_start();
        // Make sure the session hasn't expired, and destroy it if it has
        if(self::validateSession())
        {
            // Check to see if the session is new or a hijacking attempt
            if(!self::preventHijacking())
            {
                // Reset session data and regenerate id
                $_SESSION = array();
                $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                self::regenerateSession();

                // Give a 5% chance of the session id changing on any request
            }elseif(rand(1, 100) <= 5){
                self::regenerateSession();
            }
        }else{
            $_SESSION = array();
            session_destroy();
            session_start();
        }
    }

    static function regenerateSession()
    {
        // If this session is obsolete it means there already is a new id
        if(isset($_SESSION['OBSOLETE']) || $_SESSION['OBSOLETE'] == true){
			return;
		}
        // Set current session to expire in 10 seconds
        $_SESSION['OBSOLETE'] = true;
        $_SESSION['EXPIRES'] = time() + 10;

        // Create new session without destroying the old one
        session_regenerate_id(false);

        // Grab current session ID and close both sessions to allow other scripts to use them
        $newSession = session_id();
        session_write_close();

        // Set session ID to the new one, and start it back up again
        session_id($newSession);
        session_start();
        // Now we unset the obsolete and expiration values for the session we want to keep
        unset($_SESSION['OBSOLETE']);
        unset($_SESSION['EXPIRES']);
    }

    static protected function validateSession()
    {
        if( isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES']) ){
            return false;
		}
        if(isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time()){
			return false;
		}	
        return true;
    }

    static protected function preventHijacking()
    {
        if(!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent'])){
            return false;
		}
        if($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR']){
            return false;
		}
        if( $_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT']){
            return false;
		}
        return true;
    }
}

/**
 * Class SessionManager
 * Utility class to secure and start a session
 */
class SessionManager{
    const NO_AUTHATTEMPT_OBJECT_ERROR = "SessionManager.login: login attempt when AuthAttempt object is invalid.";
    const NO_MEMBERSESSION_OBJECT_ERROR = "SessionManager.login: User logged in without a Session Data object.";

    private static $reservedKeys = array(
        'logged',
        'MemberID',
        'username',
        'email',
        'MemberTypeDescription',
        'StartPage',
        'LastPage',

        'pid',
        'defaultLat',
        'defaultLong',
        'defaultLocation',
        'defaultZip',

        'IndividualID',

        'MerchantID',
        'MerchantPID',
        'MerchantName',
        'IsPlanSubscriber',

        'OrgID',
        'OrganizationName',
        'EIN'
    );

    public static function start($sitename, $cookieLimitSeconds = 0, $cookiePath = '/', $cookieDomain = null, $secure = true){
        $sitename = preg_replace("/[^A-Za-z0-9 ]/", '', $sitename);
        SessionManagerBase::sessionStart($sitename, $cookieLimitSeconds, $cookiePath, $cookieDomain, $secure);
    }

    // TODO: move this somewhere else -- it doesn't belong here at all
    private static function setRememberMe($authAttempt, $rememberMe){
        //keep cookie if indicated
        if ($rememberMe){
            $expire=time()+60*60*24*30;
            setcookie("username", $authAttempt->attemptedUsername, $expire);
        } else {
            setcookie("username", "", time()-3600);
        }
    }

    public static function login(AuthAttempt $authAttempt, MemberSessionBase $sessionBase = null, $rememberMe = false, $testMode = false){
        if (isset($_SESSION["LastPage"])){
            $redirectPageTarget = $_SESSION["LastPage"];
        }
        self::logout($testMode);

        if (isset($redirectPageTarget)){
            $_SESSION["LastPage"] = $redirectPageTarget;
        }

        if (isset($authAttempt) && !($authAttempt->message)){
            $_SESSION['logged'] = true;

            self::setUserSessionVariable('MemberID', $authAttempt->memberId);
            self::setUserName(htmlspecialchars($authAttempt->memberFullName));
            self::setUserEmail(htmlspecialchars($authAttempt->memberEmail));
            self::setStartPage($authAttempt->startPage);
            if ($authAttempt->isFirstTime){
                self::setStartPage("FirstTimeLogin");
            }

            if (!$testMode){
                self::setRememberMe($authAttempt, $rememberMe);
            }

            if ($sessionBase){
                self::setUserSessionVariable('MemberTypeDescription', $sessionBase->entityType);
                self::setUserSessionVariable('pid', $sessionBase->pid);
                self::setUserSessionVariable('defaultLat', $sessionBase->lat);
                self::setUserSessionVariable('defaultLong', $sessionBase->long);
                self::setUserSessionVariable('defaultLocation', $sessionBase->cityStateZip);
                self::setUserSessionVariable('defaultZip', $sessionBase->zip);
                switch ($sessionBase->entityType){
                    case "Individual":
                        self::setUserSessionVariable('IndividualID', $sessionBase->id);
                        break;
                    case "Merchant":
                        self::setUserSessionVariable('MerchantID', $sessionBase->id);
                        self::setUserSessionVariable('MerchantPID', $sessionBase->pid);
                        self::setUserSessionVariable('MerchantName', $sessionBase->name);
                        self::setUserSessionVariable('IsPlanSubscriber', $sessionBase->isPlanSubscriber);
                        break;
                    case "Organization":
                        self::setUserSessionVariable('OrgID', $sessionBase->id);
                        self::setUserSessionVariable('OrganizationName', $sessionBase->name);
                        self::setUserSessionVariable('EIN', $sessionBase->ein);
                        break;
                }
            } else {
                trigger_error(self::NO_MEMBERSESSION_OBJECT_ERROR, E_USER_ERROR);
            }
        } else {
            trigger_error(self::NO_AUTHATTEMPT_OBJECT_ERROR, E_USER_ERROR);
        }
    }

    public static function logout($testMode = false){
        foreach (self::$reservedKeys as $varName){
            unset($_SESSION[$varName]);
        }
        self::unsetTmpVar('LoginAlert');
        self::unsetTmpVar('Alert');
        if (!$testMode){
            SessionManagerBase::regenerateSession();
        }
    }

    public static function setTmpVar($key, $value){
        if (!in_array($key, self::$reservedKeys) && !in_array($key, self::$adminReservedKeys)){
            $_SESSION[$key] = $value;
        } else {
            trigger_error("Attempt to set session variable for reserved key '".$key."' to value '".$value."'", E_USER_ERROR);
        }
    }

    public static function getTmpVar($key){
        if (!in_array($key, self::$reservedKeys) && !in_array($key, self::$adminReservedKeys)){
            return $_SESSION[$key];
        } else {
            trigger_error("Attempt to get session variable for reserved key '".$key."'", E_USER_ERROR);
        }
        return null;
    }

    public static function unsetTmpVar($key){
        if (!in_array($key, self::$reservedKeys) && !in_array($key, self::$adminReservedKeys)){
            unset($_SESSION[$key]);
        } else {
            trigger_error("Attempt to unset session variable for reserved key '".$key."'", E_USER_ERROR);
        }
    }

    public static function pushValidationMessage($message){
        self::setTmpVar('Alert', $message);
    }

    public static function peekValidationMessage(){
        return self::getTmpVar('Alert');
    }

    public static function popValidationMessage(){
        $message = self::getTmpVar('Alert');
        self::unsetTmpVar('Alert');
        return $message;
    }

    public static function isLoggedIn(){
        return isset($_SESSION['logged']) && $_SESSION['logged'];
    }

    private static function setUserSessionVariable($key, $value){
        if (self::isLoggedIn()){
            $_SESSION[$key] = $value;
        }
    }

    private static function getUserSessionVariable($key, $default = null){
        return self::isLoggedIn() ? $_SESSION[$key] : $default;
    }

    public static function getUserName(){
        return self::getUserSessionVariable('username');
    }

    public static function setUserName($firstName, $lastName = null){
        self::setUserSessionVariable('username', trim($firstName." ".$lastName));
    }

    public static function getUserEmail(){
        return self::getUserSessionVariable('email');
    }

    public static function setUserEmail($value){
        self::setUserSessionVariable('email', $value);
    }

    public static function getMemberId(){
        return self::getUserSessionVariable('MemberID', 0);
    }

    public static function getMemberType(){
        return self::getUserSessionVariable('MemberTypeDescription');
    }

    public static function getUserPID(){
        return self::getUserSessionVariable('pid');
    }

    public static function getUserZip(){
        return self::getUserSessionVariable('defaultZip');
    }

    public static function getUserLat(){
        return self::getUserSessionVariable('defaultLat');
    }

    public static function getUserLong(){
        return self::getUserSessionVariable('defaultLong');
    }

    public static function getUserCityStateZip(){
        return self::getUserSessionVariable('defaultLocation');
    }

    public static function isIndividual(){
        return self::isLoggedIn() && isset($_SESSION['IndividualID']) && $_SESSION['IndividualID'] > 0;
    }

    public static function getIndividualId(){
        return self::isIndividual() ? $_SESSION['IndividualID'] : 0;
    }

    public static function isMerchant(){
        return self::isLoggedIn() && isset($_SESSION['MerchantID']) && $_SESSION['MerchantID'] > 0;
    }

    public static function getMerchantId(){
        return self::isMerchant() ? $_SESSION['MerchantID'] : 0 ;
    }

    public static function getMerchantPID(){
        return self::isMerchant() ? $_SESSION['MerchantPID'] : null;
    }

    public static function getMerchantName(){
        return self::isMerchant() ? $_SESSION['MerchantName'] : null;
    }

    public static function setMerchantName($value){
        if (self::isMerchant()){
            self::setUserSessionVariable('MerchantName', $value);
        }
    }

    public static function isPlanSubscriber(){
        return self::getUserSessionVariable("IsPlanSubscriber", false);
    }

    public static function isOrganization(){
        return self::isLoggedIn() && isset($_SESSION['OrgID']) && $_SESSION['OrgID'] > 0;
    }

    public static function getOrganizationId(){
        return self::isOrganization() ? $_SESSION['OrgID'] : 0;
    }

    public static function getOrganizationName(){
        return self::isOrganization() ? $_SESSION['OrganizationName'] : null;
    }

    public static function setOrganizationName($value){
        if (self::isOrganization()){
            self::setUserSessionVariable('OrganizationName', $value);
        }
    }

    public static function getOrganizationEIN(){
        return self::isOrganization() ? $_SESSION['EIN'] : null;
    }

    public static function getLastPage(){
        return $_SESSION['LastPage'];
    }

    public static function setLastPage($pageUrl){
        $_SESSION['LastPage'] = $pageUrl;
    }

    public static function getStartPage(){
        return $_SESSION['StartPage'];
    }

    public static function setStartPage($pageName){
        $_SESSION['StartPage'] = $pageName;
    }

    public static function getLastPageForRedirect(){
        $lastPage = self::getLastPage();
        unset($_SESSION['StartPage']);
        unset($_SESSION['LastPage']);
        return $lastPage;
    }

    //
    // Admin login
    //

    private static $adminReservedKeys = array(
        'AdminID',
        'AdminEmail',
        'AdminUsername',
        'AdminAuthObject'
    );

    public static function loginAdmin(AdminAuthAttempt $authAttempt){
        self::logoutAdmin();
        $_SESSION['AdminID'] = $authAttempt->adminId;
        $_SESSION['AdminEmail'] = $authAttempt->adminEmail;
        $_SESSION['AdminUsername'] = $authAttempt->attemptedUsername;
        $_SESSION['AdminAuthObject'] = (array)$authAttempt;
    }

    public static function logoutAdmin($testMode = false){
        foreach (self::$adminReservedKeys as $varName){
            unset($_SESSION[$varName]);
        }

        if (!$testMode){
            SessionManagerBase::regenerateSession();
        }
    }

    public static function isAdmin(){
		if (isset($_SESSION['AdminID']) && trim($_SESSION['AdminID']) !=""){
			return true;
		}else{
			return false;
		}
    }

    public static function getAdminId(){
        return self::isAdmin() ? $_SESSION['AdminID'] : 0;
    }

    private static function getAdminSessionVariable($key, $default = null){
        return self::isAdmin() ? $_SESSION[$key] : $default;
    }

    public static function getAdminEmail(){
        return self::getAdminSessionVariable("AdminEmail");
    }

    public static function getAdminUsername(){
        return self::getAdminSessionVariable("AdminUsername");
    }

    public static function getAdminAuthObject(){
        return self::isAdmin() ? (object)$_SESSION['AdminAuthObject']: null;
    }
}