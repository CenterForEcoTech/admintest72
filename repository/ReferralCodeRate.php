<?php
class ReferralCodeRate{
    const PercentageType = 0;
    const FlatRateType = 1;
    const AnnualRateType = 2;

    private $referralCode;
    private $validOnDate = null;
    private $validUntilDate = null;
    private $validOnTime = null;
    private $validUntilTime = null;
    private $rateType = null;
    private $rateAmount = null;

    public function __construct($referralCode, $validOnDate, $validUntilDate, $rateType, $rateAmount){
        $this->referralCode = $referralCode;
        $this->rateType = $rateType;
        $this->rateAmount = $rateAmount;
        $this->validOnDate = $validOnDate;
        $this->validUntilDate = $validUntilDate;

        if ($validOnDate !== "0000-00-00" && strtotime($validOnDate) !== false){
            $this->validOnTime = strtotime($validOnDate);
        }

        if ($validUntilDate !== "0000-00-00" && strtotime($validUntilDate) !== false){
            $this->validUntilTime = strtotime($validUntilDate);
        }
    }

    public function isUpgradeReady(){
        return $this->rateType == self::PercentageType;
    }

    public function jsonSerialize(){
        return (object)array(
            "referralCode" => $this->referralCode,
            "rateType" => $this->rateType,
            "rateAmount" => $this->rateAmount,
            "validOnDate" => $this->validOnDate,
            "validUntilDate" => $this->validUntilDate,
            "validOnTime" => $this->validOnTime,
            "validUntilTime" =>$this->validUntilTime,
            "currentTime" => time()
        );
    }

    public static function getRateTypeFromString($rateTypeString){
        switch (strtolower($rateTypeString)){
            case "percentage":
                return self::PercentageType;
            case "flatfee":
                return self::FlatRateType;
            case "annual":
                return self::AnnualRateType;
            default;
                return self::PercentageType;
        }
    }

    public function getBillingCode(){
        return $this->referralCode;
    }

    public function getTransactionFeePercentage(){
        if ($this->rateType == self::PercentageType){
            return $this->rateAmount;
        }
        return 0;
    }

    public function getMonthlyCost(){
        if ($this->rateType == self::FlatRateType){
            return $this->getPeriodicFee();
        }
        if ($this->rateType == self::AnnualRateType){
            return $this->rateAmount / 12;
        }
        return "";
    }

    public function getPeriodicFee(){
        if ($this->rateType != self::PercentageType){
            $dollarsOnly = floor($this->rateAmount);
            if ($this->rateAmount - $dollarsOnly < 0.001){
                return $dollarsOnly;
            }
        }
        return 0;
    }

    public function getFeePeriod(){
        if ($this->rateType == self::FlatRateType){
            return "month";
        }
        if ($this->rateType == self::AnnualRateType){
            return "year";
        }
        return "";
    }

    /**
     * This runs the calculation against this rate without regard to dates.
     * @param $applicableAmount
     * @return float|null
     */
    public function calculateDefaultAmount($applicableAmount){
        if ($this->rateType == self::PercentageType){
            return $applicableAmount * $this->rateAmount / 100;
        } else if ($this->rateType == self::FlatRateType){
            // this is not really applicable here since it is a flat rate--it should really be handled as a scheduled
            // transaction; Handled manually for now, so always report zero here.
            return 0.0; // FIXME: or eliminate FlatRateType here
        } else if ($this->rateType == self::AnnualRateType){
            return 0.0;
        }
        return 0.0;
    }

    /**
     * Returns the calculated fraction of the applicable amount, assuming the rate applies to the date specified.
     * If the rate does not apply to the date, then the rate calculated against the $defaultRate will be returned.
     * @param $applicableAmount
     * @param $applicableDate
     * @param ReferralCodeRate $defaultRate (this should be a "valid all the time" rate).
     * @return float
     */
    public function calculateAmount($applicableAmount, $applicableDate, ReferralCodeRate $defaultRate = null){
        if ($this->isApplicable($applicableDate)){
            return $this->calculateDefaultAmount($applicableAmount);
        } else if ($defaultRate != null){
            return $defaultRate->calculateDefaultAmount($applicableAmount);
        }
    }

    /**
     * Returns true if this referral code is applicable to the specified date
     * @param $thisDate
     * @return bool
     */
    public function isApplicable($thisDate){
        if (empty($this->validOnTime) && empty($this->validUntilTime)){
            return true; // always valid; nevermind if the user passed in an invalid date
        }

        $thisTime = strtotime($thisDate);
        if ($thisTime === false){
            // not a valid date
            return false;
        }

        if ($this->validOnTime && $thisTime < $this->validOnTime){
            return false; // we haven't reached the valid on date yet.
        }

        if ($this->validUntilTime && $thisTime > $this->validUntilTime){
            return false; // we have passed the valid until date.
        }
        // by this time, we're good!
        return true;
    }

    /**
     * Returns a value to be used as a key in an array that collects line items based on this referral code rate.
     * @return string
     */
    public function getDiscriminator(){
        if (strlen($this->referralCode)){
            return $this->referralCode;
        }
        return "Standard Rate";
    }

    public function getExpirationNotice(){
        if ($this->validUntilTime != null && time() < $this->validUntilTime){
            return "Your Referral Code '".$this->referralCode."' will expire on  ".date("n/j/Y", $this->validUntilTime);
        }
        return "";
    }

    public function getDescription($siteName = "CETDashboard")
    {
        $text = $this->getExpirationNotice();
        $text .= "  Service Fee for using ".$siteName;
        if ($this->rateType == self::PercentageType){
            $text .=  " is ".$this->rateAmount."% of Sales Items.";
        }
        if ($this->rateType == self::FlatRateType){
            $text .=  " is ".$this->rateAmount." per month invoiced separately.";
        }
        if ($this->rateType == self::AnnualRateType){
            $text .=  " is ".$this->rateAmount." per year invoiced separately.";
        }
        return trim($text);
    }

    /**
     * @static
     * @returns ReferralCodeRate
     */
    public static function getDefaultRate(){
        return new ReferralCodeRate(null,null,null,self::PercentageType, 3);
    }
}
?>