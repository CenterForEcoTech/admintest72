<?php
include_once("DataContracts.php");

/**
 * This is the event service API
 *
 */
interface eventAPI {
	/**
	 * Returns all valid future events, defined by the business rules.
	 * @return array of EventRecord
	 */
	public function getAll();

    /**
     * Returns the next x upcoming events in ascending order, without regard to location
     * @param $startIndex
     * @param $maxRecords integer number of records to return
     * @return array of EventRecord
     */
	public function getNext($startIndex, $maxRecords);
}

/**
 * Utility service
 */
interface LocationProvider{
    /**
     * Get state abbreviation, returns 2-letter abbreviation or null
     * @param $stateName
     * @internal param $stateAbr
     * @return string
     */
    public function getStateAbbr($stateName);

    /**
     * Given a Venue data object (see DataContracts), returns the relevant DateTimeZone
     * @param $venue
     * @return DateTimeZone
     */
    public function getTimeZone(Venue $venue);

    /**
     * Given a Venue data object (see DataContracts), returns the relevant FIPS code (based on zip code)
     * @abstract
     * @param Venue $venue
     * @return string
     */
    public function getFIPSCode(Venue $venue);
}

class CharityBase {
    public $ein = "";
    public $name = "";
    public $address1 = "";
    public $address2 = "";
    public $city = "";
    public $region = "";
    public $postalCode = "";
    public $country = "";
    public $phone = "";
    public $url = "";
    public $NTEECode = "";
    public $lat = "";
    public $long = "";

    /**
     * Although not strongly typed here, $companyRecord is expected to be of type OrganizationRecord (see MemberProvider)
     * @param $companyRecord
     */
    public function mapToOrgRecord($companyRecord){
        $companyRecord->ein = $this->ein;
        // DO NOT MAP ANYTHING TO THE FiscalSponsorEIN OR YOU WILL CAUSE AN INFINITE LOOP
        $companyRecord->name = ucwords(strtolower($this->name));
        $companyRecord->address = $this->address1;
        $companyRecord->city = $this->city;
        $companyRecord->state = $this->region;
        $companyRecord->postalCode = $this->postalCode;
        $companyRecord->phone = $this->phone;
        $companyRecord->website = $this->url;
        $companyRecord->country = $this->country;
        $companyRecord->lat = $this->lat;
        $companyRecord->long = $this->long;
        $companyRecord->officialname = $this->name;
        return $companyRecord;
    }
}

class CharitySearchCriteria extends PaginationCriteria{
    public $term = "";
    public $state = "";
    public $isRegistered = false;
}

interface CharitySearchEngine {

    /**
     * Given a CharitySearchCriteria, returns a PaginatedResult object.
     * @param CharitySearchCriteria $criteria
     * @return PaginatedResult containing a collection of CharityBase
     */
    public function search(CharitySearchCriteria $criteria);
}

interface CharityLookup{
    
    /**
     * Also called "reverse lookup," given a tax id (or EIN) lookup the public charity that matches.
     * Expectations:
     *      1) It returns zero or one match (never returns fiscally sponsored charities)
     *      2) When possible, it returns no match for a charity that has lost its IRS status.
     * @param $ein
     * @return CharityBase
     */
    public function getPublicCharity($ein);
}
?>