<?php
require_once('Indexing_API.php');

$IndexingProvider = new Indexing_Api($IndexingProviderURL);

/*
//Here is an example of creating a public searchable index called IndexName
$IndexOptions = array('public_search'=>true);
$CreateIndex = $IndexingProvider->create_index($IndexName,$IndexOptions);
print_r($CreateIndex);
*/

/*
//Here is an example of getting a object of all the indexes
$ListIndex = $IndexingProvider->list_indexes();
print_r($ListIndex);
*/

//Calling the Indexing_Index calls requires the IndexingProvider and an IndexName
$IndexingThisIndex = new Indexing_Index($IndexingProvider,$IndexName);

/*
//Here is an example of testing if IndexName exists returns 1=true
$IndexExists = $IndexingThisIndex->exists();
print_r($IndexExists);
*/

/*
//Here is an example of testing if IndexName has started returns 1=true else reponse message
$IndexStarted = $IndexingThisIndex->has_started();
print_r($IndexStarted);
*/

/*
//Here is an example of testing the status of IndexName has started returns status of 'Live' if exists
$IndexStatus = $IndexingThisIndex->get_status();
print_r($IndexStatus);
*/

/*
//Here is an example of getting IndexName code to return string of Code
$IndexCode = $IndexingThisIndex->get_code();
print_r($IndexCode);
*/

/*
//Here is an example of getting IndexName document size as an integer of number of documents
$IndexSize = $IndexingThisIndex->get_size();
print_r($IndexSize);
*/

/*
//Here is an example of getting IndexName creation time as YYYY-MM-DDTHH:MM:SS
$IndexCreationTime = $IndexingThisIndex->get_creation_time();
print_r($IndexCreationTime);
*/

/*
//Here is an example of IndexName is publich searachable returns 1=true
$IndexIsPublic = $IndexingThisIndex->is_public_search_enabled();
print_r($IndexIsPublic);
*/

/*
//Here is an example of Updateing IndexName returns http code 200 if successful and 204 if IndexName already exists
//Might not be working properly
$IndexOptions = array('public_search'=>true);
$IndexUpdate = $IndexingThisIndex->update_index($options);
print_r($IndexUpdate);
*/

/*
//Here is an example of Deleting an IndexName returns http code 200 if successful
//$IndexDelete = $IndexingThisIndex->delete_index();
print_r($IndexDelete);
*/

/*
//Here is an example of adding a single new document to IndexName returns http code 200 if successful
$docid = rand(10,1000);
$fields = array('name'=>'yosh','email'=>'yosh@causetown','address'=>'Millerton ny 12546','occupation'=>'computer guru','mylocation'=>'[30.2669,-97.7428]');
$variables = array(1=>12,2=>10,3=>45);
$categories = array('Type'=>'computers','City'=>'millerton','State'=>'New York');

$IndexAddDocument = $IndexingThisIndex->add_document($docid, $fields,$variables,$categories);
print_r($IndexAddDocument);
*/

/*
//Here is an example of adding a multiple documents to IndexName returns array of success information [added] =1 true or [error] if not successful
//we are adding the lat and long as document variables for easier scoring
$x = 1;
while ($x < 10){
	$var1 = $x+1;
	$var2 = $x+2;
	$var3 = $x+3;
	$name = "yosh ".$x;
	$lat = "30.266".$x;
	$long = "-97.7428".$x;
	$datestr = (2+$x);
	//to represent event date
	$dateval = strtotime("10/".$datestr."/2012");

	$mylocation = "[".$lat.",".$long."]";
	$documents[$x]["docid"] = rand(10,1000);
	$documents[$x]["fields"] = array('name'=>$name,'email'=>'andrew@causetown','address'=>'New York ny 12546','occupation'=>'computer guru','mylocation'=>$mylocation);
	$documents[$x]["variables"] = array(0=>$lat,1=>$long,2 =>$dateval);
	$documents[$x]["categories"] = array('Type'=>'computers','City'=>'millerton','State'=>'New York');
	$x++;
}

$IndexAddDocuments = $IndexingThisIndex->add_documents($documents);
print_r($IndexAddDocuments);
*/

/*
//Here is an example of deleting a document based on docid, returns http 200
//$IndexDeleteDocument = $IndexingThisIndex->delete_document(37);
*/

/*
//Here is an example of deleting a documents based on docid, returns http 200 if successful
$docids = array("37","14","15");
//$IndexDeleteDocuments = $IndexingThisIndex->delete_documents($docids);
*/

/*
//Here is an example of a search query.  Returns array ["matches"] = integer of results, ["results"] is an object with ["docid"] and ["query_relevance_score"] along with all fields if fetch='*'
$function_filters = NULL; //An array with filters for function scores
//Example: array(2 => array(array(2,6), array(7, 11), array(15,NULL)))
//Scoring function 2 must return a value between 2 and 6 OR 7 and 11 OR greater than 15 for documents matching this query.
$docvar_filters = NULL; //Example array(0 => array(array(1,3), array(2,NULL))); Document variable 0 should be between 1 and 3 OR greater than 2
$variables = array(0 =>30.2669, 1=>-97.7428, 2=>strtotime("10/5/2012")); //Which variables should be present when scoring with a function defined in scoring_functions
//$variables = NULL;
//An array with filters for document variables
$category_filters = array('Type' => array('computers', 'business'),'City'=>array('Millerton')); //Document category Type should be 'computers' OR 'buisness' AND type 'City' should be 'Millerton'
$category_filters = NULL;
$fetch_fields = array('*'); //retrieves all fields
//$fetch_fields = NULL;
$snippet_fields = NULL;
$scoring_function = (1); //use function 1
//$scoring_function = NULL; 
$len = 0; //how many records to show
$start = 0; //Beginnig record
$query = "name:yosh&fetch_variables=true&fetch_categories=true";
//possible to add match_any_field=true to search all fields not just ones defined in query
//$query = "name:1,address:New York";
//+var0=30.2669+var1=-97.7428";
$IndexSearch = $IndexingThisIndex->search($query, $start, $len,$scoring_function,$snippet_fields, $fetch_fields, $category_filters, $variables, $docvar_filters, $function_filters);
print_r($IndexSearch);
*/

/*
//Here is an example of looping through the search results and updating the variables for the docID
//Apparently you have to update all the variables.  You cannot just update one
$x = 0;
while ($x < count($IndexSearch->results)){
	$datestr = (2+$x);
	$lat = "30.266".$x;
	$long = "-97.7428".$x;

	$dateval = strtotime("10/".$datestr."/2012");
	$docid =  (string)$IndexSearch->results[$x]->docid;
	$variables = array(0=>$lat,1=>$long,2 =>$dateval);
	//print_r($variables);
	$IndexUpdateVariables = $IndexingThisIndex->update_variables($docid, $variables);
	print_r($IndexUpdateVariables);
	$x++;
}
*/

/*
//Here is an example of updaing variables by docid
$variables = array(0 =>99);
$IndexUpdateVariables = $IndexingThisIndex->update_variables('662', $variables);
print_r($IndexUpdateVariables);
*/

/*
//Here is an example of updaing categoried by docid
$catgeories['City']='Largo';
$IndexUpdateCategories = $IndexingThisIndex->update_categories('662', $catgeories);
print_r($IndexUpdateCategories);
*/
/*
//Here is an example of promoting a docid
$query = "name:yosh";
$IndexPromote = $IndexingThisIndex->promote('121', $query);
print_r($IndexPromote);
*/

/*
//Here is an example of retrieving all functions for IndexName
$IndexFunctionList = $IndexingThisIndex->list_functions();
print_r($IndexFunctionList);
*/

//The following three functions are created to 0 sort by events dates closest in the future, 1 farthest in the future, 2 distance function for houndsleuth, 3, distance function for IndexDen
//Requires variable 0 to be lat, 1 to be long, 3 timestamp of event date
/*
//Here is an example of adding a function to an IndexName that sets the function index to 0, and sorts by event date which is stored as a timestamp as document variable 2
$IndexFunctionDelete = $IndexingThisIndex->delete_function('0');
$function_index = '0';
$definition = "if(doc.var[2] > q[2], doc.var[2], age)";
$IndexFunctionAdd = $IndexingThisIndex->add_function($function_index, $definition);
print_r($IndexFunctionAdd);
*/

/*
//Here is an example of adding a function to an IndexName that sets the function index to 1, and sorts by event date which is stored as a timestamp as document variable 2
//in reverse order
//$IndexFunctionDelete = $IndexingThisIndex->delete_function('1');
$function_index = '1';
$definition = "if(doc.var[2] > q[2], -doc.var[2], age)";
$IndexFunctionAdd = $IndexingThisIndex->add_function($function_index, $definition);
print_r($IndexFunctionAdd);
*/

/*
//Here is an example of adding a function to an IndexName that sets the function index to 0, thus the default function to use and defines distance based off of the field 'mylocation' and two query points to be provided in the search query
$function_index = '1';
$definition = "distance(mylocation, geopoint(q[0], q[1]))";
$IndexFunctionAdd = $IndexingThisIndex->add_function($function_index, $definition);
print_r($IndexFunctionAdd);
*/

/*
//Here is an example of adding a function to an IndexName that sets the function index to 1, thus the default function to use and defines distance in miles (could be km) based off of the variables 1 and 2 for lat and long
$function_index = '2';
$definition = "miles(q[0], q[1], d[0], d[1])"; //q is query variable and d is document variable
$IndexFunctionAdd = $IndexingThisIndex->add_function($function_index, $definition);
print_r($IndexFunctionAdd);
*/

/*
//Here is a an example with a distance function but withing a parameter measured in meters for q[4]
$function_index = '1';
$definition = "distance(mylocation, geopoint(q[0], q[1]))<q[2]";
$IndexFunctionAdd = $IndexingThisIndex->add_function($function_index, $definition);
print_r($IndexFunctionAdd);
*/

/*
//Here is an example of deleting a function with the function index of 0
$IndexFunctionDelete = $IndexingThisIndex->delete_function('0');
print_r($IndexFunctionDelete);
$IndexFunctionDelete = $IndexingThisIndex->delete_function('1');
print_r($IndexFunctionDelete);
*/

//echo "done";
?>