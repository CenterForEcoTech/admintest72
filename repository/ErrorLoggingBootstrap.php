<?php
include_once __DIR__ . '/Psr/Log/LoggerInterface.php';
include_once __DIR__ . '/Monolog/Logger.php';

include_once __DIR__ . '/Monolog/Handler/HandlerInterface.php';
include_once __DIR__ . '/Monolog/Handler/AbstractHandler.php';
include_once __DIR__ . '/Monolog/Handler/AbstractProcessingHandler.php';

include_once __DIR__ . '/Monolog/Formatter/FormatterInterface.php';
include_once __DIR__ . '/Monolog/Formatter/NormalizerFormatter.php';
include_once __DIR__ . '/Monolog/Formatter/LineFormatter.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

include_once __DIR__ . '/mySqlProvider/ErrorLoggingProvider.php';


// create a log channel
$siteLogger = new Logger('name');
$siteLogger->pushHandler(new ErrorLoggingProvider($mysqli, Logger::WARNING));

function errorHandler($errorLevel , $errstr, $errfile, $errline, $errcontext)
{
    global $siteLogger;
    // Perform your error handling here, respecting error_reporting() and
    // $errno.  This is where you can log the errors.  The choice of logger
    // that you use is based on your preference.  So long as it implements
    // the observer pattern you will be able to easily add logging for any
    // type of output you desire.

    $coreContext = ErrorContext::extract($errcontext);

    $coreContext["line"] = $errline;

    switch ($errorLevel){
        case E_USER_ERROR:
            $siteLogger->addError($errstr, $coreContext);
            break;
        case E_ERROR:
            $siteLogger->addError($errstr, $coreContext);
            break;
        case E_PARSE:
            $siteLogger->addWarning($errstr, $coreContext);
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
            $siteLogger->addError($errstr, $coreContext);
            break;
        case E_USER_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
            $siteLogger->addWarning($errstr, $coreContext);
            break;
    }
    //print_r($loggedError);
}

$previousErrorHandler = set_error_handler('errorHandler');

function exceptionHandler(Exception $exception){
    // not used (yet) and is generally not a good idea. But we can use it if we have lots of exceptions not being handled
}

function translateErrorType($type)
{
    $return ="";
    if($type & E_ERROR) // 1 //
        $return.='& E_ERROR ';
    if($type & E_WARNING) // 2 //
        $return.='& E_WARNING ';
    if($type & E_PARSE) // 4 //
        $return.='& E_PARSE ';
    if($type & E_NOTICE) // 8 //
        $return.='& E_NOTICE ';
    if($type & E_CORE_ERROR) // 16 //
        $return.='& E_CORE_ERROR ';
    if($type & E_CORE_WARNING) // 32 //
        $return.='& E_CORE_WARNING ';
    if($type & E_COMPILE_ERROR) // 64 //
        $return.='& E_COMPILE_ERROR ';
    if($type & E_COMPILE_WARNING) // 128 //
        $return.='& E_COMPILE_WARNING ';
    if($type & E_USER_ERROR) // 256 //
        $return.='& E_USER_ERROR ';
    if($type & E_USER_WARNING) // 512 //
        $return.='& E_USER_WARNING ';
    if($type & E_USER_NOTICE) // 1024 //
        $return.='& E_USER_NOTICE ';
    if($type & E_STRICT) // 2048 //
        $return.='& E_STRICT ';
    if($type & E_RECOVERABLE_ERROR) // 4096 //
        $return.='& E_RECOVERABLE_ERROR ';
    if($type & E_DEPRECATED) // 8192 //
        $return.='& E_DEPRECATED ';
    if($type & E_USER_DEPRECATED) // 16384 //
        $return.='& E_USER_DEPRECATED ';
    return substr($return,2);
}

function shutdownFunction()
{
    $err = error_get_last();

    if (!isset($err))
    {
        return;
    }

    $handledErrorTypes = array(
        E_USER_ERROR      => 'USER_ERROR',
        E_USER_WARNING    => 'USER_WARNING',
        E_ERROR           => 'ERROR',
        E_PARSE           => 'PARSE',
        E_CORE_ERROR      => 'CORE_ERROR',
        E_CORE_WARNING    => 'CORE_WARNING',
        E_COMPILE_ERROR   => 'COMPILE_ERROR',
        E_COMPILE_WARNING => 'COMPILE_WARNING');

    // If our last error wasn't fatal then this must be a normal shutdown.
    if (!isset($handledErrorTypes[$err['type']]))
    {
        return;
    }

    if (!headers_sent())
    {
        header('HTTP/1.1 500 Internal Server Error');
    }

    errorHandler($err['type'], $err['message'], $err['file'], $err['line'], $GLOBALS);
}

register_shutdown_function('shutdownFunction');