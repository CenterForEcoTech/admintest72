<?php
include_once("_getRootFolder.php");
include_once($rootFolder."/PHPMailer/class.phpmailer.php");

interface EmailLogger{
    public function log(EmailLoggerMessage $message, EmailResponse $response = null, $isDevMode = false);
    public function logDeferred(EmailLoggerMessage $message);
    public function logDeferredSent(EmailLoggerMessage $message, $historyId);
}

interface EmailLoggerMessage{
    public function getMemberId();
    public function getFromText();
    public function getToText();
    public function getSubject();
    public function getMessage();
    public function getDeferredEmailLogId();
}

class EmailProvider {
    private $host;
    private $port;
    private $user;
    private $pass;
    private $enableDebug = 0;
    private $enableAuth = true;
    private $securePrefix = "ssl";
    private $throwsExceptions = true;
    /* @var $logger EmailLogger */
    private $logger;

    public function __construct(EmailLogger $emailLogger, $host, $port, $user, $pass, $throwsExceptions = true){
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->throwsExceptions = $throwsExceptions;
        $this->logger = $emailLogger;
    }

    public function setDebug($debug = 0){
        $this->enableDebug = $debug;
    }

    public function setAuth($auth = true){
        $this->enableAuth = $auth;
    }

    public function setSecurePrefix($prefix = "ssl"){
        $this->securePrefix = $prefix;
    }

    public function smtpSend(EmailMessage $message, $isDevMode = false){
        $response = new EmailResponse($message);
        $mailer = new PHPMailer($this->throwsExceptions);
        $mailer->IsSMTP();

        // log the attempt
        $loggerResponse = $this->logger->log($message);
        $response->logId = $loggerResponse->insertedId;

        try {
            $mailer->SMTPDebug = $this->enableDebug;
            $mailer->SMTPAuth = $this->enableAuth;
            $mailer->SMTPSecure = $this->securePrefix;
            $mailer->Host = $this->host;
            $mailer->Port = $this->port;
            $mailer->Username = $this->user;
            $mailer->Password = $this->pass;

            // set recipients
            foreach($message->ToEmailAddressArray as $address){
                $mailer->AddAddress($address->address, $address->name);
            }
            // set from
            $mailer->SetFrom($message->FromEmailAddress->address, $message->FromEmailAddress->name, $message->ReplyToEmailAddress->address == $message->FromEmailAddress->address);
            // set reply to
            $mailer->AddReplyTo($message->ReplyToEmailAddress->address, $message->ReplyToEmailAddress->name);
            // add CC
            foreach($message->ccEmailsArray as $address){
                $mailer->AddCC($address->address, $address->name);
            }
            // add BCC
            foreach($message->bccEmailsArray as $address){
                $mailer->AddBCC($address->address, $address->name);
            }

            // add subject
            $mailer->Subject = $message->Subject;

            // add body text
            if ($message->IsHtml){
                $mailer->MsgHTML($message->Body);
            } else {
                $mailer->ContentType = 'text/plain';
                $mailer->IsHTML(false);
                $mailer->Body = $message->Body;
            }

            if (!$isDevMode){
                $response->emailSent = $mailer->Send();
            }

        } catch (phpmailerException $e){
            $response->setClientError($e->errorMessage());
        } catch (Exception $e){
            $response->setServerError($e->getMessage());
        }

        // log
        $loggerResponse = $this->logger->log($message, $response, $isDevMode);
        $response->logMessage = $loggerResponse->error;
        if ($response->emailSent|| $isDevMode){
            // log success
            $this->logger->logDeferredSent($message, $loggerResponse->insertedId);
        }
        return $response;
    }
}

class EmailAddress {
    public $address;
    public $name = ''; // default

    public function __construct($address, $name){
        $this->address = $address;
        $this->name = $name;
    }

    public function toString(){
        return trim($this->name. " <".$this->address.">");
    }

    public static function convert($deserializedObject){
        return new EmailAddress($deserializedObject->address, $deserializedObject->name);
    }
}

class EmailMessage implements EmailLoggerMessage {
    public $DeferredLogId;
    public $MemberID = 0;
    public $FromEmailAddress;
    public $ReplyToEmailAddress; // usually the same as replyto
    public $ToEmailAddressArray = array();
    public $ccEmailsArray = array();
    public $bccEmailsArray = array();
    public $Subject = '';
    public $Body = '';
    public $IsHtml = false;

    public function setFrom($address, $name){
        $this->FromEmailAddress = new EmailAddress($address, $name);
        if (!isset($this->ReplyToEmailAddress)){
            $this->setReplyTo($address, $name);
        }
    }

    public function setReplyTo($address, $name){
        $this->ReplyToEmailAddress = new EmailAddress($address, $name);
    }

    public function addAddress($toEmail, $toName = ''){
        $this->ToEmailAddressArray[] = new EmailAddress($toEmail, $toName);
    }

    public function addCC($ccEmail, $ccName = ''){
        $this->ccEmailsArray[] = new EmailAddress($ccEmail, $ccName);
    }

    public function addBCC($bccEmail, $bccName = ''){
        $this->bccEmailsArray[] = new EmailAddress($bccEmail, $bccName);
    }

    public function setPlainTextBody($bodyText){
        $this->IsHtml = false;
        $this->Body = $bodyText;
    }

    public function setHtmlBody($bodyText){
        $this->IsHtml = true;
        $this->Body = $bodyText;
    }

    //
    // EmailLoggerMessage implementation
    //

    public function getMemberId(){
        return $this->MemberID;
    }

    public function getFromText(){
        if (is_object($this->FromEmailAddress) && get_class($this->FromEmailAddress) === 'EmailAddress'){
            $this->FromEmailAddress->toString();
        }
    }

    public function getToText(){
        return $this->flatten($this->ToEmailAddressArray, "TO:").
                $this->flatten($this->ccEmailsArray, "CC:").
                $this->flatten($this->bccEmailsArray, "BCC:");
    }

    private function flatten($arrayOfEmails, $prefix){
        $resultString = "";
        foreach($arrayOfEmails as $emailAddress){
            if (strlen($resultString) == 0){
                $resultString = $prefix;
            } else {
                $resultString .= ",";
            }
            $resultString .= $emailAddress->toString();
        }
        return $resultString;
    }

    public function getSubject(){
        return $this->Subject;
    }

    public function getMessage(){
        return $this->Body;
    }

    public function getDeferredEmailLogId(){
        return $this->DeferredLogId;
    }

    /**
     * Utility function to convert stdClass object to EmailMessage
     * @static
     * @param $deserializedObject
     * @return EmailMessage
     */
    public static function convert($deserializedObject){
        $emailMessage = new EmailMessage();
        // scalar values
        if ($deserializedObject->DeferredLogId){
            $emailMessage->DeferredLogId = $deserializedObject->DeferredLogId;
        }
        if ($deserializedObject->MemberID){
            $emailMessage->MemberID = $deserializedObject->MemberID;
        }
        if ($deserializedObject->Subject){
            $emailMessage->Subject = $deserializedObject->Subject;
        }
        if ($deserializedObject->Body){
            $emailMessage->Body = $deserializedObject->Body;
        }
        if ($deserializedObject->IsHtml){
            $emailMessage->IsHtml = $deserializedObject->IsHtml;
        }
        // simple email
        if ($deserializedObject->FromEmailAddress){
            $emailMessage->FromEmailAddress = EmailAddress::convert($deserializedObject->FromEmailAddress);
        }
        if ($deserializedObject->ReplyToEmailAddress){
            $emailMessage->ReplyToEmailAddress = EmailAddress::convert($deserializedObject->ReplyToEmailAddress);
        }
        // array of email
        if (count($deserializedObject->ToEmailAddressArray)){
            foreach ($deserializedObject->ToEmailAddressArray as $deserializedAddress){
                $emailMessage->ToEmailAddressArray[] = EmailAddress::convert($deserializedAddress);
            }
        }
        if (count($deserializedObject->ccEmailsArray)){
            foreach ($deserializedObject->ccEmailsArray as $deserializedAddress){
                $emailMessage->ccEmailsArray[] = EmailAddress::convert($deserializedAddress);
            }
        }
        if (count($deserializedObject->bccEmailsArray)){
            foreach ($deserializedObject->bccEmailsArray as $deserializedAddress){
                $emailMessage->bccEmailsArray[] = EmailAddress::convert($deserializedAddress);
            }
        }
        return $emailMessage;
    }
}

class EmailResponse{
    public $emailSent = false;
    public $emailMessage;
    public $message = "";
    public $logId;
    public $logMessage;
    private $errorCode;
    private static $SERVER_ERROR = 500;
    private static $REQUEST_ERROR = 300;

    public function __construct($emailMessage){
        $this->emailMessage = $emailMessage;
    }

    public function setServerError($message){
        $this->errorCode = self::$SERVER_ERROR;
        $this->message = $message;
    }

    public function setClientError($message){
        $this->errorCode = self::$REQUEST_ERROR;
        $this->message = $message;
    }

    public function isServerError(){
        return $this->errorCode == self::$SERVER_ERROR;
    }

    public function isClientError(){
        return $this->errorCode == self::$REQUEST_ERROR;
    }

    public function getErrorForLog(){
        if ($this->errorCode){
            return $this->errorCode.": ".$this->message;
        }
        return "";
    }
}
?>