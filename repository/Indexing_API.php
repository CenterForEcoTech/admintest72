<?php
$IndexingProviderURL = $Config_IndexingURLPrivate;
$IndexName = $Config_IndexingEventsIndex;


class Indexing_Api
{
    /*
     * Basic client for a HoundSleuth account.
     * It has methods to manage and access the indexes of the account.
     */

    private $api_url = NULL;

    function __construct($api_url)
    {
        $this->api_url = rtrim($api_url, "/");
    }

	//not called directly
    public function get_index($index_name)
    {
        return new Indexing_Index($this, $index_name);
    }

	//can be called directly and produces output
    public function list_indexes()
    {
        try {
            return json_decode($this->api_call('GET', $this->indexes_url())->response);
        } catch (Indexing_Exception_HttpException $e) {
        }
    }

	//can be called directly but does not produce output
    public function create_index($index_name, $options = array())
    {
        $index = $this->get_index($index_name);
        $index->create_index($options);
        return $index;
    }

	//not called directly
    public function indexes_url()
    {
        return $this->api_url . '/v1/indexes';
    }

	//not called directly
    public function index_url($index_name)
    {
        return $this->indexes_url() . "/" . urlencode($index_name);
    }

    /**
     * Make a call to the api
     * @throws Indexing_Exception_HttpException
     * @param string $method HTTP method
     * @param string $url URL to call
     * @param array $params query parameters or body
     * @param array $http_options curlopt options
     * @return Indexing_Response
     */
	 //not called directly
    public function api_call($method, $url, $params = array(), $http_options = array())
    {
        if ($method == "GET" || $method == "DELETE") {
            $args = http_build_query($params);
            
            // remove the php special encoding of parameters
            // see http://www.php.net/manual/en/function.http-build-query.php#78603
            $args = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $args);
            
			if ($GLOBALS['Config_IndexingService']=="HoundSleuth"){
				$url .= '?' . $args;
			}else{
				$url .= '?' . preg_replace('/&amp;/','&',urldecode($args));
			}
            $args = '';
        } else {
            $args = json_encode($params);
        }
		//echo $url."\r\n<Br>";
		
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_CUSTOMREQUEST, $method); // Tell curl to use HTTP method of choice
        curl_setopt($session, CURLOPT_POSTFIELDS, $args); // Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_HEADER, false); // Tell curl not to return headers
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true); // Tell curl to return the response
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Expect:')); //Fixes the HTTP/1.1 417 Expectation Failed

        foreach ($http_options as $curlopt => $value) {
            curl_setopt($session, $curlopt, $value);
        }

        $response = curl_exec($session);
		//print_r($url); 
        $http_code = curl_getinfo($session, CURLINFO_HTTP_CODE);
        curl_close($session);

        // TODO: reinstate this exception and stop returning 200 status for every call, even error calls
		/*
        if (floor($http_code / 100) == 2) {
            return new Indexing_Response($http_code, $response);
        }
        throw new Indexing_Exception_HttpException($http_code,$response);
		*/
		
		try
		{
			if (floor($http_code / 100) == 2) {
				return new Indexing_Response($http_code, $response);
			}
			throw new Indexing_Exception_HttpException($response);
		}
		catch (Indexing_Exception_HttpException $e)
		{
			$error = array('error' => $e->error_message($response));
			$response = json_encode($error);
			return new Indexing_Response(200, $response);
		}		
	}
}

//not called directly...called via api_call
class Indexing_Response
{
    public $status = NULL;
    public $response = NULL;

    function __construct($status, $response)
    {
        $this->status = $status;
        $this->response = $response;
    }
}

/**
 * Converts an array of 2 elements (which can be NULL) to a colon-separated string containing those elements.
 * NULL elements are replaced by '*'
 *
 * Examples:
 *     HoundSleuth_map_range(array(2,4)) => "2:4";
 *     HoundSleuth_map_range(array(NULL,3)) => "*:3";
 *     HoundSleuth_map_range(array(5,NULL)) => "5:*";
 *     HoundSleuth_map_range(array(NULL,NULL)) => "*:*";
 *
 *
 * @param val: an array of 2 elements.
 * @return string
 */
function HoundSleuth_map_range($val) {
    return sprintf("%s:%s", ($val[0] === NULL ? "*" : $val[0]), ($val[1] === NULL ? "*" : $val[1]));
}

class IndexingSearchCriteria{
    public $queryString;
    public $start;
    public $len;
    public $scoring_function;
    public $snippet_fields;
    public $fetch_fields;
    public $category_filters;
    public $query_variables;
    public $docvar_filters;
    public $function_filters;
}

class Indexing_Index {

    /*
    * Client for a specific index.
    * It allows to inspect the status of the index.
    * It also provides methods for indexing and searching said index.
    */

    private $api = NULL;
    private $index_url = NULL;
    private $metadata = NULL;


    /**
     * You should not call this directly, but though Api#get_index
     * @param \Indexing_Api $api
     * @param  $index_name
     * @param null $metadata
     */
    public function __construct(Indexing_Api $api, $index_name, $metadata = NULL) {
        $this->api = $api;
        $this->index_url = $api->index_url(str_replace('/', '', $index_name));
        $this->metadata = $metadata;
    }

	//can be called directly returns 1=true
    public function exists() {
        /*
         * Returns whether an index for the name of this instance
         * exists, if it doesn't it can be created by calling
         * create_index()
         */
		    if ($this->refresh_metadata()){         
				return true;
			}
		/*
        try {
            $this->refresh_metadata();
            return true;
        } catch (Indexing_Exception_HttpException $e) {
            if ($e->getCode() == 404) {
                return false;
            } else {
                throw $e;
            }
        }
		*/
    }

	//can be called directly returns 1=true, else return response message
    public function has_started() {
        /*
         * Returns whether this index is responsive. Newly created
         * indexes can take a little while to get started.
         * If this method returns False most methods in this class
         * will raise an Indexing_Exception_HttpException with a status of 503.
         */
            $this->refresh_metadata();
			return $this->metadata->{'started'};	
    }

	//can be called directly returns status such as 'LIVE'
    public function get_status() {
        $this->refresh_metadata();
        return $this->metadata->{'status'};
    }

	//can be called directly returns string of Index code
    public function get_code() {
        $this->refresh_metadata();
        return $this->metadata->{'code'};
    }

	//can be called directly returns integer of number of documents
    public function get_size() {
        $this->refresh_metadata();
        return $this->metadata->{'size'};
    }
	
	//can be called directly returns creation time as YYYY-MM-DDTHH:MM:SS
    public function get_creation_time() {
        $this->refresh_metadata();
        return $this->metadata->{'creation_time'};
    }

	//can be called directly return 1=true
    public function is_public_search_enabled() {
        $this->refresh_metadata();
        return $this->metadata->{'public_search'};
    }

	//not called directly
    public function create_index( $options = array()) {
        /*
         * Creates this index.
         * If it already existed a IndexAlreadyExists exception is raised.
         * If the account has reached the limit a TooManyIndexes exception is raised
         */

           if (!$this->exists() ) {
               try {
                   $res = $this->api->api_call('PUT', $this->index_url, $options);
               } catch (Indexing_Exception_HttpException $e) {
               }
               return $res->status;
            }
 		 
			return false;
		/* 
        try {
            if ( $this->exists() ) { 
                throw new Indexing_Exception_IndexAlreadyExists();//'An index for the given name already exists');
            }
            $res = $this->api->api_call('PUT', $this->index_url, $options);
            return $res->status;
        } catch (Indexing_Exception_HttpException $e) {
            if ($e->getCode() == 409) {
                throw new Indexing_Exception_TooManyIndexes($e->getMessage());
            }
            throw $e;
        }
		*/
    }

	//can be called directly returns http_code 200 if successful and 204 if IndexName already exists
    public function update_index($options) {
        /*
         * Update this index.
         * If it doesn't exist a IndexDoesNotExist exception is raised.
         */
        if ( ! $this->exists() ){
            throw new Indexing_Exception_IndexDoesNotExist('An index for the given name doesn\'t exist');
        }

        try {
            $res = $this->api->api_call('PUT', $this->index_url, $options);
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }


	//can be called directly
    public function delete_index() {
        try {
            $res = $this->api->api_call('DELETE', $this->index_url);
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }

	//can be called directly returns http code 200 if successfull
    public function add_document($docid, $fields, $variables = NULL, $categories = NULL) {
        /*
         * Indexes a document for the given docid and fields.
         * Arguments:
         *     docid: unique document identifier. A String no longer than 1024 bytes. Can not be NULL
         *     fields: map with the document fields. field names and values MUST be UTF-8 encoded.
         *     variables (optional): map integer -> float with values for variables that can
         *                           later be used in scoring functions during searches.
         */
        try {
            $res = $this->api->api_call('PUT', $this->docs_url(), $this->as_document($docid, $fields, $variables, $categories));
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }

	//can be called directly returns an array of ['added']=1 true or ['error'] if not added
    public function add_documents($documents = array()) {
        /*
         * Indexes an array of documents. Each element (document) on the array needs to be an
         * array, with 'docid', 'fields' and optionally 'variables' keys and 'categories'.
         * Arguments:
         *     documents: an array of arrays, each representing a document
         */
        $data = array();
        foreach ($documents as $i => $doc_data) {
            try {
                // make sure docid is set
                if (!array_key_exists("docid", $doc_data)) {
                    throw new InvalidArgumentException("document $i lacks 'docid'");
                }
                $docid = $doc_data['docid'];

                // make sure fields is set
                if (!array_key_exists("fields", $doc_data)) {
                    throw new InvalidArgumentException("document $i lacks 'fields'");
                }
                $fields = $doc_data['fields'];

                // set $variables
                if (!array_key_exists("variables", $doc_data)) {
                    $variables = NULL;
                } else {
                    $variables = $doc_data['variables'];
                }

                // set $variables
                if (!array_key_exists("categories", $doc_data)) {
                    $categories = NULL;
                } else {
                    $categories = $doc_data['categories'];
                }

                $data[] = $this->as_document($docid, $fields, $variables, $categories);
            } catch (InvalidArgumentException $iae) {
                throw new InvalidArgumentException("while processing document $i: " . $iae->getMessage());
            }
        }

        try {
            $res = $this->api->api_call('PUT', $this->docs_url(), $data);
        } catch (Indexing_Exception_HttpException $e) {
        }
        return json_decode($res->response);
    }

	//can be called directly returns http code 200
    public function delete_document($docid) {
        /*
         * Deletes the given docid from the index if it existed. otherwise, does nothing.
         * Arguments:
         *     docid: unique document identifier
         */
        try {
            $res = $this->api->api_call('DELETE', $this->docs_url(), array("docid" => $docid));
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }

	//can be called directly, not sure if working properly
    public function delete_documents($docids) {
        /*
         * Deletes the given docids from the index if they existed. otherwise, does nothing.
         * Arguments:
         *     docids: unique document identifiers
         */
        $data = array("docid" => $docids);
        try {
            $res = $this->api->api_call('DELETE', $this->docs_url(), $data);
        } catch (Indexing_Exception_HttpException $e) {
        }
        return json_decode($res->response);
    }

    /*
     * Performs a delete on the results of a search.
     * See 'search' for parameter explanation.
     */
	 //can be called directly
    public function delete_by_search($query, $start=NULL, $scoring_function=NULL, $category_filters=NULL, $variables=NULL, $docvar_filters=NULL, $function_filters=NULL) {

        $params = $this->as_search_param( $query, $start, NULL, NULL, NULL, NULL, $category_filters, $variables, $docvar_filters, $function_filters);

        try {
            $res = $this->api->api_call('DELETE', $this->search_url(), $params);
        } catch (Indexing_Exception_HttpException $e) {
        }
        return json_decode($res->response);
    }

	//can be called directly returns http code 200 if successful, docid must be stored as string
    public function update_variables($docid, $variables) {
        /*
         * Updates the variables of the document for the given docid.
         * Arguments:
         *     docid: unique document identifier
         *     variables: map integer -> float with values for variables that can
         *                later be used in scoring functions during searches.
         */
        try {
            $res = $this->api->api_call('PUT', $this->variables_url(), array("docid" => $docid, "variables" => $this->convert_to_map($variables)));
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }
	
	//can be called directly returns http code 200 if successful, docid must be stored as string
    public function update_categories($docid, $categories) {
        /*
         * Updates the category values of the document for the given docid.
         * Arguments:
         *     docid: unique document identifier
         *     categories: map string -> string where each key is a category name pointing to its value
         */
		 $params = array("docid" => $docid, "categories" => $categories);
        try {
            $res = $this->api->api_call('PUT', $this->categories_url(), $params);
        } catch (Indexing_Exception_HttpException $e) {
        }
        print_r($params);
        return $res->status;
    }

	//can be called directly returns http code 200 if successful, docid must be stored as string
	//Not sure if this actually works
    public function promote($docid, $query) {
        /*
         * Makes the given docid the top result of the given query.
         * Arguments:
         *     docid: unique document identifier
         *     query: the query for which to promote the document
         */
        try {
            $res = $this->api->api_call('PUT', $this->promote_url(), array("docid" => $docid, "query" => $query));
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }

	//can be called directly returns http code 200 if successful, function_index must be stored as string
    public function add_function($function_index, $definition) {
        try {
            $res = $this->api->api_call('PUT', $this->function_url($function_index), array("definition" => $definition));
            return $res->status;
        } catch (Indexing_Exception_HttpException $e) {
            if ($e->getCode() == 400) {
                throw new Indexing_Exception_InvalidDefinition($e->getMessage());
            }
            throw $e;
        }
    }

	//can be called directly returns http code 200 if successful, function_index must be stored as string
    public function delete_function($function_index) {
        try {
            $res = $this->api->api_call('DELETE', $this->function_url($function_index));
        } catch (Indexing_Exception_HttpException $e) {
        }
        return $res->status;
    }

	//can be called directly returns array of functions
    public function list_functions() {
        try {
            $res = $this->api->api_call('GET', $this->functions_url());
        } catch (Indexing_Exception_HttpException $e) {
        }
        return json_decode($res->response);
    }

    public function searchWithCriteria(IndexingSearchCriteria $criteria){
        try {
            return $this->search(
                $criteria->queryString,
                $criteria->start,
                $criteria->len,
                $criteria->scoring_function,
                $criteria->snippet_fields,
                $criteria->fetch_fields,
                $criteria->category_filters,
                $criteria->query_variables,
                $criteria->docvar_filters,
                $criteria->function_filters
            );
        } catch (Indexing_Exception_HttpException $e) {
        } catch (Indexing_Exception_InvalidQuery $e) {
        }
    }


    /*
     * Performs a search.
     *
     * @param variables: An array with 'query variables'. Example: array( 0 => 3, 1 => 34);
     * @param docvar_filters: An array with filters for document variables.
     *     Example: array(0 => array(array(1,4), array(6, 9), array(16,NULL)))
     *     Document variable 0 should be between 1 and 4 OR 6 and 9 OR greater than 16
     * @param function_filters: An array with filters for function scores.
     *     Example: array(2 => array(array(2,6), array(7, 11), array(15,NULL)))
     *     Scoring function 2 must return a value between 2 and 6 OR 7 and 11 OR greater than 15 for documents matching this query.
     *
     */
	 //can be called directly return an array if data
    public function search($query, $start = NULL, $len = NULL, $scoring_function = NULL, $snippet_fields = NULL, $fetch_fields = NULL, $category_filters = NULL, $variables = NULL, $docvar_filters = NULL, $function_filters = NULL) {

        $params = $this->as_search_param( $query, $start, $len, $scoring_function, $snippet_fields, $fetch_fields, $category_filters, $variables, $docvar_filters, $function_filters);

        try {
            $res = $this->api->api_call('GET', $this->search_url(), $params);
            return json_decode($res->response);
        } catch (Indexing_Exception_HttpException $e) {
            if ($e->getCode() == 400) {
                throw new Indexing_Exception_InvalidQuery($e->getMessage());
            }
            throw $e;
        }
    }


	//not called directly
    // Creates a 'document'
    private function as_document($docid, $fields, $variables = NULL, $categories = NULL) {
        if (NULL == $docid) throw new InvalidArgumentException("\$docid can't be NULL");
        if (mb_strlen($docid, '8bit') > 1024) throw new InvalidArgumentException("\$docid can't be longer than 1024 bytes");
        $data = array("docid" => $docid, "fields" => $fields);
        if ($variables != NULL) {
            $data["variables"] = $this->convert_to_map($variables);
        }

        if ($categories != NULL) {
            $data["categories"] = $categories;
        }
        return $data;
    }

	//not called directly
    private function as_search_param( $query, $start = NULL, $len = NULL, $scoring_function = NULL, $snippet_fields = NULL, $fetch_fields = NULL, $category_filters = NULL, $variables = NULL, $docvar_filters = NULL, $function_filters = NULL) {

        $params = array("q" => $query);
        if ($start != NULL) {
            $params["start"] = $start;
        }
        if ($len != NULL) {
            $params["len"] = $len;
        }
        if ($scoring_function != NULL) {
            $params["function"] = (string)$scoring_function;
        }
        if ($snippet_fields != NULL) {
            $params["snippet"] = $snippet_fields;
        }
        if ($fetch_fields != NULL) {
            $params["fetch"] = $fetch_fields;
        }
        if ($category_filters != NULL) {
            $params["category_filters"] = json_encode($category_filters);
        }
        if ($variables) {
            foreach ($variables as $k => $v)
            {
                $params["var" . strval($k)] = $v;
            }
        }

        if ($docvar_filters) {
            // $docvar_filters is something like
            // { 3 => [ (1, 3), (5, NULL) ]} to filter_docvar3 => 1:3,5:*
            foreach ($docvar_filters as $k => $v) {
                $params["filter_docvar" . strval($k)] = implode(array_map('HoundSleuth_map_range', $v), ",");
            }
        }

        if ($function_filters) {
            // $function_filters is something like
            // { 2 => [ (1, 4), (7, NULL) ]} to filter_function2 => 1:4,7:*
            foreach ($function_filters as $k => $v) {
                $params["filter_function" . strval($k)] = implode(array_map('HoundSleuth_map_range', $v), ",");
            }
        }
       

        return $params;
    }

	//not called directly
    private function get_metadata() {
        if ($this->metadata == NULL) {
            return $this->refresh_metadata();
        }
        return $this->metadata;
    }

	//not called directly
    private function refresh_metadata() {
        try {
            $res = $this->api->api_call('GET', $this->index_url, array());
        } catch (Indexing_Exception_HttpException $e) {
        }
        $this->metadata = json_decode($res->response);
        return $this->metadata;
    }

	//not called directly
    private function docs_url() {
        return $this->index_url . "/docs";
    }

	//not called directly
    private function variables_url() {
        return $this->index_url . "/docs/variables";
    }

	//not called directly
    private function categories_url() {
        return $this->index_url . "/docs/categories";
    }

	//not called directly
    private function promote_url() {
        return $this->index_url . "/promote";
    }

	//not called directly
    private function search_url() {
        return $this->index_url . "/search";
    }

	//not called directly
    private function functions_url() {
        return $this->index_url . "/functions";
    }

	//not called directly
    private function function_url($n) {
        return $this->index_url . "/functions/" . $n;
    }

	//not called directly
    private function convert_to_map($array_object) {
        $result = new stdClass();

        for ($i = 0; $i < sizeof($array_object); ++$i) {
            $result->{$i} = $array_object[$i];
        }

        return $result;
    }

}

class Indexing_Exception extends Exception{ }

class Indexing_Exception_HttpException extends Indexing_Exception{
	public function error_message($response){
		return $response;
	}
 }

class Indexing_Exception_IndexAlreadyExists extends Indexing_Exception{
	public function error_message($response){
		//return $response;
	}


 }

class Indexing_Exception_IndexDoesNotExist extends Indexing_Exception{ }

class Indexing_Exception_InvalidDefinition extends Indexing_Exception{ }

class Indexing_Exception_InvalidQuery extends Indexing_Exception{ }

class Indexing_Exception_InvalidResponseFromServer extends Indexing_Exception{ }

class Indexing_Exception_InvalidUrl extends Indexing_Exception{ }

class Indexing_Exception_TooManyIndexes extends Indexing_Exception{ }

class Indexing_Exception_Unauthorized extends Indexing_Exception{ }


?>