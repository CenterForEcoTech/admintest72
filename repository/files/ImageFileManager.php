<?php
/*
 * ImageFileManager's sole purpose is to manage the files being uploaded to a specific directory and subfolders,
 * as well as deleting them. It should be server agnostic, session agnostic, user agnostic.
 * Permissions should be set outside this manager. Whatever calls this manager should have permissions to call it.
 *
 * Most of this file is generic, but the JqueryFileUploadConverter is specific to the jQuery-File-Upload plugin.
 */

/**
 * PendingUploadFile holds metadata about a file that is being uploaded. It is generic.
 */
class PendingUploadFile {

    // PHP File Upload error message codes:
    // http://php.net/manual/en/features.file-upload.errors.php
    private static $ERROR_MESSAGES = array(
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini',
        'max_file_size' => 'File is too big',
        'min_file_size' => 'File is too small',
        'accept_file_types' => 'Filetype not allowed',
        'max_number_of_files' => 'Maximum number of files exceeded',
        'max_width' => 'Image exceeds maximum width',
        'min_width' => 'Image requires a minimum width',
        'max_height' => 'Image exceeds maximum height',
        'min_height' => 'Image requires a minimum height',
        'not_allowed' => 'Upload not permitted.'
    );

    // directly from upload
    public $tmpName;
    public $fileName;
    public $size;
    public $type;
    public $width = 0;
    public $height = 0;
    public $error;
    public $contentRange;
    public $origFileName;
    public $serverContentLength;

    // derived/calculated from data
    public $errorMessage;

    public function __construct($tmpName, $fileName, $size, $type, $error, $contentRange, $origFileName, $serverContentLength){
        $this->tmpName = $tmpName;
        $this->fileName = $fileName;
        $this->size = $size;
        $this->type = $type;
        $this->error = $error;
        $this->contentRange = $contentRange;
        $this->origFileName = $origFileName;
        $this->serverContentLength = $serverContentLength;
        $this->setImageDimensions();
        $this->resetErrorMessage();
    }

    private function setImageDimensions(){
        $uploaded_file = $this->tmpName;
        list($img_width, $img_height) = @getimagesize($uploaded_file);
        if (is_int($img_width)) {
            $this->width = $img_width;
            $this->height = $img_height;
        }
    }

    public function resetErrorMessage(){
        $this->errorMessage = self::getErrorMessage($this->error);
    }

    public function isNew(){
        // subclasses should override if they allow overwriting existing image files
        return true;
    }

    public static function getErrorMessage($error) {
        return array_key_exists($error, self::$ERROR_MESSAGES) ?
            self::$ERROR_MESSAGES[$error] : $error;
    }

    /**
     * Converts any string into a valid file name with the same extension as the original file name.
     * @static
     * @param $text
     * @param $origFileName
     * @return string
     */
    public static function sanitizedFilenameFromText($text, $origFileName){
        $quickSanitizedTitle = preg_replace("/[^a-z0-9-_]/", "-", strtolower($text));
        if (strlen($quickSanitizedTitle)){
            $ext = strtolower(pathinfo($origFileName, PATHINFO_EXTENSION));
            return $quickSanitizedTitle.".".$ext;
        }
        return $origFileName;
    }
}

/**
 * MediaLibraryFile is a subclass of PendingUploadFile that expects an uploaded file along with form values to be stored with
 * it.
 */
class MediaLibraryFile extends PendingUploadFile{

    // form data optionally associated
    public $formValuesArray = array();

    /**
     * Returns true if this represents a file that does not have a matching id in our metadata system.
     * @return bool
     */
    public function isNew(){
        if (count($this->formValuesArray) && isset($this->formValuesArray['id'])){
            $id = $this->formValuesArray['id'];
            if (is_numeric($id) && $id > 0){
                return false;
            }
        }
        return true;
    }
}

/**
 * Specific to the jQuery File Upload plugin, functionality is separated here so that another plugin may be substituted easily.
 */
class JqueryFileUploadConverter{

    /**
     * This helper method converts uploaded files into a collection of MediaLibraryFile to be processed. Its sole business
     * is to return MediaLibraryFile objects.
     * @static
     * @param $SERVER array variable
     * @param $FILES array variable
     * @param $POST array variable
     * @param string $filesParamName
     * @param null $fileNameFormField
     * @return array of MediaLibraryFile
     */
    public static function convertToMediaLibraryFile($SERVER, $FILES, $POST, $filesParamName = "files", $fileNameFormField = null){
        $upload = isset($FILES[$filesParamName]) ? $FILES[$filesParamName] : null;
        // Parse the Content-Disposition header, if available:
        $file_name = isset($SERVER['HTTP_CONTENT_DISPOSITION']) ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/',
                '',
                $SERVER['HTTP_CONTENT_DISPOSITION']
            )) : null;
        $file_type = isset($SERVER['HTTP_CONTENT_DESCRIPTION']) ?
            $SERVER['HTTP_CONTENT_DESCRIPTION'] : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range = isset($SERVER['HTTP_CONTENT_RANGE']) ?
            preg_split('/[^0-9]+/', $SERVER['HTTP_CONTENT_RANGE']) : null;
        $size =  $content_range ? $content_range[3] : null;

        // now build the return array.
        $info = array();
        if ($upload) {
            if (is_array($upload['tmp_name'])){
                // param_name is an array identifier like "files[]",
                // $FILES is a multi-dimensional array:
                foreach ($upload['tmp_name'] as $index => $value) {
                    if (isset($POST[$fileNameFormField]) && isset($POST[$fileNameFormField][$index])){
                        $file_name = PendingUploadFile::sanitizedFilenameFromText($POST[$fileNameFormField][$index], $upload['name'][$index]);
                    }
                    $thisFileInfo =  new MediaLibraryFile(
                        $upload['tmp_name'][$index],
                        $file_name ? $file_name : $upload['name'][$index],
                        $size ? $size : $upload['size'][$index],
                        $file_type ? $file_type : $upload['type'][$index],
                        $upload['error'][$index],
                        $content_range,
                        $upload['name'][$index],
                        $SERVER['CONTENT_LENGTH']
                    );

                    $postValues = array();
                    // now, assuming $POST entries are all arrays...
                    foreach($POST as $name => $postValue){
                        if (is_array($postValue)){
                            $postValues[$name] = $postValue[$index];
                        }
                    }
                    $thisFileInfo->formValuesArray = $postValues;
                    $info[] = $thisFileInfo;
                }
            } else {
                $index = 0;
                // param_name is a single object identifier like "file",
                // $FILES is a one-dimensional array:
                if (isset($POST[$fileNameFormField]) && isset($POST[$fileNameFormField][$index])){
                    $file_name = PendingUploadFile::sanitizedFilenameFromText($POST[$fileNameFormField][$index], $upload['name'][$index]);
                }
                $thisFileInfo =  new MediaLibraryFile(
                    isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                    $file_name ? $file_name : (isset($upload['name']) ?
                        $upload['name'] : null),
                    $size ? $size : (isset($upload['size']) ?
                        $upload['size'] : $SERVER['CONTENT_LENGTH']),
                    $file_type ? $file_type : (isset($upload['type']) ?
                        $upload['type'] : $SERVER['CONTENT_TYPE']),
                    isset($upload['error']) ? $upload['error'] : null,
                    $content_range,
                    $upload['name'][$index],
                    $SERVER['CONTENT_LENGTH']
                );
                $postValues = array();
                // now, assuming $POST entries are all arrays...
                foreach($POST as $name => $value){
                    if (is_array($value)){
                        $postValues[$name] = $value[$index];
                    }
                }
                $thisFileInfo->formValuesArray = $postValues;
                $info[] = $thisFileInfo;
            }
        }
        return $info;
    }
}

/**
 * UploadFileManagerOptions represents the configuration of files to be uploaded to a specific subdirectory.
 */
class UploadFileManagerOptions{
    private static $ACCEPTED_MIME_TYPES = array(
        "application/pdf",
        "image/gif",
        "image/jpeg",
        "image/png"
    );
    public $directory;
    public $url;
    public $maxFileSize = null;
    public $minFileSize = 1;
    public $mkdirMode = 0755;

    /**
     * @param $urlRoot string e.g., "http://localhost/uploads"
     * @param $directoryPath string e.g., dirname($SERVER['SCRIPT_FILENAME'])
     */
    public function __construct($urlRoot, $directoryPath){
        $this->directory = $directoryPath;
        $this->url = $urlRoot;
    }

    /**
     * Helper function to get the url for the script file.
     * @static
     * @param $SERVER string the global param
     * @return string
     */
    public static function getUrlToScript($SERVER){
        $https = !empty($SERVER['HTTPS']) && $SERVER['HTTPS'] !== 'off';
        return
            ($https ? 'https://' : 'http://').
            (isset($SERVER['HTTP_HOST']) ? $SERVER['HTTP_HOST'] : ($SERVER['SERVER_NAME'].
                ($https && $SERVER['SERVER_PORT'] === 443 ||
                $SERVER['SERVER_PORT'] === 80 ? '' : ':'.$SERVER['SERVER_PORT']))).
            substr($SERVER['SCRIPT_NAME'],0, strrpos($SERVER['SCRIPT_NAME'], '/'));
    }

    /**
     * Constructs a url for the specified file name and version if any. This method does not make any assumptions about
     * the existence of the file.
     * @param $file_name string
     * @return string
     */
    public function getUrl($file_name = null){
        $root = rtrim($this->url, '/') . '/';
        return $root.rawurlencode($file_name);
    }

    /**
     * Constructs a path for the specified file and version if any. This method does not make any assumptions about
     * the existence of the file.
     * @param $file_name string
     * @return string
     */
    public function getPath($file_name = null) {
        $root = rtrim($this->directory, '/') . '/';
        return $root.$file_name;
    }

    /**
     * Returns true if the file exists in the main directory.
     * @param $file_name string
     * @return bool
     */
    public function isExistingFile($file_name) {
        $file_path = $this->getPath($file_name);
        if (is_file($file_path) && $file_name[0] !== '.') {
            return true;
        }
        return false;
    }

    private function upcount_name_callback($matches) {
        $index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return ' ('.$index.')'.$ext;
    }

    private function upcount_name($name) {
        return preg_replace_callback(
            '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }

    public function cleanFileName(PendingUploadFile $fileInfo) {
        $name = $fileInfo->fileName;
        $filenamePart = pathinfo($name, PATHINFO_FILENAME);
        $file_name = PendingUploadFile::sanitizedFilenameFromText($filenamePart, $name);
        $type = $fileInfo->type;
        $content_range = $fileInfo->contentRange;
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $file_name = trim(basename(stripslashes($file_name)), ".\x00..\x20");
        // Add missing file extension for known image types:
        if (strpos($file_name, '.') === false &&
            preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
            $file_name .= '.'.$matches[1];
        }
        while(is_dir($this->getPath($file_name))) {
            $file_name = $this->upcount_name($file_name);
        }
        // don't allow new files to overwrite existing files.
        if ($fileInfo->isNew()){
            $uploaded_bytes = self::fixIntegerOverflow(intval($content_range[1]));
            while(is_file($this->getPath($file_name))) {
                if ($uploaded_bytes === $this->getFilesize($file_name)) {
                    break;
                }
                $file_name = $this->upcount_name($file_name);
            }
        }
        $fileInfo->fileName = $file_name;
        return $fileInfo;
    }

    public function getFileSize($filename, $clear_stat_cache = false) {
        if ($clear_stat_cache) {
            clearstatcache();
        }
        $file_path = $this->getPath($filename);
        return self::fixIntegerOverflow(filesize($file_path));
    }

    /**
     * Returns basic data about a file on the disk and its versions if they exist in different sizes.
     * @param $file_name string
     * @return null|stdClass
     */
    public function getMetadata($file_name) {
        if ($this->isExistingFile($file_name)) {
            $file = new stdClass();
            $file->name = $file_name;
            $file->size = $this->getFileSize($file_name);
            $file->url = $this->getUrl($file->name);
            return $file;
        }
        return null;
    }

    // Fix for overflowing signed 32 bit integers,
    // works for sizes up to 2^32-1 bytes (4 GiB - 1):
    public static function fixIntegerOverflow($size) {
        if ($size < 0) {
            $size += 2.0 * (PHP_INT_MAX + 1);
        }
        return $size;
    }

    public function validate(PendingUploadFile $fileInfo) {
		/*
        if ($fileInfo->error ||
            !$this->directoryCanHoldMoreFiles() ||
            !self::validatePostMaxSize($fileInfo) ||
            !$this->validateFileType($fileInfo) ||
            !$this->validateFileSize($fileInfo)
        ) {
            $fileInfo->resetErrorMessage();
            return false;
        }
		*/
        return true;
    }

    private static function validatePostMaxSize(PendingUploadFile $fileInfo){
        $uploadedContentLength = self::fixIntegerOverflow(intval($fileInfo->serverContentLength));
        $serverMaxValue = trim(ini_get('post_max_size'));
        $scale = strtolower(substr($serverMaxValue, -1, 1)); // last char in string, which stands for GB, MB, KB
        switch($scale) {
            case 'g':
                $serverMaxValue *= 1024;
            case 'm':
                $serverMaxValue *= 1024;
            case 'k':
                $serverMaxValue *= 1024;
        }
        $maxLength = self::fixIntegerOverflow($serverMaxValue);
        if ($uploadedContentLength > $maxLength){
            $fileInfo->error = 'post_max_size';
            return false;
        }
        return true;
    }

    protected function validateFileType(PendingUploadFile $fileInfo){
		/*
        $uploaded_file = $fileInfo->tmpName;
        $fInfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $fInfo->file($uploaded_file);
        if (empty($mimeType)){
            trigger_error("finfo-file not working?", E_USER_ERROR);
        }
        if (!in_array($mimeType, self::$ACCEPTED_MIME_TYPES)){
            $fileInfo->error = 'accept_file_types';
            return false;
        }
		*/
        return true;
    }

    private function validateFileSize(PendingUploadFile $fileInfo){
        $uploaded_file = $fileInfo->tmpName;
        $file_size = $fileInfo->serverContentLength;
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = self::fixIntegerOverflow(filesize($uploaded_file));
        }
        if ($this->maxFileSize && (
                $file_size > $this->maxFileSize ||
                $fileInfo->size > $this->maxFileSize)
        ) {
            $fileInfo->error = 'max_file_size';
            return false;
        }
        if ($this->minFileSize &&
            $file_size < $this->minFileSize) {
            $fileInfo->error = 'min_file_size';
            return false;
        }
        return true;
    }

    private function directoryCanHoldMoreFiles(){
        // TODO: if we really want to constrain a directory to hold a maximum NUMBER of files (rather than maximum total SIZE of files, implement it here.
        return true;
    }
}

/**
 * ImageVersion represents the configuration of an image dimension and quality. These are used to indicate that an image ought
 * to be resized to the specifics in the version.
 */
class ImageVersion {
    public $folderName;
    public $maxWidth;
    public $maxHeight;
    public $jpgQuality;
    public $pngCompression;

    /**
     * @param $folderName
     * @param $maxWidth
     * @param $maxHeight
     * @param $jpgQuality Integer from 0 - 100, where 0 is terrible and 100 is best/biggest file.
     * @param $pngCompression Integer from 0 - 9, where 0 is no compression and 9 is maximum. 1 is fastest, 9 is slowest, 6 is default
     */
    public function __construct($folderName, $maxWidth, $maxHeight, $jpgQuality, $pngCompression = 6){
        $this->folderName = $folderName;
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
        $this->jpgQuality = $jpgQuality;
        $this->pngCompression = $pngCompression;
    }
}

/**
 * ImageFileManagerOptions represents the configuration of image files to be uploaded to a specific subdirectory.
 * This holds information about the directory structure, the kinds of image versions the upload should create (and look for),
 * and has functions to validate an image file that is being uploaded.
 */
class ImageFileManagerOptions extends UploadFileManagerOptions{
    private static $MAIN_FILE_FOLDER = "/files/";
    private static $THUMBNAIL_VERSION = 'thumbnail';
    private static $MEDIUM_VERSION = 'medium';
    public $imageVersions = array();
    public $maxImageHeight = null;
    public $maxImageWidth = null;
    public $minImageHeight = 1;
    public $minImageWidth = 1;
    public $orientFileOnUpload = false;

    /**
     * @param $urlRoot string e.g., "http://localhost/uploads"
     * @param $directoryPath string e.g., dirname($SERVER['SCRIPT_FILENAME'])
     * @param $enableThumbnail bool default = true
     * @param $enableMedium bool default = false
     */
    public function __construct($urlRoot, $directoryPath, $enableThumbnail = true, $enableMedium = false){
        $this->directory = $directoryPath.self::$MAIN_FILE_FOLDER;
        $this->url = $urlRoot.self::$MAIN_FILE_FOLDER;
        if ($enableThumbnail){
            $this->setThumbnail();
        }
        if ($enableMedium){
            $this->setMedium();
        }
    }

    private function addImageVersion($name, $maxWidth, $maxHeight, $jpgQuality = 75){
        $this->imageVersions[] = new ImageVersion($name, $maxWidth, $maxHeight, $jpgQuality);
    }

    /**
     * Enables thumbnails and sets max width and height
     * @param $maxWidth int default = 80
     * @param $maxHeight int default = 80
     */
    public function setThumbnail($maxWidth = 80, $maxHeight = 80){
        $this->addImageVersion(self::$THUMBNAIL_VERSION, $maxWidth, $maxHeight);
    }

    /**
     * Enables medium sized images
     * @param $maxWidth int default = 800
     * @param $maxHeight int default = 600
     * @param $jpgQuality int default = 80
     */
    public function setMedium($maxWidth = 800, $maxHeight = 600, $jpgQuality = 80){
        $this->addImageVersion(self::$MEDIUM_VERSION, $maxWidth, $maxHeight, $jpgQuality);
    }

    /**
     * Sets the overall maximum size of any image uploaded. If this is not set, the original image is uploaded
     * in its original size.
     * @param $maxWidth int
     * @param $maxHeight int
     * @param $jpgQuality int
     */
    public function setMaximums($maxWidth = 800, $maxHeight = 600, $jpgQuality = 95){
        if ($maxWidth <= 0 && $maxHeight <= 0){
            unset($this->imageVersions['']);
        } else {
            $this->addImageVersion('', $maxWidth, $maxHeight, $jpgQuality);
        }
    }

    /**
     * Returns basic data about a file on the disk and its versions if they exist in different sizes.
     * @param $file_name string
     * @return null|stdClass
     */
    public function getMetadata($file_name) {
        $file = parent::getMetadata($file_name);
        if ($file){
            foreach($this->imageVersions as $version) {
                if (!empty($version->folderName)) {
                    if (is_file($this->getPath($file_name, $version))) {
                        $file->{$version->folderName.'_url'} = $this->getUrl(
                            $file->name,
                            $version
                        );
                    }
                }
            }
        }
        return $file;
    }

    /**
     * Constructs a url for the specified file name and version if any. This method does not make any assumptions about
     * the existence of the file.
     * @param $file_name string
     * @param $version ImageVersion $version
     * @return string
     */
    public function getUrl($file_name = null, ImageVersion $version = null){
        $version_path = $version == null ? '' : rawurlencode($version->folderName).'/';
        return $this->url.$version_path.rawurlencode($file_name);
    }

    /**
     * Constructs a path for the specified file and version if any. This method does not make any assumptions about
     * the existence of the file.
     * @param $file_name string
     * @param $version ImageVersion $version
     * @return string
     */
    public function getPath($file_name = null, ImageVersion $version = null) {
        $file_name = $file_name ? $file_name : '';
        $version_path = $version == null ? '' : $version->folderName.'/';
        return $this->directory.$version_path.$file_name;
    }

    public function validate(PendingUploadFile $fileInfo) {
        $validFile = parent::validate($fileInfo);
        if ($validFile && !$this->validateImageSize($fileInfo)){
            $fileInfo->resetErrorMessage();
            $validFile = false;
        }
        return $validFile;
    }

    protected function validateFileType(PendingUploadFile $fileInfo){
        if (!parent::validateFileType($fileInfo)){
            return false;
        }
        // in spite of checking mime type, we are going to check that it's truly a real image
        $ext = strtolower(pathinfo($fileInfo->origFileName, PATHINFO_EXTENSION));
        $masterFilePath = $fileInfo->tmpName;
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($masterFilePath);
                break;
            case 'gif':
                $src_img = @imagecreatefromgif($masterFilePath);
                break;
            case 'png':
                $src_img = @imagecreatefrompng($masterFilePath);
                break;
            default:
                $src_img = null;
        }
        if (!$src_img){
            $fileInfo->error = 'accept_file_types';
            return false;
        }
        @imagedestroy($src_img);
        return true;
    }

    private function validateImageSize(PendingUploadFile $fileInfo){
        $img_width = $fileInfo->width;
        $img_height = $fileInfo->height;
        if (is_int($img_width)) {
            if ($this->maxImageWidth && $img_width > $this->maxImageWidth) {
                $fileInfo->error = 'max_width';
                return false;
            }
            if ($this->maxImageHeight && $img_height > $this->maxImageHeight) {
                $fileInfo->error = 'max_height';
                return false;
            }
            if ($this->minImageWidth && $img_width < $this->minImageWidth) {
                $fileInfo->error = 'min_width';
                return false;
            }
            if ($this->minImageHeight && $img_height < $this->minImageHeight) {
                $fileInfo->error = 'min_height';
                return false;
            }
        }
        return true;
    }
}

/**
 * UploadedFileMetadataHandler is called both before an attempted upload as well as after an attempted upload so that
 * the metadata may be properly processed based upon the status of the upload.
 */
interface UploadedFileMetadataHandler{
    function preUploadCallback(PendingUploadFile $fileInfo, UploadFileManagerOptions $options);
    function postUploadCallback(PendingUploadFile $fileInfo, UploadFileManagerOptions $options);
    function preDeleteCallback($filename, UploadFileManagerOptions $options);
    function postDeleteCallback($filename, UploadFileManagerOptions $options);
}

/**
 * This is more or less the controller that interfaces with the disk.
 * Notably, the GeneralFileManager does not permit partial file uploads.
 */
class GeneralFileManager {
    private $options;
    private $metadataHandler;

    public static function getInstance(UploadFileManagerOptions $options, UploadedFileMetadataHandler $handler = null){
        if (self::hasPerms($options->directory, $options)){
            return new GeneralFileManager($options, $handler);
        }
        return null;
    }

    public function acceptsPartialUploads(){
        return false; // image manager never accepts partial uploads
    }

    /**
     * Returns basic data about a file on the disk and its versions if they exist in different sizes.
     * @param $file_name string
     * @return null|stdClass
     */
    public function getMetadata($file_name) {
        return $this->options->getMetadata($file_name);
    }

    public function upload(PendingUploadFile $fileInfo){
        $file = new stdClass();
        $this->options->cleanFileName($fileInfo); // this step may change the filename based on its uniqueness on the file system
        $file->error = $fileInfo->error;
        $file->type = $fileInfo->type;

        if (!$this->options->validate($fileInfo)){
            $file->error = $fileInfo->errorMessage;
            trigger_error("GeneralFileManager.upload validation failed.", E_USER_ERROR);
            return $file;
        } else {
            $file->size = UploadFileManagerOptions::fixIntegerOverflow(intval($fileInfo->size));
            $file->height = $fileInfo->height;
            $file->width = $fileInfo->width;
            $ready = $this->prepareUploadPath();
            if ($ready){
                if ($this->metadataHandler){
                    // the handler's business logic may change the target filename yet again prior to the upload
                    $this->metadataHandler->preUploadCallback($fileInfo, $this->options);
                }

                $masterFilePath = $this->options->getPath($fileInfo->fileName);
                try{
                    move_uploaded_file($fileInfo->tmpName, $masterFilePath);
                }
                catch (Exception $e){
                    trigger_error("GeneralFileManager.upload Upload Failed:".$e->getMessage(), E_USER_ERROR);
                }
                if (!file_exists($masterFilePath)){
                    trigger_error("GeneralFileManager.upload Upload Failed.", E_USER_WARNING);
                    $fileInfo->error = 7;
                    $fileInfo->resetErrorMessage();
                    $file->error = $fileInfo->errorMessage;
                } else {

                    $this->doUploadProcess($fileInfo, $file);

                    if ($this->metadataHandler){
                        $this->metadataHandler->postUploadCallback($fileInfo, $this->options);
                    }
                }
            } else {
                $fileInfo->error = 'not_allowed';
                $fileInfo->resetErrorMessage();
                $file->error = $fileInfo->errorMessage;
            }
        }
        // get the final data and assign to metadata object
        $file->name = $fileInfo->fileName;
        $file->url = $this->options->getUrl($fileInfo->fileName);
        return $file;
    }

    public function deleteFile($file_name) {

        if ($this->metadataHandler){
            // the handler's business logic may change the target filename yet again prior to the upload
            $this->metadataHandler->preDeleteCallback($file_name, $this->options);
        }

        $file_path = $this->options->getPath($file_name);
        $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);

        if ($this->metadataHandler){
            // the handler's business logic may change the target filename yet again prior to the upload
            $this->metadataHandler->postDeleteCallback($file_name, $this->options);
        }

        return print_r($this->metadataHandler->preDeleteCallback($file_name, $this->options),true);
    }

    protected function __construct(UploadFileManagerOptions $options, UploadedFileMetadataHandler $handler = null){
        $this->options = $options;
        $this->metadataHandler = $handler;
    }

    protected function doUploadProcess(PendingUploadFile $fileInfo, stdClass &$file){
    }

    protected function getManagerOptions(){
        return $this->options;
    }

    protected static function hasPerms($directory, UploadFileManagerOptions $options){
        if (!is_dir($directory)){
            mkdir($directory, $options->mkdirMode, true);
        }
        $isReadable = is_readable($directory);
        $isWritable = is_writable($directory);
        if(!$isReadable || !$isWritable){
            return false;
        }
        return true;
    }

    protected function prepareUploadPath(ImageVersion $version = null){
        $uploadDir = $this->options->getPath(null, $version);
        if (!is_dir($uploadDir)){
            mkdir($uploadDir, $this->options->mkdirMode, true);
        }
        return is_dir($uploadDir);
    }

    protected function getFileObject($file_name) {
        if ($this->options->isExistingFile($file_name)) {
            $file = new stdClass();
            $file->name = $file_name;
            $file->size = $this->options->getFileSize($file_name);
            $file->url = $this->options->getUrl($file->name);
            return $file;
        }
        return null;
    }

    public function getFileObjects($iteration_method = 'getFileObject') {
        $upload_dir = $this->options->getPath();
        if (!is_dir($upload_dir)) {
            return array();
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            scandir($upload_dir)
        )));
    }
}

/**
 * This is more or less the controller that interfaces with the disk.
 */
class ImageFileManager extends GeneralFileManager{

    public static function getInstance(ImageFileManagerOptions $options, UploadedFileMetadataHandler $handler = null){
        if (self::hasPerms($options->directory)){
            return new ImageFileManager($options, $handler);
        }
        return null;
    }

    protected function __construct(ImageFileManagerOptions $options, UploadedFileMetadataHandler $handler = null){
        parent::__construct($options, $handler);
    }

    protected function doUploadProcess(PendingUploadFile $fileInfo, stdClass &$file){
        /* @var $options ImageFileManagerOptions */
        $options = $this->getManagerOptions();

        // first save the original file regardless of what its size is
        $masterFilePath = $options->getPath($fileInfo->fileName);
        move_uploaded_file($fileInfo->tmpName, $masterFilePath);

        if ($options->orientFileOnUpload){
            self::orientFile($masterFilePath);
        }

        // now iterate over the versions and save the scaled versions
        foreach ($options->imageVersions as $version){
            if ($this->saveScaledImage($fileInfo, $version, $options)){
                if (!empty($version->folderName)){
                    $file->{$version->folderName."_url"} = $options->getUrl($fileInfo->fileName, $version);
                } else {
                    $file->size = $options->getFileSize($fileInfo->fileName, true);
                }
            }
        }
    }

    protected function getFileObject($file_name) {
        $file = parent::getFileObject($file_name);
        if ($file){
            /* @var $options ImageFileManagerOptions */
            $options = $this->getManagerOptions();
            foreach($options->imageVersions as $version) {
                if (!empty($version->folderName)) {
                    if (is_file($options->getPath($file_name, $version))) {
                        $file->{$version->folderName.'_url'} = $options->getUrl(
                            $file->name,
                            $version
                        );
                    }
                }
            }
        }
        return $file;
    }

    private function saveScaledImage(PendingUploadFile $fileInfo, ImageVersion $version, ImageFileManagerOptions $options) {
        $masterFilePath = $options->getPath($fileInfo->fileName);
        $versionFilePath = $options->getPath($fileInfo->fileName, $version);

        $ready = $this->prepareUploadPath($version);

        if (!$ready){
            return false;
        }

        list($img_width, $img_height) = @getimagesize($masterFilePath);
        if (!$img_width || !$img_height) {
            return false;
        }
        $scale = min(
            $version->maxWidth / $img_width,
            $version->maxHeight / $img_height
        );
        if ($scale >= 1) {
            if ($masterFilePath !== $versionFilePath) {
                return copy($masterFilePath, $versionFilePath);
            }
            return true;
        }
        $new_width = $img_width * $scale;
        $new_height = $img_height * $scale;
        $new_img = @imagecreatetruecolor($new_width, $new_height);
        $ext = strtolower(pathinfo($fileInfo->origFileName, PATHINFO_EXTENSION));
        ini_set('memory_limit', '64M');
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($masterFilePath);
                $writeImageFunction = 'imagejpeg';
                $image_quality = $version->jpgQuality;
                break;
            case 'gif':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                $src_img = @imagecreatefromgif($masterFilePath);
                $writeImageFunction = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                @imagealphablending($new_img, false);
                @imagesavealpha($new_img, true);
                $src_img = @imagecreatefrompng($masterFilePath);
                $writeImageFunction = 'imagepng';
                $image_quality = $version->pngCompression;
                break;
            default:
                $src_img = null;
        }
        $success = $src_img && @imagecopyresampled(
            $new_img,
            $src_img,
            0, 0, 0, 0,
            $new_width,
            $new_height,
            $img_width,
            $img_height
        ) && $writeImageFunction($new_img, $versionFilePath, $image_quality);
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($src_img);
        @imagedestroy($new_img);
        return $success;
    }

    private static function orientFile($file_path) {
        if (!function_exists('exif_read_data')) {
            return false;
        }
        $exif = @exif_read_data($file_path);
        if ($exif === false) {
            return false;
        }
        $orientation = intval(@$exif['Orientation']);
        if (!in_array($orientation, array(3, 6, 8))) {
            return false;
        }
        $image = @imagecreatefromjpeg($file_path);
        switch ($orientation) {
            case 3:
                $image = @imagerotate($image, 180, 0);
                break;
            case 6:
                $image = @imagerotate($image, 270, 0);
                break;
            case 8:
                $image = @imagerotate($image, 90, 0);
                break;
            default:
                return false;
        }
        $success = imagejpeg($image, $file_path);
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($image);
        return $success;
    }
}
?>