<?php
abstract class FileCacheManager {

    abstract protected function getDirectory();

    protected function hasPerms(){
        $directory = $this->getDirectory();
        if (!is_dir($directory)){
            mkdir($directory, 0755, true);
        }
        $isReadable = is_readable($directory);
        $isWritable = is_writable($directory);
        if(!$isReadable || !$isWritable){
            return false;
        }
        return true;
    }
}

interface CampaignEventHelper{
    public function getRecords();
}

/**
 * Class CampaignCacheProvider
 * Retrieves records from a file cache, and if the file cache does not exist, creates it.
 */
class CampaignCacheProvider extends FileCacheManager{
    private $directory;
    private $helper;

    public function __construct($directory, CampaignEventHelper $helper){
        $this->directory = $directory;
        $this->helper = $helper;
    }

    /**
     * Returns an array of EventRecord for the specified campaign name.
     * @param $campaignName string
     * @param $forceRefresh bool
     * @return array of EventRecord
     */
    public function getRecords($campaignName, $forceRefresh = false){
        if (!$forceRefresh){
            $cacheRecords = $this->getRecordsFromCache($campaignName);
            if (is_object($cacheRecords) && isset($cacheRecords->collection)){
                return $cacheRecords->collection;
            }
            if ($cacheRecords == self::FILE_DECODE_ERROR){
                // this can happen if another process is in the middle of writing the file.
                sleep(1); // sleep for 1 second, try one more time, then let it force refresh
                $cacheRecords = $this->getRecordsFromCache($campaignName);
                if (is_object($cacheRecords) && isset($cacheRecords->collection)){
                    return $cacheRecords->collection;
                }
            }
            if ($cacheRecords == self::FILE_CACHE_OUT_OF_DATE){
                // let this fall through and force a refresh passively
            }
        }
        // requires refresh, or there is some other problem with the cache
        $records = $this->helper->getRecords();
        $this->writeRecordsToCache($campaignName, $records);
        return $records;
    }

    const FILE_READ_ERROR = -1;
    const FILE_EMPTY_ERROR = -2;
    const FILE_DECODE_ERROR = -3;
    const FILE_INVALID_CONTENT_ERROR = -4;
    const FILE_CACHE_OUT_OF_DATE = -5;

    private function getRecordsFromCache($campaignName){
        if (!$this->hasPerms()){
            trigger_error("CampaignCacheProvider.getRecordsFromCache permission error: ".$this->directory, E_USER_ERROR);
        }
        $filePath = $this->getFilePath($campaignName);
        if (file_exists($filePath) && (filemtime($filePath) > (time() - 60 * 5 ))){
            // file is less than 5 minutes old
            $rawJsonString = file_get_contents($filePath);
            if ($rawJsonString === false){
                // file read failed!
                trigger_error("CampaignCacheProvider.getRecordsFromCache file read error: ".$filePath, E_USER_ERROR);
                return self::FILE_READ_ERROR;
            }
            if (strlen($rawJsonString) == 0){
                trigger_error("CampaignCacheProvider.getRecordsFromCache cache file returned empty string: ".$rawJsonString, E_USER_ERROR);
                return self::FILE_EMPTY_ERROR;
            }
            $recordCache = json_decode($rawJsonString);
            if ($recordCache === null){
                trigger_error("CampaignCacheProvider.getRecordsFromCache cache file decode error: ".$rawJsonString, E_USER_ERROR);
                return self::FILE_DECODE_ERROR;
            }
            if (!is_object($recordCache) || !isset($recordCache->collection)){
                trigger_error("CampaignCacheProvider.getRecordsFromCache cache file did not contain an array: ".$rawJsonString, E_USER_ERROR);
                return self::FILE_INVALID_CONTENT_ERROR;
            }
            return $recordCache;
        }
        return self::FILE_CACHE_OUT_OF_DATE;
    }

    const FILE_WRITE_ERROR = -100;

    private function writeRecordsToCache($campaignName, array $records){
        $cacheObject = (object)array(
            "campaignName" => $campaignName,
            "collection" => $records
        );
        $filePath = $this->getFilePath($campaignName);
        $cacheString = json_encode($cacheObject);
        $fileStatus = file_put_contents($filePath, $cacheString, LOCK_EX);
        if ($fileStatus === false){
            trigger_error("CampaignCacheProvider.writeRecordsToCache failed: ".$filePath." data: ".$cacheString, E_USER_ERROR);
            return self::FILE_INVALID_CONTENT_ERROR;
        }
        return $fileStatus;
    }

    private function getFilePath($campaignName){
        return rtrim($this->directory, '/') . '/'.$campaignName.".txt";
    }

    protected function getDirectory(){
        return $this->directory;
    }
}