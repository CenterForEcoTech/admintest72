<?php

class AuthAttempt {
    const INDIVIDUAL_MEMBER_TYPE = 1;
    const MERCHANT_MEMBER_TYPE = 2;
    const ORGANIZATION_MEMBER_TYPE = 3;

    public $message;
    public $memberId;
    public $memberTypeId;
    public $memberEmail;
    public $memberFullName;
    public $isFirstTime;
    public $startPage;
    public $attemptedUsername;
}

class AdminAuthAttempt extends AuthAttempt{
    public $adminFullName;
    public $adminEmail;
    public $adminId;
    public $adminSecurityGroupId;
}

class ParseUsernameResponse {
    public $message;
    public $memberId;
    public $memberEmail;
    public $memberPhone;
    public $attemptedUsername;
    public $isEmpty;
}

class ParseAdminUsernameResponse {
    public $message;
    public $attemptedUsername;
    public $isEmpty;
}

class ResetRequest {
    public $message;
    public $memberId;
    public $memberEmail;
    public $memberPhone;
    public $hasPendingVerification = false;
    public $verificationCode;
    public $attemptedUsername;
}

class MemberSessionBase{
    public $entityType;
    public $id;
    public $email;
    public $lat;
    public $long;
    public $city;
    public $state;
    public $zip;
    public $pid;
    public $name;
    public $ein;

    protected function __construct($dataRowArray = null){
        if ($dataRowArray && is_array($dataRowArray)){
            foreach ($dataRowArray as $key => $value){
                if (property_exists($this, $key)){
                    $this->$key = htmlspecialchars($value);
                }
            }
        }
    }

    public function __get($key){
        switch ($key){
            case "cityStateZip":
                return $this->city.", ".$this->state." ".$this->zip;
            default:
                return $this->$key;
        }
    }
}

class IndividualSession extends MemberSessionBase{
    public function __construct($dataRowArray = null){
        $this->entityType = 'Individual';
        parent::__construct($dataRowArray);
    }
}

class MerchantSession extends MemberSessionBase{
    public $isPlanSubscriber = false;

    public function __construct($dataRowArray = null){
        $this->entityType = 'Merchant';
        parent::__construct($dataRowArray);
    }
}

class OrganizationSession extends MemberSessionBase{
    public function __construct($dataRowArray = null){
        $this->entityType = 'Organization';
        parent::__construct($dataRowArray);
    }
}