<?php
/*
 * This exists largely for documentation purposes, and not yet endorsed for implementation
 */

//
// API contracts
//

interface UserLoginRequest{
    /**
     * Username as expected by the system.
     * @abstract
     * @param $username
     * @return mixed
     */
    public function setUsername($username);

    /**
     * Password as expected by the system. This contract does not imply any requirement with respect to encryption.
     * @abstract
     * @param $password
     * @return mixed
     */
    public function setPassword($password);
}

interface UserLoginResponse{

    /**
     * Returns the singleton user object.
     * @abstract
     * @return User
     */
    public function getUser();

    /**
     * Returns the one or many accounts this user has privileges for
     * @abstract
     * @return array of UserAccount
     */
    public function getAccounts();
}

interface AccountType{
    /**
     * Returns the generic name for this account type. e.g. "Merchant"
     * @abstract
     * @return string
     */
    public function getTypeName();

    /**
     * Given a predefined key constant, returns some expected value or object for that key.
     * @abstract
     * @param $key
     * @return mixed
     */
    public function getDefaultPreference($key);
}

interface AuthorizedUser{
    /**
     * Returns the user object.
     * @abstract
     * @return User
     */
    public function getUser();

    /**
     * Returns true if the user is authorized for a specific action as specified by the key and optional parameters.
     * @abstract
     * @param $key
     * @param array $params
     * @return bool
     */
    public function isAuthorized($key, array $params = null);
}

interface UserAccount{
    /**
     * Returns the account type.
     * @abstract
     * @return AccountType
     */
    public function getAccountType();

    /**
     * Returns an array of authorized users.
     * @abstract
     * @return array of AuthorizedUser
     */
    public function getAuthorizedUsers();
}

//
// Idealized data contracts (public members only)
//

class AccountCredentials{
    public $username;
}

class User implements AuthorizedUser{
    /**
     * A user may have many credentials. This is the array of them (all their phone numbers, emails, CTID's etc).
     * This same user may use any of these methods to log into their one account. They may also be able to "join up" these various
     * emails and phone numbers to the same account.
     * @var array of AccountCredentials
     */
    public function getAccountCredentials()
    {
        // TODO: implement getAccountCrednentials() method
    }

    /**
     * Key to the user's principle account (customer account usually)
     * @var int
     */
    public $primaryAccountId;

    /**
     * Returns a collection of UserAccount for the purpose of allowing a user to select and "switch" accounts.
     * @return array of UserAccount
     */
    public function getUserAccounts()
    {
        // TODO: implement getUserAccounts() method.
    }

    /**
     * Returns the user object.
     * @return User
     */
    public function getUser()
    {
        return $this;
    }

    /**
     * Returns true if the user is authorized for a specific action as specified by the key and optional parameters.
     * @param $key
     * @param array $params
     * @return bool
     */
    public function isAuthorized($key, array $params = null)
    {
        // TODO: Implement isAuthorized() method.
    }
}

abstract class UserAccountBase implements UserAccount{

    /**
     * Returns the account type.
     * @return AccountType
     */
    public function getAccountType()
    {
        // TODO: Implement getAccountType() method.
    }

    /**
     * Returns an array of authorized users.
     * @return array of AuthorizedUser
     */
    public function getAuthorizedUsers()
    {
        // TODO: Implement getAuthorizedUsers() method.
    }
}

class MerchantAccount extends UserAccountBase{
    /**
     * Key to the primary (or only) venue
     * @var int
     */
    public $primaryVenueId;

    /**
     * Returns an array of venue object.
     * @return array of Venue
     */
    public function getVenues(){
        // TODO: Implement getVenues() method;
    }
}

class NonprofitAccount extends UserAccountBase{

}

class CustomerAccount extends UserAccountBase{

}

?>