<?php
/**
 * Private api for crud operations
 */
interface EventManager{

    /**
     * Given a valid $eventRecord, delete it from the system.
     * @abstract
     * @param EventRecord $eventRecord
     * @param $arrayOfVenues
     * @param int $memberId
     * @param null $adminId
     * @internal param \of $Array Venue $arrayOfVenues
     * @return mixed
     */
    public function deleteEvent(EventRecord $eventRecord, $arrayOfVenues, $memberId = 0, $adminId = null);

    /**
     * Given a valid $eventRecord, update it in the system
     * @abstract
     * @param EventRecord $eventRecord
     * @param int $memberId
     * @param null $adminId
     * @return mixed
     */
    public function updateEvent(EventRecord $eventRecord, $memberId = 0, $adminId = null);

    /**
     * Given a valid $eventRecord, create it in the system
     * @abstract
     * @param EventRecord $eventRecord
     * @param int $memberId
     * @param null $adminId
     * @return mixed
     */
    public function createEvent(EventRecord $eventRecord, $memberId = 0, $adminId = null);
}

interface ExternalIdXwalk{
    public function setEventBriteVenueID($eventVenueId, $eventBriteVenueId);
    public function setEventBriteID(EventRecord $eventRecord);
    public function setFacebookID(EventRecord $eventRecord);
}

/**
 * Private api for internal event logger
 */
interface EventLogger{
    public function log($eventId, $logAction, $memberId, $adminId = null);
}

/**
 * Private api for internal system logger
 */
interface SystemLogger{
    public function log($sourceLocation, $message);
}

interface InvoiceIntegration{
    /**
     * Returns the name of the integration that matches the registered InvoiceProvider
     * @abstract
     * @return string name value
     */
    public function getProviderName();

    /**
     * Returns the number of weeks until an invoice is due.
     * @abstract
     * @return int
     */
    public function getWeeksUntilDue();

    /**
     * Creates a customer in the integration system and returns that external system's identifier for this customer.
     * @abstract
     * @param MerchantInfo $merchantInfo
     * @return string identifier
     */
    public function createCustomer(MerchantInfo $merchantInfo);

    /**
     * Given an array of invoice records, creates invoices in the integration system and updates the invoice records
     * with the external system's identifier for each invoice.
     * @abstract
     * @param array $invoiceRecords
     * @return array $invoiceRecords
     */
    public function createInvoice(array $invoiceRecords);

    /**
     * Given a MerchantInfo object, this integration returns the url that merchant can follow to pay the bill. May return
     * an empty string or null if no url access to payment is supported. Should not throw an exception.
     * @abstract
     * @param MerchantInfo $merchantInfo
     * @return string
     */
    public function getPaymentUrl(MerchantInfo $merchantInfo);

    /**
     * Given an external customer ID returns all payments made by this customer in the last x months
     * @param $externalCustomerId
     * @param $months int number of months (default = 2)
     * @return array $invoicePayment InvoicePayment
     */
    public function getInvoicePayments($externalCustomerId, $months = 2);
}

interface TransactionProvider{
    /**
     * Returns an array of EventTransaction for the specified Invoice ID.
     * @abstract
     * @param $invoiceId
     * @return EventTransactionRecord
     */
    public function getInvoiceTransactions($invoiceId);
}

interface SiteConfigProvider{
    public function setConfig($name, $value);
}

// data contracts
include_once("DataContracts.php");
include_once("InvoiceRecord.php");
include_once("ReferralCodeRate.php");