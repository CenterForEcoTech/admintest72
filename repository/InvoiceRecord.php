<?php
/**
 * Private data contract for merchant record (e.g., for invoicing)
 */
class MerchantInfo{
    public $name = ""; // preferred merchant name
    public $officialName = ""; // official merchant name

    public $addressLine1 = "";
    public $addressLine2 = "";
    public $addressLine3 = "";
    public $addressLine4 = "";
    public $city = "";
    public $state = "";
    public $country = "";
    public $postalCode = "";

    public $contactFirstName = "";
    public $contactLastName = "";
    public $contactEmail = "";
    public $contactPhone = "";
    public $altPhone = "";
    public $fax = "";

    public $merchantID = "";
    public $billingId = "";
    public $billingCode = ""; // "currentReferralCode"
    public $referralCodeRate = null;

    public function getApplicableRate($effectiveDate, ReferralCodeRate $defaultRate){
        $merchantRate = $this->referralCodeRate;
        if (isset($merchantRate) && is_a($merchantRate, "ReferralCodeRate")){
            /* @var $merchantRate ReferralCodeRate */
            if ($merchantRate->isApplicable($effectiveDate)){
                return $merchantRate;
            }
        }
        return $defaultRate;
    }
}

class InvoiceRecord{
    private $id;
    private $siteName = null;
    private $merchantInfo = null;
    private $externalInvoiceId = null;
    private $lineCollection = array();
    private $lineItems = null;
    private $eventNames = array();
    private $totalSales = 0.0;
    private $totalDonations = 0.0;
    private $totalFees = 0.0;
    private $numberOfWeeksUntilDue = 2;
    public $transactionId = null; // set by the invoice processor and used to match up operation result to invoice
    private $poNumber = null;
    private $invoiceDate = null;
    private $dueDate = null;
    private $request = null;
    private $responseString = null;

    public function __construct($id, MerchantInfo $merchantInfo, $siteName, $numberOfWeeksUntilDue = 2){
        $this->id = $id;
        $this->merchantInfo = $merchantInfo;
        $this->siteName = $siteName;
        if (is_numeric($numberOfWeeksUntilDue) && $numberOfWeeksUntilDue >= 0){
            $this->numberOfWeeksUntilDue = $numberOfWeeksUntilDue;
        }
    }

    public function isReady(){
        $hasCustomerId = !empty($this->merchantInfo) && strlen("".$this->merchantInfo->billingId) > 0;
        $hasInvoiceId = is_numeric($this->id) && $this->id > 0;
        $hasLineItems = count($this->lineItems) > 0;
        return $hasCustomerId && $hasInvoiceId && $hasLineItems;
    }

    /**
     * Undocumented business rule: if the invoice is coming from one of our test systems, we need to mark it as a test by
     * passing a value into the poNumber (e.g., "TEST-localhost"). This is carry-over logic from the refactoring (not decided
     * at the point this code was written).
     * @param $poNumberValue
     */
    public function markAsTestUsingPoNumber($poNumberValue){
        $this->poNumber = $poNumberValue;
    }

    public function getPoNumber(){
        return $this->poNumber;
    }

    public function getInvoiceNumber(){
        return $this->id;
    }

    public function getInvoiceDate(){
        return $this->invoiceDate;
    }

    public function setInvoiceDate($invoiceDate){
        if (strtotime($invoiceDate) !== false){
            $this->invoiceDate = date("n/j/Y", strtotime($invoiceDate));
            $this->dueDate = date("n/j/Y", strtotime($invoiceDate." +".$this->numberOfWeeksUntilDue." weeks"));
        }
    }

    public function getDueDate(){
        return $this->dueDate;
    }

    public function getDescription($paymentUrl){
        $thankYouPhrase = "Thank you for hosting ".$this->siteName." events!";
        $paymentPhrase = "Please remit $".$this->getAmountDue()." now by going to ".$paymentUrl.".";
        if (count($this->eventNames) == 1){
            $eventName = reset($this->eventNames);
            $thankYouPhrase = "Thank you for hosting the ".$this->siteName." event, '".$eventName."'."; // not adding date because there may have been more than one event with the same name
        } else if (count($this->eventNames) > 1){
            $thankYouPhrase = "Thank you for hosting ".count($this->eventNames)." ".$this->siteName." events (";
            $isFirst = true;
            foreach ($this->eventNames as $key=>$eventName){
                if ($isFirst){
                    $isFirst = false;
                } else {
                    $thankYouPhrase .= ", ";
                }
                $thankYouPhrase .= "'".$eventName."'";
            }
            $thankYouPhrase .= ").";
        }
        return $thankYouPhrase." ".$paymentPhrase;
    }

    public function getCustomerId(){
        return $this->merchantInfo->billingId;
    }

    public function getMerchantId(){
        return $this->merchantInfo->merchantID;
    }

    public function getMerchantInfo(){
        return $this->merchantInfo;
    }

    public function getExternalInvoiceId(){
        return $this->externalInvoiceId;
    }

    public function setExternalInvoiceId($externalInvoiceId){
        $this->externalInvoiceId = $externalInvoiceId;
    }

    public function getApiRequest(){
        return $this->request;
    }

    public function setApiRequest($request){
        $this->request = $request;
    }

    public function getApiResponse(){
        return $this->responseString;
    }

    public function setApiResponse($responseString){
        $this->responseString = (string)$responseString;
    }

    public function getAmountDue(){
        $amountDue = $this->totalDonations + $this->totalFees;
        return number_format($amountDue, 2, '.', '');
    }

    public function getDonationAmount(){
        return number_format($this->totalDonations, 2, '.', '');
    }

    public function getFeeAmount(){
        return number_format($this->totalFees, 2, '.', '');
    }

    /**
     * Adds a Donation line to the invoice
     * @param DonationLineItemBase $lineItem
     */
    public function addDonationLineItem(DonationLineItemBase $lineItem){
        $feeRate = $lineItem->getFeeRate();
        $discriminator = $feeRate->getDiscriminator();
        $eventName = $lineItem->getEventName();
        if (strlen($eventName) && !isset($this->eventNames[strtolower($eventName)])){
            $this->eventNames[strtolower($eventName)] = $eventName;
        }
        if (!isset($this->lineCollection[$discriminator])){
            $this->lineCollection[$discriminator] = array();
        }
        $this->lineCollection[$discriminator][] = $lineItem;

        $this->totalDonations += $lineItem->getAmount();
        $this->totalSales += $lineItem->getTotalSales();
    }

    public function generateFeeLineItems($siteName){
        $finalCollection = array();
        foreach ($this->lineCollection as $lineCollection){
            $feeRate = $lineCollection[0]->getFeeRate();
            $totalSales = 0.0;
            foreach ($lineCollection as $lineItem){
                /* @var $lineItem DonationLineItemBase */
                $totalSales += $lineItem->getTotalSales();
                $finalCollection[] = $lineItem;
            }
            $feeLine = new ServiceFeeLineItem($totalSales, $feeRate, $siteName);
            $this->totalFees += $feeLine->getAmount();
            $finalCollection[] = $feeLine;
        }
        $this->lineItems = $finalCollection;
    }

    public function getInvoiceLineItems(){
        if ($this->isReady()){
            return $this->lineItems;
        }
        return null;
    }
}
abstract class InvoiceLineItemBase {
    // line item types
    const ServiceFee = 0;
    const FCDonation = 1;
    const CCDonation = 2;

    abstract public function getItemType();

    /**
     * Returns the quantity for this line item
     * @return int
     */
    public function getQuantity(){
        return 1; // default
    }

    /**
     * Returns the amount being invoiced for this item.
     * @return float
     */
    abstract public function getAmount();

    /**
     * Returns the description text
     * @return string
     */
    abstract public function getDescription();

    /**
     * Returns the rate applicable to this line.
     * @abstract
     * @return ReferralCodeRate
     */
    abstract public function getFeeRate();
}
abstract class DonationLineItemBase extends InvoiceLineItemBase{
    private $totalSaleAmount = 0.0;
    private $donationPercentage = 0;
    private $charityName = "";
    private $eventName = "";
    private $eventDate = null;
    private $transactionID = 0;
    private $transCode = "";
    private $customerDisplayText = "";
    private $cachedAmount = null;
    private $feeRate = null;

    protected function __construct($totalSaleAmount, $donationPercentage, $charityName, $eventName, $eventDate, $transactionID, $transCode, $customerDisplayText, $amount = null){
        $this->totalSaleAmount = (is_numeric($totalSaleAmount)) ? $totalSaleAmount : 0.0;
        $this->donationPercentage = (is_numeric($donationPercentage) ? $donationPercentage : 0);
        $this->charityName = $charityName;
        $this->eventName = $eventName;
        $this->eventDate = $eventDate;
        $this->transactionID = $transactionID;
        $this->transCode = $transCode;
        $this->customerDisplayText = $customerDisplayText;
        $this->setAmount($amount);
    }

    protected function setFeeRate(ReferralCodeRate $rate){
        $this->feeRate = $rate;
    }

    /**
     * @return null|ReferralCodeRate
     */
    public function getFeeRate(){
        return $this->feeRate;
    }

    /**
     * Builds an invoice line item for the specified EventTransactionRecord
     * @static
     * @param EventTransactionRecord $record
     * @param MerchantInfo $merchantInfo
     * @param ReferralCodeRate $defaultRate
     * @return CustomerChoiceLineItem|FeaturedCauseLineItem|null
     */
    public static function buildLineItem(EventTransactionRecord $record, MerchantInfo $merchantInfo, ReferralCodeRate $defaultRate){
        $lineItem = null;
        $totalSaleAmount = $record->eligibleAmount;
        $donationPercentage = $record->percentageAmount;
        $charityName = $record->getCharityDisplayText();
        $eventName = $record->eventTitle;
        $eventDate = $record->eventDate;
        $transactionID = $record->id;
        $transCode = $record->transCode;
        $customerDisplayText = $record->getCustomerDisplayText();
        $generatedAmount = $record->generatedAmount;
        $feeRate = $merchantInfo->getApplicableRate($eventDate, $defaultRate);
        if ($record->isFeaturedCause()){
            $lineItem = new FeaturedCauseLineItem($totalSaleAmount, $donationPercentage, $charityName, $eventName, $eventDate, $transactionID, $transCode, $customerDisplayText, $generatedAmount);
        } else {
            $lineItem = new CustomerChoiceLineItem($totalSaleAmount, $donationPercentage, $charityName, $eventName, $eventDate, $transactionID, $transCode, $customerDisplayText, $generatedAmount);
        }
        $lineItem->setFeeRate($feeRate);
        return $lineItem;
    }

    private function setAmount($amount){
        $this->cachedAmount = (is_numeric($amount) || $amount == null) ? $amount : null;
    }

    public function getTotalSales(){
        return $this->totalSaleAmount;
    }

    public function getEventName(){
        return $this->eventName;
    }

    public function getAmount(){
        if ($this->cachedAmount == null){
            $calculatedAmount = $this->totalSaleAmount * $this->donationPercentage / 100;
            $this->setAmount($calculatedAmount);
        }
        return $this->cachedAmount;
    }

    public function getDescription(){
        return "$".money($this->totalSaleAmount)." transaction Benefiting ".$this->charityName." by ".$this->customerDisplayText;
    }

    public function getTransCode(){
        return $this->transCode;
    }
}
class FeaturedCauseLineItem extends DonationLineItemBase{

    public function getItemType(){
        return parent::FCDonation;
    }
}
class CustomerChoiceLineItem extends DonationLineItemBase{

    public function getItemType(){
        return parent::CCDonation;
    }
}
class ServiceFeeLineItem extends InvoiceLineItemBase{
    protected $applicableAmount = 0.0;
    protected $feeRate = null;
    protected $cachedAmount = null;
    protected $siteName = "our service";

    public function __construct($applicableSaleAmount, ReferralCodeRate $feeRate, $siteName, $amount = null){
        $this->applicableAmount = $applicableSaleAmount;
        $this->feeRate = $feeRate;
        $this->siteName = $siteName;
        $this->setAmount($amount);
    }

    public function getItemType(){
        return parent::ServiceFee;
    }

    private function setAmount($amount){
        $this->cachedAmount = (is_numeric($amount) || $amount == null) ? $amount : null;
    }

    /**
     * Returns the amount being invoiced for this item.
     * @return float
     */
    public function getAmount()
    {
        if ($this->cachedAmount == null){
            $calculatedAmount = $this->feeRate->calculateDefaultAmount($this->applicableAmount);
            $this->setAmount($calculatedAmount);
        }
        return $this->cachedAmount;
    }

    /**
     * Returns the description text
     * @return string
     */
    public function getDescription()
    {
        return $this->feeRate->getDescription($this->siteName);
    }

    public function getFeeRate(){
        return $this->feeRate;
    }
}

class SimpleInvoiceRow {
    const PAID_STATUS = "Funds Received";

    public $id;
    public $amount;
    public $amountPaid;
    public $status;
    public $externalId;
    public $externalCustomerId;

    public static function mapTo($dbRow){
        if (is_array($dbRow)){
            $dbRow = (object)$dbRow;
        }
        $record = new SimpleInvoiceRow();
        $record->id = $dbRow->id;
        $record->amount = $dbRow->amount;
        $record->amountPaid = $dbRow->amountPaid;
        $record->status = $dbRow->status;
        $record->externalId = $dbRow->externalId;
        $record->externalCustomerId = $dbRow->externalCustomerId;
        return $record;
    }
}

class InvoicePayment{
    public $externalPaymentId;
    public $externalInvoiceId;
    public $amountPaid;
    public $paymentDate;
}
?>