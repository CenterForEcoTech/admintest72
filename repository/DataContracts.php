<?php
include_once("_getRootFolder.php");
include_once($rootFolder."/repository/utils/UrlLinker.php");



/**
 * This is the Paginated Result contract. This wraps a collection returned by a function that supports a PaginatedResult.
 */
class PaginatedResult{
	/**
	 * @var int zero-based position in a list
	 */
	public $startIndex;
	/**
	 * @var int page length (number of records to return)
	 */
	public $maxLength;
	/**
	 * @var array collection. May be any type of collection, but should be iterable with foreach
	 */
	public $collection = array();
	/**
	 * @var bool true if there are more "pages" to retrieve
	 */
	public $hasMore;
}

/**
 * Generic result class to handle pagination by pages rather than start index
 */
class PagedResult extends PaginatedResult {
    /**
     * Total records in unpaginated result set
     * @var $totalRecords int
     */
    public $totalRecords;

    /**
     * Helper function to set this boolean value in the parent class.
     * TODO: this really needs to not be a required step to use this object.
     */
    public function setHasMore(){
        $this->hasMore = ($this->startIndex + count($this->collection) < $this->totalRecords);
    }

    public function __get($name){
        switch ($name){
            case "start":
                return $this->startIndex;
            case "size":
                return $this->maxLength;
            default:
                return $this->$name;
        }
    }
}

class PaginationCriteria {
    public $keyword = null;
    public $start = 0;
    public $size = 10;
    public $sortColumn = null;
    public $sortOrder = "ASC";
    public $id; // easy placeholder for search by id

    public function __construct($id = null){
        $this->id = $id;
    }

    /**
     * The presence of this variable does not indicate that the API supports multi-column sort
     * @var array of MultiColumnSort
     */
    public $multiColumnSort = array();
}

class MultiColumnSort{
    public $column;
    public $order;

    public function __construct($column, $sortOrder = "asc"){
        if (strtolower($sortOrder) !== "asc"){
            $sortOrder = "desc";
        }
        $this->column = $column;
        $this->order = $sortOrder;
    }
}


class ProductCriteria extends PaginationCriteria{
    public $hasEvents;
    private $productName;

    public function setProductName($name){
        $this->productName = str_replace("-", " ", $name);
    }

    public function getProductName(){
        return $this->productName;
    }
}

class ProductRecord{
    public $id = 0;
    public $efi;
    public $description;
    public $csgCode;
    public $bulbStyle;
    public $bulbType;
    public $bulbClass;
    public $unitsPerPack;
	public $unitsPerCase;
    public $minimumQty;
    public $minimumDays;
	public $currentDaysOnHandQty;
	public $currentDaysOnHandDays;
    public $cost;
    public $retail;
    public $activeId;
    public $canBeOrderedId;
    public $displayOrderId;

    public function setImageUrl($serverTagImgPath){
        if ($this->imageFileName){
            $this->imageUrl = $serverTagImgPath.$this->imageFileName;
        }
    }

    public static function castAs(stdClass $object){
        // construct the core object
        $record = new ProductRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    public static function convertSet(array $productQueryResult, $serverTagImgPath){
        $collection = array();
        foreach ($productQueryResult as $productRecord){
            $collection[] = self::convert((object) $productRecord, $serverTagImgPath);
        }
        return $collection;
    }

    public static function convert($productRecord, $serverTagImgPath){
        $productRecord = new ProductRecord();
        $productRecord->id = $productRecord->ProductList_ID;
        $productRecord->efi = $productRecord->ProductList_EFI;
        $productRecord->description = $productRecord->ProductList_Description;
        $productRecord->csgCode = $productRecord->ProductList_CSGCode;
        $productRecord->bulbStyle = $productRecord->ProductList_BulbStyle;
        $productRecord->bulbType = $productRecord->ProductList_BulbType;
        $productRecord->unitsPerPack = $productRecord->ProductList_UnitsPerPack;
        $productRecord->unitsPerCase = $productRecord->ProductList_UnitsPerCase;
        $productRecord->minimum = $productRecord->ProductList_Minimum;
        $productRecord->cost = $productRecord->ProductList_Cost;
        $productRecord->retail = $productRecord->ProductList_Retail;
        $productRecord->activeId = $productRecord->ProductList_ActiveID;
        $productRecord->canBeOrderedId = $productRecord->ProductList_CanBeOrderedID;
        $productRecord->displayOrderId = $productRecord->ProductList_DisplayOrderID;
        return $productRecord;
    }
}
class ProductTransferRecord{
    public $efi;
    public $transferDate;
    public $units;
	public $movementType;
    public $warehouseFrom;
    public $warehouseTo;
	public $createdBy;
    public $purchaseOrder;
	public $timeStamp;

    public static function castAs(stdClass $object){
        // construct the core object
        $record = new ProductTransferRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class ProductTagRecord{
    public $ProductList_ID = 0;
    public $ProductList_EFI = "";
    public $ProductList_Description = "";
    public $ProductList_CSGCode = "";
    public $ProductList_BulbStyle = "";
    public $ProductList_BulbType = "";
    public $ProductList_UnitsPerPack = 0;
    public $ProductList_UnitsPerCase = 0;
    public $ProductList_Minimum = 0;
    public $ProductList_Cost = 0;
    public $ProductList_Retail = 0;
    public $ProductList_ActiveID = 0;
    public $ProductList_CanBeOrderedID = 0;
    public $ProductList_DisplayOrderID = 0;

    public function getNormalObject(){
        return self::getNormalizedObject($this);
    }

    public static function getNormalizedObject($productRecordLikeObject){
        $hack = new stdClass();
        $hack->ID = $productRecordLikeObject->ProductList_ID;
        $hack->EFI = $productRecordLikeObject->ProductList_EFI;
        $hack->Description = $productRecordLikeObject->ProductList_Description;
        $hack->CSGCode = $productRecordLikeObject->ProductList_CSGCode;
        $hack->BulbStyle = $productRecordLikeObject->ProductList_BulbStyle;
        $hack->BulbType = $productRecordLikeObject->ProductList_BulbType;
        $hack->UnitsPerPack = $productRecordLikeObject->ProductList_UnitsPerPack;
        $hack->UnitsPerCase = $productRecordLikeObject->ProductList_UnitsPerCase;
        $hack->Minimum = $productRecordLikeObject->ProductList_Minimum;
        $hack->Cost = $productRecordLikeObject->ProductList_Cost;
        $hack->Retail = $productRecordLikeObject->ProductList_Retail;
        $hack->ActiveID = $productRecordLikeObject->ProductList_ActiveID;
        $hack->CanBeOrderedID = $productRecordLikeObject->ProductList_CanBeOrderedID;
        $hack->DisplayOrderID = $productRecordLikeObject->ProductList_DisplayOrderID;
        return $hack;
    }

    public static function getQueryName($productName){
        return str_replace("-"," ",$productName);
    }
}
class WarehouseCriteria extends PaginationCriteria{
    public $hasEvents;
    private $warehouseName;

    public function setWarehouseName($name){
        $this->warehouseName = str_replace("-", " ", $name);
    }

    public function getWarehouseName(){
        return $this->warehouseName;
    }

}

class WarehouseRecord{
    public $id = 0;
    public $displayOrderId;
	public $description;
    public $name;
    public $linkedToEmployeeId;
    public $linkedToSecondEmployeeId;
    public $activeId;
    public $excludeFromCountId;
	public $startDate;
	public $endDate;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new WarehouseRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class CSGDailyDataRecord{
	public $siteId;
	public $projectId;
	public $meastable;
	public $id0;
	public $id1;
	public $installed_dt;
	public $apptDate;
	public $auditor_name;
	public $crew;
	public $partId;
	public $qty;
	public $efiPart;

    public static function castAs(stdClass $object){
        // construct the core object
        $record = new CSGDailyDataRecord();

        // iterate over object
        foreach($object as $key => $value) {
			if ($key && $value){
				if (!is_object($value)){
					$record->$key = $value;
				}
			}
        }
        return $record;
    }
	public static function convertColumnName($colName){
		$colName = str_replace('"','',strtolower($colName));
		if (substr($colName,-2)=="id"){$colName = str_replace("id","Id",$colName);}
		$colName = str_replace("date","Date",$colName);
		$colName = str_replace("efipart","efiPart",$colName);
		if ($colName == "heaDate"){$colName="apptDate";}
		return $colName;
	}
}

class TableRecord{
    public $id = 0;
	public $description;
    public $name;
    public $orderId;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new TableRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HRTrainingInfoRecord{
    public $id = 0;
	public $name;
    public $code;
    public $description;
    public $categories;
    public $purposes;
	public $events;
	public $deliveryMethod;
	public $deliveryDetails;
    public $displayOrderId;
	public $isCertification;
	public $partOfCertificationId;
	public $cost;
	public $costType;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HRTrainingInfoRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HRTrainingMatrixRecord{
    public $id = 0;
	public $trainingName;
    public $trainingId;
    public $trainingDisplayId;
    public $employeeId;
    public $employeeFullName;
	public $status;
	public $timeStamp;
	public $addedByAdmin;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HRTrainingMatrixRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HRTrainingCategoriesRecord{
    public $id = 0;
	public $name;
    public $code;
    public $parentCategoryId;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HRTrainingCategoriesRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}
class HRTrainingCategoriesMatrixRecord{
    public $id = 0;
	public $infoId;
    public $categoryId;
 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HRTrainingCategoriesMatrixRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}
class HREmployeeRecord{
    public $id = 0;
	public $firstName;
    public $lastName;
    public $fullName;
    public $email;
    public $phone;
    public $hireDate;
    public $endDate;
    public $officeLocation;
	public $displayOrderId;
	public $department;
	public $title;
	public $manager;
	public $isManager;
	public $hoursTrackingGBS;
	public $trainingCodes;
	

    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HREmployeeRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
	public function setPhone($phoneValue){
        $this->phone = preg_replace("/[^0-9]/", "", $phoneValue); // remove all non-numeric
    }
}

class HREmployeeDepartmentsRecord{
    public $id = 0;
	public $name;
    public $director;
	public $displayOrderId;
	public $color;
	
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HREmployeeDepartmentsRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HREmployeeTitlesRecord{
    public $id = 0;
	public $name;
    public $activeId;
	public $displayOrderId;
	
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HREmployeeTitlesRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HREmployeeCategoriesRecord{
    public $id = 0;
	public $name;
    public $activeId;
	public $displayOrderId;
	
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HREmployeeCategoriesRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class HRTrainingActivityRecord{
    public $id = 0;
	public $employeeId;
	public $employeeName;
    public $trainingId;
	public $trainingName;
    public $trainingCode;
    public $status;
    public $location;
    public $cost;
    public $companyPaid;
    public $employeePaid;
    public $startDate;
    public $endDate;
    public $effectiveDate;
    public $expirationDate;
    public $notes;
	public $lastEditedTimeStamp;
	public $updatedByAdmin;

 
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new HRTrainingActivityRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class AdminUserRecord{
    public $id = 0;
	public $userName;
	public $userEmail;
    public $userPassword;
    public $firstName;
    public $lastName;
    public $fullName;
	public $securityGroupId;
	public $status;
	public $addedByAdminId;
	public $displayOrderId;
	
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new AdminUserRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}
			
class AdminSecurityGroupsRecord{
    public $id = 0;
	public $name;
    public $description;
	public $displayOrderId;
	
    public static function castAs(stdClass $object){
        // construct the core object
        $record = new AdminSecurityGroupsRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }
}

class StringUtils {

	public static function camelCase($str, $exclude = array()){
		// replace accents by equivalent non-accents
		$str = self::replaceAccents($str);
		// non-alpha and non-numeric characters become spaces
		$str = preg_replace('/[^a-z0-9' . implode("", $exclude) . ']+/i', ' ', $str);
		// uppercase the first character of each word
		$str = ucwords(trim($str));
		return lcfirst(str_replace(" ", "", $str));
	}
 
	public static function replaceAccents($str) {
		$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
		$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");
		return str_replace($search, $replace, $str);
	}
}

//******No Yet Used Below ************//







class IndexedEventVariables{

    const DOCVAR_LAT = 0;
    const DOCVAR_LONG = 1;
    const DOCVAR_STARTDATE = 2;
    const DOCVAR_ENDDATE = 3;
    const DOCVAR_MERCHANT_ID = 4;
    const DOCVAR_NATIONAL_EVENT = 5;
    const DOCVAR_ENDDATE_DATE = 6;
    const DOCVAR_ONLINE_EVENT = 7;

    const QUERYVAR_LAT = 0;
    const QUERYVAR_LONG = 1;
    const QUERYVAR_CURRENTDATE = 2;

    //d[0]=lat
    //d[1]=long
    //d[2]=startDateTime
    //d[3]=endDateTime
    //d[4]=venueID
    //d[5]=isNationalEvent

    //q[0]=userlat
    //q[1]=userlong
}

interface MergeTagSwapper {
    /**
     * Does a "mail merge" using specific rules.
     * @param $templateText string that contains "merge tags"
     * @param $context object used to merge data
     * @return string merged content
     */
    public function merge($templateText, $context);
}

class DefaultEventMergeTagSwapper implements MergeTagSwapper{

    /**
     * Does a "mail merge" using specific rules on an event description (presumably coming from an event template)
     * @param $templateText string that contains "merge tags"
     * @param $context object used to merge data
     * @return string merged content
     */
    public function merge($templateText, $context)
    {
        if ($context instanceof EventRecord){
            /* @var $context EventRecord */
            // brute force for now
            $templateText = str_replace("*|BUSINESS_NAME|*", $context->venue->name, $templateText);
            $templateText = str_replace("*|DONATION_PHRASE|*", $context->getFullOfferText(), $templateText);
            $templateText = str_replace("*|CHARITY_AMOUNT|*", $context->getOfferText(), $templateText);
        }
        return $templateText;
    }
}

/**
 * This is the event data contract
 *
 */
class EventRecord {
    public $isOwnedByMerchant = false;
    public $isDraft = false;
    public $publishedEventId = 0;
    public $eventTemplateId = 0;
    public $eventTemplateUrl = null;
	public $id = 0;
    public $versionId = 0;
    public $versionStatus = "";
	public $title = "";
	public $description = "";
	public $createdDate = NULL;
	public $lastUpdatedDate = NULL;
	public $startDate = NULL; // this is a time
	public $endDate = NULL; // this is a time
	public $isMultiDay = false;
    public $isNationalEvent = false;
    public $isCausetownEvent = true;
    public $isIndexedAsCC = false; // only used by indexden mapping
    public $whyHostThisDescription = "";
    public $expectedAttendance = "";
    public $commaDelimitedTagNames = "";
    public $customAttendLink = null;
    public $isOnlineEvent = false;
    public $onlineLocationLabel = "";
    public $onlineLocationLink = null;
    public $suppressCharitySelectionForm = false;
    public $suppressPrintFlier = false;

    public $templateTagNames;
    public $calendarIconPath;
    public $calendarIconFile;
    /* @var $venue Venue */
	public $venue;
    /* @var $cause Cause */
	public $cause;


	public function __construct(){
		// it is tempting to initialize here, but we shouldn't because this is the data contract
        // and it limits the implementer's ability to compose the contract with appropriate subclasses
		//$this->venue = new Venue();
	}

    public function getCalendarIconUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getLargeImageUrl($this->calendarIconPath, $this->calendarIconFile, $CurrentServer, $siteRoot);
    }

    public function getCampaignUrl($CurrentServer){
        $campaignName = "";
        if ($this->templateTagNames){
            $campaignNamesArray = explode(",", $this->templateTagNames);
            $campaignName = $campaignNamesArray[0];
        }
        if ($this->commaDelimitedTagNames){
            $campaignNamesArray = explode(",", $this->commaDelimitedTagNames);
            $campaignName = $campaignNamesArray[0];
        }
        if ($campaignName){
            return $CurrentServer."campaign/".str_replace(" ", "-", $campaignName);
        }
        return "";
    }

    public function isExpiredEvent(){
        $endDate = strtotime(date("Y-m-d", $this->endDate));
        $currentDate = strtotime(date("Y-m-d", time()));
        return $endDate < $currentDate;
    }

    public function getFormattedDescription(MergeTagSwapper $tagSwapper = null){
        $urlConverter = new UrlLinker();
        $formattedDescription = nl2br($urlConverter->htmlEscapeAndLinkUrls($this->description));
        if (strpos($formattedDescription, "|*") > 0 && $tagSwapper == null){
            $tagSwapper = new DefaultEventMergeTagSwapper();
        }
        if ($tagSwapper !== null){
            $formattedDescription = $tagSwapper->merge($formattedDescription, $this);
        }
        return $formattedDescription;
    }

    public function isFeaturedCause(){
        return isset($this->cause) && isset($this->cause->name) && strlen($this->cause->name) > 0;
    }

    public function isCustomerChoice(){
        return !(empty($this->venue->isFlatDonation) && empty($this->venue->customerChoicePercentage));
    }

    private static function getTimeFromString($dateTimeString){
        $parsedDate = date_parse($dateTimeString);
        if (checkdate($parsedDate['month'], $parsedDate['day'], $parsedDate['year'])){
            return strtotime($dateTimeString);
        }
        return null;
    }

    private static function getBestTime($preferredTimeString, $defaultTimeString){
        $parsedTime = date_parse($preferredTimeString);
        if (is_numeric($parsedTime['hour']) && is_numeric($parsedTime['minute'])){
            return $preferredTimeString;
        }
        return $defaultTimeString;
    }

    public function setStartDate($dateString, $timeString, $defaultTimeString = "10:00"){
        $this->startDate = static::getTimeFromString($dateString." ". static::getBestTime($timeString, $defaultTimeString));
    }

    public function setEndDate($dateString, $timeString, $defaultTimeString = "22:00"){
        $this->endDate = static::getTimeFromString($dateString." ". static::getBestTime($timeString, $defaultTimeString));
    }

    public function getStartDateString($format = "Y-m-d"){
        return date($format, $this->startDate);
    }

    public function getEndDateString($format = "Y-m-d"){
        return date($format, $this->endDate);
    }

    public function getStartTimeString($format = "H:i"){
        return date($format, $this->startDate);
    }

    public function getEndTimeString($format = "H:i"){
        return date($format, $this->endDate);
    }

    public function getCombinedDateStringForEmailTemplate(){
        $startDateString = date("F j", $this->startDate);
        $endDateString = date("F j", $this->endDate);
        if ($startDateString !== $endDateString){
            return " from ".$startDateString." through ".$endDateString;
        }
        return " on ".$startDateString;
    }

    public function getCombinedDateTimeString($makeShort = false){
        $dateTimeText = "";

        // construct the date part
        $startDateYear = date("Y", $this->startDate);
        $endDateYear = date("Y", $this->endDate);
        $startDateText = date("F j", $this->startDate);
        $endDateText = date("F j", $this->endDate);
        if ($startDateYear == $endDateYear){
            // most likely scenario
            if ($startDateText == $endDateText){
                $dateTimeText .= $startDateText.", ".$startDateYear;
                if ($makeShort){
                    $dateTimeText = date("n/j/Y", $this->startDate);
                }
            } else {
                $dateTimeText .= $startDateText."&ndash;".date("j, Y", $this->endDate);
                if ($makeShort){
                    $dateTimeText = date("n/j", $this->startDate)."-".date("n/j/Y", $this->endDate);
                }
            }
        } else {
            // super unlikely scenario
            $dateTimeText .= date("M j, Y", $this->startDate)." to ".date("M j, Y", $this->endDate);
            if ($makeShort){
                $dateTimeText = date("n/j/Y", $this->startDate)."-".date("n/j/Y", $this->endDate);
            }
        }

        // construct the time part
        if ($makeShort){
            $dateTimeText .= " (".date("g:ia", $this->startDate)."-".date("g:ia", $this->endDate).")";
        } else {
            $dateTimeText .= " from ".date("g:ia", $this->startDate)." &ndash; ".date("g:ia", $this->endDate);
        }
        return $dateTimeText;
    }

    public function canPrintCharitySelection(){
        return $this->isCausetownEvent && !$this->suppressCharitySelectionForm && time() < $this->endDate;
    }

    public function canPrintFlierSelection(){
        return $this->isCausetownEvent && !$this->suppressPrintFlier && time() < $this->endDate;
    }

    /**
     * Given a stdClass that used to be an EventRecord, cast it back (presumably because it was passed in as json)
     * @static
     * @param stdClass $object
     * @return \EventRecord
     */
    public static function castAsEventRecord(stdClass $object){
        // construct the core object
        $eventRecord = new EventRecord();
        $venue = new Venue();
        $cause = new Cause();
        $eventRecord->venue = $venue;
        $eventRecord->cause = $cause;

        // iterate over event
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $eventRecord->$key = $value;
            }
        }

        // iterate over venue
        if ($object->venue){
            $eventRecord->venue = Venue::castAsVenue($object->venue);
        }

        // iterate over cause
        if ($object->cause){
            foreach ($object->cause as $key => $value){
                $cause->$key = $value;
            }
        }
        return $eventRecord;
    }

    /**
     * Given an indexed document (created by convertToIndexedDocument, or retrieved from indexing service), convert
     * to an EventRecord.
     * @param $indexedDocument Array of arrays
     * @return EventRecord
     */
    public static function convertFromIndexedDocument($indexedDocument){
        $eventRecord = new EventRecord();
        $eventRecord->query_relevance_score = isset($indexedDocument->query_relevance_score) ? $indexedDocument->query_relevance_score : null;
        $venue = new Venue();
        $cause = new Cause();
        $eventRecord->venue = $venue;
        $eventRecord->cause = $cause;
        if (is_object($indexedDocument)){
            $eventRecord->docid = $indexedDocument->docid;
            $indexedFieldsObject = $indexedDocument;
            $tagNames = $indexedFieldsObject->category_Tags;
        } else {
            $eventRecord->docid = $indexedDocument['docid'];
            $indexedFieldsObject = (object)$indexedDocument["fields"];
            $tagNames = $indexedDocument["categories"]["Tags"];
        }

        // TODO: if desired, code logic to automatically convert properties based on naming convention. For now, we just hard code them all
        // published events
        $eventRecord->id = $indexedFieldsObject->eventId;
        $eventRecord->title = $indexedFieldsObject->eventName;
        $eventRecord->description = $indexedFieldsObject->eventDescription;
        $eventRecord->createdDate = $indexedFieldsObject->createdDate;
        $eventRecord->lastUpdatedDate = $indexedFieldsObject->lastUpdatedDate;
        $eventRecord->setStartDate($indexedFieldsObject->startDateText, $indexedFieldsObject->startTimeText);
        $eventRecord->setEndDate($indexedFieldsObject->endDateText, $indexedFieldsObject->endTimeText);
        $eventRecord->commaDelimitedTagNames = $tagNames;
        $eventRecord->isNationalEvent = $indexedFieldsObject->isNationalEvent;
        $eventRecord->isOnlineEvent = $indexedFieldsObject->isOnlineEvent;
        $eventRecord->onlineLocationLabel = $indexedFieldsObject->onlineLocationLabel;
        $eventRecord->onlineLocationLink = $indexedFieldsObject->onlineLocationLink;
        $eventRecord->isIndexedAsCC = $indexedFieldsObject->isCustomerChoice;
        $eventRecord->suppressCharitySelectionForm = $indexedFieldsObject->suppressCharitySelectionForm;
        $eventRecord->suppressPrintFlier = $indexedFieldsObject->suppressPrintFlier;
        $eventRecord->calendarIconFile = $indexedFieldsObject->calendarIconFile;
        $eventRecord->calendarIconPath = $indexedFieldsObject->calendarIconPath;
        $eventRecord->templateTagNames = $indexedFieldsObject->templateTagNames;

        $venue->id = $indexedFieldsObject->venueId;
        $venue->pid = $indexedFieldsObject->venuePid;
        $venue->name = $indexedFieldsObject->venueName;
        $venue->streetAddr = $indexedFieldsObject->venueStreetAddr;
        $venue->city = $indexedFieldsObject->venueCity;
        $venue->region = $indexedFieldsObject->venueRegion;
        $venue->postalCode = $indexedFieldsObject->venuePostalCode;
        $venue->country = $indexedFieldsObject->venueCountry;
        $venue->fipsCounty = $indexedFieldsObject->venueFipsCounty;
        $venue->url = $indexedFieldsObject->venueUrl;
        $venue->phone = $indexedFieldsObject->venuePhone;
        $venue->featuredCausePercentage = $indexedFieldsObject->venueFeaturedCausePercentage;
        $venue->customerChoicePercentage = $indexedFieldsObject->venueCustomerChoicePercentage;
        $venue->lat = $indexedFieldsObject->venueLatitude;
        $venue->long = $indexedFieldsObject->venueLongitude;
        $venue->isFlatDonation = $indexedFieldsObject->venueIsFlatDonation;
        $venue->donationPerUnit = $indexedFieldsObject->venueDonationPerUnit;
        $venue->unitDescriptor = $indexedFieldsObject->venueUnitDescriptor;
        $venue->flatDonationActionPhrase = $indexedFieldsObject->venueDonationActionPhrase;
        $venue->logoPath = $indexedFieldsObject->venueLogoPath;
        $venue->logoFile = $indexedFieldsObject->venueLogoFile;
        $venue->neighborhood = $indexedFieldsObject->venueNeighborhood;
        $venue->keywords = $indexedFieldsObject->venueKeywords;
        $venue->isFcOnly = $indexedFieldsObject->isFeaturedCauseOnly;

        $cause->id = $indexedFieldsObject->causeId;
        $cause->pid = $indexedFieldsObject->causePid;
        $cause->name = $indexedFieldsObject->causeName;
        $cause->url = $indexedFieldsObject->causeUrl;

        // unpublished events (we don't need these at this time)
        //$eventRecord->versionStatus = $indexedFieldsObject->versionStatus;
        //$eventRecord->expectedAttendance = $indexedFieldsObject->expectedAttendance;

        return $eventRecord;
    }

	//See also convertToRetrievedIndexedDocument for documents retrieved from index
	//The following to to mimic what a Document structure ready to being added to index looks like
    public function convertToIndexedDocument(){
        // cache reused values
        $id = $this->id;
        $venueId = $this->venue->id;
        $venueName = $this->venue->name;
        $venueCity = $this->venue->city;
        $venueRegion = $this->venue->region;
        $title = $this->title;
        $description = htmlspecialchars_decode($this->description);
        $causeName = $this->cause->name;
        $keywords = $this->venue->keywords;
        $neighborhoods = $this->venue->neighborhood;
        $isCustomerChoice = $this->venue->isFlatDonation || $this->venue->customerChoicePercentage ? 1 : 0;

        // set defaults
        $lat = (is_numeric($this->venue->lat)) ? $this->venue->lat : 0;
        $long = (is_numeric($this->venue->long)) ? $this->venue->long : 0;

        //Normalize State
        $searchableRegion = $venueRegion;
        if (function_exists('StateAbbrToFull')){
            if (strlen($venueRegion)==2){
                $searchableRegion = $venueRegion." ".StateAbbrToFull($venueRegion);
            }else{
                $searchableRegion = $venueRegion." ".BillComStateFix($venueRegion);
            }
        }

        // searchable text field
        $textField = "Event ".$title." ".$description." ".$venueName." ".$venueCity." ".$searchableRegion." ".$causeName." ".$keywords." ".$neighborhoods;
		$fieldsArray = array(
			"text" => $textField,
			"eventId" => $id,
			"eventName" => $this->title,
			"eventDescription" => $description,
			"createdDate" => $this->createdDate,
			"lastUpdatedDate" => $this->lastUpdatedDate,
			"startDateText" => date("Y-m-d", $this->startDate),
			"startTimeText" => date("H:i", $this->startDate),
			"endDateText" => date("Y-m-d", $this->endDate),
			"endTimeText" => date("H:i", $this->endDate),
            "isNationalEvent" => $this->isNationalEvent,
            "isOnlineEvent" => $this->isOnlineEvent ? 1 : 0,
            "onlineLocationLabel" => $this->onlineLocationLabel,
            "onlineLocationLink" => $this->onlineLocationLink,
            "suppressCharitySelectionForm" => $this->suppressCharitySelectionForm ? 1 : 0,
            "suppressPrintFlier" => $this->suppressPrintFlier ? 1 : 0,
            "isCustomerChoice" => $isCustomerChoice,
            "isFeaturedCauseOnly" => $this->venue->isFcOnly,
            "calendarIconFile" => $this->calendarIconFile."",
            "calendarIconPath" => $this->calendarIconPath."",
            "templateTagNames" => $this->templateTagNames."",
			"venueId" => $this->venue->id,
			"venuePid" => $this->venue->pid,
			"venueName" => $this->venue->name,
			"venueStreetAddr" => $this->venue->streetAddr,
			"venueCity" => $this->venue->city,
			"venueRegion" => $this->venue->region,
			"venuePostalCode" => $this->venue->postalCode,
			"venueCountry" => $this->venue->country,
			"venueFipsCounty" => $this->venue->fipsCounty,
			"venueUrl" => $this->venue->url,
			"venuePhone" => $this->venue->phone,
			"venueFeaturedCausePercentage" => $this->venue->featuredCausePercentage,
			"venueCustomerChoicePercentage" => $this->venue->customerChoicePercentage,
			"venueLatitude" => $lat,
			"venueLongitude" => $long,
            "venueIsFlatDonation" => $this->venue->isFlatDonation,
            "venueDonationPerUnit" => $this->venue->donationPerUnit,
            "venueUnitDescriptor" => $this->venue->unitDescriptor,
            "venueDonationActionPhrase" => $this->venue->flatDonationActionPhrase,
            "venueLogoPath" => $this->venue->logoPath,
            "venueLogoFile" => $this->venue->logoFile,
            "venueNeighborhood" => $this->venue->neighborhood,
            "venueKeywords" => $this->venue->keywords,
			"causeId" => $this->cause->id ? $this->cause->id : "",
			"causePid" => $this->cause->pid ? $this->cause->pid : "",
			"causeName" => $this->cause->name ? $this->cause->name : "",
			"causeUrl" => $this->cause->url ? $this->cause->url : "",

			// read only values
			"venueFipsStateAndCounty" => $this->venue->getFipsStateAndCounty(),
            "category_Tags" => $this->commaDelimitedTagNames

			// unpublished events, not relevant unless we intend to use indexing for unpublished events
			//"versionStatus" => $this->versionStatus,
			//"expectedAttendance" => $this->expectedAttendance,

			// irrelevant for indexing, right? if relevant, should be added to the text field since we don't use for display or rss
			//"venueDescription" => $this->venue->description,
			//"venueFacebookEventId" => $this->venue->facebookEventID,
			//"venueEventbriteEventId" => $this->venue->eventBriteEventID,
			//"venueEventbriteVenueId" => $this->venue->eventBriteVenueID,
			//"causeEin" => $this->cause->ein // in this case, we should include the cause EIN as well as the parent EIN as searchable if that is desired
		);
		
		$SearchableTagsArray = explode(",",$this->commaDelimitedTagNames);
		$SearchableTags = "TAG".str_replace(",","TAG TAG",$this->commaDelimitedTagNames)."TAG";
		if (count($SearchableTagsArray)){
			$tagArray["TAG_LIST"] = $SearchableTags;
			$fieldsArray = array_merge($fieldsArray, $tagArray);			
		}
		/*
		if (count($SearchableTagsArray)){
			foreach($SearchableTagsArray as $TagValue){
				$tagArray["TAG_".$TagValue] = $TagValue;
			}
			$fieldsArray = array_merge($fieldsArray, $tagArray);			
		}
		*/

		$variablesArray = array(
			0 => $lat,
			1 => $long,
			2 => $this->startDate,
			3 => $this->endDate,
			4 => $venueId,
            5 => $this->isNationalEvent,
            6 => strtotime(date("Y-m-d", $this->endDate)), // the date of the end date only,
            7 => $this->isOnlineEvent
        );
		$categoriesArray = array(
			"Tags" => $this->commaDelimitedTagNames,
			"City" => $this->venue->city,
			"State" => $this->venue->region
        );
			
		$indexDocument = array(
            "docid" => $id."_".$venueId,
			"fields" => $fieldsArray,
			"variables" => $variablesArray,
			"categories" => $categoriesArray
		);
        return $indexDocument;
    }

    function convertToSocialEventRecord(EventRecord $target){

        if (!isset($target)){
            $target = new EventRecord();
        }

        $target->id = $this->id;
        $target->title = $this->title;
        $target->description = $this->getEmbellishedDescription();
        $target->startDate = $this->startDate;
        $target->endDate = $this->endDate;
        $target->commaDelimitedTagNames = $this->commaDelimitedTagNames;
        $target->venue = $this->venue;
        $target->cause = $this->cause;
        return $target;
    }

    public function getOfferPhraseForRSS(){
        $venue = $this->venue;
        $cause = $this->cause;
        $featuredCausePercentage = $venue->featuredCausePercentage;
        $customerChoicePercentage = $venue->customerChoicePercentage;
        $isFlatDonation = $venue->isFlatDonation;
        $donationPerUnit = $venue->donationPerUnit;
        $donationActionPhrase = $venue->flatDonationActionPhrase;
        $isCC = $customerChoicePercentage || $isFlatDonation;

        $phrase = "";
        $customerChoicePhrase = "";
        if ($isFlatDonation){
            $phrase .= "$".$donationPerUnit." ".$donationActionPhrase;
        } else if ($featuredCausePercentage){
            $phrase .= $featuredCausePercentage."% of your purchase";
            if ($customerChoicePercentage && $customerChoicePercentage != $featuredCausePercentage){
                $customerChoicePhrase .= $customerChoicePercentage."% of your purchase";
            }
        } else if ($customerChoicePercentage){
            $phrase .= $customerChoicePercentage."% of your purchase";
        }

        $phrase .= " at ".$venue->name." will be donated to ";
        if ($cause->name){
            $phrase .= $cause->name;
            if ($isCC && !$venue->isFcOnly){
                if ($customerChoicePhrase){
                    // different percentage to CC
                    $phrase .= " or ".$customerChoicePhrase." to any other charity in the country that you choose!";
                } else {
                    $phrase .= " or any other charity in the country that you choose!";
                }
            } else {
                $phrase .= "!";
            }
        } else {
            $phrase .= "any charity in the country that you choose!";
        }

        return trim($phrase);
    }

    public function getOfferSentence($includeVenueName = true){
        $venue = $this->venue;
        $cause = $this->cause;

        $sentence = ($includeVenueName) ? $venue->name : "";
        $featuredCausePercentage = $venue->featuredCausePercentage;
        $customerChoicePercentage = $venue->customerChoicePercentage;
        $isFlatDonation = $venue->isFlatDonation;
        $donationPerUnit = $venue->donationPerUnit;
        $donationActionPhrase = $venue->flatDonationActionPhrase;

        if ($featuredCausePercentage || $customerChoicePercentage || $isFlatDonation){
            $sentence .= " will donate ";
        }

        if ($isFlatDonation){
            if (isset($cause->name)){
                $sentence .= "$".$donationPerUnit." to ".$cause->name;
                if (!$venue->isFcOnly){
                    $sentence .= " or any charity you choose ".$donationActionPhrase;
                }
            } else {
                $sentence .= "$".$donationPerUnit." to your favorite cause ".$donationActionPhrase;
            }
        } else {
            // percentages

            if (isset($cause->name)){
                if ($featuredCausePercentage){
                    $sentence .= $featuredCausePercentage."% of your purchase to ".$cause->name;
                }
                if ($customerChoicePercentage){
                    $sentence .= " or ";
                }
            }
            if ($customerChoicePercentage){
                if ($customerChoicePercentage != $featuredCausePercentage){
                    $sentence .= $customerChoicePercentage."% of your purchase ";
                }
                $sentence .= " to your favorite cause";
            }
        }

        //Add a period to the end of the Description if left off
        if (substr(trim($sentence),-1)!='.'){
            $sentence .= ".";
        }
        return $sentence;
    }

    /**
     * This returns the RAW description text with just the tag swapping done. NO LINE BREAKS, URL CONVERSIONS.
     * @param MergeTagSwapper $tagSwapper
     * @return string
     */
    public function getFacebookDescription(MergeTagSwapper $tagSwapper = null){
        $description = $this->description;
        if (strpos($description, "|*") > 0 && $tagSwapper == null){
            $tagSwapper = new DefaultEventMergeTagSwapper();
        }
        if ($tagSwapper !== null){
            $description = $tagSwapper->merge($description, $this);
        }
        return $description;
    }

    // TODO: refactor the social event record stuff into its own object so it's clear which description is which
    public function getEmbellishedDescription(MergeTagSwapper $tagSwapper = null){
        $description = trim($this->description);
        if (strpos($description, "|*") > 0 && $tagSwapper == null){
            $tagSwapper = new DefaultEventMergeTagSwapper();
        }
        if ($tagSwapper !== null){
            $description = $tagSwapper->merge($description, $this);
        }
        return $description;
    }

    /**
     * Example: $1.00 for each item
     * Example: 15% of your purchase
     * @return string
     */
    public function getOfferText(){
        if (!empty($this->venue->isFlatDonation)){
            return "$".$this->venue->donationPerUnit." ".$this->venue->flatDonationActionPhrase;
        }
        if (!empty($this->cause->name) && !empty($this->venue->featuredCausePercentage)){
            return $this->venue->featuredCausePercentage."% of your purchase";
        }
        if (!empty($this->venue->customerChoicePercentage)){
            return $this->venue->customerChoicePercentage."% of your purchase";
        }
        return "";
    }

    /**
     * Example: $1.00 for each item
     * Example: 15% of your purchase to Your Charity
     * Example: 15% of your purchase to Your Charity or 20% to any charity you choose
     * Example: 15% to any charity you choose
     * @return string
     */
    public function getFullOfferText(){
        if (!empty($this->venue->isFlatDonation)){
            return "$".$this->venue->donationPerUnit." ".$this->venue->flatDonationActionPhrase;
        }
        $workingText = "";
        $causeName = "";
        if (!empty($this->cause->name) && !empty($this->venue->featuredCausePercentage)){
            $workingText = $this->venue->featuredCausePercentage."% of your purchase";
            $causeName = $this->cause->name;
        }
        if (!empty($this->venue->customerChoicePercentage) && $this->venue->customerChoicePercentage != $this->venue->featuredCausePercentage){
            $workingText .= ($causeName) ? " to ".$causeName." or " : "";
            $workingText .= $this->venue->customerChoicePercentage."% of your purchase";
            $workingText .= ($causeName) ? " to any charity you choose": "";
        }
        return $workingText;
    }

    /**
     * Returns a brief description of the offer. Includes a Featured Cause Name ONLY if it is a Featured Cause Only event,
     * or if the CustomerChoice percentage amount is different from the Featured Cause Percentage amount.
     * @return string
     */
    public function getFullOfferTextForBusinessList(){
        $workingText = "";
        $causeName = $this->cause->name;

        if ($this->venue->isFlatDonation) {
            // flat donation
            $workingText = "$".$this->venue->donationPerUnit." ".$this->venue->flatDonationActionPhrase;
            if (!empty($causeName) && $this->venue->isFcOnly){
                // only add cause name if it is a featured cause only event
                $workingText .= " to ".$causeName;
            }
            return $workingText;
        } else {
            // percentage
            // if there's a featured cause AND there's a featured cause percentage, add the percentage amount.
            // cause name will be added ONLY if it is featured cause ONLY, OR if the CC percentage is different
            if (!empty($causeName) && !empty($this->venue->featuredCausePercentage)){
                $workingText = $this->venue->featuredCausePercentage."% of your purchase";
                if (!empty($this->venue->customerChoicePercentage) && $this->venue->customerChoicePercentage != $this->venue->featuredCausePercentage){
                    // cc percentage is different
                    $workingText .= " to ".$causeName." or ".$this->venue->customerChoicePercentage."% of your purchase to any charity you choose";
                } else if (empty($this->venue->customerChoicePercentage)){
                    // is fc only
                    $workingText .= " to ".$causeName;
                }
            } else {
                $workingText .= $this->venue->customerChoicePercentage."% of your purchase";
            }
        }
        return $workingText;
    }

    public function getFullOfferTextForMapBubble(){
        $workingText = "";
        $causeName = $this->cause->name;

        if ($this->venue->isFlatDonation) {
            // flat donation
            $workingText = "$".$this->venue->donationPerUnit." ".$this->venue->flatDonationActionPhrase;
            if (!empty($causeName)){
                // FC
                $workingText .= " to ".$causeName;
                if (!$this->venue->isFcOnly){
                    // also CC
                    $workingText .= " or to Any Charity You Choose";
                }
            } else {
                // CC only
                $workingText .= " to Any Charity You Choose";
            }
            return $workingText;
        } else {
            // percentage
            if (!empty($causeName) && !empty($this->venue->featuredCausePercentage)){
                // FC
                $workingText = $this->venue->featuredCausePercentage."% of your purchase to ".$causeName;
                if (!empty($this->venue->customerChoicePercentage)){
                    // Also CC
                    if ($this->venue->customerChoicePercentage != $this->venue->featuredCausePercentage){
                        // cc percentage is different
                        $workingText .= " or ".$this->venue->customerChoicePercentage."% of your purchase to Any Charity You Choose";
                    } else {
                        $workingText .= " or to Any Charity You Choose";
                    }
                }
            } else {
                // CC only
                $workingText .= $this->venue->customerChoicePercentage."% of your purchase to Any Charity You Choose";
            }
        }
        return $workingText;
    }

    public function getFullOfferTextForEmailTemplate(){
        $workingText = "";
        $causeName = $this->cause->name;

        if ($this->venue->isFlatDonation) {
            // flat donation
            $workingText = "$".$this->venue->donationPerUnit." ".$this->venue->flatDonationActionPhrase;
            if (!empty($causeName)){
                // FC
                $workingText .= " to ".$causeName;
                if (!$this->venue->isFcOnly){
                    // also CC
                    $workingText .= " or to any charity customers choose";
                }
            } else {
                // CC only
                $workingText .= " to to any charity customers choose";
            }
            return $workingText;
        } else {
            // percentage
            if (!empty($causeName) && !empty($this->venue->featuredCausePercentage)){
                // FC
                $workingText = $this->venue->featuredCausePercentage."% of sales to ".$causeName;
                if (!empty($this->venue->customerChoicePercentage)){
                    // Also CC
                    if ($this->venue->customerChoicePercentage != $this->venue->featuredCausePercentage){
                        // cc percentage is different
                        $workingText .= " or ".$this->venue->customerChoicePercentage."% of sales to any charity customers choose";
                    } else {
                        $workingText .= " or to any charity customers choose";
                    }
                }
            } else {
                // CC only
                $workingText .= $this->venue->customerChoicePercentage."% of sales to any charity customers choose";
            }
        }
        return $workingText;
    }

    public function getWhereOrHowStatement(){
        if ($this->isNationalEvent || $this->isOnlineEvent){

        } else {
            return $this->venue->getSingleLineAddress();
        }
    }

    public static function getCustomerChoiceRestrictions(EventRecord $record = null){

        $earliestTime = time()+3600; // at least one hour ahead
        $earliestTimeRounded = roundUpToHalfHour($earliestTime);

        // add selection restrictions if any
        $restrictions = array();

        // set date edit restrictions
        $startTimeStamp = $earliestTimeRounded;
        $restrictions["timeRangeLimit"] = new stdClass();
        $restrictions["timeRangeLimit"]->minValue = date("m/d/Y H:i", $startTimeStamp);

        $restrictions["featuredCausePercentage"] = new stdClass();
        $restrictions["featuredCausePercentage"]->isEditable = false;
        $restrictions["featuredCause"]->isEditable = true; // merchant can select a cause

        // customer choice percentage
        $restrictions["customerChoicePercentage"] = new stdClass();
        $restrictions["customerChoicePercentage"]->isEditable = true;
        $restrictions["customerChoicePercentage"]->minValue = 1;

        // flat donation
        $restrictions["flatDonation"] = new stdClass();
        $restrictions["flatDonation"]->isEditable = SessionManager::isPlanSubscriber();

        // private description to local business
        $restrictions["whyHostThisDescription"] = new stdClass();
        $restrictions["whyHostThisDescription"]->isEditable = false;

        // expected attendance
        $restrictions["expectedAttendance"] = new stdClass();
        $restrictions["expectedAttendance"]->isEditable = false;

        if ($record != null){
            // override if event record is passed in and it is an edit (as opposed to create)
            if ($record->id && !$record->isDraft){
                $restrictions["featuredCause"]->isEditable = false;
            }

            // override if event record is passed in and both venue and cause are on it
            if ($record->venue->id && $record->cause->id){
                $restrictions["featuredCausePercentage"]->isEditable = true;
                $restrictions["featuredCausePercentage"]->minValue = 1;
                $restrictions["customerChoicePercentage"]->minValue = 0;
            }
        }
        return $restrictions;
    }
}

/**
 * This is the venue data contract
 *
 */
class Venue {
	public $id = "";
    public $pid = "";
	public $location = "";
	public $lat = "";
	public $long = "";
	public $name = "";
	public $streetAddr = "";
	public $city = "";
	public $region = "";
	public $postalCode = "";
	public $country = "";
	public $url = "";
	public $phone = "";
	public $featuredCausePercentage = "";
	public $customerChoicePercentage = "";
    public $isFlatDonation = "";
    public $isFcOnly = false; // override for flat donation
    public $donationPerUnit = "";
    public $unitDescriptor = "";
    public $flatDonationActionPhrase = "";
    public $fipsCounty = "";
    public $description; // TODO: determine if this is really being used anywhere
    public $logoPath;
    public $logoFile;
    public $eventTemplatePremiumId;
    public $keywords;
    public $neighborhood;

	/* social ids */
	public $facebookEventID;
	public $eventBriteEventID;
    public $eventBriteVenueID;
    public $publicProfileLink;

	public function setURL($url){
		if (substr($url,0,4) == "http"){
			$this->url = $url;
		} else if (strlen($url)){
			$this->url = "http://".$url;
		}
		return $this->url;
	}

    private function stripJoin($array, $delimiter){
        $newString = '';
        foreach($array as $field){
            if (strlen($newString) && strlen($field)){
                $newString .= $delimiter;
            }
            $newString .= $field;
        }
        return $newString;
    }

	public function getSingleLineAddress(){
		$addressFields = array($this->streetAddr, $this->city, $this->region, $this->postalCode);
		$address = $this->stripJoin($addressFields, ", ");
		return trim($address);
	}

    public function getMultiLineAddress(){
        $addressLines = array();
        $addressLines[] = $this->streetAddr;
        $addressLines[] = trim($this->stripJoin(array($this->city, $this->region), ", ")." ".$this->postalCode);
        return $addressLines;
    }

    public function getFipsStateAndCounty(){
        $fipsValue = $this->region.$this->fipsCounty;
        return (strlen($fipsValue) == 5) ? $fipsValue : "";
    }

    public function getLogoImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getSmallImageUrl($this->logoPath, $this->logoFile, $CurrentServer, $siteRoot);
    }

    public function getLargeLogoImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getLargeImageUrl($this->logoPath, $this->logoFile, $CurrentServer, $siteRoot);
    }

    public static function castAsVenue(stdClass $object){
        // construct the core object
        $venue = new Venue();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $venue->$key = $value;
            }
        }
        return $venue;
    }
}

/**
 * This is the cause data contract
 */
class Cause {
	public $id;
    public $pid;
	public $name;
	public $ein;
	public $url;

	public function setURL($url){
		if (substr($url,0,4) == "http"){
			$this->url = $url;
		} else if (strlen($url)){
			$this->url = "http://".$url;
		}
		return $this->url;
	}
}


class MerchantImageCriteria extends PaginationCriteria{
    public $merchantId;
    public $catalogId;
    /* @var bool true means limit to visible only, otherwise, nothing */
    public $isVisible;
    /* @var bool true means include merchant metadata */
    public $includeMerchantMetadata;

    /* @var bool true means randomize the results */
    public $randomize;
}

class MediaCriteria extends PaginationCriteria{
    public $isImage = false;

    public function __construct(PaginationCriteria $criteria = null){
        if ($criteria){
            foreach (get_object_vars($criteria) as $prop => $value){
                $this->$prop = $value;
            }
        }
    }
}

class MediaRecord {
    const ACTIVE_STATUS = "ACTIVE";
    const PENDING_STATUS = "PENDING";
    const DELETED_STATUS = "DELETED";

    public $id;
    public $fileName;
    public $folderUrl;
    public $fileType;
    public $fileSize;
    public $error;
    public $title;
    public $description;
    public $keywords;
    public $status;
    public $createdDate;
    public $createdBy;
    public $updatedDate;
    public $updatedBy;
}

class ImageRecord extends MediaRecord{
    public $width;
    public $height;
}

class MerchantImageRecord{
    const APPROVED_STATUS = "APPROVED";
    const PENDING_STATUS = "PENDING";
    const DELETED_STATUS = "DELETED";
    const DENIED_STATUS = "DENIED";

    static $APPROVAL_STATUSES = array(
        self::PENDING_STATUS,
        self::APPROVED_STATUS,
        self::DENIED_STATUS
    );

    /* @var ImageRecord */
    public $imageRecord; // image and its metadata
    public $id;
    public $sortOrder;

    public $imageCaption; // customizable caption

    public $priceRangeLow; // low price in range, or single price if only one price
    public $priceRangeHigh;
    public $isOnePrice; // indicator that it is not a range

    public $descriptionMarkDownSource;
    public $description;

    public $status = self::PENDING_STATUS; // pending, approved, denied, deleted
    public $externalLinkUrl;
    public $expirationDate;

    public $merchantId;
    public $merchantName;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $website;

    public $createdByMemberId;
    public $createdDate;
    public $updatedByMemberId;
    public $updatedDate;

    public function getCaptionDisplay(){
        if ($this->imageCaption && strlen($this->imageCaption)){
            return $this->imageCaption;
        }
        $priceDisplay = $this->getPriceDisplay();
        $caption = $this->imageRecord->title;
        if ($priceDisplay){
            $caption .= " (".$priceDisplay.")";
        }
        return $caption;
    }

    public function getPriceDisplay(){
        $priceDisplay = "";
        if ($this->isOnePrice){
            if ($this->priceRangeLow > 0){
                $priceDisplay .= "$".$this->priceRangeLow;
            }
        } else {
            if ($this->priceRangeLow > 0 && $this->priceRangeHigh > 0){
                $priceDisplay .= "$".$this->priceRangeLow. " &ndash; $".$this->priceRangeHigh;
            } else if ($this->priceRangeLow > 0){
                $priceDisplay .= "starting at $".$this->priceRangeLow;
            } else if ($this->priceRangeHigh > 0){
                $priceDisplay .= "up to $".$this->priceRangeHigh;
            } else {
                // no price range and no single price?
            }
        }
        return $priceDisplay;
    }

    private function stripJoin($array, $delimiter){
        $newString = '';
        foreach($array as $field){
            if (strlen($newString) && strlen($field)){
                $newString .= $delimiter;
            }
            $newString .= $field;
        }
        return $newString;
    }

    public function getSingleLineAddress(){
        $addressFields = array($this->address, $this->city, $this->state, $this->zip);
        $address = $this->stripJoin($addressFields, ", ");
        return trim($address);
    }

    public function setWebsite($url){
        if (substr($url,0,4) == "http"){
            $this->website = $url;
        } else if (strlen($url)){
            $this->website = "http://".$url;
        }
        return $this->website;
    }

    public function setExternalLinkUrl($url){
        if (substr($url,0,4) == "http"){
            $this->externalLinkUrl = $url;
        } else if (strlen($url)){
            $this->externalLinkUrl = "http://".$url;
        }
        return $this->externalLinkUrl;
    }

    public function setSinglePrice($priceValue){
        $this->priceRangeLow = $priceValue;
        $this->isOnePrice = true;
    }

    public function setPriceRange($lowValue, $highValue){
        $this->priceRangeLow = $lowValue;
        $this->priceRangeHigh = $highValue;
        $this->isOnePrice = false;
    }

    public function setDescription($markdown, $html){
        $this->descriptionMarkDownSource = $markdown;
        $this->description = $html;
    }
	
    public function isVisible(){
        return $this->status === self::APPROVED_STATUS && (!strlen($this->expirationDate) || strtotime($this->expirationDate) > time());
    }

    public function getImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getSmallImageUrl($this->imageRecord->folderUrl, $this->imageRecord->fileName, $CurrentServer, $siteRoot);
    }

    public function getLargeImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getLargeImageUrl($this->imageRecord->folderUrl, $this->imageRecord->fileName, $CurrentServer, $siteRoot);
    }
}

class MerchantCatalogCriteria extends PaginationCriteria{
    public $name;
    public $isVisible;
}

class MerchantCatalogRecord{
    const ACTIVE_STATUS = "ACTIVE";
    const DELETED_STATUS = "DELETED";
    public $id;
    public $name;
    public $title;
    public $subtitle;
    public $description;
    public $numMerchants;
    public $status = self::ACTIVE_STATUS;
    /* @var array of MerchantCatalogMerchantRecord */
    public $merchantArray;
}

class MerchantCatalogMerchantRecord{
    public $id;
    public $catalogId;
    public $merchantId;

    public $name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $pid;

    public $numImages;

    public $logoFile;
    public $logoPath;

    public function getLogoImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getSmallImageUrl($this->logoPath, $this->logoFile, $CurrentServer, $siteRoot);
    }
}

class EventCriteria extends PaginationCriteria{
    /**
     * Set to true to retrieve only upcoming events
     * @var bool
     */
    public $upcomingOnly;

    /**
     * Set to true to retrieve only published events (as opposed to nonprofit proposed events that are not hosted)
     * @var bool
     */
    public $publishedOnly = true;

    /**
     * Set to 1 to filter to online only, -1 to filter on NOT online
     * @var int
     */
    public $onlineOnly = 0;

    /**
     * Set to event ID to retrieve only records for the specified event
     * @var int
     */
    public $eventId;

    /**
     * Set to merchant ID to retrieve only records for the specified merchant
     * @var int
     */
    public $merchantId;

    /**
     * Set to organization ID to retrieve only records for the specified organization
     * @var int
     */
    public $orgId;

    /**
     * The campaign name
     * @var string
     */
    public $queryTagName;

    /**
     * Is a merchant event
     * @var bool
     */
    public $isMerchantEvent;

    public function __get($name){
        if ($name == "tagName"){
            return $this->queryTagName;
        }
        return $this->$name;
    }

    public function __set($name, $value){
        if ($name == "tagName"){
            $this->queryTagName = TagRecord::getQueryName($value);
        } else {
            $this->$name = $value;
        }
    }
}

if (!isset($Config_IndexDistanceFromEvent)){
    $Config_IndexDistanceFromEvent = 100;
}

class EventByLocationCriteria extends EventCriteria {

    public function __construct(){
        global $Config_IndexDistanceFromEvent;
        parent::__construct();
        $this->maxRadiusMiles = $Config_IndexDistanceFromEvent;
    }

    /**
     * If this and userLong are set, then records will be limited to a specified distance from this point
     * @var float
     */
    public $userLat;

    /**
     * If this and userLat are set, then records will be limited to a specified distance from this point
     * @var
     */
    public $userLong;

    /**
     * If userLat and userLong are set, this indicates the maximum number of miles to limit the results. Zero means no limit.
     * @var int default set in config
     */
    public $maxRadiusMiles;

    public $limitByDistance = false;
}

class AdminEventCriteria extends EventCriteria{

    /**
     * The event template id from which this event was created
     * @var int
     */
    public $eventTemplateId;
}

class EventTemplatePremiumCriteria extends PaginationCriteria {
    public $templateID;
}

class EventTemplateCloneCriteria {

    public $record;
    public $clonePremiums;
}

abstract class EventTemplateBase{
    public $id;
    public $pageHeader;
    public $pageSubtitle;
    private $instructions;
    public $instructionsMarkdownSource;
    public $instructionsHtmlSource;
    public $name;
    public $url;
    public $eventName;
    public $description;
    private $startDateTime;
    private $startTimeString;
    private $endDateTime;
    private $endTimeString;
    private $percentage;
    public $calendarIconPath;
    public $calendarIconFile;
    public $createdDate;
    public $lastEditedDate;
    public $tags;
    public $tagNamesArray = array();
    public $tagsArray = array();
    public $tagReferences = array();
    public $premiums = array();

    public function __get($name){
        switch ($name){
            case "startDate" :
                return ($this->startDateTime) ? date("Y-m-d", $this->startDateTime) : NULL;
                break;
            case "startTime" :
                return ($this->startTimeString) ? $this->startTimeString : NULL;
                break;
            case "endDate" :
                return ($this->endDateTime) ? date("Y-m-d", $this->endDateTime) : NULL;
                break;
            case "endTime" :
                return ($this->endTimeString) ? $this->endTimeString : NULL;
                break;
            case "percentageID":
                return $this->percentage;
                break;
            default:
                return isset($this->$name) ? $this->$name : NULL;
        }
    }

    public function __set($name, $value){
        switch ($name){
            case "startDate" :
                $this->startDateTime = self::getTimeFromString($value);
                break;
            case "startTime" :
                $this->startTimeString = self::getBestTime($value);
                break;
            case "endDate" :
                $this->endDateTime = self::getTimeFromString($value);
                break;
            case "endTime" :
                $this->endTimeString = self::getBestTime($value);
                break;
            case "percentageID":
                $this->percentage = is_numeric($value) ? $value : 0;
                break;
            default:
                $this->$name = $value;
        }
    }

    private static function getTimeFromString($dateTimeString){
        $parsedDate = date_parse($dateTimeString);
        if (checkdate($parsedDate['month'], $parsedDate['day'], $parsedDate['year'])){
            return strtotime($dateTimeString);
        }
        return null;
    }

    private static function getBestTime($preferredTimeString){
        $parsedTime = date_parse($preferredTimeString);
        if (is_numeric($parsedTime['hour']) && is_numeric($parsedTime['minute'])){
            return $preferredTimeString;
        }
        return null;
    }

    public function getOldInstructions(){
        return $this->instructions;
    }

    public function getInstructionsHtml(){
        if ($this->instructionsHtmlSource){
            return $this->instructionsHtmlSource;
        }
        return nl2br($this->instructions);
    }

    public function getCalendarIconUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getLargeImageUrl($this->calendarIconPath, $this->calendarIconFile, $CurrentServer, $siteRoot);
    }
}

class EventTemplatePremium {
    public $id;
    public $eventTemplateId;
    public $name;
    public $price;
    public $sortOrder;
    public $displayTextMarkdownSource;
    public $displayText;
    public $isActive;
    public $isDefault;

    public function __construct($object){
        if ($object){
            $this->id = $object->id;
            $this->eventTemplateId = $object->eventTemplateId;
            $this->name = $object->name;
            $this->price = $object->price;
            $this->sortOrder = $object->sortOrder;
            $this->displayTextMarkdownSource = $object->displayTextMarkdownSource;
            $this->displayText = $object->displayText;
            $this->isActive = $object->status ? 1 : 0;
            $this->isDefault = $object->isDefault ? 1 : 0;
        }
    }

    public function isNew(){
        return !(is_numeric($this->id) && $this->id > 0);
    }
}

class EventTransactionRecord{
    private static $CODE_PREFIX_LENGTH = 5;
    private static $CODE_SUFFIX_LENGTH = 2;
    public static $CUSTOMER_CHOICE_TEXT = "Customer Choice";
    public static $CUSTOMER_CHOSE_ACTION = "Customer Selected";
    public static $CUSTOMER_REQUIRED = "Required for receipt";
    public static $ANONYMOUS_DONOR = "anonymous";
    public static $TRANSCODE_CHARS = "abcdefghjkmnpqrstuvwxyz0123456789";
    public $id = 0;
    private $privateTransCode;
    public $transCode;
    public $recipientText; // recipient name or "Customer Choice" or "Choice Not Found"
    public $recipientAction; // usually null or "Customer Selected"
    public $orgID = 0; // the Causetown registered nonprofit id
    public $orgEin; // ein of the recipient
    public $orgName;
    public $eventId = 0;
    public $eventDate;
    public $eventTitle;
    public $featuredCauseOrgId;
    public $eligibleAmount;
    public $percentageAmount;
    public $generatedAmount;
    public $isFlatDonation;
    public $flatDonationUnits;
    public $timestamp;
    public $merchantName;
    public $merchantId = 0;
    public $customer;
    public $customerId = 0;
    public $eventMatchId = 0;
    public $invoiceId = 0;
    public $transactionId = 0;
    // TODO: moved parsed phone and email into a request object
    public $parsedPhone;
    public $parsedEmail;
    public $verifiedMemberId = 0;
    public $isInPotOfMoney = false;
    public $ip;

    public function __construct($id, $privateTransCode){
        $this->id = $id;
        $this->privateTransCode = $privateTransCode;
        $this->transCode = str_replace("_",$id,$privateTransCode);
    }

    public function __get($name){
        switch ($name){
            case "InvoiceID" :
                return $this->invoiceId;
                break;
            case "TransactionID" :
                return $this->transactionId;
                break;
            default:
                return isset($this->$name) ? $this->$name : NULL;
        }
    }

    public function __set($name, $value){
        switch ($name){
            case "InvoiceID" :
                $this->invoiceId = $value;
                break;
            case "TransactionID" :
                $this->transactionId = $value;
                break;
            default:
                $this->$name = $value;
        }
    }

    public static function create($ip){
        $record = new EventTransactionRecord(0, "");
        $record->resetTranscode();
        $record->ip = $ip;
        return $record;
    }

    public function setId($id){
        $this->id = $id;
        $this->transCode = str_replace("_", $this->id, $this->privateTransCode);
    }

    private function resetTranscode(){
        $this->privateTransCode = substr(str_shuffle(self::$TRANSCODE_CHARS), 0, self::$CODE_PREFIX_LENGTH)."_".substr(str_shuffle(self::$TRANSCODE_CHARS), 0, self::$CODE_SUFFIX_LENGTH);
        $this->transCode = str_replace("_", $this->id, $this->privateTransCode);
    }

    public function getPrivateTranscode(){
        return $this->privateTransCode;
    }

    /**
     * Returns true if this is a featured cause, which means ProposedByOrgId needs to be in the object
     * build process.
     */
    public function isFeaturedCause(){
        return $this->orgID && $this->featuredCauseOrgId == $this->orgID;
    }

    public static function isCharityAssigned(EventTransactionRecord $record){
        return empty($record->orgID) || !is_numeric($record->orgID) || $record->orgID <= 0;
    }

    public static function isCustomerAssigned(EventTransactionRecord $record){
        if (is_numeric($record->customerId) && $record->customerId > 0){
            return true;
        } else if ($record->parsedEmail){
            return true;
        } else if ($record->parsedPhone){
            return true;
        }
        return false;
    }

    public function setCustomer($customerID, $customerString){
        if ($customerID){
            $this->customer = "ct".$customerID;
        } else {
            $this->customer = self::$ANONYMOUS_DONOR;
            //$this->customer = $customerString;
        }
        //if (substr($this->customer,0, strlen(self::$CUSTOMER_REQUIRED)) == self::$CUSTOMER_REQUIRED){
        //	$this->customer = self::$ANONYMOUS_DONOR;
        //}
    }

    public function donationsNotFor($orgId){
        $isForThisOrg = $this->orgID == $orgId;
        if ($isForThisOrg){
            return false; // matches on orgID
        }
        return true;
    }

    public function recipientNotAssigned(){
        return $this->recipientText == self::$CUSTOMER_CHOICE_TEXT &&
            !($this->recipientAction) &&
            !($this->orgEin) &&
            !($this->orgID);
    }

    public function getCustomerDisplayText(){
        $customerDisplay = "(non-registered customer)";
        if ($this->customerId){
            $customerDisplay = "ct".$this->customerId;
        } else if (strpos($this->customer, "anonymous") !== false){
            $customerDisplay = $this->customer;
        }
        return $customerDisplay;
    }

    public function getCharityDisplayText(){
        $orgDisplayName = $this->orgName;
        if (!($orgDisplayName)){
            $orgDisplayName = "undeclared Customer Choice";
        }
        return $orgDisplayName;
    }
}

class CampaignCriteria extends PaginationCriteria{
    public $hasEvents;
    private $campaignName;

    public function setCampaignName($name){
        $this->campaignName = str_replace("-", " ", $name);
    }

    public function getCampaignName(){
        return $this->campaignName;
    }
}

class CampaignRecord{
    public $id = 0;
    public $name;
    public $snippet;
    public $heading;
    public $subHeading;
    public $callToAction = "Join Kevin Bacon\nin giving back to your\ncommunity this holiday season!";
    private $description;
    public $descriptionMarkdownSource;
    public $descriptionHtmlSource;
    public $showMap;
    public $showBizList;
    public $notShowingBizListBlurb;
    public $imageFileName;
    public $imageUrl;
    public $url;
    public $eventUrlOverride;
    public $addWrap = 0;
    public $mapZoomLevel;
    public $mapCenterLat;
    public $mapCenterLong;
    public $posterImageUrl;
    public $posterUrl;

    public function getOldDescription(){
        return $this->description;
    }

    public function getDescriptionHtml(){
        if ($this->descriptionHtmlSource){
            return $this->descriptionHtmlSource;
        }
        return nl2br($this->description);
    }

    public function setDescription($markdown, $html){
        $this->descriptionMarkdownSource = $markdown;
        $this->descriptionHtmlSource = $html;
    }
	
    public function setImageUrl($serverTagImgPath){
        if ($this->imageFileName){
            $this->imageUrl = $serverTagImgPath.$this->imageFileName;
        }
    }

    public static function castAs(stdClass $object){
        // construct the core object
        $record = new CampaignRecord();

        // iterate over object
        foreach($object as $key => $value) {
            if (!is_object($value)){
                $record->$key = $value;
            }
        }
        return $record;
    }

    public static function convertSet(array $tagQueryResult, $serverTagImgPath){
        $collection = array();
        foreach ($tagQueryResult as $tagRecord){
            $collection[] = self::convert((object) $tagRecord, $serverTagImgPath);
        }
        return $collection;
    }

    public static function convert($tagRecord, $serverTagImgPath){
        $campaignRecord = new CampaignRecord();
        $campaignRecord->id = $tagRecord->TagList_ID;
        $campaignRecord->name = $tagRecord->TagList_Name;
        $campaignRecord->snippet = $tagRecord->TagList_Description;
        $campaignRecord->heading = $tagRecord->TagList_CampaignHeading;
        $campaignRecord->subHeading = $tagRecord->TagList_CampaignSubHeading;
        $campaignRecord->description = $tagRecord->TagList_CampaignDescription;
        $campaignRecord->notShowingBizListBlurb = $tagRecord->TagList_NotShowingBizListBlurb;
        $campaignRecord->showMap = $tagRecord->TagList_ShowMap;
        $campaignRecord->showBizList = $tagRecord->TagList_ShowBizList;
        $campaignRecord->imageFileName = $tagRecord->TagList_Image;
        $campaignRecord->url = $tagRecord->TagList_URL;
        $campaignRecord->eventUrlOverride = $tagRecord->TagList_EventURLOverride;
        $campaignRecord->addWrap = ($tagRecord->TagList_AddWrap) ? 1 : "";
        $campaignRecord->mapZoomLevel = $tagRecord->map_zoom_level;
        $campaignRecord->mapCenterLat = $tagRecord->map_center_lat;
        $campaignRecord->mapCenterLong = $tagRecord->map_center_long;

        $campaignRecord->setImageUrl($serverTagImgPath);
        return $campaignRecord;
    }
}




class TagRecord{
    public $TagList_ID = 0;
    public $TagList_Name =  "";
    public $TagList_Description = "";
    public $TagList_CampaignHeading = "";
    public $TagList_CampaignSubHeading = "";
    public $TagList_CampaignDescription = "";
    public $TagList_Image = "";
    public $TagList_URL = "";
    public $TagList_EventURLOverride = "";
    public $TagList_AddWrap = false;
    public $map_zoom_level;
    public $map_center_lat;
    public $map_center_long;

    public function getNormalObject(){
        return self::getNormalizedObject($this);
    }

    public static function getNormalizedObject($tagRecordLikeObject){
        $hack = new stdClass();
        $hack->ID = $tagRecordLikeObject->TagList_ID;
        $hack->Name = $tagRecordLikeObject->TagList_Name;
        $hack->Description = $tagRecordLikeObject->TagList_Description;
        $hack->CampaignHeading = $tagRecordLikeObject->TagList_CampaignHeading;
        $hack->CampaignSubHeading = $tagRecordLikeObject->TagList_CampaignSubHeading;
        $hack->CampaignDescription = $tagRecordLikeObject->TagList_CampaignDescription;
        $hack->Image = $tagRecordLikeObject->TagList_Image;
        $hack->URL = $tagRecordLikeObject->TagList_URL;
        $hack->EventURLOverride = $tagRecordLikeObject->TagList_EventURLOverride;
        $hack->AddWrap = ($tagRecordLikeObject->TagList_AddWrap) ? 1 : "";
        $hack->map_zoom_level = $tagRecordLikeObject->map_zoom_level;
        $hack->map_center_lat = $tagRecordLikeObject->map_center_lat;
        $hack->map_center_long = $tagRecordLikeObject->map_center_long;
        return $hack;
    }

    public static function getQueryName($tagName){
        return str_replace("-"," ",$tagName);
    }
}
class TagReference{
    public $id = 0;
    public $tagID = 0;
    public $sortOrder = 0;
    public $displayText = "";
    public $hoverText = "";
    public $url = "";
    public $targetID = "_blank";
    public $page = "";
    public $isActive = true;

    public function isNew(){
        return !(is_numeric($this->id) && $this->id > 0);
    }
}

interface ReportCriteria {
    // implementation specific
}

interface CachedReportRunner {
    /**
     * Returns true if the cache can be refreshed.
     * @abstract
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function canRun(ReportCriteria $criteria);

    /**
     * Attempts to refresh the cache for a report with the given parameters. Returns true if successful.
     * @abstract
     * @param ReportCriteria $criteria
     * @return boolean
     */
    public function run(ReportCriteria $criteria);

    /**
     * Returns data from the cache. This function does not ensure the cache is up to date. If it is desirable to update the cache,
     * the "run" function must be executed.
     * @abstract
     * @param ReportCriteria $criteria
     * @return mixed
     */
    public function get(ReportCriteria $criteria);
}

class DonationsSummaryRow{
    public $breakdownValue; // e.g., charity name, week name, month name, etc
    public $eligibleSales;
    public $donationsGenerated;
    public $isYearAggregate;
    public $isGrandTotal;
}

class ImageLibraryHelper{

    /**
     * Utility function to get a URL to an imagethat actually exists on the current server
     * @static
     * @param $folder_url
     * @param $file_name
     * @param $CurrentServer
     * @param $siteRoot
     * @return string
     */
    public static function getSmallImageUrl($folder_url, $file_name, $CurrentServer, $siteRoot){
        if ($folder_url && $file_name){
            $imageUrls = array(
                $folder_url."thumbnail/".$file_name,
                $folder_url.$file_name
            );
            if (strpos($folder_url, $CurrentServer) === false){
                // folder url points somewhere else; look on current server as well
                // making the assumption that all image library files are in the /media/ path
                $currentServerUrl = preg_replace("/(.*)(\/)(media\/.*)/", $CurrentServer."$3", $folder_url);
                $imageUrls[] = $currentServerUrl."thumbnail/".$file_name;
                $imageUrls[] = $currentServerUrl.$file_name;
            }
            foreach($imageUrls as $imageUrl){
                $filePath = str_replace($CurrentServer, $siteRoot, $imageUrl);
                if (file_exists($filePath)){
                    return $imageUrl;
                    break;
                }
            }
        }
        return "";
    }

    public static function getLargeImageUrl($folder_url, $file_name, $CurrentServer, $siteRoot){
        if ($folder_url && $file_name){
            $imageUrls = array(
                $folder_url.$file_name,
                $folder_url."thumbnail/".$file_name
            );
            if (strpos($folder_url, $CurrentServer) === false){
                // folder url points somewhere else; look on current server as well
                // making the assumption that all image library files are in the /media/ path
                $currentServerUrl = preg_replace("/(.*)(\/)(media\/.*)/", $CurrentServer."$3", $folder_url);
                $imageUrls[] = $currentServerUrl.$file_name;
                $imageUrls[] = $currentServerUrl."thumbnail/".$file_name;
            }
            foreach($imageUrls as $imageUrl){
                $filePath = str_replace($CurrentServer, $siteRoot, $imageUrl);
                if (file_exists($filePath)){
                    return $imageUrl;
                    break;
                }
            }
        }
        return "";
    }
}

abstract class SocialMessageSample {
    // type constants
    const TWEET_TYPE = "tweet";
    const SHARE_TYPE = "share";
    const EMAIL_TYPE = "email";
    // synonyms
    const FACEBOOK_POST_TYPE = self::SHARE_TYPE;
    const BLURB_TYPE = self::EMAIL_TYPE;
    const NEWSLETTER_TYPE = self::EMAIL_TYPE;

    // relevantEntity constants
    const EVENT_ENTITY = "event";
    const CAMPAIGN_ENTITY = "campaign";

    // status constnts
    const ACTIVE_STATUS = 'ACTIVE';
    const DRAFT_STATUS = 'DRAFT';
    const DELETED_STATUS = 'DELETED';

    const URL_PLACEHOLDER = "{{url}}";

    static $AVAILABLE_STATUSES = array(
        self::ACTIVE_STATUS,
        self::DRAFT_STATUS,
        self::DELETED_STATUS
    );

    public static function getAvailableStatuses(){
        return self::$AVAILABLE_STATUSES;
    }

    public static function getRelevantEntity($requestedEntity){
        $relevantEntity = self::EVENT_ENTITY; // default
        switch (strtolower($requestedEntity)){
            case self::EVENT_ENTITY:
                $relevantEntity = self::EVENT_ENTITY;
                break;
            case self::CAMPAIGN_ENTITY:
                $relevantEntity = self::CAMPAIGN_ENTITY;
                break;
        }
        return $relevantEntity;
    }

    /* @var int */
    public $id;
    /* @var string */
    public $type;
    /* @var string */
    public $message;
    /* @var string */
    public $relevantEntity;
    /* @var int */
    public $relevantEntityId = 0;
    /* @var string */
    public $status;
    /* @var int */
    public $sortOrder = 0;
    /* @var string */
    public $createdDateString;
    /* @var int */
    public $createdByAdminId;
    /* @var string */
    public $updatedDateString;
    /* @var int */
    public $updatedByAdminId;

    /**
     * Returns true if the SocialMessageSample passes data validation.
     * @return bool true if data pass validation
     */
    public abstract function isValid();

    protected function __construct($type, $message, $relevantEntity, $relevantEntityId = 0){
        $this->type = $type;
        $this->message = $message;
        $this->relevantEntity = $relevantEntity;
        $this->relevantEntityId = $relevantEntityId;
    }

    public function getMergedText($urlString){
        $urlReplacedString = preg_replace("/".self::URL_PLACEHOLDER."/", $urlString, $this->message);
        if ($this->type == self::EMAIL_TYPE){
            $urlConverter = new UrlLinker();
            return nl2br($urlConverter->htmlEscapeAndLinkUrls($urlReplacedString));
        }
        // TODO: possibly link urls using UrlLinker (for email type messages)
        return nl2br($urlReplacedString);
    }
}

class TweetSample extends SocialMessageSample {
    const URL_TEMPLATE = "{{url}}";
    static $MAX_LENGTH = 140;
    static $MAX_HTTPS_URL_LENGTH = 20; // may change

    /**
     * Returns true if the SocialMessageSample passes data validation.
     * @return bool true if data pass validation
     */
    public function isValid()
    {
        $tweetSize = self::calculateTweetSize($this->message);
        return $tweetSize <= self::$MAX_LENGTH;
    }

    public static function calculateTweetSize($string){
        // twitter automatically swaps out urls longer than 20 characters
        $remainderText = preg_replace("/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i", self::URL_TEMPLATE, $string);
        $numUrls = substr_count($remainderText, self::URL_TEMPLATE);
        if ($numUrls){
            $remainderText = str_replace(self::URL_TEMPLATE, "", $remainderText);
        }
        $maxUrlSize = $numUrls * self::$MAX_HTTPS_URL_LENGTH;
        return strlen($remainderText) + $maxUrlSize;
    }

    public static function build($message, $relevantEntity, $relevantEntityId = 0)
    {
        return new TweetSample(parent::TWEET_TYPE, $message, $relevantEntity, $relevantEntityId);
    }
}

class FacebookPostSample extends SocialMessageSample{

    static $MAX_LENGTH = 63206;

    /**
     * Returns true if the SocialMessageSample passes data validation.
     * @return bool true if data pass validation
     */
    public function isValid()
    {
        return strlen($this->message) <= self::$MAX_LENGTH;
    }

    public static function build($message, $relevantEntity, $relevantEntityId = 0)
    {
        return new FacebookPostSample(parent::FACEBOOK_POST_TYPE, $message, $relevantEntity, $relevantEntityId);
    }
}

class NewsletterBlurbSample extends SocialMessageSample{

    /**
     * Returns true if the SocialMessageSample passes data validation.
     * @return bool true if data pass validation
     */
    public function isValid()
    {
        return true; // no specific rules yet
    }

    public static function build($message, $relevantEntity, $relevantEntityId = 0)
    {
        return new NewsletterBlurbSample(parent::NEWSLETTER_TYPE, $message, $relevantEntity, $relevantEntityId);
    }
}

class SocialMessageSampleFactory{

    static $SUPPORTED_TYPES = array(
        SocialMessageSample::TWEET_TYPE,
        SocialMessageSample::FACEBOOK_POST_TYPE,
        SocialMessageSample::NEWSLETTER_TYPE
    );

    public static function getSupportedTypes(){
        return self::$SUPPORTED_TYPES;
    }

    /**
     * Given a db object, attempts to build supported social message sample objects
     * @param stdClass $dbObject
     * @return null|SocialMessageSample
     */
    public static function buildFrom(stdClass $dbObject){
        if ($dbObject->type){
            switch($dbObject->type){
                case SocialMessageSample::TWEET_TYPE:
                    return self::mapTo(TweetSample::build($dbObject->message, $dbObject->relevant_entity, $dbObject->relevant_entity_id), $dbObject);
                    ;                    break;
                case SocialMessageSample::FACEBOOK_POST_TYPE:
                    return self::mapTo(FacebookPostSample::build($dbObject->message, $dbObject->relevant_entity, $dbObject->relevant_entity_id), $dbObject);
                    break;
                case SocialMessageSample::NEWSLETTER_TYPE:
                    return self::mapTo(NewsletterBlurbSample::build($dbObject->message, $dbObject->relevant_entity, $dbObject->relevant_entity_id), $dbObject);
                    break;
            }
        }
        return null;
    }

    private static function mapTo(SocialMessageSample $newObject, stdClass $dbObject){
        $newObject->id = $dbObject->id;
        $newObject->status = $dbObject->status;
        $newObject->sortOrder = $dbObject->sort_order;
        $newObject->createdDateString = $dbObject->created_date;
        $newObject->createdByAdminId = $dbObject->created_by;
        $newObject->updatedDateString = $dbObject->updated_date;
        $newObject->updatedByAdminId = $dbObject->updated_by;
        return $newObject;
    }
}

class MerchantProfilesCriteria extends PaginationCriteria{
    public $memberId; // to support searching for a merchant by member id
    public $state; // region, province, etc.
}

class OrganizationProfilesCriteria extends PaginationCriteria{
    public $memberId;
    public $campaignId;
    public $fiscalSponsorEin;
}

class InvoiceReportCriteria extends PaginationCriteria{
    public $merchantId;
    public $weeksUntilDue = 2;
}
class InvoiceReportData extends PagedResult{
    public $hasOverdueInvoices = false;
}
class InvoiceReportRow{
    const FUNDS_RECEIVED = "Funds Received";
    const PARTIAL_PAYMENT = "Partial Payment";

    public $id = 0;
    public $merchantId = 0;
    public $donationAmount = 0.00;
    public $feeAmount = 0.00;
    public $totalAmount = 0.00;
    public $amountPaid = 0.00;
    public $status;
    public $invoiceDate;
    public $dueDate;

    public function isOverdue(){
        return $this->getBalance() > 0 && $this->isPastDue();
    }

    private function isPastDue(){
        return strtotime('now') > strtotime($this->dueDate);
    }

    public function getBalance(){
        return $this->status == self::FUNDS_RECEIVED ? 0 : $this->totalAmount - $this->amountPaid;
    }

    public function getSerializableObject($fundsReceivedText = "Paid", $partialPaymentText = "Partial", $overdueText = "Due Now", $defaultText = ""){
        $statusText = $defaultText;
        if ($this->isOverdue()){
            $statusText = $overdueText;
        } else if ($this->status == self::FUNDS_RECEIVED){
            $statusText = $fundsReceivedText;
        } else if ($statusText == self::PARTIAL_PAYMENT){
            $statusText = $partialPaymentText;
        }

        $balanceDue = $this->getBalance();
        if ($balanceDue < 0){
            $balanceDue = 0;
        }

        return (object)array(
            "id" => $this->id,
            "date" => date("n/j/Y", strtotime($this->invoiceDate)),
            "dueDate" => date("n/j/Y", strtotime($this->dueDate)),
            "donationAmount" => $this->donationAmount / 1,
            "feeAmount" => $this->feeAmount / 1,
            "totalAmount" => $this->totalAmount / 1,
            "balanceDue" => $balanceDue,
            "status" => $statusText,
            "isOverdue" => $this->isOverdue()
        );
    }
}


class ContactRecord {
    public $firstname;
    public $lastname;
    public $title;
    public $email;
    public $emailConfirm; // holdover
    public $phone;

    public function __get($name){
        return isset($this->$name) ? $this->$name : null;
    }

    public function setPhone($phoneValue){
        $this->phone = preg_replace("/[^0-9]/", "", $phoneValue); // remove all non-numeric
    }
}

abstract class MemberRecord extends ContactRecord{
    public $id;
    protected $typeId;
    public $startPage;

    public function __get($name) {
        return isset($this->$name) ? $this->$name : null;
    }
}

class IndividualMemberRecord extends MemberRecord{
    /**
     * True if the user allows sharing contact info with nonprofit.
     * @var bool
     */
    public $shareWithNonprofit = true;
    /**
     * True if the user wants to be notified when a local business starts a new promotion
     * @var bool
     */
    public $notifyNewEvents = false;

    public function __construct(){
        $this->typeId = 1;
    }
}

class MerchantMemberRecord extends MemberRecord{
    public function __construct(){
        $this->typeId = 2;
    }
}

class OrganizationMemberRecord extends MemberRecord{
    public function __construct(){
        $this->typeId = 3;
    }
}

abstract class CompanyRecord{
    public $id;
    public $ein;
    public $fiscalSponsorEin;
    public $ncesSchoolId;
    public $differentnamethanfiscalsponsor;
    public $name;
    public $address;
    public $city;
    public $state;
    public $postalCode;
    public $country;
    public $phone;
    public $lat;
    public $long;
    public $cityGridIdName;
    public $cityGridProfileId = 0;
    public $website;
    public $neighborhood;
    public $defaultStartTime;
    public $defaultEndTime;
    public $defaultPercentage;
    public $registeredreferralid;
    public $officialname;
    public $keywords;
    /* @var ContactRecord */
    public $primaryContact; // ContactRecord
    public $pid; //permalink id

    public $rawDescriptionText; // raw text: public to allow serialization
    public $descriptionHtml;
    public $isDescriptionPublic = false;

    public function __construct(){
    }

    protected function htmlEscapeAndLinkUrls($text){
        $urlConverter = new UrlLinker();
        return nl2br($urlConverter->htmlEscapeAndLinkUrls($text));
    }

    public function __get($name){
        switch ($name){
            case "description" :
                return ($this->rawDescriptionText && $this->isDescriptionPublic) ? $this->descriptionHtml : "";
                break;
            case "descriptionText" :
                return ($this->rawDescriptionText) ? $this->descriptionHtml : "";
                break;
            default:
                return isset($this->$name) ? $this->$name : NULL;
        }
    }

    public function __set($name, $value){
        switch ($name){
            case "descriptionText" :
                $this->rawDescriptionText = $value;
                $this->descriptionHtml = $this->htmlEscapeAndLinkUrls($value);
                break;
            case "state" :
                $this->state = StateNameAndAbbrConverter::nameToAbbr($value);
                break;
            default:
                $this->$name = $value;
        }
    }

    public function setURL($url){
        if (substr($url,0,4) == "http"){
            $this->website = $url;
        } else if (strlen($url)){
            $this->website = "http://".$url;
        }
        return $this->website;
    }

    public function getFormattedAddress($newline = "\n"){
        $outputString = "";
        if (strlen($this->address)){
            $outputString .= $this->address.$newline;
        }
        if ($this->city || $this->state || $this->postalcode){
            $outputString .= $this->city.", ".$this->state." ".$this->postalCode;
        }
        return trim($outputString);
    }

    /**
     * This is a helper function to grab all the pertinent data on a company we would want to display in the
     * Admin report.
     */
    public function getCompactDump($newline = "\n", $linkifyUrl = false){
        $outputString = $this->name.$newline;
        $outputString .= $this->getFormattedAddress($newline);
        if (strlen($this->website)){
            if ($linkifyUrl){
                $outputString .= $newline."<a href='".$this->website."' target='_blank'>".$this->website."</a>";
            } else {
                $outputString .= $newline.$this->website;
            }
        }
        if (strlen($this->phone)){
            $outputString .= $newline.$this->phone;
        }
        if ($this->primaryContact){
            $contact = $this->primaryContact;
            /* @var $contact ContactRecord */
            if (strlen($contact->firstname) || strlen($contact->lastname)){
                $outputString .= $newline.$contact->firstname." ".$contact->lastname;
            }
            if (strlen($contact->email)){
                $outputString .= $newline.$contact->email;
            }
            if (strlen($contact->phone)){
                $outputString .= $newline.$contact->phone;
            }
        }
        return trim($outputString);
    }
}

class MerchantRecord extends CompanyRecord{
    public $memberId;
    public $eventBriteVenueId;
    public $hasPreferredOrgs;
    private $availableDates; // Array of AvailableDateRecord
    public $rawCustomTransactionEmailText; // raw text; public to allow serialization
    public $customTransactionEmailHtml;
    public $referralCodeText = null;
    public $currentreferralid;
    public $referredByMemberId = 0;
    public $logoPath;
    public $logoFile;
    public $donationshow;

    public function __construct(){
        parent::__construct();
        $this->availableDates = array();
    }

    public function __get($name){
        switch ($name){
            case "customTransactionEmailText" :
                return ($this->rawCustomTransactionEmailText) ? $this->customTransactionEmailHtml : "";
                break;
            default:
                return parent::__get($name);
        }
    }

    public function __set($name, $value){
        switch ($name){
            case "customTransactionEmailText" :
                $this->rawCustomTransactionEmailText = $value;
                $this->customTransactionEmailHtml = $this->htmlEscapeAndLinkUrls($value);
                break;
            default:
                parent::__set($name, $value);
        }
    }

    public function getAvailableDate($date){
        $dateRecord = null;
        if (isset($this->availableDates[$date])){
            $dateRecord = $this->availableDates[$date];
        }
        return $dateRecord;
    }

    public function getAvailableDates(){
        $collection = array();
        foreach($this->availableDates as $record){
            $collection[] = $record;
        }
        return $collection;
    }

    public function addAvailableDate(AvailableDateRecord $availableDate){
        $this->availableDates[$availableDate->date] = $availableDate; // to loosely enforce uniqueness on date
    }

    public function getLogoImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getSmallImageUrl($this->logoPath, $this->logoFile, $CurrentServer, $siteRoot);
    }

    public function getLargeLogoImageUrl($CurrentServer, $siteRoot){
        return ImageLibraryHelper::getLargeImageUrl($this->logoPath, $this->logoFile, $CurrentServer, $siteRoot);
    }
}

class MerchantPreferredOrg {
    public $id;
    public $merchantID;
    public $orgID;
    public $sortOrder;
    public $orgName;
    public $orgEin;
    public $orgAddress;

    public function isValid(){
        return $this->merchantID > 0 && $this->orgID > 0;
    }

    public static function map($dbrow){
        $record = new MerchantPreferredOrg();
        $record->id = $dbrow->id;
        $record->merchantID = $dbrow->merchantID;
        $record->orgID = $dbrow->orgID;
        $record->sortOrder = $dbrow->sortOrder;
        $record->orgName = $dbrow->orgName;
        $record->orgEin = $dbrow->orgEin;
        $record->orgAddress = $dbrow->orgAddress." ".$dbrow->orgCity." ".$dbrow->orgState." ".$dbrow->orgZip;
        return $record;
    }
}

class AvailableDateRecord {
    public $id;
    public $availabilityId;
    public $date;
    public $startTime;
    public $endTime;
    public $percentage;
    public $hasMatchingEvent;

    public function __construct($date, $percentage, $startTime, $endTime, $id = 0, $availabilityId = 0, $hasMatchingEvent = 0){
        $this->date = $date;
        $this->percentage = $percentage;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->id = $id;
        $this->availabilityId = $availabilityId;
        $this->hasMatchingEvent = ($hasMatchingEvent) ? 1 : 0;
    }
}

class OrganizationRecord extends CompanyRecord{
    public $isFiscallySponsored = false;
    public $rawFiscalSponsorName;

    public function __get($name){
        switch ($name){
            case "differentNameThanFiscalSponsor" :
                return ($this->isFiscallySponsored) ? "true" : "";
                break;
            case "fiscalSponsor" :
                return ($this->isFiscallySponsored) ? $this->rawFiscalSponsorName : "";
                break;
            default:
                return parent::__get($name);
        }
    }

    public function __set($name, $value){
        switch ($name){
            case "differentNameThanFiscalSponsor" :
                $this->isFiscallySponsored = strlen($value) > 0;
                break;
            case "fiscalSponsor" :
                $this->rawFiscalSponsorName = $value;
                break;
            default:
                parent::__set($name, $value);
        }
    }
}

class SocialMediaHandle {
    public static $FACEBOOK_NAME = 'facebook';
    public static $TWITTER_NAME = 'twitter';

    public static $MERCHANT_ENTITY_NAME = "merchant";
    public static $ORG_ENTITY_NAME = "organization";
    public static $INDIVIDUAL_ENTITY_NAME = "individual";

    public static $ACTIVE_STATUS = 'ACTIVE';
    public static $DELETED_STATUS = 'DELETED';

    public $id;
    public $entityType;
    public $entityId;
    public $status;
    public $socialNetworkName;
    public $socialHandle;
    public $originalIdentifierText;

    public function getMerchantId(){
        return ($this->entityType == self::$MERCHANT_ENTITY_NAME) ? $this->entityId : null;
    }

    public function setMerchantId($merchantId){
        $this->entityType = self::$MERCHANT_ENTITY_NAME;
        $this->entityId = $merchantId;
    }

    public function getOrganizationId(){
        return ($this->entityType == self::$ORG_ENTITY_NAME) ? $this->entityId : null;
    }

    public function setOrganizationId($orgId){
        $this->entityType = self::$ORG_ENTITY_NAME;
        $this->entityId = $orgId;
    }

    public function getIndividualId(){
        return ($this->entityType == self::$INDIVIDUAL_ENTITY_NAME) ? $this->entityId : null;
    }

    public function setIndividualId($indId){
        $this->entityType = self::$INDIVIDUAL_ENTITY_NAME;
        $this->entityId = $indId;
    }

    public static function createTwitterHandle($handleText){
        return self::createHandle(self::$TWITTER_NAME, $handleText);
    }

    public static function createFacebookHandle($handleText){
        return self::createHandle(self::$FACEBOOK_NAME, $handleText);
    }

    private static function createHandle($networkName, $handleText){
        $handle = new SocialMediaHandle();
        $handle->socialNetworkName = $networkName;
        $handle->originalIdentifierText = $handleText;
        $handle->status = self::$ACTIVE_STATUS;
        $handle->socialHandle = self::getUsernameFromHandleText($handleText);
        return $handle;
    }

    private static function getUsernameFromHandleText($handleText){
        $pathParts = explode("/", $handleText);
        if (count($pathParts) > 2){
            $username = $pathParts[count($pathParts) - 1];
            if (!strlen($username)){
                $username = $pathParts[count($pathParts) - 2];
            }

            return $username;
        } else {
            return preg_replace("/@?(.*)/", "$1", $handleText);
        }
    }

    public function getPublicUrl(){
        switch ($this->socialNetworkName){
            case self::$FACEBOOK_NAME:
                if (filter_var($this->originalIdentifierText, FILTER_VALIDATE_URL)){
                    return $this->originalIdentifierText;
                }
                return "https://www.facebook.com/".$this->socialHandle;
                break;
            case self::$TWITTER_NAME:
                return "https://twitter.com/".$this->socialHandle;
                break;
            default:
                return "";
        }
    }
}

class SocialMediaHandleCriteria extends PaginationCriteria{
    public $orgId;
    public $merchantId;
    public $individualId;
    public $socialNetworkName;
    public $activeOnly;
}

?>
