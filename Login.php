<?php
include_once( "Global_Variables.php");
$isAjaxRequest = isAjaxRequest();
//get the posted values
//removes sql injections from the data
//print_r($_POST);
$user_name= trim(strtolower(htmlspecialchars(addslashes($_POST['user_name']),ENT_QUOTES)));
$pass_word = trim($_POST['password']);
$staySignedIn = $_POST['MobileStaySignedIn']=="yes";
if ( isset($_SERVER["REMOTE_ADDR"]) ){
	$ip=$_SERVER["REMOTE_ADDR"];
} elseif (isset($_SERVER["SERVER_ADDR"])){
	$ip=$_SERVER["SERVER_ADDR"];
} else {
	$ip="Not Available";
}

// Call the AuthProvider
include_once($dbProviderFolder."AuthProvider.php");
$authProvider = new AuthProvider($mysqli);
$authAttempt = $authProvider->authenticate($user_name, $pass_word, $ip);
if ($authAttempt->message){
    if ($isAjaxRequest){
        $authResponse = new stdClass();
        $authResponse->isLoggedIn = false;
        output_json($authResponse);
    } else {
        SessionManager::setTmpVar('LoginAlert', $authAttempt->message);
        setcookie("username", "", time()-3600);
        header('location:Login');
        die();
    }
}

$CreateSessionVariablesProvider = new CreateSessionVariablesProvider($mysqli);
$sessionData = $CreateSessionVariablesProvider->getSessionData($authAttempt);
SessionManager::login($authAttempt, $sessionData, $staySignedIn);

if ($isAjaxRequest){
    $authResponse = new stdClass();
    $authResponse->isLoggedIn = true;
    $authResponse->memberTypeDescription = SessionManager::getMemberType();
    $authResponse->userName = SessionManager::getUserName();
	
    output_json($authResponse);
} else {
    //all is good then send to Menus
    header('location:Home?LoggedIn');
}
?>