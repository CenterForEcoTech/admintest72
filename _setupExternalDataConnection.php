<?php

if (!isset($dbConfigExternal)){
    trigger_error("_setupExternalDataConnection.php This script requires the database configuration for the external data database in siteconf.", E_USER_ERROR);
    die(); // no db config for the target db.
}

$externalDataConn = new mysqli($dbConfigExternal->serv,$dbConfigExternal->user,$dbConfigExternal->pass,$dbConfigExternal->inst);
if ($externalDataConn->connect_error) {
    trigger_error("_setupExternalDataConnection.php: Failed to connect to MySQL: ".$externalDataConn->connect_error, E_USER_ERROR);
}