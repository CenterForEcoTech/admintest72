<?php
$mustBeMerchant = true;
include('LoginRequiredForThisPage.php');
if(!$isAccessDenied){
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Create a ".$SiteName." Listing";
		$PageHeader1 = "Create a New ".$SiteName." Listing (It&rsquo;s Easy)!";

	}
	if ($load_getContent){
		if ($BaseLink == "mobile/mobile_"){
			$BaseLink = "";
		}
        $eventProvider = getEventProvider();
        /* @var $eventProvider EventProvider */
        $sampleMerchantProposedEvents = $eventProvider->getNextMerchantProposedEvents($merchantID, 0, 1);
		?>
		<div id="merchant-create-event" class="twelve columns">
			<div id="local_businesses" class="four columns user_types">
				<h2><span>Businesses</span></h2>

			</div>
			<div class="seven columns">
				<form class="override_skel" method="post" action="<?php echo $CurrentServer;?><?php echo $CleanURLFolder.$BaseLink;?>ManageEvents/Create">
					<label class="centered">
						<h3>Pick a Date for Your Event</h3>
						<span class="highlight">
							<input type="text" id="Event_Date" class="auto-watermarked" name="Event_Date" title="mm/dd/yyyy" />
						</span>
					</label>
				</form>
                <?php if(count($sampleMerchantProposedEvents->collection) > 0) {?>
				<div id="or_graphic">
					<span>OR</span>
				</div>
				<h3 class="centered">Choose an existing nonprofit event to host below</h3>
                <?php } ?>
			</div>
		</div>
    <?php if (count($sampleMerchantProposedEvents->collection) > 0 ) { ?>
        <div id="event-section" class="sub_section twelve columns">

            <ul id="event-list"></ul>
            <a id="bottom-bookmark"></a>
        </div>

        <script type="text/javascript">
            $(function(){
                var startIndex = 0,
                    pageLength = 10,
                    eventList = $("#event-list"),
                    eventListPosition = eventList.offset(),
                    eventURL = "<?php echo $CurrentServer."ProposedEvents"; ?>",
                    topElement = $("#event-section"),
                    bottomElement = $("#bottom-bookmark"),
                    scrollToElement = null,
                    loadEvents = function(){
                        eventList.load(eventURL,
                            {
                                startIndex: startIndex,
                                pageLength: pageLength
                            },
                            function(responseText){
                                if (responseText){
                                    $("#event-section").show();
                                    if (scrollToElement){
                                        $('html, body').animate({
                                            scrollTop: scrollToElement.offset().top
                                        }, 500);
                                    }
                                } else {
                                    $("#event-section").hide();
                                }
                            });
                    };

                loadEvents();

                $("#event-list").delegate(".event-nav", "click", function(e){
                    var self = $(this),
                        isNext = self.hasClass("next");
                    e.preventDefault();
                    if (isNext){
                        scrollToElement = topElement;
                    } else {
                        scrollToElement = bottomElement;
                    }
                    startIndex = self.attr("data-start");
                    pageLength = self.attr("data-length");
                    loadEvents();
                });
            });
        </script>

        <?php } ?>
		<script type="text/javascript">
			$(function(){
				$("#Event_Date").datepicker({
					minDate:0,
					onSelect: function(){
						$(this).closest("form").submit();
					}
				});
			});
		</script>
<?php }// logged in
}
?>
