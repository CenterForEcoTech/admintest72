<?php
require_once("Global_Variables.php");
if (isset($_POST['Email'])){
	$EmailFrom = htmlspecialchars($_POST['Email']);
	$EmailFromName = htmlspecialchars($_POST['Name']);
	$replyto = htmlspecialchars($_POST['Email']);
	$replytoName = htmlspecialchars($_POST['Name']);
	$EmailSubject = htmlspecialchars($_POST['Subject']);
	$EmailMsg = htmlspecialchars($_POST['Message']);
	$EmailTo = $Config_ContactUsEmail;
	$EmailToName = $Config_ContactUsEmailName;
	$AddCommonFooter = false;
	if ($EmailMsg){
		include($_SERVER['DOCUMENT_ROOT'].$Config_DevFolder."Email_Send.php");
	}
	if (isAjaxRequest()){
		if ($SimpleEmailAlert){
			echo $SimpleEmailAlert; die();
		} else {
			echo ''; die();
		}
		die();
	}
}
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Contact ".$SiteName;
		$PageHeader1 = "Contact Us";
	}
if ($load_getContent){

	$EmailSubject = "";
	$EmailMsg = "";
	$EmailFromName = "";
	$EmailFrom = "";
    $EmailBody = "";
	$ShowExtra = true;
	if ($_GET["issue"]){
		$contactUsIssue = $_GET["issue"];
        // RewriteRule ^ContactUs/ChooseRecipient/(.+) Index.php?PageName=ContactUs&issue=chooseRecipient&trans=$1isModal=1 [PT,QSA]
		if ($contactUsIssue == "chooseRecipient"){
			$EmailSubject = "Choose Recipient for Donation (".$_GET["trans"].")";
		}

        //RewriteRule ^ContactUs/RequestCustomURL/(.+) Index.php?PageName=ContactUs&issue=customUrl&pid=$1&isModal=1 [PT,QSA]
        if ($contactUsIssue == "customUrl"){
            $EmailSubject = "Request Custom URL for (".$_GET["pid"].")";
            $EmailBody = "Please accept my request to change my permalink id from \n\"".$_GET["pid"]."\" to the following: \n\n";
        }

        //RewriteRule ^ContactUs/ProfilePageSuggestions Index.php?PageName=ContactUs&issue=profilePage&isModal=1 [PT,QSA]
        if ($contactUsIssue == "profilePage"){
            $EmailSubject = "Suggestions for ".$_GET["loc"]." Profile Page";
            $EmailBody = "I was on a ".$_GET["loc"]." Profile Page (".$CurrentServer."ct/".$_GET["pid"].") and had a suggestion: \n\n";
        }

        //RewriteRule ^ContactUs/NoResults Index.php?PageName=ContactUs&issue=noResults&loc=$1&isModal=1 [PT,QSA]
        if ($contactUsIssue == "noResults"){
            $EmailSubject = "No Results Found in ".$_GET["loc"];
            $EmailBody = "A search in ".$_GET["loc"]." had no results.  The following business should be contacted to join ".$SiteName.": \n\n";
        }
	}
	if (SessionManager::getUserEmail()){
		$EmailFrom = SessionManager::getUserEmail();
	}
	if (isLoggedIn()){
		$EmailFromName = SessionManager::getUserName();
	}
	$isModal = GetParameter("isModal");
	if ($isModal){
		$ShowExtra = false;
	}
	?>
	<div id="contact-us">
		<?php if ($isModal) { ?>
		<h2 class="ten columns">Contact Us</h2>
		<?php } ?>
		<div class="clear"></div>
		<?php echo $SimpleEmailAlert;?>
		<?php if ($ShowExtra) { ?>
		<div class="contact-us-row" class="clear">
			Please get in touch with us at any time if you need assistance or want to learn more about <?php echo $SiteName;?>!<Br>
			<br>
			Center for EcoTechnology.<br>
			112 Elm St<br>
			Pittsfield, MA 02101<br>
			<br>
		</div>
		<div class="clear"></div>
		<?php } ?>
		<div id="contact-form-container">
			<span id="error-message" class="alert"></span>
			<form id="contact-us-form" class="override_skel eight columns" action="<?php echo $CurrentServer;?><?php if ($BaseLink != "mobile/mobile_"){echo str_replace("_","",$BaseLink)."/";}?>ContactUs/Send" method="post">
				<label>
					<div>Your Name:</div>
					<span class="highlight">
						<input class="required" type="text" name="Name" value="<?php echo $EmailFromName; ?>">
					</span>
					<span class="alert"></span>
				</label>
				<label>
					<div>Your Email:</div>
					<span class="highlight">
						<input class="email" type="text" name="Email" value="<?php echo $EmailFrom; ?>">
					</span>
					<span class="alert"></span>
				</label>
				<label>
					<div>Subject:</div>
					<span class="highlight">
						<input class="required eight columns" type="text" name="Subject" value="<?php echo $EmailSubject; ?>">
					</span>
					<span class="alert"></span>
				</label>
				<label>
					<div>Message:</div>
					<span class="highlight">
						<textarea class="required eight columns" name="Message"><?php echo $EmailBody;?></textarea>
					</span>
					<span class="alert"></span>
				</label>
				<div class="btn">
					<input type="submit" id="contact-form-submit" value="Submit">
				</div>
				<input type="hidden" name="isModal" value="<?php echo $isModal;?>">
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$("#contact-us-form").validate();
			$("#contact-form-submit").on('click', function(e){
				var formContainer = $("#contact-form-container"),
					data = $(this).closest("form").serialize(),
					url = formContainer.find("form").attr("action");
				e.preventDefault();
				$.post(
					url,
					data,
					function(data){
						if (data){
							formContainer.html(data);
						} else {
							$("#error-message").html("Please fill out the form before submitting.");
						}
					},
					"html");
			});
		});
	</script>
<?php
}
?>