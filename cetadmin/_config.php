<?php
$adminFolder = "cetadmin/";
include_once("_getRootFolder.php");

$dbDownTemplate = $rootFolder.$adminFolder."_dbIsDown.php";
include_once($rootFolder.'Global_Variables.php');
if (!$byPassLogin){
	if (!isAdmin() && $_SERVER['SCRIPT_FILENAME'] != $rootFolder.$adminFolder.'index.php'){
		header("HTTP/1.0 401 Unauthorized");
		if (isAjaxRequest()){
			die();
		}
		header("location:".$CurrentServer.$adminFolder); // redirect to admin root.
	}
}
$isAdminConsole = true;

$authData = SessionManager::getAdminAuthObject();
$adminSecurityGroupId = $authData->adminSecurityGroupId;
$adminSecurityGroups = explode(",",$authData->adminSecurityGroups);
include_once("_adminSecurityGroups.php");
?>