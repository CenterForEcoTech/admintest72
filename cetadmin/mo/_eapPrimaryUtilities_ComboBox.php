<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxUtilitiesPrimary" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "$('#primaryUtility').val($(ui.item).val());";
							// echo "window.location = '?nav=manage-codes&CodeID='+$(ui.item).val();";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxUtilitiesPrimary parameters " id="SelectedUtilityPrimary" data-hiddenid="primaryUtility">
				<option value="">Select one...</option>
				<?php 
					foreach ($UtilityPrimaries as $PrimaryUtilityName){
						echo "<option value=\"".$PrimaryUtilityName."\"".($SelectedPrimaryUtility==$PrimaryUtilityName ? " selected" : "").">".$PrimaryUtilityName."</option>";
					}
				?>
			  </select>
			</div>
		</div>