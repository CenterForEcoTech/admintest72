<?php

function initializeAnalytics()
{
  // Creates and returns the Analytics Reporting service object.

  // Load the Google API PHP Client Library.
  require_once '../../cetadmin/gbs/google-api-php-client/src/Google/autoload.php';

  // Use the developers console and replace the values with your
  // service account email, and relative location of your key file.
  $SERVICE_ACCOUNT_EMAIL = "cetdashboard@astral-robot-131618.iam.gserviceaccount.com";
  $KEY_FILE_LOCATION = "../../cetadmin/gbs/client_secrets.p12";

  // Create and configure a new client object.
  $client = new Google_Client();
  $client->setApplicationName("Hello Analytics Reporting");
  $analytics = new Google_Service_AnalyticsReporting($client);

  // Read the generated client_secrets.p12 key.
  $key = file_get_contents($KEY_FILE_LOCATION);
  $cred = new Google_Auth_AssertionCredentials($SERVICE_ACCOUNT_EMAIL,array(Google_Service_Analytics::ANALYTICS_READONLY),$key);
  //echo "<pre>".print_r($cred);
  $client->setAssertionCredentials($cred);
  if($client->getAuth()->isAccessTokenExpired()) {
    $client->getAuth()->refreshTokenWithAssertion($cred);
  }

  return $analytics;
}


function getReport(&$analytics,$metricsToUse,$startDate,$endDate) {

  // Replace with your view ID, for example XXXX.
  $VIEW_ID = "56788920";

  // Create the DateRange object.
  $dateRange = new Google_Service_AnalyticsReporting_DateRange();
  $dateRange->setStartDate($startDate);
  $dateRange->setEndDate($endDate);

  // Create the Metrics object.
  foreach ($metricsToUse as $expression=>$alias){
	  ${$expression} = new Google_Service_AnalyticsReporting_Metric();
	  ${$expression}->setExpression("ga:".$expression);
	  ${$expression}->setAlias($alias);
	  $metricsRequested[] = ${$expression};
  }	


  // Create the ReportRequest object.
  $request = new Google_Service_AnalyticsReporting_ReportRequest();
  $request->setViewId($VIEW_ID);
  $request->setDateRanges($dateRange);
  $request->setMetrics($metricsRequested);

  $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
  $body->setReportRequests( array( $request) );
  return $analytics->reports->batchGet( $body );
}

function printResults(&$reports) {
	$results = array();
  for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
    $report = $reports[ $reportIndex ];
    $header = $report->getColumnHeader();
    $dimensionHeaders = $header->getDimensions();
    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
	//print_r($metricHeaders);
    $rows = $report->getData()->getRows();

    for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
      $row = $rows[ $rowIndex ];
      $dimensions = $row->getDimensions();
      $metrics = $row->getMetrics();
	 // print_r($metrics);
	  $metrics =$metrics[0]->values;
      for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
        print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
      }
      for ($j = 0; $j < count( $metricHeaders ) && $j < count( $metrics ); $j++) {
        $entry = $metricHeaders[$j];
        $value = $metrics[$j];
        //print("Metric type: " . $entry->getType() . "\n" );
        //print($entry->getName() . ": " . $value . "\n");
		if ($entry->getType() == "FLOAT"){
			$value = round($value,2);
		}
		if ($entry->getType() == "TIME"){
			$value = ltrim(gmdate("H:i:s",$value),"00:0");
			$value = ltrim($value,"00:");
		}
		if ($entry->getType() == "PERCENT"){
			$value = round($value,2)."%";
		}
		$results[$entry->getName()] = $value;
      }
    }
  }
  return $results;
}

$analytics = initializeAnalytics();
$response = getReport($analytics,$metricsToUse,$startDate,$endDate);
//print_r($response);
$analyticsResults = printResults($response);
?>
