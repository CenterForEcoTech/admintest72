<?php
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = $path;
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
set_time_limit (600);

include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$results = new stdClass();
$relativeLink ="../residential/";
require($thispath.'residential/pdfProcessor/fpdm.php');
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$results = $_GET;
		echo "this is get";
        break;

    case "POST":
		
		$newRecord = json_decode($currentRequest->rawData);
		$newRecord = (array)$newRecord;
		// $results = $newRecord;
		$pdf = new FPDM($thispath.'gbs/importfiles/sf270_template.pdf');
		$fileLocation = $newRecord["fileLocation"];

		$absolutePath = $newRecord["absolutePath"];
		unset($newRecord["absolutePath"]);
		unset($newRecord["fileLocation"]);
		//unset($newRecord['boilerType1']);
		//unset($newRecord['boilerType2']);
		$pdf->Load($newRecord, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
		$pdf->Merge();
		$target = str_replace(" ", "_", $newRecord["SF270Name"]);
		//$pdf->Output();
		$pdf->Output('f',$absolutePath);
		$results ="<a href='".$fileLocation."' target='".$target."' >".str_replace($CurrentServer.$adminFolder."gbs/sf270Files/","",$fileLocation)."</a><Br>";
		break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		if ($action == "newCell"){
			$addNewCell = $residentialProvider->insertResAuditCell($newRecord);
			$results->results = $addNewCell;
		}elseif ($action == "removeCell"){
			$removeCell = $residentialProvider->removeResAuditCell($newRecord);
			$results->results = $removeCell;
		}else{
			$results->data = "no action matched";
		}
			
		header("HTTP/1.0 201 Created");
        break;
}
output_json($results);
die();
?>