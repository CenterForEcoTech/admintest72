<?php
ini_set('max_execution_time',600);
include_once("../_config.php");
include_once("../_datatableModelBase.php");
//error_reporting(E_ALL);
$pageTitle = "Admin - GBS";
//print_pre($_SESSION);
$navSession = explode("&",$_SESSION['SalesForceQueryString']);
if ($_SESSION["SalesForceReport"] == "RWTAPipeline"){$navSession[] = $_SESSION['SalesForceQueryString'] = "nav=invoicingTool-RWTAPipeline";}
$sessionNav = "";
//print_pre($_SESSION);
if (count($navSession)){
	foreach ($navSession as $qvariable){
		if (substr($qvariable,0,4)=="nav="){
			$sessionNav = str_replace("nav=","",$qvariable);
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-BGAS" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-RWTAPipeline" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-GreenTeam" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-NGRIDInspection" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-TRC" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-PollutionPrevention" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-BostonZero" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-MassCEC" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-RUS" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			if (!isset($_GET["nav"]) && $sessionNav=="invoicingTool-MMWEC" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."gbs/?redirect=1&".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			/*
			if ($sessionNav=="ESScheduler" && !$_GET['redirect']){
				echo "<script>window.location.replace('".$CurrentServer.($_SESSION['isStaff'] ? "cetstaff/" : $adminFolder)."residential/?redirect=1".$_SESSION['SalesForceQueryString']."');</script>";
				$_SESSION['isStaff'] = false;
			}
			*/
		}
	}
}

$nav = (isset($_GET["nav"]) ? $_GET["nav"] : $sessionNav);
if ($nav){
    switch ($nav){
        case "manage-eaptemplates":
            $displayPage = "views/_eapTemplates.php";
            break;
        case "manage-codes":
            $displayPage = "views/_manageCodes.php";
            break;
        case "manage-groups":
            $displayPage = "views/_manageGroups.php";
            break;
        case "manage-rates":
            $displayPage = "views/_manageBillingRates.php";
            break;
        case "manage-bullets":
            $displayPage = "views/_manageBullets.php";
            break;
        case "manage-farms":
            $displayPage = "views/_manageFarms.php";
            break;
        case "manage-hoursentry":
            $displayPage = "views/_manageHoursEntry.php";
            break;
        case "manage-submittedhours":
            $displayPage = "views/_manageSubmittedHours.php";
            break;
        case "check-submittedhours":
            $displayPage = "views/_checkSubmittedHours.php";
            break;
		case "reports":
			$displayPage = "_reportsTabs.php";
			break;
        case "report-bycode":
            $displayPage = "views/_reportByCode.php";
            break;
        case "report-bygroup":
            $displayPage = "views/_reportByGroup.php";
            break;
        case "report-bystaff":
            $displayPage = "views/_reportByStaff.php";
            break;
        case "report-rawdata":
            $displayPage = "views/_manageRawData.php";
            break;
        case "invoicingTools":
            $displayPage = "_invoicingToolsTab.php";
            break;
        case "invoicingTool-RWTAPipeline":
            $displayPage = "views/_invoicingTools_RWTAPipeline.php";
            break;
        case "salesforceConnect":
            $displayPage = "views/_connectToSalesForce.php";
            break;
        case "invoicingTool-Eversource":
            $displayPage = "views/_invoicingTools_Eversource.php";
            break;
        case "invoicingTool-MFEP":
            $displayPage = "views/_invoicingTools_MFEP.php";
            break;
        case "invoicingTool-NGRIDInspection":
            $displayPage = "views/_invoicingTools_NGRIDInspection.php";
            break;
        case "invoicingTool-BGAS":
            $displayPage = "views/_invoicingTools_BGAS.php";
            break;
        case "invoicingTool-SmallContractHours":
            $displayPage = "views/_invoicingTools_SmallContractHours.php";
            break;
        case "invoicingTool-HealthyHomes":
            $displayPage = "views/_invoicingTools_HealthyHomes.php";
            break;
        case "invoicingTool-MassCEC":
            $displayPage = "views/_invoicingTools_MassCEC.php";
            break;
        case "invoicingTool-RUS":
            $displayPage = "views/_invoicingTools_RUS.php";
            break;
        case "invoicingTool-Passthrough":
            $displayPage = "views/_invoicingTools_Passthrough.php";
            break;
        case "invoicingTool-Bullets":
            $displayPage = "views/_invoicingTools_Bullets.php";
            break;
        case "invoicingTool-BulletsQuarterly":
            $displayPage = "views/_invoicingTools_BulletsQuarterly.php";
            break;
        case "invoicingTool-GreenTeam":
            $displayPage = "views/_invoicingTools_GreenTeam.php";
            break;
        case "invoicingTool-Covanta":
            $displayPage = "views/_invoicingTools_Covanta.php";
            break;
        case "invoicingTool-PollutionPrevention":
            $displayPage = "views/_invoicingTools_PollutionPrevention.php";
            break;
        case "invoicingTool-BostonZero":
            $displayPage = "views/_invoicingTools_BostonZero.php";
            break;
        case "invoicingTool-ColumbiaGas":
            $displayPage = "views/_invoicingTools_ColumbiaGas.php";
            break;
        case "invoicingTool-BigY":
            $displayPage = "views/_invoicingTools_BigY.php";
            break;
        case "invoicingTool-TRC":
            $displayPage = "views/_invoicingTools_TRC.php";
            break;
        case "invoicingTool-MMWEC":
            $displayPage = "views/_invoicingTools_MMWEC.php";
            break;
        case "invoicingTool-Status":
            $displayPage = "views/_invoicingTools_StatusStatic.php";
            break;
        case "invoicingTool-Trigger":
            $displayPage = "views/_invoicingTools_ProcessingTrigger.php";
            break;
        case "invoicingTool-DirectInstalls":
            $displayPage = "views/_invoicingTools_DirectInstalls.php";
            break;
        case "invoicingTool-FileConverter":
            $displayPage = "views/_invoicingTools_FileConverter.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>GBS Administration</h1>';
include_once("_gbsMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Process Time: {$time}";
?>