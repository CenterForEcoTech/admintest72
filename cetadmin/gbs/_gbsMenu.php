<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_GBS)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "130";
	$invoicingToolsDropDownItemsTop = "130";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GBS))){?>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>hours?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Admin (moved to Hours tab)</a></li>
		<!--	<li><a id="manage-eaptemplates" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-eaptemplates" class="button-link">Manage EAP Templates</a></li>
			
			<li><a id="manage-codes" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-codes" class="button-link">Manage Codes</a></li>
			<li><a id="manage-groups" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-groups" class="button-link">Manage Groups</a></li>
			<li><a id="manage-rates" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-rates" class="button-link">Manage Billing Rates</a></li>
			<li><a id="manage-submittedhours" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-submittedhours&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Manage Submitted Hours</a></li>
			<li><a id="manage-hoursentry" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Entry</a></li>
			<li id="ReportMenuDropDown"><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=reports" class="button-link">Reports</a>
				<?php //($nav != "reports" ? include('_reportsTabs.php') : "");?>
			</li>
			-->
			<li><a id="manage-bullets" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-bullets" class="button-link">Manage Static Report Bullets</a></li>
			<li><a id="manage-farms" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-farms" class="button-link">Manage Farms</a></li>
			<li id="InvoicingToolMenuDropDown"><a id="invoicingTools" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTools" class="button-link">Commercial InvoicingTools</a>
				<?php ($nav != "invoicingTools" ? include('_invoicingToolsTab.php') : "");?>
			</li>
<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
			$("#invoicingToolDropDownItems").hide();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$("#invoicingTools.button-link").on("mouseover",function(){
			$("#invoicingToolDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "report-bycode":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-bygroup":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-bystaff":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-rawdata":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "invoicingTools":
					$nav = "invoicingTools";
					$showInvoicingToolsDropDown = true;
					break;
				case "invoicingTool-RWTAPipeline":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "salesforceConnect":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-Eversource":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-MFEP":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-NGRIDInspection":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-BGAS":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-SmallContractHours":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-Passthrough":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-GreenTeam":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-Covanta":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-ColumbiaGas":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-BigY":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-TRC":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-RUS":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-FileConverter":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
		
		
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>