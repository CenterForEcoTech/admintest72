<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run

require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$attachmentFolder = ($attachmentFolder ? : "attachments");
$inputFileName = 'salesforce/'.$attachmentFolder.'/'.$fileName;
echo "opening ".$inputFileName."<br>";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//get Account# and LocationID
$locationIDText = "";
$locationID = "";
$accountNumberText = "";
$accountNumber = "";
try {
    $locationIDText = $objPHPExcel->getActiveSheet()->getCell('F1')->getValue();
} catch (PHPExcel_Exception $e) {
}
if (trim($locationIDText) == "Loc ID"){
    try {
        $locationID = $objPHPExcel->getActiveSheet()->getCell('F2')->getValue();
    } catch (PHPExcel_Exception $e) {
    }
}
try {
    $accountNumberText = $objPHPExcel->getActiveSheet()->getCell('E1')->getValue();
} catch (PHPExcel_Exception $e) {
}
if (trim($accountNumberText) == "Acct #"){
    try {
        $accountNumber = $objPHPExcel->getActiveSheet()->getCell('E2')->getValue();
    } catch (PHPExcel_Exception $e) {
    }
}else{
	if (!$locationID){
        try {
            $locationID = $objPHPExcel->getActiveSheet()->getCell('E1')->getValue();
        } catch (PHPExcel_Exception $e) {
        }
    }
}
//alternative location for Jamie O
if ($locationID == "" || substr($locationID,0,1) == "7"){
    try {
        $locationID = $objPHPExcel->getActiveSheet()->getCell('E2')->getValue();
    } catch (PHPExcel_Exception $e) {
    }
}
if ($accountNumber == "" || substr($accountNumber,0,1) == "5"){
    try {
        $accountNumber = $objPHPExcel->getActiveSheet()->getCell('E1')->getValue();
    } catch (PHPExcel_Exception $e) {
    }
}

//  Get worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$headerRow = $sheet->rangeToArray('A3:P3', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	$headerNames[getNameFromNumber($col+1)]=$val;
	$headerNumbers[($col+1)]=$val;
}

//  Loop through each row of the worksheet in turn
for ($row = 4; $row <= 100; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$rowsRaw[($row-3)][$headerNumbers[($k+1)]] = $v;
	}
}
//cleanup rows
foreach ($rowsRaw as $rowId=>$rowDetails){
	//workaround for column renames
	$rowDetails["kWh Savings/Unit"] = ($rowDetails["Annual kWh Savings/Unit"] ? $rowDetails["Annual kWh Savings/Unit"] : $rowDetails["kWh Savings/Unit"]);
	$rowDetails["fileName"] = $actualFileName;
	$rowDetails["contractorLookup"] = ($attachmentResults[$actualFileName]["ContractorName"] ? : $attachmentResults[$parentId]["ContractorName"]);
	$rowDetails["contractorInfo"] = ($attachmentResults[$actualFileName]["ContractorInfo"] ? : $attachmentResults[$parentId]["ContractorInfo"]);
	$rowDetails["accountType"] = ($attachmentResults[$actualFileName]["accountType"] ? : $attachmentResults[$parentId][$actualFileName]["accountType"]);
	$rowDetails["utilityAccountNumber"] = $accountNumber;
	$rowDetails["utilityLocationID"] = $locationID;

	if (trim($rowDetails["Measure Description"]) != '' && (int)$rowDetails["Qty"] > 0){
		$rowsClean[] = $rowDetails;
	}
}
?>