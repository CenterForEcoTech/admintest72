<?php
error_reporting(E_ALL & ~E_NOTICE);

// session_start() has to go right at the top, before any output!
session_start();
//echo "isset(SESSION['enterpriseSessionId']=".isset($_SESSION['enterpriseSessionId'])."='".$_SESSION['enterpriseSessionId']."'<Br>";
//echo "isset(SESSION['partnerSessionId']=".isset($_SESSION['partnerSessionId'])."='".$_SESSION['partnerSessionId']."'<Br>";

//unset($_SESSION['enterpriseSessionId']);
//unset($_SESSION['partnerSessionId']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>REST/OAuth Example</title>
    </head>
    <body>
        <tt>
            <?php

            require_once ($_SERVER["DOCUMENT_ROOT"].'sforce-php/soapclient/SforcePartnerClient.php');
            require_once ($_SERVER["DOCUMENT_ROOT"].'sforce-php/soapclient/SforceEnterpriseClient.php');

            define("USERNAME", $Config_SalesforceUserName);
            define("PASSWORD", $Config_SalesforcePassword);
            define("SECURITY_TOKEN", $Config_SalesforceSecurityToken);

            try {
                echo "<table border=\"1\"><tr><td><pre>";
                echo "First with the enterprise client<br/><br/>\n";

                $mySforceConnection = new SforceEnterpriseClient();
                $mySforceConnection->createConnection($_SERVER["DOCUMENT_ROOT"]."sforce-php/soapclient/enterprise.wsdl.xml");

                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (isset($_SESSION['enterpriseSessionId'])) {
                    $location = $_SESSION['enterpriseLocation'];
                    $sessionId = $_SESSION['enterpriseSessionId'];

                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);

                    echo "Used session ID for enterprise<br/><br/>\n";
                } else {
                    $mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);

                    $_SESSION['enterpriseLocation'] = $mySforceConnection->getLocation();
                    $_SESSION['enterpriseSessionId'] = $mySforceConnection->getSessionId();

                    echo "Logged in with enterprise<br/><br/>\n";
                }
		$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId IN ('a01U000001KJ8VXIA1','a01U000001JgMMWIA3')";
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_r($response);
		foreach ($response->records as $record) {
			$parentId = $record->ParentId;
			$name = $record->Name;
			$Attachments[$parentId][] = array("id"=>$record->Id,"Name"=>$name,"BodyLength"=>$bodyLength);
		}
		
		print_r($Attachments);
		echo "<hr>";
		
/*				
				$reportFields = array("CreatedBy","CreatedById","CreatedDate","Description","DeveloperName","FeedSubscriptionsForEntity","Feeds","FolderName","Format","IsDeleted","LastModifiedBy","LastModifiedById","LastModifiedDate","LastReferencedDate","LastRunDate","LastViewedDate","Name","NamespacePrefix","Owner","OwnerId","SystemModstamp");
				
				$query = "SELECT ".implode(", ",$reportFields)." FROM Report";
				echo $query;
                $response = $mySforceConnection->query($query);
                echo "Results of query '$query'<br/><br/>\n";
                foreach ($response->records as $record) {
					foreach ($reportFields as $fieldName){
						echo $fieldName.": ".$record->{$fieldName}."<br/>\n";
					}
                }
*/
                echo "</pre></td></tr></table>";
            } catch (Exception $e) {
                echo "Exception ".$e->faultstring."<br/><br/>\n";
                echo "Last Request:<br/><br/>\n";
                echo $mySforceConnection->getLastRequestHeaders();
                echo "<br/><br/>\n";
                echo $mySforceConnection->getLastRequest();
                echo "<br/><br/>\n";
                echo "Last Response:<br/><br/>\n";
                echo $mySforceConnection->getLastResponseHeaders();
                echo "<br/><br/>\n";
                echo $mySforceConnection->getLastResponse();
            }
           ?>
        </tt>
    </body>
</html>