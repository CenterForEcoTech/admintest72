<?php
require_once 'salesforce/config.php';

session_start();
require_once('rest_functions.php');
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
$reportId = "00OU0000002mva5"; //883 Account - Last Billing Month

//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
if (checkSession($instance_url, $reportId, $access_token)){
	ob_start();
	
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
	
		$report883 = retrieve_report_883($instance_url, $reportId, $access_token);
		$fieldNamesArray = $report883["fieldNamesArray"];
		//print_r($fieldNamesArray);
		$reportRowsUnSorted = $report883["rows"];
		$reportRowsRaw = array_orderby($reportRowsUnSorted, 'Upgrade__c.Name', SORT_ASC, 'Upgrade__c.Green_Prospect_Type__c', SORT_ASC);
		//print_pre($reportRowsRaw);
																		

		$AllMeasuresHeader = 
			array(
				"A"=>array("title"=>"Order#","object"=>"calculate_rowId","width"=>"7"),
				"B"=>array("title"=>"Project ID","object"=>"calculate_projectId","width"=>"10"),
				"C"=>array("title"=>"Project Name","object"=>"calculate_actualAccountName","width"=>"30"),
				"D"=>array("title"=>"Town","object"=>"calculate_town","width"=>"11"),
				"E"=>array("title"=>"Included in Invoice (Month)","object"=>"calculate_invoiceMonth","width"=>"14"),
				"F"=>array("title"=>"Contractor","object"=>"calculate_contractor","width"=>"16"),
				"G"=>array("title"=>"EFI #","object"=>"EFI Item Number","width"=>"12"),
				
				"H"=>array("title"=>"IN(CLINC) or IM(CLCCC)?","object"=>"calculate_clinc","width"=>"11"),
				"I"=>array("title"=>"Measure Detail","object"=>"Measure Description","width"=>"34"),
				"J"=>array("title"=>"Measure Description","object"=>"Measure Category","width"=>"41"),
				"K"=>array("title"=>"Unit Quantity","object"=>"Qty","width"=>"8"),
				"L"=>array("title"=>"Unit Cost","object"=>"Cost/Unit","width"=>"12"),
				"M"=>array("title"=>"Unit Incentive","object"=>"Incentive/Unit","width"=>"12"),
				"N"=>array("title"=>"Annual kWh Savings/Unit","object"=>"kWh Savings/Unit","width"=>"12"),
				"O"=>array("title"=>"Measure Life","object"=>"Measure Life","width"=>"8"),
				"P"=>array("title"=>"Dmd kWh /Unit","object"=>"Demand kWh Reduction","width"=>"8"),
				"Q"=>array("title"=>"Annual kWh Savings","object"=>"Annual kWh Savings","width"=>"8"),
				"R"=>array("title"=>"Lifetime kWh Savings","object"=>"Lifetime kWh Savings","width"=>"8"),
				"S"=>array("title"=>"Total Cost","object"=>"Total Cost","width"=>"14"),
				"T"=>array("title"=>"Total Incentive","object"=>"Total Incentive","width"=>"14"),
				"V"=>array("title"=>"Cost_Calc","object"=>"calculate_cost","width"=>"14"),
				"W"=>array("title"=>"Cost_Variance","object"=>"calculate_costVariance","width"=>"14"),
				"X"=>array("title"=>"Total_Calc","object"=>"calculate_total","width"=>"14"),
				"Y"=>array("title"=>"Total_Variance","object"=>"calculate_totalVariance","width"=>"14")
			);


		$AllJobsHeader = 
			array(
				"A"=>array("title"=>"Order#","object"=>"calculate_rowId","width"=>9),
				"B"=>array("title"=>"Project ID","object"=>"Upgrade__c.Multifamily_Job_ID__c","width"=>12),
				"C"=>array("title"=>"Project Name","object"=>"Upgrade__c.Name","width"=>28),
				"D"=>array("title"=>"Green Prospect Type","object"=>"Upgrade__c.Green_Prospect_Type__c","width"=>43),
				"E"=>array("title"=>"Address","object"=>"Account.BillingAddress","width"=>22),
				"F"=>array("title"=>"Town","object"=>"Account.BillingCity","width"=>17),
				"G"=>array("title"=>"Zip Code","object"=>"Account.BillingPostalCode","width"=>8),
				
				"H"=>array("title"=>"Phone","object"=>"Account.Phone","width"=>16),
				"I"=>array("title"=>"Master Meters?","object"=>"calculate_masterMeters","width"=>15),
				"J"=>array("title"=>"Date of Install","object"=>"Upgrade__c.GP_Stage_Completed__c","width"=>16),
				"K"=>array("title"=>"Included in Invoice (Month)","object"=>"calculate_invoiceMonth","width"=>16),
				"L"=>array("title"=>"Primary Heating Fuel","object"=>"Account.Heating_Fuel_Type__c","width"=>11),
				"M"=>array("title"=>"Total Dwelling Units","object"=>"Account.Total_Dwelling_Units__c","width"=>9),
				"N"=>array("title"=>"# Units in Contract","object"=>"Upgrade__c.Dwelling_Units_Affected__c","width"=>9),
				"O"=>array("title"=>"Job Included Audit?","object"=>"calculate_includedAudit","width"=>9),
				"P"=>array("title"=>"Type of Audit","object"=>"calculate_typeAudit","width"=>18)
			);
			
			
		$rowColors = array("FFE680","F2D9E6","E6F2FF","B3E6CC","F0F0E0","B0F0F","E0E8F0","60C8A0","D07090","D0C0D0","90C830","F0FFFF","60A0A0");	
		$rowCounter = 2; //skip first row for header
		$projectIdCount = 0;
		$colorCount = 0;
		//add some rows
		$Adjustments = array();
		foreach ($reportRowsRaw as $rowId=>$rowDetails){
			$projectName = $rowDetails["Upgrade__c.Name"];
			if (strpos($projectName,"-DI")){
				$JobNameParts = explode("-",$projectName);
			}elseif (strpos($projectName," DI")){
				$JobNameParts = explode(" DI",$projectName);
			}else{
				$JobNameParts = explode("-",$projectName);
			}
			$JobName = trim($JobNameParts[0]);
			$accountId = $rowDetails["accountId"];
			//print_r($greenProspects); //ARRAY DESCRIBE ids associated to an account when doing a lookup for attachments associated to this as the parentId;
			$address = $rowDetails["Account.BillingAddress"];
			$completedDateStrToTime = strtotime($rowDetails["Upgrade__c.GP_Stage_Completed__c"]);
			$invoiceMonth = date("F",strtotime(date("Y")."-".(date("m",$completedDateStrToTime)+(date("d",$completedDateStrToTime) > 21 ? 1 : 0))."-01"));
			if ($JobName == $JobNameLast){
				$auditType = $auditType;
				$auditIncludedDisplay = $auditIncludedDisplay;
			}else{
				$auditType = "";
				$auditIncludedDisplay = "";
				$rowColor[$JobName] = $colorCount;
				$colorCount++;
			}
			
			$auditIncluded = (strpos(strtolower($rowDetails["Upgrade__c.Green_Prospect_Type__c"]),"audit") ? true : false);
			if ($auditIncluded){
				$auditType = ($auditType ? $auditType : ($rowDetails["Account.Heating_Fuel_Type__c"]=="Electric" ? "WholeBldg" : "PiggyBack"));
				$auditIncludedDisplay = "yes";
				$greenProspectsWithAudits[$JobName][]=$accountId;
			}else{
				$greenProspects[$JobName][]=$accountId;
				$greenProspectsAccountId[$JobName][$accountId][]=$rowDetails["Upgrade__c.Green_Prospect_Type__c"];
				$auditType = "n/a";
				$auditIncludedDisplay = "no";
			}
			
			foreach ($AllJobsHeader as $column=>$headerData){
				$dataDisplay = $rowDetails[$headerData["object"]];
				if (strpos(" ".$headerData["object"],"calculate")){
					$calculateType = str_replace("calculate_","",$headerData["object"]);
					switch ($calculateType){
						case "rowId":
							$dataDisplay = ($rowCounter-1);
							break;
						case "invoiceMonth":
							$dataDisplay = $invoiceMonth;
							break;
						case "includedAudit":
							$dataDisplay = $auditIncludedDisplay;
							break;
						case "typeAudit":
							$dataDisplay = $auditType;
							break;
						case "masterMeters":
							$dataDisplay = "";
							break;
						default:
							$dataDisplay = $headerData["object"];
							break;
					}
				}else{
					$objectType = $headerData["object"];
					switch ($objectType){
						case "Upgrade__c.Multifamily_Job_ID__c";
							//switch to new naming format
							//if ($dataDisplay == "-"){
								if ($JobName != $JobNameLast){
									$projectIdCount++;
								}
								$dataDisplay = date("Ym-").$projectIdCount;
								$Adjustments["ALL_JOBS"][$JobName]["ProjectID"] = "Project ID set to ".$dataDisplay;
								//print_r($Adjustments); //ARRAY DESCRIBE show anytime we alter data for export what was changed;
							//}
							$ProjectIDs[$JobName] = $dataDisplay;
							//print_r($ProjectIDs); //ARRAY DESCRIBE show projectId based on short Account Name
							$AccountNamesByProjectId[$dataDisplay] = $JobName;
							//print_r($AccountNamesByProjectId);  //ARRAY DESCRIBE lookup short accountName by projectId
							break;
						case "Account.BillingAddress":
							$dataDisplay = str_replace("<br>",", ",$dataDisplay);
							break;
						case "Account.BillingCity":
							$AccountTownLookup[$JobName] = $dataDisplay;
							//print_r($AccountTownLookup); //ARRAY DESCRIBE lookup town by accountName used in ALL_MEASURES;
							break;
						case "Account.BillingPostalCode":
							if ($dataDisplay == "-"){
								$dataDisplay = retrieve_zipcode($address,GOOGLE_APIKEY);
								$Adjustments["ALL_JOBS"][$JobName]["PostalCode"] = "PostalCode set to ".$dataDisplay;
								//print_r($Adjustments); //ARRAY DESCRIBE show anytime we alter data for export what was changed;
							}
							break;
						case "Account.Heating_Fuel_Type__c":
							if ($dataDisplay == "Electric"){
								$dataDisplay = "E";
							}elseif ($dataDisplay == "Natural Gas"){
								$dataDisplay = "G";
							}else{
								$dataDisplay = $dataDisplay;
							}
							break;
						case "Upgrade__c.Green_Prospect_Type__c":
							if ($dataDisplay == "883 Audit"){
								$AccountHasAudit[$JobName]=array("accountId"=>$accountId,"GreenProspect"=>$dataDisplay,"HeatingFuel"=>$rowDetails["Account.Heating_Fuel_Type__c"]);
								//print_r($AccountHasAudit); //ARRAY DESCRIBE get list of Accounts with audits to make sure all audits are also included on measures since they do not have an attachment file
							}
							break;
						case "Upgrade__c.GP_Stage_Completed__c";
							$dataDisplay = date("m/d/Y",$completedDateStrToTime);
							break;
						default:
							$dataDisplay = $dataDisplay;
							break;
					}
				}
				$reportRows[$rowId][$column.$rowCounter]=$dataDisplay;
				//print_r($reportRows); //ARRAY DESCRIBE these are the rows used for ALL_JOBS sheet
			}
			$JobNameLast = $JobName;
			$rowCounter++;	
		}
//	print_pre($greenProspectsAccountId);	
		//now get attachments for each of the projects
		include_once('salesforce/soap_connect.php');
		foreach ($greenProspects as $accountName=>$parentIds){
			//get Contractor Names associated with the Green Prospects
			foreach ($parentIds as $idLookup){
				$query = "SELECT Contractor__c, Account__c FROM Upgrade__c where Id = '".$idLookup."'";
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				foreach ($response->records as $record){
					if ($record->Contractor__c){
						$ContractorLookup[$idLookup]=$record->Contractor__c;
						//print_pre($ContractorLookup); //ARRAY Get Contractor Field name associated with Green Prospect based on parentIds
					}
				}
			}

			
			
			$ids = implode("','",$parentIds);
			//echo $accountName." = ".$ids."<br>";
			$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId IN ('".$ids."')";
			$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
			//print_r($response);
			if (count($response->records)){
				foreach ($response->records as $record) {
					$parentId = $record->ParentId;
					$name = $record->Name;
					$id = $record->Id;
					$fileParts = explode(".",$name);
					$Attachments[$parentId][] = array("Id"=>$id,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts,"ContractorName"=>$ContractorLookup[$parentId]);
					//print_r($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
				}
			}else{
				$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
				$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
			}
			$query = "SELECT Name FROM Account WHERE Id IN (SELECT Account__c FROM Upgrade__c WHERE Id ='".$parentIds[0]."')";
			//$query = "SELECT Account__c FROM Upgrade__c WHERE Id ='".$parentIds[0]."'";
			$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
			foreach ($response->records as $record) {
				$AccountNamesLookup[trim($accountName)] = $record->Name;
				//print_r($AccountNamesLookup); //ARRAY DESCRIBE used to get the full account name from the Green Prospect shorthand form
			}
			
			//print_pre($response->records);

		}
//		print_pre($ContractorLookup);
//		print_pre($Attachments);
		
		foreach ($greenProspectsWithAudits as $accountName=>$parentIds){
			$id = $parentIds[0];
			$Attachments[$id][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
			$query = "SELECT Name FROM Account WHERE Id IN (SELECT Account__c FROM Upgrade__c WHERE Id ='".$id."')";
			//$query = "SELECT Account__c FROM Upgrade__c WHERE Id ='".$parentIds[0]."'";
			$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
			foreach ($response->records as $record) {
				$AccountNamesLookup[trim($accountName)] = $record->Name;
				//print_r($AccountNamesLookup); //ARRAY DESCRIBE used to get the full account name from the Green Prospect shorthand form
			}
		}
//		print_pre($Attachments);
		foreach ($Attachments as $parentId=>$attachmentList){
			foreach ($attachmentList as $attachmentInfo){
				if ($attachmentInfo["NoAttachments"]){
					$attachmentResults[$attachmentInfo["Name"]]["NoAttachments"] = true;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["HeatingFuel"] = $attachmentInfo["HeatingFuel"];
					$attachmentResults[$attachmentInfo["Name"]]["GreenProspect"] = $attachmentInfo["GreenProspect"];
				}else{
					//echo $instance_url." ".$attachmentInfo["Id"]." ".$attachmentInfo["Name"]."<Br>";
					$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
					$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $attachment;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $attachmentInfo["FileParts"][0];
					$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
					$attachmentResults[$attachmentInfo["Name"]]["ContractorName"] = $attachmentInfo["ContractorName"];
					//print_r($attachmentResults); //ARRAY DESCRIBE results and details of downloaded files
				}
			}
		}
		//print_pre($attachmentResults);
		//Get contractor names from pdf files
		foreach ($attachmentResults as $attachmentName=>$attachmentParts){
			$ContractorName = $attachmentParts["ContractorName"] ? : "CET";
			$SavedName = $attachmentParts["SavedName"];
			$fileInfo = array("fileName"=>$SavedName,"contractor"=>$ContractorName);
			if (trim($SavedName)){
				$ContractorAttachmentLookupByFileName[$SavedName]=$ContractorName;
			}

		}
			
		ob_flush();
		flush();
		foreach ($attachmentResults as $AttachmentName=>$AttachmentSavedItem){
			$AttachmentSavedName = $AttachmentSavedItem["SavedName"];
			$AttachmentAccountName = $AttachmentSavedItem["AccountName"];
			$NoAttachments = $AttachmentSavedItem["NoAttachments"];
			if (substr($AttachmentSavedName,-4)=="xlsx" && !strpos($AttachmentSavedName,"AccountList")){
				$ToParse[$AttachmentAccountName][] = $AttachmentSavedName;
			}
			if ($NoAttachments){
				$ToParse[$AttachmentAccountName."_NoAttachment"] = $AttachmentSavedItem;
			}
			if (substr($AttachmentSavedName,-3)=="pdf"){
					$thisFileLocation = $siteRoot.$adminFolder."gbs/salesforce/attachments/".$AttachmentSavedName;
					unlink($thisFileLocation);
			}
		}
		//parse the downloaded files
		//print_pre($ToParse);
		foreach ($ToParse as $accountname=>$fileNames){
			if (strpos($accountname,"_NoAttachment")){
				$measureDetail = ($fileNames["HeatingFuel"] == "Electric" ? "Whole Building Energy Assessment" : "PiggyBack Site Visit");
				$auditCost = ($fileNames["HeatingFuel"] == "Electric" ? 375 : 100);
				$rowsRaw = array(
					"EFI Item Number"=>"n/a",
					"Measure Description"=>$measureDetail,
					"Measure Category"=>$measureDetail,
					"Qty"=>1,
					"Cost/Unit"=>$auditCost,
					"Incentive/Unit"=>$auditCost,
					"kWh Savings/Unit"=>"0",
					"Measure Life"=>"0",
					"Demand kWh Reduction"=>"",
					"Annual kWh Savings"=>"0",
					"Lifetime kWh Savings"=>"0",
					"Total Cost"=>$auditCost,
					"Total Incentive"=>$auditCost
				);
				$rowsClean = array();
				$rowsClean[] = $rowsRaw;
				$parsedRows[str_replace("_NoAttachment","",$accountname)][$fileNames["GreenProspect"]] = $rowsClean;
			}else{
				foreach ($fileNames as $fileName){
					include('salesforce/read_attachmentfile.php');
					$parsedRows[$accountname][$fileName] = $rowsClean;
					$thisFileLocation = $siteRoot.$adminFolder."gbs/salesforce/attachments/".$fileName;
					unlink($thisFileLocation);
					//print_r($parsedRows); //ARRAY DESCRIBE rows parsed into an array
				}
			}
		}
		ob_flush();
		flush();
		//print_pre($parsedRows);
		//get MasterPriceList
		$masterpricelistResults = $GBSProvider->invoiceTool_getEversourceMasterPricelist();
		$masterpricelistCollection = $masterpricelistResults->collection;
		foreach ($masterpricelistCollection as $result){
			$EversourceMasterPriceByEFI[$result->EFI_Item_Number] = $result;
			$EversourceMasterPriceByDescription[strtolower(trim($result->Measure_Description))] = $result;
		}
	
		$rowCounter =2;	//reset to use second row
		$rowId = 0;
		foreach ($ProjectIDs as $accountName=>$projectId){
			if (count($parsedRows[$accountName])){ //if just an audit is recorded there will not be an attachment
				foreach ($parsedRows[$accountName] as $savedFileName=>$rowDetail){
					foreach ($rowDetail as $rowDetails){
						$EFI = rtrim($rowDetails["EFI Item Number"],"M");
						$MeasureDescription = strtolower(trim($rowDetails["Measure Description"]));
						$isAudit = false;
						if ($rowDetails["Measure Description"] == "Whole Building Energy Assessment" || $rowDetails["Measure Description"] =="PiggyBack Site Visit"){
							$isAudit = true;
						}
						if (strcmp(trim($rowDetails["Incentive/Unit"]),trim($rowDetails["Cost/Unit"]))){
							$Adjustments["ALL_MEASURES"][$accountName]["row ".($rowId+1).": ".$rowDetails["Measure Description"]][] = "Incentive/Unit (".$rowDetails["Incentive/Unit"].") did not match Cost/Unit (".$rowDetails["Cost/Unit"].")";
						}
						if (strcmp(trim($rowDetails["Total Incentive"]),trim($rowDetails["Total Cost"]))){
							$Adjustments["ALL_MEASURES"][$accountName]["row ".($rowId+1).": ".$rowDetails["Measure Description"]][] = "Total Incentive (".$rowDetails["Total Incentive"].") did not match Total Cost (".$rowDetails["Total Cost"].")";
						}
						if (strtolower($EFI) != "n/a"){
							if (strcmp(number_format($rowDetails["Cost/Unit"],2),$EversourceMasterPriceByEFI[$EFI]->Cost_Unit)){
								$Adjustments["ALL_MEASURES"][$accountName]["row ".($rowId+1).": ".$rowDetails["Measure Description"]][] = "Cost/Unit (".number_format($rowDetails["Cost/Unit"],2).") did not match Master Price List Cost/Unit (".$EversourceMasterPriceByEFI[$EFI]->Cost_Unit.")";
							}
							if (strcmp(number_format($rowDetails["Incentive/Unit"],2),$EversourceMasterPriceByEFI[$EFI]->Incentive_Unit)){
								$Adjustments["ALL_MEASURES"][$accountName]["row ".($rowId+1).": ".$rowDetails["Measure Description"]][] = "Incentive/Unit (".number_format($rowDetails["Incentive/Unit"],2).") did not match Master Price List Incentive/Unit (".$EversourceMasterPriceByEFI[$EFI]->Incentive_Unit.")";
							}
						}
						
						foreach ($AllMeasuresHeader as $column=>$headerData){
							//$dataDisplay = ($rowDetails[$headerData["object"]] ? $rowDetails[$headerData["object"]] : "");
							$dataDisplay = trim($rowDetails[$headerData["object"]]);
							if (strpos(" ".$headerData["object"],"calculate")){
								$calculateType = str_replace("calculate_","",$headerData["object"]);
								switch ($calculateType){
									case "rowId":
										$dataDisplay = ($rowCounter-1);
										break;
									case "projectId":
										$dataDisplay = $projectId;
										break;
									case "actualAccountName":
										$dataDisplay = $AccountNamesLookup[$accountName];
										break;
									case "town":
										$dataDisplay = $AccountTownLookup[$accountName];
										break;
									case "contractor":
										$dataDisplay = ($ContractorAttachmentLookupByFileName[$savedFileName] ? $ContractorAttachmentLookupByFileName[$savedFileName] : "CET");
										break;
									case "invoiceMonth":
										$dataDisplay = $invoiceMonth;
										break;
									case "clinc":
										if (strtolower($EFI) != "n/a"){
											$dataDisplay = $EversourceMasterPriceByEFI[$EFI]->INorIM;
										}else{
											$dataDisplay = $EversourceMasterPriceByDescription[$MeasureDescription]->INorIM;
										}
										break;
									default:
										$dataDisplay = $headerData["object"];
										break;
								}
							}else{
								$objectType = $headerData["object"];
								switch ($objectType){
									case "Measure Category":
										$dataDisplay = ($dataDisplay ? $dataDisplay : $rowDetails["Measure Description"]);
										$dataDisplay = ($dataDisplay == "7005.609M" ? $dataDisplay = "Smart Strip" : $dataDisplay);
										break;
									case "Demand kWh Reduction":
										if (strtolower($EFI) != "n/a"){
											$dataDisplay = ($dataDisplay ? $dataDisplay : $EversourceMasterPriceByEFI[$EFI]->Dmd_kWh_Unit);
										}else{
											$dataDisplay = ($dataDisplay ? $dataDisplay : $EversourceMasterPriceByDescription[$MeasureDescription]->Dmd_kWh_Unit);
										}
										break;
									default:
										$dataDisplay = $dataDisplay;
										break;
								}
							}
							$measureRows[$rowId][$column.$rowCounter]=$dataDisplay;
							//print_r($reportRows); //ARRAY DESCRIBE these are the rows used for ALL_JOBS sheet
						}//end foreach AllMeasuresHeader
						$rowCounter++;
						$rowId++;
					}//end foreach rowDeatils
				}//end foreach parsedRows
			}else{
				//print_pre($AccountHasAudit[$accountName]);
				//Do with Only Audit
			}//end if count
		}
		//create the trackingFile
		include_once('salesforce/reports/report883.php');
		echo "<br><h3>Eversource Invoice 883 Report</h3>";
		$SaleforceTabCodes = array("Raw Data"=>"RawData","Audits"=>"Audits","Attachments"=>"Attachments","Tracking File"=>"TrackingFile","Adjustments"=>"Adjustments");
		foreach ($SaleforceTabCodes as $TabName=>$TabCode){
			$SalesForceHashes[] = $TabCode;
			$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
		}
		echo '<ul id="admin-tabs" class="tabs">';
				foreach ($TabListItems as $TabItem){
					echo $TabItem;
				}
		echo '</ul>';		
		echo '<div class="tabs-content">';
			echo "<div id='RawData' class='tab-content'>
				Raw Data from report 883:
				<table class='simpleTable'><thead><tr>";
				foreach ($fieldNamesArray as $fieldName=>$fieldInfo){
					echo "<th>".$fieldInfo["label"]."</th>";
				}
				echo "<th>Attachment ParentId</th>";
				echo "</tr></thead><tbody>";
				foreach ($reportRowsRaw as $rowId=>$rowDetails){
					echo "<tr>";
					foreach ($rowDetails as $fieldName=>$fieldValue){
						echo "<td nowrap='nowrap'>".$fieldValue."</td>";
						//echo "<td>".$fieldNamesArray[$fieldName]["label"].": ".$fieldValue."<br>";
					}
					echo "</tr>";
					
				}
				echo "</tbody></table><Br>";
			echo "</div>";
			
			echo "<div id='Audits' class='tab-content'>";
				echo "The Following Projects had Audits included in the Salesforce Data:";
				foreach ($AccountHasAudit as $projectName=>$projectData){
					echo "<br>".$projectName." - ".$projectData["GreenProspect"]." ".$projectData["HeatingFuel"];
				}
			echo "</div>";

			echo "<div id='Attachments' class='tab-content'>";
				echo "The Following Attachments were downloaded:<br>
						<table class='simpleTable'><thead><tr><th>Project</th><th>FileName</th><th>Saved Name</th></thead></tr><tbody>";
				foreach ($attachmentResults as $AttachmentName=>$AttachmentSavedItem){
					$AttachmentSavedName = $AttachmentSavedItem["SavedName"];
					$AttachmentAccountName = $AttachmentSavedItem["AccountName"];
					$NoAttachments = $AttachmentSavedItem["NoAttachments"];
					if ($NoAttachments){
						echo "<tr><td>".$AttachmentAccountName."</td><td>no attachments</td><td>".$AttachmentSavedItem["HeatingFuel"]." ".$AttachmentSavedItem["GreenProspect"]."</td></tr>";
						//echo $AttachmentAccountName." had no attachments but here is what it did have: ";
						//print_pre($AttachmentSavedItem);
					}else{
						echo "<tr><td>".$AttachmentAccountName."</td><td>".$AttachmentName."</td><td>".$AttachmentSavedName."</td></tr>";
					}
				}
				echo "</tbody></table>";
			echo "</div>";
			
			echo "<div id='TrackingFile' class='tab-content'>";
				echo "Parsed File: ".$EverSourceTrackingFileLink;
				echo "Open this file and make any necessary edits<br>Then upload approved file here:<br>";
				?>
					<form method="post" action="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Eversource&type=trackingFile" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" onChange="makeFileList();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton" style="display:none;">
							</div>
						</div>
					
						<script type="text/javascript">
							function makeFileList() {
								var input = document.getElementById("filesToUpload");
								var ul = document.getElementById("fileList");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton').style.display = 'none';
								}
							}
						</script>
					</form>
			<?php
			echo "</div>";
			

			echo "<div id='Adjustments' class='tab-content'>";
				echo "<Br><br>The Following Adjustments were made:";
				echo "<table class='simpleTable'><thead><tr><th>Tab</th><th>Project Name</th><th>Item</th><th>Details</th></tr></thead><tbody>";
				foreach ($Adjustments as $tabName=>$ProjectDetails){
					foreach ($ProjectDetails as $ProjectName=>$Items){
						foreach ($Items as $Item=>$details){
							echo "<tr><td nowrap='nowrap'>".$tabName."</td><td nowrap='nowrap'>".$ProjectName."</td><td nowrap='nowrap'>".$Item."</td><td nowrap='nowrap'>";
							if (is_array($details)){
								echo implode("<hr>",$details);
							}else{
								echo $details;
							}
							echo "</td></tr>";
						}
					}
					
				}
				echo "</tbody></table>";
			echo "</div>";
		echo '</div>'; //end tabbed content

}else{
	echo "<script>window.location.replace('".AUTH_URL."');</script>";
	//echo AUTH_URL;
	//header('Location: '.AUTH_URL);
}
?>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$SalesForceHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("Adjustments");
		}
	});
</script>
