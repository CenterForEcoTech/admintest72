<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run


$file = fopen($ImportFileName,"r");
$rowCounter = 0;
while(! feof($file))
  {
	  $thisRow = fgetcsv($file,0,"|");
	  if ($rowCounter < 1){
		 $headerRow = $thisRow; 
	  }else{
		$dataRows[] =  $thisRow;
	  }
	  $rowCounter++;
  }
fclose($file);
foreach ($dataRows as $rowID=>$rowData){
	$cleanedRow = array();
	foreach ($rowData as $col=>$val){
		$cleanedRow[$headerRow[$col]]=$val;
	}
	if ($cleanedRow["EXT_JOB_ID"]){
		$cleanedRows[] = $cleanedRow;
	}
}
foreach ($headerRow as $col=>$val){
	$headerNames[($col+1)]=$val;
	$headerNumbers[($col+1)]=str_replace(" ","",$val);
}
$rowsRaw = $cleanedRows;

//print_pre($headerNames);
//print_pre($rowsRaw);
?>