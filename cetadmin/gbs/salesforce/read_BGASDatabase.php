<?php
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = $trackingFile;

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get MDAR worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$headerRow = $sheet->rangeToArray('A1:'.$highestColumn.'1', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	//since headers vary from month to month from the file, standardize them
	$val = strtolower($val);
	if (strpos(" ".$val,"business")){$val = "BusinessPartner";}
	if (strpos(" ".$val,"tele")){$val = "Phone";}
	if (strpos(" ".$val,"account")){$val = "Account";}
	if ($val=="premise"){$val = "Premise";}
	if (strpos(" ".$val,"rate")){$val = "Rate";}
	if (strpos(" ".$val,"first")){$val = "FirstName";}
	if (strpos(" ".$val,"last")){$val = "LastName";}
	if (strpos(" ".$val,"full")){$val = "StreetAddress";}
	if (strpos(" ".$val,"concat")){$val = "StreetAddress";}
	if ($val=="unit"){$val = "Unit";}
	if ($val=="city"){$val = "City";}
	if (strpos(" ".$val,"house")){$val = "HouseNumber";}
	if (strpos(" ".$val,"street")){$val = "StreetName";}
	if ($val=="state"){$val = "State";}
	if (strpos(" ".$val,"zip")){$val = "ZipCode";}
	if (strpos(" ".$val,"meter")){$val = "Meter";}
	if ($val=="device"){$val = "Device";}
	if (strpos(" ".$val,"billing")){$val = "BillingPortion";}
	if (strpos(" ".$val,"begin")){$val = "BillingPeriodBegin";}
	if (strpos(" ".$val,"end")){$val = "BillingPeriodEnd";}
	if ($val=="amount"){$val = "Amount";}
	if ($val=="therms"){$val = "Therms";}

	$headerNumbers[($col+1)]=$val;
}
//print_pre($headerNumbers);
//  Loop through each row of the worksheet in turn
for ($row = 2; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	foreach($rowData[0] as $k=>$v){
		//echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$BGASDatabase[($row-2)][$headerNumbers[($k+1)]] = $v;
	}
}
echo ($highestRow-1)." BGAS Database rows have been read and processed<br>";
echo count($BGASDatabase)." rows to be inserted<br>";
foreach ($BGASDatabase as $rowID=>$record){
	$sqlrow = floor($rowID/400);
	$sqlVal = "(";
	foreach ($record as $key=>$val){
		$sqlVal .= "'".addslashes($val)."',";
	}
	$sqlVal = rtrim($sqlVal,",").")";
	$sqls[$sqlrow][] = $sqlVal;
	//print_pre($record);
	//$insertResult = $GBSProvider->addBGASCustomer($record, getAdminId());
}

//first clear out old data:
$deleteResult = $GBSProvider->deleteBGASCustomers();
if ($deleteResult->success){echo "Old records removed<br>";}
//print_pre($sqls);
foreach ($sqls as $sqlGroup => $values){
//	print_pre($values);
	$insertResult = $GBSProvider->addBGASCustomerSQL($values, $headerNumbers);
	$affectedRows = $affectedRows+$insertResult->affectedRows;
//	$sqlcount++;
//	if ($sqlcount ==2){break;}
}
echo $affectedRows. " rows successfully added<br>";
?>