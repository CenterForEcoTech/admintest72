<?php
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/DEPInternalReport.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get MDAR worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$columnsArray = array("C","D","F");
foreach ($columnsArray as $column){
	$rowId = 18;
	while ($rowId < 28){
        try {
            $code = $objPHPExcel->getActiveSheet()->getCellValue("A" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $DEPInternalValues[$code][$column] = $objPHPExcel->getActiveSheet()->getCellValue($column . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
	}
}
print_pre($DEPInternalValues);
//print_pre($NONData);
//print_pre($NONDataNormalized);
?>