<?php //phpinfo();
error_reporting(E_ALL & ~E_NOTICE);

$spath = getcwd();
if (!strpos($spath,"xampp")){
	$spath = "/home/cetdash/cetdashboard.info/";
}else{
	$spath = $_SERVER["DOCUMENT_ROOT"];
}



// session_start() has to go right at the top, before any output!
unset($_SESSION['enterpriseSessionId']);
require_once (/sforce-php/soapclient/SforceEnterpriseClient.php);

$mySforceConnection = new SforceEnterpriseClient();
$mySforceConnection->createConnection($spath."/sforce-php/soapclient/enterprise.wsdl.xml");

// Simple example of session management - first call will do
// login, refresh will use session ID and location cached in
// PHP session
if (isset($_SESSION['enterpriseSessionId'])) {
	$location = $_SESSION['enterpriseLocation'];
	$sessionId = $_SESSION['enterpriseSessionId'];

	$mySforceConnection->setEndpoint($location);
	$mySforceConnection->setSessionHeader($sessionId);

} else {
	$mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);

	$_SESSION['enterpriseLocation'] = $mySforceConnection->getLocation();
	$_SESSION['enterpriseSessionId'] = $mySforceConnection->getSessionId();

}
?>