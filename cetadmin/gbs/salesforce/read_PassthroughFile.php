<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run

require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "").$ImportFileName;
//echo "opening ".$inputFileName."<br>";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(1);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$JobIDColumn = "AA";

$headerRow = $sheet->rangeToArray("A1:".$JobIDColumn."1", NULL, TRUE, FALSE);
//print_pre($headerRow);
foreach ($headerRow[0] as $col=>$val){
	//if ($col == 26){$val = "JobID";} //reactivated 8/7/2017
	if (strtolower($val) == "project"){$val = "JobID";}
	$headerNames[getNameFromNumber($col+1)]=$val;
	$headerNumbers[($col+1)]=str_replace("#","",str_replace(" ","",$val));
}
//print_pre($headerNumbers);
//print_pre($headerNames);
//  Loop through each row of the worksheet in turn
for ($row = 2; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':'.$JobIDColumn . $row, NULL, TRUE, FALSE);
    foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$rowsRaw[($row-3)][$headerNumbers[($k+1)]] = $v;
	}
}
//print_pre($rowsRaw);
//cleanup dates
foreach ($rowsRaw as $rowId=>$rowDetails){
	//workaround for column renames
	if ($rowDetails["Date"]){
		$rowDetails["Date"] = exceldatetotimestamp($rowDetails["Date"]);
//		if (trim($rowDetails["AccountID"]) && trim($rowDetails["TransDescription"]) != "Beginning Balance" && trim($rowDetails["TransDescription"]) != "Ending Balance" && trim($rowDetails["Jrnl"])!=""){
			$rowsClean[] = $rowDetails;
			$jobIDs[$rowDetails["JobID"]]["count"]++;
			$jobIDs[$rowDetails["JobID"]]["amount"] = bcadd($jobIDs[$rowDetails["JobID"]]["amount"],$rowDetails["Debit"],2);
//		}
	}
}
?>