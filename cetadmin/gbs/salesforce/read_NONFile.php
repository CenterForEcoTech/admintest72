<?php
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = $trackingFile;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get MDAR worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$headerRow = $sheet->rangeToArray('A1:'.$highestColumn.'1', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	$val = strtolower($val);
	$headerNumbers[($col+1)]=$val;
}
//  Loop through each row of the worksheet in turn
for ($row = 2; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	foreach($rowData[0] as $k=>$v){
		//echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		if (strpos($headerNumbers[($k+1)],"_date")){
			if ($v){
				$v = exceldatetotimestamp($v);
			}
		}
		$NONData[($row-2)][$headerNumbers[($k+1)]] = $v;
		//create normalized name data
	}
	$NONDataNormalized[normalizeAccountName($rowData[0][2])]=$NONData[($row-2)];
}
//print_pre($NONData);
//print_pre($NONDataNormalized);
?>