<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run

require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "").$ImportFileName;
//echo "opening ".$inputFileName."<br>";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$headerRow = $sheet->rangeToArray('A1:'.$highestColumn.'1', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	$headerNames[getNameFromNumber($col+1)]=$val;
	$headerNumbers[($col+1)]=str_replace(" ","",$val);
}

//  Loop through each row of the worksheet in turn
for ($row = 2; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':'.$highestColumn . $row, NULL, TRUE, FALSE);
    foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$rowsRaw[($row-2)][$headerNumbers[($k+1)]] = str_replace(chr(194),"",$v);
	}
}
//print_pre($rowsRaw);
?>