<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("Etrac Title")
							 ->setSubject("Etrac Subject")
							 ->setDescription("Etrac Description");
//START ALL_MEASURES
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($AllMeasuresHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($headerData["width"]);
    } catch (PHPExcel_Exception $e) {
    }
}
foreach ($measureRows as $rowId=>$columnCounter){
	$thisLastRow = ($rowId+2);
	$projectId = $columnCounter["B".$thisLastRow];
	$accountName = $AccountNamesByProjectId[$projectId];
	foreach ($columnCounter as $cell=>$dataDisplay){
		switch ($dataDisplay){
			case "calculate_cost":
				$dataDisplay = "=K".$thisLastRow."*L".$thisLastRow;
				break;
			case "calculate_costVariance";
				$dataDisplay = '=IF(V'.$thisLastRow.' = S'.$thisLastRow.',"",(V'.$thisLastRow.'-S'.$thisLastRow.'))';
				break;
			case "calculate_total";
				$dataDisplay = "=K".$thisLastRow."*M".$thisLastRow;
				break;
			case "calculate_totalVariance";
				$dataDisplay = '=IF(X'.$thisLastRow.' = T'.$thisLastRow.',"",(X'.$thisLastRow.'-T'.$thisLastRow.'))';
				break;
			default:
				$dataDisplay = $dataDisplay;
				break;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }

    }
	//style row
	//echo "Row Color: A".($rowId+2).":P".($rowId+2)." set to ".$rowColors[$rowColor[$accountName]]."<br>";
    try {
        $objPHPExcel->getActiveSheet()->getStyle("B" . $thisLastRow . ":P" . $thisLastRow)->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FF' . $rowColors[$rowColor[$accountName]])
            ),
                'borders' => array(
                    'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
                ),
                'font' => array('size' => 11)

            )
        );
    } catch (PHPExcel_Exception $e) {
    }

}
//style sheet
try {
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(45);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setAutoFilter('A1:T1');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("L2:M" . $thisLastRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""-"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("S2:T" . $thisLastRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""-"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("G2:R" . $thisLastRow)->applyFromArray(
        array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("Q2:R" . $thisLastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FFFFFF00')
        ),
            'borders' => array(
                'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
            'font' => array('size' => 11)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("S2:T" . $thisLastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FF90C830')
        ),
            'borders' => array(
                'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
            'font' => array('size' => 11)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:T1")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FF6699ff')
        ),
            'borders' => array(
                'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
            'font' => array('size' => 11,
                'color' => array('argb' => 'FFFFFFFF')
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("Q1:R1")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FF00ccff')
        ),
            'font' => array('size' => 11,
                'color' => array('argb' => 'FF000000')
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("S1:T1")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FF669900')
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}


$rowCounter++;
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('ALL_MEASURES');
} catch (PHPExcel_Exception $e) {
}
//END ALL_MEASURES
							 
							 
//START ALL_JOBS
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($AllJobsHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($headerData["width"]);
    } catch (PHPExcel_Exception $e) {
    }
}
foreach ($reportRows as $rowId=>$columnCounter){
	$LastRow = ($rowId+2);
	$projectId = $columnCounter["B".($rowId+2)];
	$accountName = $AccountNamesByProjectId[$projectId];
	foreach ($columnCounter as $cell=>$dataDisplay){
		//echo $AccountNamesLookup[$accountName]." ".$cell."=".$dataDisplay."<br>";
		if (substr($cell,0,1) == "C"){$dataDisplay = $AccountNamesLookup[$accountName];}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
}
//style
try {
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(45);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setAutoFilter('A1:P1');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:P1")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => 'FF6699ff')
        ),
            'borders' => array(
                'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
            'font' => array('size' => 11,
                'color' => array('argb' => 'FFFFFFFF')
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A2:P" . $LastRow)->applyFromArray(
        array('borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
            'font' => array('size' => 12)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('G1:G' . $LastRow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('G2:G' . $LastRow)->applyFromArray(
        array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('J2:J' . $LastRow)->applyFromArray(
        array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('M2:O' . $LastRow)->applyFromArray(
        array('alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}

// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('ALL_JOBS');
} catch (PHPExcel_Exception $e) {
}
//END ALL JOBS

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$trackingFile = "exportEverSourceTracking_".date("Y_m_d").".xlsx";

$pathExtension = "salesforce".$pathSeparator."exports".$pathSeparator.$trackingFile;
$webpathExtension = "salesforce".$pathSeparator."exports".$webpathSeparator.$trackingFile;
$saveLocation = $path.$pathExtension;


//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$EverSourceTrackingFileLink = "<a id='EverSourceTrackingFile' href='".$webtrackingLink."'>".$trackingFile."</a><br>";
?>