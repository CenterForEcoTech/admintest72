<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/SmallContractHours_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "SmallContract".$pathSeparator."SmallContractHours_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$ExcelTabArray = array("Haz Waste","FINK","Rathmann","College Fund C&I","SBSWMD","Xfer Station Inspection","Small Consult","Marketing&Outreach Hours");
foreach ($ExcelTabArray as $indexId=>$excelTabName){
	$excelTabNameParts = explode(" ",$excelTabName);
    try {
        $objPHPExcel->setActiveSheetIndex($indexId);
    } catch (PHPExcel_Exception $e) {
    }
    $excelTabCount = $EmployeesInGroup[$excelTabName];
	if ($excelTabCount > 1){
		$rowShift = $excelTabCount-1;
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	if ($excelTabName == "College Fund C&I"){
		$billingRateName = $excelTabNameParts[0];
		$columnShift = (count($CodeWithHoursInGroup[$excelTabName])-1);
        try {
            $objPHPExcel->getActiveSheet()->insertNewColumnBefore('E', $columnShift);
        } catch (PHPExcel_Exception $e) {
        }
        $columnNumber = 4;
		$columnTotal = "A"; //incase no hours
		$columnTotalHours = "B"; //incase no hours
		
//		print_pre($CodeWithHoursInGroup[$excelTabName]);
//		print_pre($EmployeesInGroupByEmployeeWithHours[$excelTabName]);
		foreach ($CodeWithHoursInGroup[$excelTabName] as $code=>$EmployeeInfo){
			$column = getNameFromNumber($columnNumber);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . "1", $code);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . "1")->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'FD9E2F3')
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
            $codeColumn[$code] = $column;
			$columnNumber++;
		}
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours[$excelTabName] as $employee_name=>$codeInfo){
			$EmployeeNameParts = explode("_",$employee_name);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
            } catch (PHPExcel_Exception $e) {
            }
            foreach ($codeInfo as $code=>$hours){
				$column = $codeColumn[$code];
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
            }
			if (!$BillingRateByEmployeeJustRate[$billingRateName][$employee_name]){
				$Adjustments["Alert"]["Staff missing Billing Rate '".$billingRateName."'"][]=str_replace(", ","_",$employee_name);
			}
			$categoryRate = $BillingRateByEmployeeJustRate[$billingRateName][$employee_name];
			$columnLastCode = getNameFromNumber((4+$columnShift));
			$columnTotalHours = getNameFromNumber((5+$columnShift));
			$columnRate = getNameFromNumber((6+$columnShift));
			$columnTotal = getNameFromNumber((7+$columnShift));
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($columnTotalHours . $rowId, "=SUM(D" . $rowId . ":" . $columnLastCode . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($columnRate . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($columnTotal . $rowId, "=" . $columnRate . $rowId . "*" . $columnTotalHours . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $hoursTotal = $objPHPExcel->getActiveSheet()->getCell($columnTotal . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            if (!in_array($excelTabName,$previouslyRan)){//only run a BillingRateType once if combined with other reports
				$revenueCodes[$excelTabName][$EmployeesRevenueCodeByName[$employee_name]] = bcadd($revenueCodes[$excelTabName][$EmployeesRevenueCodeByName[$employee_name]],$hoursTotal,2);
				$revenueCodesByEmployee[$excelTabName][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $hoursTotal;
			}
			$rowId++;

		}
		//add Totals Row
		foreach ($codeColumn as $code=>$column){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "=SUM(" . $column . "2:" . $column . ($rowId - 1) . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . $rowId)->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'FFFE598')
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($columnTotalHours . $rowId, "=SUM(" . $columnTotalHours . "2:" . $columnTotalHours . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($columnTotal . $rowId, "=SUM(" . $columnTotal . "2:" . $columnTotal . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
    }elseif ($excelTabName == "Small Consult"){
		$smallConsultColumn = array("550A"=>"D","550P"=>"E","550W"=>"F","633 West Hartford CT Waste"=>"G");
		foreach ($smallConsultColumn as $code=>$column){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . "1", $code);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . "1")->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'FD9E2F3')
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
            foreach ($InvoiceHourData[$code] as $employee_name=>$hours){
				$smallConsultRows[$employee_name][$code] = $hours;
			}
		}
		$rowId = 2;
		if (count($smallConsultRows)){
			foreach ($smallConsultRows as $employee_name=>$codeData){
				$EmployeeNameParts = explode("_",$employee_name);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
                } catch (PHPExcel_Exception $e) {
                }
                foreach ($codeData as $code=>$hours){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue($smallConsultColumn[$code] . $rowId, $hours);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($code == "550A"){
						$BillingRateType = "550A - Small Consulting";
						$categoryRate = 65;
						$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+bcmul($hours,$categoryRate,2);
						$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+bcmul($hours,$categoryRate,2);
					}
				}
				
				$rowId++;
			}
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=SUM(D2:D" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=SUM(E2:E" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=SUM(F2:F" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("D" . ($rowId) . ":F" . ($rowId))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFE598')
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }

    }elseif ($excelTabName == "Rathmann"){
		$rathmannColumn = array("1031"=>"D","1031PA"=>"G");
		foreach ($rathmannColumn as $code=>$column){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . "1", $code);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . "1")->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'FD9E2F3')
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
            foreach ($InvoiceHourData[$code] as $employee_name=>$hours){
				$rathmannRows[$employee_name][$code] = $hours;
			}
		}
		$rowId = 2;
		if (count($rathmannRows)){
			foreach ($rathmannRows as $employee_name=>$codeData){
				$EmployeeNameParts = explode("_",$employee_name);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
                } catch (PHPExcel_Exception $e) {
                }
                foreach ($codeData as $code=>$hours){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue($rathmannColumn[$code] . $rowId, $hours);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $billRateCode = "Rathmann";
					$BillingRateType = $CodesByName[$code]->description;
					$categoryRate = ($BillingRateByEmployeeJustRate[$billRateCode][$employee_name] ? $BillingRateByEmployeeJustRate[$billRateCode][$employee_name] : 160);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $categoryRate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=D" . $rowId . "*E" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $categoryRate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, "=G" . $rowId . "*H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if (!$BillingRateByEmployeeJustRate[$billRateCode][$employee_name]){
						$Adjustments["Alert"]["Staff missing Billing Rate '".$billRateCode."'"][]=str_replace("_",", ",$employee_name);
					}
					$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+bcmul($hours,$categoryRate,2);
					$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+bcmul($hours,$categoryRate,2);
				}
				
				$rowId++;
			}
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=SUM(D2:D" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=SUM(F2:F" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "=SUM(G2:G" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, "=SUM(I2:I" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("D" . ($rowId) . ":I" . ($rowId))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFE598')
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }elseif ($excelTabName == "Marketing&Outreach Hours"){
		//header Rows:
		$rowId = 1;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "LastName");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "FirstName");
        } catch (PHPExcel_Exception $e) {
        }
        $columnNumber = 3;
		foreach ($marketingDepartmentCodes as $codeName=>$id){
			$column = getNameFromNumber($columnNumber);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $codeName);
            } catch (PHPExcel_Exception $e) {
            }
            $columnNumber++;
		}
		$column = getNameFromNumber($columnNumber);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "TotalBillable");
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
		//employee rows
		foreach ($marketingDepartmentHours as $employeeName=>$codeInfo){
			$EmployeeNameParts = explode("_",$employeeName);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeeNameParts[0]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[1]);
            } catch (PHPExcel_Exception $e) {
            }
            $columnNumber = 3;
			foreach ($marketingDepartmentCodes as $codeName=>$id){
				$column = getNameFromNumber($columnNumber);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $codeInfo[$codeName]);
                } catch (PHPExcel_Exception $e) {
                }
                $lastColumn = $column;
				$columnNumber++;
			}
			$column = getNameFromNumber($columnNumber);
			//echo "=SUM(C".$rowId.":".$lastColumn.$rowId.")<br>";
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "=SUM(C" . $rowId . ":" . $lastColumn . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }


            $lastRow = $rowId;
			$rowId++;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Totals");
        } catch (PHPExcel_Exception $e) {
        }
        $columnNumber = 3;
		foreach ($marketingDepartmentCodes as $codeName=>$id){
			$column = getNameFromNumber($columnNumber);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "=SUM(" . $column . "2:" . $column . $lastRow . ")");
            } catch (PHPExcel_Exception $e) {
            }
            $columnNumber++;
		}
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":" . $column . $rowId)->getFont()->setBold(true);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":" . $column . $rowId)->applyFromArray(
                array('borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        $column = getNameFromNumber($columnNumber);
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . "1:" . $column . $rowId)->getFont()->setBold(true);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . "1:" . $column . $rowId)->applyFromArray(
                array('borders' => array(
                    'left' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }else{
		$columnNumber = 4;
		foreach ($CodesByGroupName[$excelTabName] as $code){
			if ($InvoiceHourDataByCode[$code]){
				$column = getNameFromNumber($columnNumber);

                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . "1", $code);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle($column . "1")->applyFromArray(
                        array('fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('argb' => 'FD9E2F3')
                        )
                        )
                    );
                } catch (PHPExcel_Exception $e) {
                }
                $rowId = 2;
				
				if (count($InvoiceHourData[$code])){
					$BillingRateType = $code;
					if (!in_array($BillingRateType,$previouslyRan)){//only run a BillingRateType once if combined with other reports
						if (count($InvoiceHourData[$code])){
							foreach ($InvoiceHourData[$code] as $employee_name=>$hours){
								$includeRevenueCode = false;
								$EmployeeNameParts = explode("_",$employee_name);
                                try {
                                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
                                } catch (PHPExcel_Exception $e) {
                                }
                                try {
                                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
                                } catch (PHPExcel_Exception $e) {
                                }
                                try {
                                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
                                } catch (PHPExcel_Exception $e) {
                                }
                                try {
                                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $hours);
                                } catch (PHPExcel_Exception $e) {
                                }
                                if ($excelTabName == "FINK" || $excelTabName == "Rathmann" || $excelTabName == "632 EPA CT Waste" || $excelTabName == "631 RBDG CT Waste" || $excelTabName == "Rhode Island Waste"){
									$includeRevenueCode = true;
									$billRateCode = ($excelTabName == "FINK" ? "Fink" : "FedGrant");
									$billRateCode = ($excelTabName == "Rathmann" ? "Rathmann" : $billRateCode);
									$BillingRateType = $CodesByName[$code]->description;
									$categoryRate = ($BillingRateByEmployeeJustRate[$billRateCode][$employee_name] ? $BillingRateByEmployeeJustRate[$billRateCode][$employee_name] : 65);
									if (!$BillingRateByEmployeeJustRate[$billRateCode][$employee_name]){
										$Adjustments["Alert"]["Staff missing Billing Rate '".$billRateCode."'"][]=str_replace("_",", ",$employee_name);
									}
									if ($excelTabName == "Rathmann"){
										echo $code." ".$employee_name." ".$rowId." hours: ".$hours."<br>";
										if ($code == "1031PA"){
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $hours);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $categoryRate);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, "=G" . $rowId . "*H" . $rowId);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                        }
										if ($code == "1031"){
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $hours);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $categoryRate);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                            try {
                                                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=D" . $rowId . "*E" . $rowId);
                                            } catch (PHPExcel_Exception $e) {
                                            }
                                        }
									}else{
                                        try {
                                            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $categoryRate);
                                        } catch (PHPExcel_Exception $e) {
                                        }
                                        try {
                                            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=D" . $rowId . "*E" . $rowId);
                                        } catch (PHPExcel_Exception $e) {
                                        }
                                    }
								}else{
									if ($code == "550A" || $code == "591"){
										$includeRevenueCode = true;
										$categoryRate = 65;
									}
								}
								if ($includeRevenueCode){
									if ($BillingRateType == "591"){$BillingRateType = "591 - SBSWMD";}
									$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+bcmul($hours,$categoryRate,2);
									$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+bcmul($hours,$categoryRate,2);
								}
								$rowId++;
							}
						}
					}//only run a BillingRateType once if combined with other reports

				}
				//add SummaryRow
				if ($excelTabName == "FINK" ||$excelTabName == "632 EPA CT Waste" || $excelTabName == "631 RBDG CT Waste" || $excelTabName == "Rhode Island Waste"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=SUM(F2:F" . ($rowId - 1) . ")");
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				if ($excelTabName != "College Fund C&I" && $excelTabName != "Rathmann"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "=SUM(" . $column . "2:" . $column . ($rowId - 1) . ")");
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle($column . $rowId)->applyFromArray(
                            array('fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('argb' => 'FFFE598')
                            )
                            )
                        );
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				$columnNumber++;
			}
			
		}
	}//end if College Fund
	$lastIndex = $indexId;
}

//Now Create Invoices
//591 Invoice
try {
    $objPHPExcel->setActiveSheetIndex(($lastIndex + 1));
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["591 - SBSWMD"] = "591-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["591 - SBSWMD"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A28", "Service Provided from " . date("n/j/y", strtotime($invoiceDate)) . " - " . date("n/t/y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A18", date("F", strtotime($invoiceDate)) . " 2016 Consulting Services:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F18", $InvoiceHourDataByCodeCategory["591"]["Staff"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A19", date("F", strtotime($invoiceDate)) . " 2016 Bookkeeping:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F19", $InvoiceHourDataByCodeCategory["591"]["Bookkeeper"]);
} catch (PHPExcel_Exception $e) {
}

//550A Invoice
try {
    $objPHPExcel->setActiveSheetIndex(($lastIndex + 2));
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["550A - Small Consulting"] = "550A-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["550A - Small Consulting"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A28", "Service Provided from " . date("n/j/y", strtotime($invoice550AStartDate)) . " - " . date("n/t/y", strtotime($invoice550AEndDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A18", "Recycling Consulting for " . date("F 1S", strtotime($invoice550AStartDate)) . " - " . date("F tS Y", strtotime($invoice550AEndDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F18", $InvoiceHourDataByCode["550A"]);
} catch (PHPExcel_Exception $e) {
}

$CETInternalFiscalRevenueSheetIndex = ($lastIndex+3);
$CETInternalFiscalRevenueSheetName = "SmallConsult";	
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');



//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
//$triggerResultFiles[] = $CurrentServer.$adminFolder."gbs/".$webpathExtension;
$triggerResultFiles["Small Consultant"] = $saveLocation;

?>