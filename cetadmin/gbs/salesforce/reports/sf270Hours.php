<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

//  Read your Excel workbook
$objPHPExcel = new PHPExcel();

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "SF270".$pathSeparator.$sf270FileName."_SF270Hours_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$saveLocation = ($isStaff ? str_replace("cetstaff","cetadmin",$saveLocation) : $saveLocation);
$webpathExtension = ($isStaff ? "../../cetadmin/gbs/" : "").$webpathExtension;
//echo $saveLocation;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$border = array(
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
$activeSheet = 0;
try {
    $objPHPExcel->setActiveSheetIndex($activeSheet);
} catch (PHPExcel_Exception $e) {
}
foreach ($SF270Hours as $sf270Name=>$employeeData){
	if ($activeSheet > 0){
        try {
            $objPHPExcel->createSheet($activeSheet);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->setActiveSheetIndex($activeSheet);
        } catch (PHPExcel_Exception $e) {
        }
    }
    try {
        $objPHPExcel->getActiveSheet()->setTitle($sf270Name);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId = 1;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, date("F", strtotime($invoiceDate)));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "INVOICE");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "DATE");
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $sf270Name);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $partialPayVar);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, date("m/15/Y"));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A1:C" . $rowId)->applyFromArray($BoldStyle);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle('B' . $rowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
	$rowId++;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "PERSONNEL");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "RATE");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "TOTAL");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "HOURS");
    } catch (PHPExcel_Exception $e) {
    }

    $rowId++;

	
	
	$firstRow = $rowId;
	
	foreach ($employeeData as $employee_name=>$hours){
		$categoryRate = $SF270BillingRateByEmployee[$employee_name];

        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $categoryRate);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=(+D" . $rowId . "*B" . $rowId . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $hours);
        } catch (PHPExcel_Exception $e) {
        }
        $lastRow = $rowId;
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":D" . $rowId)->applyFromArray($border);
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
	}
	
	$rowId++;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "TOTAL PERSONNEL");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=SUM(C" . $firstRow . ":C" . $lastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=SUM(D" . $firstRow . ":D" . $lastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":D" . $rowId)->applyFromArray($BoldStyle);
    } catch (PHPExcel_Exception $e) {
    }


    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(25);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(10);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(12);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("B5:C" . $rowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"_);_(@_)');
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle('B5:F256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    } catch (PHPExcel_Exception $e) {
    }


    $activeSheet++;

	
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$sf270HoursFile = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";

?>