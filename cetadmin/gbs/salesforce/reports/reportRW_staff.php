<?php
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$diversionThisMonthRecycled = ($diversionReportThisMonth["Recycled"] < 100 && $diversionReportThisMonth["Recycled"] >= 1 ? round($diversionReportThisMonth["Recycled"]) : $diversionReportThisMonth["Recycled"]);
	$diversionThisMonthReused = ($diversionReportThisMonth["Reused"] < 100 && $diversionReportThisMonth["Reused"] >= 1 ? round($diversionReportThisMonth["Reused"]) : $diversionReportThisMonth["Reused"]);
	$diversionThisMonthFoodDonated = ($diversionReportThisMonth["Food Donated"] < 100 && $diversionReportThisMonth["Food Donated"] >= 1 ? round($diversionReportThisMonth["Food Donated"]) : $diversionReportThisMonth["Food Donated"]);
	$diversionThisMonthComposted = ($diversionReportThisMonth["Composted"] < 100 && $diversionReportThisMonth["Composted"] >= 1 ? round($diversionReportThisMonth["Composted"]) : $diversionReportThisMonth["Composted"]);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "RW-" . date("Y-m", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D13", date("F", strtotime($invoiceDate)) . " Diversion");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D14", ($diversionThisMonthRecycled ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F14", round($diversionReportYTD["Recycled"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D15", ($diversionReportThisMonth["Lamps"] ?: "0") . " units");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F15", $diversionReportYTD["Lamps"] . " units");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D16", ($diversionThisMonthReused ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F16", round($diversionReportYTD["Reused"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D17", ($diversionThisMonthFoodDonated ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F17", round($diversionReportYTD["Food Donated"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D18", ($diversionThisMonthComposted ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F18", round($diversionReportYTD["Composted"]) . " tons");
} catch (PHPExcel_Exception $e) {
}

$HighlightStyle = array('fill'=> array('type'=> PHPExcel_Style_Fill::FILL_SOLID,'color'=> array('argb' => 'FFFFF00')));
	//531 = Hotline, 532 = WasteWise & C/U, 533 = Marketing (bullets), 534 = Technical Assistance, 535 = Website, 107 = Compost Site Technical Assistance, 536D (not used), 536 = Best Management Practices
	$CodeOrderArray = array("531"=>27,"532"=>45,"Marketing"=>54,"533"=>60,"533Targeted"=>71,"534"=>79,"535"=>93,"538C"=>118,"536D"=>125,"536"=>126);
	$rowsAdded = 0;
	foreach ($CodeOrderArray as $InvoiceCode=>$DetailRow){
		$rowId = $DetailRow+$rowsAdded;
		$statusRowStart = $rowId;
		$countRowsToDisplay = $bulletsCodeCount[$InvoiceCode];
		
		if ($InvoiceCode == "Marketing"){
			$DataAnalyistRow = 49+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($dataAnalysisThisMonth["MarketingEvents"] ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($dataAnalysisYTD["MarketingEvents"] ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowShift = 0;
		if ($countRowsToDisplay > 1){
			$rowShift = $countRowsToDisplay;
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		if ($InvoiceCode == "536"){
			//resort based on Primary Campaign Name
			foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
				$status = ($Status == "Completed" ? "Completed" : "Not Completed");
				foreach ($rowsToDisplay as $rowToDisplay){
					$primaryCampaignName = $rowToDisplay["primaryCampaign"];
					$secondaryCampaignName = ($rowToDisplay["secondaryCampaignName"] ? : "Ongoing");
					$campaignName = (substr($primaryCampaignName,0,3) == "536" ? $primaryCampaignName : $secondaryCampaignName);
					$campaignName = ($campaignName == "536 Best Management Practices" ? "536 The Best Management Practices" : $campaignName);
					$bulletsResorted[$status."_".$campaignName][] = $rowToDisplay;
				}
			}
			ksort($bulletsResorted);
			$bulletsByCampaignCodeSorted[$InvoiceCode] = $bulletsResorted;
		}
		
		foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
			if ($InvoiceCode == "536"){
				$statusparts = explode("_",$Status);
				$Status = $statusparts[0];
				$campaignName = $statusparts[1];
			}else{
				$campaignName = "";
			}
			foreach ($rowsToDisplay as $rowToDisplay){
				$statusDetails = nl2br($rowToDisplay["statusDetail"],false);
				$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
				$statusDetails = str_replace("<br>"," ",$statusDetails);
				$statusDetails = str_replace("\r\n"," ",$statusDetails);
				$statusDetails = str_replace("\n\r"," ",$statusDetails);
				$statusDetails = str_replace("\r"," ",$statusDetails);
				$statusDetails = str_replace("\n"," ",$statusDetails);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                } catch (PHPExcel_Exception $e) {
                }
                $width = mb_strwidth ($value); //Return the width of the string
				if ($InvoiceCode == "536"){
					//add border between campaigns
					if ($oldCampaignName && $campaignName != $oldCampaignName){
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":H" . $rowId)->applyFromArray($TopBorder);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					$oldCampaignName = $campaignName;
				}

				$rowHeight = 15;
				if ($width > 80){
					$rowHeight = ($rowHeight*ceil($width/80));
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
					//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				
				$statusRowEnd = $rowId;
				$rowId++;
				if ($rowShift){
					$rowsAdded++;
				}
			}
		}

        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":B" . $statusRowEnd)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }
        if ($InvoiceCode == "531"){
			$DataAnalyistRow = 30+$rowsAdded;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $DataAnalyistRow, $dataAnalysisThisMonth["CreatedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $DataAnalyistRow, $dataAnalysisYTD["CreatedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 1), $dataAnalysisThisMonth["CompletedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 1), $dataAnalysisYTD["CompletedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 2), $dataAnalysisThisMonth["CreatedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 2), $dataAnalysisYTD["CreatedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 3), $dataAnalysisThisMonth["CompletedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 3), $dataAnalysisYTD["CompletedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 4), "=F" . $DataAnalyistRow . "+F" . ($DataAnalyistRow + 2));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 4), "=G" . $DataAnalyistRow . "+G" . ($DataAnalyistRow + 2));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 5), $dataAnalysisThisMonth["Open"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 6), "=F" . ($DataAnalyistRow + 1) . "+F" . ($DataAnalyistRow + 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 6), "=G" . ($DataAnalyistRow + 1) . "+G" . ($DataAnalyistRow + 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 7), "=F" . ($DataAnalyistRow + 4) . "+F" . ($DataAnalyistRow + 5));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 7), "=G" . ($DataAnalyistRow + 4) . "+F" . ($DataAnalyistRow + 5));
            } catch (PHPExcel_Exception $e) {
            }
        }
		if ($InvoiceCode == "533"){
			$DataAnalyistRow = 83+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($NONReportTypeCountThisMonth["completed"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("D".$DataAnalyistRow,"NONReportTypeCountThisMonthcompleted");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($NONReportTypeCount["completed"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("D".($DataAnalyistRow+1),"NONReportTypeCountcompleted");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), ($NONReportTypeCount["TA"] + $NONReportTypeCount["outreach"]));
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("D".($DataAnalyistRow+2),"TA+outreach");
		}
		if ($InvoiceCode == "535"){
			$DataAnalyistRow = 95+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $DataAnalyistRow, date("F", strtotime($invoiceDate)) . " Website Stats:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 1), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), money($analyticsResults["totalVisits"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 2), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), money($analyticsResults["pageviews"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 3), $analyticsResults["pageviewsPerSession"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 4), $analyticsResults["avgSessionDuration"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 5), $analyticsResults["percentNewSession"]);
            } catch (PHPExcel_Exception $e) {
            }
        }

		
		//$BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		//$objPHPExcel->getActiveSheet()->getStyle("A".$statusRowStart.":".$LastColumn.$statusRowEnd)->applyFromArray($BStyle);		
		
	}//end foreach CodeOrderArray
?>