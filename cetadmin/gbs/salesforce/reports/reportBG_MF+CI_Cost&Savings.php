<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/BG_MF+CI_Cost&Savings_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
//echo "found ".$inputFileName;
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "BGAS".$pathSeparator."BG_MF+CI_Cost&Savings_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;
	$AllBorders = array(
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
//Savings Tab	
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//add month information
//code=>array(title,savingsAdminRow,CategoryStartRow,SubtotalRow,ItemStartRow,ItemSubtotalRow,CopayRow,Total)
//$codeArray = array(811=>array("Multifamily",4,19,21,24,26,27,28),810=>array("C&I Retro",6,32,34,37,39,40,41));
$codeArray = array(
	"811_Multifamily Residential"=>array("Multifamily Residential",4,19,21,24,26,27,28),
	"811_Multifamily Commercial"=>array("Multifamily Commercial",7,32,34,37,39,40,41),
	"810_Small C&I"=>array("Small C&I",10,45,47,50,52,53,54),
	"810_Large C&I"=>array("Large C&I",13,58,60,63,65,66,67)
	);
	
	/*
	//array (segment,Savings Start Row, Invoice Hours Start Row,Invoice Hours Subtotal,Invoice Item Start Row,Invoice Item Subtotal,Invoice Wifi Copay,Invoice Segment Total)
	811=>array("Multifamily Residential",4,19,21,24,26,27,28),
	810=>array("C&I Retro",6,32,34,37,39,40,41));
	*/
$thisRowId = 3;
//print_pre($campaignResultBreakdownByCode["installation"]);
//echo "<hr>";
foreach ($codeArray as $codeSegment=>$programInfo){
	$programTitle = $programInfo[0];
	
	ksort($campaignResultBreakdownByCode["installation"][$codeSegment]);
	ksort($campaignResultBreakdownByAccounts[$codeSegment]);
	//print_pre($campaignResultBreakdownByCode["installation"][$code]);
	$installedAccounts = array();
	//var_dump ($installedAccounts);
	//print_pre($campaignResultBreakdownByAccounts[$code]);
	
	foreach ($campaignResultBreakdownByAccounts[$codeSegment] as $accountId=>$accountCount){
		$installedGPs[$accountId] = array();
		//echo $codeSegment." ".$accountId."<br>";
		//print_pre($campaignResultBreakdownByCode["installation"][$codeSegment][$accountId]);
		foreach ($campaignResultBreakdownByCode["installation"][$codeSegment][$accountId] as $details){
			
			$gp = $details["GPLink"];
			if (!in_array($accountId,$installedAccounts)){$installedAccounts[] = $accountId;}
			if (!in_array($gp,$installedGPs[$accountId])){$installedGPs[$accountId][] = $gp;}
			$programTitle = $accountInfoSegment[$details["accountId"]];
			$qty = $details["qty"]; //Metric__c.Number__c $details["rowDetails"["Qty"]];
			$unitPrice = $details["incentiveAmount"];
			$unitInstallPrice = $details["installAmount"];
			$incentiveAmount = bcmul($unitPrice,$details["qty"],2);
			$installAmount = bcmul($unitInstallPrice,$details["qty"],2);
			$accountName = $accountNames[$details["accountId"]];
			$jobId = $accountJobId[$details["accountId"]];
			$rate = $details["rate"];
			$address = ($details["street"] ? : $gpNames[$details["GPLink"]]);
			//$testMeasure = $details[$rowDetails["Measure Description"]];
			//print_pre $testMeasure; //didnt work
			$measureName = $details["measureName"]; //change to pull from metrics (Metric__c.Unit__c) $details[$rowDetails["Measure Description"]];
			$city = $details["city"];
			$totalCost = $details["totalCost"];
			$contractor = $details["contractor"];
			$invoiceAccounts[$codeSegment][$accountName][$city]["totalCost"] = bcadd($invoiceAccounts[$codeSegment][$accountName][$city]["totalCost"],$totalCost,2);
			$invoiceAccounts[$codeSegment][$accountName][$city]["rate"] = $rate;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $jobId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["utilityLocationID"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("C" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, " " . $details["utilityAccountNumber"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $programTitle);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, date("m/d/Y", strtotime($details["incentiveDate"])));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $accountName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $details["accountPhoneNumber"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $details["contactFirstName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $details["contactLastName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $details["contactPhone"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $address);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $details["city"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $details["dwellingUnits"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $measureName);
            } catch (PHPExcel_Exception $e) {
            } //change to pull from metrics
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, $qty);
            } catch (PHPExcel_Exception $e) {
            } //change to pull from metrics
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("V" . $thisRowId, $contractor);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("W" . $thisRowId, $details["contractorPhone"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("X" . $thisRowId, $details["contractorAddress"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Y" . $thisRowId, $details["contractorCity"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Z" . $thisRowId, $details["contractorState"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AA" . $thisRowId, $incentiveAmount);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AB" . $thisRowId, $installAmount);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AC" . $thisRowId, $details["annualSavings"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $details["totalHours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $totalCost);
            } catch (PHPExcel_Exception $e) {
            }
            if ($contractor == "CET"){
				$intalledProducts[$codeSegment] = $intalledProducts[$codeSegment]+$incentiveAmount;
				$actualInstalledItems[$codeSegment][$measureName]["qty"] = $actualInstalledItems[$codeSegment][$measureName]["qty"]+$qty;
				$actualInstalledItems[$codeSegment][$measureName]["unitPrice"] = $unitInstallPrice;
			}
			$thisRowId++;
		}//end foreach campaignResultBreakdownByCode
		//now add additional just hours records
		if ($campaignResultBreakdownByCodeAdditional["justhours"][$codeSegment][$accountId]){
			$justHours = $campaignResultBreakdownByCodeAdditional["justhours"][$codeSegment][$accountId];
			foreach ($justHours as $gp=>$details){
				if (!in_array($gp,$installedGPs[$accountId])){
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($thisRowId, 1);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $qty = $details["qty"];
						$incentiveAmount = bcmul($details["incentiveAmount"],$details["qty"],2);
						$installAmount = bcmul($details["installAmount"],$details["qty"],2);
						$accountName = $accountNames[$details["accountId"]];
						$programTitle = $accountInfoSegment[$details["accountId"]];
						$jobId = $accountJobId[$details["accountId"]];
						$rate = $details["rate"];
						$address = ($details["street"] ? : $gpNames[$details["GPLink"]]);
						$city = $details["city"];
						$totalCost = $details["totalCost"];
						$contractor = $details["contractor"];
						$invoiceAccounts[$codeSegment][$accountName][$city]["totalCost"] = bcadd($invoiceAccounts[$codeSegment][$accountName][$city]["totalCost"],$totalCost,2);
						$invoiceAccounts[$codeSegment][$accountName][$city]["rate"] = $rate;
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $jobId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["utilityLocationID"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("C" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, " " . $details["utilityAccountNumber"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $programTitle);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, date("m/d/Y", strtotime($details["incentiveDate"])));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $accountName);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $details["accountPhoneNumber"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $details["contactFirstName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $details["contactLastName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $details["contactPhone"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $address);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $city);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $details["dwellingUnits"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $details["measureName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, $qty);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("V" . $thisRowId, $contractor);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("W" . $thisRowId, $details["contractorPhone"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("X" . $thisRowId, $details["contractorAddress"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Y" . $thisRowId, $details["contractorCity"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Z" . $thisRowId, $details["contractorState"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AA" . $thisRowId, $incentiveAmount);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AB" . $thisRowId, $installAmount);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AC" . $thisRowId, $details["annualSavings"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $details["totalHours"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $totalCost);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($contractor == "CET"){
							$intalledProducts[$codeSegment] = $intalledProducts[$codeSegment]+$incentiveAmount;
						}
					$thisRowId++;
				}
				
			}
		}
	}	
	//print_pre($adminTotalHours);
	//echo "<hr>".$code. " AD".$thisRowId." = ".$adminTotalHours[$code]."<br>";
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $programTitle);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("V" . $thisRowId, "CET");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("W" . $thisRowId, "413-586-7350");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("X" . $thisRowId, "320 Riverside Dr. - 1A");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("Y" . $thisRowId, "Florence");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("Z" . $thisRowId, "MA");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AA" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AB" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AC" . $thisRowId, "n/a");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $adminTotalHours[$codeSegment]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $adminTotalCost[$codeSegment]);
    } catch (PHPExcel_Exception $e) {
    }
    $summaryRows[$codeSegment]["adminHours"] = $thisRowId; //because we will be deleting row2 as a spacer row
	$thisRowId++;
	$thisRowId++;
}

//add borders to all cells
try {
    $objPHPExcel->getActiveSheet()->getStyle('A1:AE' . ($thisRowId - 1))->applyFromArray($AllBorders);
} catch (PHPExcel_Exception $e) {
}


//update invoice number
/*this code was moved to _invoiceTools_BGAS.php to accommodate the revenue code generation line 36
$startInvoiceNumberFromJuly2015 = 65;	
$monthsFromJuly2015 = datediff("2015-07-01", $invoiceDate, "month");
$invoiceNumber = $startInvoiceNumberFromJuly2015+$monthsFromJuly2015;
*/	

//Invoice Tab
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $invoiceNumber);
} catch (PHPExcel_Exception $e) {
} //This is where the invoice number is set
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
//code=>array(title,savingsAdminRow,CategoryStartRow,SubtotalRow,ItemStartRow,ItemSubtotalRow,CopayRow)
$rowShift = 0;
//print_pre($invoicingResults);
foreach ($codeArray as $codeSegment=>$programInfo){
	//add hours
	$codeParts = explode("_",$codeSegment);
	$code = $codeParts[0];
	$CategoryStartRow = $programInfo[2]+$rowShift;
	$CategorySubtotalRow = $programInfo[3]+$rowShift;
	$categoryCount = 0;
	$thisRowId = $CategoryStartRow;
	foreach ($BillingRateByCategory["BGAS_8"] as $thisCategory=>$categoryInfo){
		if ($invoicingResults[$codeSegment][$thisCategory]){$categoryCount++;}
		//echo "segment=". $codeSegment." category=".$thisCategory." this categoryCount=".$categoryCount."<br>";
		foreach ($invoicingResults[$codeSegment][$thisCategory] as $thisRate=>$hours){
			if ($categoryCount > 2){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($CategorySubtotalRow, 1);
                } catch (PHPExcel_Exception $e) {
                }
                $CategorySubtotalRow++;
				$rowShift++;
			}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $thisCategory);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $thisRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=E" . $thisRowId . "*F" . $thisRowId);
            } catch (PHPExcel_Exception $e) {
            }
            $lastRow = $thisRowId;
			$thisRowId++;
		}
	}	
	if ($categoryCount > 0){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $CategorySubtotalRow, "=SUM(H" . $CategoryStartRow . ":H" . $lastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
    }

	//add Item
	$ItemStartRow = $programInfo[4]+$rowShift;
	$ItemSubtotalRow = $programInfo[5]+$rowShift;
	$ItemCount = 0;
	$thisRowId = $ItemStartRow;
	
	if (count($actualInstalledItems[$codeSegment])){
		foreach ($actualInstalledItems[$codeSegment] as $itemName=>$itemInfo){
			$ItemCount++;
			if ($ItemCount > 2){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($ItemSubtotalRow, 1);
                } catch (PHPExcel_Exception $e) {
                }
                $ItemSubtotalRow++;
				$rowShift++;
			}
			$WifiCopayCount = 0;
			if (strpos(" ".$itemName,"WiFi Thermostat")){$itemName = "WiFi Thermostat";$WifiCopayCount = $itemInfo["qty"];}
			if (strpos(" ".$itemName,"WiFi Hub")){$itemName = "WiFi Hub";}
			if (strpos(" ".$itemName,"Nest E")){$itemName = "WiFi Hub";} //Lauren added this
			if (strpos(" ".$itemName,"Aerator")){$itemName = "Aerator";}
			if (strpos(" ".$itemName,"1.7gpm Earth Shower")){$itemName = "Showerhead";}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $itemName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $itemInfo["qty"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $itemInfo["unitPrice"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=E" . $thisRowId . "*F" . $thisRowId);
            } catch (PHPExcel_Exception $e) {
            }
            $lastRow = $thisRowId;
			$thisRowId++;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $ItemSubtotalRow, "=SUM(H" . $ItemStartRow . ":H" . $lastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
        //update Wifi Copay
		if ($WifiCopayCount){
			$WifiCopayRow = $programInfo[6]+$rowShift;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $WifiCopayRow, $WifiCopayCount);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $WifiCopayRow, "=-(E" . $WifiCopayRow . "*F" . $WifiCopayRow . ")");
            } catch (PHPExcel_Exception $e) {
            }
            $WifiCopayTotal = "+H".$WifiCopayRow;
		}else{
			$WifiCopayRow = $programInfo[6]+$rowShift;
            try {
                $objPHPExcel->getActiveSheet()->removeRow(($WifiCopayRow), 1);
            } catch (PHPExcel_Exception $e) {
            }
            $rowShift = ($rowShift-1);
			$WifiCopayTotal = "";
		}
	}else{
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($ItemStartRow + 1), 1);
        } catch (PHPExcel_Exception $e) {
        }
        $ItemSubtotalRow = $ItemSubtotalRow-1;
		$rowShift = ($rowShift-1);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "None");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $ItemSubtotalRow, "=SUM(H" . $ItemStartRow . ":H" . $thisRowId . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $WifiCopayRow = $programInfo[6]+$rowShift;
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($WifiCopayRow), 1);
        } catch (PHPExcel_Exception $e) {
        }
        $rowShift = ($rowShift-1);
		$WifiCopayTotal = "";
	}
	//sectionTotal Row
	$totalRow = $programInfo[7]+$rowShift;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $totalRow, "=H" . $CategorySubtotalRow . "+H" . $ItemSubtotalRow . $WifiCopayTotal);
    } catch (PHPExcel_Exception $e) {
    }
}

//print_pre($actualInstalledItems);
//print_pre($intalledProducts[$codeSegment]);
foreach ($actualInstalledItems as $codeSegment=>$itemInfo){
	foreach ($itemInfo as $measureName=>$measureInfo){
		$unitPrice = $measureInfo["unitPrice"];
		$source = "BGAS 810/811";
		$cost = $standardizedNames[$measureName]["Cost"];
		$EFI = $standardizedNames[$measureName]["EFI"];
		$description = $standardizedNames[$measureName]["Description"];
		$qty = $measureInfo["qty"];
		// 40222 = ISM Materials
		$dollarAmount = bcmul($cost,$qty,2);
		if ($measureName == "Wifi Hub"){
			echo "Details: ";
			print_pre($details); //use this to check field names
			echo "<br>";
			$dollarAmount = bcsub($dollarAmount,50,2);
		}
		$revenueCodes[$source][40222] = bcadd($revenueCodes[$source][40222],$dollarAmount,2);
		$revenueCodesByEmployee[$source][40222][$EFI."|".$description."|".$qty] = $dollarAmount;
		
		$invoiceAmount = bcmul($qty,$unitPrice,2);
		// 40220 = ISM Labor
		$laborAmount = bcsub($invoiceAmount,$dollarAmount,2);
		$revenueCodes[$source][40220] = bcadd($revenueCodes[$source][40220],$laborAmount,2);
		$revenueCodesByEmployee[$source][40220][$EFI."|".$description."|".$qty] = $laborAmount;
		
		
	}
	

}

$CETInternalFiscalRevenueSheetIndex = 2;	
$CETInternalFiscalRevenueSheetName = "BGAS";
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');
//echo "Item Info: ";
//var_dump($itemInfo);
//echo "<br>";
//echo "Wifi CoPay Count: ".$WifiCopayCount; //empty
//echo "Wifi Copay total: ".$WifiCopayCount; //empty
//echo "Actual installed: ".$actualInstalledItems; //empty
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>