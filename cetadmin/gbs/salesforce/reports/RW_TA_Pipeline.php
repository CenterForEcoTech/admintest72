<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/RW_TA_Pipeline_Templatev2.xlsx"; //Lauren remove v2 if it crashes
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RW".$pathSeparator."RW_TA_Pipeline_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$SheetIndex = 1; //sets the data on the TA tab
foreach ($ExcelData as $TabName=>$PipelineRecords){
	//echo "Test GPPhase: ".$GPPhase."<br>";
    try {
        $objPHPExcel->setActiveSheetIndex($SheetIndex);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId = 6;
	$TotalInProgressCount = array();
	foreach ($PipelineRecords as $Pipeline=>$rowsToDisplay){
		//echo "<br> Test GPPhase: ".$GPPhase."<br>";
		
		
		
		$pipelineRowStart = $rowId;
		if (count($rowsToDisplay)){
			foreach ($rowsToDisplay as $rowToDisplay){
				//echo "<br>Test Opportunity Lead Origination from variable $opportunityLead: ".$opportunityLead."<br>";
				//echo "Test Opportunity Lead Origination from rowToDisplay: ".$rowToDisplay["Upgrade__c.Opportunity_Lead_Origination__c"]."<br>";
				if ($rowToDisplay["Account Name"] == $priorAccountName && $rowToDisplay["Last Action Date"] == $priorLastActionDate){
					$rowId = $rowId-1;
					$Number .= "\n".$rowToDisplay["Number"];
					$Unit .= "\n".$rowToDisplay["Unit"];
					$Material .= "\n".$rowToDisplay["Material"]."   ";
				}else{
					$Number = $rowToDisplay["Number"];
					$Unit = $rowToDisplay["Unit"];
					$Material = $rowToDisplay["Material"];
				}
//Sets the data for each field
				//$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,$rowToDisplay["Primary Campaign: Campaign Name"]); changed per Emily and Lauren's restructuring 8.22.2019
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $rowToDisplay["Green Prospect Type"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $rowToDisplay["Account Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $rowToDisplay["Initiation Date"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $rowToDisplay["Last Action Date"]);
                } catch (PHPExcel_Exception $e) {
                }
                if ($TabName == "Mini TA"){					//split the Mini TAs - doesn't contain DEP Summary Report column
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $rowToDisplay["Green Prospect Phase"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $rowToDisplay["Owner: Full Name"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    //$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,$rowToDisplay["Green Prospect Type"]);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $Number);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $Unit);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $Material);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $rowToDisplay["Current Status Detail"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $rowToDisplay["Opportunity Lead"]);
                    } catch (PHPExcel_Exception $e) {
                    } //$rowToDisplay["Opportunity Lead"]); //just added 9/4/2019  $opportunityLead
					$LastColumn = "K";
					$pipelineRowEnd = $rowId; //added 12.31.2019
					
				}else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $rowToDisplay["RW DEP Summary Report Submitted"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $rowToDisplay["Green Prospect Phase"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $rowToDisplay["Owner: Full Name"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    //$objPHPExcel->getActiveSheet()->setCellValue("H".$rowId,$rowToDisplay["Green Prospect Type"]); moved to A per Emily and Lauren's restructuring 8.22.2019
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $Number);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $Unit);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $Material);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $rowToDisplay["Current Status Detail"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, $rowToDisplay["Opportunity Lead"]);
                    } catch (PHPExcel_Exception $e) {
                    } //just added 9/4/2019 - removed $opportunityLead
					$LastColumn = "L";
				}
				$pipelineRowEnd = $rowId;
				$rowId++;
				$priorAccountName = $rowToDisplay["Account Name"];
				$priorLastActionDate = $rowToDisplay["Last Action Date"];
				$GPPhase = $rowToDisplay["Green Prospect Phase"];
			}
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $pipelineRowStart . ":" . $LastColumn . $pipelineRowEnd)->getAlignment()->setWrapText(true);
            } catch (PHPExcel_Exception $e) {
            }
            $BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $pipelineRowStart . ":" . $LastColumn . $pipelineRowEnd)->applyFromArray($BStyle);
            } catch (PHPExcel_Exception $e) {
            }

            //echo "Test pipeline: ".$pipeline."<br>";
			//echo "Test Pipeline: ".$Pipeline."<br>";
			//echo "Test GPPhase: ".$GPPhase."<br>";
			
			if ($Pipeline == "Does Not Apply"){ //consolidate this?
	//if (strpos($Pipeline,  "Completed") !== false){ //need to try
			//if (strpos($GPPhase, 'Completed')===0){  //need to try
			//if ($Pipeline == "Referral-Referred to TA" || "Completed-Measure Implemented" || "Completed-Measure does not apply" || "Completed-Measure Not Implemented" || "Completed-Back in Compliance"){
				$CompletedCount = ($pipelineRowEnd-$pipelineRowStart+1); // removed +1 LH
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D2", "=COUNTA(A" . $pipelineRowStart . ":A" . $pipelineRowEnd . ")");
                } catch (PHPExcel_Exception $e) {
                }
                //$CompletedCount++;
				//$objPHPExcel->getActiveSheet()->setCellValue("D2",$CompletedCount);
				//$objPHPExcel->getActiveSheet()->setCellValue("D2","=SUM(".implode("+",$CompletedCount).")");
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Total Completed  TA");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $CompletedCount);
                } catch (PHPExcel_Exception $e) {
                }
                $rowColor = 'FF90EE90';
				
			}else{
	
//	if (strpos($greenProspectPhase, "Complete") !== true) {
//if ($Pipeline == "90+ Days" || "61-90 Days" || "31-60 Days" || "1-30 Days"){ 
				//$pipelineRowStart = $rowId+1;
				//echo "Test Pipeline row start: ".$pipelineRowStart;
				//echo "<br> Test Pipeline row end: ".$pipelineRowEnd."<br>";
				$InProgressCount = ($pipelineRowEnd-$pipelineRowStart+1);
				$TotalInProgressCount[] = "COUNTA(A".$pipelineRowStart.":A".$pipelineRowEnd.")";
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C2", ($InProgressCount - $CompletedCount));
                } catch (PHPExcel_Exception $e) {
                } //put this back LH
				//$objPHPExcel->getActiveSheet()->setCellValue("C2","=SUM(".implode("+",$TotalInProgressCount).")");
				//$objPHPExcel->getActiveSheet()->setCellValue("C2",$TotalInProgressCount);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Total TA In Progress");
                } catch (PHPExcel_Exception $e) {
                }
                //$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$InProgressCount);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, ($InProgressCount - $CompletedCount));
                } catch (PHPExcel_Exception $e) {
                }
                $rowColor = 'FFFFFF00';
			}
			//echo "Test GPPhase: ".$GPPhase."<br>";
			//echo "Test Pipeline: ".$Pipeline."<br>";
			
/*			$GPPhase = $rowToDisplay["Green Prospect Phase"];
			
	if ($GPPhase == "Completed-Measure Implemented" || "Completed-Measure Not Implemented" || "Completed-Measure does not apply" || "Completed-Back in Compliance" || "Referral-Referred to TA"){ //consolidate this?
				$CompletedCount = ($pipelineRowEnd-$pipelineRowStart+1);
				$objPHPExcel->getActiveSheet()->setCellValue("D2","=COUNTA(A".$pipelineRowStart.":A".$pipelineRowEnd.")");
				$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,"Total Completed  TA");
				$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$CompletedCount);
				$rowColor = 'FF90EE90';
			}else{
				$InProgressCount = ($pipelineRowEnd-$pipelineRowStart+1);
				$TotalInProgressCount[] = "COUNTA(A".$pipelineRowStart.":A".$pipelineRowEnd.")";
				$objPHPExcel->getActiveSheet()->setCellValue("C2","=SUM(".implode("+",$TotalInProgressCount).")");
				$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,"Total TA In Progress");
				$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$TotalInProgressCount);
				$rowColor = 'FFFFFF00';
			}
			*/
            try {
                $objPHPExcel->getActiveSheet()->mergeCells("A" . $rowId . ":" . "E" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":" . $LastColumn . $rowId)->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => $rowColor)
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }

            $rowId++;
		}
	}
	$SheetIndex++;
    try {
        $objPHPExcel->setActiveSheetIndex(4);
    } catch (PHPExcel_Exception $e) {
    } //LH change back to 5 if crash
//$objPHPExcel->getActiveSheet()->setCellValue("A1",$lastDateofInvoiceDate);
$completedCount = $NONReportTypeCount["completed"];
$inprogressCount = $NONReportTypeCount["outreach"];
$completedSummaryRow = 7;
if ($completedCount > 1){
	$rowShift = $completedCount-1;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($completedSummaryRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowId = 6;
foreach ($NONReport["completed"] as $ProjectStatus=>$nonAccounts){
	foreach ($nonAccounts as $thisNONAccount){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $thisNONAccount["opportunityLead"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $thisNONAccount["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisNONAccount["initiationDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $thisNONAccount["lastActionDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $thisNONAccount["DEPReportSubmitted"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $thisNONAccount["greenProspectPhase"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisNONAccount["owner"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $thisNONAccount["prospectType"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $thisNONAccount["number"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $thisNONAccount["unit"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $thisNONAccount["material"]);
        } catch (PHPExcel_Exception $e) {
        }

        if (strlen($thisNONAccount["name"]) > 35){
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight((15 * ceil(strlen($thisNONAccount["name"]) / 35)));
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight(15);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		$rowId++;
	}
}
//add SummaryRow
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=COUNTA(D6:D" . ($rowId - 1) . ")");
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
}


try {
    foreach ($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) {
        $rd->setRowHeight(-1);
    }
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>