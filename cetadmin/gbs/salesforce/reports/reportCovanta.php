<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/Covantas_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughsByJobID[$results->JobID][] = $results;
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID"){
			$passThroughHeaders[$key] = 1;
		}
	}
}
//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "Covanta".$pathSeparator."Covantas_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

	$InvoiceStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$HighlightStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFFFF00'))
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$FooterStyle = array(
		'font'=>array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$SectionStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Fineprint = array(
		'font'  => array('bold'=>true,'size'=>8,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$AmountDueStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
//Covanta 516Summary	
$InvoiceNumber["CovantaSemass"] = "516-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
$rowsAdded = 0;
	$InvoiceCodeArray = array("516O"=>"CovantaSemass","516R"=>"CovantaSemass","516"=>"CovantaSemass"); //516O and 516R deactivated for February 2017
	foreach ($InvoiceCodeArray as $InvoiceCode=>$BillingRateType){
		if ($InvoiceCode == "516O"){$rowsStart = 4;}
		if ($InvoiceCode == "516R"){$rowsStart = 9;}
		if ($InvoiceCode == "516"){$rowsStart = 14;}
		$thisRowId = $rowsStart+$rowsAdded;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . ($thisRowId - 1), date("F", strtotime($invoiceDate)));
        } catch (PHPExcel_Exception $e) {
        }
        $hoursCount = count($InvoiceDataHours[$InvoiceCode]);
		if ($hoursCount > 1){
			$rowShift = ($hoursCount-1);
			//echo $InvoiceCode." will add ".$rowShift." before row ".($thisRowId+1)."<br>";
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
            } catch (PHPExcel_Exception $e) {
            }
        }
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$BillingRateType][$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
				//echo $Employee_Name." will add 1 row before ".($thisRowId+1)."<bR>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":B" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
				$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);
				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
			$thisSum = $theseHours;
//			$objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId+1),1);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":B" . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            if ($thisSum){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "=SUM(B" . $categoryStartRow . ":B" . $thisLastRow . ")");
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "0");
                } catch (PHPExcel_Exception $e) {
                }
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->applyFromArray($HighlightStyle);
            } catch (PHPExcel_Exception $e) {
            }
            //$SummaryValues[$InvoiceCode][$CategoryName]["hours"] = "='516-Summary'!B".$thisRowId;
			//$SummaryValues[$InvoiceCode][$CategoryName]["rate"] = $categoryRate;
			$SummaryValues["516"][$CategoryName]["hours"][] = "'516-Summary'!B".$thisRowId;
			$SummaryValues["516"][$CategoryName]["rate"] = $categoryRate;
			$thisRowId++;
			$rowsAdded++;
		}
	}
//Covanta 516	
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F20", "=" . implode('+', $SummaryValues["516"]["Project Manager"]["hours"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F21", "=" . implode('+', $SummaryValues["516"]["Business Specialist"]["hours"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F22", "=" . implode('+', $SummaryValues["516"]["Business Support Specialist"]["hours"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F23", "=" . implode('+', $SummaryValues["516"]["EcoFellows"]["hours"]));
} catch (PHPExcel_Exception $e) {
}
/* deactivated Feb 2017
$objPHPExcel->getActiveSheet()->setCellValue("F26",$SummaryValues["516R"]["Project Manager"]["hours"]);
$objPHPExcel->getActiveSheet()->setCellValue("F27",$SummaryValues["516R"]["Business Specialist"]["hours"]);
$objPHPExcel->getActiveSheet()->setCellValue("F28",$SummaryValues["516R"]["Business Support Specialist"]["hours"]);
$objPHPExcel->getActiveSheet()->setCellValue("F29",$SummaryValues["516R"]["EcoFellows"]["hours"]);
*/
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G20", $SummaryValues["516"]["Project Manager"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G21", $SummaryValues["516"]["Business Specialist"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G22", $SummaryValues["516"]["Business Support Specialist"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G23", $SummaryValues["516"]["EcoFellows"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
/* deactivated Feb 2017
$objPHPExcel->getActiveSheet()->setCellValue("G26",$SummaryValues["516R"]["Project Manager"]["rate"]);
$objPHPExcel->getActiveSheet()->setCellValue("G27",$SummaryValues["516R"]["Business Specialist"]["rate"]);
$objPHPExcel->getActiveSheet()->setCellValue("G28",$SummaryValues["516R"]["Business Support Specialist"]["rate"]);
$objPHPExcel->getActiveSheet()->setCellValue("G29",$SummaryValues["516R"]["EcoFellows"]["rate"]);
*/
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["CovantaSemass"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}


//Semass Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", $InvoiceNumber["CovantaSemass"]);
} catch (PHPExcel_Exception $e) {
}

$rowId = 20;
$bulletsCount = count($reportBulletsSemass);
$bulletValue = "•";
if ($bulletsCount){
	if ($bulletsCount > 1){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, ($bulletsCount - 1));
        } catch (PHPExcel_Exception $e) {
        }
    }
	//print_pre($reportBulletsSemass);
	function compare_tasks($a, $b)
  {
    return strnatcmp($a["taskStatus"], $b["taskStatus"]);
  }
  
  usort($reportBulletsSemass, "compare_tasks");
	
	foreach ($reportBulletsSemass as $bullet){
		//print_pre($bullet);
		$taskStatus = $bullet["taskStatus"];
		$currentStatus = $bullet["status"];
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $taskStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $bulletValue . $currentStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
        } catch (PHPExcel_Exception $e) {
        }
        $width = mb_strwidth ($value); //Return the width of the string

		$rowHeight = 20;
		if ($width > 80){
			$rowHeight = ($rowHeight*ceil($width/80));
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		
		$rowId++;
	}
	
/*	foreach ($taskDetails as $taskDetail){
		$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,$taskDetail);
		
	}
	$rowId++; */
}	


	//now add events
	if ($eventCounterSemass){
		$rowId++;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Events:");
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
		foreach ($EventsSemass as $startStr=>$typeData){
			ksort($typeData);
			foreach ($typeData as $type=>$events){
				foreach ($events as $eventData){
					//print_pre($eventData);
					$eventDate = $eventData["eventDate"];
					$subject = $eventData["event"];
					$description = $eventData["description"];
					$attendance = $eventData["attendance"];
					$eventDisplay = "[".$subject."] ".$description.($attendance > 0 ? " (".$attendance." in attendance)" : "");

                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $eventDate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $eventDisplay);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 20;
					if ($width > 80){
						$rowHeight = ($rowHeight*ceil($width/80));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					
				}
			}
		}
		
	}




	
//Covanta 519 Summary
$InvoiceNumber["CovantaHaverhill"] = "H519-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->setActiveSheetIndex(4);
} catch (PHPExcel_Exception $e) {
}
$rowsAdded = 18;
	$SubTotalRows = array();
	//inactive code
	//"518N"=>"Green Team - Newsletter Update"
	$InvoiceCodeArray = array("519"=>"CovantaHaverhill");
//	print_pre($InvoiceDataHours);
	foreach ($InvoiceCodeArray as $InvoiceCode=>$BillingRateType){
		$thisRowId = $rowsAdded;
		//create Invoice Code header row
		//echo $InvoiceText." at row".$thisRowId."<br>";
		$InvoiceCodeSubtotalRows = array();
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$BillingRateType][$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			if ($categoryEmployeeCount >= 1){
				$rowShift = $categoryEmployeeCount;
				//echo "will add ".$rowShift." before row ".($thisRowId+1)."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
				$CovantaHaverhillInvoiceData[$CategoryName]["hours"] = $CovantaHaverhillInvoiceData[$CategoryName]["hours"]+$hours;
				$CovantaHaverhillInvoiceData[$CategoryName]["rate"] = $categoryRate;
				
				$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);
				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
			$thisSum = round($theseHours*$categoryRate,2);
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            if ($thisSum){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=SUM(F" . $categoryStartRow . ":F" . $thisLastRow . ")");
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "0");
                } catch (PHPExcel_Exception $e) {
                }
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisSum);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
            } catch (PHPExcel_Exception $e) {
            }
            $InvoiceCodeSubtotalRows[] = $thisRowId;
			$thisRowId++;
			$rowsAdded++;
		}
		
		$thisRowId++;

        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $InvoiceCodeSubtotalRows));
        } catch (PHPExcel_Exception $e) {
        }
        $ServicesSubtotalRow = $thisRowId;
		
		$thisRowId = $thisRowId+4;
		$incentiveSubtotalStart = $thisRowId;
		/*passthrough and incentives deactivated 2018-06 by request of staff due to no longer processing checks
		//add any passthrough items
		if (count($passThroughsByJobID[$InvoiceCode])){
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId+1),count($passThroughsByJobID[$InvoiceCode]));
			foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
				//print_pre($thisPassThrough);
				$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,$thisPassThrough->Memo);
				$objPHPExcel->getActiveSheet()->setCellValue("D".$thisRowId,$thisPassThrough->SourceName);
				$objPHPExcel->getActiveSheet()->setCellValue("F".$thisRowId,MySQLDate($thisPassThrough->Date));
				$objPHPExcel->getActiveSheet()->setCellValue("H".$thisRowId,$thisPassThrough->Debit);
				
				$revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$thisPassThrough->Debit,2);
				$revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName],$thisPassThrough->Debit,2);
				
//				$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":H".$thisRowId)->applyFromArray($SummaryStyle);
				$objPHPExcel->getActiveSheet()->getStyle("H".$thisRowId.":H".$thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
				$thisRowId++;
				$rowsAdded++;
				$incentiveSubtotalEnd = $thisRowId;
			}
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,"None");
			$objPHPExcel->getActiveSheet()->setCellValue("H".$thisRowId,0);
			$objPHPExcel->getActiveSheet()->getStyle("H".$thisRowId.":H".$thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
			$incentiveSubtotalEnd = $thisRowId;
		}
		*/
		
	}
	//Incentive Subtotal
	//$objPHPExcel->getActiveSheet()->setCellValue("H".($thisRowId+1),"=SUM(H".$incentiveSubtotalStart.":H".$incentiveSubtotalEnd.")");

try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["CovantaHaverhill"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}

//Covanta 519 Invoice
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F20", $CovantaHaverhillInvoiceData["Project Manager"]["hours"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F21", $CovantaHaverhillInvoiceData["Business Specialist"]["hours"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F22", $CovantaHaverhillInvoiceData["Business Support Specialist"]["hours"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F23", $CovantaHaverhillInvoiceData["EcoFellows"]["hours"]);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("G20", $CovantaHaverhillInvoiceData["Project Manager"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G21", $CovantaHaverhillInvoiceData["Business Specialist"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G22", $CovantaHaverhillInvoiceData["Business Support Specialist"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G23", $CovantaHaverhillInvoiceData["EcoFellows"]["rate"]);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["CovantaHaverhill"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}


//Haverhill Report
try {
    $objPHPExcel->setActiveSheetIndex(5);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", $InvoiceNumber["CovantaHaverhill"]);
} catch (PHPExcel_Exception $e) {
}


$rowId = 20;
$bulletsCount = count($reportBullets);
$bulletValue = "•";
if ($bulletsCount){
	if ($bulletsCount > 1){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, ($bulletsCount - 1));
        } catch (PHPExcel_Exception $e) {
        }
    }
	function compare_haverhill_tasks($a, $b)
	{
		return strnatcmp($a["HaverhillTaskStatus"],$b["HaverhillTaskStatus"]);
	}
	usort($reportBullets, "compare_haverhill_tasks");

	foreach ($reportBullets as $bullet){
		//var_dump($HaverhillTaskStatus);
		$HaverhillTaskStatus = $bullet["HaverhillTaskStatus"];
		$hCurrentStatus = $bullet["hCurrentStatus"];
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $HaverhillTaskStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $bulletValue . $hCurrentStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
        } catch (PHPExcel_Exception $e) {
        }
        $width = mb_strwidth ($value); //Return the width of the string

		$rowHeight = 20;
		if ($width > 80){
			$rowHeight = ($rowHeight*ceil($width/80));
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		
		$rowId++;
	}
}	

	//now add events
	if ($eventCounter){
		$rowId++;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Events:");
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
		foreach ($Events as $startStr=>$typeData){
			ksort($typeData);
			foreach ($typeData as $type=>$events){
				foreach ($events as $eventData){
					//print_pre($eventData);
					$eventDate = $eventData["eventDate"];
					$subject = $eventData["event"];
					$description = $eventData["description"];
					$attendance = $eventData["attendance"];
					$eventDisplay = "[".$subject."] ".$description.($attendance > 0 ? " (".$attendance." in attendance)" : "");

                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $eventDate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $eventDisplay);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 20;
					if ($width > 80){
						$rowHeight = ($rowHeight*ceil($width/80));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					
				}
			}
		}
		
	}
	
	
	

$CETInternalFiscalRevenueSheetIndex = 6;
$CETInternalFiscalRevenueSheetName = "Covanta";	
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
//$triggerResultFiles[] = $CurrentServer.$adminFolder."gbs/".$webpathExtension;
$triggerResultFiles["Covanta"] = $saveLocation;
?>