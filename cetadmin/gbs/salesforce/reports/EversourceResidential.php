<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/EversourceResidential_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "Eversource".$pathSeparator."Residential_Invoice_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$AmountDueStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FD8D8D8')),
	'font'  => array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)

);
$JustBoldStyle = array(
	'font'  => array('bold'=>true)
);

$SubTotalStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
);

//Residential Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["EversourceResidential"] = $invoiceNumber;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F1", $InvoiceNumber["EversourceResidential"]);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 14;
$CLINCRow = 16;
$rowShift = count($InvoiceRowData);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}
ksort($InvoiceRowData);
//print_pre($InvoiceRowData);
foreach ($InvoiceRowData as $Description=>$descriptionInfo){
	$DescriptionParts = explode("_",$Description);
	$Qty = $descriptionInfo["QTY"];
	$UnitCost = $descriptionInfo["UnitCost"];
	$LaborCost = $descriptionInfo["LaborCost"];
	$Type = $descriptionInfo["Type"];
	$rateType = $rateTypeID[$DescriptionParts[1]];
	$MEAID = $descriptionInfo["MEAID"];
	$TotalCost = bcmul($Qty,$UnitCost,2);
	$TotalLaborCost = bcmul($LaborCost,$Qty,2);
	$TotalISMCost = bcsub($TotalCost,$TotalLaborCost,2);

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $MEAID);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $Qty);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $Type);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $DescriptionParts[0]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $UnitCost);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=B" . $thisRowId . "*E" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    if (strpos(" ".$rateType,"HPC")){
		$employeeName = "HPC";
		$revenueCodes["EversourceResidential"]["40952"] = $revenueCodes["EversourceResidential"]["40952"]+$TotalCost; //labor
		$revenueCodesByEmployee["EversourceResidential"]["40952"][$employeeName] = $revenueCodesByEmployee["EversourceResidential"]["40952"][$employeeName]+$TotalCost; //labor
	}elseif (trim($DescriptionParts[0]) == "Piggyback Fee"){
		$employeeName = "Piggyback Fee";
		$revenueCodes["EversourceResidential"]["40202"] = $revenueCodes["EversourceResidential"]["40202"]+$TotalCost; //labor
		$revenueCodesByEmployee["EversourceResidential"]["40202"][$employeeName] = $revenueCodesByEmployee["EversourceResidential"]["40202"][$employeeName]+$TotalCost; //labor
	}else{
		$employeeName = ($descriptionInfo["EFI"] ? : " ")."|".$DescriptionParts[0];
		$revenueCodes["EversourceResidential"]["40220"] = $revenueCodes["EversourceResidential"]["40220"]+$TotalLaborCost; //labor
		$revenueCodes["EversourceResidential"]["40222"] = $revenueCodes["EversourceResidential"]["40222"]+$TotalISMCost; //material
		$combinedQuantity[$employeeName] = ($combinedQuantity[$employeeName]+$Qty);
		$revenueCodesByEmployee["EversourceResidential"]["40220"][$employeeName] = $revenueCodesByEmployee["EversourceResidential"]["40220"][$employeeName]+$TotalLaborCost; //labor
		$revenueCodesByEmployee["EversourceResidential"]["40222"][$employeeName] = $revenueCodesByEmployee["EversourceResidential"]["40222"][$employeeName]+$TotalISMCost; //material
	}
	$lastItemRow = $thisRowId;
	$thisRowId++;
}

//Add summary Rows

//$objPHPExcel->getActiveSheet()->setCellValue("F".$thisRowId,"C13 to C".$lastItemRow);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUMIF(C14:C' . $lastItemRow . ',"CLINC",F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}
$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUMIF(C14:C' . $lastItemRow . ',"CLCCC",F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}
$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUM(F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}

$thisRowId = $thisRowId+3;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUMIF(C14:C' . $lastItemRow . ',"CLINC",F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}
$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUMIF(C14:C' . $lastItemRow . ',"CLCCC",F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}
$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, '=SUM(F14:F' . $lastItemRow . ')');
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B4", date("F Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}

$CETInternalFiscalRevenueSheetIndex = 1;
$CETInternalFiscalRevenueSheetName = "EversourceResidential";	
//Create CET Internal Fiscal Revenue Sheet
$GLCode = " – 1";
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>