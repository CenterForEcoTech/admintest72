<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

if (!$isStaff){
	//GET DEPInternal Data
	$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "").$DEPInternalReportFileName;
	//  Read your Excel workbook
	try {
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
	} catch (Exception $e) {
		die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
		. '": ' . $e->getMessage());
	}

	//  Get DEP data
    try {
        $sheet = $objPHPExcel->getSheet(0);
    } catch (PHPExcel_Exception $e) {
    }
    $columnsArray = array("C","D","F");
	foreach ($columnsArray as $column){
		$rowId = 18;
		while ($rowId < 28){
            try {
                $code = $objPHPExcel->getActiveSheet()->getCell("A" . $rowId)->getValue();
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $DEPInternalValues[$code][$column] = $objPHPExcel->getActiveSheet()->getCell($column . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;
		}
	}
	//print_pre($DEPInternalValues);
}

//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughJobId = $results->JobID;
	$passThroughJobId = ($passThroughJobId == "537A" || $passThroughJobId == "537B" ? "534/539/537AB" : $passThroughJobId);
	
	$passThroughsByJobID[$passThroughJobId][] = $results;
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID"){
			$passThroughHeaders[$key] = 1;
		}
	}
}
$reportFileName = ($isStaff ? "RW_Report_Template_v4.xlsx" : "RW_FullReport_Template.xlsx");
//echo $reportFileName;
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/".$reportFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RW".$pathSeparator."RW_".($isStaff ? "Report_" : "Invoice_").date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;

$TopBorder = array(
	'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

if ($isStaff){
	//print_pre($diversionReportThisMonth);
	$fileToInclude = $path.'salesforce'.$pathSeparator.'reports'.$pathSeparator.'reportRW_staff_v2.php';
	//echo '<br>Including: '.$fileToInclude.'<Br>';
	//echo "file Exists: ".file_exists($fileToInclude)."<br>";
	include_once ($fileToInclude);
}// end if isStaff

if (!$isStaff){
	//$InvoiceCodeArray = array("531"=>array("Hotline","E"),"532"=>array("WasteWise","F"),"533"=>array("Marketing","G"),"534/539"=>array("Technical Assistance","H"),"535"=>array("Website","I"),"537A"=>array("Hospitality","J"),"537B"=>array("Food Waste Generators Subject to Ban","K"),"537C"=>array("State Facilities","L"),"536"=>array("Best Management Practices","M"),"538C"=>array("Compost Site Technical Assistance","N"),"DEP"=>array("DEP: Green Business Specialist",100));
	$InvoiceCodeArray = array("531"=>array("Hotline","E"),"532"=>array("WasteWise","F"),"533"=>array("Marketing","G")
		,"534/539/537AB"=>array("Technical Assistance","H")
		,"535"=>array("Website","I"),"538C"=>array("Compost Site Technical Assistance","N"),"538D"=>array("Construction and Demolition Technical Assistance","H") //8.5.2019 Lauren removed ,"537C"=>array("State Facilities","L") and ,"536"=>array("Best Management Practices","M")
//		,"DEP"=>array("DEP: Green Business Specialist",100) //section removed due to personnel changes as of Spring 2018
	);
	//Create Allocation data
    try {
        $objPHPExcel->setActiveSheetIndex(2);
    } catch (PHPExcel_Exception $e) {
    }
    //
	$ActualHours = $InvoiceDataHours["530B"]["Program Manager"]["Macaluso_Lorenzo"]+$InvoiceDataHours["530L"]["Program Manager"]["Macaluso_Lorenzo"];
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C5", $ActualHours);
    } catch (PHPExcel_Exception $e) {
    }
    foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){

		if ($InvoiceCode != "DEP"){
			$InvoiceCodeOriginal = $InvoiceCode;
			$InvoiceCode = ($InvoiceCode == "534/539/537AB" ? "534" : $InvoiceCode);
			$InvoiceCode = ($InvoiceCode == "536" ? "536D" : $InvoiceCode);
			$thisColumn = $InvoiceItem[1];
			$DEPValueContract = $DEPInternalValues[$InvoiceCode]["C"];
			$DEPValueMonth = $DEPInternalValues[$InvoiceCode]["D"];
			$DEPValueBilled = $DEPInternalValues[$InvoiceCode]["F"];
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "9", $DEPValueContract);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "11", $DEPValueMonth);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "12", $DEPValueBilled);
            } catch (PHPExcel_Exception $e) {
            }
            $hoursOriginalLorenzo = $InvoiceDataHours[$InvoiceCodeOriginal]["Program Manager"]["Macaluso_Lorenzo"];
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "6", $hoursOriginalLorenzo);
            } catch (PHPExcel_Exception $e) {
            }
            $hoursTotalLorenzo = $hoursTotalLorenzo+$hoursOriginalLorenzo;
            try {
                $calculatedDifference = $objPHPExcel->getActiveSheet()->getCell($thisColumn . "13")->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            if ($calculatedDifference > 0){
				$codesToReallocated[] = $InvoiceCode;
			}
		}
	}
	//now reallocate the actual hours based on weight of originalhours
    try {
        $adjustmentRequiredTotal = $objPHPExcel->getActiveSheet()->getCell("D5")->getCalculatedValue();
    } catch (PHPExcel_Exception $e) {
    }
    $allocatedhoursTotal = 0;
	while ($allocatedhoursTotal < $adjustmentRequiredTotal){
		foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){
			//$InvoiceCode = ($InvoiceCode == "534/539" ? "534" : $InvoiceCode);
			if ($InvoiceCode != "DEP"){
				if (in_array($InvoiceCode,$codesToReallocated)){ //only reallocate to codes with positive balance
					$thisColumn = $InvoiceItem[1];
                    try {
                        $previouslyAllocatedHours = $objPHPExcel->getActiveSheet()->getCell($thisColumn . "5")->getCalculatedValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    if (!$hoursOriginalLorenzoByCode[$InvoiceCode]){
						$hoursOriginalLorenzoByCode[$InvoiceCode] = ($InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] ? : 0.5);
					}
//					$hoursOriginalLorenzoByCode[$InvoiceCode] = ($hoursOriginalLorenzoByCode[$InvoiceCode] ? $hoursOriginalLorenzo[$InvoiceCode] : ($InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] ? : 0.5));
					$hoursWeight = ($hoursOriginalLorenzoByCode[$InvoiceCode]/$hoursTotalLorenzo);
					$valueTimes4 = ($ActualHours*$hoursWeight)*4; //used to keep things to the closes quarter or half
					$allocatedHoursCeil = ceil($valueTimes4)/4;
					$allocatedHoursFloor = floor($valueTimes4)/4;
					$allocated = false;
					$allocatedHours = 0.25;
					if (!$allocated && ($allocatedhoursTotal+$allocatedHoursCeil) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using Ceil: ".$allocatedhoursTotal."+".$allocatedHoursCeil.". is less than or equal to ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = $allocatedHoursCeil;
						$allocated = true;
						
					}
					if (!$allocated && ($allocatedhoursTotal+$allocatedHoursFloor) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using Floor: ".$allocatedhoursTotal."+".$allocatedHoursFloor." is less than or equal to ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = $allocatedHoursFloor;
						$allocated = true;
					}
					if (!$allocated && ($allocatedhoursTotal+0.25) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using 0.25: ".$allocatedhoursTotal."+".$allocatedHoursFloor." is greater than ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = 0.25;
						$allocated = true;
					}
					if ($allocated){
						//echo $InvoiceCode." has previously allocated ".$previouslyAllocatedHours." and will now use ".($allocatedHours+$previouslyAllocatedHours)." as allocated hours<br>";
						$InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] = $InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"]+$allocatedHours;
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "5", ($allocatedHours + $previouslyAllocatedHours));
                        } catch (PHPExcel_Exception $e) {
                        }
                        $allocatedhoursTotal = $allocatedhoursTotal+$allocatedHours;
					}
				}
			}
		}
	}//end while $allocatedhoursTotal <= $adjustmentRequiredTotal

	//Populate RW Invoice Sheet
    try {
        $objPHPExcel->setActiveSheetIndex(1);
    } catch (PHPExcel_Exception $e) {
    }
    $InvoiceCodeRow = 15;
	$rowsAdded = 0;

	$InvoiceStyle = array(
		//'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'7FF0F0F0')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		//'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'7FF0F0F0')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		//'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')), previous color scheme
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'7FF0F0F0')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$AmountDueStyle = array(
		//'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'7FF0F0F0')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
	$rowsAdded = 15;
	$SubTotalRows = array();
	//print_pre($InvoiceDataHours);
	foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){
		$InvoiceText = $InvoiceItem[0];
		$thisRowId = $rowsAdded;
		//create Invoice Code header row
		//echo $InvoiceText." at row".$thisRowId."<br>";
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $InvoiceText);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($InvoiceStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
		$InvoiceCodeSubtotalRows = array();
		//echo $InvoiceCode;
		//print_pre($InvoiceDataHours[$InvoiceCode]);
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$BillingRateType = "RW";
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			if ($categoryEmployeeCount > 1){
				$rowShift = $categoryEmployeeCount;
				//echo "will add ".$rowShift." before row ".($thisRowId+1)."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
//				if ($Employee_Name != "Macaluso_Lorenzo"){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                if ($InvoiceCode == "DEP"){
						//rates last updated 7/1/2016
						//$categoryRate = ($Employee_Name == "Macaluso_Lorenzo" ? 99.00 : 82.50); //other case is for Josh Cook 
						//$totalHours = ($Employee_Name == "Macaluso_Lorenzo" ? 297.00 : 7775.43); //other case is for Josh Cook
						
						//rates last updated 11/14/2016 (also need to update _invoiceTools_RWTAPipeline.php lines 154-155
						//$categoryRate = ($Employee_Name == "Macaluso_Lorenzo" ? 115.00 : 95.00); //other case is for Josh Cook 
						//$totalHours = ($Employee_Name == "Macaluso_Lorenzo" ? 297 : 7775.43); //other case is for Josh Cook
						
						//hours last updated 8/7/2017
						$categoryRate = ($Employee_Name == "Kohler_Lisa" ? 95.00 : 95.00); //other case is for Josh Cook 
						$totalHours = ($Employee_Name == "Kohler_Lisa" ? 237.50 : 8019.35); //other case is for Josh Cook

                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $totalHours);
                        } catch (PHPExcel_Exception $e) {
                        }

                        $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = bcadd($revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]],$totalHours,2);
						$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$totalHours,2);

                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
                        } catch (PHPExcel_Exception $e) {
                        }
                        $InvoiceCodeSubtotalRows[] = $thisRowId;
					}else{
						$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
						$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);
					}
					
					
					$thisLastRow = $thisRowId;
					$theseHours = $theseHours+$hours;
					$thisRowId++;
					$rowsAdded++;
//				}
			}
			if ($InvoiceCode != "DEP"){
				$thisSum = round($theseHours*$categoryRate,2);
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
                } catch (PHPExcel_Exception $e) {
                }
                if ($thisSum){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=SUM(F" . $categoryStartRow . ":F" . $thisLastRow . ")");
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "0");
                    } catch (PHPExcel_Exception $e) {
                    }
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisSum);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
                } catch (PHPExcel_Exception $e) {
                }
                $InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
			}
		}
		//add any passthrough items
		/*
		if (count($passThroughsByJobID[$InvoiceCode])){
			$BillingRateType = "RW";
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId+1),1);
			$thisRowId++;
			$rowsAdded++;
			foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
				$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,MySQLDate($thisPassThrough->Date)." ".$thisPassThrough->SourceName);
				$objPHPExcel->getActiveSheet()->setCellValue("H".$thisRowId,$thisPassThrough->Debit);
				
				$revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$thisPassThrough->Debit,2);
				$revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName],$thisPassThrough->Debit,2);
				
				$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":H".$thisRowId)->applyFromArray($SummaryStyle);
				$objPHPExcel->getActiveSheet()->getStyle("H".$thisRowId.":H".$thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
				$InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
			}
		};
		*/
		$itemsToInclude = array();
		if (count($passThroughsByJobID[$InvoiceCode])){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            $thisRowId++;
			$rowsAdded++;
			foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
				$itemCheck = $thisPassThrough->itemCheck;
				$itemsToInclude[$itemCheck] = "passThroughsByItem";
				if ($attachmentsByItem[$itemCheck]){
					$itemsInBoth[$itemCheck] = 1;
				}else{
					$Adjustments["Alert"]["Passthrough match not found as attachments"][] = $thisPassThrough->VendorName." ".$thisPassThrough->Description." ".date("m/d/Y",strtotime($thisPassThrough->CompletedDate))." $".$thisPassThrough->Number;
				}
			}	
		}
		if (count($attachmentsByJobID[$InvoiceCode])){
			foreach ($attachmentsByJobID[$InvoiceCode] as $thisPassThrough){
				$itemCheck = $thisPassThrough->itemCheck;
				$itemsToInclude[$itemCheck] = "attachmentsByItem";
				if ($passThroughsByItem[$itemCheck]){
					$itemsInBoth[$itemCheck] = 1;
				}else{
					$Adjustments["Alert"]["Attachment match not found in Passthrough file"][] = $thisPassThrough->VendorName." ".$thisPassThrough->Description." ".date("m/d/Y",strtotime($thisPassThrough->CompletedDate))." $".$thisPassThrough->Number;
				}
			}
		}
		foreach ($itemsToInclude as $itemCheck=>$source){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            $thisPassThrough = ${$source}[$itemCheck];
				print_pre($thisPassThrough);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $thisPassThrough->VendorName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $thisPassThrough->Description);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, date("m/d/Y", strtotime($thisPassThrough->CompletedDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisPassThrough->Number);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
            } catch (PHPExcel_Exception $e) {
            }
            $dollar = str_replace(",","",$thisPassThrough->Number);
				$revenueCodes["RW"][40952] = bcadd($revenueCodes["RW"][40952],$dollar,2);
				$revenueCodesByEmployee["RW"][40952][$thisPassThrough->VendorName] = bcadd($revenueCodesByEmployee["RW"][40952][$thisPassThrough->VendorName],$dollar,2);
				$InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
			$ExpensesSectionEnd = $thisRowId;
		}		
		
		

		
		
		//add spacing row between invoice codes
		if (count($InvoiceCodeSubtotalRows)){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Subtotal:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $InvoiceCodeSubtotalRows));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SubtotalStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $SubTotalRows[] = $thisRowId;
		}else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "No Recorded Hours");
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 2), 3);
        } catch (PHPExcel_Exception $e) {
        }
        $rowsAdded = $rowsAdded+3;
	}
	$thisRowId++;
	$thisRowId++;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Subtotal:");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $SubTotalRows));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->applyFromArray($FinalSubtotalStyle);
    } catch (PHPExcel_Exception $e) {
    }

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . ($thisRowId + 2), "Amount Due:");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . ($thisRowId + 2), "=H" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 2) . ":H" . ($thisRowId + 2))->applyFromArray($AmountDueStyle);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H8", "=H" . ($thisRowId + 2));
    } catch (PHPExcel_Exception $e) {
    }

    $InvoiceNumber["RW"] = "RW-".date("y-m",strtotime($invoiceDate));
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H5", date("m/15/Y"));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H6", $InvoiceNumber["RW"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A7", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
    } catch (PHPExcel_Exception $e) {
    }

    $CETInternalFiscalRevenueSheetIndex = 3;
	$CETInternalFiscalRevenueSheetName = "RW";	
	//Create CET Internal Fiscal Revenue Sheet
	include_once('CETInternalRevenueCodeSheet.php');


    try {
        $objPHPExcel->setActiveSheetIndex(1);
    } catch (PHPExcel_Exception $e) {
    }
}else{
    try {
        $objPHPExcel->setActiveSheetIndex(0);
    } catch (PHPExcel_Exception $e) {
    }
} //end if isStaff
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>