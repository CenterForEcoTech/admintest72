<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/NGRIDInspections_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "NGRID".$pathSeparator."524_Invoice_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$AmountDueStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FD8D8D8')),
	'font'  => array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)

);
$TotalStyle = array(
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK))
);
$JustBoldStyle = array(
	'font'  => array('bold'=>true)
);

$SubTotalStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
);

//524 Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 3;
$rowShift = count($summaryData);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}
ksort($summaryData);
foreach ($summaryData as $rowInfo=>$workCategory){
		$rowData = explode("_",$rowInfo);
		$siteWorkHrs = ($workCategory["SiteWork"] ? : 0);
		$travelHrs = ($workCategory["Travel"] ? : 0);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "='524 Invoice'!G15");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $rowData[0]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $rowData[1]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $rowData[2]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rowData[3]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $rowData[4]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $rowData[5]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $siteWorkHrs);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, "=G" . $thisRowId . "*H" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $travelHrs);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, "=(G" . $thisRowId . "/2)*J" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, "=I" . $thisRowId . "+K" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId++;
}
	//total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("L" . ($thisRowId + 1), "=SUM(L3:L" . ($thisRowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}


//new 524 Reportv2 as of 7/27/3018
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
//print_pre($allTimeCards);
ksort($summaryDataWithDetails);
foreach ($allTimeCards as $info){
	//print_pre($detailInfo);
	$billingRate = $info["billingRate"];
	$category = $info["category"];
	$hours = $info["hours"];
	$travelCost = 0;
	$travelHours = 0;
	$siteWorkCost = 0;
	$siteHours = 0;
	if ($category == "Travel"){
		$billingRate = bcdiv($billingRate,2,2);
		$travelCost = bcmul($hours,$billingRate);
		$travelHours = $hours;
	}else{
		$siteWorkCost = bcmul($hours,$billingRate);
		$siteHours = $hours;
	}


    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $info["requestorName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $info["name"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $info["Application Number"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $info["city"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $info["state"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $info["GPType"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $info["inspector"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $info["dateReceived"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $info["dateWorked"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $info["dateSentBackToNGRID"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $info["currentStatusDetail"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $billingRate);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $siteHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $siteWorkCost);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $travelHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $travelCost);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, "=N" . $thisRowId . "+P" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId++;
}
	//total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("Q" . ($thisRowId + 1), "=SUM(Q2:Q" . ($thisRowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}

//new 524 Reportv3 as of 8/7/3018
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
//print_pre($allTimeCards);
ksort($summaryDataWithDetails);
foreach ($allTimeCards as $info){
	//print_pre($info);
	$billingRate = $info["billingRate"];
	$billingCategory = $info["billingCategory"];
	$category = $info["category"];
	$hours = $info["hours"];
	$travelCost = 0;
	$travelHours = 0;
	$siteWorkCost = 0;
	$siteHours = 0;
	if ($category == "Travel"){
		$billingRate = bcdiv($billingRate,2,2);
		$travelCost = bcmul($hours,$billingRate);
		$travelHours = $hours;
	}else{
		$siteWorkCost = bcmul($hours,$billingRate);
		$siteHours = $hours;
	}
	
	$detailsByApplication[$info["Application Number"]]["info"] = $info;
	$detailsByApplication[$info["Application Number"]][$category][$billingCategory]["hours"][] = $hours;
	$detailsByApplication[$info["Application Number"]][$category][$billingCategory]["rate"] = $billingRate;
}
foreach ($detailsByApplication as $applicationNumber=>$infoData){
	$categories = array("Travel","SiteWork");
	$staffMember = array("Engineer","Senior_Specialist");
	foreach ($categories as $category){
			foreach ($staffMember as $staff){
				$arrayStaffName = str_replace("_"," ",$staff);
				$staff = str_replace("_","",$staff);
				$hourName = $staff.$category."Hours";
				$rateName = $staff.$category."Rate";
				${$hourName} = 0;
				${$rateName} = 0;
				if (count($infoData[$category])){
					${$hourName} = array_sum($infoData[$category][$arrayStaffName]["hours"]);
					${$rateName} = $infoData[$category][$arrayStaffName]["rate"];
				}
			}
	}


    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $infoData["info"]["requestorName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $infoData["info"]["name"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $infoData["info"]["Application Number"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $infoData["info"]["city"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $infoData["info"]["state"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $infoData["info"]["GPType"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $infoData["info"]["inspector"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $infoData["info"]["dateReceived"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $infoData["info"]["dateWorked"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $infoData["info"]["dateSentBackToNGRID"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $infoData["info"]["currentStatusDetail"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $SeniorSpecialistTravelHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $SeniorSpecialistTravelRate);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $EngineerTravelHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $EngineerTravelRate);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $SeniorSpecialistSiteWorkHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $SeniorSpecialistSiteWorkRate);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("R" . $thisRowId, $EngineerSiteWorkHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("S" . $thisRowId, $EngineerSiteWorkRate);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, "=(L" . $thisRowId . "*M" . $thisRowId . ")+(N" . $thisRowId . "*O" . $thisRowId . ")+(P" . $thisRowId . "*Q" . $thisRowId . ")+(R" . $thisRowId . "*S" . $thisRowId . ")");
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId++;
}
	//total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . ($thisRowId + 1), "Total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("T" . ($thisRowId + 1), "=SUM(T2:T" . ($thisRowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 1) . ":T" . ($thisRowId + 1))->applyFromArray($TotalStyle);
} catch (PHPExcel_Exception $e) {
}

//524 Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["NGRIDInspections"] = "524-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", $InvoiceNumber["NGRIDInspections"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 19;
foreach($summaryRowData as $rowId=>$columnData){
	foreach ($columnData as $columnId=>$data){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($columnId, $data);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$thisRowId++;
}
foreach ($subTotals as $Hcell){
	$Gcell = str_replace("H","G",$Hcell);
	//$objPHPExcel->getActiveSheet()->getStyle($Gcell.":".$Hcell)->applyFromArray($SubTotalStyle);
	//$objPHPExcel->getActiveSheet()->getStyle($Gcell)->applyFromArray($JustBoldStyle);
	$lastSubTotalRow = $Hcell;
}
	$thisRowId++;
	//Total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Amount Due:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=" . $lastSubTotalRow);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($AmountDueStyle);
} catch (PHPExcel_Exception $e) {
}


$CETInternalFiscalRevenueSheetIndex = 4;
$CETInternalFiscalRevenueSheetName = "NGRID524";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>