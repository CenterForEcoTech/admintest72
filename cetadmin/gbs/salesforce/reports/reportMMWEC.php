<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/MMWEC_template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "MMWEC".$pathSeparator."MMWEC_Invoice_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$AmountDueStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FD8D8D8')),
	'font'  => array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)

);
$JustBoldStyle = array(
	'font'  => array('bold'=>true)
);

$SubTotalStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
);
//Call Log Report
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
$rowShift = count($callRecord);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}
ksort($callRecord);
foreach ($callRecord as $id=>$record){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, date("Y", strtotime($record["taskDueDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, date("m", strtotime($record["taskDueDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, date("d", strtotime($record["taskDueDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $record["firstName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $record["lastName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $record["street"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $record["city"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $record["zip"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $record["phone"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $record["email"]);
    } catch (PHPExcel_Exception $e) {
    }

    /*
    $objPHPExcel->getActiveSheet()->setCellValue("K".$thisRowId,$record["energyCompany"]);
    $objPHPExcel->getActiveSheet()->setCellValue("J".$thisRowId,$record["mobile"]);
    $objPHPExcel->getActiveSheet()->setCellValue("K".$thisRowId,$record["yearBuilt"]);
    $objPHPExcel->getActiveSheet()->setCellValue("L".$thisRowId,$record["heatingFuel"]);
    $objPHPExcel->getActiveSheet()->setCellValue("M".$thisRowId,$record["description"]);
    $objPHPExcel->getActiveSheet()->setCellValue("N".$thisRowId,$record["status"]);
    $objPHPExcel->getActiveSheet()->setCellValue("O".$thisRowId,$record["auditStartDate"]);

    */
		$thisRowId++;
}



//Audit Data Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
$rowShift = count($mmwecRecord);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}
ksort($mmwecRecord);

foreach ($mmwecRecord as $id=>$record){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, date("Y", strtotime($record["auditDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, date("m", strtotime($record["auditDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, date("d", strtotime($record["auditDate"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $record["auditType"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $record["lastName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $record["firstName"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $record["street"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $record["city"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $record["town2"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $record["email"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $record["bulbsInstalled"]);
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId++;
}


//Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["MMWEC"] = "MMWEC-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["MMWEC"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
$auditTypes = array("Monthly Program Fee"=>"F20","Audit"=>"F21","Verification Inspection"=>"F22","Presentation"=>"F23");
$MMWECCount["Monthly Program Fee"] = 1;
foreach ($auditTypes as $type=>$cell){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($cell, $MMWECCount[$type]);
    } catch (PHPExcel_Exception $e) {
    }
    $sumCell = str_replace("F","H",$cell);
    try {
        $revenueCodesByEmployee["MMWEC"]["40202"][$type] = $objPHPExcel->getActiveSheet()->getCell($sumCell)->getCalculatedValue();
    } catch (PHPExcel_Exception $e) {
    }
}
try {
    $revenueCodes["MMWEC"]["40202"] = $objPHPExcel->getActiveSheet()->getCell("H26")->getCalculatedValue();
} catch (PHPExcel_Exception $e) {
}


$CETInternalFiscalRevenueSheetIndex = 3;
$CETInternalFiscalRevenueSheetName = "MMWEC";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>