<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/BigY_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}


//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughsByJobID[$results->JobID][] = $results;
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID"){
			$passThroughHeaders[$key] = 1;
		}
	}
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "BigY".$pathSeparator."BigY_".date("Y-m", strtotime($invoiceEndDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

	$InvoiceStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$HighlightStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFFFF00'))
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$FooterStyle = array(
		'font'=>array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$SectionStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Fineprint = array(
		'font'  => array('bold'=>true,'size'=>8,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$AmountDueStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
//Big Y 540Summary	
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
//add month information
$columnCount = 2;
//print_pre($InvoiceYearMonths);
$TotalsColumn = "B";
if (count($InvoiceYearMonths)){
	foreach ($InvoiceYearMonths as $YearMonth=>$count){
		$MonthName = date("F",strtotime($YearMonth."-01"));
		$column = getNameFromNumber($columnCount);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . "2", $MonthName);
        } catch (PHPExcel_Exception $e) {
        }
        $columnCount++;
		$TotalsColumn = getNameFromNumber($columnCount);
	}
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($TotalsColumn . "2", "TOTAL");
    } catch (PHPExcel_Exception $e) {
    }
}
$rowsAdded = 0;

$InvoiceCodeArray = array("540"=>"BigY");
	foreach ($InvoiceCodeArray as $InvoiceCode=>$BillingRateType){
		$rowsStart = 3;
		$thisRowId = $rowsStart+$rowsAdded;
		$hoursCount = count($InvoiceDataHours[$InvoiceCode]);
			if ($hoursCount > 1){
				$rowShift = $hoursCount;
				//echo "will add ".$rowShift." before row ".($thisRowId+1)."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            $categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$BillingRateType][$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
				$columnCount = 2;
				foreach ($InvoiceYearMonths as $YearMonth=>$count){
					$column = getNameFromNumber($columnCount);
					$hour = $InvoiceDataByMonth[$Employee_Name][$InvoiceCode][$YearMonth];
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue($column . $thisRowId, $hour);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $lastColumn = $column;
					$columnCount++;
				}
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":" . $lastColumn . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($TotalsColumn . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }

                $TotalHours = bcmul($categoryRate,$hours,2);
				$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = bcadd($revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]],$TotalHours,2);
				$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$TotalHours,2);
				
				
				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
			$thisSum = $theseHours;
//			$objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId+1),1);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":" . $TotalsColumn . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            if ($thisSum){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($TotalsColumn . $thisRowId, "=SUM(" . $TotalsColumn . $categoryStartRow . ":" . $TotalsColumn . $thisLastRow . ")");
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($TotalsColumn . $thisRowId, "0");
                } catch (PHPExcel_Exception $e) {
                }
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($TotalsColumn . $thisRowId)->applyFromArray($HighlightStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $SummaryValues[$InvoiceCode][$CategoryName]["hours"] = "='540-Summary'!".$TotalsColumn.$thisRowId;
			$SummaryValues[$InvoiceCode][$CategoryName]["rate"] = $categoryRate;
			$thisRowId++;
			$rowsAdded++;
		}
	}

//Big Y 540	

	$CETInternalFiscalRevenueSheetIndex = 2;
	$CETInternalFiscalRevenueSheetName = "BigY";	
	//Create CET Internal Fiscal Revenue Sheet
	include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F21", $SummaryValues["540"]["Program Manager"]["hours"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G21", $SummaryValues["540"]["Program Manager"]["rate"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F22", $SummaryValues["540"]["Staff"]["hours"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G22", $SummaryValues["540"]["Staff"]["rate"]);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", "540-" . date("y-m", strtotime($invoiceEndDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceStartDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceEndDate)))));
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>
