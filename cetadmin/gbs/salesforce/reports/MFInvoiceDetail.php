<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/Invoice_Detail_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}


$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}
$invoiceCountNumber = ($invoiceCountNumber <=9 ? "0".$invoiceCountNumber : $invoiceCountNumber);
$invoiceName = $invoiceCountNumber."_".$ContractorName."_".$JobName."_Installed_Measures_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."exports".$pathSeparator.$invoiceName;
$webpathExtension = "salesforce".$pathSeparator."exports".$webpathSeparator.$invoiceName;

$saveLocation = $path.$pathExtension;

//add IN MEASURES
if ($INnewRowsToAdd > 0){
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, $INnewRowsToAdd);
    } catch (PHPExcel_Exception $e) {
    }
    $INRowRemoved = false;
}
if (!$hasIN){
    try {
        $objPHPExcel->getActiveSheet()->removeRow(12);
    } catch (PHPExcel_Exception $e) {
    }
    $INRowRemoved = true;
}
//add the IN data
	foreach ($MFInvoiceDetailDataINorIM["IN"] as $cell=>$dataDisplay){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }

//add IM MEASURES
$IMMeasureRow = ($IMFormulaRow);
if ($IMnewRowsToAdd > 0){
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($IMMeasureRow, $IMnewRowsToAdd);
    } catch (PHPExcel_Exception $e) {
    }
}
if (!$hasIM){
    try {
        $objPHPExcel->getActiveSheet()->removeRow(($IMMeasureRow - 1));
    } catch (PHPExcel_Exception $e) {
    }
}
//add the IM data
	foreach ($MFInvoiceDetailDataINorIM["IM"] as $cell=>$dataDisplay){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
//add non inserted data	
foreach ($MFInvoiceDetailData as $cell=>$dataDisplay){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
    } catch (PHPExcel_Exception $e) {
    }
}

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$InvoiceDetailLinks[] = "<a id='EverSourceTrackingFile' href='".$webtrackingLink."'>".$invoiceName."</a><br>";
?>