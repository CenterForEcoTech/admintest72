<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/RUSHours_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RUS".$pathSeparator."RUSHours_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;

$RIAllocationField = '\'Billing logic\'!$C$6';
$CTAllocationField = '\'Billing logic\'!$C$7';
$PAAllocationField = '\'Billing logic\'!$C$8';
$RUSAllocationField = '\'Billing logic\'!$C$13';

$RDBGCTAllocationField ='\'Billing logic\'!$J$18';
$RecycleCTAllocationField ='\'Billing logic\'!$J$19';
$RUSCTAllocationField ='\'Billing logic\'!$J$20';
$RDBDSECCTAllocationField ='\'Billing logic\'!$J$21';

$RDBGRIAllocationField ='\'Billing logic\'!$J$27';
$EPAInvoiceAllocationField ='\'Billing logic\'!$J$28';
$RUSRIAllocationField ='\'Billing logic\'!$J$29';
$RDBDSECRIAllocationField ='\'Billing logic\'!$J$30';

$ClaneilPAAllocationField ='\'Billing logic\'!$J$35';
$RUSPAAllocationField ='\'Billing logic\'!$J$36';

$fringeRate = "34%";

$revenueCodeOrder = array(40202,40205,40259,40262,40952);
$ExcelTabArray = array("Orginal logic","Billing logic","1030 RUS Coded raw Hours","CT Hours","RI Hours","PA Hours","RUS Invoice","EPA Invoice");
foreach ($ExcelTabArray as $indexId=>$excelTabName){
    try {
        $objPHPExcel->setActiveSheetIndex($indexId);
    } catch (PHPExcel_Exception $e) {
    }
    $excelTabCount = $EmployeesInGroup[$excelTabName];
	$rowShift = count($EmployeesInGroupByEmployeeWithHours["RUSHours"]);

	if ($excelTabName == "1030 RUS Coded raw Hours"){
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$employeCells[$employee_name]["1030 Share"]=$rowId;
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$hours = ($codeInfo["1030RUS"] ? $codeInfo["1030RUS"] : 0);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=B" . $rowId . "*C" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;

		}
	}elseif ($excelTabName == "CT Hours"){
		$allocatedRowId = 12;
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($allocatedRowId + ($rowShift - 3)), ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$SF270BillingRateByEmployee[$employee_name] = $categoryRate;
			if (!$categoryRate){
				$Adjustments["Alert"]["Staff missing Billing Rate Code"][]=str_replace("_",", ",$employee_name);
			}
			
			$hours = ($codeInfo["1030CT"] ? $codeInfo["1030CT"] : 0);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            $employeeHourTotalByCode[$employee_name]["1030CT"]["total"] = $hours;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=SUM(B" . $rowId . ":B" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=B" . $rowId . "*D" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;
		}
		
		//add allocations
		$rowId = ($allocatedRowId-1)+($rowShift-3);
		$hoursRowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$employeCells[$employee_name]["CT Allocation"]=$rowId;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "=B" . $hoursRowId . "*" . $RDBGCTAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030CT"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030CT"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            } //Lorenzo wants quarter hour billing as of 2/15/2018
			$SF270Hours["RBDG CT"][$employee_name] = $thisHourRoundedToNearestQuarter;
			//$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,"=SUM(B".$rowId.":B".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=C" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("D" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $RBDGCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RBDGCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - RBDG CT";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=B" . $hoursRowId . "*" . $RecycleCTAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030CT"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030CT"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"=SUM(F".$rowId.":F".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, "=G" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("H" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $RecycleCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RecycleCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - Recycle CT";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=B" . $hoursRowId . "*" . $RDBDSECCTAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("J" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030CT"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030CT"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			$SF270Hours["RBDG SECD CT"][$employee_name] = $thisHourRoundedToNearestQuarter;
			//$objPHPExcel->getActiveSheet()->setCellValue("K".$rowId,"=SUM(J".$rowId.":J".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, "=K" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("M" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("L" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $RBDGSECDCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RBDGSECDCTRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - RBDG SECD CT";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("N" . $rowId, "=B" . $hoursRowId . "*" . $RUSCTAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("N" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$thisHourRoundedToNearestQuarter = ($employeeHourTotalByCode[$employee_name]["1030CT"]["total"]-$employeeHourTotalByCode[$employee_name]["1030CT"]["parts"]);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("O" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("O".$rowId,"=SUM(N".$rowId.":N".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("P" . $rowId, "=O" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Q" . $rowId, "=C" . $rowId . "+G" . $rowId . "+O" . $rowId . "+K" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("Q".$rowId,"=B".$rowId."+F".$rowId."+N".$rowId."+J".$rowId);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("R" . $rowId, "=D" . $rowId . "+H" . $rowId . "+L" . $rowId . "+P" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }

            $rowId++;
			$hoursRowId++;

		}
		$employeCells["totalCheck"]["CT Hours"] = $rowId;
		$rowId = $rowId+8;
		foreach ($revenueCodeOrder as $index=>$code){
			$thisRowId = $rowId+$index;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $RBDGCTRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $RecycleCTRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $RBDGSECDCTRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		/*set SF270 data*/
		$sfDataRow = ($rowShift  > 3 ? (27+(2*($rowShift-3))) : 27);
        try {
            $thisValue = $objPHPExcel->getActiveSheet()->getCell("D" . $sfDataRow)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        $SF270Data["RBDG CT"]["Personell"] = $thisValue;

        try {
            $thisValue = $objPHPExcel->getActiveSheet()->getCell("L" . $sfDataRow)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        $SF270Data["RBDG SECD CT"]["Personell"] = $thisValue;
		
	}elseif ($excelTabName == "RI Hours"){
		$allocatedRowId = 12;
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($allocatedRowId + ($rowShift - 3)), ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$SF270BillingRateByEmployee[$employee_name] = $categoryRate;
			if (!$categoryRate){
				$Adjustments["Alert"]["Staff missing Billing Rate Code"][]=str_replace("_",", ",$employee_name);
			}
			
			$hours = ($codeInfo["1030RI"] ? $codeInfo["1030RI"] : 0);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            $employeeHourTotalByCode[$employee_name]["1030RI"]["total"] = $hours;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=SUM(B" . $rowId . ":B" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=B" . $rowId . "*D" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;
		}
		
		//add allocations
		$rowId = ($allocatedRowId-1)+($rowShift-3);
		$hoursRowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$employeCells[$employee_name]["RI Allocation"]=$rowId;
			$employeCells[$employee_name]["RI Hours"] = $rowId;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "=B" . $hoursRowId . "*" . $RDBGRIAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030RI"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030RI"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			$SF270Hours["RBDG RI"][$employee_name] = $thisHourRoundedToNearestQuarter;
			//$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,"=SUM(B".$rowId.":B".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=C" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("D" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $RBDGRIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RBDGRIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - RBDG RI";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=B" . $hoursRowId . "*" . $EPAInvoiceAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030RI"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030RI"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"=SUM(F".$rowId.":F".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, "=G" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("H" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $EPARIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $EPARIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=B" . $hoursRowId . "*" . $RDBDSECRIAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("J" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030RI"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030RI"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			$SF270Hours["RBDG SECD RI"][$employee_name] = $thisHourRoundedToNearestQuarter;
			//$objPHPExcel->getActiveSheet()->setCellValue("K".$rowId,"=SUM(J".$rowId.":J".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, "=K" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("M" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("L" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $RBDGSECDRIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RBDGSECDRIRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - RBDG SECD RI";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("N" . $rowId, "=B" . $hoursRowId . "*" . $RUSRIAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("N" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$thisHourRoundedToNearestQuarter = ($employeeHourTotalByCode[$employee_name]["1030RI"]["total"]-$employeeHourTotalByCode[$employee_name]["1030RI"]["parts"]);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("O" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("O".$rowId,"=SUM(N".$rowId.":N".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("P" . $rowId, "=O" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Q" . $rowId, "=C" . $rowId . "+G" . $rowId . "+O" . $rowId . "+K" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("Q".$rowId,"=B".$rowId."+F".$rowId."+J".$rowId."+N".$rowId);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("R" . $rowId, "=D" . $rowId . "+H" . $rowId . "+L" . $rowId . "+P" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }

            $rowId++;
			$hoursRowId++;

		}
		$employeCells["totalCheck"]["RI Hours"] = $rowId;	
		
		$rowId = $rowId+8;
		foreach ($revenueCodeOrder as $index=>$code){
			$thisRowId = $rowId+$index;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $RBDGRIRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $EPARIRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $RBDGSECDRIRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		/*set SF270 data*/
		$sfDataRow = ($rowShift  > 3 ? (27+(2*($rowShift-3))) : 27);
        try {
            $thisValue = $objPHPExcel->getActiveSheet()->getCell("D" . $sfDataRow)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        $SF270Data["RBDG RI"]["Personell"] = $thisValue;

        try {
            $thisValue = $objPHPExcel->getActiveSheet()->getCell("L" . $sfDataRow)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        $SF270Data["RBDG SECD RI"]["Personell"] = $thisValue;
		
	}elseif ($excelTabName == "PA Hours"){
		$allocatedRowId = 12;
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($allocatedRowId + ($rowShift - 3)), ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$SF270BillingRateByEmployee[$employee_name] = $categoryRate;
			if (!$categoryRate){
				$Adjustments["Alert"]["Staff missing Billing Rate Code"][]=str_replace("_",", ",$employee_name);
			}
			
			$hours = ($codeInfo["1030PA"] ? $codeInfo["1030PA"] : 0);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            $employeeHourTotalByCode[$employee_name]["1030PA"]["total"] = $hours;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=SUM(B" . $rowId . ":B" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=B" . $rowId . "*D" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;
		}
		
		//add allocations
		$rowId = ($allocatedRowId-1)+($rowShift-3);
		$hoursRowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$employeCells[$employee_name]["PA Allocation"]=$rowId;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "=B" . $hoursRowId . "*" . $ClaneilPAAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$employeeHourTotalByCode[$employee_name]["1030PA"]["parts"] = ($employeeHourTotalByCode[$employee_name]["1030PA"]["parts"]+$thisHourRoundedToNearestQuarter);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,"=SUM(B".$rowId.":B".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=C" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisAmount = $objPHPExcel->getActiveSheet()->getCell("D" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $ClaneilPARevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $ClaneilPARevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$BillingRateType = "270 - Claneil PA";
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$thisAmount;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$thisAmount;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=B" . $hoursRowId . "*" . $RUSPAAllocationField);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $thisHourRoundedToNearestQuarter = floorToFraction($thisHour);
			$thisHourRoundedToNearestQuarter = ($employeeHourTotalByCode[$employee_name]["1030PA"]["total"]-$employeeHourTotalByCode[$employee_name]["1030PA"]["parts"]);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisHourRoundedToNearestQuarter);
            } catch (PHPExcel_Exception $e) {
            }//Lorenzo wants quarter hour billing as of 2/15/2018
			//$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"=SUM(F".$rowId.":F".$rowId.")");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, "=G" . $rowId . "*D" . $hoursRowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=C" . $rowId . "+G" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("J".$rowId,"=B".$rowId."+F".$rowId);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, "=D" . $rowId . "+H" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }

            $rowId++;
			$hoursRowId++;

		}
		$employeCells["totalCheck"]["PA Hours"] = $rowId;
		$rowId = $rowId+8;
		foreach ($revenueCodeOrder as $index=>$code){
			$thisRowId = $rowId+$index;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $ClaneilPARevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
	}elseif ($excelTabName == "RUS Invoice"){
		$totalCheckRow = 6;
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
            $totalCheckRow = 6+($rowShift-3);
		}
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$SF270BillingRateByEmployee[$employee_name] = $categoryRate;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "='1030 RUS Coded raw Hours'!B" . $employeCells[$employee_name]["1030 Share"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=B" . $rowId . "*J" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "='CT Hours'!O" . $employeCells[$employee_name]["CT Allocation"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=D" . $rowId . "*J" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "='RI Hours'!O" . $employeCells[$employee_name]["RI Allocation"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "=F" . $rowId . "*J" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, "='PA Hours'!G" . $employeCells[$employee_name]["PA Allocation"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, "=H" . $rowId . "*J" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, "=(SUM(B" . $rowId . "+D" . $rowId . "+F" . $rowId . "+H" . $rowId . ")*J" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $billableHours = $objPHPExcel->getActiveSheet()->getCell("K" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, "=B" . $rowId . "+D" . $rowId . "+F" . $rowId . "+H" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("L" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $SF270Hours["RUS"][$employee_name] = $thisHour;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("M" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, "=(SUM(B" . $rowId . "+D" . $rowId . "+F" . $rowId . "+H" . $rowId . ")*J" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            $RUSRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $RUSRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$billableHours;

			$BillingRateType = "RUS Invoice";
			
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$billableHours;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$billableHours;
			
			
			$rowId++;
		}
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $totalCheckRow, "='CT Hours'!P" . $employeCells["totalCheck"]["CT Hours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $totalCheckRow, "='RI Hours'!P" . $employeCells["totalCheck"]["RI Hours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $totalCheckRow, "='PA Hours'!H" . $employeCells["totalCheck"]["PA Hours"]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		$rowId = $totalCheckRow+3;
		foreach ($revenueCodeOrder as $index=>$code){
			$thisRowId = $rowId+$index;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $RUSRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		/*set SF270 data*/
		$sfDataRow = ($rowShift  > 3 ? (14+($rowShift-3)) : 14);
        try {
            $thisValue = $objPHPExcel->getActiveSheet()->getCell("C" . $sfDataRow)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        $SF270Data["RUS"]["Personell"] = $thisValue;
		
	}elseif ($excelTabName == "EPA Invoice"){
		$totalCheckRow = 6;
		if ($rowShift > 3){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, ($rowShift - 3));
            } catch (PHPExcel_Exception $e) {
            }
            $totalCheckRow = 6+($rowShift-3);
		}
		
		//add hours
		$rowId = 2;
		foreach ($EmployeesInGroupByEmployeeWithHours["RUSHours"] as $employee_name=>$codeInfo){
			$categoryRate = $BillingRateByEmployeeJustRate["EPA"][$employee_name];
			$SF270BillingRateByEmployee[$employee_name] = $categoryRate;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("_", ", ", $employee_name));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "='RI Hours'!F" . $employeCells[$employee_name]["RI Hours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "='RI Hours'!G" . $employeCells[$employee_name]["RI Hours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=C" . $rowId . "*D" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $fringeRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "=E" . $rowId . "*F" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, "=E" . $rowId . "-G" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, "=SUM(G" . $rowId . ":H" . $rowId . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $billableHours = $objPHPExcel->getActiveSheet()->getCell("E" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, "=E" . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
            } catch (PHPExcel_Exception $e) {
            }
            $EAPInvoiceRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]] = $EAPInvoiceRevenueCodes[$EmployeesRevenueCodeByName[$employee_name]]+$billableHours;

			$BillingRateType = "RI EPA Invoice";
			
			$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$billableHours;
			$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$billableHours;
			
			
			$rowId++;
		}
		$rowId = $rowId+3;
		foreach ($revenueCodeOrder as $index=>$code){
			$thisRowId = $rowId+$index;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $EAPInvoiceRevenueCodes[$code]);
            } catch (PHPExcel_Exception $e) {
            }
        }

	}else{
		//Do nothing
	}// end if tabName
	$lastIndex = $indexId;
}// end for each

$CETInternalFiscalRevenueSheetIndex = ($lastIndex+1);
$CETInternalFiscalRevenueSheetName = "RUS-1030";	
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');


//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
//$triggerResultFiles[] = $CurrentServer.$adminFolder."gbs/".$webpathExtension;
$triggerResultFiles["Small Consultant"] = $saveLocation;

?>