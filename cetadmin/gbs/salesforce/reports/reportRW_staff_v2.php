<?php
	$BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$diversionThisMonthReduced = ($diversionReportThisMonth["Reduced"] < 100 && $diversionReportThisMonth["Reduced"] >= 1 ? round($diversionReportThisMonth["Reduced"]) : $diversionReportThisMonth["Reduced"]);
	$diversionThisMonthRecycled = ($diversionReportThisMonth["Recycled"] < 100 && $diversionReportThisMonth["Recycled"] >= 1 ? round($diversionReportThisMonth["Recycled"]) : $diversionReportThisMonth["Recycled"]);
	$diversionThisMonthReused = ($diversionReportThisMonth["Reused"] < 100 && $diversionReportThisMonth["Reused"] >= 1 ? round($diversionReportThisMonth["Reused"]) : $diversionReportThisMonth["Reused"]);
	$diversionThisMonthFoodDonated = ($diversionReportThisMonth["Food Donated"] < 100 && $diversionReportThisMonth["Food Donated"] >= 1 ? round($diversionReportThisMonth["Food Donated"]) : $diversionReportThisMonth["Food Donated"]);
	$diversionThisMonthComposted = $diversionReportThisMonth["Composted"]+$diversionReportThisMonth["Animal feed"]+$diversionReportThisMonth["Anaerobic Digestion"];	
	$diversionThisMonthComposted = ($diversionThisMonthComposted < 100 && $diversionThisMonthComposted >= 1 ? round($diversionThisMonthComposted) : $diversionThisMonthComposted);
	$diversionYTDComposted = $diversionReportYTD["Composted"]+$diversionReportYTD["Animal feed"]+$diversionReportYTD["Anaerobic Digestion"];
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G6", date("m/15/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "RW-" . date("Y-m", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D16", date("F", strtotime($invoiceDate)) . " Diversion");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D17", ($diversionThisMonthRecycled ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F17", round($diversionReportYTD["Recycled"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D18", ($diversionReportThisMonth["Lamps"] ?: "0") . " units");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F18", $diversionReportYTD["Lamps"] . " units");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D19", ($diversionThisMonthReused ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F19", round($diversionReportYTD["Reused"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D20", ($diversionThisMonthFoodDonated ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F20", round($diversionReportYTD["Food Donated"]) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D21", ($diversionThisMonthComposted ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F21", round($diversionYTDComposted) . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D22", ($diversionThisMonthReduced ?: "0") . " tons");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F22", round($diversionReportYTD["Reduced"], 1) . " tons");
} catch (PHPExcel_Exception $e) {
}

$HighlightStyle = array('fill'=> array('type'=> PHPExcel_Style_Fill::FILL_SOLID,'color'=> array('argb' => 'FFFFF00')));
	//532 = WasteWise & C/U, 533 = Marketing (bullets), 534 = Technical Assistance, 535 = Website, 538C = Compost Site TA, 536 = Best Management Practices
	//$CodeOrderArray = array("EventsCompleted"=>28,"EventsUpcoming"=>32,"DataAnalyist"=>36,"532"=>48,"Marketing"=>57,"533"=>62,"533Targeted"=>68,"534"=>82,"535"=>95,"538C"=>126,"536D"=>133,"536"=>134);
	//$CodeOrderArray = array("EventsCompleted"=>30,"EventsUpcoming"=>34,"DataAnalyist"=>44,"532"=>59,"Marketing"=>63,"CaseStudiesCompleted"=>70,"CaseStudiesInprocess"=>75,"533"=>79,"Website"=>83,"BusinessWarning"=>107,"CompostSiteTier"=>118,"CDTA"=>139,"BMPDevelopment"=>158); //added CDTA
	$CodeOrderArray = array("EventsCompleted"=>30,"EventsUpcoming"=>35,"DataAnalyist"=>45,"532"=>60,"Marketing"=>64,"CaseStudiesCompleted"=>78,"CaseStudiesInprocess"=>83,"533"=>87,"Website"=>91,"BusinessWarning"=>115,"CompostSiteTier"=>126,"CDTA"=>150); //Adjusted for additional section, removed ,"BMPDevelopment"=>165 8/22/2019 - Added 1 to all sections to account for reduction on table 12.30.2019
	$rowsAdded = 0;
	$rowShift = 0;
	foreach ($CodeOrderArray as $InvoiceCode=>$DetailRow){
		$rowId = $DetailRow+$rowsAdded;
		$statusRowStart = $rowId;
		$countRowsToDisplay = $bulletsCodeCount[$InvoiceCode];
		//print "bulletsCodeCount".$InvoiceCode."= ".$bulletsCodeCount[$InvoiceCode];
		
		if ($InvoiceCode == "Marketing"){
			$DataAnalyistRow = $DetailRow+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($MTDRWevents ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($YTDRWevents ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
        }
//			$DataAnalyistRow = $DetailRow+$rowsAdded;
		
		if ($InvoiceCode == "EventsCompleted"){
			$rowShift = $eventsCompletedCount;
			if ($rowShift){
				//echo "Will add Events Completed $rowShift at row: $statusRowStart <br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
                foreach ($eventsCompleted as $eventDate=>$events){
					foreach ($events as $event){
						$eventDate = date("m/d/Y",strtotime($event["eventDate"]));
						$workPlan = $event["workPlan"];
						$eventName = $event["event"];
						$description = $event["description"];
						$attendance = $event["attendance"];
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $eventDate);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $workPlan);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $eventName);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("C" . $rowId . ":E" . $rowId);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $description);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("F" . $rowId . ":G" . $rowId);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $attendance);
                        } catch (PHPExcel_Exception $e) {
                        }
                        $statusRowEnd = $rowId;
                        try {
                            $value = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getValue();
                        } catch (PHPExcel_Exception $e) {
                        }
                        $width = mb_strwidth ($value); //Return the width of the string
						$rowHeight = 35;
						if ($width > 55){
							$rowHeight = ($rowHeight*ceil($width/55));
                            try {
                                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":F" . $rowId)->getAlignment()->setWrapText(true);
                            } catch (PHPExcel_Exception $e) {
                            }
                        }else{
                            try {
                                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                            } catch (PHPExcel_Exception $e) {
                            }
                        }
						
						$rowId++;
						$rowsAdded++;
					}
				}
				if ($statusRowStart && $statusRowEnd){
					//echo "Events Completed rows were from $statusRowStart to $statusRowEnd<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":B" . $statusRowEnd)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			}
		}
		
		if ($InvoiceCode == "EventsUpcoming"){
			$rowShift = $eventsUpcomingCount;
			if ($rowShift){
				//echo "Will add upcoming events $rowShift at row: $statusRowStart <br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
                foreach ($eventsUpcoming as $eventDate=>$events){
					foreach ($events as $event){
						$eventDate = ($event["eventDate"] != "TBD" ? date("m/d/Y",strtotime($event["eventDate"])) : "TBD");
						$workPlan = $event["workPlan"];
						$eventName = $event["event"];
						$description = $event["description"];
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $eventDate);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $workPlan);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $eventName);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("C" . $rowId . ":E" . $rowId);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $description);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("F" . $rowId . ":H" . $rowId);
                        } catch (PHPExcel_Exception $e) {
                        }
                        $statusRowEnd = $rowId;
                        try {
                            $value = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getValue();
                        } catch (PHPExcel_Exception $e) {
                        }
                        $width = mb_strwidth ($value); //Return the width of the string
						$rowHeight = 35;
						if ($width > 70){
							$rowHeight = ($rowHeight*ceil($width/70));
                            try {
                                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                            } catch (PHPExcel_Exception $e) {
                            }
                        }else{
                            try {
                                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                            } catch (PHPExcel_Exception $e) {
                            }
                        }
						
						$rowId++;
						$rowsAdded++;
					}
				}
				if ($statusRowStart && $statusRowEnd){
					//echo "upcoming events rows were from $statusRowStart to $statusRowEnd<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":B" . $statusRowEnd)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			}
		
		}
		
		if ($InvoiceCode == "DataAnalyist"){
			$DataAnalyistRow = $DetailRow+$rowsAdded;
			//$objPHPExcel->getActiveSheet()->setCellValue("F".$DataAnalyistRow,$dataAnalysisThisMonth["CreatedBI"]); //New Business and Institution MTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $DataAnalyistRow, $newRequestsBI);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("G".$DataAnalyistRow,$dataAnalysisYTD["CreatedBI"]); //New Businesses and Institution YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $DataAnalyistRow, $YTDnewRequestsBI);
            } catch (PHPExcel_Exception $e) {
            } //New Businesses and Institution YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 1), $newRequestsProc);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+1),$dataAnalysisThisMonth["CreatedHP"]); //New Haulers & Processors MTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+1),$dataAnalysisYTD["CreatedHP"]); //New Haulers & Processors YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 1), $YTDnewRequestsProc);
            } catch (PHPExcel_Exception $e) {
            } //New Haulers & Processors YTD
			//$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+2),"=F".$DataAnalyistRow."+F".($DataAnalyistRow+1)); // New BI MTD + New HP MTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 2), $newRequestsMTD);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 2), "=G" . $DataAnalyistRow . "+G" . ($DataAnalyistRow + 1));
            } catch (PHPExcel_Exception $e) {
            } // New BI YTD + New HP YTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+2),$newRequestsYTD); // New BI YTD + New HP YTD
			//$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+3),$dataAnalysisThisMonth["CompletedBI"]); //Completed Businesses and Institutions MTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 3), $MTDCompRequestsBI);
            } catch (PHPExcel_Exception $e) {
            } //Completed Businesses and Institutions MTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+3),$dataAnalysisYTD["CompletedBI"]); //Completed Businesses and Institutions YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 3), $YTDCompRequestsBI);
            } catch (PHPExcel_Exception $e) {
            } //Completed Businesses and Institutions YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 4), $MTDCompRequestsProc);
            } catch (PHPExcel_Exception $e) {
            } //Completed Haulers and Processors MTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 4), $YTDCompRequestsProc);
            } catch (PHPExcel_Exception $e) {
            } //Completed Haulers and Processors YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 5), "=F" . ($DataAnalyistRow + 3) . "+F" . ($DataAnalyistRow + 4));
            } catch (PHPExcel_Exception $e) {
            } //Total number of completed MTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 5), "=G" . ($DataAnalyistRow + 3) . "+G" . ($DataAnalyistRow + 4));
            } catch (PHPExcel_Exception $e) {
            } //Total number of completed ytd
			//$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+6),$dataAnalysisThisMonth["Open"]); //Total Number of previously open requests receiving assistance
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 6), $dataAnalysisThisMonth["previouslyOpen"]);
            } catch (PHPExcel_Exception $e) {
            } //Total Number of previously open requests receiving assistance
			//$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+7),"=F".($DataAnalyistRow+2)."+F".($DataAnalyistRow+6)); //Total receiving assistance MTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+7),"=G".($DataAnalyistRow+2)."+F".($DataAnalyistRow+6)); //total receiving assistance YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 7), $totalRequestsMTD);
            } catch (PHPExcel_Exception $e) {
            } //total receiving assistance YTD
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 7), $totalRequestsYTD);
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+3),$dataAnalysisThisMonth["CompletedBI"]); //Completed Businesses and Institutions MTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+3),$dataAnalysisYTD["CompletedBI"]); //Completed Businesses and Institutions YTD
			//$objPHPExcel->getActiveSheet()->setCellValue("F".($DataAnalyistRow+4),"=F".$DataAnalyistRow."+F".($DataAnalyistRow+1)); // New BI MTD + New HP MTD
			//$objPHPExcel->getActiveSheet()->setCellValue("G".($DataAnalyistRow+4),"=G".$DataAnalyistRow."+G".($DataAnalyistRow+1)); // New BI YTD + New HP YTD

		}
	
		if ($InvoiceCode == "532"){
			$rowShift = $bulletsCodeCount[$InvoiceCode];
			if ($rowShift > 1){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
				
				
				$campaignName = "";
				foreach ($rowsToDisplay as $rowToDisplay){
					$statusDetails = nl2br($rowToDisplay["statusDetail"],false);
					$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
					$statusDetails = str_replace("<br>"," ",$statusDetails);
					$statusDetails = str_replace("\r\n"," ",$statusDetails);
					$statusDetails = str_replace("\n\r"," ",$statusDetails);
					$statusDetails = str_replace("\r"," ",$statusDetails);
					$statusDetails = str_replace("\n"," ",$statusDetails);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 15;
					if ($width > 70){
						$rowHeight = ($rowHeight*ceil($width/70));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
						//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					$statusRowEnd = $rowId;
					$rowId++;
					if ($rowShift>1){
						$rowsAdded++;
					}
				}
			}
			if ($statusRowStart && $statusRowEnd){
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":H" . $statusRowEnd)->getAlignment()->setWrapText(true);
                } catch (PHPExcel_Exception $e) {
                }
            }
		}
		if ($InvoiceCode == "CaseStudiesCompleted"){
			//echo "Will add CaseStudies Completed $rowShift at row: $statusRowStart <br>"; 
			//echo "Row ID is : ".$rowId;
			//$rowId = $rowId + 2;
			$rowShift = count($caseStudies["Completed"]);
			if ($rowShift){
				if ($rowShift > 1){
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, ($rowShift - 1));
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				foreach ($caseStudies["Completed"] as $caseStudy){
					$topic = $caseStudy["topic"];
					$type = $caseStudy["type"];
					$entity = $caseStudy["entity"];
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $topic);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("A" . $rowId . ":C" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $type);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $entity);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("E" . $rowId . ":F" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $statusRowEnd = $rowId;
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("D" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string
					$rowHeight = 20;
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($width > 45){
						$rowHeight = ($rowHeight*ceil($width/45));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					if ($rowShift > 1){
						$rowId++;
						$rowsAdded++;
					}
				}
				$rowId = $rowId + 2;
				if ($statusRowStart && $statusRowEnd){
					//echo "case studies completed rows were from $statusRowStart to $statusRowEnd<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $statusRowStart . ":F" . $statusRowEnd)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $statusRowStart . ":F" . $statusRowEnd)->applyFromArray($BStyle);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			}
			$rowsDeleted = 1;
            try {
                $objPHPExcel->getActiveSheet()->removeRow(($statusRowStart - 1));
            } catch (PHPExcel_Exception $e) {
            }
        }
			if ($InvoiceCode == "CaseStudiesInprocess"){
			$rowShift = count($caseStudies["Inprocess"]);
			//$statusRowStart++; //added by LH
			$statusRowEnd = $statusRowStart+$rowShift; //added by LH
			if ($rowShift){
				if ($rowShift > 1){
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                    } catch (PHPExcel_Exception $e) {
                    } //($rowShift-1)
				}
				foreach ($caseStudies["Inprocess"] as $caseStudy){
					$topic = $caseStudy["topic"];
					$type = $caseStudy["type"];
					$entity = $caseStudy["entity"];
					$status = $caseStudy["status"];
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $topic);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("A" . $rowId . ":C" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $type);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $entity);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("E" . $rowId . ":F" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $status);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("G" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $statusRowEnd = $rowId; //LH moved down
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("G" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string
					$rowHeight = 20;
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($width > 25){
						$rowHeight = ($rowHeight*ceil($width/25));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					if ($rowShift > 1){
						$rowId++;
						$rowsAdded++;
						
					}
				}
				if ($statusRowStart && $statusRowEnd){
					//echo "Case studies in process rows were from $statusRowStart to $statusRowEnd<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $statusRowStart . ":H" . $statusRowEnd)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $statusRowStart . ":H" . $statusRowEnd)->applyFromArray($BStyle);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			}
			$rowsDeleted++;
                try {
                    $objPHPExcel->getActiveSheet()->removeRow(($statusRowStart - 1));
                } catch (PHPExcel_Exception $e) {
                }

            }
		if ($InvoiceCode == "533"){
			$rowShift = $bulletsCodeCount[$InvoiceCode];
			if ($rowShift > 1){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
				
				$campaignName = "";
				foreach ($rowsToDisplay as $rowToDisplay){
					$statusDetails = nl2br($rowToDisplay["statusDetail"],false);
					$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
					$statusDetails = str_replace("<br>"," ",$statusDetails);
					$statusDetails = str_replace("\r\n"," ",$statusDetails);
					$statusDetails = str_replace("\n\r"," ",$statusDetails);
					$statusDetails = str_replace("\r"," ",$statusDetails);
					$statusDetails = str_replace("\n"," ",$statusDetails);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 15;
					if ($width > 70){
						$rowHeight = ($rowHeight*ceil($width/70));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
						//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					$statusRowEnd = $rowId;
					$rowId++;
					if ($rowShift>1){
						$rowsAdded++;
					}
				}
			}
			if ($statusRowStart && $statusRowEnd){
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":H" . $statusRowEnd)->getAlignment()->setWrapText(true);
                } catch (PHPExcel_Exception $e) {
                }
            }
		}
		//print $bulletsByCampaignCodeSorted;
		//echo "Bullets by Campaign Code Sorted: ".($bulletsByCampaignCodeSorted["535 Update Blog Entry"]);
		//echo "bulletsByCampaignCodeSorted: ".$bulletsByCampaignCodeSorted["535"]["535 Update Blog Entry"];
		
		if ($InvoiceCode == "Website"){
			if ($shiftRows == "Down"){$rowsAdded = $rowsAdded+1;}
			if ($shiftRows == "Up"){$rowsAdded = $rowsAdded-1;}
			$DataAnalyistRow = $DetailRow+($rowsAdded+1);
			//print "bulletsByCampaignCodeSorted[InvoiceCode]= ".$bulletsByCampaignCodeSorted[InvoiceCode]; 
			//print "rowToDisplay[primaryCampaignCode]= ".$rowToDisplay["primaryCampaignCode"]." "; //533
			//print "WebsiteBlogPosts= ".count($websiteBlogPosts); //empty
			//echo "Website starts at row: ".$DataAnalyistRow."<br>";
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, $websiteNewsletterDateSent);
            } catch (PHPExcel_Exception $e) {
            }
            $DataAnalyistRow++;
			//echo "Blog count at row ".$DataAnalyistRow."<br>";
			//echo "Blog count is: ".count($InvoiceCode["535 Update Blog Entry"])." "; //1
			//echo "bulletsCodeCount: ".count($bulletsCodeCount["535 Update Blog Entry"])." "; //0
			//echo "gpType Blog Count: ".count($gpType["535 Update Blog Entry"])." "; //1
			//print "gpType Blog Entry array: ".$gpType["535 Update Blog Entry"]." "; //empty
			//echo "TAActivities[initiated]: ".count($TAActivities["initiated"]); //1
			//echo "TAActivities[Completed]: ".count($TAActivities["Completed"]);	//2	 	
			//echo "break".$gpType["535 Update Blog Entry"]; //empty
			
			$countBlogPosts = count($TAActivities["initiated"])+count($TAActivities["Completed"]); //added this line
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow), ($countBlogPosts));
            } catch (PHPExcel_Exception $e) {
            } //count
			
			$DataAnalyistRow = $DataAnalyistRow+2;		
			//echo "Stats start at row ".$DataAnalyistRow."<br>";
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $DataAnalyistRow, date("F", strtotime($invoiceDate)) . " Website Stats:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 1), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), money($analyticsResults["totalVisits"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 2), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), money($analyticsResults["pageviews"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 3), $analyticsResults["pageviewsPerSession"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 4), $analyticsResults["avgSessionDuration"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 5), $analyticsResults["percentNewSession"]);
            } catch (PHPExcel_Exception $e) {
            }

            $rowId = $statusRowStart = $DataAnalyistRow+11;
			$rowShift = $bulletsCodeCount["535"];
			if ($rowShift > 1){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			//print "<Br> gpType: ".$gpType."<Br>";
			//echo "InvoiceCode= ".$InvoiceCode;
			foreach ($bulletsByCampaignCodeSorted["535"] as $Status=>$rowsToDisplay){
				//if ($gpType !== "535 Update Blog Entry"){ //added this, delete if crash
				//print "thisRowStart= ".$thisRowStart;
				//echo "rowToDisplay for primary campaign code: ".$rowToDisplay["primaryCampaignCode"];
				//echo "rowToDisplay for greenProspectPhase: ".$rowToDisplay[$greenProspectPhase];	//empty			
				$campaignName = "";
				foreach ($rowsToDisplay as $rowToDisplay){
					$statusDetails = nl2br($rowToDisplay["statusDetail"],false);
					$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
					$statusDetails = str_replace("<br>"," ",$statusDetails);
					$statusDetails = str_replace("\r\n"," ",$statusDetails);
					$statusDetails = str_replace("\n\r"," ",$statusDetails);
					$statusDetails = str_replace("\r"," ",$statusDetails);
					$statusDetails = str_replace("\n"," ",$statusDetails);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 15;
					if ($width > 70){
						$rowHeight = ($rowHeight*ceil($width/70));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
						//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					$statusRowEnd = $rowId;
					$rowId++;
					if ($rowShift>1){
						$rowsAdded++;
					}
				}
				
			} 
			
			//echo "Website Status Row Start = ".$statusRowStart."Status Row End = ".$statusRowEnd."<br>";
			if ($statusRowStart && $statusRowEnd){
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":H" . $statusRowEnd)->getAlignment()->setWrapText(true);
                } catch (PHPExcel_Exception $e) {
                }
            }
			
			//echo "gpType: ".$gpType;
			//echo "greenProspectPhase: ".$greenProspectPhase;
		}
		//echo "Invoice Code: ".$InvoiceCode."<br>";
		
		if ($InvoiceCode == "BusinessWarning"){
			$DataAnalyistRow = $DetailRow+($rowsAdded+1);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($NONReportTypeCountThisMonth["completed"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("D".$DataAnalyistRow,"NONReportTypeCountThisMonthcompleted");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($NONReportTypeCount["completed"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            //$objPHPExcel->getActiveSheet()->setCellValue("D".($DataAnalyistRow+1),"NONReportTypeCountcompleted");
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), ($NONReportTypeCount["TA"] + $NONReportTypeCount["outreach"]));
            } catch (PHPExcel_Exception $e) {
            }
        }
		//echo "NonReport Type Count: ".$NONReportTypeCount["TA"]."<br>";
		if ($InvoiceCode == "CompostSiteTier"){
			$DataAnalyistRow = $DetailRow+($rowsAdded+1);
			
			//echo "starting CompostSiteTier at: ".$DataAnalyistRow."<br>";
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $DataAnalyistRow, ($tierCount["completed"]["Tier 1"]["ThisMonth"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($tierCount["completed"]["Tier 1"]["ThisYear"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 1), ($tierCount["completed"]["Tier 2"]["ThisMonth"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($tierCount["completed"]["Tier 2"]["ThisYear"] ?: 0));
            } catch (PHPExcel_Exception $e) {
            }
            $tierTypes = array("completed"=>5,"initiated"=>9,"ongoing"=>13);
			$thisRowsAdded = 0;
			foreach ($tierTypes as $tierType=>$rowStart){
				$thisRowStart = $DataAnalyistRow+$rowStart+$thisRowsAdded;
				//echo $tierType." rowStart: (thisRowStart)".$thisRowStart." = ".$DataAnalyistRow."+(rowstart)".$rowStart."+(rowsAdded)".$thisRowsAdded."<br>";
				ksort($tiers[$tierType]);
				
				$rowShift = $tiers[$tierType]["count"];
				if ($rowShift > 1){
					//echo $tierType." rowshift: ".($rowShift-1)."<br>";
					//echo "Tier: ".$tierType." will insert ".$rowShift." before row ".$thisRowStart."<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($thisRowStart, ($rowShift - 1));
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				
				
				$thisRowsAddedJustNow = 0;
				$thisCounter = 1;
				foreach ($tiers[$tierType] as $tier=>$rowDatas){
					foreach ($rowDatas as $rowData){
						$thisRowNum = $thisRowStart+$thisRowsAddedJustNow; //thisRowStart
						//echo $tierType." ".$thisRowNum."=".$rowData["account"]." ".$tier."<br> Rows Added Just Now= ".$thisRowsAddedJustNow."<br>";
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowNum, $rowData["account"]);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("A" . $thisRowNum . ":C" . $thisRowNum);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowNum, $rowData["tier"]);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowNum . ":D" . $thisRowNum)->getFont()->setBold(false);
                        } catch (PHPExcel_Exception $e) {
                        }
                        if ($rowShift > 1 && $thisCounter > 1){
							$thisRowsAdded++;
							//echo "Just added row ".$thisRowsAdded."<br>";
							$rowsAdded++;							
						}
						$thisRowsAddedJustNow++;
						$thisCounter++;						
					}
				}
			}
		
			//$bulletsByGPType
			//$rowId = $thisRowNum+6;
			//$objPHPExcel->getActiveSheet()->insertNewRowBefore($thisRowNum,$rowShift);//added this Lauren
			$DataAnalyistRow++;
			//$rowId++;
			
			
			//$rowId = ($thisRowNum + $thisRowsAddedJustNow);
			//$rowId++;
			//$rowId++;
			//print "Row to display= ".$rowToDisplay; //array
			//$thisRowsAddedJustNow = 4; 
			//print "<br> thisRowsAddedJustNow= ".$thisRowsAddedJustNow;  //4
			//print "<br> statusRowStart= ".$statusRowStart; //156
			//print "<br> statusRowEnd= ".$statusRowEnd;
			//print "<br> thisRowNum= ".$thisRowNum; //173
			//print "<br> thisRowStart= ".$thisRowStart;  //171
			
			//print "<br> rowsAdded= ".$rowsAdded; //34
			//print "<br> rowsDeleted= ".$rowsDeleted; //2
			//print "<br> rowsToDisplay= ".count($rowsToDisplay); //array 5
			//print "<br> rowstoDisplay= ".count($rowstoDisplay); //empty 0
			//print "<br> rowstodisplay= ".count($rowstodisplay); //empty 0
			//print "<br> rowstarter= ".$rowStarter;	//empty		
			//print "<br> DataAnalyistRow= ".$DataAnalyistRow; //158
			//$rowId = $rowId+6; // changed this from 194;
			//echo "<br> Row is: ".$rowId."<br>";
			$rowId = $thisRowNum;
			$rowId++;
			$rowId++;
			$rowId++;
			$rowId++;
			//print "<br> rowId= ".$rowId; //190
			foreach ($bulletsByGPType["538C Outreach & Follow Up"] as $Status=>$rowsToDisplay){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, 1);
                } catch (PHPExcel_Exception $e) {
                }  //change back to rowId
				
				
				$campaignName = "";
				foreach ($rowsToDisplay as $rowToDisplay){
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, 1);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $statusDetails = nl2br($rowToDisplay["statusDetail"],false);
					$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
					$statusDetails = str_replace("<br>"," ",$statusDetails);
					$statusDetails = str_replace("\r\n"," ",$statusDetails);
					$statusDetails = str_replace("\n\r"," ",$statusDetails);
					$statusDetails = str_replace("\r"," ",$statusDetails);
					$statusDetails = str_replace("\n"," ",$statusDetails);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 15;
					if ($width > 70){
						$rowHeight = ($rowHeight*ceil($width/70));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
						//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					
					$statusRowStart = $rowId;
					$statusRowEnd = ($statusRowStart + $rowsAdded);
					$rowsAdded++;
				}
				}
				if ($statusRowStart && $statusRowEnd){
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":H" . $statusRowEnd)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			//echo "Status Row Start = ".$statusRowStart." Status Row End = ".$statusRowEnd." Rows Added = ".$rowsAdded."<br>";
			}
			//echo "RowID before ".$InvoiceCode." = ".$rowId." ";
			//echo "InvoiceCode = ".$InvoiceCode."<br>";
			if ($InvoiceCode == "CDTA"){
			//$DataAnalyistRow = $DetailRow+($rowsAdded);
			//echo "Phase count print: ";
			//echo count($phases["completed"]);
			//print $phaseCount["completed"][2]["ThisMonth"];
			//echo "  This year:  ";
			//print $phaseCount["completed"]["2"]["ThisYear"];
			//echo "<br> Phase count vardump ";
			//var_dump ($phases[$phaseType]["count"]);
			//echo "starting CompostSiteTier at: ".$DataAnalyistRow."<br>";
			//$rowId = $rowId + 2;
			//$rowId++;
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, ("Phase 1"));
                } catch (PHPExcel_Exception $e) {
                } //rowId=150
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, ($phaseCount["completed"][1]["ThisMonth"] ?: 0));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, ($phaseCount["completed"]["1"]["ThisYear"] ?: 0));
                } catch (PHPExcel_Exception $e) {
                } //"" LH
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . ($rowId + 1), ("Phase 2"));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . ($rowId + 1), ($phaseCount["completed"][2]["ThisMonth"] ?: 0));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 1), ($phaseCount["completed"]["2"]["ThisYear"] ?: 0));
                } catch (PHPExcel_Exception $e) {
                } //"" LH
			$phaseTypes = array("completed"=>5,"initiated"=>9,"ongoing"=>13);
			$thisRowsAdded = 0;
			foreach ($phaseTypes as $phaseType=>$rowStart){
				$thisRowStart = $rowId+$rowStart+$thisRowsAdded;
				//echo $phaseType." rowStart: (thisRowStart)".$thisRowStart." = ".$rowId."+(rowstart)".$rowStart."+(rowsAdded)".$thisRowsAdded."<br>";
				ksort($phases[$phaseType]);
				
				$rowShift = $phases[$phaseType]["count"];
				if ($rowShift > 1){
					//echo $tierType." rowshift: ".($rowShift-1)."<br>";
					//echo "Tier: ".$tierType." will insert ".$rowShift." before row ".$thisRowStart."<br>";
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($thisRowStart, ($rowShift - 1));
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				
				
				$thisRowsAddedJustNow = 0;
				$thisCounter = 1;
				foreach ($phases[$phaseType] as $phase=>$rowDatas){
					foreach ($rowDatas as $rowData){
						$thisRowNum = $thisRowStart+$thisRowsAddedJustNow; //thisRowStart
						//echo $tierType." ".$thisRowNum."=".$rowData["account"]." ".$tier."<br> Rows Added Just Now= ".$thisRowsAddedJustNow."<br>";
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowNum, $rowData["account"]);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->mergeCells("A" . $thisRowNum . ":C" . $thisRowNum);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowNum, $rowData["phase"]);
                        } catch (PHPExcel_Exception $e) {
                        }
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowNum . ":D" . $thisRowNum)->getFont()->setBold(false);
                        } catch (PHPExcel_Exception $e) {
                        }
                        if ($rowShift > 1 && $thisCounter > 1){
							$thisRowsAdded++;
							//echo "Just added row ".$thisRowsAdded."<br>";
							$rowsAdded++;							
						}
						$thisRowsAddedJustNow++;
						$thisCounter++;						
					}
				}
			}
			} 
		
		//echo "ending CompostSiteTier with DataAnalyistRow at: ".$DataAnalyistRow."<br>";
		//echo "DataAnalyistRow= ".$DataAnalyistRow; 
		//echo "RowStarter= ".$rowStarter; //empty
		//echo "rowId= ".$rowId."<br>";
		if ($InvoiceCode == "BMPDevelopment"){
			if ($shiftRows == "Down"){$rowsAdded = $rowsAdded-1;} //reverse the affect
			if ($shiftRows == "Up"){$rowsAdded = $rowsAdded+1;}  //reverse the affect
			$gpList = array("Process Outline"=>"C","Stakeholder Meetings"=>"D","Draft Document"=>"E","Stakeholder Review"=>"F","Document Edits"=>"G","Post to Website"=>"H");
			$campaignList = array("536 Source Reduction BMP"=>171,"536 Furniture Reuse BMP"=>172); //158 & 159
			foreach ($campaignList as $primaryCampaign=>$rowStarter){
					foreach ($gpList as $gpName=>$column){
						$DataAnalyistRow = $rowStarter+($rowsAdded)-($rowsDeleted);			
						//echo "<br>".$primaryCampaign." ".$column.$DataAnalyistRow."=".$bmpStatus[$primaryCampaign][$gpName]."<br>"; //comment this back out Lauren
						$displayValue = $bmpStatus[$primaryCampaign][$gpName];
						$displayValue = ($displayValue == "blank" ? "" : $displayValue);
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue($column . $DataAnalyistRow, $displayValue);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
			}
		}
        try {
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
        } catch (PHPExcel_Exception $e) {
        } //LH added 1/8/2020
		//print_pre($evalTypes["CreatedBI"]["2020-01"]);
		//print_pre($dataAnalysisThisMonth["CreatedBI"]);
	}//end foreach CodeOrderArray
?>