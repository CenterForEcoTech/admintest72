<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/HealthyHomes_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "HealthyHomes".$pathSeparator."HealthyHomes_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$ExcelTabArray = array("Billing Logic","Total Hours","EPA","E4");
//$ExcelTabArray = array("Billing Logic","Total Hours","EPA");
foreach ($ExcelTabArray as $indexId=>$excelTabName){
    try {
        $objPHPExcel->setActiveSheetIndex($indexId);
    } catch (PHPExcel_Exception $e) {
    }
    $excelTabCount = $EmployeesInGroup[$excelTabName];
	if ($excelTabCount > 1){
		$rowShift = $excelTabCount-1;
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(3, $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	if ($excelTabName == "Billing Logic"){
		echo "doing nothing for ".$excelTabName."<br>";
		//do nothing
	}elseif ($excelTabName == "Total Hours"){
		$EPAAllocationRate = '$H$2';
		$E4AllocationRate = '$H$3'; //comment out this line for new template
		$columnNumber = 4;
		foreach ($CodesByGroupName[$excelTabName] as $code){
			if ($InvoiceHourDataByCode[$code]){
				$column = getNameFromNumber($columnNumber);

                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D1", $code);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("D1")->applyFromArray(
                        array('fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('argb' => 'FD9E2F3')
                        )
                        )
                    );
                } catch (PHPExcel_Exception $e) {
                }
                $rowId = 2;
				
				if (count($InvoiceHourData[$code])){
					if (count($InvoiceHourData[$code])){
						foreach ($InvoiceHourData[$code] as $employee_name=>$hours){
							$EmployeeNameParts = explode("_",$employee_name);
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $hours);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=D" . $rowId . "*'Billing logic'!" . $EPAAllocationRate);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=D" . $rowId . "*'Billing logic'!" . $E4AllocationRate);
                            } catch (PHPExcel_Exception $e) {
                            }
                            try {
                                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "=E" . $rowId . "+F" . $rowId);
                            } catch (PHPExcel_Exception $e) {
                            }
                            $rowId++;
						}
					}

				}
				//add SummaryRow
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "=SUM(" . $column . "2:" . $column . ($rowId - 1) . ")");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=SUM(E2:E" . ($rowId - 1) . ")");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=SUM(F2:F" . ($rowId - 1) . ")");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "=SUM(G2:G" . ($rowId - 1) . ")");
                } catch (PHPExcel_Exception $e) {
                }
            }
			
		}
	}//end if Total Hours
	else{
		$code = "814 Healthy Homes";
		//$code = "815 Revitalize CDC";
		$rowId = 2;
		$totalHourColumn = ($excelTabName == "E4" ? "F" : "E");
		if (count($InvoiceHourData[$code])){
			if (count($InvoiceHourData[$code])){
				foreach ($InvoiceHourData[$code] as $employee_name=>$hours){
					$BillingRateType = $excelTabName;
					$EmployeeNameParts = explode("_",$employee_name);
					$categoryRate = $BillingRateByEmployeeJustRate[$BillingRateType][$employee_name];
					if (!$BillingRateByEmployeeJustRate[$BillingRateType][$employee_name]){
						$Adjustments["Alert"]["Staff missing Billing Rate '".$BillingRateType."'"][]=str_replace("_",", ",$employee_name);
					}
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $EmployeesRevenueCodeByName[$employee_name]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $EmployeeNameParts[0]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EmployeeNameParts[1]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "='Total Hours'!" . $totalHourColumn . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $categoryRate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=D" . $rowId . "*E" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $allocatedTotal = $objPHPExcel->getActiveSheet()->getCell("F" . $rowId)->getCalculatedValue();
                    } catch (PHPExcel_Exception $e) {
                    }

                    $RevCodeID = ($BillingRateType == "EPA" ? "MA " : "");
					$BillingRateType = $RevCodeID.$BillingRateType;
					$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+$allocatedTotal;
					$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+$allocatedTotal;
					$rowId++;
				}
			}

		}
		//add SummaryRow
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=SUM(D2:D" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, "=SUM(F2:F" . ($rowId - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
    }
	$lastIndex = $indexId;
}
$InvoiceNumber["814"] = "814-".date("y-m",strtotime($invoiceDate));
$CETInternalFiscalRevenueSheetIndex = ($lastIndex+1);
$CETInternalFiscalRevenueSheetName = "HealthyHomes";	
$pilotProgram = true;
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');



//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
//$triggerResultFiles[] = $CurrentServer.$adminFolder."gbs/".$webpathExtension;
$triggerResultFiles["Healthy Homes"] = $saveLocation;

?>