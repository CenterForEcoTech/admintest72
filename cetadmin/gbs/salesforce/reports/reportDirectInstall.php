<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("Direct Installed Measures")
							 ->setSubject("Direct Installed Measures")
							 ->setDescription("Direct Installed Measures");
//START CurrentMonth
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowId = 1;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Source");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, "Measure Name");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "EFI");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "QTY");
} catch (PHPExcel_Exception $e) {
}
$rowId = 2;
foreach ($itemsByEFI as $source=>$efiInfo){
	foreach ($efiInfo as $EFI=>$qty){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $source);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $standardizedEFI[$EFI]["Description"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $EFI);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $qty);
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
	}
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->applyFromArray(
        array(
            'font' => array('bold' => true),
            'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("C1:C" . $rowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
} catch (PHPExcel_Exception $e) {
}

// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle(date("F Y", strtotime($invoiceDate)) . " Direct Installs");
} catch (PHPExcel_Exception $e) {
}
//END CurrentMonth
							 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$directInstallFile = "DirectInstallMeasures_".date("Y_m_d").".xlsx";


$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "DirectInstalls".$pathSeparator."DirectInstallMeasures_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$directInstallFileLink = "<a id='directInstallFile' href='".$webtrackingLink."'>".$directInstallFile."</a><br>";
?>