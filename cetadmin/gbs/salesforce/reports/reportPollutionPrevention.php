<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/PollutionPrevention_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughsByJobID[$results->JobID][] = $results;
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID"){
			$passThroughHeaders[$key] = 1;
		}
	}
}
//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "PollutionPrevention".$pathSeparator."930_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

	$InvoiceStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$HighlightStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFFFF00'))
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$FooterStyle = array(
		'font'=>array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$SectionStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Fineprint = array(
		'font'  => array('bold'=>true,'size'=>8,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$AmountDueStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
//930Summary	
$InvoiceNumber["PollutionPrevention"] = "930-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$rowsAdded = 0;
	$InvoiceCodeArray = array("930"=>"PollutionPrevention");
	$rowsStart = 3;
	foreach ($InvoiceCodeArray as $InvoiceCode=>$BillingRateType){
		$thisRowId = $rowsStart+$rowsAdded;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . ($thisRowId - 1), date("F", strtotime($invoiceDate)));
        } catch (PHPExcel_Exception $e) {
        }
        $hoursCount = count($InvoiceDataHours[$InvoiceCode]);
		if ($hoursCount > 1){
			$rowShift = ($hoursCount-1);
			//echo $InvoiceCode." will add ".$rowShift." before row ".($thisRowId+1)."<br>";
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
            } catch (PHPExcel_Exception $e) {
            }
        }
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$BillingRateType][$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
				//echo $Employee_Name." will add 1 row before ".($thisRowId+1)."<bR>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":B" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
				$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);
				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
			$thisSum = $theseHours;
//			$objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId+1),1);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":B" . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            if ($thisSum){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "=SUM(B" . $categoryStartRow . ":B" . $thisLastRow . ")");
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "0");
                } catch (PHPExcel_Exception $e) {
                }
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->applyFromArray($HighlightStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $SummaryValues["930"][$CategoryName]["hours"][] = "'930-Summary'!B".$thisRowId;
			$SummaryValues["930"][$CategoryName]["rate"] = $categoryRate;
			$thisRowId++;
			$rowsAdded++;
		}
	}
	
//PollutionPrevention 930	
$rowsAdded = 0;
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F20", "=" . implode('+', $SummaryValues["930"]["Support Staff"]["hours"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G20", $SummaryValues["930"]["Support Staff"]["rate"]);
} catch (PHPExcel_Exception $e) {
}

$InvoiceCode = 930;
	$BillingRateType = "PollutionPrevention";
	$thisRowId = 25;
	if (count($passThroughsByJobID[$InvoiceCode])){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), count($passThroughsByJobID[$InvoiceCode]));
        } catch (PHPExcel_Exception $e) {
        }
        foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
			if (is_array($thisPassThrough)){
				//print_pre($thisPassThrough);
				$memo = ucwords(strtolower($thisPassThrough[0]));
				$sourceName = ucwords(strtolower($thisPassThrough[1]));
				$thisDate = str_replace(".","/",$thisPassThrough[2]);
				$debit = $thisPassThrough[3];
			}else{
				$memo = $thisPassThrough->Memo;
				$sourceName = $thisPassThrough->SourceName;
				$thisDate = MySQLDate($thisPassThrough->Date);
				$debit = $thisPassThrough->Debit;
			}

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $memo);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $sourceName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $thisDate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $debit);
            } catch (PHPExcel_Exception $e) {
            }

            $revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$debit,2);
			$revenueCodesByEmployee[$BillingRateType]["40952"][$sourceName] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"][$sourceName],$debit,2);
			
			
//				$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":H".$thisRowId)->applyFromArray($SummaryStyle);
            try {
                $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
            } catch (PHPExcel_Exception $e) {
            }
            $thisRowId++;
			$rowsAdded++;
			$incentiveSubtotalEnd = $thisRowId;
		}
	}else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "None");
        } catch (PHPExcel_Exception $e) {
        }
//		$objPHPExcel->getActiveSheet()->setCellValue("H".$thisRowId,0);
//		$objPHPExcel->getActiveSheet()->getStyle("H".$thisRowId.":H".$thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
		$incentiveSubtotalEnd = $thisRowId;
	}

//PO#: 175487 Valid through end of March 2017
$poNumber = "176441"; //April hours not billed, valid starting May 2017
$poNumber = "178726"; //Active Sept 2017
$poNumber = "179170"; //Active Oct1-Dec31 2017
$poNumber = "180306"; //Active Jan1 2018
$poNumber = "181324"; //Active Apr1-July 2018
$poNumber = "185545"; //Active Feb 25 - September 30 2019
if (date("F") == "October"){echo "<H4>PO HAS EXPIRED</H4>";}
$poRow = 31+$rowsAdded;

try {
    $objPHPExcel->getActiveSheet()->setCellValue("H6", date("m/15/Y", strtotime($invoiceDate . " +1 Month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["PollutionPrevention"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $poRow, "PO#: " . $poNumber);
} catch (PHPExcel_Exception $e) {
}


//Report
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G6", date("m/15/Y", strtotime($invoiceDate . " +1 month")));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A15", "='930'!A" . $poRow);
} catch (PHPExcel_Exception $e) {
}
$rowId = 19;
$bulletsCount = count($p2iBullets);
$bulletValue = "•";
if ($bulletsCount){
	if ($bulletsCount > 1){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, ($bulletsCount - 1));
        } catch (PHPExcel_Exception $e) {
        }
    }
	foreach ($p2iBullets as $bullet){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $bulletValue);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $bullet);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
        } catch (PHPExcel_Exception $e) {
        }
        $width = mb_strwidth ($value); //Return the width of the string

		$rowHeight = 20;
		if ($width > 80){
			$rowHeight = ($rowHeight*ceil($width/80));
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		
		$rowId++;
	}
	
	//now add events
	if ($eventCounter){
		$rowId++;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Events:");
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
		foreach ($P2IEvents as $startStr=>$typeData){
			ksort($typeData);
			foreach ($typeData as $type=>$events){
				foreach ($events as $eventData){
					//print_pre($eventData);
					$eventDate = $eventData["eventDate"];
					$subject = $eventData["event"];
					$description = $eventData["description"];
					$attendance = $eventData["attendance"];
					$eventDisplay = "[".$subject."] ".$description.($attendance > 0 ? " (".$attendance." in attendance)" : "");

                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $eventDate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $eventDisplay);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
                    } catch (PHPExcel_Exception $e) {
                    }

                    try {
                        $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    $width = mb_strwidth ($value); //Return the width of the string

					$rowHeight = 20;
					if ($width > 80){
						$rowHeight = ($rowHeight*ceil($width/80));
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					$rowId++;
					
				}
			}
		}
		
	}
}	

	

$CETInternalFiscalRevenueSheetIndex = 3;
$CETInternalFiscalRevenueSheetName = "PollutionPrevention";	
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
//$triggerResultFiles[] = $CurrentServer.$adminFolder."gbs/".$webpathExtension;
$triggerResultFiles["PollutionPrevention"] = $saveLocation;
?>