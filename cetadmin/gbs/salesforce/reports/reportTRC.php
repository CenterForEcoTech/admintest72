<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/TRC_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "TRC".$pathSeparator."TRC_".date("Y-m", strtotime($invoiceEndDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Total = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF4B084')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
//TRC 515V Invoice	
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$corridorRowStart = 19;
$corridorSummaryStart = 22;
$corridorCount = count($GeoBillingCounts);
$SummaryRow  = 20;
//print_pre($GeoBillingCounts);
if ($corridorCount > 2){
	$rowShift = $corridorCount-2;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($SummaryRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
    $addedRowForRevenueCode = $rowShift;
}else{
	$addedRowForRevenueCode = 0;
}

foreach ($GeoBillingCounts as $corridor=>$count){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $corridorRowStart, "Completion of Corridor " . $corridor . " Site Visits");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $corridorRowStart, $count);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $corridorRowStart, $geoBillingRate[$corridor]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $corridorRowStart, '=IF(G' . $corridorRowStart . '>0,F' . $corridorRowStart . '*G' . $corridorRowStart . ',"")');
    } catch (PHPExcel_Exception $e) {
    }
    $corridorRowStart++;
}
$corridorRowStart++;
//echo "using H".($corridorSummaryStart+$addedRowForRevenueCode);
try {
    $revenueCodesByRatioTotalAmount["TRC"] = $objPHPExcel->getActiveSheet()->getCell("H" . ($corridorSummaryStart + $addedRowForRevenueCode))->getCalculatedValue();
} catch (PHPExcel_Exception $e) {
}
$revenueCodesByRatioTotalCell["TRC"] = "'515V'!H".($corridorSummaryStart+$addedRowForRevenueCode);

$InvoiceNumber["TRC"] = "515V-".date("y-m",strtotime($invoiceEndDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["TRC"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceStartDate)) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceEndDate)))));
} catch (PHPExcel_Exception $e) {
}


//515V Summary	
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
//add month information
$thisRowId = 1;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Account");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "Address");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "Corridor");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, "Date");
} catch (PHPExcel_Exception $e) {
}
//$objPHPExcel->getActiveSheet()->setCellValue("E".$thisRowId,"Bin Left"); //removed 2/1/2017 in favor of FedExTracking
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "FedExTracking");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "Location:ID");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "TRC Status");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray($BoldStyle);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
foreach ($TRCresultsByCorridor as $corridor=>$corridorDetails){
	foreach ($corridorDetails as $details){
		//commented out to change to trcStatusDetail 2/26/2019
		//$statusDetail = 0;
		$zipCode = $details["zipCode"];
		$city = $details["city"];
		$thisCorridor = $corridor;
		//only show statusdetail after 'mailing intro letter' - commented out since the field isn't pulled anymore 2/26/2019
		//$actualStatus = str_replace("<br>","",$details["statusDetail"]);
		//$editedStatus = str_replace("mailed intro","mailing intro", strtolower($actualStatus));
		//$statusDetailLocation = (stripos(strtolower($editedStatus),"mailing intro letter") ? stripos(strtolower($editedStatus),"mailing intro letter")+20: 0);
		//$status = ltrim(substr($actualStatus,($statusDetailLocation)),".");
		$trcStatusDetail = $details["trcStatusDetail"];
		echo $trcStatusDetail;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $details["account"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $thisCorridor);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $details["completedDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $details["fedExTracking"]);
        } catch (PHPExcel_Exception $e) {
        }
        /* removed 2/1/2017 in favor of FedExTracking
        $objPHPExcel->getActiveSheet()->setCellValue("E".$thisRowId,$details["boxLeft"]);
        if (strtolower($details["boxLeft"]) == "yes"){
            $objPHPExcel->getActiveSheet()->getStyle("E".$thisRowId)->applyFromArray($YesStyle);
        }else{
            $objPHPExcel->getActiveSheet()->getStyle("E".$thisRowId)->applyFromArray($NoStyle);
        }
        */
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $details["TRCLocationID"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $trcStatusDetail);
        } catch (PHPExcel_Exception $e) {
        }
        //$objPHPExcel->getActiveSheet()->getColumnDimension("G1:G".$thisRowId)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getStyle("G1:G10")->getAlignment()->setWrapText(true);
		foreach(range('B','G') as $columnID) {
            try {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$lastRow = $thisRowId;
		$thisRowId++;
	}
}
$BordersStyle = array(
	'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);
if ($lastRow){
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A1:G" . $lastRow)->applyFromArray($BordersStyle);
    } catch (PHPExcel_Exception $e) {
    }
}
//show summary row


/* removed 2/1/2017 in favor of FedExTracking
$objPHPExcel->getActiveSheet()->setCellValue('A'.$thisRowId,'Total With Bins');
$thisRowId++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.($thisRowId),'Total Without Bins');
$thisRowId++;
*/
try {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . ($thisRowId), 'Total Business Visits Completed');
} catch (PHPExcel_Exception $e) {
}


/* removed 2/1/2017 in favor of FedExTracking
$objPHPExcel->getActiveSheet()->setCellValue('E'.$thisRowId,'=COUNTIF(E2:E'.$lastRow.',"Yes")');
$thisRowId++;
$objPHPExcel->getActiveSheet()->setCellValue('E'.$thisRowId,'=COUNTIF(E2:E'.$lastRow.',"No")');
$thisRowId++;
$objPHPExcel->getActiveSheet()->setCellValue("E".$thisRowId,"=SUM(E".($thisRowId-2).":E".($thisRowId-1).")");
*/
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "=" . ($lastRow - 1));
} catch (PHPExcel_Exception $e) {
}

/* removed 2/1/2017 in favor of FedExTracking
$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":F".$thisRowId)->applyFromArray($YesTotal);
$thisRowId++;
$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":F".$thisRowId)->applyFromArray($NoTotal);
$thisRowId++;
*/
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray($Total);
} catch (PHPExcel_Exception $e) {
}


//515I Quarterly Report	
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
$GPTypeCodes = array("515O"=>19,"515H"=>27,"515P"=>35);
foreach ($GPTypeCodes as $GPType=>$startRow){
	$rowId = ($startRow+$rowsShifted-1);
	$rowCount = count($bulletsByGPType[$GPType]);
	if ($rowCount > 1){
		$rowShift = ($rowCount-1);
		$rowsShifted = $rowsShifted+$rowShift;
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($rowId + 1), $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	foreach ($bulletsByGPType[$GPType] as $detail){
		//print_pre($detail);
		$cleanName = str_replace("Illinois-","",$detail["name"]);
		$cleanName = str_replace("TRC","",$cleanName);
		if (substr("Illinois ",0,9)){
			$cleanName = str_replace("Illinois ","",$cleanName);
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $cleanName);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $detail["deliverable"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
        } catch (PHPExcel_Exception $e) {
        }
        $width = mb_strwidth ($value); //Return the width of the string
				if ($InvoiceCode == "536"){
					//add border between campaigns
					if ($oldCampaignName && $campaignName != $oldCampaignName){
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":H" . $rowId)->applyFromArray($TopBorder);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					$oldCampaignName = $campaignName;
				}

				$rowHeight = 20;
				if ($width > 80){
					$rowHeight = ($rowHeight*ceil($width/80));
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
		
		
		$rowId++;
	}
	
}
$CETInternalFiscalRevenueSheetIndex = 4;
$CETInternalFiscalRevenueSheetName = "TRC";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";

?>