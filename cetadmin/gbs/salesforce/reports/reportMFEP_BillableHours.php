<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("MFEP Billable Hours Allocation")
							 ->setSubject("MFEP Billable Hours Allocation")
							 ->setDescription("MFEP Billable Hours Allocation");
//START CurrentMonth
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
$funderColumnCount =1;
foreach ($funderColumn as $funder=>$codeColumn){
	foreach ($codeColumn as $code=>$column){
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(9);
        } catch (PHPExcel_Exception $e) {
        }
        $colorName = (strpos($code,"-R")?"green":"lightblue");
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . "2")->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF' . $hexColors[$colorName])
                ),
                    'font' => array('size' => 11)
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }
	if ($funderColumnCount <2){
		foreach ($codeColumn as $code=>$column){
			$colorName = (strpos($code,"-R")?"green":"lightblue");
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . ($startAllCombinedHoursRow - 1))->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'FF' . $hexColors[$colorName])
                    ),
                        'font' => array('size' => 11)
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
        }
	}
	if ($funderColumnCount ==2){
        try {
            $objPHPExcel->getActiveSheet()->getStyle($codeColumn["Start"] . $startAllCombinedHoursRow . ":" . $codeColumn["Start"] . $combinedLastRow)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF' . $hexColors["silver"])
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle($codeColumn["Start"] . ($combinedLastRow + 1))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF' . $hexColors["salmon"])
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($codeColumn["Start"] . ($combinedLastRow + 1), "=SUM(" . $codeColumn["Start"] . $startAllCombinedHoursRow . ":" . $codeColumn["Start"] . $combinedLastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }

    }
	$funderColumnCount++;
}

foreach ($reportRows as $rowId=>$cellInfo){
	foreach ($cellInfo as $cell=>$dataDisplay){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
}

//style sheet
try {
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A3:A" . $LastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["olive"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $startAllCombinedHoursRow . ":A" . $combinedLastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["olive"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}


try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(20);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($LastRow + 1) . ":" . $funderColumn[$funder]["AllHoursTotal"] . ($LastRow + 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["summaryorange"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($combinedLastRow + 1) . ":" . $funderColumn[$first_funder]["Total"] . ($combinedLastRow + 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["summaryorange"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}

$funderTotalCount = 1;
	foreach ($funderTotals as $column){
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . "3:" . $column . $LastRow)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["peach"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . ($LastRow + 1))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["salmon"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        if ($funderTotalCount <2){
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . $startAllCombinedHoursRow . ":" . $column . $combinedLastRow)->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => $hexColors["peach"])
                    )

                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . ($combinedLastRow + 1))->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => $hexColors["salmon"])
                    )

                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
        }
		$funderTotalCount++;
	}
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle("totals check");
} catch (PHPExcel_Exception $e) {
}
//END CurrentMonth

$funderCount = 1;
foreach ($funderColumn as $funder=>$codeColumn){
	//create a new worksheet
    try {
        $objPHPExcel->createSheet();
    } catch (PHPExcel_Exception $e) {
    }
    // Add header data

    try {
        $objPHPExcel->setActiveSheetIndex($funderCount);
    } catch (PHPExcel_Exception $e) {
    }
    $funderTitle = $funder;
    try {
        $objPHPExcel->getActiveSheet()->setTitle($funderTitle . " Hours");
    } catch (PHPExcel_Exception $e) {
    }
    // echo $objPHPExcel->getActiveSheet()->getTitle()."<br>";
	$TOTALRowFound = false;
	foreach ($funderRows[$funder] as $rowId=>$cellInfo){
		foreach ($cellInfo as $cell=>$dataDisplay){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
            } catch (PHPExcel_Exception $e) {
            }
        }
		// print_pre($cellInfo);
        try {
            $employee_name = $objPHPExcel->getActiveSheet()->getCell("A" . $rowId)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        if ($employee_name == "TOTAL"){$TOTALRowFound = true;}
		if (strpos($employee_name,",") && $funder != "MDAR" && $TOTALRowFound){
			$employee_name = str_replace(", ","_",$employee_name);
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("'" . $funderTitle . " Hours'!D" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            // echo $funder." ".$employee_name ." ="."'".$funderTitle." Hours'!D".$rowId." sheet: ".$funderTitle." Hours"."<br>";
			$SF270Hours["MA ".$funder][$employee_name] = $thisHour;
		}
		if ($employee_name == "TOTAL PERSONNEL") {
            try {
                $thisHour = $objPHPExcel->getActiveSheet()->getCell("'" . $funderTitle . " Hours'!C" . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $SF270Data["MA ".$funder]["Personell"] = $thisHour;
		}
		
	}
	
	//style the sheet
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(18);
    } catch (PHPExcel_Exception $e) {
    }
    foreach ($codeColumn as $code=>$column){
//		$objPHPExcel->getActiveSheet()->getColumnDimension($thisFunderColumn[$code])->setWidth(9);	
		if ($funder == "MDAR"){$colorName = (strpos($code,"-R")?"green":"lightblue");}else{$colorName="lightblue";}
        try {
            $objPHPExcel->getActiveSheet()->getStyle($thisFunderColumn[$code] . "2")->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF' . $hexColors[$colorName])
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }

    }


    try {
        $objPHPExcel->getActiveSheet()->getStyle("A3:A" . $LastFunderRow)->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => $hexColors["olive"])
            )

            )
        );
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . ($LastFunderRow + 1) . ":" . $thisFunderColumn["Total"] . ($LastFunderRow + 1))->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => $hexColors["summaryorange"])
            )

            )
        );
    } catch (PHPExcel_Exception $e) {
    }

    if ($funder=="MDAR"){
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $StartMDARCombinedRows . ":A" . $LastMDARCombinedRows)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["olive"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . ($LastMDARCombinedRows + 1) . ":" . $thisFunderColumn["Total"] . ($LastMDARCombinedRows + 1))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["summaryorange"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($thisFunderColumn["Total"] . "3:" . $thisFunderColumn["Total"] . $LastFunderRow)->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => $hexColors["peach"])
            )

            )
        );
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($thisFunderColumn["Total"] . ($LastFunderRow + 1))->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => $hexColors["salmon"])
            )

            )
        );
    } catch (PHPExcel_Exception $e) {
    }
    if ($funder == "RBDG"){
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(14);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(18);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $StartRBDGCombinedRows . ":C" . $LastRBDGCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("F" . $StartRBDGCombinedRows . ":F" . $LastRBDGCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . $StartRBDGCombinedRows . ":I" . $LastRBDGCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("C" . $TotalRBDGCombinedRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . $TotalRBDGCombinedRow . ":I" . $TotalRBDGCombinedRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("I" . $StartRBDGCombinedRows . ":I" . $LastRBDGCombinedRows)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF00')
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }

    }
	if ($funder == "SECD"){
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(14);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(12);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(18);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $StartSECDCombinedRows . ":C" . $LastSECDCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("F" . $StartSECDCombinedRows . ":F" . $LastSECDCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . $StartSECDCombinedRows . ":I" . $LastSECDCombinedRows)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("C" . $TotalSECDCombinedRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . $TotalSECDCombinedRow . ":I" . $TotalSECDCombinedRow)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("I" . $StartSECDCombinedRows . ":I" . $LastSECDCombinedRows)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF00')
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }

    }
	// Rename worksheet
//	$funderTitle = substr($funder,0,20);
	
	$funderCount++;

}

$BoldUnderlineStyle = array(
	'font'=>array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

/*
//add direct bill sheet
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex($funderCount);
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(10);	
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(20);	
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(20);	
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(12);	
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(12);	
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(12);	

$objPHPExcel->getActiveSheet()->setCellValue("A1","Category");
$objPHPExcel->getActiveSheet()->setCellValue("B1","Employee");
$objPHPExcel->getActiveSheet()->setCellValue("C1","Allocated Hours");
$objPHPExcel->getActiveSheet()->setCellValue("D1","Direct Billed Hours");
$objPHPExcel->getActiveSheet()->setCellValue("E1","Total Hours");
$objPHPExcel->getActiveSheet()->setCellValue("F1","Billing Rate");
$objPHPExcel->getActiveSheet()->setCellValue("G1","Total Cost");

$rowId = 2;
$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,"NRCS");
$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,$BillableHours["SECD"]["560A"]);
$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,$totalHoursByCode["562"]);
$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,"=C".$rowId."+D".$rowId);
$NRCSTotalRowId = $rowId;
$objPHPExcel->getActiveSheet()->getStyle("A1:G2")->applyFromArray($BoldUnderlineStyle);

$rowId++;
foreach ($totalHoursByCodeByEmployee["562"] as $employeeName=>$hours){
	if ($hours > 0){
		$objPHPExcel->getActiveSheet()->setCellValue("B".$rowId,str_replace("_",", ",$employeeName));
//		$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,$BillableHoursDirectBilling["SECD"]["560A"][$employeeName]);
		$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,$totalHoursByCodeByEmployee["562"][$employeeName]);
//		$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,"=C".$rowId."+D".$rowId);
		$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$BillingRateByEmployeeJustRate[$employeeName]);
		$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"=D".$rowId."*F".$rowId);
		$rowId++;
	}
}
	$objPHPExcel->getActiveSheet()->setCellValue("G".$NRCSTotalRowId,"=sum(G".($NRCSTotalRowId+1).":G".($rowId-1).")");
$rowId++;

$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,"RBEG");
$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,$BillableHours["RBEG"]["560A"]);
$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,$totalHoursByCode["561"]);
$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,"=C".$rowId."+D".$rowId);
$RBEGTotalRowId = $rowId;
$objPHPExcel->getActiveSheet()->getStyle("A".$rowId.":G".$rowId)->applyFromArray($BoldUnderlineStyle);
$rowId++;
foreach ($totalHoursByCodeByEmployee["561"] as $employeeName=>$hours){
	if ($hours > 0 ){
		$objPHPExcel->getActiveSheet()->setCellValue("B".$rowId,str_replace("_",", ",$employeeName));
//		$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,$BillableHoursDirectBilling["RBEG"]["560A"][$employeeName]);
		$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,$totalHoursByCodeByEmployee["561"][$employeeName]);
//		$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,"=C".$rowId."+D".$rowId);
		$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$BillingRateByEmployeeJustRate[$employeeName]);
		$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"=D".$rowId."*F".$rowId);
		$rowId++;
	}
}
$objPHPExcel->getActiveSheet()->getStyle("F1:G".$rowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
$objPHPExcel->getActiveSheet()->setCellValue("G".$RBEGTotalRowId,"=sum(G".($RBEGTotalRowId+1).":G".($rowId-1).")");

$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,"=E".$NRCSTotalRowId."+E".$RBEGTotalRowId);
$objPHPExcel->getActiveSheet()->getStyle("E".$rowId)->applyFromArray($BoldUnderlineStyle);




// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle("Direct Billed Hours");
*/
							 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$BillableFile = "MFEPBillableHoursFile_".date("Y_m_d").".xlsx";

$pathExtension = "invoicing".$pathSeparator."MFEP".$pathSeparator.$BillableFile;
$webpathExtension = "invoicing".$webpathSeparator."MFEP".$webpathSeparator.$BillableFile;
$saveLocation = $path.$pathExtension;


//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
$billableFilewebtrackingLink = $webtrackingLink;
//echo $webtrackingLink."<Br>";
$BillableFileLink = "<a id='allocationFile' href='".$webtrackingLink."'>".$BillableFile."</a><br>";
?>