<?php
$generalLedgerCode = ($GLCode ? $GLCode : " – 1"); //default to commercial unless otherwise stated on the invoicingTools page
// $pilotProgram used to alter revenueCode,  This is true / false set on the salesforce/report page just before including this page.

//print_pre($revenueCodesByEmployee);
//print_pre($revenueCodesByRatioTotalAmount);
foreach ($revenueCodesByEmployee as $BillingRateType=>$revenueCodeInfo){
	if (!in_array($BillingRateType,$previouslyRan)){//only run a BillingRateType once if combined with other reports
		
		if ($revenueCodesByRatioTotalAmount[$BillingRateType]){
			foreach ($revenueCodeInfo as $revenueCode=>$employeeNameInfo){
				foreach ($employeeNameInfo as $employeeName=>$hours){
					$ratio = bcdiv($hours,$revenueCodeTotalHours[$BillingRateType],7);
					$dollarAmount = bcmul($revenueCodesByRatioTotalAmount[$BillingRateType],$ratio,7);
		//			echo $revenueCode." ".$employeeName." ".$hours." ".$ratio." ".$dollarAmount." ".$revenueCodesByRatioTotalAmount[$BillingRateType]."<br>";
					//$revenueCodesRatio[$BillingRateType][$revenueCode] = bcadd($revenueCodesRatio[$BillingRateType][$revenueCode],$dollarAmount,7);
					//$revenueCodesByEmployeeRatio[$BillingRateType][$revenueCode][$employeeName] = round($dollarAmount,7);
					if ($pilotProgram){
						switch ($revenueCode){
							case "40202":
								$revenueCode = "40920";
								break;
							case "40205":
								$revenueCode = "40922";
								break;
						}
					}

					$revenueCodesRatio[$BillingRateType][$revenueCode] = bcadd($revenueCodesRatio[$BillingRateType][$revenueCode],$ratio,7);
					$revenueCodesByEmployeeRatio[$BillingRateType][$revenueCode][$employeeName] = $ratio;

					
				}
			}
			$revenueCodes[$BillingRateType] = $revenueCodesRatio[$BillingRateType];
			$revenueCodesByEmployee[$BillingRateType] = $revenueCodesByEmployeeRatio[$BillingRateType];
		}
	}
}
//print_pre($revenueCodes);
//print_pre($revenueCodesByEmployee);

//get revenue code data
if (!$hrEmployeeProvider){
	include_once($dbProviderFolder."HREmployeeProvider.php");
	$hrEmployeeProvider = new HREmployeeProvider($dataConn);
}
$revenueCodeResult = $hrEmployeeProvider->getRevenueCodes();
foreach ($revenueCodeResult as $revenueCode){
	$revenueCodesByCode[$revenueCode["HRRevenueCode_Code"]] = $revenueCode["HRRevenueCode_Name"];
}
	$JustBoldStyle= array('font'=>array('bold'=>true));
	
	$BoldUnderlineStyle = array(
		'font'=>array('bold'=>true),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$BreakdownStyle = array(
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2'))
	);
	$BreakdownTotalStyle = array(
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$HighlightStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFFF00'))
	);

try {
    $objPHPExcel->createSheet($CETInternalFiscalRevenueSheetIndex);
} catch (PHPExcel_Exception $e) {
} //Setting index when creating
try {
    $objPHPExcel->setActiveSheetIndex($CETInternalFiscalRevenueSheetIndex);
} catch (PHPExcel_Exception $e) {
}
$thisRowId = 1;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Invoice Date: " . date("Y-m", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
$thisRowId = 2;
try {
    $objPHPExcel->getActiveSheet()->setTitle(($CETInternalFiscalRevenueSheetName ? $CETInternalFiscalRevenueSheetName : "Revenue Codes"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Source");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "Revenue Name");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "Revenue Code");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, "Amount");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "Total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(18);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(30);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(15);
} catch (PHPExcel_Exception $e) {
}

if (count($revenueCodesByRatioTotalCell)){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "Actual Invoice Amount for Detecting Rounding Issues");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(25);
    } catch (PHPExcel_Exception $e) {
    }
}

$thisRowId++;
//ksort($revenueCodes);
foreach ($revenueCodes as $BillingRateType=>$revenueCodeInfo){
	if (!in_array($BillingRateType,$previouslyRan)){//only run a BillingRateType once if combined with other reports
		$BillingRateTypeRowStart = $thisRowId;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $BillingRateType);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId)->applyFromArray($JustBoldStyle);
        } catch (PHPExcel_Exception $e) {
        }
        ksort($revenueCodeInfo);
		foreach ($revenueCodeInfo as $revenueCode=>$total){
			if ($pilotProgram){
				switch ($revenueCode){
					case "40202":
						$revenueCode = "40920";
						break;
					case "40205":
						$revenueCode = "40922";
						break;
				}
			}
			
			$revenueCodeDisplay = $revenueCode.$generalLedgerCode;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $revenueCodesByCode[$revenueCode]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $revenueCodeDisplay);
            } catch (PHPExcel_Exception $e) {
            }
            if ($revenueCodesByRatioTotalCell[$BillingRateType]){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, "=round((" . $total . "*" . $revenueCodesByRatioTotalCell[$BillingRateType] . "),2)");
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, round($total, 2));
                } catch (PHPExcel_Exception $e) {
                }
            }
				
			$BillingRateTypeRowEnd = $thisRowId;
			$thisRowId++;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $BillingRateType . " Total");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "Invoice Number: " . $InvoiceNumber[$BillingRateType]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "=SUM(D" . $BillingRateTypeRowStart . ":D" . $BillingRateTypeRowEnd . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $invoicedAmountInvoice = ($InvoiceNumber[$BillingRateType] ? : $BillingRateType);
        try {
            $invoicedAmount[$invoicedAmountInvoice] = $objPHPExcel->getActiveSheet()->getCell("E" . $thisRowId)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        if ($revenueCodesByRatioTotalCell[$BillingRateType]){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=" . $revenueCodesByRatioTotalCell[$BillingRateType]);
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId . ":E" . $thisRowId)->applyFromArray($BreakdownTotalStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
		$thisRowId++;
	}//end if previously ran
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D2:F" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
$thisRowId = $thisRowId+3;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "Revenue Code Breakdown");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":E" . $thisRowId)->applyFromArray($BreakdownStyle);
} catch (PHPExcel_Exception $e) {
}
$BreakdownRowStart = $thisRowId;
$BreakdownRowEnd = $thisRowId;
$thisRowId++;
//print_pre($revenueCodesByEmployee);
foreach ($revenueCodesByEmployee as $BillingRateType=>$revenueCodeInfo){
	if (!in_array($BillingRateType,$previouslyRan)){//only run a BillingRateType once if combined with other reports
		$BillingRateTypeRowStart = $thisRowId;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $BillingRateType);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId)->applyFromArray($BoldUnderlineStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
		ksort($revenueCodeInfo);
		foreach ($revenueCodeInfo as $revenueCode=>$employeeNameInfo){
			if ($pilotProgram){
				switch ($revenueCode){
					case "40202":
						$revenueCode = "40920";
						break;
					case "40205":
						$revenueCode = "40922";
						break;
				}
			}
			
			$revenueCodeDisplay = $revenueCode.$generalLedgerCode;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $revenueCodeDisplay . " - " . $revenueCodesByCode[$revenueCode]);
            } catch (PHPExcel_Exception $e) {
            }
            if ($revenueCode == "40222"){ //40222 is ISM Materials
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "Qty");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } catch (PHPExcel_Exception $e) {
                }
            }
	//		$objPHPExcel->getActiveSheet()->setCellValue("C".$thisRowId,$revenueCode);
			$thisRowId++;
			$revenueCodeRowStart = $thisRowId;
			foreach ($employeeNameInfo as $employeeName=>$total){ 
				if (strpos($employeeName,"|")){
					$nameParts = explode("|",$employeeName);
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $nameParts[0]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($revenueCode == "40222"){ //40222 is ISM Materials
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $nameParts[1]);
                        } catch (PHPExcel_Exception $e) {
                        }
                        $qtyDisplay = ($nameParts[2] ? : $combinedQuantity[$employeeName]);
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $qtyDisplay);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }else{
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $nameParts[1]);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
				}else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $employeeName));
                    } catch (PHPExcel_Exception $e) {
                    }
                }
	//			$objPHPExcel->getActiveSheet()->setCellValue("C".$thisRowId,$revenueCode);
				if ($revenueCodesByRatioTotalCell[$BillingRateType]){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, "=round((" . $total . "*" . $revenueCodesByRatioTotalCell[$BillingRateType] . "),2)");
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $total);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				$revenueCodeRowEnd = $thisRowId;
				$BillingRateTypeRowEnd = $thisRowId;
				$thisRowId++;
			}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $revenueCodeDisplay . " - " . $revenueCodesByCode[$revenueCode] . " Total");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "=SUM(D" . $revenueCodeRowStart . ":D" . $revenueCodeRowEnd . ")");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId . ":E" . $thisRowId)->applyFromArray($BreakdownTotalStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $BreakdownRowEnd = $thisRowId;
			$thisRowId++;
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":E" . $thisRowId)->applyFromArray($BreakdownTotalStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $thisRowId++;
			if ($revenueCode == "40222"){ //40222 is ISM Materials
				$ISMMaterialRows[] = array("start"=>$revenueCodeRowStart,"end"=>$revenueCodeRowEnd);
			}
		}
	//	$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,$BillingRateType." Total");
	//	$objPHPExcel->getActiveSheet()->setCellValue("D".$thisRowId,"=SUM(C".$BillingRateTypeRowStart.":C".$BillingRateTypeRowEnd.")");
		$thisRowId++;
		$thisRowId++;
		$previouslyRan[]=$BillingRateType;
	}//end if previously ran
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D2:E" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $BreakdownRowStart . ":E" . $BreakdownRowEnd)->applyFromArray($BreakdownStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:A" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
} catch (PHPExcel_Exception $e) {
}
if (count($ISMMaterialRows)){
	foreach ($ISMMaterialRows as $ISMSection){
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $ISMSection["start"] . ":C" . $ISMSection["end"])->applyFromArray($HighlightStyle);
        } catch (PHPExcel_Exception $e) {
        }
    }
}	
//add invoicing information to invoice amount history
include_once($dbProviderFolder."InvoicingProvider.php");
$invoicingProvider = new InvoicingProvider($dataConn);

foreach ($invoicedAmount as $invoiceNumber=>$amount){
	$invoiceCodeParts = explode("-",$invoiceNumber);
	$invoiceCode = ($invoiceCodeParts[0] ? : $invoiceNumber);
	$recordToInsert = new stdClass();
	$recordToInsert->invoiceCode = $invoiceCodeParts[0];
	$recordToInsert->invoiceNumber = $invoiceNumber;
	$recordToInsert->invoiceAmount = $amount;
	$recordToInsert->invoiceDate = date("Y-m-d",strtotime($invoiceDate));
	$recordToInsert->sourcePage = $_SERVER['REQUEST_URI'];
//	print_pre($recordToInsert);
	$insertResults = $invoicingProvider->insertInvoicingAmount($recordToInsert,getAdminId());
}
//	print_pre($revenueCodes);
//	print_pre($revenueCodesByEmployee);
?>