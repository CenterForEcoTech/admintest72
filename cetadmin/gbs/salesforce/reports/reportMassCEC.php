<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = $inputFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/" : "")."importfiles/MassCECReport_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//print_pre($passThroughsByJobID);
$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "MassCEC".$pathSeparator."MassCEC_Report_".date("Y-m", strtotime($invoiceDate))."_raw.xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."gbs/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

//Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/d/Y", strtotime($invoiceArray[$invoiceNumber]["startDate"])) . "-" . date("m/d/Y", strtotime(date("m/t/Y", strtotime($invoiceArray[$invoiceNumber]["endDate"])))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", $invoiceNumber);
} catch (PHPExcel_Exception $e) {
}
$rowId = 19;
$bulletsCount = $massCECBulletCount;
if ($bulletsCount){
	if ($bulletsCount > 1){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowId, ($bulletsCount - 1));
        } catch (PHPExcel_Exception $e) {
        }
    }
	foreach ($massCECBullets as $taskType=>$statuses){
		ksort($statuses);
		foreach ($statuses as $status=>$bullets){
			$task = $taskType.($status == 'In Progress' ? '*' : '');
			foreach ($bullets as $bullet){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $taskType . ($status == 'In Progress' ? '*' : ''));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $bullet["subject"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $bullet["detail"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->mergeCells("C" . $rowId . ":H" . $rowId);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":H" . $rowId)->getAlignment()->setWrapText(true);
                } catch (PHPExcel_Exception $e) {
                }

                try {
                    $value = $objPHPExcel->getActiveSheet()->getCell("C" . $rowId)->getValue();
                } catch (PHPExcel_Exception $e) {
                }
                $width = mb_strwidth ($value); //Return the width of the string

				$rowHeight = 20;
				if ($width > 80){
					$rowHeight = ($rowHeight*ceil($width/80));
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				
				
				$rowId++;
			}
		}
	}
}	

//invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $invoiceNumber);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F j", strtotime($invoiceArray[$invoiceNumber]["startDate"])) . " to " . date("F j, Y", strtotime(date("m/t/Y", strtotime($invoiceArray[$invoiceNumber]["endDate"])))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A17", $invoiceArray[$invoiceNumber]["title"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H17", $invoiceArray[$invoiceNumber]["invoiceAmount"]);
} catch (PHPExcel_Exception $e) {
}


$rowId = 19;
foreach ($invoiceArray[$invoiceNumber]["deliverables"] as $bullet){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $bullet);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>