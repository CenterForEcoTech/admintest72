<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxReportName" ).combobox({
				select: function(event, ui){
					<?php 
						if ($reportNameComboBoxFunction){
							echo $reportNameComboBoxFunction;
						}else{
							echo "$('#reportName').val($(ui.item).val());";
							// echo "window.location = '?nav=manage-codes&CodeID='+$(ui.item).val();";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxReportName parameters " id="SelectedReportName" data-hiddenid="reportName">
				<option value="">Select one...</option>
				<?php 
					foreach ($ReportNames as $ReportName=>$count){
						echo "<option value=\"".$ReportName."\"".($SelectedReportName==$ReportName ? " selected" : "").">".$ReportName."</option>";
					}
				?>
			  </select>
			</div>
		</div>