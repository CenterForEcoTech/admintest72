<pre>
<?php
$xml_string = '
 <demographic invoice_number="">
 <customer>
<project_number>100A</project_number>
  <contract_number />
<electric_account_number>54053045050</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>417 North St</project_name>
  <business_name />
<last_name>Whaling</last_name>
<first_name>George</first_name>
<service_address_number>417</service_address_number>
<service_address_street>North St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>14152</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>101A</project_number>
  <contract_number />
<electric_account_number>54291091031</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>443 North St</project_name>
  <business_name />
<last_name>Whaling</last_name>
<first_name>George</first_name>
<service_address_number>443</service_address_number>
<service_address_street>North</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>14956</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>102A</project_number>
  <contract_number />
<electric_account_number>54025415084</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>495 Main St</project_name>
  <business_name />
<last_name>Dunphy</last_name>
<first_name>John</first_name>
<service_address_number>495</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Ashfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01330</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5600</conditioned_sq_footage>
<primary_heating_fuel>Propane</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>103A</project_number>
  <contract_number />
<electric_account_number>54043202092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Sumner Place</project_name>
  <business_name />
<last_name>Place</last_name>
<first_name>Sumner</first_name>
<service_address_number>34</service_address_number>
<service_address_street>Sumner Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7700</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>103B</project_number>
  <contract_number />
<electric_account_number>54043202092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Sumner Place</project_name>
  <business_name />
<last_name>Place</last_name>
<first_name>Sumner</first_name>
<service_address_number>34</service_address_number>
<service_address_street>Sumner Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7700</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>104A</project_number>
  <contract_number />
<electric_account_number>54016794042</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Countryside Condos</project_name>
  <business_name />
<last_name>Powell</last_name>
<first_name>Betsy</first_name>
<service_address_number>103</service_address_number>
<service_address_street>Countryside Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1300</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>104B</project_number>
  <contract_number />
<electric_account_number>54016794042</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Countryside Condos</project_name>
  <business_name />
<last_name>Powell</last_name>
<first_name>Betsy</first_name>
<service_address_number>103</service_address_number>
<service_address_street>Countryside Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1300</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>105A</project_number>
  <contract_number />
<electric_account_number>54455071027</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Kubala</project_name>
  <business_name />
<last_name>Kubala</last_name>
<first_name>Joseph</first_name>
<service_address_number>4</service_address_number>
<service_address_street>Pell St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Ludlow</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01056</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>4992</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>105B</project_number>
  <contract_number />
<electric_account_number>54455071027</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Kubala</project_name>
  <business_name />
<last_name>Kubala</last_name>
<first_name>Joseph</first_name>
<service_address_number>4</service_address_number>
<service_address_street>Pell St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Ludlow</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01056</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>4992</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>106A</project_number>
  <contract_number />
<electric_account_number>54824091003</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Whaling-6 Maplewood</project_name>
  <business_name />
<last_name>Whaling</last_name>
<first_name>George</first_name>
<service_address_number>6</service_address_number>
<service_address_street>Maplewood</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>21000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>107A</project_number>
  <contract_number />
<electric_account_number>54946445079</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Mill Hollow</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Jones</first_name>
<service_address_number>143</service_address_number>
<service_address_street>Summer St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>26028</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>108A</project_number>
  <contract_number />
<electric_account_number>54457825024</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial Village</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>81</service_address_number>
<service_address_street>Belchertown Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>180000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>108B</project_number>
  <contract_number />
<electric_account_number>54457825024</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial Village</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>81</service_address_number>
<service_address_street>Belchertown Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>180000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>109A</project_number>
  <contract_number />
<electric_account_number>54933091019</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>37 S. Prospect St</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>37</service_address_number>
<service_address_street>S. Prospect St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>4500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>11A</project_number>
  <contract_number />
<electric_account_number>541141171077</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Albano Apts</project_name>
  <business_name />
<last_name>Albano</last_name>
<first_name>Michael</first_name>
<service_address_number>83</service_address_number>
<service_address_street>W. Housatonic St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3900</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>110A</project_number>
  <contract_number />
<electric_account_number>542448135030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Presidential Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>950</service_address_number>
<service_address_street>N. Pleasant</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>12936</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>111A</project_number>
  <contract_number />
<electric_account_number>54756191081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Winston Court</project_name>
  <business_name />
<last_name>Decoster</last_name>
<first_name>Dominque</first_name>
<service_address_number>12</service_address_number>
<service_address_street>Winston</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>15360</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>112B</project_number>
  <contract_number />
<electric_account_number>54122574072</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Mill Valley Estates</project_name>
  <business_name />
<last_name>Warren</last_name>
<first_name>Sarah</first_name>
<service_address_number>420</service_address_number>
<service_address_street>Riverglade Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>220000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>112A</project_number>
  <contract_number />
<electric_account_number>54122574072</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Mill Valley Estates</project_name>
  <business_name />
<last_name>Warren</last_name>
<first_name>Sarah</first_name>
<service_address_number>420</service_address_number>
<service_address_street>Riverglade Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>220000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>113A</project_number>
  <contract_number />
<electric_account_number>54076484062</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Chatham Court</project_name>
  <business_name />
<last_name>Rodriquez</last_name>
<first_name>Dan</first_name>
<service_address_number>127</service_address_number>
<service_address_street>Sumner Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>61000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>114A</project_number>
  <contract_number />
<electric_account_number>54177045069</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Greenleaf Court</project_name>
  <business_name />
<last_name>Rodriquez</last_name>
<first_name>Dan</first_name>
<service_address_number>145</service_address_number>
<service_address_street>Sumner Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>51000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>115A</project_number>
  <contract_number />
<electric_account_number>54206145047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Parkview Apartments</project_name>
  <business_name />
<last_name>Rodriqeuz</last_name>
<first_name>Dan</first_name>
<service_address_number>290</service_address_number>
<service_address_street>Sumner Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>27000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>116A</project_number>
  <contract_number />
<electric_account_number>54625202031</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Main Street Condos</project_name>
  <business_name />
<last_name>Laporte</last_name>
<first_name>Rosemary</first_name>
<service_address_number>355</service_address_number>
<service_address_street>Main St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>22000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>117A</project_number>
  <contract_number />
<electric_account_number>54376971008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Maple Avon</project_name>
  <business_name />
<last_name>Buynicki</last_name>
<first_name>Stephen</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>20000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>117B</project_number>
  <contract_number />
<electric_account_number>54376971008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Maple Avon</project_name>
  <business_name />
<last_name>Buynicki</last_name>
<first_name>Stephen</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>20000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>117C</project_number>
  <contract_number />
<electric_account_number>54376971008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Maple Avon</project_name>
  <business_name />
<last_name>Buynicki</last_name>
<first_name>Stephen</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>20000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>117D</project_number>
  <contract_number />
<electric_account_number>54376971008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Maple Avon</project_name>
  <business_name />
<last_name>Buynicki</last_name>
<first_name>Stephen</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>20000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>117E</project_number>
  <contract_number />
<electric_account_number>54376971008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Maple Avon</project_name>
  <business_name />
<last_name>Buynicki</last_name>
<first_name>Stephen</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>20000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>118A</project_number>
  <contract_number />
<electric_account_number>54807761077</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Golden Eagle Apts</project_name>
  <business_name />
<last_name>Henderson</last_name>
<first_name>Steven</first_name>
<service_address_number>129</service_address_number>
<service_address_street>White St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>12000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>118B</project_number>
  <contract_number />
<electric_account_number>54807761077</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Golden Eagle Apts</project_name>
  <business_name />
<last_name>Henderson</last_name>
<first_name>Steven</first_name>
<service_address_number>129</service_address_number>
<service_address_street>White St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>12000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>119A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Orchards</project_name>
  <business_name />
<last_name>katz</last_name>
<first_name>Judy</first_name>
<service_address_number>270</service_address_number>
<service_address_street>Appleton Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1200</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>12B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Chapman St.</project_name>
  <business_name />
<last_name>Plaut</last_name>
<first_name>Dave</first_name>
<service_address_number>9</service_address_number>
<service_address_street>Chapman St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>4608</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>120A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Watkins</project_name>
  <business_name />
<last_name>Watkins</last_name>
<first_name>Bob</first_name>
<service_address_number>652</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Dalton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01227</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5192</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>121A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Southview</project_name>
  <business_name />
<last_name>Chevalier</last_name>
<first_name>Luc</first_name>
<service_address_number>9</service_address_number>
<service_address_street>Cynthia Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>2100</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>122A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park West</project_name>
  <business_name />
<last_name>Henderson</last_name>
<first_name>Steve</first_name>
<service_address_number>32</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>74000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>122A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park West</project_name>
  <business_name />
<last_name>Henderson</last_name>
<first_name>Steve</first_name>
<service_address_number>32</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>74000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>122C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park West</project_name>
  <business_name />
<last_name>Henderson</last_name>
<first_name>Steve</first_name>
<service_address_number>32</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>74000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>123A</project_number>
  <contract_number />
<electric_account_number>54488691080</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>North Village</project_name>
  <business_name />
<last_name>Lucey</last_name>
<first_name>Mike</first_name>
<service_address_number>1000</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>120000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>124A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hillcrest Apts</project_name>
  <business_name />
<last_name>Chedade</last_name>
<first_name>Nadim</first_name>
<service_address_number>21</service_address_number>
<service_address_street>Highland Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>30756</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>124B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hillcrest Apts</project_name>
  <business_name />
<last_name>Chedade</last_name>
<first_name>Nadim</first_name>
<service_address_number>21</service_address_number>
<service_address_street>Highland Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>30756</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>125A</project_number>
  <contract_number />
<electric_account_number>54555891092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Worcester Manor</project_name>
  <business_name />
<last_name>Keedy</last_name>
<first_name>Deborah</first_name>
<service_address_number>961</service_address_number>
<service_address_street>Worcester St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01101</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>13500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>126A</project_number>
  <contract_number />
<electric_account_number>54662816073</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>127 College Hwy</project_name>
  <business_name />
<last_name>Hyjek</last_name>
<first_name>Robert</first_name>
<service_address_number>127</service_address_number>
<service_address_street>College Highway</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01073</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>126B</project_number>
  <contract_number />
<electric_account_number>54662816073</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>127 College Hwy</project_name>
  <business_name />
<last_name>Hyjek</last_name>
<first_name>Robert</first_name>
<service_address_number>127</service_address_number>
<service_address_street>College Highway</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01073</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>126C</project_number>
  <contract_number />
<electric_account_number>54662816073</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>127 College Hwy</project_name>
  <business_name />
<last_name>Hyjek</last_name>
<first_name>Robert</first_name>
<service_address_number>127</service_address_number>
<service_address_street>College Highway</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01073</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>127A</project_number>
  <contract_number />
<electric_account_number>54685416091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>42 Franklin St</project_name>
  <business_name />
<last_name>Renaud</last_name>
<first_name>Jolene</first_name>
<service_address_number>42</service_address_number>
<service_address_street>Franklin St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>127B</project_number>
  <contract_number />
<electric_account_number>54685416091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>42 Franklin St</project_name>
  <business_name />
<last_name>Renaud</last_name>
<first_name>Jolene</first_name>
<service_address_number>42</service_address_number>
<service_address_street>Franklin St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>127C</project_number>
  <contract_number />
<electric_account_number>54685416091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>42 Franklin St</project_name>
  <business_name />
<last_name>Renaud</last_name>
<first_name>Jolene</first_name>
<service_address_number>42</service_address_number>
<service_address_street>Franklin St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>128A</project_number>
  <contract_number />
<electric_account_number>54290326024</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Golden Hill Apts</project_name>
  <business_name />
<last_name>Hayes</last_name>
<first_name>Ann</first_name>
<service_address_number>22</service_address_number>
<service_address_street>Bartlett Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>129A</project_number>
  <contract_number />
<electric_account_number>54697671097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Forest St Apts</project_name>
  <business_name />
<last_name>Hall</last_name>
<first_name>Craig</first_name>
<service_address_number>45</service_address_number>
<service_address_street>Forest St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Lee</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01238</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7500</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>129B</project_number>
  <contract_number />
<electric_account_number>54697671097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Forest St Apts</project_name>
  <business_name />
<last_name>Hall</last_name>
<first_name>Craig</first_name>
<service_address_number>45</service_address_number>
<service_address_street>Forest St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Lee</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01238</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7500</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>129C</project_number>
  <contract_number />
<electric_account_number>54697671097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Forest St Apts</project_name>
  <business_name />
<last_name>Hall</last_name>
<first_name>Craig</first_name>
<service_address_number>45</service_address_number>
<service_address_street>Forest St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Lee</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01238</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7500</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>13B</project_number>
  <contract_number />
<electric_account_number>54910302009</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St.</project_name>
  <business_name />
<last_name>Gaetani</last_name>
<first_name>Craig</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6480</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>13A</project_number>
  <contract_number />
<electric_account_number>54910302009</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St.</project_name>
  <business_name />
<last_name>Gaetani</last_name>
<first_name>Craig</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6480</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>130A</project_number>
  <contract_number />
<electric_account_number>54564571081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pond Ridge</project_name>
  <business_name />
<last_name>Greene</last_name>
<first_name>Robert</first_name>
<service_address_number>370</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Sunderland</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10936</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Propane</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>130B</project_number>
  <contract_number />
<electric_account_number>54564571081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pond Ridge</project_name>
  <business_name />
<last_name>Greene</last_name>
<first_name>Robert</first_name>
<service_address_number>370</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Sunderland</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10936</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Propane</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>131A</project_number>
  <contract_number />
<electric_account_number>54256791070</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Oak Hill</project_name>
  <business_name />
<last_name>Iacuessa</last_name>
<first_name>Brenda</first_name>
<service_address_number>467</service_address_number>
<service_address_street>Crane Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>40000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>132A</project_number>
  <contract_number />
<electric_account_number>54566002044</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Dartmouth Condos</project_name>
  <business_name />
<last_name>Koch</last_name>
<first_name>Wendi</first_name>
<service_address_number>81</service_address_number>
<service_address_street>Dartmouth St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>36000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>133A</project_number>
  <contract_number />
<electric_account_number>5456002052</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Orchards</project_name>
  <business_name />
<last_name>Zimmerman</last_name>
<first_name>Mildred</first_name>
<service_address_number>266</service_address_number>
<service_address_street>Appleton St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10550</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>134A</project_number>
  <contract_number />
<electric_account_number>54819471087</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>College Park</project_name>
  <business_name />
<last_name>Perry</last_name>
<first_name>Sue</first_name>
<service_address_number>25</service_address_number>
<service_address_street>Keegan Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>134B</project_number>
  <contract_number />
<electric_account_number>54819471087</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>College Park</project_name>
  <business_name />
<last_name>Perry</last_name>
<first_name>Sue</first_name>
<service_address_number>25</service_address_number>
<service_address_street>Keegan Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135A</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135B</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135C</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135D</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135E</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135F</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135G</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135H</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135I</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>135J</project_number>
  <contract_number />
<electric_account_number>54904826039</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park St</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>348- 372</service_address_number>
<service_address_street>Park St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>91224</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>136A</project_number>
  <contract_number />
<electric_account_number>54297791022</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Schoolhouse Apts</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>215</service_address_number>
<service_address_street>Kings Highway</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>24552</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>136B</project_number>
  <contract_number />
<electric_account_number>54297791022</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Schoolhouse Apts</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>215</service_address_number>
<service_address_street>Kings Highway</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>24552</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>137A</project_number>
  <contract_number />
<electric_account_number>54396465015</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lexington Manor</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>105</service_address_number>
<service_address_street>Laurel St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Lee</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01238</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19140</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>137B</project_number>
  <contract_number />
<electric_account_number>54396465015</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lexington Manor</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Tim</first_name>
<service_address_number>105</service_address_number>
<service_address_street>Laurel St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Lee</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01238</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19140</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>138A</project_number>
  <contract_number />
<electric_account_number>54913002036</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>52 Chapman St</project_name>
  <business_name />
<last_name>Levin</last_name>
<first_name>Elliott</first_name>
<service_address_number>52</service_address_number>
<service_address_street>Chapman St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>139A</project_number>
  <contract_number />
<electric_account_number>54778806000</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>219 Amity St</project_name>
  <business_name />
<last_name>Peralta</last_name>
<first_name>Yurix</first_name>
<service_address_number>219</service_address_number>
<service_address_street>Amity St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>14A</project_number>
  <contract_number />
<electric_account_number>54031981061</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Ashley Arms</project_name>
  <business_name />
<last_name>Ashley Arms</last_name>
<first_name>Ashley Arms</first_name>
<service_address_number>131</service_address_number>
<service_address_street>Ashley</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>48552</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>140A</project_number>
  <contract_number />
<electric_account_number>54869102061</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Tan Brook Condos</project_name>
  <business_name />
<last_name>Morin</last_name>
<first_name>Emily</first_name>
<service_address_number>87</service_address_number>
<service_address_street>E. Pleasant St - Unit F</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1639</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>141A</project_number>
  <contract_number />
<electric_account_number>54332181031</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire Village</project_name>
  <business_name />
<last_name>Dratfield</last_name>
<first_name>Naomi</first_name>
<service_address_number>39</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1200</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>142A</project_number>
  <contract_number />
<electric_account_number>54006561054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>85 School St</project_name>
  <business_name />
<last_name>Yenner</last_name>
<first_name>William</first_name>
<service_address_number>85</service_address_number>
<service_address_street>School St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>2500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>143A</project_number>
  <contract_number />
<electric_account_number>54292336054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>159 Old Belchertown Rd</project_name>
  <business_name />
<last_name></last_name>
<first_name>159 Old Belchertown Rd</first_name>
<service_address_number>159</service_address_number>
<service_address_street>Old Belchertown Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7744</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>143B</project_number>
  <contract_number />
<electric_account_number>54292336054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>159 Old Belchertown Rd</project_name>
  <business_name />
<last_name></last_name>
<first_name>159 Old Belchertown Rd</first_name>
<service_address_number>159</service_address_number>
<service_address_street>Old Belchertown Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7744</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>144A</project_number>
  <contract_number />
<electric_account_number>54763975034</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Millhouse</project_name>
  <business_name />
<last_name>Apartments</last_name>
<first_name>Millhouse</first_name>
<service_address_number>75</service_address_number>
<service_address_street>Wells St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>120500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>145A</project_number>
  <contract_number />
<electric_account_number>54130891054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial Estates</project_name>
  <business_name />
<last_name>Estates</last_name>
<first_name>Colonial</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Beacon Cir</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01119</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>610000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>145B</project_number>
  <contract_number />
<electric_account_number>54130891054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial Estates</project_name>
  <business_name />
<last_name>Estates</last_name>
<first_name>Colonial</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Beacon Cir</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01119</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>610000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>146A</project_number>
  <contract_number />
<electric_account_number>54148881022</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Wentworth Estates</project_name>
  <business_name />
<last_name>Johnson</last_name>
<first_name>David</first_name>
<service_address_number>1600</service_address_number>
<service_address_street>Memorial Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>250000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>147 A</project_number>
  <contract_number />
<electric_account_number>54203955091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Worthy Ave</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Jeff</first_name>
<service_address_number>51</service_address_number>
<service_address_street>Worthy Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>147B</project_number>
  <contract_number />
<electric_account_number>54203955091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Worthy Ave</project_name>
  <business_name />
<last_name>Knight</last_name>
<first_name>Jeff</first_name>
<service_address_number>51</service_address_number>
<service_address_street>Worthy Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>148A</project_number>
  <contract_number />
<electric_account_number>54371291097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Regency Park</project_name>
  <business_name />
<last_name>Marsh</last_name>
<first_name>Diana</first_name>
<service_address_number>132</service_address_number>
<service_address_street>Regency Park Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>115600</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>149A</project_number>
  <contract_number />
<electric_account_number>54715205084</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Porta Villa Condos</project_name>
  <business_name />
<last_name>Basdekis</last_name>
<first_name>Kristen</first_name>
<service_address_number>147</service_address_number>
<service_address_street>Porter Lake Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01106</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>25000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>15A</project_number>
  <contract_number />
<electric_account_number>54985481043</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lakewood Apts</project_name>
  <business_name />
<last_name>Lakewood Apts</last_name>
<first_name>Lakewood Apts</first_name>
<service_address_number>160</service_address_number>
<service_address_street>Point Grove</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>96800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>15B</project_number>
  <contract_number />
<electric_account_number>54985481043</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lakewood Apts</project_name>
  <business_name />
<last_name>Lakewood Apts</last_name>
<first_name>Lakewood Apts</first_name>
<service_address_number>160</service_address_number>
<service_address_street>Point Grove</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>96800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>15C</project_number>
  <contract_number />
<electric_account_number>54985481043</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lakewood Apts</project_name>
  <business_name />
<last_name>Lakewood Apts</last_name>
<first_name>Lakewood Apts</first_name>
<service_address_number>160</service_address_number>
<service_address_street>Point Grove</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>96800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>151A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Patriot  Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>11-53</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>95250</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>151B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Patriot  Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>11-53</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>95250</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>152A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pilgrim Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>65-71</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>136500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>152B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pilgrim Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>65-71</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>136500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>153A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Craigwood Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>24-44</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>54000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>153B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Craigwood Village</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>24-44</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>54000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>154A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Bradford Arms-Craig</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>62</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>154B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Bradford Arms-Craig</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>62</service_address_number>
<service_address_street>Craig Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19800</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>155A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Bradford Arms</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>47-70</service_address_number>
<service_address_street>Bradford St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>36000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>155B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Bradford Arms</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>47-70</service_address_number>
<service_address_street>Bradford St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>36000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>155C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Bradford Arms</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>47-70</service_address_number>
<service_address_street>Bradford St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>36000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>156A</project_number>
  <contract_number />
<electric_account_number>54704291012</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial House</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>33</service_address_number>
<service_address_street>Bradford St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>29400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>156B</project_number>
  <contract_number />
<electric_account_number>54704291012</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Colonial House</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>33</service_address_number>
<service_address_street>Bradford St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>29400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>157A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler - 2</project_name>
  <business_name />
<last_name>Morizio</last_name>
<first_name>Peter</first_name>
<service_address_number>12</service_address_number>
<service_address_street>Feeding Hills Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>13000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>157B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler - 2</project_name>
  <business_name />
<last_name>Morizio</last_name>
<first_name>Peter</first_name>
<service_address_number>12</service_address_number>
<service_address_street>Feeding Hills Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>13000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>157C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler - 2</project_name>
  <business_name />
<last_name>Morizio</last_name>
<first_name>Peter</first_name>
<service_address_number>12</service_address_number>
<service_address_street>Feeding Hills Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>13000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>158A</project_number>
  <contract_number />
<electric_account_number>54569891013</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Stoney Side Condos</project_name>
  <business_name />
<last_name>Spencer</last_name>
<first_name>Lindsey</first_name>
<service_address_number>346</service_address_number>
<service_address_street>Federal St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Montague</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01351</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1056</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>158B</project_number>
  <contract_number />
<electric_account_number>54569891013</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Stoney Side Condos</project_name>
  <business_name />
<last_name>Spencer</last_name>
<first_name>Lindsey</first_name>
<service_address_number>346</service_address_number>
<service_address_street>Federal St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Montague</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01351</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1056</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>158C</project_number>
  <contract_number />
<electric_account_number>54569891013</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Stoney Side Condos</project_name>
  <business_name />
<last_name>Spencer</last_name>
<first_name>Lindsey</first_name>
<service_address_number>346</service_address_number>
<service_address_street>Federal St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Montague</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01351</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1056</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>159A</project_number>
  <contract_number />
<electric_account_number>54364494047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler-C Hwy</project_name>
  <business_name />
<last_name>Morizo</last_name>
<first_name>Peter</first_name>
<service_address_number>628</service_address_number>
<service_address_street>College Hwy</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>159B</project_number>
  <contract_number />
<electric_account_number>54364494047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler-C Hwy</project_name>
  <business_name />
<last_name>Morizo</last_name>
<first_name>Peter</first_name>
<service_address_number>628</service_address_number>
<service_address_street>College Hwy</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>159C</project_number>
  <contract_number />
<electric_account_number>54364494047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Captain Fowler-C Hwy</project_name>
  <business_name />
<last_name>Morizo</last_name>
<first_name>Peter</first_name>
<service_address_number>628</service_address_number>
<service_address_street>College Hwy</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>16A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Sutton Place</project_name>
  <business_name />
<last_name>Sutton Place</last_name>
<first_name>Sutton Place</first_name>
<service_address_number>191</service_address_number>
<service_address_street>Maple St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>52000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>160A</project_number>
  <contract_number />
<electric_account_number>54980526016</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>17 Adams St</project_name>
  <business_name />
<last_name>Cabral</last_name>
<first_name>James</first_name>
<service_address_number>17</service_address_number>
<service_address_street>Adams St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5434</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>160B</project_number>
  <contract_number />
<electric_account_number>54980526016</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>17 Adams St</project_name>
  <business_name />
<last_name>Cabral</last_name>
<first_name>James</first_name>
<service_address_number>17</service_address_number>
<service_address_street>Adams St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5434</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>161</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pulpit Hilll</project_name>
  <business_name />
<last_name>Bair</last_name>
<first_name>Nancy</first_name>
<service_address_number>3</service_address_number>
<service_address_street>Pulpit Hill</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Propane</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Propane</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>162A</project_number>
  <contract_number />
<electric_account_number>54995256088</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>104 Appleton Ave</project_name>
  <business_name />
<last_name>Southard</last_name>
<first_name>Nolan</first_name>
<service_address_number>104</service_address_number>
<service_address_street>Appleton Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3922</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>163A</project_number>
  <contract_number />
<electric_account_number>54114134018</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Harvest Valley</project_name>
  <business_name />
<last_name>Jopson</last_name>
<first_name>Russ</first_name>
<service_address_number>273</service_address_number>
<service_address_street>East St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>96000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>163B</project_number>
  <contract_number />
<electric_account_number>54114134018</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Harvest Valley</project_name>
  <business_name />
<last_name>Jopson</last_name>
<first_name>Russ</first_name>
<service_address_number>273</service_address_number>
<service_address_street>East St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>96000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>164A</project_number>
  <contract_number />
<electric_account_number>54872781091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>1 Winston CT</project_name>
  <business_name />
<last_name>Campbell</last_name>
<first_name>Kathy</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Winston Ct</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1584</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>165A</project_number>
  <contract_number />
<electric_account_number>54592202030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Deerfield Meadows</project_name>
  <business_name />
<last_name>Plumbley</last_name>
<first_name>Bill</first_name>
<service_address_number>11A</service_address_number>
<service_address_street>Duncan Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>S. Deerfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>84000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>166A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Greenleaves</project_name>
  <business_name />
<last_name>Mileski</last_name>
<first_name>Peg</first_name>
<service_address_number>24-26</service_address_number>
<service_address_street>Greeenleaves Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>120000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>166B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Greenleaves</project_name>
  <business_name />
<last_name>Mileski</last_name>
<first_name>Peg</first_name>
<service_address_number>24-26</service_address_number>
<service_address_street>Greeenleaves Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>120000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>167A</project_number>
  <contract_number />
<electric_account_number>54240202077</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lakecrest</project_name>
  <business_name />
<last_name>Bellora</last_name>
<first_name>Marc</first_name>
<service_address_number>47</service_address_number>
<service_address_street>Lakecrest Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>167B</project_number>
  <contract_number />
<electric_account_number>54240202077</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lakecrest</project_name>
  <business_name />
<last_name>Bellora</last_name>
<first_name>Marc</first_name>
<service_address_number>47</service_address_number>
<service_address_street>Lakecrest Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>168A</project_number>
  <contract_number />
<electric_account_number>54867002016</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Center Pond</project_name>
  <business_name />
<last_name>Seymour</last_name>
<first_name>Martha</first_name>
<service_address_number>60</service_address_number>
<service_address_street>North St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Dalton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01226</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>32000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>168B</project_number>
  <contract_number />
<electric_account_number>54867002016</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Center Pond</project_name>
  <business_name />
<last_name>Seymour</last_name>
<first_name>Martha</first_name>
<service_address_number>60</service_address_number>
<service_address_street>North St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Dalton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01226</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>32000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>169A</project_number>
  <contract_number />
<electric_account_number>54644402083</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>James Meadow</project_name>
  <business_name />
<last_name>Britten</last_name>
<first_name>Nancy</first_name>
<service_address_number>F4</service_address_number>
<service_address_street>Pondview Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>65000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel></primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>17A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>L Street</project_name>
  <business_name />
<last_name>Lewandowski</last_name>
<first_name>Jodi</first_name>
<service_address_number>163-167</service_address_number>
<service_address_street>L Street</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Turners Falls</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01376</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5592</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>170A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pines</project_name>
  <business_name />
<last_name>Hayer</last_name>
<first_name>Mae</first_name>
<service_address_number>1450</service_address_number>
<service_address_street>North St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>40000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>171A</project_number>
  <contract_number />
<electric_account_number>54983781014</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pine Grove/Ice Pond Woods</project_name>
  <business_name />
<last_name>Kamins</last_name>
<first_name>Patrick</first_name>
<service_address_number></service_address_number>
<service_address_street>Pine Grove/Crossbrook</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip></service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>72000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>172A</project_number>
  <contract_number />
<electric_account_number>54986302040</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Waters Edge</project_name>
  <business_name />
<last_name>Savoie</last_name>
<first_name>Debra</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Waters Edge</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Ludlow</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01056</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>150000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Gas</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>173A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>196 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>196</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>15219</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>173B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>196 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>196</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>15219</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>173C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>196 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>196</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>15219</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>174A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Cathedral Apts</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>308</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>14400</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>174B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Cathedral Apts</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>308</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>14400</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>175A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>220 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>220</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>11167</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>175B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>220 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>220</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>11167</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>175C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>220 N. Pleasant St</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>220</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>11167</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>176A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Halcourt Gardens</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>21 &amp;26</service_address_number>
<service_address_street>Hallock St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>27263</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>176B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Halcourt Gardens</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>21 &amp;26</service_address_number>
<service_address_street>Hallock St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>27263</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>176c</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Halcourt Gardens</project_name>
  <business_name />
<last_name></last_name>
<first_name>RLS Properties</first_name>
<service_address_number>21 &amp;26</service_address_number>
<service_address_street>Hallock St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>27263</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>177A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>768 Belmont Ave</project_name>
  <business_name />
<last_name>Sares</last_name>
<first_name>Peter</first_name>
<service_address_number>768</service_address_number>
<service_address_street>Belmont Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5297</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>178A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>127 Main St Apts</project_name>
  <business_name />
<last_name>Kwolek</last_name>
<first_name>Diane</first_name>
<service_address_number>127</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6700</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>179A</project_number>
  <contract_number />
<electric_account_number>54292891082</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Prospect Ave Apts</project_name>
  <business_name />
<last_name>Bonneau</last_name>
<first_name>Helen</first_name>
<service_address_number>21 &amp; 31</service_address_number>
<service_address_street>Prospect Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>8300</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>18A</project_number>
  <contract_number />
<electric_account_number>54132181033</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire  Village</project_name>
  <business_name />
<last_name>Melcher</last_name>
<first_name>Marcia</first_name>
<service_address_number>16</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1650</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>180A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Capri Apts</project_name>
  <business_name />
<last_name>Hooben</last_name>
<first_name>Gertrude</first_name>
<service_address_number>97-109</service_address_number>
<service_address_street>River St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>W. Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>16000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>181A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plantation Condos</project_name>
  <business_name />
<last_name>Hogan</last_name>
<first_name>Mike</first_name>
<service_address_number>1-120</service_address_number>
<service_address_street>Plantation Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>166000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>181B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plantation Condos</project_name>
  <business_name />
<last_name>Hogan</last_name>
<first_name>Mike</first_name>
<service_address_number>1-120</service_address_number>
<service_address_street>Plantation Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>166000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>181C</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plantation Condos</project_name>
  <business_name />
<last_name>Hogan</last_name>
<first_name>Mike</first_name>
<service_address_number>1-120</service_address_number>
<service_address_street>Plantation Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>166000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>181D</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plantation Condos</project_name>
  <business_name />
<last_name>Hogan</last_name>
<first_name>Mike</first_name>
<service_address_number>1-120</service_address_number>
<service_address_street>Plantation Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>166000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>182A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>296 Turners Falls Rd</project_name>
  <business_name />
<last_name>Peletier</last_name>
<first_name>Sheila</first_name>
<service_address_number>296</service_address_number>
<service_address_street>Turners Falls Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Montague</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01351</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>4976</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>183A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Stone Mill Condos</project_name>
  <business_name />
<last_name>Lewis</last_name>
<first_name>Ronald</first_name>
<service_address_number>488</service_address_number>
<service_address_street>E. Housatonic St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Dalton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01240</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>184A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>5-7 Osgood St</project_name>
  <business_name />
<last_name>Audet</last_name>
<first_name>Mike</first_name>
<service_address_number>5-7</service_address_number>
<service_address_street>Osgood St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3590</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>185A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Mill House Apartments</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>643</service_address_number>
<service_address_street>Suffield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>3200</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>186A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Meadow House Apts</project_name>
  <business_name />
<last_name>Abrahamson</last_name>
<first_name>Paul</first_name>
<service_address_number>410 &amp; 410R</service_address_number>
<service_address_street>Meadow St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>32000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>187</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plum Tree Apts</project_name>
  <business_name />
<last_name>Radetsky</last_name>
<first_name>Jack</first_name>
<service_address_number>11</service_address_number>
<service_address_street>Plumtree Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Sunderland</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5200</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>187A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Plum Tree Apts</project_name>
  <business_name />
<last_name>Radetsky</last_name>
<first_name>Jack</first_name>
<service_address_number>11</service_address_number>
<service_address_street>Plumtree Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Sunderland</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5200</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>188A</project_number>
  <contract_number />
<electric_account_number>54831781034</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Park Ave Apt</project_name>
  <business_name />
<last_name>Perras</last_name>
<first_name>Arnold</first_name>
<service_address_number>225-237</service_address_number>
<service_address_street>Park Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Dalton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01240</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>17000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>189A</project_number>
  <contract_number />
<electric_account_number>54975374000</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>433 North St</project_name>
  <business_name />
<last_name>Downing</last_name>
<first_name>Ben</first_name>
<service_address_number>433</service_address_number>
<service_address_street>North St-301</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>19A</project_number>
  <contract_number />
<electric_account_number>54936181080</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire Village</project_name>
  <business_name />
<last_name>Dubois</last_name>
<first_name>Dave</first_name>
<service_address_number>5</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>800</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>190A</project_number>
  <contract_number />
<electric_account_number>54612502021</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>American Inn</project_name>
  <business_name />
<last_name>Houlihan</last_name>
<first_name>Brian</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Sawmill Park</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Southwick</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01077</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>150000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>191A</project_number>
  <contract_number />
<electric_account_number>5421109053</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>170-2 Avenue A</project_name>
  <business_name />
<last_name>Martineau</last_name>
<first_name>Jim</first_name>
<service_address_number>170-2</service_address_number>
<service_address_street>Avenue A</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Turners Falls</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01375</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6780</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>192A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Berkshire Mtn Lodge</project_name>
  <business_name />
<last_name>Crigler</last_name>
<first_name>Matt</first_name>
<service_address_number>8</service_address_number>
<service_address_street>Dan Fox Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsifeld</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>100000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>193A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Classical Condos</project_name>
  <business_name />
<last_name>Costa</last_name>
<first_name>Carol</first_name>
<service_address_number>235</service_address_number>
<service_address_street>State St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>100000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>20A</project_number>
  <contract_number />
<electric_account_number>54748425006</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire Village</project_name>
  <business_name />
<last_name>Naughton</last_name>
<first_name>Katherine</first_name>
<service_address_number>26</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1650</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>21A</project_number>
  <contract_number />
<electric_account_number>54118081090</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire Village</project_name>
  <business_name />
<last_name>Baj</last_name>
<first_name>Rose</first_name>
<service_address_number>31</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>800</conditioned_sq_footage>
<primary_heating_fuel>Propane</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>22A</project_number>
  <contract_number />
<electric_account_number>54810181065</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Hampshire Village</project_name>
  <business_name />
<last_name>Guntupalli</last_name>
<first_name>Srinivasa</first_name>
<service_address_number>37</service_address_number>
<service_address_street>Autumn Lane</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1650</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>23A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>2-4 Pomeroy Ct</project_name>
  <business_name />
<last_name>Pomeroy Court</last_name>
<first_name>Pomeroy Court</first_name>
<service_address_number>2-4</service_address_number>
<service_address_street>Pomeroy Ct.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>2640</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>23B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>2-4 Pomeroy Ct</project_name>
  <business_name />
<last_name>Pomeroy Court</last_name>
<first_name>Pomeroy Court</first_name>
<service_address_number>2-4</service_address_number>
<service_address_street>Pomeroy Ct.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>2640</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>24B</project_number>
  <contract_number />
<electric_account_number>54962255030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St</project_name>
  <business_name />
<last_name>Fish</last_name>
<first_name>Fish</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1024</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>24A</project_number>
  <contract_number />
<electric_account_number>54962255030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St</project_name>
  <business_name />
<last_name>Fish</last_name>
<first_name>Fish</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1024</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>25B</project_number>
  <contract_number />
<electric_account_number>54287561047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St</project_name>
  <business_name />
<last_name>Barcz</last_name>
<first_name>Barcz</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1024</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>25A</project_number>
  <contract_number />
<electric_account_number>54287561047</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St</project_name>
  <business_name />
<last_name>Barcz</last_name>
<first_name>Barcz</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1024</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>26A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Indian St</project_name>
  <business_name />
<last_name>Mayernik</last_name>
<first_name>Mayernik</first_name>
<service_address_number>19</service_address_number>
<service_address_street>Indian</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1024</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>27A</project_number>
  <contract_number />
<electric_account_number>54258871094</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>356-364 Belmont Ave</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>354-364</service_address_number>
<service_address_street>Belmont Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>27B</project_number>
  <contract_number />
<electric_account_number>54258871094</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>356-364 Belmont Ave</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>354-364</service_address_number>
<service_address_street>Belmont Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>28A</project_number>
  <contract_number />
<electric_account_number>54506791029</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>125-127 Belmont</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>125-135</service_address_number>
<service_address_street>Belmont</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>28B</project_number>
  <contract_number />
<electric_account_number>54506791029</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>125-127 Belmont</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>125-135</service_address_number>
<service_address_street>Belmont</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>29A</project_number>
  <contract_number />
<electric_account_number>54380515049</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>85-91 Woodside</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>85-91</service_address_number>
<service_address_street>Woodside Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>29B</project_number>
  <contract_number />
<electric_account_number>54380515049</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>85-91 Woodside</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>85-91</service_address_number>
<service_address_street>Woodside Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>29C</project_number>
  <contract_number />
<electric_account_number>54380515049</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>85-91 Woodside</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>85-91</service_address_number>
<service_address_street>Woodside Dr</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>10400</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>30</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Greenfield Acres</project_name>
  <business_name />
<last_name>Ackerman</last_name>
<first_name>Trudy</first_name>
<service_address_number>10</service_address_number>
<service_address_street>Congress St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Greenfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01301</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>75000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>31A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>18-24 Benton</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>18-24</service_address_number>
<service_address_street>Benton</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>9750</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>31B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>18-24 Benton</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>18-24</service_address_number>
<service_address_street>Benton</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>9750</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>32A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>183-191 Boston Rd</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>183-191</service_address_number>
<service_address_street>Boston Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>32B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>183-191 Boston Rd</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>183-191</service_address_number>
<service_address_street>Boston Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>19500</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>33A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lawton Arms</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>30-114</service_address_number>
<service_address_street>Lawton</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>61750</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>33B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Lawton Arms</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>30-114</service_address_number>
<service_address_street>Lawton</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>61750</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>34A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>62 Main St</project_name>
  <business_name />
<last_name>Moore</last_name>
<first_name>Pete</first_name>
<service_address_number>62</service_address_number>
<service_address_street>Main St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Hatfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01038</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>9600</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>35A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>187 Main</project_name>
  <business_name />
<last_name>Kuznik</last_name>
<first_name>Donna</first_name>
<service_address_number>187</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Northfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01360</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6200</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>36A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Beekman Place Condos</project_name>
  <business_name />
<last_name>Beekman Place</last_name>
<first_name>Beekman Place</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Beekman Dr.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>265000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>36B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Beekman Place Condos</project_name>
  <business_name />
<last_name>Beekman Place</last_name>
<first_name>Beekman Place</first_name>
<service_address_number>1</service_address_number>
<service_address_street>Beekman Dr.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01001</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>265000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>37A</project_number>
  <contract_number />
<electric_account_number>5442171059</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>4th St</project_name>
  <business_name />
<last_name>Elbaum - 4th St</last_name>
<first_name>Elbaum - 4th St</first_name>
<service_address_number>88-86</service_address_number>
<service_address_street>4th St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Turners Falls</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01349</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>5500</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>38A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>255 Federal St</project_name>
  <business_name />
<last_name>255 Federal</last_name>
<first_name>255 Federal</first_name>
<service_address_number>255</service_address_number>
<service_address_street>Federal St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Montague</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01351</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6000</conditioned_sq_footage>
<primary_heating_fuel>Oil</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>39A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>St. James Manor</project_name>
  <business_name />
<last_name>St. James Manor</last_name>
<first_name>St. James Manor</first_name>
<service_address_number>746</service_address_number>
<service_address_street>St. James Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01104</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>42600</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>40A</project_number>
  <contract_number />
<electric_account_number>54695535005</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>62 Main St</project_name>
  <business_name />
<last_name>62 Main-1</last_name>
<first_name>62 Main-1</first_name>
<service_address_number>62 - 1</service_address_number>
<service_address_street>Main St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Hatfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01038</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1100</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>41A</project_number>
  <contract_number />
<electric_account_number>54851102046</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>62 Main St</project_name>
  <business_name />
<last_name>62 Main-2</last_name>
<first_name>62 Main-2</first_name>
<service_address_number>62-2</service_address_number>
<service_address_street>Main St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Hatfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01038</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1100</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>42A</project_number>
  <contract_number />
<electric_account_number>54264102070</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>62 Main</project_name>
  <business_name />
<last_name>62 Main-3</last_name>
<first_name>62 Main-3</first_name>
<service_address_number>62</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Hatfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01038</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>1100</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>43A</project_number>
  <contract_number />
<electric_account_number>54853691046</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 19</project_name>
  <business_name />
<last_name>LaFlamme</last_name>
<first_name>Walt</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Propane</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>43B</project_number>
  <contract_number />
<electric_account_number>54853691046</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 19</project_name>
  <business_name />
<last_name>LaFlamme</last_name>
<first_name>Walt</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Propane</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>44A</project_number>
  <contract_number />
<electric_account_number>54044384014</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit 18</project_name>
  <business_name />
<last_name>Brown</last_name>
<first_name>Dave</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>44B</project_number>
  <contract_number />
<electric_account_number>54044384014</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit 18</project_name>
  <business_name />
<last_name>Brown</last_name>
<first_name>Dave</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>45A</project_number>
  <contract_number />
<electric_account_number>54315691030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit 15</project_name>
  <business_name />
<last_name>Mazur</last_name>
<first_name>Joyce</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>46A</project_number>
  <contract_number />
<electric_account_number>54468274022</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 16</project_name>
  <business_name />
<last_name>Incampo</last_name>
<first_name>Anthony</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>47A</project_number>
  <contract_number />
<electric_account_number>5220935068</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 50</project_name>
  <business_name />
<last_name>Dupere</last_name>
<first_name>Adam</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>48A</project_number>
  <contract_number />
<electric_account_number>54870691011</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 26</project_name>
  <business_name />
<last_name>Facto</last_name>
<first_name>Bill</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>48B</project_number>
  <contract_number />
<electric_account_number>54870691011</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 26</project_name>
  <business_name />
<last_name>Facto</last_name>
<first_name>Bill</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>49A</project_number>
  <contract_number />
<electric_account_number>54770691012</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 28</project_name>
  <business_name />
<last_name>Duval</last_name>
<first_name>Rose</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Oil</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>50A</project_number>
  <contract_number />
<electric_account_number>54470691015</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 31</project_name>
  <business_name />
<last_name>Malloy</last_name>
<first_name>Gail</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>50B</project_number>
  <contract_number />
<electric_account_number>54470691015</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 31</project_name>
  <business_name />
<last_name>Malloy</last_name>
<first_name>Gail</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>51A</project_number>
  <contract_number />
<electric_account_number>54705691038</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 36</project_name>
  <business_name />
<last_name>Strader</last_name>
<first_name>Jill</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>52A</project_number>
  <contract_number />
<electric_account_number>54689591097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit 44</project_name>
  <business_name />
<last_name>KNIght</last_name>
<first_name>Amy</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>52B</project_number>
  <contract_number />
<electric_account_number>54689591097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit 44</project_name>
  <business_name />
<last_name>KNIght</last_name>
<first_name>Amy</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>53A</project_number>
  <contract_number />
<electric_account_number>54595861097</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 51</project_name>
  <business_name />
<last_name>Tetreault</last_name>
<first_name>Meaghan</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>54A</project_number>
  <contract_number />
<electric_account_number>54583691050</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier- Unit 7</project_name>
  <business_name />
<last_name>Jachym</last_name>
<first_name>Carol</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>54B</project_number>
  <contract_number />
<electric_account_number>54583691050</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier- Unit 7</project_name>
  <business_name />
<last_name>Jachym</last_name>
<first_name>Carol</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>55A</project_number>
  <contract_number />
<electric_account_number>54748691086</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 42</project_name>
  <business_name />
<last_name>Sasnowich</last_name>
<first_name>Rebecca</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>55B</project_number>
  <contract_number />
<electric_account_number>54748691086</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 42</project_name>
  <business_name />
<last_name>Sasnowich</last_name>
<first_name>Rebecca</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>56A</project_number>
  <contract_number />
<electric_account_number>54319125092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 11</project_name>
  <business_name />
<last_name>Nelson</last_name>
<first_name>Jan</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>56B</project_number>
  <contract_number />
<electric_account_number>54319125092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 11</project_name>
  <business_name />
<last_name>Nelson</last_name>
<first_name>Jan</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>57A</project_number>
  <contract_number />
<electric_account_number>542448691081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 55</project_name>
  <business_name />
<last_name>Tamsin</last_name>
<first_name>JIm</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>58A</project_number>
  <contract_number />
<electric_account_number>54248691081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 53</project_name>
  <business_name />
<last_name>Johnson</last_name>
<first_name>Carol</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>58B</project_number>
  <contract_number />
<electric_account_number>54248691081</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 53</project_name>
  <business_name />
<last_name>Johnson</last_name>
<first_name>Carol</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>59A</project_number>
  <contract_number />
<electric_account_number>54289691008</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 47</project_name>
  <business_name />
<last_name>Varney</last_name>
<first_name>Laura</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>60A</project_number>
  <contract_number />
<electric_account_number>54667945091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 25</project_name>
  <business_name />
<last_name>Schaniguine</last_name>
<first_name>Magali</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>60B</project_number>
  <contract_number />
<electric_account_number>54667945091</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 25</project_name>
  <business_name />
<last_name>Schaniguine</last_name>
<first_name>Magali</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>61A</project_number>
  <contract_number />
<electric_account_number>54715691036</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 12</project_name>
  <business_name />
<last_name>Dierdre</last_name>
<first_name>Heather</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>61B</project_number>
  <contract_number />
<electric_account_number>54715691036</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 12</project_name>
  <business_name />
<last_name>Dierdre</last_name>
<first_name>Heather</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>62A</project_number>
  <contract_number />
<electric_account_number>54591535075</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 46</project_name>
  <business_name />
<last_name>Doubleday</last_name>
<first_name>Luke</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>63A</project_number>
  <contract_number />
<electric_account_number>54324305028</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 63</project_name>
  <business_name />
<last_name>Bloniarz</last_name>
<first_name>Ed</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>64A</project_number>
  <contract_number />
<electric_account_number>54453691040</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 32</project_name>
  <business_name />
<last_name>Cleary</last_name>
<first_name>Sharon</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>64B</project_number>
  <contract_number />
<electric_account_number>54453691040</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 32</project_name>
  <business_name />
<last_name>Cleary</last_name>
<first_name>Sharon</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>65A</project_number>
  <contract_number />
<electric_account_number>54368091062</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier - Unit-61</project_name>
  <business_name />
<last_name>Brooks</last_name>
<first_name>Deana</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>66A</project_number>
  <contract_number />
<electric_account_number>54380691014</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 2</project_name>
  <business_name />
<last_name>Laine</last_name>
<first_name>Allina</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>67A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 68</project_name>
  <business_name />
<last_name>McNanney</last_name>
<first_name>Patrick</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>68A</project_number>
  <contract_number />
<electric_account_number>54605471044</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 5</project_name>
  <business_name />
<last_name>Ouimet</last_name>
<first_name>Jamie</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>68B</project_number>
  <contract_number />
<electric_account_number>54605471044</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 5</project_name>
  <business_name />
<last_name>Ouimet</last_name>
<first_name>Jamie</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>69A</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 3</project_name>
  <business_name />
<last_name>Lynes</last_name>
<first_name>Anne</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>69B</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 3</project_name>
  <business_name />
<last_name>Lynes</last_name>
<first_name>Anne</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>69C</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 3</project_name>
  <business_name />
<last_name>Lynes</last_name>
<first_name>Anne</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>70A</project_number>
  <contract_number />
<electric_account_number>54658691084</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 4</project_name>
  <business_name />
<last_name>Ladoucer</last_name>
<first_name>Susan</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>70B</project_number>
  <contract_number />
<electric_account_number>54658691084</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 4</project_name>
  <business_name />
<last_name>Ladoucer</last_name>
<first_name>Susan</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>71A</project_number>
  <contract_number />
<electric_account_number>54329591051</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 40</project_name>
  <business_name />
<last_name>Morse</last_name>
<first_name>Audrey</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>72A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 58</project_name>
  <business_name />
<last_name>Lebeau</last_name>
<first_name>Chris</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>73A</project_number>
  <contract_number />
<electric_account_number>54505691030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 39</project_name>
  <business_name />
<last_name>Landry</last_name>
<first_name>Shirley</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>73B</project_number>
  <contract_number />
<electric_account_number>54505691030</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 39</project_name>
  <business_name />
<last_name>Landry</last_name>
<first_name>Shirley</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>74A</project_number>
  <contract_number />
<electric_account_number>54358691087</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 20</project_name>
  <business_name />
<last_name>Carenzo</last_name>
<first_name>James</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>74B</project_number>
  <contract_number />
<electric_account_number>54358691087</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 20</project_name>
  <business_name />
<last_name>Carenzo</last_name>
<first_name>James</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>75A</project_number>
  <contract_number />
<electric_account_number>54909584013</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 59</project_name>
  <business_name />
<last_name>Skinner</last_name>
<first_name>Joni</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>75B</project_number>
  <contract_number />
<electric_account_number>54909584013</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 59</project_name>
  <business_name />
<last_name>Skinner</last_name>
<first_name>Joni</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>76A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-unit 1</project_name>
  <business_name />
<last_name>Charles</last_name>
<first_name>Matt</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>76B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-unit 1</project_name>
  <business_name />
<last_name>Charles</last_name>
<first_name>Matt</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>77A</project_number>
  <contract_number />
<electric_account_number>54401455068</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 38</project_name>
  <business_name />
<last_name>Pourinski</last_name>
<first_name>Elaine</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>77B</project_number>
  <contract_number />
<electric_account_number>54401455068</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 38</project_name>
  <business_name />
<last_name>Pourinski</last_name>
<first_name>Elaine</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>78A</project_number>
  <contract_number />
<electric_account_number>5495881020</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Laurel Apartments</project_name>
  <business_name />
<last_name>Mercier</last_name>
<first_name>Doug</first_name>
<service_address_number>1343</service_address_number>
<service_address_street>Riverdale St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>00189</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>29700</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>78B</project_number>
  <contract_number />
<electric_account_number>5495881020</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Laurel Apartments</project_name>
  <business_name />
<last_name>Mercier</last_name>
<first_name>Doug</first_name>
<service_address_number>1343</service_address_number>
<service_address_street>Riverdale St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>00189</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>29700</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>78C</project_number>
  <contract_number />
<electric_account_number>5495881020</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Laurel Apartments</project_name>
  <business_name />
<last_name>Mercier</last_name>
<first_name>Doug</first_name>
<service_address_number>1343</service_address_number>
<service_address_street>Riverdale St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>00189</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>29700</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>79A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>70-74 Grenada</project_name>
  <business_name />
<last_name>Tortoriello</last_name>
<first_name>Matthew</first_name>
<service_address_number>70-74</service_address_number>
<service_address_street>Grenada St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01104</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>230000</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81A</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81B</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81C</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81D</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81E</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>81F</project_number>
  <contract_number />
<electric_account_number>54218671071</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Pynchon Townhouses</project_name>
  <business_name />
<last_name>Dreyer</last_name>
<first_name>Donna</first_name>
<service_address_number>350</service_address_number>
<service_address_street>Meadow St.</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Agawam</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01010</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>82A</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 8</project_name>
  <business_name />
<last_name>Harrington</last_name>
<first_name>Jane</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>83A</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 45</project_name>
  <business_name />
<last_name>Lucchessi</last_name>
<first_name>Laura</first_name>
<service_address_number>41</service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>84A</project_number>
  <contract_number />
<electric_account_number>54643691066</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Town Crier-Unit 62</project_name>
  <business_name />
<last_name>Connor</last_name>
<first_name>Charles</first_name>
<service_address_number></service_address_number>
<service_address_street>South St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Easthampton</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01027</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>900</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>85A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>McAlister Condos</project_name>
  <business_name />
<last_name>Holt</last_name>
<first_name>Kathy</first_name>
<service_address_number>58</service_address_number>
<service_address_street>W. Housatonic</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7998</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>85B</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>McAlister Condos</project_name>
  <business_name />
<last_name>Holt</last_name>
<first_name>Kathy</first_name>
<service_address_number>58</service_address_number>
<service_address_street>W. Housatonic</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>7998</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86A</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86B</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86C</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86D</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86E</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86F</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86G</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86H</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86I</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>86J</project_number>
  <contract_number />
<electric_account_number>54632502092</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Riverside Park Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>27</service_address_number>
<service_address_street>Montague Rd</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>87A</project_number>
  <contract_number />
<electric_account_number>54107025025</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Crestview</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>995/1001</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>87B</project_number>
  <contract_number />
<electric_account_number>54107025025</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Crestview</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>995/1001</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>87C</project_number>
  <contract_number />
<electric_account_number>54107025025</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Crestview</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>995/1001</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>88A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Perry Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>85</service_address_number>
<service_address_street>Amity</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>90A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Griggs Apts</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>124</service_address_number>
<service_address_street>Amity</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>91A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>285 Main St</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>285</service_address_number>
<service_address_street>Main</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Electricity</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>92A</project_number>
  <contract_number />
<electric_account_number>5430035054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>177 N. Pleasant</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>177</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>92B</project_number>
  <contract_number />
<electric_account_number>5430035054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>177 N. Pleasant</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>177</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>92C</project_number>
  <contract_number />
<electric_account_number>5430035054</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>177 N. Pleasant</project_name>
  <business_name />
<last_name>Realty</last_name>
<first_name>Kamins</first_name>
<service_address_number>177</service_address_number>
<service_address_street>N. Pleasant St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Amherst</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01002</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>93A</project_number>
  <contract_number />
<electric_account_number>54461025033</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>135 Belmont-Front</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>135 Front</service_address_number>
<service_address_street>Belmont</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>93C</project_number>
  <contract_number />
<electric_account_number>54461025033</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>135 Belmont-Front</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>135 Front</service_address_number>
<service_address_street>Belmont</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>94A</project_number>
  <contract_number />
<electric_account_number>54873581078</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>135 Belmont-Rear</project_name>
  <business_name />
<last_name>McGrath</last_name>
<first_name>Brian</first_name>
<service_address_number>135 Front</service_address_number>
<service_address_street>Belmont</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>0</conditioned_sq_footage>
<primary_heating_fuel>Electricity</primary_heating_fuel>
<primary_cooling_fuel>None</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>96A</project_number>
  <contract_number />
<electric_account_number>54105345052</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Sorrento</project_name>
  <business_name />
<last_name>Properties</last_name>
<first_name>Atlas</first_name>
<service_address_number>66</service_address_number>
<service_address_street>Sorrento St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01108</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>8000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>97A</project_number>
  <contract_number />
<electric_account_number>54105345052</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Union St</project_name>
  <business_name />
<last_name>Properties</last_name>
<first_name>Atlas</first_name>
<service_address_number>528-532</service_address_number>
<service_address_street>Union St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01109</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>12000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>98A</project_number>
  <contract_number />
<electric_account_number></electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>Westfield St</project_name>
  <business_name />
<last_name>Properties</last_name>
<first_name>Atlas</first_name>
<service_address_number>758</service_address_number>
<service_address_street>Westfield St</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>West Springfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01089</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>N</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>14000</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
 <customer>
<project_number>99A</project_number>
  <contract_number />
<electric_account_number>54180171027</electric_account_number>
<electric_company_code>WMECO</electric_company_code>
  <gas_account_number />
  <gas_company_code />
  <project_name>10 Kent Ave Apartments</project_name>
  <business_name />
<last_name>Whaling</last_name>
<first_name>George</first_name>
<service_address_number>10</service_address_number>
<service_address_street>Kent Ave</service_address_street>
<service_address_overflow></service_address_overflow>
<service_address_city>Pittsfield</service_address_city>
<service_address_state>MA</service_address_state>
<service_address_zip>01201</service_address_zip>
  <mailing_address_number />
  <mailing_address_street />
  <mailing_address_overflow />
  <mailing_address_city />
  <mailing_address_state />
  <mailing_address_zip />
  <home_phone />
  <work_phone />
  <email_address />
  <dwelling_code />
<rent_own>Y</rent_own>
<low_income_flag>N</low_income_flag>
<conditioned_sq_footage>6048</conditioned_sq_footage>
<primary_heating_fuel>Gas</primary_heating_fuel>
<primary_cooling_fuel>Electricity</primary_cooling_fuel>
<primary_dhw_fuel>Gas</primary_dhw_fuel>
  <area_code/>
  <secondary_heating_fuel />
  <secondary_dhw_fuel />
  <cogeneration_flag >N</cogeneration_flag >
  <cogeneration_type />
  </customer>
</demographic>';

$xml = simplexml_load_string($xml_string);
$json = json_encode($xml);
$array = json_decode($json,TRUE);
//print_r($array);
foreach ($array["customer"] as $customer){
	$project_name = $customer["project_name"];
	$last_name = $customer["last_name"];
	$first_name = $customer["first_name"];
	$service_address_number = $customer["service_address_number"];
	$service_address_street = $customer["service_address_street"];
	$service_address_city = $customer["service_address_city"];
	$service_address_state = $customer["service_address_state"];
	$primary_heating_fuel = $customer["primary_heating_fuel"];
	$infoArray = array(
		"account"=>$project_name,
		"last_name"=>$last_name,
		"first_name"=>$first_name,
		"address"=>$service_address_number,
		"street"=>$service_address_street,
		"city"=>$service_address_city,
		"state"=>$service_address_state,
		"zip"=>$customer["service_address_zip"],
		"primary_heating_fuel"=>$primary_heating_fuel
	);
	$heatingAccounts[$primary_heating_fuel][] = $infoArray;
}
//	print_r($heatingAccounts["Oil"]);
ksort($heatingAccounts);
echo "<table><tr><td>Heating Fuel</td><td>Account</td><td>FirstName</td><td>LastName</td><td>address</td><td>Street</td><td>City</td><td>State</td><td>Zip</td></tr>";
foreach($heatingAccounts as $fuelType=>$accounts){
	foreach ($accounts as $details){
		echo "<tr>
			<td>".$fuelType."</td>
			<td>".$details["account"]."</td>
			<td>".$details["last_name"]."</td>
			<td>".$details["first_name"]."</td>
			<td>".$details["address"]."</td>
			<td>".$details["street"]."</td>
			<td>".$details["city"]."</td>
			<td>".$details["state"]."</td>
			<td>".$details["zip"]."</td>
			</tr>";
	}
}
echo "</table>";
?>