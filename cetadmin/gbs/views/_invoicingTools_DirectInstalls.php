<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the <span id="invoiceDate"></span> Direct Installs EFI Data for Fiscal </h3>
<?php
$_SESSION['SalesForceReport'] = "DirectInstalls";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");

$GBSProvider = new GBSProvider($dataConn);

$paginationResult = $GBSProvider->invoiceTool_getProductInfo();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->MeasureName){
		if (strpos($record->MeasureName,",")){
			$measureNames = explode(",",$record->MeasureName);
		}else{
			$measureNames = array($record->MeasureName);
		}
		foreach ($measureNames as $measureName){
			$standardizedNames[$measureName]["EFI"] = $record->EFI;
			$standardizedNames[$measureName]["Description"] = $record->Description;
		}
		$standardizedNames[$record->BGasName]["EFI"] = $record->EFI;
		$standardizedNames[$record->BGasName]["Description"] = $record->Description;
		$standardizedEFI[$record->EFI]["Description"] = $record->Description;
	}
}
//print_pre($standardizedNames);
$setTab = "Installs";
$TabCodes = array("Installs"=>"Installs","Adjustments"=>"Adjustments");
foreach ($TabCodes as $TabName=>$TabCode){
	$TabHashes[] = $TabCode;
	$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
}
echo '<ul id="admin-tabs" class="tabs">';
		foreach ($TabListItems as $TabItem){
			echo $TabItem;
		}
echo '</ul>';		


//get Reports from SalesForceReport
require_once 'salesforce/config.php';
require_once('salesforce/rest_functions.php');
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
$attachmentFolder = "523_BGAS";
$reportFolder = "523_BGAS";
$reportId = "00OU0000003EGUh"; //Invoicing/BGAS 523 Campaign Report

//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
if (!checkSession($instance_url, $reportId, $access_token)){
	session_start();
	$_SESSION['SalesForceReport'] = "DirectInstalls";
	$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
	echo "<script>window.location.replace('".AUTH_URL."');</script>";
	//echo AUTH_URL;
	//header('Location: '.AUTH_URL);
}else{					
	ob_start();
	
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
}//end if checkSession
	
	echo '<div class="tabs-content">';
		echo "<div id='Installs' class='tab-content'>";
			
			$reportId = "00OU0000002yJqM"; //Invoicing/810 Berkshire Gas Completed
			$reportName = "810 Berkshire Gas Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);
				$accountAddress[$AccountNameLink] = $address;
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$gpNames[$GPNameLink] = $greenProspectName;
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Location_Number__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$gpDateCompleted[$GPNameLink] = $completedDate;
				$metricName = $rowDetail["Metric__c.Name"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				$evaluationDate = $completedDate;
				$gpByCode[$GPNameLink] = 810;
				if (strtotime($evaluationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$completed[810][] = $rowDetail;
					$includedAccountIds[] = $AccountNameLink;
				}
			}
			$reportId = "00OU0000002yKcB"; //Invoicing/810 Berkshire Gas Completed
			$reportName = "811 Berkshire Gas Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);				
				$accountAddress[$AccountNameLink] = $address;
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$gpNames[$GPNameLink] = $greenProspectName;
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Location_Number__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$gpDateCompleted[$GPNameLink] = $completedDate;
				$metricName = $rowDetail["Metric__c.Name"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				$gpByCode[$GPNameLink] = 811;

				$evaluationDate = $completedDate;
				if (strtotime($evaluationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$completed[811][] = $rowDetail;
					$includedAccountIds[] = $AccountNameLink;
				}
			}
				
		
		//print_pre($campaignResultBreakdown["outreach"]);
		include_once('salesforce/soap_connect.php');
		//print_pre($completed);
		
		//get Contact Info
		$includedAccounts = implode("','",$includedAccountIds);
		$query = "SELECT Id, AccountId, LastName, FirstName, Name, Phone, Daytime_Phone__c FROM Contact WHERE AccountId IN ('".$includedAccounts."')";
		//echo $query;
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_pre($response);
		foreach ($response->records as $record){
			$contactLookup[$record->AccountId]["firstName"] = $record->FirstName;
			$contactLookup[$record->AccountId]["lastName"] = $record->LastName;
			$contactLookup[$record->AccountId]["phone"] = ($record->Phone ? : $record->Daytime_Phone__c);
		}
		//print_pre($contactLookup);

		
		foreach ($completed as $code=>$completedResults){
			foreach ($completedResults as $id=>$results){
				$accountId = $results["AccountNameLinkValue"];
				$accountName = $accountNames[$accountId];
				$idLookup = $results["GPNameLinkValue"];
				//get Contractor Names associated with the Green Prospects
				$query = "SELECT Contractor__c, Contractor_Contact_Info__c, Account__c FROM Upgrade__c where Id = '".$idLookup."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				foreach ($response->records as $record){
					if ($record->Contractor__c){
						$ContractorLookup[$idLookup]["Name"]=$record->Contractor__c;
						$ContractorLookup[$idLookup]["Info"]=$record->Contractor_Contact_Info__c;
					}
				}
				
				/*New Method for Attachments*/
				$query = "SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId='".$idLookup."'";
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->LinkedEntityId;
						$contentDocumentId = $record->ContentDocumentId;
						$query2 = "SELECT Id,Title,Description,FileType,FileExtension,VersionData FROM ContentVersion WHERE ContentDocumentId='".$contentDocumentId."'";
						$response2 = $mySforceConnection->query($query2); //sending request and getting response using SOAP
						foreach ($response2->records as $record2) {
							$name = str_replace(" ","_",$record2->Title);
							$fileType = $record2->FileType;
							$fileExtension = $record2->FileExtension;
							$versionData = $record2->VersionData;
							if (strpos(" ".$fileType,"EXCEL") && !strpos(strtolower($name),"screeningtool")){
								$thisAttachmentInfo = array("accountId"=>$accountId,"Id"=>$contentDocumentId,"code"=>$code,"Name"=>$name,"FileExtension"=>$fileExtension,"AccountName"=>$accountName,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
								$attachmentName = retrieve_attachmentName($thisAttachmentInfo);
								$thisAttachmentInfo["SavedName"] = $attachmentName;
								$destination = "salesforce/".$attachmentFolder."/".$attachmentName;
								//echo $destination."<br>";
								$file = fopen($destination, "w+");
								fputs($file, $versionData);
								fclose($file);
								$Attachments[$parentId][] = $thisAttachmentInfo;
								//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
								
							}
							
						}
						//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
					}
				}else{
					//$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
					//$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
					$GPLink = "";
					foreach ($parentIds as $GPLinks){
						$GPLink .= $GPNameLinksArray[$GPLinks];
					}
					$Adjustments["Alert"]["Missing Attachment"][] = $accountName." ".$GPLink;
					//echo $GPLink." missing Attachment<br>";
				}
				
			
				/*Old Method for Getting Attachments */
				/*
				//print_pre($ContractorLookup); //ARRAY Get Contractor Field name associated with Green Prospect based on parentIds
				//get attachments
				//echo $accountName." = ".$ids."<br>";
				$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId ='".$idLookup."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->ParentId;
						$name = $record->Name;
						$id = $record->Id;
						$fileParts = explode(".",$name);
						if (in_array("xlsx",$fileParts) && !strpos(strtolower($name),"screeningtool")){
							$Attachments[$parentId][] = array("accountId"=>$accountId,"Id"=>$id,"code"=>$code,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
						}
						//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
					}
				}else{
					//$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
					//$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
					$GPLink = "";
					foreach ($parentIds as $GPLinks){
						$GPLink .= $GPNameLinksArray[$GPLinks];
					}
					$Adjustments["Alert"]["Missing Attachment"][] = $accountName." ".$GPLink;
				}
				*/
				/*Old Method for Getting Attachments */

			}//end foreach completedResults
		}//end foreach completed

		//print_pre($AccountNamesLookup);
		//print_pre($ContractorLookup);
		//print_pre($Attachments);
		
		foreach ($Attachments as $parentId=>$attachmentList){
			foreach ($attachmentList as $attachmentInfo){
				if ($attachmentInfo["NoAttachments"]){
					$attachmentResults[$attachmentInfo["Name"]]["NoAttachments"] = true;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["HeatingFuel"] = $attachmentInfo["HeatingFuel"];
					$attachmentResults[$attachmentInfo["Name"]]["GreenProspect"] = $attachmentInfo["GreenProspect"];
					$attachmentResults[$attachmentInfo["Name"]]["parentId"] = $parentId;
				}else{
					$attachmentId = $attachmentInfo["Id"];
					//echo $instance_url." ".$attachmentInfo["Id"]." ".$attachmentInfo["Name"]."<Br>";
					$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
					//echo $attachment." just saved<br>";
					$attachmentResults[$parentId][$attachmentId]["SavedName"] = $attachment;
					$attachmentResults[$parentId][$attachmentId]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$parentId][$attachmentId]["FileName"] = $attachmentInfo["FileParts"][0];
					$attachmentResults[$parentId][$attachmentId]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
					$attachmentResults[$parentId][$attachmentId]["ActualFileName"] = $attachmentInfo["Name"];
					$attachmentResults[$parentId][$attachmentId]["ContractorName"] = $attachmentInfo["ContractorName"];
					$attachmentResults[$parentId][$attachmentId]["parentId"] = $parentId;
					$attachmentResults[$parentId][$attachmentId]["accountId"] = $attachmentInfo["accountId"];
					$attachmentResults[$parentId][$attachmentId]["accountType"] = $accountTypes[$parentId];
					$attachmentResults[$parentId][$attachmentId]["code"] = $attachmentInfo["code"];

					//print_r($attachmentResults); //ARRAY DESCRIBE results and details of downloaded files
				}
			}
		}
		//print_pre($attachmentResults);
		ob_flush();
		flush();
		
		foreach ($attachmentResults as $parentId=>$attachmentIdInfo){
			foreach ($attachmentIdInfo as $attachmentId=>$AttachmentSavedItem){
				$AttachmentSavedName = $AttachmentSavedItem["SavedName"];
				$AttachmentAccountName = $AttachmentSavedItem["AccountName"];
				$AccountId = $AttachmentSavedItem["accountId"];
				$parentId = $parentId;
				$NoAttachments = $AttachmentSavedItem["NoAttachments"];
				if (substr($AttachmentSavedName,-4)=="xlsx" && !strpos($AttachmentSavedName,"AccountList")){
					$ToParse[$AttachmentAccountName][$AttachmentSavedItem["ActualFileName"]] = $AttachmentSavedName;
					$ToParseByParentId[$parentId][$AccountId][$AttachmentSavedItem["ActualFileName"]] = $AttachmentSavedName;
				}
			}
		}
		//print_pre($ToParseByParentId);
		foreach ($ToParseByParentId as $parentId=>$accountIdInfo){
			foreach ($accountIdInfo as $accountId=>$actualFileNameInfo){
				foreach ($actualFileNameInfo as $actualFileName=>$fileName){
					include('salesforce/read_attachmentfile.php');
					$parsedRows[$accountId][$parentId][$fileName] = $rowsClean;
					$thisFileLocation = $siteRoot.$adminFolder."gbs/salesforce/".$attachmentFolder."/".$fileName;
					//echo "will unlink ".$thisFileLocation."<br>";
					//unlink($thisFileLocation);
					//print_r($parsedRows); //ARRAY DESCRIBE rows parsed into an array
				}
			}
		}
		ob_flush();
		flush();
		//print_pre($parsedRows);
		//create installation information records
		//print_pre($accountAddress);
		//print_pre($gpUtilityInfo);
		
		if (count($parsedRows)){
			foreach ($parsedRows as $accountId=>$parentIdInfo){
				$accountNameText = $accountNames[$accountId];
				$accountPhoneNumber = $accountPhone[$accountId];
				$address = $accountAddress[$accountId];
				$contactLastName = $contactLookup[$accountId]["lastName"];
				$contactFirstName = $contactLookup[$accountId]["firstName"];
				$contactPhone = $contactLookup[$accountId]["phone"];
				$contractor = "";

				foreach ($parentIdInfo as $gpLink=>$fileInfo){
					$thisAccountNumber = ($gpUtilityInfo[$gpLink]["utilityAccountNumber"] == "-" ? "" : $gpUtilityInfo[$gpLink]["utilityAccountNumber"]);
					$thisLocationID = ($gpUtilityInfo[$gpLink]["utilityLocationID"] == "-" ? "" : $gpUtilityInfo[$gpLink]["utilityLocationID"]);
					$installedItem = array();
					$totalHours = $gpTotalHours[$gpLink];
					$totalCost = $gpTotalCost[$gpLink];
					$code = $gpByCode[$gpLink];
					$incentiveDate = $gpDateCompleted[$gpLink];
					$contractor = ($ContractorLookup[$gpLink]["Name"] ? : "CET");
					$contractorInfo = ($ContractorLookup[$gpLink]["Info"] ? : "320 Riverside Dr. -1A");
					$contractorInfoParts = preg_split('/\r\n|\r|\n/', $contractorInfo);
					$contractorPhone = ($contractorInfoParts[2] ? : "413-586-7350");
					$contractorCityStateZip = explode(",",$contractorInfoParts[1]);
					$contractorCity = ($contractorCityStateZip[0] ? : "Florence");
					$contractorStateZip = explode(" ",$contractorCityStateZip[1]);
					$contractorState = ($contractorStateZip[0] ? : "MA");
					
					foreach ($fileInfo as $fileSavedName=>$fileRecords){
						$accountName = $accountInfoName[$accountId];
						$accountType = "";
						$GPLink = $GPNameLinksArray[$gpLink];
						foreach ($fileRecords as $fileRecord){
							$accountTypes = explode(" ",$fileRecord["accountType"]);
							$accountType = $accountTypes[0];
							$measure = $fileRecord["Measure Description"];
							$measure = (strpos(" ".strtolower($measure),"aerator") ? "Aerator" : $measure);
							$measure = (strpos(" ".strtolower($measure),"showerhead") ? "Showerhead" : $measure);
							$measure = str_replace("Building 36","",$measure);
							$qty = $fileRecord["Qty"];
							if ($fileRecord["Qty"]){
								$installedItem[] = $qty." ".$measure.($qty > 1 ? "s" : "");
								$installedItems = $qty." ".$measure;
								$measureName = $fileRecord["Measure Description"];
								$incentiveAmount = $fileRecord["Incentive/Unit"];
								$installAmount = $fileRecord["Cost/Unit"];
								$annualSavings = $fileRecord["Annual Therm Savings"];
								//."_".."x".$fileRecord["Qty"]."=".$fileRecord["Total Cost"];
								if ($installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"] && $fileRecord["Cost/Unit"] != $installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"]){
									$Adjustments["Alert"]["Installation Issue"][] = $accountName. " ".$GPLink." has ".$fileRecord["Measure Description"]." with Cost/Item at $".money($fileRecord["Cost/Unit"])." but previously captured at ".$contractor." $".money($installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"]);
								}
								if (strpos(" ".strtolower($fileRecord["Measure Description"]),"insulation") && $contractor == "No Contractor Indicated"){
									$Adjustments["Alert"]["Installation Issue"][] = $accountName. " ".$GPLink." has ".$fileRecord["Measure Description"]." installed with No Contractor Indicated with Cost/Item at $".money($fileRecord["Cost/Unit"])." <b>Item not included on Invoice</b>";
								}else{
									$installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Qty"] = $installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Qty"]+(int)$fileRecord["Qty"];
									$installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"] = $fileRecord["Cost/Unit"];
								}
								$thisResultArray = array(
									"utilityAccountNumber"=>$accountNumber,"utilityLocationID"=>$locationID,
									"accountId"=>$accountId,"accountName"=>$accountName,"accountPhoneNumber"=>$accountPhoneNumber,"GPLink"=>$gpLink,"GreenProspect"=>$GPLink,
									"code"=>$code,"incentiveDate"=>$incentiveDate,
									"contactFirstName"=>$contactFirstName,"contactLastName"=>$contactLastName,"contactPhone"=>$contactPhone,
									"street"=>$completedAccountDetails[$accountId]["Account.BillingStreet"],"city"=>$completedAccountDetails[$accountId]["Account.BillingCity"],
									"dwellingUnits"=>$dwellingUnits[$gpLink],"measureName"=>$measureName,"qty"=>$fileRecord["Qty"],
									"contractor"=>$contractor,"contractorInfo"=>$contractorInfo,"contractorPhone"=>$contractorPhone,"contractorAddress"=>$contractorInfoParts[0],"contractorCity"=>$contractorCity,"contractorState"=>$contractorState,
									"incentiveAmount"=>$incentiveAmount,"installAmount"=>$installAmount,"annualSavings"=>$annualSavings,"totalHours"=>$totalHours,"totalCost"=>$totalCost,
									"installedItems"=>$installedItems,"accountType"=>$accountType,
									"rowDetails"=>$fileRecords);
								if ($contractor == "CET"){
									$source = "BGAS";
									$itemsByType[$source][$measureName] = $itemsByType[$source][$measureName]+$fileRecord["Qty"];

									$campaignResultBreakdown["installation"][] = $thisResultArray;
									$campaignResultBreakdownByAccounts[$code][$accountId]=1;
									$campaignResultBreakdownByCode["installation"][$code][$accountId][] = $thisResultArray;
									$campaignResultBreakdownByCodeCount["installation"][$code]++;
								}

							}//end if fielRecord[Qty]
							//set totalHours to zero so only shows up on first GP for the account
							$totalHours = "n/a";
							$totalCost = "0.00";
						}//end foreach fileRecords
					}//end foreach gpLinkInfo
				}//end foreach fileInfo
			}//end foreach parsedRows
		}//end if parsedRows
		
		//include_once('salesforce/reports/reportBG_MF+CI_Cost&Savings.php');
		
		
		//CMA Data
			$reportId = "00OU0000002y9Nv"; //Invoicing/525 Complete Report
			$reportName = "525 Complete Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = str_replace("&",",",$rowDetail["Upgrade__c.Name"]);
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$number = ($number == "-" ? "" : str_replace(",","",$number));
				$GPPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$dateInvoiced = $rowDetail["Upgrade__c.Date_Invoiced__c"];
				//[Metric__c.Unit__c] => Therms
				//[Upgrade__c.Current_Status_Detail__c
				//only use 525D Gas DI types
				if ($GPType == "525D Gas DI"){
					//only use stage completed in this invoice period
					if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						//only use items not already invoiced
							$greenProspectNameParser = explode("(",$greenProspectName);
							$nameQualifier = $greenProspectNameParser[0];
							if ($accountNames[$accountName]>1){
								$accountName = $accountName." - ".str_replace("-DI","",$nameQualifier);
							}
							if (trim($dateInvoiced) != "-"){							
								$Adjustments["Alerts"]["Item in Invoice Period but already marked with Invoice Date<br>".$reportLink][] = $accountName." ".$GPNameHref."<br>Stage Completed: ".$completedDate." Invoiced Date:".$dateInvoiced;
							}
							$accountName = strtotime($completedDate)."_".$accountName;
							$installItems = $greenProspectNameParser[1];
							$installItemsParser = explode(",",$installItems);
							//
							//get the metrics for this green prospect
							$query = "SELECT Name, Number__c, Unit__c, Material__c, Unit_Value__c, Savings__c, Funding_Source__c, Annual_Savings_Resulting_From_Mea__c FROM Metric__c WHERE Green_Prospect__c ='".$GPNameLink."'";
							//echo $query;
							$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
							//print_pre($response);
							$hasTherms = false;
							if (count($response->records)){
								foreach ($response->records as $record) {
									$metricNumber = $record->Number__c;
									$metricType = strtolower($record->Unit__c);
									if ($metricType == "therms"){$hasTherms = true;}
									$metricType = (strpos(" ".$metricType,"hand held") ? "HH" : $metricType);
									$metricType = (strpos(" ".$metricType,"aerator") ? "A" : $metricType);
									$metricType = (strpos(" ".$metricType,"shower head") ? "SH" : $metricType);
									$metricType = (strpos(" ".$metricType,"spray") ? "SPRY" : $metricType);
									//print_pre($record);
									$source = "CMA";
									$items[$accountName][$metricType] = $metricNumber;
									$itemsByType[$source][$metricType] = $itemsByType[$source][$metricType]+$metricNumber;
									$totalItemsInstalled[$metricType] = $totalItemsInstalled[$metricType]+$metricNumber;
								}
								if (!$hasTherms){
									$Adjustments["Alerts"]["Therms indicated but no other installed measures"][] = $AccountNameHref." [".$GPNameHref."]";
								}
							}

							//check for consistency
							foreach ($installItemsParser as $installedItem){
								preg_match_all('/(\d)|(\w)/', $installedItem, $matches);
								if ($items[$accountName][implode($matches[2])] && $items[$accountName][strtoupper(implode($matches[2]))] != implode($matches[1])){
									$Adjustments["Alerts"]["Measure metrics do not match from Green Prospect Name"][] = $AccountNameHref." Metric for ".implode($matches[2])."=".$items[$accountName][implode($matches[2])]." <> [".$GPNameHref."]";
								}
								if (implode($matches[1]) && !$items[$accountName][strtoupper(implode($matches[2]))]){
									$Adjustments["Alerts"]["Measure in Green Prospect Name not found in Metrics"][] = $AccountNameHref." [".$GPNameHref."] missing metric for ".implode($matches[2])."=".implode($matches[1]);
								}
							}
					
					}
				}
			}
		//End CMA Data 
		ksort($itemsByType);
//		print_pre($itemsByType);
		foreach ($itemsByType as $source=>$typeInfo){
			foreach ($typeInfo as $typeName=>$qty){
				if ($standardizedNames[$typeName]["EFI"]){
					$itemsByEFI[$source][$standardizedNames[$typeName]["EFI"]] = $qty;
				}
			}
		}
		ksort($itemsByEFI);
		$directInstallTable =  "<table class='simpleTable'><thead><tr><th>Source</th><th>Measure</th><th>EFI</th><th>Qty</th></tr></thead>";
		$directInstallTable .= "<tbody>";
		foreach ($itemsByEFI as $source=>$efiInfo){
			foreach ($efiInfo as $EFI=>$qty){
				$directInstallTable .= "<tr><td>".$source."</td><td>".$standardizedEFI[$EFI]["Description"]."</td><td>".$EFI."</td><td>".$qty."</td></tr>";
			}
		}
		$directInstallTable .= "</tbody></table>";
		include_once('salesforce/reports/reportDirectInstall.php');
		echo $directInstallFileLink;
		echo $directInstallTable;
		
		echo "</div>";//end CampaignReport tab-content

		include_once('_adjustmentsTab.php');
		
	echo "</div>"; //end tabs-content
?>
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li>1. Down load <?php echo $directInstallFileLink;?>
	
</ul>

<script type="text/javascript">
	$(function () {
		$("#invoiceDate").html('<?php echo date("F Y", strtotime($invoiceDate));?>');
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
		
	});
</script>