<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<?php
$_SESSION['SalesForceReport'] = "ColumbiaGas";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$paginationResult = $GBSProvider->invoiceTool_getProductInfo();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	//print_pre($record);
	if ($record->MeasureName){
		if (strpos($record->MeasureName,",")){
			$measureNames = explode(",",$record->MeasureName);
		}else{
			$measureNames = array($record->MeasureName);
		}
		foreach ($measureNames as $measureName){
			$standardizedNames[$measureName]["EFI"] = $record->EFI;
			$standardizedNames[$measureName]["Description"] = $record->Description;
			$standardizedNames[$measureName]["Cost"] = $record->Cost;
		}
		$standardizedNames[$record->BGasName]["EFI"] = $record->EFI;
		$standardizedNames[$record->BGasName]["Description"] = $record->Description;
		$standardizedNames[$record->BGasName]["Cost"] = $record->Cost;
		$standardizedEFI[$record->EFI]["Description"] = $record->Description;
		$standardizedEFI[$record->EFI]["Cost"] = $record->Cost;
		$standardizedEFI[$record->EFI]["MeasureName"] = $record->MeasureName;
	}
}



$billingRatesArray = array("ColumbiaGas"=>array("525J"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
			$BillingCategoryByEmployee[$Employee] = $record->GBSBillingRate_Name;
		}
	}
}
//print_pre($BillingRateByCategory);
// comment the above line back out 

$setTab = "Hours";
	if ($isStaff){
		$TabCodes = array("Pipeline"=>"Invoice","Adjustments"=>"Adjustments");
		$setTab = "Invoice";
		echo "<h3>This tool will create the Columbia Gas Pipeline and Report files for ".date("F Y",strtotime($invoiceDate))."</h3>";

	}else{
		$TabCodes = array("Hours"=>"Hours","525 Invoice"=>"Invoice","525DI Invoice"=>"InvoiceDI","Adjustments"=>"Adjustments","Notes"=>"Notes");
		echo "<h3>This tool will create the Columbia Gas Invoice files</h3>";
	}
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Columbia Gas";
			$SelectedGroupsID = 7; //GreenTeam
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					//Since TRC is not billed by hours capture all codes to determine ratio
					$revenueCodeTotalHours["525Fee"] = bcadd($revenueCodeTotalHours["525Fee"],$hour,2);
					$revenueCodesByEmployee["525Fee"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee["525Fee"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$hour,2);
					$revenueCodeTotalHours["525Incentive"] = bcadd($revenueCodeTotalHours["525Incentive"],$hour,2);
					$revenueCodesByEmployee["525Incentive"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee["525Incentive"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$hour,2);
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if ($BillingCategory == "Admin Staff" && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate".$InvoiceCode][]=str_replace("_",", ",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once $rootFolder.$adminFolder.'gbs/salesforce/config.php';
			require_once($rootFolder.$adminFolder.'gbs/salesforce/soap_connect.php');
			require_once($rootFolder.$adminFolder.'gbs/salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00OU0000003eEB8"; //Invoicing/525 Completed Report
			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
			$reportId = "00OU0000003eEB8"; //Invoicing/525 Completed Report
			$reportName = "525 Completed Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
			
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$applicationNumber = $rowDetail["Upgrade__c.Application_Number__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$number = $rowDetail["Metric__c.Number__c"];
				$number = ($number == "-" ? "" : str_replace(",","",$number));
				$AccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$billingCity = $rowDetail["Account.BillingCity"];
				$GPPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$stageCompleted = strtotime($rowDetail["Upgrade__c.GP_Stage_Completed__c"]);
				if (!$AccountNumber){
					$Adjustments["Alert"]["Completed Account missing Account Number"][] = $GPNameHref;
				}
				//only add completed items with Number metric
				if ($number){
					$completedInfo = array("Name"=>$accountName,"AccountNumber"=>$AccountNumber,"City"=>$billingCity,"GPName"=>$greenProspectName,"Phase"=>$GPPhase,"Number"=>$number,"ApplicationNumber"=>$applicationNumber,"stageCompleted"=>$stageCompleted,"GPLink"=>$GPNameLink);
					$pipelineItems[$GPPhase][] = $completedInfo;
				}
				
			}
			
			$reportId = "00O0P000004JLcOUAW"; //Invoicing/525 Pipeline Report by Phase
			$reportName = "525 Pipeline Report by Phase Invoicing";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			//echo '<pre>'; print_r($reportResults); echo '</pre>';
			
			//print_r($reportResults);
			//echo $reportRowsSorted;
			
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
			
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				//$AccountNameLink = $rowDetail["Account.Id"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$applicationNumber = $rowDetail["Upgrade__c.Application_Number__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$number = $rowDetail["Metric__c.Number__c"];
				$number = ($number == "-" ? "" : str_replace(",","",$number));
				$AccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$billingCity = $rowDetail["Account.BillingCity"];
				$GPPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				//below, if GP Phase is Implementation in Progress, return installation in progress, otherwise return gp phase
				$GPPhase = ($GPPhase == "Implementation-In-Progress" ? "Installation-In-Progress" : $GPPhase);
				$completedInfo = array("Name"=>$accountName,"AccountNumber"=>$AccountNumber,"City"=>$billingCity,"GPName"=>$greenProspectName,"Phase"=>$rowDetail["Upgrade__c.Green_Prospect_Phase__c"],"Number"=>$number,"ApplicationNumber"=>$applicationNumber);
				$pipelineItems[$GPPhase][] = $completedInfo;
				//print_r($completedInfo);
				//var_dump($reportRowsSorted);
				//only add completed items with Number metric
				}
			//echo $GPPhase."=".count($pipelineItems[$GPPhase])."<br><hr>";
			//echo '<pre>'; print_r($reportResults); echo '</pre>';
			
			//Contacted reports:
			$reportId = "00OU0000002y907"; //Invoicing/525 Accounts Contacted
			$reportName = "525 Accounts Contacted";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			//print_pre($reportRowsSorted);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$CustomerReport["ContactedYTD"][$AccountNameLink] = 1; // = 1;
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$CustomerReport["ContactedMTD"][$AccountNameLink] = 1; // = 1;
				}

			}
			
			//echo "sum of account links = " . array_sum($CustomerReport["ContactedMTD"]) . "\n";
			//echo count($CustomerReport["ContactedMTD"]);
			//print_pre($CustomerReport["ContactedMTD"]);
			
			$reportId = "00OU0000002y90WMAQ"; //Invoicing/525 Walkthrough
			$reportName = "525 Walkthrough";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$accountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$CustomerReport["WalkthroughYTD"][$AccountNameLink]=1; // = 1;
				$lastActivityDate = $rowDetail["Upgrade__c.LastActivityDate"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$applicationNumber = ($rowDetail["Upgrade__c.Application_Number__c"] =="-" ? "" : $rowDetail["Upgrade__c.Application_Number__c"]);
				$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$secondaryCampaign = ($rowDetail["Upgrade__c.Secondary_Campaign_Association__c"] == "-" ? "" : $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"]);
				$leadSource = ($secondaryCampaign == "525 Columbia Gas" ? "" : $secondaryCampaign);
				$status = ($rowDetail["Upgrade__c.Green_Prospect_Status__c"] == "-" ? "" : $rowDetail["Upgrade__c.Green_Prospect_Status__c"]);
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$CustomerReport["WalkthroughMTD"][$AccountNameLink]=1; // = 1;
					$CMAReport["Walkthrough"][] = array("date"=>$completedDate,"accountName"=>$accountName,"accountNumber"=>$accountNumber,"visionID"=>$applicationNumber,"primaryCampaign"=>$primaryCampaign,"secondaryCampaign"=>$secondaryCampaign,"status"=>$status,"lastActionDate"=>$lastActionDate,"leadSource"=>$leadSource);
				}
			}
			//print_pre($CMAReport["Walkthrough"]);
			//print_pre($CustomerReport["WalkthroughYTD"]);

			//print_pre($CustomerReport["ContactedMTD"]);
			//echo "Walkthrough MTD from CMA Report: ";
			//echo count($CMAReport["Walkthrough"]);
			//echo "  Contacted MTD from customer report: ";
			//echo count($CustomerReport["ContactedMTD"])."<br>";
			//echo "  Contacted YTD from customer report: ";
			//echo count($CustomerReport["ContactedYTD"])."<br>";
			//echo " Walkthrough YTD: <br>";
			//echo count($CustomerReport["WalkthroughYTD"])."<br>";
			//echo "  Walkthrough MTD ";
			//echo count($CustomerReport["WalkthroughMTD"])."<br>";
			//var_dump ($CustomerReport);
			//echo "<br>";
			//var_dump ($CMAReport);
			//echo "<br>";
			
			/*Deprecated as of 8/10/2018 by Heather
			$reportId = "00OU0000002y90b"; //Invoicing/525 No Interest/No Op
			$reportName = "525 No Interest/No Op";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
			
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$CustomerReport["NoInterest"][$AccountNameLink] = 1;
			}
			*/
			
			/*added 8/10/2018 by Heather*/
			$reportId = "00O0P000003yXlTUAU"; //CMA/525 Events Report
			$reportName = "525 Events Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				$dueDate = $rowDetail["DUE_DATE"];
				$primaryCampaign = $rowDetail["Activity.Primary_campaign__c"];
				//$primaryCampaign = $rowDetail["Activity.Primary_Campaign__c.Name"];
				$secondaryCampaign = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				$contractorsEngaged = ($rowDetail["Activity.of_contractors_engaged__c"] == "-" ? "None Noted" : $rowDetail["Activity.of_contractors_engaged__c"]);
				$subject = $rowDetail["SUBJECT"];
				$includeInCount = false;
				if (strpos(" ".$primaryCampaign,"525")){$includeInCount = true;}
				if (strpos(" ".$secondaryCampaign,"525")){$includeInCount = true;}
				
				if ($includeInCount){
					$contractorEngagement["eventsAttendedYTD"]++; //=1
					if (strtotime($dueDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($dueDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$contractorEngagement["eventsAttendedMTD"]++; //=1
						$CMAReport["events"][] = array("date"=>$dueDate,"subject"=>$subject,"numberOfContractors"=>$contractorsEngaged);
					}
				}
				
			}
			
			
			$reportId = "00O0P000003yXjwUAE"; //CMA/525 Events Report
			$reportName = "525 Engagement/Event Meeting-invoicing";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c"];
				$secondaryCampaign = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$contractorsEngaged = ($rowDetail["Upgrade__c.of_contractors_engaged__c"] == "-" ? "Not Noted" : $rowDetail["Upgrade__c.of_contractors_engaged__c"]);
				$subject = $rowDetail["CUST_NAME"];
				$currentStatusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$includeInCount = false;
				if (strpos(" ".$primaryCampaign,"525")){$includeInCount = true;}
				if (strpos(" ".$secondaryCampaign,"525")){$includeInCount = true;}
				if ($includeInCount){
					$contractorEngagement["eventsAttendedYTD"]++;
					if (strtotime($initiationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($initiationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$contractorEngagement["eventsAttendedMTD"]++;
						$CMAReport["events"][] = array("date"=>$initiationDate,"subject"=>$subject,"numberOfContractors"=>$contractorsEngaged,"notes"=>$currentStatusDetail);
					}
				}
				
			}
			
			// 00O0P000003phm5  525 Contractor Engaged/Trained CY2018
			$reportId = "00O0P000003phm5UAA"; //CMA/525 Contractor Engaged/Trained CY2018
			$reportName = "525 Contractor Engaged/Trained CY2018";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Name",SORT_ASC);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$contractor = ($rowDetail["Upgrade__c.Contractor__c"] == "-" ? "Not Indicated" : $rowDetail["Upgrade__c.Contractor__c"]);
				$howDidWeEngage = ($rowDetail["Upgrade__c.How_did_we_engage_contractor__c"] == "-" ? "Not Recorded" : $rowDetail["Upgrade__c.How_did_we_engage_contractor__c"]);
				$meetingDate = $rowDetail["Upgrade__c.Meeting_date__c"];
				$cmaIncentive = ($rowDetail["Upgrade__c.CMA_Incentive_Training__c"] == "-" ? "Not Captured" : $rowDetail["Upgrade__c.CMA_Incentive_Training__c"]);
				$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c"];
				$secondaryCampaign = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$currentStatusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$includeInCount = true;
				//$includeInCount = false;
				//if (strpos(" ".$primaryCampaign,"525")){$includeInCount = true;}
				//if (strpos(" ".$secondaryCampaign,"525")){$includeInCount = true;}
				
				
				//changed $initiationDate to $meetingDate below
				if ($includeInCount){
					$contractorEngagement["engagedTrainedYTD"]++;
					if (strtotime($meetingDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($meetingDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$contractorEngagement["engagedTrainedMTD"]++;
						$initiationDate = ($initiationDate == "-" ? "" : date("m/d/Y",strtotime($initiationDate)));
						$meetingDate = ($meetingDate == "-" ? "" : date("m/d/Y",strtotime($meetingDate)));
						$CMAReport["contractorEngaged"][] = array("date"=>$meetingDate,"contractor"=>$contractor,"howDidWeEngage"=>$howDidWeEngage,"meetingDate"=>$meetingDate,"cmaIncentive"=>$cmaIncentive,"notes"=>$currentStatusDetail);
					}
				}
				
			}
			//echo count($includeInCount); //1?
			//print_pre($CMAReport["contractorEngaged"]); //empty
			//echo count($CMAReport["contractorEngaged"]);
			//print_pre($contractorEngagement["eventsAttendedMTD"]); //0
			//echo count($contractorEngagement["eventsAttendedMTD"]); //empty 
			//Get Bullets
			$reportId = "00OU0000003e65BMAQ"; //Invoicing/Monthly Report Bullets
			$reportName = "Monthly Report Bullets";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			// comment the above line back out
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				if (date("m/d/Y",strtotime($completedDate)) != "12/31/1969"){
					$stageCompleted = " [Completed: ".date("n/d/Y",strtotime($completedDate))."]";
				}else{
					$stageCompleted = " [Not Yet Completed]";
				}
				$StatusDetails = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				if ($GPType == "525 Engagement - Event/Meeting" || $GPType == "525 Outreach" && trim($StatusDetails)){
					//if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						
						$Bullets[]=array("accountName"=>$accountName.$stageCompleted,"status"=>str_replace("<br>","",$StatusDetails));
						$Adjustments["Review"]["Bullet with GPType '525 Engagemet - Event/Meeting' or '525 Outreach'"][] = "[<b>".$accountName."</b> ".$GPNameHref.$stageCompleted."] ".str_replace("<br>","",$StatusDetails);
					//}
				}
			}
			
			$reportId = "00O0P000002yUmNUAU"; //Invoicing/525 Engineer Time Cards
			$reportName = "525 Engineer Time Cards";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$timeCardCreatedBy = $rowDetail["Time_Card__c.CreatedBy.Name"];
				$timeCardHours = $rowDetail["Time_Card__c.Hours_Worked__c"];
				$timeCardDate = $rowDetail["Time_Card__c.Date_Worked__c"];
				$employeeNameParts = explode(" ",$timeCardCreatedBy);
				$Employee = $employeeNameParts[1]."_".$employeeNameParts[0];
				$billingCategory = $BillingCategoryByEmployee[$Employee];
				$billingRate = $BillingRateByEmployee[$Employee]["ColumbiaGas"][$billingCategory];
				if (!$billingCategory){
					$Adjustments["Alert"]["No Billing Rate for Employee"][] = $timeCardCreatedBy;
				}

				if (strtotime($timeCardDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($timeCardDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink] = bcadd($timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink],$timeCardHours,2);
					$timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate] = bcadd($timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate],$timeCardHours,2);
					$timeCardsHoursByRate[$billingRate][$accountName]=bcadd($timeCardsHoursByRate[$billingRate][$accountName],$timeCardHours,2);
					$timeCardsHoursCount[$billingRate."_".$accountName]=1;
					$timeCardsByCategory[$billingCategory] = bcadd($timeCardsByCategory[$billingCategory],$timeCardHours,2);
					$timeCardsByEmployee[$Employee] = bcadd($timeCardsByEmployee[$Employee],$hours,2);
					$timeCards[$timeCardCreatedBy][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameHref,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"dateWorked"=>$timeCardDate,"billingCategory"=>$billingCategory,"billingRate"=>$billingRate);
				}
			}
			//print_pre($timeCards);		
			foreach ($timeCardsByEmployee as $Employee=>$hours){
				if ($InvoiceHourData["525J"][$Employee] != $hours){
					$Adjustments["Alert"]["Mismatch Hours between Salesforce and CET Hours"][] = $Employee." SF hours=".$hours." CET Hours=".$InvoiceHourData["525J"][$Employee];
				}
				
			}
			
			include_once('salesforce/reports/reportColumbiaGasPipeline.php');
			echo $ReportsFileLink;
			
			//print_pre($pipelineItems);
		echo "</div>";
		echo "<div id='InvoiceDI' class='tab-content'>";

		
			$reportId = "00OU0000002y9NvMAI"; //Invoicing/525 Complete Report
			$reportName = "525 Complete Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
					$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
					$accountNames[$accountName]++;
			}
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = str_replace("&",",",$rowDetail["Upgrade__c.Name"]);
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$number = ($number == "-" ? "" : str_replace(",","",$number));
				$AccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$billingAddress = $rowDetail["Account.BillingAddress"];
				$addressParts = explode(",",$billingAddress);
				$street = $addressParts[0];
				$billingCity = $rowDetail["Account.BillingCity"];
				$billingZip = $rowDetail["Account.BillingPostalCode"];
				$GPPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$dateInvoiced = $rowDetail["Upgrade__c.Date_Invoiced__c"];
				//[Metric__c.Unit__c] => Therms
				//$GPStatus = [Upgrade__c.Current_Status_Detail__c
				
				//only use 525D Gas DI or 525D CMA Small Business DI types - add additional types with: OR $GPType == "XYZ"
				if ($GPType == "525D Gas DI" OR $GPType == "525D CMA Small Business DI" OR $GPType == "525D CMA Greater Lawrence DI"){
					//only use stage completed in this invoice period
					if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						//only use items not already invoiced
							$greenProspectNameParser = explode("(",$greenProspectName);
							$nameQualifier = $greenProspectNameParser[0];
							if ($accountNames[$accountName]>1){
								$accountName = $accountName." - ".str_replace("-DI","",$nameQualifier);
							}
							if (trim($dateInvoiced) != "-"){							
								$Adjustments["Alerts"]["Item in Invoice Period but already marked with Invoice Date<br>".$reportLink][] = $accountName." ".$GPNameHref."<br>Stage Completed: ".$completedDate." Invoiced Date:".$dateInvoiced;
							}
							$accountName = strtotime($completedDate)."_".$accountName;
							$installItems = $greenProspectNameParser[1];
							$items[$accountName]["actual"] = $installItems;
							$items[$accountName]["accountNumber"] = $AccountNumber;
							$items[$accountName]["address"] = $street;
							$items[$accountName]["city"] = ucwords(strtolower($billingCity));
							$items[$accountName]["zip"] = substr($billingZip,0,5);
							$items[$accountName]["installDate"] = date("n/d/Y",strtotime($completedDate));
							$installItemsParser = explode(",",$installItems);
							//
							//get the metrics for this green prospect
							$query = "SELECT Name, Number__c, Unit__c, Material__c, Unit_Value__c, Savings__c, Funding_Source__c, Annual_Savings_Resulting_From_Mea__c FROM Metric__c WHERE Green_Prospect__c ='".$GPNameLink."'";
							//echo $query;
							$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
							//print_pre($response);
							$hasTherms = false;
							if (count($response->records)){
								foreach ($response->records as $record) {
									$metricNumber = $record->Number__c;
									$metricType = strtolower($record->Unit__c);
									if ($metricType == "therms"){$hasTherms = true;}
									$metricType = (strpos(" ".$metricType,"hand held") ? "HH" : $metricType);
									$metricType = (strpos(" ".$metricType,"aerator") ? "A" : $metricType);
									$metricType = (strpos(" ".$metricType,"shower head") ? "SH" : $metricType);
									$metricType = (strpos(" ".$metricType,"spray") ? "SPRY" : $metricType);
									//print_pre($record);
									$items[$accountName][$metricType] = $metricNumber;
									$totalItemsInstalled[$metricType] = $totalItemsInstalled[$metricType]+$metricNumber;
									$source = "CMA DI";
									$itemsByType[$source][$metricType] = $itemsByType[$source][$metricType]+$metricNumber;
									
								}
								if (!$hasTherms){
									$Adjustments["Alerts"]["Therms indicated but no other installed measures"][] = $AccountNameHref." [".$GPNameHref."]";
								}
							}

							//Deprecated 2018-07-24 with CMA meeting with Paulina
							/*
							//check for consistency
							foreach ($installItemsParser as $installedItem){
								preg_match_all('/(\d)|(\w)/', $installedItem, $matches);
								if ($items[$accountName][implode($matches[2])] && $items[$accountName][strtoupper(implode($matches[2]))] != implode($matches[1])){
									$Adjustments["Alerts"]["Measure metrics do not match from Green Prospect Name"][] = $AccountNameHref." Metric for ".implode($matches[2])."=".$items[$accountName][implode($matches[2])]." <> [".$GPNameHref."]";
								}
								if (implode($matches[1]) && !$items[$accountName][strtoupper(implode($matches[2]))]){
									$Adjustments["Alerts"]["Measure in Green Prospect Name not found in Metrics"][] = $AccountNameHref." [".$GPNameHref."] missing metric for ".implode($matches[2])."=".implode($matches[1]);
								}
							}
							*/
					
					}
				}
			}
			ksort($items);
				
			//itemsByType used for inventoryTracking
			ksort($itemsByType);
			print_pre($itemsByType);
			foreach ($itemsByType as $source=>$typeInfo){
				foreach ($typeInfo as $typeName=>$qty){
					if ($standardizedNames[$typeName]["EFI"]){
						$itemsByEFI[$source][$standardizedNames[$typeName]["EFI"]] = $qty;
					}
				}
			}
			ksort($itemsByEFI);
				
				
			include_once('salesforce/reports/reportColumbiaGasDI.php');
			echo $ReportsFileLink;
		
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "Columbia Gas");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the Files and review 525 Report Bullets.
	<li>4. Adjust Therms bonus data or delete if necessary.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>