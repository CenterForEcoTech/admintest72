<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HoursProvider.php");
$gbsProvider = new GBSProvider($dataConn);
$criteria = new stdClass();
$criteria->includeAllStatus = true;
$paginationResult = $gbsProvider->invoiceTool_getStaticBullets($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);
foreach ($resultArray as $result=>$record){
	$id = $record->GBSBullet_ID;
	$displayText = str_replace("  "," ",trim($record->GBSBullet_Content));
	$reportName = $record->GBSBullet_Report;
	$invoiceCode = $record->GBSBullet_InvoiceCode;
	$status = $record->GBSBullet_Status;
	$bulletsById[$id] = array("id"=>$id,"displayText"=>$displayText,"reportName"=>$reportName,"invoiceCode"=>$invoiceCode,"status"=>$status);
	$bulletsByReportName[$reportName][] = array("id"=>$id,"displayText"=>$displayText,"reportName"=>$reportName,"invoiceCode"=>$invoiceCode,"status"=>$status);
	$ReportNames[$reportName] = 1;
}
//print_pre($ReportNames);

$gbsProvider = new HoursProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$CodesActive[$record->name]=$record;
	}
}


$SelectedID = $_GET['ID'];
$addBullet = $_GET['addBullet'];

?>
<style>
.displayTextDiv {border:1pt solid black; margin:5pt;float:left;padding:5pt;cursor:pointer;}
.bulletDisplayText {min-width:300px;cursor:pointer;}
.selectedBulletDisplayText {border:1pt solid #CCCC00;background-color:#E6E600;}
</style>
	<?php if ($addBullet || $SelectedID){?>
		<form id="BulletUpdateForm" class="basic-form override_skel">
			<fieldset class="row">
				<legend>Static Report Bullets</legend>
				<div class="fifteen columns">
					<div style="display:none;">
						ID<?php if (!$addBullet){?><input type="text" name="id" value="<?php echo $bulletsById[$SelectedID]["id"];?>"><?php }?>
					</div>
					<div class="fourteen columns">
						<br clear="both">
						<div class="three columns">Report Name:</div>
							<div class="ten columns">
								<input type="hidden" style="width:100px;" id="reportName" name="reportName" value="<?php echo $SelectedReportName=$bulletsById[$SelectedID]["reportName"];?>">
								<?php 
									$codesComboBoxFunction = "";
									include('_bulletReportName_ComboBox.php');
								?>
							</div>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Invoice Code:</div>
							<div class="ten columns">
								<input type="hidden" style="width:100px;" id="invoiceCode" name="invoiceCode" value="<?php echo $SelectedInvoiceCode=$bulletsById[$SelectedID]["invoiceCode"];?>">
								<?php 
									$codesComboBoxFunction = "
									var invoiceCodeText = $(ui.item).text();
										invoiceCodeSplit = invoiceCodeText.split(\" \");
									$('#invoiceCode').val(invoiceCodeSplit[0]);";
									$codesComboBoxHideLabel = true;
									include('_CodesComboBox.php');
								?>
							</div>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Display Text:</div>
							<div class="ten columns">
								<textarea name="displayText" id="displayText" style="width:100%;height:200px;"><?php echo $bulletsById[$SelectedID]["displayText"];?></textarea>
							</div>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Status:</div>
							<div class="ten columns">
								<select name="status" id="status">
									<option value="Active"<?php echo ($bulletsById[$SelectedID]["status"] == "Active" ? " selected" : "");?>>Active</option>
									<option value="InActive"<?php echo ($bulletsById[$SelectedID]["status"] == "InActive" ? " selected" : "");?>>InActive</option>
								</select>
							</div>
							
					</div>
				</div>
			</fieldset>
			<div id="codeResults" class="fifteen columns"></div>
			<?php if (!$ReadOnlyTrue){?>
				<div class="fifteen columns">
					<a href="?nav=manage-bullets&ID=<?php echo $SelectedID;?>" id="cancel" class="button-link">Cancel</a>
					<?php if ($addBullet){?>
						<a href="save-code-add" id="save-code-add" class="button-link do-not-navigate">+Add</a>
						<input type="hidden" name="action" value="bullet_add">
					<?php }else{?>
						<a href="save-code-update" id="save-code-update" class="button-link do-not-navigate">Save</a>
						<input type="hidden" name="action" value="bullet_update">
					<?php }?>

				</div>
				<?php if (!$addBullet){?>
					<div class="fifteen columns">
						<a href="?nav=manage-bullets&addBullet=true" class="button-link" style="float:right;">+ Add New Static Bullet</a>
					</div>
				<?php }?>
			<?php }//if not read only ?>
		</form>
<?php }//end if $addBullet || $SelectedID ?>
				<?php if (!$addBullet && !$SelectedID){?>
					<div class="fifteen columns">
						<?php if (!$ReadOnlyTrue){?>
							<a href="?nav=manage-bullets&addBullet=true" class="button-link" style="float:right;">+ Add New Static Bullet</a>
						<?php }//end ReadOnly ?>
					</div>
<?php }?>
<br clear="all">
<br clear="all">
<div class="row">
	<div class="sixteen columns">
		<table class="BulletTemplates">
			<thead>
				<tr>
					<th nowrap=nowrap>InvoiceCode ReportName</th>
					<th>DisplayText</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				//print_pre($bulletsByReportName);
				foreach ($bulletsByReportName as $reportName=>$reportCount){
					foreach ($reportCount as $bulletInfo){
				?>
					<tr>
						<td valign="top" nowrap=nowrap><?php echo $bulletInfo["invoiceCode"]." ".$reportName;?></td>
						<td valign="top" class="bulletDisplayText" data-bulletid="<?php echo $bulletInfo["id"];?>"><?php echo $bulletInfo["displayText"];?></td>
						<td valign="top"><?php echo $bulletInfo["status"];?></td>
					</tr>
				<?php }
				}?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(function(){
		var tableBulletTemplates = $('.BulletTemplates').DataTable({
			//dom: 'T<"clear">lfrtip',
			//tableTools: {
			//	"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			//},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "asc"]],
			"bPaginate": true,
			"iDisplayLength": 500,
			"aoColumnDefs": [
                { "bWidth": '200px', "aTargets": [0] }
            ]
		});
		new $.fn.dataTable.FixedColumns( tableBulletTemplates );

		$(".bulletDisplayText").on('click',function(){
			//console.log(this);
			var $this = $(this);
			if ($this.attr('data-bulletid')){
				window.location = '?nav=manage-bullets&ID='+$this.attr('data-bulletid');
			}
		});
		
		$('select').css('height','22px');
		var dropdownlist = $('select');
		dropdownlist.css('width','auto');
		$('input[type="text"]').css('height','22px');
		$('input[type="checkbox"]').css('width','13px');
		$(".input_text_tiny").css({'width':'25px','height':'20px'});
		$(".input_text_small").css({'width':'45px','height':'20px'});
		
		 var formElement = $("#BulletUpdateForm");
		 
		 formElement.validate({
			rules: {
				reportName: {required: true},
				invoiceCode: {required: true},
				displayText: {required: true}
			}
		});
		$("#cancel").click(function(e){
			e.preventDefault();
			parent.history.back(); 
			return false;							   
		});
		
			$("#save-code-add").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var submittedData = formElement.serializeObject(),
						data = JSON.stringify(submittedData);
					//console.log(data);
					//console.log(submittedData.reportName);
					$.ajax({
						url: "ApiBulletManagement.php",
						type: "POST",
						data: data,
						success: function(data){
							//console.log(data);
							$('#codeResults').show().html("Added To Table").addClass("alert").addClass("info").fadeOut(5000);
							tableBulletTemplates.row.add([
								submittedData.invoiceCode+' '+submittedData.reportName,
								submittedData.displayText,
								submittedData.status
							]).draw(false);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
		<?php if ($SelectedID){?>
			$("#save-code-update").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var data = JSON.stringify(formElement.serializeObject());
					//console.log(data);
					$.ajax({
						url: "ApiBulletManagement.php",
						type: "POST",
						data: data,
						success: function(data){
							//console.log(data);
							$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
							location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
			
		<?php }//end if SelectedID ?>
	});
</script>