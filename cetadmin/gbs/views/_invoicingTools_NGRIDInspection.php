<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<?php
$_SESSION['SalesForceReport'] = "NGRIDInspection";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$billingRatesArray = array("NGRID_Inspection"=>array("524B","524T"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
			$BillingCategoryByEmployee[$Employee] = $record->GBSBillingRate_Name;
		}
	}
}
//print_pre($BillingRateByCategory["NGRID_Inspection"]);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","524 Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	echo "<h3>This tool will create the NGRID Inspection Invoice files</h3>";
	
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "NGRID Inspection";
			$SelectedGroupsID = 6; //GreenTeam
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if ($BillingCategory == "Specialist" && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate".$InvoiceCode][]=str_replace("_",", ",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once 'salesforce/config.php';
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00O0P000002yYZf"; //Invoicing/524 NGRID Inspections
			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
			$reportId = "00O0P000002yYZk"; //Invoicing/524 NGRID Inspection Time Cards
			$reportName = "524 NGRID Inspection Time Cards";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Time_Card__c.Date_Worked__c",SORT_ASC);
			echo "There are ".count($reportRowsSorted)." rows<Br>";
			//print_pre($reportRowsSorted);
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				$requestorName = $rowDetail["Upgrade__c.Ngrid_Inspection_Requestor__c"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$AccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$billingCity = $rowDetail["Account.BillingCity"];
				$billingState = $rowDetail["Account.BillingState"];
				
				$dateReceived = $rowDetail["Upgrade__c.CreatedDate"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$dateSentBackToNGRID = $rowDetail["Upgrade__c.Date_Sent_Back_to_NGRID__c"];
				
				$currentStatusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = (strpos(strtolower($rowDetail["Upgrade__c.Green_Prospect_Type__c"]),"post") ? "Post" : "Pre") ;
				$stageCompleted = strtotime($completedDate);
				$applicationNumber = $rowDetail["Upgrade__c.Application_Number__c"];
				$timeCardCreatedBy = $rowDetail["Time_Card__c.CreatedBy.Name"];
				$timeCode = ($rowDetail["Time_Card__c.Work_Category__c"] == "Travel" ? "Travel" : "SiteWork");
				$timeCardHours = $rowDetail["Time_Card__c.Hours_Worked__c"];
				$hours = $timeCardHours;
				$timeCardDate = $rowDetail["Time_Card__c.Date_Worked__c"];
				if($rowDetail["Time_Card__c.Work_Category__c"] == "On-site"){
					$dateInspected[$applicationNumber] = $timeCardDate;
				}
				if (!$applicationNumber){
					$Adjustments["Alert"]["Completed Account missing Application Number"][] = $GPNameHref;
				}
				$employeeNameParts = explode(" ",$timeCardCreatedBy);
				$Employee = $employeeNameParts[1]."_".$employeeNameParts[0];
				$billingCategory = $BillingCategoryByEmployee[$Employee];
				$billingRate = $BillingRateByEmployee[$Employee]["NGRID_Inspection"][$billingCategory];
				if (!$billingCategory){
					$Adjustments["Alert"]["No Billing Rate for Employee"][] = $timeCardCreatedBy;
				}
				if (strtotime($timeCardDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($timeCardDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink] = bcadd($timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink],$timeCardHours,2);
					$timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate] = bcadd($timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate],$timeCardHours,2);
					$timeCardsHoursByRate[$billingRate][$accountName]=bcadd($timeCardsHoursByRate[$billingRate][$accountName],$timeCardHours,2);
					$timeCardsHoursCount[$billingRate."_".$accountName]=1;
					$timeCardsByCategory[$billingState][$billingCategory][$timeCode] = bcadd($timeCardsByCategory[$billingState][$billingCategory][$timeCode],$timeCardHours,2);
					$timeCardsByEmployee[$Employee] = bcadd($timeCardsByEmployee[$Employee],$hours,2);
					$timeCardInfo = array(
						"name"=>$accountName,
						"accountNameLink"=>$AccountNameHref,
						"GPName"=>$greenProspectName,
						"GPNameLink"=>$GPNameHref,
						"GPType"=>$GPType,
						"Application Number"=>$applicationNumber,
						"reportLink"=>$reportLink,
						"category"=>$timeCode,
						"hours"=>$timeCardHours,
						"dateWorked"=>$dateInspected[$applicationNumber],
						"billingCategory"=>$billingCategory,
						"billingRate"=>$billingRate,
						"state"=>$billingState,
						"city"=>$billingCity,
						"requestorName"=>$requestorName,
						"dateReceived"=>$dateReceived,
						"dateSentBackToNGRID"=>$dateSentBackToNGRID,
						"currentStatusDetail"=>$currentStatusDetail,
						"inspector"=>$timeCardCreatedBy
						);
					$timeCardsByCreated[$timeCardCreatedBy][$applicationNumber][$timeCode][] = $timeCardInfo;
					$timeCards[$timeCode][] = $timeCardInfo;
					$allTimeCards[] = $timeCardInfo;
					$timeCardsByApplicationNumber[$applicationNumber][$timeCode][] = $timeCardInfo;
				}
			}
			foreach ($timeCardsByCreated as $createdBy=>$applicationInfo){
				foreach ($applicationInfo as $applicationNumber=>$categoryInfo){
					foreach ($categoryInfo as $category=>$hourInfo){
						foreach ($hourInfo as $id=>$hourData){
							$dateID = date("m/d/Y",strtotime($hourData["dateWorked"]))."_".$createdBy."_".$hourData["GPType"]."_".$applicationNumber."_".$hourData["name"]."_".$hourData["billingRate"];
							$summaryData[$dateID][$category] = bcadd($summaryData[$dateID][$category],$hourData["hours"],2);
							//for new version of report 7/27/2018
							$summaryDataWithDetails[$dateID]["details"] = $hourInfo;
							$summaryDataWithDetails[$dateID][$category] = bcadd($summaryData[$dateID][$category],$hourData["hours"],2);
						}
					}
				}
			}
				
			//print_pre($timeCardsByCategory);	
			foreach ($timeCardsByEmployee as $Employee=>$hours){
				if (($InvoiceHourData["524B"][$Employee]+$InvoiceHourData["524T"][$Employee]) != $hours){
					$Adjustments["Alert"]["Mismatch Hours between Salesforce and CET Hours"][] = str_replace("_",", ",$Employee)." SF hours=".$hours." CET Hours=".($InvoiceHourData["524B"][$Employee]+$InvoiceHourData["524T"][$Employee]);
				}
				
			}
			
			ksort($summaryData);
			//print_pre($summaryData);
			echo "Report";
			echo "<table class='simpleTable'>";
			echo "<thead><tr><th>Date</th><th>Insp</th><th>Pre/Post</th><th>App#</th><th>Customer Name</th><th>Rate</th><th>Site Work Hrs</th><th>Site Work Hrs Cost</th><th>Travel Hrs</th><th>Travel Cost</th><th>Sub Total</th></thead>";
			echo "<tbody>";
			foreach ($summaryData as $rowInfo=>$workCategory){
					$rowData = explode("_",$rowInfo);
					$billingRate = $rowData[5];
					$siteWorkHrs = ($workCategory["SiteWork"] ? : 0);
					$travelHrs = ($workCategory["Travel"] ? : 0);
					$siteWorkCost = ($billingRate*$siteWorkHrs);
					$travelCost = (($billingRate/2)*$travelHrs);
					$sumCost = ($siteWorkCost+$travelCost);
					$totalAmount = bcadd($totalAmount,$sumCost,2);
					echo "<tr>";
						echo "<td>".$rowData[0]."</td>
						<td>".$rowData[1]."</td>
						<td>".$rowData[2]."</td>
						<td>".$rowData[3]."</td>
						<td>".$rowData[4]."</td>
						<td>".$rowData[5]."</td>
						<td>".$siteWorkHrs."</td>
						<td>".$siteWorkCost."</td><td>".$travelHrs."</td><td>".$travelCost."</td><td>".$sumCost."</td>";
					echo "</tr>";
			}
			echo "<tr><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td>Total</td><td>".money($totalAmount)."</td></tr>";
			echo "</tbody>";
			echo "</table>";
			
			ksort($timeCardsByCategory);
			$thisRowId = 19;
			foreach ($timeCardsByCategory as $state=>$rateCategoryInfo){
				$state = ($state == "MA" ? "Massachusetts" : "Rhode Island");
				$summaryRowData[$thisRowId]["A".$thisRowId] = $state." Inspections assigned by National Grid";
				$thisRowId++;
				$stateRowStarts[$state] = $thisRowId;
				foreach ($rateCategoryInfo as $billingCategory=>$workCategory){
					foreach ($workCategory as $work=>$hours){
						if ($hours > 0){
							$thisRate = $BillingRateByCategory["NGRID_Inspection"][$billingCategory]->GBSBillingRate_Rate;
							$thisRate = ($work == "Travel" ? ($thisRate/2) : $thisRate);
							$summaryRowData[$thisRowId]["A".$thisRowId] = $billingCategory;
							$summaryRowData[$thisRowId]["B".$thisRowId] = $work;
							$summaryRowData[$thisRowId]["F".$thisRowId] = $hours;
							$summaryRowData[$thisRowId]["G".$thisRowId] = $thisRate;
							$summaryRowData[$thisRowId]["H".$thisRowId] = "=G".$thisRowId."*F".$thisRowId;
							
							$revenueCodes["NGRIDInspection"]["40202"] = $revenueCodes["NGRIDInspection"]["40202"]+bcmul($hours,$thisRate,2);
							$revenueCodesByEmployee["NGRIDInspection"]["40202"][$state] = $revenueCodesByEmployee["NGRIDInspection"]["40202"][$state]+bcmul($hours,$thisRate,2);
							
							$thisRowId++;
						}
					}
					//SubTotal Row
					$summaryRowData[$thisRowId]["G".$thisRowId] = "SubTotal:";
					$summaryRowData[$thisRowId]["H".$thisRowId] = "=SUM(H".$stateRowStarts[$state].":H".($thisRowId-1).")";
					$subTotals[] = "H".$thisRowId;
				}
				$thisRowId++;
			}
			//print_pre($summaryRowData);
			$columns = array("A","B","C","D","E","F","G","H");
			echo "<br><hr>Invoice";
			echo "<table class='simpleTable'>";
			echo "<thead><tr>";
				foreach ($columns as $letter){
					echo "<th>".$letter."</th>";
				}
			echo "</tr></thead>";
			echo "<tbody>";
				foreach ($summaryRowData as $rowId=>$columnData){
					echo "<tr>";
						foreach ($columns as $letter){
							echo "<td>".$columnData[$letter.$rowId]."</td>";
						}
					echo "</tr>";
				}
			echo "<tr><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td>Total</td><td>".money($totalAmount)."</td></tr>";

			echo "</tbody>";
			echo "</table>";
			
			include_once('salesforce/reports/reportNGRIDInspection.php');
			echo $ReportsFileLink;
			
			//print_pre($pipelineItems);
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "NGRID Inspections");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the <?php echo str_replace("<br>","",$ReportsFileLink);?> and review 524 Invoice and Report.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>