<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the BGAS Invoice files<span id="invoiceDate"></span></h3>
<?php
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$_SESSION['SalesForceReport'] = "BGAS";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}

$GBSProvider = new GBSProvider($dataConn);
$paginationResult = $GBSProvider->invoiceTool_getProductInfo();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
//	print_pre($record);
	$standardizedNames[$record->MeasureName]["EFI"] = $record->EFI;
	$standardizedNames[$record->MeasureName]["Description"] = $record->Description;
	$standardizedNames[$record->MeasureName]["BGas"] = $record->BGasName;
	$standardizedNames[$record->MeasureName]["Cost"] = $record->Cost;
	
	$standardizedNames[$record->BGasName]["EFI"] = $record->EFI;
	$standardizedNames[$record->BGasName]["Description"] = $record->Description;
	$standardizedNames[$record->BGasName]["Cost"] = $record->Cost;
	
	$standardizedEFI[$record->EFI]["Description"] = $record->Description;
	$standardizedEFI[$record->EFI]["Cost"] = $record->Cost;
	$standardizedEFI[$record->EFI]["MeasureName"] = $record->MeasureName;
}

$paginationResult = $GBSProvider->invoiceTool_getProductInfo();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	//print_pre($record);
	if ($record->MeasureName){
		if (strpos($record->MeasureName,",")){
			$measureNames = explode(",",$record->MeasureName);
		}else{
			$measureNames = array($record->MeasureName);
		}
		foreach ($measureNames as $measureName){
			$standardizedNames[$measureName]["EFI"] = $record->EFI;
			$standardizedNames[$measureName]["Description"] = $record->Description;
			$standardizedNames[$measureName]["Cost"] = $record->Cost;
		}
		$standardizedNames[$record->BGasName]["EFI"] = $record->EFI;
		$standardizedNames[$record->BGasName]["Description"] = $record->Description;
		$standardizedNames[$record->BGasName]["Cost"] = $record->Cost;
		$standardizedEFI[$record->EFI]["Description"] = $record->Description;
		$standardizedEFI[$record->EFI]["Cost"] = $record->Cost;
		$standardizedEFI[$record->EFI]["MeasureName"] = $record->MeasureName;
	}
}



//BGAS 810/811 update invoice number
$startInvoiceNumberFromJuly2015 = 65;	
$monthsFromJuly2015 = datediff("2015-07-01", $invoiceDate, "month");
$invoiceNumber = $startInvoiceNumberFromJuly2015+$monthsFromJuly2015;
$InvoiceNumber["BGAS 523"] = "523-".date("y-n",strtotime($invoiceDate));
$InvoiceNumber["BGAS 810/811"] = $invoiceNumber;

//Get Billing Rates
$billingRatesInvoiceNames = array("BGAS_5","BGAS_8");
foreach ($billingRatesInvoiceNames as $invoiceName){
	$criteria = new stdClass();
	$criteria->invoiceName = $invoiceName;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$criteria->invoiceName][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateEmployees[$criteria->invoiceName][$record->GBSBillingRate_Name][] = $Employee;
			$BillingRateByEmployees[$criteria->invoiceName][$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}	
//print_pre($BillingRateByCategory);
//print_pre($BillingRateByEmployees);
$setTab = "Hours";
$TabCodes = array("Hours"=>"Hours","BGas Accounts File"=>"BGasAccounts","Campaign Report"=>"CampaignReport","Outreach"=>"Outreach","Adjustments"=>"Adjustments","Notes"=>"Notes");
foreach ($TabCodes as $TabName=>$TabCode){
	$TabHashes[] = $TabCode;
	$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
}
echo '<ul id="admin-tabs" class="tabs">';
		foreach ($TabListItems as $TabItem){
			echo $TabItem;
		}
echo '</ul>';		


//get Reports from SalesForceReport
require_once 'salesforce/config.php';
require_once('salesforce/rest_functions.php');
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
$attachmentFolder = "523_BGAS";
$reportFolder = "523_BGAS";
$reportId = "00OU0000003EGUh"; //Invoicing/BGAS 523 Campaign Report


//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
if (!checkSession($instance_url, $reportId, $access_token)){
	session_start();
	$_SESSION['SalesForceReport'] = "BGAS";
	$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
	echo "<script>window.location.replace('".AUTH_URL."');</script>";
	//echo AUTH_URL;
	//header('Location: '.AUTH_URL);
}else{					
	ob_start();
	
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
}//end if checkSession

	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "BGAS MF & BGAS C+I Walkthrough & Projects";
			$SelectedGroupsID = "BGAS";
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceDataByDate);
				foreach ($BillingRateByCategory as $InvoiceName=>$CategoryNameInfo){
					foreach ($CategoryNameInfo as $CategoryName=>$RateInfo){
						foreach ($InvoiceData as $Employee_Name=>$hours){
							if (!$EmployeesRevenueCodeByName[$Employee_Name]){
								$Adjustments["Alert"]["Staff missing Revenue Code"][str_replace("_",", ",$Employee_Name)]=" ";
							}
//							echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
//							print_pre($hours);
							if (in_array($Employee_Name,$BillingRateEmployees[$InvoiceName][$CategoryName])){
								foreach ($hours as $code=>$hour){
									if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
										if (!$IncludedEmployees[$code][$Employee_Name]){
											//$InvoiceName = $code;
											$InvoiceHourDataByCategory[$code][$CategoryName]["hours"] = $InvoiceHourDataByCategory[$code][$CategoryName]["hours"]+$hour;
											$InvoiceHourDataByCategory[$code][$CategoryName]["rate"] = $RateInfo->GBSBillingRate_Rate;
											$IncludedEmployees[$code][$Employee_Name] = 1;
										}
									}
								}
							}else{
								$ExcludedEmployees[$Employee_Name] = $hours;
							}
						}
					}
				}
				//find all excluded Employees
				foreach ($IncludedEmployees as $code=>$Employee_Name){
					foreach ($Employee_Name as $EmployeeName=>$val){
						unset($ExcludedEmployees[$EmployeeName]);
					}
				}
				foreach ($ExcludedEmployees as $Employee_Name=>$val){
					$hoursDisplay = array();
					foreach ($val as $code=>$hours){
						if ($hours && $hours != "0.00" && $code !="TotalHours"){
							$hoursDisplay[] = $code.": ".$hours;
						}
					}
					if (count($hoursDisplay)){
						$Adjustments["Alert"]["Employees missing Billing Rate Code"][] = str_replace("_",", ",$Employee_Name).implode(", ",$hoursDisplay);
					}
				}
				
			//print_pre($InvoiceHourDataByCategory);
			
		echo "</div>";
		echo "<div id='BGasAccounts' class='tab-content'>";
			$ImportFileName = "importfiles/BGasAccounts".date("Y_m",strtotime($invoiceDate)).".xlsx";
			echo "ImportFileName ".$ImportFileName."<br>";
			$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
			$link_file = $CurrentServer.$adminFolder."gbs/".$ImportFileName;
			if(count($_FILES['filesToUpload1'])) {
				$tmpFileName = $_FILES["filesToUpload1"]["tmp_name"][0];
				if ($_GET['type'] == "bgasAccountFile"){
					$ImportFileReceived = true;
					move_uploaded_file($tmpFileName, $target_file);
					//$results = $GBSProvider->invoiceTool_loadExternalData($target_file,"gbs_bgas_customer_accounts");
					//print_pre($results);
				}
			}
			if ($ImportFileReceived || $_GET['useUploadedFile']){
				//new import that data and parse it out 
				$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
				$_SESSION['ImportFileName'] = $ImportFileName;
				echo "Reading <a href='".$link_file."'>uploaded BGas Account file</a><br>";
				ob_flush();
				include_once('salesforce/read_BGASDatabase.php');
			}
			if (!$ImportFileReceived){
				if (file_exists($target_file)){
					$lastUploadedDateParts = explode("File",$ImportFileName);
					$lastBGasAccountUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
					echo "Using <a href='".$link_file."'>Existing Uploaded BGas Account File</a> last uploaded ".date("F Y",strtotime($lastBGasAccountUploadedDate))."<br>";
					$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
					$trackingFile = $ImportFileName;
					//include_once('salesforce/read_BGASDatabase.php');
				}else{
					echo "No Prior BGas Account File uploaded.  Please upload the most recent BGas Account File<br>";
				}
			}
		
			?>
				<a id='updateBGasAccountFile' href="#" class='button-link do-not-navigate'>Update BGas Account File</a><br>
				<div id="updateBGasAccountFileForm" style='display:none;'>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?type=bgasAccountFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload1[]" id="filesToUpload1" multiple="" onChange="makeFileList1();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList1"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList1() {
								var input = document.getElementById("filesToUpload1");
								var ul = document.getElementById("fileList1");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
						</script>
					</form>
				</div>
			<?php
		echo "</div>";
		echo "<div id='CampaignReport' class='tab-content'>";
			
			$reportId = "00OU0000002yJqM"; //Invoicing/810 Berkshire Gas Completed
			$reportName = "810 Berkshire Gas Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);
				$accountAddress[$AccountNameLink] = $address;
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$gpNames[$GPNameLink] = $greenProspectName;
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Utility_Location_ID__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$gpDateCompleted[$GPNameLink] = $completedDate;
				$metricName = $rowDetail["Metric__c.Name"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				$evaluationDate = $completedDate;
				$gpByCode[$GPNameLink] = 810;
				if (strtotime($evaluationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$completed[810][] = $rowDetail;
					$includedAccountIds[] = $AccountNameLink;
					$checkForDocuments[810][$GPNameLink] = $GPNameHref;
				}
			}
			$reportId = "00OU0000002yKcB"; //Invoicing/810 Berkshire Gas Completed
			$reportName = "811 Berkshire Gas Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);				
				$accountAddress[$AccountNameLink] = $address;
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$gpNames[$GPNameLink] = $greenProspectName;
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Utility_Location_ID__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$gpDateCompleted[$GPNameLink] = $completedDate;
				$metricName = $rowDetail["Metric__c.Name"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				$gpByCode[$GPNameLink] = 811;

				$evaluationDate = $completedDate;
				if (strtotime($evaluationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$completed[811][] = $rowDetail;
					$includedAccountIds[] = $AccountNameLink;
					$checkForDocuments[811][$GPNameLink] = $GPNameHref;
				}
			}
				
			$reportId = "00OU0000002yK0q"; //Invoicing/523 Timecard Report
			$reportName = "523 Timecard Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$accountGasAccountNumber = $rowDetail["Account.Gas_Account_Number__c"];
				$accountGasLocationID = $rowDetail["Account.Gas_Location_ID__c"];
				$accountUtilityInfo[$AccountNameLink]["utilityAccountNumber"] = trim($accountGasAccountNumber);
				$accountUtilityInfo[$AccountNameLink]["utilityLocationID"] = trim($accountGasLocationID);
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}

				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);				
				$accountAddress[$AccountNameLink] = $address;
				$addressParts = explode(",",$address);
				$street = rtrim($rowDetail["Account.BillingStreet"],".");
				$accountStreet[$AccountNameLink] = $street;
				$city = $rowDetail["Account.BillingCity"];
				$accountCity[$AccountNameLink] = $city;
				$address = $street.", ".$city;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$utilityAccountNumber = (trim($rowDetail["Upgrade__c.Account_Number__c"]) != "-" ? $rowDetail["Upgrade__c.Account_Number__c"] : $accountGasAccountNumber);
				$utilityLocationID = (trim($rowDetail["Upgrade__c.Utility_Location_ID__c"]) != "-" ? $rowDetail["Upgrade__c.Utility_Location_ID__c"] : $accountGasLocationID);
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$gpNames[$GPNameLink] = $greenProspectName;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$timeCardCreatedBy = $rowDetail["Time_Card__c.CreatedBy.Name"];
				$timeCardName = $rowDetail["Time_Card__c.Name"];
				$timeCardHours = $rowDetail["Time_Card__c.Hours_Worked__c"];
				$timeCardDate = $rowDetail["Time_Card__c.Date_Worked__c"];
				$gpDateCompleted[$GPNameLink] = $timeCardDate;
				$accountInfoName[$AccountNameLink] = $AccountNameHref;
				$accountInfoSegment[$AccountNameLink] = $segment;
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				$timeCode = 523;
				$gpByCode[$GPNameLink] = $timeCode;
				if (strtotime($timeCardDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($timeCardDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink] = bcadd($timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink],$timeCardHours,2);
					$timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate] = bcadd($timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate],$timeCardHours,2);
					$timeCardsHoursByAccount[$timeCode][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCardsHoursBySegmentAccount[$timeCode][$segment][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCards[$timeCode][$timeCardCreatedBy][] = array("name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"phase"=>$greenProspectPhase,"dateWorked"=>$timeCardDate);
					$includedAccountIds[] = $AccountNameLink;
				//}
				}
				//print_pre($utilityLocationID."  ".$timeCardDate." <br>");
				//var_dump($gpUtilityInfo[$GPNameLink]);
				//echo "<br> next <br>";
			}
			
			//print_pre($completed);
		
		
			$reportId = "00OU0000003EGUh"; //Invoicing/BGAS 523 Campaign Report
			$reportName = "BGAS 523 Campaign Report";
			$reportResults = retrieve_report_TLevel($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}
				
				$accountInfoName[$AccountNameLink] = $AccountNameHref;
				$accountInfoSegment[$AccountNameLink] = $segment;
				$accountGasAccountNumber = $rowDetail["Account.Gas_Account_Number__c"];
				$accountGasLocationID = $rowDetail["Account.Gas_Location_ID__c"];
				$accountUtilityInfo[$AccountNameLink]["utilityAccountNumber"] = trim($accountGasAccountNumber);
				$accountUtilityInfo[$AccountNameLink]["utilityLocationID"] = trim($accountGasLocationID);
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);
				$accountAddress[$AccountNameLink] = $address;
				$addressParts = explode(",",$address);
				$street = rtrim($rowDetail["Account.BillingStreet"],".");
				$accountStreet[$AccountNameLink] = $street;
				$city = $rowDetail["Account.BillingCity"];
				$accountCity[$AccountNameLink] = $city;
				$address = rtrim($addressParts[0],".").", ".$city;
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$utilityAccountNumber = (trim($rowDetail["Upgrade__c.Account_Number__c"]) != "-" ? $rowDetail["Upgrade__c.Account_Number__c"] : $accountGasAccountNumber);
				$utilityLocationID = (trim($rowDetail["Upgrade__c.Utility_Location_ID__c"]) != "-" ? $rowDetail["Upgrade__c.Utility_Location_ID__c"] : $accountGasLocationID);
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$gpNames[$GPNameLink] = $greenProspectName;
				$greenProspects[$accountName][] = $GPNameLink;
				$accountTypes[$GPNameLink] = $greenProspectType;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$completedAccountDetails[$AccountNameLink] = $rowDetail;				
				$gpByCode[$GPNameLink] = substr($greenProspectType,0,3);
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$campaignResults[] = array("accountId"=>$AccountNameLink,"name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPType"=>$greenProspectType,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"phase"=>$greenProspectPhase,"status"=>$statusDetail,"reportLink"=>$reportLink);
					$campaignTypes[$greenProspectType][] = array("accountId"=>$AccountNameLink,"name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPType"=>$greenProspectType,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"phase"=>$greenProspectPhase,"status"=>$statusDetail,"reportLink"=>$reportLink);
					$campaignResultsAccounts[$AccountNameLink]=array("name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"accountType"=>$greenProspectType,"parentId"=>$GPNameLink,"GPLink"=>$GPNameHref);
					$includedAccountIds[] = $AccountNameLink;
				}
			}
			foreach ($campaignTypes as $gpType=>$results){
				foreach ($results as $result){
					if ($gpType == "523 Walkthrough"){
						//$campaignResultBreakdown["walkthrough"][] = $result;
						$campaignResultBreakdown["walkthrough"][$result["accountId"]] = $result;
					}
					$campaignResultBreakdown["outreach"][$result["accountId"]] = $result;
				}
			}
			
		
		//get 810B hours to see if any multifamily hours should also have Account included in outreach
			$reportId = "00OU0000003dvE7"; //Invoicing/810B Timecard Report-All
			$reportName = "810B Timecard Report-All";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
//			$reportResults = retrieve_report_TLevel($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}
				
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);
				$accountAddress[$AccountNameLink] = $address;
				$addressParts = explode(",",$address);
				$street = rtrim($rowDetail["Account.BillingStreet"],".");
				$city = $rowDetail["Account.BillingCity"];
				$address = $street.", ".$city;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Utility_Location_ID__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$gpNames[$GPNameLink] = $greenProspectName;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$timeCardCreatedBy = $rowDetail["Time_Card__c.CreatedBy.Name"];
				$timeCardName = $rowDetail["Time_Card__c.Name"];
				$timeCardHours = $rowDetail["Time_Card__c.Hours_Worked__c"];
				$timeCardDate = $rowDetail["Time_Card__c.Date_Worked__c"];
				$gpDateCompleted[$GPNameLink] = $timeCardDate;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$accountInfoName[$AccountNameLink] = $AccountNameHref;
				$accountInfoSegment[$AccountNameLink] = $segment;
				$timeCode = 810;
				$gpByCode[$GPNameLink] = $timeCode;
				$completedAccountDetails[$AccountNameLink] = $rowDetail;				
				if (strtotime($timeCardDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($timeCardDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink] = bcadd($timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink],$timeCardHours,2);
					$timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate] = bcadd($timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate],$timeCardHours,2);
					$timeCardsHoursByAccount[$timeCode][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCardsHoursBySegmentAccount[$timeCode][$segment][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCards[$timeCode][$timeCardCreatedBy][] = array("name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"phase"=>$greenProspectPhase,"dateWorked"=>$timeCardDate);
					$campaignResultBreakdown["outreach"][$AccountNameLink] = array("accountId"=>$AccountNameLink,"name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"phase"=>$greenProspectPhase);
					$includedAccountIds[] = $AccountNameLink;
				}
			}
			
		//get 811B hours to see if any multifamily hours should also have Account included in outreach
			$reportId = "00OU0000003dvFj"; //Invoicing/811B Timecard Report-Analyst
			$reportName = "811B Timecard Report-Analyst";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$segment = $rowDetail["Account.Bgas_Business_Segment__c"];
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$accountNames[$AccountNameLink] = $accountName;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				if (!$segment || $segment == "-"){
					$Adjustments["Alert"]["Account Missing BGAS Business Segment"][$owner." ".$AccountNameLink] = $AccountNameHref;
				}
				
				$jobId = $rowDetail["Account.JobID__c"];
				$accountJobId[$AccountNameLink] = $jobId;
				$accountPhone[$AccountNameLink] = $rowDetail["Account.Phone"];
				$address = str_replace("<br>","&",$rowDetail["Account.BillingAddress"]);
				$address = str_replace(PHP_EOL," ",$address);
				$accountAddress[$AccountNameLink] = $address;
				$addressParts = explode(",",$address);
				$street = rtrim($rowDetail["Account.BillingStreet"],".");
				$city = $rowDetail["Account.BillingCity"];
				$address = $street.", ".$city;
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$utilityAccountNumber = $rowDetail["Upgrade__c.Account_Number__c"];
				$utilityLocationID = $rowDetail["Upgrade__c.Utility_Location_ID__c"];
				$gpUtilityInfo[$GPNameLink]["utilityAccountNumber"] = trim($utilityAccountNumber);
				$gpUtilityInfo[$GPNameLink]["utilityLocationID"] = trim($utilityLocationID);
				$gpNames[$GPNameLink] = $greenProspectName;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPNameLinksArray[$GPNameLink] = $GPNameHref;
				$dwellingUnits[$GPNameLink] = $rowDetail["Upgrade__c.Dwelling_Units_Affected__c"];
				$timeCardCreatedBy = $rowDetail["Time_Card__c.CreatedBy.Name"];
				$timeCardName = $rowDetail["Time_Card__c.Name"];
				$timeCardHours = $rowDetail["Time_Card__c.Hours_Worked__c"];
				$timeCardDate = $rowDetail["Time_Card__c.Date_Worked__c"];
				$gpDateCompleted[$GPNameLink] = $timeCardDate;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$accountInfoName[$AccountNameLink] = $AccountNameHref;
				$accountInfoSegment[$AccountNameLink] = $segment;
				$timeCode = 811;
				$gpByCode[$GPNameLink] = $timeCode;
				$completedAccountDetails[$AccountNameLink] = $rowDetail;
				if (strtotime($timeCardDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($timeCardDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
				//if ($accountName != "General BGAS C&I Hours"){
					$timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink] = bcadd($timeCardsHours[$timeCode][$timeCardCreatedBy][$AccountNameLink][$GPNameLink],$timeCardHours,2);
					$timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate] = bcadd($timeCardsHoursByDate[$timeCode][$timeCardCreatedBy][$timeCardDate],$timeCardHours,2);
					$timeCardsHoursByAccount[$timeCode][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCardsHoursBySegmentAccount[$timeCode][$segment][$AccountNameLink][$GPNameLink]=$timeCardDate;
					$timeCards[$timeCode][$timeCardCreatedBy][] = array("name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"phase"=>$greenProspectPhase,"dateWorked"=>$timeCardDate);
					$campaignResultBreakdown["outreach"][$AccountNameLink] = array("accountId"=>$AccountNameLink,"name"=>$accountName,"segment"=>$segment,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"hours"=>$timeCardHours,"phase"=>$greenProspectPhase);
					$includedAccountIds[] = $AccountNameLink;
				//}
				}
			}
			
			
//			print_pre($timeCards);
			//print_pre($campaignResultBreakdown["outreach"]["General BGAS C&I Hours"]);
			
			ksort($campaignResultBreakdown["outreach"]);
	
		//print_pre($campaignResultBreakdown["outreach"]);
		include_once('salesforce/soap_connect.php');
		//print_pre($completed);
		
		//get Contact Info
		$includedAccounts = implode("','",$includedAccountIds);
		$query = "SELECT Id, AccountId, LastName, FirstName, Name, Phone, Daytime_Phone__c FROM Contact WHERE AccountId IN ('".$includedAccounts."')";
		//echo $query;
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_pre($response);
		foreach ($response->records as $record){
			$contactLookup[$record->AccountId]["firstName"] = $record->FirstName;
			$contactLookup[$record->AccountId]["lastName"] = $record->LastName;
			$contactLookup[$record->AccountId]["phone"] = ($record->Phone ? : $record->Daytime_Phone__c);
		}
		//print_pre($contactLookup);

		
		//print_pre($checkForDocuments);
		//print_pre($completed);
		foreach ($completed as $code=>$completedResults){
			foreach ($completedResults as $id=>$results){
				$accountId = $results["AccountNameLinkValue"];
				$accountName = $accountNames[$accountId];
				$idLookup = $results["GPNameLinkValue"];
				//get Contractor Names associated with the Green Prospects
				$query = "SELECT Contractor__c, Contractor_Contact_Info__c, Account__c FROM Upgrade__c where Id = '".$idLookup."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				foreach ($response->records as $record){
					if ($record->Contractor__c){
						$ContractorLookup[$idLookup]["Name"]=$record->Contractor__c;
						$ContractorLookup[$idLookup]["Info"]=$record->Contractor_Contact_Info__c;
					}
				}
				
				/*New Method for Attachments*/
				$query = "SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId='".$idLookup."'";
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->LinkedEntityId;
						$contentDocumentId = $record->ContentDocumentId;
						$query2 = "SELECT Id,Title,Description,FileType,FileExtension,VersionData FROM ContentVersion WHERE ContentDocumentId='".$contentDocumentId."'";
						$response2 = $mySforceConnection->query($query2); //sending request and getting response using SOAP
						foreach ($response2->records as $record2) {
							$name = str_replace(" ","_",$record2->Title);
							$fileType = $record2->FileType;
							$fileExtension = $record2->FileExtension;
							$versionData = $record2->VersionData;
							if (strpos(" ".$fileType,"EXCEL") && !strpos(strtolower($name),"screeningtool")){
								$thisAttachmentInfo = array("accountId"=>$accountId,"Id"=>$contentDocumentId,"code"=>$code,"Name"=>$name,"FileExtension"=>$fileExtension,"AccountName"=>$accountName,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
								$attachmentName = retrieve_attachmentName($thisAttachmentInfo);
								$thisAttachmentInfo["SavedName"] = $attachmentName;
								$destination = "salesforce/".$attachmentFolder."/".$attachmentName;
								//echo $destination."<br>";
								$file = fopen($destination, "w+");
								fputs($file, $versionData);
								fclose($file);
								$Attachments[$parentId][] = $thisAttachmentInfo;
								//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
								
							}
							
						}
						//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
					}
				}else{
					//$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
					//$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
					$GPLink = "";
					foreach ($parentIds as $GPLinks){
						$GPLink .= $GPNameLinksArray[$GPLinks];
					}
					$Adjustments["Alert"]["Missing Attachment"][] = $accountName." ".$GPLink;
					//echo $GPLink." missing Attachment<br>";
				}
				
				
				/*End New Method*/
			
				/* Old Method For Getting Attachments*/
				/*
				//print_pre($ContractorLookup); //ARRAY Get Contractor Field name associated with Green Prospect based on parentIds
				//get attachments
				//echo $accountName." = ".$ids."<br>";
				$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId ='".$idLookup."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				//print_pre($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->ParentId;
						$name = $record->Name;
						$id = $record->Id;
						$fileParts = explode(".",$name);
						if (in_array("xlsx",$fileParts) && !strpos(strtolower($name),"screeningtool")){
							$Attachments[$parentId][] = array("accountId"=>$accountId,"Id"=>$id,"code"=>$code,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
						}
						//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
					}
				}else{
					//$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
					//$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
					$GPLink = "";
					foreach ($parentIds as $GPLinks){
						$GPLink .= $GPNameLinksArray[$GPLinks];
					}
					$Adjustments["Alert"]["Missing Attachment"][] = $accountName." ".$GPLink;
				}
				*/
				/*End old method*/
			}//end foreach completedResults
		}//end foreach completed

		//print_pre($AccountNamesLookup);
		//print_pre($ContractorLookup);
		//print_pre($Attachments);
		
		/* Old Method For Getting Attachments*/
		/*
		foreach ($Attachments as $parentId=>$attachmentList){
			foreach ($attachmentList as $attachmentInfo){
				if ($attachmentInfo["NoAttachments"]){
					$attachmentResults[$attachmentInfo["Name"]]["NoAttachments"] = true;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["HeatingFuel"] = $attachmentInfo["HeatingFuel"];
					$attachmentResults[$attachmentInfo["Name"]]["GreenProspect"] = $attachmentInfo["GreenProspect"];
					$attachmentResults[$attachmentInfo["Name"]]["parentId"] = $parentId;
				}else{
					$attachmentId = $attachmentInfo["Id"];
					//echo $instance_url." ".$attachmentInfo["Id"]." ".$attachmentInfo["Name"]."<Br>";
					$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
					//echo $attachment." just saved<br>";
					$attachmentResults[$parentId][$attachmentId]["SavedName"] = $attachment;
					$attachmentResults[$parentId][$attachmentId]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$parentId][$attachmentId]["FileName"] = $attachmentInfo["FileParts"][0];
					$attachmentResults[$parentId][$attachmentId]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
					$attachmentResults[$parentId][$attachmentId]["ActualFileName"] = $attachmentInfo["Name"];
					$attachmentResults[$parentId][$attachmentId]["ContractorName"] = $attachmentInfo["ContractorName"];
					$attachmentResults[$parentId][$attachmentId]["parentId"] = $parentId;
					$attachmentResults[$parentId][$attachmentId]["accountId"] = $attachmentInfo["accountId"];
					$attachmentResults[$parentId][$attachmentId]["accountType"] = $accountTypes[$parentId];
					$attachmentResults[$parentId][$attachmentId]["code"] = $attachmentInfo["code"];

					//print_r($attachmentResults); //ARRAY DESCRIBE results and details of downloaded files
				}
			}
		}
		*/
		/* End Old Method For Getting Attachments*/
		/*New Method for Getting Attachment*/
		if (count($Attachments)){
			foreach ($Attachments as $parentId=>$attachmentList){
				foreach ($attachmentList as $attachmentInfo){
					//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
					$attachmentId = $attachmentInfo["Id"];
					$versionData = $attachmentInfo["versionData"];
					//echo $instance_url." ".$attachmentInfo["Id"]." ".$attachmentInfo["Name"]."<Br>";
					$fileUrl = $instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId;
					$fileUrl = $instance_url.$versionData;
					//$attachmentSavedName = retrieve_attachmentName($attachmentInfo,$fileUrl,$access_token);
				
					//echo $attachment." just saved<br>";
					$attachmentResults[$parentId][$attachmentId]["SavedName"] = $attachmentInfo["SavedName"];
					$attachmentResults[$parentId][$attachmentId]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$parentId][$attachmentId]["FileName"] = $attachmentInfo["Name"];
					$attachmentResults[$parentId][$attachmentId]["FileExtension"] = $attachmentInfo["FileExtension"];
					$attachmentResults[$parentId][$attachmentId]["ActualFileName"] = $attachmentInfo["Name"];
					$attachmentResults[$parentId][$attachmentId]["ContractorName"] = $attachmentInfo["ContractorName"];
					$attachmentResults[$parentId][$attachmentId]["parentId"] = $parentId;
					$attachmentResults[$parentId][$attachmentId]["accountId"] = $attachmentInfo["accountId"];
					$attachmentResults[$parentId][$attachmentId]["accountType"] = $accountTypes[$parentId];
					$attachmentResults[$parentId][$attachmentId]["code"] = $attachmentInfo["code"];
				}
			}
		}
		/*End New Method for Getting Attachment*/
		//print_pre($attachmentResults);
		ob_flush();
		flush();
		
		foreach ($attachmentResults as $parentId=>$attachmentIdInfo){
			foreach ($attachmentIdInfo as $attachmentId=>$AttachmentSavedItem){
				$AttachmentSavedName = $AttachmentSavedItem["SavedName"];
				$AttachmentAccountName = $AttachmentSavedItem["AccountName"];
				$AccountId = $AttachmentSavedItem["accountId"];
				$parentId = $parentId;
				$NoAttachments = $AttachmentSavedItem["NoAttachments"];
				if (substr($AttachmentSavedName,-4)=="xlsx" && !strpos($AttachmentSavedName,"AccountList")){
					$ToParse[$AttachmentAccountName][$AttachmentSavedItem["ActualFileName"]] = $AttachmentSavedName;
					$ToParseByParentId[$parentId][$AccountId][$AttachmentSavedItem["ActualFileName"]] = $AttachmentSavedName;
				}
			}
		}
		//print_pre($ToParseByParentId);
		foreach ($ToParseByParentId as $parentId=>$accountIdInfo){
			foreach ($accountIdInfo as $accountId=>$actualFileNameInfo){
				foreach ($actualFileNameInfo as $actualFileName=>$fileName){
					include('salesforce/read_attachmentfile.php');
					$parsedRows[$accountId][$parentId][$fileName] = $rowsClean;
					$thisFileLocation = $siteRoot.$adminFolder."gbs/salesforce/".$attachmentFolder."/".$fileName;
					//echo "will unlink ".$thisFileLocation."<br>";
					//unlink($thisFileLocation);
					//print_r($parsedRows); //ARRAY DESCRIBE rows parsed into an array
				}
			}
		}
		ob_flush();
		flush();
		//print_pre($parsedRows);
		//create installation information records
		//print_pre($accountAddress);
		//print_pre($gpUtilityInfo);
		if (count($timeCardsHours)){
			$timeCardsTable = "Results from 810, 811 and 523 Timecards";
			$timeCardsTable .="<table class='simpleTable'>
					<thead><tr><th>Code</th><th>Employee</th><th>Billing Category</th><th>Billing Rate</th><th>Hours</th><th>Cost</th><th>Account</th><th>Segment</th><th>GPName</th><th>DwellingUnits</th></tr></thead><tbody>";
			foreach ($timeCardsHours as $code=>$employeeInfo){
				$color = ($code == 523 ? "blue" : ($code == "810" ? "green" : "orange"));
				$hourCode = $code.($code == "523" ? "" : "B");					
				foreach ($employeeInfo as $employee=>$accountInfo){
					$employeeNameParts = explode(" ",$employee);
					$billRateCode = ($code == 523 ? "BGAS_5" : "BGAS_8");
					$revenueRateCode = ($code == 523 ? "BGAS 523" : "BGAS 810/811");
					$employeeName = $employeeNameParts[1]."_".$employeeNameParts[0];
					$rateInfo = $BillingRateByEmployees[$billRateCode][$employeeName];
					if (!$rateInfo){
						$Adjustments["Alert"]["Missing Billing Rate Information"] = $employee." for ".$billRateCode;
					}
					foreach($accountInfo as $account=>$gpInfo){
						$jobId = $accountJobId[$account];
						$segment = $accountInfoSegment[$account];
						$codeSegment = $code."_".$segment;
						$accountNameText = $accountNames[$account];
						$accountPhoneNumber = $accountPhone[$account];
						$address = $accountAddress[$account];
						$contactLastName = strtoupper($contactLookup[$account]["lastName"]);
						$contactFirstName = strtoupper($contactLookup[$account]["firstName"]);
						$contactPhone = $contactLookup[$account]["phone"];
						$contractor = "";
						foreach ($gpInfo as $gp=>$hour){
							$thisTimeCardItem = $employeeName.$account.$gp;
							$thisAccountNumber = ($gpUtilityInfo[$gp]["utilityAccountNumber"] == "-" ? $accountUtilityInfo[$account]["utilityAccountNumber"] : $gpUtilityInfo[$gp]["utilityAccountNumber"]);
							$thisLocationID = ($gpUtilityInfo[$gp]["utilityLocationID"] == "-" ? $accountUtilityInfo[$account]["utilityLocationID"] : $gpUtilityInfo[$gp]["utilityLocationID"]);

							$contractor = ($ContractorLookup[$gp]["Name"] ? : "CET");
							$contractorInfo = ($ContractorLookup[$gp]["Info"] ? : "320 Riverside Dr. -1A");
							$contractorInfoParts = preg_split('/\r\n|\r|\n/', $contractorInfo);
							$contractorPhone = ($contractorInfoParts[2] ? : "413-586-7350");
							$contractorCityStateZip = explode(",",$contractorInfoParts[1]);
							$contractorCity = ($contractorCityStateZip[0] ? : "Florence");
							$contractorStateZip = explode(" ",$contractorCityStateZip[1]);
							$contractorState = ($contractorStateZip[0] ? : "MA");
							
							$thisCategory = key($rateInfo);
							$thisRate = $rateInfo[key($rateInfo)];
							$thisCost = ($thisRate*$hour);
							if ($jobId == "7899" || $jobId == "14923" || $jobId == "13511"){ //jobId 7899 for General BGAS C&I Hours, 14923 for BGAS Outreach, 13511 for Campaign Related Meetings from Salesforce
								//echo $jobId." ".$accountNameText." ".$employeeName." ".$code." ".$hour." ".$thisCost;
								//print_pre($employeeInfo);
								$jobIdCode = ($jobId == "7899" ? 810 : 523);
								$adminTotalHoursToBeDivided[$jobIdCode][$thisCategory]["hours"] = bcadd($adminTotalHoursToBeDivided[$jobIdCode][$thisCategory]["hours"],$hour,2);
								$adminTotalHoursToBeDivided[$jobIdCode][$thisCategory]["rate"] = $thisRate;
								$adminTotalCostToBeDivided[$jobIdCode][$thisCategory] = bcadd($adminTotalCostToBeDivided[$jobIdCode][$thisCategory],$thisCost,2);
								$salesForceEmployees[$code][$employeeName] = bcadd($salesForceEmployees[$code][$employeeName],$hour,2);
								//$invoicingHoursByCodeSegment[$code]["totalHours"] = bcadd($invoicingHoursByCodeSegment[$code]["totalHours"],$hour,2);
								//$invoicingHoursByCodeSegment[$code]["segments"][$segment] = bcadd($invoicingHoursByCodeSegment[$code]["segments"][$segment],$hour,2);
								
								if (!in_array($thisTimeCardItem,$timeCardRevenue[$thisTimeCardItem])){
									$revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]] = $revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]]+bcmul($hour,$thisRate,2);
									$revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]][$employeeName] = $revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]][$employeeName]+bcmul($hour,$thisRate,2);
									$timeCardRevenue[$thisTimeCardItem][] = $thisTimeCardItem;
								}
							}else{
								$GPName = $GPNameLinksArray[$gp];
								$GPName = ($code == 523 && trim($GPName) == '' ? "Outreach" : $GPName);
								$GPExcelName = ($GPName != 'Outreach' ? $gpNames[$gp] : $GPName);

								$savingsArray = array("code"=>$code,"employee"=>$employee,"billingCategory"=>$thisCategory,"billingRate"=>$thisRate,"hour"=>$hour,"cost"=>$thisCost,"accountName"=>$accountNames[$account],"greenProspect"=>$GPExcelName,"dwellingUnits"=>$dwellingUnits[$gp]);
								$savingsData[$code][] = $savingsArray;
								$timeCardsTable .="<tr style='color:".$color.";'>
									<td>".$code."</td>
									<td>".$employee."</td>
									<td>".$thisCategory."</td>
									<td>$".money($thisRate)."</td>
									<td>".$hour."</td>
									<td>$".money($thisCost)."</td>
									<td>".$accountInfoName[$account]."</td>
									<td>".$segment."</td>
									<td>".$GPName."</td>
									<td>".$dwellingUnits[$gp]."</td>
								</tr>";
								$invoicingResults[$codeSegment][$thisCategory][$thisRate] = bcadd($invoicingResults[$codeSegment][$thisCategory][$thisRate],$hour,2);
								$invoicingHoursByCodeSegment[$code]["totalHours"] = bcadd($invoicingHoursByCodeSegment[$code]["totalHours"],$hour,2);
								$invoicingHoursByCodeSegment[$code]["segments"][$segment] = bcadd($invoicingHoursByCodeSegment[$code]["segments"][$segment],$hour,2);
								
								if (!in_array($thisTimeCardItem,$timeCardRevenue[$thisTimeCardItem])){
									$revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]] = $revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]]+bcmul($hour,$thisRate,2);
									$revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]][$employeeName] = $revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employeeName]][$employeeName]+bcmul($hour,$thisRate,2);
									$timeCardRevenue[$thisTimeCardItem][] = $thisTimeCardItem;
								}
								
								$gpTotalHours[$gp] = bcadd($gpTotalHours[$gp],$hour,2);
								$gpTotalCost[$gp] = bcadd($gpTotalCost[$gp],$thisCost,2);
								$salesForceEmployees[$code][$employeeName] = bcadd($salesForceEmployees[$code][$employeeName],$hour,2);
								$salesForceByGP[$code][$account][$gp]["hours"] = bcadd($salesForceByGP[$code][$account][$gp]["hours"],$hour,2);
								$salesForceByGP[$code][$account][$gp]["cost"] = bcadd($salesForceByGP[$code][$account][$gp]["cost"],$thisCost,2);
								$salesForceByGP[$code][$account][$gp]["employee"] = $employee;
								$salesForceByCategory[$code][$thisCategory]["hours"] = bcadd($salesForceByCategory[$code][$thisCategory]["hours"],$hour,2);
								$salesForceByCategory[$code][$thisCategory]["rate"] = $BillingRateByCategory["BGAS_5"][$thisCategory]->GBSBillingRate_Rate;
								
								//echo $accountInfoName[$account]." accountNumber=".$thisAccountNumber."<br>";
								$thisAccountNumber = (substr($thisAccountNumber,0,1) == "0" ? ltrim($thisAccountNumber,"0") : $thisAccountNumber);
								$thisAccountNumber = (strlen($thisAccountNumber) == 12 ? $thisAccountNumber : substr($thisAccountNumber,0,12));
								$BGasResult = $GBSProvider->getBGASCustomer($thisAccountNumber,$thisLocationID);
								//print_pre($BGasResult);
								$rate = "";
								$street = $completedAccountDetails[$account]["Account.BillingStreet"];
								$city = $completedAccountDetails[$account]["Account.BillingCity"];
								if (count($BGasResult)){
									$contactFirstName = ($BGasResult->LastName != "#" && $BGasResult->FirstName ? $BGasResult->FirstName : $contactFirstName);
									$contactLastName = ($BGasResult->LastName != "#" && $BGasResult->LastName? $BGasResult->LastName: $contactLastName);
									$rateParts = explode(",",$BGasResult->Rate);
									//print_pre($rateParts);
									$rate = trim($rateParts[(count($rateParts)-1)]);
									//echo $rate."<hr>";
									
									$contactPhone = ($BGasResult->Phone !="#" ? $BGasResult->Phone : $contactPhone);
									$street = $BGasResult->HouseNumber." ".$BGasResult->StreetName;
									$city = $BGasResult->City;
								}
								
								$segment = $accountInfoSegment[$account];
								if ($segment == "-"){
									$Adjustments["Alert"]["Business Segment Missing"][] = $gp;
								}
								$thisResultArray = array(
									"utilityAccountNumber"=>$thisAccountNumber,"utilityLocationID"=>$thisLocationID,"rate"=>$rate,
									"accountId"=>$account,"accountName"=>$accountInfoName[$account],"segment"=>$segment,"accountPhoneNumber"=>$accountPhone[$account],"GPLink"=>$gp,"GreenProspect"=>$gpNames[$gp],
									"code"=>$code,"incentiveDate"=>$timeCardsHoursByAccount[$code][$account][$gp],
									
									"contactFirstName"=>$contactFirstName,"contactLastName"=>$contactLastName,"contactPhone"=>$contactPhone,
									"street"=>$street,"city"=>$city,
									"dwellingUnits"=>$dwellingUnits[$gp],"measureName"=>"n/a","qty"=>"n/a",
									"contractor"=>$contractor,"contractorInfo"=>$contractorInfo,"contractorPhone"=>$contractorPhone,"contractorAddress"=>$contractorInfoParts[0],"contractorCity"=>$contractorCity,"contractorState"=>$contractorState,
									"incentiveAmount"=>"n/a","installAmount"=>"n/a","annualSavings"=>0,			
									
									"totalHours"=>$gpTotalHours[$gp],"totalCost"=>$gpTotalCost[$gp]
								);
								//print_pre($thisResultArray);	
								$codeSegment = $code."_".$segment;
								$campaignResultBreakdownByAccounts[$codeSegment][$account]=1;
								$campaignResultBreakdownByCodeAdditional["justhours"][$codeSegment][$account][$gp] = $thisResultArray;
							}//end if jobId 7899
						}//end foreach gpInfo
					}//end foreach accountInfo
				}//end foreach employeeInfo
				
				//determine ratio of segment to divide up admin hours
				foreach ($invoicingHoursByCodeSegment as $code=>$hourInfo){
					$totalHours = $hourInfo["totalHours"];
					foreach ($hourInfo["segments"] as $segment=>$segmentHour){
						$segmentRatioByCode[$code][$segment] = bcdiv($segmentHour,$totalHours,9);
					}
				}
				//print_pre($segmentRatioByCode);

				//now add hours from Dashboard
				foreach ($IncludedEmployees[$hourCode] as $employee=>$count){
					$employeeNameParts = explode("_",$employee);
					$hour = $InvoiceData[$employee][$hourCode];
					$employeeName = $employeeNameParts[1]." ".$employeeNameParts[0];
					//echo $employee."=".$salesForceEmployees[$code][$employee].":".$hour."<br>";
					$showEmployee = true;
					if ($salesForceEmployees[$code][$employee]){
						$showEmployee = false;

						//check hours are the same
						if ($salesForceEmployees[$code][$employee] != $hour){
							//echo "'".$salesForceEmployees[$code][$employee]."' = '".$hour."'<br>";
							$CEThoursDescribe = "";
							foreach ($InvoiceDataByDate[$employee][$hourCode] as $hourDate=>$hourWorked){
								$CEThoursDescribe .= $hourWorked." hours ".MySQLDate($hourDate)."<br>";
							}
							$SFhoursDescribe = "";
							$SFhoursSorted = array();
							foreach ($timeCardsHoursByDate[$code][$employeeName] as $hourDate=>$hourWorked){
								$SFhoursSorted[strtotime($hourDate)] = $hourWorked;
							}
							ksort($SFhoursSorted);
							foreach ($SFhoursSorted as $hourDate=>$hourWorked){
								$SFhoursDescribe .= $hourWorked." hours ".date("m/d/Y",$hourDate)."<br>";
							}
							/*
							foreach ($timeCardsHoursByDate[$code][$employeeName] as $hourDate=>$hourWorked){
								$SFhoursDescribe .= $hourWorked." hours ".$hourDate."<br>";
							}
							*/
							$Adjustments["Alert"]["CET Dashboard hours do not match"][] = 
								$employeeName." for ".$code." in Salesforce has ".$salesForceEmployees[$code][$employee]." but in CET shows ".$hour."
									<div style='border:0pt solid black;width:350px;'>
										<div style='float:left;border:0pt solid red;width:145px;padding:2px;'>
											SalesForce ".$salesForceEmployees[$code][$employee]." hours:<br>".$SFhoursDescribe."
										</div>
										<div style='float:right;border:0pt solid blue;width:145px;padding:2px;'>
											CET ".$hour." hours:<br>".$CEThoursDescribe."
										</div>
									</div><br clear='all'>";
							/* Mike Kania wants to use the higher of the two numbers as per his email on 8/9/2016
								From: Mike Kania 
								Sent: Tuesday, August 09, 2016 1:58 PM
								To: Yosh Schulman; Liz Budd
								Subject: RE: Bgas hours

								For all months. I don’t want to spend 3 hours correcting a I hour mistake. What you have stated below is how I want to proceed. Thanks.

								From: Yosh Schulman 
								Sent: Tuesday, August 09, 2016 1:47 PM
								To: Mike Kania; Liz Budd
								Subject: RE: Bgas hours

								The Dashboard is the higher number.  Not sure if we can adjust Salesforce.  Adjusting CET Dashboard is easy in either direction.
								So in the case of a disparagement between hours, if CET Dashboard is higher use it, otherwise use the Salesforce hours and adjust the Dashboard to match?  Or are you just talking for this month?
							*/
							//determine which hour is higher
							$hour = ($hour > $salesForceEmployees[$code][$employee] ? : $salesForceEmployees[$code][$employee]);
							$showEmployee = false; //Mike decided not to apply it to general admin hours 8/10/2016
						}
					}
					if ($showEmployee){
						$billRateCode = ($code == 523 ? "BGAS_5" : "BGAS_8");
						$revenueRateCode = ($code == 523 ? "BGAS 523" : "BGAS 810/811");
						//print_pre($InvoiceData[$employee]);
						$rateInfo = $BillingRateByEmployees[$billRateCode][$employee];
						$thisRate = $rateInfo[key($rateInfo)];
						$thisCategory = key($rateInfo);
						$thisCost = ($thisRate*$hour);
						$savingsArray = array("code"=>$code,"employee"=>$employee,"billingCategory"=>$thisCategory,"billingRate"=>$thisRate,"hour"=>$hour,"cost"=>$thisCost,"accountName"=>"Admin Hours","greenProspect"=>"Admin Hours","dwellingUnits"=>"-");
						$savingsData[$code][] = $savingsArray;
						$timeCardsTable .="<tr style='color:".$color.";'>
							<td>".$code."</td>
							<td>".$employeeName."</td>
							<td>".$thisCategory."</td>
							<td>$".money($thisRate)."</td>
							<td>".$hour."</td>
							<td>$".money($thisCost)."</td>
							<td>Admin Hours</td>
							<td>Segment</td>
							<td>Admin Hours</td>
							<td>-</td>
						</tr>";
						
						$segmentRatioByCode[$code][$segment] = bcdiv($segmentHour,$totalHours,4);
						//echo "<hr>".$employeeName.":<br>";
						foreach ($segmentRatioByCode[$code] as $segment=>$segmentRatio){
							$codeSegment = $code."_".$segment;
							$thisHour = floorToFraction(round(bcmul($hour,$segmentRatio,9),2));
							$thisNewCost = ($thisRate*$thisHour);
							$invoicingResults[$codeSegment][$thisCategory][$thisRate] = bcadd($invoicingResults[$codeSegment][$thisCategory][$thisRate],$thisHour,2);
							$adminTotalHours[$codeSegment] = bcadd($adminTotalHours[$codeSegment],$thisHour,2);
							$adminTotalCost[$codeSegment] = bcadd($adminTotalCost[$codeSegment],$thisNewCost,2);
						}
						
						$salesForceByCategory[$code][$thisCategory]["hours"] = bcadd($salesForceByCategory[$code][$thisCategory]["hours"],$hour,2);
						$salesForceByCategory[$code][$thisCategory]["rate"] = $thisRate;
						
						$revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employee]] = $revenueCodes[$revenueRateCode][$EmployeesRevenueCodeByName[$employee]]+bcmul($hour,$thisRate,2);
						$revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employee]][$employee] = $revenueCodesByEmployee[$revenueRateCode][$EmployeesRevenueCodeByName[$employee]][$employee]+bcmul($hour,$thisRate,2);
						
					}
				}//end foreach timeCardsHours[code]
			}//end foreach timeCardsHours
			$timeCardsTable .= "</tbody></table>";
		}//end if count(timeCardsTable)
		
		//print_pre($adminTotalHoursToBeDivided);
		//now convert Salesforce General Admin hours based on percentage of segment work
		foreach ($segmentRatioByCode as $code=>$segmentInfo){
			foreach ($segmentInfo as $segment=>$segmentRatio){		
				$hourInfo = $adminTotalHoursToBeDivided[$code];
				if (count($hourInfo)){
					foreach ($hourInfo as $thisCategory=>$hourInfo){
						$hour = $hourInfo["hours"];
						$thisRate = $hourInfo["rate"];
						if ($hour > 0){
							$codeSegment = $code."_".$segment;
							$thisHour = floorToFraction(round(bcmul($hour,$segmentRatio,9),2));
							$thisCost = $adminTotalCostToBeDivided[$code][$thisCategory];
							$thisNewCost = round(bcmul($thisCost,$segmentRatio,9),2);
							//echo "converting ".$thisCategory." ".$hour." ".$thisCost." to ".$thisHour." ".$thisNewCost."<Br>";
							
							$invoicingResults[$codeSegment][$thisCategory][$thisRate] = bcadd($invoicingResults[$codeSegment][$thisCategory][$thisRate],$thisHour,2);
							$adminTotalHours[$codeSegment] = bcadd($adminTotalHours[$codeSegment],$thisHour,2);
							$adminTotalCost[$codeSegment] = bcadd($adminTotalCost[$codeSegment],$thisNewCost,2);

						
						}
					}
				}
			}
		}
		
		
		//print_pre($savingsData[810]);
		//print_pre($salesForceByCategory[523]);

		echo $timeCardsTable;
		
		$standardizedBGASMeasureNames = array(
			'Aerator','Aerator (1.5 gpm)','Airsealing','Attic Insulation','Attic insulation','Basement Ceiling Insulation',
			'Crawlspace Ceiling Insulation','Duct Insulation','Duct Sealing','Heating Pipe Insulation','Lux PSP511 thermostat',
			'Lux PSP511 thermostat (Heating and Cooling)','Pipe insulation (3 ft 3/4")','Pipe Wrap','Rim Insulation','Showerhead',
			'Spray valve','Showerhead with TSV','TSV','T-stat - No Cooling','T-stat - Cooling',
			'Wall insulation','WiFi-stat - No Cooling','WiFi-stat – Cooling','Wifi Hub');
			// add Wifi Nest here?
		
		//print_pre($parsedRows);
		//echo "end parsed rows<hr>";
		if (count($parsedRows)){
			foreach ($parsedRows as $accountId=>$parentIdInfo){
				$accountNameText = $accountNames[$accountId];
				$accountPhoneNumber = $accountPhone[$accountId];
				$address = $accountAddress[$accountId];
				$contactLastName = strtoupper($contactLookup[$accountId]["lastName"]);
				$contactFirstName = strtoupper($contactLookup[$accountId]["firstName"]);
				$contactPhone = $contactLookup[$accountId]["phone"];
				$contractor = "";

				foreach ($parentIdInfo as $gpLink=>$fileInfo){
					$thisAccountNumber = ($gpUtilityInfo[$gpLink]["utilityAccountNumber"] == "-" ? $accountUtilityInfo[$accountId]["utilityAccountNumber"] : $gpUtilityInfo[$gpLink]["utilityAccountNumber"]);
					$thisLocationID = ($gpUtilityInfo[$gpLink]["utilityLocationID"] == "-" ? $accountUtilityInfo[$accountId]["utilityLocationID"] : $gpUtilityInfo[$gpLink]["utilityLocationID"]);
					$installedItem = array();
					$totalHours = $gpTotalHours[$gpLink];
					$totalCost = $gpTotalCost[$gpLink];
					$code = $gpByCode[$gpLink];
					$incentiveDate = $gpDateCompleted[$gpLink];
					$contractor = ($ContractorLookup[$gpLink]["Name"] ? : "CET");
					$contractorInfo = ($ContractorLookup[$gpLink]["Info"] ? : "320 Riverside Dr. -1A");
					$contractorInfoParts = preg_split('/\r\n|\r|\n/', $contractorInfo);
					$contractorPhone = ($contractorInfoParts[2] ? : "413-586-7350");
					$contractorCityStateZip = explode(",",$contractorInfoParts[1]);
					$contractorCity = ($contractorCityStateZip[0] ? : "Florence");
					$contractorStateZip = explode(" ",$contractorCityStateZip[1]);
					$contractorState = ($contractorStateZip[0] ? : "MA");
					
					foreach ($fileInfo as $fileSavedName=>$fileRecords){
						$accountName = $accountInfoName[$accountId];
						$segment = $accountInfoSegment[$accountId];
						$accountType = "";
						$GPLink = $GPNameLinksArray[$gpLink];
						foreach ($fileRecords as $fileRecord){
							if (trim($fileRecord["utilityAccountNumber"]) && trim($fileRecord["utilityAccountNumber"]) != $thisAccountNumber){
								$Adjustments["Alert"]["Utility Account Number Issue"][$accountName." ".$GPLink] = "attachment has utility account number ".$fileRecord["utilityAccountNumber"]." but Salesforce ".($thisAccountNumber ? "has ".$thisAccountNumber : "is missing");
							}
							$accountNumber = ($fileRecord["utilityAccountNumber"] ? : $thisAccountNumber);
							if (trim($fileRecord["utilityLocationID"]) && trim($fileRecord["utilityLocationID"]) != $thisLocationID){
								$Adjustments["Alert"]["Utility Location ID Issue"][$accountName." ".$GPLink] = " attachment has utility location ID ".$fileRecord["utilityLocationID"]." but Salesforce ".($thisLocationID ? "has ".$thisLocationID : "is missing");
							}
							$locationID = ($fileRecord["utilityLocationID"] ? : $thisLocationID);
							$accountTypes = explode(" ",$fileRecord["accountType"]);
							$accountType = $accountTypes[0];
							$measure = $fileRecord["Measure Description"];
							$measure = (strpos(" ".strtolower($measure),"aerator") ? "Aerator" : $measure);
							$measure = (strpos(" ".strtolower($measure),"showerhead") ? "Showerhead" : $measure);
							$measure = str_replace("Building 36","",$measure);
							$qty = $fileRecord["Qty"];
							if ($fileRecord["Qty"]){
								$installedItem[] = $qty." ".$measure.($qty > 1 ? "s" : "");
								$installedItems = $qty." ".$measure;
								$measureName = $fileRecord["Measure Description"];
								$displayValue = $measureName;
									//straggler standardizedNames
									//standardize Nest E here?
									$displayValue = ($displayValue == "Showerhead (NIA 2917CH 1.7gpm Earth Shower)" ? "Showerhead" : $displayValue);
									//$displayValue = ($displayValue == "Aerator (1.5 gpm)" ? "A" : $displayValue);
									//$displayValue = ($displayValue == "Spray valve" ? "SPRY" : $displayValue);
									$displayValue = ($displayValue == "Honeywell Thermostat" ? "Lux PSP511 thermostat (Heating and Cooling)" : $displayValue);
									
									$displayValue = ($standardizedNames[$displayValue]["BGas"] ? : $displayValue);
									if(!in_array($displayValue,$standardizedBGASMeasureNames)){
										$Adjustments["Alert"]["NonStandardBGASMeasureName"][] = $displayValue;
										$Adjustments["Alert"]["StandardBGASMeasureNames"] = implode("<br>",$standardizedBGASMeasureNames);
									}
								$measureName = (!in_array($displayValue,$standardizedBGASMeasureNames) ? $displayValue." [NON BGAS STANDARD NAME]" : $displayValue);
								
								$incentiveAmount = $fileRecord["Incentive/Unit"];
								$installAmount = $fileRecord["Cost/Unit"];
								if (strpos(" ".$measureName,"WiFi-stat")){$installAmount = bcsub($installAmount,50,2);}//because customer $50 copay
								$annualSavings = $fileRecord["Annual Therm Savings"];
								//."_".."x".$fileRecord["Qty"]."=".$fileRecord["Total Cost"];
								if ($installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"] && $fileRecord["Cost/Unit"] != $installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"]){
									$Adjustments["Alert"]["Installation Issue"][] = $accountName. " ".$GPLink." has ".$fileRecord["Measure Description"]." with Cost/Item at $".money($fileRecord["Cost/Unit"])." but previously captured at ".$contractor." $".money($installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"]);
								}
								if (strpos(" ".strtolower($fileRecord["Measure Description"]),"insulation") && $contractor == "No Contractor Indicated"){
									$Adjustments["Alert"]["Installation Issue"][] = $accountName. " ".$GPLink." has ".$fileRecord["Measure Description"]." installed with No Contractor Indicated with Cost/Item at $".money($fileRecord["Cost/Unit"])." <b>Item not included on Invoice</b>";
								}else{
									$installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Qty"] = $installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Qty"]+(int)$fileRecord["Qty"];
									$installedByAccountType[$accountType][$contractor][$fileRecord["Measure Description"]]["Cost"] = $fileRecord["Cost/Unit"];
								}
								
								//get BGas Data
//								echo $accountName." accountNumber".$accountNumber."<Br>";
								$accountNumber = (substr($accountNumber,0,1) == "0" ? ltrim($accountNumber,"0") : $accountNumber);
								$accountNumber = (strlen($accountNumber) == 12 ? $accountNumber : substr($accountNumber,0,12));
								$BGasResult = $GBSProvider->getBGASCustomer($accountNumber,$locationID);
								//print_pre($BGasResult);
								if ($accountNumber=="700010026543"){
								//	print_pre($BGasResult);
								}
								$rate = "";
								$street = $completedAccountDetails[$accountId]["Account.BillingStreet"];
								$city = $completedAccountDetails[$accountId]["Account.BillingCity"];
								if (count($BGasResult)){
									$contactFirstName = ($BGasResult->LastName != "#" && $BGasResult->FirstName ? $BGasResult->FirstName : $contactFirstName);
									$contactLastName = ($BGasResult->LastName != "#" && $BGasResult->LastName ? $BGasResult->LastName: $contactLastName);
									$rateParts = explode(",",$BGasResult->Rate);
									$rate = trim($rateParts[(count($rateParts)-1)]);
									$contactPhone = ($BGasResult->Phone !="#" ? $BGasResult->Phone : $contactPhone);
									$street = $BGasResult->HouseNumber." ".$BGasResult->StreetName;
									$city = $BGasResult->City;
								}
								
								$segment = $accountInfoSegment[$accountId];
								$thisResultArray = array(
									"utilityAccountNumber"=>$accountNumber,"utilityLocationID"=>$locationID,"rate"=>$rate,
									"accountId"=>$accountId,"accountName"=>$accountName,"segment"=>$segment,"accountPhoneNumber"=>$accountPhoneNumber,"GPLink"=>$gpLink,"GreenProspect"=>$GPLink,
									"code"=>$code,"incentiveDate"=>$incentiveDate,
									"contactFirstName"=>$contactFirstName,"contactLastName"=>$contactLastName,"contactPhone"=>$contactPhone,
									"street"=>$street,"city"=>$city,
									"dwellingUnits"=>$dwellingUnits[$gpLink],"measureName"=>$measureName,"qty"=>$fileRecord["Qty"],
									"contractor"=>$contractor,"contractorInfo"=>$contractorInfo,"contractorPhone"=>$contractorPhone,"contractorAddress"=>$contractorInfoParts[0],"contractorCity"=>$contractorCity,"contractorState"=>$contractorState,
									"incentiveAmount"=>$incentiveAmount,"installAmount"=>$installAmount,"annualSavings"=>$annualSavings,"totalHours"=>$totalHours,"totalCost"=>$totalCost,
									"installedItems"=>$installedItems,"accountType"=>$accountType,
									"rowDetails"=>$fileRecords);
								$codeSegment = $code."_".$segment;	
								$campaignResultBreakdown["installation"][] = $thisResultArray;
								$campaignResultBreakdownBySegmentAccount[$segment][$code][$account]=1;
								$campaignResultBreakdownByAccounts[$codeSegment][$accountId]=1;
								$campaignResultBreakdownByCode["installation"][$codeSegment][$accountId][] = $thisResultArray;
								$campaignResultBreakdownByCodeCount["installation"][$codeSegment]++;

							}//end if fielRecord[Qty]
							//set totalHours to zero so only shows up on first GP for the account
							$totalHours = "n/a";
							$totalCost = "0.00";
						}//end foreach fileRecords
					}//end foreach gpLinkInfo
					if (count($installedItem) && $contractor == "CET"){
						$installedItems = implode(", ",$installedItem);
						//$thisResultArray = array("accountId"=>$accountId,"name"=>$accountNameText,"GPLink"=>$gpLink,"GreenProspect"=>$GPLink,"address"=>$accountAddress[$accountId],"street"=>$completedAccountDetails[$accountId]["Account.BillingStreet"],"city"=>$completedAccountDetails[$accountId]["Account.BillingCity"],"installedItems"=>$installedItems,"accountType"=>$accountType,"contractor"=>$contractor,"rowDetails"=>$fileRecords);
						$thisActivityArray = array("accountId"=>$accountId,"name"=>$accountNameText,"address"=>$address,"installedItems"=>$installedItems,"utilityAccountNumber"=>$accountNumber,"utilityLocationID"=>$locationID);
						$bgasInstallationActivity[] = $thisActivityArray;
					}
				}//end foreach fileInfo
			}//end foreach parsedRows
		}//end if parsedRows
	
		include_once('salesforce/reports/reportBG_MF+CI_Cost&Savings.php');
		echo $BGASCostAndIncentives = $ReportsFileLink;
		
		//var_dump ($thisResultArray);
		//print_pre($campaignResultBreakdownByCode["installation"]);
		$installTableColumns = array(
			"Program Title"=>"code","Rate Code"=>"rateCode","Incentive Date"=>"incentiveDate",
			"Customer First Name"=>"customerFirstName","Customer Last Name / Business Name"=>"accountName","Customer Phone Number"=>"accountPhoneNumber",
			"Renter / Owner"=>"rentOwn","Contact First Name"=>"contactFirstName","Contact Last Name"=>"contactLastName","Contact Phone Number"=>"contactPhone",
			"Installation Street"=>"street","City"=>"city","DwellUnits"=>"dwellingUnits",
			"Measure Name"=>"measureName","Measure Efficiency"=>"measureEfficiency","Baseline Efficiency"=>"baselineEfficiency",
			"Number of Units"=>"qty","Type of Unit"=>"typeofUnit",
			"ContractorName"=>"contractor","ContractorPhone"=>"contractorPhone","ContractorAddress"=>"contractorAddress","ContractorCity"=>"contractorCity","ContractorState"=>"contractorState",
			"Incentive Amount"=>"incentiveAmount","Total Installation Cost"=>"installAmount","Annual Savings"=>"annualSavings","Total Hours"=>"totalHours","Total Cost of Hours"=>"totalCost"
		);
		$InstallTable ="Installed Items<table class='simpleTable'>";
		$InstallTable .= "<thead><tr>";
		foreach ($installTableColumns as $columnName=>$columnKey){
			$InstallTable .= "<th>".$columnName."</th>";
		}
		$InstallTable .= "</tr></thead><tbody>";
		foreach ($campaignResultBreakdown["installation"] as $installInfo){
			$InstallTable .= "<tr>";
			foreach ($installTableColumns as $columnName=>$columnKey){
				$displayValue = $installInfo[$columnKey];
/*				
				if ($columnKey == "measureName"){
					//straggler standardizedNames
					$displayValue = ($displayValue == "Aerator (1.5 gpm)" ? "A" : $displayValue);
					$displayValue = ($displayValue == "Showerhead (NIA 2917CH 1.7gpm Earth Shower)" ? "SH" : $displayValue);
					$displayValue = ($displayValue == "Spray valve" ? "SPRY" : $displayValue);
					
					$displayValue = ($standardizedNames[$displayValue]["BGas"] ? : $displayValue);
				}
*/
				$InstallTable .= "<td>".$displayValue."</td>";
			}
			$InstallTable .= "</tr>";
			
		}//end foreach campaignResultBreakdown["installation"]
		$InstallTable .= "</tbody></table>";
		echo $InstallTable;
		$outreachTableColumns = array(
			"Staff"=>"employee",
			"Customer Last Name / Business Name"=>"accountName",
			"Customer Phone Number"=>"accountPhoneNumber",
			"Street"=>"street","City"=>"city","Total Hours"=>"totalHours","Total Cost of Hours"=>"totalCost"
		);
//		print_pre($salesForceByGP[523]);
		$OutreachTable ="523 Outreach Items<table class='simpleTable'>";
		$OutreachTable .= "<thead><tr>";
		foreach ($outreachTableColumns as $columnName=>$columnKey){
			$OutreachTable .= "<th>".$columnName."</th>";
		}
		$OutreachTable .= "<th>Green Prospect</th>";
		
		$OutreachTable .= "</tr></thead><tbody>";
		foreach ($salesForceByGP[523] as $account=>$gpInfo){
			foreach ($gpInfo as $gp=>$details){
				$segment = $accountInfoSegment[$account];
				$HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["hours"] = $HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["hours"]+$details["hours"];
				$HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["cost"] = $HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["cost"]+$details["cost"];
				$HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["gp"] = $gp;
				$HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["locationId"] = ($HoursRateCodes[$segment][523][$accountNames[$account]."QQQ".$account]["locationId"] ? : $gpUtilityInfo[$gp]["utilityLocationID"]);
				$thisArray=array("accountName"=>$accountInfoName[$account],"segment"=>$segment,"accountPhoneNumber"=>$accountPhone[$account],"street"=>$accountStreet[$account],"city"=>$accountCity[$account],"totalHours"=>$details["hours"],"totalCost"=>$details["cost"],"employee"=>$details["employee"]);
				$OutreachTable .= "<tr>";
				foreach ($outreachTableColumns as $columnName=>$columnKey){
					$OutreachTable .= "<td>".$thisArray[$columnKey]."</td>";
				}
				$OutreachTable .= "<td>".$GPNameLinksArray[$gp]."</td>";
				$OutreachTable .= "</tr>";
			}
		}//end foreach salesForceByGP
		$OutreachTable .= "</tbody></table>";
		echo $OutreachTable;
		
		if (count($campaignResults)){
			$campaignResultsTable = "Results from ".$reportLink;
			$campaignResultsTable .="<table class='simpleTable'>
					<thead><tr><th>Name</th><th>Address</th><th>GPType</th><th>GPName</th><th>Phase</th><th>Status</th></tr></thead><tbody>";
			foreach ($campaignResults as $result){
				$color = "black";
				
				$campaignResultsTable .="<tr style='color:".$color.";'>
						<td>".$result["accountNameLink"]."</td>
						<td>".$result["address"]."</td>
						<td>".$result["GPType"]."</td>
						<td>".$result["GPNameLink"]."</td>
						<td>".$result["phase"]."</td>
						<td>".$result["status"]."</td>
					</tr>";
			}
			$campaignResultsTable .= "</tbody></table>";
		}
		
		//print_pre($campaignResultBreakdown["installation"]);
		include_once('salesforce/reports/523BGAS.php');
		echo $BGAS523ReportLink = $ReportsFileLink;
		echo $campaignResultsTable;
			
		echo "</div>";//end CampaignReport tab-content

		echo "<div id='Outreach' class='tab-content'>";
			if (count($campaignResultBreakdown["outreach"])){
				//print_pre($campaignResultBreakdown["outreach"]);
				echo "<table class='simpleTable'><thead><tr><th>Name</th><th>Address</th><th>GPName</th><th>From Report</th></tr></thead><tbody>";
				foreach ($campaignResultBreakdown["outreach"] as $accountName=>$OutreachInfo){
					if ($accountName != "General BGAS C&I Hours"){
						echo "<tr><td>".$OutreachInfo["accountNameLink"]."</td><td>".$OutreachInfo["address"]."</td><td>".$OutreachInfo["GPNameLink"]."</td><td>".$OutreachInfo["reportLink"]."</td></tr>";
					}
				}
				echo "</tbody></table>";
			}else{
				echo "None Detected";
			}			
		echo "</div>";
		
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "BGAS");
		include_once('_notesTab.php');
		
		echo "</div>"; //end tabs
	echo "</div>"; //end tabs-content
	//print_pre($campaignResultBreakdownByCode);
?>
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<li>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li>1. Download the <?php echo $BGAS523ReportLink;?>
	<li>2. Download the <?php echo $BGASCostAndIncentives;?>
	<li>3. Access the Bgas Customer database found in L:\GHS\FTP\BGAs customers to update the LocationId, AccountId, Customer Contact Name, Customer Contact Phone, and Install Address in the BGAS C&I File
	<li>4. Updates Salesforce Green Prospect with AccountId and LocationId from BGAS customers list if not previously entered.
<!--	
	<li>4. Standardize the measure names to:
Aerator (1.5 gpm)<br>
Airsealing<br>
Attic insulation<br>
Basement Ceiling Insulation<br>
Crawlspace Ceiling Insulation<br>
Lux PSP511 thermostat<br>
Lux PSP511 thermostat (Heating and Cooling)<br>
Lux PSP722 thermostat<br>
Heating Pipe Insulation<br>
Heating Pipe Insulation<br>
Pipe Wrap<br>
Rim Insulation<br>
Showerhead<br>
Spray valve<br>
Wall insulation<br>
Wifi Hub<br>
WiFi-stat – Cooling<br>
WiFi-stat - No Cooling<br>
-->
	<li>5. Move the Invoice tab to a new file and print as pdf.
	
</ul>

<script type="text/javascript">
	$(function () {
		$("#updateBGasAccountFile").on('click',function(){
			$("#updateBGasAccountFileForm").toggle();
		});

		$("#invoiceDate").html(' for <?php echo date("F Y", strtotime($invoiceDate));?>');
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
		
	});
</script>