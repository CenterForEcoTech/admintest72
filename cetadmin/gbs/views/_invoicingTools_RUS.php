<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the RUS Hours file</h3>
<?php
$_SESSION['SalesForceReport'] = "RUS";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
$invoice550AEndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

include_once($dbProviderFolder."ResidentialProvider.php");
$ResidentialProvider = new ResidentialProvider($dataConn);


//Get Billing Rates
$billingRatesInvoiceNames = array("EPA");
foreach ($billingRatesInvoiceNames as $invoiceName){
	$criteria = new stdClass();
	$criteria->invoiceName = $invoiceName;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$criteria->invoiceName][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateEmployees[$criteria->invoiceName][$record->GBSBillingRate_Name][] = $Employee;
			$BillingRateByEmployees[$criteria->invoiceName][$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
			$BillingRateByEmployeeJustRate[$criteria->invoiceName][$Employee] = $record->GBSBillingRate_Rate;
		}
	}
}	



$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Files"=>"Files","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "RUS Hours";
			$SelectedGroupsID = "RUSHours";
			$CodesByGroupName = array();
			$sourceFolder = "gbs";
			include('../hours/views/_reportByCode.php');
			//print_pre($CodesByGroupName);
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
//			print_pre($CodesInGroup);
//			print_pre($InvoiceData);
			
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
						$InvoiceHourDataByEmployee[$Employee_Name][$code] = $InvoiceHourDataByEmployee[$Employee_Name][$code]+$hour;
						$codeCategory = ($Employee_Name == "Christman_Penny" || $Employee_Name == "Lester_Quinn" ? "Bookkeeper" : "Staff");
						$InvoiceHourDataByCodeCategory[$code][$codeCategory] = $InvoiceHourDataByCodeCategory[$code][$codeCategory]+$hour;
						
						foreach ($CodesByGroupName as $GroupName=>$GroupCodes){
							if (in_array($code,$GroupCodes)){
								$CodeWithHoursInGroup[$GroupName][$code][$Employee_Name] = $InvoiceHourDataByEmployee[$Employee_Name][$code];
								$EmployeesInGroupByEmployeeWithHours[$GroupName][$Employee_Name][$code] = $InvoiceHourDataByEmployee[$Employee_Name][$code];
								if (!in_array($Employee_Name,$EmployeesInGroupByEmployee[$GroupName])){
									$EmployeesInGroup[$GroupName]++;
									$EmployeesInGroupByEmployee[$GroupName][] = $Employee_Name;
								}
							}
						}
					}
				}
			}
			//print_pre($EmployeesInGroup);
			//print_pre($InvoiceHourData);
			//print_pre($CodeWithHoursInGroup);
			//print_pre($InvoiceHourDataByCode);
			//print_pre($InvoiceHourDataByCodeCategory);
			
		echo "</div>";
		echo "<div id='Files' class='tab-content'>";
				$invoice550AStartDate = $invoiceDate;
				//print_pre($EmployeesInGroupByEmployeeWithHours);
				$partialPayVar = 7 + dateDiff("4/1/2018",date("m/d/Y",strtotime($invoiceDate)),"month");
				//echo $partialPayVar;
				$criteria = new stdClass;

				
				$month = date("n",strtotime($invoiceDate));
				if($month<10){
					$periodStartYear = date("Y",strtotime($invoiceDate))-1;
				}else{
					$periodStartYear = date("Y",strtotime($invoiceDate));

				}
				$criteria->periodStartDate = $periodStartYear."-10-1";
				$criteria->source="RUS";
				$criteria->periodEndDate = date("Y-m-d",strtotime($invoiceDate." -1 month"));
				$sf270Data = $ResidentialProvider->getSF270Data($criteria);
				
				$sf270results = $sf270Data->collection;

				if (count($sf270results)) {
					foreach ($sf270results as $result){
						$SF270Data[$result->AccountType]["SF270Name"] = $result->AccountType;
						$SF270Data[$result->AccountType]["PriorPersonell"] += $result->PaymentsAmount;
						$SF270Data[$result->AccountType]["PriorExpenses"] += $result->ExpenseAmount;
						$SF270Data[$result->AccountType]["StartDate"] = date("m/d/Y",strtotime($StartDate));
						$SF270Data[$result->AccountType]["EndDate"] = date("m/d/Y",strtotime($EndDate));
						$SF270Data[$result->AccountType]["SubmittedDate"] = date("m/15/Y");
						$SF270Data[$result->AccountType]["RequestNumber"] = $partialPayVar;
						$SFTableHeaders[$result->AccountType] = $result->AccountType;
		
					}
				}else{
					$SFTableHeaders = array("RUS","RBDG SECD RI","RBDG RI","RBDG CT","RBDG SECD CT");
				}
				$queryStringItems = array();
				foreach ($_GET as $key=>$val){
					$queryStringItems[] = $key."=".$val;
				}
				$queryString = implode("&",$queryStringItems);
				if (count($_POST)){
					foreach ($_POST as $key=>$val){
						$keyParts = explode("|",$key);
						$SF270Data[str_replace("_"," ",$keyParts[0])][$keyParts[1]] = $val;
					}
				}
				echo "Enter any SF270 Expenses or InKind amounts that should appear on the SF270 form then click 'Create SF270 PDFs' button<br>";
				echo "<form name='sfData' id='sfData' method='POST' action='?".$queryString."#Files'><table class='simpleTable'>";
				echo "<thead><tr><th>type</th>";
				foreach ($SFTableHeaders as $accountType){
					echo "<th>".$accountType."</th>";
				}
				echo "</tr></thead>";
				echo "<tbody>";
				$types = array("Expenses","InKind");
				foreach ($types as $type){
					echo "<tr><td>".$type."</td>";
					foreach ($SFTableHeaders as $accountType){
						$thisName = str_replace(" ","_",$accountType)."|".$type;
						echo "<td>$<input type='text' style='width:50px;padding-left:5px;' value='".($_POST[$thisName] ? $_POST[$thisName] : "0.00")."' name='".$thisName."'></td>";
					}
					echo "</tr>";
				}
				echo "</tbody></table></form>";
				echo "<div style='float:right;'><a href='#' id='CreatePDFS' class='button-link'>Create SF270 PDFs</a></div><br><br>";
				include_once('salesforce/reports/reportRUSHours.php');
				echo $ReportsFileLink;
				//Now create the 270PDFS
				$path = getcwd();
				if (!strpos($path,"xampp")){
					$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
					set_include_path(".:/home/cetdash/pear/share/pear");
					$thispath = $path;
				}else{
					$testingServer = true;
					$path = "";
					$thispath = "../";
				}
				$relativeLink = "../residential/";				
				require($thispath.'residential/pdfProcessor/fpdm.php');
				
				if (count($SF270Data) && count($_POST)){
					foreach ($SF270Data as $sf270Name=>$data){
						// echo $sf270Name."<Br>";
						// print_pre($data);
						if ($data["SF270Name"]) {
							$fileName = str_replace(" ","-",$data["SF270Name"])."-SF270-".date("Y-m", strtotime($invoiceDate)).".pdf";
							// echo $fileName;
							$filePath = $thispath."gbs/sf270Files/".$fileName;
							$data["sponsor"] = "USDA - Rural Development";
							$data["CertificationNameAndTitle"] = "Heather McCreary, Information Services Manager";
							$data["CertificationPhone"] = "423-586-7350 x 235";
							$personellTot = $data["PriorPersonell"]+$data["Personell"];
							$data["PersonellSumA"] = money($personellTot);
							$data["PersonellSumC"] = money($personellTot);
							$data["PersonellSumE"] = money($personellTot);
							$data["PersonellSumG"] = money($personellTot);

							$expensesTot = $data["PriorExpenses"]+$data["Expenses"];
							$data["ExpensesSumA"] = money($expensesTot);
							$data["ExpensesSumC"] = money($expensesTot);
							$data["ExpensesSumE"] = money($expensesTot);
							$data["ExpensesSumG"] = money($expensesTot);
							$data["TotalA"] = money($personellTot + $expensesTot);
							$data["TotalC"] = money($personellTot + $expensesTot);
							$data["TotalE"] = money($personellTot + $expensesTot);
							$data["TotalG"] = money($personellTot + $expensesTot);
							$data["TotalF"] = money($data["InKind"]);
							$data["TotalH"] =money( $data["PriorExpenses"]+$data["PriorPersonell"]);
							$data["TotalI"] = money($data["Personell"]+$data["Expenses"]);
							$data["ProgramA"] = "Personell";
							$data["ProgramB"] = "Expenses";
							$data["PriorPersonell"] = money($data["PriorPersonell"]);
							$data["Personell"] = money($data["Personell"]);
							$data["ProgramC"] = "In-Kind";
							$data["PriorExpenses"] = money($data["PriorExpenses"]);
							$data["Expenses"] = money($data["Expenses"]);
							// process the pdfs
							$data = array_map('strval', $data);
							$data["fileLocation"] = $CurrentServer.$adminFolder."gbs/sf270Files/".$fileName;
							$data["absolutePath"] = $thispath."gbs/sf270Files/".$fileName;
							$dataArray[$data["SF270Name"]] = $data;
							// echo $data["fileLocation"];
							
							// print_pre($data);

							// echo "TEST";
							$results = $ResidentialProvider ->insertSF270Data($data,"RUS");
							// print_pre($data["Expenses"]);
							
						}else{
							echo "<br><span class = 'info alert'>No ".$sf270Name." data to display</span>";
						}
						
						
					}
					$sf270FileName = "RUS_";
					include_once('salesforce/reports/sf270Hours.php');
					echo "<br><fieldset><legend>SF270 Files</legend><div id='SF270Links'></div><br>".$sf270HoursFile."</fieldset>";
				}
				
				

		echo "</div>";
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "Small Consultant");
		include_once('_notesTab.php');
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Files tab, download the Hours File and send to Quinn.
</ul>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		$("#CreatePDFS").on('click',function(e){
			e.preventDefault();
			$("#sfData").submit();
		});
		<?php if(count($_POST)){?>
			var sf270Data = <?php echo json_encode($dataArray);?>;
			console.log(sf270Data);
			$.each(sf270Data,function(key,val){
				console.log(key);
				console.log(val);
				$.ajax({
					url: "SF270FormFill.php",
					type: "POST",
					data: JSON.stringify(val),
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					success: function(resultdata){
						$("#SF270Links").append(resultdata);
						console.log(resultdata);
					}
				});
			})
		<?php }?>

		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>
