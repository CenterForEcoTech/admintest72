<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Boston Zero Waste Invoice files</h3>
<?php
$_SESSION['SalesForceReport'] = "BostonZero";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$billingRatesArray = array("BostonZero"=>array("551")); //uses same as PollutionPrevention
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}
//print_pre($billingRatesArray);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Boston Zero Waste - 551";
			$SelectedGroupsID = 66;
			$InvoiceData = array();
			$InvoiceHourData = array();
			$navDropDown = "invoicingTool-BostonZero";
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					$code = (string)$code;
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			//print_pre($InvoiceHourData);
			//print_pre($BillingRateByCategory);
			$billableEmployees = array("Billings_Heather","Fabel_Emily","Macaluso_Lorenzo"); //indicated by Emily Fabel these are the only acceptable hours to be billed (as of April 2018)
			//print_pre($billableEmployees);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						$InvoiceCode = (string)$InvoiceCode;
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								//echo $Employee_Name;
								if (in_array($Employee_Name,$billableEmployees)){
									if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
										if ($hours && $hours != "0.00"){
											$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
											$IncludedEmployees[] = $Employee_Name;
										}
									}
									if (($BillingCategory == "Support Staff") && !in_array($Employee_Name,$IncludedEmployees)){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$Adjustments["Alert"]["Staff missing Billing Rate for ".$BillingRateType][]=str_replace("_",", ",$Employee_Name);
									}
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once 'salesforce/config.php';
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00O0P000003TRVN"; //Invoicing/551 Boston Zero Waste

			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				$_SESSION['SalesForceReport'] = "BostonZero";
				$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
		
			$reportId = "00O0P000003TRVN"; //Invoicing/551 Boston Zero Waste
			$reportName = "551 Boston Zero Waste";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$reportBullets[] = $currentStatus;
				}
			}
			
			//Include Events too!
			$reportId = "00O0P000003Td1i"; //Invoicing/Boston Zero Waste Events
			$reportName = "Boston Zero Waste Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$eventCounter = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$eventDate = $rowDetail["START_DATE_TIME"];
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				if (strtotime($eventDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($eventDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$Events[strtotime($eventDate)]["completed"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
				if (strtotime($eventDate) > strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$Events[strtotime($eventDate)]["upcoming"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
			}
			ksort($Events);
			
			
			
			
				//set previously invoiced	
				include_once($dbProviderFolder."InvoicingProvider.php");
				$invoicingProvider = new InvoicingProvider($dataConn);
	
				$InvoiceNumber["BostonZero"] = "551-".date("y-m",strtotime($invoiceDate));

				$criteria = new stdClass();
				$criteria->invoiceCode = $InvoiceCode;
				$criteria->invoiceNumber = $InvoiceNumber["BostonZero"];
				$criteria->startDate = '2017-04-01';
				$priorInvoiceAmount = $invoicingProvider->getInvoicingAmountHistory($criteria);
				//print_pre($priorInvoiceAmount);
				foreach ($priorInvoiceAmount as $record){
					$previousTotalAmount = bcadd($previousTotalAmount,$record["invoiceAmount"],2);
					$previousDetails .= "Invoice ".$record["invoiceNumber"]." Date: ".date("m/d/Y",strtotime($record["invoiceDate"]))." Amount: $".money($record["invoiceAmount"])."<br>";
				}
				echo "Previously Invoiced Amount Total = <span id='previousAmount' style='text-decoration:underline;cursor:pointer;' title='click for details'>$".money($previousTotalAmount)."</span>
					<div id='previousDetails' style='display:none;'>".$previousDetails."</div><br>";
			
				include_once('salesforce/reports/reportBostonZero.php');
				echo $ReportsFileLink;
				
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "BostonZero");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the File: <?php echo $ReportsFileLink;?>.
</ul>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		
		$("#previousAmount").on('click',function(){
			$("#previousDetails").toggle();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>
