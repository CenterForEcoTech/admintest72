<?php
//error_reporting(E_ALL);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$_SESSION['SalesForceReport'] = "RWTAPipeline";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}

$attachmentFolder="RW";

include_once($dbProviderFolder."GBSProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date("m/01/Y")." -1 month")));
//echo $invoiceDate;
$lastDateofInvoiceDate = date("m/t/Y",strtotime($invoiceDate));

$NONQuartersStart = (date("m") < 7 ? (date("Y")-1) : date("Y"))."-07-01";
$invoiceQuarter = $NONQuartersStart;
while (strtotime(date("Y-m-d",strtotime(date()."-1 month"))) >= strtotime($NONQuartersStart)){
	$invoiceQuarter = $NONQuartersStart;
	$NONQuartersStart = date("Y-m-d",strtotime($NONQuartersStart."+3 Months"));
}

$CurrentYear = (date("n") > 7 ? date("Y") : (date("Y")-1));
if (date("m") == "01"){
	//$CurrentYear = $CurrentYear-1;
}
$NextYear = $CurrentYear+1;
$CurrentFiscalYear = (date("n") > 7 ? $CurrentYear : $NextYear);
$CurrentFiscalYear = $CurrentYear;
$CurrentFY = $CurrentFiscalYear."-07-01";
//echo "Current FY: ".$CurrentFY;
if(count($_FILES['filesToUpload'])) {
	$fileReceived = true;
	foreach ($_FILES['filesToUpload']["tmp_name"] as $fileKey=>$fileValue) {
		$folderLocationToGet = $fileValue;
		$fileLocations[] = $folderLocationToGet;
	}
}

//Get Billing Rates
$criteria = new stdClass();
$criteria->invoiceName = "RecyclingWorks";
$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	$BillingRateByCategory[$record->GBSBillingRate_Name]=$record;
	foreach ($EmployeeMembers as $Employee){
		$BillingRateByEmployee[$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		$BillingRateByEmployeeJustRate[$Employee] = $record->GBSBillingRate_Rate;
	}
}

function csv_to_array($string='', $row_delimiter=PHP_EOL, $delimiter = "," , $enclosure = '"' , $escape = "\\" )
{
    $rows = array_filter(explode($row_delimiter, $string));
    $header = NULL;
    $data = array();

    foreach($rows as $row)
    {
        $row = str_getcsv ($row, $delimiter, $enclosure , $escape);
		
        if(!$header)
            $header = $row;
        else
            $data[] = array_combine($header, $row);
    }

    return $data;
}

$csvRows = array();
foreach ($fileLocations as $file){
		$fileLocation = str_replace("\\","/",$file);
		$csvstring = file_get_contents($fileLocation,"r");
		$csv[] = csv_to_array($csvstring,"\r\n");
		$csvRows = $csvRows + $csv;
}
//print_pre($csv);
//now combine each of the csv into one array
foreach ($csv as $id=>$csvRowData){
	foreach ($csvRowData as $row=>$rowInfo){
		$csvCombined[] = $rowInfo;
	}
}

//check for consistancy
foreach ($csvCombined as $rowId=>$rowContent){
	if (!is_array($rowContent)){
		echo "row ".$rowId." has bad formating<br>";
	}
}
//print_pre($csvCombined);
if ($isStaff){
	echo "<h3>This tool will create RW Report, NON Pipeline, and TA Pipeline Report</h3>";
	$setMasterTab = "NON";
	$masterTabCodes = array("NON Pipeline Report"=>"NON","TA Pipeline Report"=>"RWReport","RW Report"=>"Bullets","Adjustments"=>"Adjustments","Notes"=>"Notes");
}else{
	echo "<h3>This tool will create RW Invoice</h3>";
	$setMasterTab = "Hours";
	$masterTabCodes = array("Hours"=>"Hours","RW Invoice"=>"Bullets","Adjustments"=>"Adjustments","Notes"=>"Notes");
}
foreach ($masterTabCodes as $TabName=>$TabCode){
	$masterTabHashes[] = $TabCode;
	$masterTabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
}
echo '<ul id="admin-tabs" class="tabs">';
		foreach ($masterTabListItems as $TabItem){
			echo $TabItem;
		}
echo '</ul>';		
echo '<div class="tabs-content">';
if (!$isStaff){
	echo "<div id='Hours' class='tab-content'>";
		$InvoiceTool = true;
		$InvoicingToolCodeGroup = "Recycling Works";
		$SelectedGroupsID = 12; //Recycling Works
		include('views/_reportByCode.php');
		//echo "<br clear='all'>";
		$IncludedEmployees = array();
		$ExcludedEmployees = array();
		foreach ($InvoiceData as $Employee_Name=>$hours){
//				echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
//				print_pre($hours);
			foreach ($hours as $code=>$hour){
				if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
					$code = ($code == "534" || $code == "539" || $code == "537A" || $code == "537B" || $code == "537C" ? "534/539/537AB" : $code); //LH added 537C 9.27.2019
					
					$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
					$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
				}
			}
		}
		ksort($InvoiceHourData);
		//print_pre($InvoiceHourData);
		foreach ($BillingRateByCategory as $BillingCategory=>$RateInfo){
			foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
				//echo "InvoiceCode ".$InvoiceCode."=";
				//echo $InvoiceCode."<br>";
				$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
				foreach ($EmployeeHours as $Employee_Name=>$hours){
					if (!$EmployeesRevenueCodeByName[$Employee_Name]){
						$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
					}
					if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingCategory]){
						if ($hours && $hours != "0.00"){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							$IncludedEmployees[] = $Employee_Name;
						}
					}
					if ($Employee_Name != "Cook_Josh"){ //ignore Josh Cook because billed at flat rate of hours
						if ($BillingCategory == "EcoFellow" && !in_array($Employee_Name,$IncludedEmployees)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							$Adjustments["Alert"]["Staff missing Billing Rate"][]=str_replace(", ","_",$Employee_Name);
						}
					}
				}
			}
			
		}
		//$InvoiceDataHours["DEP"]["NoCategory"]["Macaluso_Lorenzo"] = 2.50; //updated 11/14/2016
		//$InvoiceDataHours["DEP"]["NoCategory"]["Cook_Josh"] = 82.00; //updated 11/14/2016
//		$InvoiceDataHours["DEP"]["NoCategory"]["Macaluso_Lorenzo"] = 0; //updated 08/07/2017
		$InvoiceDataHours["DEP"]["NoCategory"]["Kohler_Lisa"] = 2.50; //updated 08/07/2017
		$InvoiceDataHours["DEP"]["NoCategory"]["Cook_Josh"] = 84.00; //updated 08/07/2017
		//print_pre($InvoiceDataHours);
		
		
		//print_pre($InvoiceHourData);
	
	echo "</div>";
}//end if !isStaff
//echo "CSVCombined= ";
//var_dump $csvCombined;
require_once 'salesforce/config.php';
require_once('salesforce/soap_connect.php');
require_once('salesforce/rest_functions.php');
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
$reportId = "00OU0000003e4x6"; //Invoicing/RW TA In-Progress

//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
if (!checkSession($instance_url, $reportId, $access_token)){
	session_start();
	$_SESSION['SalesForceReport'] = "BGAS";
	$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
	echo "<script>window.location.replace('".AUTH_URL."');</script>";
	//echo AUTH_URL;
	//header('Location: '.AUTH_URL);
}else{					
	ob_start();
	
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
}//end if checkSession
function normalizeAccountName($name){
	$name = str_replace(".","",$name);
	$name = str_replace(" ","",$name);
	$name = str_replace("'","",$name);
	$name = str_replace(",","",$name);
	$name = strtolower($name);
	return $name;
}


if ($isStaff){
	echo "<div id='NON' class='tab-content'>";
			$ImportFileName = "importfiles/NONFile".date("Y_m",strtotime($invoiceQuarter)).".xlsx";
			echo "ImportFileName ".$ImportFileName."<br>";
			$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
			//echo $target_file;
			$link_file = $CurrentServer.$adminFolder."gbs/".$ImportFileName;
			if(count($_FILES['filesToUpload1'])) {
				$tmpFileName = $_FILES["filesToUpload1"]["tmp_name"][0];
				if ($_GET['type'] == "nonFile"){
					$ImportFileReceived = true;
					move_uploaded_file($tmpFileName, $target_file);
					//$results = $GBSProvider->invoiceTool_loadExternalData($target_file,"gbs_bgas_customer_accounts");
					//print_pre($results);
				}
			}
			if ($ImportFileReceived || $_GET['useUploadedFile']){
				//new import that data and parse it out 
				$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $target_file);
				$_SESSION['ImportFileName'] = $ImportFileName;
				echo "Reading <a href='".$link_file."'>uploaded NON file</a><br>";
				ob_flush();
				include_once('salesforce/read_NONFile.php');
			}
			if (!$ImportFileReceived){
				if (file_exists($target_file)){
					$lastUploadedDateParts = explode("File",$ImportFileName);
					$lastNONUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
					echo "Using <a href='".$link_file."'>Existing Uploaded NON File</a> last uploaded ".date("F Y",strtotime($lastNONUploadedDate))."<br>";
					$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
					$trackingFile = $ImportFileName;
					include_once('salesforce/read_NONFile.php');
				}else{
					echo "No Prior NON File uploaded.  Please upload the most recent NON File<br>";
				}
			}
		
	?>
				<a id='updateNONFile' href="#" class='button-link do-not-navigate'>Update NON File</a><br>
				<div id="updateNONFileForm" style='display:none;'>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?nav=invoicingTool-RWTAPipeline&type=nonFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload1[]" id="filesToUpload1" multiple="" onChange="makeFileList1();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList1"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList1() {
								var input = document.getElementById("filesToUpload1");
								var ul = document.getElementById("fileList1");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
						</script>
					</form>
				</div>
		<?php
			$reportId = "00OU0000003e4x6"; //Invoicing/RW TA In-Progress
			$reportName = "RW TA In-Progress";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			//echo " <br> In progress: <br>";
			//print_pre($RWTAInProgress);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignNameParts = explode(" ",$primaryCampaignName);
				$primaryCampaignCode = ($primaryCampaignNameParts[0] == "534" || $primaryCampaignNameParts[0] == "537A" || $primaryCampaignNameParts[0] == "537B" || $primaryCampaignNameParts[0] == "537C" ? "534/537AB" : $primaryCampaignNameParts[0]); //LH added 537C into array 9/27/2019
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$DEPReportSubmitted = $rowDetail["Upgrade__c.RW_DEP_Summary_Report_Submitted__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$billingAddress = str_replacE("<br>","",$rowDetail["Account.BillingAddress"]);
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				//$pipeline = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				$RWTAInProgress[$AccountNameLink] = $rowDetail;
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				$TAInfo = array("Account Name"=> $accountName,
					"Initiation Date" => date("m/d/Y",strtotime($initiationDate)),
                    "Last Action Date" => date("m/d/Y",strtotime($lastActionDate)),
					"Green Prospect Phase" => $greenProspectPhase,
					"Pipeline" => $pipeline,
					"RW DEP Summary Report Submitted" => $DEPReportSubmitted,
                    "Owner: Full Name" => $owner,
					"Green Prospect Name" => $greenProspectName,
					"Green Prospect Type" => $prospectType,
					"Number" => $number,
                    "Unit" => $unit,
                    "Material" => $material,
                    "Other: Explain" => $otherExplain,
					"Current Status Detail" => $statusDetail,
                    "Address" => $billingAddress,
					"Primary Campaign: Campaign Name" => $primaryCampaignName,
					"PrimaryCampaignCode" => $primaryCampaignCode,
					"Opportunity Lead Origination" => $opportunityLead
					);
				$TAPipelineItems[] = $TAInfo;	
			}
			echo $reportLink;
			//echo " <br> In progress: <br>";
			//print_pre($RWTAInProgress);
			//print_pre($TAPipelineItems);
			
			$reportId = "00OU0000003e4xz"; //Invoicing/RW TA Completed 2015-2016
			$reportName = "RW TA Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//			print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignNameParts = explode(" " ,$primaryCampaignName);
				$primaryCampaignCode = ($primaryCampaignNameParts[0] == "534" || $primaryCampaignNameParts[0] == "537A" || $primaryCampaignNameParts[0] == "537B" || $primaryCampaignNameParts[0] == "537C" ? "534/537AB" : $primaryCampaignNameParts[0]); //LH tested Mini TAs, doesn't work  $primaryCampaignNameParts[0] == "539" ||
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$DEPReportSubmitted = $rowDetail["Upgrade__c.RW_DEP_Summary_Report_Submitted__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$billingAddress = str_replace("<br>","",$rowDetail["Account.BillingAddress"]);
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				//$pipeline = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				if ($primaryCampaignName != "539 Mini TA"){
					$RWTACompleted[$AccountNameLink] = $rowDetail;
				} //Lauren commented out if
				$TAInfo = array("Account Name"=> $accountName,
					"Initiation Date" => date("m/d/Y",strtotime($initiationDate)),
                    "Last Action Date" => date("m/d/Y",strtotime($lastActionDate)),
					"Green Prospect Phase" => $greenProspectPhase,
					"Pipeline" => $pipeline,
					"RW DEP Summary Report Submitted" => $DEPReportSubmitted,
                    "Owner: Full Name" => $owner,
					"Green Prospect Name" => $greenProspectName,
					"Green Prospect Type" => $prospectType,
					"Number" => $number,
                    "Unit" => $unit,
                    "Material" => $material,
                    "Other: Explain" => $otherExplain,
					"Current Status Detail" => $statusDetail,
                    "Address" => $billingAddress,
					"Primary Campaign: Campaign Name" => $primaryCampaignName,
					"PrimaryCampaignCode" => $primaryCampaignCode,
					"Opportunity Lead Origination" => $opportunityLead
					);
				$TAPipelineItems[] = $TAInfo;	
			}
			echo "<br>".$reportLink;
			//echo "Test TA Pipeline items: ";
			//print_pre($TAPipelineItems);
			//echo "<br> test in TA Pipeline: ".$inTAPipeline;
			//echo "<br> Test: ";
			//var_dump ($RWTACompleted);
			
			//find any outliers who had previous closed TA's so that if they  have a current TA they get 'Yes'
			$reportId = "00O0P000002yS7X"; //Invoicing/RW TA Complete Prior to Current FY
			$reportName = "RW TA Complete Prior to Current FY";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$RWTACompletedOutlier[$AccountNameLink] = $accountName;
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
			}
			echo "<br>".$reportLink;
			//echo "<br> Completed: <br>";
			//print_pre($RWTACompleted);
			
			$reportId = "00O0P000003uxcYUAQ"; //Invoicing/RecyclingWorks Hotlines - for reporting
			//$reportId = "00O0P000003uqZJUAY"; //Invoicing/RecyclingWorks Hotlines - New for FY20
			$reportName = "Invoicing/RecyclingWorks Hotlines - for reporting";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$dataAnalysisThisMonth["previouslyOpen"]++; //counts previously open hotline calls
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRSkUAM"; //Invoicing/RecyclingWorks Hotlines - for reporting
			//$reportId = "00O0P000003uqZJUAY"; //Invoicing/RecyclingWorks Hotlines - New for FY20
			$reportName = "YTD RW Events Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$YTDRWevents++; //counts previously open hotline calls
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRT9UAM"; //Invoicing/RecyclingWorks Hotlines - for reporting
			//$reportId = "00O0P000003uqZJUAY"; //Invoicing/RecyclingWorks Hotlines - New for FY20
			$reportName = "MTD RW Events Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$MTDRWevents++; //counts previously open hotline calls
			}
			echo "<br>".$reportLink;
			
			//var_dump ($dataAnalysisThisMonth);
			$reportId = "00O0P000003vRNQUA2"; //RW Total Requests Receiving Assistance
			$reportName = "RW Total Requests Receiving Assistance";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$totalRequestsYTD++; //counts previously open hotline calls
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRPvUAM"; //RW MTD Requests Receiving Assistance
			$reportName = "RW MTD Requests Receiving Assistance";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$totalRequestsMTD++; //counts total for month
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRQ0UAM"; //RW New Requests
			$reportName = "RW New Requests";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$newRequestsMTD++; //counts total for month
				$sector = $rowDetail["Account.Sector_c__c"];
				if ($sector == "Processor" || $sector == "Hauler"){
					$newRequestsProc++;
				}else{
				$newRequestsBI++;
				}
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRRNUA2"; //YTD RW New Requests
			$reportName = "YTD RW New Requests";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$newRequestsYTD++; //counts total for month
				$sector = $rowDetail["Account.Sector_c__c"];
				if ($sector == "Processor" || $sector == "Hauler"){
					$YTDnewRequestsProc++;
				}else{
				$YTDnewRequestsBI++;
				}
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRS1UAM"; //YTD RW Completed Requests
			$reportName = "YTD RW Completed Requests";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$completedRequestsYTD++; //counts total for month
				$sector = $rowDetail["Account.Sector_c__c"];
				if ($sector == "Processor" || $sector == "Hauler"){
					$YTDCompRequestsProc++;
				}else{
				$YTDCompRequestsBI++;
				}
			}
			echo "<br>".$reportLink;
			
			$reportId = "00O0P000003vRSBUA2"; //MTD RW Completed Requests
			$reportName = "MTD RW Completed Requests";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$completedRequestsMTD++; //counts total for month
				$sector = $rowDetail["Account.Sector_c__c"];
				if ($sector == "Processor" || $sector == "Hauler"){
					$MTDCompRequestsProc++;
				}else{
				$MTDCompRequestsBI++;
				}
			}
			echo "<br>".$reportLink;
			
			
			$reportId = "00OU0000003e1Sb"; //Invoicing/RW NON Completed
			$reportName = "RW NON Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$NONCompletedReport[$AccountNameLink] = 1;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				//$pipeline = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$DEPReportSubmitted = $rowDetail["RW DEP Summary Report Submitted"];
				$owner = $rowDetail["owner"];
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$stageCompleted = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				
				//echo strtotime($lastNONUploadedDate)." > ".strtotime($lastActionDate)."<br>";
				//echo normalizeAccountName($accountName)[$NONType]."<br>";
				//echo "NONType: ".$NONType."<br>";
				
				$inTAPipeline = ($RWTACompleted[$AccountNameLink] || $RWTAInProgress[$AccountNameLink] || $RWTACompletedOutlier[$AccountNameLink] ? true : false);
				$NONType = ($NONDataNormalized[normalizeAccountName($accountName)] ? "outreach" : "completed");
				$NONType = (strtotime($lastNONUploadedDate) > strtotime($lastActionDate) ? $NONType : "completed");
				$NONType = ($NONType=="outreach" && !strpos(" ".$primaryCampaignName,"533") ? "TA" : $NONType); //Lauren Referred to TA set here
				$TA = ($inTAPipeline ? "Yes" : "");
				$TA = ($inTAPipeline && strpos(" ".$primaryCampaignName,"539") && $NONType == "TA" ? "" : $TA); //LH add opportunity lead origination here?
				if ($greenProspectPhase == "Completed-Measure Not Implemented" || $greenProspectPhase == "Completed-Measure Implemented" || $greenProspectPhase == "Implementation-In-Progress" || $greenProspectPhase == "Completed-Back in Compliance"){$NONType = "completed";$TA = "";}
				$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase,"opportunityLead"=>$opportunityLead,"primaryCampaign"=>$primaryCampaignName,"prospectType"=>$prospectType,"owner"=>$owner,"DEPReportSubmitted"=>$DEPReportSubmitted);
				
				//echo "completed '".normalizeAccountName($accountName)."'<br>";
				//echo "Normalized: ".$NONDataNormalized[normalizeAccountName($accountName)]."<br>";
				//echo "Array dump: ";
				//var_dump ($NONDataNormalized)."<br>";
				
				$NONAccounts[normalizeAccountName($accountName)][$NONType][$greenProspectPhase][$lastActionDate][] = $infoArray;
				$NONOutReach[normalizeAccountName($accountName)] = $infoArray;
				$NONOutReachByStatus[$NONType][$AccountNameLink] = $infoArray;
				$NONOutReachByAddress[$normalizedAddress] = normalizeAccountName($accountName);
				$NONOutReachByAccount[$AccountNameLink] = $infoArray;
				//echo "<br>Info array: <br>";
				//var_dump ($infoArray);
				//echo "<br>";
				//$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink);
			}
			echo "<br>".$reportLink;
			//echo "Outliers: <br>";
			//print_pre($RWTACompletedOutlier);

			
			$reportId = "00OU0000003e1SM"; //Invoicing/RW NON Outreach GPs
			$reportName = "RW NON Outreach GPs";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				//$pipeline = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$stageCompleted = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$NONType = (strpos(" ".$greenProspectPhase,"Completed") ? "completed" : "outreach");
				$inTAPipeline = ($RWTACompleted[$AccountNameLink] || $RWTAInProgress[$AccountNameLink] ? true : false);
				$NONType = ($NONType=="outreach" && !strpos(" ".$primaryCampaignName,"533") ? "TA" : $NONType);  //disabled August 2016 to combine with outreach
				$TA = ($inTAPipeline ? "Yes" : "");
				$TA = ($inTAPipeline && strpos(" ".$primaryCampaignName,"539") && $NONType == "TA" ? "" : $TA);  //disabled August 2016 to combine with outreach
				$NONType = (strpos(" ".$greenProspectPhase,"Completed") ? "completed" : "outreach"); //added August 2016 to only have two groups ie eliminate TA type
				if ($greenProspectPhase == "Completed-Measure Not Implemented"){$NONType = "completed";$TA = "";}
				$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase,"opportunityLead"=>$opportunityLead);
				if ($NONType == "completed" && $NONAccounts[normalizeAccountName($accountName)]["TA"]){
					unset($NONAccounts[normalizeAccountName($accountName)]["TA"]);
				}
				$includeThisItem = true;
				//have only completed items for current FY
				$withinFY = ($NONType == "completed" && strtotime($lastActionDate) >= strtotime($CurrentFY) ? true : false);
				if ($NONType == "completed"){
					//echo $accountName." ".$NONType." ".$lastActionDate." >=".$CurrentFY." '".$withinFY."'<br>";
					if ($withinFY){
						$includeThisItem = true;
					}else{
						$includeThisItem = false;
					}
				}
				if ($includeThisItem){
					$NONAccounts[normalizeAccountName($accountName)][$NONType][$greenProspectPhase][$lastActionDate][] = $infoArray;
				
					//echo "outreach '".normalizeAccountName($accountName)."'<br>";
					$Added[] = normalizeAccountName($accountName);
					$NONOutReach[normalizeAccountName($accountName)] = $infoArray;
					$NONOutReachByStatus[$NONType][$AccountNameLink] = $infoArray;
					$NONOutReachByAddress[$normalizedAddress] = normalizeAccountName($accountName);
					$NONOutReachByAccount[$AccountNameLink] = $infoArray;
					//$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink);
				}
			}
			
			echo "<br>".$reportLink."<br>";
			//echo "<br> Added: <br>";
			//var_dump ($Added)."<br>";
			//echo "<br>";
			
			//echo normalizeAccountName($accountName)[$NONType];
			
			ksort($NONAccounts);
			
			//print_pre($NONOutReachByAccount);
			

			$reportId = "00OU0000003e10X"; //Invoicing/Business Accounts
			$reportName = "Business Accounts";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//			print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingAddress"]);
				$addressParts = explode(",",$address);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				//$address = $street.", ".$city;
				$addressToLower = trim(strtolower($addressParts[0]));
				//$normalizedAddress = rtrim(str_replace(" street","",trim(strtolower($addressParts[0])))," st");
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectStage = $rowDetail["Upgrade__c.Green_Prospect_Stage__c"];
				$greenProspectStatus = $rowDetail["Upgrade__c.Green_Prospect_Status__c"];
				$greenProspectDetails = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$gpDetails = array("stage"=>$greenProspectStage,"status"=>$greenProspectStatus,"details"=>$greenProspectDetails);
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$SFAccounts[normalizeAccountName($accountName)][$GPNameLink] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"GPInfo"=>$gpDetails,"reportLink"=>$reportLink);
				$SFAccounts[normalizeAccountName($accountName)]["accountInfo"] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"reportLink"=>$reportLink);
				$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"GPInfo"=>$gpDetails,"reportLink"=>$reportLink);
				$SFAccountsByAddress[$normalizedAddress] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"addressStrToLower"=>strtolower($addressParts[0]),"addressStrReplace_street"=>str_replace(" street","",strtolower($addressParts[0])),"has_street"=>$has_street,"has_st"=>$has_st,"city"=>$city);
				$SFAccountsByAccountLink[$AccountNameLink] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"addressStrToLower"=>strtolower($addressParts[0]),"addressStrReplace_street"=>str_replace(" street","",strtolower($addressParts[0])),"has_street"=>$has_street,"has_st"=>$has_st,"city"=>$city);
			}
			
			//print_pre($SFAccountsByAddress);
			//print_pre($NONData);
			if (count($NONData)){
				foreach ($NONData as $NONRow){
					$accountName = trim($NONRow["reg_obj_name"]);
					$billingAddress = trim($NONRow["reg_obj_mail_addr"]);
					$billingCity = trim($NONRow["town_name"]);
					
					$addressToLower = strtolower($billingAddress);
					$has_street = (substr($addressToLower,-7)==" street" ? true : false);
					$has_st = (substr($addressToLower,-3)==" st" ? true : false);
					$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
					$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
					if ($accountName){
						$matchFound = false;
						if (count($SFAccounts[normalizeAccountName($accountName)])){
							$matchFound = true;
							if ($SFAccountsByAddress[$normalizedAddress]){
								$Adjustments["Review"]["NON Accounts found via Name and Address exact match"][] = $SFAccountsByAddress[$normalizedAddress]["accountNameHref"];
								$accountNameLink = $SFAccountsByAddress[$normalizedAddress]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
							}else{
								$Adjustments["Alert"]["NON Accounts found via Name exact match but with mismatched Address"][] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["accountNameHref"]."<Br>Data in Sales Force:&nbsp;&nbsp;".$SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"]."<Br>Data in NON Report: ". $billingAddress.", ".$billingCity;
								$accountNameLink = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
							}
							$AccountNamesLookup[trim($accountName)] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"];
							$AccountNamesLookup[$SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["name"]] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"];
							
						}else{
							if ($SFAccountsByAddress[$normalizedAddress]){
								$matchFound = true;
								$Adjustments["Alert"]["NON Accounts only found by Address exact match but with mismatched Name"][] = strtoupper($SFAccountsByAddress[$normalizedAddress]["accountNameHref"])." vs NON Data: ".$accountName;
								$accountNameLink = $SFAccountsByAddress[$normalizedAddress]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
								$AccountNamesLookup[trim($accountName)] = $normalizedAddress;
								$AccountNamesLookup[$SFAccountsByAddress[$normalizedAddress]["name"]] = $normalizedAddress;
							}
						}
						$NONType = ($NONType ? : "outreach");
						if (!$matchFound){
							//last check 
							if ($NONAccounts[normalizeAccountName($accountName)]){
								$Adjustments["Review"]["NON Accounts found in NONAccounts"][] = $accountName;
								foreach ($NONAccounts[normalizeAccountName($accountName)] as $NONType=>$info){
									$NONType = $NONType;
								}
								$AccountNamesLookup[trim($accountName)] = $normalizedAddress;
								$NONAccountsFound[$NONType][] = $NONOutReach[normalizeAccountName($accountName)]["accountNameLink"];
							}else{
								$thisZip = retrieve_zipcode($billingAddress.", ".$billingCity." MA ",GOOGLE_APIKEY);
								$nameParts = explode(" ",$accountName);
								$addLink = "<a title='click to prepopulate Add Account in SalesForce' target='SalesForceAddAccount' href='".$instance_url."/001/e?retURL=%2F001%2Fo&RecordType=012U00000009QUy&ent=Account&acc2=".urlencode($accountName)."&acc17street=".urlencode($billingAddress)."&acc17city=".urlencode($billingCity)."&acc17state=MA&acc17zip=".$thisZip."'>".$accountName."</a>";
								$searchLink = "<a title='click to search in SalesForce' target='SalesForceSearch' href='".$instance_url."//_ui/search/ui/UnifiedSearchResults?searchType=2&sen=001&sen=a01&sen=00O&str=".$nameParts[0]."'>".$accountName."</a>";
								$Adjustments["Review"]["NON New Accounts (not found in SF via Name or Address)<br><a href='".$instance_url."/001/o' target='salesForce'>Add New Account To SalesForce</a>"][] = $searchLink." ".$billingAddress.", ".$billingCity." MA ".$thisZip;
								$AccountNamesLookup[trim($accountName)] = "NEW";
								$NONAccountsNotFound[] = $searchLink." ".$billingAddress.", ".$billingCity." MA ".$thisZip;
								$accountInfoArray = array("name"=>"NEW ".ucwords($accountName),"city"=>$billingCity,"GPName"=>"NON Outreach","TA"=>"No","initiationDate"=>date("m/d/Y",strtotime($NONRow["perf_date"])),"lastActionDate"=>date("m/d/Y",strtotime($invoiceQuarter)));
								
								//$NONAccounts[ucwords($accountName)]["outreach"]["Contact-In-Progress"][$invoiceQuarter][] = $accountInfoArray;
							}
						}else{
							$NONAccountsFound[$NONType][] = $accountNameLink;
							$NONAccountsFound[$accountNameLink] = 1;
//							$Phase = $NONOutReachByStatus[$NONType][$accountNameLink]["greenProspectPhase"];
							//$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase);
							//print_pre($NONOutReachByStatus[$NONType][$accountNameLink]);
//							$NONReport[$NONType][$Phase][] = $NONOutReachByStatus[$NONType][$accountNameLink];
//							$NONReportTypeCount[$NONType]++;
						}
					}
				}
/*				echo "NONAccounts Found: ";
				print_pre($NONAccountsFound)."<br>";
				echo "NONAccountsNotFound: "; 
				print_pre($NONAccountsNotFound)."<br>";
				echo "NONAccounts: ";
				print_pre($NONAccounts)."<br>";
				*/
				
				foreach ($NONAccounts as $normalizedAccountName=>$NONTypeInfo){
					$accountName = ($NONOutReach[$normalizedAccountName]["name"] ? : $accountName);
					foreach ($NONTypeInfo as $NONType=>$PhaseInfo){
						foreach ($PhaseInfo as $Phase=>$lastActionDateInfo){
							foreach ($lastActionDateInfo as $lastActionDate=>$accountInfo){
								$NONReport[$NONType][$Phase][] = $accountInfo[0];
								$NONReportTypeCount[$NONType]++;
								if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
								$NONReportTypeCountThisMonth[$NONType]++;
								}
							}
						}
					}
				}
				include_once('salesforce/reports/RW_NON_Pipeline.php');
				echo "<Br>Download NON Pipeline Report: ".$ReportsFileLink."<br>";
				
				
				//print_pre($NONReport);
				echo "<table class='simpleTable'>";
				echo "<thead>
						<tr>
							<th>Type</th>
							<th>Account Name</th>
							<th>City</th>
							<th>Project</th>
							<th>Status</th>
							<th>Referred to TA?</th>
							<th>Initiation Date</th>
							<th>Last Action Date</th>
						</tr>
					  </thead>
					  <tbody>";
				foreach ($NONReport["completed"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						//check if date is within current FY for completed items only
						$withinFY = (strtotime($thisNONAccount["lastActionDate"]) >= strtotime($CurrentFY) ? true : false);
						if ($withinFY){
							echo "<tr>
									<td>Completed</td>
									<td>".$thisNONAccount["name"]."</td>
									<td>".ucwords($thisNONAccount["city"])."</td>
									<td>".$thisNONAccount["GPName"]."</td>
									<td>".$ProjectStatus."</td>
									<td>".$thisNONAccount["TA"]."</td>
									<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
									<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
								</tr>";
						}
					}
				}
				echo "<tr><td colspan=3>Completed: Total</td><td>".$NONReportTypeCount["completed"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				foreach ($NONReport["TA"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						echo "<tr>
								<td>Outreach to TA</td>
								<td>".$thisNONAccount["name"]."</td>
								<td>".ucwords($thisNONAccount["city"])."</td>
								<td>".$thisNONAccount["GPName"]."</td>
								<td>".$ProjectStatus."</td>
								<td>".$thisNONAccount[""]."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
							</tr>";
					}
				}
				echo "<tr><td colspan=3>Outreach to TA: Total</td><td>".$NONReportTypeCount["TA"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				foreach ($NONReport["outreach"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						echo "<tr>
								<td>Projects In-Progress</td>
								<td>".$thisNONAccount["name"]."</td>
								<td>".ucwords($thisNONAccount["city"])."</td>
								<td>".$thisNONAccount["GPName"]."</td>
								<td>".$ProjectStatus."</td>
								<td>".$thisNONAccount[""]."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
							</tr>";
					}
				}
				echo "<tr><td colspan=3>Projects In-Progress: Total</td><td>".$NONReportTypeCount["outreach"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				echo "</tbody></table>";

					
				echo "<h4>Data from DEP NON file</h4>";
				echo "<table class='simpleTable'>";
				echo "<thead>
						<tr>
							<th>Name</th>
							<th>Address</th>
							<th>Town</th>
						</tr>
					  </thead>
					  <tbody>";
				foreach ($NONData as $NONRow){
					$accountName = $NONRow["reg_obj_name"];
					if ($accountName){
						echo "<tr>
								<td>".$accountName."</td>
								<td>".$NONRow["reg_obj_mail_addr"]."</td>
								<td>".$NONRow["town_name"]."</td>
							  </tr>";
					}
				}
				echo "</tbody></table>";
			}
?>
		<b>Instructions for NON Pipeline</b>
		<ul>
			<li>1. If new quarterly file from DEP is available, use the upload feature to add the new file.
			<li>2. Review the Adjustments tab to insure Accounts that are were not matched can be matched.  This may require making changes in Salesforce or ignoring the alerts/reviews.
			<li>3. If you made any changes in Salesforce refresh this page.
			<li>4. Download NON Pipeline report: <?php echo $ReportsFileLink;?>.
		</ul>

<?php
	echo "</div>";
}//end if isStaff
	echo "<div id='Bullets' class='tab-content'>";
		$shiftRows = (isset($_GET['ShiftRows']) ? $_GET['ShiftRows'] : "Up");
		echo "Shift Rows:<select style='width:100px;' name='ShiftRows' id='ShiftRows'><option value=''></option><option value='Up'".($shiftRows == "Up" ? " selected" : "").">Up</option><option value='Down'".($shiftRows == "Down" ? " selected" : "").">Down</option></select><br>";

		if (!$isStaff){
			$ImportFileName = "importfiles/DEPInternalReport".date("Y_m",strtotime($invoiceDate))."1.xlsx";
			$link_file = $CurrentServer.$adminFolder."gbs/".$ImportFileName;
			//echo "ImportFileName ".$ImportFileName."<br>";
			$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
			if(count($_FILES['filesToUpload2'])) {
				$tmpFileName = $_FILES["filesToUpload2"]["tmp_name"][0];
				if ($_GET['type'] == "DEPFile"){
					$ImportFileReceived = true;
					move_uploaded_file($tmpFileName, $target_file);
					//$results = $GBSProvider->invoiceTool_loadExternalData($target_file,"gbs_bgas_customer_accounts");
					//print_pre($results);
				}
			}
			if ($ImportFileReceived || $_GET['useUploadedFile']){
				//new import that data and parse it out 
				$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
				$_SESSION['ImportFileName'] = $ImportFileName;
				echo "Using <a href='".$link_file."'>DEP Internal Report</a> just uploaded<br>";
				ob_flush();
			}
			if (!$ImportFileReceived){
				if (file_exists($target_file)){
					$lastUploadedDateParts = explode("File",$ImportFileName);
					$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
					echo "Using <a href='".$link_file."'>Existing Uploaded DEP File</a> last uploaded ".date("F Y",strtotime($lastDEPUploadedDate))."<br>";
					$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
					$trackingFile = $ImportFileName;
				}else{
					echo "No Prior DEP File uploaded.  Please upload the most recent DEP Internal Report File.  This file is used to re-allocate Lorenzo's DEP hours to other billing categories.<br>";
					$ImportFileName = "importfiles/DEPInternalReport.xlsx";

				}
			}
			$DEPInternalReportFileName = $ImportFileName;
		
	?>
			<b>Instructions</b>
			<ul>
				<li>1. Update the DEP Internal File by clicking the button below and choosing the appropriate FY file from L:\Green Business\Internal Reports\DEP, then clicking the 'Begin Processing' button once the file has been chosen.
				<li>2. Download RW Invoice file from the link below.
				<li>3. Adjust layout in the RW Invoice file to insert rows so that sections are not broken between pages.
				<li>4. Verify the passthrough items have documentation saved to the L drive.
				<li>5. To retrieve the NON Pipeline Report, RW TA Pipeline Report, and the RW Report, logout of the Admin Dashboard and login to the Staff Dashboard to run this same Toolset.
			</ul>
				<a id='updateDEPFile' href="#" class='button-link do-not-navigate'>Update DEP Internal File</a><br>
				<div id="updateDEPFileForm" style='display:none;'>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?nav=invoicingTool-RWTAPipeline&type=DEPFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload2[]" id="filesToUpload2" multiple="" onChange="makeFileList2();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList2"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton2" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList2() {
								var input = document.getElementById("filesToUpload2");
								var ul = document.getElementById("fileList2");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton2').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton2').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton2').style.display = 'none';
								}
							}
						</script>
					</form>
				</div>
		<?php
		}//end if !isStaff
			/*Version 2 reports*/
			$reportId = "00O0P000003j6ET"; //Invoicing/RW Events Completed
			$reportName = "RW GP Events Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$workPlanTypes = array("532C"=>"WasteWise","532 "=>"WasteWise","533 "=>"Marketing","538C"=>"Compost Site TA"); //,"536 "=>"BMP" removed 8/5/2019 as requested by Emily Fabel
			$eventsCompletedCount = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$eventDate = $rowDetail["Upgrade__c.Event_Date__c"];
				$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c"];
				$secondaryName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				
				$gpName = $rowDetail["CUST_NAME"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Metric__c.Number__c"],0);
				$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$workPlanType = substr($gpType,0,4);
				
				$workPlan = $workPlanTypes[$workPlanType];
				if ($workPlan == ""){$workPlan = $workPlanTypes[substr($secondaryName,0,4)];}
				
				//only take in events with primary or secondary campaign starting with 53* as requested by Emma Sabel 2/8/2017
				$includeEvent = false;
				if (strpos(" ".$primaryCampaign,"53")){$includeEvent = true;}
				if (strpos(" ".$secondaryName,"53")){$includeEvent = true;}
				if ($includeEvent){				
					$eventsCompleted[strtotime($eventDate)][] = array("eventDate"=>$eventDate,"workPlan"=>$workPlan,"event"=>$gpName,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventsCompletedCount++; 
					if (substr($primaryCampaign,0,3)=="533" || substr($secondaryCampaign,0,3)=="533"){
						$evalType = "MarketingEvents";
						$dataAnalysisYTD[$evalType]++;
						if (strtotime($eventDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
							$dataAnalysisThisMonth[$evalType]++;
						}
					}
				}
			}
			ksort($eventsCompleted);
			
			
			//$reportId = "00O0P000003vRSkUAM"; //Invoicing/YTD RW Events Completed
			$reportId = "00O0P000003Tch4"; //Invoicing/RW Events Completed			
			//$reportName = "YTD RW Events Completed";
			$reportName = "RW Events Completed";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$eventDate = $rowDetail["START_DATE_TIME"];
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				$workPlanType = substr($gpName,0,4);
				
				$workPlan = $workPlanTypes[$workPlanType];
				if ($workPlan == ""){$workPlan = $workPlanTypes[substr($secondaryName,0,4)];}
				
				//only take in events with primary or secondary campaign starting with 53* as requested by Emma Sabel 2/8/2017
				$includeEvent = false;
				if (strpos(" ".$gpName,"53")){$includeEvent = true;}
				if (strpos(" ".$secondaryName,"53")){$includeEvent = true;}
				if ($includeEvent){
					$eventsCompleted[strtotime($eventDate)][] = array("eventDate"=>$eventDate,"workPlan"=>$workPlan,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventsCompletedCount++; 
					if (substr($gpName,0,3)=="533" || substr($secondaryCampaign,0,3)=="533"){ //$secondaryName?
						$evalType = "MarketingEvents";
						$dataAnalysisYTD[$evalType]++;
						if (strtotime($eventDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
							$dataAnalysisThisMonth[$evalType]++;
						}
					}
				}
			}
			ksort($eventsCompleted);
			
					
			$reportId = "00O0P000003j6Ei"; //Invoicing/RW Events Completed
			$reportName = "RW GP Upcoming Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$eventsUpcomingCount = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$eventDate = ($rowDetail["Upgrade__c.Event_Date__c"] == "-" ? "TBD" : $rowDetail["Upgrade__c.Event_Date__c"]);
				$eventDateSort = ($eventDate == "TBD" ? "2020-01-01" : $eventDate);
				$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c"];
				$secondaryName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$gpName = $rowDetail["CUST_NAME"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$workPlanType = substr($gpType,0,4);
				
				$workPlan = $workPlanTypes[$workPlanType];
				$includeEvent = false;
				if (strpos(" ".$primaryCampaign,"53")){$includeEvent = true;}
				if (strpos(" ".$secondaryName,"53")){$includeEvent = true;}
				if ($includeEvent){			
					$eventsUpcoming[strtotime($eventDateSort)][] = array("eventDate"=>$eventDate,"workPlan"=>$workPlan,"event"=>$gpName,"description"=>$currentStatus);
					$eventsUpcomingCount++;
				}
			}
			ksort($eventsUpcoming);
			
			$reportId = "00O0P000003Tchd"; //Invoicing/RW Events Completed
			//$reportId = "00O0P000003vRSkUAM"; 
			$reportName = "RW Upcoming Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				
				$eventDate = $rowDetail["START_DATE_TIME"];
				$eventDateSort = ($eventDate == "TBD" ? "2020-01-01" : $eventDate);
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				$workPlanType = substr($gpName,0,4);
				
				$workPlan = $workPlanTypes[$workPlanType];
				//only take in events with primary or secondary campaign starting with 53* as requested by Emma Sabel 2/8/2017
				$includeEvent = false;
				if (strpos(" ".$gpName,"53")){$includeEvent = true;}
				if (strpos(" ".$secondaryName,"53")){$includeEvent = true;}
				if ($includeEvent){				
					if (strtotime($eventDate) > strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$eventsUpcoming[strtotime($eventDateSort)][] = array("eventDate"=>$eventDate,"workPlan"=>$workPlan,"event"=>$subject,"description"=>$currentStatus);
						$eventsUpcomingCount++;
					}
				}
			}
			ksort($eventsUpcoming);
			
			
			$reportId = "00O0P000003j6HI"; //Invoicing/RW Events Completed
			$reportName = "RW Case Study Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$gpName = $rowDetail["CUST_NAME"];
				$account = $rowDetail["FK_NAME"];
				$gpType = (strpos($rowDetail["Upgrade__c.Green_Prospect_Type__c"],"Video") ? "Video" : "Written");
				
				$gpPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$caseStudies["Completed"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
				}else{
					if ($completedDate == "-"){
						$caseStudies["Inprocess"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
					}
				}
			}
			
			$reportId = "00O0P000003j6HS"; //Invoicing/RW Events Completed
			$reportName = "RW Website Blog Post Count";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$gpName = $rowDetail["CUST_NAME"];
				$account = $rowDetail["FK_NAME"];
				$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				if ($gpType == "535 Update Blog Entry"){
					$websiteBlogPosts[] = $rowDetail;
				}else{
					$websiteNewsletterDateSent = date("F j, Y",strtotime($completedDate));
				}
			}
			
			$reportId = "00O0P000003j6HX"; //Invoicing/RW Compost Site TA Tiers
			$reportName = "RW Compost Site TA Tiers";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$gpName = $rowDetail["Upgrade__c.Name"];
				$gpNameLink = $rowDetails["GPNameLinkValue"];
				$account = $rowDetail["Account.Name"];
				$accountLink = $rowDetails["AcountNameLinkValue"];
				$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$tierParts = explode(" - ",$gpType);
				$tier = trim($tierParts[1]);
				$tierCount[$tier]["ThisYear"] = $tierCount[$tier]["ThisYear"]+1;
				$gpPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];

				//if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
				//	$caseStudies["Completed"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
				//}else{
				//	if ($completedDate == "-"){
				//		$caseStudies["Inprocess"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
				//	}
				//}
				if ($completedDate != "-"){
					$tierCount["completed"][$tier]["ThisYear"] = $tierCount["completed"][$tier]["ThisYear"]+1;
				}
				if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$tiers["completed"][$tier][] = array("account"=>$account,"tier"=>$tier);
					$tiers["completed"]["count"] = $tiers["completed"]["count"]+1;
					$tierCount["completed"][$tier]["ThisMonth"] = $tierCount["completed"][$tier]["ThisMonth"]+1;
					$TAActivities["Completed"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate); //added this line
				}elseif (strtotime($initiationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($initiationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$tiers["initiated"][$tier][] = array("account"=>$account,"tier"=>$tier);
					$tiers["initiated"]["count"] = $tiers["initiated"]["count"]+1;
					$tierCount[$tier]["ThisMonth"] = $tierCount[$tier]["ThisMonth"]+1;
					$TAActivities["initiated"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate); //added this line
				}else{
					if ($completedDate == "-"){
						$tiers["ongoing"][$tier][] = array("account"=>$account,"tier"=>$tier);
						$tiers["ongoing"]["count"] = $tiers["ongoing"]["count"]+1;
					}
				}
				//need to add bullets for GP Type 538C Outreach and Followup and Primary Campaign 538C Compost Site TA to array to map to spreadsheet for "Other Compost Site TA Activites"
				
				
			}
			
			$reportId = "00O0P000003qTdnUAE"; //Invoicing/RW C & D TA Phases
			$reportName = "RW C & D TA Phases";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			//print_pre($reportRowsUnSorted);
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$gpName = $rowDetail["Upgrade__c.Name"];
				$gpNameLink = $rowDetails["GPNameLinkValue"];
				$account = $rowDetail["Account.Name"];
				$accountLink = $rowDetails["AcountNameLinkValue"];
				$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$gpPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$gpPhaseParts = explode(" ",$gpType);
				$phase = trim($gpPhaseParts[4]);
				$phaseCount[$gpType]["ThisYear"] = $phaseCount[$gpType]["ThisYear"]+1;
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				//print "Phasecount this year: ".$phaseCount[$gpType]["ThisYear"]."<br>";

				//if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
				//	$caseStudies["Completed"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
				//}else{
				//	if ($completedDate == "-"){
				//		$caseStudies["Inprocess"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate);
				//	}
				//}
				//print_r (explode(" ",$gpType)); //test
			
				if ($completedDate != "-"){
					$phaseCount["completed"][$phase]["ThisYear"] = $phaseCount["completed"][$phase]["ThisYear"]+1;
				}
				if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$phases["completed"][$phase][] = array("account"=>$account,"phase"=>$phase);
					$phases["completed"]["count"] = $phases["completed"]["count"]+1;
					$phaseCount["completed"][$phase]["ThisMonth"] = $phaseCount["completed"][$phase]["ThisMonth"]+1;
					//$TAActivities["Completed"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate); //added this line
				}elseif (strtotime($initiationDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($initiationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$phases["initiated"][$phase][] = array("account"=>$account,"phase"=>$phase);
					$phases["initiated"]["count"] = $phases["initiated"]["count"]+1;
					$phaseCount[$phase]["ThisMonth"] = $phaseCount[$phase]["ThisMonth"]+1;
					//$TAActivities["initiated"][] = array("topic"=>$gpName,"type"=>$gpType,"entity"=>$account,"status"=>$currentStatus,"completedDate"=>$completedDate); //added this line
				}else{
					if ($completedDate == "-"){
						$phases["ongoing"][$phase][] = array("account"=>$account,"phase"=>$phase);
						$phases["ongoing"]["count"] = $phases["ongoing"]["count"]+1;
					}
				}
				
			}
			
/*  Deprecated as of 8/5/2019 per Emily Fabel. Not used this FY
			$reportId = "00O0P000003TPgS"; //Invoicing/RW Status of BMP Development
			$reportName = "RW Status of BMP Development";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$gpName = $rowDetail["CUST_NAME"]; //Process Outline
				$primaryCampaign = $rowDetail["FK_NAME"]; //536 Furniture Reuse BMP
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"]; //In process
				$bmpStatus[$primaryCampaign][$gpName] = $currentStatus;
			}
			
			/*End Version 2 reports*/

		
			//get Receipts for Passthrough comparison
			$reportId = "00O0P000002z5D9"; //Invoicing/RW Receipts
			$reportName = "RW Receipts";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$parentIds[] = $GPNameLink;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$GPTypeParts = explode(" ",$GPType);
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];

				
				if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					/*New Method for Checking Attachments */
					$query = "SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId='".$GPNameLink."'";
					//echo $query."<br>";
					$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
					//print_pre($response->records);
					if (count($response->records)){
						foreach ($response->records as $record) {
							$parentId = $record->LinkedEntityId;
							$contentDocumentId = $record->ContentDocumentId;
							$query2 = "SELECT Id,Title,Description,FileType,FileExtension FROM ContentVersion WHERE ContentDocumentId='".$contentDocumentId."'";
							//echo $query2."<br>";
							$response2 = $mySforceConnection->query($query2); //sending request and getting response using SOAP
							foreach ($response2->records as $record2) {
								//print_pre($record2);
								$name = $record2->Title;
								//$versionData = $record2->VersionData; //does not need file for processing
								
								$nameParts = explode("_",$name);
								$vendorName = $nameParts[0];
								$receiptDescription = $nameParts[1];
								$receiptDate = str_replace(".pdf","",$nameParts[2]);
								$receiptDate = date("n/d/Y",strtotime($receiptDate));
								//print_pre($nameParts);
								
									/* does not need file for processing
									$thisAttachmentInfo = array("accountId"=>$accountId,"Id"=>$contentDocumentId,"code"=>$code,"Name"=>$name,"FileExtension"=>$fileExtension,"AccountName"=>$accountName,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
									//print_pre($thisAttachmentInfo);
									$attachmentName = retrieve_attachmentName($thisAttachmentInfo);
									$thisAttachmentInfo["SavedName"] = $attachmentName;
									
									$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $thisAttachmentInfo["SavedName"];
									$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $thisAttachmentInfo["AccountName"];
									$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $name;
									$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $fileExtension;
									
									
									$destination = "salesforce/".$attachmentFolder."/".$attachmentName;
									//echo $destination."<br>";
									$file = fopen($destination, "w+");
									fputs($file, $versionData);
									fclose($file);
									$Attachments[$parentId][] = $thisAttachmentInfo;
									*/
									
									$itemCheck = $GPTypeParts[0].date("Y-m-d",strtotime($completedDate)).$nameParts[0];
									$ReceiptInfo = new stdClass();
									$ReceiptInfo->VendorName = $nameParts[0];
									$ReceiptInfo->Description = $receiptDescription;
									$ReceiptInfo->CompletedDate = $completedDate;
									$ReceiptInfo->Number = $number;
									$ReceiptInfo->Unit = $unit;
									$ReceiptInfo->itemCheck = $itemCheck;
									//print_pre($ReceiptInfo);
									$attachmentsByJobID[$GPTypeParts[0]][] = $ReceiptInfo;	
									$attachmentsByItem[$itemCheck] = $ReceiptInfo;
									
									
									//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
								
							}
							//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
						}
					}else{
						$Adjustments["Alert"]["Missing Receipt Attachment"][] = $GPNameHref." missing attachment for $".money($number)." ".$otherExplain;
						//echo $GPLink." missing Attachment<br>";
					}
					/*End New Method for Checking Attachments */
				}//end if in timeline
				
				
				/* Old Method for Getting Attachments*/
				/*
				//check for attachments
				$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId ='".$GPNameLink."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
//				print_r($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->ParentId;
						$name = $record->Name;
						$id = $record->Id;
						$fileParts = explode(".",$name);
						$attachmentInfo = array("Id"=>$id,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts,"Folder"=>"GreenTeam");
						$nameParts = explode("_",$name);
						$vendorName = $nameParts[0];
						$receiptDescription = $nameParts[1];
						$receiptDate = str_replace(".pdf","",$nameParts[2]);
						$receiptDate = date("n/d/Y",strtotime($receiptDate));
						//print_pre($nameParts);
						$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
						$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $attachment;
						$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
						$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $attachmentInfo["FileParts"][0];
						$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
						
						
						if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
							$itemCheck = $GPTypeParts[0].date("Y-m-d",strtotime($completedDate)).$nameParts[0];
							$ReceiptInfo = new stdClass();
							$ReceiptInfo->VendorName = $nameParts[0];
							$ReceiptInfo->Description = $receiptDescription;
							$ReceiptInfo->CompletedDate = $completedDate;
							$ReceiptInfo->Number = $number;
							$ReceiptInfo->Unit = $unit;
							$ReceiptInfo->itemCheck = $itemCheck;
//							print_pre($ReceiptInfo);
							$attachmentsByJobID[$GPTypeParts[0]][] = $ReceiptInfo;	
							$attachmentsByItem[$itemCheck] = $ReceiptInfo;
						}
						
					}
				}else{
					$Adjustments["Alert"]["Missing Receipt Attachment"][] = $GPNameHref." missing attachment for $".money($number)." ".$otherExplain;
				}
				*/
				/*End old method for getting attachmets)*/
			}
		
		
		
		
			$reportId = "00OU0000003e6wK"; //Invoicing/531 Diversion Report Com,Rec,Reu,Redu
			$reportName = "531 Diversion Report Composted,Recycled,Reused,Reduced";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignParts = explode(" ",$primaryCampaignName);
				$secondaryCampaignName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$secondaryCampaignParts = explode(" ",$secondaryCampaignName);
				$createdDate = $rowDetail["Metric__c.CreatedDate"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$metricType = $rowDetail["Metric__c.Type__c"];
				$metricName = $rowDetail["Metric__c.Name"];
				$metricTons = floatval(str_replace(",","",trim($rowDetail["Metric__c.Weight_in_Tons__c"])));
				//$metricTons = floatval($metricTons);
				if (strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$diversionReportYTD[$metricType] = ($diversionReportYTD[$metricType]+$metricTons);
					$diversionReport["YTD"][$metricType][$GPNameHref] = $diversionReportYTD[$metricType];
					if (strtotime($completedDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$thisAddedValue = bcadd($diversionReportThisMonth[$metricType],$metricTons,2);
						$diversionReportThisMonth[$metricType] = $thisAddedValue;
						$diversionReport[$invoiceDate][$metricType][$gpNameLink] = $thisAddedValue;
					}
				}
			}
			ksort($diversionReportThisMonth);
			ksort($diversionReportYTD);
			
			$reportId = "00OU0000003e73z"; //Invoicing/RW Lamps Recycled YTD
			$reportName = "RW Lamps Recycled YTD 2018-2019";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$YTDLampReport = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];

				$createdDate = $rowDetail["Metric__c.CreatedDate"];
				$metricName = $rowDetail["Metric__c.Name"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				if ($metricUnit == "Unit"){
					$units = floatval(str_replace(",","",trim($rowDetail["Metric__c.Number__c"])));
				}else{
					$units = floatval(str_replace(" units","",str_replace(",","",trim($rowDetail["Metric__c.Other_Explain__c"]))));
				}
				//$units = ($metricName == "unit" ? $metricNumber : floatval(str_replace(" units","",$rowDetail["Metric__c.Other_Explain__c"])));
				$metricType = "Lamps";
				if (strtotime($createdDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					//echo $greenProspectName." ".$units." = ".$rowDetail["Metric__c.Number__c"]."<br>";
					$diversionReportYTD[$metricType] = ($diversionReportYTD[$metricType]+$units);
					$diversionReport["YTD"][$metricType] = $diversionReportYTD[$metricType];
					if (strtotime($createdDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
						$diversionReportThisMonth[$metricType] = ($diversionReportThisMonth[$metricType]+$units);
						$diversionReport[$invoiceDate][$metricType][$gpNameLink] = $units;
					}
				}
			}
			//print_pre($diversionReport);
			//print_pre($diversionReportThisMonth);
			$reportId = "00OU0000003e74E"; //Invoicing/RW Lamps Recycled Last Month
			$reportName = "RW Lamps Recycled Last Month";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$lastMonthLamps = 0;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				if ($metricUnit == "Unit"){
					$units = floatval(str_replace(",","",trim($rowDetail["Metric__c.Number__c"])));
				}else{
					$units = floatval(str_replace(" units","",str_replace(",","",trim($rowDetail["Metric__c.Other_Explain__c"]))));
				}
				$lastMonthLamps = ($lastMonthLamps+$units);
			}
			if ($lastMonthLamps != $diversionReportThisMonth["Lamps"]){
				$Adjustments["Alert"]["Last Month Lamp Calculation Error<br>".$YTDLampReport."<br>".$reportLink][] = "YTD Lamp last month units shown as ".($diversionReportThisMonth["Lamps"] ? $diversionReportThisMonth["Lamps"] : "0" )." but Last Month Lamp Units shown as ".$lastMonthLamps." likely due to GP Completed date is used in RW Lamps Recycled Last Month report, but Lamps Recycled YTD report used metric created date";
			}
	

			$reportId = "00O0P000003eWFA"; //Invoicing/531 YTD Modified or Created
			$reportName = "531 YTD Modified or Created";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportRowsSorts = array_orderby($reportRowsUnSorted, 'field_key', SORT_ASC);
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;			
			//print_pre($reportRowsSorts);
			$gpsReceivedAssistanceYTD = array();
			$gpsReceivedAssistanceThisMonth = array();
			$invoiceYearMonth = date("Y-m",strtotime($invoiceDate));
			foreach ($reportRowsSorts as $rowId=>$rowDetail){
				$modifiedDate = explode(" ",$rowDetail["CREATED_DATE"]);
				$evaluationDate = $modifiedDate[0];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				if (trim($completedDate) != "-"){
					$completedDateParts = explode(" ",$completedDate);
					$completedDate = $completedDateParts[0];
				}
				$yearMonthEvalDate = date("Y-m",strtotime($evaluationDate));
				$yearMonthCompletedDate = date("Y-m",strtotime($completedDate));
				$gpId = $rowDetail["CUST_ID"];
				$businessType = $rowDetail["Upgrade__c.Business_Type__c"];
				$bizType = ($businessType == "Hauler" || $businessType == "Processor" ? "HP" : "BI");
				$fieldKey = $rowDetail["field_key"];
				if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					if (trim($fieldKey) == "Created."){
						$evalTypes["Created".$bizType][$yearMonthEvalDate][$gpId] = 1;
					}else{
						//only count open if it wasn't actually created in the same period
						if (!$evalTypes["Created".$bizType][$yearMonthEvalDate][$gpId]){
							$evalTypes["Open"][$yearMonthEvalDate][$gpId] = 1;
						}
					}
				}
				if (trim($completedDate) != "-"){
					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate))) && strtotime($completedDate) >= strtotime($CurrentFY)){
//					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate))) && strtotime($completedDate) >= strtotime($invoiceDate)){
						$evalTypes["Completed".$bizType][$yearMonthCompletedDate][$gpId]=1;
					}
				}
			}
			
			$criteria = new stdClass();
			$criteria->FiscalYear = $CurrentFY;
			$rwTrackerDataResults = $GBSProvider->getRWTracker($criteria);
			//print_pre($rwTrackerDataResults->collection);
			foreach ($rwTrackerDataResults->collection as $record){
				if ($rwTrackerData[$record->Type][$record->YearMonth]["count"]){
					if ($rwTrackerData[$record->Type][$record->YearMonth]["count"] != $record->ItemCount){
						$priorCurrentYearMonth = $rwTrackerData[$record->Type][$record->YearMonth]["priorCurrentYearMonth"];
						$priorItems = (array)$rwTrackerData[$record->Type][$record->YearMonth][$priorCurrentYearMonth]["items"];
						$currentItems = (array)json_decode($record->Items);
						foreach ($priorItems as $gpId=>$count){
							if (!$currentItems[$gpId]){
								$gpsAdjusted["removed"][] = $gpId;
							}
						}
						foreach ($currentItems as $gpId=>$count){
							if (!$priorItems[$gpId]){
								$gpsAdjusted["added"][] = $gpId;
							}
						}
						$gpAdjustedText = "";
						if (count($gpsAdjusted)){
							foreach ($gpsAdjusted as $direction=>$gps){
								$gpAdjustedText .= "<hr>GPs ".$direction.":<br>";
								foreach ($gps as $gpId){
									$gpAdjustedText .= $gpId."<br>";
								}
							}
						}
						$Adjustments["Alert"][$record->Type." count for ".$record->YearMonth." has changed"][] = "In ".$priorCurrentYearMonth." was ".$rwTrackerData[$record->Type][$record->YearMonth]["count"]." but in ".$record->CurrentYearMonth." changed to ".$record->ItemCount.$gpAdjustedText;
					}
					$rwTrackerData[$record->Type][$record->YearMonth]["count"] = $record->ItemCount;
					$rwTrackerData[$record->Type][$record->YearMonth]["priorCurrentYearMonth"] = $record->CurrentYearMonth;
					$rwTrackerData[$record->Type][$record->YearMonth][$record->CurrentYearMonth]["items"] = json_decode($record->Items);
					
				}else{
					$rwTrackerData[$record->Type][$record->YearMonth]["count"] = $record->ItemCount;
					$rwTrackerData[$record->Type][$record->YearMonth]["priorCurrentYearMonth"] = $record->CurrentYearMonth;
					$rwTrackerData[$record->Type][$record->YearMonth][$record->CurrentYearMonth]["items"] = json_decode($record->Items);
				}
				$rwTrackerData[$record->Type][$record->YearMonth]["items"] = json_decode($record->Items);
			}
			//print_pre($rwTrackerData);
			
			//print_pre($evalTypes["CreatedBI"]["2020-01"]);
			foreach ($evalTypes as $evalType=>$yearMonthInfo){
				ksort($yearMonthInfo);
				foreach ($yearMonthInfo as $evalYearMonth=>$requests){
					//weed out next month items
					if (strtotime($evalYearMonth."-01") <= strtotime($invoiceYearMonth."-01")){ 
						if ($rwTrackerData[$evalType][$evalYearMonth]["count"] != count($requests)){
							//echo $evalType.$evalYearMonth." ".$rwTrackerData[$evalType][$evalYearMonth]["count"]."!= ".count($requests)."<br>";
							$misMatchedWas = array();
							$misMatchedIs = array();
							foreach ($rwTrackerData[$evalType][$evalYearMonth]["items"] as $gpId=>$gpCount){
								$misMatchedWas[] = "<a href='".$instance_url."/".$gpId."' target='".$gpId."'>".$gpId."</a>";
							}
							foreach ($requests as $gpId=>$gpCount){
								$misMatchedIs[] = "<a href='".$instance_url."/".$gpId."' target='".$gpId."'>".$gpId."</a>";
							}
							$Adjustments["Alert"][$evalType." count for ".$evalYearMonth." has changed"][] = "Was ".$rwTrackerData[$evalType][$evalYearMonth]["count"].":<br>".implode("<Br>",$misMatchedWas)."<hr>Is Now ".count($requests).":<br>".implode("<br>",$misMatchedIs);
							
						}
						$dataAnalysisYTD[$evalType] = $dataAnalysisYTD[$evalType]+count($requests);
						//echo $evalType." ".$evalYearMonth."=".count($requests)."<br>";
						//capture this information for tracking purposes to see when months data starts to change
						$record = new stdClass();
						$record->Type = $evalType;
						$record->YearMonth = $evalYearMonth;
						$record->ItemCount = count($requests);
						$record->Items = json_encode($requests);
						$record->CurrentYearMonth = $invoiceYearMonth;
						$record->FiscalYear = $CurrentFY;
						$record->ItemCount = count($requests);
						$record->Items = json_encode($requests);
						$insertResult = $GBSProvider->insertRWTracker($record);
						//echo $insertResult;
						if ($evalYearMonth == $invoiceYearMonth){
							$dataAnalysisThisMonth[$evalType] = count($requests);
						}
					}
				}
			}
			//print_pre($evalTypes);
//			print_pre($dataAnalysisYTD)["previouslyOpen"];
//			print_pre($dataAnalysisThisMonth);
//			$dataAnalysisYTD["RequestedAssistance"] = $gpsReceivedAssistanceYTDCount;
//			$dataAnalysisThisMonth["RequestedAssistance"] = $gpsReceivedAssistanceThisMonthCount;
			
			/* Deprecated on 7/30/2018 from meeting with Emily Fabel and Lauren
			$reportId = "00OU0000003e7OO"; //Invoicing/RW Marketing Report
			$reportName = "RW Marketing Report";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;			
			//$dataAnalysisThisMonth["YTDReceivedAssistCount"] = count($reportRowsUnSorted);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$metricType = $rowDetail["Metric__c.Type__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$evaluationDate = $completedDate;

				$evalType = "MarketingEvents";
				
				$possiblePresentation = false;
				$definitePresentation = false;
				//only counting presentations
				if (strpos(" ".strtolower($statusDetail),"presentation")){
					$possiblePresentation = true;
				}
				if (strpos($prospectType,"Presentation")){
					$definitePresentation = true;
					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$dataAnalysisYTD[$evalType]++;
						if (strtotime($evaluationDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
							$dataAnalysisThisMonth[$evalType]++;
							$marketingEvents[] = $statusDetail;
							$infoArray = array(
								"name"=>$metricType." ".(integer)$metricNumber,
								"primaryCampaignCode"=>"Marketing",
								"secondaryCampaignCode"=>"Marketing",
								"status"=>"Completed",
								"statusDetail"=>$statusDetail
								);
							$bulletsByCampaignCode["Marketing"][$greenProspectPhase][] = $infoArray;
						}
					}
				}
				if ($possiblePresentation && !$definitePresentation){
					$Adjustments["Review"]["Possible Marketing Presentation but not Prospect Type of Presentation<br>".$reportLink][] = $GPNameHref." Last Action Date:". $lastActionDate." Details: ".$metricType." ".$metricNumber." ".$statusDetail;
				}
			}
			*/
			//Get Google Analytics
			//https://developers.google.com/analytics/devguides/reporting/core/dimsmets
			$metricsToUse = array("sessions"=>"totalVisits","pageviews"=>"pageviews","pageviewsPerSession"=>"pageviewsPerSession","avgSessionDuration"=>"avgSessionDuration","percentNewSessions"=>"percentNewSession");
			$startDate = date("Y-m-d",strtotime($invoiceDate));
			$endDate = date("Y-m-t",strtotime($invoiceDate));
			include_once('../../cetadmin/gbs/_googleAnalytics.php');

	
			$reportId = "00OU0000003e65B"; //Invoicing/Monthly Report Bullets
			$reportName = "Monthly Report Bullets";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			//$bulletCodes = array();
			$b= 1; //RW Report bullets are codes 531-538
			while ($b < 9){
				$RWReportBulletCodes[] = "53".$b;
				$b++;
			}
//			if ($_GET['version'] == "_v2"){
				$ignoreBulletsFor = array("532C CURC Forum","532 WasteWise Forum","533 Presentation - CET","533 Presentation - DEP GBS","533 Webinar","533 Case Study","533 Case Study - Video","535 Update Blog Entry","535 Blog Entry","535 Newsletter");
//			}
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];				
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignParts = explode(" ",$primaryCampaignName);
				$primaryCampaignSecondaryPart = ($primaryCampaignParts[1] == "Targeted" ? "Targeted" : "");
				$includeBullet = true;
				if (in_array(trim($prospectType),$ignoreBulletsFor)){
					$includeBullet = false;	
				}
				$primaryCampaign = trim($primaryCampaignParts[0]);
				$primaryCampaign = ($primaryCampaign == "534" || $primaryCampaign == "537A" || $primaryCampaign == "537B" ? "534/537AB" : $primaryCampaign).$primaryCampaignSecondaryPart; //|| $primaryCampaign == "537C" added by LH
				$secondaryCampaignName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				
				$secondaryCampaigns = explode(";",$secondaryCampaignName);
				$code2 = array();
				//RW TA bullets need to be grabbed - GP Type: 538C Outreach and Followup / Primary Campaign: 538C Compost Site TA
				foreach ($secondaryCampaigns as $secondaryCampaignItem){
					if (trim($secondaryCampaignItem) != "-"){
						$codeParts2 = explode(" ",trim($secondaryCampaignItem));
						$secondaryCampaign = trim($codeParts2[0]);
						$secondaryCampaignSecondaryPart = ($codeParts2[1] == "Targeted" ? "Targeted" : "");
						$secondaryCampaign = ($secondaryCampaign == "534" || $secondaryCampaign == "537A"|| $secondaryCampaign == "537B" ? "534/537AB" : $secondaryCampaign).$secondaryCampaignSecondaryPart;

						if ($secondaryCampaign == "1030"){
							$secondaryCampaign = 	$secondaryCampaign.$codeParts2[1];
						}
						$code2[] = $secondaryCampaign;
						//echo $accountName." ".$code." ".$code2."<br>";
					}
				}
				if (count($code2)){
					$secondaryCampaign = implode(";",$code2);
				}else{
					$secondaryCampaign = "";
				}
				
				
				//$secondaryCampaignParts = explode(" ",$secondaryCampaignName);
				//$secondaryCampaignSecondaryPart = ($secondaryCampaignParts[1] == "Targeted" ? "Targeted" : "");
				//$secondaryCampaign = trim($secondaryCampaignParts[0]);
				//$secondaryCampaign = ($secondaryCampaign == "534" || $secondaryCampaign == "537A" || $secondaryCampaign == "537B" ? "534/537AB" : $secondaryCampaign).$secondaryCampaignSecondaryPart;
				$greenProspectPhase = (strpos(" ".$rowDetail["Upgrade__c.Green_Prospect_Phase__c"],"Completed") ? "Completed" : "DDD".$rowDetail["Upgrade__c.Green_Prospect_Phase__c"]);
				
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$infoArray = array(
					"name"=>$accountName,
					"accountNameHref"=>$AccountNameHref,
					"accountNameLink"=>$AccountNameLink,
					"GPName"=>$greenProspectName,
					"GPNameLink"=>$GPNameHref,
					"GPType"=>$prospectType,
					"reportLink"=>$reportLink,
					"primaryCampaign"=>$primaryCampaignName,
					"primaryCampaignCode"=>$primaryCampaign,
					"secondaryCampaignName"=>$secondaryCampaignName,
					"secondaryCampaignCode"=>$secondaryCampaign,
					"status"=>$greenProspectPhase,
					"statusDetail"=>$statusDetail,
					"Opportunity Lead"=>$opportunityLead
					);
				if ($includeBullet){
					$bulletsByCampaignCode[$primaryCampaign][$greenProspectPhase][] = $infoArray;
					$bulletsByGPType[$prospectType][$greenProspectPhase][] = $infoArray;
					
					/*
					if (!in_array($primaryCampaignName,$bulletCodes)){
						$bulletCodes[] = $primaryCampaignName;
					}
					*/
					$codesByAccount[$AccountNameLink][] = $primaryCampaignName;
					if (trim($secondaryCampaignName) != "-"){
						foreach ($code2 as $thisCode){
							$codesByAccount[$AccountNameLink][] = $thisCode;
							if ($thisCode != "" && $thisCode != $primaryCampaign){
								$bulletsByCampaignCode[$thisCode][$greenProspectPhase][] = $infoArray;
							}
						}
					}
				}
			}
			
			//print_pre($bulletsByGPType);
			//print_pre($bulletsByCampaignCode);
			//Hotline (531) Bullets are completely static, so reset them to only use database static bullets
			$bulletsByCampaignCode["531"] = array();
			
			/* deprecated 9/1/2017 by Emily Fabel
			//get static bullets 
			$criteria = new stdClass();
			$criteria->reportName = "RecyclingWorks"; 
			$staticBulletResults = $GBSProvider->invoiceTool_getStaticBullets($criteria);
			foreach ($staticBulletResults->collection as $results){
				$infoArray = array(
					"name"=>null,
					"primaryCampaignCode"=>$results->GBSBullet_InvoiceCode." Ongoing",
					"status"=>"Ongoing",
					"statusDetail"=>$results->GBSBullet_Content
					);
				$bulletsByCampaignCode[$results->GBSBullet_InvoiceCode]["Ongoing"][] = $infoArray;
			}
			*/

			//sort($bulletCodes);
			ksort($bulletsByCampaignCode);
			//echo($bulletsByCampaignCode);
			foreach ($bulletsByCampaignCode as $code=>$statusInfo){
				ksort($statusInfo);
				$bulletsByCampaignCodeSorted[$code] = $statusInfo;
				$codeCount = 0;
				foreach ($statusInfo as $status=>$statusRows){
					$codeCount = $codeCount+count($statusRows);
				}
				$bulletsCodeCount[$code] = $codeCount;
			}

			
			include_once('salesforce/reports/reportRW.php');
			echo $ReportsFileLink;

			//print_pre($bulletsByCampaignCodeSorted);
			

	echo "</div>";
	echo "<div id='RWReport' class='tab-content'>";
	?>
		<style>
			table {font-family: "Calibri";font-size:11pt;}
		</style>
		<h3>This tool will help group the Salesforce Dashboard reports for:<br>RW TA Completed - <?php echo $CurrentYear."-".$NextYear;?><br>RW TA In-Progress</h3>
		<?php
			$mergedFieldOrder = array(
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"RW DEP Summary Report Submitted"=>"85",
				"Green Prospect Phase"=>"71",
				"Owner: Full Name"=>"60",
				"Green Prospect Type"=>"71",
				"Number"=>"65",
				"Unit"=>"34",
				"Material"=>"67",
				"Current Status Detail"=>"62",
				"Opportunity Lead"=>"92");
				
				
			$miniTAFieldOrder = array(
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"Green Prospect Phase"=>"71",
				"Owner: Full Name"=>"60",
				"Green Prospect Type"=>"71",
				"Number"=>"65",
				"Unit"=>"34",
				"Material"=>"67",
				"Current Status Detail"=>"62",
				"Opportunity Lead"=>"92");

			$mapFieldOrder = array(
				"Green Prospect Name"=>"71",
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"RW DEP Summary Report Submitted"=>"85",
				"Green Prospect Phase"=>"71",
				"Green Prospect Type"=>"71",
				"Materials Diverted"=>"71",
				"Address"=>"64",
				"Pipeline"=>"65",
				"Opportunity Lead"=>"92");
				
				
			$extraFields = array(
				"Address"=>"64",
				"Pipeline"=>"65",
				"Opportunity Lead"=>"92"
				);
			//$PipelineSortOrder = array("Completed-Measure Not Implemented" || "Completed-Measure Implemented" || "Referral-Referred to TA" || "Completed-Measure does not apply" || "Completed-Back in Compliance","Implementation-In-Progress" || "Assessment-In-Progress");
			$PipelineSortOrder = array("Does Not Apply","1-30 Days" || "31-60 Days" || "61-90 Days" || "90+ Days"); //LH Change this to GPPhase Order
			//var_dump($PipelineSortOrder);
			//var_dump ($TAPipelineItems);
			//echo "<br> extra fields";
			//print_pre ($extraFields);
			//echo "<br>";
			if (count($TAPipelineItems)){
				//$RWTabCodes = array("Hospitality"=>"537A","FW Generators"=>"537B","State Facilities"=>"537C","General Recycling"=>"534","Mini TA"=>"539");
				//$RWTabCodes = array("Technical Assistance"=>"534/539/537AB","State Facilities"=>"537C");
				$RWTabCodes = array("Technical Assistance"=>"534/537AB","Mini TA"=>"539"); //8/5/2019 Lauren removed "State Facilities"=>"537C", per Emily Fabel's direction 
				$csvCombined = array();
				$csvCombined = $TAPipelineItems;
				echo "Instructions:<br>If Pipeline Errors tab is available, double click tab to display<br>";
				echo "Follow instructions in Impact Map tab to create Impact Map URL to be added to the report.";
//				echo "There are ".count($csvCombined)." rows<br>";
//				echo "There are ".count($TAPipelineItems)." TAPipeline rows<br>";
				//$mergedFile = convert_to_csv($csvCombined, 'Merged.csv', ',');
					$TableHeaderTop = "<thead><tr>";
					foreach ($mergedFieldOrder as $field=>$width){
						$TableColumns .= "<th valign=\"top\" width=\"".$width."\">".str_replace("Primary Campaign:","",$field)."</th>";
					}
					$TableHeaderBottom = "</tr></thead>";		
					$TableHeaderFull = $TableHeaderTop.$TableColumns.$TableHeaderBottom;

					$TableHeaderTopMiniTA = "<thead><tr>";
					foreach ($miniTAFieldOrder as $field=>$width){
						$TableColumnsMiniTA .= "<th valign=\"top\" width=\"".$width."\">".str_replace("Primary Campaign:","",$field)."</th>";
					}
					$TableHeaderBottomMiniTA = "</tr></thead>";		
					$TableHeaderFullMiniTA = $TableHeaderTopMiniTA.$TableColumnsMiniTA.$TableHeaderBottomMiniTA;

					$TableHeaderTopImpactMap = "<thead><tr>";
					foreach ($mapFieldOrder as $field=>$width){
						$TableColumnsImpactMap .= "<th valign=\"top\" width=\"".$width."\">".$field."</th>";
					}
					$TableHeaderBottomImpactMap = "</tr></thead>";		
					$TableHeaderFullImpactMap = $TableHeaderTopImpactMap.$TableColumnsImpactMap.$TableHeaderBottomImpactMap;
					
				foreach ($RWTabCodes as $TabName=>$TabCode){
					$RWHashes[] = str_replace("/","_",$TabCode);
					$TabListItems[] .= '<li><a id="tab-'.str_replace("/","_",$TabCode).'" href="#'.str_replace("/","_",$TabCode).'">'.$TabName.'</a></li>';
					//sort csvCombined by Pipeline Order
					$RWTabRowContent = "";
					$mergedFieldOrderToUse = ($TabCode == "539" ? $miniTAFieldOrder : $mergedFieldOrder);
					$tableHeaderToUse = ($TabCode == "539" ? $TableHeaderFullMiniTA : $TableHeaderFull);
					foreach ($PipelineSortOrder as $Pipeline){
						$pipelineRecordsfound = false;
						$rowDisplay = array();
						$rowToDisplay = array();
						foreach ($csvCombined as $id=>$rowInfo){
							if (strpos(" ".$rowInfo["PrimaryCampaignCode"],$TabCode)){
								$pipelineErrorFound = (strpos(" ".$rowInfo["Green Prospect Phase"],"Completed") && $rowInfo["Pipeline"] != "Does Not Apply" ? true : false);
								if ($pipelineErrorFound){
									//print_pre($rowInfo);
									$pipelineErrors[$id] = $rowInfo;
									//now correct the pipeline for display
									$rowInfo["Pipeline"] = "Does Not Apply";
								}
								if ($rowInfo["Pipeline"] == $Pipeline){
									//echo $rowInfo["Account Name"]." ".$rowInfo["Pipeline"]."<br>";
									$pipelineRecordsfound = true;
									$rowValues = array();
									$rowValueData = array();
									//cleanup
									if ($rowInfo["Material"] == "Other"){$rowInfo["Material"] = $rowInfo["Other: Explain"];}
									/*Currently calculated in Template via formula
									$DivertedMaterialsByType[$rowInfo["Material"]]["count"]++;
									if (strtolower(trim($rowInfo["Unit"])) == "tons"){
										$DivertedMaterialsByType[$rowInfo["Material"]]["tons"] = $DivertedMaterialsByType[$rowInfo["Material"]]["tons"]+$rowInfo["Number"];
									}
									*/
									foreach ($mergedFieldOrderToUse as $field=>$width){
										$rowValues[] = array($rowInfo[$field],$width);
										$rowValueData[$field] = $rowInfo[$field];
										//echo $field."=".$rowInfo[$field].", ".$width;
									}//end foreach mergedFieldOrder
									if (count($rowValues)){
										$rowDisplay[] = $rowValues;
										$rowToDisplay[] = $rowValueData;
									}
								}
							}
						}//end foreach csvCombined
						$rowToDisplayOrdered = array_orderby($rowToDisplay,"Account Name",SORT_ASC);

						$ExcelData[$TabName][$Pipeline] = $rowToDisplayOrdered;
						if (count($rowDisplay)){
							//show fields
							foreach ($rowDisplay as $rowID=>$rowValues){
								$RWTabRowContent .= "<tr>";
								foreach ($rowValues as $value){
									$thisValue = $value;
									$value = ($thisValue[0] ? $thisValue[0] : "&nbsp;");
									$width = ($thisValue[1] ? $thisValue[1] : "10");
									$RWTabRowContent .= "<td valign=\"top\" width=\"".$width."\">".$value."</td>";
								}
								$RWTabRowContent .= "</tr>";
							}
							$PipelineDisplayText = ($Pipeline == "Does Not Apply" ? "Total Completed ".$TabName." TA" : "Total ".$TabName." TA In-Progress, Expected Completed in ".$Pipeline);
							$PipelineDisplayColor = ($Pipeline == "Does Not Apply" ? "lightgreen" : "yellow");
							$RWTabRowContent .= "<tr style='background-color:".$PipelineDisplayColor.";'><td colspan='".count($mergedFieldOrderToUse)."'>".$PipelineDisplayText."</td></tr>";
						}
					}//end foreach PipelineSortOrder
					$RWTabContent ="<h3>".$TabName." (".$TabCode."):</h3><button class='selectAll' data-tableid='Table".str_replace("/","_",$TabCode)."' onclick=\"selectElementContents(document.getElementById('Table".str_replace("/","_",$TabCode)."'))\" >Select All</button>";
					$RWTabContent .= "<table border='1' cellspacing='0' cellpadding='2' width='100%'>".$tableHeaderToUse;
					$RWTabContent .= "</table>";
					$RWTabContent .= "<div id='Table".str_replace("/","_",$TabCode)."'><table border='1' cellspacing='0' cellpadding='2' width='100%'>";
					$RWTabContent .= $RWTabRowContent."</table></div>";
					$RWTabContentArray[$TabCode] = $RWTabContent;
				}//end foreach RWTabCodes
				//add Impact Map Info
					$RWHashes[] = "ImpactMap";
					$TabListItems[] .= '<li><a id="tab-ImpactMap" href="#ImpactMap">Impact Map</a></li>';
					//sort csvCombined by Pipeline Order
					$RWTabRowContent = "";
					$mergedFieldOrderToUse = $mapFieldOrder;
					$tableHeaderToUse = $TableHeaderFullImpactMap;
					$rowDisplay = array();
					foreach ($csvCombined as $id=>$rowInfo){
						$includeRow = true;
						$rowValues = array();
						if ($rowInfo["Primary Campaign: Campaign Name"] == "539 Mini TA")  {
							if (strpos($rowInfo["Opportunity Lead"], '533') == true ) {
							$includeRow = false; //introduced 2017-12 by Emma to eliminiate Mini TA from Impact Map - added on by Lauren Holway 2019-9 to remove these items from Mini TA
						}
						}						
						//cleanup
						if ($rowInfo["Material"] == "Other"){$rowInfo["Material"] = $rowInfo["Other: Explain"];}
						$rowInfo["Materials Diverted"] = trim($rowInfo["Number"]." ".$rowInfo["Unit"]." of ".$rowInfo["Material"]);
						$rowInfo["Materials Diverted"] = ($rowInfo["Materials Diverted"] == "of" ? "" : $rowInfo["Materials Diverted"]);

						if ($includeRow){
							foreach ($mergedFieldOrderToUse as $field=>$width){
									$rowValues[] = array($rowInfo[$field],$width);
								//echo $field."=".$rowInfo[$field].", ".$width;
							}//end foreach mergedFieldOrder
							if (count($rowValues)){
								$rowDisplay[] = $rowValues;
							}
						}
					}//end foreach csvCombined
					if (count($rowDisplay)){
						//show fields
						foreach ($rowDisplay as $rowID=>$rowValues){
							$RWTabRowContent .= "<tr>";
							foreach ($rowValues as $value){
								$thisValue = $value;
								$value = ($thisValue[0] ? $thisValue[0] : "&nbsp;");
								$width = ($thisValue[1] ? $thisValue[1] : "10");
								$RWTabRowContent .= "<td valign=\"top\" width=\"".$width."\">".$value."</td>";
							}
							$RWTabRowContent .= "</tr>";
						}
					}
					$RWTabContent ="<h3>Impact Map:</h3>
						<ul><li>1. Select All data and paste into <a href='https://batchgeo.com/' target='batchGeo'>https://batchgeo.com/</a> box.
							<li>2. Click 'Validate and Set Options' button.
							<li>3. Click the 'Advanced Options' and set Title = Account Name, Group by = Primary Campaign.
							<li>4. Click the Make Map button.  It will take a few minutes to process all records and create the map.
							<li>5. Zoom out and delete pins outside MA service area, center, save and copy link URL.
							<li>6. Add link URL to final tab of the RW TA Tracking Pipeline Report
						</ul>
					<button class='selectAll' data-tableid='TableImpactMap' onclick=\"selectElementContents(document.getElementById('TableImpactMap'))\" >Select All</button>";
					$RWTabContent .= "<div id='TableImpactMap'><table border='1' cellspacing='0' cellpadding='2' width='100%'>".$tableHeaderToUse.$RWTabRowContent;
					$RWTabContent .= "</table></div>";
					$RWTabContentArray["ImpactMap"] = $RWTabContent;
				//end Impact Map

				
				
				if (count($pipelineErrors)){
					$RWTabContent = "<h5>Update the Salesforce Dashboard to correct the Pipeline value to 'Does Not Apply.'<br>These errors were detected and corrected in the TA Pipeline download file, but should still be corrected in Salesforce.</h5>";
					$RWTabContent .= "<table id='ErrorTable'>";
					foreach ($pipelineErrors as $errorId=>$errorValue){
						$columns = array_keys($errorValue);
						if (!$columnsIncluded){
							$RWTabContent .="<thead><tr>";
							foreach ($columns as $column){
								$RWTabContent .="<th valign-'top'>".$column."</th>";
							}
							$RWTabContent .="</tr></thead>";
							$columnsIncluded = true;
						}
						$RWTabContent .="<tbody><tr>";
						foreach ($errorValue as $columnName=>$data){
							$dataDisplay = (strlen($data) > 50 ? "<span title='".$data."'>".substr($data,0,50)."...</span>" : $data); //this is truncating the current status detail to 50 characters
							$fontcolor = ($columnName == "Pipeline" ? " style='color:red;'" : "");
							$RWTabContent .="<td valign-'top' nowrap='nowrap'".$fontcolor.">".$dataDisplay."</td>";
						}
						$RWTabContent .="</tr></tbody>";
					}
					$RWTabContent .= "</table>";
					$RWTabContentArray["Error"] = $RWTabContent;
					
					$TabListItems[] = '<li><a id="tab-Error" href="#Error" style="color:red;" class="active">Pipeline Errors</a></li>';
				}
				//print_pre($ExcelData);
				include_once('salesforce/reports/RW_TA_Pipeline.php');
				echo "<Br>Download TA Pipeline Report: ".$ReportsFileLink."<br>";
				
				echo '<ul id="admin-tabs" class="tabs">';
						foreach ($TabListItems as $TabItem){
							echo $TabItem;
						}
				echo '</ul>';
				echo '<div class="tabs-content">';
				foreach ($RWTabContentArray as $TabCode=>$RWTabContent){
					echo '<div id="'.str_replace("/","_",$TabCode).'" class="tab-content">'.$RWTabContent.'</div>';
				}
				echo '</div>';
			

			
			}//end Files not uploaded
		?>

		<script type="text/javascript">
			function selectElementContents(el) {
				var body = document.body, range, sel;
				if (document.createRange && window.getSelection) {
					range = document.createRange();
					sel = window.getSelection();
					sel.removeAllRanges();
					try {
						range.selectNodeContents(el);
						sel.addRange(range);
					} catch (e) {
						range.selectNode(el);
						sel.addRange(range);
					}
				} else if (body.createTextRange) {
					range = body.createTextRange();
					range.moveToElementText(el);
					range.select();
				}
			}
			
			$(function(){
				$("#ShiftRows").on('change',function(){
					var $this = $(this),
						thisVal = $this.val();
						window.location = '<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?&nav=invoicingTool-RWTAPipeline&ShiftRows='+thisVal;
				});
				var tabs = $('ul.tabs'),
					selectAllClass = $(".selectAll");
					
				

				tabs.each(function(i) {

					//Get all tabs
					var tab = $(this).find('> li > a');
					tab.click(function(e) {

						//Get Location of tab's content
						var contentLocation = $(this).attr('href');

						//Let go if not a hashed one
						if(contentLocation.charAt(0)=="#") {

							e.preventDefault();

							//Make Tab Active
							tab.removeClass('active');
							$(this).addClass('active');

							//Show Tab Content & add active class
							$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
							ttInstances = TableTools.fnGetMasters();
							for (i in ttInstances) {
								if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
							}

						}
					});
				});		
				$(".tab-content").hide();
				$("#tab-RawData").click(function(){
					$("#RawDataDiv").show();
				});
				var hash = window.location.hash,
					tabHashes = new Array("#<?php echo implode("\",\"#",$RWHashes);?>"),
					isTabHash = $.inArray(hash, tabHashes) !== -1,
					isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
					setTabInner = function(tabid){
						var tab = $("#tab-" + tabid),
							content = $("#" + tabid);
						tab.addClass("active");
						content.show();
					};
				$("#tab-Error").click();
				setTabInner("<?php echo (count($pipelineErrors) ? "Error" : "537A");?>");
				$("#Error").show();
				
				$('#ErrorTable').DataTable({
					"fnInitComplete": function() {
							$('#ErrorTable tbody tr').each(function(){
									$(this).find('td:eq(0)').attr('nowrap', 'nowrap');
							});
						},			
					"scrollX": true,
					"bJQueryUI": true,
					"bSearchable":false,
					"bFilter":false,
					"bAutoWidth": false,
					"bSort": false,
					"order": [[0, "dec"]],
					"bPaginate": false,
					"iDisplayLength": 100
				});
				
			});
			
		</script>
<?php
	echo "</div>";
	include_once('_adjustmentsTab.php');
	$invoiceCodeName = "RecyclingWorks";
	include_once('_notesTab.php');
echo "</div>"; //end tabs-content
?>
<br clear="all">
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">
<b>Instructions</b>
<ul>
	<li>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li>3. If you are logged in via the Admin Portal: Follow the instructions in the RW Invoice tab, download the File.
	<li>4. If you are logged in vai the Staff Portal: Follow instructions in each Tab.  Download NON Pipeline, TA Pipeline, and RW Invoice
</ul>
<script type="text/javascript">
	$(function () {
		$("#updateNONFile").on('click',function(){
			$("#updateNONFileForm").toggle();
		});
		$("#updateDEPFile").on('click',function(){
			$("#updateDEPFileForm").toggle();
		});
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
//			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
//			"iDisplayLength": 5,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$masterTabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setMasterTab;?>");
		}
		
	});
</script>