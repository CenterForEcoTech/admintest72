<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the TRC Invoice file</h3>
<?php
$_SESSION['SalesForceReport'] = "TRC";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
$invoiceStartDate = $invoiceDate;
$invoiceEndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}


include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);
/*Depricated in favor of GeoBilling
//Get Corridors
$corridorResult = $GBSProvider->invoiceTool_getCorridor();
foreach ($corridorResult->collection as $result=>$record){
	$city = $record->City;
	$zipCode = $record->Zip;
	$corridor = $record->Corridor;
	$Corridors[$city][$zipCode]=$corridor;
	$CorridorByZip[$zipCode]=$corridor;
}
$corridorRate["Rt. 95"] = 175;
$corridorRate["Rt. 91"] = 125;
*/
//Get GeoBilling
$geoBillingResult = $GBSProvider->invoiceTool_getGeoBilling();
//print_pre($geoBillingResult);
foreach ($geoBillingResult->collection as $result=>$record){
	$state = $record->State;
	$category = $record->Category;
	$geoBillingByState[$state][$category."_".$record->GeoBilliingDefinition_ID] = $record;
	$geoBillingRate[$category] = $record->CostPerSite;
}
//print_pre($geoBillingByState);
foreach ($geoBillingByState as $state=>$category){
	if ($state == "CT" || $state == "MA" || $state == "NH" || $state == "VT" || $state == "NY"){
		foreach ($category as $categoryInfo){
			$geoBilling[$state][$categoryInfo->CostPerSite."_".$categoryInfo->Category."_".$categoryInfo->GeoBilliingDefinition_ID] = $categoryInfo;
		}
	}
	if ($state == "ME" || $state == "RI"){
		foreach ($category as $categoryInfo){
			$geoBilling[$state] = $categoryInfo->CostPerSite;
		}
	}
}
//print_pre($geoBilling);
/* DEPRICATED
$corridorRate["Rt. 95"] = 175;
$corridorRate["Rt. 91"] = 125;
*/

//print_pre($Corridors);

//Get Billing Rates
$billingRatesArray = array("TRC"=>array("515V"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}
//print_pre($billingRatesArray);
$setTab = "Invoice";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Quarterly Report"=>"QuarterlyReport","File"=>"File","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "TRC";
			$SelectedGroupsID = 30;
			include('views/_reportByCode.php');
			//print_pre($CodesByGroupName);
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
//			print_pre($InvoiceYearMonths);
//			print_pre($InvoiceDataByMonth);
//			print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					//Since TRC is not billed by hours capture all codes to determine ratio
					$revenueCodeTotalHours["TRC"] = bcadd($revenueCodeTotalHours["TRC"],$hour,2);
					$revenueCodesByEmployee["TRC"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee["TRC"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$hour,2);
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType])){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if ($BillingCategory == "Staff" && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate"][]=str_replace(", ","_",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once 'salesforce/config.php';
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00OU0000002yNYv"; //Invoicing/515V Store Visits
			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}		
			if (!isset($access_token) || $access_token == "") {
				die("Error - access token missing from session!");
			}
			if (!isset($instance_url) || $instance_url == "") {
				die("Error - instance URL missing from session!");
			}

			$reportId = "00OU0000002yNYv"; //Invoicing/515V Store Visits
			$reportName = "515V Store Visits";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			//print_pre($geoBilling["MA"]);

			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$completedDate = explode(" ",$rowDetail["Upgrade__c.GP_Stage_Completed__c"]);
				$completedDate = $completedDate[0];
				$TRCLocationID = $rowDetail["Account.TRC_Location_ID__c"];
				$TRCLocationName = $rowDetail["Account.TRC_Location_Name__c"];

				$address = $rowDetail["Account.BillingAddress"];
				$city = $rowDetail["Account.BillingCity"];
				$state = strtoupper($rowDetail["Account.BillingState"]);
				$zipCode = $rowDetail["Account.BillingPostalCode"];
				$lat = $rowDetail["Account.BillingLatitude"];
				$useGoogle = false;
				if ($lat == "-"){
					$useGoogle = true;
				}
				$long = $rowDetail["Account.BillingLongitude"];
				if ($long == "-"){
					$useGoogle = true;
				}
				if ($useGoogle){
					//get geoBilling if lat and long are not found in SF
					$addressFrom = $address.", ".$city." ".$state." ".$zipCode;
				    $formattedAddrFrom = str_replace(' ','+',$addressFrom);

					$geocodeFrom = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$formattedAddrFrom.'&sensor=false');
					$outputFrom = json_decode($geocodeFrom);
					//Get latitude and longitude from geo data
					$lat = $outputFrom->results[0]->geometry->location->lat;
					$long = $outputFrom->results[0]->geometry->location->lng;
				}
				$thisLat = (float)$lat;
				$thisLong = (float)$long;
				//echo $thisLat." ".$thisLong."<Br>";
				
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$trcStatusDetail = $rowDetail["Upgrade__c.TRC_Status_Details__c"];
				$fedExTracking = $rowDetail["Upgrade__c.FedEx_Tracking_ID_for_TRC_Shipment__c"];
				
				//echo "Test: ".$trcStatusDetail;
				
				$tobeIncluded = false;
				if (trim($completedDate)=="-"){
					$tobeIncluded = false;
				}else{
					if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceEndDate)))){
						$tobeIncluded = true;
					}
				}
				if ($tobeIncluded){
					$thisGeoBillingRate ="";
					if ($state == "CT" || $state == "MA" || $state == "NH" || $state == "VT" || $state == "NY"){
						if ($state == "MA"){
							$thisGeoBillingRate = 125.00;
						}
						krsort($geoBilling[$state]);
						$rateFound = false;
						foreach ($geoBilling[$state] as $rateCategory=>$rateInfo){
							$rateCategoryParts = explode("_",$rateCategory);
							/* used to test when an item doesn't find a corridor
							if ($address == "186 E. Main Street, Fredonia, NY 14063"){
								echo $AccountNameHref." ".$address." ".$rateCategory." [".$rateFound."] ".$rateInfo->CostPerSite."<br>";
								echo $thisLat." >=".$rateInfo->NorthOfThisLattitude." && ".$thisLat." <= ".$rateInfo->SouthOfThisLattitude." && ".$thisLong." <= ".$rateInfo->WestOfThisLongitude." && ".$thisLong." >= ".$rateInfo->EastOfThisLongitude."<br>";
							}
							*/
							if ($thisLat >= (float)$rateInfo->NorthOfThisLattitude && $thisLat <= (float)$rateInfo->SouthOfThisLattitude && $thisLong <= (float)$rateInfo->WestOfThisLongitude && $thisLong >= (float)$rateInfo->EastOfThisLongitude && !$rateFound){
								$thisGeoBillingRate = $rateInfo->CostPerSite;
								$thisCorridor = $rateCategoryParts[1];
								$GeoBillingCounts[$rateCategoryParts[1]]++;
								$rateFound = true;
							}
						}
						if ($state == "MA" && !$rateFound){
							if ($thisGeoBillingRate == 125.00){
								$thisCorridor = "MA - Mainland";
								$GeoBillingCounts["MA - Mainland"]++;
							}
						}
					}
					if ($state == "ME" || $state == "RI"){
						$thisGeoBillingRate = $geoBilling[$state];
						$thisCorridor = $state;
						$GeoBillingCounts[$state]++;
						$rateFound = true;
					}
					
					if (!$thisGeoBillingRate){
						$Adjustments["Alert"]["No Corridor Detected"][] = $AccountNameHref." ".$address;
						$thisGeoBillingRate = 215.00; //As pricing list for FY 2018
						$thisCorridor = "Outside New England and NY";
						$geoBillingRate[$thisCorridor] = 215.00;
						$GeoBillingCounts[$thisCorridor]++;
						$rateFound = true;
					}
					if (trim($TRCLocationName) != "-" && $TRCLocationName != $thisCorridor){
						$Adjustments["Alert"]["Location Name different than System Detected Corridor"][] = $AccountNameHref." ".$address." ".$TRCLocationName." != ".$thisCorridor;
					}
					
					
					$boxLeft = ($greenProspectPhase == "Completed-Measure Implemented" ? "Yes":"No");
					$boxLeftCount[$boxLeft]++;
					$infoArray = array("TRCLocationID"=>$TRCLocationID,"account"=>$accountName,"accountName"=>$AccountNameHref,"greenProspect"=>$GPNameHref,"phase"=>$greenProspectPhase,"completedDate"=>$completedDate,"address"=>$address,"trcStatusDetail"=>$trcStatusDetail,"city"=>$city,"zipCode"=>$zipCode,"boxLeft"=>$boxLeft,"corridor"=>$thisCorridor,"fedExTracking"=>$fedExTracking);
					if ($boxLeft == "Yes" && trim($completedDate)=="-"){
						$Adjustments["Alert"]["No Completed Date"][] = $AccountNameHref." [".$GPNameHref."]";
					}
					$TRCresults[] = $infoArray;				
					$TRCresultsByCorridor[$thisCorridor][] = $infoArray;				
				}
			}
			//print_pre($TRCresultsByCorridor);
			if (count($TRCresults)){
				$corridorTable = "<table class='simpleTable'>
					<thead><tr><th>Account</th><th>LocationID</th><th>GPName</th><th>Phase</th><th>CompletedDate</th><th>Address</th><th>Corridor</th><th>Box Left</th><th>TRC Status Detail</th></tr></thead>";
				$corridorTable .= "<tbody>";
				foreach ($TRCresultsByCorridor as $corridor=>$corridorDetails){
					foreach ($corridorDetails as $details){
						$corridorTable .= "<tr>
								<td>".$details["accountName"]."</td>
								<td>".$details["TRCLocationID"]."</td>
								<td>".$details["greenProspect"]."</td>
								<td>".$details["phase"]."</td>
								<td>".$details["completedDate"]."</td>
								<td>".$details["address"]."</td>
								<td>".$details["corridor"]."</td>
								<td>".$details["boxLeft"]."</td>
								<td>".$details["trcStatusDetail"]."</td>
								</tr>";
					}
				}
				$corridorTable .= "</tbody></table>";
				//print_pre($GeoBillingCounts);
					
					foreach ($GeoBillingCounts as $corridor=>$count){
						echo $corridor." visits: ".$count."<br>";
					}
					foreach ($boxLeftCount as $left=>$count){
						echo "Bins Left ".$left.": ".$count."<br>";
					}
					echo $corridorTable;
			}else{
				$Adjustments["Alert"]["No records found"][] = implode("<br>",$reportLinks);
			}
		echo "</div>";
		
		echo "<div id='QuarterlyReport' class='tab-content'>";
		
		$reportId = "00O0P0000036kIV"; //Invoicing/Monthly Report Bullets
			$reportName = "515 TRC Illinois Report Data";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$CustName = $rowDetail["CUST_NAME"];
				$GPTypeDetail = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$GPTypeParts = explode(" ",$GPTypeDetail);
				$GPType = $GPTypeParts[0];
				
				$bulletsByGPType[$GPType][] = array("name"=>$CustName,"deliverable"=>$statusDetail);
			}
			//print_pre($bulletsByGPType);
			
			ksort($bulletsByGPType);
					
		echo "</div>";
		
		echo "<div id='File' class='tab-content'>";
			$invoiceDate515V = $invoiceDate;
			$invoiceEndDate515V = $invoiceEndDate;
			include_once('salesforce/reports/reportTRC.php');
			echo $ReportsFileLink;

		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "TRC");
		include_once('_notesTab.php');

	echo "</div>";
?>	
<br clear="all">
The following Salesforce Reports were used:
<?php foreach ($reportLinks as $reportLink){
	echo "<br>".$reportLink;
}
?>
<br clear="all">
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the <?php echo $ReportsFileLink;?> File.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>