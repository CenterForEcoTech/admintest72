<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

$isMFEP = ($_GET['EAPMFEPTemplateID'] ? true : false);

include_once($dbProviderFolder."GBSProvider.php");
$gbsProvider = new GBSProvider($dataConn);
$paginationResult = $gbsProvider->getEAPTemplates();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$displayText = str_replace("  "," ",trim($record->displayText));
	$MeasureList[$record->measure] = 1;
	$UtilityPrimaryList[$record->primaryUtility] = 1;
	$UtilitySecondaryList[$record->secondaryUtility] = 1;
	$UtilityPermutationsList[$record->primaryUtility."/".$record->secondaryUtility] = 1;
	$DisplayTextList[$record->primaryUtility][$displayText] = $record->measure;
	$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["displayText"] = $displayText;
	$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["id"] = $record->id;
	$AEPTemplatesByID[$record->id] = $record;
	$AEPTemplatesByMeasure[$record->measure][]=$record;
}
ksort($AEPTemplatesByMeasure);
foreach ($MeasureList as $Measure=>$count){
	$Measures[] = $Measure;
}
foreach ($UtilityPrimaryList as $UtilityPrimary=>$count){
	$UtilityPrimaries[] = $UtilityPrimary;
}
foreach ($UtilitySecondaryList as $UtilitySecondary=>$count){
	$UtilitySecondaries[] = $UtilitySecondary;
}
foreach ($UtilityPermutationsList as $UtilityCombo=>$count){
	$UtilityPermutations[] = $UtilityCombo;
}
ksort($UtilityPermutations);
foreach ($DisplayTextList as $Utility=>$DisplayInfo){
	foreach ($DisplayInfo as $DisplayText=>$measure){
		if (count($DisplayTexts[$measure])){
			if (!in_array(trim($DisplayText),$DisplayTexts[$measure])){
				$DisplayTexts[$measure][] = trim($DisplayText);
			}
		}else{
			$DisplayTexts[$measure][] = trim($DisplayText);
		}
	}
}
//getMFEP data
$criteria = new stdClass();
$criteria->isMfep = true;
$paginationResult = $gbsProvider->getEAPTemplates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AEPTemplatesMFEP[$record->measure]["id"] = $record->id;
	$AEPTemplatesMFEP[$record->measure]["displayText"] = $record->displayText;
	$AEPTemplatesMFEPByID[$record->id] = $record;
	if ($isMFEP){
		$displayText = str_replace("  "," ",trim($record->displayText));
		$MeasureList[$record->measure] = 1;
		$UtilityPrimaryList[$record->primaryUtility] = 1;
		$UtilitySecondaryList[$record->secondaryUtility] = 1;
		$UtilityPermutationsList[$record->primaryUtility."/".$record->secondaryUtility] = 1;
		$DisplayTextList[$record->primaryUtility][$displayText] = $record->measure;
		$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["displayText"] = $displayText;
		$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["id"] = $record->id;
		$AEPTemplatesByID[$record->id] = $record;
		$AEPTemplatesByMeasure[$record->measure][]=$record;
		
		
	}
}



$SelectedEAPTemplateID = ($_GET['EAPTemplateID'] ? $_GET['EAPTemplateID'] : $_GET['EAPMFEPTemplateID']);
$addTemplate = $_GET['addTemplate'];

?>
<style>
.displayTextDiv {border:1pt solid black; margin:5pt;float:left;padding:5pt;cursor:pointer;}
.eapTemplateDisplayText {min-width:300px;cursor:pointer;}
.selectedTemplateDisplayText {border:1pt solid #CCCC00;background-color:#E6E600;}
</style>
	<?php if ($addTemplate || $SelectedEAPTemplateID){?>
		<form id="EAPUpdateForm" class="basic-form override_skel">
			<fieldset class="row">
				<legend>EAP Templates</legend>
				<div class="fifteen columns">
					<div style="display:none;">
						ID<?php if (!$addTemplate){?><input type="text" name="id" value="<?php echo $AEPTemplatesByID[$SelectedEAPTemplateID]->id;?>"><?php }?>
					</div>
					<div class="fourteen columns">
						<br clear="both">
						<div class="three columns">Measure:</div>
							<div class="ten columns">
								<input type="hidden" id="measure" name="measure" class="hiddenValue" style="width:400px;" value="<?php echo $SelectedMeasure=$AEPTemplatesByID[$SelectedEAPTemplateID]->measure;?>">
								<?php 
									if ($SelectedMeasure){
										$measureClass = str_replace("(","",$SelectedMeasure);
										$measureClass = str_replace(")","",$measureClass);
										$measureClass = str_replace(" ","_",$measureClass);
										$measureClassHide = "
												$('.displayTextDivFieldset').hide();
												$('.".$measureClass."').show();";
									}

								
									$codesComboBoxDivColumns = "nine";
									$customComboBoxInput = "width: 400px;";
									$comboBoxRemoveIfInvalid = " ";
									$codesComboBoxFunction = "
										var thisMeasure = $(ui.item).val(),
											thisMeasureClass = thisMeasure.replace('(',''),
											thisMeasureClass = thisMeasureClass.replace(')',''),
											thisMeasureClass = thisMeasureClass.replace(/\ /g,'_'),
											thisMeasureClass = '.'+thisMeasureClass;
										$('#measure').val(thisMeasure);
										$('.displayTextDivFieldset').hide();
										$(thisMeasureClass).show();
										";
									include('_eapMeasures_ComboBox.php');
								?>
							</div>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Primary Utility:</div>
							<div class="ten columns">
								<input type="hidden" style="width:100px;" id="primaryUtility" name="primaryUtility" value="<?php echo $SelectedPrimaryUtility=$AEPTemplatesByID[$SelectedEAPTemplateID]->primaryUtility;?>">
								<?php 
									$codesComboBoxFunction = "";
									include('_eapPrimaryUtilities_ComboBox.php');
								?>
							</div>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Secondary Utility:</div>
							<div class="ten columns">
								<input type="hidden" style="width:100px;" id="secondaryUtility" name="secondaryUtility" value="<?php echo $SelectedSecondaryUtility=$AEPTemplatesByID[$SelectedEAPTemplateID]->secondaryUtility;?>">
								<?php 
									include('_eapSecondaryUtilities_ComboBox.php');
								?>
							</div>
						<?php if (($SelectedEAPTemplateID && $AEPTemplatesByID[$SelectedEAPTemplateID]->isMfep) || !$SelectedEAPTemplateID){?>
						<br clear="both">
						<br clear="both">
						<div class="three columns">Is MFEP:</div>
							<div class="ten columns">
								<input type="checkbox" id="isMFEPCheckbox"<?php echo ($AEPTemplatesByID[$SelectedEAPTemplateID]->isMfep ? " checked" : "");?>>
							</div>
						<?php }?>
						<input type="hidden" name="isMfep" id="isMFEP" value="<?php echo ($AEPTemplatesByID[$SelectedEAPTemplateID]->isMfep ? "1" : "0");?>">
						<br clear="both">
						<br clear="both">
						<div class="three columns">Display Text:</div>
							<div class="ten columns">
								<textarea name="displayText" id="displayText" style="width:100%;height:200px;"><?php echo $SelectedSecondaryUtility=$AEPTemplatesByID[$SelectedEAPTemplateID]->displayText;?></textarea>
							</div>
					</div>
				</div>
			</fieldset>
			<div id="codeResults" class="fifteen columns"></div>
			<?php if (!$ReadOnlyTrue){?>
				<div class="fifteen columns">
					<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-eaptempates&EAPTemplateID=<?php echo $SelectedEAPTemplateID;?>" id="cancel" class="button-link">Cancel</a>
					<?php if ($addTemplate){?>
						<a href="save-code-add" id="save-code-add" class="button-link do-not-navigate">+Add</a>
						<input type="hidden" name="action" value="eap_add">
					<?php }else{?>
						<a href="save-code-update" id="save-code-update" class="button-link do-not-navigate">Save</a>
						<input type="hidden" name="action" value="eap_update">
					<?php }?>

				</div>
				<?php if (!$addTemplate){?>
					<div class="fifteen columns">
						<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-eaptemplates&addTemplate=true" class="button-link" style="float:right;">+ Add New Template</a>
					</div>
				<?php }?>
			<?php }//if not read only ?>
			<fieldset>
				<legend>Display Text Options</legend>
				<?php 
					foreach ($DisplayTexts as $measure=>$DisplayTextInfo){
						$measureClass = str_replace("(","",$measure);
						$measureClass = str_replace(")","",$measureClass);
						$measureClass = str_replace(" ","_",$measureClass);
						echo "<fieldset class='displayTextDivFieldset ".$measureClass."'><legend>".$measure."</legend>";
						$DisplayTextInfo1 = array_unique($DisplayTextInfo);
						ksort($DisplayTextInfo1);
						foreach ($DisplayTextInfo1 as $DisplayText){
							echo "<div class='displayTextDiv seven columns'>".$DisplayText."</div>";
						}
						echo "</fieldset>";
					}
				?>
			</fieldset>
		</form>
<?php }//end if $addTemplate || $SelectedEAPTemplateID ?>
				<?php if (!$addTemplate && !$SelectedEAPTemplateID){?>
					<div class="fifteen columns">
						<?php if (!$ReadOnlyTrue){?>
							<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-eaptemplates&addTemplate=true" class="button-link" style="float:right;">+ Add New Template</a>
						<?php }//end ReadOnly ?>
					</div>
<?php }?>
<div class="row">
	<div class="sixteen columns">
		<table class="EAPTemplates">
			<thead>
				<tr>
					<th style="box-sizing:border-box;">Measure</th>
					<?php foreach ($UtilityPermutations as $UtilityPermuation){
							$Utilities = explode("/",$UtilityPermuation);
							$PrimaryUtility = $Utilities[0];
							switch ($PrimaryUtility){
								case "Berkshire Gas":
									$backgroundColor = "#FFC000";
									$borderColor = "#CC9A00";
									break;
								case "Columbia Gas (Small)":
									$backgroundColor = "#FF0000";
									$borderColor = "#CC0000";
									break;
								case "Columbia Gas (Large)":
									$backgroundColor = "#92D050";
									$borderColor = "#75A640";
									break;
								case "National Grid Gas":
									$backgroundColor = "#00B0F0";
									$borderColor = "#008DC0";
									break;
							}
					?>
						<th style="color:black;background-image:none;background-color:<?php echo $backgroundColor;?>;border:1pt solid <?php echo $borderColor;?>"><?php echo $UtilityPermuation;?></th>
					<?php }?>
					<th style="color:black;background-image:none;background-color:white;border:1pt solid grey">MFEP</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($AEPTemplatesByMeasure as $measure=>$templateInfo){?>
				<tr>
					<td valign="top" style="min-width:200px;"><?php echo $measure;?></td>
					<?php 
						foreach ($UtilityPermutations as $UtilityPermuation){
							$Utilities = explode("/",$UtilityPermuation);
							$displayText = $AEPTemplates[$measure][$Utilities[0]][$Utilities[1]]["displayText"];
							$templateID = $AEPTemplates[$measure][$Utilities[0]][$Utilities[1]]["id"];
							strlen($displayText);
							echo "<td valign='top' title='".$measure."-".$UtilityPermuation."' class='".($SelectedEAPTemplateID == $templateID ? "selectedTemplateDisplayText" : "eapTemplateDisplayText")."' data-templateid='".$templateID."'>".$displayText."</td>";
						}
						echo "<td valign='top' title='".$measure."-MFEP' class='".($SelectedEAPTemplateID == $AEPTemplatesMFEP[$measure]["id"] ? " selectedTemplateDisplayText" : "eapTemplateDisplayText")."' data-templateMFEPid='".$AEPTemplatesMFEP[$measure]["id"]."'>".$AEPTemplatesMFEP[$measure]["displayText"]."</td>";
					?>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(function(){
		$("#isMFEPCheckbox").on('click',function(){
			var $this = $(this);
			if ($this.prop('checked')){
				$("#isMFEP").val(1);
			}else{
				$("#isMFEP").val(0);				
			}
		});
		var tableEAPTemplates = $('.EAPTemplates').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "asc"]],
			"bPaginate": true,
			"iDisplayLength": 500,
			"aoColumnDefs": [
                { "bWidth": '200px', "aTargets": [0] }
            ]
		});
		new $.fn.dataTable.FixedColumns( tableEAPTemplates );

		$(".eapTemplateDisplayText").on('click',function(){
			//console.log(this);
			var $this = $(this);
			if ($this.attr('data-templateid')){
				window.location = '?nav=manage-eaptemplates&EAPTemplateID='+$this.attr('data-templateid');
			}else{
				window.location = '?nav=manage-eaptemplates&EAPMFEPTemplateID='+$this.attr('data-templateMFEPid');
			}
		});
		
		var displayTextDiv = $(".displayTextDiv");
		displayTextDiv.on("mouseover",function(){
			$(this).css('background-color','lightblue');
		}).on("mouseout",function(){
			$(this).css('background-color','white');
		}).on("click",function(){
			$("#displayText").val($(this).text());
		});

		var codeSelectForm = $("#codeSelectForm");
			
		codeSelectForm.hide();	
		
		$('select').css('height','22px');
		var dropdownlist = $('select');
		dropdownlist.css('width','auto');
		$('input[type="text"]').css('height','22px');
		$('input[type="checkbox"]').css('width','13px');
		$(".input_text_tiny").css({'width':'25px','height':'20px'});
		$(".input_text_small").css({'width':'45px','height':'20px'});
		
		 var formElement = $("#EAPUpdateForm");
		 
		 formElement.validate({
			rules: {
				measure: {
					required: true
				},
				primaryUtility: {
					required: function(){
						if (!$("#isMFEPCheckbox").prop('checked')){
							return true;
						}else{
							return false;
						}
					}
				},
				secondaryUtility: {
					required: function(){
						if (!$("#isMFEPCheckbox").prop('checked')){
							return true;
						}else{
							return false;
						}
					}
				},
				displayText: {
					required: true
				}
			}
		});
		$("#cancel").click(function(e){
			e.preventDefault();
			parent.history.back(); 
			return false;							   
		});
		
			$("#save-code-add").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var data = JSON.stringify(formElement.serializeObject());
					//console.log(data);
					$.ajax({
						url: "ApiEAPManagement.php",
						type: "PUT",
						data: data,
						success: function(data){
							//console.log(data);
							$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
		<?php if ($SelectedEAPTemplateID){?>
			$("#save-code-update").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var data = JSON.stringify(formElement.serializeObject());
					//console.log(data);
					$.ajax({
						url: "ApiEAPManagement.php",
						type: "PUT",
						data: data,
						success: function(data){
							//console.log(data);
							$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
			
		<?php }//end if SelectedCodesID ?>
		<?php echo $measureClassHide;?>
	});
</script>