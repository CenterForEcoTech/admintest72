<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Quarterly Bullets based on Green Prospects with Changes Prior Quarter</h3>
(currently limited to Primary Campaigns that contain '1030')<br>
<?php
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

	require_once 'salesforce/config.php';
	require_once('salesforce/rest_functions.php');
	$access_token = $_SESSION['access_token'];
	$instance_url = $_SESSION['instance_url'];
	$reportId = "00O0P000003Tc8E"; //Invoicing/Limited GPs Modified Last Quarter
	//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
	if (!checkSession($instance_url, $reportId, $access_token)){
		echo "<script>window.location.replace('".AUTH_URL."');</script>";
		//echo AUTH_URL;
		//header('Location: '.AUTH_URL);
	}		
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
	$reportId = "00O0P000003Tc8E"; //Invoicing/Limited GPs Modified Last Quarter
	$reportName = "Limited GPs Modified Last Quarter";
	$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
	$reportRowsUnSorted = $reportResults["rows"];
	$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
	$reportLinks[] = $reportLink;
	//print_pre($reportRowsUnSorted);
	$reportRowsSorted = array_orderby($reportRowsUnSorted, 'CREATED_DATE', SORT_ASC);
	foreach ($reportRowsSorted as $rowId=>$rowDetail){
		$fieldKey = $rowDetail["field_key"];
		$owner = $rowDetail["CREATED"];
		$accountName = $rowDetail["Upgrade__c.Account__c"];
		$AccountNameLink = $rowDetail["Upgrade__c.Accountid__c"];
		$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
		$greenProspectName = $rowDetail["CUST_NAME"];
		$GPNameLink = $rowDetail["CUST_ID"];
		$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
		if (trim($accountName) == "-"){
			$Adjustments["Alert"]["Missing Account"][] = $GPNameHref;
		}
		$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
		$gpPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
		$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
		$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c"];
		$secondaryCampaign = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
		$codeParts = explode(" ",$primaryCampaign);
		$code = $codeParts[0];
		$secondaryCampaigns = explode(";",$secondaryCampaign);
		$code2 = array();
		foreach ($secondaryCampaigns as $secondaryCampaignItem){
			if (trim($secondaryCampaignItem) != "-"){
				$codeParts2 = explode(" ",trim($secondaryCampaignItem));
				if ($codeParts2[0] == "1030"){
					$code2[] = $codeParts2[0].$codeParts2[1];
				}else{
					$code2[] = $codeParts2[0];
				}
				//echo $accountName." ".$code." ".$code2."<br>";
			}
		}
		$createdDate = $rowDetail["CREATED_DATE"];
		$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
		$stageCompletedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
		$newValue = $rowDetail["NEWVAL"];
		$infoArray = array("owner"=>$owner,"accountName"=>$accountName,"accountLink"=>$AccountNameHref,"account"=>$AccountNameLink,"greenProspectName"=>$greenProspectName,"greenProspect"=>$GPNameHref,"phase"=>$gpPhase,"type"=>$gpType,"primaryCampaign"=>$primaryCampaign,"secondaryCampaign"=>$secondaryCampaign,"createdDate"=>$createdDate,"stageCompletedDate"=>$stageCompletedDate,"statusDetail"=>$statusDetail,"initiationDate"=>$initiationDate);
		$includeItem = ($fieldKey == "Current Status Detail" && trim($newValue) == "-" ? false : true);
		if ($statusDetail !="-" && $includeItem){
			$bulletsByCodeAccount["1 All Bullets"][$AccountNameLink][$GPNameLink] = $infoArray;
			$bulletsByCodeAccount[$code][$AccountNameLink][$GPNameLink] = $infoArray;
			if (count($code2)){
				foreach ($code2 as $thisCode2){
					$bulletsByCodeAccount[$thisCode2][$AccountNameLink][$GPNameLink] = $infoArray;
				}
			}
		}
	}
	
	ksort($bulletsByCodeAccount);
	include_once('salesforce/reports/reportBulletsQuarterly.php');
	include_once('_adjustmentsTab.php');
	echo "<hr><br>";
	
	echo $ReportsFileLink;
	$bulletTable = "	<table class='simpleTable'>
		<thead><tr><th>Owner</th><th>Account</th><th>Green Prospect</th><th>Phase</th><th>Type</th><th>Primary Campaign</th><th>Secondary Campaign</th><th>Created Date</th><th>Stage Completed Date</th><th>InitiationDate</th><th>Status Detail</th></tr></thead>
		<tbody>";
			foreach ($bulletsByCodeAccount["1 All Bullets"] as $accountId=>$gpInfo){
				foreach ($gpInfo as $gpId=>$detail){
					$bulletCount++;
					$bulletTable .= "<tr>
							<td>".$detail["owner"]."</td>
							<td>".$detail["accountLink"]."</td>
							<td>".$detail["greenProspect"]."</td>
							<td>".$detail["phase"]."</td>
							<td>".$detail["type"]."</td>
							<td>".$detail["primaryCampaign"]."</td>
							<td>".$detail["secondaryCampaign"]."</td>
							<td>".$detail["createdDate"]."</td>
							<td>".$detail["stageCompletedDate"]."</td>
							<td>".$detail["initiationDate"]."</td>
							<td>".$detail["statusDetail"]."</td>
						 </tr>";
				}
			}
	$bulletTable .= "</tbody>
		</table>";
		echo $bulletCount." Bulletpoints<br>".$bulletTable;
?>
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
	
<script type="text/javascript">
	$(function () {
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
	});
</script>