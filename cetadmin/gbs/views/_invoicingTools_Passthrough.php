<?php
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$InvoicingProvider = new InvoicingProvider($dataConn);
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

$invoicingCodes = $InvoicingProvider->getInvoicingCodes();
foreach ($invoicingCodes->collection as $result){
	$allCodes = explode(",",$result->codes);
	foreach ($allCodes as $code){
		$invoicingByCode[$code] = $result;
	}
	$invoicingByAllCodes[$result->codes] = $result;
	$invoicingByDisplayName[$result->displayName] = $result;
}
//print_pre($invoicingByAllCodes);
//print_pre($invoicingByCode);


$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughsByJobID[$results->JobID][] = $results;
	$passThroughsByJobIDAmount[$results->JobID] = bcadd($passThroughsByJobIDAmount[$results->JobID],$results->DebitAmt,2);
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID" && $key != "CreditAmt" && $key != "Balance" && $key != "Jrnl" && $key != "AccountID"){
			$key = ($key == "JobID" ? "AAAJobID" : $key);
			$passThroughHeaders[$key] = 1;
		}
	}
}
ksort($passThroughHeaders);
//print_pre($passThroughsByJobID);

$ImportFileName = "importfiles/PassthroughFile".date("Y_m",strtotime($invoiceDate)).".xlsx";
//echo "ImportFileName ".$ImportFileName."<br>";
$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
if(count($_FILES['fileToUpload1'])) {
	$tmpFileName = $_FILES["fileToUpload1"]["tmp_name"][0];
	if ($_GET['type'] == "PassthroughFile"){
		$ImportFileReceived = true;
		move_uploaded_file($tmpFileName, $target_file);
	}
}
if ($ImportFileReceived || $_GET['useUploadedFile']){
	//new import that data and parse it out 
	$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
	$_SESSION['ImportFileName'] = $ImportFileName;
	echo "Using Passthrough File just uploaded<br>";
	ob_flush();
	include_once('salesforce/read_PassthroughFile.php');
	//print_pre($rowsClean);
	$insertResult = $GBSProvider->insertPassthrough($rowsClean,getAdminId());
	//print_pre($insertResult);
	foreach ($jobIDs as $jobID=>$jobIDcount){
		$jobIDStart = substr($jobID,0,3);
		$invoiceCodeName = $invoicingByCode[$jobIDStart]->displayName;
		$passThroughJobIDs[$invoiceCodeName] = 1;
		$jobCountChange = false;
		$jobAmountChange = false;
		if (count($passThroughsByJobID[$jobID]) && $jobIDcount["count"] != count($passThroughsByJobID[$jobID])){
			$jobCountChange = true;
		}
		if ($passThroughsByJobIDAmount[$jobID] && $jobIDcount["amount"] != $passThroughsByJobIDAmount[$jobID]){
			$jobAmountChange = true;
		}
		if ($jobAmountChange || $jobCountChange){
			$changedJobIDSinceLastUpdate[$jobID] = "<br>".($jobAmountChange ? $jobID." ".$invoiceCodeName." Total Amount Changed from $".money($passThroughsByJobIDAmount[$jobID])." to $".money($jobIDcount["amount"]) : $jobID." Total Count Changed from ".count($passThroughsByJobID[$jobID])." to ".$jobIDcount["count"]);
		}
	}
	foreach ($passThroughJobIDs as $displayName=>$count){
		$includedJobs .= ($displayName ? : 'NoJobID').", ";
	}
	$includedJobs = rtrim($includedJobs,", ");
	$passthroughText = "Passthroughs for ".$includedJobs;
	$changedAlert = "";
	if (count($changedJobIDSinceLastUpdate)){
		foreach ($changedJobIDSinceLastUpdate as $jobID=>$changedResults){
			$changedAlert .= $changedResults."<br>";
		}
		if ($changedAlert){
			echo "The following JobID Codes have been updated:<br>".$changedAlert."<br><br>";
		}
	}
	//email yosh an alert it was uploaded
					$path = getcwd();
					$pathSeparator = "/";
					if (!strpos($path,"xampp")){
						$thisPath = ".:/home/cetdash/pear/share/pear";
						set_include_path($thisPath);
					}else{
						$pathSeparator = "\\";
					}
	
					require_once "Mail.php";  
					$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
					$to = "Yosh Schulman <yosh.schulman@cetonline.org>, Heather McCreary <heather.mccreary@cetonline.org>"; 
					$to = "Yosh Schulman <yosh.schulman@cetonline.org>"; 
					$recipients = $to;
					$subject = "Passthrough File Uploaded".($changedAlert ? " with Changes" : ""); 
					$body = "Uploaded by ".$_SESSION['AdminAuthObject']['adminFullName']."<br>".$passthroughText."<br>".$changedAlert;
					$body = str_replace("<br>","\r\n",$body);
					$host = "smtp.office365.com"; 
					$port = "587"; 
					$username = $_SESSION['AdminEmail']; 
					$password = $_SESSION['AdminAuthenticationCode'];  
					$headers = array (
						'From' => $from,   
						'To' => $to, 
						'Reply-to' => $from,
						'Subject' => $subject
					); 
					$smtp = Mail::factory(
						'smtp',   
						array (
							'host' => $host,     
							'port' => $port,     
							'auth' => true,     
							'username' => $username,     
							'password' => $password
						)
					);  
					$mail = $smtp->send($recipients, $headers, $body);
					if (!$mail){	
						echo "<br><h2>There was a problem sending the alert email.  Please take a screen shot of this page and send it to Yosh</h2>";
					}
	$triggerType = "PassthroughFile";
	include_once('views'.$pathSeparator.'_invoicingTools_ProcessingTrigger.php');
}
if (!$ImportFileReceived){
	if (file_exists($target_file)){
		$lastUploadedDateParts = explode("File",$ImportFileName);
		$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
		
		echo "Using <a href='".$ImportFileName."'>Existing Uploaded Passthrough File</a> last uploaded ".date("F d, Y H:ia",filemtime($target_file))."<br>";
		$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
		$trackingFile = $ImportFileName;
	}else{
		echo "No Prior Passthrough File uploaded.  Please upload the most recent Passthrough File<br>";
	}
}
?>
	<a id='updatePassthroughFile' href="#" class='button-link do-not-navigate'>Update Passthrough File</a><br>
	<div id="updatePassthroughFileForm" style='display:none;'>
		<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?type=PassthroughFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
			<div class="row">
				<div class="eight columns">
					<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
				</div>
				<div class="eight columns">
					<strong>File You Selected:</strong>
					<ul id="fileList1"><li>No Files Selected</li></ul>
					<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
				</div>
			</div>
			
			<script type="text/javascript">
				function makeFileList1() {
					var input = document.getElementById("fileToUpload1");
					var ul = document.getElementById("fileList1");
					while (ul.hasChildNodes()) {
						ul.removeChild(ul.firstChild);
					}
					for (var i = 0; i < input.files.length; i++) {
						var li = document.createElement("li"),
							fileName = input.files[i].name,
							fileLength = fileName.length,
							fileExt = fileName.substr(fileLength-4);
							console.log(fileExt);
						if (fileExt != "xlsx"){
							li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'none';
						}else{
							li.innerHTML = input.files[i].name;
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'block';
						}
					}
					if(!ul.hasChildNodes()) {
						var li = document.createElement("li");
						li.innerHTML = 'No Files Selected';
						ul.appendChild(li);
						document.getElementById('submitButton1').style.display = 'none';
					}
				}
			</script>
		</form>
	</div>
	<div>
		<br><Br>
		<?php echo count($passThroughs);?> Passthrough Records for <?php echo date("F Y",strtotime($invoiceDate));?>
		<?php if (count($passThroughs)){?>
			<table class="simpleTable">
				<thead>
					<tr>
						<?php
							foreach ($passThroughHeaders as $header=>$val){
								echo "<th>".str_replace("AAA","",$header)."</th>";
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($passThroughs as $result){
							echo "<tr>";
							foreach ($passThroughHeaders as $header=>$val){
								$header = str_replace("AAA","",$header);
								$displayValue = ($header == "Date" ? mysqlDate($result->{$header}) : $result->{$header});
								$displayValue = ($header == "UpdatedLastDate" ? date("n/d/y g:sa",strtotime($result->{$header})) : $displayValue);
								echo "<td nowrap>".str_replace(" Passthrough","",$displayValue)."</td>";
							}
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		<?php }else{?>
			<div style='height:2000px;'>&nbsp;</div>
		<?php }?>
	</div>

<script type="text/javascript">
	$(function () {
		<?php if ($ImportFileReceived && !count($passThroughs)){?>//location.href='?nav=invoicingTool-Passthrough';<?php }?>
		$("#updatePassthroughFile").on('click',function(){
			$("#updatePassthroughFileForm").toggle();
		});
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   true,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
	});
</script>