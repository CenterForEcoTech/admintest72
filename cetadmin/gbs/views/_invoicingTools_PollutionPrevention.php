<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Pollution Prevention Invoice files</h3>
<?php
$_SESSION['SalesForceReport'] = "PollutionPrevention";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
$attachmentFolder="P2I";

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$billingRatesArray = array("PollutionPrevention"=>array("930"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}
//print_pre($billingRatesArray);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Pollution Prevention - 930";
			$SelectedGroupsID = 63; //GreenTeam
			$InvoiceData = array();
			$InvoiceHourData = array();
			$navDropDown = "invoicingTool-PollutionPrevention";
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					$code = (string)$code;
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			//print_pre($InvoiceHourData);
			//print_pre($BillingRateByCategory);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						$InvoiceCode = (string)$InvoiceCode;
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if (($BillingCategory == "Support Staff") && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate for ".$BillingRateType][]=str_replace("_",", ",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once 'salesforce/config.php';
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00O0P000003TPpU"; //Invoicing/P2I 930 Pollution Prevention

			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				$_SESSION['SalesForceReport'] = "P2I";
				$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
		
			$reportId = "00O0P000003TPpU"; //Invoicing/P2I 930 Pollution Prevention
			$reportName = "P2I 930 Pollution Prevention";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"]; //emily fabel wants to use this in addition to completed date
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$currentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$useThisRecord = false;
				
				
				if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$useThisRecord = true;
				}
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$useThisRecord = true;
				}
				
				If ($currentStatus == "receipt"){
					if ($useThisRecord){
						$p2iReceipts[] = $rowDetail;
					}
					$useThisRecord = false;
				}
				
				
				if ($useThisRecord){
					$p2iBullets[] = $currentStatus;
				}

			}
			//print_pre($p2iReceipts);
			if (count($p2iReceipts)){
				$passThroughs = array();
				foreach ($p2iReceipts as $receipt){
					$GPNameLink = $receipt["CUST_ID"];
					
					$query = "SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId='".$GPNameLink."'";
					//echo $query."<br>";
					$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
					//print_pre($response->records);
					if (count($response->records)){
						foreach ($response->records as $record) {
							$parentId = $record->LinkedEntityId;
							$contentDocumentId = $record->ContentDocumentId;
							$query2 = "SELECT Id,Title,Description,FileType,FileExtension,VersionData FROM ContentVersion WHERE ContentDocumentId='".$contentDocumentId."'";
							$response2 = $mySforceConnection->query($query2); //sending request and getting response using SOAP
							foreach ($response2->records as $record2) {
								//print_pre($record2);
								$name = $record2->Title;
								$fileType = $record2->FileType;
								$fileExtension = $record2->FileExtension;
								$versionData = $record2->VersionData;
								
								$nameParts = explode("_",$name);
								$vendorName = $nameParts[0];
								$receiptDescription = $nameParts[1];
								$receiptDate = str_replace(".pdf","",$nameParts[2]);
								$receiptDate = date("n/d/Y",strtotime($receiptDate));
								//print_pre($nameParts);
								
								
									$thisAttachmentInfo = array("accountId"=>$accountId,"Id"=>$contentDocumentId,"code"=>$code,"Name"=>$name,"FileExtension"=>$fileExtension,"AccountName"=>$accountName,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>$attachmentFolder,"parentId"=>$parentId);
									//print_pre($thisAttachmentInfo);
									$attachmentName = retrieve_attachmentName($thisAttachmentInfo);
									$thisAttachmentInfo["SavedName"] = $attachmentName;
									
									$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $thisAttachmentInfo["SavedName"];
									$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $thisAttachmentInfo["AccountName"];
									$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $name;
									$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $fileExtension;
									
									
									$destination = "salesforce/".$attachmentFolder."/".$attachmentName;
									//echo $destination."<br>";
									$file = fopen($destination, "w+");
									fputs($file, $versionData);
									fclose($file);
									$Attachments[$parentId][] = $thisAttachmentInfo;
									
									$itemCheck = "930".date("Y-m-d",strtotime($completedDate)).$nameParts[0];

									/*
									$thisPassthrough = new stdClass();
									$thisPassThrough->Memo = ucwords(strtolower($nameParts[0]));
									$thisPassThrough->SourceName = ucwords(strtolower($nameParts[1]));
									$thisPassThrough->Date = date("Y-m-d",strtotime($nameParts[2])); //YYYY-mm-dd
									$thisPassThrough->Debit = $nameParts[3];
									*/
									
									array_push($passThroughs,$nameParts);
									
									/*
									$ReceiptInfo = new stdClass();
									$ReceiptInfo->VendorName = $nameParts[0];
									$ReceiptInfo->Description = $receiptDescription;
									$ReceiptInfo->CompletedDate = $completedDate;
									$ReceiptInfo->Number = $number;
									$ReceiptInfo->Unit = $unit;
									$ReceiptInfo->itemCheck = $itemCheck;
		//							print_pre($ReceiptInfo);
									$attachmentsByJobID["930"][] = $ReceiptInfo;	
									$attachmentsByItem[$itemCheck] = $ReceiptInfo;
									*/
									
									//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
								
							}
							//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
						}
					}else{
						$Adjustments["Alert"]["Missing Receipt Attachment"][] = $GPNameHref." missing attachment for $".money($number)." ".$otherExplain;
						//echo $GPLink." missing Attachment<br>";
					}
				}
				
				$passthroughCounter = 0;
				$passThroughsByJobID[930] = array();
				foreach ($passThroughs as $nameParts){
					$thisNewPassthroughId = ${$thisPassthrough.$passthroughCounter};
					$passThroughsByJobID[930][] = $nameParts;
					$passthroughCounter++;

				}
			}
			//print_pre($passThroughsByJobID);
			
			//Include Events too!
			$reportId = "00O0P000003Td0V"; //Invoicing/P2I Event
			$reportName = "P2I Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$eventCounter = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$eventDate = $rowDetail["START_DATE_TIME"];
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				if (strtotime($eventDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($eventDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$P2IEvents[strtotime($eventDate)]["completed"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
				if (strtotime($eventDate) > strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$P2IEvents[strtotime($eventDate)]["upcoming"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
			}
			ksort($P2IEvents);
			//print_pre($P2IEvents);
			
				include_once('salesforce/reports/reportPollutionPrevention.php');
				echo $ReportsFileLink;
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "PollutionPrevention");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the File: <?php echo $ReportsFileLink;?>.
</ul>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>
