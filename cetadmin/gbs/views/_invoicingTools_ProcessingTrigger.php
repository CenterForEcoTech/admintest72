<?php
$processTrigger = true;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");

$triggerType = ($triggerType ? : $_GET['triggerType']);
$triggerType = ($triggerType ? : "PassthroughFile");

$GBSProvider = new GBSProvider($dataConn);
$InvoicingProvider = new InvoicingProvider($dataConn);
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

$invoicingCodes = $InvoicingProvider->getInvoicingCodes();
foreach ($invoicingCodes->collection as $result){
	$allCodes = explode(",",$result->codes);
	foreach ($allCodes as $code){
		$invoicingByCode[$code] = $result;
	}
	$invoicingByAllCodes[$result->codes] = $result;
	$invoicingByDisplayName[$result->displayName] = $result;
}
//print_pre($invoicingByDisplayName);
//print_pre($invoicingByCode);

	foreach ($invoicingByDisplayName as $displayName=>$results){
		if ($results->processTrigger == $triggerType){
			echo $displayName.": ";
			$include_file = $results->processFile;
			echo "<div style='border:1pt solid red;display:none;'>";
			include_once($include_file);
			echo "</div>".$ReportsFileLink." created<br>";
//			print_pre($results);
		}
	}
	
	//Send notification and links to the files
		$path = getcwd();
		$pathSeparator = "/";
		if (!strpos($path,"xampp")){
			$thisPath = ".:/home/cetdash/pear/share/pear";
			set_include_path($thisPath);
		}
	require_once "Mail.php";  
	require_once "Mail/mime.php";  
	$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
//	$to = "Yosh Schulman <yosh.schulman@cetonline.org>, Heather McCreary <heather.mccreary@cetonline.org>"; 
	$to = "Heather McCreary <heather.mccreary@cetonline.org>"; 
	$recipients = $to;
	$subject = "Triggered Invoice Files Created ".date("m/d/Y"); 
	$body = "The following invoices have been triggered  by ".$triggerType.":<Br>";
	foreach ($triggerResultFiles as $resultFileSource=>$resultFile){
		$body .= $resultFileSource."<br>";
	}

	$host = "smtp.office365.com"; 
	$port = "587"; 
	$username = $_SESSION['AdminEmail']; 
	$password = $_SESSION['AdminAuthenticationCode'];  
	$headers = array (
		'From' => $from,   
		'To' => $to, 
		'Reply-to' => $from,
		'Subject' => $subject
	); 
	
 $mime = new Mail_mime("\n");
    // if client reading this mail, reads only text-part (or cannot read HTML messages),
    // he'll see this text body message
    $mime->setTXTBody($body);
    // if client reading this email, reads HTML (multi-part) messages,
    // he'll see this HTML body message
	$mime->setHTMLBody($body);
    // now, we'll add local file as an attachment. Second argument is a mime type
    // specification for XLSX filetype.
	foreach ($triggerResultFiles as $resultFileSource=>$resultFile){
		echo "attaching ".$resultFile."<br>";
		$mime->addAttachment($resultFile);
	}
    // get body of the e-mail message from Mail_mime class.
    $body = $mime->get();
    // get headers
    $hdrs = $mime->headers($headers);
    // now, lets send e-mail using the PEAR Mail class.
	
	$smtp = Mail::factory(
		'smtp',   
		array (
			'host' => $host,     
			'port' => $port,     
			'auth' => true,     
			'username' => $username,     
			'password' => $password
		)
	);  
	$mailsend = $smtp->send($recipients, $hdrs, $body);
	if (!$mailsend){	
		echo "<br><h2>There was a problem sending the alert email.  Please take a screen shot of this page and send it to Yosh</h2>";
	}
?>