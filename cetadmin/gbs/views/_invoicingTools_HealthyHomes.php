<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Healthy Homes file</h3>
<?php
$_SESSION['SalesForceReport'] = "HealthyHomes";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
$invoice550AEndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

//Get Billing Rates
$GLCode = " – 1"; //General Ledger Residential class
$billingRatesInvoiceNames = array("E4","EPA");
foreach ($billingRatesInvoiceNames as $invoiceName){
	$criteria = new stdClass();
	$criteria->invoiceName = $invoiceName;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$criteria->invoiceName][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateEmployees[$criteria->invoiceName][$record->GBSBillingRate_Name][] = $Employee;
			$BillingRateByEmployees[$criteria->invoiceName][$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
			$BillingRateByEmployeeJustRate[$criteria->invoiceName][$Employee] = $record->GBSBillingRate_Rate;
		}
	}
}	

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Files"=>"Files","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Healthy Homes";
			$SelectedGroupsID = "HealthyHomes";
			$CodesByGroupName = array();
			$sourceFolder = "gbs";
			$navDropDown=$nav;
			include('../hours/views/_reportByCode.php');
			//print_pre($CodesByGroupName);
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
//			print_pre($CodesInGroup);
//			print_pre($InvoiceData);
			
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
						$InvoiceHourDataByEmployee[$Employee_Name][$code] = $InvoiceHourDataByEmployee[$Employee_Name][$code]+$hour;
						$codeCategory = ($Employee_Name == "Christman_Penny" || $Employee_Name == "Lester_Quinn" ? "Bookkeeper" : "Staff");
						$InvoiceHourDataByCodeCategory[$code][$codeCategory] = $InvoiceHourDataByCodeCategory[$code][$codeCategory]+$hour;
						
						foreach ($CodesByGroupName as $GroupName=>$GroupCodes){
							if (in_array($code,$GroupCodes)){
								$CodeWithHoursInGroup[$GroupName][$code][$Employee_Name] = $InvoiceHourDataByEmployee[$Employee_Name][$code];
								$EmployeesInGroupByEmployeeWithHours[$GroupName][$Employee_Name][$code] = $InvoiceHourDataByEmployee[$Employee_Name][$code];
								if (!in_array($Employee_Name,$EmployeesInGroupByEmployee[$GroupName])){
									$EmployeesInGroup[$GroupName]++;
									$EmployeesInGroup["EPA"]++;
									$EmployeesInGroup["E4"]++;
									$EmployeesInGroupByEmployee[$GroupName][] = $Employee_Name;
								}
							}
						}
					}
				}
			}
			//print_pre($EmployeesInGroup);
			//print_pre($InvoiceHourData);
			//print_pre($CodeWithHoursInGroup);
			//print_pre($InvoiceHourDataByCode);
			//print_pre($InvoiceHourDataByCodeCategory);
			
		echo "</div>";
		echo "<div id='Files' class='tab-content'>";
				$invoice550AStartDate = $invoiceDate;
				include_once('salesforce/reports/reportHealthyHomes.php');
				echo $ReportsFileLink;

		echo "</div>";
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "Healthy Homes");
		include_once('_notesTab.php');
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Files tab, download the Hours File and send to Quinn.
</ul>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>
