<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the MFEP Invoice files</h3>
<?php
$_SESSION['SalesForceReport'] = "MFEP";
foreach($_GET as $key=>$val){$mfepQueryString .= "&".$key."=".$val;}
$_SESSION['MFEPQueryString'] = $mfepQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date("m/01/Y")." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);
include_once($dbProviderFolder."ResidentialProvider.php");
$ResidentialProvider = new ResidentialProvider($dataConn);


//Get Billing Rates
$criteria = new stdClass();
$criteria->invoiceName = "MFEP";
$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	$BillingRateByCategory[$record->GBSBillingRate_Name]=$record;
	foreach ($EmployeeMembers as $Employee){
		$BillingRateByEmployee[$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		$BillingRateByEmployeeJustRate[$Employee] = $record->GBSBillingRate_Rate;
	}
	/*
	$BillingRateByID[$record->GBSBillingRate_ID] = $record;
	$BillingRateByName[$record->GBSBillingRate_Name] = $record;
	$BillingRateByInvoiceName[$record->GBSBillingRate_InvoiceName][$record->GBSBillingRate_Name] = $record;
	*/
}
//Get Billing Rates
$criteria = new stdClass();
$criteria->invoiceName = "FedGrant";
$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	foreach ($EmployeeMembers as $Employee){
		$FedBillingRateByEmployeeJustRate[$Employee] = $record->GBSBillingRate_Rate;
	}
}
//Get GPTypes
$criteria = new stdClass();
$paginationResult = $GBSProvider->invoiceTool_getGPTypes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GPTypeName = $record->GPType_Name;
	$GPTypeBulletCode[$GPTypeName]=$record->GPType_BulletCode;
}

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Raw Hours"=>"RawHours","Allocation Multipliers"=>"Allocations","Hours Allocation"=>"HoursAllocation","Billable Hours"=>"BillableHours","MFEP Files"=>"Files","Adjustments"=>"Adjustments","Notes"=>"Notes");
//	if ($_GET['type']=="billableHoursFile"){
		$TabCodes["Cover Page & Invoice"]="CoverPageLogic";
//	}else{
		unset($_SESSION['MFEPBillableHoursFileName']);
//	}
	
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "MFEP";
			$SelectedGroupsID = 19;
			$MFEPTool = true;
			include_once('views/_reportByCode.php');
		echo "</div>";
		
		echo "<div id='RawHours' class='tab-content'>";
			if (count($InvoiceData)){
				echo "<table class='simpleTable'>
						<thead>
							<tr>
								<th>Employee_Name</th>";
							foreach ($invoiceDataHeader as $header){
								echo "<th>".$header."</th>";
							}
				echo "		</tr>
						
						</thead>
						<tbody>";
						foreach ($InvoiceData as $Employee_Name=>$invoiceCode){
							if (!$EmployeesRevenueCodeByName[$Employee_Name]){
								$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
							}
							echo "<tr><td>".$Employee_Name."</td>";
							foreach ($invoiceDataHeader as $code){
								echo "<td>".$invoiceCode[$code]."</td>";
								$totalHoursByCode[$code] = bcadd($totalHoursByCode[$code],$invoiceCode[$code],2);
								$totalHoursByCodeByEmployee[$code][$Employee_Name] = bcadd($totalHoursByCodeByEmployee[$code][$Employee_Name],$invoiceCode[$code],2);
							}
							echo "</tr>";
						}
					
				echo "</tbody></table>";
			}else{
				$NoStaffHours = "No Staff With Hours between ".$StartDate." and ".$EndDate." with Status '".($_GET['Status'] ? : "Approved")."'";
				echo $NoStaffHours;
			}
//			print_pre($totalHours);
		echo "</div>";
		
		echo "<div id='Allocations' class='tab-content'>";
		
			$allocationResult = $GBSProvider->invoiceTool_getMFEPAllocations();
			foreach ($allocationResult->collection as $allocation){
				$thisID = $allocation->MFEPAllocations_ID;
				$thisName = $allocation->MFEPAllocations_Name;
				$thisMultiplier = $allocation->MFEPAllocations_Multiplier;
				$thisAverageHourlyRate = $allocation->MFEPAllocations_AverageHourlyRate;
				$allocationMultiplier[$thisName]["multiplier"]=$thisMultiplier;
				$allocationMultiplier[$thisName]["averageHourlyRate"]=$thisAverageHourlyRate;
				echo "<div class='row'>
						<div class='two columns'>Name:<input type='text' id='Name".$thisID."' value='".$thisName."' style='width:100px;'></div>
						<div class='two columns'>Multiplier:<input type='text' id='Multiplier".$thisID."' value='".$thisMultiplier."' style='width:100px;'></div>
						<div class='three columns'>Average Hourly Rate:<input type='text' id='AverageHourlyRate".$thisID."' value='".$thisAverageHourlyRate."' style='width:100px;'></div>
						<div class='three columns'><br><a class='changeAllocation button-link' data-allocationId='".$thisID."'>Change</a><span style='display:none' id='Alert".$thisID."'></span></div>
					</div>";
			}
	
		echo "</div>";

		echo "<div id='HoursAllocation' class='tab-content'>";
			if ($NoStaffHours){
				echo $NoStaffHours;
			}else{
				$hexColors = array("blue"=>"9BC2E6","green"=>"92D050","white"=>"FFFFFF","olive"=>"C6E0B4","peach"=>"FFF2CC","silver"=>"D0CECE","yellow"=>"FFFF00","salmon"=>"F8CBAD","darkblue"=>"8EA9DB","lightblue"=>"BDD7EE","pink"=>"FCE4D6","summaryorange"=>"FFE699");
				$allocationCodeHeader = 
					array(
						"A"=>array("title"=>"","object"=>"space","width"=>12,"color"=>$hexColors["white"]),
						"B"=>array("title"=>"","object"=>"space","width"=>16,"color"=>$hexColors["white"]),
						"C"=>array("title"=>"","object"=>"space","width"=>13,"color"=>$hexColors["white"]),
						"D"=>array("title"=>"560A Development, Outreach, Marketing","object"=>"560A","width"=>12,"color"=>$hexColors["blue"]),
						"E"=>array("title"=>"560A-R","object"=>"560A-R","width"=>12,"color"=>$hexColors["green"]),
						"F"=>array("title"=>"560B BMP","object"=>"560B","width"=>12,"color"=>$hexColors["blue"]),
						"G"=>array("title"=>"560C TA","object"=>"560C","width"=>12,"color"=>$hexColors["blue"]),
						"H"=>array("title"=>"560C-R","object"=>"560C-R","width"=>12,"color"=>$hexColors["green"]),
						"I"=>array("title"=>"560D Database","object"=>"560D","width"=>12,"color"=>$hexColors["blue"]),
						"J"=>array("title"=>"560D-R","object"=>"560D-R","width"=>12,"color"=>$hexColors["green"]),
						"K"=>array("title"=>"560I Incentives","object"=>"560I","width"=>12,"color"=>$hexColors["blue"]),
						"L"=>array("title"=>"560IA","object"=>"560IA","width"=>12,"color"=>$hexColors["green"]),
						"M"=>array("title"=>"TOTAL HOURS","object"=>"TotalHours","width"=>12,"color"=>$hexColors["blue"]),
						"N"=>array("title"=>"561","object"=>"561","width"=>12,"color"=>$hexColors["white"]),
						"O"=>array("title"=>"562","object"=>"562","width"=>12,"color"=>$hexColors["white"])
					);
				function toNearestFraction($number, $denominator = 1,$direction = 'round'){
					$x = $number * $denominator;
					if ($direction == 'floor'){
						$x = floor($x);
					}else{
						$x = round($x);
					}
					$x = $x / $denominator;
					return $x;
				}
				// Returns the key at the end of the array
				function endKey($array){
					end($array);
					return key($array);
				}
				$lastAllocationFunder = endKey($allocationMultiplier);
				$rowId = 3;
				foreach ($InvoiceData as $Employee_Name=>$hoursData){
					$NameRows[] = $rowId;
					$employeeNameParts = explode("_",$Employee_Name);
					$firstName = $employeeNameParts[1];
					$lastName = $employeeNameParts[0];
	//				echo $firstName." ".$lastName.":<br>";
					$reportRows[$rowId]["B".$rowId]=trim(str_replace("_",", ",$Employee_Name));
					$reportRows[$rowId]["C".$rowId]="multiplier";
					//make header row
					foreach ($allocationCodeHeader as $column=>$headerData){
						$thisObject = $headerData["object"];
						if ($thisObject != "space"){
							$dataDisplay = floatval($hoursData[$thisObject]);
							if ($thisObject == "560C"){
								$dataDisplay = $dataDisplay+floatval($hoursData["560 CET Audit"]);
							}
							$totals[$thisObject]["actual"] = $totals[$thisObject]["actual"]+$dataDisplay;
		//					echo $thisObject."=".$dataDisplay."<br>";
							if ($thisObject == "TotalHours"){
								$reportRows[$rowId][$column.$rowId]="=SUM(D".$rowId.":L".$rowId.")";
								$reportRows[$rowId]["P".$rowId]=$dataDisplay;
							}else{
								if ($thisObject == "561" || $thisObject == "562"){
									if ($dataDisplay > 0){
										$YellowCells[] = $column.$rowId;
									}else{
										$dataDisplay = "";
									}
								}else{
									$TotalActualHours[$Employee_Name] = $TotalActualHours[$Employee_Name]+$dataDisplay;
								}
								$reportRows[$rowId][$column.$rowId]=$dataDisplay;
							}
						}
					}
//					print_pre($reportRows);
					$rowId++;
					//make allocation rows
					$allocatedAmount = array();
					$BillableHoursHeader = array();
					foreach ($allocationMultiplier as $funder=>$allocationInfo){
						$multiplier = floatval($allocationInfo["multiplier"]);
						$reportRows[$rowId]["A".$rowId]=$funder;
						$reportRows[$rowId]["C".$rowId]=$multiplier;
						foreach ($allocationCodeHeader as $column=>$headerData){
							$thisObject = $headerData["object"];
							if ($thisObject != "space" && $thisObject != "561" && $thisObject != "562"){
								$dataDisplay = floatval($hoursData[$thisObject]);
								if ($thisObject == "560C"){
									$dataDisplay = $dataDisplay+floatval($hoursData["560 CET Audit"]);
								}
			//					echo $thisObject."=".$dataDisplay."<br>";
								$thisAllocatedAmount = bcmul($multiplier,$dataDisplay,10);
								$roundAmount = toNearestFraction($thisAllocatedAmount,4);
								$floorAmount = toNearestFraction($thisAllocatedAmount,4,'floor');
								$useThisAmount = ($allocatedAmount[$thisObject]+$roundAmount <= $dataDisplay ? $roundAmount : $floorAmount);
								$allocatedAmount[$thisObject] = $allocatedAmount[$thisObject]+$useThisAmount;
								if ($funder == $lastAllocationFunder && $allocatedAmount[$thisObject] == 0){$allocatedAmount[$thisObject] = $dataDisplay;$useThisAmount = $dataDisplay;}
								//echo $funder.": ".$allocatedAmount." = ".toNearestFraction($allocatedAmount,4,'floor')." or ".toNearestFraction($allocatedAmount,4,'ceil')." : using ".$useThisAmount."<br>";
								$totals[$thisObject]["invoiced"] = $totals[$thisObject]["invoiced"]+$useThisAmount;
								if ($thisObject == "TotalHours"){
									$reportRows[$rowId][$column.$rowId]="=SUM(D".$rowId.":L".$rowId.")";
								}else{
									$BillableHours[$Employee_Name][$funder][$thisObject] = $useThisAmount;
									$BillableHours[$Employee_Name][$funder]["Total"] = $BillableHours[$Employee_Name][$funder]["Total"]+$useThisAmount;
									$BillableHours[$Employee_Name][$thisObject] = $BillableHours[$Employee_Name][$thisObject]+$useThisAmount;
									$BillableHours[$Employee_Name]["All Hours"] = $BillableHours[$Employee_Name]["All Hours"]+$useThisAmount;
									$BillableHours[$funder][$thisObject] = $BillableHours[$funder][$thisObject]+$useThisAmount;
									$BillableHoursDirectBilling[$funder][$thisObject][$Employee_Name] = $BillableHoursDirectBilling[$funder][$thisObject][$Employee_Name]+$useThisAmount;
									if (!in_array($thisObject,$BillableHoursHeader)){$BillableHoursHeader[] = $thisObject;}
									//$invoiceRows[$rowId][$column.$rowId]=$useThisAmount; //THIS AMOUNT FOR INVOICE
									
									$reportRows[$rowId][$column.$rowId]=$thisAllocatedAmount;
								}
								$SubtractOriginalAmounts[$column][] = $column.$rowId;
									//echo "actual: ".$dataDisplay." allocated: ".$allocatedAmounts."<br>";
							}
						}
						$rowId++;
					}
					//echo "<hr>";
				}
//				print_pre($BillableHours["SECD"]);
//				print_pre($BillableHours["RBDG"]);
				$LastRow = ($rowId-1);
				//print_pre($reportRows);
				//print_pre($totals);
				//include summary rows
				$reportRows[2]["P2"] = "sum of all hours";
				$reportRows[$rowId]["B".$rowId] = "TOTAL adjusted +Orginial";
				$reportRows[$rowId]["N".$rowId] = "=M".$rowId."+SUM(N3:O".$LastRow.")";
				$reportRows[$rowId]["B".($rowId+1)] = "Subtract orginial";
				$reportRows[$rowId]["B".($rowId+2)] = "TOTAL adjusted  SUM";
				
				//include Prediction DataTable
				$reportRows[2]["T2"] = "Total Hours Billed to each grant";
				$reportRows[3]["T3"] = "Grant Source";
				$reportRows[3]["U3"] = "multiplier";
				$reportRows[3]["V3"] = "Development";
				$reportRows[3]["W3"] = "TA";
				$reportRows[3]["X3"] = "Database";
				$reportRows[3]["Y3"] = "BMP";
				$reportRows[3]["Z3"] = "Incentive";
				$reportRows[3]["AA3"] = "Total Hours to each grant";
				$reportRows[3]["AC3"] = "predicted dollar amount";
				$reportRows[4]["T4"] = "Total Hours Billed by code";
				$reportRows[4]["V4"] = "=D".($rowId+2)."+E".($rowId+2);
				$reportRows[4]["W4"] = "=G".($rowId+2)."+H".($rowId+2);
				$reportRows[4]["X4"] = "=I".($rowId+2)."+J".($rowId+2);
				$reportRows[4]["Y4"] = "=F".($rowId+2);
				$reportRows[4]["Z4"] = "=K".($rowId+2)."+L".($rowId+2);
				$predictRow = 5;
				foreach ($allocationMultiplier as $funder=>$allocationInfo){
					$averageHourlyRate = floatval($allocationInfo["averageHourlyRate"]);
					$multiplier = floatval($allocationInfo["multiplier"]);
					$reportRows[$predictRow]["T".$predictRow]=$funder;
					$reportRows[$predictRow]["U".$predictRow]=$multiplier;
					$reportRows[$predictRow]["V".$predictRow]="=U".$predictRow."*V4";
					$reportRows[$predictRow]["W".$predictRow]="=U".$predictRow."*W4";
					$reportRows[$predictRow]["X".$predictRow]="=U".$predictRow."*X4";
					$reportRows[$predictRow]["Y".$predictRow]="=U".$predictRow."*Y4";
					$reportRows[$predictRow]["Z".$predictRow]="=U".$predictRow."*Z4";
					$reportRows[$predictRow]["AA".$predictRow]="=SUM(V".$predictRow.":Z".$predictRow.")".($funder == "SECD"?"+SUM(O3:O".$LastRow.")" : "");
					$reportRows[$predictRow]["AB".$predictRow]=$averageHourlyRate;
					$reportRows[$predictRow]["AC".$predictRow]="=AA".$predictRow."*AB".$predictRow;
					$predictRow++;
				}
				$reportRows[$predictRow]["T".$predictRow] = "formula check";
				$reportRows[$predictRow]["V".$predictRow] = "=SUM(V5:V".($predictRow-1).")";
				$reportRows[$predictRow]["W".$predictRow] = "=SUM(W5:W".($predictRow-1).")";
				$reportRows[$predictRow]["X".$predictRow] = "=SUM(X5:X".($predictRow-1).")";
				$reportRows[$predictRow]["Y".$predictRow] = "=SUM(Y5:Y".($predictRow-1).")";
				$reportRows[$predictRow]["Z".$predictRow] = "=SUM(Z5:Z".($predictRow-1).")";
				$reportRows[$predictRow]["AA".$predictRow] = "=SUM(AA5:AA".($predictRow-1).")";
				
				$rangeRow = $predictRow+3;
				$reportRows[$rangeRow]["U".$rangeRow] = date("F", strtotime($invoiceDate))." Bill Prediction";
				$rangeRow++;
				$reportRows[$rangeRow]["T".$rangeRow] = "high";
				$reportRows[$rangeRow]["U".$rangeRow] = "=200*U5";
				$reportRows[$rangeRow]["V".$rangeRow] = "=U".$rangeRow."*AB5";
				$rangeRow++;
				$reportRows[$rangeRow]["T".$rangeRow] = "mid";
				$reportRows[$rangeRow]["U".$rangeRow] = "=180*U5";
				$reportRows[$rangeRow]["V".$rangeRow] = "=U".$rangeRow."*AB5";
				$rangeRow++;
				$reportRows[$rangeRow]["T".$rangeRow] = "low";
				$reportRows[$rangeRow]["U".$rangeRow] = "=160*U5";
				$reportRows[$rangeRow]["V".$rangeRow] = "=U".$rangeRow."*AB5";
				
				include_once('salesforce/reports/reportMFEP.php');
				echo $allocationFileLink;
			}//end if NoStaffHours
		echo "</div>";
		echo "<div id='BillableHours' class='tab-content'>";
			$reportRows = array();
			if (!count($BillableHoursHeader)){
				echo "No Billable Hours";
			}else{
				ksort($BillableHoursHeader);
				echo "<h3>Totals Check</h3>";
				$columnNumber = 2;
				foreach ($allocationMultiplier as $funder=>$allocationInfo){
					$funderRowId = 1;
					$funderColumnNumber = 2;
					$reportRows[1][getNameFromNumber($columnNumber)."1"] = $funder;
					$funderRows[$funder][1][getNameFromNumber(1)."1"] = $funder;
					$rowId = 3;
					echo "<h4>".$funder."</h4>";
					echo "<table class='simpleTable'><thead><tr><th>Employee_Name</th>";
						$funderColumn[$funder]["Start"]=getNameFromNumber($columnNumber);
						foreach ($BillableHoursHeader as $billCode){
							echo "<th>".$billCode."</th>";
							$funderColumn[$funder][$billCode]=getNameFromNumber($columnNumber);
							$thisFunderColumn[trim($billCode)]=getNameFromNumber($funderColumnNumber);
							$reportRows[2][getNameFromNumber($columnNumber)."2"] = $billCode;
							$columnNumber++;
							$funderRows[$funder][2][getNameFromNumber($funderColumnNumber)."2"] = $billCode;
							$funderColumnNumber++;
						}
						$funderColumn[$funder]["Total"]=getNameFromNumber($columnNumber);
						$funderColumn[$funder]["AllHoursTotal"]=getNameFromNumber($columnNumber+1);
						$funderTotals[] = getNameFromNumber($columnNumber);
						$reportRows[2][getNameFromNumber($columnNumber)."2"] = "Total";
						$funderRows[$funder][2][getNameFromNumber($funderColumnNumber)."2"] = "Total";
						$thisFunderColumn["Total"]=getNameFromNumber($funderColumnNumber);
						$columnNumber++;
						$funderColumnNumber++;
					echo "</tr></thead><tbody>";
					$funderRowId = $funderRowId+2;
					foreach ($BillableHours as $Employee_Name=>$hoursInfo){
						if (!array_key_exists($Employee_Name,$allocationMultiplier)){
							echo "<tr><td>".$Employee_Name."</td>";
							$reportRows[$rowId]["A".$rowId] = str_replace("_",", ",$Employee_Name);
							$funderRows[$funder][$funderRowId]["A".$funderRowId] = str_replace("_",", ",$Employee_Name);
							foreach ($BillableHoursHeader as $billCode){
								echo "<td>".$hoursInfo[$funder][$billCode]."</td>";
								$reportRows[$rowId][$funderColumn[$funder][$billCode].$rowId] = $hoursInfo[$funder][$billCode];
								$funderRows[$funder][$funderRowId][$thisFunderColumn[$billCode].$funderRowId] = "='totals check'!".$funderColumn[$funder][$billCode].$rowId;
	//							$funderRows[$funder][$funderRowId][$thisFunderColumn[$billCode].$funderRowId] = $hoursInfo[$funder][$billCode];

								$LastColumn = $funderColumn[$funder][$billCode];
								$LastFunderColumn = $thisFunderColumn[$billCode];
							}
							$reportRows[$rowId][$funderColumn[$funder]["Total"].$rowId] = "=SUM(".$funderColumn[$funder]["Start"].$rowId.":".$LastColumn.$rowId.")";
							$funderRows[$funder][$funderRowId][$thisFunderColumn["Total"].$funderRowId] = "=SUM(B".$funderRowId.":".$LastFunderColumn.$funderRowId.")";
							$funderEmployeTotalRow[$funder][$Employee_Name] = $funderRowId;
							echo "</tr>";
							$reportRows[$rowId][$funderColumn[$funder]["AllHoursTotal"].$rowId] = "=SUM(".implode($rowId.",",$funderTotals).$rowId.")";
							$LastRow = $rowId;
							$LastFunderRow = $funderRowId;
							$rowId++;
							$funderRowId++;
						}
					}
						echo "<tr><td>".$funder." Totals</td>";
						foreach ($BillableHoursHeader as $billCode){
							echo "<td>".$BillableHours[$funder][$billCode]."</td>";
						}
						echo "</tr>";
					echo "</tbody></table><hr>";
					
					$funderRows[$funder][$funderRowId]["A".$funderRowId] = "TOTAL";
					foreach ($thisFunderColumn as $column){
							$funderRows[$funder][$funderRowId][$column.$funderRowId] = "=SUM(".$column."3:".$column.$LastFunderRow.")";
					}
					if($funder=="MDAR"){
						$funderRowId = $funderRowId+2;
						$funderRows[$funder][$funderRowId]["E".$funderRowId] = "Total Combined Areas for Liz Double Check";
						$funderRowId++;
						$StartMDARCombinedRows = $funderRowId;
						foreach ($BillableHours as $Employee_Name=>$hoursInfo){
							if (!array_key_exists($Employee_Name,$allocationMultiplier)){
								$funderRows[$funder][$funderRowId]["A".$funderRowId] = str_replace("_",", ",$Employee_Name);
								foreach ($BillableHoursHeader as $billCode){
									switch($billCode){
										case "560A":
											$funderRows[$funder][$funderRowId][$thisFunderColumn[$billCode].$funderRowId] = "=SUM(".$thisFunderColumn[$billCode].($funderRowId-($LastFunderRow+1))."+".$thisFunderColumn["560A-R"].($funderRowId-($LastFunderRow+1)).")";
											break;
										case "560C":
											$funderRows[$funder][$funderRowId][$thisFunderColumn[$billCode].$funderRowId] = "=SUM(".$thisFunderColumn[$billCode].($funderRowId-($LastFunderRow+1))."+".$thisFunderColumn["560C-R"].($funderRowId-($LastFunderRow+1)).")";
											break;
										case "560D":
											$funderRows[$funder][$funderRowId][$thisFunderColumn[$billCode].$funderRowId] = "=SUM(".$thisFunderColumn[$billCode].($funderRowId-($LastFunderRow+1))."+".$thisFunderColumn["560D-R"].($funderRowId-($LastFunderRow+1)).")";
											break;
										case "560IA":
											$CoverPageLogic["560i-ia CP Calcs"][$Employee_Name] = $hoursInfo[$funder][$billCode];
											break;
									}
								}
								$LastMDARCombinedRows = $funderRowId;
								$funderRowId++;
							}
						}
						$funderRows[$funder][$funderRowId]["A".$funderRowId] = "TOTAL";
						foreach ($thisFunderColumn as $column){
								$funderRows[$funder][$funderRowId][$column.$funderRowId] = "=SUM(".$column.$StartMDARCombinedRows.":".$column.$LastMDARCombinedRows.")";
						}
					}
					
					if($funder=="RBDG"){
						$funderRowId = $funderRowId+2;
						$funderRows[$funder][$funderRowId]["E".$funderRowId] = "USDA RBDG Summary for Quinn";
						$funderRowId++;
						$funderRows[$funder][$funderRowId]["A".$funderRowId] = "PERSONNEL";
						$funderRows[$funder][$funderRowId]["B".$funderRowId] = "MFEP Rate";
						$funderRows[$funder][$funderRowId]["C".$funderRowId] = "Cost";
						$funderRows[$funder][$funderRowId]["D".$funderRowId] = "Hours";
						
						$funderRows[$funder][$funderRowId]["F".$funderRowId] = "Fed Grant Rate";
						$funderRows[$funder][$funderRowId]["G".$funderRowId] = "Direct Hours";
						$funderRows[$funder][$funderRowId]["H".$funderRowId] = "Direct Cost";
						$funderRows[$funder][$funderRowId]["I".$funderRowId] = "Total Fed Grant Cost";
						$funderRows[$funder][$funderRowId]["K".$funderRowId] = "CET Revenue Code";
						
						$funderRowId++;
						$StartRBDGCombinedRows = $funderRowId;
						foreach ($BillableHours as $Employee_Name=>$hoursInfo){
							if (!array_key_exists($Employee_Name,$allocationMultiplier)){
								$funderRows[$funder][$funderRowId]["A".$funderRowId] = str_replace("_",", ",$Employee_Name);
								$funderRows[$funder][$funderRowId]["B".$funderRowId] = ($BillingRateByEmployeeJustRate[$Employee_Name] ? : $BillingRateByCategory["Administrative Staff"]->GBSBillingRate_Rate);
								$funderRows[$funder][$funderRowId]["C".$funderRowId] = "=(+D".$funderRowId."*B".$funderRowId.")";
								foreach ($BillableHoursHeader as $billCode){
									$funderRows[$funder][$funderRowId]["D".$funderRowId] = "=".$thisFunderColumn["Total"].$funderEmployeTotalRow[$funder][$Employee_Name];
									$thisFedRate = $FedBillingRateByEmployeeJustRate[$Employee_Name];
									if (!$thisFedRate){
										if (!is_array($Adjustments["Alerts"]["Fed Grant Billing Rate Missing"])){$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"] = array();}
										if (!in_array(str_replace("_",", ",$Employee_Name),$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"])){
											$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"][] = str_replace("_",", ",$Employee_Name);
										}
									}
									$funderRows[$funder][$funderRowId]["F".$funderRowId] = $FedBillingRateByEmployeeJustRate[$Employee_Name];
									$funderRows[$funder][$funderRowId]["G".$funderRowId] = $totalHoursByCodeByEmployee["561"][$Employee_Name];
									$funderRows[$funder][$funderRowId]["H".$funderRowId] = "=(+G".$funderRowId."*F".$funderRowId.")";
									$funderRows[$funder][$funderRowId]["I".$funderRowId] = "=((+G".$funderRowId."+D".$funderRowId.")*F".$funderRowId.")";
									$funderRows[$funder][$funderRowId]["K".$funderRowId] = $EmployeesRevenueCodeByName[$Employee_Name];
								}
								$LastRBDGCombinedRows = $funderRowId;
								$funderRowId++;
							}
						}
						$funderRowId++;
						$TotalRBDGCombinedRow = $funderRowId;
						$funderRows[$funder][$funderRowId]["A".$funderRowId] = "TOTAL PERSONNEL";
						$funderRows[$funder][$funderRowId]["C".$funderRowId] = "=SUM(C".$StartRBDGCombinedRows.":C".$LastRBDGCombinedRows.")";
						$funderRows[$funder][$funderRowId]["D".$funderRowId] = "=SUM(D".$StartRBDGCombinedRows.":D".$LastRBDGCombinedRows.")";
						$funderRows[$funder][$funderRowId]["G".$funderRowId] = "=SUM(G".$StartRBDGCombinedRows.":G".$LastRBDGCombinedRows.")";
						$funderRows[$funder][$funderRowId]["H".$funderRowId] = "=SUM(H".$StartRBDGCombinedRows.":H".$LastRBDGCombinedRows.")";
						$funderRows[$funder][$funderRowId]["I".$funderRowId] = "=SUM(I".$StartRBDGCombinedRows.":I".$LastRBDGCombinedRows.")";
					}
					if($funder=="SECD"){
						$funderRowId = $funderRowId+2;
						$funderRows[$funder][$funderRowId]["E".$funderRowId] = "USDA RBDG Summary for Quinn";
						$funderRowId++;
						$funderRows[$funder][$funderRowId]["A".$funderRowId] = "PERSONNEL";
						$funderRows[$funder][$funderRowId]["B".$funderRowId] = "MFEP Rate";
						$funderRows[$funder][$funderRowId]["C".$funderRowId] = "Cost";
						$funderRows[$funder][$funderRowId]["D".$funderRowId] = "Hours";
						
						$funderRows[$funder][$funderRowId]["F".$funderRowId] = "Fed Grant Rate";
						$funderRows[$funder][$funderRowId]["G".$funderRowId] = "Direct Hours";
						$funderRows[$funder][$funderRowId]["H".$funderRowId] = "Direct Cost";
						$funderRows[$funder][$funderRowId]["I".$funderRowId] = "Total Fed Grant Cost";
						$funderRows[$funder][$funderRowId]["K".$funderRowId] = "CET Revenue Code";
						
						$funderRowId++;
						$StartSECDCombinedRows = $funderRowId;
						foreach ($BillableHours as $Employee_Name=>$hoursInfo){
							if (!array_key_exists($Employee_Name,$allocationMultiplier)){
								$funderRows[$funder][$funderRowId]["A".$funderRowId] = str_replace("_",", ",$Employee_Name);
								$funderRows[$funder][$funderRowId]["B".$funderRowId] = ($BillingRateByEmployeeJustRate[$Employee_Name] ? : $BillingRateByCategory["Administrative Staff"]->GBSBillingRate_Rate);
								$funderRows[$funder][$funderRowId]["C".$funderRowId] = "=(+D".$funderRowId."*B".$funderRowId.")";
								foreach ($BillableHoursHeader as $billCode){
									$funderRows[$funder][$funderRowId]["D".$funderRowId] = "=".$thisFunderColumn["Total"].$funderEmployeTotalRow[$funder][$Employee_Name];
									$thisFedRate = $FedBillingRateByEmployeeJustRate[$Employee_Name];
									if (!$thisFedRate){
										if (!is_array($Adjustments["Alerts"]["Fed Grant Billing Rate Missing"])){$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"] = array();}
										if (!in_array(str_replace("_",", ",$Employee_Name),$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"])){
											$Adjustments["Alerts"]["Fed Grant Billing Rate Missing"][] = str_replace("_",", ",$Employee_Name);
										}
									}
									$funderRows[$funder][$funderRowId]["F".$funderRowId] = $FedBillingRateByEmployeeJustRate[$Employee_Name];
									$funderRows[$funder][$funderRowId]["G".$funderRowId] = $totalHoursByCodeByEmployee["562"][$Employee_Name];
									$funderRows[$funder][$funderRowId]["H".$funderRowId] = "=(+G".$funderRowId."*F".$funderRowId.")";
									$funderRows[$funder][$funderRowId]["I".$funderRowId] = "=((+G".$funderRowId."+D".$funderRowId.")*F".$funderRowId.")";
									$funderRows[$funder][$funderRowId]["K".$funderRowId] = $EmployeesRevenueCodeByName[$Employee_Name];
								}
								$LastSECDCombinedRows = $funderRowId;
								$funderRowId++;
							}
						}
						$funderRowId++;
						$TotalSECDCombinedRow = $funderRowId;
						$funderRows[$funder][$funderRowId]["A".$funderRowId] = "TOTAL PERSONNEL";
						$funderRows[$funder][$funderRowId]["C".$funderRowId] = "=SUM(C".$StartSECDCombinedRows.":C".$LastSECDCombinedRows.")";
						$funderRows[$funder][$funderRowId]["D".$funderRowId] = "=SUM(D".$StartSECDCombinedRows.":D".$LastSECDCombinedRows.")";
						$funderRows[$funder][$funderRowId]["G".$funderRowId] = "=SUM(G".$StartSECDCombinedRows.":G".$LastSECDCombinedRows.")";
						$funderRows[$funder][$funderRowId]["H".$funderRowId] = "=SUM(H".$StartSECDCombinedRows.":H".$LastSECDCombinedRows.")";
						$funderRows[$funder][$funderRowId]["I".$funderRowId] = "=SUM(I".$StartSECDCombinedRows.":I".$LastSECDCombinedRows.")";
					}
					
				}
				$reportRows[2][$funderColumn[$funder]["AllHoursTotal"]."2"] = "ALL HOURS TOTAL";
				$reportRows[$rowId]["A".$rowId] = "TOTAL";
				foreach ($funderColumn as $funder=>$columnInfo){
					foreach ($columnInfo as $name=>$column){
						$reportRows[$rowId][$column.$rowId] = "=SUM(".$column."3:".$column.$LastRow.")";
					}
				}

				$rowId = $rowId+2;
				$reportRows[$rowId]["A".$rowId] = "All Hours Combined";
				reset($allocationMultiplier);
				$first_funder = key($allocationMultiplier);
				$rowId++;
				echo "<h4>All Hours Combined</h4>";
				echo "<table class='simpleTable'><thead><tr><th>Employee_Name</th>";
					foreach ($BillableHoursHeader as $billCode){
						echo "<th>".$billCode."</th>";
						$reportRows[$rowId][$funderColumn[$first_funder][$billCode].$rowId] = $billCode;
					}
					$combinedHeaderRow = $rowId;
					$reportRows[$rowId][$funderColumn[$first_funder]["Total"].$rowId] = "Total";
					$rowId++;
					$startAllCombinedHoursRow = $rowId;
					echo "<th>All Hours</th></tr></thead><tbody>";
					foreach ($BillableHours as $Employee_Name=>$hoursInfo){
						if (!array_key_exists($Employee_Name,$allocationMultiplier)){
							echo "<tr><td>".$Employee_Name."</td>";
							$reportRows[$rowId]["A".$rowId] = str_replace("_",", ",$Employee_Name);
							$columnCounter = 3;
							foreach ($BillableHoursHeader as $billCode){
								echo "<td>".$hoursInfo[$billCode]."</td>";
								$reportRows[$rowId][$funderColumn[$first_funder][$billCode].$rowId] = $hoursInfo[$billCode];
								$combinedLastColumn = $funderColumn[$first_funder][$billCode];
								$columnCounter++;
							}
							echo "<td>".$hoursInfo["All Hours"]."</td></tr>";
							$reportRows[$rowId][$funderColumn[$first_funder]["Total"].$rowId] = "=SUM(B".$rowId.":".$combinedLastColumn.$rowId.")";
							$reportRows[$rowId][getNameFromNumber($columnCounter).$rowId] = $TotalActualHours[$Employee_Name];
							$reportRows[$combinedHeaderRow][getNameFromNumber($columnCounter).$combinedHeaderRow] = "Actual Hours";
							$reportRows[$combinedHeaderRow][getNameFromNumber($columnCounter+1).$combinedHeaderRow] = "Variance";
							$reportRows[$rowId][getNameFromNumber($columnCounter+1).$rowId] = "=".getNameFromNumber($columnCounter).$rowId."-".getNameFromNumber($columnCounter-1).$rowId;
							$combinedLastRow = $rowId;
							$rowId++;
						}
					}
					$reportRows[$rowId]["A".$rowId] = "TOTAL";
					foreach ($BillableHoursHeader as $billCode){
						$column = $funderColumn[$first_funder][$billCode];
						$reportRows[$rowId][$column.$rowId] = "=SUM(".$column.$startAllCombinedHoursRow.":".$column.($rowId-1).")";
					}
					$reportRows[$rowId][$funderColumn[$first_funder]["Total"].$rowId] = "=SUM(".$funderColumn[$first_funder]["Total"].$startAllCombinedHoursRow.":".$funderColumn[$first_funder]["Total"].($rowId-1).")";
				
				echo "</tbody></table>";
				ksort($reportRows);
				//print_pre($funderRows);
				include_once('salesforce/reports/reportMFEP_BillableHours.php');
				echo $BillableFileLink;
			}
		echo "</div>";
		
		echo "<div id='Files' class='tab-content'>";
			if (!count($BillableHoursHeader)){
				echo "No Billable Hours";
			}else{
					echo "Allocation File: ".$allocationFileLink;
					echo "Billable Hours Allocated File: ".$BillableFileLink;
					echo "<br>If you need to make allocation adjustments: Open Billable Hours Allocated File and make any necessary edits<br>Then upload approved file here:<br>";
					
					?>
					<form method="post" action="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-MFEP&type=billableHoursFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
							<div class="row">
								<div class="eight columns">
									<input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" onChange="makeFileList();" /><Br>
								</div>
								<div class="eight columns">
									<strong>File You Selected:</strong>
									<ul id="fileList"><li>No Files Selected</li></ul>
									<input type="submit" value="Begin Processing" id="submitButton" style="display:none;">
								</div>
							</div>
						
							<script type="text/javascript">
								function makeFileList() {
									var input = document.getElementById("filesToUpload");
									var ul = document.getElementById("fileList");
									while (ul.hasChildNodes()) {
										ul.removeChild(ul.firstChild);
									}
									for (var i = 0; i < input.files.length; i++) {
										var li = document.createElement("li"),
											fileName = input.files[i].name,
											fileLength = fileName.length,
											fileExt = fileName.substr(fileLength-4);
											console.log(fileExt);
										if (fileExt != "xlsx"){
											li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
											ul.appendChild(li);
											document.getElementById('submitButton').style.display = 'none';
										}else{
											li.innerHTML = input.files[i].name;
											ul.appendChild(li);
											document.getElementById('submitButton').style.display = 'block';
										}
									}
									if(!ul.hasChildNodes()) {
										var li = document.createElement("li");
										li.innerHTML = 'No Files Selected';
										ul.appendChild(li);
										document.getElementById('submitButton').style.display = 'none';
									}
								}
							</script>
						</form>
		<?php	
			}//end if !count($BillableHoursHeader)

		echo "</div>";
		
		echo "<div id='CoverPageLogic' class='tab-content'>";
			if(count($_FILES['filesToUpload'])) {
				$tmpFileName = $_FILES["filesToUpload"]["tmp_name"][0];
				if ($_GET['type'] == "billableHoursFile"){
					$billableHoursFileReceived = true;
					$billableHoursFileName = "salesforce/invoicingExports/560_MFEP/BillableHoursAllocatedFileToUse".date("Y_m_d").".xlsx";
					$target_file = $siteRoot.$adminFolder."gbs/".$billableHoursFileName;
					move_uploaded_file($tmpFileName, $target_file);
					
					$billableHoursFileName = "importfiles/MFEPBillableHoursAllocatedFile.xlsx";
					$target_file_copy = $siteRoot.$adminFolder."gbs/".$billableHoursFileName;
					copy($target_file, $target_file_copy);
					

				}
			}
			if ($billableHoursFileReceived || $_GET['useUploadedFile'] || $billableFilewebtrackingLink){
				$setTab = "CoverPageLogic";
				//new import that data and parse it out 
				if ($billableFilewebtrackingLink){
					$trackingFile = $billableFilewebtrackingLink;
					//echo $trackingFile."<br>";
				}
				if ($billableHoursFileReceived || $_GET['useUploadedFile']){
					$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $billableHoursFileName);
				}
				$_SESSION['MFEPBillableHoursFileName'] = $trackingFile;
				$runCoverPageLogiTab = true;
			}
			if ($runCoverPageLogiTab){
				include_once('salesforce/read_560MFEPBillableHoursFile.php');
				//create 560i-ia CP Calcs, 560D tab data
				//print_pre($CoverPageTemplateRows);
				foreach ($CoverPageTemplateRows as $tabRows=>$codes){
					//print_pre($tabRows);
					//foreach ($tabRows as $id=>$codes){
//						print_pre($codes);
						foreach ($codes as $code=>$hours){
							if ($code == "560IA" && $hours>0){
								$CoverPageTabName = "560i-ia CP Calcs";
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name."=".$hours."<br>";
									$CoverPageTemplateDataHours[$CoverPageTabName][$Employee_Name]["C"]=$hours;
									$InvoiceDataHours["560IA"][$Employee_Name][$Column]=$hours;
								}
							}
							if ($code == "560I" && $hours>0){
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name."=".$hours."<br>";
									$InvoiceDataHours["560I"][$Employee_Name][$Column]=$hours;
								}
							}
							if ($code == "560B" && $hours>0){
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name."=".$hours."<br>";
									$InvoiceDataHours["560B"][$Employee_Name][$Column]=$hours;
								}
							}
							if (($code == "560D" || $code == "560D-R")  && $hours>0){
								$CoverPageTabName = "560D CP Calcs";
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name."=".$hours."<br>";
									$Column = ($code=="560D-R" ? "C" : "D");
									$CoverPageTemplateDataHours[$CoverPageTabName][$Employee_Name][$Column]=$hours;
									$InvoiceDataHours["560D"][$Employee_Name][$Column]=$hours;
								}
							}
							if (($code == "560C" || $code == "560C-R")  && $hours>0){
								$CoverPageTabName = "560C CP Calcs";
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name."=".$hours."<br>";
									$Column = ($code=="560C-R" ? "C" : "D");
									$CoverPageTemplateDataHours[$CoverPageTabName][$Employee_Name][$Column]=$hours;
									$InvoiceDataHours["560C"][$Employee_Name][$Column]=$hours;
								}
							}
							if (($code == "560A" || $code == "560A-R")  && $hours>0){
								$CoverPageTabName = "560A CP Calcs";
								$Employee_Name = $codes["Employee_Name"];
								if ($Employee_Name != "TOTAL"){
									//echo $Employee_Name.$Column."=".$hours."<br>";
									$Column = ($code=="560A-R" ? "C" : "D");
									$CoverPageTemplateDataHours[$CoverPageTabName][$Employee_Name][$Column]=$hours;
									$InvoiceDataHours["560A"][$Employee_Name][$Column]=$hours;
								}
							}
							
							
						}
					//}
				}// end foreach CoverPageTemplateRows
				ksort($CoverPageTemplateDataHours);
				ksort($InvoiceDataHours);
				$IncludedEmployees = array();
				foreach ($BillingRateByCategory as $BillingCategory=>$RateInfo){
					foreach ($CoverPageTemplateDataHours as $CoverPageTabName=>$EmployeeHours){
						foreach ($EmployeeHours as $Employee_Name=>$hours){
							if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingCategory]){
								$CoverPageTemplateData[$CoverPageTabName][$BillingCategory][$Employee_Name]=$hours;
								$IncludedEmployees[] = $Employee_Name;
							}
							if ($BillingCategory == "Administrative Staff" && !in_array($Employee_Name,$IncludedEmployees)){
								$CoverPageTemplateData[$CoverPageTabName][$BillingCategory][$Employee_Name]=$hours;
								if (!in_array($Employee_Name,$Adjustments["Alerts"]["These Employees Are Not Included in any Billing Rate Category and have been included in the Administrative Staff Category"])){
									$Adjustments["Alerts"]["These Employees Are Not Included in any Billing Rate Category and have been included in the Administrative Staff Category"][]=$Employee_Name;
								}
							}
						}
					}
					foreach ($InvoiceDataHours as $InvoiceCode=>$EmployeeHours){
						$InvoiceData[$InvoiceCode][$BillingCategory] = array();
						foreach ($EmployeeHours as $Employee_Name=>$hours){
							if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingCategory]){
								$InvoiceData[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							}
							if ($BillingCategory == "Administrative Staff" && !in_array($Employee_Name,$IncludedEmployees)){
								$InvoiceData[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							}
						}
					}
					
				}

				//print_pre($CoverPageTemplateData);
				//print_pre($InvoiceDataHours);
				include_once('salesforce/reports/560MFEP_CoverPageLogic.php');
				echo $MFEPCoverPageLogicLink;
				
				
				//get MFEP Reports from SalesForceReport
				require_once 'salesforce/config.php';
				require_once('salesforce/rest_functions.php');
				$access_token = $_SESSION['access_token'];
				$instance_url = $_SESSION['instance_url'];
				$reportId = "00OU0000003EDWO"; //Invoicing/MFEP - New Accounts Last Month
				//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
				if (!checkSession($instance_url, $reportId, $access_token)){
					echo "<script>window.location.replace('".AUTH_URL."');</script>";
					//echo AUTH_URL;
					//header('Location: '.AUTH_URL);
				}else{					
					ob_start();
					
					if (!isset($access_token) || $access_token == "") {
						die("Error - access token missing from session!");
					}
					if (!isset($instance_url) || $instance_url == "") {
						die("Error - instance URL missing from session!");
					}
						$newFarmsReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP - New Accounts Last Month</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){
							echo $invoiceDate." ".date("m/01/Y",strtotime($TodaysDate))."Start Date of Time Period is Current Month so Items are pulling Salesforce data for current month<br>";
							$reportId = "00OU0000003e84y"; //Invoicing/MFEP - New Accounts This Month
							$newFarmsReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP - New Accounts This Month</a>";
							echo "Using SalesForce Report ".$newFarmsReportLink."<bR>";
						}
						$reportLinks[] = $newFarmsReportLink;
						$reportMFEPNewAccounts = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$fieldNamesArray = $reportMFEPNewAccounts["fieldNamesArray"];
						$reportRowsUnSorted = $reportMFEPNewAccounts["rows"];
						$newFarms = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						//print_pre($newFarms);
						$newFarmCount = count($newFarms);
						
						
						//Get Completed Walkthrough
						$reportId = "00OU0000003EDWT"; //Invoicing/MFEP Walkthrough Completed
						$scopedAuditsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Walkthrough Completed</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){
							$reportId = "00OU0000003e85m"; //Invoicing/MFEP Walkthrough Completed This Month
							$scopedAuditsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Walkthrough Completed This Month</a>";
							echo "Using SalesForce Report ".$scopedAuditsLink."<br>";
						}	
						$reportLinks[] = $scopedAuditsLink;
						$reportMFEPWalkthroughComplete = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $reportMFEPWalkthroughComplete["rows"];
						//look for issues
						$scopingAudits = array();
						foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
							$owner = $rowDetail["Upgrade__c.Owner.Name"];
							$accountName = $rowDetail["Account.Name"];
							$address = $rowDetail["Account.BillingAddress"];
							$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
							$greenProspectName = $rowDetail["Upgrade__c.Name"];
							$greenProspectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
							$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
							$GPNameLink = $rowDetail["GPNameLinkValue"];
							$GPNameHref = " GPName: <a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							//make sure completed date within range
							if (strtotime($completedDate) >= strtotime($StartDate) && strtotime($completedDate) <= strtotime($EndDate)){
								if (!strpos(strtolower($statusDetail),"walkthrough")){
									$Adjustments["Walkthrough-EAP"]["Current Status details do not mention 'walkthrough'"][] = "<u>Possible Wrong GPType</u>: ".$accountName.$GPNameHref." [GPType: ".$greenProspectType."] Walkthrough-EAP Current Status details: ".$statusDetail." [owned by ".$owner."]";
								}else{
									$Adjustments["Review"]["Walkthrough-EAP Current Status to make sure to be included as completed Walkthough"][] = $accountName.$GPNameHref." Walkthrough-EAP Current Status details: ".$statusDetail." [owned by ".$owner."]";
									$scopingAudits[] = array("name"=>$accountName,"address"=>$address,"AccountNameLinkValue"=>$AccountNameLink);
								}
							}
						}
						$scopingAuditCount = count($scopingAudits);
						
						$reportId = "00OU0000003EDWi"; //Invoicing/MFEP Completed Projects funded by MFEP
						$MFEPProjectsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Complete Projects</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){		
							$reportId = "00OU0000003e85r"; //Invoicing/MFEP Completed Projects funded by MFEP This Month
							$MFEPProjectsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Complete Projects This Month</a>";
							echo "Using SalesForce Report ".$MFEPProjectsLink."<br>";
						}	
						$reportLinks[] = $MFEPProjectsLink;
						$repotMFEPProjects = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $repotMFEPProjects["rows"];
						//print_pre($reportRowsUnSorted);
						$MFEPProjects = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						//print_pre($newFarms);
						$MFEPProjectsCount = count($MFEPProjects);
						//print_pre($MFEPProjects);

						$projectIncentives = array();
						foreach ($MFEPProjects as $rowId=>$rowDetail){
							$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
							$accountName = (trim($rowDetail["Account.Name"]) == 'Massachusetts Farm Energy Program (MFEP)' ? $otherExplain : $rowDetail["Account.Name"]);
							$address = $rowDetail["Account.BillingAddress"];
							$greenProspectName = $rowDetail["Upgrade__c.Name"];
							$gpType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
							$dateCompleted = date("m/d/Y",strtotime($rowDetail["Upgrade__c.GP_Stage_Completed__c"]));
							$fundingSource = $rowDetail["Metric__c.Funding_Source__c"];
							$incentiveValue = $rowDetail["Metric__c.Grant_Incentive_Value__c"];
							$incentiveValue = ereg_replace("[^0-9.]", "", $incentiveValue); 
							$GPNameLink = $rowDetail["GPNameLinkValue"];
							$GPNameHref = " GPName: <a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$projectIncentives[] = array("name"=>$accountName,"address"=>$address,"source"=>$fundingSource,"incentive"=>$incentiveValue,"AccountNameLink"=>$AccountNameLink,"date"=>$dateCompleted);
							$projectIncentivesByAccountGPType[$accountName][$gpType] = 1;
							//look for status detail with walkthrough in it but wrong GPType
						}
						
						$reportId = "00OU0000003EDWY"; //Invoicing/MFEP Bullets
						$reportMFEPBullets = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $reportMFEPBullets["rows"];
						$MFEPBulletsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Bullets</a>";
						$reportLinks[] = $MFEPBulletsLink;

						//print_pre($reportRowsUnSorted);
						foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
							$owner = $rowDetail["Upgrade__c.Owner.Name"];
							$accountName = $rowDetail["Account.Name"];
							$accountNameLinkValue = $rowDetail["AccountNameLinkValue"];
							$accountNamesByGP[$accountNameLinkValue] = $accountName;
							$greenProspectName = $rowDetail["Upgrade__c.Name"];
							$GPNameLink = $rowDetail["GPNameLinkValue"];
							$accountNamesByGP[$GPNameLink] = $accountName;
							$GPNameHref = " GPName: <a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
							$metricID = $rowDetail["Metric__c.Id"];
							if (trim($metricID) != "-"){
								$metricsGP[$metricID]=$GPNameLink;
								$metrics[] = $metricID;
							}
							$address = $rowDetail["Account.BillingAddress"];
							$primaryCampaign = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
							$greenProspectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
							$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
							$phase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
							//look for status detail with walkthrough in it but wrong GPType
							//filter out unnecessary items for bullets as explained by Megan Denardo 6/1/2016
							if  ($greenProspectType != "560 Outreach Call" && $greenProspectType != "560 Hotline Inquiry"){
								
								if (strpos(strtolower($statusDetail),"walkthrough") && $greenProspectType !="560 Walkthrough-EAP"){
									$Adjustments["Review"]["Bullet Mentions Walkthrough but GPType does not equal 560 Walkthrough-EAP<br>".$MFEPBulletsLink][] = "<u>Possible Wrong GPType</u>: ".$accountName.$GPNameHref." [GPType: ".$greenProspectType."] Current Status details: ".$statusDetail." [owned by ".$owner."]";
								}
								$bullets[]=array("name"=>$accountName,"accountId"=>$accountNameLinkValue,"gpId"=>$GPNameLink,"gpName"=>$greenProspectName,"owner"=>$owner,"primaryCampaign"=>$primaryCampaign,"GPType"=>$greenProspectType,"GPName"=>$greenProspectName,"statusDetail"=>$statusDetail,"phase"=>$phase);
							}
							
						}
						//print_pre($metrics);
						
						//Get the Metrics for the GP's
						include_once('salesforce/soap_connect.php');
						$metricsToGet = implode("','",$metrics);
						$query = "SELECT Id, Green_Prospect__c, Number__c, Unit__c, Material__c, Other_Explain__c, Grant_Incentive_Value__c, Measure_Cost__c, Funding_Source__c, Annual_Savings_Resulting_From_Mea__c FROM Metric__c WHERE ID IN ('".$metricsToGet."')";
						//echo $query;
						$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
						//print_pre($response);
						foreach ($response->records as $record){
							$infoArray = array(
								"Funding_Source"=>$record->Funding_Source__c,
								"Number"=>$record->Number__c,
								"Material"=>$record->Material__c,
								"Other_Explain"=>$record->Other_Explain__c,
								"Measure_Cost\$"=>$record->Measure_Cost__c,
								"Grant_Incentive\$"=>$record->Grant_Incentive_Value__c,
								"Annual_Savings\$"=>$record->Annual_Savings_Resulting_From_Mea__c
							);
								
							$metricsLookup[$record->Green_Prospect__c][]=$infoArray;
						}
						//print_pre($metricsLookup);
						
						
						$bulletsSorted = array_orderby($bullets,"GPType",SORT_ASC,"name",SORT_ASC);//not used to create Report because nuance for decidine which bullet goes where and what to say.  Requires human touch
						foreach ($bulletsSorted as $bullet){
							$gpType = $bullet["GPType"];
							$gpId = $bullet["gpId"];
							$gpName = $bullet["gpName"];
							$accountName = $bullet["name"];
							$code = ($GPTypeBulletCode[$gpType] ? : "560B"); //default bullet code to use incase GPType not in database
							if (!$GPTypeBulletCode[$gpType]){$Adjustments["Alert"]["GP Type with no Bullet Code Allocation"][] = $gpType;}
							if ($projectIncentivesByAccountGPType[$accountName][$gpType]){$code = "560I";}
							$Adjustments["Review"]["Bullets<br>".$MFEPBulletsLink][$gpType] = " using bullet code ".$code;
							$gpMetrics = $metricsLookup[$gpId];
							$status = (strpos(" ".strtolower($bullet["phase"]),"completed") ? "Completed" : (strpos(" ".strtolower($bullet["phase"]),"in-progress") ? "In-Progress" : $bullet["phase"]));
							$bulletArray = array("name"=>$bullet["name"],"gpName"=>$gpName,"accountId"=>$bullet["accountId"],"code"=>$code,"status"=>$status,"deliverables"=>$bullet["statusDetail"],"metrics"=>$gpMetrics);
							$BulletDataByCode[$code][] = $bulletArray;
							$BulletDataByFarm[$bullet["name"]][] = $bulletArray;
							$BulletData[] = $bulletArray;
							$gpTypes[$bullet["GPType"]] = 1;
							
						}
						//print_pre($BulletData);
						
						$reportId = "00OU0000003EDWd"; //Invoicing/MFEP Complete Audit by Month
						$MFEPAuditsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Complete Audit</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){
							$reportId = "00OU0000003e866"; //Invoicing/MFEP Complete Audit by This Month
							$MFEPAuditsLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Complete Audit This Month</a>";
							echo "Using SalesForce Report ".$MFEPAuditsLink."<br>";
						}	
						$reportLinks[] = $MFEPAuditsLink;
						$reportMFEPAudits = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						//print_pre($reportMFEPAudits);
						$reportRowsUnSorted = $reportMFEPAudits["rows"];
						$MFEPAudits = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						//print_pre($newFarms);
						$MFEPAuditsCount = count($MFEPAudits);
						//print_pre($MFEPAudits);
						//echo "Lauren";

						$auditIncentives = array();
						foreach ($MFEPAudits as $rowId=>$rowDetail){
							$owner = $rowDetail["Upgrade__c.Owner.Name"];
							$accountName = $rowDetail["Account.Name"];
							$address = $rowDetail["Account.BillingAddress"];
							$greenProspectName = $rowDetail["Upgrade__c.Name"];
							$dateCompleted = date("m/d/Y",strtotime($rowDetail["Upgrade__c.GP_Stage_Completed__c"]));
							$fundingSource = $rowDetail["Metric__c.Funding_Source__c"];
							$incentiveValue = $rowDetail["Metric__c.Grant_Incentive_Value__c"];
							$incentiveValue = ereg_replace("[^0-9.]", "", $incentiveValue); 
							$GPNameLink = $rowDetail["GPNameLinkValue"];
							$GPNameHref = " GPName: <a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$auditIncentives[] = array("name"=>$accountName,"address"=>$address,"source"=>$fundingSource,"incentive"=>$incentiveValue,"AccountNameLink"=>$AccountNameLink,"date"=>$dateCompleted);
							//look for status detail with walkthrough in it but wrong GPType
						}
						//print_pre($auditIncentives);
						$reportId = "00OU0000003EDZ3"; //Invoicing/MFEP Jobs Active in Previous Month
						$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Jobs Active</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){							$reportId = "00OU0000003e86B"; //Invoicing/MFEP Jobs Active in This Month
							$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Jobs Active This Month</a>";
							echo "Using SalesForce Report ".$MFEPReportLink."<br>";
						}	
						$reportLinks[] = $MFEPReportLink;
						$repotMFEPJobsActive = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $repotMFEPJobsActive["rows"];
						$MFEPJobsActive = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						$MFEPJobsActiveCount = count($MFEPJobsActive);
	
						//print_pre($MFEPJobsActive);
						foreach ($MFEPJobsActive as $rowId=>$rowDetail){
							$accountName = $rowDetail["Account.Name"];
							$city = $rowDetail["Account.BillingCity"];
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$farms[$accountName] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"active","reportLink"=>$MFEPReportLink);
							$farmsById[$AccountNameLink] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"new","reportLink"=>$MFEPReportLink);
							//look for status detail with walkthrough in it but wrong GPType
						}
						
						$reportId = "00OU0000003EDY0"; //Invoicing/560 MFEP Jobs Created in Previous Month
						$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Jobs Created</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){							$reportId = "00OU0000003e86L"; //Invoicing/560 MFEP Jobs Created This Month
							$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Jobs Created This Month</a>";
							echo "Using SalesForce Report ".$MFEPReportLink."<br>";
						}	
						$reportLinks[] = $MFEPReportLink;

						$repotMFEPJobsCreated = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $repotMFEPJobsCreated["rows"];
						$MFEPJobsCreated = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						$MFEPJobsCreatedCount = count($MFEPJobsCreated);

						foreach ($MFEPJobsCreated as $rowId=>$rowDetail){
							$accountName = $rowDetail["Account.Name"];
							$city = $rowDetail["Account.BillingCity"];
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$farms[$accountName] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"active","reportLink"=>$MFEPReportLink);
							$farmsById[$AccountNameLink] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"new","reportLink"=>$MFEPReportLink);
							//look for status detail with walkthrough in it but wrong GPType
						}
						
						$reportId = "00OU0000003EDYj"; //Invoicing/MFEP Farms Created in Previous Month
						$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Farms Created</a>";
						if (strtotime($invoiceDate)>= strtotime(date("m/01/Y",strtotime($TodaysDate)))){							$reportId = "00OU0000003e86V"; //Invoicing/MFEP Farms Created in This Month
							$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Farms Created This Month</a>";
							echo "Using SalesForce Report ".$MFEPReportLink."<br>";
						}	
						$reportLinks[] = $MFEPReportLink;
						$repotMFEPFarmsCreated = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $repotMFEPFarmsCreated["rows"];
						$MFEPFarmsCreated = array_orderby($reportRowsUnSorted, 'ACCOUNT.NAME', SORT_ASC);
						$MFEPFarmsCreatedCount = count($MFEPFarmsCreated);
	
						//print_pre($MFEPFarmsCreated);
						foreach ($MFEPFarmsCreated as $rowId=>$rowDetail){
							$accountName = $rowDetail["ACCOUNT.NAME"];
							$city = $rowDetail["ADDRESS1_CITY"];
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$farms[$accountName] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"new","reportLink"=>$MFEPReportLink);
							$farmsById[$AccountNameLink] = array("name"=>$accountName,"city"=>$city,"AccountNameLink"=>$AccountNameLink,"status"=>"new","reportLink"=>$MFEPReportLink);
							//look for status detail with walkthrough in it but wrong GPType
						}
						
						$farmsSorted = array_orderby($farms,'status', SORT_DESC,'name', SORT_ASC);
						//print_pre($farmsSorted);
						
						
						$reportId = "00OU0000002yDPg"; //Invoicing/MFEP Pre-Invoicing Completion
						$MFEPReportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>MFEP Pre-Invoicing Completion</a>";
						$reportLinks[] = $MFEPReportLink;

						$report = retrieve_report_TLevel($instance_url, $reportId, $access_token);
						$reportRowsUnSorted = $report["rows"];
						//print_pre($reportRowsUnSorted);
						foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
							$accountName = $rowDetail["Account.Name"];
							$AccountNameLink = $rowDetail["AccountNameLinkValue"];
							$greenProspectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
							$incentiveAmount = $rowDetail["Metric__c.Grant_Incentive_Value__c"];
							$record = array();
							$record["SalesforceID"] = $AccountNameLink;
							$record["AccountName"] = $accountName;
							$record["InvoiceDate"] = date("Y-m-d",strtotime($invoiceDate));
							$record["Type"] = $greenProspectType;
							$record["IncentiveAmount"] = $incentiveAmount;
							$criteria = new stdClass();
							$criteria->salesForceID = $AccountNameLink;
							$preInvoicedResults = $GBSProvider->invoiceTool_getPreInvoiced($criteria);
							$resultArray = $preInvoicedResults->collection;
							if (count($resultArray)){
								foreach ($resultArray as $result=>$record){							
									if (strtotime($record->GBSPreInvoiced_InvoiceDate) == strtotime($invoiceDate)){ 
										$preInvoicedNew[] = $record;
									}
								}
							}else{
								//print_pre($record);
								$insertResults[] = $GBSProvider->insertPreInvoiced($record);
								$record = new stdClass();
								$record->GBSPreInvoiced_AccountName = $accountName;
								$record->GBSPreInvoiced_Type = $greenProspectType;
								$record->GBSPreInvoiced_IncentiveAmount = $incentiveAmount;
								$record->GBSPreInvoiced_InvoiceDate = $invoiceDate;
								$preInvoicedNew[] = $record;
							}
						}
						//print_pre($insertResults);
						//print_pre($preInvoicedNew);
						
						
				}
				
				ksort($CoverPageItems);
				include_once('salesforce/reports/560MFEP_CoverPage.php');
				echo "CoverPage/Invoice/Reports: ".$MFEPCoverPageLink;
				echo "Billable Hours Allocated File: ".$BillableFileLink;


			}//end if billable hours file received
			$invoice550AStartDate = $invoiceDate;
			//print_pre($EmployeesInGroupByEmployeeWithHours);
			$partialPayVar = 7 + dateDiff("4/1/2018",date("m/d/Y",strtotime($invoiceDate)),"month");
			//echo $partialPayVar;
			$criteria = new stdClass;

			
			$month = date("n",strtotime($invoiceDate));
			if($month<10){
				$periodStartYear = date("Y",strtotime($invoiceDate))-1;
			}else{
				$periodStartYear = date("Y",strtotime($invoiceDate));

			}
			$criteria->periodStartDate = $periodStartYear."-10-1";
			$criteria->source="MFEP";
			$criteria->periodEndDate = date("Y-m-d",strtotime($invoiceDate." -1 month"));
			$sf270Data = $ResidentialProvider->getSF270Data($criteria);
			
			$sf270results = $sf270Data->collection;

			if (count($sf270results)) {
				foreach ($sf270results as $result){
					$SF270Data[$result->AccountType]["SF270Name"] = $result->AccountType;
					$SF270Data[$result->AccountType]["PriorPersonell"] += $result->PaymentsAmount;
					$SF270Data[$result->AccountType]["PriorExpenses"] += $result->ExpenseAmount;
					$SF270Data[$result->AccountType]["StartDate"] = date("m/d/Y",strtotime($StartDate));
					$SF270Data[$result->AccountType]["EndDate"] = date("m/d/Y",strtotime($EndDate));
					$SF270Data[$result->AccountType]["SubmittedDate"] = date("m/15/Y");
					$SF270Data[$result->AccountType]["RequestNumber"] = $partialPayVar;
					$SFTableHeaders[$result->AccountType] = $result->AccountType;
	
				}
			}else{
				$SFTableHeaders = array("MA SECD","MA RBDG");
			}
			$queryStringItems = array();
			foreach ($_GET as $key=>$val){
				$queryStringItems[] = $key."=".$val;
			}
			$queryString = implode("&",$queryStringItems);
			if (count($_POST)){
				foreach ($_POST as $key=>$val){
					$keyParts = explode("|",$key);
					$SF270Data[str_replace("_"," ",$keyParts[0])][$keyParts[1]] = $val;
				}
			}
			echo "Enter any SF270 Expenses or InKind amounts that should appear on the SF270 form then click 'Create SF270 PDFs' button<br>";
			echo "<form name='sfData' id='sfData' method='POST' action='?".$queryString."#Files'><table class='simpleTable'>";
			echo "<thead><tr><th>type</th>";
			foreach ($SFTableHeaders as $accountType){
				echo "<th>".$accountType."</th>";
			}
			echo "</tr></thead>";
			echo "<tbody>";
			$types = array("Expenses","InKind");
			foreach ($types as $type){
				echo "<tr><td>".$type."</td>";
				foreach ($SFTableHeaders as $accountType){
					$thisName = str_replace(" ","_",$accountType)."|".$type;
					echo "<td>$<input type='text' style='width:50px;padding-left:5px;' value='".($_POST[$thisName] ? $_POST[$thisName] : "0.00")."' name='".$thisName."'></td>";
				}
				echo "</tr>";
			}
			echo "</tbody></table></form>";
			echo "<div style='float:right;'><a href='#' id='CreatePDFS' class='button-link'>Create SF270 PDFs</a></div><br><br>";
			echo $ReportsFileLink;
			//Now create the 270PDFS
			$path = getcwd();
			if (!strpos($path,"xampp")){
				$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
				set_include_path(".:/home/cetdash/pear/share/pear");
				$thispath = $path;
			}else{
				$testingServer = true;
				$path = "";
				$thispath = "../";
			}
			$relativeLink = "../residential/";				
			require($thispath.'residential/pdfProcessor/fpdm.php');
			foreach ($FedBillingRateByEmployeeJustRate as $employee=>$rate){
				$SF270BillingRateByEmployee[$employee_name] = $rate;
			}

			if (count($SF270Data) && count($_POST)){
				foreach ($SF270Data as $sf270Name=>$data){
					// echo $sf270Name."<Br>";
					// print_pre($data);
					if ($data["SF270Name"]) {
						$fileName = str_replace(" ","-",$data["SF270Name"])."-SF270-".date("Y-m", strtotime($invoiceDate)).".pdf";
						// echo $fileName;
						$filePath = $thispath."gbs/sf270Files/".$fileName;
						$data["sponsor"] = "USDA - Rural Development";
						$data["CertificationNameAndTitle"] = "Heather McCreary, Information Services Manager";
						$data["CertificationPhone"] = "423-586-7350 x 235";
						$personellTot = $data["PriorPersonell"]+$data["Personell"];
						$data["PersonellSumA"] = money($personellTot);
						$data["PersonellSumC"] = money($personellTot);
						$data["PersonellSumE"] = money($personellTot);
						$data["PersonellSumG"] = money($personellTot);
						$expensesTot = $data["PriorExpenses"]+$data["Expenses"];
						$data["ExpensesSumA"] = money($expensesTot);
						$data["ExpensesSumC"] = money($expensesTot);
						$data["ExpensesSumE"] = money($expensesTot);
						$data["ExpensesSumG"] = money($expensesTot);
						$data["TotalA"] = money($personellTot + $expensesTot);
						$data["TotalC"] = money($personellTot + $expensesTot);
						$data["TotalE"] = money($personellTot + $expensesTot);
						$data["TotalG"] = money($personellTot + $expensesTot);
						$data["TotalF"] = money($data["InKind"]);
						$data["TotalH"] =money( $data["PriorExpenses"]+$data["PriorPersonell"]);
						$data["TotalI"] = money($data["Personell"]+$data["Expenses"]);
						$data["ProgramA"] = "Personell";
						$data["ProgramB"] = "Expenses";
						$data["PriorPersonell"] = money($data["PriorPersonell"]);
						$data["Personell"] = money($data["Personell"]);
						$data["ProgramC"] = "In-Kind";
						$data["PriorExpenses"] = money($data["PriorExpenses"]);
						$data["Expenses"] = money($data["Expenses"]);
						// process the pdfs
						$data = array_map('strval', $data);
						$data["fileLocation"] = $CurrentServer.$adminFolder."gbs/sf270Files/".$fileName;
						$data["absolutePath"] = $thispath."gbs/sf270Files/".$fileName;
						$dataArray[$data["SF270Name"]] = $data;
						// echo $data["fileLocation"];
						// print_pre($data);
						$results = $ResidentialProvider ->insertSF270Data($data,"MFEP");
						// echo "TEST";
					}else{
							echo "<br><span class = 'info alert'>No ".$sf270Name." data to display</span>";
					}	
					

					

				}
				$sf270FileName = "MFEP_";
				include_once('salesforce/reports/sf270Hours.php');
				echo "<fieldset><legend>SF270 Files</legend><br><div id='SF270Links'>";
				echo $sf270HoursFile;
				echo "</div></fieldset><br>";
			}

		echo "</div>";
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "MFEP");
		include_once('_notesTab.php');
		
		
	echo "</div>";
?>	
<br clear="all">
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">
<b>Instructions</b>
<ul>
	<li>This Process defaults to run in prior Month's period.
	<li>If the first tab to appear is 'Adjustments' review the 'Alert' category and adjust in Salesforce and rerun this process if necessary.
	<li>Go to Cover Page & Invoice tab and download <?php echo $MFEPCoverPageLink;?> and <?php echo $BillableFileLink;?>
	<br>
	If you need to adjust the hours:<br>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>2. Review the Allocation Multipliers tab.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the MFEP Files tab, download the Billable Hours File and make any allocation changes using the Allocations File for reference.  Once satisfied with the Billable Hours File, use the 'Choose File' upload button to process that file for creating the Invoicing Cover Page.
	<li<?php echo ($billableHoursFileReceived ? " class='alert'" : "");?>>4. Review the Adjustments Tab and try to correct any items.  Then re-upload Billable Hours File in the MFEP Files tab.
	<li>5. Download Cover Page Logic Template and CoverPage, Invoice file (includes Farms list and section for Report to go).
</ul>
<script type="text/javascript">
	$(function () {
		$("#CreatePDFS").on('click',function(e){
			e.preventDefault();
			$("#sfData").submit();
		});
		<?php if(count($_POST)){?>
			var sf270Data = <?php echo json_encode($dataArray);?>;
			//console.log(sf270Data);
			$.each(sf270Data,function(key,val){
				//console.log(key);
				//console.log(val);
				$.ajax({
					url: "SF270FormFill.php",
					type: "POST",
					data: JSON.stringify(val),
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					success: function(resultdata){
						$("#SF270Links").append(resultdata);
						//console.log(resultdata);
					}
				});
			})
		<?php }?>
		$(".changeAllocation").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisId = $this.attr('data-allocationId'),
				thisNameId = "#Name"+thisId,
				thisMultiplierId = "#Multiplier"+thisId,
				thisAverageHourlyRateId = "#AverageHourlyRate"+thisId,
				thisAlert = "#Alert"+thisId,
				thisNameValue = $(thisNameId).val(),
				thisMultiplierValue = $(thisMultiplierId).val(),
				thisAverageHourlyRateValue = $(thisAverageHourlyRateId).val(),
				data = {};
				
				data["action"] = "MFEP_AllocationChange";
				data["name"] = thisNameValue;
				data["multiplier"] = thisMultiplierValue;
				data["averageHourlyRate"] = thisAverageHourlyRateValue;
				data["id"] = thisId;
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisAlert).show().html("Success").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>