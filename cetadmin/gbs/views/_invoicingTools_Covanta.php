<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Covanta Invoice files</h3>
<?php
$_SESSION['SalesForceReport'] = "Covanta";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$billingRatesArray = array("CovantaHaverhill"=>array("519"),"CovantaSemass"=>array("516O","516R","516"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}
//print_pre($billingRatesArray);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Covanta";
			$SelectedGroupsID = "Covanta"; //GreenTeam
			$InvoiceData = array();
			$InvoiceHourData = array();
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					$code = (string)$code;
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			//print_pre($InvoiceHourData);
			//print_pre($BillingRateByCategory);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						$InvoiceCode = (string)$InvoiceCode;
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								//echo $Employee_Name."=".$hours." ".$BillingRateType."<Br>";
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[$BillingRateType][] = $Employee_Name;
									}
								}
								if (($BillingCategory == "EcoFellows" || $BillingCategory == "Admin Staff") && !in_array($Employee_Name,$IncludedEmployees[$BillingRateType])){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate for ".$BillingRateType][]=str_replace("_",", ",$Employee_Name);
								}
							}
						}
					}
				}
			}
			//print_pre($IncludedEmployees);
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
			
			require_once 'salesforce/config.php';
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00O0P000003TRVN"; //Invoicing/551 Boston Zero Waste

			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
			
			$reportId = "00O0P000003Td2M"; //Invoicing/Covanta Haverhill GPs LastMonth
			$reportName = "Covanta Haverhill GPs LastMonth";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$hCurrentStatus = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$HaverhillTaskStatus = $rowDetail["Upgrade__c.Covanta_Task_Dropdown__c"];
				
				//print "task dropdown: ".($rowDetail["Upgrade__c.Covanta_Task_Dropdown__c"])."<br>";
				//print "last action date: ".($rowDetail["Upgrade__c.Last_Action_Date__c"])."<br>";
				//print_pre($rowDetail);

				if ($HaverhillTaskStatus == "General Media Creation"){
				$HaverhillTaskStatus = "Task A.4";
				}
				
				if ($HaverhillTaskStatus == "Boston Outreach"){
				$HaverhillTaskStatus = "Task A.2";
				}
				
				if ($HaverhillTaskStatus == "Community Resources & Updates"){
				$HaverhillTaskStatus = "Task A.5";
				}
				
				if ($HaverhillTaskStatus == "Community Group Outreach (COA, VFW)"){
				$HaverhillTaskStatus = "Task B.1";
				}
				
				if ($HaverhillTaskStatus == "Health Care"){
				$HaverhillTaskStatus = "Task G.1";
				}
				
				if ($HaverhillTaskStatus == "Health Departments"){
				$HaverhillTaskStatus = "Task B.2";
				}
				
				if ($HaverhillTaskStatus == "HVAC Contractors"){
				$HaverhillTaskStatus = "Task E.1";
				}
				
				if ($HaverhillTaskStatus == "Newsletter"){
				$HaverhillTaskStatus = "Task A.3";
				}
				
				if ($HaverhillTaskStatus == "Miscellaneous Hazardous Items"){
				$HaverhillTaskStatus = "Task G.1";
				}
				
				if ($HaverhillTaskStatus == "Partners & Reporting"){
				$HaverhillTaskStatus = "Task A.1";
				}
				
				if ($HaverhillTaskStatus == "Schools"){
				$HaverhillTaskStatus = "Task F.1";
				}
				
				if ($HaverhillTaskStatus == "Thermometer Exchange Program"){
				$HaverhillTaskStatus = "Task D.1";
				}
				
				if ($HaverhillTaskStatus == "Conferences and Cross Promotion"){
				$HaverhillTaskStatus = "Task C.1";
				}
				
				if ($HaverhillTaskStatus == "Universal Waste Sheds"){
				$HaverhillTaskStatus = "Task C.2";
				}
				
				if ($HaverhillTaskStatus == "-"){
				$HaverhillTaskStatus = " ";
				}

				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$reportBullets[] = array("HaverhillTaskStatus"=>$HaverhillTaskStatus,"hCurrentStatus"=>$hCurrentStatus);
				}
			}
			
			//Include Events too!
			$reportId = "00O0P000003Td1n"; //Invoicing/Covanta Haverhill Events
			$reportName = "Covanta Haverhill Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$eventCounter = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$eventDate = $rowDetail["START_DATE_TIME"];
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				if (strtotime($eventDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($eventDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$Events[strtotime($eventDate)]["completed"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
				if (strtotime($eventDate) > strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$Events[strtotime($eventDate)]["upcoming"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounter++;
				}
			}
			
			$reportId = "00O0P000003TlXz"; //Invoicing/Covanta Semass GPs LastMonth
			$reportName = "Covanta Semass GPs LastMonth";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print ($rowDetail)."<br>";
				
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$status = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$taskStatus = $rowDetail["Upgrade__c.Covanta_Task_Dropdown__c"]; //Lauren added this, set variable for task dropdown
				//print "task dropdown: ".($rowDetail["Upgrade__c.Covanta_Task_Dropdown__c"])."<br>";
				//print_pre($rowDetail);
				//print_pre($taskStatus);
				if ($taskStatus == "General Media Creation"){
				$taskStatus = "Task 1.0";
				}
				
				if ($taskStatus == "Boston Outreach"){
				$taskStatus = "Task 3.0-4";
				}
				
				if ($taskStatus == "Community Resources & Updates"){
				$taskStatus = "Task 3.0-1";
				}
				
				if ($taskStatus == "Community Group Outreach (COA, VFW)"){
				$taskStatus = "Task 4.0-1a";
				}
				
				if ($taskStatus == "Health Care"){
				$taskStatus = "Task 4.0-1a";
				}
				
				if ($taskStatus == "Health Departments"){
				$taskStatus = "Task 4.0-2";
				}
				
				if ($taskStatus == "HVAC Contractors"){
				$taskStatus = "Task 4.0-2";
				}
				
				if ($taskStatus == "Newsletter"){
				$taskStatus = "Task 3.0-2";
				}
				
				if ($taskStatus == "Miscellaneous Hazardous Items"){
				$taskStatus = "Task 4.0-3";
				}
				
				if ($taskStatus == "Partners & Reporting"){
				$taskStatus = "Task 3.0-3";
				}
				
				if ($taskStatus == "Schools"){
				$taskStatus = "Task 4.0-2";
				}
								
				if ($taskStatus == "Conferences and Cross Promotion"){
				$taskStatus = "Task 2.0";
				}
				
				if ($taskStatus == "Universal Waste Sheds"){
				$taskStatus = "Task 4.0-1b";
				}
				
				if ($taskStatus == "Program Administration"){
				$taskStatus = "Task 5.0";
				}
				
				if ($taskStatus == "-"){
				$taskStatus = " ";
				}
				
				if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$reportBulletsSemass[] = array("taskStatus"=>$taskStatus,"status"=>$status);
					//$taskDetails[] = $taskStatus;
				}
				
			}
			
			
			
			$reportId = "00O0P000003TlXp"; //Invoicing/Covanta Semass Events
			$reportName = "Covanta Semass Events";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$eventCounterSemass = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$eventDate = $rowDetail["START_DATE_TIME"];
				$gpName = $rowDetail["Activity.Primary_campaign__c"];
				$secondaryName = $rowDetail["Activity.Secondary_Campaign_Association__c"];
				if ($gpName == "-"){$gpName = $secondaryName;}
				$subject = $rowDetail["SUBJECT"];
				$currentStatus = $rowDetail["Activity.Activity_Current_Status_Detail__c"];
				$attendanceMetric = round($rowDetail["Activity.Event_Total_in_Attendance__c"],0);
				if (strtotime($eventDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($eventDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$EventsSemass[strtotime($eventDate)]["completed"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounterSemass++;
				}
				if (strtotime($eventDate) > strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$EventsSemass[strtotime($eventDate)]["upcoming"][] = array("eventDate"=>$eventDate,"event"=>$subject,"description"=>$currentStatus,"attendance"=>$attendanceMetric);
					$eventCounterSemass++;
				}
			}
			
			ksort($Events);
			ksort($EventsSemass);
			
			
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
				include_once('salesforce/reports/reportCovanta.php');
				echo $ReportsFileLink;
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "Covanta");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the File: <?php echo $ReportsFileLink;?>.
</ul>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>
