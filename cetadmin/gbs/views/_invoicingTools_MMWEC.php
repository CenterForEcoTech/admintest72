<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<?php
$_SESSION['SalesForceReport'] = "MMWEC";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}
//print_pre($EmployeesRevenueCodeByName);

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

$billingRatesArray = array("MMWEC"=>array("MMWEC"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
			$BillingCategoryByEmployee[$Employee] = $record->GBSBillingRate_Name;
		}
	}
}
//print_pre($BillingRateByCategory["NGRID_Inspection"]);

$setTab = "Invoice";
	$TabCodes = array("Hours"=>"Hours","MMWEC Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	echo "<h3>This tool will create the MMWEC Invoice and Report files</h3>";
	
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "MMWEC";
			//$SelectedGroupsID = 6; //GreenTeam
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType],true)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if ($BillingCategory == "Specialist" && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate".$InvoiceCode][]=str_replace("_",", ",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once 'salesforce/config.php';
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00O0P000003yXc7UAE"; //Invoicing/524 NGRID Inspections
			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
			$reportId = "00O0P000003yXc7"; //MMWEC Audits for the month Invoicing
			$reportName = "MMWEC Audit for the month Invoicing";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Opportunity.Audit_Date_Start__c",SORT_ASC);
			//echo "There are ".count($reportRowsSorted)." rows<Br>";
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				//print_pre($rowDetail);
				$auditStartDate = $rowDetail["Opportunity.Audit_Date_Start__c"];
				$email = ($rowDetail["Account.Account_E_mail__c"] == "-" ? "" : $rowDetail["Account.Account_E_mail__c"]);
				$lastName = $rowDetail["Account.AVSFQB__Last_Name__c"];
				$firstName = $rowDetail["Account.AVSFQB__First_Name__c"];
				$bulbsInstalled = ($rowDetail["Opportunity.X9W_bulbs_installed__c"]=="-" ? 0 : $rowDetail["Opportunity.X9W_bulbs_installed__c"]);
				$unit = ($rowDetail["Opportunity.MMWEC_Unit__c"] == "-" ? "" : " ".$rowDetail["Opportunity.MMWEC_Unit__c"]);
				$street = $rowDetail["ADDRESS1_STREET"].$unit; 
				$city = $rowDetail["ADDRESS1_CITY"];
				$stage = $rowDetail["STAGE_NAME"];
				$auditType = $rowDetail["Opportunity.Audit_Type__c"];
				$town2 = $rowDetail["Account.MMWEC_Energy_Company__c"];
				if (strtotime($auditStartDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($auditStartDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$mmwecRecord[strtotime($auditStartDate).$rowId] = array(
						"auditDate"=>$auditStartDate,
						"lastName"=>$lastName,
						"firstName"=>$firstName,
						"bulbsInstalled"=>$bulbsInstalled,
						"city"=>$city,
						"stage"=>$stage,
						"auditType"=>$auditType,
						"email"=>$email,
						"street"=>$street,
						"town2"=>$town2
						);
				}
			}
			ksort($mmwecRecord);
			echo "Invoice Data";
			echo "<table class='simpleTable'>";
			echo "<thead><tr><th>Audit Date</th><th>Last Name</th><th>First Name</th><th>Bulb Installed</th><th>City</th><th>Stage</th><th>Town2</th><th>Audit Type</th><th>Email</th></thead>";
			echo "<tbody>";
			foreach ($mmwecRecord as $record){
					$auditType = ($record["auditType"] == "MMWEC" ? "Audit" : $record["auditType"]);
					$MMWECCount[$auditType]++;
					echo "<tr>";
						echo "<td>".$record["auditDate"]."</td>
						<td>".$record["lastName"]."</td>
						<td>".$record["firstName"]."</td>
						<td>".$record["bulbsInstalled"]."</td>
						<td>".$record["city"]."</td>
						<td>".$record["stage"]."</td>
						<td>".$record["town2"]."</td> 
						<td>".$record["auditType"]."</td>
						<td>".$record["email"]."</td>";
					echo "</tr>";
			}
			//added the town2 and email line above
			echo "</tbody>";
			echo "</table>";
			
			$reportId = "00O0P000003yXiP"; //MMWEC/Call Log Monthly Data Invoicing
			$reportName = "Call Log Monthly Data Invoicing";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$reportRowsSorted = array_orderby($reportRowsUnSorted,"Account.Task_Due_Date__c",SORT_ASC);
			//print_pre($reportResults);
			//print_pre(retrieve_report_asCSV($instance_url, $reportId, $access_token));
			//echo "There are ".count($reportRowsSorted)." rows<Br>";
			//print_pre($reportRowsSorted);
			//echo "There are ".count($reportResults)." rows<Br>";
			foreach ($reportRowsSorted as $rowId=>$rowDetail){
				$recordType = $rowDetail["Activity.RecordType"];
				//$createdDate = $rowDetail["Activity.CreatedDate"];
				$taskDueDate = $rowDetail["Account.Task_Due_Date__c"];
				$firstName = $rowDetail["Account.AVSFQB__First_Name__c"];
				$lastName = $rowDetail["Account.AVSFQB__Last_Name__c"];
				$street = $rowDetail["Account.BillingStreet"]; 
				$email = ($rowDetail["Account.Account_E_mail__c"] == "-" ? "" : $rowDetail["Account.Account_E_mail__c"]);
				$city = $rowDetail["Account.BillingCity"]; 
				$zip = $rowDetail["Account.BillingPostalCode"]; 
				$phone = $rowDetail["Account.Phone"];
				$mobile = ($rowDetail["Account.Mobile__c"] == "-" ? "" : $rowDetail["Account.Mobile__c"]);
				$yearBuilt = $rowDetail["Account.Year_Built__c"];
				$heatingFuel = $rowDetail["Account.Heating_Fuel_Type__c"];
				$description = ($rowDetail["Activity.Description"] == "-" ? "" : $rowDetail["Activity.Description"]);
				$status = $rowDetail["Activity.Status"];
				$auditStartDate = ($rowDetail["Opportunity.Audit_Date_Start__c"] == "-" ? "" : $rowDetail["Opportunity.Audit_Date_Start__c"]);
				$energyCompany = ($rowDetail["Account.MMWEC_Energy_Company__c.Name"] == "-" ? "" : $rowDetail["Account.MMWEC_Energy_Company__c.Name"]);
				if (strtotime($taskDueDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($taskDueDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$callRecord[strtotime($taskDueDate).$rowId] = array(
						"recordType"=>$recordType,
						"taskDueDate"=>$taskDueDate,
						"firstName"=>$firstName,
						"lastName"=>$lastName,
						"street"=>$street,
						"city"=>$city,
						"zip"=>$zip,
						"phone"=>$phone,
						"mobile"=>$mobile,
						"yearBuilt"=>$yearBuilt,
						"heatingFuel"=>$heatingFuel,
						"description"=>$description,
						"status"=>$status,
						"auditStartDate"=>$auditStartDate,
						"energyCompany"=>$energyCompany, //added this line
						"email"=>$email
						);
				}

			}
			echo "Call Log Data";
			echo "<table class='simpleTable'>";
			echo "<thead><tr><th>Task Due Date</th><th>Name</th><th>Address</th><th>Phone</th><th>Year Built</th><th>Fuel</th><th>Comments</th><th>Status</th><th>Audit Start Date</th><th>Energy Company</th><th>Email</th></thead>";
			echo "<tbody>";
			foreach ($callRecord as $record){
					echo "<tr>";
						echo "<td>".$record["taskDueDate"]."</td>
						<td>".$record["firstName"]." ".$record["lastName"]."</td>
						<td>".$record["street"]." ".$record["city"]."</td>
						<td>".$record["phone"]." ".$record["mobile"]."</td>
						<td>".$record["yearBuilt"]."</td>
						<td>".$record["heatingFuel"]."</td>
						<td>".$record["description"]."</td>
						<td>".$record["status"]."</td>
						<td>".$record["auditStartDate"]."</td>
						<td>".$record["energyCompany"]."</td>
						<td>".$record["email"]."</td>";
					echo "</tr>";
			}
			echo "</tbody>";
			echo "</table>";
			
			
			
			
			include_once('salesforce/reports/reportMMWEC.php');
			echo $ReportsFileLink;
			
			//print_pre($pipelineItems);
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "MMWEC");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the <?php echo str_replace("<br>","",$ReportsFileLink);?> and review MMWEC Invoice and Report.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>