<?php
$_SESSION['SalesForceReport'] = "Eversource";
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$setTab = "EversourceAccountImport";
//get MasterPriceList
$masterpricelistResults = $GBSProvider->invoiceTool_getEversourceMasterPricelist();
$masterpricelistCollection = $masterpricelistResults->collection;
foreach ($masterpricelistCollection as $result){
//	$EversourceMasterPriceByEFI[$result->EFI_Item_Number] = $result;
//	$EversourceMasterPriceByDescription[strtolower(trim($result->Measure_Description))] = $result;
	$EversourceMasterPriceByCategory[strtolower(trim($result->Measure_Category))] = $result;
	$EversourceMasterPriceByMEAID[$result->MEA_ID] = $result;
}
//print_pre($EversourceMasterPriceByCategory);

$fileLoadSuccessfull = ($_GET['fileLoadSuccessfull'] ? true : false);

if(count($_FILES['filesToUpload'])) {
	$tmpFileName = $_FILES["filesToUpload"]["tmp_name"][0];
	if ($_GET['type'] == "trackingFile"){
		$trackingFileReceived = true;
		$trackingFileName = "salesforce/exports/EversourceTrackingFileToUse".date("Y_m_d").".xlsx";
		$target_file = $siteRoot.$adminFolder."gbs/".$trackingFileName;
		move_uploaded_file($tmpFileName, $target_file);
		
		$trackingFileName = "importfiles/EversourceTrackingFile.xlsx";
		$target_file_copy = $siteRoot.$adminFolder."gbs/".$trackingFileName;
		copy($target_file, $target_file_copy);
		

	}else{
		$target_file = $siteRoot.$adminFolder."gbs/importfiles/EversourceCustomerAccount.csv";
		move_uploaded_file($tmpFileName, $target_file);
		$results = $GBSProvider->invoiceTool_loadExternalData($target_file);
		if ($results["success"]){
			$fileReceived = true;
			$fileLoadSuccessfull = true;
		}else{
			$fileFailed = true;
		}
	}
}

if ($trackingFileReceived){
	//new import that data and parse it out 
	$trackingFile = $trackingFileName;
	include_once('salesforce/read_883TrackingFile.php');
	$projectAddress = array();
	foreach ($rowsAllJobs as $rowId=>$rowData){
		$projectID = $rowData["Project ID"];
		$projectAddress[$projectID]["projectName"] = $rowData["Project Name"];
		$projectAddress[$projectID]["address"] = $rowData["Address"];
		$projectAddress[$projectID]["town"] = $rowData["Town"];
		$projectAddress[$projectID]["heatType"] = $rowData["Primary Heating Fuel"];
		$projectAddress[$projectID]["installDate"] = date("n/d/Y",strtotime($rowData["Date of Install"]));
		$projectAddress[$projectID]["masterMeter"] = strtolower($rowData["Master Meters?"]);
		if (date("Y",strtotime($rowData["Date of Install"])) == "1969"){$projectAddress[$projectID]["installDate"] = date("n/d/Y",(($rowData["Date of Install"] - 25568) * 86400));} //correction for copy paste of dates in Excel
		$etrackJobCount[$projectID] = ((int)$rowData["# Units in Contract"] > $projectAddress[$projectID]["units"] ? (int)$rowData["# Units in Contract"]: $projectAddress[$projectID]["units"]);
		$etrackGreenProspect[$projectID][$rowData["Green Prospect Type"]] = (int)$rowData["# Units in Contract"];
		$TotalUnits = (int)$rowData["# Units in Contract"];
		$projectAddress[$projectID]["units"] = $TotalUnits;
//		if (trim($rowData["Green Prospect Type"]) == "883 Audit" && $rowData["Type of Audit"] == "piggyback"){
//			$projectAddress[$projectID]["affectedUnits"] = $projectAddress[$projectID]["affectedUnits"];
//		}else{
			$projectAddress[$projectID]["affectedUnits"] = ((int)$rowData["# Units in Contract"] > $projectAddress[$projectID]["affectedUnits"] ? (int)$rowData["# Units in Contract"]: $projectAddress[$projectID]["affectedUnits"]);
//		}
		$projectAddress[$projectID]["invoiceDate"] = date("n/d/Y", strtotime($rowData["Included in Invoice (Month)"]." 21"));
		$projectAddress[$projectID]["totalDwellingUnits"] = (int)$rowData["Total Dwelling Units"];
		$invoiceDate = date("n/d/Y", strtotime($rowData["Included in Invoice (Month)"]." 22"));
		
	}
		
	//calculate Invoice Number
	$InvoiceStartYear = 2016;
	$InvoiceCurrentYear = date("Y");
	$InvoiceYearDifference = ($InvoiceStartYear-$InvoiceCurrentYear);
	$InvoiceYearMultiplier = ($InvoiceYearDifference*12);
	$InvoiceId = 60+$InvoiceYearMultiplier+date("n",strtotime($invoiceDate));
	
	//create the orders information
	foreach ($rowsAllMeasuresByProjectID as $projectID=>$measures){
		foreach ($measures as $id=>$measureData){
			$Description = trim(strtolower($measureData["Measure Description"]));
			$EtracDescription = ($EversourceMasterPriceByCategory[$Description]->ETRAC_DESCRIPTION ? $EversourceMasterPriceByCategory[$Description]->ETRAC_DESCRIPTION : $measureData["Measure Description"]);
			$AnnualkWhSavings = (float)$measureData["Annual kWh Savings/Unit"];
			$AnnualkWhSavingsTotal = (float)$measureData["Annual kWh Savings"];
			$measureQty = (int)$measureData["Unit Quantity"];
			$UnitCost = $measureData["Unit Incentive"];
			$UnitCostTotal = $measureData["Total Incentive"];
			$Contractor = $measureData["Contractor"];
			$DmdkWh = ($EversourceMasterPriceByCategory[$Description]->Dmd_kWh_Unit ? $EversourceMasterPriceByCategory[$Description]->Dmd_kWh_Unit : $measureData["Dmd kWh /Unit"]);
			$MEA_ID = $EversourceMasterPriceByCategory[$Description]->MEA_ID;
			$Template = $EversourceMasterPriceByCategory[$Description]->Template;
			$TotalUnits = ($etrackGreenProspect[$projectID][$Template] ? $etrackGreenProspect[$projectID][$Template] : 1);
			$INorIM = $EversourceMasterPriceByCategory[$Description]->INorIM;
			$MFInvoiceSummary[$projectID][$INorIM]["Qty"] = $MFInvoiceSummary[$projectID][$INorIM]["Qty"]+$measureQty;
			$MFInvoiceSummary[$projectID][$INorIM]["AnnualkWhSavings"] = bcadd($MFInvoiceSummary[$projectID][$INorIM]["AnnualkWhSavings"],$AnnualkWhSavingsTotal,1);
			$MFInvoiceSummary[$projectID][$INorIM]["Unit Cost"] = bcadd($MFInvoiceSummary[$projectID][$INorIM]["Unit Cost"],$UnitCostTotal,2);
			$MFInvoiceDetails[$projectID][$Contractor][$INorIM][] = $measureData;

			
			//echo $projectID."=".$Description." ".$Template."=".$TotalUnits."<Br>";
			$jobCount = 1;
			$AppliedUnits = $TotalUnits;
			if ($measureQty <= $TotalUnits){ //if there are fewer qty than units then put one qty in each unit until qty reached.  This will leave out some of the units
				//echo $projectID." has qty of ".$measureQty." which is less than or equal to".$TotalUnits."<br>";
				$AppliedUnits = $measureQty;
				$QtyPerUnit = 1;
				
				//unless it is a piggyback
				if ($MEA_ID == "MF_AUDIT_FEE" || $MEA_ID=="PIGGYBACK_FEE"){
					$AppliedUnits = $TotalUnits;
					$QtyPerUnit = 1;
					$UnitCostOrg = $UnitCost;
					$UnitCost = array();
					$UnitCost[1] = $UnitCostOrg;
					$unitCostLoop = 2;
					while ($unitCostLoop <= $TotalUnits){
						$UnitCost[$unitCostLoop] = 0;
						$unitCostLoop++;
					}
				}
				
			}else{
				//echo $projectID." has ".$measureQty." to be divided over ".$TotalUnits."<br>";
				if ($measureQty % $AppliedUnits == 0){ //if Qty is evenly dividable amongst all the units do so
					$QtyPerUnit = $measureQty/$AppliedUnits;
					//echo $projectID." has evenly divided ".$measureQty." over ".$AppliedUnits." = ".$QtyPerUnit."<br>";
				}else{ //give the first half more than the second half
					$FirstHalfUnits = ceil($AppliedUnits/2);
					$SecondHalfUnits = floor($AppliedUnits/2);
					//echo $projectID." divided ".$AppliedUnits."units into ".$FirstHalfUnits." and ".$SecondHalfUnits."<Br>";
					$firstLoop =1;
					$totalQty = 0;
					$firstLoopComplete = false;
					$loopQty = array();
					while($firstLoop <= $AppliedUnits){
						$loopQty[$firstLoop] = 1;
						$firstLoop++;
						$totalQty++;
						if ($totalQty == $measureQty){
							$firstLoopComplete = true;
							break;
						}
					}
					//now that each record has 1 keep adding one to each unit until totalQty is equal to measureQty
					if (!$firstLoopComplete){
						$secondLoop =1;
							while($secondLoop <= ($AppliedUnits+1)){
								$totalQty++;
								if ($totalQty == ($measureQty+1)){
									//echo "broke";
									break;
								}
								$loopQty[$secondLoop]++;
								if ($secondLoop == ($AppliedUnits)){
									$secondLoop = 1;
								}else{
									$secondLoop++;
								}
							}
					}
					$QtyPerUnit = $loopQty;
				}
			}
			while ($jobCount <= $AppliedUnits){
				$ordersData = array();
				$thisQty = (is_array($QtyPerUnit) ? $loopQty[$jobCount]: $QtyPerUnit);
				$thisUnitCost = (is_array($UnitCost) ? $UnitCost[$jobCount] : $UnitCost);
				$thisAmt = bcmul($thisUnitCost,$thisQty,2);
				$ordersData["PRC_ALIAS"] = "MF";
				$ordersData["EXT_JOB_ID"] = $measureData["Project ID"]."-".($jobCount < 10 ? "0".$jobCount : $jobCount);
				$ordersData["MEA_ID"] = $MEA_ID;
				$ordersData["PART_ID"] = $EversourceMasterPriceByCategory[$Description]->PART_ID;
				$ordersData["DESCRIPTION"] = $EtracDescription;
				$ordersData["INSTALL_LOCATION"] = "";
				$ordersData["QTY"] = $thisQty;
				$ordersData["ENR_SAVINGS"] = bcmul($AnnualkWhSavings,$thisQty,3);
				$ordersData["DMD_KW_RED"] = (bcmul($DmdkWh,$thisQty,3) > 0 ? bcmul($DmdkWh,$thisQty,3) : "");
				$ordersData["AUTH_INCENTIVE_AMT"] = money($thisAmt);
				$ordersData["TOTAL_COST"] = money($thisAmt);
				$orders[] = $ordersData;
				
				$MFInvoice[$MEA_ID]["Qty"] = $MFInvoice[$MEA_ID]["Qty"]+($thisAmt > 0 ? $thisQty : 0);
				$MFInvoice[$MEA_ID]["Amt"] = bcadd($MFInvoice[$MEA_ID]["Amt"],$thisAmt,2);
				
				$jobCount++;
			}
		}//foreach measure
	}//foreach MeasureRow
	
	
	//print_pre($orders);
	
	
}
$totalRowCount_Jobs = 0;
foreach ($etrackJobCount as $projectId=>$rows){
		$totalRowCount_Jobs = $totalRowCount_Jobs+$rows;
}

if (count($projectAddress)){
	$setTab = "AllFiles";

	//print_pre($extrackJobs);
	foreach ($projectAddress as $projectId=>$addressInfo){
		$addressParts = explode(",",strtoupper($addressInfo["address"]));
		$streetStuff = explode(" (",$addressParts[0]);
		$town = strtoupper($addressInfo["town"]);
		$streetParts = explode(" ",$streetStuff[0]);
		if (is_numeric($streetParts[0])){
			$Street_NumberStart = $streetParts[0];
			$Street_NumberEnd = $streetParts[0];
			$Street_Name = trim(str_replace($streetParts[0],"",$streetStuff[0]));
		}else{
			if (strpos($streetParts[0],"-")){
				$addressNumbers = explode("-",$streetParts[0]);
				$Street_NumberStart = (int)$addressNumbers[0];
				$Street_NumberEnd = (int)$addressNumbers[1];
				$Street_Name = trim(str_replace($streetParts[0],"",$streetStuff[0]));
			}else{
				$Street_NumberStart = 0;
				$Street_NumberEnd = 0;
				$Street_Name =$streetStuff[0];
			}
		}
		$accountSearch[] = array("STREET_NUMBER_start"=>$Street_NumberStart,"STREET_NUMBER_end"=>$Street_NumberEnd,"STREET_NAME"=>$Street_Name,"SERVICE_TOWN"=>$town,"addressInfo"=>$addressInfo,"projectId"=>$projectId);
	}

	$eTrackJobsHeader = 
		array(
			"A"=>array("title"=>"EXT_INVOICE_ID","object"=>"calculate_invoiceID","width"=>9),
			"B"=>array("title"=>"EXT_INVOICE_DT","object"=>"calculate_invoiceDate","width"=>16),
			"C"=>array("title"=>"VENDOR_CODE","object"=>"value_14327","width"=>9),
			"D"=>array("title"=>"EXT_INVOICE_NAME","object"=>"calculate_invoiceName","width"=>21),
			"E"=>array("title"=>"MDW_ACT_ID","object"=>"BILLING_ACCOUNT_NUMBER","width"=>18),
			"F"=>array("title"=>"EXT_JOB_ID","object"=>"calculate_jobId","width"=>20),
			"G"=>array("title"=>"EXT_JOB_FIRST_NAME","object"=>"CUSTOMER_FIRST_NAME","width"=>28),
			"H"=>array("title"=>"EXT_JOB_LAST_NAME","object"=>"CUSTOMER_LAST_NAME","width"=>50),
			
			"I"=>array("title"=>"EXT_ADDRESS","object"=>"calculate_address","width"=>18),
			"J"=>array("title"=>"EXT_APT","object"=>"STREET_MISC","width"=>9),
			"K"=>array("title"=>"EXT_CITY","object"=>"SERVICE_TOWN","width"=>17),
			"L"=>array("title"=>"NUM_UNITS","object"=>"value_1","width"=>13),
			"M"=>array("title"=>"OWN_RENT","object"=>"value_O","width"=>8),
			"N"=>array("title"=>"PRIM_HEAT_TYPE","object"=>"calculate_heatType","width"=>9),
			"O"=>array("title"=>"REQUESTED_DT","object"=>"calculate_requestedDate","width"=>15),
			"P"=>array("title"=>"ASSESSMENT_DT","object"=>"calculate_assessmentDate","width"=>15)
		);
			
	$eTrackOrdersHeader = 
		array(
			"A"=>array("title"=>"PRC_ALIAS","object"=>"PRC_ALIAS","width"=>10),
			"B"=>array("title"=>"EXT_JOB_ID","object"=>"EXT_JOB_ID","width"=>19),
			"C"=>array("title"=>"MEA_ID","object"=>"MEA_ID","width"=>42),
			"D"=>array("title"=>"PART_ID","object"=>"PART_ID","width"=>9),
			"E"=>array("title"=>"DESCRIPTION","object"=>"DESCRIPTION","width"=>40),
			"F"=>array("title"=>"INSTALL_LOCATION","object"=>"INSTALL_LOCATION","width"=>18),
			"G"=>array("title"=>"QTY","object"=>"QTY","width"=>10),
			"H"=>array("title"=>"ENR_SAVINGS","object"=>"ENR_SAVINGS","width"=>13),
			
			"I"=>array("title"=>"DMD_KW_RED","object"=>"DMD_KW_RED","width"=>13),
			"J"=>array("title"=>"AUTH_INCENTIVE_AMT","object"=>"AUTH_INCENTIVE_AMT","width"=>21),
			"K"=>array("title"=>"TOTAL_COST","object"=>"TOTAL_COST","width"=>12)
		);
	?>
	<style>
		table {font-family: "Calibri";font-size:11pt;}
	</style>
	<h3>This tool will create the Eversource eTrack Support file</h3>
	<?php
	
	echo "<br><h3>Eversource eTrack Report</h3>";
	$SaleforceTabCodes = array("Eversource Account Import"=>"EversourceAccountImport","Matched Accounts"=>"MatchedAccounts","eTrack-Jobs"=>"eTrackJobs","eTrack-Orders"=>"eTrackOrders","All Files"=>"AllFiles");
	foreach ($SaleforceTabCodes as $TabName=>$TabCode){
		$SalesForceHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='MatchedAccounts' class='tab-content'>";
			echo "<table class='simpleTable'><thead><tr><th>ProjectID</th><th>Project Name</th><th>Units with Jobs</th><th>Total Accounts at Address</th></tr></thead><tbody>";
			foreach ($accountSearch as $accountInfo){
				$eversourceAccountsResults = $GBSProvider->invoiceTool_getEversourceAccounts($accountInfo,$criteria);
				$eversourceCollection = $eversourceAccountsResults->collection;
				$record = 0;
				while ($record < $etrackJobCount[$accountInfo["projectId"]]){
					if (count($eversourceCollection[$record])){
						$accountRecords[$accountInfo["projectId"]][] = $eversourceCollection[$record];
						$lastAvailableRecord = $record+1;
					}else{
						$MasterMeters[$accountInfo["projectId"]][$lastAvailableRecord] = ($MasterMeters[$accountInfo["projectId"]][$lastAvailableRecord]+1);
					}
					$record++;
				}
				echo "<tr>
					<td>".$accountInfo["projectId"]."</td>
					<td>".$accountInfo["addressInfo"]["projectName"]."</td>
					<td>".$etrackJobCount[$accountInfo["projectId"]]." unit".($etrackJobCount[$accountInfo["projectId"]] > 1 ? "s" : "")."</td>
					<td>".count($eversourceAccountsResults->collection)." records matched on address. ".($etrackJobCount[$accountInfo["projectId"]] > count($eversourceAccountsResults->collection) ? "(possible Master Meter Situation?)" : "")."</td></tr>";
			}
			echo "</tbody><tfoot><tr><td></td><td align='right'>Total Units</td><td>".$totalRowCount_Jobs."</td><td></td></tr></tfoot></table><Br><Br>";
		echo "</div>";
	
		echo "<div id='eTrackJobs' class='tab-content'>";
			echo "<table class='simpleTable'><thead><tr>";
			foreach ($eTrackJobsHeader as $column=>$headerData){
				echo "<td nowrap='nowrap'>".$headerData["title"]."</td>";
			}
			echo "</tr></thead><tbody>";
	
			$rowCounter = 2; //skip first row for header
			$rowId = 0;
			foreach ($accountRecords as $JobId=>$accountRecordList){
				$jobCount = 1;
				foreach ($accountRecordList as $accountInfo){
					echo "<tr>";
					foreach ($eTrackJobsHeader as $column=>$headerData){
						if (strpos(" ".$headerData["object"],"calculate")){
							$calculateType = str_replace("calculate_","",$headerData["object"]);
							switch ($calculateType){
								case "invoiceID":
									$dataDisplay = $InvoiceId;
									break;
								case "invoiceDate":
									$dataDisplay = $projectAddress[$JobId]["invoiceDate"];
									break;
								case "invoiceName":
									$dataDisplay = $projectAddress[$JobId]["projectName"];
									break;
								case "jobId":
									$dataDisplay = $JobId."-".($jobCount < 10 ? "0".$jobCount : $jobCount);
									break;
								case "address":
									$dataDisplay =  $accountInfo->STREET_NUMBER." ".$accountInfo->FULL_STREETNAME;
									break;
								case "heatType":
									$dataDisplay = $projectAddress[$JobId]["heatType"];
									break;
								case "requestedDate":
									$dataDisplay = $projectAddress[$JobId]["installDate"];
									break;
								case "assessmentDate":
									$dataDisplay = $projectAddress[$JobId]["installDate"];
									break;
								default:
									$dataDisplay = $calculateType;
								break;
							}
						}elseif(strpos(" ".$headerData["object"],"value_")){
							if ($headerData["title"] == "NUM_UNITS"){
								if ($MasterMeters[$JobId][$jobCount]){
									$value = ($MasterMeters[$JobId][$jobCount]+1);
								}else{
									$value = str_replace("value_","",$headerData["object"]);							
								}
							}else{
								$value = str_replace("value_","",$headerData["object"]);
							}
							$dataDisplay = $value;
						}else{
							$dataDisplay = $accountInfo->{$headerData["object"]};
						}
						
						echo "<td nowrap='nowrap'>".$dataDisplay."</td>";
						$eTrackReportJobs[$rowId][$column.$rowCounter]=$dataDisplay;
					}
					echo "</tr>";
					$jobCount++;
					$rowCounter++;
					$rowId++;
				}
			}
			echo "</tbody></table>";
			//Add standard CET row
			$eTrackReportJobs[$rowId]["A".$rowCounter]=$InvoiceId;
			$eTrackReportJobs[$rowId]["B".$rowCounter]=$invoiceDate;
			$eTrackReportJobs[$rowId]["C".$rowCounter]="14327";
			$eTrackReportJobs[$rowId]["D".$rowCounter]="CET";
			$eTrackReportJobs[$rowId]["E".$rowCounter]="UNKNOWN";
			$eTrackReportJobs[$rowId]["F".$rowCounter]="CET_FEE_".date("Y_m", strtotime($invoiceDate));
			$eTrackReportJobs[$rowId]["G".$rowCounter]="";
			$eTrackReportJobs[$rowId]["H".$rowCounter]="";
			$eTrackReportJobs[$rowId]["I".$rowCounter]="";
			$eTrackReportJobs[$rowId]["J".$rowCounter]="";
			$eTrackReportJobs[$rowId]["K".$rowCounter]="NORTHAMPTON";					
			$eTrackReportJobs[$rowId]["L".$rowCounter]="";
			$eTrackReportJobs[$rowId]["M".$rowCounter]="";
			$eTrackReportJobs[$rowId]["N".$rowCounter]="";
			$eTrackReportJobs[$rowId]["O".$rowCounter]="";
			$eTrackReportJobs[$rowId]["P".$rowCounter]="";
			
			include_once('salesforce/reports/eTrackJobs.php');
			
		echo "</div>";

		echo "<div id='eTrackOrders' class='tab-content'>";
			echo "<table class='simpleTable'><thead><tr>";
			foreach ($eTrackOrdersHeader as $column=>$headerData){
				echo "<td nowrap='nowrap'>".$headerData["title"]."</td>";
			}
			echo "</tr></thead><tbody>";
			$rowCounter = 2; //skip first row for header
			$rowId = 0;
			foreach ($orders as $orderId=>$order){
				echo "<tr>";
					foreach ($eTrackOrdersHeader as $column=>$headerData){
						$dataDisplay = $order[$headerData["object"]];
						echo "<td nowrap='nowrap'>".$dataDisplay."</td>";
						$eTrackReportOrders[$rowId][$column.$rowCounter]=$dataDisplay;
					}
				echo "</tr>";
				$rowCounter++;
				$rowId++;
			}
			echo "</tbody></table>";
			//Add standard CET row
			$eTrackReportOrders[$rowId]["A".$rowCounter]="MF";
			$eTrackReportOrders[$rowId]["B".$rowCounter]="CET_FEE_".date("Y_m", strtotime($invoiceDate));
			$eTrackReportOrders[$rowId]["C".$rowCounter]="MF_MGMT_FEE";
			$eTrackReportOrders[$rowId]["D".$rowCounter]="";
			$eTrackReportOrders[$rowId]["E".$rowCounter]="";
			$eTrackReportOrders[$rowId]["F".$rowCounter]="";
			$eTrackReportOrders[$rowId]["G".$rowCounter]="1";
			$eTrackReportOrders[$rowId]["H".$rowCounter]="0";
			$eTrackReportOrders[$rowId]["I".$rowCounter]="0";
			$eTrackReportOrders[$rowId]["J".$rowCounter]="7000.00";
			$eTrackReportOrders[$rowId]["K".$rowCounter]="7000.00";
			
			include_once('salesforce/reports/eTrackOrders.php');
			
		echo "</div>";
		
//		echo "<div id='MFInvoice' class='tab-content'>";
			foreach ($MFInvoice as $MEA_ID=>$rowDetails){
				$MEARow = $EversourceMasterPriceByMEAID[$MEA_ID]->MFInvoiceRow;
				$MFInvoiceRowData["B".$MEARow] = $rowDetails["Qty"];
				$MFInvoiceRowData["G".$MEARow] = $rowDetails["Amt"];
			}
			//set nonMeasure data
			$InvoicePO = "02261404";
			$MFInvoiceRowData["C2"] = date("F, Y", strtotime($invoiceDate." -1 day"));
			$MFInvoiceRowData["F1"] = $InvoiceId;
			$MFInvoiceRowData["F2"] = $invoiceDate;
			$MFInvoiceRowData["F3"] = $InvoicePO;
			$MFInvoiceRowData["F4"] = "00001";
			include_once('salesforce/reports/MFInvoice01.php');
			//echo $InvoiceFile01Link;
//		echo "</div>";
		
//		echo "<div id='MFInvoiceSummary' class='tab-content'>";
			$projectRow = 4;
			$MFInvoiceSummaryRowData["A1"] = date("n/d/Y", strtotime($invoiceDate." -1 day"));
			foreach ($projectAddress as $projectId=>$projectInfo){
				$MFInvoiceSummaryRowData["A".$projectRow] = $projectInfo["projectName"];
				$MFInvoiceSummaryRowData["B".$projectRow] = $projectInfo["totalDwellingUnits"];
				$MFInvoiceSummaryRowData["C".$projectRow] = $projectInfo["affectedUnits"];
				$MFInvoiceSummaryRowData["D".$projectRow] = $MFInvoiceSummary[$projectId]["IN"]["Qty"];
				$MFInvoiceSummaryRowData["E".$projectRow] = $MFInvoiceSummary[$projectId]["IN"]["AnnualkWhSavings"];
				$MFInvoiceSummaryRowData["F".$projectRow] = $MFInvoiceSummary[$projectId]["IN"]["Unit Cost"];
				$MFInvoiceSummaryRowData["G".$projectRow] = $MFInvoiceSummary[$projectId]["IM"]["Qty"];
				$MFInvoiceSummaryRowData["H".$projectRow] = $MFInvoiceSummary[$projectId]["IM"]["Unit Cost"];
				$lastProjectRow = $projectRow;
				$projectRow++;
			}
			//add formula row
			$projectRow = $projectRow+2;
			$MFInvoiceSummaryRowData["B".$projectRow] = "=SUM(B4:B".$lastProjectRow.")";
			$MFInvoiceSummaryRowData["C".$projectRow] = "=SUM(C4:C".$lastProjectRow.")";
			$MFInvoiceSummaryRowData["D".$projectRow] = "=SUM(D4:D".$lastProjectRow.")";
			$MFInvoiceSummaryRowData["E".$projectRow] = "=SUM(E4:E".$lastProjectRow.")";
			$MFInvoiceSummaryRowData["F".$projectRow] = "=SUM(F4:F".$lastProjectRow.")";
			$MFInvoiceSummaryRowData["G".$projectRow] = "=SUM(G4:G".($lastProjectRow+2).")";
			$MFInvoiceSummaryRowData["H".$projectRow] = "=SUM(H4:H".($lastProjectRow+2).")";
			
			$MFInvoiceSummaryRowData["D".($projectRow+2)] = "=(D".$projectRow."+G".$projectRow.")";
			$MFInvoiceSummaryRowData["D".($projectRow+3)] = "=(F".$projectRow."+H".$projectRow.")";
			$MFInvoiceSummaryRowData["D".($projectRow+5)] = "=E".$projectRow;
			
			include_once('salesforce/reports/MFInvoiceSummary02.php');
			//echo $InvoiceFile02Link;
//		echo "</div>";
		
//		echo "<div id='InvoiceDetails' class='tab-content'>";
			$invoiceCountNumber = 2;
//			print_pre($MFInvoiceDetails["201603-5"]);
//			echo " count" .count($MFInvoiceDetails["201603-5"]["CET"]["IN"]);
			foreach ($MFInvoiceDetails as $projectId=>$ContractorInfo){
				//get the right order for contractors always putting CET first
				$ContractorOrder = array();
				$ContractorOrder[] = "CET";
				foreach ($ContractorInfo as $Contractor=>$INorIMData){
					if ($Contractor != "CET"){
						$ContractorOrder[] = trim($Contractor);
					}
				}
				foreach ($ContractorOrder as $ContractorName){
					$JobName = $projectAddress[$projectId]["projectName"];
					
					$MFInvoiceDetailData = array();
					$MFInvoiceDetailData["A3"] = $projectId;
					$MFInvoiceDetailData["B3"] = $projectAddress[$projectId]["projectName"];
					$MFInvoiceDetailData["A4"] = "Contractor: ".$ContractorName;
					$MFInvoiceDetailData["A6"] = "Ref: Invoice #".$InvoiceId." ".date("F, Y", strtotime($invoiceDate));
					$thisAddress = $projectAddress[$projectId]["address"];
					$thisTown = $projectAddress[$projectId]["town"];
					$thisAddressParts = explode(", ".$thisTown,$thisAddress);
					$MFInvoiceDetailData["A8"] = str_replace("<br>","",$thisAddressParts[0]);
					$MFInvoiceDetailData["A9"] = $projectAddress[$projectId]["town"]." MA";
					
					$hasIN = (count($ContractorInfo[$ContractorName]["IN"]) > 0 ? true : false);
					$hasIM = (count($ContractorInfo[$ContractorName]["IM"]) > 0 ? true : false);
					
					

					$invoiceCountNumber++;
					$INnewRowsToAdd = 0;
					$IMnewRowsToAdd = 0;
					foreach ($ContractorInfo[$ContractorName] as $INorIM=>$INorIMInfo){
						$MFInvoiceDetailDataINorIM = array();
						$detailRow = 12;
						if (count($ContractorInfo[$ContractorName]["IN"])){
							$INnewRowsToAdd = (count($ContractorInfo[$ContractorName]["IN"])-1);
							foreach ($ContractorInfo[$ContractorName]["IN"] as $id=>$thisData){
								$MFInvoiceDetailDataINorIM["IN"]["A".$detailRow] = $thisData["Measure Detail"];
								$MFInvoiceDetailDataINorIM["IN"]["C".$detailRow] = $thisData["Measure Description"];
								$MFInvoiceDetailDataINorIM["IN"]["D".$detailRow] = $thisData["Unit Quantity"];
								$MFInvoiceDetailDataINorIM["IN"]["E".$detailRow] = $thisData["Unit Cost"];
								$MFInvoiceDetailDataINorIM["IN"]["F".$detailRow] = $thisData["Total Cost"];
								$MFInvoiceDetailDataINorIM["IN"]["G".$detailRow] = $thisData["Unit Incentive"];
								$MFInvoiceDetailDataINorIM["IN"]["H".$detailRow] = $thisData["Total Incentive"];
								$lastDetailRow = $detailRow;
								$detailRow++;
							}
							//add formula row
							$MFInvoiceDetailDataINorIM["IN"]["F".$detailRow] = "=SUM(F12:F".$lastDetailRow.")";
							$MFInvoiceDetailDataINorIM["IN"]["H".$detailRow] = "=SUM(H12:H".$lastDetailRow.")";
						}
						$INFormulaRow = $detailRow;

						$IMStartRow = $detailRow = $detailRow+($hasIM ? 3 : 4);
						if (count($ContractorInfo[$ContractorName]["IM"])){
							$IMnewRowsToAdd = (count($ContractorInfo[$ContractorName]["IM"])-1);
							foreach ($ContractorInfo[$ContractorName]["IM"] as $id=>$thisData){
								$MFInvoiceDetailDataINorIM["IM"]["A".$detailRow] = $thisData["Measure Detail"];
								$MFInvoiceDetailDataINorIM["IM"]["C".$detailRow] = $thisData["Measure Description"];
								$MFInvoiceDetailDataINorIM["IM"]["D".$detailRow] = $thisData["Unit Quantity"];
								$MFInvoiceDetailDataINorIM["IM"]["E".$detailRow] = $thisData["Unit Cost"];
								$MFInvoiceDetailDataINorIM["IM"]["F".$detailRow] = $thisData["Total Cost"];
								$MFInvoiceDetailDataINorIM["IM"]["G".$detailRow] = $thisData["Unit Incentive"];
								$MFInvoiceDetailDataINorIM["IM"]["H".$detailRow] = $thisData["Total Incentive"];
								$lastDetailRow = $detailRow;
								$detailRow++;
							}
							//add formula row
							$MFInvoiceDetailDataINorIM["IM"]["F".$detailRow] = "=SUM(F".$IMStartRow.":F".$lastDetailRow.")";
							$MFInvoiceDetailDataINorIM["IM"]["H".$detailRow] = "=SUM(H".$IMStartRow.":H".$lastDetailRow.")";
						}
						
						$IMFormulaRow = $detailRow;
						
						//add grand total formula row
						$detailRow = $detailRow+($hasIM ? 2 : 1);
						$MFInvoiceDetailData["F".$detailRow] = "=F".$INFormulaRow."+F".$IMFormulaRow;
						$MFInvoiceDetailData["H".$detailRow] = "=H".$INFormulaRow."+H".$IMFormulaRow;
					}
					//print_pre($MFInvoiceDetailData);
					include('salesforce/reports/MFInvoiceDetail.php');
				}
			}
			foreach ($InvoiceDetailLinks as $link){
				//echo $link."<br>";
			}
//		echo "</div>";

		echo "<div id='AllFiles' class='tab-content'>";
//			echo $eTrackJobsFileLink;
//			echo $eTrackJobsFileLinkPipeDelimited;
//			echo $eTrackOrdersFileLink;
//			echo $eTrackOrdersFileLinkPipeDelimited;
//			echo $InvoiceFile01Link;
//			echo $InvoiceFile02Link;
//			foreach ($InvoiceDetailLinks as $link){
//				echo $link;
//			}
			$folderLocation = $siteRoot.$adminFolder."gbs/salesforce/exports";
			// Get real path for our folder
			$rootPath = realpath($folderLocation);

			// Initialize archive object
			$zip = new ZipArchive();
			$ZipFileName = 'salesforce/883_Eversource/'.date("Y_m",strtotime($invoiceDate)).'_Created'.date("Y_m_d").'.zip';
			$zip->open($ZipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

			// Create recursive directory iterator
			/** @var SplFileInfo[] $files */
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($rootPath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
				// Skip directories (they would be added automatically)
				if (!$file->isDir())
				{
					// Get real and relative path for current file
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath) + 1);

					// Add current file to archive
					$zip->addFile($filePath, $relativePath);
					$filesToDelete[] = $filePath;
				}
			}

			// Zip archive will be created only after closing object
			$zip->close();	
			// Delete all files from "delete list"
			foreach ($filesToDelete as $file)
			{
				unlink($file);
			}		
			//also Delete EversourceTrackingFile
			$thisEversourceTrackingFileLocation = $siteRoot.$adminFolder."gbs/salesforce/importfiles/EversourceTrackingFile.xlsx";
			unlink($thisEversourceTrackingFileLocation);
			
			echo "<a href='".$CurrentServer.$adminFolder."gbs/".$ZipFileName."'><img src='".$CurrentServer.$adminFolder."images/zip.png'>".date("Y_m",strtotime($invoiceDate)).".zip</a>";
		echo "</div>";

}//end if $projectAddress

if ($fileLoadSuccessfull){	
	//now get and parse account information
	$eversourceAccountsResults = $GBSProvider->invoiceTool_getEversourceAccounts();
}

	if ($fileReceived){
		echo "<span class='alert";
		if ($fileFailed){
			echo "'>There was a problem loading the file</span>";
		}else{
			echo " info'>File successfully loaded</span>";
		}
		echo "<br><br>";
	}else{
?>	
		<div id="EversourceAccountImport" class="tab-content">
			After you have received the Eversource Customer Account file, save it as a .csv file type and click the 'Choose Files' button below to select the file<br>
			<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>gbs/?nav=invoicingTool-Eversource" enctype="multipart/form-data">
				<div class="row">
					<div class="eight columns">
						<input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" onChange="makeFileList();" /><Br>
					</div>
					<div class="eight columns">
						<strong>File You Selected:</strong>
						<ul id="fileList"><li>No Files Selected</li></ul>
						<input type="submit" value="Begin Processing" id="submitButton" style="display:none;">
					</div>
				</div>
		</div>
	</div> <!--end tabs-->
		<script type="text/javascript">
			function makeFileList() {
				var input = document.getElementById("filesToUpload");
				var ul = document.getElementById("fileList");
				while (ul.hasChildNodes()) {
					ul.removeChild(ul.firstChild);
				}
				for (var i = 0; i < input.files.length; i++) {
					var li = document.createElement("li"),
						fileName = input.files[i].name,
						fileLength = fileName.length,
						fileExt = fileName.substr(fileLength-3);
						console.log(fileExt);
					if (fileExt != "csv"){
						li.innerHTML = 'You must save '+fileName+' as a csv file first';
						ul.appendChild(li);
						document.getElementById('submitButton').style.display = 'none';
					}else{
						li.innerHTML = input.files[i].name;
						ul.appendChild(li);
						document.getElementById('submitButton').style.display = 'block';
					}
				}
				if(!ul.hasChildNodes()) {
					var li = document.createElement("li");
					li.innerHTML = 'No Files Selected';
					ul.appendChild(li);
					document.getElementById('submitButton').style.display = 'none';
				}
			}
		</script>
	</form>
<?php
	}//end Files not uploaded
?>
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	

<b>Instructions</b>
<ul>
	<li<?php echo ($trackingFileReceived ? " class='alert info'" : "");?>>1. Connect to Salesforce to download and review Tracking File and attachments.
	<li<?php echo ($fileLoadSuccessfull ? " class='alert info'" : "");?>>1. Receive Eversource Customer Account file and save as .csv type.
	<li<?php echo ($fileLoadSuccessfull ? " class='alert info'" : "");?>>2. Use the above tool to upload this file.
<!--
	<ul>
		<li>4. Click <a href="https://na12.salesforce.com/00OU0000002jdbF" target="SFDashboardTACompleted">RW TA Completed - <?php echo date("Y")."-".date("Y",strtotime(date()."+1 year"));?></a> in Dashboard. (The link in these instructions will work if already logged into Salesforce in this same browser
		<li>5. Click Export Details button
		<li>6. Make sure Export File Encoding is ISO-8859-1 (should be the first option by default)
		<li>7. Select Export File Format of Comma Delimited .csv
		<li>8. Click the 'Export' button and then open up the downloaded file with Excel.
		<li>9. In Excel, click 'File' in the upper left corner then 'Save As'.
		<li>10. Choose the Folder location to be FOLDER.
		<li>11. Change the file name to 'Completed.csv' with Save As Type CSV (Comma delimited)(*.csv) and click 'Save'. NOTE there are three type of CSV file types, make sure using the first one (right after file type Microsoft Excel 5.0/95 Workbook)
		<li>12. Click 'Yes' to the screen prompt to keep using format then you can close this file without saving again.
	</ul>
	<li>13. Navigate back to Dashboards</li>
	<li><b>Create In-Progress.csv file:</b></li>
	<ul>
		<li>14. Click <a href="https://na12.salesforce.com/00OU0000001Ii6W" target="SFDashboardTAInProgress">RW TA In-Progress</a> in Dashboard. (The link in these instructions will work if already logged into Salesforce in this same browser
		<li>15. Click Export Details button
		<li>16. Make sure Export File Encoding is ISO-8859-1 (should be the first option by default)
		<li>17. Select Export File Format of Comma Delimited .csv
		<li>18. Click the 'Export' button and then open up the downloaded file with Excel.
		<li>19. In Excel, click 'File' in the upper left corner then 'Save As'.
		<li>20. Choose the Folder location to be FOLDER.
		<li>21. Change the file name to 'In-Progress.csv' with Save As Type CSV (Comma delimited)(*.csv) and click 'Save'. NOTE there are three type of CSV file types, make sure using the first one.
		<li>22. Click 'Yes' to the screen prompt to keep using format then you can close this file without saving again.
	</ul>
	-->
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$SalesForceHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>

