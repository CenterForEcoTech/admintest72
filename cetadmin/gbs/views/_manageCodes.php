<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");

$gbsProvider = new HoursProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$CodesActive[$record->name]=$record;
		$CodesByName[$record->name]=$record;
	}else{
		$CodesInActive[$record->name]=$record;
	}

}
//resort the products without display order by CodeID
ksort($CodesActive);
ksort($CodesInActive);
$SelectedCodesID = $_GET['CodeID'];
$SelectedCodeName = $_GET['CodeName'];
if ($SelectedCodeName && !$SelectedCodesID){
	$SelectedCodesID = $CodesByName[$SelectedCodeName]->id;
}

//Get groups
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
}
ksort($GroupsByName);


$addCode = $_GET['addCode'];
$displayOrderId = $CodeByID[$SelectedCodesID]->displayOrderId;
if ($addCode){$displayOrderId = (count($CodesActive)+1);}
//get employees with this Code
if ($SelectedCodesID){
	include_once($dbProviderFolder."HREmployeeProvider.php");
	$hrEmployeeProvider = new HREmployeeProvider($dataConn);
	$criteria = new stdClass();
	$paginationResult = $hrEmployeeProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		$TrainingEmployees[$record->fullName]["id"] = $record->id;
		$TrainingEmployees[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
		$EmployeesByID[$record->id]= $record;

	}

	ksort($TrainingEmployees);
	
	//get matrix of employees with this training
	$criteria = new stdClass();
	$criteria->codesId = $SelectedCodesID;
	//$criteria->department = $ThisDepartmentShort;
	$CodeMatrixResults = $gbsProvider->getCodesMatrix($criteria);
	$CodeMatrixCollection = $CodeMatrixResults->collection;
	if (count($CodeMatrixCollection)){
		foreach ($CodeMatrixCollection as $id=>$collection){
			$CodeMatrix[] = $collection;
			$EmployeesActive[$EmployeesByID[$collection->employeeId]->firstName." ".$EmployeesByID[$collection->employeeId]->lastName]=$EmployeesByID[$collection->employeeId];
			$MatrixInfo[$collection->employeeId][] = $collection;
			$EmployeesByDepartmentUnsorted[$EmployeesByID[$collection->employeeId]->department][]=$EmployeesByID[$collection->employeeId];
		}
	}

	include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
	$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
	$criteria = new stdClass();
	$criteria->showAll = true;
	$criteria->noLimit = true;
	$paginationResult = $hrEmployeeDepartmentsProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$DepartmentsByName[$record->name] = $record;
		$EmployeesByDepartment[$record->name] = $EmployeesByDepartmentUnsorted[$record->name];
		
	}
	
}//end if SelectedCodeName



?>
<?php if (!$addCode){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 360px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:380px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable3 {border: 2px solid #6d9eeb;height:367px;}
  #sortable4 {height:367px;}
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  </style>
<br>
  
	<div class="fifteen columns">
		<?php include('_CodesComboBox.php');?>
		<div class="two columns">
			<button id="showCodeSelectForm" style="font-size:8pt;">Change Display Order</button>
		</div>
	</div>
	<fieldset id="codeSelectForm">
		<legend>Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Active Codes</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($CodesActive as $code=>$codeInfo){
						echo "<li id=\"".$codeInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-codes&CodeID=".$codeInfo->id."\">".$codeInfo->description."</a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>InActive Codes</b></div>
				<ul id="sortable2" class="connectedSortable">
				<?php 
					foreach ($CodesInActive as $code=>$codeInfo){
						echo "<li id=\"".$codeInfo->id."\" class=\"ui-state-disabled\"><a href=\"?nav=manage-codes&CodeID=".$codeInfo->id."\" style=\"text-decoration:none;\"><span class=\"codeName\">".$codeInfo->description."</span></a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addCode?>
<?php if ($SelectedCodesID || $addCode){?>
	<form id="CodeUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Code Details</legend>
			<div class="fifteen columns">
				<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">
				<div style="display:none;">
					<?php if (!$addCode){?><input type="text" name="id" value="<?php echo $CodeByID[$SelectedCodesID]->id;?>"><?php }?>
				</div>
				<div class="fourteen columns">
					<div class="two columns">Name:</div><div class="ten columns">
						<input type="text" id="name" name="name" style="width:100px;" value="<?php echo $CodeByID[$SelectedCodesID]->name;?>">
						<input type="checkbox" id="nonbillable" name="nonbillable" value="1"<?php echo ($CodeByID[$SelectedCodesID]->nonbillable ? " checked" : "");?>>Non-Billable &nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" id="dormant" name="dormant" value="1"<?php echo ($CodeByID[$SelectedCodesID]->dormant ? " checked" : "");?>>Dormant (prevent from being assigned)</div>
					<br clear="both">
					<div class="two columns">Description:</div><div class="eleven columns"><input type="text" style="width:100%" id="description" name="description" value="<?php echo $CodeByID[$SelectedCodesID]->description;?>"></div>
					<br clear="both">
					<br clear="both">
					<!--
					<div class="two columns">Department:</div><div class="eleven columns">
						<select name="department"><option value="<?php echo $ThisDepartmentShort;?>"<?php if ($CodeByID[$SelectedCodesID]->department == $ThisDepartmentShort){echo " selected";}?>><?php echo $ThisDepartmentShort;?></option><option value="MO"<?php if ($CodeByID[$SelectedCodesID]->department == "MO"){echo " selected";}?>>MO</option></select>
					</div>
					-->
					<input type="hidden" name="department" value="All">
					<div class="two columns">Group:</div>
					<div class="eleven columns">
						<?php $groupsComboBoxHideLabel = true;
								$groupsComboBoxFunction = "//nothing";
								$SelectedGroupsID = $CodeByID[$SelectedCodesID]->groupId;
								include_once('_GroupsComboBox.php');
						?>
					</div>
				</div>
			</div>
		</fieldset>
		<div id="codeResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-codes&CodeID=<?php echo $SelectedCodesID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addCode){?>
					<a href="save-code-add" id="save-code-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="code_add">
				<?php }else{?>
					<a href="save-code-update" id="save-code-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="code_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
	<?php if ($SelectedCodesID && !$CodeByID[$SelectedCodesID]->dormant){?>
			<fieldset>
				<legend>Assign Staff to this Code</legend>
				<div class="fifteen columns">
					<div class="three columns" id="codesMatrixMessage">&nbsp;</div>
					<br clear="all">
					<div class="six columns"> 
						<div class="five columns"><b>Staff Assigned to use this Code</b><Br><span style="font-size:10pt;">&nbsp;</span></div>
						<ul id="sortable3" class="connectedSortable">
						<?php 
							ksort($EmployeesActive);
							foreach ($EmployeesActive as $employee=>$employeeInfo){
								foreach ($MatrixInfo[$employeeInfo->id] as $matrixInfo){
									$AssignedEmployees[] = $employeeInfo->id;
									$affectiveTimeStamp = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLTimeStampDisplay($matrixInfo->timeStamp));
									$affectiveDate = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLDate($matrixInfo->timeStamp));
									echo "<li id=\"".$employeeInfo->id."\" data-matrixId=\"".$matrixInfo->id."\" class=\"ui-state-default\" title='".$matrixInfo->status." ".$affectiveTimeStamp." by ".$matrixInfo->addedByAdmin." added into the system on ".MySQLTimeStampDisplay($matrixInfo->timeStamp)."'>".$employeeInfo->fullName."</a></li>";
									$removeEmployees[] = $matrixInfo->id;
								}
							}
						?>
						</ul>
							<?php if (!$ReadOnlyTrue){?>
								<?php if (count($AssignedEmployees)){?>
									<br clear="all"><br>
									<a href="remove-all-employees" id="remove-all-employees" class="button-link do-not-navigate" title="This button will remove currently assigned staff from using this code">Remove Code for All Staff</a>
									<div class="four columns" id="codesMatrixMessageAllRemove">&nbsp;</div>
								<?php }?>
							<?php }?>
					</div>
					<div class="one columns">&nbsp;</div>
					<div class="six columns">
						<div class="five columns"><b>All Staff</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
						<div class="six columns">
							<ul id="sortable4" class="connectedSortable">
							<?php 
								foreach ($EmployeesByID as $employee=>$employeeInfo){
									if (!in_array($employeeInfo->id,$AssignedEmployees)){
										$remainingEmployees[] = $employeeInfo->id;
										echo "<li id=\"".$employeeInfo->id."\" class=\"ui-state-default\">".$employeeInfo->fullName."</li>";
									}
								}
							?>
							</ul>
							<?php if (!$ReadOnlyTrue){?>
								<?php if (count($remainingEmployees)){?>
									<br clear="all"><br>
									<a href="add-to-all-employees" id="add-to-all-employees" class="button-link do-not-navigate" title="This button will allow all remaining staff to start using this code">Assign Code To All Staff</a>
									<div class="four columns" id="codesMatrixMessageAll">&nbsp;</div>
								<?php }?>
							<?php }?>
						</div>
					</div>
				</div>
				<fieldset>
					<legend>Instructions</legend>
					To assign staff to use this code: Drag one or more names from 'All Staff' to 'Staff Assigned to use this Code'.<br>
					To assign this code to all staff at once: Click the button 'Assign Code To All Staff'.<br>
					To remove staff from the 'Staff Assigned to use this Code': Drag one or more names to 'All Staff'.<br>
					To remove all staff at once: Click the button 'Remove Code for All Staff'
				</fieldset>
			</fieldset>
	<?php }//end if SelectedCodesID ?>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-codes&addCode=true" class="button-link">+ Add New Code</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
	

	var codeSelectForm = $("#codeSelectForm");
		
	codeSelectForm.hide();	
	
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedCodesID){?>
		$("#codeSelectForm").hide();
	<?php }?>
	$("#showCodeSelectForm").click(function(){
		$("#codeSelectForm").toggle();		
	});

	$("#codeSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#CodeUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-code-add").click(function(e){

			$('#codeResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						//console.log(data);
						$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						var displayOrderValue = parseInt($('#displayOrderId').val());
						$("#displayOrderId").val((displayOrderValue+1));	
						$("#name").val('');	
						$("#description").val('');	
						$("#nonbillable").removeAttr('checked');	
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#codeResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedCodesID){?>
		$("#save-code-update").click(function(e){

			$('#codeResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#codeResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
		$("#add-to-all-employees").click(function(e){
			$('#codesMatrixMessageAll').show().html("").removeClass("alert").removeClass("info");
			e.preventDefault();
				//console.log(JSON.stringify(formElement.serializeObject()));
			var DisplayArray = new Array();
			var assignedEmployees = new Array('<?php echo implode("','",$remainingEmployees);?>');
			if (assignedEmployees.length){
				$("#add-to-all-employees").fadeOut();
				DisplayArray.push({action: 'Insert_CodesMatrix',department: '<?php echo $ThisDepartmentShort;?>',codesId:<?php echo $SelectedCodesID;?>,items:assignedEmployees});
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(DisplayArray),
					success: function(data){
						$('#codesMatrixMessageAll').fadeIn().html("Codes "+data+" for all Staff").addClass("alert").addClass("info").fadeOut(3000);
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#codesMatrixMessageAll').fadeIn().html(message).removeClass("info").addClass("alert");
					}
				});
			}//end if AssignedEmployees.length
		});
		$("#remove-all-employees").click(function(e){
			$('#codesMatrixMessageAllRemove').show().html("").removeClass("alert").removeClass("info");
			e.preventDefault();
				//console.log(JSON.stringify(formElement.serializeObject()));
			var DisplayArray = new Array();
			var assignedEmployees = new Array('<?php echo implode("','",$removeEmployees);?>');
			if (assignedEmployees.length){
				$("#remove-all-employees").fadeOut();
				DisplayArray.push({action: 'Remove_CodesMatrix',department: '<?php echo $ThisDepartmentShort;?>',codesId:<?php echo $SelectedCodesID;?>,items:assignedEmployees});
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(DisplayArray),
					success: function(data){
						$('#codesMatrixMessageAllRemove').fadeIn().html("Codes "+data+" for all Staff").addClass("alert").addClass("info").fadeOut(3000);
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#codesMatrixMessageAllRemove').fadeIn().html(message).removeClass("info").addClass("alert");
					}
				});
			}//end if AssignedEmployees.length
		});
	<?php }//end if SelectedCodesID ?>
});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");

	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
				if ($(ui.item).parents('#sortable1').length > 0) {
					$(ui.item).switchClass('ui-state-disabled', 'ui-state-default');
				} else {
					$(ui.item).switchClass('ui-state-default', 'ui-state-disabled');
				}					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiCodesManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedCodesID){?>
			$( "#sortable3, #sortable4" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4").sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedEmployees = new Array();
					var alreadyAssigned = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value);
							thisMatrixID = $thisLi.attr('data-matrixId');
						if (thisMatrixID){
							alreadyAssigned.push(thisMatrixID);
						}else{
							assignedEmployees.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedEmployees.length){
						DisplayArray.push({action: 'Insert_CodesMatrix',department: '<?php echo $ThisDepartmentShort;?>',codesId:<?php echo $SelectedCodesID;?>,items:assignedEmployees});
					}
					if (alreadyAssigned.length){
						DisplayArray.push({action: 'Remove_CodesMatrix',department: '<?php echo $ThisDepartmentShort;?>',codesId:<?php echo $SelectedCodesID;?>,items:alreadyAssigned});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						$.ajax({
							url: "ApiCodesManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								$('#codesMatrixMessage').fadeIn().html("Codes "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#codesMatrixMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
		<?php }//end if selectedCodeID ?>

<?php }//not in readonly?>
});
</script>