<?php
include_once($dbProviderFolder."InvoicingProvider.php");
$InvoicingProvider = new InvoicingProvider($dataConn);

$criteria = new stdClass();
$criteria->displayName = $invoiceCodeName;
$invoicingCodes = $InvoicingProvider->getInvoicingCodes($criteria);
foreach ($invoicingCodes->collection as $result){
	$allCodes = explode(",",$result->codes);
	foreach ($allCodes as $code){
		$invoicingByCode[$code] = $result;
	}
	$invoicingByAllCodes[$result->codes] = $result;
	$invoicingByDisplayName[$result->displayName] = $result;
}
//print_pre($invoicingByDisplayName);
//print_pre($invoicingByAllCodes);
//print_pre($invoicingByCode);
$codesAffected = $invoicingByDisplayName[$invoiceCodeName]->codes;
$processRequirements = $invoicingByDisplayName[$invoiceCodeName]->processRequirements;
$processingTrigger = $invoicingByDisplayName[$invoiceCodeName]->processTrigger;
$processRecipient = $invoicingByDisplayName[$invoiceCodeName]->processRecipient;
$status = $invoicingByDisplayName[$invoiceCodeName]->status;
$notes = $invoicingByDisplayName[$invoiceCodeName]->notes;
$lastUpdated = $invoicingByDisplayName[$invoiceCodeName]->lastUpdatedBy." ".date("m/d/Y G:i a",strtotime($invoicingByDisplayName[$invoiceCodeName]->lastUpdatedDate));

$divHeight = (strlen($notes) > 1000 ? (((strlen($notes)/90)*30)+100) : 200);

$criteria->type = 'Notes';
$invoicingStatusHistoryResults = $InvoicingProvider->getInvoicingStatusHistory($criteria);
foreach ($invoicingStatusHistoryResults as $statusHistory){
	$history[] = array("date"=>date("m/d/Y G:i a",strtotime($statusHistory["InvoicingStatusHistory_TimeStamp"])),"from"=>$statusHistory["InvoicingStatusHistory_Old"],"to"=>$statusHistory["InvoicingStatusHistory_New"]);
}

		echo "<div id='Notes' class='tab-content'>";
			echo "<div class='twelve columns'>";
			echo "<br>Codes affected: ".$codesAffected;

/*
			echo "<br>Process Requirments: ".$processRequirements;
			echo "<br>Process Triggered By: ".$processTrigger;
			echo "<br>Recipient of Process when run: ".$processRecipient;
			echo "<br>Current Status: ".$status;
*/
			echo "<br>Status Last Updated: ".$lastUpdated;

			echo "<br><Br></div>";
?>
			<div class="twelve columns" style="border:0pt solid red;height:<?php echo ($divHeight+100);?>px;">
				Notes for <?php echo $invoiceCodeName;?> Invoicing Process: <br>
				<div id="notesDisplay"></div>
				<textarea style="width:100%;height:<?php echo $divHeight;?>px;" id="notes"><?php echo $notes;?></textarea>
				<button class="notesSubmit">Update Processing Notes</button>
			</div>
<?php
		if (count($history)){
			echo "<br clear='all'><br><br><div class='twelve columns'>Note Change History:<br>
				<table class='simpleTable'>
					<thead><tr><th>Change Date</th><th>Previous Note</th><th>Changed To</th></thead>
					<tbody>";
				foreach ($history as $historyItem){
					echo "<tr><td><span style='display:none;'>".strtotime($historyItem["date"])."</span>".$historyItem["date"]."</td><td>".$historyItem["from"]."</td><td>".$historyItem["to"]."</td></tr>";
				}
			echo "</tbody></table>";
		}
		echo "</div>";
	echo "</div>";
?>
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script>
	$(document).ready(function() {
		var notes = $("#notes"),
			notesSubmit = $(".notesSubmit");
		var updateComments = function(){
			var thisVal = notes.val();
			
			var Comments = {};
				Comments["action"] = "invoicing_note";
				Comments["invoicingCodeName"] = "<?php echo $invoiceCodeName;?>";
				Comments["note"] = thisVal;
				data = Comments;
				console.log(data);
			$.ajax({
				url: "ApiCodesManagement.php",
				type: "POST",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data);
					if (data.success){
						$("#notesDisplay").removeClass('alert').removeClass('info').addClass('info').html("Successfully Updated");
					}else{
						$("#notesDisplay").removeClass('alert').removeClass('info').addClass('alert').html("There was an Error Updating");
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					notes.val(message);
				}
			});
		};
		notesSubmit.on('click',updateComments);
	});
</script>
<?php } //don't use javascript if being autoprocessed to get report files?>
