<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the MassCEC Report file</h3>
<?php
$_SESSION['SalesForceReport'] = "MassCEC";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = date("m/01/Y");

include_once($siteRoot."_setupDataConnection.php");

$setTab = "Report";
	$TabCodes = array("Report"=>"Report","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Report' class='tab-content'>";
			$invoiceArray["622-1"]=array("title"=>"Task1 - Campaign Development","startDate"=>"2017-07-26","endDate"=>"2017-10-31","invoiceNumber"=>"622-1","invoiceAmount"=>23400,"deliverables"=>array("Deliverable Report","Attended kick-off meeting","Marketing and presentation materials","Pre-screening protocol","Funding fact sheet","Completed W-9"));
			$invoiceArray["622-2"]=array("title"=>"Task2 - Campaign Launch","startDate"=>"2017-11-01","endDate"=>"2018-01-31","invoiceNumber"=>"622-2","invoiceAmount"=>31320,"deliverables"=>array("Deliverable Report, including KPI data"));
			$invoiceArray["622-3"]=array("title"=>"Task3 - Customer Engagement","startDate"=>"2018-02-01","endDate"=>"2018-04-30","invoiceNumber"=>"622-3","invoiceAmount"=>24720,"deliverables"=>array("Deliverable Report, including KPI data","Webinar","Attend check-in meeting"));
			$invoiceArray["622-4"]=array("title"=>"Task4 - Ongoing Execution of Campaign (Part 1)","startDate"=>"2018-05-01","endDate"=>"2018-10-31","invoiceNumber"=>"622-4","invoiceAmount"=>28200,"deliverables"=>array("Deliverable Report, including KPI data","Video and print case studies"));
			$invoiceArray["622-5"]=array("title"=>"Task5 - Ongoing Execution of Campaign (Part 2) & Project Completion","startDate"=>"2018-11-01","endDate"=>"2019-08-30","invoiceNumber"=>"622-5","invoiceAmount"=>28200,"deliverables"=>array("Deliverable Report, including KPI data","Final Evaluation Report","Debriefing interview with MassCEC staff","All marketing and outreach collateral, presentation materials, and case studies"));
			$invoiceArray["622-6"]=array("title"=>"Task6 - Performance Hold-Back","startDate"=>"2019-05-01","endDate"=>"2019-08-30","invoiceNumber"=>"622-6","invoiceAmount"=>20640,"deliverables"=>array("Achievement of Performance Hold-Back Requirement"));
			echo "<div class='row'>
					<div class='eight'>
						<form name='invoiceNumberForm' id='invoiceNumberForm' method='get'>
						<input type='hidden' name='nav' value='".$_GET['nav']."'>
						Invoice Referrence: <select id='invoiceNumber' name='invoiceNumber'><option></option>";
						foreach ($invoiceArray as $invoiceNumber=>$invoiceDetails){
							echo "<option value='".$invoiceNumber."'".($_GET['invoiceNumber'] == $invoiceNumber ? " selected" : "").">".$invoiceDetails["title"].": ".date("m/d/Y",strtotime($invoiceDetails["startDate"]))."-".date("m/d/Y",strtotime($invoiceDetails["endDate"]))."</option>";
							
						}
						echo "</select>
					</div>
				</div>";
			if ($_GET['invoiceNumber']){	
				$invoiceNumber = $_GET['invoiceNumber'];
				echo "Using Tasks and Events with Due Date between ".date("m/d/Y",strtotime($invoiceArray[$invoiceNumber]["startDate"])) . " and ".date("m/d/Y",strtotime($invoiceArray[$invoiceNumber]["endDate"]))."<br><br>";
				require_once 'salesforce/config.php';
				require_once('salesforce/soap_connect.php');
				require_once('salesforce/rest_functions.php');
				$access_token = $_SESSION['access_token'];
				$instance_url = $_SESSION['instance_url'];
				$reportId = "00O0P000003TUBm"; //Invoicing/MassCEC Task Report

				//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
				if (!checkSession($instance_url, $reportId, $access_token)){
					session_start();
					$_SESSION['SalesForceReport'] = "P2I";
					$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
					echo "<script>window.location.replace('".AUTH_URL."');</script>";
					//echo AUTH_URL;
					//header('Location: '.AUTH_URL);
				}else{					
					ob_start();
					
					if (!isset($access_token) || $access_token == "") {
						die("Error - access token missing from session!");
					}
					if (!isset($instance_url) || $instance_url == "") {
						die("Error - instance URL missing from session!");
					}
				}//end if checkSession
			
				$reportId = "00O0P000003TUBm"; //Invoicing/MassCEC Task Report
				$reportName = "MassCEC Task Report";
				$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
				$reportRowsUnSorted = $reportResults["rows"];
				$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
				$reportLinks[] = $reportLink;
				foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
					$taskType = $rowDetail["TASK_TYPE"];
					if (trim($taskType) == "-"){$taskType = "Event";}
					$subject = $rowDetail["SUBJECT"];
					$status = $rowDetail["STATUS"];
					$dueDate = $rowDetail["DUE_DATE"];
					$lastUpdate = $rowDetail["LAST_UPDATE"];
					$currentStatus = str_replace("<br>","",$rowDetail["Activity.Activity_Current_Status_Detail__c"]);
					$notes = $rowDetail["FULL_DESCRIPTION"];
					$useThisRecord = false;
					if (strtotime($dueDate) >= strtotime($invoiceArray[$invoiceNumber]["startDate"]) && strtotime($dueDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceArray[$invoiceNumber]["endDate"])))){
						$useThisRecord = true;
					}
					if ($useThisRecord){
						$massCECBullets[$taskType][$status][] = array("subject"=>$subject,"detail"=>$currentStatus);
						$massCECBulletCount++;
					}

				}
				
				//also get GPs'
				$reportId = "00O0P000003TfmH"; //Invoicing/MassCEC GP Report
				$reportName = "MassCEC GP Report";
				$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
				$reportRowsUnSorted = $reportResults["rows"];
				$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
				$reportLinks[] = $reportLink;
				//print_pre($reportRowsUnSorted);
				foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
					$taskType = "Green Prospect";
					if (trim($taskType) == "-"){$taskType = "Event";}
					$subject = $rowDetail["CUST_NAME"];
					$status = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
					$dueDate = $rowDetail["CUST_LAST_UPDATE"];
					$currentStatus = str_replace("<br>","",$rowDetail["Upgrade__c.Current_Status_Detail__c"]);
					$useThisRecord = false;
					if (strtotime($dueDate) >= strtotime($invoiceArray[$invoiceNumber]["startDate"]) && strtotime($dueDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceArray[$invoiceNumber]["endDate"])))){
						$useThisRecord = true;
					}
					if ($useThisRecord){
						$massCECBullets[$taskType][$status][] = array("subject"=>$subject,"detail"=>$currentStatus);
						$massCECBulletCount++;
					}

				}
				
				
				
					//print_pre($massCECBullets);
					include_once('salesforce/reports/reportMassCEC.php');
					echo $ReportsFileLink;
			}else{
				echo "<Br><span class='alert'>Indicate the Start and End Dates</span>";
			}
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "MassCEC");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
	
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script type="text/javascript">
	$(function () {
		$("#invoiceNumber").on('change',function(){
			$("#invoiceNumberForm").submit();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php }//don't use javascript if being autoprocessed to get report files?>