<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."GBSProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'bullet_update'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->updateStaticBullet($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}
		if (trim($newRecord->action) == 'bullet_add'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->addStaticBullet($newRecord, getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
    case "PUT":
		echo "this is put";
        break;
}
output_json($results);
die();
?>