<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxFarms" ).combobox({
				select: function(event, ui){
					<?php if (!$farmsComboBoxFunction){?>
					window.location = "?nav=manage-farms&FarmID="+$(ui.item).val();
					<?php }else{
						echo $farmsComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($farmsComboBoxDivColumns ? $farmsComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$farmsComboBoxHideLabel){?>
				<label>Start typing group name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxFarms parameters" name="farmId" id="SelectedGroup">
				<option value="">Select one...</option>
				<?php 
					foreach ($FarmsByName as $name=>$farmInfo){
						echo "<option value=\"".$farmInfo->id."\"".($SelectedFarmsID==$farmInfo->id ? " selected" : "").">".$farmInfo->name."</option>";
					}
				?>
			  </select>
			</div>
		</div>