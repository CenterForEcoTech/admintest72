<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);


$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		$results = "Empty GET";
        header("HTTP/1.0 200 Success");
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = trim($newRecord->action);
		if ($action == "MFEP_AllocationChange"){
			$response = $GBSProvider->invoiceTool_updateMFEPAllocations($newRecord,getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif ($action == "BillingRate_RemoveEmployee"){
			$response = $GBSProvider->invoiceTool_removeEmployeeFromBillingRate($newRecord,getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif ($action == "BillingRate_Update"){
			$response = $GBSProvider->invoiceTool_updateBillingRates($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "BillingRate_AddEmployee"){
			$response = $GBSProvider->invoiceTool_addEmployeeFromBillingRate($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
			
		}
        break;
}
output_json($results);
die();
?>