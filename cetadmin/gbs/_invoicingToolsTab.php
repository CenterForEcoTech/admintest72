<?php
$invoiceDate = date("m/01/Y",strtotime(date()." -1 month"));
$ImportFileName = "importfiles/PassthroughFile".date("Y_m",strtotime($invoiceDate)).".xlsx";
//echo "ImportFileName ".$ImportFileName."<br>";
$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
$passThroughFile = "Not Yet Uploaded for ".date("F Y",strtotime($invoiceDate));
$passThroughColorTrue = "green";
$passThroughColorFalse = "orange";
$passThroughColor = $passThroughColorFalse;
if (file_exists($target_file)){
	$passThroughFile = "Last modified: ".date("F d, Y H:ia",filemtime($target_file));
	$passThroughColor = $passThroughColorTrue;
}
?>
<div id="<?php echo (!$showInvoicingToolsDropDown ? "invoicingToolDropDownItems" : "invoicingToolItems");?>">
	<a id="report-bgas" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-BGAS" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">BGAS 523,810,811</a><br>
	<a id="report-bigy" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-BigY" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColor;?>">BigY 540</a><br>
	<a id="report-columbiagas" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-ColumbiaGas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">Columbia Gas 525, 525D</a><br>
	<a id="report-covanta" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Covanta" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColor;?>">Covanta 516, 519</a><br>
	<a id="report-greenteam" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-GreenTeam" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColor;?>">Green Team 518</a><br>
	<a id="report-healthyhomes" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-HealthyHomes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">Healthy Homes 814</a><br>
	<a id="report-mfep" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-MFEP" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">MFEP 560</a><br>
	<a id="report-ngridinspection" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-NGRIDInspection" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">NGRID 524</a><br>
	
	<a id="report-pollutionprevention" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-PollutionPrevention" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColor;?>">Pollution Prevention 930</a><br>
	<a id="report-rw" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-RWTAPipeline" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColor;?>"><?php echo ($isStaff ? "RW TA Pipeline" : "Recycling Works");?> 531-539</a><br>
	<a id="report-smallcontracthours" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-SmallContractHours" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">Small Contract Hours 550, 552, 570, 591, 592, 620, 630</a><br>
	<a id="report-trc" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-TRC" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">TRC 515V</a><br>
	<a id="report-rus" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-RUS" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">RUS 1030</a><br>
	<a id="report-mmwec" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-MMWEC" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">MMWEC</a><br>

	<hr>
	<a id="report-passthrough" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Passthrough" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Passthrough File Upload</a> <?php echo $passThroughFile;?><br>
	<a id="report-bullets" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Bullets" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Last Month Bullets</a><br>
	<a id="report-bulletsquartlery" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-BulletsQuarterly" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Last Quarter Bullets</a><br>
	<a id="report-directinstalls" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-DirectInstalls" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Direct Installs Bgas & CMA</a><br>
	<a id="report-status" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Status" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Invoice Status</a><br>
	<a id="fileconverter" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-FileConverter" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">File Converter</a><br>

	<hr>Key:
		<br clear="all">
		<div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorFalse;?>">This Button Type Requires Passthrough File But File Not Yet Uploaded </div>
		<br clear="all">
		<div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:<?php echo $passThroughColorTrue;?>">Ready to Be Run once Hours are Approved</div>
		
	<!--
	<hr>In Process of Changes:
	<br>
			<a id="report-eversource" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=invoicingTool-Eversource" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Eversource</a><br>
			<a id="report-salesforceconnect" href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=salesforceConnect" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Salesforce Connection (For Eversource)</a><br>
	-->

</div>