<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxUtilitiesSecondary" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "$('#secondaryUtility').val($(ui.item).val());";
							// echo "window.location = '?nav=manage-codes&CodeID='+$(ui.item).val();";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxUtilitiesSecondary parameters " id="SelectedUtilitySecondary" data-hiddenid="secondaryUtility">
				<option value="">Select one...</option>
				<?php 
					foreach ($UtilitySecondaries as $SecondaryUtilityName){
						echo "<option value=\"".$SecondaryUtilityName."\"".($SelectedSecondaryUtility==$SecondaryUtilityName ? " selected" : "").">".$SecondaryUtilityName."</option>";
					}
				?>
			  </select>
			</div>
		</div>