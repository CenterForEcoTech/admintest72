<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once ($siteRoot.$adminFolder.'excelExportClasses/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("HES Stats Fiscal Year Title")
							 ->setSubject("HES Stats Fiscal Year Subject")
							 ->setDescription("HES Stats Fiscal Year Description");
// Add header data
$months = array("Nov"=>$FiscalYearStartYear,"Dec"=>$FiscalYearStartYear,"Jan"=>$FiscalYearEndYear,"Feb"=>$FiscalYearEndYear,"Mar"=>$FiscalYearEndYear,"Apr"=>$FiscalYearEndYear,"May"=>$FiscalYearEndYear,"Jun"=>$FiscalYearEndYear,"Jul"=>$FiscalYearEndYear,"Aug"=>$FiscalYearEndYear,"Sep"=>$FiscalYearEndYear,"Oct"=>$FiscalYearEndYear,"YTD"=>$FiscalYearStartYear."-".$FiscalYearEndYear);
$sheetIndex = 0;
foreach ($months as $month=>$year){
	$rowId = 1;
	if ($sheetIndex < 1){
        try {
            $objPHPExcel->setActiveSheetIndex(0);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->createSheet($sheetIndex);
        } catch (PHPExcel_Exception $e) {
        } //Setting index when creating
        try {
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	//create header row
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Item");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $ES);
        } catch (PHPExcel_Exception $e) {
        }
        $columnCount++;
	}
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Total");
    } catch (PHPExcel_Exception $e) {
    }


    $rowId++;
	//create Contract Open
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Contracts Open");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["Contract"]["ContractsOpen"]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["Contract"]["ContractsOpen"]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$columnCount++;
	}
	$column = getNameFromNumber($columnCount);
	if ($month == "YTD"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["Contract"]["ContractsOpen"]);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsOpen"]);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	$rowId++;
	//create Contract Pending
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Contracts Pending");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["Contract"]["ContractsPending"]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["Contract"]["ContractsPending"]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$columnCount++;
	}
	$column = getNameFromNumber($columnCount);
	if ($month == "YTD"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["Contract"]["ContractsPending"]);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsPending"]);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	$rowId++;
	//create Contract Info Closed Count
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Contracts Closed");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["Contract"]["ContractsClosedCount"]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["Contract"]["ContractsClosedCount"]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$columnCount++;
	}
	$column = getNameFromNumber($columnCount);
	if ($month == "YTD"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["Contract"]["ContractsClosedCount"]);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsClosedCount"]);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	$rowId++;
	//create Contract Info Amount
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Contract Amount");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["Contract"]["ContractsClosedAmount"]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["Contract"]["ContractsClosedAmount"]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$columnCount++;
	}
	$column = getNameFromNumber($columnCount);
	if ($month == "YTD"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["Contract"]["ContractsClosedAmount"]);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsClosedAmount"]);
        } catch (PHPExcel_Exception $e) {
        }
    }
	//set format to currency
    try {
        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":" . $column . $rowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_(@_)');
    } catch (PHPExcel_Exception $e) {
    }


    $rowId++;
	//create Visit Info
	ksort($VisitTypesFound);
	foreach ($VisitTypesFound as $visitType=>$count){
		$columnCount = 1;
		$column = getNameFromNumber($columnCount);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $visitType);
        } catch (PHPExcel_Exception $e) {
        }

        $columnCount++;
		foreach ($ESNameList as $ES=>$count){
			$column = getNameFromNumber($columnCount);
			if ($month == "YTD"){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["VisitTypes"][$visitType]);
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["VisitTypes"][$visitType]);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$columnCount++;
		}
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["VisitTypes"][$visitType]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["VisitTypes"][$visitType]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowId++;
	}
	/*
	//WIFI Scheduled Visits
		$columnCount = 1;
		$column = getNameFromNumber($columnCount);
		$objPHPExcel->getActiveSheet()->setCellValue($column.$rowId,"WIFI Appts Scheduled");
		
		$columnCount++;
		foreach ($ESNameList as $ES=>$count){
			$column = getNameFromNumber($columnCount);
			if ($month == "YTD"){
				$objPHPExcel->getActiveSheet()->setCellValue($column.$rowId,$wifiScheduledVisitsByES[$ES]);
			}else{
				$objPHPExcel->getActiveSheet()->setCellValue($column.$rowId,$wifiScheduledVisitsByESByDate[$month][$ES]);
			}
			$columnCount++;
		}
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
			$objPHPExcel->getActiveSheet()->setCellValue($column.$rowId,$wifiScheduledVisitsByES["ZZZTotal"]);
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue($column.$rowId,$wifiScheduledVisitsByESByDate[$month]["ZZZTotal"]);
		}
		$rowId++;
	*/

	//create ISM Info
	foreach ($ISMSToInclude as $bulbType=>$ISMInfo){
		foreach ($ISMInfo as $ISMName=>$record){
		$columnCount = 1;
		$column = getNameFromNumber($columnCount);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $ISMName . (!$bulbType ? " (non-bulb)" : ""));
            } catch (PHPExcel_Exception $e) {
            }

            $columnCount++;
		foreach ($ESNameList as $ES=>$count){
			$column = getNameFromNumber($columnCount);
			if ($month == "YTD"){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults[$ES]["ISMs"][$ISMName]);
                } catch (PHPExcel_Exception $e) {
                }
            }else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month][$ES]["ISMs"][$ISMName]);
                } catch (PHPExcel_Exception $e) {
                }
                if ($bulbType && $bulbType !='1(WIFI)'){
					$bulbTotal[$ES] = $bulbTotal[$ES]+$extractResultsByMonth[$month][$ES]["ISMs"][$ISMName];
					$bulbTotalByMonth[$month][$ES] = $bulbTotalByMonth[$month][$ES]+$extractResultsByMonth[$month][$ES]["ISMs"][$ISMName];
				}
				
			}
			$columnCount++;
		}
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResults["ZZZTotal"]["ISMs"][$ISMName]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $extractResultsByMonth[$month]["ZZZTotal"]["ISMs"][$ISMName]);
            } catch (PHPExcel_Exception $e) {
            }
            if ($bulbType){
				$bulbTotal["ZZZTotal"] = $bulbTotal["ZZZTotal"]+$extractResultsByMonth[$month]["ZZZTotal"]["ISMs"][$ISMName];
				$bulbTotalByMonth[$month]["ZZZTotal"] = $bulbTotalByMonth[$month]["ZZZTotal"]+$extractResultsByMonth[$month]["ZZZTotal"]["ISMs"][$ISMName];
			}
		}
			
		$rowId++;
		}
	}
	
	$rowId++;
	// create bulb Total
	$columnCount = 1;
	$column = getNameFromNumber($columnCount);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, "Bulb Total");
    } catch (PHPExcel_Exception $e) {
    }
    $columnCount++;
	foreach ($ESNameList as $ES=>$count){
		$column = getNameFromNumber($columnCount);
		if ($month == "YTD"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $bulbTotal[$ES]);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $bulbTotalByMonth[$month][$ES]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$columnCount++;
	}
	$column = getNameFromNumber($columnCount);
	if ($month == "YTD"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $bulbTotal["ZZZTotal"]);
        } catch (PHPExcel_Exception $e) {
        }
    }else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowId, $bulbTotalByMonth["ZZZTotal"]);
        } catch (PHPExcel_Exception $e) {
        }
    }
		
	


	//set width
    try {
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(35);
    } catch (PHPExcel_Exception $e) {
    }

    //set border
	$BoldUnderlineStyle = array(
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A1:" . $column . $rowId)->applyFromArray($BoldUnderlineStyle);
    } catch (PHPExcel_Exception $e) {
    }

    // Rename worksheet
    try {
        $objPHPExcel->getActiveSheet()->setTitle($month . "-" . $year);
    } catch (PHPExcel_Exception $e) {
    }

    $rowId++;
	$sheetIndex++;
}



$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$thisExcelFile = "HESStats_".date("Y-m-d",strtotime($startDate))."-".date("Y-m-d",strtotime($endDate)).".xlsx";

$pathExtension = "HESStats".$pathSeparator.$thisExcelFile;
$webpathExtension = "HESStats".$webpathSeparator.$thisExcelFile;
$saveLocation = $path.$pathExtension;

$ExportFile = "HESStats/".$thisExcelFile;

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
$thisExcelFileLink = "<a href='".$webtrackingLink."'>".$thisExcelFile."</a><br>";

$ExportFileLink = $thisExcelFileLink;
?>