<?php
$htmlBodyTemplateEnd = 
	"<div style='background-color:white;width:100%'>
		<div style='background-color:silver;width:80%;text-align:center;padding:2pt;'>
			<div style='margin-bottom:5pt;padding:5pt;text-align:center;font-size:14pt;width:95%;background-color:white;'>
				Confirmed Booking for:<br>
				<h3>".$customerName."</h3><hr>".
				$apptType." with ".$ES."<br><br>".
				$apptDate."<br>".
				$apptStartTime." - ".$apptEndTime."<Br><br>
				Location:<br>".$address."<br><br>
				Please have your most recent gas bill and electric bill available
			</div>
			For questions or corrections contact:<br>
			1-800-238-1221  Select option 3.<br>
			<br>
			Center for EcoTechnology<br>
			112 Elm St, Pittsfield, MA, 01201, United States<br>
			1-800-944-3212<Br>
			http://cetonline.org
		</div>
	</div>
	<p>&nbsp;</p>
	<p>For questions regarding your scheduled Home Energy Assessment please call 1-800-238-1221 and select option 3.<br></p>
	";
	
	
$txtBodyTemplate = "Confirmed Booking for:\r\n".$customerName."\r\n\r\n".
							$apptType." with ".$ES."\r\n\r\n".
							$apptDate."\r\n".
							$apptStartTime." - ".$apptEndTime."\r\n\r\n
							Location: ".$address."\r\n\r\n
							Please have your most recent gas bill and electric bill available\r\n\r\n
							
							For questions or corrections contact:\r\n
							Kacie Dean (413)-586-7350 ext 265\r\n
							\r\n
							Center for EcoTechnology\r\n
							112 Elm St, Pittsfield, MA, 01201, United States\r\n
							1-800-944-3212 \r\n
							http://cetonline.org \r\n";	
	
?>