<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = $ImportFileName;

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "NotesFiles".$pathSeparator."NotesFiles_".date("y_m", strtotime($invoiceDate)).".xlsx";
$pathExtension = $ReportsFile;
$webpathExtension = $ReportsFile;

$saveLocation = $path.$pathExtension;


$JustBoldStyle= array('font'=>array('bold'=>true));
$JustFamilyStyle= array('font'=>array('name'=>'Microsoft Sans Serif'));
try {
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Microsoft Sans Serif')->setSize(10);
} catch (PHPExcel_Exception $e) {
}

//copy the first row fields
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $templateSheet = $objPHPExcel->getActiveSheet()->copy();
} catch (PHPExcel_Exception $e) {
}
$thisSheet = clone $templateSheet;
try {
    $objPHPExcel->getActiveSheet()->setTitle("SQL");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(27);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(21);
} catch (PHPExcel_Exception $e) {
}


//Notes tab
try {
    $objPHPExcel->createSheet(1);
} catch (PHPExcel_Exception $e) {
}
//$objPHPExcel->addSheet($thisSheet);
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setTitle("Notes");
} catch (PHPExcel_Exception $e) {
}
//remove unused columns
try {
    $objPHPExcel->getActiveSheet()->removeColumn("O", 35);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("L");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("K");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("J");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("I");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("H");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("G");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("F");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->removeColumn("E");
} catch (PHPExcel_Exception $e) {
}

//add row header rows
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A1", "SITEID");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "CUST_FIRST_NAME");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "CUST_LAST_NAME");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "ADDRESS");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "ACCTCUST");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "LOCATIONID");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Status");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Rate");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(27);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($JustBoldStyle);
} catch (PHPExcel_Exception $e) {
}
//remove rows
$highestRow = count($extractDataLocationIds);
//$objPHPExcel->getActiveSheet()->removeRow(2,$highestRow);

$rowId = 2;
foreach ($BgasNotes as $locationId=>$rate){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $extractDataByLocationID[$locationId]["SITEID"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $extractDataByLocationID[$locationId]["CUST_FIRST_NAME"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $extractDataByLocationID[$locationId]["CUST_LAST_NAME"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $extractDataByLocationID[$locationId]["ADDRESS"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, " " . $extractDataByLocationID[$locationId]["ACCTCUST"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $extractDataByLocationID[$locationId]["LOCATIONID"]);
    } catch (PHPExcel_Exception $e) {
    }
//	$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,"");
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $rate);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:H" . $rowId)->applyFromArray($JustFamilyStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('E:E')->getNumberFormat()->setFormatCode(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setTitle("SQL Results");
} catch (PHPExcel_Exception $e) {
}

try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>