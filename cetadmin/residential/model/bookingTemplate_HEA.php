<?php
$file = "wififlier.pdf";
$htmlBodyTemplateIntro = "

<p>Thank you for scheduling your no-cost Mass Save&reg; Home Energy Assessment. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.</p>
<p><strong>Appointment Details: </strong>Site ID: ".$siteId."</p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong> (Your Energy Specialist will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>2 hours</strong> for the completion of the assessment.)</p>
<p><strong>Notes For Assessment:</strong></p>
<ul>
	<li>For account verification and usage details, please have a current gas and electric bill available for your Energy Specialist.</li>
	<li>An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.</li>
	<li>Please clear obstacles to attic accesses, heating and hot water equipment.</li>
	<li>Please secure all pets.</li>
</ul>
<p><strong>What to Expect:</strong></p>
<p>At the conclusion of your assessment, the Energy Specialist will provide a report detailing opportunities for all applicable rebates and incentives, including:</p>
<ul>
	<li>No-cost air sealing</li>
	<li>75% instant incentive on recommended insulation improvements</li>
	<li>Generous rebates on qualifying heating, cooling and water heating equipment</li>
</ul>
<p>During the Assessment, energy saving products installed at no cost may include:</p>
<ul>
	<li>ENERGY STAR<sup>&reg;</sup>LED light bulbs</li>
	<li>Low-flow showerheads</li>
	<li>Faucet aerators</li>
	<li>Advanced power strips</li>
	<li>Programmable thermostats or discounted&nbsp;<a target='massSave' href='https://www.masssave.com/en/learn/residential/smart-thermostat-faq/'>wireless thermostats&nbsp;</a>(installed at no cost at a second appointment)</li>
</ul>
<p><strong>Enhanced Incentives</strong></p>
<p>Mass Save now offers enhanced incentives and rebates to income-eligible households. Enhanced offerings include:</p>
<ul>
	<li>75% instant incentive on recommended insulation improvements</li>
	<li>$350 clothes washer rebate*</li>
</ul>
<p>*Please note existing appliances need to be verified as eligible for these rebates during a Mass Save Site visit.</p>
<p>Visit <a href='http://masssave.com/qualify' target='massSave'>masssave.com/qualify</a> for details.&nbsp; If you think you may qualify for enhanced incentives, we recommend that you begin the verification process prior to your Home Energy Assessment by calling 1-866-537-7267 and selecting option 1.</p>";

$htmlBodyTemplateEnd = "
<p>&nbsp;</p>
<p>For questions regarding your scheduled Home Energy Assessment please call 1-800-944-3212.</p>
";

$textBodyTemplateIntro = 
"Thank you for scheduling your no-cost Mass Save Home Energy Assessment. You're on your way to energy efficiency, greater savings, and improved comfort.
\r\n\r\n
	Appointment Details: Site ID: ".$siteId."
\r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." (Your Energy Specialist will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 2 hours for the completion of the assessment.)
\r\n\r\n
	Notes For Assessment:
\r\n\r\n
\r\n\r\n
	For account verification and usage details, please have a current gas and electric bill available for your Energy Specialist.
\r\n\r\n
	An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.
\r\n\r\n
	Please clear obstacles to attic accesses, heating and hot water equipment.
\r\n\r\n
	Please secure all pets.
\r\n\r\n
\r\n\r\n
	What to Expect:
\r\n\r\n
	At the conclusion of your assessment, the Energy Specialist will provide a report detailing opportunities for all applicable rebates and incentives, including:
\r\n\r\n
	No-cost air sealing
\r\n\r\n
	75% instant incentive on recommended insulation improvements
\r\n\r\n
	Generous rebates on qualifying heating, cooling and water heating equipment
\r\n\r\n
\r\n\r\n
	During the Assessment, energy saving products installed at no cost may include:
\r\n\r\n
		ENERGY STAR<sup>&#174;</sup> LED light bulbs
\r\n\r\n
		Low-flow showerheads
\r\n\r\n
		Faucet aerators
\r\n\r\n
		Advanced power strips
\r\n\r\n
		Programmable thermostats or discounted wireless thermostats (installed at no cost at a second appointment)
\r\n\r\n
\r\n\r\n
	Enhanced Incentives
\r\n\r\n
	Mass Save now offers enhanced incentives and rebates to income-eligible households. Enhanced offerings include:
\r\n\r\n
	75% instant incentive on recommended insulation improvements
\r\n\r\n
	$350 clothes washer rebate*
\r\n\r\n
	*Please note existing appliances need to be verified as eligible for these rebates during a Mass Save Site visit.
\r\n\r\n
\r\n\r\n
	Visit http://masssave.com/qualify for details. If you think you may qualify for enhanced incentives, we recommend that you begin the verification process prior to your Home Energy Assessment by calling 1-866-537-7267 and selecting option 1.
\r\n\r\n";
$textBodyTemplateEnd = "
\r\n\r\n
	For questions regarding your scheduled Home Energy Assessment please call 1-800-944-3212.
";