<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run
$measureDataClean = array();//clear out from last run
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."residential/" : "").$ImportFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$extractSheets = array("Installed Today","Contract_Weather","Contract_AirSealing","Contract_WIFI");
$extractFields["Installed Today"]=array(0=>"DESCRIPTION",4=>"PROP_QTY,INS_QTY",6=>"UTIL_PRICE",8=>"CUST_PRICE",13=>"MEASTABLE",14=>"PROP_PARTID,INS_PARTID",15=>"MBTU_SAVINGS",16=>"MEASURELIFE",17=>"ContractType",18=>"ICTV_AWARE_SIR",19=>"ICTV_AWARE_SIMPLEPAYBACK");
$extractFields["Contract_Weather"]=array(1=>"DESCRIPTION",6=>"PROP_QTY,INS_QTY",12=>"CUST_PRICE",13=>"MEASTABLE",14=>"PROP_PARTID,INS_PARTID",16=>"MBTU_SAVINGS",17=>"MEASURELIFE",18=>"UTIL_PRICE",20=>"ICTV_AWARE_SIR",21=>"ICTV_AWARE_SIMPLEPAYBACK");
$extractFields["Contract_AirSealing"]=array(1=>"DESCRIPTION",6=>"PROP_QTY,INS_QTY",12=>"CUST_PRICE",13=>"MEASTABLE",14=>"PROP_PARTID,INS_PARTID",16=>"MBTU_SAVINGS",17=>"MEASURELIFE",18=>"UTIL_PRICE",20=>"ICTV_AWARE_SIR",21=>"ICTV_AWARE_SIMPLEPAYBACK");
//$extractFields["Contract_WIFI"]=array(1=>"DESCRIPTION",6=>"PROP_QTY,INS_QTY",12=>"CUST_PRICE",13=>"MEASTABLE",14=>"PROP_PARTID,INS_PARTID",16=>"MBTU_SAVINGS",17=>"MEASURELIFE",18=>"UTIL_PRICE",20=>"ICTV_AWARE_SIR",21=>"ICTV_AWARE_SIMPLEPAYBACK");
$extractFields["Contract_WIFI"]=array(1=>"DESCRIPTION",6=>"PROP_QTY,INS_QTY",8=>"UTIL_PRICE",10=>"CUST_PRICE",12=>"TOTAL_PRICE",13=>"MEASTABLE",14=>"PROP_PARTID,INS_PARTID",16=>"MBTU_SAVINGS",17=>"MEASURELIFE",20=>"ICTV_AWARE_SIR",21=>"ICTV_AWARE_SIMPLEPAYBACK");

$worksheetNames = $objReader->listWorksheetNames($inputFileName);
//$worksheetNames = array("This","That","The Other");
foreach ($worksheetNames as $sheetName) {
	$sheetArray[] = $sheetName;
//    echo $sheetName, '<br />';
}
//print_pre($sheetArray);
$sheetIndex = 0;
$dataRecord = array();
$efiFlag = 0;
do {
    try {
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
    } catch (PHPExcel_Exception $e) {
    }
    $currentSheetTitle = $sheetArray[$sheetIndex];
	//echo $currentSheetTitle."<br>";
	//print_pre($auditFieldsBySheet[$currentSheetTitle])."<br>";
	foreach ($auditFieldsBySheet[$currentSheetTitle] as $id=>$info){
		$cell = $info["cell"];
		$fieldName = $info["fieldName"];
        try {
            $thisCellValue = $objPHPExcel->getActiveSheet()->getCell($cell)->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        //echo $fieldName ."=".$thisCellValue."<bR>";
		$dataRecordBySheet[$currentSheetTitle][$fieldName] = $thisCellValue;
		$dataRecordByFieldName[$fieldName] = $thisCellValue;
	}
//	echo "<hr>";
	//get SiteId and ProjectId
	if ($currentSheetTitle == "Customer Info"){
        try {
            $siteId = $objPHPExcel->getActiveSheet()->getCell("C17")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        //$AccountName = $objPHPExcel->getActiveSheet()->getCell("C3")->getCalculatedValue();
        try {
            $projectId = $objPHPExcel->getActiveSheet()->getCell("G17")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        //$contractorName = $objPHPExcel->getActiveSheet()->getCell("S8")->getCalculatedValue();
        try {
            $moderateIncome = $objPHPExcel->getActiveSheet()->getCell("O17")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        //$SprayFoam = $objPHPExcel->getActiveSheet()->getCell("P17")->getCalculatedValue();
		//$OVERTIME = $objPHPExcel->getActiveSheet()->getCell("P14")->getCalculatedValue();
		//$BUSINESSTYPE = $objPHPExcel->getActiveSheet()->getCell("C24")->getCalculatedValue();
		//$PermitFee = $objPHPExcel->getActiveSheet()->getCell("L17")->getCalculatedValue();
        try {
            $electricProvider = $objPHPExcel->getActiveSheet()->getCell("C20")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $createCustomerDocumentsStatus = $objPHPExcel->getActiveSheet()->getCell("E34")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $createCustomerDocumentsStatusText = $objPHPExcel->getActiveSheet()->getCell("E33")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $ezDocVersionDate = $objPHPExcel->getActiveSheet()->getCell("T38")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }

    }
	if ($currentSheetTitle == "Mechanical"){
        try {
            $efiHeatingTrigger = $objPHPExcel->getActiveSheet()->getCell("J30")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $efiACTrigger = $objPHPExcel->getActiveSheet()->getCell("J175")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        if (trim($efiHeatingTrigger) != "" || trim($efiACTrigger) != ""){$efiFlag = 1;}
	}
	if ($currentSheetTitle == "Appliances"){
        try {
            $efiFridgeTrigger = $objPHPExcel->getActiveSheet()->getCell("H24")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $efiWasherTrigger = $objPHPExcel->getActiveSheet()->getCell("H47")->getCalculatedValue();
        } catch (PHPExcel_Exception $e) {
        }
        if (trim($efiFridgeTrigger) != "" || trim($efiWasherTrigger) != ""){$efiFlag = 1;}
	}

	//if sheet is a contract or ism installed then do something different
	if (in_array($currentSheetTitle,$extractSheets)){
		$RangeEnd = 50;
		if ($currentSheetTitle == "Contract_Weather"){
            try {
                $objWorksheet = $objPHPExcel->getActiveSheet();
            } catch (PHPExcel_Exception $e) {
            }
            $i = 1;
			foreach ($objWorksheet->getRowIterator() as $row) {

				foreach ($row->getCellIterator() as $cell) {
					$cellValue = trim($cell->getValue());
					//echo $cellValue."<br>";
					if (strpos($cellValue,"IFNA")){
						$RangeEnd = ($i-1);
						//echo "Row ".$RangeEnd;
						break;
						
					}
				}
				if ($i > $RangeEnd) break;
				$i++;
			}		
		}
        try {
            $measureData[$currentSheetTitle] = $objPHPExcel->getActiveSheet()->rangeToArray('A1:T' . $RangeEnd, NULL, TRUE, FALSE);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	$sheetIndex++;
	$nextSheetTitle = $sheetArray[$sheetIndex];
} while ($nextSheetTitle != "Hidden After This Sheet");
//print_pre($measureData["Contract_Weather"]);
foreach ($measureData as $sheetName=>$data){
	$rowStart = false;
	$rowEnd = false;
	//echo $sheetName." rowStart=".$rowStart." rowEnd=".$rowEnd."<Br>";
	//echo $sheetName.": <br>";
	foreach ($data as $row=>$column){
		//echo $row." = '".(string)$column[0]."'<Br>";
		if (strpos(" ".$column[0],"Contract ID:")){$rowStart = $row;}
		if ((string)$column[0] == "footer"){$rowEnd = $row;break;}
	}
	//echo $sheetName." rowStart=".$rowStart." rowEnd=".$rowEnd."<Br>";
	if ($rowStart && $rowEnd){
		$measureDataRows[$sheetName]["rowStart"] = $rowStart;
		$measureDataRows[$sheetName]["rowEnd"] = $rowEnd;
	}
}
//print_pre($measureDataRows);
foreach ($measureDataRows as $sheetName=>$startEnd){
	$rowData = array();
	$rowStart = ($startEnd["rowStart"]+1);
	$rowEnd = $startEnd["rowEnd"];
	if ($rowEnd>$rowStart){
		$contractID = str_replace("Contract ID: ","",$measureData[$sheetName][$startEnd["rowStart"]][0]);
		while ($rowStart<$rowEnd){
			if ($sheetName == "Installed Today" && $measureData[$sheetName][$rowStart][17] == "_WIFI"){
				echo "skipping Installed Today row ".$rowStart."<Br>";
				//skip rows of WIFI's on Installed Today sheet
			}else{
				foreach ($extractFields[$sheetName] as $col=>$field){
					if (strpos($field,",")){
						$fields = explode(",",$field);
						foreach ($fields as $thisField){
							$rowData[$thisField] = $measureData[$sheetName][$rowStart][$col];
						}
					}else{
						$rowData[$field] = $measureData[$sheetName][$rowStart][$col];
					}
				}
				
				//PAYEE
				$rowData["PAYEE"] = "BERKSHIR-XTRACT";
				if ($rowData["MEASTABLE"] == "VW_LIGT_BULB_RCMDN" || $rowData["PROP_PARTID"] == "TrickleStarPowerStrip"){
					if ($electricProvider == "National Grid" || $electricProvider == "NGRID"){
						$rowData["PAYEE"] = "BG-NGRID-PI-418B";
					}
					if ($electricProvider == "Eversource"){
						$rowData["PAYEE"] = "WME-PI-418";
					}
				}
				if ($rowData["MEASTABLE"] == "WIFI"){
						$rowData["PAYEE"] = "Manual";
				}
				if ($rowData["PROP_PARTID"] == "Additional Wireless Thermostat 2 and 3" || trim($rowData["PROP_PARTID"]) == "Repeater 1-3"){
						$rowData["PAYEE"] = "CET";
				}
					
				//MEAS_STATUS
				$rowData["MEAS_STATUS"] = "BILLING";
				if (strpos($contractID,"WIFI")){
					$rowData["MEAS_STATUS"] = "WIFI";
				}
				if (strpos($contractID,"ISM")){
					$rowData["MEAS_STATUS"] = "INSTALLED";
				}
				if ($moderateIncome == "Yes"){
					$rowData["MEAS_STATUS"] = "BILLING_LI";
				}
				
				//Other fixes
				if (trim($rowData["PROP_PARTID"]) == "Repeater 1-3"){
					$rowData["PAYEE"] = "CET";
					$rowData["MEAS_STATUS"] = "WIFI";
					$rowData["INSTALLER_NAME"] = "CET";
				}

				
				$measureDataClean[$sheetName][$contractID][] = $rowData;
			}
			$rowStart++;
		}
	}//end if rows between start and end
}

//  Get worksheet dimensions

?>