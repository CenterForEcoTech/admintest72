<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = "model/bgasInvoice_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "InvoiceFiles".$pathSeparator."InvoiceFiles_".date("Y_m", strtotime($invoiceDate))."_raw.xlsx";
$pathExtension = $ReportsFile;
$webpathExtension = $ReportsFile;

$saveLocation = $path.$pathExtension;

//Invoice tab
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E15", $invoiceExtractDataInstalls["FeeHEAVisit"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E16", $invoiceExtractDataInstalls["FeeHEAAddUnitVisit"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E17", $invoiceExtractDataInstalls["FeeShvVisit"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E18", $invoiceExtractDataInstalls["FeeRenterVisit"]["count"]);
} catch (PHPExcel_Exception $e) {
}


//Installs tab
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
//Thermostat
	$ThermostatCount = ($invoiceExtractDataInstalls["HWellThermostat_Boiler"]["count"]+$invoiceExtractDataInstalls["HWellThermostat_Furnace"]["count"]+$invoiceExtractDataInstalls["HWellThermostat_Combo"]["count"]);
	$ThermostatTherms = ($invoiceExtractDataInstalls["HWellThermostat_Boiler"]["therms"]+$invoiceExtractDataInstalls["HWellThermostat_Furnace"]["therms"]+$invoiceExtractDataInstalls["HWellThermostat_Combo"]["therms"]);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B8", $ThermostatCount);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", $ThermostatTherms);
} catch (PHPExcel_Exception $e) {
}
$ThermostatCountHPC = ($invoiceExtractDataInstalls["HWellThermostat_Boiler_HPC"]["count"]+$invoiceExtractDataInstalls["HWellThermostat_Furnace_HPC"]["count"]+$invoiceExtractDataInstalls["HWellThermostat_Combo_HPC"]["count"]);
	$ThermostatThermsHPC = ($invoiceExtractDataInstalls["HWellThermostat_Boiler_HPC"]["therms"]+$invoiceExtractDataInstalls["HWellThermostat_Furnace_HPC"]["therms"]+$invoiceExtractDataInstalls["HWellThermostat_Combo_HPC"]["therms"]);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B9", ($ThermostatCountHPC ?: ""));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G9", ($ThermostatThermsHPC ?: ""));
} catch (PHPExcel_Exception $e) {
}

//Aerator with flip
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B12", $invoiceExtractDataInstalls["AeratorFlip_22"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G12", $invoiceExtractDataInstalls["AeratorFlip_22"]["therms"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B13", $invoiceExtractDataInstalls["AeratorFlip_22_HPC"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G13", $invoiceExtractDataInstalls["AeratorFlip_22_HPC"]["therms"]);
} catch (PHPExcel_Exception $e) {
}

//Aerator without flip
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B14", $invoiceExtractDataInstalls["WFP2a"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G14", $invoiceExtractDataInstalls["WFP2a"]["therms"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B15", $invoiceExtractDataInstalls["WFP2a_HPC"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G15", $invoiceExtractDataInstalls["WFP2a_HPC"]["therms"]);
} catch (PHPExcel_Exception $e) {
}

//Showerhead
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B16", $invoiceExtractDataInstalls["ShowerheadEarth_17"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G16", $invoiceExtractDataInstalls["ShowerheadEarth_17"]["therms"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B17", $invoiceExtractDataInstalls["ShowerheadEarth_17_HPC"]["count"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G17", $invoiceExtractDataInstalls["ShowerheadEarth_17_HPC"]["therms"]);
} catch (PHPExcel_Exception $e) {
}


//Weatherization tab
try {
    $objPHPExcel->setActiveSheetIndex(5);
} catch (PHPExcel_Exception $e) {
}
$rowId = 16;
ksort($reqData);
foreach ($reqData as $contractor=>$jobNameData){
	ksort($jobNameData);
	foreach ($jobNameData as $jobName=>$siteData){
		$isNewParticipant = (count($newParticipant[$jobName]) > 1 ? false : true);
		foreach ($siteData as $siteId=>$jobs){
			foreach ($jobs as $jobData){
				if (!strpos(" ".strtolower($jobData["installationContractor"]),"american")){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $jobData["jobName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $extractDataBySiteID[$siteId]["locationId"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $jobData["totalCost"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $jobData["bgasCost"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, ($isNewParticipant ? "1" : "0"));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "New");
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, ($jobData["jobType"] == "airSealing" ? $invoiceExtractData[$siteId]["asTherms"] : $invoiceExtractData[$siteId]["insTherms"]));
                    } catch (PHPExcel_Exception $e) {
                    }
//					$objPHPExcel->getActiveSheet()->setCellValue("J".$rowId,$jobData["jobType"]);
//					$objPHPExcel->getActiveSheet()->setCellValue("K".$rowId,"AS ".$jobData["asBgas"]." INS ".$jobData["insBgas"]);
//					$objPHPExcel->getActiveSheet()->setCellValue("L".$rowId,"INS ".$jobData["insBgas"]." AS ".$jobData["asBgas"]);
					$isNewParticipant = false;
					$rowId++;
				}else{
					$HPCWeatherization[$jobName][$siteId][] = $jobData;
				}
				
			}//end foreach jobs
			
		}//end foreach siteData
	}//end foreach jobNameData
}// end foreach contractor
$rowId = 63;//populate HPC Incentives
ksort($reqData);
foreach ($HPCWeatherization as $jobName=>$siteData){
	$isNewParticipant = (count($newParticipant[$jobData["jobName"]]) > 1 ? false : true);
	foreach ($siteData as $siteId=>$jobs){
		foreach ($jobs as $jobData){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $jobData["jobName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $extractDataBySiteID[$siteId]["locationId"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $jobData["totalCost"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $jobData["bgasCost"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, ($isNewParticipant ? "1" : "0"));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, "New");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, ($jobData["jobType"] == "airSealing" ? $invoiceExtractData[$siteId]["asTherms"] : $invoiceExtractData[$siteId]["insTherms"]));
            } catch (PHPExcel_Exception $e) {
            }
            $isNewParticipant = false;
			$rowId++;
		}
	}
}//end foreach HPCWeatherization
$rowId = 84;//populate Low Income 
ksort($reqData);
foreach ($lowIncomeSiteIds as $siteId=>$fullName){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $fullName);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $extractDataBySiteID[$siteId]["locationId"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, ($invoiceExtractData[$siteId]["asTherms"] + $invoiceExtractData[$siteId]["insTherms"]));
    } catch (PHPExcel_Exception $e) {
    }
    $rowId++;
}//end lowIncomeSites
//echo $saveLocation."<Br>";


//Hours tab
try {
    $objPHPExcel->setActiveSheetIndex(6);
} catch (PHPExcel_Exception $e) {
}
//Events
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B4", $InvoiceHourDataByBillingRateCode["Events"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B5", $InvoiceHourDataByBillingRateCode["Events"]["Deptartment Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B6", $InvoiceHourDataByBillingRateCode["Events"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//HPC Program
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B16", $InvoiceHourDataByBillingRateCode["HPC"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B17", $InvoiceHourDataByBillingRateCode["HPC"]["Deptartment Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B18", $InvoiceHourDataByBillingRateCode["HPC"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Pre Weatherization Incentive Hours
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B27", $InvoiceHourDataByBillingRateCode["PreWx"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B28", $InvoiceHourDataByBillingRateCode["PreWx"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B29", $InvoiceHourDataByBillingRateCode["PreWx"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Renter Initiative
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B38", $InvoiceHourDataByBillingRateCode["Renter"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B39", $InvoiceHourDataByBillingRateCode["Renter"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B40", $InvoiceHourDataByBillingRateCode["Renter"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Whole House Incentive Offering
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B49", $InvoiceHourDataByBillingRateCode["Wholehouse/Landlord"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B50", $InvoiceHourDataByBillingRateCode["Wholehouse/Landlord"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B51", $InvoiceHourDataByBillingRateCode["Wholehouse/Landlord"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Moderate Income Initiative
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B60", $InvoiceHourDataByBillingRateCode["Moderate Income"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B61", $InvoiceHourDataByBillingRateCode["Moderate Income"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B62", $InvoiceHourDataByBillingRateCode["Moderate Income"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//OPower
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B71", $InvoiceHourDataByBillingRateCode["OPower"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B72", $InvoiceHourDataByBillingRateCode["OPower"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B73", $InvoiceHourDataByBillingRateCode["OPower"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Wireless Thermostat Offering
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B82", $InvoiceHourDataByBillingRateCode["WiFi"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B83", $InvoiceHourDataByBillingRateCode["WiFi"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B84", $InvoiceHourDataByBillingRateCode["WiFi"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B85", $InvoiceHourDataByBillingRateCode["WiFi"]["Technical Field Staff"]);
} catch (PHPExcel_Exception $e) {
}

//Early Boiler Replacement
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B93", $InvoiceHourDataByBillingRateCode["EBR"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B94", $InvoiceHourDataByBillingRateCode["EBR"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B95", $InvoiceHourDataByBillingRateCode["EBR"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//Summer Sizzler Replacement
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B104", $InvoiceHourDataByBillingRateCode["EBR"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B105", $InvoiceHourDataByBillingRateCode["EBR"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B106", $InvoiceHourDataByBillingRateCode["EBR"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}

//KPI Data
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B115", $InvoiceHourDataByBillingRateCode["KPI Data"]["Program Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B116", $InvoiceHourDataByBillingRateCode["KPI Data"]["Department Manager"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B117", $InvoiceHourDataByBillingRateCode["KPI Data"]["Admin Assistant"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>