<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run

require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."residential/" : "").$ImportFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader("Excel2007");
//	$objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "EZDocHighlightedTemplate.xlsm";

$pathExtension = "HESDataFiles".$pathSeparator.$ReportsFile;
$webpathExtension = "HESDataFiles".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;


$highlighted = array(
	'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFFF00'))
);

if(!defined('EOL')) define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
/*
if($objPHPExcel->hasMacros()){
    echo 'macros in file '.EOL;
    if($objPHPExcel->hasMacrosCertificate())
        echo ' signed '.EOL;
    else
        echo ' not signed'.EOL;
}else
    echo 'no macros in file'.EOL;
if($objPHPExcel->hasRibbon()){
    echo 'Ribbon in file'.EOL;
    if($objPHPExcel->hasRibbonBinObjects())
        echo 'additional objects for the Ribbon'.EOL;
    else
        echo 'NO additional object for the Ribbon'.EOL;
}else
    echo 'No ribbon'.EOL;
*/
$worksheetNames = $objReader->listWorksheetNames($inputFileName);
//$worksheetNames = array("This","That","The Other");
foreach ($worksheetNames as $sheetName) {
	$sheetArray[] = $sheetName;
//    echo $sheetName, '<br />';
}
//print_pre($sheetArray);
$sheetIndex = 0;
do {
    try {
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
    } catch (PHPExcel_Exception $e) {
    }
    $currentSheetTitle = $sheetArray[$sheetIndex];
	echo $currentSheetTitle."<br>";
	
//		$objValidation = $objPHPExcel->getActiveSheet()->getDataValidationCollection("A2");
    try {
        $objValidation = $objPHPExcel->getActiveSheet();
    } catch (PHPExcel_Exception $e) {
    }
    //echo "A2 validation:".$objPHPExcel->getActiveSheet()->dataValidationExists("A2").EOL;
		
		//print_pre($objValidation);
	
	
	//print_pre($auditFieldsBySheet[$currentSheetTitle]);
	foreach ($auditFieldsBySheet[$currentSheetTitle] as $id=>$info){
		$cell= $info["cell"];
		$fieldName = $info["fieldName"];
		//echo $fieldName ."=".$objPHPExcel->getActiveSheet()->getCell($cell)->getCalculatedValue()."<bR>";
        try {
            $objPHPExcel->getActiveSheet()->getStyle($cell)->applyFromArray($highlighted);
        } catch (PHPExcel_Exception $e) {
        }

    }
	echo "<hr>";
	
	$sheetIndex++;
	$nextSheetTitle = $sheetArray[$sheetIndex];
} while ($nextSheetTitle != "Hidden After This Sheet");


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
echo $ReportsFileLink;

//  Get worksheet dimensions

?>