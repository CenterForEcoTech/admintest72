<?php
$htmlBodyTemplateIntro = "

<p>Thank you for scheduling your no-cost Mass Save&reg; Special Home Visit. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.</p>
<p><strong>Appointment Details: </strong>Site ID: ".$siteId."</p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong> (Your Energy Specialist will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>1 hour</strong> for the completion of the appointment.)</p>
<p><strong>Notes for Special Home Visit:</strong></p>
<ul>
	<li>For account verification and usage details, please have a current Berkshire Gas and electric bill available for your Energy Specialist.</li>
	<li>An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.</li>
	<li>Please clear obstacles to attic accesses, heating and hot water equipment.</li>
	<li>Please secure all pets.</li>
</ul>";

$htmlBodyTemplateMiddle = "<p><strong>What to Expect:</strong></p>
<p>At the conclusion of your Special Home Visit, the Energy Specialist will provide information regarding all applicable rebates and incentives.</p>
<p>During the SHV, energy saving products installed at no cost may include:</p>
<ul>
	<li>ENERGY STAR<sup>&reg;</sup>LED light bulbs</li>
	<li>Low-flow showerheads</li>
	<li>Faucet aerators</li>
	<li>Advanced power strips</li>
	<li>Programmable thermostats or discounted&nbsp;<a target='massSave' href='https://www.masssave.com/en/saving/residential-rebates/wireless-thermostat-installation-incentive/?rb=31458&amp;ca=20772594&amp;_o=31458&amp;_t=20772594&amp;ra=REPLACE_ME_WITH_YOUR_CACHE_BUSTING%27%20style'>wireless thermostats&nbsp;</a>(installed at no cost at a second appointment)</li>
</ul>";

$htmlBodyTemplateEnd = "
<p>&nbsp;</p>
<p>For questions regarding your scheduled Special Home Visit please call 1-800-944-3212.</p>
";

$textBodyTemplateIntro = 
"Thank you for scheduling your no-cost Mass Save Special Home Visit. You're on your way to energy efficiency, greater savings, and improved comfort.
\r\n\r\n
	Appointment Details: Site ID: ".$siteId."
\r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." (Your Energy Specialist will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 1 hour for the completion of the appointment.)
\r\n\r\n
	Notes for Special Home Visit:
\r\n\r\n
\r\n\r\n
	For account verification and usage details, please have a current Berkshire Gas and electric bill available for your Energy Specialist.
\r\n\r\n
	An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.
\r\n\r\n
	Please clear obstacles to attic accesses, heating and hot water equipment.
\r\n\r\n
	Please secure all pets.
\r\n\r\n";

$textBodyTemplateMiddle = "
\r\n\r\n
	What to Expect:
\r\n\r\n
	At the conclusion of your Special Home Visit, the Energy Specialist will provide information regarding all applicable rebates and incentives.
\r\n\r\n
	During the SHV, energy saving products installed at no cost may include:
\r\n\r\n
		ENERGY STAR<sup>&#174;</sup> LED light bulbs
\r\n\r\n
		Low-flow showerheads
\r\n\r\n
		Faucet aerators
\r\n\r\n
		Advanced power strips
\r\n\r\n
		Programmable thermostats or discounted wireless thermostats (installed at no cost at a second appointment)
\r\n\r\n";

$textBodyTemplateEnd = "
\r\n\r\n
	For questions regarding your scheduled Special Home Visit please call 1-800-944-3212.
";