<?php
$htmlBodyTemplateIntro = "
<p>Thank you for scheduling your no-cost Westfield Gas and Electric Energy Audit. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.</p>
<p><strong>Appointment Details: </strong>".str_replace("CustomerID","Customer ID: ",$siteId)."</p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong> (Your Energy Auditor will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>1 hour</strong> for the completion of the assessment.)</p>
<p><strong>Notes For Energy Audit:</strong></p>
<ul>
	<li>For account verification and usage details, please have a current Westfield Gas and Electric bill available for your Energy Auditor.</li>
	<li>An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.</li>
	<li>Please clear obstacles to attic accesses, heating and hot water equipment.</li>
	<li>Please secure all pets.</li>
</ul>
<p><strong>What to Expect:</strong></p>
<p>The Energy Auditor will provide a report within five business days detailing opportunities for all applicable rebates for:</p>
<ul>
	<li>Air sealing and Insulation</li>
	<li>High Efficient heating, cooling and water heating equipment</li>
	<li>Appliances</li>
	<li>Wireless Enabled Thermostats</li>
	<li>Pool Pumps</li>
</ul>
<p>For questions regarding your scheduled Energy Audit 1-844-403-7960.</p>
";

$textBodyTemplateIntro = "
Thank you for scheduling your no-cost Westfield Gas and Electric Energy Audit. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.
\r\n\r\n
	Appointment Details: Customer ID: ".$siteId."
r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." (Your Energy Auditor will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 1 hour for the completion of the assessment.)
\r\n\r\n
	Notes For Assessment:
\r\n\r\n
\r\n\r\n
	For account verification and usage details, please have a current Westfield Gas and Electric bill available for your Energy Auditor.
\r\n\r\n
	An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.
\r\n\r\n
	Please clear obstacles to attic accesses, heating and hot water equipment.
\r\n\r\n
	Please secure all pets.
\r\n\r\n
\r\n\r\n
	What to Expect:
\r\n\r\n
	The Energy Auditor will provide a report within five business days detailing opportunities for all applicable rebates for:
\r\n\r\n
	Air sealing and Insulation
\r\n\r\n
	High Efficient heating, cooling and water heating equipment
\r\n\r\n
	Appliances
\r\n\r\n
	Wireless Enabled Thermostats
\r\n\r\n
	Pool Pumps
\r\n\r\n
\r\n\r\n
	For questions regarding your scheduled Energy Audit 1-844-403-7960.
";