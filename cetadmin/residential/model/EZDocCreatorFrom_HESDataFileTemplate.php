<?php
$headerNames = array(); //clear out from last run
$headerNumbers = array(); //clear out from last run
$rowsRaw = array(); //clear out from last run
$rowsClean = array(); //clear out from last run

require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$ImportFileName = "HESDataFiles/".($documentTemplate ? : "EZDocTemplate.xlsm");
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."residential/" : "").$ImportFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader("Excel2007");
//	$objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$pathExtension = "HESDataFiles".$pathSeparator.$ReportsFile;
$webpathExtension = "HESDataFiles".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

$worksheetNames = $objReader->listWorksheetNames($inputFileName);
//$worksheetNames = array("Customer Info","Building Info");
foreach ($worksheetNames as $sheetName) {
	$sheetArray[] = $sheetName;
}
$sheetIndex = 0;
do {
    try {
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
    } catch (PHPExcel_Exception $e) {
    }
    $currentSheetTitle = $sheetArray[$sheetIndex];
	if ($cellData[$currentSheetTitle]){
		foreach ($cellData[$currentSheetTitle] as $cell=>$value){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $value);
            } catch (PHPExcel_Exception $e) {
            }
        }
	}
	$sheetIndex++;
	$nextSheetTitle = $sheetArray[$sheetIndex];
} while ($nextSheetTitle != "Attic");


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}

if (!$byPassLogin){
	$results->save = $saveLocation;
    try {
        $results->saveLog = $objWriter->save($saveLocation);
    } catch (PHPExcel_Writer_Exception $e) {
    }
    //echo $webtrackingLink."<Br>";
	$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
	$results->apiAccess = $apiAccess;
	if (!$apiAccess){
		echo $ReportsFileLink;
	}else{
		$results->fileUpdate = true;
		unlink("HESDataFiles/".$documentTemplate);
	}
}else{
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	// It will be called file.xls
	header('Content-Disposition: attachment; filename="'.$ReportsFile.'"');
	header("Cache-Control: max-age=0");
    try {
        $objWriter->save("php://output");
    } catch (PHPExcel_Writer_Exception $e) {
    }
}


?>