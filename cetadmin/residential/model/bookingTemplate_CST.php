<?php
$htmlBodyTemplateIntro = "

<p>Thank you for scheduling your no-cost Mass Save&reg; appointment.</p>
<p><strong>Appointment Details: </strong>Site ID: ".$siteId."</p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong> (Your Energy Specialist will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>1 hour</strong> for the completion of the appointment.)</p>
<p><strong>Notes For Appointment:</strong></p>
<ul>
	<li>An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.</li>
	<li>Please clear obstacles to attic accesses, heating and hot water equipment.</li>
	<li>Please secure all pets.</li>
</ul>
<p>&nbsp;</p>
<p>For questions regarding your scheduled appointment please call 1-800-944-3212.</p>
";

$textBodyTemplateIntro = 
"Thank you for scheduling your no-cost Mass Save appointment.
\r\n\r\n
	Appointment Details: Site ID: ".$siteId."
\r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." (Your Energy Specialist will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 1 hour for the completion of the appointment.)
\r\n\r\n
	Notes For Appointment:
\r\n\r\n
	An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment.
\r\n\r\n
	Please clear obstacles to attic accesses, heating and hot water equipment.
\r\n\r\n
	Please secure all pets.
\r\n\r\n
\r\n\r\n
	For questions regarding your scheduled appointment please call 1-800-944-3212.
";