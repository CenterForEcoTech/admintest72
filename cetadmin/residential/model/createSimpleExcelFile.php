<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once ($siteRoot.$adminFolder.'excelExportClasses/PHPExcel.php');

if ($energyName == "BGAS"){
	$inputFileName = "model/bgasExtract_TemplateV2.xlsx";

	//  Read your Excel workbook
	try {
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
	} catch (Exception $e) {
		die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
		. '": ' . $e->getMessage());
	}
	
}else{
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
}

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle($energyName." ".$energyType." ExtractFile Title")
							 ->setSubject($energyName." ".$energyType." ExtractFile Subject")
							 ->setDescription($energyName." ".$energyType." ExtractFile Description");
//START CurrentMonth
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

if ($energyName != "BGAS"){
	foreach ($energyHeaderFields as $cellId=>$dataDisplay){
		$cell = getNameFromNumber(($cellId+1))."1";
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
        //set formatting for numbers and dates
		//$objPHPExcel->getActiveSheet()->getStyle('K2:K1024')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
	}
}

foreach ($energyDataRows as $rowId=>$rowData){
	$rowId = ($rowId+2);
	foreach ($rowData as $cellId=>$dataDisplay){
		$cell = getNameFromNumber(($cellId+1)).$rowId;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
}

// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()
        ->setTitle($energyType);
} catch (PHPExcel_Exception $e) {
}
//END CurrentMonth

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$thisExcelFile = $energyName.$energyType."_".date("Ymt",strtotime($invoiceDate)).".xlsx";

$pathExtension = $energyName."Extracts".$pathSeparator.$thisExcelFile;
$webpathExtension = $energyName."Extracts".$webpathSeparator.$thisExcelFile;
$saveLocation = $path.$pathExtension;
if ($energyName != "BGAS"){
	$files_to_zip[] = $energyName."Extracts".$pathSeparator.$thisExcelFile;
}else{
	$bgasExportFile = $energyName."Extracts/".$thisExcelFile;
}
//echo "Saved in: ".$saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
$thisExcelFileLink = "<a id='allocationFile' href='".$webtrackingLink."'>".$thisExcelFile."</a><br>";
if ($energyName != "BGAS"){
	$exportFileLinks[] = $thisExcelFileLink;
}else{
	$bgasExportFileLink = $thisExcelFileLink;
}

?>