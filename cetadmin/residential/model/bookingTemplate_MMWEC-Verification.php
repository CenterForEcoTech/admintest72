<?php
$htmlBodyTemplateIntro = "
<p>Dear ".$customerName."</p>
<p>Thank you for scheduling your no-cost HELPS Verification Visit.</p>
<p><strong>Appointment Details: </strong></p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong></p>
<p>Your Energy Auditor will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>20 minutes</strong> for the completion of the verification visit.</p>
<p><strong>Notes For Verification Visit:</strong></p>
<ul>
	<li>Please have a copy of the paid contractor invoice(s), listing the energy efficiency work installed, available for review.
	<li>An adult over 18 years of age must be present
	
</ul>
<p><strong>What to Expect:</strong></p>
<ul>
	<li>The Energy Auditor will review work installed per the contractor invoice and they will provide a verification inspection sheet confirming the work installed.
</ul>
<p>For questions regarding your scheduled Verification Visit please call 1-888-333-7525.</p>
<p>Thank you!</p>
";

$textBodyTemplateIntro = "
Dear 	".$customerName."
\r\n\r\n
Thank you for scheduling your no-cost HELPS Verification Visit.
\r\n\r\n
	Appointment Details:
r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." 
\r\n\r\n
	Your Energy Auditor will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 20 minutes for the completion of the Verification Visit.
\r\n\r\n
	Notes For Verification Visit:
\r\n\r\n
\r\n\r\n
	-Invoice(s) of installed measure(s) to be verified\r\n
	-An adult over 18 years of age must be present\r\n
	-Please clear objstacles to any attic accesses, heating, and hot water equipment requiring verification\r\n
	-Please secure all pets\r\n
\r\n\r\n
\r\n\r\n
	What to Expect:
\r\n\r\n
\r\n\r\n
	-The Energy Auditor will review work installed per the contractor invoice and they will provide a verification inspection sheet confirming the work installed.
\r\n\r\n	
\r\n\r\n
	For questions regarding your scheduled Verification Visit please call 1-888-333-7525.
\r\n\r\n
Thank you!
";