<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once ($siteRoot.$adminFolder.'excelExportClasses/PHPExcel.php');
$templateFileName = $siteRoot.$adminFolder.'residential/model/'.$exceltemplateFile;
if (file_exists($fileName)){
	$message = "fileName Exisits";
	$inputFileName = $fileName;

	//  Read your Excel workbook
	try {
		//$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($inputFileName);
	} catch (Exception $e) {
		die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
		. '": ' . $e->getMessage());
	}
	$toDeleteFirstSheet = false;
}else{
	$message = "fileName Does Not Exisit";
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	$toDeleteFirstSheet = true;
}
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("Print Confirmation Title")
							 ->setSubject("Print Confirmation Subject")
							 ->setDescription("Print Confirmation Description");

try {
    $objPHPExcelTemplate = PHPExcel_IOFactory::load($templateFileName);
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $templateWorksheet = $objPHPExcelTemplate->getActiveSheet();
} catch (PHPExcel_Exception $e) {
}
$templateWorksheet->setTitle($customerName."-".date("g-i-s"));
try {
    $objPHPExcel->addExternalSheet($templateWorksheet);
} catch (PHPExcel_Exception $e) {
}

if ($toDeleteFirstSheet){
    try {
        $objPHPExcel->removeSheetByIndex(0);
    } catch (PHPExcel_Exception $e) {
    }
}

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$sheetCount = $objPHPExcel->getSheetCount();
try {
    $objPHPExcel->setActiveSheetIndex(($sheetCount - 1));
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B3", "Dear " . $customerName . ",");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E5", $siteIdDisplay);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B7", $customerName);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B9", $address);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B11", date("F d, Y", strtotime($apptDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D11", $apptStartTime);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G13", date("g:iA", strtotime($apptStartTime . " -15 minutes")) . " - " . date("g:iA", strtotime($apptStartTime . " +15 minutes")));
} catch (PHPExcel_Exception $e) {
}
if ($NotesForEnergyAudit){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B17", $NotesForEnergyAudit);
    } catch (PHPExcel_Exception $e) {
    }
}
if ($WhatToExpect){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B24", $WhatToExpect);
    } catch (PHPExcel_Exception $e) {
    }
}

$saveLocation = $fileName;

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$message .= " saved ".$saveLocation;
	
	
/*
*/
							 
// $objPHPExcel->getActiveSheet()->setCellValue($cell,$dataDisplay);
// Rename worksheet
//$objPHPExcel->getActiveSheet()->setTitle($energyType);
?>