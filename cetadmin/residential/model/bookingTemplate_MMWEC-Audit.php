<?php
//$NotesForEnergyAudit = $MMWECEnergyCompanyByName[$electricProvider]->Notes_For_Energy_Audit__c;
//$WhatToExpect = $MMWECEnergyCompanyByName[$electricProvider]->What_to_Expect__c;
$htmlBodyTemplateIntro = "
<p>Thank you for scheduling your no-cost HELPS Audit. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.</p>
<p><strong>Appointment Details: </strong></p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong></p>
<p>Your Energy Auditor will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>1 hour</strong> for the completion of the assessment.</p>
<p><strong>Notes For Energy Audit:</strong></p>
<ul>
	<li>For account verification and usage details, please have a current bill available for your Energy Specialist.</li>
	<li>An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment. Please clear obstacles to attic accesses, heating and hot water equipment.</li>
	<li>Please secure all pets.</li>
</ul>
<p><strong>What to Expect:</strong></p>
<p>The Energy Auditor will provide a report within three business days detailing opportunities for all recommendations for lighting, appliances, and weatherization improvements, including applicable appliance rebates.</p>

<p>For questions regarding your scheduled Energy Audit 1-888-333-7525.</p>
";

$textBodyTemplateIntro = "
Thank you for scheduling your no-cost HELPS Audit. You&rsquo;re on your way to energy efficiency, greater savings, and improved comfort.
\r\n\r\n
	Appointment Details:
r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." 
\r\n\r\n
	Your Energy Auditor will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 1 hour for the completion of the assessment.
\r\n\r\n
	Notes For Energy Audit:
\r\n\r\n
	For account verification and usage details, please have a current electric bill available for your Energy Specialist.
\r\n\r\n
	An adult over 18 years of age must be present, and it is recommended that a key decision maker be present for the assessment. Please clear obstacles to attic accesses, heating and hot water equipment.
\r\n\r\n
	Please secure all pets.
\r\n\r\n
\r\n\r\n
	What to Expect:
\r\n\r\n
	The Energy Auditor will provide a report within three business days detailing opportunities for all recommendations for lighting, appliances, and weatherization improvements, including applicable appliance rebates.
\r\n\r\n
\r\n\r\n
	For questions regarding your scheduled Energy Audit 1-888-333-7525.
";