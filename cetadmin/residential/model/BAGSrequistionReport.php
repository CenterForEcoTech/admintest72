<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = "model/bgasRequisition_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RequisitionFiles".$pathSeparator."RequisitionFiles_".date("y_m", strtotime($invoiceDate)).".xlsx";
$pathExtension = $ReportsFile;
$webpathExtension = $ReportsFile;

$saveLocation = $path.$pathExtension;
//RES-SF
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$rowId = 7;
ksort($reqData);
foreach ($reqData as $contractor=>$jobNameData){
	ksort($jobNameData);
	foreach ($jobNameData as $jobName=>$siteData){
		foreach ($siteData as $siteId=>$jobs){
			foreach ($jobs as $jobData){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $jobData["jobName"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $jobData["totalCost"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, "=B" . $rowId . "-D" . $rowId . ($jobData["preWeatherizationIncentive"] ? "-E" . $rowId : ""));
                } catch (PHPExcel_Exception $e) {
                } //$jobData["customerCost"]
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $jobData["bgasCost"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $jobData["preWeatherizationIncentive"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, (60 * $jobData["permit"]));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $jobData["installationContractor"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $jobData["deposit"]);
                } catch (PHPExcel_Exception $e) {
                }
                if ($jobData["totalToPay"] == "BothAirSealingAndInsulation"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=D" . ($rowId - 1) . "+D" . $rowId . "+E" . $rowId . "+F" . $rowId . "+I" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				if ($jobData["totalToPay"] == "JustInsulation"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=D" . $rowId . "+E" . $rowId . "+F" . $rowId . "+I" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				if ($jobData["totalToPay"] == "JustAirSealing"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, "=D" . $rowId . "+E" . $rowId . "+F" . $rowId . "+I" . $rowId);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				$rowId++;
			}//end foreach jobs
			
		}//end foreach siteData
	}//end foreach jobNameData
}// end foreach contractor

//recon sheet
try {
    $objPHPExcel->setActiveSheetIndex(6);
} catch (PHPExcel_Exception $e) {
}
$rowId = 2;
ksort($reconData);
foreach ($reconData as $contractor=>$jobNameData){
	ksort($jobNameData);
	foreach ($jobNameData as $jobName=>$siteData){
		$reconDisplay[]=$siteData;
	}
}	
$jobNameCount = count($reconJobCount);
if ($jobNameCount > 2){
	$rowShift = ($jobNameCount-2);
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(($rowId + 1), $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}

foreach ($reconDisplay as $count=>$siteData){
	foreach ($siteData as $siteId=>$jobData){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Berkshire Gas");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $jobData["firstName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $jobData["lastName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $jobData["asFinal"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $jobData["asBgas"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $jobData["insFinal"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $jobData["insBgas"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $jobData["preWeatherizationIncentive"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $jobData["installationContractor"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $jobData["deposit"]);
        } catch (PHPExcel_Exception $e) {
        }
        $rowId++;
		
	}//end foreach siteData
}//end foreach jobNameData


//echo $saveLocation."<Br>";
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>