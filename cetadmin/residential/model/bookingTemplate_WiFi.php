<?php
$file = "wififlier.pdf";
$htmlBodyTemplateIntro = "

<p>Thank you for scheduling your Mass Save&reg; Wireless Thermostat Installation.</p>
<p><strong>Appointment Details: </strong>Site ID: ".$siteId."</p>
<p><strong>".$customerName."</strong></p>
<p><strong>".$address."</strong></p>
<p><strong>".date("F d, Y",strtotime($apptDate))."</strong></p>
<p><strong>".$apptStartTime."</strong> (Your Energy Specialist will arrive between <strong>".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". </strong>Please allow <strong>1-2 hours</strong> for the completion of the installation.)</p>
<p><strong>Notes for the WiFi Installation:</strong></p>
<ul>
	<li>Please make sure you have your wireless account name and password for the technician.</li>
</ul>
<p>&nbsp;</p>
<p>For questions regarding your scheduled WiFi Thermostat Installation please call 1-800-944-3212.</p>
";

$textBodyTemplateIntro = 
"Thank you for scheduling your Mass Save Wireless Thermostat Installation.
\r\n\r\n
	Appointment Details: Site ID: ".$siteId."
\r\n\r\n
	".$customerName."
\r\n\r\n
	".$address."
\r\n\r\n
	".date("F d, Y",strtotime($apptDate))."
\r\n\r\n
	".$apptStartTime." (Your Energy Specialist will arrive between	".date("g:iA",strtotime($apptStartTime." -15 minutes"))." - ".date("g:iA",strtotime($apptStartTime." +15 minutes")).". Please allow 1-2 hours for the completion of the installation.)
\r\n\r\n
	Notes for the WiFi Installation:
\r\n\r\n
\r\n\r\n
	Please make sure you have your wireless account name and password for the technician.
\r\n\r\n
	For questions regarding your scheduled WiFi Thermostat Installation please call 1-800-944-3212.
";