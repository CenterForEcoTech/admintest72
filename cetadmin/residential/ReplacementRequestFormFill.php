<?php
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = $path;
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
set_time_limit (600);

include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$results = new stdClass();
require($thispath.'residential/pdfProcessor/fpdm.php');
			

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$results = $_GET;

		echo "this is get";
        break;

    case "POST":
		
		$action = $_GET['action'];
		if($action=="boiler"||$action=="furnace"){
			$newRecord = json_decode($currentRequest->rawData);
			$newRecord = (array)$newRecord;
			$recordType = "ERROR NO RECORD TYPE";
			if( $action == "furnace"){
				$pdf = new FPDM($thispath.'residential/model/processedEFR2.pdf');
				$recordType = "EFR_FORM";
			}else if ($action == "boiler"){
				$pdf = new FPDM($thispath.'residential/model/processedEBR6.pdf');
				$recordType = "EBR_FORM";
			}
			$now = time();
			$filePath = $thispath."residential/pdfResults/";
			$fileName = str_replace(" ","",$newRecord["accHolderName"]).$now.".pdf";
			//print_pre( $newRecord);
			unset($newRecord['action']);//remove this later
			unset($newRecord["uploadedPhotos"]);
			$attachedFiles = explode("|", $newRecord["fileLocations"]);
			unset($newRecord["fileLocations"]);
			//unset($newRecord['boilerType1']);
			//unset($newRecord['boilerType2']);
			$pdf->Load($newRecord, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
			$pdf->Merge();
			//$pdf->Output();
			$pdf->Output('f',$filePath.$fileName);
			$results = $CurrentServer.$adminFolder."residential/pdfResults/".$fileName;
			//file_put_contents("testingPDF.pdf", $pdfFile);
			
			require_once "Mail.php";  
			require_once "Mail/mime.php";  
			if($action == "boiler" || $action == "furnace" ){
				$from = "Center For EcoTechnology <no-reply@cetonline.org>"; 
				$username = "no-reply@cetonline.org"; 
				$password = "Dark76wall!12";  
				$to = ($newRecord["email"]!="" ? $newRecord["email"]."," : "").($newRecord["rebateEmail"]!="" ? $newRecord["rebateEmail"]."," : "")."Rebate@cetonline.org";
				$recipients = $to;
				$subject = "EBR/EFR MF ".$newRecord["accHolderName"];
				$body = 'Based on our inspection of your heating system we have determined that you qualify to participate in the Early Boiler/Furnace Replacement Rebate program. Attached please find the Early Boiler/Furnace Rebate form you\'ll submit within 60 calendar days of installation of your heating system, and within one year of the issuance of this form. Please review the instructions and "Terms & Conditions" on the form prior to installation of your new heating system.
					<br><br>
					If you have questions regarding the status of your rebate or whether specific equipment qualifies, please contact EFI at 1-800-232-0672.  For all other questions, please call 1-800-238-1221 ext.297.';
			}

			$host = "smtp.office365.com"; 
			$port = "587"; 
			
			$headers = array (
				'From' => $from,   
				'To' => $to, 
				'Reply-to' => $from,
				'Subject' => $subject
			); 		
			
			$mime = new Mail_mime("\n");

			$mime->setHTMLBody($body);
			
			$mime->addAttachment($filePath.$fileName);
			foreach ($attachedFiles as $fileLocation) {
				$mime->addAttachment($fileLocation);
			}
			$body = $mime->get();
			$hdrs = $mime->headers($headers);
			$smtp = Mail::factory(
				'smtp',   
				array (
					'host' => $host,     
					'port' => $port,     
					'auth' => true,     
					'username' => $username,     
					'password' => $password
				)
			);
			$mailsend = $smtp->send($recipients, $hdrs, $body);
			$insertResponse = $residentialProvider->pdfFormInsert($recipients,$recordType,$currentRequest->rawData,getAdminId());
			//$results = $insertResponse;
		}else if($action=="uploadPhotoAttachment"){
			$count = 0;
			foreach($_FILES as $fileInfo){
				$fileName = time()+$count.str_replace(")","",str_replace("(","",str_replace(" ","_",$fileInfo['name'])));
				$fileDestination = $thispath."residential/ebr-efr_Images/".$fileName;
				move_uploaded_file($fileInfo["tmp_name"], $fileDestination);
				$theseResults[] = $fileDestination;
				$count = $count +1;
			}
			$results = implode("|", $theseResults);
		}

		header("HTTP/1.0 201 Created");
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		if ($action == "newCell"){
			$addNewCell = $residentialProvider->insertResAuditCell($newRecord);
			$results->results = $addNewCell;
		}elseif ($action == "removeCell"){
			$removeCell = $residentialProvider->removeResAuditCell($newRecord);
			$results->results = $removeCell;
		}else{
			$results->data = "no action matched";
		}
			
		header("HTTP/1.0 201 Created");
        break;
}
output_json($results);
die();
?>