<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxProjectID" ).combobox({
				select: function(event, ui){
					window.location = "?nav=EZDocCreator&ProjectID="+$(ui.item).val();
				}
			});
		  });
		</script>	
		<div class="eight columns">
			<div>
				<label>You must indicate a ProjectID to retrieve: </label>
			  <select class="comboboxProjectID parameters" id="SelectedProjectID">
				<option value="">Select one...</option>
				<?php 
					foreach ($projectIds as $projectId){
						$projectIdParts = explode("_",$projectId);
						echo "<option value=\"".$projectIdParts[0]."\"".($ProjectID == $projectIdParts[0] ? " selected" : "").">".$projectId."</option>";
					}
				?>
			  </select>
			</div>
		</div>