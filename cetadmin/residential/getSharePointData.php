<?php
ini_set('memory_limit', '1200M');
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = "";
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}

include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
$newRecord = json_decode($currentRequest->rawData);

include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$results = new stdClass();

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
$cleanDataRequest = $newRecord->cleanDataRequest;
include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
$results->executionTime = $execution_time;
$results->newRecord = $newRecord;		

$resultsFound = false;
$resultObj = new stdClass();
if ($newRecord->siteId && $newRecord->projectId){
	foreach ($Views as $ID=>$GUI){
		$ArrayName = "SPRecord".$ID;
		//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
		$thisFileContents = null;
		$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
		${$ArrayName} = json_decode($thisFileContents,true);
		$thisFileContents = null;
		foreach (${$ArrayName} as $SPData){
			$spData = (object)$SPData;
			//$results[] = $cetDataProvider->insertSharePointData($spData);
			$siteId = $spData->site_x0020_id;
			$projectId = $spData->project_x0020_id;
			if ($projectId == $newRecord->projectId && $siteId == $newRecord->siteId){
				$resultObj = $spData;
				$resultsFound = true;
			}
			erase_val($SPData);
			$SPData = null;
		}//end foreach sprecord
		${$ArrayName} = null;
	}//end foreach view
}//end if siteID and ProjectId

if ($resultsFound){
	$docId = $newRecord->docId;
	$documentTemplateFile = $docId.'.xlsm';
	$documentTemplate = $docId.'.xlsx';
	
	rename("HESDataFiles/".$documentTemplateFile,"HESDataFiles/".$documentTemplate);
	//rename file since it can only handle xlsx files
	
	$results->file = $docId;
	$sharepointRecord = (array)$resultObj;
	$results->resultArray = $sharepointRecord;
$sharepointRecord["intakeauditstartdatetime"] = $sharepointRecord["hea_x0020_date"];

	include_once('views/_EZDocCreator_CellDataCustomerInfo.php');//standard items	
	
	$cellData["Customer Info"]["S8"] = $sharepointRecord["contractor"];
	$cellData["Customer Info"]["S20"] = $sharepointRecord["deposit_x0020_amount"];
	

	$apiAccess = true;
	$ReportsFile = $documentTemplateFile;
	$results->reportFile = $documentTemplate;
	include('model/EZDocCreatorFrom_HESDataFileTemplate.php');
}



echo json_encode($results);
?>