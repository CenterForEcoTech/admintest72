<?php
include_once("../_config.php");
	$fileName = $_GET["fileName"];
	$fileUrl = $CurrentServer.$adminFolder."residential/EversourceExtracts/".$fileName;
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($fileUrl));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fileUrl));
    readfile($fileUrl);
    exit;
?>
