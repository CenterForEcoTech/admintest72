<?php
error_reporting(0);
    $formattedAddrFrom = urlencode(str_replace("#","Apt",$_GET['priorAddress']));
    $formattedAddrTo = urlencode(str_replace("#","Apt",$_GET['currentAddress']));
	if ($_GET['priorAddress']){
		$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$formattedAddrFrom.'&destinations='.$formattedAddrTo.'&units=imperial&key=AIzaSyCsqwNKCPvAm4_QI4TuNUwANhl4VbRhGQ8';
		//echo $url."<br>";
		$googleDrivingMatrix = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$formattedAddrFrom.'&destinations='.$formattedAddrTo.'&units=imperial&key=AIzaSyCsqwNKCPvAm4_QI4TuNUwANhl4VbRhGQ8');
		$output = json_decode($googleDrivingMatrix);
		//print_r($output);
		$distance = $output->rows[0]->elements[0]->distance->text;
		
		$milage = (strpos($distance,"mi") ? trim(str_replace(" mi","",$distance)) : 0);
		$results["distance"] = $distance;
		$distanceAlert = ($milage > 30 ? "<span style='color:red;font-weight:bold;'>" : "")."Distance from Prior Apt: ".$distance.($milage > 30 ? "</span>" : "")."<br>";
		$results["alerts"] = false;
		If ($milage > 30){
			$results["alerts"] = true;
		}
		
		$results["distanceAlert"] = $distanceAlert;
		echo json_encode($results);
	}
?>