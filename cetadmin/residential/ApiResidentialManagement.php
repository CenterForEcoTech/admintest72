<?php
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = "";
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}


set_time_limit (600);

//used for updating HES data
include_once($siteRoot.$adminFolder.'/sharepoint/SharePointAPI.php');
use Thybag\SharePointAPI;


include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");

require_once '../gbs/salesforce/config.php';
include_once('../gbs/salesforce/soap_connect.php');
//get the Energy company from Salesforce
$query = "SELECT Id, Name, Notes_For_Energy_Audit__c, What_to_Expect__c FROM MMWEC_Energy_Company__c";
//echo $query;
$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
//print_pre($response);
foreach ($response->records as $record){
	$MMWECEnergyCompanyByName[$record->Name] = $record;
}

$currentRequest = new Request();
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$results = $_GET;
		echo "this is get";
        break;
    case "POST":
		
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		if ($action == "addBlock"){
			$record = new stdClass();
			$record->es = $newRecord->es;
			$record->startTimeStamp = date("Y-m-d H:i:s",strtotime($newRecord->startTimeStamp));
			$record->endTimeStamp = date("Y-m-d H:i:s",strtotime($newRecord->endTimeStamp));
			$record->reason = $newRecord->reason;
			$record->note = $newRecord->note;
			$addBlock = $residentialProvider->addScheduleBlock($record,getAdminId());
			$insertedId = $addBlock->insertedId;

			
			$scheduledData = new stdClass();
			$scheduledData->energySpecialist = $newRecord->es;
			$scheduledData->apptDate = date("Y-m-d H:i:s",strtotime($newRecord->startTimeStamp));
			$scheduledData->endDate = date("Y-m-d H:i:s",strtotime($newRecord->endTimeStamp));
			$scheduledData->siteId = "BlockedID".$insertedId;
			$scheduledData->projectId = "BlockedID".$insertedId;
			$scheduledData->electricProvider = "BLOCKED";

			$scheduledData->customerName = $newRecord->reason;
			$scheduledData->email = $newRecord->note;
			$scheduledData->address = NULL;
			$scheduledData->visitType = "BLOCKED";
			$scheduledData->status = "";
			//print_pre($scheduledData);
			$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
			$results = $addBlock;
		}
		if ($action == "getBlocked"){
			$criteria = new stdClass();
			$getBlocked = $residentialProvider->getScheduleBlocked($criteria);
			foreach ($getBlocked->collection as $record){
				$getBlock[strtotime($record->ResidentialScheduledBlocked_StartTimeStamp).$record->ResidentialScheduledBlocked_ID]["es"] = $record->ResidentialScheduledBlocked_ES;
				$getBlock[strtotime($record->ResidentialScheduledBlocked_StartTimeStamp).$record->ResidentialScheduledBlocked_ID]["startTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_StartTimeStamp));
				$getBlock[strtotime($record->ResidentialScheduledBlocked_StartTimeStamp).$record->ResidentialScheduledBlocked_ID]["endTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_EndTimeStamp));
				$getBlock[strtotime($record->ResidentialScheduledBlocked_StartTimeStamp).$record->ResidentialScheduledBlocked_ID]["reason"] = $record->ResidentialScheduledBlocked_Reason;
				$blockedHours .= $record->ResidentialScheduledBlocked_ES." ".$record->ResidentialScheduledBlocked_Reason." [".date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_StartTimeStamp))." - ".date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_EndTimeStamp))."]<br>";
			}

			$results = $blockedHours;
		}
		if ($action == "removeBlock"){
			$blockedId = str_replace("BlockedID","",$newRecord->blockedId);
			$results->scheduled = $residentialProvider->cancelApt($newRecord->scheduledId);
			$results->blocked = $residentialProvider->cancelBlocked($blockedId);
		}
		
		if ($action == "addAvailability"){
			$record = new stdClass();
			$record->es = $newRecord->es;
			$record->days = $newRecord->days;
			$record->adate = $newRecord->adate;
			$record->startTime = $newRecord->startTime;
			$record->endTime = $newRecord->endTime;
			$record->totalHours = $newRecord->totalHours;
			$record->types = $newRecord->types;
			$addAvailability = $residentialProvider->addAvailability($record,getAdminId());
			$results = $addAvailability;
		}
		if ($action == "availabilityStatus"){
			$record = new stdClass();
			$record->status = $newRecord->status;
			$record->id = $newRecord->id;
			$results = $residentialProvider->availabilityStatus($newRecord->status,$newRecord->id);
		}		
		if ($action == "updateAvailability"){
			$record = new stdClass();
			$record->es = $newRecord->es;
			$record->days = $newRecord->days;
			$record->adate = $newRecord->adate;
			$record->startTime = $newRecord->startTime;
			$record->endTime = $newRecord->endTime;
			$record->totalHours = $newRecord->totalHours;
			$record->types = $newRecord->types;
			$record->status = $newRecord->status;
			$record->id = $newRecord->id;
			$updateAvailability = $residentialProvider->updateAvailability($record,getAdminId());
			$results = $updateAvailability;
		}
		
		
		if ($action == "checkoutEZDoc"){
			$checkOutEZDoc = $residentialProvider->checkOutEZDoc($newRecord->docId,$newRecord->checkedOutBy,$newRecord->docType);
			$results = $checkOutEZDoc;
		}
		if ($action == "removeBgasExtractDate"){
			$results = $residentialProvider->removeBgasExtractedDate($newRecord->docId);
		}
		if ($action == "removeDoerExtractDate"){
			$results = $residentialProvider->removeDoerExtractedDate($newRecord->docId);
		}
		if ($action == "ismExtract"){
			$records[] = $newRecord->docId;
			$ismExtract = $residentialProvider->ismExtract($records);
			$results = $ismExtract;
		}
		if ($action == "ismExtractAll"){
			$ismExtract = $residentialProvider->ismExtract($newRecord->docIds);
			if (count($newRecord->noElectricIsms)){
				foreach ($newRecord->noElectricIsms as $docId){
					$ismNoElectricExtract[] = $residentialProvider->ismNoElectricSet($docId);
				}
			}
			//also mark the piggyBacks
			$piggyBacks = $newRecord->piggyBacks;
			//$piggyBacksData = $piggyBacks["piggyBackData"];
			if (count($piggyBacks->piggyBackData)){
				foreach ($piggyBacks->piggyBackData as $siteId=>$info){
					$addedResult = $residentialProvider->addPiggyBack($siteId,$info->thisDate,$info->contract);
					//$addedResults[] = $addedResult;
					//print_pre($addedResults);
				}

			}
			//$results = $addedResults;
			$results = $ismExtract;
		}
		if ($action == "mergeRecords"){
			$mergeRecords = $residentialProvider->mergeRecords($newRecord);
			$results = $mergeRecords;
		}
		if ($action == "bgasExtractAll"){
			$ismDocIds = $newRecord->ISMsDocIds;
			$measureDocIds = $newRecord->MeasureDocIds;
			if (count($ismDocIds)){
				$bgasISMExtract = $residentialProvider->bgasISMExtract($ismDocIds);
				$results->isms = $bgasISMExtract;
			}
			if (count($measureDocIds)){
				$measureISMExtract = $residentialProvider->bgasMeasureExtract($measureDocIds);
				$results->measures = $measureISMExtract;
			}
		}
		if ($action == "bgasFlagged"){
			$bgasFlagged = $residentialProvider->bgasFlagged($newRecord->docId);
			$results = $bgasFlagged;
			
		}
		
		if ($action == "doerFlagged"){
			$doerFlagged = $residentialProvider->doerFlagged($newRecord->docId);
			$results = $doerFlagged;
		}
		if ($action == "cancelApt"){
			$results = $residentialProvider->cancelApt($newRecord->apptId);
		}
		if ($action == "confirmApt"){
			$isReminder = false;
			$confirmed = $residentialProvider->confirmApt($newRecord->apptId);
			
			$customerName = ucwords(strtolower($newRecord->customerName));
			$apptType = $newRecord->apptType;
			$ES = $newRecord->ES;
			$siteId = $newRecord->siteId;
			$siteIdDisplay = "Site ID: ".$newRecord->siteId;
			$apptDate = $newRecord->apptDate;
			$apptStartTime = $newRecord->apptStartTime;
			$electricProvider = $newRecord->electricProvider;
			$address = str_replace(" Ma "," MA ",ucwords(strtolower($newRecord->address)));
			
			$subjectTitle = $apptType;
			$logoImage = "BGAS";
			$NotesForEnergyAudit = "";
			$WhatToExpect = "";
			
			switch ($apptType){
				case "Combustion Safety Revisit":
					$templateFile = "bookingTemplate_CST.php";
					$exceltemplateFile = "bookingTemplate_CST.xlsx";
					break;
				case "HEA":
				case "HEAAU":
					$templateFile = "bookingTemplate_HEA.php";
					$exceltemplateFile = "bookingTemplate_HEA.xlsx";
					$subjectTitle = "Energy Assesment";
					break;
				case "Inspection":
					$templateFile = "bookingTemplate_CST.php";
					$exceltemplateFile = "bookingTemplate_CST.xlsx";
					break;
				case "SHV":
					$templateFile = "bookingTemplate_SHV.php";
					$exceltemplateFile = "bookingTemplate_SHV.xlsx";
					$subjectTitle = "Special Home Visit";
					break;
				case "WGE":
					$siteIdDisplay = str_replace("CustomerID","Customer ID: ",$siteId);
					$templateFile = "bookingTemplate_WGE.php";
					$exceltemplateFile = "bookingTemplate_WGE.xlsx";
					$subjectTitle = "WGE Energy Audit";
					$logoImage = "WGE";
					break;
				case "WiFi":
					$templateFile = "bookingTemplate_WiFi.php";
					$exceltemplateFile = "bookingTemplate_WiFi.xlsx";
					$subjectTitle = "Wireless Thermostat";
					break;
				case "MMWEC-Audit":
					//uses require salesforce files at top of this page
					$NotesForEnergyAudit = $MMWECEnergyCompanyByName[$electricProvider]->Notes_For_Energy_Audit__c;
					$WhatToExpect = $MMWECEnergyCompanyByName[$electricProvider]->What_to_Expect__c;
					
					$templateFile = "bookingTemplate_MMWEC-Audit.php";
					$exceltemplateFile = "bookingTemplate_MMWEC-Audit.xlsx";
					$subjectTitle = "Energy Audit";
					break;
				case "MMWEC-Verification Inspection":
				case "MMWEC-Verification Visit":
					//uses require salesforce files at top of this page
					$templateFile = "bookingTemplate_MMWEC-Verification.php";
					$exceltemplateFile = "bookingTemplate_MMWEC-Verification.xlsx";
					$subjectTitle = "Verification Visit";
					break;
				default:
					$exceltemplateFile = "bookingTemplate_HEA.xlsx";
					$templateFile = "bookingTemplate_Generic.php";
					break;
			}
			include('model/'.$templateFile);
			
			$htmlBodyTemplate = $htmlBodyTemplateIntro.(!$isReminder ? $htmlBodyTemplateMiddle : "").$htmlBodyTemplateEnd;
			$textBodyTemplate = $textBodyTemplateIntro.(!$isReminder ? $textBodyTemplateMiddle : "").$textBodyTemplateEnd;

			
			$htmlBody = $htmlBodyTemplate;
			$txtBody = $textBodyTemplate;
			$subject = (!$isReminder ? "Confirmed: " : "Reminder: ").$subjectTitle." visit on ".$newRecord->apptDate." with ".$newRecord->ES;
			
			$fileName = $siteRoot.$adminFolder.'residential/PrintConfirmations/printConfirmations_'.date("Y-m-d").'.xlsx';
			
			if ($newRecord->email && $confirmed->success){
				//send emails if there are some to send
				require_once "Mail.php";  
				require_once "Mail/mime.php";  
				/*
				$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">";
				$username = $_SESSION['AdminEmail'];
				$password = $_SESSION['AdminAuthenticationCode'];
				*/
				$from = "Center For EcoTechnology <no-reply@cetonline.org>"; 
				$username = "no-reply@cetonline.org"; 
				$password = "Dark76wall!12";  
				
				$host = "smtp.office365.com"; 
				$port = "587"; 

				$to = $newRecord->customerName." <".$newRecord->email.">";
				if ($testingServer){
					$to = "Yosh Schulman <yschulman@gmail.com>";
				}
				$bcc = ",Kacie Dean <Kacie.Dean@cetonline.org>";
				$recipients = $to.$bcc;

				$crlf = "\r\n";
				
				$headers = array (
					'From' => $from,
					'To' => $to, 
					'Reply-to' => "Kacie Dean <Kacie.Dean@cetonline.org>", 
					'Subject' => $subject
				); 
				$mime = new Mail_mime($crlf);
				$mime->setTXTBody($txtBody);
				$mime->setHTMLBody($htmlBody);
				$body = $mime->get();
				$headers = $mime->headers($headers);
						
				$smtp = Mail::factory(
					'smtp',   
					array (
						'host' => $host,     
						'port' => $port,     
						'auth' => true,     
						'username' => $username,     
						'password' => $password
					)
				);  
				$mail = $smtp->send($recipients, $headers, $body);
				if ($mail){ 	
					$message = " and Email Sent";
				}else{
					$message = print_r($mail,true);
				}
				$printMessage = "";
			}else{
				include($path.$thispath.'/residential/model/createPrintConfirmation.php');
				//$message = ' and printed';
			}
			$results = $message."_".$newRecord->apptId;
		}
		if ($action == "confirmAptNoMessage"){
			$isReminder = false;
			$confirmed = $residentialProvider->confirmApt($newRecord->apptId);
			$message = "";
			if (!$confirmed->success){
				$message = " Error";
			}
			$results = $message."_".$newRecord->apptId;
		}
		if ($action == "sendPrintFile"){
			$fileName = $siteRoot.$adminFolder.'residential/PrintConfirmations/printConfirmations_'.date("Y-m-d").'.xlsx';
			if (file_exists($fileName)){
				include($siteRoot.$adminFolder.'residential/model/printConfirmation.php');
				$results = $message;
			}else{
				$results = "not found: ".$fileName;
			}
		}
		if ($action == "contractorFlagged"){
			$contractorFlagged = $residentialProvider->contractorFlagged($newRecord->docId);
			$results = $contractorFlagged;
		}
		if ($action == "markEFIExtractDate"){
			$docIds = $newRecord->docIds;
			$markEFIExtractDate = $residentialProvider->markEFIExtractDate($docIds);
			
			$results = $markEFIExtractDate;
		}
		if ($action == "lead_notInterested"){
			$notInterested = $residentialProvider->leadNotInterested($newRecord->id);
			$results = $notInterested;
		}
		if ($action == "scheduleAppt"){
			//concatinate start and end times
			$thisStartStr = $newRecord->startTime;
			$thisStartDate = date("Y-m-d",$thisStartStr);
			$thisStartTime = $newRecord->startTimePicker;
			$newStartDateTime = $thisStartDate." ".$thisStartTime;
			$newStartDateTimeStr = strtotime($newStartDateTime);

			$thisEndStr = $newRecord->endTime;
			$thisEndDate = date("Y-m-d",$thisEndStr);
			$thisEndTime = $newRecord->endTimePicker;
			$newEndDateTime = $thisEndDate." ".$thisEndTime;
			$newEndDateTimeStr = strtotime($newEndDateTime);
			
			$clientParts = explode(" - ",$newRecord->client);
			
			//prep object for adding to dashboard
			$scheduledData = new stdClass();
			
			$apptType = $newRecord->apptType;
			if ($apptType == "HES"){
				//set dashboard elements
				$scheduledData->siteId = $newRecord->siteId;
				$scheduledData->projectId = $newRecord->projectId;
				$scheduledData->hesId = $newRecord->id;
				$scheduledData->electricProvider = $newRecord->electricProvider;
				$scheduledData->visitType = $newRecord->MMWECAuditType;

				$Config_SharepointPWEscaped = "CETWanu1375";
                try {
                    $sp = new SharePointAPI('it_consultant@cetonline.org', $Config_SharepointPWEscaped, $siteRoot . $adminFolder . 'sharepoint/_wsdl.xml', 'SPONLINE');
                } catch (Exception $e) {
                }
                $spoId = $newRecord->id;
				$spoStart = gmdate("Y-m-d\TH:i:s",$newStartDateTimeStr).'Z';
				$spoEnd = gmdate("Y-m-d\TH:i:s",$newEndDateTimeStr).'Z';
				$sp->update('HES Customer Data',$spoId, array('IntakeAuditType'=>$newRecord->MMWECAuditType,'Energy_x0020_Specialist'=>$newRecord->es,'IntakeAuditStartDateTime'=>$spoStart,'IntakeAuditEndDateTime'=>$spoEnd));
				
			}
			if ($apptType == "Travel"){
				//set dashboard elements
				$scheduledData->siteId = "";
				$scheduledData->projectId = "";
				$scheduledData->electricProvider = "";
				$scheduledData->visitType = "Travel";
				$clientParts[1] = $newRecord->electricProvider;
				$clientParts[0] = "";
			}
			
			if ($apptType == "WGE"){
				//set dashboard elements
				$scheduledData->siteId = "CustomerID".$newRecord->id;
				$scheduledData->projectId = "CustomerID".$newRecord->id;
				$scheduledData->electricProvider = "WGE";
				$scheduledData->visitType = "WGE";
				
				//update Muni record
				include_once($dbProviderFolder."MuniProvider.php");
				$muniProvider = new MuniProvider($dataConn);
				$updateRecord = new stdClass();
				$updateRecord->customerId = $newRecord->id;
				$updateRecord->auditor = $newRecord->es;
				$updateRecord->scheduledDate = date("m/d/Y",$newRecord->startTime);
				$updateRecord->scheduledTime = date("g:ia",strtotime($newRecord->startTimePicker));
				$response = $muniProvider->updateAuditor($updateRecord,getAdminId());
				$results = $updateRecord;
				
			}
			
			if ($apptType == "MMWEC"){
				//set dashboard elements
				$scheduledData->siteId = "SalesforceID".$newRecord->id;
				$scheduledData->projectId = "SalesforceID".$newRecord->id;
				$scheduledData->electricProvider = $newRecord->electricProvider;
				$scheduledData->visitType = "MMWEC-".$newRecord->MMWECAuditType;
				
				//uses require salesforce files at top of this page to update Salesforce data
				//update salesforce record
				$sObject1 = new stdclass();
				$sObject1->Id = $newRecord->id;
				$sObject1->MMWEC_Auditor__c = $newRecord->esMMWECID;
				$sObject1->Audit_Date_Start__c = gmdate("Y-m-d\TH:i:s",$newStartDateTimeStr).'.000Z';
				$sObject1->Audit_Date_Stop__c = gmdate("Y-m-d\TH:i:s",$newEndDateTimeStr).'.000Z';
				$sObject1->StageName = 'Audit Scheduled';
				$sObject1->Audit_Type_Text__c = $newRecord->MMWECAuditType;
				$response = $mySforceConnection->update(array ($sObject1), 'Opportunity');
				$results = $response;
			}
			

			//now insert into dashboard database
			$scheduledData->apptDate = date("Y-m-d H:i:s",$newStartDateTimeStr);
			$scheduledData->endDate = date("Y-m-d H:i:s",$newEndDateTimeStr);

			$scheduledData->energySpecialist = $newRecord->es;
			$scheduledData->customerName = $clientParts[0];
			$scheduledData->address = $clientParts[1];
			$scheduledData->status = "";
			$scheduledData->email = $newRecord->email;
			$results = $scheduledData;
			//print_pre($scheduledData);
			$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
			
			//if travel then update record with siteId so that it does not get overwritten
			if ($apptType == "Travel"){
				$recordId = $insertScheduleResults->insertedId;
				$updateResults = $residentialProvider->updateTravelScheduleData($recordId);
			}
		}
		if ($action == "deleteSchedule"){
			//concatinate start and end times
			$thisApptId = $newRecord->apptId;
			$thisSiteId = strtolower($newRecord->siteId);
			$thisHesId = $newRecord->hesId;
			
			$apptType = "HES"; //default
			if (strpos(" ".$thisSiteId,"customer")){$apptType = "WGE";}
			if (strpos(" ".$thisSiteId,"salesforce")){$apptType = "MMWEC";}
			if (strpos(" ".$thisSiteId,"travel")){$apptType = "travel";}
			
			if ($apptType == "HES"){
				//set dashboard elements
				$Config_SharepointPWEscaped = "CETWanu1375";
                try {
                    $sp = new SharePointAPI('it_consultant@cetonline.org', $Config_SharepointPWEscaped, $siteRoot . $adminFolder . 'sharepoint/_wsdl.xml', 'SPONLINE');
                } catch (Exception $e) {
                }
                $spoId = $thisHesId;
				$sp->update('HES Customer Data',$spoId, array('Energy_x0020_Specialist'=>'','IntakeAuditStartDateTime'=>'','IntakeAuditEndDateTime'=>''));
				
			}
			
			if ($apptType == "Travel"){
				//nothing special needed
			}
			
			if ($apptType == "WGE"){
				//update Muni record
				include_once($dbProviderFolder."MuniProvider.php");
				$muniProvider = new MuniProvider($dataConn);
				$updateRecord = new stdClass();
				$updateRecord->customerId = str_replace("customerid","",$thisSiteId);
				$updateRecord->auditor = NULL;
				$updateRecord->scheduledDate = "0000-00-00";
				$updateRecord->scheduledTime = NULL;
				$response = $muniProvider->updateAuditor($updateRecord,getAdminId());
				$results = $response;
				
			}
			
			if ($apptType == "MMWEC"){
				//set dashboard elements
				$salesforceId = str_replace("SalesforceID","",$newRecord->siteId);
				//uses require salesforce files at top of this page to update Salesforce data
				//update salesforce record
				//first cancel that apt to reset the values, then reset to audit ready to be scheduled
				$sObject1 = new stdclass();
				$sObject1->Id = $salesforceId;
				$sObject1->StageName = 'Cancelled';
				$response = $mySforceConnection->update(array ($sObject1), 'Opportunity');
				
				$sObject2 = new stdclass();
				$sObject2->Id = $salesforceId;
				$sObject2->StageName = 'Audit Needs to be Scheduled';
				$response = $mySforceConnection->update(array ($sObject2), 'Opportunity');
				$results = $response;
			}
			

			//now update dashboard database
			$deleteScheduleResults = $residentialProvider->cancelApt($thisApptId);
			
			//$results = $deleteScheduleResults;
		}
		header("HTTP/1.0 201 Created");
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		$results->results = true;
		if ($action == "newCell"){
			$addNewCell = $residentialProvider->insertResAuditCell($newRecord);
			$results->results = $addNewCell;
		}elseif ($action == "removeCell"){
			$removeCell = $residentialProvider->removeResAuditCell($newRecord);
			$results->results = $removeCell;
		}else{
			$results->data = "no action matched";
		}
		header("HTTP/1.0 201 Created");
        break;
}
output_json($results);
die();
?>