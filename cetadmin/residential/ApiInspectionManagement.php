<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$results = $_GET;
		echo "this is get";
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		$results = $newRecord;
		header("HTTP/1.0 201 Created");
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		$siteId = $newRecord->siteId;
		$status = $newRecord->status;
		$refusedInspection = $CSGDataProvider->insertInspectionRefused($siteId,$status,getAdminId());
		$results = $refusedInspection;
		header("HTTP/1.0 201 Created");
        break;
}
output_json($results);
die();
?>