<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Residential)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "130";
	$invoicingToolsDropDownItemsTop = "130";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>hours?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Admin (moved to Hours tab)</a></li>
<!--			<li><a id="ebrContract" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=ebrContract" class="button-link">Early Boiler/Furnace Replacement Contract</a></li>
-->		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GHS_Inspection)) || $ThisMenuSuperAdmin){?>
<!--		
	<li><a id="inspection-schedule" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=inspection-schedule" class="button-link">Inspection Schedule Report</a></li>
			<li><a id="report-ClosureRate" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=report-ClosureRate" class="button-link">Closure Rates</a></li>
-->			
			<li><a id="report-BGASRequisite" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=report-BGASRequisite" class="button-link">BGAS Requisition File</a></li>
			<li><a id="report-BGASExtractProcessor" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=report-BGASExtractProcessor" class="button-link">BGAS Extract File Processor</a></li>
<!--			<li><a id="BilledLastMonth" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=BilledLastMonth" class="button-link">Billed Last Month</a></li>
-->			<li><a id="EFIExtractor" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=EFIExtractor" class="button-link">EFI Extract File Generator</a></li>
<!--			<li><a id="SolarAccessConverter" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=SolarAccessConverter" class="button-link">Solar Access Converter</a></li>
-->		<?php }?>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_ResidentialHESStats))){?>
<!--			<li><a id="HESStats" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=HESStats" class="button-link">HES Stats File Generator</a></li>
-->		<?php }?>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Residential))){?>
			<li><a id="ESScheduler" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=ESScheduler" class="button-link">ES Scheduler</a></li>
			<li><a id="EZDocTemplate" href="<?php echo $CurrentServer.$adminFolder;?>residential/HESDataFiles/EZDocTemplate.xlsm" class="button-link">EZ-Doc Template</a></li>
			<li><a id="EZDocCreator" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=EZDocCreator" class="button-link">EZ-Doc Creator</a></li>
			<li><a id="HES-DataImporter" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=HES-DataImporter" class="button-link">HES Data Importer (EZ-Doc Import)</a></li>
			<li><a id="HES-DataReviewer" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=HES-DataReviewer" class="button-link">HES Data Reviewer (EZ-Doc Review)</a></li>
	<!--		<li><a id="Leads" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=Leads" class="button-link">Leads Importer (From EnergySavvy Survey)</a></li>
	-->	<?php }?>
		
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>