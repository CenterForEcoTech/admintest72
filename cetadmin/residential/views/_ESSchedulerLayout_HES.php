<?php
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = "";
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

//Get the cancelled items and remove them first
include_once($thispath.$path.'sharepoint/SharePointAPIFiles_ToBeScheduled.php');
if ($SharepointArray["warning"] != "No data returned."){
	//echo count($SharepointArray). " HES records<br>"; 
	foreach ($SharepointArray as $id=>$record){
		//print_pre($record);
		$id = $record["id"];
		$siteId = $record["site_x0020_id"];
		$projectId = $record["project_x0020_id"];
		$firstName = $record["first_x0020_name"];
		$lastName = $record["last_x0020_name"];
		$phone = $record["phone_x0020_number"];
		$visitType = $record["intakeaudittype"];
		$ToBeScheduled["HES"][]= array("value"=>$id,"label"=>$firstName."  ".$lastName." - ".$visitType." ".$record["street_x0020_address"]." ".$record["town"],"siteId"=>$siteId,"projectId"=>$projectId,"email"=>$record["customer_x0020_e_x002d_mail"],"phone"=>$phone,"electricProvider"=>$record["intakesiteinfoelectric"],"mmwecAuditType"=>$visitType,"source"=>"HES");
	}
}
?>