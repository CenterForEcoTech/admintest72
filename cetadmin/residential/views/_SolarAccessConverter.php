<?php
ini_set('memory_limit', '7048M');
//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();

if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

/*
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$auditFieldResults = $residentialProvider->getResAuditFields();
$auditFieldCollection = $auditFieldResults->collection;
//Get list of active warehouse locations
foreach ($auditFieldCollection as $auditField){
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["cell"] = $auditField->cell;
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["fieldName"] = $auditField->fieldName;
}
//print_pre($auditFieldsBySheet);
*/
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
.unmatched {background-color:yellow;border:1pt solid black;}
</style>
<h1>Solar Access Data Converter</h1>
<?php
$setTab = "File";
	If($isStaff){
		$TabCodes = array("File"=>"File");
	}else{
		$TabCodes = array("File"=>"File");
	}
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
?>
	<div id='File' class='tab-content'>
		<h1>Convert Qualtrix file to Salesforce dataloader file</h1>
		<?php
		//echo "ImportFileName ".$ImportFileName."<br>";
		//echo $target_file;
		if(count($_FILES['fileToUpload1'])) {
			ob_start();
			$fileCounter = 0;
			foreach ($_FILES as $file){
				$fileCounted = count($file["name"]);
				while ($fileCounter < $fileCounted){
					$dataFileName = strtotime('today')."_".str_replace(" ","",$file["name"][$fileCounter]);
					//$dataFileName = str_replace(".xlsm",".xlsx",$dataFileName);
					$target_file = $siteRoot.$adminFolder."residential/SolarAccessData/".$dataFileName;
					$ImportFileName = "SolarAccessData/".$dataFileName;
					//echo $target_file;
					$tmpFileName = $file["tmp_name"][$fileCounter];
					if ($_GET['type'] == "uploadFile"){
						$ImportFileReceived = true;
						move_uploaded_file($tmpFileName, $target_file);
						$uploadedFiles[$fileCounter] = array("ImportFileName"=>$ImportFileName,"dataFileName"=>$dataFileName,"target_file"=>$target_file);
					}
					$fileCounter++;
				}
			}
		}
		if (count($uploadedFiles) || $_GET['useUploadedFile']){
			if ($_GET['useUploadedFile']){
				$uploadedFiles[0] = array("ImportFileName"=>$_GET['useUploadedFile'],"dataFileName"=>$dataFileName,"target_file"=>$target_file);
	
			}
			echo "Processing ".count($uploadedFiles)." Files...<hr>";
			foreach ($uploadedFiles as $uploadedFile){
				$ImportFileName = $uploadedFile["ImportFileName"];
				$dataFileName = $uploadedFile["dataFileName"];
				$target_file = $uploadedFile["target_file"];
				echo "Processing: ".$dataFileName."<br>";
				ob_flush();
				include($siteRoot.$adminFolder.'residential/views/_SolarAccessDataProcessor.php');
				echo "<hr>";
				ob_flush();
			}
				
		}
		
/*		
		if ($ImportFileReceived || $_GET['useUploadedFile']){
			include('views/_HES-DataImporterCanUpload.php');
		}
*/
		?>


			<a id='uploadFile' href="#" class='button-link do-not-navigate'>Upload<?php echo ($successfullyUploaded ? " Another" : "");?> File</a><br>
			<div id="uploadFileForm" style='display:none;'>
				<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>residential/?type=uploadFile&nav=<?php echo $nav;?>" enctype="multipart/form-data">
					<div class="row">
						<div class="eight columns">
							<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
						</div>
						<div class="eight columns">
							<strong>File You Selected:</strong>
							<ul id="fileList1"><li>No Files Selected</li></ul>
							<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
						</div>
					</div>
					
					<script type="text/javascript">
						function makeFileList1() {
							var input = document.getElementById("fileToUpload1");
							var ul = document.getElementById("fileList1");
							while (ul.hasChildNodes()) {
								ul.removeChild(ul.firstChild);
							}
							for (var i = 0; i < input.files.length; i++) {
								var li = document.createElement("li"),
									fileName = input.files[i].name,
									fileLength = fileName.length,
									fileExt = fileName.substr(fileLength-4);
									console.log(fileExt);
								if (fileExt == ".csv"){
									li.innerHTML = input.files[i].name;
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'block';
								}else{
									li.innerHTML = fileName+' must be a .csv file';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
							if(!ul.hasChildNodes()) {
								var li = document.createElement("li");
								li.innerHTML = 'No Files Selected';
								ul.appendChild(li);
								document.getElementById('submitButton1').style.display = 'none';
							}
						}
					</script>
				</form>
			</div>

		<script type="text/javascript">
			$(function () {
				
				$("#uploadFile").on('click',function(){
					$("#uploadFileForm").toggle();
				});
				
				var table = $('.simpleTable').DataTable({
					"scrollX": true,
		//			"scrollY": "300px",
					"bJQueryUI": true,
					"bSearchable":false,
					"bFilter":false,
					"bAutoWidth": true,
					"bSort": true,
					"iDisplayLength": 25,
					"paging":   false,
		//			"ordering": false,
					"scrollCollapse": true,
					"info":     false
				});
				var tableLimited = $('.simpleTableLimited').DataTable({
					"scrollX": true,
					"scrollY": "240px",
					"bJQueryUI": true,
					"bSearchable":false,
					"bFilter":false,
					"bAutoWidth": true,
					"bSort": true,
					"iDisplayLength": 10,
					"paging":   false,
		//			"ordering": false,
					"scrollCollapse": true,
					"info":     false
				});
				$(".DataTables_sort_wrapper").first().click().click();
				
				var getExtractFilters = function(){
					var monthYear = $("#MonthPicker").val(),
						filterValues = new Array(monthYear);
						
						return filterValues;
				};
				
				var updateExtractFilters = function(){
					var Filters=getExtractFilters(),
						StartDate = $("#StartDate").val();
						EndDate = $("#EndDate").val();
					
					window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>residential/?monthYear="+Filters[0]+"&StartDate="+StartDate+"&EndDate="+EndDate+"&nav=<?php echo $nav;?>#";
				}
				$(".parameters").on('change',function(){
					updateExtractFilters();
				});
				
				$('.date-picker').datepicker({
					showOn: "button",
					buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
					buttonImageOnly: true,
					buttonText: 'Click to change month',
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					dateFormat: 'mm/dd/yy',
					minDate: '01/01/14',
					maxDate: '<?php echo date("m/t/Y");?>',
					onClose:
						function(dateText, inst){
							var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
							var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
							$(this).datepicker('setDate', new Date(year, month, 1));
							$("#StartDate").datepicker('setDate', new Date(year, month, 1));
							var date2 = $(this).datepicker('getDate');
							date2.setMonth(date2.getMonth() +1);
							date2.setDate(date2.getDate() -1);
							$("#EndDate").datepicker('setDate', date2);
							$("#weekPicker").val('');
							updateExtractFilters();
						}
				
				});

				
			});
		</script>
	</div>
</div>
<script type="text/javascript">
	$(function () {
	
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
?>