<h1>Create Highlighted Field EZ-Doc</h1>
		<button id='showTemplateUploaded'>If you want to update the EZ Doc Template for ALL new EZ Docs</button>
		<div id='templateUploaderDiv' style='display:none'>
		<?php
		//echo "ImportFileName ".$ImportFileName."<br>";
		//echo $target_file;
		if(count($_FILES['fileToUpload2'])) {
			$dataFileName = str_replace(" ","",$_FILES["fileToUpload2"]["name"][0]);
			$dataFileName = str_replace(".xlsm",".xlsx",$dataFileName);
			$dataFileName = "EZDocTemplate.xlsx";
			$dataFileNameTemplate = "EZDocTemplate.xlsm";
			$target_file = $siteRoot.$adminFolder."residential/HESDataFiles/".$dataFileName;
			$target_fileTemplate = $siteRoot.$adminFolder."residential/HESDataFiles/".$dataFileNameTemplate;
			$ImportFileName = "HESDataFiles/".$dataFileName;
			echo $target_file;
			$tmpFileName = $_FILES["fileToUpload2"]["tmp_name"][0];
			if ($_GET['type'] == "uploadFile"){
				$ImportFileReceived2 = true;
				unlink($target_file);
				move_uploaded_file($tmpFileName, $target_file);
				unlink($target_fileTemplate);
				copy($target_file,$target_fileTemplate);
			}
		}
		if ($ImportFileReceived2 || $_GET['useUploadedFile2']){
			//new import that data and parse it out 
			$trackingFile = (trim($_GET['useUploadedFile2']) ? $_GET['useUploadedFile2'] : $ImportFileName);
			$ImportFileName = (trim($_GET['useUploadedFile2']) ? $_GET['useUploadedFile2'] : $ImportFileName);
			echo "Using File just uploaded<br>";
			ob_flush();
			include('model/highlight_HESDataFileTemplate.php');
		}
		if (!$ImportFileReceived2){
			$noExtractFile = true;

			if (file_exists($target_file)){
				$noExtractFile = false;
				$lastUploadedDateParts = explode("File",$ImportFileName);
				$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
				
				echo "<br><br><a href='".$ImportFileName."'>View Existing EZ-Doc Template File</a> last uploaded ".date("F d, Y H:ia",filemtime($target_file))."<br>";
				$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
				$trackingFile = $ImportFileName;
			}
		}
		?>


			<a id='uploadFile2' href="#" class='button-link do-not-navigate'>Upload File</a><br>
			<div id="uploadFileForm2" style='display:none;'>
				<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>residential/?type=uploadFile&nav=HES-DataImporter#FieldMap" enctype="multipart/form-data">
					<div class="row">
						<div class="eight columns">
							<input type="file" name="fileToUpload2[]" id="fileToUpload2" multiple="" onChange="makeFileList2();" /><Br>
						</div>
						<div class="eight columns">
							<strong>File You Selected:</strong>
							<ul id="fileList2"><li>No Files Selected</li></ul>
							<input type="submit" value="Begin Processing" id="submitButton2" style="display:none;">
						</div>
					</div>
					
					<script type="text/javascript">
						function makeFileList2() {
							var input = document.getElementById("fileToUpload2");
							var ul = document.getElementById("fileList2");
							while (ul.hasChildNodes()) {
								ul.removeChild(ul.firstChild);
							}
							for (var i = 0; i < input.files.length; i++) {
								var li = document.createElement("li"),
									fileName = input.files[i].name,
									fileLength = fileName.length,
									fileExt = fileName.substr(fileLength-4);
									console.log(fileExt);
								if (fileExt == ".xls" || fileExt == "xlsx" || fileExt == "xlsm"){
									li.innerHTML = input.files[i].name;
									ul.appendChild(li);
									document.getElementById('submitButton2').style.display = 'block';
								}else{
									li.innerHTML = 'You must save '+fileName+' as a .xls or .xlsx file first';
									ul.appendChild(li);
									document.getElementById('submitButton2').style.display = 'none';
								}
							}
							if(!ul.hasChildNodes()) {
								var li = document.createElement("li");
								li.innerHTML = 'No Files Selected';
								ul.appendChild(li);
								document.getElementById('submitButton2').style.display = 'none';
							}
						}
					</script>
				</form>
			</div>
		</div>

		<script type="text/javascript">
			$(function () {
				$("#uploadFile2").on('click',function(){
					$("#uploadFileForm2").toggle();
				});
				$("#showTemplateUploaded").on('click',function(){
					$("#templateUploaderDiv").toggle();
				});
			});
		</script>
