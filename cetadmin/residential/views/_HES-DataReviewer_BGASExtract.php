<?php
include_once($dbProviderFolder."ProductProvider.php");

$ProductProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $ProductProvider->get($criteria);
$resultArray = $paginationResult->collection;
print_pre($resultArray);
foreach ($resultArray as $result=>$record){
	$efiPartsToRemove["_".$record->efi] = "";
	$efiParts = explode(".",$record->efi);
	$bulbType = ($record->bulbType ? : "LED");
	$CSGCode = $record->csgCode;
	$CSGCodes = array();
	if (strpos($CSGCode,",")){
		$CSGCodes = explode(",",$CSGCode);
	}else{
		$CSGCodes = array($CSGCode);
	}
	$clearResultsCode = $bulbType.$efiParts[1];
	$record->clearResultsCode = $clearResultsCode;
	foreach ($CSGCodes as $code){
		$ProductsByCSGCodes[$code] = $record;
	}
}
//print_pre($ProductsByCSGCodes);
						$bgasFields = array("SITEID","CUST_FIRST_NAME","CUST_LAST_NAME","ADDRESS","CITY","STATE","ZIP","PLUS4","NUM_UNITS","PRIMARY_PRVD","PROVIDERID","PROVIDERCODE","ACCTCUST","LOCATIONID","SECONDARY_PRVD","PROVIDERID2","PROVIDERCODE2","ACCTCUST2","HEATFUEL","DHWFUEL","CUST_TYPE","APPTDATE","APPTTYPE","AUDITOR_FIRST_NAME","AUDITOR_LAST_NAME","CONTRACT","MEASTABLE","ID0","ID1","DESCRIPTION","PROP_PARTID","PROP_QTY","INS_PARTID","INS_QTY","MBTU_SAVINGS","DEPOSIT","CUST_PRICE","UTIL_PRICE","PAYEE","ISSUED_DT","SIGNED_DT","INSTALL_DT","INV_STATUS","MEAS_STATUS","INSTALLER_NAME","MEASURELIFE","ICTV_AWARE_SIR","ICTV_AWARE_SIMPLEPAYBACK","THERMS");
						$BGASFileShortName = "BGASExtract".date("Ymt").".txt";
						$BGASFileNameTxt = "../residential/BGASExtracts/".$BGASFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$NGRIDJobFileNameTxt);

						$fpBGAS = fopen($BGASFileNameTxt, 'w');
						fputs($fpBGAS, implode("|",$bgasFields)."\n"); //add header row
						
						$rowJobCount = count($bgasDocIds);
						$rowJobCounter = 0;
						//print_pre($bgasDocIds);
						foreach ($bgasDocIds as $electriProvider=>$docs){
							foreach ($docs as $docId=>$data){
								$possibleBgasDocIds[$docId] = 1;
								//print_pre($data);
								$siteId = $data["fields"]["SITEID"];
								$projectId = $data["fields"]["PROJECTID"];
								$contractor = $data["fields"]["Contractor_Name"];
								//print_pre($data["fields"]);
								$apptDate = date("Y-m-d",strtotime($data["fields"]["APPTDATE"]));
								//echo $apptDate." | ". $dataByFieldName->APPTDATE." | ".exceldatetotimestamp($dataByFieldName->APPTDATE)." = ";
								if ($apptDate == "1969-12-31"){$apptDate = (strpos($data["fields"]["APPTDATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["APPTDATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["APPTDATE"]))));}
								$assmntDate = date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"]));
								if ($assmntDate == "1969-12-31"){$assmntDate = (strpos($data["fields"]["ASSESSMENT_DT"],"/") ? date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["ASSESSMENT_DT"]))));}
								$audEventDate = date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"]));
								if ($audEventDate == "1969-12-31"){$audEventDate = (strpos($data["fields"]["AUD_EVENT_DATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["AUD_EVENT_DATE"]))));}
								$apptType = strtoupper($data["fields"]["APPTTYPE"]);


								
								
								
								if (!$data["fields"]["DWELL_TYPE"]){$data["fields"]["DWELL_TYPE"] = "Single Family";}
								$NumUnitsParts = explode(" ",$data["fields"]["DWELL_TYPE"]);
								
								$data["fields"]["NUM_UNITS"] = (is_numeric($NumUnitsParts[0]) ? $NumUnitsParts[0] : 1);
								
								if (!$dwellTypes[$data["fields"]["DWELL_TYPE"]]){$data["fields"]["DWELL_TYPE"] = 16;}
								
								$auditVisitTypes = array("HEA"=>"HEA","HEA VISIT"=>"HEA","HEAAU"=>"HEAAU","HEA ADDITIONAL UNIT VISIT"=>"HEAAU","SHV"=>"SHV","SPECIAL HOME VISIT"=>"SHV","RENTER"=>"RENTER","RENTER VISIT"=>"RENTER","LANDLORD VISIT"=>"LANDLORD","WIFI"=>"WIFI","WIFI SPECIAL HOME VISIT"=>"WIFI_SHV");
								$AuditType = "";
								$AuditType = $auditVisitTypes[$apptType];
								$validApptType = false;
								if ($auditVisitTypes[$apptType]){
									$validApptType = true;
								}
								$auditVisitTypesToIgnore = array("INSPECTION"=>1,"COMBUSTION SAFETY RETURN VISIT"=>1);
								if ($auditVisitTypesToIgnore[$apptType]){
									$validApptType = false;
									$docIdsNotIncluded[$docId] = "Not Extractable Visit Type: ".$apptType;
								}
								if (!$auditVisitTypes[$apptType] && !$auditVisitTypesToIgnore[$apptType]){
									$validApptType = false;
									$docIdsNotIncluded[$docId] = "No Match on Visit Type: ".$apptType;
								}
								
								if ($validApptType && $AuditType){
									//$allFields->OWNER_OCCUPIED = ($allFields->OWNER_OCCUPIED == "Owner" ? "Y" : "N");
									//$allFields->MODERATE_INCOME = ($allFields->MODERATE_INCOME == "Yes" ? "Y" : "N");
									//$allFields->LEAD_VENDOR = "CET";
									$data["fields"]["CUST_FIRST_NAME"] = strtoupper($data["fields"]["CUST_FIRST_NAME"]);
									$data["fields"]["CUST_LAST_NAME"] = strtoupper($data["fields"]["CUST_LAST_NAME"]);
									$data["fields"]["CITY"] = strtoupper($data["fields"]["CITY"]);
									$data["fields"]["PRIMARY_PRVD"] = "BERKSHIR";
									$data["fields"]["PROVIDERCODE"] = "BERKSHIR";
									$data["fields"]["SECONDARY_PRVD"] = (strpos(strtolower($data["fields"]["SECONDARY_PRVD"]),"grid") ? "NGRIDELE" : "WMECO");
									$data["fields"]["PROVIDERCODE2"] = $data["fields"]["SECONDARY_PRVD"];
									$data["fields"]["HEATFUEL"] = "GAS";
									$data["fields"]["DHWFUEL"] = ($data["fields"]["DHWFUEL"] ? : "GAS");
									$data["fields"]["DHWFUEL"] = ($data["fields"]["DHWFUEL"] == "Natural Gas" ? "GAS" : $data["fields"]["DHWFUEL"]);
									$data["fields"]["CUST_TYPE"] = (strtoupper($data["fields"]["OWNER_OCCUPIED"]) ? : "OWNER");
									$data["fields"]["APPTDATE"] = date("m/d/Y",strtotime($apptDate));
									$data["fields"]["APPTTYPE"] = $AuditType;
									$auditorParts = explode(" ",$data["fields"]["AUDITOR_FIRST_NAME"]);
									$data["fields"]["AUDITOR_FIRST_NAME"] = strtoupper($auditorParts[0]);
									$data["fields"]["AUDITOR_LAST_NAME"] = strtoupper($auditorParts[1]);
									if ($data["fields"]["AUDITOR_FIRST_NAME"] == "OVERBY"){
										$data["fields"]["AUDITOR_FIRST_NAME"] = "JAMIE";
										$data["fields"]["AUDITOR_LAST_NAME"] = "OVERBY";
									}
									$data["fields"]["PAYEE"] = "BERKSHIR-XTRACT";
									$data["fields"]["ISSUED_DT"] = date("m/d/Y",strtotime($assmntDate));
									$data["fields"]["SIGNED_DT"] = date("m/d/Y",strtotime($audEventDate));
									$data["fields"]["INSTALL_DT"] = date("m/d/Y",strtotime($audEventDate));
									$data["fields"]["INV_STATUS"] = "RFB";
									$deposit = $data["fields"]["CUSTOMER_DEPOSIT"];

									//determine Incentive Amount and Cap for Weatherization items
									$ModerateIncome = $data["fields"]["MODERATE_INCOME"];
									$WholeHouseIncentive = $data["fields"]["WHOLEBUILDING_INCENTIVE"];
									$incentiveRate = 0.5;
									$incentiveCap = 1000;
									
									if ($data["fields"]["IncentiveRate"] && $data["fields"]["IncentiveCap"]){
										$incentiveRate = $data["fields"]["IncentiveRate"];
										$incentiveCap = $data["fields"]["IncentiveCap"];
									}else{
										$incentiveRate = 0.75;
										$incentiveCap = (int)$Config_incentiveCapBase;
										//use logic todetermine rate
										
										if ($ModerateIncome == "Yes"){
											$incentiveRate = 1;
											$incentiveCap = (int)$Config_incentiveCapHigh;
										}else{
											if ($WholeHouseIncentive == "Yes"){
												$incentiveRate = .9;
												$incentiveCap = (int)$Config_incentiveCapHigh;
											}
										}
										
									}
									$extraIncentive = false;
									//used for testing
									if ($incentiveRate > .75){
										$extraIncentive = true;
										//print_pre($data["fields"]);
										//echo $docId." ModerateIncome: ".$ModerateIncome." WholeBuiling: ".$WholeHouseIncentive." IncentiveRate/Cap: ".$incentiveRate."/".$incentiveCap."<br>";
										//print_pre($extractDatesBySiteId[$siteId][$projectId]);
									}
									
									$noIsms = true;
									//echo $docId." ISMs: ".count($data["isms"])."<br>";
									if (count($data["isms"])){
										foreach ($data["isms"] as $contractNum=>$isms){
											
											foreach ($isms as $ismObj){
												$ism = (array) $ismObj;
												
												//acceptable types for electric piggyback will be payee of electric company
												$acceptableTypesArray = array("VW_APLNC_RCMDN","VW_LIGT_BULB_RCMDN");
												if ($ism["MEASTABLE"]){
													$data["fields"]["CONTRACT"] = $contractNum;
													$data["fields"]["MEASTABLE"] = $ism["MEASTABLE"];
													$data["fields"]["ID0"] = "";
													$data["fields"]["ID1"] = "";
													$data["fields"]["DESCRIPTION"] = $ism["DESCRIPTION"];
													$data["fields"]["PROP_PARTID"] = ($ism["DESCRIPTION"] == "5w TCP 2.0 LED Candle base" ? "005_TCP_TORP_LED_2030.101" : $ism["PROP_PARTID"]);
													$data["fields"]["PROP_QTY"] = $ism["PROP_QTY"];
													$data["fields"]["INS_PARTID"] = ($ism["DESCRIPTION"] == "5w TCP 2.0 LED Candle base" ? "005_TCP_TORP_LED_2030.101" : $ism["INS_PARTID"]);
													$data["fields"]["INS_QTY"] = $ism["INS_QTY"];
													$data["fields"]["MBTU_SAVINGS"] = round($ism["MBTU_SAVINGS"],4);
													$data["fields"]["DEPOSIT"] = 0;
													$data["fields"]["CUST_PRICE"] = bcmul($ism["UTIL_PRICE"],$ism["QTY"],2);
													$data["fields"]["UTIL_PRICE"] = round($ism["UTIL_PRICE"],2);
													$data["fields"]["MEAS_STATUS"] = (strpos(" ".$ism["DESCRIPTION"],"Wireless") ? "WIFI" : "INSTALLED");
													$data["fields"]["INSTALLER_NAME"] = "CET";
													$data["fields"]["MEASURELIFE"] = $ism["MEASURELIFE"];
													$data["fields"]["ICTV_AWARE_SIR"] = 0;
													$data["fields"]["ICTV_AWARE_SIMPLEPAYBACK"] = 0;
													$data["fields"]["THERMS"] = bcdiv($data["fields"]["MBTU_SAVINGS"],100,6);
													if (in_array($ism["MEASTABLE"],$acceptableTypesArray)){
														$data["fields"]["PAYEE"] = ($electriProvider == "National Grid" ? "BG-NGRID-PI-418B" : "WME-PI-418");
													}else{
														$data["fields"]["PAYEE"] = "BERKSHIR-XTRACT";
														
													}
													

													$thisRow = array();
													foreach ($bgasFields as $dataField){
														//echo $dataField." = " .$data["fields"][$dataField]."<br>";
														$thisRow[] = $data["fields"][$dataField];
													}
													$exportedBGASDocIds[$docId] = 1;
													$exportedBGASISMDocIds[$docId] = 1;
													$bgasRows[] = $thisRow;
												}//end if MEASTABLE
											}
										}
										$noIsms = false;
									}
									$contractTypes = array("Weather","AirSealing","WIFI");
									$noContracts = true;
									foreach ($contractTypes as $contractType){
										if (count($data[$contractType])){
											//echo $docId." ".$contractType.": ".count($data[$contractType])."<br>";
											$utilityPrice = 0;
											$utilityPriceTotal = 0;

											foreach ($data[$contractType] as $contractNum=>$isms){
												foreach ($isms as $ismObj){
													$ism = (array) $ismObj;
													
													//acceptable types for electric piggyback
														if ($ism["MEASTABLE"]){
															//fortesting only
															if ($extraIncentive){
																//echo $contractType;
																//print_pre($ism);
															}

															$MEAS_STATUS = (strpos(" ".$ism["DESCRIPTION"],"Wireless") ? "WIFI" : "BILLING");
															$PAYEE = ($MEAS_STATUS == "WIFI" ? (strpos($ism["DESCRIPTION"],"Combo") ? "MANUEL" : "CET") : "BERKSHIR-XTRACT"); 
															$itemPrice = $ism["CUST_PRICE"];
															$projectedUtilityPrice = bcmul($itemPrice,$incentiveRate,2);
															$projectedUtilityPriceTotal = bcadd($utilityPriceTotal,$projectedUtilityPrice,2);
															if ($contractType == "Weather"){
																if ($utilityPriceTotal < $incentiveCap){
																	if ($projectedUtilityPriceTotal > $incentiveCap){
																		$customerPrice = bcsub($projectedUtilityPriceTotal,$incentiveCap,2);
																		$incentivePrice = bcsub($projectedUtilityPrice,$customerPrice,2);
																	}else{
																		$incentivePrice = ($utilityPriceTotal < $incentiveCap ? bcmul($itemPrice,$incentiveRate,2) : 0);
																		$customerPrice = ($itemPrice - $incentivePrice);
																	}
																}else{
																	$incentivePrice = 0;
																	$customerPrice = $itemPrice;
																}
																$issuedDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["INSIssuedDate"]));
																$signedDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["INSSignedDate"]));
																$installDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["InstallDate"]));

															}elseif ($contractType == "AirSealing"){
																$customerPrice = 0;
																$incentivePrice = $itemPrice;
																$issuedDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["ASIssuedDate"]));
																$signedDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["ASSignedDate"]));
																$installDate = date("m/d/Y",strtotime($extractDatesBySiteId[$siteId][$projectId]["InstallDate"]));
															}else{
																//contractType is WIFI
																$deposit = 0;
																$customerPrice = $itemPrice;
																$incentivePrice = $ism["UTIL_PRICE"];
																
																$issuedDate = date("m/d/Y",strtotime($assmntDate));
																$signedDate = date("m/d/Y",strtotime($audEventDate));
																$installDate = date("m/d/Y",strtotime($audEventDate));
																
															}
															$data["fields"]["ISSUED_DT"] = ($issuedDate == "12/31/1969" ? "" : $issuedDate);
															$data["fields"]["SIGNED_DT"] = ($signedDate == "12/31/1969" ? "" : $signedDate);
															$data["fields"]["INSTALL_DT"] = ($installDate == "12/31/1969" ? "" : $installDate);
															
															//echo "incentiveCap: ".$incentiveCap." itemPrice: ".$itemPrice." totalPrice: ".$utilityPriceTotal." incentivePrice: ".$incentivePrice." customerPrice: ".$customerPrice."<br>";
															$utilityPriceTotal = bcadd($utilityPriceTotal,$incentivePrice,2);
															$data["fields"]["CONTRACT"] = $contractNum;
															$data["fields"]["MEASTABLE"] = $ism["MEASTABLE"];
															$data["fields"]["ID0"] = "";
															$data["fields"]["ID1"] = "";
															$data["fields"]["DESCRIPTION"] = $ism["DESCRIPTION"];
															$data["fields"]["PROP_PARTID"] = $ism["PROP_PARTID"];
															$data["fields"]["PROP_QTY"] = $ism["PROP_QTY"];
															$data["fields"]["INS_PARTID"] = $ism["INS_PARTID"];
															$data["fields"]["INS_QTY"] = $ism["INS_QTY"];
															$data["fields"]["MBTU_SAVINGS"] = round($ism["MBTU_SAVINGS"],4);
															$data["fields"]["DEPOSIT"] = $deposit;
															$data["fields"]["CUST_PRICE"] = $customerPrice;
															$data["fields"]["UTIL_PRICE"] = $incentivePrice;
															$data["fields"]["MEAS_STATUS"] = $MEAS_STATUS;
															$data["fields"]["MEASURELIFE"] = ($ism["MEASURELIFE"] ? : "0");
															$data["fields"]["ICTV_AWARE_SIR"] = 0;
															$data["fields"]["ICTV_AWARE_SIMPLEPAYBACK"] = 0;
															$data["fields"]["THERMS"] = bcdiv($data["fields"]["MBTU_SAVINGS"],100,6);
															$data["fields"]["PAYEE"] = $PAYEE;
															$data["fields"]["INSTALLER_NAME"] = ($MEAS_STATUS == "WIFI" ? "CET" : $contractor);
															$thisRow = array();
															foreach ($bgasFields as $dataField){
																echo $dataField." = " .$data["fields"][$dataField]."<br>";
																$thisRow[] = $data["fields"][$dataField];
															}
															$exportedBGASDocIds[$docId] = 1;
															$exportedBGASMeasureDocIds[$docId] = 1;
															$bgasRows[] = $thisRow;
															$noContracts = false;
														}//end if MEASTABLE
												}
												
														
											}//foreach ism
											
										}//if count contractType
									}//for each contractType
									

								}//if valid apptType
								if ($noIsms && $noContracts){
									$docIdsNotIncluded[$docId] = "No Measures to Extract";
								}

							}//foreach electricProvider
						}
						fclose($fpBGAS);
						
						$energyName = "BGAS";
						$energyType = "Extract";
						$energyHeaderFields = $bgasFields;
						$energyDataRows = $bgasRows;
						if (count($energyDataRows)){
							include('model/createSimpleExcelFile.php');
						}