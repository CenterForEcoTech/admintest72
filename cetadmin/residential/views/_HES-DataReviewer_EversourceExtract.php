<?php
						//get PiggyBack data
						$piggyBackResults = $residentialProvider->getPiggyBacks();
						$piggyBackCollection = $piggyBackResults->collection;
						foreach ($piggyBackCollection as $pbResult){
							$alreadyPiggyBackCharged[$pbResult->SITEID] = 1;
						}

						$eversourceDocIds = $docIds;
						$eversourceJobFields = array("EXT_JOB_ID","VENDOR_CODE","EXT_INVOICE_ID","DATE","FIRST_NAME","LAST_NAME","EXT_ADDRESS","EXT_APT","EXT_CITY","MDW_ACT_ID","SQUARE_FOOTAGE","AUD_EVENT_DATE","OWN_RENT","DWELL_TYPE","DHW_FUEL","PRIM_HEAT_FUEL","PRIM_HEAT_TYPE","DHW_TYPE","DHW_SIZE","YEAR_BUILT","EXT_STATE","EXT_ZIP","MAILING_FIRST_NAME","MAILING_LAST_NAME","MAILING_STREET","MAILING_ADDRESS2","MAILING_CITY","MAILING_STATE","MAILING_ZIP","MAILING_HOME_PHONE","MAILING_WORK_PHONE","EMAIL","OTHER PA","AUDITOR","NUM_UNITS","SINGLE_MULTI_CD","SEC_HEAT_FUEL","SEC_HEAT_TYPE","HTG_DIST_TYPE","INSTALL_DHW_TYPE","INSTALL_DHW_SIZE","TEMP_SET_EXIST","TEMP_SET_NEW","CENTRAL_AC","ROOM_AC","BEDROOMS","BATHROOMS","HVAC_TYPE","CHECK DATE","OCCUPANTS","REQUESTED_DT","ASSESSMENT_DT","CONTRACTOR_NAME","CONTRACTOR_ADDRESS","CONTRACTOR_CITY","CONTRACTOR_STATE","CONTRACTOR_ZIP","CONTRACTOR_PHONE","CONTRACTOR_FAX","CONTRACTOR_EMAIL");
						$eversourcePartsFields = array("EXT_JOB_ID","MEA_ID","PART_ID","DESCRIPTION","QTY","AUTH_INCENTIVE_AMT","PROCESS_ALIAS","TOTAL","INSTALL_LOCATION","ANNUAL_OP_HRS","DMD_RED_KW","ENR_SAVINGS","EXISTING_MEA_ID","EXISTING_QTY","EXISTING_KWH","EXISTING_KHW_SOURCE","EXISTING_OP_HOURS","RFG_EXIST_BRAND","RFG_EXIST_MODEL","RFG_EXIST_SIZE","EXIST_THROUGH_DOOR_ICE","EXIST_YEAR","EXIST_LOCATION","RFG_NEW_BRAND","RFG_NEW_MODEL","RFG_NEW_SIZE","THROUGH_DOOR_ICE","SAVINGS_ELE","SAVINGS_GAS","SAVINGS_OIL","SAVINGS_PROPANE","SAVINGS_OTHER","SAVINGS_UNIT","MEASURE_FUEL_TYPE","EXIST_FRIDGE_STYLE","SIZE_TYPE","NEW_FRIDGE_STYLE","NEW_KWH","INSTALLED_DATE","EXIST_EFF","INSTALL_EFF","NFRC_UFACTOR","EXIST_WATTAGE","INSTALL_WATTAGE","CUSTOMER_COST","INSTALL_GPM","INSULATION_TYPE","PRE_RVALUE","POST_RVALUE","WALL_SIDING","HOURS_WORKED","PRE_TEST","POST_TEST","EXIST_CFM50","INSTALL_CFM50");
						//added total fields below
						$eversourceTotalFields = array("EXT_JOB_ID","PROJECT_NAME","VENDOR_CODE","EXT_INVOICE_ID","DATE","FIRST_NAME","LAST_NAME","EXT_ADDRESS","EXT_APT","EXT_CITY","MDW_ACT_ID","SQUARE_FOOTAGE","AUD_EVENT_DATE","OWN_RENT","DWELL_TYPE","DHW_FUEL","PRIM_HEAT_FUEL","PRIM_HEAT_TYPE","DHW_TYPE","DHW_SIZE","YEAR_BUILT","EXT_STATE","EXT_ZIP","MAILING_FIRST_NAME","MAILING_LAST_NAME","MAILING_STREET","MAILING_ADDRESS2","MAILING_CITY","MAILING_STATE","MAILING_ZIP","MAILING_HOME_PHONE","MAILING_WORK_PHONE","EMAIL","OTHER PA","AUDITOR","NUM_UNITS","SINGLE_MULTI_CD","SEC_HEAT_FUEL","SEC_HEAT_TYPE","HTG_DIST_TYPE","INSTALL_DHW_TYPE","INSTALL_DHW_SIZE","TEMP_SET_EXIST","TEMP_SET_NEW","CENTRAL_AC","ROOM_AC","BEDROOMS","BATHROOMS","HVAC_TYPE","CHECK DATE","OCCUPANTS","REQUESTED_DT","ASSESSMENT_DT","CONTRACTOR_NAME","CONTRACTOR_ADDRESS","CONTRACTOR_CITY","CONTRACTOR_STATE","CONTRACTOR_ZIP","CONTRACTOR_PHONE","CONTRACTOR_FAX","CONTRACTOR_EMAIL","MEA_ID","PART_ID","DESCRIPTION","QTY","AUTH_INCENTIVE_AMT","PROCESS_ALIAS","TOTAL","INSTALL_LOCATION","ANNUAL_OP_HRS","DMD_RED_KW","ENR_SAVINGS","EXISTING_MEA_ID","EXISTING_QTY","EXISTING_KWH","EXISTING_KHW_SOURCE","EXISTING_OP_HOURS","RFG_EXIST_BRAND","RFG_EXIST_MODEL","RFG_EXIST_SIZE","EXIST_THROUGH_DOOR_ICE","EXIST_YEAR","EXIST_LOCATION","RFG_NEW_BRAND","RFG_NEW_MODEL","RFG_NEW_SIZE","THROUGH_DOOR_ICE","SAVINGS_ELE","SAVINGS_GAS","SAVINGS_OIL","SAVINGS_PROPANE","SAVINGS_OTHER","SAVINGS_UNIT","MEASURE_FUEL_TYPE","EXIST_FRIDGE_STYLE","SIZE_TYPE","NEW_FRIDGE_STYLE","NEW_KWH","INSTALLED_DATE","EXIST_EFF","INSTALL_EFF","NFRC_UFACTOR","EXIST_WATTAGE","INSTALL_WATTAGE","CUSTOMER_COST","INSTALL_GPM","INSULATION_TYPE","PRE_RVALUE","POST_RVALUE","WALL_SIDING","HOURS_WORKED","PRE_TEST","POST_TEST","EXIST_CFM50","INSTALL_CFM50");
						$dataFields = array("SITEID","PROJECT_NAME","VENDOR_CODE","EXT_INVOICE_ID","APPTDATE","CUST_FIRST_NAME","CUST_LAST_NAME","ADDRESS","EXT_APT","CITY","MDW_ACT_ID","SQUARE_FOOTAGE","AUD_EVENT_DATE","OWN_RENT","DWELL_TYPE","DHW_FUEL","PRIM_HEAT_FUEL","PRIM_HEAT_TYPE","DHW_TYPE","DHW_SIZE","YEAR_BUILT","STATE","ZIP","CUST_FIRST_NAME","CUST_LAST_NAME","ADDRESS","MAILING_ADDRESS2","CITY","STATE","ZIP","MAILING_HOME_PHONE","MAILING_WORK_PHONE","EMAIL","OTHER PA","AUDITOR","NUM_UNITS","SINGLE_MULTI_CD","SEC_HEAT_FUEL","SEC_HEAT_TYPE","HTG_DIST_TYPE","INSTALL_DHW_TYPE","INSTALL_DHW_SIZE","TEMP_SET_EXIST","TEMP_SET_NEW","CENTRAL_AC","ROOM_AC","BEDROOMS","BATHROOMS","HVAC_TYPE","CHECK DATE","OCCUPANTS","REQUESTED_DT","ASSESSMENT_DT","CONTRACTOR_NAME","CONTRACTOR_ADDRESS","CONTRACTOR_CITY","CONTRACTOR_STATE","CONTRACTOR_ZIP","CONTRACTOR_PHONE","CONTRACTOR_FAX","CONTRACTOR_EMAIL");
						//print_pre($eversourceTotalFields);
						//echo $dataFields["MEA_ID"];
						//print_pre($eversourceDocIds);
						$JobFileShortName = "EVERSOURCEExtractJobsWMEPI418".date("Ymt").".txt";
						$JobFileNameTxt = "../residential/EversourceExtracts/".$JobFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$JobFileNameTxt);
						$fpJob = fopen($JobFileNameTxt, 'w');
						fputs($fpJob, implode("|",$eversourceJobFields)."\n"); //add header row
						$PartFileShortName = "EVERSOURCEExtractPartsWMEPI418".date("Ymt").".txt";
						$PartFileNameTxt = "../residential/EversourceExtracts/".$PartFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$PartFileNameTxt);
						$fpPart = fopen($PartFileNameTxt, 'w');
						fputs($fpPart, implode("|",$eversourcePartsFields)); //add header row
						
						//Added by LH total file names
						
						$TotalFileShortName = "EVERSOURCEExtractTotalWMEPI418".date("Ymt").".txt";
						$TotalFileNameTxt = "../residential/EversourceExtracts/".$TotalFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$JobFileNameTxt);
						$fpTotal = fopen($TotalFileNameTxt, 'w');
						fputs($fpTotal, implode("|",$eversourceTotalFields)."\n"); //add header row
						//end of new addition
						
						$rowJobCount = 0;
						foreach ($eversourceDocIds as $docId=>$data){
							if ($data["fields"]["SECONDARY_PRVD"] == "Eversource"){
								$rowJobCount++;
							}
						}
						//standar PiggyBack items
						$pgBack["MEA_ID"] = "FEE_PIGGYBACK_P";
						$pgBack["PROJECT_NAME"] = "CET Eversource Piggyback";
						$pgBack["PART_ID"] = "PIGGYBACK_FEE";
						$pgBack["DESCRIPTION"] = "Piggyback Fee";
						$pgBack["QTY"] = 1;
						$pgBack["AUTH_INCENTIVE_AMT"] = 15;
						$pgBack["PROCESS_ALIAS"] = "RCS-E";
						$pgBack["TOTAL"] = 15;
						$pgBack["ENR_SAVINGS"] = 0;
						$pgBack["SAVINGS_ELE"] = 0;

						
						$rowJobCounter = 0;
						foreach ($eversourceDocIds as $docId=>$data){
							$siteId = $data["fields"]["SITEID"];
							$piggyBackFeeAlreadySent = false;
							//check to see if piggyBack fee had already been sent so we can list the SiteID with an 'A' at the end
							$piggyBackFeeAlreadySent = $alreadyPiggyBackCharged[$siteId];	
							if ($piggyBackFeeAlreadySent){
								$piggyBackFeeAlreadySentCounter[] = $siteId.": ".trim($data["fields"]["CUST_FIRST_NAME"])." ".trim($data["fields"]["CUST_LAST_NAME"]);
								$siteId = $siteId."A";
							}
							
							//print_pre($data);
							//**PARTS DATA**//
							$apptDate = date("Y-m-d",strtotime($data["fields"]["APPTDATE"]));
							//echo $apptDate." | ". $dataByFieldName->APPTDATE." | ".exceldatetotimestamp($dataByFieldName->APPTDATE)." = ";
							if ($apptDate == "1969-12-31"){$apptDate = (strpos($data["fields"]["APPTDATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["APPTDATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["APPTDATE"]))));}
							$assmntDate = date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"]));
							if ($assmntDate == "1969-12-31"){$assmntDate = (strpos($data["fields"]["ASSESSMENT_DT"],"/") ? date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["ASSESSMENT_DT"]))));}
							$audEventDate = date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"]));
							if ($audEventDate == "1969-12-31"){$audEventDate = (strpos($data["fields"]["AUD_EVENT_DATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["AUD_EVENT_DATE"]))));}
							
							
							foreach ($data["isms"] as $contractNum=>$isms){
								$acceptableTypesCount = 0;
								foreach ($isms as $ismObj){
									$ism = (array) $ismObj;
									
									//acceptable types for electric piggyback
									$acceptableTypesArray = array("VW_APLNC_RCMDN","VW_LIGT_BULB_RCMDN");
									if (in_array($ism["MEASTABLE"],$acceptableTypesArray)){
										$acceptableTypesCount++;	
										//print_pre($ism);
										
										$ism["EXT_JOB_ID"] = $siteId;
										$ism["MEA_ID"] = $ism["MEASTABLE"];
										$ism["PART_ID"] = ($ism["DESCRIPTION"] == "5w TCP 2.0 LED Candle base" ? "005_TCP_TORP_LED_2030.101" : $ism["INS_PARTID"]);
										$ism["QTY"] = $ism["INS_QTY"];
										$ism["ENR_SAVINGS"] = bcmul($ism["MBTU_SAVINGS"],1000,0);
										$ism["SAVINGS_ELE"] = $ism["MBTU_SAVINGS"];
										$ism["SAVINGS_UNIT"] = "BTU";
										$ism["MEASURE_FUEL_TYPE"] = "E";
										$ism["INSTALLED_DATE"] = date("m/d/Y",strtotime($apptDate));
										$ism["PROCESS_ALIAS"] = "RCS-E";
										$ism["AUTH_INCENTIVE_AMT"] = $ism["UTIL_PRICE"];
										$ism["TOTAL"] = $ism["UTIL_PRICE"];
										$thisPartRow = array();
										$thisTotalRow = array();
										foreach ($eversourcePartsFields as $field){
											$thisPartRow[] = $ism[$field];
										}
										$eversourcePartRows[] = $thisPartRow;
										fputs($fpPart,"\n".implode("|",$thisPartRow));
										foreach ($eversourceTotalFields as $field){
											$thisTotalRow[] = $ism[$field];
										}
										$eversourceTotalRows[] = $thisTotalRow;
										fputs($fpTotal,"\n".implode("|",$thisTotalRow));
									}
								}
								if ($acceptableTypesCount && !$piggyBackFeeAlreadySent){//if there were isms that were acceptable then include piggyback fee
									$pgBack["EXT_JOB_ID"] = $siteId;
									$pgBack["PROJECT_NAME"] = "CET Eversource Piggyback";									
									$pgBack["INSTALLED_DATE"] = date("m/d/Y",strtotime($apptDate));
									//add PiggyBack fee
									$thisPartRow = array();
									$thisTotalRow = array();
									foreach ($eversourcePartsFields as $field){
										$thisPartRow[] = $pgBack[$field];
										$hasPiggyBack[$siteId] = array("thisDate"=>date("Y-m-d",strtotime($apptDate)),"contract"=>$contractNum);
									}
									foreach ($eversourceTotalFields as $field){
										$thisTotalRow[] = $pgBack[$field];
										$hasPiggyBack[$siteId] = array("thisDate"=>date("Y-m-d",strtotime($apptDate)),"contract"=>$contractNum);
									}
									
									$eversourcePartRows[] = $thisPartRow;
									fputs($fpPart,"\n".implode("|",$thisPartRow));
									$eversourceTotalRows[] = $thisTotalRow;
									fputs($fpTotal,"\n".implode("|",$thisTotalRow));
								}

							}
							
							
							
							if ($acceptableTypesCount){
								//**JOB DATA**//
								$rowJobCounter++;
								$endOfLineJob = "\n";
								if ($rowJobCounter == $rowJobCount){
									$endOfLineJob = "";
								}
								$data["fields"]["EXT_JOB_ID"] = $siteId;
								$data["fields"]["VENDOR_CODE"] = 14327;
								$data["fields"]["PROJECT_NAME"] = "CET Eversource Piggyback";																
								$data["fields"]["EXT_INVOICE_ID"] = date("Ymt")."418W";
								$data["fields"]["DATE"] = date("m/t/Y");
								$data["fields"]["ASSESSMENT_DT"] = date("m/d/Y",strtotime($assmntDate));
								$data["fields"]["AUD_EVENT_DATE"] = date("m/d/Y",strtotime($audEventDate));
								$data["fields"]["FIRST_NAME"] = trim($data["fields"]["CUST_FIRST_NAME"]);
								$data["fields"]["LAST_NAME"] = trim($data["fields"]["CUST_LAST_NAME"]);
								$data["fields"]["EXT_ADDRESS"] = $data["fields"]["ADDRESS"];
								$data["fields"]["EXT_CITY"] = $data["fields"]["CITY"];
								$data["fields"]["EXT_STATE"] = $data["fields"]["STATE"];
								$data["fields"]["EXT_ZIP"] = $data["fields"]["ZIP"];
								$data["fields"]["OWN_RENT"] = ($data["fields"]["OWNER_OCCUPIED"] == "Renter" ? "R" : "O");
								//$data["fields"]["DHW_FUEL"] = "G";
								$data["fields"]["PRIM_HEAT_FUEL"] = "G";
								$data["fields"]["PRIM_HEAT_TYPE"] = "U";
								$data["fields"]["MAILING_FIRST_NAME"] = $data["fields"]["CUST_FIRST_NAME"];
								$data["fields"]["MAILING_LAST_NAME"] = $data["fields"]["CUST_LAST_NAME"];
								$data["fields"]["MAILING_STREET"] = $data["fields"]["ADDRESS"];
								$data["fields"]["MAILING_CITY"] = $data["fields"]["CITY"];
								$data["fields"]["MAILING_STATE"] = $data["fields"]["STATE"];
								$data["fields"]["MAILING_ZIP"] = $data["fields"]["ZIP"];
								if (!$data["fields"]["DWELL_TYPE"]){$data["fields"]["DWELL_TYPE"] = "Single Family";}
								$NumUnitsParts = explode(" ",$data["fields"]["DWELL_TYPE"]);
								
								$data["fields"]["NUM_UNITS"] = (is_numeric($NumUnitsParts[0]) ? $NumUnitsParts[0] : 1);
								$data["fields"]["SINGLE_MULTI_CD"] = "s";
								$data["fields"]["CENTRAL_AC"] = "N";
								
								
								$thisJobRow = array();
								$thisTotalRow = array();
								foreach ($eversourceJobFields as $dataField){
									//echo $dataField." = " .$data["fields"][$dataField]."<br>";
									$thisJobRow[] = $data["fields"][$dataField];
								}
								
								//added by LH below for total
								
								$exportedJobDocIds[] = $docId;
								$eversourceJobRows[] = $thisJobRow;
								fputs($fpJob, implode("|",$thisJobRow).$endOfLineJob);
								
								//added by LH to add to totals
								foreach ($eversourceTotalFields as $dataField){
									//echo $dataField." = " .$data["fields"][$dataField]."<br>";
									$thisTotalRow[] = $data["fields"][$dataField];
								}
								$exportedTotalDocIds[] = $docId;
								$eversourceTotalRows[] = $thisTotalRow;
								fputs($fpTotal, implode("|",$thisTotalRow).$endOfLineTotal);
							}else{
								$noElectricIsms[] = $docId;
							}//only if acceptable isms
						}
						fclose($fpJob);
						fclose($fpPart);
						fclose($fpTotal);
						
						$energyName = "Eversource";
						$energyType = "Jobs";
						$energyHeaderFields = $eversourceJobFields;
						$energyDataRows = $eversourceJobRows;
						include('model/createSimpleExcelFile.php');

						$energyType = "Parts";
						$energyHeaderFields = $eversourcePartsFields;
						$energyDataRows = $eversourcePartRows;
						include('model/createSimpleExcelFile.php');
						
						$energyType = "Total";
						$energyHeaderFields = $eversourceTotalFields;
						$energyDataRows = $eversourceTotalRows;
						include('model/createSimpleExcelFile.php');
						
						//save PiggyBackFees
						/*
						foreach ($hasPiggyBack as $siteId=>$info){
							$addedResults = $residentialProvider->addPiggyBack($siteId,$info["thisDate"],$info["contract"]);
							print_pre($addedResults);
						}
						*/
						
						
						echo "There were ".count($piggyBackFeeAlreadySentCounter)." already sent their PB fee <span id='alreadyPBSent' style='cursor:pointer;'>[+]</span>:<br>";
						echo "<div id='alreadyPBSentDiv' style='display:none;'>";
							foreach ($piggyBackFeeAlreadySentCounter as $pbData){
								echo $pbData."<br>";
							}
						echo "</div>";
						//print_pre($piggyBackFeeAlreadySentCounter);
						
						