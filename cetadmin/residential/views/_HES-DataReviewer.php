<?php
ini_set('memory_limit', '7048M');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
$monthToUse = (date("j", strtotime($TodaysDate)) < 15 ? date("M Y", strtotime("-1 month")) : date("M Y"));
$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : $monthToUse);
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));
$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
$zipPath = $path;
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);

//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

$extractEnergyDateEnd = ($_GET['extractEnergyDateEnd'] ? $_GET['extractEnergyDateEnd'] : date("m/1/Y"));
$extractBgasDateEnd = ($_GET['extractBgasDateEnd'] ? $_GET['extractBgasDateEnd'] : date("m/d/Y"));
$extractDoerDateEnd = ($_GET['extractDoerDateEnd'] ? $_GET['extractDoerDateEnd'] : date("m/d/Y"));

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$auditFieldResults = $residentialProvider->getResAuditFields();
$auditFieldCollection = $auditFieldResults->collection;
//Get list of active warehouse locations
foreach ($auditFieldCollection as $auditField){
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["cell"] = $auditField->cell;
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["fieldName"] = $auditField->fieldName;
}

$ezDocsResults = $residentialProvider->getEZDocs();
$ezDocsCollection = $ezDocsResults->collection;
//print_pre($ezDocsCollection);

foreach ($ezDocsCollection as $record){
	$documentId = $record->documentId;
	$createdDate = $record->createdDate;
	$status = $record->status;
	
	$ezDocs[$record->documentId][$status][] = $record;
	if ($record->documentId == "2017-08-14_Scagel_Jonathan_S20201795_P20201795"){
		//print_pre($record);
	}
}

?>
<style>
.ui-datepicker-trigger {cursor:pointer;}
.unmatched {background-color:yellow;border:1pt solid black;}
</style>
<h1>HES Data Reviewer (EZ-Doc Review)</h1>
<?php
$setTab = "Files";
	if ($isStaff){
		$TabCodes = array("EZ Docs Received"=>"Files");
	}else{
		$TabCodes = array("EZ Docs Received"=>"Files","Energy Extracts"=>"EnergyExtracFile","BGAS Extracts"=>"BGASExtracFile","DOER Extracts"=>"DOERExtracFile","EZ Docs Missing"=>"MissingEZ","EZ Doc Upload Errors"=>"UploadErrors");
	}
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
?>
	<div id='Files' class='tab-content'>
		<h1>HES Data Files (EZ-Doc)<br>
			<span style="font-size:14pt;" class='alert' id='syncDataDisplay'>Checking if Sharepoint Data is Available for Syncing...<img src="<?php echo $CurrentServer.$adminFolder;?>images/loading.gif" style="height:30px;text-align:center;"></span>
			<span style="font-size:12pt; display:none;" class='alert' id='ReSyncDataDisplay'><span id='reSynText'>Force New Data Pull From Sharepoint </span><img id="reSync" src="<?php echo $CurrentServer.$adminFolder;?>images/sync.png" style="height:30px;text-align:center;cursor:pointer;"></span>
		</h1>
		
		
		<table class="simpleTable" width="100%">
			<thead>
				<tr>
					<th nowrap=nowrap>SiteId<br>ProjectId</th>
					<th nowrap=nowrap>Appt Date</th>
					<th nowrap=nowrap>Cust Name</th>
					<th nowrap=nowrap>Energy Specialist</th>
					<th nowrap=nowrap>Status</th>
					<th nowrap=nowrap>History</th>
					<th nowrap=nowrap>EFI Status</th>
					<th nowrap=nowrap>ISM Status</th>
				</tr>
			</thead>
			<tbody>

			<?php
				foreach ($ezDocs as $docId=>$statusRecords){
					$theseRecords = array();
					$docIdParts = explode("_",$docId);
					$efiExtractedDate = "";
					$ismExtractedDate = "";
					$bgasISMExtractedDate = "";
					$bgasExtractedDate = "";
					$noElectricIsmsFlagged = false;
					$efiFlagged = false;
					$bgasFlagged = false;
					$doerFlagged = false;
					$contractorFlagged = false;
					$receivedLastMonth = false;
					$firstUploadedDate = "";
					//$doerFlagged
					foreach ($statusRecords as $status=>$records){
						foreach ($records as $record){
							//print_pre($record);
							//set DOER flags
							$createdRecordDate = $record->createdDate;
							$firstUploadedDate = ($firstUploadedDate ? : $createdRecordDate);
							$createCustomerDocumentsStatus = $record->createCustomerDocumentsStatus;
							$createCustomerDocumentsStatusText = $record->createCustomerDocumentsStatusText;
							$contractorFlagged = ($contractorFlagged ? : $record->contractorFlagged); 
							$noElectricIsmsFlagged = ($noElectricIsmsFlagged ? : $record->noElectricIsms);
							$bgasFlagged = ($bgasFlagged ? : $record->bgasFlagged);
							//$doerFlagged = ($doerFlagged ? : $record->doerFlagged);							
							//$doerFlagged = ($doerFlagged ? : strpos($record->docId,"DOER");							
							
							$efiFlagged = ($efiFlagged ? : $record->efiFlagged);
							$efiExtractedDate = (MySQLDate($efiExtractedDate) ? : $record->efiExtractedDate);
							$ismExtractedDate = (MySQLDate($ismExtractedDate) ? : $record->ismExtractedDate);
							$bgasISMExtractedDate = (MySQLDate($bgasISMExtractedDate) ? : $record->bgasISMExtractedDate);
							$bgasExtractedDate = (MySQLDate($bgasExtractedDate) ? : $record->bgasExtractedDate);
							$doerISMExtractedDate = (MySQLDate($doerISMExtractedDate) ? : $record->doerISMExtractedDate);
							$doerExtractedDate = (MySQLDate($doerExtractedDate) ? : $record->doerExtractedDate);
							$theseRecords[strtotime($record->createdDate)] = $record->status."_".$record->lastUpdatedBy;
							$dataByFieldName = json_decode($record->dataByFieldName);
							$primaryAccount = $dataByFieldName->ACCTCUST;
							$electricProvider = ($dataByFieldName->SECONDARY_PRVD == "NGRID" ? "National Grid" : (strpos(" ".strtolower($dataByFieldName->SECONDARY_PRVD),"nation") ? "National Grid" : $dataByFieldName->SECONDARY_PRVD));
							$electricAccount = $dataByFieldName->ACCTCUST2;
							//$AccountName = $dataByFieldName->AccountName;
							//$PermitFee = $dataByFieldName->PermitFee;
							//$SprayFoam = $dataByFieldName->SprayFoam;
							$energySpecialist = $dataByFieldName->AUDITOR_FIRST_NAME;
							$contractorName = ($dataByFieldName->Contractor_Name ? : "Contractor");
							$apptDate = date("Y-m-d",strtotime($dataByFieldName->APPTDATE));
							//$optOut = $spData->Customer_x0020_opting_x0020_out_;
							//echo $apptDate." | ". $dataByFieldName->APPTDATE." | ".exceldatetotimestamp($dataByFieldName->APPTDATE)." = ";
							if ($apptDate == "1969-12-31"){$apptDate = (strpos($dataByFieldName->APPTDATE,"/") ? date("Y-m-d",strtotime($dataByFieldName->APPTDATE)) : date("Y-m-d",strtotime(exceldatetotimestamp($dataByFieldName->APPTDATE))));}
							//echo $apptDate."<br>";
							$dataByMeasure = json_decode($record->dataByMeasure);
							//print_pre($dataByMeasure);
							//check to see if isms
							$thisMonth = date("m/1/Y");
							if (isset($_GET['runExtracts']) || isset($_GET['runBGASExtracts'])){
								$receivedLastMonth = false;
								if (isset($_GET['runExtracts'])){
									if (strtotime($firstUploadedDate) < strtotime($extractEnergyDateEnd)){
										$receivedLastMonth = true;
									}
								}
								if (isset($_GET['runBGASExtracts'])){
									if (strtotime($firstUploadedDate) < strtotime($extractBgasDateEnd)){
										$receivedLastMonth = true;
									}
								}
								if (isset($_GET['runDOERExtracts'])){
									if (strtotime($firstUploadedDate) < strtotime($extractDoerDateEnd)){
										$receivedLastMonth = true;
									}
								}
							}else{
								$receivedLastMonth = ($receivedLastMonth ? : (strtotime($firstUploadedDate) < strtotime($thisMonth) ? true : false));
							}
							//echo $createdRecordDate." ".strtotime($createdRecordDate)." <  ".$thisMonth." ".strtotime($thisMonth)."<br>";
							//echo $docId." ReceivedLastMonth = ".$receivedLastMonth."<br>";
							$installedToday = "Installed Today";
							
							//make sure the ismFlagged isn't just a visit
							$actualIsmCount = 0;
							if (count($dataByMeasure->$installedToday)){
								foreach ($dataByMeasure->$installedToday as $ism){
									foreach ($ism as $ismDetail){
										$actualIsmCount++;
										//checking for description type now occurs on the Energy Extract process and no longer needs to exclude visit ISMs here
										//however to make it clear on the review page there are no isms if just a visit is on the installedToday page do a check
										//if (!strpos(strtolower($ismDetail->DESCRIPTION)," visit")){ 
										//}
										$ismVisitChecker[]=strpos(strtolower($ismDetail->DESCRIPTION)," visit");
									}
								}
							}
							$isOnlyVisitIsms = end($ismVisitChecker);
							
							$ismFlagged = ($actualIsmCount ? true : false);
							//anything ismflagged that was uploaded before the start of this month and hasn't been pulled yet
								//used for testing
							//	echo "0: ".$docId." doerFlagged: ".$doerFlagged." bgasFlagged: ".$bgasFlagged." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate." bgasExtractedDate: ".$bgasExtractedDate." bgasFlagged: ".$bgasFlagged." measStatus: ".$measStatus."<hr>";
							if ($ismFlagged && !$noElectricIsmsFlagged && $status == "Current" && $ismExtractedDate == "0000-00-00" && $receivedLastMonth){
								//used for testing
						/*		if ((strpos($docId,"DOER") || strpos($docId,"Test")) && $status == "Current"){
									echo "1: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate." bgasExtractedDate: ".$bgasExtractedDate." <br>";
								} */
								
								//echo $ismExtractedDate."<Br>";
								//only process ones with an electric account
								//if($electricAccount){
									$energyProviderDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
									$energyProviderDocIds[$electricProvider][$docId]["isms"] = $dataByMeasure->$installedToday;
									$bgasDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
									$bgasDocIds[$electricProvider][$docId]["isms"] = $dataByMeasure->$installedToday;
									
									
								//}
							}
							//Bgas extract also needs just noneElectric EZ docs that haven't been extracted yet
							if ($ismFlagged && $noElectricIsmsFlagged && $status == "Current" && $ismExtractedDate == "0000-00-00"  && $bgasISMExtractedDate == "0000-00-00"){
								$bgasDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
								$bgasDocIds[$electricProvider][$docId]["isms"] = $dataByMeasure->$installedToday;
								
								/*used for testing
								if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
									echo "2: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate."<br>";
								}
								*/
							}
							//Bgas isms need isms extracted to the electric company but not yet to bgas
							if ($ismFlagged && $status == "Current" && $ismExtractedDate != "0000-00-00" && $bgasISMExtractedDate == "0000-00-00"){
								$bgasDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
								$bgasDocIds[$electricProvider][$docId]["isms"] = $dataByMeasure->$installedToday;
								/*used for testing
								if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
									echo "3: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate."<br>";
								}
								*/
							}
							//anything flagged that hasn't been pulled yet
							$airSealingFlagged = (count($dataByMeasure->Contract_AirSealing) ? true : false);
							$weatherFlagged = (count($dataByMeasure->Contract_Weather) ? true : false);
							$wifiFlagged = (count($dataByMeasure->Contract_WIFI) ? true : false);
							//$wifiFlagged = (count($dataByMeasure->Wifi_Sales_Receipt) ? true : false);
							/*used for testing
							if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
								print_pre($dataByMeasure);
							}
							*/

							if ($bgasFlagged && $status == "Current" && $bgasExtractedDate == "0000-00-00" && ($airSealingFlagged || $weatherFlagged || $wifiFlagged)){
								$bgasDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
								if ($airSealingFlagged){$bgasDocIds[$electricProvider][$docId]["AirSealing"] = $dataByMeasure->Contract_AirSealing;}
								if ($weatherFlagged){$bgasDocIds[$electricProvider][$docId]["Weather"] = $dataByMeasure->Contract_Weather;}
								if($wifiFlagged){$bgasDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Contract_WIFI;}
								//if($wifiFlagged){$bgasDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Wifi_Sales_Receipt;}
								/*used for testing
								if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
									echo "4: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate."<br>";
								}
								*/
								
							}
							if ($doerFlagged && $status == "Current" && $doerExtractedDate == "0000-00-00" && ($airSealingFlagged || $weatherFlagged || $wifiFlagged)){
								$doerDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
								if ($airSealingFlagged){$doerDocIds[$electricProvider][$docId]["AirSealing"] = $dataByMeasure->Contract_AirSealing;}
								if ($weatherFlagged){$doerDocIds[$electricProvider][$docId]["Weather"] = $dataByMeasure->Contract_Weather;}
								if($wifiFlagged){$doerDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Contract_WIFI;}
								//if($wifiFlagged){$bgasDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Wifi_Sales_Receipt;}
								/*used for testing
								if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
									echo "4: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate."<br>";
								}
								*/
								
							}
							/*if ($bgasFlagged && $status == "Current" && $bgasExtractedDate == "0000-00-00" && ($airSealingFlagged || $weatherFlagged || $wifiFlagged)){
								$bgasDocIds[$electricProvider][$docId]["fields"] = (array) $dataByFieldName;
								if ($airSealingFlagged){$bgasDocIds[$electricProvider][$docId]["AirSealing"] = $dataByMeasure->Contract_AirSealing;}
								if ($weatherFlagged){$bgasDocIds[$electricProvider][$docId]["Weather"] = $dataByMeasure->Contract_Weather;}
								if($wifiFlagged){$bgasDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Contract_WIFI;}
								//if($wifiFlagged){$bgasDocIds[$electricProvider][$docId]["WIFI"] = $dataByMeasure->Wifi_Sales_Receipt;}
								/*used for testing
								if ((strpos($docId,"Dest") || strpos($docId,"Trova")) && $status == "Current"){
									echo "4: ".$docId." ismFlagged: ".$ismFlagged." noElectricIsmsFlagged: ".$noElectricIsmsFlagged." status: ".$status." ismExtractedDate: ".$ismExtractedDate." receivedLastMonth:".$receivedLastMonth." bgasISMExtractedDate:".$bgasISMExtractedDate."<br>";
								}
								
								
							}*/
							
		
							
							$SiteID = trim($record->siteId);
							$ProjectID = trim($record->projectId);
							$EZDocsDisplayed[$SiteID.$ProjectID] = $docIdParts[2]." ".$docIdParts[1];
							$CheckedOutStatus = $record->checkedOutStatus;
						}
						//echo "testing count contract WIFI ".count($dataByMeasure->Contract_WIFI)."  testing count contract by wifi sales receipt: ".count($dataByMeasure->Wifi_Sales_Receipt);
						//var_dump ($dataByMeasure->Contract_WIFI)."<br>";
						//var_dump ($dataByMeasure->Wifi_Sales_Receipt); //nothing in here
						//echo "<br>";
						
					}
					ksort($theseRecords);
					$history = array();
					$createdDateHistory = array();
					$lastStatus = "";
					foreach ($theseRecords as $createdDate=>$status){
						$statusParts = explode("_",$status);
						$history[] = date("m/d/Y g:i a",$createdDate)." ".$statusParts[0];
						$createdDateHistory[] = $createdDate;
						if ($CheckedOutStatus == "Waiting For Review" || $CheckedOutStatus == "Reviewed"){
							$lastStatus = $statusParts[0]." by ".$statusParts[1];
						}else{
							$lastStatus = ($CheckedOutStatus ? : $statusParts[0]." by ".$statusParts[1]);
						}
					}
					array_reverse($history); //puts the most current one on top
					array_reverse($createdDateHistory); //get the most current created date on top for table row order
					$docIdDups[$SiteID.$ProjectID]["count"] = $docIdDups[$SiteID.$ProjectID]["count"]+1;
					$docIdDups[$SiteID.$ProjectID]["docId"] = $docId;
					$docIdDups[$SiteID.$ProjectID]["lastId"] = $record->id;
					
					echo "\r\n<tr ".($receivedLastMonth ? 'style=\"background-color:green;\"' : '').">
							<td nowrap=nowrap><span style='display:none;'>".$createdDateHistory[0]."</span>".($SiteID ? : "<span style='color:red'>NoSiteID</span>")."<br>".($ProjectID ? : "<span style='color:red'>No ProjectID</span>").($docIdDups[$SiteID.$ProjectID]["count"] > 1 ? "<br><button class='mergeRecords' data-siteid='".$SiteID."' data-projectid='".$ProjectID."' data-lastid='".$docIdDups[$SiteID.$ProjectID]["lastId"]."' data-docid='".$docIdDups[$SiteID.$ProjectID]["docId"]."'>Merge with duplicate<br>SiteID and ProjectID</button>" : "")."<br><img class='sync' src='".$CurrentServer.$adminFolder."images/sync.png' style='height:20px;display:none;cursor:pointer;' data-siteid='".$SiteID."' data-projectid='".$ProjectID."' data-docid='".$docId."'></td>
							<td nowrap=nowrap>".(date("m/d/Y",strtotime($apptDate)) == "12/31/1969" ? "<span style='color:red'>No Appt Date</span>" : date("m/d/Y",strtotime($apptDate)))."</td>
							<td nowrap=nowrap>".$docIdParts[2]." ".$docIdParts[1]."<br>".($primaryAccount ? : "<span style='color:red'>No Gas Account</span>").($createCustomerDocumentsStatus > 0 && $createCustomerDocumentsStatus < 100 ? "<br><span style='font-weight:bold;color:red' title='".$createCustomerDocumentsStatusText."'>Parsing Error</span>" : "").($airSealingFlagged ? "<br>AirSealing" : "").($weatherFlagged ? "<br>Weatherization" : "").($wifiFlagged ? "<br>WiFi" : "")."</td>
							<td nowrap=nowrap>".$energySpecialist."</td>
							<td nowrap=nowrap>";
							
						if(strpos(" ".$lastStatus,"Current")){
							if ($CheckedOutStatus == "Waiting For Review" || $CheckedOutStatus == "Reviewed"){
								$statusDisplay = $CheckedOutStatus;
							}
							$downloadLink = "Downloadable";
							$downloadedText = "Checked Out";
							$extraButton = "";
	
							if (MySQLDate($bgasExtractedDate)){
								$downloadLink = "Cloneable";
								$downloadedText = "Sent To Your Downloads";
								$statusDisplay = "<span id='extractedDateSpan-".$docId."'>BGAS Extracted on <span title='Click to remove' style='cursor:pointer;' class='bgasExtractDateRemover' data-docid='".$docId."'>".MySQLDate($bgasExtractedDate)."</span><span id='removeBgasExtractDate-".$docId."' style='display:none;'> <img class='BgasExtractDateRemoverButton' src='".$CurrentServer."images/redx-small.png' style='height:13px;' data-docid='".$docId."'></span></span>";
							}else{
								if (!$contractorFlagged){
									$extraButton = "<br><span id='contractorFlagSpan".$docId."'><a href='#' class='button-link contractorFlag' data-docid='".$docId."' style='font-size:9pt;'>Mark Sent To Contractor</a></span>";
								}else{
									$statusDisplay = "Sent to ".$contractorName;
									$bgasFlaggButton = "";
									if (!$bgasFlagged){
										$extraButton = "<br><span id='bgasFlagSpan".$docId."'><a href='#' class='button-link bgasFlag' data-docid='".$docId."' style='font-size:9pt;'>Flag For BGAS Extract</a></span>";
									}else{
										$statusDisplay = "Ready for BGAS Extract";
									}
								}
							}
							echo "<a class='availableButton button-link' id='".$docId."' data-docid='".$docId."' data-doctype='".$downloadLink."'>".$downloadLink."</a><span id='span".$docId."' style='display:none;font-weight:bold;font-size:12pt;' class='alert'>".$downloadedText."</span>";
							echo "<span style='display:block;font-weight:bold;font-size:10pt;' class='info'>".$statusDisplay."</span>";
							echo $extraButton;
						}else{
							echo "<span id='span".$docId."' style='display:block;font-weight:bold;font-size:12pt;' class='alert'>".$lastStatus."</span>";
						}
						echo "</td>";
						echo "<td nowrap=nowrap>".implode("<br>",array_reverse($history))."</td>";
						echo "<td nowrap=nowrap>".($efiFlagged ? (MySQLDate($efiExtractedDate) ? "Data Sent To EFI ".MySQLDate($efiExtractedDate) : "Data Not Yet Sent to EFI") : "None")."</td>";
						echo "<td nowrap=nowrap>".
							($isOnlyVisitIsms ? "No ISMs" : ($ismFlagged && !$noElectricIsmsFlagged ? (MySQLDate($ismExtractedDate) ? MySQLDate($ismExtractedDate) : "<span class='ismExtractButton'><a href='#' class='button-link ismExtract' data-docid='".$docId."' style='font-size:9pt;'>Extract ISMs</a></span>") : "No ".($noElectricIsmsFlagged ? "Electric" : "")." ISMs"))
							."<br>".$electricProvider."<br>".($electricAccount ? : "<span style='color:red'>No Electric Account</span>")
							."</td>";
							
					echo "</tr>";
					
				}
			
			?>
			</tbody>
		
		</table>

	</div>
<?php if (!$isStaff){?>

	<div id='EnergyExtracFile' class='tab-content'>
		<h1>Energy Provider Extract Files</h1>
		For EZ Docs originally uploaded before <input type="text" class="date" id="extractEnergyDateEnd" value="<?php echo $extractEnergyDateEnd;?>"><br>
		<?php
		if (isset($_GET['runExtracts'])){
			if (count($energyProviderDocIds)){
				foreach ($energyProviderDocIds as $energyCompany=>$docIds){
					if ($energyCompany == "Eversource"){
						include_once('_HES-DataReviewer_EversourceExtract.php');
					}//end if eversource
					if (substr(strtolower($energyCompany),-4) == "grid" || $energyCompany == "National Grid"){
						include_once('_HES-DataReviewer_NGRIDExtract.php');
					}//end if eversource
				}//end for each energy provider
				if (count($noElectricIsms)){
					echo "The following have no Electric Isms but will be marked so they don't show up again:";
					print_pre($noElectricIsms);
				};
				$zFileName = "EnergyExtracts/EnergyExtracts_".date("Ymt").".zip";
				$zippedFileName = $thispath.$zipPath."residential/".$zFileName;
				//echo $zippedFileName;
				//print_pre($files_to_zip);
				$zippedFiles = create_zip($files_to_zip,$zippedFileName,true);
				?>
					<div id="DownloadDiv">
						<button class='ismExtractAll'> <h3> Download extract data and mark documents</h3></button><Br><br>
						Eversource Job Count with ISMs and Electric Account Info: <?php echo count($eversourceJobRows);?><br>
						National Grid Job Count with ISMs and Electric Account Info: <?php echo count($ngridJobRows);?><br>
						<?php echo implode("",$exportFileLinks);?>
					</div>
					<br><a href="<?php echo $CurrentServer.$adminFolder."residential/".$zFileName;?>">Zip File</a>
					<iframe id="download1_iframe" style="display:none;"></iframe>
					<iframe id="download2_iframe" style="display:none;"></iframe>
					<iframe id="downloadNGRID1_iframe" style="display:none;"></iframe>
					<iframe id="downloadNGRID2_iframe" style="display:none;"></iframe>
				<?php
					
			}else{
				echo "<div class='alert'>No ISM data available to Extract</div>";
				
			}//end if counted $eversourceDocIds
			
			//$fh = fopen($CompleteFileNameTxt, 'r+') or die("can't open file");
			//$stat = fstat($fh);
			//ftruncate($fh, $stat['size']-2);
			//fclose($fh); 
		} else{//end if runExtracts set
			echo "<a id=\"createExtractFilesEnergy\" href=\"".$CurrentServer.$adminFolder."residential/?nav=HES-DataReviewer&runExtracts=true&extractEnergyDateEnd=".$extractEnergyDateEnd."#EnergyExtracFile\" class=\"button-link\">Create Extract Files</a><br>";
		}
		?>
	</div>
	<div id='BGASExtracFile' class='tab-content'>
		<h1>BGAS Extract Files</h1>
		For EZ Docs originally uploaded before <input type="text" class="date" id="extractBgasDateEnd" value="<?php echo $extractBgasDateEnd;?>"><br>
		<?php
		if (isset($_GET['runBGASExtracts'])){
			if (count($bgasDocIds)){
				//print_pre($bgasDocIds);
				//include Sharepoint data to get date fields
				
				//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
				include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
				foreach ($Views as $ID=>$GUI){
					$ArrayName = "SPRecord".$ID;
					//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
					$thisFileContents = null;
					$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
					${$ArrayName} = json_decode($thisFileContents,true);
					$thisFileContents = null;
					foreach (${$ArrayName} as $SPData){
						$spData = (object)$SPData;
						//$results[] = $cetDataProvider->insertSharePointData($spData);
						$siteId = $spData->site_x0020_id;
						//if ($siteId == "S20201978"){print_pre($spData);}
						$projectId = $spData->project_x0020_id;
						$ASIssuedDate = $spData->a_x002f_s_x0020_contracts_x0020_;
						$ASSignedDate = $spData->a_x002f_s_x0020_date_x0020_signe;
						$INSIssuedDate = $spData->contracts_x0020_issued_x0020_dat;
						$INSSignedDate = $spData->insulation_x0020_date_x0020_sign;
						$installDate = $spData->install_x0020_date;
						$extractDatesBySiteId[$siteId][$projectId]=array("ASIssuedDate"=>$ASIssuedDate,"ASSignedDate"=>$ASSignedDate,"INSIssuedDate"=>$INSIssuedDate,"INSSignedDate"=>$INSSignedDate,"InstallDate"=>$installDate);
						erase_val($SPData);
						$SPData = null;
					}//end foreach sprecord
					${$ArrayName} = null;
				}//end foreach view
				
				include_once('_HES-DataReviewer_BGASExtractV2.php');
				?>
					<div id="BGASDownloadDiv">
						<button class='BGASExtractAll'> <h3> Download extract data and mark documents</h3></button><Br><br>
						BGAS DocIds marked ready for Extract: <?php echo count($possibleBgasDocIds);?> <span id='showToBeMarked' style="cursor:pointer;">[+]</span><br>
							<div id="ToBeMarked" style="display:none;">
								<frameset>
									<legend>To be Marked</legend>
									<?php 
										foreach ($possibleBgasDocIds as $docId=>$count){
											echo $docId."<br>";
										}
									?>
								</frameset>
							</div>
						BGAS DocsIds included on Extract: <?php echo count($exportedBGASDocIds);?> <span id='showToBeIncluded' style="cursor:pointer;">[+]</span><br>
							<div id="ToBeIncluded" style="display:none;">
								<frameset>
									<legend>To be on Extract</legend>
									<?php 
										foreach ($exportedBGASDocIds as $docId=>$count){
											echo $docId."<bR>";
										}
									?>
								</frameset>
							</div>
						<?php
						//show all nonexported docs:
						if (count($docIdsNotIncluded)){
							echo "The following Doc Ids were not included on the extract:<Br>";
							foreach ($docIdsNotIncluded as $docId=>$reason){
								echo "<span style='font-weight:bold;font-size:10pt;padding-left:20px;'>".$docId.": <span class='alert'>".$reason."</span></span><Br>";
							}
						}
						?>
					</div>
					<?php echo $bgasExportFileLink;?>
					
					<iframe id="downloadBgas_iframe" style="display:none;"></iframe>
				<?php
			}else{
				echo "<div class='alert'>No BGAS data available to Extract</div>";
				
			}//end if counted $bgasDocIds
		} else{//end if runBGASExtracts set
			echo "<a id=\"createExtractFilesBgas\" href=\"".$CurrentServer.$adminFolder."residential/?nav=HES-DataReviewer&runBGASExtracts=true&extractBgasDateEnd=".$extractBgasDateEnd."#BGASExtracFile\" class=\"button-link\">Create Extract Files</a><br>";
		}
		?>
	</div>
	<div id='DOERExtracFile' class='tab-content'>
		<h1>DOER Extract Files</h1>
		For EZ Docs originally uploaded before <input type="text" class="date" id="extractDOERDateEnd" value="<?php echo $extractDOERDateEnd;?>"><br>
		<?php
		if (isset($_GET['runDOERExtracts'])){
			if (count($doerDocIds)){
				//print_pre($doerDocIds);
				//include Sharepoint data to get date fields
				
				//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
				include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
				foreach ($Views as $ID=>$GUI){
					$ArrayName = "SPRecord".$ID;
					//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
					$thisFileContents = null;
					$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
					${$ArrayName} = json_decode($thisFileContents,true);
					$thisFileContents = null;
					foreach (${$ArrayName} as $SPData){
						$spData = (object)$SPData;
						//$results[] = $cetDataProvider->insertSharePointData($spData);
						$siteId = $spData->site_x0020_id;
						//if ($siteId == "S20201978"){print_pre($spData);}
						$projectId = $spData->project_x0020_id;
						$ASIssuedDate = $spData->a_x002f_s_x0020_contracts_x0020_;
						$ASSignedDate = $spData->a_x002f_s_x0020_date_x0020_signe;
						$INSIssuedDate = $spData->contracts_x0020_issued_x0020_dat;
						$INSSignedDate = $spData->insulation_x0020_date_x0020_sign;
						$installDate = $spData->install_x0020_date;
						$extractDatesBySiteId[$siteId][$projectId]=array("ASIssuedDate"=>$ASIssuedDate,"ASSignedDate"=>$ASSignedDate,"INSIssuedDate"=>$INSIssuedDate,"INSSignedDate"=>$INSSignedDate,"InstallDate"=>$installDate);
						erase_val($SPData);
						$SPData = null;
					}//end foreach sprecord
					${$ArrayName} = null;
				}//end foreach view
				
				include_once('_HES-DataReviewer_DOERExtract.php');
				?>
					<div id="DOERDownloadDiv">
						<button class='DOERExtractAll'> <h3> Download extract data and mark documents</h3></button><Br><br>
						DOER DocIds marked ready for Extract: <?php echo count($possibleDOERDocIds);?> <span id='showToBeMarked' style="cursor:pointer;">[+]</span><br>
							<div id="ToBeMarked" style="display:none;">
								<frameset>
									<legend>To be Marked</legend>
									<?php 
										foreach ($possibleDOERDocIds as $docId=>$count){
											echo $docId."<br>";
										}
									?>
								</frameset>
							</div>
						DOER DocsIds included on Extract: <?php echo count($exportedDOERDocIds);?> <span id='showToBeIncluded' style="cursor:pointer;">[+]</span><br>
							<div id="ToBeIncluded" style="display:none;">
								<frameset>
									<legend>To be on Extract</legend>
									<?php 
										foreach ($exportedDOERDocIds as $docId=>$count){
											echo $docId."<bR>";
										}
									?>
								</frameset>
							</div>
						<?php
						//show all nonexported docs:
						if (count($docIdsNotIncluded)){
							echo "The following Doc Ids were not included on the extract:<Br>";
							foreach ($docIdsNotIncluded as $docId=>$reason){
								echo "<span style='font-weight:bold;font-size:10pt;padding-left:20px;'>".$docId.": <span class='alert'>".$reason."</span></span><Br>";
							}
						}
						?>
					</div>
					<?php echo $doerExportFileLink;?>
					
					<iframe id="downloadDOER_iframe" style="display:none;"></iframe>
				<?php
			}else{
				echo "<div class='alert'>No DOER data available to Extract</div>";
				
			}//end if counted $doerDocIds
		} else{//end if runDOERExtracts set
			echo "<a id=\"createExtractFilesDoer\" href=\"".$CurrentServer.$adminFolder."residential/?nav=HES-DataReviewer&runDOERExtracts=true&extractDOERDateEnd=".$extractDOERDateEnd."#DOERExtracFile\" class=\"button-link\">Create Extract Files</a><br>";
		}
		?>
	</div>
		
	<div id='MissingEZ' class='tab-content'>
		<h1>EZ Docs awaiting upload</h1>
		<?php
			$criteria = new stdClass();
			$criteria->electricProviderNot = array("WGE","BLOCKED");
			$criteria->uploadedDate = "0000-00-00";
			$criteria->statusNot = "Cancelled";
			$criteria->visitTypes = array("HEA","HEAAU","SHV","DI VISIT");
			$criteria->apptDateBefore = date("Y-m-d",strtotime($TodaysDate));
			$scheduledResults = $residentialProvider->getScheduledData($criteria);
			//print_pre($scheduledResults);
			foreach ($scheduledResults->collection as $record){
				//print_pre($record);
				$siteId = trim($record->siteId);
				$projectId = trim($record->projectId);
				//echo trim($siteId.$projectId)."=".$record->customerName."<br>";
				if (!$EZDocsDisplayed[$siteId.$projectId]){
					$missingEZDocs[] = $record;
				}
			}
			echo count($missingEZDocs)." items missing<br>";
		?>
		<table class="simpleTable">
			<thead>
				<tr>
					<th>SiteId<br>ProjectId</th>
					<th nowrap=nowrap>Appt Date</th>
					<th nowrap=nowrap>Cust Name</th>
					<th nowrap=nowrap>Energy Specialist</th>
					<th nowrap=nowrap>Visit Type</th>
					<th nowrap=nowrap>Electric Provider</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach ($missingEZDocs as $ezDoc){
						echo "<tr>
							<td>".$ezDoc->siteId."<br>".$ezDoc->projectId."</td>
							<td>".date("m/d/Y", strtotime($ezDoc->apptDate))."</td>
							<td>".$ezDoc->customerName."</td>
							<td>".$ezDoc->energySpecialist."</td>
							<td>".$ezDoc->visitType."</td>
							<td>".$ezDoc->electricProvider."</td>
							</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
	<div id='UploadErrors' class='tab-content'>
		<h1>EZ Docs Upload Errors</h1>
		
		<?php
			$criteria = new stdClass();
			$errorResults = $residentialProvider->getEZDocUploadErrors($criteria);
			if (count($errorResults->collection)){
				foreach ($errorResults->collection as $record){
					$UploadErrors[] = $record;
				}
		?>
				<table class="simpleTable">
					<thead>
						<tr>
							<th nowrap=nowrap>Uploaded Date</th>
							<th nowrap=nowrap>Uploaded By</th>
							<th nowrap=nowrap>DocumentID</th>
							<th nowrap=nowrap>Error Message</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach ($UploadErrors as $record){
								$message = json_decode($record->ResidentialEZDocUploadError_Message);
								echo "<tr>
									<td><span style='display:none;'>".strtotime($record->ResidentialEZDocUploadError_TimeStamp)."</span>".date("m/d/Y",strtotime($record->ResidentialEZDocUploadError_TimeStamp))."</td>
									<td>".$record->ResidentialEZDocUploadError_UploadedBy."</td>
									<td>".$record->ResidentialEZDocUploadError_DocumentID."</td>
									<td>".implode("<br>",$message)."</td>
									</tr>";
							}
						?>
					</tbody>
				</table>
			<?php }else{
				echo "Hurray No Errors";
			}?>
	</div>
<?php }//end if not staff?>
</div>
<script type="text/javascript">
	$(function () {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#extractEnergyDateEnd").datepicker();
		
		$("#extractEnergyDateEnd").on('change',function(){
			var $this = $(this),
				thisVal = $this.val(),
				createExtractFilesEnergy = $("#createExtractFilesEnergy");
				thisHref = "<?php echo $CurrentServer.$adminFolder;?>residential/?nav=HES-DataReviewer&runExtracts=true&extractEnergyDateEnd="+thisVal+"#EnergyExtracFile";
				createExtractFilesEnergy.attr('href',thisHref);
		});
		$("#extractBgasDateEnd").datepicker({
			maxDate:0
		});
		$("#extractBgasDateEnd").on('change',function(){
			var $this = $(this),
				thisVal = $this.val(),
				createExtractFilesBgas = $("#createExtractFilesBgas");
				thisHref = "<?php echo $CurrentServer.$adminFolder;?>residential/?nav=HES-DataReviewer&runBGASExtracts=true&extractBgasDateEnd="+thisVal+"#BGASExtracFile";
				createExtractFilesBgas.attr('href',thisHref);
		});
		
		$("#showToBeMarked").on('click',function(){
			$("#ToBeMarked").toggle();
		});
		$("#showToBeIncluded").on('click',function(){
			$("#ToBeIncluded").toggle();
		});
		$("#showToBeExcluded").on('click',function(){
			$("#ToBeExcluded").toggle();
		});
		$("#alreadyPBSent").on('click',function(){
			$("#alreadyPBSentDiv").toggle();
		});
		$('.bgasExtractDateRemover').on('click',function(){
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				thisSpan = "#removeBgasExtractDate-"+thisDocId;
			$(thisSpan).toggle();
		});
		
		$(".BgasExtractDateRemoverButton").on('click',function(){
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				thisSpan = "#extractedDateSpan-"+thisDocId;
				thisXSpan = "#removeBgasExtractDate-"+thisDocId;
				thisCloneableButton = "#"+thisDocId;
				var remove = confirm('This will remove the Bgas Extracted Date and cause this document to be included in the next Bgas Extract');
				if (remove){
					$this.attr('src', '<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>');
					var	data = {};
					data["action"] = "removeBgasExtractDate";
					data["docId"] = thisDocId;
					$.ajax({
						url: "ApiResidentialManagement.php",
						type: "POST",
						data: JSON.stringify(data),
						success: function(resultdata){
							$(thisSpan).html('BGAS Extracted Date Removed.<Br>Ready to be extracted again').addClass('alert');
							$(thisCloneableButton).html('<span class="ui-button-text">Downloadable</span>');
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
							$(thisSpan).show().html(message).addClass("alert");
						}
					});					
				}else{
					$(thisXSpan).toggle();
				}
		});
		
		$.ajax({
			url: "getSharePointData.php",
			type: "POST",
			success: function(resultdata){
				console.log(resultdata);
				$(".sync").show();
				$("#syncDataDisplay").hide();
				$("#ReSyncDataDisplay").show();
			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				$(thisAlert).show().html(message).addClass("alert");
			}
		});
		$("#reSync").on('click',function(e){
			var $this = $(this);
				$this.attr('src', '<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>');
				$("#reSynText").html('Downloading Sharepoint Data...');
				$(".sync").hide();
				var data = {};
				data["cleanDataRequest"] = true;
				$.ajax({
					url: "getSharePointData.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
						console.log(resultdata);
						$this.attr('src', '<?php echo $CurrentServer.$adminFolder."images/sync.png";?>');
						$("#reSynText").html('Force New Data Pull From Sharepoint ');
						$(".sync").show();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
				
		});
		
		$(".sync").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				thisSiteId = $this.attr('data-siteid'),
				thisProjectId = $this.attr('data-projectid');
			$this.attr('src', '<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>');
				var data = {};
				data["siteId"] = thisSiteId;
				data["projectId"] = thisProjectId;
				data["docId"] = thisDocId;
				$.ajax({
					url: "getSharePointData.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
						//console.log(resultdata);
						$this.attr('src', '<?php echo $CurrentServer.$adminFolder."images/sync.png";?>');
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log(jqXHR.responseText);
					}
				});
			

		});
		$(".mergeRecords").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisSiteId = $this.attr('data-siteid'),
				thisProjectId = $this.attr('data-projectid'),
				thisLastId = $this.attr('data-lastid'),
				thisDocId = $this.attr('data-docid');
				
			var	data = {};
				
				data["action"] = "mergeRecords";
				data["siteId"] = thisSiteId;
				data["projectId"] = thisProjectId;
				data["docId"] = thisDocId;
				data["lastId"] = thisLastId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
				
			
		});
		$(".availableButton").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				thisDocType = $this.attr('data-doctype'),
				thisSpanId = "#span"+thisDocId;
				
			var	data = {};
				
				data["action"] = "checkoutEZDoc";
				data["docId"] = thisDocId;
				data["docType"] = thisDocType;
				data["checkedOutBy"] = "<?php echo $_SESSION["AdminAuthObject"]["adminFullName"];?>";
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							window.location.href = '<?php echo $CurrentServer.$adminFolder;?>residential/HESDataFiles/'+thisDocId+'.xlsm';
							$this.hide();
							$(thisSpanId).show();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		$(".bgasFlag").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				spanResults = "#bgasFlagSpan"+thisDocId;
				
			var	data = {};
				data["action"] = "bgasFlagged";
				data["docId"] = thisDocId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							console.log(resultdata);
							$(spanResults).html("Flagged For Bgas Export");
							//location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		$(".contractorFlag").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisDocId = $this.attr('data-docid'),
				spanResults = "#contractorFlagSpan"+thisDocId;
				
			var	data = {};
				data["action"] = "contractorFlagged";
				data["docId"] = thisDocId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							console.log(resultdata);
							$(spanResults).html("Marked Sent to Contractor");
							//location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		$(".ismExtract").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisDocId = $this.attr('data-docid')
				
			var	data = {};
				
				data["action"] = "ismExtract";
				data["docId"] = thisDocId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							console.log(resultdata);
							//location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		$(".ismExtractAll").on('click',function(e){
			e.preventDefault();
			document.getElementById('download1_iframe').src = '<?php echo $CurrentServer.$adminFolder."residential/".$zFileName;?>';
			var piggyBacks = {};
				piggyBacks['piggyBackData'] = <?php echo json_encode($hasPiggyBack);?>;
			var	data = {};
			data["action"] = "ismExtractAll";
			data["docIds"] = new Array("<?php echo implode("\",\"",$exportedJobDocIds);?>");
			data["noElectricIsms"] = new Array("<?php echo implode("\",\"",$noElectricIsms);?>");
			data["piggyBacks"] = piggyBacks;
			$.ajax({
				url: "ApiResidentialManagement.php",
				type: "POST",
				data: JSON.stringify(data),
				success: function(resultData){
						console.log(resultData);
						$("#DownloadDiv").html('<span class="info">Successfully Downloaded Files and Marked Items as Exported</span>');
						$(".ismExtractButton").html('<?php echo date("m/d/Y");?>');
				
						//location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$(thisAlert).show().html(message).addClass("alert");
				}
			});
		});
		$(".bgasExtractAll").on('click',function(e){
			e.preventDefault();
			document.getElementById('downloadBgas_iframe').src = '<?php echo $CurrentServer.$adminFolder."residential/".$bgasExportFile;?>';
			var	data = {};
			data["action"] = "bgasExtractAll";
			data["ISMsDocIds"] = new Array("<?php echo implode("\",\"",array_keys($exportedBGASISMDocIds));?>");
			data["MeasureDocIds"] = new Array("<?php echo implode("\",\"",array_keys($exportedBGASMeasureDocIds));?>");
			console.log(data);
			$.ajax({
				url: "ApiResidentialManagement.php",
				type: "POST",
				data: JSON.stringify(data),
				success: function(resultData){
						console.log(resultData);
						$("#DownloadDivBGAS").html('<span class="info">Successfully Downloaded BGAS Extract File and Marked Items as Exported</span>');
						$(".bgasExtractButton").html('<?php echo date("m/d/Y");?>');
				
						//location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$(thisAlert).show().html(message).addClass("alert");
				}
			});
		});
		
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
			"scrollY": '1000px',
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			//"iDisplayLength": 100,
			"paging":   false,
			"ordering": true,
			"scrollCollapse": true,
			"info":     false,
			"aaSorting": [[ 0, "desc" ]]
		});
	
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
?>