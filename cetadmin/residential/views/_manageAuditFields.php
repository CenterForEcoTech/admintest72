<link rel="stylesheet" href="<?php echo $CurrentServer.$adminFolder;?>css/jquery.fileupload-ui.css">
<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");

$residentialProvider = new ResidentialProvider($dataConn);
$paginationResult = $residentialProvider->getResAuditFields();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AuditFieldByID[$record->id] = $record;
	$AuditFieldBySheetName[$record->sheet][] = $record;
	$AuditFieldByDisplayName[$record->sheet] = $record->sheet;
}
foreach($AuditFieldBySheetName as $SheetName=>$cellInfo){
	$SheetCellDisplay .= "<div class='SheetName' id='".str_replace(" ","",$SheetName)."' style='display:none;'><span class='SheetNameSpan'>".$SheetName."</span><br>";
		$thisSheetCellDisplay = "";
		foreach ($cellInfo as $record){
			$thisID = $record->id;
			$thisSheetName = $record->sheet;
			$cell = $record->cell;
			$fieldName = $record->fieldName;
			$fieldSource = $record->fieldSource;
			$thisSheetCellDisplay = $thisSheetCellDisplay."<span id='cellList".$thisID."'>
				<img src='".$CurrentServer."images/redx-small.png' style='height:10px;cursor:pointer;' class='removeCell' data-cellid='".$thisID."' data-cellname='".trim($cell)."' data-sheetname='".$thisSheetName."'> ".str_replace("_",", ",trim($cell)).": ".$fieldName.($fieldSource ? " [".$fieldSource."]" : "")."<br></span>\r\n";
		}
		$SheetCellDisplay .= "<div class='row template-upload'>
				<div class='six columns'>Cells To Capture Data From:<div id='cellListDiv".str_replace(" ","",$SheetName)."' style='padding-left:20px;'>".$thisSheetCellDisplay."</div></div>
			</div>";
		
	$SheetCellDisplay .= "</div>";	
}
?>
<style>
	.SheetName {border:1pt dashed red;}
	.SheetNameSpan {font-weight:bold;}
</style>

<?php
ksort($AuditFieldByDisplayName);
echo "<select id='sheetNameSelector'><option value=''>Select Sheet Name</option>";
foreach ($AuditFieldByDisplayName as $auditFieldSheetName=>$sheetName){
	echo "<option value='".str_replace(" ","",$sheetName)."'>".$auditFieldSheetName."</option>";
}
	echo "<option value='New Sheet'>New Sheet</option>";
echo "</select>";
?>
<br clear="all">
<div class="twenty columns" id='addCellFields' style='display:none;'>
	<div class="row">
		<div id="newSheetNameDiv" style="display:none;" class="ten columns"><div class="four columns" style='text-align:right;'>Sheet Name:</div><input type="text" name="sheet" value="" id="newSheetName" class="four columns"></div>
		<input type="hidden" id="newSheetTrue" value="false">
	</div>
	<div class="row">
		<div class="four columns" style='text-align:right;'>New Cell:<br><span style='font-size:10pt;'>(ColumnLetter RowNumber)</span></div><input type="text" id="newCell" class="one column">
		<div class="two columns" style='text-align:right;'>FieldName:</div><input type="text" id="newFieldName" class="two columns">
		<div class="two columns" style='text-align:right;'>Source:</div><input type="text" id="newFieldSource" class="two columns" value="EZ Doc">
		<div class="two columns"><button class='btn btn-success addCellButton'>Add</button></div>
	</div>
</div>
<br clear="all">
<?php echo $SheetCellDisplay; ?>
<br clear='all'>
Instructions:<br>
<ol>
	<li>1. Use the pulldown to select the Sheet you want to adjust cell capture information for
	<li>2. To add a cell to the sheet, click the green 'Add Cell' button for that sheet and then use the fields that will appear at the top of the page to enter in cell and fieldName
	<li>3. To adjust the cell or fieldName, make the adjustments then click the 'Change' button
<ol>
<script type="text/javascript">
	$(function () {
		$("#sheetNameSelector").on('change',function(){
			$(".SheetName").hide();
			var $this = $(this),
				sheetName = "#"+$this.val();
			if ($this.val() == "New Sheet"){
				$("#newSheetNameDiv").show();
				$("#newSheetName").val('');
				$("#newSheetTrue").val('True');
			}else{
				$("#newSheetNameDiv").hide();
				$("#newSheetName").val($("#sheetNameSelector option:selected").text());
				$("#newSheetTrue").val('False');
			}
			$("#addCellFields").show();
			$(sheetName).show();
		});
		$("#newCell").keyup(function(){
			this.value = this.value.toUpperCase();
		});
		$(".removeCell").on('click',function(){
			var $this = $(this),
				thisId = $this.attr('data-cellid'),
				thisCell = $this.attr('data-cellname'),
				thisSheet = $this.attr('data-sheetname'),
				thisCellList = "#cellList"+thisId;
			var thisconfirm = confirm("Are you Sure you want to stop capturing data from cell "+thisCell+" from tbe '"+thisSheet+"' sheet?");
			if (thisconfirm){
				data = {};
				
				data["action"] = "removeCell";
				data["id"] = thisId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						if (data.results["success"]){
							$(thisCellList).hide();
						}else{
							console.log(data);
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
			}
			
		});
		$(".addCellButton").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisSheetName = $("#newSheetName").val(),
				thisCell = $("#newCell").val(),
				thisFieldName = $("#newFieldName").val(),
				thisFieldSource = $("#newFieldSource").val(),
				data = {};
			data["action"] = "newCell";
			data["sheet"] = thisSheetName;
			data["cell"] = thisCell;
			data["fieldName"] = thisFieldName;
			data["fieldSource"] = thisFieldSource;
			$.ajax({
				url: "ApiResidentialManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					if (data.results["success"]){
						var newcellid = data.results["record"].id;
						var cellListDiv = "#cellListDiv"+thisSheetName.replace(/ /g,'');
						var newCellData = "<span id='cellList"+newcellid+"'> "+thisCell+": "+thisFieldName+" "+thisFieldSource+"<br></span>";
						if ($("#newSheetTrue").val() == "True"){
							location.reload();
						}else{
							$(cellListDiv).append(newCellData);
						}
						$("#newCell").val('');
						$("#newFieldName").val('');
						$("#newFieldSource").val('EZ Doc');
						$("#newSheetNameDiv").hide();
						$("#newSheetTrue").val('False');
						
					}else{
						console.log(data);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					console.log(message);
				}
			});
		});

	});
</script>