<?php
		$siteId = $spData->site_x0020_id;
		$asBillDate = $spData->utility_x0020_billing_x0020_date;
		$asBillYearMonth = date("Y_m",strtotime($asBillDate));
		$insBillDate = $spData->insulation_x0020_billing_x0020_d;
		$insBillYearMonth = date("Y_m",strtotime($insBillDate));
		$jobName = ucwords(strtolower($spData->first_x0020_name." ".$spData->last_x0020_name));
		$newParticipant[$jobName][] = $siteId;
		if ($asBillYearMonth == $yearMonth || $insBillYearMonth == $yearMonth){
			$data = array();
			$final_airsealing = $spData->final_x0020__x0024_;
			$final_insulation = $spData->insulation_x0020_final_x0020_con;
			$data["firstName"] = $spData->first_x0020_name;
			$data["lastName"] = $spData->last_x0020_name;
			$data["asFinal"] = $final_airsealing;
			$data["insFinal"] = $final_insulation;
			
			$data["jobName"] = $jobName;
			$contractor = $spData->contractor;
			$data["installationContractor"] = $contractor;
			$noFinal = 0;
			if ($final_airsealing){
				$data["jobType"] = "airSealing";
				$data["customerCost"] = 0;
				$data["bgasCost"] = $final_airsealing;
				$data["asBgas"] = $final_airsealing;
				$data["totalCost"] = $final_airsealing;
				if (!$final_insulation){
					$data["permit"] = $spData->permitoninvoice;
					$data["totalToPay"] = "JustAirSealing";
				}
				$reqData[$contractor][$jobName][$siteId][] = $data;
			}else{
				$noFinal++;
			}
			if ($final_insulation){
				$data["jobType"] = "insulation";
				$data["permit"] = $spData->permitoninvoice;
				$data["totalCost"] = $final_insulation;
				$data["deposit"] = $spData->deposit_x0020_amount;
				$nonincentivecontractamount = $spData->nonincentivecontractamount;
				
				//if whole building incentive or moderateIncome true then bgas pays 90% and customer pays 10%
					$moderateIncome = $spData->mi_x0020_accepted; //just needs to see if was marked accepted
					//$moderateIncome  = $spData->moderate_x0020_income;
					$wholeBuildingIncentive = $spData->whole_x0020_bldg_x0020_incentive;
				if ($wholeBuildingIncentive || $moderateIncome){
					$bgasAmount = bcmul(($final_insulation-$nonincentivecontractamount),0.90,3);
					if ($bgasAmount > (int)$Config_incentiveCapHigh){
						$bgasAmount = (int)$Config_incentiveCapHigh;
					}
				}else{
					$bgasAmount = bcmul(($final_insulation-$nonincentivecontractamount),0.75,3);
					if ($bgasAmount > (int)$Config_incentiveCapBase){
						$bgasAmount = (int)$Config_incentiveCapBase;
					}
				}
				$bgasAmount = round($bgasAmount,2,PHP_ROUND_HALF_DOWN);
				$data["insBgas"] = $bgasAmount;
				$customerCost = bcsub($final_insulation,$bgasAmount,2);
				$data["preWeatherizationIncentive"] = $spData->prewxincentiveamount;
				$data["bgasCost"] = $bgasAmount;
				$data["customerCost"] = bcsub(($customerCost+$nonincentivecontractamount),$spData->prewxincentiveamount,2);
				if ($final_airsealing){
					$data["totalToPay"] = "BothAirSealingAndInsulation";
				}else{
					$data["totalToPay"] = "JustInsulation";
				}
				
				$reqData[$contractor][$jobName][$siteId][] = $data;
			}else{
				$noFinal++;
			}
			if ($noFinal == 2){
				$unreqData[$contractor][$jobName][$siteId][] = $data;
			}
			$reconData[$contractor][$jobName][$siteId] = $data;
			$reconJobCount[$jobName] = 1;
		}//end if in timeframe