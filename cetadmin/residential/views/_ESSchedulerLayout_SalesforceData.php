<?php
		require_once '../gbs/salesforce/config.php';
		include_once('../gbs/salesforce/soap_connect.php');
		//print_pre($completed);
		
		//get the auditors from Salesforce
		$query = "SELECT Id, Name, Email__c, Phone__c, Availability_Notes__c, Agency__c FROM MMWEC_Auditor__c WHERE Active__c=true";
		//echo $query;
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_pre($response);
		foreach ($response->records as $record){
			$MMWECAuditor[$record->Id] = $record;
			$NameParts = explode(" ",$record->Name);
			
			$employeeInfo = new stdClass();
			//check to see if already a CET employeeInfo
			$notCETEmpolyee = true;
			foreach ($EmployeeByName as $id=>$eRecord){
				if (trim(strtolower($eRecord->fullName)) == trim(strtolower($record->Name))){$notCETEmpolyee = false;}
			}
			if ($notCETEmpolyee){
				$employeeInfo->id = $record->Id;
				$employeeInfo->firstName = $NameParts[0];
				$employeeInfo->lastName = $NameParts[1];
				$employeeInfo->fullName = $record->Name;
				$employeeInfo->email = $record->Email__c;
				$employeeInfo->phone = $record->Phone__c;
				$employeeInfo->availabilityNotes = $record->Availability_Notes__c;
				$employeeInfo->agency = $record->Agency__c;
				$EmployeeByName[$record->Id] = $employeeInfo;
			}
			$MMWECAuditorIdByName[$NameParts[0]." ".substr($NameParts[1],0,1)] = $record->Id;
			
			
		}
		//print_pre($MMWECAuditor);
		//print_pre($EmployeeByName);
		
		//get the Energy company from Salesforce
		$query = "SELECT Id, Name, Notes_For_Energy_Audit__c, What_to_Expect__c FROM MMWEC_Energy_Company__c";
		//echo $query;
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_pre($response);
		foreach ($response->records as $record){
			$MMWECEnergyCompany[$record->Id] = $record;
		}
		
		
		//get Contact Info
		
		$query = "SELECT Id, Name, Account.Phone, Account.Name, Account.Account_E_mail__c, Account.BillingAddress, MMWEC_Auditor__c, MMWEC_ID__c, MMWEC_Type__c, StageName,Audit_Date_Start__c, Audit_Date_Stop__c, Audit_Notes__c, Visit_Details__c,MMWEC_Energy_Company__c,Audit_Type__c FROM Opportunity WHERE RecordTypeId='0120P000000NI35QAG' AND StageName='Audit Needs to be Scheduled'";
		//echo $query;
		$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
		//print_pre($response);
		if (!count($response->records)){
				$noEventTypes[3] = "MMWEC";
		}else{
			foreach ($response->records as $record){
				$thisRecord = new stdClass();
				$thisRecord->auditor = $MMWECAuditor[$record->MMWEC_Auditor__c]->Name;

				$esParts = explode(" ",$thisRecord->auditor);
				$ES = $esParts[0]." ".substr($esParts[1],0,1);
				if (trim($ES) == ""){$ES = "Kacie D";}

				
				$thisRecord->id = $record->Id;
				$thisRecord->accountName = $record->Account->Name;
				$thisRecord->email = $record->Account->Account_E_mail__c;
				$thisRecord->phone = $record->Account->Phone;
				$thisRecord->address = $record->Account->BillingAddress->street;
				$thisRecord->city = $record->Account->BillingAddress->city;
				$thisRecord->state = $record->Account->BillingAddress->state;
				$thisRecord->zipcode = $record->Account->BillingAddress->postalCode;
				$thisRecord->startDateTime = $record->Audit_Date_Start__c; // 2018-06-29T16:00:00.000Z
				$thisRecord->endDateTime = $record->Audit_Date_Stop__c; //2018-06-29T18:00:00.000Z
				$thisRecord->auditNotes = $record->Audit_Notes__c;
				$thisRecord->MMWECID = $record->MMWEC_ID__c; //MMWEC-180230
				$thisRecord->MMWECType = $record->MMWEC_Type__c;
				$thisRecord->opportunityName = $record->Name;
				$thisRecord->stageName = $record->StageName;
				$thisRecord->visitDetails = $record->Visit_details__c;
				$thisRecord->electricProvider = $record->MMWEC_Energy_Company__c;
				$thisRecord->auditType = $record->Audit_Type__c;
				$thisStartDateTime = $thisRecord->startDateTime;
				$StartDate = date("Y-m-d",strtotime($thisStartDateTime));
				$StartDateStr = strtotime($StartDate);
				$StartDates[strtotime($StartDate)] = $StartDate;
				$thisRecord->dataSource = "MMWEC";
				$ToBeScheduled["MMWEC"][]= array("value"=>$record->Id,"label"=>$thisRecord->accountName." - ".$record->Account->BillingAddress->street." ".$record->Account->BillingAddress->city,"electricProvider"=>$MMWECEnergyCompany[$record->MMWEC_Energy_Company__c]->Name,"email"=>$record->Account->Account_E_mail__c,"mmwectype"=>$record->MMWEC_Type__c,"mmwecAuditType"=>$record->Audit_Type__c,"source"=>"Salesforce");
				/*
				$scheduledEvents["AllData_".$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
				if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
					$scheduledEvents[$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
					$noEventTypes[3] = false;
				}
				*/
			}
		}//end if records
		//print_pre($ToBeScheduled);
		//print_pre($scheduledEvents);
?>