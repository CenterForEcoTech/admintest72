<?php

			//new import that data and parse it out 
			$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
			$ImportFileName = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
			$requiredItems = array();
			echo "Using File just uploaded<br>";
			ob_flush();
			include('model/read_rawHESDataFileReader.php');
			//print_pre($dataRecordByFieldName);
			$apptDate = date("Y-m-d",strtotime($dataRecordByFieldName["APPTDATE"]));
			if ($apptDate == "1969-12-31"){$apptDate = exceldatetotimestamp($dataRecordByFieldName["APPTDATE"]);}
			$thisDocumentId = $apptDate."_".trim(str_replace("'","",$dataRecordByFieldName["NAME_LAST"]))."_".trim($dataRecordByFieldName["NAME_FIRST"])."_".trim($dataRecordByFieldName["SITEID"])."_".trim($dataRecordByFieldName["PROJECTID"]);
			$renamedFile = str_replace($dataFileName,$thisDocumentId,$target_file).".xlsm";
			rename($target_file, $renamedFile); //rename the file to the docId so we can download this particular file later.			
			//first Make sure this item isn't checked out
			$isCheckedOut = $residentialProvider->isCheckedOut($thisDocumentId);
			if ($isCheckedOut){
				//allow for the person who checked it out to upload it
				if ($isCheckedOut == "Checked Out By ".$_SESSION["AdminAuthObject"]["adminFullName"]){
					echo "Thank you for checking this back in.";
					//TODO update other docId's to checkedOutStatus = null
					$canUploadFile = true;
				}else{
					$isCloned = (strpos($isCheckedOut,"Cloned") ? true : false);
					echo ($isCloned ? "This document was last " : "This Document is currently ").str_replace("Currently","",$isCheckedOut)."<br>";
					if ($isCheckedOut == "Reviewed"){
						$showDownloadlink = true;
					}
					if ($isCheckedOut == "Waiting For Review"){
						$showDownloadlink = true;
					}
					if ($showDownloadlink){
						echo "If you want to make any other changes, <a href='?nav=HES-DataReviewer' class='button-link'>Download</a> the previously uploaded document, make any changes, then upload it to check the document back in.<br><br><Br>";
					}else{
						if ($isCloned){
							echo "You must change the Project ID in the document to something different<br><span class='alert'>Your document was not uploaded.</span><Br><Br>";
						}else{
							echo "Once they have checked the document back in by re-uploading it, then you will be able to download that document<br><br><bR>";
						}
					}
					$canUploadFile = false;
				}
			}else{
				$canUploadFile = true;
			}
			if ($canUploadFile){
				$checkedOutStatus = "Waiting For Review";
				$EFIExtractedDate = "0000-00-00";
				$ISMExtractedDate = "0000-00-00";
				//print_pre($dataRecordBySheet);
				//check to see if this is a new document or one being checked back in
				$criteria = new stdClass();
				$criteria->documentId = $thisDocumentId;
				$existsEZDocResults = $residentialProvider->getEZDocs($criteria);
				if (count($existsEZDocResults->collection)){
					$checkedOutStatus = "Reviewed";
					foreach ($existsEZDocResults->collection as $result){
						if ($result->EFIExtractedDate != "0000-00-00"){
							$EFIExtractedDate = $result->efiExtractedDate;
						}
						if ($result->ISMExtractedDate != "0000-00-00"){
							$ISMExtractedDate = $result->ismExtractedDate;
						}
					}
					
				}
				
				$uploadedAble = true;	
				//items required for upload
				if (!$siteId){
					$requiredItems[] = "SiteID is missing";
					$uploadedAble = false;
				}
				if (!$projectId){
					$requiredItems[] = "ProjectID is missing";
					$uploadedAble = false;
				}
				
				//check if documents were created
				if (strtotime(str_replace(".","-",$ezDocVersionDate)) >= strtotime("7/31/2017")){
					if ($createCustomerDocumentsStatus == "0"){
						$requiredItems[] = "Customer Documents were not created.  Reopen the document and click the button for 'Create Customer Documents' and after it has completed, save the document and re-upload";
						$uploadedAble = false;
					}
				}else{
					if ($_GET['documentVerified'] != "true"){
						$requiredItems[] = "You are using a older version of the EZ Doc.  Verify you have run the 'Create Customer Documents' from the Customer Info tab of the EZ Doc before uploading.  If you have verified, <button class='documentVerified' datafile='".str_replace($siteRoot.$adminFolder."residential/","",$renamedFile)."'>Click Here</button> otherwise open the EZ doc and click the 'Create Customer Documents' button and after it has completed, save the document and re-upload";
						$uploadedAble = false;
					}
				}
				
				if ($uploadedAble){
					$record = new stdClass();
					$record->documentId = $thisDocumentId;
					$record->siteId = $siteId;
					$record->projectId = $projectId;
					$record->createdBy = $_SESSION["AdminID"];
					$record->status = "Current";
					$record->checkedOutStatus = $checkedOutStatus;
					$record->lastUpdatedTimeStamp = date("Y-m-d H:i:s");
					$record->dataByFieldName = json_encode($dataRecordByFieldName);
					$record->dataBySheet = json_encode($dataRecordBySheet);
					$record->dataByMeasure = json_encode($measureDataClean);
					$record->efiFlag = $efiFlag;
					$record->efiExtractedDate = $EFIExtractedDate;
					$record->ismExtractedDate = $ISMExtractedDate;
					$record->createCustomerDocumentsStatus = $createCustomerDocumentsStatus;
					$record->createCustomerDocumentsStatusText = $createCustomerDocumentsStatusText;
					$record->ezDocVersionDate = $ezDocVersionDate;
					//print_pre($record);
					$insertEZDocDataResults = $residentialProvider->insertResEZDocData($record,getAdminId());
					//print_pre($insertEZDocDataResults);
					
					if ($insertEZDocDataResults->success){
						$successfullyUploaded = true;
						echo "<div class='info'>Successfully Uploaded: ".trim($dataRecordByFieldName["NAME_LAST"]).", ".trim($dataRecordByFieldName["NAME_FIRST"])."</div><br>";
						if ($_GET['useUploadedFile']){
							echo "<div>If you had errors uploading otherfiles you will need to upload them one at a time.</div><br>";
						}
					}else{
						echo "<div class='alert'>There was an error and document was not uploaded<br>".print_pre($insertEZDocDataResults->error)."</div><br>";
					}
					
					if (count($measureDataClean)){
						echo "<br>Contract and Measure data received for ".trim($dataRecordByFieldName["NAME_FIRST"])." ".trim($dataRecordByFieldName["NAME_LAST"])." SiteID:".trim($dataRecordByFieldName["SITEID"])." ProjectID:".trim($dataRecordByFieldName["PROJECTID"])." ApptDate:".date("m/d/Y",strtotime($apptDate))."<br>";
						foreach ($measureDataClean as $ContractType=>$contractInfo){
							echo "<fieldset><legend>".$ContractType."</legend>";
							echo "<table><tr><th>Qty</th><th>Description</th></tr>";
							foreach ($contractInfo as $contract=>$rows){
								foreach ($rows as $row){
									echo "<tr><td align='right'>".$row["INS_QTY"]. "</td><td align='left'>".$row["DESCRIPTION"]."</td></tr>";
								}
							}
							echo "</table></fieldset><br clear='all'>";
						}
					}
				}else{
					$message = json_encode($requiredItems);
					$insertEZDocDataResults = $residentialProvider->insertEZDocUploadError($thisDocumentId,$message,getAdminId());
					//print_pre($insertEZDocDataResults);					
					echo "<div class='alert'>ERROR: File was not uploaded due to the following reasons<br>".implode("<br>",$requiredItems)."</div>";
				}


			}//end canUploadFile
			
?>