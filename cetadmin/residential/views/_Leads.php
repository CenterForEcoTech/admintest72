<?php
ini_set('memory_limit', '7048M');
//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
//print_pre($auditFieldsBySheet);
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
.unmatched {background-color:yellow;border:1pt solid black;}
</style>
<h1>Leads Importer (From Energy Savvy Survey)</h1>
		<?php
		//echo "ImportFileName ".$ImportFileName."<br>";
		//echo $target_file;
		if(count($_FILES['fileToUpload1'])) {
				//print_pre($_FILES);
				$ImportFileName = $_FILES["fileToUpload1"]["tmp_name"][0];
				ob_flush();
				
				$headerNames = array(); //clear out from last run
				$headerNumbers = array(); //clear out from last run
				$rowsRaw = array(); //clear out from last run
				$rowsClean = array(); //clear out from last run

				$csv = array_map('str_getcsv', file($ImportFileName));
				$headerRaw = array_shift($csv);
				foreach ($headerRaw as $thisRow){
					$headerRow[] = str_replace(" (enrollments)","_enrollments",str_replace(" (people)","_people",str_replace(" (premises)","_premises",str_replace(" (residential-surveys)","_residential-surveys",str_replace(" (UTC)","",$thisRow))))); 
				}
				
				foreach ($csv as $rowID=>$rowData){
					$cleanedRow = array();
					foreach ($rowData as $col=>$val){
						$cleanedRow[$headerRow[$col]]=$val;
					}
					$cleanedRows[] = $cleanedRow;
				}
				foreach ($headerRow as $col=>$val){
					$headerNames[($col+1)]=$val;
					$headerNumbers[($col+1)]=str_replace(" ","",$val);
				}
				$rowsRaw = $cleanedRows;

				//print_pre($headerNames);
				//print_pre($rowsRaw);
				$addResults = $residentialProvider->addLeads($rowsRaw,$_SESSION["AdminID"]);
				echo "Total Records Processed: ".count($rowsRaw)."<Br>";
				if ($addResults["error"]){
					$error = explode(":",$addResults["error"]);
					echo "<div class='alert'><b>Error:</b> ".end($error)."</div>";
				}
				echo "&nbsp;&nbsp;&nbsp;&nbsp;New Leads Added: ".$addResults["success"]."<bR>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;Previous Leads Found: ".$addResults["duplicates"];
				echo "<hr>";
				ob_flush();
		}
		?>


			<a id='uploadFile' href="#" class='button-link do-not-navigate'>Upload<?php echo ($successfullyUploaded ? " Another" : "");?> File</a><br>
			<div id="uploadFileForm" style='display:none;'>
				<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>residential/?type=uploadFile&nav=<?php echo $nav;?>" enctype="multipart/form-data">
					<div class="row">
						<div class="eight columns">
							<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
						</div>
						<div class="eight columns">
							<strong>File You Selected:</strong>
							<ul id="fileList1"><li>No Files Selected</li></ul>
							<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
						</div>
					</div>
					
					<script type="text/javascript">
						function makeFileList1() {
							var input = document.getElementById("fileToUpload1");
							var ul = document.getElementById("fileList1");
							while (ul.hasChildNodes()) {
								ul.removeChild(ul.firstChild);
							}
							for (var i = 0; i < input.files.length; i++) {
								var li = document.createElement("li"),
									fileName = input.files[i].name,
									fileLength = fileName.length,
									fileExt = fileName.substr(fileLength-4);
									console.log(fileExt);
								if (fileExt == ".csv"){
									li.innerHTML = input.files[i].name;
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'block';
								}else{
									li.innerHTML = 'You must save '+fileName+' as a .csv file first';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
							if(!ul.hasChildNodes()) {
								var li = document.createElement("li");
								li.innerHTML = 'No Files Selected';
								ul.appendChild(li);
								document.getElementById('submitButton1').style.display = 'none';
							}
						}
					</script>
				</form>
			</div>
			<div id="leads">
				<?php
					$leadsResults = $residentialProvider->getLeads();
					$leads = $leadsResults->collection;
					//print_pre($leads);
					foreach ($leads as $record){
						//print_pre($record);
						$id = $record->id;
						$leadRecords[$id]["name"] = $record->first_name_people." ".$record->last_name_people;
						$leadRecords[$id]["city"] = $record->city_premises;
						$leadRecords[$id]["completed"] = strtotime($record->{"completed_at_residential-surveys"});
						$leadRecords[$id]["phone"] = $record->phone_numbers_people;
						$leadRecords[$id]["email"] = $record->email_people;
						$leadRecords[$id]["source"] = $record->{"campaign_code_residential-surveys"};
						$leadRecords[$id]["siteId"] = $record->SiteId;
						$leadRecords[$id]["fullRecord"] = $record;
					}
					//print_pre($leadRecords);
				?>
				<table class="simpleTable">
					<thead>
						<tr>
							<th nowrap=nowrap>Survey Completed</th>
							<th>Name</th>
							<th>City</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Source</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($leadRecords as $id=>$record){
								$record["completed"] = (date("m/d/Y",$record["completed"]) == "12/31/1969" ? "Not Completed" : $record["completed"]);
								echo "<tr>";
								echo "<td><span style='display:none;'>".$record["completed"]."</span>".($record["completed"] != "Not Completed" ? date("m/d/Y",$record["completed"]) : $record["completed"])."</td>";
								echo "<td>".$record["name"]."</td>";
								echo "<td>".$record["city"]."</td>";
								echo "<td>".$record["phone"]."</td>";
								echo "<td>".$record["email"]."</td>";
								echo "<td>".$record["source"]."</td>";
								echo "<td nowrap=nowrap>".($record["siteId"] ? : "<span id='notInterested".$id."'><button class='notInterested' data-id='".$id."'>Mark<Br>Not Interested</button></span>")."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>

			<script type="text/javascript">
				$(function () {
					$("#uploadFile").on('click',function(){
						$("#uploadFileForm").toggle();
					});
					$(".notInterested").on('click',function(){
						var $this = $(this),
							thisId = $this.attr('data-id'),
							thisButton = "#notInterested"+thisId,
							data = {};
						
						data["action"] = "lead_notInterested";
						data["id"] = thisId;
						console.log(data);
						$.ajax({
							url: "ApiResidentialManagement.php",
							type: "POST",
							data: JSON.stringify(data),
							success: function(data){
								console.log(data.success)
								if (data.success){
									$(thisButton).html('Not Interested');
								}
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								console.log(message);
							}
						});				
							
					});
					
					var table = $('.simpleTable').DataTable({
						"scrollX": true,
			//			"scrollY": "300px",
						"bJQueryUI": true,
						"bSearchable":true,
						"bFilter":true,
						"bAutoWidth": true,
						"bSort": true,
						"iDisplayLength": 25,
						"paging":   false,
			//			"ordering": false,
						"scrollCollapse": true,
						"info":     false
					});
					
				});
			</script>
<?php
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
?>