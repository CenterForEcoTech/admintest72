<?php
include_once($dbProviderFolder."ProductProvider.php");

$ProductProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $ProductProvider->get($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);
foreach ($resultArray as $result=>$record){
	$bulbType = ($record->bulbType ? : "LED");
	$efiPartsToRemove["_".$record->efi] = "";
	$efiParts = explode(".",$record->efi);
	if ($bulbType == "LED"){
		$crcCode = substr($efiParts[0],2,1);
	}else{
		$crcCode = "";
	}
	$CSGCode = $record->csgCode;
	$CSGCodes = array();
	if (strpos($CSGCode,",")){
		$CSGCodes = explode(",",$CSGCode);
	}else{
		$CSGCodes = array($CSGCode);
	}
	
	$clearResultsCode = $bulbType.$crcCode.$efiParts[1];
	
	$record->clearResultsCode = $clearResultsCode;
	foreach ($CSGCodes as $code){
		$ProductsByCSGCodes[$code] = $record;
	}
}
//print_pre($ProductsByCSGCodes);
						$ngridDocsIds = $docIds;
						//$ngridJobFields = array("EXT_JOB_ID","VENDOR_CODE","EXT_INVOICE_ID","DATE","FIRST_NAME","LAST_NAME","EXT_ADDRESS","EXT_APT","EXT_CITY","MDW_ACT_ID","SQUARE_FOOTAGE","AUD_EVENT_DATE","OWN_RENT","DWELL_TYPE","DHW_FUEL","PRIM_HEAT_FUEL","PRIM_HEAT_TYPE","DHW_TYPE","DHW_SIZE","YEAR_BUILT","EXT_STATE","EXT_ZIP","MAILING_FIRST_NAME","MAILING_LAST_NAME","MAILING_STREET","MAILING_ADDRESS2","MAILING_CITY","MAILING_STATE","MAILING_ZIP","MAILING_HOME_PHONE","MAILING_WORK_PHONE","EMAIL","OTHER PA","AUDITOR","NUM_UNITS","SINGLE_MULTI_CD","SEC_HEAT_FUEL","SEC_HEAT_TYPE","HTG_DIST_TYPE","INSTALL_DHW_TYPE","INSTALL_DHW_SIZE","TEMP_SET_EXIST","TEMP_SET_NEW","CENTRAL_AC","ROOM_AC","BEDROOMS","BATHROOMS","HVAC_TYPE","CHECK DATE","OCCUPANTS","REQUESTED_DT","ASSESSMENT_DT","CONTRACTOR_NAME","CONTRACTOR_ADDRESS","CONTRACTOR_CITY","CONTRACTOR_STATE","CONTRACTOR_ZIP","CONTRACTOR_PHONE","CONTRACTOR_FAX","CONTRACTOR_EMAIL");
						$ngridJobFields = array("EXT_JOB_ID","EXT_INVOICE_ID","EXT_INVOICE_DT","EXT_JOB_FIRST_NAME","EXT_JOB_LAST_NAME","EXT_ADDRESS1","EXT_ADDRESS2","EXT_CITY","EXT_STATE","EXT_ZIP","MAILING_FIRST_NAME","MAILING_LAST_NAME","MAILING_STREET","MAILING_ADDRESS2","MAILING_CITY","MAILING_STATE","MAILING_ZIP","MAILING_HOME_PHONE","MAILING_WORK_PHONE","MAILING_EMAIL","MDW_ACT_ID","SQUARE_FOOTAGE","AUD_EVENT_DATE","OWN_RENT","DWELL_TYPE","DHW_FUEL","PRIM_HEATING_FUEL","PRIM_HEAT_TYPE","SEC_HEATING_FUEL","SEC_HEAT_TYPE","DHW_TYPE","DHW_SIZE","YEAR_BUILT","DISTCO","SHV_HBC_DATE","OTHER_ACCT","HVAC_TYPE","CHECK_DATE","OCCUPANTS","RATE_CODE","ICL_NO","HIC_REGISTRATION_NO","BPI_CERTIFIED_TECH","CUSTOMER_FINAL_COST","STORE_NAME","STORE_ADDRESS","STORE_CITY","STORE_STATE","STORE_ZIPCODE","STORE_PHONE#","STORE_FAX#","STORE_EMAIL");
						//$ngridPartsFields = array("EXT_JOB_ID","MEA_ID","PART_ID","DESCRIPTION","QTY","AUTH_INCENTIVE_AMT","PROCESS_ALIAS","TOTAL","INSTALL_LOCATION","ANNUAL_OP_HRS","DMD_RED_KW","ENR_SAVINGS","EXISTING_MEA_ID","EXISTING_QTY","EXISTING_KWH","EXISTING_KHW_SOURCE","EXISTING_OP_HOURS","RFG_EXIST_BRAND","RFG_EXIST_MODEL","RFG_EXIST_SIZE","EXIST_THROUGH_DOOR_ICE","EXIST_YEAR","EXIST_LOCATION","RFG_NEW_BRAND","RFG_NEW_MODEL","RFG_NEW_SIZE","THROUGH_DOOR_ICE","SAVINGS_ELE","SAVINGS_GAS","SAVINGS_OIL","SAVINGS_PROPANE","SAVINGS_OTHER","SAVINGS_UNIT","MEASURE_FUEL_TYPE","EXIST_FRIDGE_STYLE","SIZE_TYPE","NEW_FRIDGE_STYLE","NEW_KWH","INSTALLED_DATE","EXIST_EFF","INSTALL_EFF","NFRC_UFACTOR","EXIST_WATTAGE","INSTALL_WATTAGE","CUSTOMER_COST","INSTALL_GPM","INSULATION_TYPE","PRE_RVALUE","POST_RVALUE","WALL_SIDING","HOURS_WORKED","PRE_TEST","POST_TEST","EXIST_CFM50","INSTALL_CFM50");
						$ngridPartsFields = array("EXT_JOB_ID","MEASURE_CODE","PART_ID","DESCRIPTION","QTY","AUTH_INCENTIVE_AMT","TOTAL_INSTALLED_PRICE","MEASURE_FUEL_TYPE","EXIST_MEA_ID","EXIST_EQUIPMENT_MANUF","EXIST_EQUIPMENT_MODEL","EXIST_EQUIPMENT_SIZE","EXIST_EQUIPMENT_STYLE","EXIST_EQUIPMENT_KWH","EXIST_EQUIPMENT_DOOR_ICE","NEW_EQUIPMENT_MANUF","NEW_EQUPMENT_MODEL","NEW_EQUIPMENT_SIZE","SIZE_TYPE","NEW_EQUIPMENT_STYLE","NEW_EQUIPMENT_KWH","THROUGH_DOOR_ICE","INSTALLED_DATE","AFUE_RATING","NFRC_U_FACTOR","EXIST_YEAR","SAVINGS","THERM SAVINGS","EXIST_LOCATION","EXIST_KWH_SOURCE","EXIST_OP_HOURS","INSTALL_LOCATION","ANNUAL_OP_HRS","CUSTOMER_PRICE","INSULATION_TYPE","BEG_RVALUE","HOURS_WORKED","PRE_TEST","POST_TEST","CONTRACTOR_NAME","CONTRACTOR_ADDRESS","CONTRACTOR_CITY","CONTRACTOR_STATE","CONTRACTOR_ZIPCODE","CONTRACTOR_PHONE#","CONTRACTOR_FAX#","CONTRACTOR_EMAIL","FEE","SAVINGS_TOTAL","SAVINGS_ELE","SAVINGS_GAS","SAVINGS_OIL","SAVINGS_PROPANE","SAVINGS_OTHER");
						//print_pre($ngridPartsFields);
						//print_pre($ngridDocsIds);
						$NGRIDJobFileShortName = "NGRIDExtractJobsWMEPI418".date("Ymt").".txt";
						$NGRIDJobFileNameTxt = "../residential/NGRIDExtracts/".$NGRIDJobFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$NGRIDJobFileNameTxt);

						$fpJobNGRID = fopen($NGRIDJobFileNameTxt, 'w');
						fputs($fpJobNGRID, implode("|",$ngridJobFields)."\n"); //add header row
						$NGRIDPartFileShortName = "NGRIDExtractPartsWMEPI418".date("Ymt").".txt";
						$NGRIDPartFileNameTxt = "../residential/NGRIDExtracts/".$NGRIDPartFileShortName;
						//$files_to_zip[] = str_replace("../residential/","",$NGRIDPartFileNameTxt);
						$fpPartNGRID = fopen($NGRIDPartFileNameTxt, 'w');
						fputs($fpPartNGRID, implode("|",$ngridPartsFields)); //add header row
						
						$rowJobCount = 0;
						foreach ($ngridDocsIds as $docId=>$data){
							$rowJobCount++;
						}
						//standar PiggyBack items
						$invoiceID = date("Ymt")."NATGRID418W";
						
						$rowJobCounter = 0;
						foreach ($ngridDocsIds as $docId=>$data){
							$siteId = $data["fields"]["SITEID"];
							//print_pre($data);
							//**PARTS DATA**//
							$apptDate = date("Y-m-d",strtotime($data["fields"]["APPTDATE"]));
							//echo $apptDate." | ". $dataByFieldName->APPTDATE." | ".exceldatetotimestamp($dataByFieldName->APPTDATE)." = ";
							if ($apptDate == "1969-12-31"){$apptDate = (strpos($data["fields"]["APPTDATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["APPTDATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["APPTDATE"]))));}
							$assmntDate = date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"]));
							if ($assmntDate == "1969-12-31"){$assmntDate = (strpos($data["fields"]["ASSESSMENT_DT"],"/") ? date("Y-m-d",strtotime($data["fields"]["ASSESSMENT_DT"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["ASSESSMENT_DT"]))));}
							$audEventDate = date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"]));
							if ($audEventDate == "1969-12-31"){$audEventDate = (strpos($data["fields"]["AUD_EVENT_DATE"],"/") ? date("Y-m-d",strtotime($data["fields"]["AUD_EVENT_DATE"])) : date("Y-m-d",strtotime(exceldatetotimestamp($data["fields"]["AUD_EVENT_DATE"]))));}
							
							
							foreach ($data["isms"] as $contractNum=>$isms){
								$acceptableTypesCount = 0;
								foreach ($isms as $ismObj){
									$ism = (array) $ismObj;
									
									//acceptable types for electric piggyback
									$acceptableTypesArray = array("VW_APLNC_RCMDN","VW_LIGT_BULB_RCMDN");
									if (in_array($ism["MEASTABLE"],$acceptableTypesArray)){
										$acceptableTypesCount++;	
										//print_pre($ism);
										/*
										if ( strpos(" ".$ism["DESCRIPTION"],"4w")){
											print_pre($ism["PROP_PARTID"]);
											print_pre($ProductsByCSGCodes);
											$ProductsByCSGCodes[$ism["PROP_PARTID"]]->clearResultsCode;
										}
										*/
										$ism["EXT_JOB_ID"] = $siteId;
										//echo $ism["PROP_PARTID"]."<Br>";
										$ism["PROP_PARTID"] = ($ism["DESCRIPTION"] == "5w TCP 2.0 LED Candle base" ? "005_TCP_TORP_LED_2030.101" : $ism["PROP_PARTID"]);
										$ism["MEASURE_CODE"] = $ProductsByCSGCodes[$ism["PROP_PARTID"]]->clearResultsCode;
										$ism["EXIST_MEA_ID"] = $ProductsByCSGCodes[$ism["PROP_PARTID"]]->clearResultsCode;
										$ism["PART_ID"] = $ProductsByCSGCodes[$ism["PROP_PARTID"]]->clearResultsCode;
										$ism["QTY"] = $ism["INS_QTY"];
										$ism["SAVINGS_TOTAL"] = bcmul($ism["MBTU_SAVINGS"],1000,0);
										$ism["SAVINGS_ELE"] = $ism["SAVINGS_TOTAL"];
										$ism["SAVINGS_UNIT"] = "BTU";
										$ism["MEASURE_FUEL_TYPE"] = "E";
										$ism["INSTALLED_DATE"] = date("m/d/Y",strtotime($apptDate));
										$ism["PROCESS_ALIAS"] = "RCS-E";
										$ism["AUTH_INCENTIVE_AMT"] = $ism["UTIL_PRICE"];
										$ism["TOTAL_INSTALLED_PRICE"] = bcmul($ism["UTIL_PRICE"],$ism["QTY"],2);
										$ism["CUSTOMER_PRICE"] = $ism["TOTAL_INSTALLED_PRICE"];
										$ism["BEG_RVALUE"] = "0";
										$ism["HOURS_WORKED"] = "0";
										$ism["PRE_TEST"] = "0";
										$ism["POST_TEST"] = "0";
										$ism["FEE"] = "0";
										$ism["SAVINGS_GAS"] = "0";
										$ism["SAVINGS_OIL"] = "0";
										$ism["SAVINGS_PROPANE"] = "0";
										$ism["SAVINGS_OTHER"] = "0";


										
										$thisPartRow = array();
										foreach ($ngridPartsFields as $field){
											$thisPartRow[] = $ism[$field];
											//echo $field."=".$ism[$field]."<br>";
										}
										$ngridPartRows[] = $thisPartRow;
										fputs($fpPartNGRID,"\n".implode("|",$thisPartRow));
									}
								}
							}
							
							
							if ($acceptableTypesCount){
								//**JOB DATA**//
								$rowJobCounter++;
								$endOfLineJob = "\n";
								if ($rowJobCounter == $rowJobCount){
									$endOfLineJob = "";
								}
								//print_pre($data["fields"]);
								$data["fields"]["EXT_JOB_ID"] = $siteId;
								$data["fields"]["EXT_INVOICE_ID"] = $invoiceID;
								$data["fields"]["EXT_INVOICE_DT"] = date("m/t/Y");
								$data["fields"]["EXT_JOB_FIRST_NAME"] = trim($data["fields"]["CUST_FIRST_NAME"]);
								$data["fields"]["EXT_JOB_LAST_NAME"] = trim($data["fields"]["CUST_LAST_NAME"]);
								$data["fields"]["EXT_ADDRESS1"] = $data["fields"]["ADDRESS"];									
								
								$data["fields"]["ASSESSMENT_DT"] = date("m/d/Y",strtotime($assmntDate));
								$data["fields"]["AUD_EVENT_DATE"] = date("m/d/Y",strtotime($audEventDate));
								$data["fields"]["EXT_CITY"] = $data["fields"]["CITY"];
								$data["fields"]["EXT_STATE"] = $data["fields"]["STATE"];
								$data["fields"]["EXT_ZIP"] = $data["fields"]["ZIP"];
								$data["fields"]["OWN_RENT"] = ($data["fields"]["OWNER_OCCUPIED"] == "Renter" ? "R" : "O");
								$data["fields"]["DHW_FUEL"] = ($data["fields"]["DHW_FUEL"] ? : "G");
								
								/*
									DHW_Type
									C - Conventional Tank,D - Demand (instantaneous),
									H - Heat pump,T - Tankless coil,I - Indirect Tank,
									O - Other,N - None,U - Unknown
									
									DHW_Size
									XS - Extra Small (<31 gal), S - Small (31-50 gal),
									M - Medium (51-70 gal), L - Large (71-90 gal),
									XL - Extra Large (>90 gal), U - Unknown
								*/

								
								$data["fields"]["PRIM_HEATING_FUEL"] = "G";
								$data["fields"]["PRIM_HEAT_TYPE"] = "U";
								$data["fields"]["SEC_HEATING_FUEL"] = "U";
								$data["fields"]["SEC_HEAT_TYPE"] = "U";
								$data["fields"]["DHW_TYPE"] = ($data["fields"]["DHW_TYPE"] ? : "C");
								$data["fields"]["DHW_SIZE"] = ($data["fields"]["DHW_SIZE"] ? : "S");
								$data["fields"]["DISTCO"] = "05";
								$data["fields"]["HVAC_TYPE"] = ($data["fields"]["EFI_TYPE_AC"] ? : "2");
								$data["fields"]["OCCUPANTS"] = "0";
								$data["fields"]["RATE_CODE"] = "001";
								$data["fields"]["CUSTOMER_FINAL_COST"] = "0";

								$data["fields"]["MAILING_FIRST_NAME"] = $data["fields"]["CUST_FIRST_NAME"];
								$data["fields"]["MAILING_LAST_NAME"] = $data["fields"]["CUST_LAST_NAME"];
								$data["fields"]["MAILING_STREET"] = $data["fields"]["ADDRESS"];
								$data["fields"]["MAILING_CITY"] = $data["fields"]["CITY"];
								$data["fields"]["MAILING_STATE"] = $data["fields"]["STATE"];
								$data["fields"]["MAILING_ZIP"] = $data["fields"]["ZIP"];
								
								/*
								10=SFD- Single Family Detached, 09=SFA - Single Family Attached
								11=APT - Apartment, 16=U - Unknown, 12=TH - Townhome/condo
								$dwellTypes = array("Single Family Detached"=>10,"Single Family"=>09,"Apartment"=>11,"Townhome/condo"=>12);
								*/
								
								if (!$dwellTypes[$data["fields"]["DWELL_TYPE"]]){$data["fields"]["DWELL_TYPE"] = 16;}
								
								
								$thisJobRow = array();
								foreach ($ngridJobFields as $dataField){
									//echo $dataField." = " .$data["fields"][$dataField]."<br>";
									$thisJobRow[] = $data["fields"][$dataField];
								}
								$exportedJobDocIds[] = $docId;
								$ngridJobRows[] = $thisJobRow;
								fputs($fpJobNGRID, implode("|",$thisJobRow).$endOfLineJob);
							}else{
								$noElectricIsms[] = $docId;
							}//only if acceptable isms
						}
						fclose($fpJobNGRID);
						fclose($fpPartNGRID);
						
						$energyName = "NGRID";
						$energyType = "Jobs";
						$energyHeaderFields = $ngridJobFields;
						$energyDataRows = $ngridJobRows;
						include('model/createSimpleExcelFile.php');

						$energyType = "Parts";
						$energyHeaderFields = $ngridPartsFields;
						$energyDataRows = $ngridPartRows;
						include('model/createSimpleExcelFile.php');
						