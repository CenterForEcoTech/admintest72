<link rel="stylesheet" media="all" type="text/css" href="<?php echo $CurrentServer;?>css/jquery-ui-timepicker-addon.css" />
				<style>
					.ui-timepicker-select {width:75px;}
					.activeAvailabilitySpan {cursor:pointer;}
					.activeAvailabilitySpan_hover { background-color: silver; color:grey;cursor:pointer;}
					.activeAvailabilitySpan_inactive { background-color: silver; color:grey;cursor:pointer;border:1pt solid grey;}
				</style>
			<div class=="row">
				<div class="nine columns">
					<fieldset>
					<legend>Current Availability</legend>
						<a href="#" class="button-link" id='toggleSetAvailability'>Add Availability</a>

						<div id="availableResults"></div>
						<div id="availableActiveResults">
							<?php
								foreach ($availability as $es=>$info){
									foreach ($info as $id=>$record){
										$status = $record["status"];
										
										echo "<div id='originalDiv".$id."' class='activeAvailabilitySpan".($status=="inactive" ? " activeAvailabilitySpan_inactive" : "")."' data-id='".$id."' data-es='".$es."' data-totalhours='".$record["totalHours"]."' data-starttime='".$record["startTime"]."' data-endtime='".$record["endTime"]."' data-days='".$record["days"]."' data-date='".$record["date"]."' data-types='".$record["types"]."' data-status='".($status=="inactive" ? "inactive" : "Active")."' title='click to update'><b>".$es.":</b> ".$record["totalHours"]." hours (".date("ga",strtotime($record["startTime"]))." - ".date("ga",strtotime($record["endTime"])).") on ".$record["days"].$record["date"]." for ".$record["types"]." visits <span id='status".$id."'>".($status=="inactive" ? "inactive" : "")."</span></div>";
									}
									echo "<hr>";
								}
							?>
						
						</div>
					</fieldset>
				</div>
				<div class="seven columns">
					<div id="showSetAvailability" style="display:none;">
					<fieldset>
					<legend>Set Availability</legend>
							<div class="row">
								<div class="three columns">
									Energy Specialist:<br>
									<select id="addAvailability_es" style="width:125px;">
										<option value=""></option>
										<?php
											foreach ($EmployeeByName as $id=>$record){
												$name = $record->firstName." ".substr($record->lastName,0,1);
												echo "<option value='".$name."'>".$name."</option>";
											}
										?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="two columns" id="availableSpecificDate" style="display:none;">
									Specific Date:<br><input type="text" class="date" id="availableDate" disabled=disabled>
									<img src="<?php echo $CurrentServer;?>images/redx-small.png" style="width:10px;" id="removeAvailableSpecificDate">
								
								</div>
								<div class="two columns" id="availableNonSpecificDate">
									Days:<br>
									<select id='availableDay' style="width:105px;height:160px;" multiple>
										<option value="">Specific Date</option>
										<option value="Everyday" selected>Everyday</option>
										<option value="Monday">Monday</option>
										<option value="Tuesday">Tuesday</option>
										<option value="Wednesday">Wednesday</option>
										<option value="Thursday">Thursday</option>
										<option value="Friday">Friday</option>
										<option value="Saturday">Saturday</option>
										<option value="Sunday">Sunday</option>
									</select>
								</div>
								<div class="four columns">
									Types of Appointments:<br>
									<?php
										$appointmentTypes = array("HEA"=>"HES","Hybrid Visit"=>"HES","HEAAU"=>"HES", "Inspection"=>"HES","SHV"=>"HES","WiFi"=>"HES","Combustion_Safety_Revisit"=>"HES","WGE"=>"WGE","MMWEC"=>"MMWEC"); //"Virtual HEA"=>"HES", "Virtual SHV"=>"HES",
									?>
									<select id="types" name="types" style="width:175px;height:175px;" multiple>
										<option value="All" selected>All</option>
										<?php
											foreach ($appointmentTypes as $typeName=>$typeSource){
												$typeName = str_replace("_"," ",$typeName);
												echo "<option value='".$typeName."'>".$typeName."</option>";
											}
										?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="six columns">
									Times Available for <span id='availableDayDisplay'>Everyday</span>:<br>
									<!--
									<div class="row">
										<div class="five columns">
											<select id="availableTimeRange">
												<option value="09:00-11:00">9am - 11am</option>
												<option value="12:00-14:00">12pm - 2pm</option>
												<option value="15:00-17:00">3pm - 5pm</option>
											</select>
										</div>
									</div>
									-->
									<div class="row">
										<div class="two columns">
											Start Time1:
											<select id="availableStartTime" style="width:75px;">
												<?php 
													$st = 0;
													$availableStartTime = date("H:i:s",strtotime("08:00:00"));
													while ($st <= 18){
														echo "<option value='".$availableStartTime."'>".date("g:ia",strtotime($availableStartTime))."</option>";
														$availableStartTime = date("H:i:s",strtotime($availableStartTime." + 30 min"));
														$st++;
													}
													
												?>
											</select>
										</div>
										<div class="three columns">
											End Time:<br>
											<select id="availableEndTime" style="width:75px;">
												<?php 
													$st = 0;
													$availableStartTime = date("H:i:s",strtotime("09:00:00"));
													while ($st <= 18){
														echo "<option value='".$availableStartTime."'>".date("g:ia",strtotime($availableStartTime))."</option>";
														$availableStartTime = date("H:i:s",strtotime($availableStartTime." + 30 min"));
														$st++;
													}
												?>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="six columns">
											<a href="#" class="button-link" id="addAvailability">Add</a> &nbsp;&nbsp;&nbsp;
											<a href="#" class="button-link cancel" id="cancel_addAvailability" style="position:relative;float:right;">Cancel</a>
										</div>
									</div>
									<span id="addAvailabilityResults"></span>
									<span style="display:none;">Total Hours: <input type="hidden" id="totalHours" value="8"></span>
								</div>
							</div>
					</fieldset>
					</div>
				</div>
				<div class="seven columns">
					<div id="showUpdateAvailability" style="display:none;">
					<fieldset>
					<legend>Update Availability</legend>
							<div class="row">
								<div class="six columns">
									Energy Specialist: <span id="update_addAvailability_es_display"></span>
									<input type="hidden" id="update_addAvailability_es" style="width:125px;">
									<input type="hidden" id="update_originalDiv" value="">
								</div>
							</div>
							<div class="row">
								<div class="two columns" id="update_availableSpecificDate">
									Specific Date: <input type="text" class="date" id="update_availableDate">
									<img src="<?php echo $CurrentServer;?>images/redx-small.png" style="width:10px;" id="update_removeAvailableSpecificDate">
								</div>
								<div class="two columns" id="update_availableNonSpecificDate">
									Days:<br>
									<select id='update_availableDay' style="width:105px;height:160px;" multiple>
										<option value="">Specific Date</option>
										<option value="Everyday">Everyday</option>
										<option value="Monday">Monday</option>
										<option value="Tuesday">Tuesday</option>
										<option value="Wednesday">Wednesday</option>
										<option value="Thursday">Thursday</option>
										<option value="Friday">Friday</option>
										<option value="Saturday">Saturday</option>
										<option value="Sunday">Sunday</option>
									</select>
								</div>
								<div class="four columns">
									Types of Appointments:<br>
									<select id="update_types" name="types" style="width:175px;height:175px;" multiple>
										<option value="All">All</option>
										<option value="HEA">HEA</option>
										<option value="HEAAU">HEAAU</option>
										<option value="Inspection">Inspection</option>
										<option value="SHV">SHV</option>
										<option value="WiFi">WiFi</option>
										<option value="Combustion Safety Revisit">Combustion Safety Revisit</option>
										<option value="WGE">WGE</option>
										<option value="MMWEC">MMWEC</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="six columns">
									Times Available for <span id='availableDayDisplay'>Everyday</span>:<br>
									<div class="row">
										<div class="two columns">
											Start Time:
											<select id="update_availableStartTime" style="width:75px;">
												<?php 
													$st = 0;
													$availableStartTime = date("H:i:s",strtotime("08:00:00"));
													while ($st <= 18){
														echo "<option value='".$availableStartTime."'>".date("g:ia",strtotime($availableStartTime))."</option>";
														$availableStartTime = date("H:i:s",strtotime($availableStartTime." + 30 min"));
														$st++;
													}
												?>
											</select>
										</div>
										<div class="two columns">
											End Time:<br>
											<select id="update_availableEndTime" style="width:75px;">
												<?php 
													$st = 0;
													$availableStartTime = date("H:i:s",strtotime("09:00:00"));
													while ($st <= 18){
														echo "<option value='".$availableStartTime."'>".date("g:ia",strtotime($availableStartTime))."</option>";
														$availableStartTime = date("H:i:s",strtotime($availableStartTime." + 30 min"));
														$st++;
													}
												?>
											</select>
										</div>
										<div class="one column">
											Status:<br>
											<select id="update_status" style="width:65px;">
												<option value="Active" selected>Active</option>
												<option value="inactive">Inactive</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="six columns">
											<a href="#" class="button-link" id="updateAvailability">Update</a>
											<a href="#" class="button-link cancel" id="cancel_updateAvailability" style="position:relative;float:right;">Cancel</a>
										</div>
									</div>
									<span id="updateAvailabilityResults"></span>
									<span style="display:none;">
										Total Hours: <input type="text" id="update_totalHours" value=""><br>
										id: <input type="text" id="update_id" value="">
									</span>
								</div>
							</div>
					</fieldset>
					</div>
				</div>
			</div>
			
			<div class=="row">
				<div class="nine columns">
					<fieldset>
					<legend>Currently Blocked Events</legend>
						<a href="#" class="button-link" id='toggleSetBlock'>Create Blocked Event</a>

						<div id="blockedResults"></div>
						<div id="blockedActiveResults">
							<?php
								$criteria = new stdClass();
								$criteria->status = "Active";
								$criteria->future = true;
								$getBlocked = $residentialProvider->getScheduleBlocked($criteria);
								foreach ($getBlocked->collection as $record){
									$ES = $record->ResidentialScheduledBlocked_ES;
									$id = $record->ResidentialScheduledBlocked_ID;
									$getBlock[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["es"] = $record->ResidentialScheduledBlocked_ES;
									$getBlock[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["startTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_StartTimeStamp));
									$getBlock[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["endTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_EndTimeStamp));
									$getBlock[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["reason"] = $record->ResidentialScheduledBlocked_Reason;
									$getBlock[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["note"] = $record->ResidentialScheduledBlocked_Note;
									$blockedID[$id]["note"] = $record->ResidentialScheduledBlocked_Note;
								}
								ksort($getBlock);
								foreach ($getBlock as $ES=>$blockData){
									echo "<b>".$ES."</b><br>";
									krsort($blockData);
									foreach ($blockData as $timeStamp=>$idData){
										foreach ($idData as $id=>$val){
											$note = str_replace('\'','"',$val["note"]);
											echo "<span id='blockedEvent".$scheduledIdBySiteId["BlockedID".$id]."' data-scheduledid='".$scheduledIdBySiteId["BlockedID".$id]."' data-id='".$id."' data-es='".$ES."' class='blockedEventDisplay' style='cursor:pointer;' title='".($val["note"] ? $note : $val["reason"])."'>&nbsp;&nbsp;&nbsp;&nbsp;[".date("m/d g:ia",strtotime($val["startTimeStamp"]))." - ".date("m/d g:ia",strtotime($val["endTimeStamp"]))."] ".$val["reason"].($val["note"] ? "<sup>*</sup>" : "")."</span><br>";
										}
									}
									echo "<hr>";
								}
//								echo $blockedHours;
							?>
						
						</div>
						<a href="#" id="showCancelledBlockedEvents" class='button-link'>Show Cancelled Blocked Events</a><br>
						<div id="cancelledBlockedEvents" style="display:none;">
							Cancelled Blocked Events:<br>
							<div id="CancelledBlockedResults">
								<?php
									$criteria = new stdClass();
									$criteria->status = "Cancelled";
									$getBlockedCancelled = $residentialProvider->getScheduleBlocked($criteria);
									foreach ($getBlockedCancelled->collection as $record){
										$ES = $record->ResidentialScheduledBlocked_ES;
										$id = $record->ResidentialScheduledBlocked_ID;
										$blockedHoursCancelled .= $record->ResidentialScheduledBlocked_ES." ".$record->ResidentialScheduledBlocked_Reason." [".date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_StartTimeStamp))." - ".date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_EndTimeStamp))."] ".$record->ResidentialScheduledBlocked_Note."<br>";
										$getBlockCancelled[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["es"] = $record->ResidentialScheduledBlocked_ES;
										$getBlockCancelled[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["startTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_StartTimeStamp));
										$getBlockCancelled[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["endTimeStamp"] = date("m/d/Y g:i a",strtotime($record->ResidentialScheduledBlocked_EndTimeStamp));
										$getBlockCancelled[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["reason"] = $record->ResidentialScheduledBlocked_Reason;
										$getBlockCancelled[$ES][strtotime($record->ResidentialScheduledBlocked_StartTimeStamp)][$id]["note"] = $record->ResidentialScheduledBlocked_Note;
										
									}
//									echo $blockedHoursCancelled;
									ksort($getBlockCancelled);
									foreach ($getBlockCancelled as $ES=>$blockData){
										echo "<b>".$ES."</b><br>";
										krsort($blockData);
										foreach ($blockData as $timeStamp=>$idData){
											foreach ($idData as $key=>$val){
												$note = str_replace('\'','"',$val["note"]);
												echo "<span style='cursor:pointer;' title='".($val["note"] ? $note : $val["reason"])."'>&nbsp;&nbsp;&nbsp;&nbsp;[".date("m/d g:ia",strtotime($val["startTimeStamp"]))." - ".date("m/d g:ia",strtotime($val["endTimeStamp"]))."] ".$val["reason"].($val["note"] ? "<sup>*</sup>" : "")."</span><br>";
											}
										}
										echo "<hr>";
									}

								?>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="seven columns" id="showSetBlock" style="display:none;">
					<fieldset>
					<legend>Add Blocked Event</legend>
						<div class="row">
							<div class="three columns">
								Energy Specialist:<br>
								<select id="addBlock_es" style="width:125px;">
									<option value=""></option>
									<?php
										foreach ($EmployeeByName as $id=>$record){
											$name = $record->firstName." ".substr($record->lastName,0,1);
											echo "<option value='".$name."'>".$name."</option>";
										}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="three columns">
								Start Time:<br>
								<input id="addBlock_startTimeStamp" type="text" class="timestamp" style="width:140px;"><br>
							</div>
							<div class="three columns">
								End Time:<br>
								<input id="addBlock_endTimeStamp" type="text" class="timestamp" style="width:140px;"><br>
							</div>
						</div>
						<div class="row">
							<div class="three columns">
								Reason:<br>
								<select id="addBlock_reason" style="width:125px;">
									<option value=""></option>
									<option value="Meeting/Training">Meeting/Training</option>
									<option value="Vacation/Time Off">Vacation/Time Off</option>
									<option value="Rescheduling Slot">Rescheduling Slot</option>
								</select>
							</div>
							<div class="six columns">
								Note:<br>
								<input type="text" id="addBlock_note">
							</div>
							<div class="three columns">
								<br>
								<a href="#" class="button-link" id="addBlock">Add Block</a>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="seven columns" id="cancelBlock" style="display:none;">
					<fieldset>
					<legend>Cancel Blocked Event</legend>
						<div style='text-align:center'>
							Are you sure you want to cancel this block?<br>
							<blockquote>
								<span id="cancelDisplay" style="font-size:10pt;"></span>
								<input type="hidden" id="cancelBlockId">
								<input type="hidden" id="cancelScheduledId">
							</blockquote>
							<a href="#" id='confirmCancelBlock' class='button-link confirm'>Yes</a> &nbsp;&nbsp;<a href="#" id='cancelCancelBlock' class='button-link cancel'>No</a><br>
							<span style='font-size:10pt;'>(click Yes to free this time slot for other appointments)</span><br>
						</div>
					</fieldset>
				</div>
			</div>
				
				
				
				<script type="text/javascript">
					$(function () {
						var dateClass = $(".date");
						dateClass.width(80);
						dateClass.datepicker({
							minDate:0
						});
						$("#removeAvailableSpecificDate").on('click',function(){
								$("#availableDate").val('');
								$("#availableDate").attr('disabled',true);
								$("#availableDay").attr('disabled',false);
								$("#availableDay").val('Everyday');
								$("#availableNonSpecificDate").show();
								$("#availableSpecificDate").hide();
						});
						$("#update_removeAvailableSpecificDate").on('click',function(){
								$("#update_availableDate").val('');
								$("#update_availableDate").attr('disabled',true);
								$("#update_availableDay").attr('disabled',false);
								$("#update_availableDay").val('Everyday');
								$("#update_availableNonSpecificDate").show();
								$("#update_availableSpecificDate").hide();
						});
						$("#availableDate").on('change',function(){
							var $this = $(this);
							if ($this.val() != ""){
								$("#availableDay").val('');
								$("#availableDay").attr('disabled',true);
								$("#availableNonSpecificDate").hide();
							}else{
								$("#availableDay").attr('disabled',false);
								$("#availableDay").val('Everyday');
								$("#availableNonSpecificDate").show();
								$("#availableSpecificDate").hide();
							}
						});
						$("#update_availableDate").on('change',function(){
							var $this = $(this);
							if ($this.val() != ""){
								$("#update_availableDay").val('');
								$("#update_availableDay").attr('disabled',true);
								$("#update_availableNonSpecificDate").hide();
							}else{
								$("#update_availableDay").attr('disabled',false);
								$("#update_availableDay").val('Everyday');
								$("#update_availableNonSpecificDate").show();
								$("$update_availableSpecificDate").hide();
							}
						});
						$("#availableDay").on('change',function(){
							var $this = $(this);
							if ($this.val() != ""){
								$("#availableDate").val('');
								$("#availableDate").attr('disabled',true);
								$("#availableSpecificDate").hide();
							}else{
								$("#availableDate").attr('disabled',false);
								$("#availableDate").val('');
								$("#availableSpecificDate").show();
								$("#availableNonSpecificDate").hide();
							}
						});
						$("#update_availableDay").on('change',function(){
							var $this = $(this);
							if ($this.val() != ""){
								$("#update_availableDate").val('');
								$("#update_availableDate").attr('disabled',true);
								$("#update_availableSpecificDate").hide();
							}else{
								$("#update_availableDate").attr('disabled',false);
								$("#update_availableDate").val('');
								$("#update_availableSpecificDate").show();
								$("#update_availableNonSpecificDate").hide();
							}
						});

						$("#confirmCancelBlock").on('click',function(e){
							e.preventDefault();
							var thisId = $("#cancelBlockId").val(),
								thisScheduledId = $("#cancelScheduledId").val(),
								blockedId = "BlockedID"+thisId,
								fullCalendarEventId = fullCalendarEventIdBySiteId[blockedId],
								data = {};
								data["action"] = "removeBlock";
								data["blockedId"] = blockedId;
								data["scheduledId"] = thisScheduledId;
								//console.log(data);
								//console.log(blockedId+fullCalendarEventIdBySiteId[blockedId]);								
								//console.log(fullCalendarEventIdBySiteId);								
								blockedEvent = "#blockedEvent"+thisScheduledId;
								blockedDiv = "#blockedSchedule"+thisScheduledId;
								$.ajax({
									url: "ApiResidentialManagement.php",
									type: "POST",
									data: JSON.stringify(data),
									success: function(resultdata){
											$(blockedEvent).hide();
											$(blockedDiv).hide();
											$('#calendar').fullCalendar( 'removeEvents', fullCalendarEventId );
											//console.log(resultdata);
											//location.reload();
									},
									error: function(jqXHR, textStatus, errorThrown){
										var message = $.parseJSON(jqXHR.responseText);
										$(thisAlert).show().html(message).addClass("alert");
									}
								});
								

							$("#cancelBlock").toggle("slide");
						});
						$("#cancelCancelBlock").on('click',function(e){
							e.preventDefault();
							$("#cancelBlock").toggle("slide");
						});
						$(".blockedEventDisplay").on('click',function(){
							var $this = $(this),
								thisES = $this.attr('data-es');
								thisTitle = $this.attr('title');
								thisId = $this.attr('data-id');
								thisScheduledId = $this.attr('data-scheduledid');
								thisHtml = $this.html();
								$("#cancelDisplay").html('<b>'+thisES+':</b> '+thisHtml);
								$("#cancelDisplay").attr('title',thisTitle);
								$("#cancelBlockId").val(thisId);
								$("#cancelScheduledId").val(thisScheduledId);
								$("#cancelBlock").toggle("slide");
								 $(document).scrollTop( $("#cancelBlock").offset().top );  
						});
						$("#showCancelledBlockedEvents").on('click',function(e){
							e.preventDefault();
							$("#cancelledBlockedEvents").slideToggle();
						});
						$("#toggleSetBlock").on('click',function(e){
							e.preventDefault();
							$("#showSetBlock").toggle( "slide" );
							$(document).scrollTop( $("#showSetBlock").offset().top );  
						});
						$("#toggleSetAvailability").on('click',function(e){
							e.preventDefault();
							$("#addAvailabilityResults").html('');
							$("#showSetAvailability").toggle( "slide" );
							$(document).scrollTop( $("#showSetAvailability").offset().top );  
							$("#showUpdateAvailability").hide();
						});
						function hoursCalculate() {
							 var hours = parseInt($("#availableEndTime").val().split(':')[0], 10) - parseInt($("#availableStartTime").val().split(':')[0], 10);
							 if(hours < 0) hours = 24 + hours;
							 $("#totalHours").val(hours);
						 }
						 $("#availableStartTime,#availableEndTime").change(hoursCalculate);
						 hoursCalculate();						
						function update_hoursCalculate() {
							 var hours = parseInt($("#update_availableEndTime").val().split(':')[0], 10) - parseInt($("#update_availableStartTime").val().split(':')[0], 10);
							 if(hours < 0) hours = 24 + hours;
							 $("#update_totalHours").val(hours);
						 }
						 $("#update_availableStartTime,#update_availableEndTime").change(function(){
								update_hoursCalculate();
						 });
						
						$('.activeAvailabilitySpan').hover(function(){$(this).toggleClass('activeAvailabilitySpan_hover');});
						$('.activeAvailabilitySpan').on('click',function(){
							$("#updateAvailabilityResults").html('');							
							$("#showUpdateAvailability").toggle( "slide" );
							$(document).scrollTop( $("#showUpdateAvailability").offset().top );  
							$("#showSetAvailability").hide();

							var $this = $(this),
								thisId = $this.attr('data-id'),
								originalDiv = "#originalDiv"+thisId,
								thisES = $this.attr('data-es'),
								thisTotalHours = $this.attr('data-totalhours'),
								thisStartTime = $this.attr('data-starttime'),
								thisEndTime = $this.attr('data-endtime'),
								thisDays = $this.attr('data-days'),
								thisDate = $this.attr('data-date'),
								thisTypes = $this.attr('data-types'),
								thisStatusData = $this.attr('data-status'),
								thisStatusId = "#status"+thisId,
								thisStatus = $(thisStatusId).html(),
								data = {};
								
								if (thisDate){
									$("#update_availableSpecificDate").show();
									$("#update_availableNonSpecificDate").hide();
								}else{
									$("#update_availableSpecificDate").hide();
									$("#update_availableNonSpecificDate").show();
								}
								
								$("#update_addAvailability_es").val(thisES);
								$("#update_originalDiv").val(originalDiv);
								$("#update_addAvailability_es_display").html(thisES);
								$.each(thisDays.split(","), function(i,e){
									$("#update_availableDay option[value='" + e + "']").prop("selected", true);
								});
								$.each(thisTypes.split(","), function(i,e){
									$("#update_types option[value='" + e + "']").prop("selected", true);
								});
								$("#update_availableDate").val(thisDate);
								$("#update_availableStartTime").val(thisStartTime);
								$("#update_availableEndTime").val(thisEndTime);
								$("#update_status").val(thisStatusData);
								$("#update_totalHours").val(thisTotalHours);
								$("#update_id").val(thisId);
								
						});
						$("#availableDay").on('change',function(){
							var $this = $(this),
								thisVal = $this.val(),
								totalCount = thisVal.length,
								displayVal = "";
								$.each(thisVal,function(key,val){
									if (val == "Everyday"){
										displayVal = val;
										$("#availableDay").val('Everyday');
										return false;
									}else{
										if ((key+1) < totalCount){
											displayVal = displayVal+val+" and ";
										}else{
											displayVal = displayVal+val;
										}
									}
								});
							$("#availableDayDisplay").html(displayVal);	
						});
						$("#types").on('change',function(){
							var $this = $(this),
								thisVal = $this.val();
								$.each(thisVal,function(key,val){
									if (val == "All"){
										displayVal = val;
										$("#types").val('All');
										return false;
									}
								});
						});
						$('.timestamp').datetimepicker({
							timeFormat: 'h:mm tt',
							hourMin: 8,
							hourMax: 16,
							stepMinute: 30,
							controlType: 'select',
							oneLine: true,
							altFieldTimeOnly: false
						});
						$("#cancel_updateAvailability").on('click',function(e){
							e.preventDefault();
							$("#showUpdateAvailability").toggle( "slide" );
						});
						$("#updateAvailability").on('click',function(e){
							e.preventDefault();
							$("#updateAvailabilityResults").html('');							
							var es = $("#update_addAvailability_es").val(),
								days = $("#update_availableDay").val(),
								adate = $("#update_availableDate").val(),
								startTime = $("#update_availableStartTime").val(),
								endTime = $("#update_availableEndTime").val(),
								totalHours = $("#update_totalHours").val(),
								types = $("#update_types").val(),
								status = $("#update_status").val(),
								id = $("#update_id").val(),
								originalDiv = $("#update_originalDiv").val();
					
							var data = {};
								data["action"] = "updateAvailability";
								data["es"] = es;
								data["days"] = days;
								data["adate"] = adate;
								data["startTime"] = startTime;
								data["endTime"] = endTime;
								data["totalHours"] = totalHours;
								data["types"] = types;
								data["status"] = status;
								data["id"] = id;
								//console.log(data);
								$("#updateAvailabilityResults").html('processing').removeClass('alert').removeClass('info');
								$.ajax({
									url: "ApiResidentialManagement.php",
									type: "POST",
									data: JSON.stringify(data),
									success: function(resultdata){
										//console.log(resultdata);
										var updateDiv = $(originalDiv),
											newBackgroundColor = 'white';
										if (status == "inactive"){
											updateDiv.addClass('activeAvailabilitySpan_inactive');
											newBackgroundColor = 'silver';
										}else{
											updateDiv.removeClass('activeAvailabilitySpan_inactive');
										}
					

										updateDiv.animate(
											{
												backgroundColor: '#33cc33'
											}, 
											2000, 
											function(){
												updateDiv.animate({backgroundColor: newBackgroundColor}, 2000);
											}
										);
										var displayStartTime = parseInt(startTime.split(':')[0], 10),
											displayEndTime =  parseInt(endTime.split(':')[0], 10);
											if (displayStartTime > 11){
												var subtractFactor = (displayStartTime == 12 ? 0 : 12);
												displayStartTime = (displayStartTime-subtractFactor)+'pm';
											}else{
												displayStartTime = displayStartTime+'am';
											}
											if (displayEndTime > 11){
												var subtractFactor = (displayEndTime == 12 ? 0 : 12);
												displayEndTime = (displayEndTime-subtractFactor)+'pm';
											}else{
												displayEndTime = displayEndTime+'am';
											}

										
										updateDiv.attr('data-days',days);
										updateDiv.attr('data-date',adate);
										updateDiv.attr('data-starttime',startTime);
										updateDiv.attr('data-endtime',endTime);
										updateDiv.attr('data-totalhours',totalHours);
										updateDiv.attr('data-types',types);
										updateDiv.attr('data-status',status);
										if (days == null){days = "";}

										updateDiv.html('<b>'+es+':</b> '+totalHours+' hours ('+displayStartTime+' - '+displayEndTime+') on '+adate+days+' for '+types+' '+status);
										$("#updateAvailabilityResults").html('success').removeClass('alert').removeClass('info').addClass('info');
										setTimeout(function(){
											$("#showUpdateAvailability").toggle( "slide" );
										}, 1000);
									},
									error: function(jqXHR, textStatus, errorThrown){
										console.log(jqXHR.responseText);
									}
								});
						});
						$("#cancel_addAvailability").on('click',function(e){
							e.preventDefault();
							$("#showSetAvailability").toggle( "slide" );
						});
						$("#addAvailability").on('click',function(e){
							e.preventDefault();
							var es = $("#addAvailability_es").val(),
								days = $("#availableDay").val(),
								adate = $("#availableDate").val(),
								startTime = $("#availableStartTime").val(),
								endTime = $("#availableEndTime").val(),
								totalHours = $("#totalHours").val(),
								types = $("#types").val();
								var data = {};
								data["action"] = "addAvailability";
								data["es"] = es;
								data["days"] = days;
								data["adate"] = adate;
								data["startTime"] = startTime;
								data["endTime"] = endTime;
								data["totalHours"] = totalHours;
								data["types"] = types;
								if (es == ""){
									$("#addAvailabilityResults").html('Energy Specialist Required').removeClass('alert').removeClass('info').addClass('alert');
								}else{
									console.log(data);
									$("#addAvailabilityResults").html('').removeClass('alert').removeClass('info');
									$.ajax({
										url: "ApiResidentialManagement.php",
										type: "POST",
										data: JSON.stringify(data),
										success: function(resultdata){
											console.log(resultdata);
											getAvailable();										
											$("#addAvailability_es").val('');
											$("#availableDate").val('');
											$("#availableDay").val('Everyday');
											$("#availableStartTime").val('09:00:00');
											$("#availableEndTime").val('17:00:00');
											$("#totalHours").val('6');
											$("#types").val('All');
											$("#addAvailabilityResults").html('success').removeClass('alert').removeClass('info').addClass('info');
											$("#showSetAvailability").toggle( "slide" );
										},
										error: function(jqXHR, textStatus, errorThrown){
											console.log(jqXHR.responseText);
										}
									});
								}
						});
						
						$("#addBlock_startTimeStamp").on('change',function(){
							var date2 = $('#addBlock_startTimeStamp').datetimepicker('getDate'); 
							date2.setHours(date2.getHours()+1); 
							$('#addBlock_endTimeStamp').datetimepicker('setDate', date2);						
						});
						
						
						$("#addBlock").on('click',function(e){
							e.preventDefault();
							var es = $("#addBlock_es").val(),
								startTimeStamp = $("#addBlock_startTimeStamp").val(),
								endTimeStamp = $("#addBlock_endTimeStamp").val(),
								reason = $("#addBlock_reason").val();
								note = $("#addBlock_note").val();
								var data = {};
								data["action"] = "addBlock";
								data["es"] = es;
								data["startTimeStamp"] = startTimeStamp;
								data["endTimeStamp"] = endTimeStamp;
								data["reason"] = reason;
								data["note"] = note;
								$.ajax({
									url: "ApiResidentialManagement.php",
									type: "POST",
									data: JSON.stringify(data),
									success: function(resultdata){
										//console.log(resultdata);
										getBlocked();										
										$("#addBlock_es").val('');
										$("#addBlock_startTimeStamp").val('');
										$("#addBlock_endTimeStamp").val('');
										$("#addBlock_reason").val('');
										$("#addBlock_note").val('');
										
									},
									error: function(jqXHR, textStatus, errorThrown){
										console.log(jqXHR.responseText);
									}
								});
						});
						var getBlocked = function(){
							var blockedResults = $("#blockedResults"),
								justAdded = blockedResults.html(),
								es = $("#addBlock_es").val(),
								startTimeStamp = $("#addBlock_startTimeStamp").val(),
								endTimeStamp = $("#addBlock_endTimeStamp").val(),
								reason = $("#addBlock_reason").val();
								note = $("#addBlock_note").val();
							blockedResults.html(justAdded+es+" "+reason+" ["+startTimeStamp+" - "+endTimeStamp+"] "+note+"<br>");
								
						};
						var getAvailable = function(){
							var availableResults = $("#availableResults"),
								justAdded = availableResults.html(),
								es = $("#addAvailability_es").val(),
								days = $("#availableDay").val(),
								adate = $("#availableDate").val(),
								startTime = $("#availableStartTime option:selected").text(),
								endTime = $("#availableEndTime option:selected").text(),
								totalHours = $("#totalHours").val(),
								types = $("#types").val();
								if (days == null){days = "";}
							availableResults.html("<b>"+justAdded+es+":</b> "+totalHours+" hours ("+startTime+" - "+endTime+") on "+adate+days+" for " +types+" visits<hr>");
								
						};
					});
				</script>
