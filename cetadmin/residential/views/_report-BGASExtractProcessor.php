<?php
ini_set('memory_limit', '512M');
error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$filePath = $path;
	$thispath = "";
}else{
	$path = "";
	$filePath = "";
	include_once("../_config.php");
	$thispath = "../";
}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$warehouseProvider = new WarehouseProvider($dataConn);
$warehouseResults = $warehouseProvider->get($criteria = null);
$warehouseCollection = $warehouseResults->collection;
//Get list of active warehouse locations
foreach ($warehouseCollection as $warehouse){
	if ($warehouse->displayOrderId && strpos(" ".$warehouse->name,"CET")){
		$ActiveWarehouses[] = $warehouse->name;
	}
}
$ActiveWarehouseString = implode("','",$ActiveWarehouses);

include_once($dbProviderFolder."CETDataProvider.php");
$cetDataProvider = new CETDataProvider($dataConn);
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	foreach (${$ArrayName} as $SPData){
		$spData = (object)$SPData;
		//$results[] = $cetDataProvider->insertSharePointData($spData);
		$siteId = $spData->site_x0020_id;
		$SiteIDData[$siteId]["phone"] = $spData->phone_x0020_number;
		$SiteIDData[$siteId]["utility"] = $spData->utility;
		$SiteIDData[$siteId]["secondaryutility"] = $spData->secondary_x0020_utility;
		$ASfinal = $spData->final_x0020__x0024_;
		$ASoriginal = $spData->a_x002f_s_x0020_original_x0020_c;
		$INSfinal = $spData->insulation_x0020_final_x0020_con;
		$INSoriginal = $spData->insulation_x0020_original_x0020_;
		$eobCreate = false;
		if ($ASfinal != $ASoriginal){$eobCreate = true;}
		if ($INSfinal != $INSoriginal){$eobCreate = true;}
		
		$asBillDate = $spData->utility_x0020_billing_x0020_date;
		$asBillYearMonth = date("Y_m",strtotime($asBillDate));
		$insBillDate = $spData->insulation_x0020_billing_x0020_d;
		$insBillYearMonth = date("Y_m",strtotime($insBillDate));
		$includeThisData = ($asBillYearMonth == $yearMonth || $insBillYearMonth == $yearMonth ? true : false);
		
		$SiteIDData[$siteId]["final"] = $final;
		$SiteIDData[$siteId]["original"] = $original;
		
		$jobName = ucwords(strtolower($spData->first_x0020_name." ".$spData->last_x0020_name));

		if($includeThisData){
			$allData[$siteId] = $spData;
			$allSharePointSiteIds[$siteId] = $jobName;
		}
		
		if ($eobCreate && $includeThisData){
			$eobData[$siteId] = $spData;
		}
		//include data used in creating requisitionForm
		include('views/_SharePointRequisitionFields.php');
		
		erase_val($SPData);
		$SPData = null;
	}//end foreach sprecord
	//var_dump ($ArrayName);
	${$ArrayName} = null;
}//end foreach view
//echo $yearMonth." ".count($eobData[$yearMonth])." EOBs";
//print_pre($eobData);
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

$criteria = new stdClass();
$criteria->startDate = date("Y-m-d",strtotime($invoiceDate));
$criteria->endDate = date("Y-m-t",strtotime($invoiceDate));
$criteria->primaryProvider = "BERKSHIR";
$criteria->crewIn = $ActiveWarehouseString;
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
.unmatched {background-color:yellow;border:1pt solid black;}
</style>
<h1>BGAS Extract File Processor (Invoice, EOB, CRI)</h1>
<?php
$setTab = "Extract";
	$TabCodes = array("Hours"=>"Hours","Extract File Import"=>"Extract","Invoice"=>"Invoice","BGAS Notes File"=>"BGASFile","EOB"=>"EOB","CRI"=>"CRI","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
		
			include_once($dbProviderFolder."HREmployeeProvider.php");
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$criteria->showAll = true;
			$criteria->noLimit = true;
			$paginationResult = $hrEmployeeProvider->get($criteria);
			$resultArray = $paginationResult->collection;
			foreach ($resultArray as $result=>$record){
				if ($record->revenueCode){
					$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
				}
			}
			//print_pre($EmployeesRevenueCodeByName);

			include_once($dbProviderFolder."GBSProvider.php");
			$GBSProvider = new GBSProvider($dataConn);

			$billingRatesArray = array("BGAS_Residential");
			$rateType = "BGAS_Residential";
			//Get Billing Rates
			$criteria = new stdClass();
			$criteria->invoiceName = $rateType;
			$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
			$resultArray = $paginationResult->collection;
			foreach ($resultArray as $result=>$record){
				$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
				$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
				foreach ($EmployeeMembers as $Employee){
					$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
					$BillingRateNameByEmployee[$Employee][$rateType] = $record->GBSBillingRate_Name;
				}
			}
		//print_pre($BillingRateNameByEmployee);
		
		
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "BGAS Residential";
			$SelectedGroupsID = 61; 
			$InvoiceData = array();
			$InvoiceHourData = array();
			$sourceFolder = "residential";
			$hideDateRange = "BGAS Residential Invoice Hours";
			include('../hours/views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
				/* not used for BGAS Residential because rates are built onto the spreadsheet provided by BGAS
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				*/
				foreach ($hours as $code=>$hour){
					$code = (string)$code;
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
						$InvoiceHourDataByBillingRateCode[$code][$BillingRateNameByEmployee[$Employee_Name][$rateType]] = $InvoiceHourDataByBillingRateCode[$code][$BillingRateNameByEmployee[$Employee_Name][$rateType]]+$hour;
					}
				}
			}
			//print_pre($InvoiceHourDataByBillingRateCode);
			ksort($InvoiceHourData);
			//print_pre($InvoiceHourData);
			//print_pre($BillingRateByCategory);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						$InvoiceCode = (string)$InvoiceCode;
						$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
						foreach ($EmployeeHours as $Employee_Name=>$hours){
							if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
								if ($hours && $hours != "0.00"){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$IncludedEmployees[] = $Employee_Name;
								}
							}
							if (($BillingCategory == "Admin Assistant") && !in_array($Employee_Name,$IncludedEmployees)){
								$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
								$Adjustments["Alert"]["Staff missing Billing Rate for ".$BillingRateType][]=str_replace("_",", ",$Employee_Name);
							}
						}
					}
				}
			}
			ksort($InvoiceHourDataByCode);
			//print_pre($InvoiceHourDataByCode);
			//ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
?>
<div id='Extract' class='tab-content'>
	<h1>Import BGAS Extract File</h1>
	Instructions:<br>
	Receive Extracts from Heather McCreary and set date on this page<br>
	<div class="row">
		<div class="three columns">
			<h3>
				<?php echo $monthYear;?>
				<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$monthYear)));?>">
			</h3>
		</div>
	</div>
	<br clear="all">
	<?php
	$ImportFileName = "../gbs/importfiles/extract/BGASExtract_".$yearMonth.".xls";
	//echo "ImportFileName ".$ImportFileName."<br>";
	$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
	//echo $target_file;
	if(count($_FILES['fileToUpload1'])) {
		$tmpFileName = $_FILES["fileToUpload1"]["tmp_name"][0];
		if ($_GET['type'] == "uploadFile"){
			$ImportFileReceived = true;
			move_uploaded_file($tmpFileName, $target_file);
		}
	}
	if ($ImportFileReceived || $_GET['useUploadedFile']){
		//new import that data and parse it out 
		$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
		echo "Using File just uploaded<br>";
		ob_flush();
		include('../gbs/salesforce/read_rawFileReader.php');
		echo "will delete prior YearMonth ".$yearMonth." records and add new records<br>";
		$criteria2 = new stdClass();
		$criteria2->yearMonth = $yearMonth;
		//first delete old records
		$deleteExtractBgas = $cetDataProvider->deleteExtractDataBGAS($criteria2);
		//add new records
		foreach ($rowsRaw as $index=>$rowInfo){
			if ($rowInfo["SITEID"]){
				foreach ($rowInfo as $key=>$val){
					if (trim($val) == ""){
						$val = '';
					}
					if (substr($key,-3) == "_DT" || substr($key,-4) == "DATE"){
						$valorg = $val;
						$val = date("Y-m-d",strtotime($val));
						if ($val == "1969-12-31"){
	//						$dateFormat = "mm/dd/YYYY";
							$val = exceldatetotimestamp($valorg,$dateFormat);
	//						$val = date("m/d/Y",strtotime($val));
							if ($val == "12/31/1969"){
								$val = '';
							}
						}
					}
					$cleanData[$key] = $val;
				}
				$cleanRows[] = $cleanData;
			}
		}
		
		$insertExtractBgas = $cetDataProvider->insertExtractDataBGAS($cleanRows,$criteria2);
		echo count($insertExtractBgas["successes"])." records imported<br>";
		print_pre($insertExtractBgas["errors"]);
	}
	if (!$ImportFileReceived){
		$noExtractFile = true;

		if (file_exists($target_file)){
			$noExtractFile = false;
			$lastUploadedDateParts = explode("File",$ImportFileName);
			$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
			
			echo "<br><br><a href='".$ImportFileName."'>View Existing Extract File</a> last uploaded ".date("F d, Y H:ia",filemtime($target_file))."<br>";
			$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
			$trackingFile = $ImportFileName;
		}
	}
	?>


		<a id='uploadFile' href="#" class='button-link do-not-navigate'>Upload File</a><br>
		<div id="uploadFileForm" style='display:none;'>
			<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>residential/?type=uploadFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
				<div class="row">
					<div class="eight columns">
						<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
					</div>
					<div class="eight columns">
						<strong>File You Selected:</strong>
						<ul id="fileList1"><li>No Files Selected</li></ul>
						<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
					</div>
				</div>
				
				<script type="text/javascript">
					function makeFileList1() {
						var input = document.getElementById("fileToUpload1");
						var ul = document.getElementById("fileList1");
						while (ul.hasChildNodes()) {
							ul.removeChild(ul.firstChild);
						}
						for (var i = 0; i < input.files.length; i++) {
							var li = document.createElement("li"),
								fileName = input.files[i].name,
								fileLength = fileName.length,
								fileExt = fileName.substr(fileLength-4);
								console.log(fileExt);
							if (fileName.indexOf("CET") > 0){
									li.innerHTML = 'The file must only have just one tab for the results and not be the CET version.  Please make sure you are uploading the correct file.';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
							}else{
								if (fileExt == ".xls" || fileExt == "xlsx"){
									li.innerHTML = input.files[i].name;
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'block';
								}else{
									li.innerHTML = 'You must save '+fileName+' as a .xls or .xlsx file first';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
						}
						if(!ul.hasChildNodes()) {
							var li = document.createElement("li");
							li.innerHTML = 'No Files Selected';
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'none';
						}
					}
				</script>
			</form>
		</div>

	<script type="text/javascript">
		$(function () {
			$("#uploadFile").on('click',function(){
				$("#uploadFileForm").toggle();
			});
			
			var table = $('.simpleTable').DataTable({
				"scrollX": true,
	//			"scrollY": "300px",
				"bJQueryUI": true,
				"bSearchable":false,
				"bFilter":false,
				"bAutoWidth": true,
				"bSort": true,
				"iDisplayLength": 25,
				"paging":   false,
	//			"ordering": false,
				"scrollCollapse": true,
				"info":     false
			});
			var tableLimited = $('.simpleTableLimited').DataTable({
				"scrollX": true,
				"scrollY": "240px",
				"bJQueryUI": true,
				"bSearchable":false,
				"bFilter":false,
				"bAutoWidth": true,
				"bSort": true,
				"iDisplayLength": 10,
				"paging":   false,
	//			"ordering": false,
				"scrollCollapse": true,
				"info":     false
			});
			$(".DataTables_sort_wrapper").first().click().click();
			
			var getExtractFilters = function(){
				var monthYear = $("#MonthPicker").val(),
					filterValues = new Array(monthYear);
					
					return filterValues;
			};
			
			var updateExtractFilters = function(){
				var Filters=getExtractFilters(),
					StartDate = $("#StartDate").val();
					EndDate = $("#EndDate").val();
				
				window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>residential/?monthYear="+Filters[0]+"&StartDate="+StartDate+"&EndDate="+EndDate+"&nav=<?php echo $nav;?>#";
			}
			$(".parameters").on('change',function(){
				updateExtractFilters();
			});
			
			$('.date-picker').datepicker({
				showOn: "button",
				buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
				buttonImageOnly: true,
				buttonText: 'Click to change month',
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy',
				minDate: '01/01/14',
				maxDate: '<?php echo date("m/t/Y");?>',
				onClose:
					function(dateText, inst){
						var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						$(this).datepicker('setDate', new Date(year, month, 1));
						$("#StartDate").datepicker('setDate', new Date(year, month, 1));
						var date2 = $(this).datepicker('getDate');
						date2.setMonth(date2.getMonth() +1);
						date2.setDate(date2.getDate() -1);
						$("#EndDate").datepicker('setDate', date2);
						$("#weekPicker").val('');
						updateExtractFilters();
					}
			
			});

			
		});
	</script>

	<!--
	Copy the Sheet 'SQL Results' into new document.<br>
	Make sure header row is:<br>
	SITEID,CUST_FIRST_NAME,CUST_LAST_NAME,ADDRESS,CITY,STATE,ZIP,PLUS4,NUM_UNITS,PRIMARY_PRVD,PROVIDERID,PROVIDERCODE,ACCTCUST,LOCATIONID,SECONDARY_PRVD,PROVIDERID2,PROVIDERCODE2,ACCTCUST2,HEATFUEL,DHWFUEL,CUST_TYPE,APPTDATE,APPTTYPE,AUDITOR_FIRST_NAME,AUDITOR_LAST_NAME,CONTRACT,MEASTABLE,ID0,ID1,DESCRIPTION,PROP_PARTID,PROP_QTY,INS_PARTID,INS_QTY,MBTU_SAVINGS,DEPOSIT,CUST_PRICE,UTIL_PRICE,PAYEE,ISSUED_DT,SIGNED_DT,INSTALL_DT,INV_STATUS,MEAS_STATUS,INSTALLER_NAME,MEASURELIFE,ICTV_AWARE_SIR,ICTV_AWARE_SIMPLEPAYBACK,Therms<br>
	Then Remove the Header Row otherwise it gets imported as a row.<Br>
	Convert dates to YYYY-MM-DD format and save as CSV.<br>
	Open this file in NotePad++ and check for any dates that did not convert to the formate by searching for / and doing a text replace for that date in the new format.<br>
	Remove any blank last rows and re-save.<br>
	Open up PhpMyAdmin and make a backup copy of cri_extract_bgas.<br>
	Import the csv file you created into the table, cri_extract_bgas, using the header row above.<br>
	Then run this report for the date range you just imported.<br>
	-->
	For the Month of <?php echo $invoiceDate;?><br>
	To change month use Calendar to pick a month and year<br>
</div>
<div id='Invoice' class='tab-content'>
<?php
$extractBgas = $cetDataProvider->getCRIExtractDataBGAS($criteria);
$proposedContracts = $cetDataProvider->getProposedEZDocData($criteria);

$ocpReport = $cetDataProvider->getCRIOCP($criteria);
	$SiteID = 1;
	/*Deprecated 3/5/2018 by Andrea McCormack from CMCEnergy.com
	$jobCompleted[$SiteID] = array("SITEID","APPTDATE","APPTSTART","SCHEDAPPTTYPE","PROGRAMTYPE","ELECTRICUTILITYNAME","ELECTRICACCTNUM","GASUTILITYNAME","GASACCTNUM","HEATINGSOURCE","VENDOR1",
		"VENDOR2","VENDOR3","AGE","SQFT","BLDTYPE","UNITS","CONTACTNAME","CONTACTPHONE","FACILITYNAME","Custid","Fname","Lname","ADDRESS","CITY","STATE","ZIP","PHONE","CELL","EMAIL","LOWINCOME","MDATE");
	*/
	$jobCompleted[$SiteID] = array("SiteID","SchedDate","ApptStart","SchedAppType","ProgramType","ElectricUtilityName","ElectricAcctNum","GasUtilityName","GasAcctNum","HeatingSource","Vendor1","Vendor2","Vendor3","Fname","Lname","Address","City","State","Zip","Phone","Cell","Email","Vendor2Type");
		
		
	$jobInstalled[] = $jobCompleted;	
	$jobProposed[] = $jobCompleted;
	//$measureCompleted[$SiteID] = array("SITEID","MID","MDATE","MNUMERICVALUE","MTEXTVALUE","MBOOLEANVALUE","UTILITYBILLED","INSTALLER","INSTALLERTYPE");
	$measureCompleted[$SiteID] = array("SITEID","MID","MDATE","MNUMERICVALUE","LOCATION","UTILITYBILLED","INSTALLER","INSTALLERTYPE");
	$measuresProposed[$SiteID] = array("SITEID","MID","MDATE","MNUMERICVALUE","UTILITYBILLED");
	$measureInstalled[] = $measureCompleted;
	$measureProposed[] = $measuresProposed;
foreach ($extractBgas as $extractData){
	$SiteID = $extractData["SITEID"];
	$measureCompleted = array();
	$jobsCompleted[$SiteID]["SITEID"] = $SiteID;
	$measureCompleted[$SiteID]["SITEID"] = $SiteID;
	$measureCompleted[$SiteID]["MID"] = $extractData["INS_PARTID"];
	$jobsCompleted[$SiteID]["SCHEDDATE"] = MySQLDate($extractData["APPTDATE"]);
	$measureCompleted[$SiteID]["MDATE"] = MySQLDate($extractData["INSTALL_DT"]);
	$measureCompleted[$SiteID]["MNUMERICVALUE"] = $extractData["INS_QTY"];
	/*Deprecated 3/5/2018 by Andrea MCormack from CMCEnergy.com
	$measureCompleted[$SiteID]["MTEXTVALUE"] = "";
	$measureCompleted[$SiteID]["MBOOLEANVALUE"] = "";
	*/
	$measureCompleted[$SiteID]["LOCATION"] = "";
	$measureCompleted[$SiteID]["UTILITYBILLED"] = $extractData["PRIMARY_PRVD"];
	$measureCompleted[$SiteID]["INSTALLER"] = $extractData["INSTALLER_NAME"];
	$measureCompleted[$SiteID]["INSTALLERTYPE"] = "IIC";
	$jobsCompleted[$SiteID]["APPTSTART"] = "";
	$jobsCompleted[$SiteID]["SCHEDAPPTTYPE"] = $extractData["APPTTYPE"];
	$jobsCompleted[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsCompleted[$SiteID]["ELECTRICUTILITYNAME"] = $extractData["SECONDARY_PRVD"];
	$jobsCompleted[$SiteID]["ELECTRICACCTNUM"] = "";
	$jobsCompleted[$SiteID]["GASUTILITYNAME"] = $extractData["PRIMARY_PRVD"];
	$jobsCompleted[$SiteID]["GASACCTNUM"] = "";
	$jobsCompleted[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsCompleted[$SiteID]["VENDOR1"] = "CSG";
	$jobsCompleted[$SiteID]["VENDOR2"] = "CET";
	$jobsCompleted[$SiteID]["VENDOR3"] = "";
	/*Deprecated 3/5/2018 by Andrea McCormack from CMCEnergy.com
	$jobsCompleted[$SiteID]["AGE"] = "";
	$jobsCompleted[$SiteID]["SQFT"] = "";
	$jobsCompleted[$SiteID]["BLDTYPE"] = "";
	$jobsCompleted[$SiteID]["UNITS"] = "";
	$jobsCompleted[$SiteID]["CONTACTNAME"] = "";
	$jobsCompleted[$SiteID]["CONTACTPHONE"] = "";
	$jobsCompleted[$SiteID]["FACILITYNAME"] = "";
	$jobsCompleted[$SiteID]["Custid"] = $extractData["ACCTCUST"];
	*/
	$jobsCompleted[$SiteID]["Fname"] = $extractData["CUST_FIRST_NAME"];
	$jobsCompleted[$SiteID]["Lname"] = $extractData["CUST_LAST_NAME"];
	$jobsCompleted[$SiteID]["ADDRESS"] = $extractData["ADDRESS"];
	$jobsCompleted[$SiteID]["CITY"] = $extractData["CITY"];
	$jobsCompleted[$SiteID]["STATE"] = $extractData["STATE"];
	$jobsCompleted[$SiteID]["ZIP"] = (strlen($extractData["ZIP"]) >= 5 ? $extractData["ZIP"] : "0".$extractData["ZIP"]);
	$jobsCompleted[$SiteID]["PHONE"] = $SiteIDData[$SiteID]["phone"];
	$jobsCompleted[$SiteID]["CELL"] = "";
	$jobsCompleted[$SiteID]["EMAIL"] = "";
	//$jobsCompleted[$SiteID]["LOWINCOME"] = "";
	//$jobsCompleted[$SiteID]["MDATE"] = MySQLDate($extractData["INSTALL_DT"]);
	$jobsCompleted[$SiteID]["VENDOR2TYPE"] = "LEAD";
	$measureInstalled[] = $measureCompleted;
}
$jobInstalled[] = $jobsCompleted;
foreach ($proposedContracts as $ezDocData){
	$SiteID = $ezDocData["ResidentialEZDocData_SiteID"];
	$dataByFieldName = json_decode($ezDocData["ResidentialEZDocData_DataByFieldName"]);
	$dataByMeasure = json_decode($ezDocData["ResidentialEZDocData_DataByMeasure"]);
	$proposedDate = date("m/d/Y",strtotime($dataByFieldName->APPTDATE));
	if ($proposedDate == "12/31/1969"){$proposedDate = (strpos($dataByFieldName->APPTDATE,"/") ? date("m/d/Y",strtotime($dataByFieldName->APPTDATE)) : date("m/d/Y",strtotime(exceldatetotimestamp($dataByFieldName->APPTDATE,$criteria=null))));}

	foreach ($dataByMeasure as $measureType=>$measureData){
		if ($measureType !="Installed Today" && $measureType !="Contract_WIFI"){
			foreach ($measureData as $count=>$measureInfo){
				foreach ($measureInfo as $measure){
					if (trim($measure->PROP_PARTID) != ""){
						$measuresProposed = array();
						$measuresProposed[$SiteID]["SITEID"] = $SiteID;
						$measuresProposed[$SiteID]["MID"] = $measure->PROP_PARTID;
						$measuresProposed[$SiteID]["MDATE"] = $proposedDate;
						$measuresProposed[$SiteID]["MNUMERICVALUE"] = $measure->PROP_QTY;
						//$measuresProposed[$SiteID]["UTILITYBILLED"] = "BGAS"; //deprecated 3/13/2018 by request of Andrea McCormack from CMCEnergy.com to use Berkshire Gas instead.
						$measuresProposed[$SiteID]["UTILITYBILLED"] = "Berkshire Gas";
						
						$measureProposed[] = $measuresProposed;
					}
				}
			}
		}
	}
	
	//print_pre ($dataByFieldName->EXISTING_EQUIP_AGE_HEATING); //isolates Equip Age
	//print_pre ($dataByFieldName->EXISTING_HEAT_TYPE); //isolates Existing Heat Type
	//print_pre($dataByFieldName);
	//print_pre($dataByMeasure);
	//print_pre($ezDocData["ResidentialEZDocData_DataByFieldName"]["EXISTING_EQUIP_AGE_HEATING"])."<br>";
	//print_pre($ezDocData["ResidentialEZDocData_DataByFieldName"]["EXISTING_HEAT_TYPE"])."<br>";
	//echo($ezDocData["ResidentialEZDocData_DataByMeasure"])."<br>";
	//echo($ezDocData["ResidentialEZDocData_DataByFieldName"])."<br>"; //contains Existing Equipment and Age
	$jobsProposed[$SiteID]["SITEID"] = $SiteID;
	$jobsProposed[$SiteID]["SCHEDDATE"] = $proposedDate;
	$jobsProposed[$SiteID]["APPTSTART"] = "";
	$jobsProposed[$SiteID]["SCHEDAPPTTYPE"] = "";
	$jobsProposed[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsProposed[$SiteID]["ELECTRICUTILITYNAME"] = $dataByFieldName->SECONDARY_PRVD;
	$jobsProposed[$SiteID]["ELECTRICACCTNUM"] = $dataByFieldName->ACCTCUST2;
	//$jobsProposed[$SiteID]["GASUTILITYNAME"] = "BGAS"; //deprecated by Andrea McCormack from CMCEnergy.com 3/13/2018
	$jobsProposed[$SiteID]["GASUTILITYNAME"] = "Berkshire Gas";
	$jobsProposed[$SiteID]["GASACCTNUM"] = $dataByFieldName->ACCTCUST;
	$jobsProposed[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsProposed[$SiteID]["VENDOR1"] = "CET";
	$jobsProposed[$SiteID]["VENDOR2"] = $dataByFieldName->AUDITOR_FIRST_NAME;
	$jobsProposed[$SiteID]["VENDOR3"] = "";
	/*Deprecated 3/5/2018 by Andrea McCormack from CMCEnergy.com
	$jobsProposed[$SiteID]["AGE"] = "";
	$jobsProposed[$SiteID]["SQFT"] = "";
	$jobsProposed[$SiteID]["BLDTYPE"] = "";
	$jobsProposed[$SiteID]["UNITS"] = "";
	$jobsProposed[$SiteID]["CONTACTNAME"] = "";
	$jobsProposed[$SiteID]["CONTACTPHONE"] = "";
	$jobsProposed[$SiteID]["FACILITYNAME"] = "";
	$jobsProposed[$SiteID]["Custid"] = "";
	*/
	$jobsProposed[$SiteID]["Fname"] = $dataByFieldName->CUST_FIRST_NAME;
	$jobsProposed[$SiteID]["Lname"] = $dataByFieldName->CUST_LAST_NAME;
	$jobsProposed[$SiteID]["ADDRESS"] = $dataByFieldName->ADDRESS;
	$jobsProposed[$SiteID]["CITY"] = $dataByFieldName->CITY;
	$jobsProposed[$SiteID]["STATE"] = "MA";
	$jobsProposed[$SiteID]["ZIP"] = (strlen($dataByFieldName->ZIP) >= 5 ? $dataByFieldName->ZIP : "0".$dataByFieldName->ZIP);
	$jobsProposed[$SiteID]["PHONE"] = $dataByFieldName->PHONE;
	$jobsProposed[$SiteID]["CELL"] = "";
	$jobsProposed[$SiteID]["EMAIL"] = $dataByFieldName->EMAIL;
	//$jobsProposed[$SiteID]["LOWINCOME"] = "";
	//$jobsProposed[$SiteID]["MDATE"] = $proposedDate;
	$jobsProposed[$SiteID]["VENDOR2TYPE"] = "LEAD";
}
//print_pre($jobsProposed);
/* old way when CSG sent us OCP data file that went into table cri_ocp
foreach ($ocpReport as $ocpReportData){
	$SiteID = $ocpReportData["SITEID"];
	$measuresProposed = array();
	$jobsProposed[$SiteID]["SITEID"] = $SiteID;
	$measuresProposed[$SiteID]["SITEID"] = $SiteID;
	$measuresProposed[$SiteID]["MID"] = $ocpReportData["PARTID"];
	$measuresProposed[$SiteID]["MDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$measuresProposed[$SiteID]["MNUMERICVALUE"] = $ocpReportData["QTY"];
	$measuresProposed[$SiteID]["UTILITYBILLED"] = $ocpReportData["PRIMARY_PROVIDER"];
	$jobsProposed[$SiteID]["SCHEDDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$jobsProposed[$SiteID]["APPTSTART"] = "";
	$jobsProposed[$SiteID]["SCHEDAPPTTYPE"] = "";
	$jobsProposed[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsProposed[$SiteID]["ELECTRICUTILITYNAME"] = $SiteIDData[$SiteID]["secondaryutility"];
	$jobsProposed[$SiteID]["ELECTRICACCTNUM"] = "";
	$jobsProposed[$SiteID]["GASUTILITYNAME"] = $ocpReportData["PRIMARY_PROVIDER"];
	$jobsProposed[$SiteID]["GASACCTNUM"] = "";
	$jobsProposed[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsProposed[$SiteID]["VENDOR1"] = "CSG";
	$jobsProposed[$SiteID]["VENDOR2"] = $ocpReportData["CREW"];
	$jobsProposed[$SiteID]["VENDOR3"] = "";
	$jobsProposed[$SiteID]["AGE"] = "";
	$jobsProposed[$SiteID]["SQFT"] = "";
	$jobsProposed[$SiteID]["BLDTYPE"] = "";
	$jobsProposed[$SiteID]["UNITS"] = "";
	$jobsProposed[$SiteID]["CONTACTNAME"] = "";
	$jobsProposed[$SiteID]["CONTACTPHONE"] = "";
	$jobsProposed[$SiteID]["FACILITYNAME"] = "";
	$jobsProposed[$SiteID]["Custid"] = "";
	$jobsProposed[$SiteID]["Fname"] = $ocpReportData["FIRST_NAME"];
	$jobsProposed[$SiteID]["Lname"] = $ocpReportData["LAST_NAME"];
	$jobsProposed[$SiteID]["ADDRESS"] = $ocpReportData["ADDRESS"];
	$jobsProposed[$SiteID]["CITY"] = $ocpReportData["CITY"];
	$jobsProposed[$SiteID]["STATE"] = $ocpReportData["STATE"];
	$jobsProposed[$SiteID]["ZIP"] = (strlen($ocpReportData["ZIP"]) >= 5 ? $ocpReportData["ZIP"] : "0".$ocpReportData["ZIP"]);
	$jobsProposed[$SiteID]["PHONE"] = $ocpReportData["HOME"];
	$jobsProposed[$SiteID]["CELL"] = $ocpReportData["CELL"];
	$jobsProposed[$SiteID]["EMAIL"] = $ocpReportData["CUSTOMER_EMAIL"];
	$jobsProposed[$SiteID]["LOWINCOME"] = "";
	$jobsProposed[$SiteID]["MDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$measureProposed[] = $measuresProposed;

}
*/
$jobProposed[] = $jobsProposed;
	
$fileTypes = array("job","measure");
$fileCategories = array("Installed","Proposed");
//$fileCategories = array("Installed"); //ClearResults will send Proposed because they changed the format of the OCP data on us to a degree that did not provide the information we needed
$folderLocation = $filePath."residential/CRIData/";
foreach ($fileCategories as $fileCategory){
	foreach ($fileTypes as $fileType){
		$CompleteFileNameTxt = $folderLocation.$fileType.$fileCategory.$yearMonth.".txt";
		$arrayName = $fileType.$fileCategory;
		//echo $CompleteFileNameTxt."<br>";
		$fpCompleted = fopen($CompleteFileNameTxt, 'wt');
		foreach (${$arrayName} as $id=>$siteId) {
			foreach ($siteId as $field){
				fputcsv($fpCompleted, $field,"\t");
				//echo print_pre($field);
			}
		}
		//echo "<hr>";
		fclose($fpCompleted);
		//remove all quote encapsulation
		$data = file_get_contents($CompleteFileNameTxt);
		$data = str_replace('"','', $data);
		$data = str_replace('\n','\r\n', $data);
		file_put_contents($CompleteFileNameTxt, $data);
		//$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'residential/'.$CompleteFileNameTxt.'">'.$CompleteFileNameTxt.'</a><br>';
		$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'residential/'.str_replace($filePath."residential","",$CompleteFileNameTxt).'">'.str_replace($filePath."residential/CRIData/","",$CompleteFileNameTxt).'</a><br>';
		$files_to_zip[] = $CompleteFileNameTxt;

		$CompleteFileNameCsv = $folderLocation.$fileType.$fileCategory.$yearMonth.".csv";
		$fpCompleted = fopen($CompleteFileNameCsv, 'w');
		foreach (${$arrayName} as $id=>$siteId) {
			foreach ($siteId as $field){
				fputcsv($fpCompleted, $field);
			}
		}
		fclose($fpCompleted);
		//$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'residential/'.$CompleteFileNameCsv.'">'.$CompleteFileNameCsv.'</a><br><hr>';
		$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'residential/'.str_replace($filePath."residential","",$CompleteFileNameCsv).'">'.str_replace($filePath."residential/CRIData/","",$CompleteFileNameCsv).'</a><br><hr>';
		$files_to_zip[] = $CompleteFileNameCsv;
		
	}
}
$zippedFileName = $folderLocation.'CET_CRIData_'.$yearMonth.'.zip';
$zippedFiles = create_zip($files_to_zip,$zippedFileName,true,$folderLocation);

$criteria = new stdClass();
$criteria->yearMonth = $yearMonth;
$criteria->primaryProvider = "BERKSHIR";
$BgasExtractData = $cetDataProvider->getCRIExtractDataBGAS($criteria);
//print_pre($BgasExtractData);
foreach ($BgasExtractData as $extractData){
	$SiteID = $extractData["SITEID"];
	$extractDataBySiteID[$SiteID]["locationId"] = $extractData["LOCATIONID"];
	$extractDataByLocationID[$extractData["LOCATIONID"]] = $extractData;
	$extractDataLocationIds[] = $extractData["LOCATIONID"];
	$contractType = $extractData["CONTRACT"];
	if (strpos($contractType,"_A")){
		$invoiceExtractData[$SiteID]["asTherms"] = bcadd($invoiceExtractData[$SiteID]["asTherms"],bcdiv($extractData["MBTU_SAVINGS"],100,6),6);
	}
	if (strpos($contractType,"_W")){
		$invoiceExtractData[$SiteID]["insTherms"] = bcadd($invoiceExtractData[$SiteID]["insTherms"],bcdiv($extractData["MBTU_SAVINGS"],100,6),6);
	}
//	if ($extractData["MEAS_STATUS"] == "BILLING"){
		$allExtractSiteIds[$SiteID] = ucwords(strtolower($extractData["CUST_FIRST_NAME"]." ".$extractData["CUST_LAST_NAME"]));
		$allExtractSiteStatus[$SiteID] = $extractData["MEAS_STATUS"];
//	}
	if ($extractData["MEAS_STATUS"] == "INSTALLED"){
		$HPCExtention = (strpos(" ".strtolower($extractData["INSTALLER_NAME"]),"american") ? "_HPC" : "");
		$InsPartID = str_replace("_HPC","",$extractData["INS_PARTID"]).$HPCExtention;
		$invoiceExtractDataInstalls[$InsPartID]["count"] = bcadd($invoiceExtractDataInstalls[$InsPartID]["count"],$extractData["PROP_QTY"],0);
		$invoiceExtractDataInstalls[$InsPartID]["therms"] = bcadd($invoiceExtractDataInstalls[$InsPartID]["therms"],bcdiv($extractData["MBTU_SAVINGS"],100,6),6);
	}
	if ($eobData[$SiteID]){
		$eobExtractDetails[$SiteID][] = $extractData;
		$eobExtractData[$SiteID]["firstName"] = $extractData["CUST_FIRST_NAME"];
		$eobExtractData[$SiteID]["lastName"] = $extractData["CUST_LAST_NAME"];
		$eobExtractData[$SiteID]["address"] = $extractData["ADDRESS"];
		$eobExtractData[$SiteID]["city"] = $extractData["CITY"];
		$eobExtractData[$SiteID]["state"] = $extractData["STATE"];
		$eobExtractData[$SiteID]["zip"] = $extractData["ZIP"];
		$eobExtractData[$SiteID]["contractor"] = ($extractData["INSTALLER_NAME"] && strtoupper($extractData["INSTALLER_NAME"]) != "CET" ? $extractData["INSTALLER_NAME"] : $eobExtractData[$SiteID]["contractor"]);
	}
}
//print_pre($invoiceExtractDataInstalls);
	echo "Comparison of Sharepoint and Extract Site IDs<br>";
	echo "<table border=1 cellpadding=5><tr><td>SharePoint Sites</td><td>Extract File Sites</td></tr>";
	asort($allSharePointSiteIds);
	foreach ($allSharePointSiteIds as $siteId=>$fullName){
		if (strpos(" ".strtolower($allExtractSiteStatus[$siteId]),"billing")){
			$class = "matched";
			$lastMatchedExtractSiteID = $siteId;
			$status = "";
		}else{
			$class = "unmatched";
			$spacesAfterExtract[($lastMatchedExtractSiteID ? : "top")]++;
			$status = " [".$allExtractSiteStatus[$siteId]."]";
		}
		$spSiteData[$siteId] = "<span class='".$class."' id='SPsiteId".$siteId."'>".$siteId.": ".$fullName.$status."</span><br>";
	}
	asort($allExtractSiteIds);
	foreach ($allExtractSiteIds as $siteId=>$fullName){
		$status = $allExtractSiteStatus[$siteId];
			if (strpos(" ".strtolower($status),"billing")){
				if (!$allSharePointSiteIds[$siteId]){
					$class = "unmatched";
					$lowIncomeSiteIds[$siteId] = $fullName;
					$lowIncome = " [".(strtoupper($status) != "BILLING-LI" ? "possible low income" : $status)."]";
					$spacesAfterSP[($lastMatchedSPSiteID ? : "top")]++;
				}else{
					$class = "matched";
					$lowIncome = "";
					$lastMatchedSPSiteID = $siteId;
				}
				$extractSiteData[$siteId] = "<span class='".$class."'>".$fullName.": ".$siteId.$lowIncome."</span><br>";
			}
	}
	if (count($spacesAfterExtract) || count($spacesAfterSP)){
		$Adjustments["Review"]["Mismatch between Sharepoint and Extract"] = "Review the Invoice tab to see list of mismatched SiteID's.";
	}

	echo "<Tr><td valign='top'>";
		if ($spacesAfterSP["top"]){
			$x = 0;
			while ($x < $spacesAfterSP["top"]){
				echo "<br>";
				$x++;
			}
		}
	foreach ($spSiteData as $siteId=>$row){
		echo $row;
		if ($spacesAfterSP[$siteId]){
			$x = 0;
			while ($x < $spacesAfterSP[$siteId]){
				echo "<br>";
				$x++;
			}
		}
	}
	echo "</td><td valign='top'>";
		if ($spacesAfterExtract["top"]){
			$x = 0;
			while ($x < $spacesAfterExtract["top"]){
				echo "<br>";
				$x++;
			}
		}
	foreach ($extractSiteData as $siteId=>$row){
		echo $row;
		if ($spacesAfterExtract[$siteId]){
			$x = 0;
			while ($x < $spacesAfterExtract[$siteId]){
				echo "<br>";
				$x++;
			}
		}
	}

	echo "</td></tr></table>";
	echo "<h3>BGAS Invoice File</h3>";
//	print_pre($allExtractSiteIds);
//	print_pre($invoiceExtractDataInstalls);
	include_once('model/BAGSInvoiceReport.php');
	echo $ReportsFileLink;
	echo "</div>";
	
	echo "<div id='BGASFile' class='tab-content'>";
		if ($noExtractFile){
			echo "Extract File Must be Uploaded First";
		}else{
			include_once($dbProviderFolder."GBSProvider.php");
			$GBSProvider = new GBSProvider($dataConn);
			$criteria = new stdClass();
			$criteria->selectFields = "Rate, Premise";
			$criteria->inPremise = $extractDataLocationIds;
			$rateResults = $GBSProvider->getBGASCustomers($criteria);
			//print_pre($rateResults);
			foreach ($rateResults as $record){
				$BgasNoteRates[$record["Rate"]][] = $record["Premise"];
				if ($record["Rate"] != "RESIDENTIAL HEAT, R3T"){
					$BgasNotes[$record["Premise"]] = $record["Rate"];
				}
			}
			foreach ($BgasNotes as $locationId=>$rate){
				echo $extractDataByLocationID[$locationId]["SITEID"]." ".$extractDataByLocationID[$locationId]["CUST_LAST_NAME"]." ".$rate."<br>"; 
			}
			//include_once('model/BAGSNotesReport.php');
			echo $ReportsFileLink;
		}//end if no extractFile
	echo "</div>";
	
	echo "<div id='EOB' class='tab-content'>";
		echo "<h3>EOB File</h3>";
		echo count($eobData)." SharePoint Data with change in contract price<br>";
		ksort($eobData);
		ksort($eobExtractData);
		echo count($eobExtractData)." Extract Data with change in contract price<br>";
		if (count($eobData) != count($eobExtractData)){
			echo "<table border=1 cellpadding=5><tr><td>SharePoint Sites</td><td>Extract File Sites</td></tr>";
			echo "<Tr><td valign='top'>";
			foreach ($eobData as $siteId=>$data){
				echo $siteId."<br>";
			}
			echo "</td><td valign='top'>";
			foreach ($eobExtractData as $siteId=>$data){
				echo $siteId."<br>";
			}
			echo "</td></tr></table>";
			echo "Will Not Create EOB Files!<Br>";
			$Adjustments["Alert"]["EOB File Not Created"] = "Sharepoint Site ID's with change in contract price not found in Extract";
		}else{
			include_once('model/eobReport.php');
			echo $ReportsFileLink;
		}
	echo "</div>";
/*
$jobsCompleteFileNameTxt = "jobsCompleted.txt";
$fpCompleted = fopen($jobsCompleteFileNameTxt, 'w');
foreach ($jobCompleted as $fields) {
    fputcsv($fpCompleted, $fields,"\t");
}
fclose($fpCompleted);

$jobsCompleteFileNameCsv = "jobsCompleted.csv";
$fpCompleted = fopen($jobsCompleteFileNameCsv, 'w');
foreach ($jobCompleted as $fields) {
    fputcsv($fpCompleted, $fields);
}
fclose($fpCompleted);

$jobsProposedFileNameTxt = "jobsProposed.txt";
$fpProposed = fopen($jobsProposedFileNameTxt, 'w');
foreach ($jobProposed as $fields) {
    fputcsv($fpProposed, $fields,"\t");
}
fclose($fpProposed);

$jobsProposedFileNameCsv = "jobsProposed.csv";
$fpProposed = fopen($jobsProposedFileNameCsv, 'w');
foreach ($jobProposed as $fields) {
    fputcsv($fpProposed, $fields);
}
fclose($fpProposed);
*/
		echo "<div id='CRI' class='tab-content'>";
			echo "<h3>CRI Data</h3>";
				echo $fileLinks;
				echo "Zip files at: <a href='".$CurrentServer.$adminFolder."residential/CRIData/".str_replace($filePath."residential/CRIData/","",$zippedFileName)."'>".str_replace($filePath."residential/CRIData/","",$zippedFileName)."</a><br>";
		echo "</div>";
?>
<!--
<a href="<?php echo $CurrentServer.$adminFolder."residential/".$jobsCompleteFileNameTxt;?>">JobsCompleted.txt</a><br>
<a href="<?php echo $CurrentServer.$adminFolder."residential/".$jobsCompleteFileNameCsv;?>">JobsCompleted.csv</a>
<hr>
<a href="<?php echo $CurrentServer.$adminFolder."residential/".$jobsProposedFileNameTxt;?>">JobsProposed.txt</a><br>
<a href="<?php echo $CurrentServer.$adminFolder."residential/".$jobsProposedFileNameCsv;?>">JobsProposed.csv</a>
-->
<?php
		
		include_once('../gbs/views/_adjustmentsTab.php');
		$invoiceCodeName = "BGASResidentialInvoice";
		include_once('../gbs/views/_notesTab.php');
		
	echo "</div>";
?>
<script type="text/javascript">
	<?php if ($noExtractFile){?>alert('You must set Upload Extract File first');<?php }?>
	$(function () {
	
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
?>