<h3>Inspection Schedule Report	
<?php if (!$ReadOnlyTrue){?>
	<!--<a href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=edit-file&id=50" class="button-link">Upload Inspection Schedule Data</a><?php }?>-->
</h3>
<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);
$utilitySelected = ($_GET['utility'] ? $_GET['utility'] : "Berkshire Gas");
//Get Refused Inspections:
$InspectionsRefused = $CSGDataProvider->getInspectionRefused();
foreach ($InspectionsRefused as $refused){
	$recordedStatus[$refused["siteId"]] = $refused["status"];
	//$refusedInspections[]=$refused["siteId"];
}

$StartDate = ($_GET['StartDate'] ? : date("m/d/Y",strtotime(date()."-60 days")));
//get SPData for Utility
$path = ($path ? $path : $siteRoot.$adminFolder);
include_once($path.'sharepoint/SharePointAPIFiles.php');
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	//echo "<span class='memoryusage'>Processing Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	//ob_flush();
	foreach (${$ArrayName} as $SPData){
//		print_pre($SPData);
		$siteId = $SPData["site_x0020_id"];
		$lastName = $SPData["last_x0020_name"];
		$firstName = $SPData["first_x0020_name"];
		$phone = $SPData["phone_x0020_number"];
		$utility = trim($SPData["utility"]);
		$billingDateParts = explode(" ",$SPData["utility_x0020_billing_x0020_date"]);
		$inspectionDateParts = explode(" ",$SPData["inspection_x0020_date"]);
		$contractor = $SPData["contractor"];
		$town = $SPData["town"];
		$streetAddress = $SPData["street_x0020_address"];
		if (strtotime($billingDateParts[0]) > strtotime($StartDate) && $utility == "Berkshire Gas"){
			$InspectionData = new stdClass();
			$InspectionData->LastName = $lastName;
			$InspectionData->FirstName = $firstName;
			$InspectionData->InspectionDate = (strtotime($inspectionDateParts[0]) ? date("m/d/Y",strtotime($inspectionDateParts[0])) : null);
//			$InspectionData->Address = $ColumnData["Address"];
//			$InspectionData->City = $ColumnData["City"];
			$InspectionData->Cell = $phone;
//			$InspectionData->Home = $ColumnData["Home"];
//			$InspectionData->Email = (strpos($ColumnData["Email"],"@") ? $ColumnData["Email"] : "");
			$InspectionData->ContractorName = $contractor;
			$InspectionData->Town = $town;
			$InspectionData->StreetAddress = $streetAddress;
//			$InspectionData->ContractorRank = $ColumnData["Contractor_Rank"]; //L,M,H (low medium high)
//			$InspectionData->PriorCalls = ($ColumnData["Num_of_Calls"] ? $ColumnData["Num_of_Calls"] : 0);
//			$InspectionData->Campaign = $ColumnData["Campaign"];
			$InspectionReport[$siteId] = $InspectionData;
		}
		
	}//end foreach sprecord
	${$ArrayName} = null;
	//echo "<span class='memoryusage'>Completed Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	//ob_flush();

	//echo "<br>MemoryUsage:".$ArrayName." ".memory_get_usage();
}//end foreach view
//echo "<br>MemoryUsage3:".memory_get_usage();
//echo "<span class='memoryusage'><Br>Processing Complete.  Please stand by while page finishes rendering: Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
//ob_flush();

//print_pre($SPUtilities);

ksort($InspectionReport);
//print_pre($InspectionReport);
?>
<style>
	th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
	.statusSelector {
		width:150px;
	}
	.updatedSelector {
		border: 2px solid green;
	}

</style>
<script>
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				/*
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				*/
				updateFilters();
			}
		});
		var getFilters = function(){
			var StartDate = $("#StartDate").val(),
			filterValues = new Array(StartDate);
			return filterValues;
		};
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>residential/?StartDate="+Filters[0]+"&nav=inspection-schedule";
		}
		
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"scrollCollapse": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 100
		});
		
		$(".statusSelector").on('change',function(){
			var $this = $(this),
				siteId = $this.attr('data-siteid'),
				thisClass = "."+siteId,
				thisValue = $this.val(),
				thisAlertSpan = "#statusResults"+siteId,
				data = {};
				data["action"] = "mark_refused";
				data["siteId"] = siteId;
				data["status"] = thisValue;
				$('#refuseResults').hide();
				//console.log(data);
				$.ajax({
					url: "ApiInspectionManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						//console.log(data);
						$this.addClass("updatedSelector");
						$(thisAlertSpan).html('status changed').fadeOut(3000);
						//$(thisClass).hide();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#refuseResults').show().html(message).addClass("alert");
					}
				});
		});
		$("#utilitySelector").on('change',function(){
			var thisVal = $(this).val();
			location.href='<?php echo $CurrentServer.$adminFolder;?>residential/?nav=inspection-schedule&utility='+thisVal;
		});
		
	});
</script>
<?php echo count($InspectionReport);?> Berkshire Gas Sites with Billing Date After: <input type="text" class="date" id="StartDate" value="<?php echo $StartDate;?>" title="Start Date">

<?php if (count($InspectionReport)){?>
<!--	Showing Utility <select id="utilitySelector"><option value="Berkshire Gas"<?php echo ($utilitySelected == "Berkshire Gas" ? " selected" : "");?>>Berkshire Gas</option><option value="WMECO"<?php echo ($utilitySelected == "WMECO" ? " selected" : "");?>>Eversource/WMECO</option></select>-->
	<div id="refuseResults"></div>
			<table class="reportResults display" cellspacing="0">
				<thead>
					<tr>
						<th class="exportable">Inspection Date</th>
						<th class="exportable">SiteID</th>
						<th>Status</th>
						<th class="exportable">Last Name</th>
						<th class="exportable">First Name</th>
						<th class="exportable">Address</th>
						<th class="exportable">Phone</th>
						<th class="exportable">Contractor</th>
<!--						
						<th class="exportable">Address</th>
						<th class="exportable">City</th>
						<th class="exportable">Rank</th>
						<th class="exportable">Prior Calls</th>
						<th class="exportable">Campaign</th>
						<th class="exportable">Utility</th>
-->
					</tr>
				</thead>
				<tbody>
					<?php 
						$statusArray = array("Refused","Accepted","Scheduled","Left Message");
						foreach ($InspectionReport as $SiteID=>$inspectionData){
						?>
							<tr class="<?php echo $SiteID;?>">
									<td><?php echo $inspectionData->InspectionDate;?></td>
									<td><?php echo $SiteID;?></td>
									<td>
										<select class="statusSelector" data-siteid="<?php echo $SiteID;?>">
											<option></option>
											<?php 
												foreach ($statusArray as $status){
													echo "<option value='".$status."'".($recordedStatus[$SiteID] == $status ? " selected" : "").">".$status."</option>";
												}
											?>
										</select>
										<span id="statusResults<?php echo $SiteID;?>" class="alert info"></span>
									</td>
									<td><?php echo $inspectionData->LastName;?></td>
									<td><?php echo $inspectionData->FirstName;?></td>
									<td><?php echo $inspectionData->Town;?><br><?php echo $inspectionData->StreetAddress;?></td>
									<td><?php echo ($inspectionData->Cell ? $inspectionData->Cell." (cell)\r\n " : "").($inspectionData->Home ? $inspectionData->Home." (home)" : "");?></td>
									<td><?php echo $inspectionData->ContractorName;?></td>
<!--									
									<td><?php echo $inspectionData->Address;?></td>
									<td><?php echo $inspectionData->City;?></td>
									<td><?php echo ($inspectionData->ContractorRank != "N/A" ? $inspectionData->ContractorRank : "");?></td>
									<td><?php echo $inspectionData->PriorCalls;?></td>
									<td><?php echo ($inspectionData->Campaign != "NONE" ? $inspectionData->Campaign : "");?></td>
									<td><?php echo $utilitySelected;?></td>
-->
								 </tr>
						<?php
						}
					?>
				</tbody>
			</table>
<?php }//end if count ?>