<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");
$muniProvider = new MuniProvider($dataConn);

//print_pre($scheduledEvents);
//get WGE audit data
$criteria = new stdClass();
$criteria->auditStatus = 1;
$criteria->auditDateNotSet = true;
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
//echo count($customerCollection). " Muni records<br>"; 
if (count($customerCollection)){
	foreach ($customerCollection as $result=>$record){
		//print_pre($record);
		$ToBeScheduled["WGE"][]= array("value"=>$record->id,"label"=>$record->accountHolderFullName." - ".$record->installedAddress." ".$record->installedCity,"electricProvider"=>"WGE","email"=>$record->ownerEmail,"notes"=>$record->customerNotes,"source"=>"Muni");
	}
}
?>