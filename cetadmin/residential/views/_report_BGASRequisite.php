<?php
ini_set('memory_limit', '512M');
error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$warehouseProvider = new WarehouseProvider($dataConn);
$warehouseResults = $warehouseProvider->get($criteria = null);
$warehouseCollection = $warehouseResults->collection;
//Get list of active warehouse locations
foreach ($warehouseCollection as $warehouse){
	if ($warehouse->displayOrderId && strpos(" ".$warehouse->name,"CET")){
		$ActiveWarehouses[] = $warehouse->name;
	}
}
$ActiveWarehouseString = implode("','",$ActiveWarehouses);

include_once($dbProviderFolder."CETDataProvider.php");
$cetDataProvider = new CETDataProvider($dataConn);
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	foreach (${$ArrayName} as $SPData){
		$spData = (object)$SPData;
		//$results[] = $cetDataProvider->insertSharePointData($spData);
		include('views/_SharePointRequisitionFields.php');
		erase_val($SPData);
		$SPData = null;
	}//end foreach sprecord
	${$ArrayName} = null;
}//end foreach view
//echo $yearMonth." ".count($eobData[$yearMonth])." EOBs";
//print_pre($eobData);
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
</style>

<h1>BGAS Requisition File</h1>
Instructions:<br>
Set date on this page. Data is pulled from Sharepoint<br>
<div class="row">
	<div class="three columns">
		<h3>
			<?php echo $monthYear;?>
			<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$monthYear)));?>">
		</h3>
	</div>
</div>
<br clear="all">
<?php 
include_once('model/BAGSrequistionReport.php');
echo $ReportsFileLink;

print_pre($allData["S000020202131"]);
print_pre($unreqData);
/*
if (count($unreqData){
	foreach ($unreqData as $contractor=>$clientInfo){
		foreach
		
	}
}
*/
?>
<script type="text/javascript">
	$(function () {
		$("#uploadFile").on('click',function(){
			$("#uploadFileForm").toggle();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		var tableLimited = $('.simpleTableLimited').DataTable({
			"scrollX": true,
			"scrollY": "240px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 10,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
		
		var getFilters = function(){
			var monthYear = $("#MonthPicker").val(),
				filterValues = new Array(monthYear);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>residential/?monthYear="+Filters[0]+"&nav=report-BGASRequisite#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		$('.date-picker').datepicker({
			showOn: "button",
			buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
			buttonImageOnly: true,
			buttonText: 'Click to change month',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			minDate: '01/01/14',
			maxDate: '<?php echo date("m/t/Y");?>',
			onClose:
				function(dateText, inst){
					var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, month, 1));
					$("#StartDate").datepicker('setDate', new Date(year, month, 1));
					var date2 = $(this).datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$("#EndDate").datepicker('setDate', date2);
					$("#weekPicker").val('');
					updateFilters();
				}
		
		});

		
	});
</script>
