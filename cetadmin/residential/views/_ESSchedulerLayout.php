<br>
	<h1>Energy Specialist Scheduling</h1>
	<br>
<?php

$areaFilterReceived = $_GET['Area'];
$areaFilter["PioneerValley"] = array("Greenfield","Deerfield","Montague","Turners Falls","Turners","Whately","Sunderland","Hatfield","Hadley","Amherst");
$areaFilter["South Berkshire"] = array("Great Barrington","Great","Stockbridge","Lee");
$areaFilter["North Berkshire"] = array("Willimston","Clarksburg","North Adams","Adams","North","Cheshire");
$areaFilter["Central Berkshire"] = array("Dalton","Lenox","Pittsfield","Dalton","Lanesborough");
$areaFilter["Eastern Mass"] = array("Boston","Westborough","Lynn");


include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->isHES = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeByName[$record->firstName] = $record;
}
ksort($EmployeeByName);

include_once('views/_ESSchedulerLayout_SalesforceData.php');
include_once('views/_ESSchedulerLayout_Muni.php');
include_once('views/_ESSchedulerLayout_HES.php');

include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$criteria = new stdClass();
$criteria->energySpecialistNot = "Cancelled";
$criteria->statusNot = "Cancelled";
$scheduledResultsPulldown = $residentialProvider->getScheduledData($criteria);
$criteria->city= ($_GET['CityName']!= "" ? $_GET['CityName']: "");
$criteria->visitType =  ($_GET['VisitType'] != "" ? $_GET['VisitType']: "");
$criteria->energySpecialist = ($_GET['SpecialistName'] != "" ? $_GET['SpecialistName']: "");
$scheduledResults = $residentialProvider->getScheduledData($criteria);
$scheduledDates["earliest"] = strtotime(date("m/d/Y"));
$scheduledDates["latest"] = strtotime(date("m/d/Y"));
foreach ($scheduledResults->collection as $record){
	$dateOfAppt = date("Y-m-d",strtotime($record->apptDate));
	$startTimeStr = strtotime($record->apptDate);
	$esParts = explode(" ",$record->energySpecialist);
	$ES = $esParts[0]." ".substr($esParts[1],0,1);
	
	$addressParts = explode(", ", $record -> address); 
	$cityState = $addressParts[1];
	$cityPart = explode(" MA",$cityState);
	$City = $cityPart[0];
	$VisType = $record-> visitType;
	if ($areaFilterReceived){
		if (in_array($City,$areaFilter[$areaFilterReceived])){
			$include = true;
		}else{
			$include = false;
		}
	}else{
			$include = true;
	}
		if (strtotime($record->apptDate) >= strtotime('Today')){
			if ($include){
				$ESList[$ES] = 1;
				$scheduledDataResults[$dateOfAppt][$ES][$startTimeStr.$record->id] = $record;
				$scheduledDates["latest"] = (strtotime($record->apptDate) > $scheduledDates["latest"] ? strtotime($record->apptDate) : $scheduledDates["latest"]);
			}
		}else{
			$ESListPast[$ES] = 1;
			$scheduledDataResultsPast[$dateOfAppt][$ES][$startTimeStr.$record->id] = $record;
		}
	
}
//print_pre($scheduledDataResults["2018-07-17"]);
//add one extra month to the latest date for availability purposes
$scheduledDates["latest"] = strtotime(date("Y-m-d",$scheduledDates["latest"])." +1 month");
$daysBetweenEarliestAndLatest = datediff(date("m/d/Y",$scheduledDates["earliest"]),date("m/d/Y",$scheduledDates["latest"]));


$filteredByList = array();
	if ($_GET['Area']){
		$filteredByList[] = "Area: ".$_GET['Area'];
	}
	if ($_GET['CityName']){
		$filteredByList[] = "City: ".$_GET['CityName'];
	}
	if ($_GET['SpecialistName']){
		$filteredByList[] = "ES: ".$_GET['SpecialistName'];
	}
	if ($_GET['VisitType']){
		$filteredByList[] = "VisitType: ".$_GET['VisitType'];
	}

$availableCriteria = new stdClass();
if (!count($filteredByList)){
	$getAvailability = $residentialProvider->getScheduleAvailable($availableCriteria);
	//print_pre($getAvailability);
	foreach ($getAvailability->collection as $record){
		$es = $record->ResidentialScheduledAvailable_ES;
		$id = $record->ResidentialScheduledAvailable_ID;
		$includeAvailability = false;
		if ($record->ResidentialScheduledAvailable_Date){
			$includeAvailability = false;
			if (strtotime($record->ResidentialScheduledAvailable_Date) >= strtotime(date("Y-m-d"))){
				$includeAvailability = true;
			}
		}else{
			$includeAvailability = true;
		}
		if ($includeAvailability){
			$availability[$es][$id]["days"] = $record->ResidentialScheduledAvailable_Days;
			$availability[$es][$id]["ES"] = $record->ResidentialScheduledAvailable_ES;
			$availability[$es][$id]["date"] = $record->ResidentialScheduledAvailable_Date;
			$availability[$es][$id]["startTime"] = $record->ResidentialScheduledAvailable_StartTime;
			$availability[$es][$id]["endTime"] = $record->ResidentialScheduledAvailable_EndTime;
			$availability[$es][$id]["totalHours"] = $record->ResidentialScheduledAvailable_TotalHours;
			$availability[$es][$id]["types"] = $record->ResidentialScheduledAvailable_Types;
			$availability[$es][$id]["status"] = $record->ResidentialScheduledAvailable_Status;
			if ($record->ResidentialScheduledAvailable_Status == "Active"){
				$ESList[$es] = 1;
				$ESListAvailable[$es] = 1;
				if ($record->ResidentialScheduledAvailable_Days){
					$availableDays = explode(",",$record->ResidentialScheduledAvailable_Days);
					foreach ($availableDays as $availableDay){
						$availabilityBy["Day"][$availableDay][$es][] = $record;
					}
					//$availabilityByDay["DaysOfTheWeek"][] = $availability[$es][$id];
					
				}
				//Kacie D requested that Date will override Day of the week and remove other availabilities
				if ($record->ResidentialScheduledAvailable_Date){
					$availabilityBy["Date"][strtotime($record->ResidentialScheduledAvailable_Date)][$es][] = $record;
					//$availabilityBy["Date"][] = $availability[$es][$id];
				}
			}
		}
	}
}//end if not filtered
ksort($availability);
foreach ($ESListAvailable as $ESAvailable=>$count){
	$lastDate = date("Y-m-d",$scheduledDates["earliest"]);
	$dateTimeIncluded[$ESAvailable] = array();
	while (strtotime($lastDate) <= $scheduledDates["latest"]){
		$thisDayOfTheWeek = date("l",strtotime($lastDate));
		//echo $lastDate." = ".$thisDayOfTheWeek."<Br>";
		//echo "check who has availability:<br>";
		//check by day of the week
		if ($availabilityBy["Day"][$thisDayOfTheWeek][$ESAvailable]){
			//echo "found availablilty on ".$thisDayOfTheWeek."<br>";
			foreach ($availabilityBy["Day"][$thisDayOfTheWeek] as $ES=>$availableThisDateInfo){
				//echo $ES.":<br>";
				foreach ($availableThisDateInfo as $id=>$availableInfo){
					$apptDate = date("Y-m-d",strtotime($lastDate));
					$apptStartTime = $availableInfo->ResidentialScheduledAvailable_StartTime;
					$apptEndTime = date("H:i:s",strtotime($apptStartTime." +1 hour"));
					//go by 1 hour increments to create available slots per each hour
					//echo "is ".$apptEndTime ." = ". $availableInfo->ResidentialScheduledAvailable_EndTime."<br>";
					$originalAvailabilityTypes = $availableInfo->ResidentialScheduledAvailable_Types;
					while (strtotime($apptEndTime) <= strtotime($availableInfo->ResidentialScheduledAvailable_EndTime)){
						$dateTimeIncluded[$ESAvailable][strtotime($apptDate." ".$apptStartTime)] = false;
						
						$additionalType = "";
						//also check by Date to see if a specific date has a specific type of availablilty
						//echo $ES." ".$apptDate."<br>";
						if (count($availabilityBy["Date"][strtotime($apptDate)][$ES])){
						
							//loop through all the times to see if availability to add this type
							foreach ($availabilityBy["Date"][strtotime($apptDate)][$ES] as $thisAvailableDate){
								$thisAvailableStart = $thisAvailableDate->ResidentialScheduledAvailable_StartTime;
								$thisAvailableEnd = $thisAvailableDate->ResidentialScheduledAvailable_EndTime;
								if (strtotime($apptStartTime) >= strtotime($thisAvailableStart) && strtotime($thisAvailableEnd) >= strtotime($apptStartTime."+ 1 hour")){
									//$availableInfo->ResidentialScheduledAvailable_Types."-".$thisAvailableDate->ResidentialScheduledAvailable_Types."
									//".strpos(" ".$availableInfo->ResidentialScheduledAvailable_Types,$thisAvailableDate->ResidentialScheduledAvailable_Types)."<br>";
									
									//Kacie D wants this to override other appointment types not add it to them so the following if statement is now being excluded
									$dateTimeIncluded[$ESAvailable][strtotime($apptDate." ".$apptStartTime)] = true;
									$originalAvailabilityTypes = $thisAvailableDate->ResidentialScheduledAvailable_Types;
									/*
									if (!strpos(" ".$availableInfo->ResidentialScheduledAvailable_Types,$thisAvailableDate->ResidentialScheduledAvailable_Types)){
										//echo $thisAvailableStart." <= ".$apptStartTime." || ".$thisAvailableEnd." >= ".$apptEndTime."".$thisAvailableDate->ResidentialScheduledAvailable_Types."<br>";
										$dateTimeIncluded[$ESAvailable][strtotime($apptDate." ".$apptStartTime)] = true;
										$additionalType = ",".$thisAvailableDate->ResidentialScheduledAvailable_Types;
									}
									*/
								}
							}
						}
						
						/* moved into hubviewbystaff calculation
						$originalAvailabilityTypesArray = explode(",",$originalAvailabilityTypes);
						foreach ($originalAvailabilityTypesArray as $thisType){
								if (!$alreadyCountedType[strtotime($apptDate." ".$apptStartTime)][$ESAvailable][$thisType]){
									$aptTypePerMonthCount[strtotime($apptDate)][$thisType]++;
									//echo $ESAvailable." ".$apptDate." ".$apptStartTime." ".$thisType."=".$aptTypePerMonthCount[strtotime($apptDate)][$ESAvailable][$thisType]."<br>";
									$alreadyCountedType[strtotime($apptDate." ".$apptStartTime)][$ESAvailable][$thisType] = 1;
								}
						}
						*/
						
						
						$thisAvailableType = $originalAvailabilityTypes.$additionalType;
						$thisRecord = new stdClass();
						$thisRecord->id = $thisAvailableType;
						$thisRecord->energySpecialist = $ES;
						$thisRecord->apptDate = $apptDate." ".$apptStartTime;
						$thisRecord->endDate = $apptDate." ".$apptEndTime;
						$thisRecord->siteId = "Open";
						$thisRecord->projectId = "Open";
						$thisRecord->electricProvider = "";
						$thisRecord->customerName = "";
						$thisRecord->email = "";
						$thisRecord->address = "";
						$thisRecord->visitType = $thisAvailableType;
						$thisRecord->ezDocUploadedDate = "";
						$thisRecord->status = "Confirmed";				
						
						$scheduledDataResults[$apptDate][$ES][strtotime($apptStartTime).$thisAvailableType] = $thisRecord;	
						$totalHoursAvailable[$ES][$apptDate] = $totalHoursAvailable[$ES][$apptDate]+$availableInfo->ResidentialScheduledAvailable_TotalHours;
						
						//move start and end time up by 1 hour
						$apptStartTime = $apptEndTime;
						$apptEndTime = date("H:i:s",strtotime($apptEndTime." +1 hour"));
						//echo "is ".$apptEndTime ." = ". $availableInfo->ResidentialScheduledAvailable_EndTime."<br>";
						
					}
				}
				//print_pre($availableThisDateInfo);
			}
		}
		//check by date if Day of the week was not set
		if ($availabilityBy["Date"][strtotime($lastDate)][$ESAvailable]){
			//echo "found availablilty on ".$lastDate."<br>";
			foreach ($availabilityBy["Date"][strtotime($lastDate)] as $ES=>$availableThisDateInfo){
				//echo $ES.":<br>";
				foreach ($availableThisDateInfo as $id=>$availableInfo){
					$apptDate = date("Y-m-d",strtotime($lastDate));
					$apptStartTime = $availableInfo->ResidentialScheduledAvailable_StartTime;
					$apptEndTime = date("H:i:s",strtotime($apptStartTime." +1 hour"));
					//go by 1 hour increments to create available slots per each hour
					while (strtotime($apptEndTime) <= strtotime($availableInfo->ResidentialScheduledAvailable_EndTime)){
						if (!$dateTimeIncluded[$ESAvailable][strtotime($apptDate." ".$apptStartTime)]){

							/*Moved into hubviewbystaff calculation
							$originalAvailabilityTypesArray = explode(",",$availableInfo->ResidentialScheduledAvailable_Types);
							foreach ($originalAvailabilityTypesArray as $thisType){
								if (!$alreadyCountedType[strtotime($apptDate." ".$apptStartTime)][$ESAvailable][$thisType]){
									$aptTypePerMonthCount[strtotime($apptDate)][$thisType]++;
									//echo $ESAvailable." ".$apptDate." ".$apptStartTime." ".$thisType."=".$aptTypePerMonthCount[strtotime($apptDate)][$ESAvailable][$thisType]."<br>";
									$alreadyCountedType[strtotime($apptDate." ".$apptStartTime)][$ESAvailable][$thisType] = 1;
								}
							}
							*/


							$thisRecord = new stdClass();
							$thisRecord->id = $availableInfo->ResidentialScheduledAvailable_Types;
							$thisRecord->energySpecialist = $ES;
							$thisRecord->apptDate = $apptDate." ".$apptStartTime;
							$thisRecord->endDate = $apptDate." ".$apptEndTime;
							$thisRecord->siteId = "Open";
							$thisRecord->projectId = "Open";
							$thisRecord->electricProvider = "";
							$thisRecord->customerName = "";
							$thisRecord->email = "";
							$thisRecord->address = "";
							$thisRecord->visitType = $availableInfo->ResidentialScheduledAvailable_Types;
							$thisRecord->ezDocUploadedDate = "";
							$thisRecord->status = "Confirmed";				
							
							$scheduledDataResults[$apptDate][$ES][strtotime($apptStartTime).$availableInfo->ResidentialScheduledAvailable_Types] = $thisRecord;	
							$totalHoursAvailable[$ES][$apptDate] = $totalHoursAvailable[$ES][$apptDate]+$availableInfo->ResidentialScheduledAvailable_TotalHours;
						}
						
						//move start and end time up by 1 hour
						$apptStartTime = $apptEndTime;
						$apptEndTime = date("H:i:s",strtotime($apptEndTime." +1 hour"));
					}
				}
				//print_pre($availableThisDateInfo);
			}
		}
		
		//check availability
		$lastDate = date("Y-m-d",strtotime($lastDate." + 1 Day"));
	}

}//end for each available ES
//print_pre($totalHoursAvailable);
//print_pre($availabilityBy);

//print_pre($scheduledDataResults["2018-07-26"]["Chad S"]);
foreach ($scheduledResultsPulldown->collection as $record){
	$scheduledIdBySiteId[$record->siteId] = $record->id;
	//print_pre($record);
	
	$dateOfAppt = date("Y-m-d",strtotime($record->apptDate));
	$startTimeStr = strtotime($record->apptDate);
	$esParts = explode(" ",$record->energySpecialist);
	$ES = $esParts[0]." ".substr($esParts[1],0,1);
	//print_pre($record);
	
	$addressParts = explode(", ", $record -> address); 
	$cityState = $addressParts[1];
	$cityPart = explode(" MA",$cityState);
	$City = $cityPart[0];
	$VisType = $record-> visitType;
	$ElecProvider = $record-> electricProvider; //added by LH
	if (strtotime($record->apptDate) >= strtotime('Today')){
		$ESPullList[$ES] = 1;
		$CityList[ucwords(strtolower($City))] = 1;
		$VisitList[$VisType]= 1;
		$ElecProviderList[$ElecProvider]= 1; //added by LH
		$scheduledDataResultsPulldown[$dateOfAppt][$ES][$startTimeStr] = $record;
	}
}

ksort($CityList);
ksort($ESPullList);
ksort($VisitList);
ksort($ElecProviderList); //added by LH
//print_pre($scheduledDataResults["2017-12-20"]);
foreach ($scheduledDataResultsPulldown as $calDate=>$esInfo){
						
			foreach ($ESPullList as $ES=>$count){
				$hoursUsed = 0;
				$apptList = "";
				$apptCount = count($esInfo[$ES]);
				$apptCounter = 0;
				$priorEndTime = strtotime("2001-01-01"); //something wayout in the past
				$priorAddress = "";
				$priorName = "";
				foreach ($esInfo[$ES] as $appt=>$apptInfo){
					if ($apptInfo->visitType == "BLOCKED"){
						$notBlocked = false;

						$thisCalDate = $calDate;
						//get the number of days blocked
						$days = datediff($apptInfo->apptDate,$apptInfo->endDate);
						$thisDay = 0;
						if ($days > 0){
							while ($thisDay < $days){
								$thisCalDate = date("Y-m-d",strtotime($calDate." + ".$thisDay." days"));
								$thisEndDate = $thisCalDate." 17:00:00";
								if ($thisDay == 0){
									$thisStartDate = $apptInfo->apptDate;
								}else{
									$thisStartDate = $thisCalDate." 09:00:00";
								}
								$hours = datediff($thisStartDate,$thisEndDate,"hours");
								if ($hours){
									//echo $ES." ".$thisCalDate." on ".$thisStartDate." to ".$thisEndDate." = ".$hours." hours<bR>";
									$energySpecialistHoursUsed[$ES][$thisCalDate]= $energySpecialistHoursUsed[$ES][$thisCalDate]+$hours;
								}
								$thisDay++;
							}
							//get the number of hours for the last day
							if ($thisDay == $days){
								$thisCalDate = date("Y-m-d",strtotime($calDate." + ".$thisDay." days"));
								$thisStartDate = $thisCalDate." 09:00:00";
								$thisEndDate = $apptInfo->endDate;
								$hours = datediff($thisStartDate,$thisEndDate,"hours");
								if ($hours){
									//echo $ES." ".$thisCalDate." on ".$thisStartDate." to ".$thisEndDate." = ".$hours." hours<bR>";
									$energySpecialistHoursUsed[$ES][$thisCalDate]= $energySpecialistHoursUsed[$ES][$thisCalDate]+$hours;
								}
							}
						}else{
							//calculate just the hours
							$thisCalDate = date("Y-m-d",strtotime($apptInfo->endDate));
							$hours = datediff($apptInfo->apptDate,$apptInfo->endDate,"hours");
							if ($hours){
								//echo $ES." ".$thisCalDate." on ".$apptInfo->apptDate." to ".$apptInfo->endDate." = ".$hours." hours<bR>";
								$energySpecialistHoursUsed[$ES][$thisCalDate]= $energySpecialistHoursUsed[$ES][$thisCalDate]+$hours;
							}
						}
						//echo $ES." Blocked initially ".$calDate." then on ".$apptInfo->apptDate." to ".$apptInfo->endDate." which is ".($days > 0 ? $days." days" : $hours." hours")."<br>";
						//echo $ES."<Br>";
						//print_pre($energySpecialistHoursUsed[$ES]);
					}else{
						$notBlocked = true;
						$addressParts = explode(", ", $apptInfo->address); 
						$cityState = $addressParts[1];
						$cityPart = explode(" MA",$cityState);
						$City = $cityPart[0];
						
						$startTime= date("h:i a",strtotime($apptInfo->apptDate));
						$endTime = date("h:i a",strtotime($apptInfo->endDate)) ;
                        try {
                            $date1 = new DateTime($startTime);
                        } catch (Exception $e) {
                        }
                        try {
                            $date2 = new DateTime($endTime);
                        } catch (Exception $e) {
                        }
                        $diff = $date2->diff($date1);
						$hours = $diff->h;
						
						$Address = $apptInfo->address;
						$customerName = $apptInfo->customerName;
						
						//Kacie wants Travel slots to reset prior address checker
						if ($apptInfo->visitType == "Travel"){
							$priorAddress = "";
						}
						
						if ($priorAddress){
							$formattedAddrFrom = str_replace(' ','+',$priorAddress);
							$formattedAddrTo = str_replace(' ','+',$Address);
							
							$priorAddressChecker[] = array($formattedAddrFrom,$formattedAddrTo,$apptInfo->siteId.$apptInfo->projectId,$ES,date("m/d/Y",strtotime($calDate)),$priorName,$customerName);
						}
						$priorAddress = $Address;
						$priorName = $customerName;
						
						$apptCounter++;
						$priorEndTime = strtotime($calDate." ".$endTime);
						$hoursUsed+=$hours;
					}//end if not BLOCKED
					
				}
//				if ($notBlocked){
					$hoursAval = 6 - $hoursUsed;
					$energySpecialistHoursUsed[$ES][$calDate]= $energySpecialistHoursUsed[$ES][$calDate]+$hoursUsed;
					//print_pre($energySpecialistHoursUsed);
//				}
			}
			
		
	}
//print_pre($energySpecialistHoursUsed["Chad S"]);
include_once($dbProviderFolder."MuniProvider.php");
$muniProvider = new MuniProvider($dataConn);
$criteria = new stdClass();
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
foreach ($customerCollection as $record){
	$muniCustomers[strtoupper($record->accountHolderFullName)] = $record->id;
}
?>
<?php if (!$isStaff){?>
	<fieldset style='width:90%;'>
		<legend>Filters</legend>
		<?php if ($hideDateRange){ echo $hideDateRange;?><div style='display:none;'><?php }?>
		<div class="two columns">
			Area<br>
			<select class="parameters" id="Area" style="width:100px;">
				<option></option>
				<?php foreach ($areaFilter as $area => $cities){
					echo("<option value='$area'".($_GET["Area"]==$area ? " selected" : "").">$area</option>");
				}?>	
			</select>
					
		</div>

		<div class="two columns">
			City<br>
			<select class="parameters" id="CityName" style="width:100px;">
				<option></option>
				<?php foreach ($CityList as $city => $key){
					echo("<option value='$city'".($_GET["CityName"]==$city ? " selected" : "").">$city</option>");
				}?>	
			</select>
					
		</div>
		<div class="two columns">
		Specialist<br>
			<select class="parameters" id="SpecialistName" style="width:100px;height:50px;" multiple>
				<option></option>
				<?php foreach ($ESPullList as $ES => $key){
					echo("<option value='$ES'".(strpos(" ".$_GET["SpecialistName"],$ES) ? " selected" : "").">$ES</option>");
				}?>	
			</select>
		</div><div class="two columns">
			Visit Type<br>
			<select class="parameters" id="VisitType" style="width:100px;">
				<option></option>
				<?php foreach ($VisitList as $Visit => $key){
					echo("<option value='$Visit'".($_GET["VisitType"]==$Visit ? " selected" : "").">$Visit</option>");
				}?>	
			</select>
		</div>

		<div class="two columns">
			<br>
			<button id="resetbutton">Reset Filters</button>
		</div>
	</fieldset>
<?php
}//end if not isStaff

/*  		<div class="two columns">
			Test Filter Type<br>
			<select class="parameters" id="electricProvider" style="width:100px;">
				<option></option>
				<?php foreach ($ElecProviderList as $ElecProvider => $key){
					echo("<option value='$ElecProvider'".($_GET["electricProvider"]==$ElecProvider ? " selected" : "").">$ElecProvider</option>");
				}?>	
			</select>
		</div> 
add and edit this section above to create another filter*/		
$setTab = "ScheduledData";
	if ($isStaff){
		$TabCodes = array();
	}else{
		$TabCodes = array("Currently Scheduled Data"=>"ScheduledData","Previously Scheduled Data"=>"PriorScheduledData","Current Mailer"=>"Mailer","Availability"=>"Availability","Assigned Analysis"=>"AssignedAnalysis");
	}
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo "</div><!--container-->";
	echo '<div class="tabs-content">';
?>
		<div id="Availability" class='container tab-content'>
			<?php include_once('views/_ESSchedulerLayout_availability.php');?>
		</div>
		<div id="Mailer" class='container tab-content'>
			
			<?php 
				if (isset($_GET['runMailer'])){
					include_once('views/_ESScheduler.php');
				}else{
					if (!$isStaff){
						echo "<div id='mailerData'><button id='runMailer'><h3>Show Current Mailer Data</h3></button></div>";
					}
				}
			?>
		</div>
		<div id='ScheduledData' class='tab-content'>
			<style>
				.blockedSchedule {border: 1pt solid blue; background-color:lightblue;padding:5pt;}
			</style>
			<?php if (!$isStaff){?><h1>Currently Scheduled Data</h1><?php }?>
			<style>
			/* Style the tab */
			div.tab2 {
				overflow: hidden;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
				width:auto;
			}

			/* Style the buttons inside the tab */
			div.tab2 button {
				background-color: inherit;
				float: left;
				border: none;
				outline: none;
				cursor: pointer;
				padding: 14px 16px;
				transition: 0.3s;
				font-size: 17px;
			}

			/* Change background color of buttons on hover */
			div.tab2 button:hover {
				background-color: #ddd;
			}

			/* Create an active/current tablink class */
			div.tab2 button.active {
				background-color: #ccc;
			}

			/* Style the tab content */
			.tab2content {
				display: block;
				padding: 6px 12px;
				-webkit-animation: fadeEffect 1s;
				animation: fadeEffect 1s;
			}

			/* Fade in tabs */
			@-webkit-keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}

			@keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}
			table.dataTable tbody td {
				white-space: nowrap;
			}			
			</style>
			
			<div id="overlapErrors" class="alert" style="color:red;"></div>
			<div class="tab2">
			  <button id="hubViewButton" class="tab2links active" data-viewname="HubView"><img src="<?php echo $CurrentServer;?>images/hubView.png" style='height:50px;'><Br>Hub View</button>
			<?php if (!$isStaff){?>
			  <button class="tab2links" data-viewname="CalendarView"><img src="<?php echo $CurrentServer;?>images/calendarView.png" style='height:50px;'><Br>Calendar View</button>
			  <!--<button id="listViewButton" class="tab2links" data-viewname="ListView"><img src="<?php echo $CurrentServer;?>images/listView.png" style='height:50px;'><br>List View</button>-->
			<?php }//end if staff?>
			  <button class="tab2links" data-viewname="Refreshing" id='refreshData'><span id="refreshDataImage"><img src="<?php echo $CurrentServer;?>images/refreshView.png" style='height:50px;'><br>Refresh Data</span></button>
			<?php if (!$isStaff){?>
				<?php if (!count($filteredByList)){?>
					<button class="tab2links confirmAll" data-viewname="HubView"><img src="<?php echo $CurrentServer;?>images/sendConfirmation.png" style='height:50px;'><br><span id="confirmAllButton">Send Confirmations</span><span class="info alert" id="confirmAllResultsNumber" style="display:none;">0</span> <span class="info alert" id="confirmAllResults"></span></button>
				<?php }?>
			<?php }//end if staff?>
			  <button class="tab2links" data-viewname="HubView" id='showDeleteButton'><span id="deleteScheduleButtonImage"><img src="<?php echo $CurrentServer;?>images/trash.png"><br>Show Delete Button</span></button>
			</div>

			<div id="Refreshing" class="tab2content" style="display:none;">
				<?php 
					$i = 1;
					while ($i < 31){
						$refreshingArray[] = "Refreshing_".$i.".jpg";
						$i++;
					}
					shuffle($refreshingArray);
				?>
				Something Refreshing:<br><img class='ten columns' src="<?php echo $CurrentServer."images/".$refreshingArray[0];?>">
			</div>
			
			
			
			
			<div id="ListView" class="tab2content" style="display:none;">
				<table class="simpleTable" id='currentScheduledData'>
					<thead>
						<tr>
							<th nowrap=nowrap>Date</th>
							<?php 
								foreach ($ESList as $ES=>$count){
									echo "<th nowrap=nowrap>".$ES."</th>";
								}
							
							?>
						</tr>
					</thead>
					<tbody>
						<?php 
							$ESIDCount = 0;
							$ESIDEventColors[0] = ", eventColor: '#3a87ad',eventTextColor: 'white',eventBorder:'#2E6C8A' ";
							$ESIDEventColors[1] = ", eventColor: 'green',eventTextColor: 'white',eventBorder:'#006600'  ";
							$ESIDEventColors[2] = ", eventColor: 'orange',eventTextColor: 'black',eventBorder:'#D69D33' ";
							$ESIDEventColors[3] = ", eventColor: '#D2691E',eventTextColor: 'white',eventBorder:'#A85418' ";
							$ESIDEventColors[4] = ", eventColor: '#9C9A40',eventTextColor: 'white',eventBorder:'#7D7B33' ";
							$ESIDEventColors[5] = ", eventColor: '#005960',eventTextColor: 'white',eventBorder:'#00474D' ";
							$ESIDEventColors[6] = ", eventColor: '#C48F65',eventTextColor: 'black',eventBorder:'#D0A584' ";
							$ESIDEventColors[7] = ", eventColor: '#F3D6E4',eventTextColor: 'black',eventBorder:'#C2ABB6' ";
							$ESIDEventColors[8] = ", eventColor: '#672E3B',eventTextColor: 'white',eventBorder:'#52252F' ";
							$ESIDEventColors[9] = ", eventColor: '#DC4C46',eventTextColor: 'white',eventBorder:'#B03D38' ";
							
							foreach ($ESList as $ES=>$count){
								$ESIDCounter[$ES] = $ESIDCount;
								$ESColorPart = explode("'",$ESIDEventColors[$ESIDCount]);
								$ESColor[$ES] = array("background"=>$ESColorPart[1],"color"=>$ESColorPart[3],"border"=>$ESColorPart[5]);
								$calendarResourceList .= "{ id: '".$ESIDCount."', title: '".$ES."'".$ESIDEventColors[$ESIDCount].",businessHours: {start: '09:00',end: '18:00'}}";
								$ESIDCount++;
								if ($ESIDCount < count($ESList)){$calendarResourceList .= ",";}
							}
							foreach ($scheduledDataResults as $calDate=>$esInfo){
								$thisDisplayTitle = date("m/d/y",strtotime($calDate))."<br>".date("l",strtotime($calDate));
								echo "<tr>
									<td nowrap=nowrap title='".str_replace("<br>"," - ",$thisDisplayTitle)."'><span style='display:none;font-size:10pt;'>".strtotime($calDate)."</span>".$thisDisplayTitle."</td>";
									foreach ($ESList as $ES=>$count){
										
										$hoursAllowedPerDay = (date("D",strtotime($calDate)) == "Fri" ? 4 :6);
										$hoursUsed = 0;
										$apptList = "";
										$apptCount = count($esInfo[$ES]);
										$apptCounter = 0;
										$priorEndTime = strtotime("2001-01-01"); //something way in the past
										$priorAddress = "";
										$priorName = "";
										//do this to get the next index item for comparing availability times if next appointment starts in the middle of prior availability
										foreach ($esInfo[$ES] as $appt=>$apptInfo){
											$startTime= date("h:i a",strtotime($apptInfo->apptDate));
											$endTime = date("h:i a",strtotime($apptInfo->endDate)) ;
											if ($apptInfo->siteId != "Open"){
												$alreadyUsedTimeSlot[$ES][$calDate][]=array("startTime"=>strtotime($startTime),"endTime"=>strtotime($endTime),"address"=>$apptInfo->address,"customerName"=>$apptInfo->customerName);
											}
										}
										$nextIndex = 0;
										foreach ($esInfo[$ES] as $appt=>$apptInfo){
											$thisAddressParts = explode(",",$apptInfo->address);
											$thisCityParts = explode(" MA ",trim($thisAddressParts[1]));
											$thisCity = $thisCityParts[0];
											$nextIndex++;
											$nextApptInfo = $nextApptInfoObj[$nextIndex];
											
											$thisEventDetails = "";
											$startTime= date("h:i a",strtotime($apptInfo->apptDate));
											$STARTTime = date("g:i A",strtotime($apptInfo->apptDate));
											if ($priorEndTime > strtotime($calDate." ".$startTime) && $apptInfo->siteId != "Open"){
												$overlapErrors[] = date("m/d/Y",strtotime($calDate))." ".$ES.": ".$apptInfo->customerName." in ".ucwords(strtolower($thisCity))." with ".$priorName;
												$overlapErrorsBySiteProject[$apptInfo->siteId.$apptInfo->projectId] = date("m/d/Y",strtotime($calDate))." ".$ES.": ".$apptInfo->customerName." in ".ucwords(strtolower($thisCity))." with ".$priorName;
												$apptList .= "<span style='color:red;'>OVERLAP ERROR</span><br>";
												$thisEventDetails .= "<span style='color:red;background-color:pink;'>OVERLAP ERROR</span><br>";
											}
											$priorName = $apptInfo->customerName;
											$endTime = date("h:i a",strtotime($apptInfo->endDate)) ;
											$ENDTime = date("g:i A",strtotime($apptInfo->endDate)) ;
											//check for same start end time
											if (strtotime($apptInfo->apptDate) == strtotime($apptInfo->endDate)){
												$overlapErrors[] = date("m/d/Y",strtotime($calDate))." ".$ES." has same end time: ".$apptInfo->customerName;
												$overlapErrorsBySiteProject[$apptInfo->siteId.$apptInfo->projectId] = date("m/d/Y",strtotime($calDate))." ".$ES." has same end time: ".$apptInfo->customerName;
												$apptList .= "<span style='color:red;'>END TIME ERROR</span><br>";
												$thisEventDetails .= "<span style='color:red;background-color:pink;'>END TIME ERROR</span><br>";
											}
											
											
											if ($apptInfo->visitType == "BLOCKED"){
												$id = str_replace("BlockedID","",$apptInfo->siteId);
												$apptList .= "<div class='blockedSchedule' id='blockedSchedule".$apptInfo->id."'>BLOCKED: ".$startTime." - ".$endTime." ".$apptInfo->customerName. "[".$ES."]";
												$apptList .= "<br>".($apptInfo->email ? $apptInfo->email."<br>" : "")."<button class='removeBlock' data-scheduledid='".$apptInfo->id."' data-blockedid='".$apptInfo->siteId."'>Remove Block</button></div>";
												$thisEventDetails .= "<div class='blockedSchedule'>BLOCKED: ".$startTime." - ".$endTime." ".$apptInfo->customerName." [".$ES."]<br>".$apptInfo->email."</div>";
												$apptInfo->eventDetails = $thisEventDetails;
												//print_pre($apptInfo);
												$alreadyUsedTimeSlot[$ES][$calDate][]=array("startTime"=>strtotime($startTime),"endTime"=>strtotime($endTime),"address"=>$apptInfo->address,"customerName"=>$apptInfo->customerName);
												$hubViewData[$calDate][strtotime($startTime)][$ES][strtotime($endTime)][] = $apptInfo;
												$hubViewDataByStaff[strtotime($calDate)][$ES][strtotime($startTime)][strtotime($endTime)][] = $apptInfo;
												$resourceCalendarEvent[]=array("resourceId"=>$ESIDCounter[$ES],"start"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->apptDate)),"end"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->endDate)),"title"=>$apptInfo->visitType."-".$apptInfo->customerName,"description"=>$thisEventDetails,"fulldetails"=>$apptInfo->customerName." [".$ES."]<br>".$blockedID[$id]["note"],"siteId"=>$apptInfo->siteId);
												
											}else{
												if ($apptInfo->siteId != "Open"){

													$apptList .= $startTime." - ".$endTime." ".$apptInfo->visitType."<br>";
													$thisEventDetails .= $startTime." - ".$endTime." ".$apptInfo->visitType."<br>";
													if ($apptInfo->visitType == "WGE"){
														$link = "<a target='Muni' href='".$CurrentServer.$adminFolder."muni/?nav=customer-data&CustomerID=".$muniCustomers[strtoupper($apptInfo->customerName)]."'>".$apptInfo->customerName."</a>";
													}else{
														$link = $apptInfo->customerName;
													}
													$Address = $apptInfo->address;
													if ($priorAddress){
														$distanceAlert = "<br><span id='distanceAlert".$apptInfo->siteId.$apptInfo->projectId."'></span>";
													}
													$priorAddress = $Address;
													
													
													$apptList .= "<span class='customerName'>".$link."<br>SiteId: ".$apptInfo->siteId." ProjectId: ".$apptInfo->projectId."<br>".$Address."<br>".$apptInfo->electricProvider.$distanceAlert."</span>";
													$thisFullEventDetails =str_replace("<br>","",$thisEventDetails) ." [".$ES."]<br><span class='customerName'>".$link."<br>SiteId: ".$apptInfo->siteId." ProjectId: ".$apptInfo->projectId."<br>".$Address."<br>".$apptInfo->electricProvider.$distanceAlert."</span>";
													$thisEventDetails .="<span class='customerName'>".$link."<br>".$Address."<br>".$apptInfo->electricProvider.$distanceAlert."</span>";
													$apptInfo->eventDetails = $thisFullEventDetails;
													/*
													if ($apptInfo->status == "Confirmed"){
														$apptList .= "<br><span class='info'>Confirmed</span>";
													}else{
														$thisButton = "<br><span id='confirm".$apptInfo->id."'>
															<button class='confirmApt' title='".(!$apptInfo->email ? " will only mark confirmed" : " will mark confirmed and send email")."'
																data-apptid='".$apptInfo->id."' 
																data-siteid='".$apptInfo->siteId."' 
																data-email='".$apptInfo->email."'
																data-customername='".$apptInfo->customerName."'
																data-appttype='".$apptInfo->visitType."'
																data-apptdate='".date("l, F jS, Y",strtotime($calDate." ".$startTime))."'
																data-apptstarttime='".$STARTTime."'
																data-apptendtime='".$ENDTime."'
																data-address='".$Address."'
																data-es='".$ES."'
															>Send Confirmation".(!$apptInfo->email ? " via Phone" : "")."</button></span>";
														$apptList .= $thisButton;	
													}
													*/
													$apptList .= ($apptCounter < $apptCount ? "<hr>" : "");
													$alreadyUsedTimeSlot[$ES][$calDate][]=array("startTime"=>strtotime($startTime),"endTime"=>strtotime($endTime),"address"=>$apptInfo->address,"customerName"=>$apptInfo->customerName);
													$hubViewData[$calDate][strtotime($startTime)][$ES][strtotime($endTime)][] = $apptInfo;
													$hubViewDataByStaff[strtotime($calDate)][$ES][strtotime($startTime)][strtotime($endTime)][] = $apptInfo;
													$resourceCalendarEvent[]=array("resourceId"=>$ESIDCounter[$ES],"start"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->apptDate)),"end"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->endDate)),"title"=>$apptInfo->visitType."-".$apptInfo->customerName." SiteId: ".$apptInfo->siteId,"description"=>$thisEventDetails,"fulldetails"=>"<div style='color:".$ESColor[$ES]["color"].";padding:2px;background:".$ESColor[$ES]["background"].";'>".$thisFullEventDetails."</div>");
												}else{
													if ($calDate == "2018-08-21"){
														echo "HERE YOSH\r\n";
														foreach ($alreadyUsedTimeSlot["Chad S"]["2018-08-21"] as $info){
															$thisApptStartHour = date("h",strtotime($startTime));
															$usedStartHour = date("h",$info["startTime"]);
															echo $thisApptStartHour ."==". $usedStartHour."\r\n";
															if ($thisApptStartHour == $usedStartHour){
																echo "is already used\r\n";
																print_pre($apptInfo);
																$includeThisAvailability = false;
															}
														}
													}
													$apptCounter = $apptCounter-1;
													//only include not filled slots
													$includeThisAvailability = true;
													$priorInfo = array();
													$nextInfo = array();
													foreach ($alreadyUsedTimeSlot[$ES][$calDate] as $times){
														//find prior Appt Info
														if (strtotime($startTime) >= $times["endTime"]){
															$priorInfo = $times;
														}
														if (strtotime($endTime) <= $times["startTime"] && !count($nextInfo)){
															$nextInfo = $times;
														}
														
														if (strtotime($startTime) >= $times["startTime"] && strtotime($endTime) <=$times["endTime"]){
															$includeThisAvailability = false;
														}
														//$apptList .= $ES." ".$calDate." startTime=".$times["startTime"]." endTime=".$times["endTime"]."<br>";
														//also look ahead to next time slot
														if (strtotime($startTime) >= $times["startTime"] && strtotime($startTime) <= $times["endTime"]) {
															$includeThisAvailability = false;
														}
														
														
														//check if next time slot is taken up by a start time between the start and end of this open slot
														$thisApptStartHour = date("h",strtotime($startTime));
														$usedStartHour = date("h",$times["startTime"]);
														if ($thisApptStartHour == $usedStartHour){
															$includeThisAvailability = false;
														}

														
													}
													//also eliminate availability if allready booked
													if ($apptCounter >= 5){
														$includeThisAvailability = false;
													}
													
													//also if totally booked remove other availability
													$hoursUsed =$energySpecialistHoursUsed[$ES][$calDate];
													$hoursAval = $hoursAllowedPerDay - $hoursUsed;
													if ($hoursAval < 1){
														$includeThisAvailability = false;
													}

													
													if ($energySpecialistHoursUsed[$ES][$calDate] >= $hoursAllowedPerDay){ 
														$includeThisAvailability = false; //don't include avaiability if already used up 6 hours
													}
													
													
													
													if ($includeThisAvailability){
														$priorInfoDisplay = "<span style='border:1pt solid silver;background-color:#E2E4FF;font-size:10pt;font-weight:bold;'>".(date("g:ia",$priorInfo["endTime"]) == "7:00pm" ? "No Prior Apt" : "Prior Apt: ".$priorInfo["customerName"]." in ".$priorInfo["address"]." ending ".date("g:ia",$priorInfo["endTime"]))."</span><br>";
														$nextInfoDisplay = "<br><span style='border:1pt solid silver;background-color:#E2E4FF;font-size:10pt;font-weight:bold;'>".(date("g:ia",$nextInfo["startTime"]) == "7:00pm" ? "No Next Apt" : "Next Apt: ".$nextInfo["customerName"]." in ".$nextInfo["address"]." starting ".date("g:ia",$nextInfo["startTime"]))."</span>";
														$apptList .= $startTime." - ".$endTime." Available For: ".$apptInfo->visitType."<br>";
														$thisEventDetails .= $startTime." - ".$endTime." Available For: ".$apptInfo->visitType."<br>";
														$thisFullEventDetails =str_replace("<br>","",$thisEventDetails) ." [".$ES."]";
														$apptInfo->eventDetails = "<div class='openSlotDetails'>".$priorInfoDisplay."<span style='font-size:10pt;'>".$thisFullEventDetails."</span>".$nextInfoDisplay."</span></div>";
														$apptList .= ($apptCounter < $apptCount ? "<hr>" : "");
														$hubViewData[$calDate][strtotime($startTime)][$ES][strtotime($endTime)][] = $apptInfo;
														$hubViewDataByStaff[strtotime($calDate)][$ES][strtotime($startTime)][strtotime($endTime)][] = $apptInfo;
														$resourceCalendarEvent[]=array("resourceId"=>$ESIDCounter[$ES],"start"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->apptDate)),"end"=>date("Y-m-d\TH:i:s",strtotime($apptInfo->endDate)),"title"=>"Available for ".$apptInfo->visitType,"description"=>$thisEventDetails,"fulldetails"=>"<div style='color:".$ESColor[$ES]["color"].";padding:2px;background:".$ESColor[$ES]["background"].";'>".$thisFullEventDetails."</div>");
													}
												}//end if Available
											}
											$apptCounter++;
											$priorEndTime = strtotime($calDate." ".$endTime);
										}
										if ((int)$totalHoursAvailable[$ES][$calDate] < 0){
												echo "<td nowrap=nowrap><button style=background-color:orange;><h5>Not Available ".$ES." ".$calDate."</h5></button><br>$apptList";
										}else{
										
											$hoursUsed =$energySpecialistHoursUsed[$ES][$calDate];
											$hoursAval = $hoursAllowedPerDay - $hoursUsed;
											if ($apptCounter < 4 && $hoursAval >0){
												echo "<td nowrap=nowrap><button style=background-color:#e6f9ff; class = hoursAval><h4> $hoursAval Hour".($hoursAval > 1 ? "s" : "")." Available </h4></button><br>$apptList";
											}elseif ($apptCounter < 4 && $hoursUsed == 0){
												echo "<td nowrap=nowrap><button style=background-color:#A3E4D7; class = noAppointments><h4>All Hours Available</h4></button>";
											}else{
												echo "<td nowrap=nowrap bgcolor=#EAECEE>$apptList";
											}
											if ($apptCounter < 4 && $hoursAval >0){
												$hoursAvailableText = $hoursAval." Hour".($hoursAval > 1 ? "s" : "")." Available";
												$resourceCalendarEvent[]=array("resourceId"=>$ESIDCounter[$ES],"start"=>date("Y-m-d",strtotime($apptInfo->apptDate)),"end"=>date("Y-m-d",strtotime($apptInfo->apptDate)),"title"=>$hoursAvailableText,"description"=>$hoursAvailableText,"fulldetails"=>$hoursAvailableText." [".$ES."]");
											}elseif ($apptCounter < 4 && $hoursUsed == 0){
												$resourceCalendarEvent[]=array("resourceId"=>$ESIDCounter[$ES],"start"=>date("Y-m-d",strtotime($apptInfo->apptDate)),"end"=>date("Y-m-d",strtotime($apptInfo->apptDate)),"title"=>"All Hours Available","description"=>"All Hours Available","fulldetails"=>"All Hours Available"." [".$ES."]");
											}
										}
									}
								echo "</td></tr>";
							}
						?>
					</tbody>
				</table>
			</div>
			<div id="CalendarView" class="tab2content" style="display:none;">
				<link rel='stylesheet' href='<?php echo $CurrentServer.$adminFolder;?>css/fullcalendar.css' />
				<style>
				.fc-slats .fc-widget-contentv {
					 height: 140px !important;
				}		
				.fc-resized-row { height: 140px !important; }			
				div.ui-tooltip {
					max-width: 600px;
					padding:5px;
				}
				.openSlotDetails {background-color:yellow;border:1pt solid red;}
				#DataTables_Table_0_wrapper {width:1500px;}
				.dataTables_filter {float:left;}
				</style>
				<div id='calendar'></div>
			</div>
			<div id="HubView" class="tab2content" style='margin: auto; width: 90%;padding: 10px;'>
				<style>
				  #availableClients-label {
					display: block;
					font-weight: bold;
					margin-bottom: 1em;
				  }
				  #availableClients-icon {
					float: left;
					height: 32px;
					width: 32px;
				  }
				  #availableClients-description {
					margin: 0;
					padding: 0;
				  }
				  </style>				
				<script>
				$( function() {
					$(".dateTimePickerClass").timepicker({
						timeFormat: 'h:mm TT',
						hourMin: 8,
						hourMax: 16,
						stepMinute: 30,
						controlType: 'select',
						oneLine: true,
						altFieldTimeOnly: false
					});
					$('#reserve_startTimePicker').on('change', function() {
						dStartTime = new Date("<?php echo date("Y-m-d");?> "+$('#reserve_startTimePicker').val());
						dEndTime = new Date("<?php echo date("Y-m-d");?> "+$('#reserve_startTimePicker').val());
						//HEA appt are 2 hours so when the start time changes also bump up the end time
						if ($("#MMWECAuditType").val() == "HEA" || $("#MMWECAuditType").val() == "HEAAU"){
							dEndTime.setHours( dStartTime.getHours() + 2 );
						}else{
							dEndTime.setHours( dStartTime.getHours() + 1 );
						}
						endTimeDisplay = dEndTime.toLocaleTimeString();
						$("#reserve_endTimePicker").val(endTimeDisplay.replace(":00 "," "));
					});

					var availableClients_Blank = [{}];
					var availableClients_MMWEC = <?php echo json_encode($ToBeScheduled["MMWEC"]);?>;
					var availableClients_WGE = <?php echo json_encode($ToBeScheduled["WGE"]);?>;
					var availableClients_HES = <?php echo json_encode($ToBeScheduled["HES"]);?>;
					$( "#availableClients" ).autocomplete({
					  minLength: 0,
					  source: availableClients,
					  focus: function( event, ui ) {
						$( "#availableClients" ).val( ui.item.label );
						return false;
					  },
					  select: function( event, ui ) {
						$( "#MMWECAuditTypeDisplay" ).html('');
						$( "#availableClients" ).val('');
						$( "#availableClients-id" ).val( ui.item.value );
						$( "#availableClients-siteId" ).val( ui.item.siteId );
						$( "#availableClients-projectId" ).val( ui.item.projectId );
						$( "#availableClients-description" ).html( ui.item.label );
						$( "#availableClients-electricProvider" ).val( ui.item.electricProvider );
						$( "#availableClients-email" ).val( ui.item.email );
						$( "#MMWECType" ).val( ui.item.mmwectype );
						//console.log(ui.item.mmwecAuditType+" "+$("#MMWECAuditType").val());
						if (ui.item.mmwecAuditType != ""){
							if (ui.item.mmwecAuditType !=  $("#MMWECAuditType").val()){
								$( "#MMWECAuditTypeDisplay" ).html(ui.item.source+" Record has Audit Type as: "+ ui.item.mmwecAuditType );
							}
						}
						$(".ui-dialog-buttonset").show()
				 
						return false;
					  }
					});	
					
					var dialog, form,
				 
					  // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
					  emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
					  name = $( "#name" ),
					  email = $( "#email" ),
					  password = $( "#password" ),
					  allFields = $( [] ).add( name ).add( email ).add( password ),
					  tips = $( ".validateTips" );
				 
					function updateTips( t ) {
					  tips
						.text( t )
						.addClass( "ui-state-highlight" );
					  setTimeout(function() {
						tips.removeClass( "ui-state-highlight", 1500 );
					  }, 500 );
					}
				 
					function checkLength( o, n, min, max ) {
					  if ( o.val().length > max || o.val().length < min ) {
						o.addClass( "ui-state-error" );
						updateTips( "Length of " + n + " must be between " +
						  min + " and " + max + "." );
						return false;
					  } else {
						return true;
					  }
					}
				 
					function checkRegexp( o, regexp, n ) {
					  if ( !( regexp.test( o.val() ) ) ) {
						o.addClass( "ui-state-error" );
						updateTips( n );
						return false;
					  } else {
						return true;
					  }
					}
				 
					function scheduleAppt() {
						var reserveId = $("#reserve_id").val(),
							reserveButton = "#"+reserveId,
							startTime = "#startTime"+reserveId,
							endTime = "#endTime"+reserveId,
							visitType = "#visitType"+reserveId,
							customerName = "#customerName"+reserveId;
							customerLocation = "#customerLocation"+reserveId;
							customerNameParts = $("#availableClients-description").html().split(" - ");
							confirmSpot = "#confirmSpot"+reserveId;
							
						var data = {};
						data["action"] = "scheduleAppt";
						data["es"] = $("#reserve_ES").val();
						data["esMMWECID"] = $("#reserve_ESmmwecId").val();
						data["startTime"] = $("#reserve_startTime").val();
						data["endTime"] = $("#reserve_endTime").val();
						data["startTimePicker"] = $("#reserve_startTimePicker").val();
						data["endTimePicker"] = $("#reserve_endTimePicker").val();
						data["apptType"] = $("#reserve_apptType").val();
						data["id"] = $("#availableClients-id").val();
						data["siteId"] = $("#availableClients-siteId").val();
						data["projectId"] = $("#availableClients-projectId").val();
						data["client"] = $("#availableClients-description").html();
						data["electricProvider"] = $("#availableClients-electricProvider").val();
						data["email"] = $("#availableClients-email").val();
						data["MMWECType"] = $("#MMWECType").val();
						data["MMWECAuditType"] = $("#MMWECAuditType").val();
						//console.log(data);
						$(reserveButton).html("<img src='<?php echo $CurrentServer."images/ajaxLoaderSmall.gif";?>'>");
						$.ajax({
							url: "ApiResidentialManagement.php",
							type: "POST",
							data: JSON.stringify(data),
							success: function(resultdata){
									//console.log(resultdata);
									var clientNameParts = 
									$(reserveButton).html("");
									$(startTime).attr('style','');
									$(endTime).attr('style','');
									$(visitType).attr('style','');
									$(customerName).attr('style','').html(customerNameParts[0]);
									$(customerLocation).attr('style','').html(customerNameParts[1]);
									$(confirmSpot).html('Reload Page to Access Confirmation Button');
									if (resultdata.visitType == "Travel"){
										$(customerLocation).attr('style','').html(resultdata.address);
										$(visitType).attr('style','').html("Travel");
										$(confirmSpot).html('');
									}
									//location.reload();
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$(thisAlert).show().html(message).addClass("alert");
							}
						});
						dialog.dialog( "close" );
						
						
					  return true;
					}	
					
					var dialogDefaultHeight = 525;
					var dialogDefaultWidth = 600;
					dialog = $( "#dialog-form" ).dialog({
					  autoOpen: false,
					  height: dialogDefaultHeight,
					  width: dialogDefaultWidth,
					  modal: true,
					  buttons: {
						"Schedule This Appt": scheduleAppt,
						Cancel: function() {
						  dialog.dialog( "close" );
						}
					  },
					  close: function() {
						form[ 0 ].reset();
						allFields.removeClass( "ui-state-error" );
					  }
					});
				 
					form = dialog.find( "form" ).on( "submit", function( event ) {
					  event.preventDefault();
					  scheduleAppt();
					});
				 
					$( ".reserveApt" ).button().on( "click", function() {
						//first clear radio button selected list
						$(".dynamicApptType").removeAttr('checked');
						$("#MMWECAuditTypes").hide();
						$("#HESMMWECAuditTypes").hide();
						$( "#MMWECAuditTypeDisplay" ).html('');
						$(".aptTypeSource").hide();
						$("#dialog-clientSelect").hide();
						$(".ui-dialog-buttonset").hide();
						
						var $this = $(this),
							thisES = $this.attr('data-es'),
							thisESmmwecId = $this.attr('data-mmwecauditorid'),
							thisStartTime = $this.attr('data-starttime'),
							thisEndTime = $this.attr('data-endtime'),
							thisApptType = $this.attr('data-appttype'),
							thisReserveId = $this.attr('data-reserveid');
							
						$("#reserve_id").val(thisReserveId);
						$("#reserve_startTime").val(thisStartTime);
						$("#reserve_endTime").val(thisEndTime);
						$("#reserve_apptType").val(thisApptType);
						$("#reserve_ES").val(thisES);
						$("#reserve_ESmmwecId").val(thisESmmwecId);
						$("#availableClients-description").html('');
						var reserveTypes = thisApptType.split(",");
						$("#reserveTypesDisplay").html('');
						
						var aptTypeArray = new Array();
						<?php foreach ($appointmentTypes as $aptType=>$source){
							echo "aptTypeArray['".$aptType."']='".$source."';\r\n";
						}?>
						$.each(reserveTypes,function(key,val){
							var sourceName = val.split(' ').join('_');
							var sourceType = aptTypeArray[sourceName];
							var thisSource = ".source_"+sourceType;
							var thisRadio = "#radio_"+sourceType;
							$(thisSource).show();
							if (reserveTypes.length < 2){
								$(thisRadio).click();
								var thisAuditType = "#MMWECAuditType"+sourceName.replace(" ","");
								$(thisAuditType).click();
								//$("#dialog-clientSelect").show();
							}
						});
					  dialog.dialog( "open" );
						var d = new Date(thisStartTime*1000);
						var dateDisplay = d.toLocaleString();
						dialog.dialog('option', 'title', 'Reserve '+thisES+' Appt for '+dateDisplay);
						
					});	
					
					$(".dynamicApptType").on('click',function(){
						$("#dialog-clientSelect").show();
						$("#HESMMWECAuditTypes").hide();
						$("#MMWECAuditTypes").hide();
						$("#TravelReasonDiv").hide();
						dialog.dialog('option', 'height', dialogDefaultHeight);
						var newDialogHeight = dialogDefaultHeight;
						
						var $this = $(this),
							thisSource = $this.attr('data-source');
							//console.log(thisSource);
						$("#reserve_apptType").val(thisSource);
						//set default to blank
						$( "#availableClients" ).autocomplete({
						  source: availableClients_Blank
						});
						var dStartTime = new Date($("#reserve_startTime").val()*1000);
						var dEndTime = new Date($("#reserve_endTime").val()*1000);
						var startTimeDisplay = dStartTime.toLocaleTimeString();
						var endTimeDisplay = dEndTime.toLocaleTimeString();
						$("#reserve_startTimePicker").val(startTimeDisplay.replace(":00 "," "));
						$("#clientSelectorDiv").show();
						if (thisSource == "Travel"){
							$(".ui-dialog-buttonset").show()
							$("#clientSelectorDiv").hide();
							$("#TravelReasonDiv").show();
							newDialogHeight = (dialogDefaultHeight*.7);
						}
						if (thisSource == "WGE"){
							$( "#availableClients" ).autocomplete({
							  source: availableClients_WGE
							});
							newDialogHeight = (dialogDefaultHeight*.8);
						}
						if (thisSource == "MMWEC"){
							$( "#availableClients" ).autocomplete({
							  source: availableClients_MMWEC
							});
							$("#MMWECAuditTypes").show();
							$("#MMWECAuditTypeAudit").prop("checked", true).click();
						}
						if (thisSource == "HES"){
							$( "#availableClients" ).autocomplete({
							  source: availableClients_HES
							});
							$("#HESMMWECAuditTypes").show();
							$("#MMWECAuditTypeHEA").prop("checked", true).click();
							//set default start end times
							//default is 2 hour appointment
							dEndTime.setHours( dStartTime.getHours() + 2 );
							endTimeDisplay = dEndTime.toLocaleTimeString();
						}
						dialog.dialog('option', 'height', newDialogHeight);
						$("#reserve_endTimePicker").val(endTimeDisplay.replace(":00 "," "));
						
					});
					
					$(".mmwecAuditTypes").on('click',function(){
						var $this = $(this),
							thisValue = $this.val();
						$("#MMWECAuditType").val(thisValue);
						//change default end time
						var dStartTime = new Date($("#reserve_startTime").val()*1000);
						var dEndTime = new Date($("#reserve_endTime").val()*1000);
						var endTimeDisplay = dEndTime.toLocaleTimeString();
						if (thisValue == "Inspection" || thisValue == "SHV" || thisValue == "WiFi" || thisValue == "Combustion Safety Revisit" || thisValue == "Audit" || thisValue == "Verification Inspection" || thisValue == "Presentation"){
							dEndTime.setHours( dStartTime.getHours() + 1 );
							endTimeDisplay = dEndTime.toLocaleTimeString();
						}
						if (thisValue == "HEA" || thisValue == "HEAAU"){
							dEndTime.setHours( dStartTime.getHours() + 2 );
							endTimeDisplay = dEndTime.toLocaleTimeString();
						}
						$("#reserve_endTimePicker").val(endTimeDisplay.replace(":00 "," "));
						
						
						
					});
					
					$("#travelReason").on('keyup',function(){
						var $this = $(this);
						$("#availableClients-electricProvider").val($this.val());
					});
					
				});
				</script>				
				<div id="dialog-form" title="Reserve Appt">
					<p>
						Which Kind of Appointment:<br>
						<div class="row">
							<div class="six columns">
								<?php
									$HESAuditTypeDisplay = 0;
									foreach ($appointmentTypes as $typeName=>$typeSource){
										if ($typeSource == "HES"){
											if ($HESAuditTypeDisplay < 1){
												echo "<span class='source_HES two columns aptTypeSource' style='display:none;'><input type='radio' name='dynamicApptType' id='radio_HES' class='dynamicApptType' data-source='".$typeSource."' value='HES'>HES&nbsp;&nbsp;</span>";
											}
											$HESAuditTypeDisplay++;
										}else{
											echo "<span class='source_".$typeName." two columns aptTypeSource' style='display:none;'><input type='radio' name='dynamicApptType' id='radio_".$typeName."' class='dynamicApptType' data-source='".$typeSource."' value='".$typeName."'>".str_replace("_"," ",$typeName)."&nbsp;&nbsp;</span>";
										}
									}
									echo "<span class='source_Travel' two columns'><input type='radio' name='dynamicApptType' class='dynamicApptType' data-source='Travel' value='Travel'>Travel Time</span>";
								?>
							</div>
						</div>
						<div id='MMWECAuditTypes' style='display:none;'>
							Which Type:<br>
							<input type='radio' class='mmwecAuditTypes' name='MMWECAuditType' id='MMWECAuditTypeAudit' value='Audit'>Audit &nbsp;&nbsp;
							<input type='radio' class='mmwecAuditTypes' name='MMWECAuditType' value='Verification Inspection'>Verification Inspection &nbsp;&nbsp;
							<input type='radio' class='mmwecAuditTypes' name='MMWECAuditType' value='Presentation'>Presentation &nbsp;&nbsp;
						</div>
						<div id='HESMMWECAuditTypes' style='display:none;'>
							Which Type:<br>
							<?php 
								$auditTypeCounter= 0;
								foreach ($appointmentTypes as $typeName=>$typeSource){
									if ($typeSource == "HES"){
										echo "<input type='radio' class='mmwecAuditTypes' name='MMWECAuditType' id='MMWECAuditType".str_replace(" ","",$typeName)."' value='".$typeName."'>".str_replace("_"," ",$typeName)." &nbsp;&nbsp;";
										if ($auditTypeCounter==2){echo "<Br>";}
										$auditTypeCounter++;
									}
								}
							?>
						</div>
						<span id="MMWECAuditTypeDisplay" class="alert"></span>
					</p>
					<div id='dialog-clientSelect' style='display:none;'>
						StartTime: <input type="text" id="reserve_startTimePicker" value="" class="dateTimePickerClass" style="width:100px;"><br>
						EndTime: <input type="text" id="reserve_endTimePicker" value="" class="dateTimePickerClass" style="width:100px;"><br>
						<div id="TravelReasonDiv">
							Description of Reason for Travel Block:<br>
							<input type="text" id="travelReason">
						</div>
						<div id='clientSelectorDiv'>
							<p class="validateTips">Type the Name or City to find Client to Schedule<br>
					 
								<form>
									<div style="display:none;">
										reserveID:<input type="text" id="reserve_id" value=""><br>
										ApptType<input type="text" id="reserve_apptType" value=""><br>
										ES<input type="text" id="reserve_ES" value=""><br>
										ESmmwecID<input type="text" id="reserve_ESmmwecId" value=""><br>
										availableClientsId<input type="text" id="availableClients-id"><br>
										electricProvider<input type="text" id="availableClients-electricProvider"><br>
										email<input type="text" id="availableClients-email"><br>
										mmwecType<input type="text" id="MMWECType"><br>
										mmwecAuditType<input type="text" id="MMWECAuditType"><br>
										siteId<input type="text" id="availableClients-siteId"><br>
										projectId<input type="text" id="availableClients-projectId"><br>
										StartTime: <input type="text" id="reserve_startTime" value=""><br>
										EndTime: <input type="text" id="reserve_endTime" value=""><br>
										<hr>
									</div>

									<input id="availableClients">
									<p id="availableClients-description"></p>
									<!-- Allow form submission with keyboard without duplicating the dialog button -->
									<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
								</form>
							</p>
						</div>
					</div>				
				</div>
				<style>
					#DataTables_Table_0_wrapper {width: 100%;}
					.confirmAptNoMessage {text-decoration: underline;cursor:pointer;font-size:8pt;font-weight:bold;color:orange;}
				</style>
				<table class="simpleTable2" style="width:100%">
					<thead>
						<tr>
							<th>Date</th>
							<th>Staff</th>
							<th>sTime</th>
							<th>eTime</th>
							<th>Type</th>
							<th>Name</th>
							<th>Location</th>
							<th>SiteId</th>
							<th>Status</th>
							<th>Distance</th>
						</tr>
					</thead>
					<tbody>
					<?php
						ksort($hubViewDataByStaff);
						$noapptIdCounter = 1;
						foreach ($hubViewDataByStaff as $calDate=>$ESInfo){
							ksort($ESInfo);
							foreach ($ESInfo as $ES=>$startTimeInfo){
								ksort($startTimeInfo);
								foreach ($startTimeInfo as $startTimeStr=>$endTimeInfo){
									//$startTime = date("H:i a",$startTimeStr);
									foreach ($endTimeInfo as $endTimeStr=>$apptCount){
										foreach ($apptCount as $thisCount=>$apptInfo){
											$endTime = date("H:i",$endTimeStr);
											$siteIdDisplay = $apptInfo->siteId;
											$customerName = $apptInfo->customerName;
											$address = $apptInfo->address;
											$customTdStyle = "";
											$statusDisplay = $apptInfo->status;
											$statusDate = $apptInfo->statusDate;
											$showConfirmButton = true;
											$showReserveButton = false;
											$showDeleteButton = false;
											$showDeleteConfirmedAptButton = false;
											$thisButton = "";
											if ($siteIdDisplay == "Open"){
												$customerName = "Open";
												$siteIdDisplay = "";
												$customTdStyle = "color:Red;background-color:Yellow";
												$statusDisplay = "";
												$showConfirmButton = false;
												$showReserveButton = true;
											}
											if ($apptInfo->visitType == "BLOCKED"){
												$siteIdDisplay = "";
												$blockedId = str_replace("BlockedID","",$apptInfo->siteId);
												$address = $blockedID[$blockedId]["note"];
												$customTdStyle = "color:Silver;background-color:grey";
												$statusDisplay = "";
												$showConfirmButton = false;
											}
											if ($apptInfo->visitType == "Travel"){
												$siteIdDisplay = "";
												$customTdStyle = "color:Silver;background-color:grey";
												$statusDisplay = "";
												$showConfirmButton = false;
												$showDeleteButton = true;
											}
											$deleteScheduleButton = "";
											if ($showConfirmButton){
													if ($apptInfo->status != "Confirmed"){
														$showDeleteButton = true;
														$STARTTime = date("g:i A",strtotime($apptInfo->apptDate));
														$ENDTime = date("g:i A",strtotime($apptInfo->endDate)) ;

														$thisButton = "<span id='confirm".$apptInfo->id."'>
															<button class='confirmApt' title='".(!$apptInfo->email ? " will only mark confirmed" : " will mark confirmed and send email")."'
																data-apptid='".$apptInfo->id."' 
																data-siteid='".$apptInfo->siteId."' 
																data-email='".$apptInfo->email."'
																data-customername='".$apptInfo->customerName."'
																data-appttype='".$apptInfo->visitType."'
																data-apptdate='".date("l, F jS, Y",$calDate)."'
																data-apptstarttime='".$STARTTime."'
																data-apptendtime='".$ENDTime."'
																data-address='".$address."'
																data-electricprovider='".$apptInfo->electricProvider."'
																data-es='".$ES."'
															>Confirm".(!$apptInfo->email ? " via Phone" : " via Email")."</button>
															".($apptInfo->email ? "<br><span class='confirmAptNoMessage' title='will only mark confirmed' data-apptid='".$apptInfo->id."'>Just Mark Confirmed</span>" : "")."
															</span>";
													}
											}
											if ($showDeleteButton){
												//include delete button only if item was added but not confirmed as this could be a mistake...otherwise would need to be cancelled	
												$deleteScheduleButton = "<span class='deleteScheduleButtonSpan' id='deleteScheduleButtonSpan".$noapptIdCounter."' style='position:relative;float:right;display:none;'>
													<img src='".$CurrentServer."images/redx-small.png' class='deleteScheduleButton'	
														data-apptid='".$apptInfo->id."' 
														data-siteid='".$apptInfo->siteId."'
														data-hesid='".$apptInfo->hesId."'
														data-counterid='".$noapptIdCounter."'
													>
													</span>
												";
											}
											
											$deleteConfirmedAptButton = "";
											if ($statusDisplay == "Confirmed"){
												$statusDisplay = "<span style='color:green;'>".$statusDisplay."</span>";
												$showDeleteConfirmedAptButton = true;
												//include delete button only if item was added but not confirmed as this could be a mistake...otherwise would need to be cancelled	
												$deleteConfirmedAptButton = "<span class='deleteScheduleButtonSpan' id='deleteConfirmedAptButtonSpan".$noapptIdCounter."' style='position:relative;float:right;display:none;'>
													<img src='".$CurrentServer."images/redx-small.png' class='deleteConfirmedAptButton'	
														data-apptid='".$apptInfo->id."' 
														data-siteid='".$apptInfo->siteId."'
														data-hesid='".$apptInfo->hesId."'
														data-counterid='".$noapptIdCounter."'
													>
													</span>
												";
											}
											$statusDisplay = (MySQLDate($statusDate) ? $statusDisplay."<br>".MySQLDate($statusDate) : $statusDisplay).$deleteConfirmedAptButton;
											$ESColorPart = explode("'",$ESIDEventColors[$ESIDCounter[$ES]]);
											$eventDetails = $apptInfo->eventDetails."<br>".$statusDisplay;
											$visitType = $apptInfo->visitType;
											$hidden_utility = array();
											$bgasVisitTypes = array("Combustion Safety Revisit","HEA","HEAAU","Inspection","SHV","WiFi");	
											
											$hidden_utility = array();

											if (strpos(" ".strtolower($visitType),"mmwec")){
														$hidden_utility[] = "Mmwec";
											}
											if (strpos(" ".strtolower($visitType),"wge")){
												$hidden_utility[] = "WGE";
											}
											if (in_array($visitType,$bgasVisitTypes)){
												$hidden_utility[] = "Bgas";												
											}

											if (strlen($visitType) > 10){
												$visitType = substr($visitType,0,10)."...";
											}
											$reserveButton = "";
											if ($showReserveButton){
												//if (strpos(" ".$apptInfo->visitType,"MMWEC") || strpos(" ".$apptInfo->visitType,"WGE")){
												//if (strpos(" ".$apptInfo->visitType,"MMWEC")){
													$reserveStartTime = date("Y-m-d",$calDate)." ".date("H:i:s",$startTimeStr);
													$reserveEndTime = date("Y-m-d",$calDate)." ".date("H:i:s",strtotime($endTime));
													$reserveButton = "<span id='reserve".$noapptIdCounter."'>
															<button class='reserveApt'
																data-reserveid='reserve".$noapptIdCounter."'
																data-apptid='".$noapptIdCounter."'
																data-es='".$ES."'
																data-mmwecauditorid='".$MMWECAuditorIdByName[$ES]."'
																data-starttime='".strtotime($reserveStartTime)."' 
																data-endtime='".strtotime($reserveEndTime)."' 
																data-appttype='".$apptInfo->visitType."'
															>+</button></span>";
													$statusDisplay = "<span id='confirmSpotreserve".$noapptIdCounter."' class='alert'></span><span style='display:none;'>".$reserveStartTime."=".strtotime($reserveStartTime)."<br>".$reserveEndTime."=".strtotime($reserveEndTime)."</span>";
															
												//}
											}
											//accommodate for linking directly to Salesforce
											if (strpos(" ".$siteIdDisplay,"Salesforce")){
												$customerName = "<a href='https://cet.my.salesforce.com/".str_replace("SalesforceID","",$siteIdDisplay)."' target='_Blank'>".$customerName."</a>";
											}
											//accommodate for linking directly to Muni
											if (strpos(" ".$siteIdDisplay,"Customer")){
												$customerName = "<a href='".$CurrentServer.$adminFolder."muni/?nav=customer-data&CustomerID=".str_replace("CustomerID","",$siteIdDisplay)."' target='_Blank'>".$customerName."</a>";
											}
											
											//"background"=>$ESColorPart[1]
											//"color"=>$ESColorPart[3]
											
											//check for overlapErrors
											$thisOverlap = $overlapErrorsBySiteProject[$apptInfo->siteId.$apptInfo->projectId];
											
											//create custom row color based on Energy Specialist
											$customESStyle = "";
											if (count($ESColor[$ES])){
												$customESStyle = " style='";
												$customESStyle .= "background-color:".$ESColor[$ES]["background"].";border:1pt solid ".$ESColor[$ES]["border"].";color:".$ESColor[$ES]["color"].";";
												$customESStyle .= "'";
											}
											//track displayed assignments
											$originalAvailabilityTypes = explode(",",$apptInfo->visitType);
											foreach ($originalAvailabilityTypes as $thisType){
												if (!$alreadyCountedType[strtotime(date("m/d/Y D",$calDate)." ".date("h:i",$startTimeStr))][$ES][$thisType]){
													$aptTypePerMonthCount[$calDate][$thisType]++;
													//echo $ESAvailable." ".$apptDate." ".$apptStartTime." ".$thisType."=".$aptTypePerMonthCount[strtotime($apptDate)][$ESAvailable][$thisType]."<br>";
													$alreadyCountedType[strtotime(date("m/d/Y D",$calDate)." ".date("h:i",$startTimeStr))][$ES][$thisType] = 1;
												}
											}
											
											echo "<tr title=\"".$eventDetails."\" id='hubRow".$apptInfo->siteId.$apptInfo->projectId."' style='cursor:default;'>";
												echo "<td ".$customESStyle."><span style='display:none'>".$calDate.$ES.$startTimeStr."</span>".date("m/d/Y D",$calDate)." <span style='color:red;font-weight:bold;background-color:pink;".($thisOverlap ? "border:1pt solid red;padding:1pt;'" : "")."' id='warn".$apptInfo->siteId.$apptInfo->projectId."'>".($thisOverlap ? " Alert" : "")."</span>".$reserveButton."</td>";
												echo "<td ".$customESStyle."><span style='display:none'>".$ES.$calDate.$startTimeStr."</span>".$ES."</td>";
												echo "<td style='".$customTdStyle."' id='startTimereserve".$noapptIdCounter."'>".date("g:ia",$startTimeStr)."</td>";
												echo "<td style='".$customTdStyle."' id='endTimereserve".$noapptIdCounter."'>".date("g:ia",strtotime($endTime))."</td>";
												echo "<td style='".$customTdStyle."' id='visitTypereserve".$noapptIdCounter."'>".$visitType.$deleteScheduleButton."<span style='display:none'>".implode(", ",$hidden_utility) . "</span></td>";
												echo "<td style='".$customTdStyle."' id='customerNamereserve".$noapptIdCounter."'>".$customerName."</td>";
												echo "<td style='".$customTdStyle."' id='customerLocationreserve".$noapptIdCounter."'>".$address."</td>";
												echo "<td id='siteIdreserve".$noapptIdCounter."'>".$siteIdDisplay."</td>";
												echo "<td id='statusreserve".$noapptIdCounter."'>".$statusDisplay.$thisButton."</td>";
												echo "<td id='distancereserve".$noapptIdCounter."'><span id='hubdistanceWarn".$apptInfo->siteId.$apptInfo->projectId."'></span></td>";
											echo "</tr>";
											$noapptIdCounter++;
										}
									}
								}
							}
						}
					?>
					</tbody>
				</table>
			</div><!-- hub view-->
		</div> <!--scheduleAppt-->
		<div id='PriorScheduledData' class='container tab-content'>
			<h1>Previously Scheduled Data</h1>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Date</th>
						<?php 
							foreach ($ESListPast as $ES=>$count){
								echo "<th nowrap=nowrap>".$ES."</th>";
							}
						
						?>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($scheduledDataResultsPast as $calDate=>$esInfo){
							echo "<tr>
								<td><span style='display:none;'>".strtotime($calDate)."</span>".date("m/d/Y",strtotime($calDate))."</td>";
								foreach ($ESListPast as $ES=>$count){
									
									$hoursUsed = 0;
									$apptList = "";
									$apptCount = count($esInfo[$ES]);
									$apptCounter = 0;
									foreach ($esInfo[$ES] as $appt=>$apptInfo){
										$startTime= date("h:i a",strtotime($apptInfo->apptDate));
										$endTime = date("h:i a",strtotime($apptInfo->endDate)) ;
                                        try {
                                            $date1 = new DateTime($startTime);
                                        } catch (Exception $e) {
                                        }
                                        try {
                                            $date2 = new DateTime($endTime);
                                        } catch (Exception $e) {
                                        }
                                        $diff = $date2->diff($date1);
										$hours = $diff->h;
										$apptList .= $startTime." - ".$endTime." ".$apptInfo->visitType."<br>";
										$apptList .= "<span class='customerName'>".$apptInfo->customerName."<br>".$apptInfo->address."<br>".$apptInfo->electricProvider."</span>";
										$apptList .= ($apptCounter < $apptCount ? "<hr>" : "");
										$apptCounter++;
										$hoursUsed+=$hours;
										
										//$aptTypePerMonthCount[strtotime($apptInfo->apptDate)][$ES][$apptInfo->visitType]++;
										if (!$alreadyPastCountedType[strtotime($apptInfo->apptDate)][$ES][$thisType]){
											$aptTypePerMonthCount[strtotime(date("m/d/Y",strtotime($apptInfo->apptDate)))][$apptInfo->visitType]++;
											//echo $ESAvailable." ".$apptDate." ".$apptStartTime." ".$thisType."=".$aptTypePerMonthCount[strtotime($apptDate)][$ESAvailable][$thisType]."<br>";
											$alreadyPastCountedType[strtotime($apptInfo->apptDate)][$ES][$thisType] = 1;
										}
										
										
										
									}
									$hoursAval = 7 - $hoursUsed;
									/*
									if ($apptCounter < 4 && $hoursUsed < 6 && $hoursUsed > 0){
										echo "<td><button style=background-color:#e6f9ff; class = hoursAval><h4> $hoursAval Hours Available </h4></button><br>$apptList";
									}
									elseif ($apptCounter < 4 && $hoursUsed == 0){
										echo "<td><button style=background-color:#A3E4D7; class = noAppointments><h4>All Hours Available</h4></button>";
									}
									elseif($apptCounter < 4 && $hoursUsed == 6){
										echo "<td><button style=background-color:#e6f9ff; class = hoursAval><h4> 1 Hour Available</h4></button><br>$apptList";
									}
									else{
										echo "<td bgcolor=#EAECEE>$apptList";
									}
									*/
									echo "<td>$apptList";
								}
							echo "</td></tr>";
						}
					?>
				</tbody>
			</table>
		</div>
		<div id="AssignedAnalysis" class='container tab-content'>
			<?php
				ksort($aptTypePerMonthCount);
				foreach ($aptTypePerMonthCount as $strDate=>$apptCount){
						foreach ($apptCount as $type=>$value){
							$typeLookup = str_replace(" ","_",$type);
							$typeLookupParts = explode("-",$typeLookup);
							$category = ($appointmentTypes[$typeLookupParts[0]] ? : "Other");
							$apptYearMonthBreakdown[date("Y-m",$strDate)][$category][$type] = $apptYearMonthBreakdown[date("Y-m",$strDate)][$category][$type]+$value;
							$apptYearMonthBreakdown[date("Y-m",$strDate)]["totalCount"] = $apptYearMonthBreakdown[date("Y-m",$strDate)]["totalCount"]+$value;
							$apptYearMonthBreakdown[date("Y-m",$strDate)]["daysInMonth"] = date("t",$strDate);
						}
				}
			
			?>
			<h1>Assignment Analysis</h1>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Year</th>
						<th>Month</th>
						<th>Category</th>
						<th>Type</th>
						<th>Days Assigned</th>
						<th>Total Assignments</th>
						<th>Percentage of Assignment</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($apptYearMonthBreakdown as $YearMonth=>$yearMonthInfo){
							foreach ($yearMonthInfo as $category=>$categoryValue){
								if ($category != "totalCount" && $category !="daysInMonth"){
									foreach ($categoryValue as $type=>$typeValue){
										echo "<tr>
												<td><span style='display:none;'>".strtotime($YearMonth."-01")."</span>".date("Y",strtotime($YearMonth."-01"))."</td>
												<td>".date("F",strtotime($YearMonth."-01"))."</td>
												<td>".$category."</td>
												<td>".$type."</td>
												<td>".$typeValue."</td>
												<td>".$yearMonthInfo["totalCount"]."</td>
												<td>".bcmul(bcdiv($typeValue,$yearMonthInfo["totalCount"],4),100,2)."%</td>
											</tr>";
									}
								}
							}
						}
					?>
				</tbody>
			</table>
		</div>

	</div> <!-- tab contets-->
	<script type="text/javascript">
	$(function () {
		$('#calendar').fullCalendar({
			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			defaultView: 'agendaDay',
			editable: true,
			selectable: true,
			minTime: "08:00:00",
			maxTime: "19:00:00",
			titleFormat: "ddd MMM D, YYYY",
			eventLimit: true, // allow "more" link when too many events
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaDay,agendaTwoDay,agendaWeek,month'
			},
			views: {
				agendaTwoDay: {
					type: 'agenda',
					duration: { days: 2 },

					// views that are more than a day will NOT do this behavior by default
					// so, we need to explicitly enable it
					groupByResource: true

					//// uncomment this line to group by day FIRST with resources underneath
					//groupByDateAndResource: true
				}
			},
			//// uncomment this line to hide the all-day slot
			//allDaySlot: false,

			resources: [
				<?php
					echo $calendarResourceList;
				?>
			],
			events: [
				<?php
					$resourceCounter = 0;
					foreach ($resourceCalendarEvent as $id=>$eventData){
						if ($eventData["siteId"]){
							$fullCalendarEventIdBySiteId[$eventData["siteId"]] = $id;
						}
						echo "{ id: '".$id."', resourceId: '".$eventData["resourceId"]."', start: '".$eventData["start"]."', title: '".$eventData["title"]."', end: '".$eventData["end"]."', description: '".str_replace("'","",$eventData["description"])."', fulldetails: '".str_replace("'","",$eventData["fulldetails"])."'}";
						$resourceCounter++;
						if ($resourceCounter < count($resourceCalendarEvent)){echo ",";}
					}
				?>
			],
			eventRender: function( event, element, view ) {
				element.find('.fc-time').html(''); 
				element.find('.fc-title').html(event.description); 
			},			
			eventMouseover: function(data, event, view) {
//				console.log(data);
				tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#ddd;border-radius: 3px;position:absolute;z-index:10001;padding:2px ;">' + data.fulldetails + '</div>';
				$("body").append(tooltip);
				$(this).mouseover(function (e) {
					$(this).css('z-index', 10000);
					$('.tooltiptopicevent').fadeIn('500');
					$('.tooltiptopicevent').fadeTo('10', 1.9);
				}).mousemove(function (e) {
					$('.tooltiptopicevent').css('top', e.pageY + 10);
					$('.tooltiptopicevent').css('left', e.pageX + 20);
				});
			},
			eventMouseout: function (data, event, view) {
				$(this).css('z-index', 8);

				$('.tooltiptopicevent').remove();

			},
			dayClick: function () {
				tooltip.hide()
			},
			eventResizeStart: function () {
				tooltip.hide()
			},
			eventDragStart: function () {
				tooltip.hide()
			},
			viewDisplay: function () {
				tooltip.hide()
			},
			select: function(start, end, jsEvent, view, resource) {
				console.log(
					'select',
					start.format(),
					end.format(),
					resource ? resource.id : '(no resource)'
				);
			},
			navLinks: true,
			navLinkDayClick: function(date, jsEvent) {
				$("#calendar").fullCalendar('changeView', 'agendaDay');
				$("#calendar").fullCalendar('gotoDate', date);
			}
		});
		
		var table2 = $('.simpleTable2').DataTable({
			"scrollX": true,
			"scrollY": '600px',
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,	
			"iDisplayLength": 600,
			"paging":   false,
			"ordering": true,
			"scrollCollapse": true,
			"info":     false
		});
		
		
		table2.$('tr').tooltip( {
			show: {
				effect: "slideDown"
			},
			"track": false,
			"fade": 500,
			disabled: true,
			close: function( event, ui ) { 
				$(this).tooltip('disable'); 
				/* instead of $(this) you could also use $(event.target) */
			}		
		} );		
		table2.$('tr').on('click', function () {
			 $(this).tooltip('enable').tooltip('open');
		});		
		$("#overlapErrors").html('<?php echo (count($overlapErrors)? '<b>Overlap Errors:</b><Br>'.implode("<br>",$overlapErrors) : '');?>');
		<?php if (count($priorAddressChecker)){?>
			$("#overlapErrors").append("<br><b>Distance Alerts:</b>");
			var priorAddressChecker = function(checkThese){
				var checkThese = JSON.parse(checkThese),
					totalItems = checkThese.length;
				$.each(checkThese,function(key,item){
						var priorAddress = item[0],
							currentAddress = item[1],
							spanId = "#distanceAlert"+item[2],
							hubspanId = "#hubdistanceWarn"+item[2],
							hubRowId = "#hubRow"+item[2],
							warnRowId = "#warn"+item[2],
							ES = item[3],
							calDate = item[4],
							priorName = item[5],
							customerName = item[6];
							var href = '<?php echo $CurrentServer.$adminFolder."/residential/getDistance.php?";?>priorAddress='+priorAddress+'&currentAddress='+currentAddress;
						$.ajax({
							url: href,  //Pass URL here 
							type: "GET", //Also use GET method
							success: function(data) {
								var resultData = JSON.parse(data),
									thisDistanceAlert = resultData.distanceAlert;
								
									$(spanId).html(thisDistanceAlert);
									$(hubspanId).html(thisDistanceAlert);
									$(hubRowId).attr('title',$(hubRowId).attr('title')+thisDistanceAlert);
								if (resultData.alerts){
									$(warnRowId).html('Alert');
									$(warnRowId).css('border','1pt solid red');
									$(warnRowId).css('padding','1px');
									var alertText = "<br>"+item[4]+" "+item[3]+" has travel distance of "+resultData.distance+" between "+item[5]+" and "+item[6];
									$("#overlapErrors").append(alertText);
								}
								if (key == (totalItems-1)){
									//alert('done');
									table2.row().invalidate().draw();									
									//done processing them all
								}
							}		
							
						});
				});
				
			};
			var priorAddresses = new Array('<?php echo json_encode($priorAddressChecker);?>');
			priorAddressChecker(priorAddresses);
			
		<?php }?>
		
		var getFilters = function(){
			var visitType = ($("#VisitType").val() ? $("#VisitType").val() : ''),
				cityName = ($("#CityName").val() ? $("#CityName").val() : ''),
				energySpecialist = ($("#SpecialistName").val() ? $("#SpecialistName").val() : ''),
				area = ($("#Area").val() ? $("#Area").val() : ''),
				filterValues = new Array(visitType,cityName,energySpecialist,area);
				
				return filterValues;
		};
		$("#resetbutton").on('click',function(){
			$(".parameters").val('');
			updateFilters();
			
		});
		
		$(".cancelApt").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				apptId = $this.attr('data-apptid');
				console.log(apptId);
				
			var	data = {};
				data["action"] = "cancelApt";
				data["apptId"] = apptId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							//console.log(resultdata);
							location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
				
				
		});
		$(".removeBlock").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				scheduledId = $this.attr('data-scheduledid'),
				blockedId = $this.attr('data-blockedid'),
				blockedDiv = "#blockedSchedule"+scheduledId,
				blockedEvent = "#blockedEvent"+scheduledId,
				fullCalendarEventId = fullCalendarEventIdBySiteId[blockedId];

				var	data = {};
					data["action"] = "removeBlock";
					data["scheduledId"] = scheduledId;
					data["blockedId"] = blockedId;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
							$(blockedDiv).hide();
							$(blockedEvent).hide();
							$('#calendar').fullCalendar( 'removeEvents', fullCalendarEventId );
							
							//console.log(resultdata);
							//location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
			
		});
		$(".deleteScheduleButton").on('click',function(e){
			e.preventDefault();
			var confirmDelete = confirm("Are you sure you want to reset this appt?");
			if (confirmDelete){
				var $this = $(this),
					apptId = $this.attr('data-apptid'),
					siteId = $this.attr('data-siteid'),
					hesId = $this.attr('data-hesid'),
					counterId = $this.attr('data-counterid'),
					visitTypeId = "#visitTypereserve"+counterId,
					customerNameId = "#customerNamereserve"+counterId,
					locationId = "#customerLocationreserve"+counterId,
					siteidreserve = "#siteIdreserve"+counterId,
					statusreserve = "#statusreserve"+counterId,
					distancereserve = "#distancereserve"+counterId,
					deleteButtonSpan = "#deleteScheduleButtonSpan"+counterId;
					
					$(deleteButtonSpan).html("<img src='<?php echo $CurrentServer;?>images/ajaxLoaderSmall.gif'>");
					
					
					var	data = {};
						data["action"] = "deleteSchedule";
						data["apptId"] = apptId;
						data["siteId"] = siteId;
						data["hesId"] = hesId;
						//console.log(data);
						$.ajax({
							url: "ApiResidentialManagement.php",
							type: "POST",
							data: JSON.stringify(data),
							success: function(resultdata){
								//console.log(resultdata);
								$(deleteButtonSpan).html('');
								$(visitTypeId).html('');
								$(customerNameId).html('');
								$(locationId).html('');
								$(siteidreserve).html('');
								$(statusreserve).html('');
								$(distancereserve).html('');
								
								
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								console.log(message);
							}
						});
			}
		});
		$(".deleteConfirmedAptButton").on('click',function(e){
			e.preventDefault();
			var confirmDelete = confirm("Are you sure you want to remove this confirmed appointment?\r\nYou will need to have first marked it as cancelled or removed the record from the source location otherwise the appointment will be reinserted on next data refresh.\r\nBe sure staff and client have been informed if appropriate.");
			if (confirmDelete){
				var $this = $(this),
					apptId = $this.attr('data-apptid'),
					siteId = $this.attr('data-siteid'),
					hesId = $this.attr('data-hesid'),
					counterId = $this.attr('data-counterid'),
					visitTypeId = "#visitTypereserve"+counterId,
					customerNameId = "#customerNamereserve"+counterId,
					locationId = "#customerLocationreserve"+counterId,
					siteidreserve = "#siteIdreserve"+counterId,
					statusreserve = "#statusreserve"+counterId,
					distancereserve = "#distancereserve"+counterId,
					deleteButtonSpan = "#deleteConfirmedAptButtonSpan"+counterId;
					
					$(deleteButtonSpan).html("<img src='<?php echo $CurrentServer;?>images/ajaxLoaderSmall.gif'>");
					
					
					var	data = {};
						data["action"] = "deleteSchedule";
						data["apptId"] = apptId;
						data["siteId"] = siteId;
						data["hesId"] = hesId;
						//console.log(data);
						$.ajax({
							url: "ApiResidentialManagement.php",
							type: "POST",
							data: JSON.stringify(data),
							success: function(resultdata){
								//console.log(resultdata);
								$(deleteButtonSpan).html('');
								$(visitTypeId).html('');
								$(customerNameId).html('');
								$(locationId).html('');
								$(siteidreserve).html('');
								$(statusreserve).html('');
								$(distancereserve).html('');
								
								
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								console.log(message);
							}
						});
			}
		});
		$(".confirmAll").on('click',function(e){
			var eachCounter = -1;
			$(".confirmApt").each(function(k,v){
				eachCounter = k;
			});
			//for testing
			//eachCounter = 4;
			if ((eachCounter+1) > 0){
				var confirmToProceed = confirm('This will send '+(eachCounter+1)+' confirmations');
				var confirmMessage = "You Have Chosen Wisely";
			}else{
				var confirmMessage = "There are no confirmations ready to be sent";
				var confirmToProceed = false;
			}
			if (confirmToProceed){
				$("#confirmAllButton").html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:20px;">');

				var confirmAllResultsNumber = $("#confirmAllResultsNumber");
				var confirmAllResults = $("#confirmAllResults");
				$(".confirmApt").each(function(k,v){
					//for testing
					//if (k <= eachCounter){
						var $this = $(v);
						apptId = $this.attr('data-apptid'),
						siteId = $this.attr('data-siteid'),
						email = $this.attr('data-email'),
						customerName = $this.attr('data-customername'),
						apptType = $this.attr('data-appttype'),
						apptDate = $this.attr('data-apptdate'),
						apptStartTime = $this.attr('data-apptstarttime'),
						apptEndTime = $this.attr('data-apptendtime'),
						electricProvider = $this.attr('data-electricprovider'),
						ES = $this.attr('data-es'),
						address = $this.attr('data-address'),
						spanId = "#confirm"+apptId;
							$(spanId).html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:20px;">');
							var	data = {};
							data["action"] = "confirmApt";
							data["apptId"] = apptId;
							data["siteId"] = siteId;
							data["customerName"] = customerName;
							data["apptType"] = apptType;
							data["apptDate"] = apptDate;
							data["apptStartTime"] = apptStartTime;
							data["apptEndTime"] = apptEndTime;
							data["electricProvider"] = electricProvider;
							data["ES"] = ES;
							data["address"] = address;
							
						if (email != ""){
							data["email"] = email;
						}
						//console.log('dataToSend');
						//console.log(data);
						$.ajax({
							url: "ApiResidentialManagement.php",
							type: "POST",
							data: JSON.stringify(data),
							success: function(resultdata){
								//console.log('dataReceived');
								//console.log(resultdata);
									var resultId = resultdata.split("_"),
										spanId = "#confirm"+resultId[1];
										//console.log('update spandId '+spanId);
									$(spanId).html("<span class='info'>Marked Confirmed"+resultId[0]+"</span>");
									var thisNumber = parseInt(confirmAllResultsNumber.html())+1;
									confirmAllResultsNumber.html(thisNumber);
									confirmAllResultsNumber.show();
									confirmAllResults.html('Confirmations Created');
									//console.log(thisNumber+"=="+(eachCounter+1));
									if (thisNumber == (eachCounter+1)){
										$("#confirmAllButton").html('');
										var	newdata = {};
										newdata["action"] = "sendPrintFile";
										//console.log(newdata);
										$.ajax({
											url: "ApiResidentialManagement.php",
											type: "POST",
											data: JSON.stringify(newdata),
											success: function(printdata){
												//console.log('printedData');
												//console.log(printdata);
											},
											error: function(jqXHR, textStatus, errorThrown){
												var message = $.parseJSON(jqXHR.responseText);
												$(thisAlert).show().html(message).addClass("alert");
											}
										});
										alert('all done');
										
									}else{
										//console.log('not end of list yet');
									}
									//location.reload();
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$(thisAlert).show().html(message).addClass("alert");
							}
						});
					//}//end testing
				});
			}else{
				alert(confirmMessage);
			}
		});
		
		$(".confirmApt").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				apptId = $this.attr('data-apptid'),
				siteId = $this.attr('data-siteid'),
				email = $this.attr('data-email'),
				customerName = $this.attr('data-customername'),
				apptType = $this.attr('data-appttype'),
				apptDate = $this.attr('data-apptdate'),
				apptStartTime = $this.attr('data-apptstarttime'),
				apptEndTime = $this.attr('data-apptendtime'),
				electricProvider = $this.attr('data-electricprovider'),
				ES = $this.attr('data-es'),
				address = $this.attr('data-address'),
				spanId = "#confirm"+apptId;
				$(spanId).html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:20px;">');
				
				var	data = {};
					data["action"] = "confirmApt";
					data["apptId"] = apptId;
					data["siteId"] = siteId;
					data["customerName"] = customerName;
					data["apptType"] = apptType;
					data["apptDate"] = apptDate;
					data["apptStartTime"] = apptStartTime;
					data["apptEndTime"] = apptEndTime;
					data["electricProvider"] = electricProvider;
					data["ES"] = ES;
					data["address"] = address;
					if (email != ""){
						data["email"] = email;
					}else{
						alert("No email on record. Please confirm with them via a phone call");
					}
					$.ajax({
						url: "ApiResidentialManagement.php",
						type: "POST",
						data: JSON.stringify(data),
						success: function(resultdata){
								var resultId = resultdata.split("_"),
									spanId = "#confirm"+resultId[1];
								$(spanId).html("<span class='info'>Marked Confirmed"+resultId[0]+"</span>");
								//location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$(thisAlert).show().html(message).addClass("alert");
						}
					});
				
			
				
		});
		$(".confirmAptNoMessage").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				apptId = $this.attr('data-apptid'),
				spanId = "#confirm"+apptId;
				$(spanId).html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:20px;">');
				
				var	data = {};
					data["action"] = "confirmAptNoMessage";
					data["apptId"] = apptId;
					$.ajax({
						url: "ApiResidentialManagement.php",
						type: "POST",
						data: JSON.stringify(data),
						success: function(resultdata){
								var resultId = resultdata.split("_"),
									spanId = "#confirm"+resultId[1];
								$(spanId).html("<span class='info'>Marked Confirmed"+resultId[0]+"</span>");
								//location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$(thisAlert).show().html(message).addClass("alert");
						}
					});
				
			
				
		});
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder).($sourceFolder ? : "residential");?>/?VisitType="+Filters[0]+"&CityName="+Filters[1]+"&SpecialistName="+Filters[2]+"&Area="+Filters[3]+"&nav=<?php echo $navDropDown;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		$("#runMailer").on('click',function(e){
			$("#mailerData").html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:50px;"><br>Generating Current Mailer Data....will take a few moments');
			var href = '<?php echo $CurrentServer.$adminFolder."residential/?nav=ESSchedulerDisplay";?>';
				$.ajax({
					url: href,  //Pass URL here 
					type: "GET", //Also use GET method
					success: function(data) {
						var dataDisplay = $(data).find('#ESSchedulerDataDisplay').html();
						$("#mailerData").html(dataDisplay);
					}			
				});
		});
		$("#refreshData").on('click',function(e){
			$("#refreshDataImage").html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:50px;"><br>...one moment...');
			var href = '<?php echo $CurrentServer.$adminFolder."residential/?nav=ESSchedulerDisplay";?>';
				$.ajax({
					url: href,  //Pass URL here 
					type: "GET", //Also use GET method
					success: function(data) {
						location.reload();
					}			
				});
		});
		$("#showDeleteButton").on('click',function(e){
			e.preventDefault();
			$(".deleteScheduleButtonSpan").toggle();
		});
		
		
		var table = $('.simpleTable').DataTable({
			columnDefs: [
				{ width: 100, targets: 0 }
			],
			"scrollX": true,
			"scrollY": '600px',
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,	
			"iDisplayLength": 600,
			"paging":   true,
			"ordering": true,
			"scrollCollapse": true,
			"info":     false,
			"fixedColumns": {"heightMatch": 'auto'}

		});
		//double click the header date column to align rows because of fixedColumn function
		setTimeout( function(){ 
			table.column(0).header().click();
		}  , 1000 );
		setTimeout( function(){ 
			table.column(0).header().click();
		}  , 1500 );
		
		$(".tab2links").on('click',function(){
			var $this = $(this),
				viewName = "#"+$this.attr('data-viewname');
			$(".tab2content").hide();
			$(".tab2links").removeClass("active");
			$(viewName).show();
			$this.addClass("active");
		});
		
		setTimeout( function(){ 
			$("#hubViewButton").click();
		}  , 1500 );
	<?php
		if (count($filteredByList)){
			echo '$("#currentScheduledData_length").html($("#currentScheduledData_length").html()+\' <b>Filtered By</b> '.implode(", ",$filteredByList).'\');';
			echo '$("#DataTables_Table_0_filter").html(\'<b>Filtered By</b> '.implode(", ",$filteredByList).' <span style="color:red;">Open Time Slots' . (!$isStaff ? " & Send All Confirmations disabled" : "") . ' while Filters are active</span> \'+$("#DataTables_Table_0_filter").html());';

		}
	?>
	
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };
        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
	var fullCalendarEventIdBySiteId = <?php echo json_encode($fullCalendarEventIdBySiteId);?>;
</script>