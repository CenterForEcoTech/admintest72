<?php
$row = 1;
$dataRow = 0;
if (($handle = fopen($target_file, "r")) !== FALSE) {
  while (($data = fgetcsv($handle)) !== FALSE) {
    $num = count($data);
    //echo "<p> $num fields in line $row: <br /></p>\n";
	if ($row < 2){$headerRow = $data;}
	if ($row == 2){$headerValue = $data;}
	if ($row == 3){$headerTypes = $data;}
	if ($row > 3){
		for ($c=0; $c < $num; $c++) {
			$csvRows[$dataRow][$headerRow[$c]."|".$headerValue[$c]]=$data[$c];
			//clean
			$cleanCsvRows[$dataRow][$headerValue[$c]]=(!$cleanCsvRows[$dataRow][$headerValue[$c]] ? $data[$c] : $cleanCsvRows[$dataRow][$headerValue[$c]]);
			//echo $data[$c] . "<br />\n";
		}
		$dataRow++;
	}
    $row++;
  }
  fclose($handle);
}
echo "Processing ".$dataRow." rows of data:<br>";
//print_pre($csvRows);
//print_pre($cleanCsvRows);

//echo "<hr>new<br>";

$dataLoaderFields = array(
	"Response ID","Contact information: - First Name:","Contact information: - Last Name:","Company Name","Contact information: - Phone Number:",
	"Contact information: - E-Mail Address:","Are you a homeowner?",
	"How did you learn about Solar Access? - Selected Choice","How did you learn about Solar Access? - Referred by another organization: - Text","How did you learn about Solar Access? - Other: - Text",
	"Where in Massachusetts do you live? - I live in:","What is your heating fuel? - Selected Choice",
	"How many people are in your household?","What is your total household income?","What kind of home do you live in?","Score"
);


foreach ($cleanCsvRows as $row=>$cleanData){
	$dataLoaderRow = array();
	foreach ($dataLoaderFields as $fieldName){
		$thisData = $cleanData[$fieldName];
		if ($fieldName == "What kind of home do you live in?"){
			if ($thisData == "Single family"){$thisData = "Single family home";}
			if ($thisData == "2 to 4 family home"){$thisData = "2-4 unit family home";}
		}
		if ($fieldName == "Company Name"){$thisData = $cleanData["Contact information: - First Name:"]." ".$cleanData["Contact information: - Last Name:"];}
		$dataLoaderData[$row][$fieldName] = $thisData;
		$dataLoaderRow[] = $thisData;
	}
	$dataLoaderRows[] = $dataLoaderRow; 
}

//print_pre($dataLoaderRows);

# Generate CSV data from array
$newFileName = str_replace(".csv","_dataloader.csv",$dataFileName);
$target_file = $siteRoot.$adminFolder."residential/SolarAccessData/".$newFileName;
        $fh = fopen($target_file, 'w');
        # write out the headers
        fputcsv($fh, $dataLoaderFields);
        foreach ($dataLoaderRows as $row) {
             fputcsv($fh, $row);
        }
        fclose($fh);
echo "<a href='".$CurrentServer.$adminFolder."residential/SolarAccessData/".$newFileName."'>".$newFileName."</a><Br>";

?>