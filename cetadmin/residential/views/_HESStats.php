<?php
$startAgo = (date("n",strtotime($TodaysDate)) <= 11 ? $TodaysDate." - 1 Year" : $TodaysDate); 
$FiscalYearStart = date("11/1/Y",strtotime($startAgo)); //change 2017 to Y
$FiscalYearStartYear = date("Y",strtotime($startAgo)); 
$EndAgo = (date("n",strtotime($TodaysDate)) > 11 ? $TodaysDate." + 1 Year" : $TodaysDate); 
$FiscalYearEnd = date("10/31/Y",strtotime($EndAgo)); 
$FiscalYearEndYear = date("Y",strtotime($EndAgo)); 
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $FiscalYearStart);
//$StartDate = date("2017/11/1"); //trying to set a hard date
$startDate = date("Y-m-d",strtotime($StartDate));
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $FiscalYearEnd);
//$EndDate = date("2018/10/31"); //trying to set a hard end date
$endDate = date("Y-m-d",strtotime($EndDate));


$path = ($path ? $path : $siteRoot.$adminFolder);
include_once($path.'sharepoint/SharePointAPIViews.php');
//echo date("Y-m-d",strtotime(date()." - 1 day"))." 12 pm";
$cachedTimeStamp = strtotime(date("Y-m-d",strtotime(date()." - 1 day"))." 12 pm");
$cachedFiledLocationName = $path.'sharepoint/cache/SharepointData_'.$cachedTimeStamp;

foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	foreach (${$ArrayName} as $SPData){
		$spData = (object)$SPData;
		//$results[] = $cetDataProvider->insertSharePointData($spData);
		$ESFullName = strtoupper($spData->energy_x0020_specialist);
		$ESNameParts = explode(" ",$ESFullName);
		$ESFirstName = ($ESNameParts[0] == "JENNIFER" ? "JEN" : $ESNameParts[0]);
		$ES = $ESFirstName." ".substr($ESNameParts[1],0,1);
		$heaDate = strtotime($spData->hea_x0020_date);
		$apptType = $spData->intakeaudittype;
		$status = $spData->status;
		$siteId = $spData->site_x0020_id;
		
		$asIssued = $spData->a_x002f_s_x0020_contracts_x0020_;
		$asSigned = $spData->a_x002f_s_x0020_date_x0020_signe;
		$asBillDate = $spData->utility_x0020_billing_x0020_date;
		$asBillYearMonth = date("Y_m",strtotime($asBillDate));
		
		$insIssued = $spData->contracts_x0020_issued_x0020_dat;
		$insSigned = $spData->insulation_x0020_date_x0020_sign;
		$insBillDate = $spData->insulation_x0020_billing_x0020_d;
		$insBillYearMonth = date("Y_m",strtotime($insBillDate));
		$includeThisData = false;
//		$includeThisData = (strtotime($startDate) <= $heaDate && $heaDate <= strtotime($endDate) ? true : false);
		if ($asIssued){
			$includeThisData = (strtotime($startDate) <= strtotime($asIssued) && strtotime($asIssued) <= strtotime($endDate) ? true : false);
			$month = date("M",strtotime($asIssued));
		}
		if ($insIssued){
			$includeThisData = (strtotime($startDate) <= strtotime($insIssued) && strtotime($insIssued) <= strtotime($endDate) ? true : $includeThisData);
			$month = ($InstallMonth ? $InstallMonth : date("M",strtotime($insIssued)));
		}
		
		if ($includeThisData && $apptType!="WiFi" && $aptType!="Inspection"){
				
				echo "Energy Specialist: ".$ES."<br>";
				echo "Site ID: ".$siteId."<br>";
				echo "Appt Type: ".$apptType."<br>";
				echo "HEADate: ".date("m/d/Y",$heaDate)."<bR>";
				echo "asIssued: ".$asIssued."<br>";
				echo "asSigned: ".$asSigned."<br>";
				echo "asBillDate: ".$asBillDate."<br>";
				echo "insIssued: ".$insIssued."<br>";
				echo "insSigned: ".$insSigned."<br>";
				echo "insBillDate: ".$insBillDate."<br>";
				echo "status: ".$status."<br>";
				echo "<hr>";
				
			$open = 0;
			//if ($extractData["MEAS_STATUS"] == "Open Contracts Pending" OR $extractData["MEAS_STATUS"] == "Customer Required Action/Roadblocks"){open = 1;}
			if ($status == "Open Contracts Pending" OR $status == "Customer Required Action/Roadblocks"){$open = 1;}	//commented out originally		
			if ($asIssued && !$asSigned){$open = 1;}
			if ($insIssued && !$insSigned){$open = 1;}
			
			$pending = 0;
			//if ($extractData["MEAS_STATUS"] == "To Be Received/Input" OR $extractData["MEAS_STATUS"] == "Open Contracts Pending"){pending = 1;}
			if ($status == "To Be Received/Input" OR $status == "Open Contracts Pending"){$pending = 1;}  //commented out originally
			if ($asSigned && !$asBillDate){$pending = 1;}
			if ($insSigned && !$insBillDate){$pending = 1;}

			$extractResults[$ES]["Contract"]["ContractsOpen"] = $extractResults[$ES]["Contract"]["ContractsOpen"]+$open;
			$extractResultsByMonth[$month][$ES]["Contract"]["ContractsOpen"] = $extractResultsByMonth[$month][$ES]["Contract"]["ContractsOpen"]+$open;
			$extractResults["ZZZTotal"]["Contract"]["ContractsOpen"] = $extractResults["ZZZTotal"]["Contract"]["ContractsOpen"]+$open;
			$extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsOpen"] = $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsOpen"]+$open;

			$extractResults[$ES]["Contract"]["ContractsPending"] = $extractResults[$ES]["Contract"]["ContractsPending"]+$pending;
			$extractResultsByMonth[$month][$ES]["Contract"]["ContractsPending"] = $extractResultsByMonth[$month][$ES]["Contract"]["ContractsPending"]+$pending;
			//$extractResultsByMonth[$month][$ES]["Contract"]["ContractsPendingSites"][] = $siteId;
			$extractResults["ZZZTotal"]["Contract"]["ContractsPending"] = $extractResults["ZZZTotal"]["Contract"]["ContractsPending"]+$pending;
			$extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsPending"] = $extractResultsByMonth[$month]["ZZZTotal"]["Contract"]["ContractsPending"]+$pending;
			$contracts[$siteId][] = $spData;
			$contractData[$ES][] = array("siteID"=>$siteId,"HEADate"=>date("m/d/Y",$heaDate),"apptType="=>$apptType,"airSealingIssued"=>$asIssued,"airSealingSigned"=>$asSigned,"airSealingBillDate"=>$asBillDate,"insIssued"=>$insIssued,"insSigned"=>$insSigned,"insBillDate"=>$insBillDate);
			print "Pending Results: ".$extractResults["ZZZTotal"]["Contract"]["ContractsPending"]."<br>";
			print "Open Results: ".$extractResults["ZZZTotal"]["Contract"]["ContractsOpen"]."<br>";
			echo "<hr>";
		}
		
		
		erase_val($SPData);
		$SPData = null;
	}//end foreach sprecord
	${$ArrayName} = null;
}
//print_pre($extractResultsByMonth);
//echo count($contracts);
//print_pre($contracts);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ProductProvider.php");
$ProductProvider = new ProductProvider($dataConn);
include_once($dbProviderFolder."CETDataProvider.php");
$cetDataProvider = new CETDataProvider($dataConn);

$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $ProductProvider->get($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);
foreach ($resultArray as $result=>$record){
	$efiPartsToRemove["_".$record->efi] = "";
	$efiParts = explode(".",$record->efi);
	$bulbType = ($record->bulbType ? : "LED");
	$bulbStyle = $record->bulbStyle;
	$CSGCode = $record->csgCode;
	$CSGCodes = array();
	if (strpos($CSGCode,",")){
		$CSGCodes = explode(",",$CSGCode);
	}else{
		$CSGCodes = array($CSGCode);
	}
	$clearResultsCode = $bulbType.$efiParts[1];
	$record->clearResultsCode = $clearResultsCode;
	foreach ($CSGCodes as $code){
		$ProductsByCSGCodes[$code] = $record;
	}
	$active = $record->displayOrderId;
	$ProductsByBulbStyle[$bulbStyle][] = $record;
	if ($active){
		$ProductsByBulbStyle_Active[$bulbStyle][] = $record;
	}else{
		$ProductsByBulbStyle_InActive[$bulbStyle][] = $record;
	}
}
ksort($ProductsByBulbStyle);
//print_pre($ProductsByBulbStyle);

include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$criteria = new StdClass();
$criteria->endDate = $endDate;
$criteria->startDate = $startDate;
$criteria->visitType = "WiFi";
$scheduledResults = $residentialProvider->getScheduledData($criteria);
foreach ($scheduledResults->collection as $wifiScheduledVisits){
	$ES = strtoupper($wifiScheduledVisits->energySpecialist);
	$InstallMonth = date("M",strtotime($wifiScheduledVisits->apptDate));
	$visitType = "WIFI Appts Scheduled";
	$VisitTypesFound[$visitType] = 1;
	$extractResults[$ES]["VisitTypes"][$visitType] = $extractResults[$ES]["VisitTypes"][$visitType]+1;
	$extractResults["ZZZTotal"]["VisitTypes"][$visitType] = $extractResults["ZZZTotal"]["VisitTypes"][$visitType]+1;
	
	$extractResultsByMonth[$InstallMonth][$ES]["VisitTypes"][$visitType] = $extractResultsByMonth[$InstallMonth][$ES]["VisitTypes"][$visitType]+1;
	$extractResultsByMonth[$InstallMonth]["ZZZTotal"]["VisitTypes"][$visitType] = $extractResultsByMonth[$InstallMonth]["ZZZTotal"]["VisitTypes"][$visitType]+1;
}

//print_pre($wifiScheduledVisitsByESByDate);
$criteria = new StdClass();
$criteria->endDate = $endDate;
$criteria->startDate = $startDate;
$criteria->orderBy = "SiteID";
$extractBgas = $cetDataProvider->getCRIExtractDataBGAS($criteria);
$SiteIDsIncluded = array();
foreach ($extractBgas as $extractData){
	//print_pre($extractData);
	$ES = $extractData["AUDITOR_FIRST_NAME"]." ".substr($extractData["AUDITOR_LAST_NAME"],0,1);
	$ESFull = $extractData["AUDITOR_FIRST_NAME"]." ".$extractData["AUDITOR_LAST_NAME"];
	$InstallDate = strtotime($extractData["INSTALL_DT"]);
	$InstallMonth = date("M",$InstallDate);
	
	if (trim($ES) != ''){
		$ESNameList[$ES] = 1;
		$ESFullNameList[$ESFull] = 1;
	}
	//get ISM Info
	$ISMName = ($ProductsByCSGCodes[$extractData["INS_PARTID"]] ? $ProductsByCSGCodes[$extractData["INS_PARTID"]]->description : "NonISM");
	
	//also get Wifi items
	if (strpos($extractData["CONTRACT"],"_WIFI")){
		$ISMName = $extractData["DESCRIPTION"];
	}
	
	if ($ISMName != "NonISM"){
		$extractResults[$ES]["ISMs"][$ISMName] = $extractResults[$ES]["ISMs"][$ISMName]+$extractData["INS_QTY"];
		$extractResults["ZZZTotal"]["ISMs"][$ISMName] = $extractResults["ZZZTotal"]["ISMs"][$ISMName]+$extractData["INS_QTY"];
		
		$extractResultsByMonth[$InstallMonth][$ES]["ISMs"][$ISMName] = $extractResultsByMonth[$InstallMonth][$ES]["ISMs"][$ISMName]+$extractData["INS_QTY"];
		$extractResultsByMonth[$InstallMonth]["ZZZTotal"]["ISMs"][$ISMName] = $extractResultsByMonth[$InstallMonth]["ZZZTotal"]["ISMs"][$ISMName]+$extractData["INS_QTY"];
		
		
		$ISMSFound[$ISMName] = 1; //used to get a master set of all ISMs to list for the rows
	}
	
	//get VisitType Info
	$SiteSuffix = (strpos($extractData["CONTRACT"],"_WIFI") ? "_WIFI" : "");
	$SiteID = $extractData["SITEID"].strtotime($extractData["APPTDATE"]).$SiteSuffix;
	$SiteIDAlreadyIncluded = in_array($SiteID,$SiteIDsIncluded);
	if (!$SiteIDAlreadyIncluded){
		$extractResults[$ES]["VisitTypes"][$extractData["APPTTYPE"]] = $extractResults[$ES]["VisitTypes"][$extractData["APPTTYPE"]]+1;
		$extractResultsVisits[$ES][$extractData["APPTTYPE"]][] = $SiteID;
		$extractResults["ZZZTotal"]["VisitTypes"][$extractData["APPTTYPE"]] = $extractResults["ZZZTotal"]["VisitTypes"][$extractData["APPTTYPE"]]+1;
		
		$extractResultsByMonth[$InstallMonth][$ES]["VisitTypes"][$extractData["APPTTYPE"]] = $extractResultsByMonth[$InstallMonth][$ES]["VisitTypes"][$extractData["APPTTYPE"]]+1;
		$extractResultsByMonth[$InstallMonth]["ZZZTotal"]["VisitTypes"][$extractData["APPTTYPE"]] = $extractResultsByMonth[$InstallMonth]["ZZZTotal"]["VisitTypes"][$extractData["APPTTYPE"]]+1;
		
		if (trim($extractData["APPTTYPE"]) != ''){
			$VisitTypesFound[$extractData["APPTTYPE"]] = 1; //used to get a master set of all VisitTypes to list for the rows
		}
		if ($extractData["MEAS_STATUS"] == "BILLING"){
			$extractResults[$ES]["Contract"]["ContractsClosedCount"] = $extractResults[$ES]["Contract"]["ContractsClosedCount"]+1;
			$extractResults["ZZZTotal"]["Contract"]["ContractsClosedCount"] = $extractResults["ZZZTotal"]["Contract"]["ContractsClosedCount"]+1;

			$extractResultsByMonth[$InstallMonth][$ES]["Contract"]["ContractsClosedCount"] = $extractResultsByMonth[$InstallMonth][$ES]["Contract"]["ContractsClosedCount"]+1;
			$extractResultsByMonth[$InstallMonth]["ZZZTotal"]["Contract"]["ContractsClosedCount"] = $extractResultsByMonth[$InstallMonth]["ZZZTotal"]["Contract"]["ContractsClosedCount"]+1;
		}
		$SiteIDsIncluded[]=$SiteID;
	}
	
	//get Contract Info of how many Sitevists had at least one completed contract (weatherization and airsealing would only count as 1, not one each, even if both were completed)
	//Wifi doesn't count in this Stats calculation for completion information
	if ($extractData["MEAS_STATUS"] == "BILLING"){
		$contractTotal = bcadd($extractData["CUST_PRICE"],$extractData["UTIL_PRICE"],2);
		$extractResults[$ES]["Contract"]["ContractsClosedAmount"] = bcadd($extractResults[$ES]["Contract"]["ContractsClosedAmount"],$contractTotal,2);
		$extractResults["ZZZTotal"]["Contract"]["ContractsClosedAmount"] = bcadd($extractResults["ZZZTotal"]["Contract"]["ContractsClosedAmount"],$contractTotal,2);
		
		$extractResultsByMonth[$InstallMonth][$ES]["Contract"]["ContractsClosedAmount"] = bcadd($extractResultsByMonth[$InstallMonth][$ES]["Contract"]["ContractsClosedAmount"],$contractTotal,2);
		$extractResultsByMonth[$InstallMonth]["ZZZTotal"]["Contract"]["ContractsClosedAmount"] = bcadd($extractResultsByMonth[$InstallMonth]["ZZZTotal"]["Contract"]["ContractsClosedAmount"],$contractTotal,2);
	}
}
ksort($ESNameList);
ksort($ESFullNameList);
/*
print_pre($ESFullNameList);
print_pre($ISMSFound);
print_pre($VisitTypesFound);
print_pre($ISMSFound);
*/
//print_pre($extractResultsVisits["ANGEL O"]);
//print_pre($extractResults["ANGEL O"]);
//print_pre($extractResults);

foreach ($ProductsByBulbStyle as $bulbStyle=>$bulbs){
	$bulbStyle = ($bulbStyle ? $bulbStyle : "2(non-bulb)");
	foreach ($bulbs as $record){ 
		//print_pre($record);
		$description = $record->description;
		$csgCodes = explode(",",$record->csgCode);
		if ($ISMSFound[$description]){
			$ISMSToInclude[$bulbStyle][$description] = $record;
		}else{
			if (count($csgCodes)){
				foreach ($csgCodes as $csgCode){
					if ($ISMSFound[$csgCode]){
						$ISMSToInclude["1(".$bulbStyle.")"][$csgCode] = $record;
					}
				}
			}
			
		}
	}
}
ksort($ISMSToInclude);
//print_pre($ISMSToInclude);

include_once('model/createTabExcelFile.php');

echo "<h1>HES Stats Generator</h1>";
?>
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo $StartDate;?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo $EndDate;?>" title="End Date">
<button id="resetFilter">reset filters</button>
<?php
if ($_GET['StartDate'] == ""){
	echo "<Br><br>".$ExportFileLink;
}
?>

<table class="reportResults">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th colspan="<?php echo count($ESNameList);?>">Energy Specialists</th>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<?php
				foreach ($ESNameList as $ES=>$count){
					echo "<th>".$ES."</th>";
				}
			?>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Contracts Open</td>
		<?php
			foreach ($ESNameList as $ES=>$count){
				echo "<td>".$extractResults[$ES]["Contract"]["ContractsOpen"]."</td>";
			}
			echo "<td>".$extractResults["ZZZTotal"]["Contract"]["ContractsOpen"]."</td>";
		?>
		</tr>
		<tr>
			<td>Contracts Pending</td>
		<?php
			foreach ($ESNameList as $ES=>$count){
				echo "<td>".$extractResults[$ES]["Contract"]["ContractsPending"]."</td>";
			}
			echo "<td>".$extractResults["ZZZTotal"]["Contract"]["ContractsPending"]."</td>";
		?>
		</tr>
		<tr>
			<td>Contracts Closed</td>
		<?php
			foreach ($ESNameList as $ES=>$count){
				echo "<td>".$extractResults[$ES]["Contract"]["ContractsClosedCount"]."</td>";
			}
			echo "<td>".$extractResults["ZZZTotal"]["Contract"]["ContractsClosedCount"]."</td>";
		?>
		</tr>
		<tr>
			<td>Contracts Closed Amount</td>
		<?php
			foreach ($ESNameList as $ES=>$count){
				echo "<td>$".money($extractResults[$ES]["Contract"]["ContractsClosedAmount"])."</td>";
			}
				echo "<td>$".money($extractResults["ZZZTotal"]["Contract"]["ContractsClosedAmount"])."</td>";
		?>
		</tr>
		<?php //visit information
			ksort($VisitTypesFound);
			foreach ($VisitTypesFound as $visitType=>$count){
				echo "<tr>
						<td>".$visitType."</td>";
						foreach ($ESNameList as $ES=>$count){
							echo "<td>".$extractResults[$ES]["VisitTypes"][$visitType]."</td>";
						}
							echo "<td>".$extractResults["ZZZTotal"]["VisitTypes"][$visitType]."</td>";
				echo "</tr>";
			}
		?>
		<?php //ism information
			ksort($ISMSToInclude);
			foreach ($ISMSToInclude as $bulbType=>$ISMInfo){
				ksort($ISMInfo);
				foreach ($ISMInfo as $ISMName=>$record){
					echo "<tr>
							<td>".$ISMName.($bulbType == "2(non-bulb)" ? " (non-bulb)" : ($bulbType == "1(WIFI)" ? " (WIFI)" : "") )."</td>";
							foreach ($ESNameList as $ES=>$count){
								echo "<td>".$extractResults[$ES]["ISMs"][$ISMName]."</td>";
							}
							echo "<td>".$extractResults["ZZZTotal"]["ISMs"][$ISMName]."</td>";
					echo "</tr>";
				}
			}
			echo "<tr><td>Bulb Total</td>";
				foreach ($ESNameList as $ES=>$count){
					echo "<td>".$bulbTotal[$ES]."</td>";
				}
				echo "<td>".$bulbTotal["ZZZTotal"]."</td>";
			echo "</tr>";
		?>
	</tbody>
<table>


<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		


		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>residential/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=<?php echo $nav;?>#";
		}
		$("#resetFilter").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		
	} );	
</script>
