<?php
	$cellData["Customer Info"]["C5"] = $sharepointRecord["first_x0020_name"];
	$cellData["Customer Info"]["G5"] = $sharepointRecord["last_x0020_name"];
	$cellData["Customer Info"]["C8"] = $sharepointRecord["street_x0020_address"];
	$cellData["Customer Info"]["G8"] = $sharepointRecord["town"];
	$cellData["Customer Info"]["M8"] = $sharepointRecord["zip_x0020_code"];
	$cellData["Customer Info"]["C11"] = $sharepointRecord["phone_x0020_number"];
	$cellData["Customer Info"]["G11"] = $sharepointRecord["customer_x0020_e_x002d_mail"];
	$cellData["Customer Info"]["G14"] = date("m/d/Y",strtotime($sharepointRecord["intakeauditstartdatetime"]));
	$cellData["Customer Info"]["O14"] = ($sharepointRecord["prewxincentiveamount"] ? : 0);
	$cellData["Customer Info"]["K17"] = ($sharepointRecord["whole_x0020_bldg_x0020_incentive"] ? "Yes" : "No");
	$cellData["Customer Info"]["O17"] = ($sharepointRecord["moderate_x0020_income"] ? "Yes" : "No");
	$cellData["Customer Info"]["C20"] = $sharepointRecord["intakesiteinfoelectric"];
	$cellData["Customer Info"]["G20"] = $sharepointRecord["intakesiteinfoelectricaccount"];
	$cellData["Customer Info"]["K20"] = $sharepointRecord["utility"];
	$cellData["Customer Info"]["O20"] = $sharepointRecord["intakesiteinfobgasaccount"];
	$cellData["Customer Info"]["O23"] = $sharepointRecord["intakesiteinfobgaslocationid"];
?>