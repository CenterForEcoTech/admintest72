<?php
ini_set('memory_limit', '7048M');
//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = "";
	include_once($path."_config.php");
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}



$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

if (!$byPassLogin){echo "<h1>EZ-Doc Creator</h1>";}


//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($thispath.$path.'sharepoint/SharePointAPIFiles_ESScheduler.php');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

$ProjectID = $_GET['ProjectID'];
//print_pre($SharepointArray);
$noEventTypes[0] = "HES";
if ($SharepointArray["warning"] == "No data returned."){
	$noEventTypes[0] = "HES";
}else{
	foreach ($SharepointArray as $id=>$record){
		$projectIds[] = $record["project_x0020_id"]."_[".$record["last_x0020_name"]."]_SiteID: ".$record["site_x0020_id"];
		$ES = $record["energy_x0020_specialist"];
		if ($ProjectID == $record["project_x0020_id"]){
			//print_pre($record);
			$sharepointRecord = $record;
			$noEventTypes[0] = false;
		}
	}
}
if (!$byPassLogin){
	include('../residential/_ProjectIDComboBox.php');
}

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$employeeDataResults = $hrEmployeeProvider->get($criteria=null);
$employeeCollection = $employeeDataResults->collection;
foreach ($employeeCollection as $record){
	$ESName = $record->firstName." ".substr($record->lastName,0,1);
	$ESEmail[$ESName] = $record->fullName." <".$record->email.">";
	$ESFullName[$ESName] = $record->fullName;
}

if ($ProjectID){
	include_once('views/_EZDocCreator_CellDataCustomerInfo.php');//standard items	
	
	$cellData["Customer Info"]["C17"] = $sharepointRecord["site_x0020_id"];
	$cellData["Customer Info"]["G17"] = $sharepointRecord["project_x0020_id"];
	$cellData["Customer Info"]["C14"] = $sharepointRecord["intakeaudittype"];
	$cellData["Customer Info"]["K14"] = date("H:i a",strtotime($sharepointRecord["intakeauditstartdatetime"]));
	$cellData["Customer Info"]["C23"] = (strtolower($sharepointRecord["intakesiteinfocustomertype"]) == "tenant" ? "Renter" : $sharepointRecord["intakesiteinfocustomertype"]);
	$cellData["Customer Info"]["O8"] = ($sharepointRecord["summersizzler"] ? "Yes" : "No");
	$cellData["Customer Info"]["K25"] = $sharepointRecord["intakeauditnotes"];
	$cellData["Customer Info"]["C26"] = ($ESFullName[$sharepointRecord["energy_x0020_specialist"]] ? $ESFullName[$sharepointRecord["energy_x0020_specialist"]] : $sharepointRecord["energy_x0020_specialist"]);

		
	$cellData["Building Info"]["C5"] = $sharepointRecord["intakesiteinfoyearbuilt"];
	$cellData["Building Info"]["C17"] = $sharepointRecord["intakesiteinfoheatingfuel"];
	$cellData["Building Info"]["C23"] = $sharepointRecord["intakesiteinfohousetype"];
	$cellData["Building Info"]["G23"] = $sharepointRecord["intakesiteinfoyearbuilt"];
	
	
	//$cellData["Building Info"]["G26"] = $sharepointRecord["totalrentalunits"];
	$cellData["Building Info"]["C26"] = $sharepointRecord["intakesiteinfobuildingcategory"];
	
//print_pre($sharepointRecord);
	$ReportsFile = $sharepointRecord["site_x0020_id"]."_".$ProjectID."_".str_replace("'","",$sharepointRecord["last_x0020_name"])."_EZDoc.xlsm";
	include('model/EZDocCreatorFrom_HESDataFileTemplate.php');
}


$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
?>