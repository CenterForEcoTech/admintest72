<?php
ini_set('memory_limit', '7048M');
//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$EFINewFormatDate = "03/12/2019"; //this is the date EFI want to start having all items and new format effective.
$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "../";
	include_once("../_config.php");
	$thispath = "../";
}
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$auditFieldResults = $residentialProvider->getResAuditFields($criteria=null);
$auditFieldCollection = $auditFieldResults->collection;
//Get list of active warehouse locations
foreach ($auditFieldCollection as $auditField){
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["cell"] = $auditField->cell;
	$auditFieldsBySheet[$auditField->sheet][$auditField->id]["fieldName"] = $auditField->fieldName;
}
$criteria = new stdClass();

/* if (strtotime(date("m/d/Y")) <= strtotime($EFINewFormatDate)){
	$criteria->efiFlagged = true; //for 2019 extract of everything: prior to only use flagged EFI items
} */
$criteria->efiFlagged = false;
$criteria->status = "Current";
$criteria->efiExtractedDate = "0000-00-00";

$ezDocsResults = $residentialProvider->getEZDocs($criteria);
$ezDocsCollection = $ezDocsResults->collection;

//print_pre($ezDocsCollection);
foreach ($ezDocsCollection as $record){
	$documentId = $record->documentId;
	$createdDate = $record->createdDate;
	$ezDocs[$record->documentId] = $record;
}
//print_pre($ezDocs);
if (!$cronJob){
	?>
	<style>
	.ui-datepicker-calendar {display: none;}
	.ui-datepicker-trigger {cursor:pointer;}
	.unmatched {background-color:yellow;border:1pt solid black;}
	</style>
	<h1>HES Data Reviewer (EZ-Doc Review)</h1>
	<?php
	$setTab = "Files";
		$TabCodes = array("Files"=>"Files","Data Review"=>"DataReview");
		foreach ($TabCodes as $TabName=>$TabCode){
			$TabHashes[] = $TabCode;
			$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
		}
		echo '<ul id="admin-tabs" class="tabs">';
				foreach ($TabListItems as $TabItem){
					echo $TabItem;
				}
		echo '</ul>';		
		echo '<div class="tabs-content">';
	?>
		<div id='Files' class='tab-content'>
			<h1>EFI Rebate Extract Data</h1>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>SiteId-ProjectId</th>
						<th>Appt Date</th>
						<th>Cust Name</th>
						<th>EFI Type</th>
					</tr>
				</thead>
				<tbody>

				<?php
}//end if !CronJob
				//added field 'MULTIFAMILY' on 3/5/2018 at request of Suzen O'Coin from efi.org
				//BuildingCategory and ERCW added to this array for headers and fields 2/14/2019
				
			//	if (strtotime(date("m/d/Y")) >= strtotime($EFINewFormatDate)){
					$efiHeader = array("PRIMARY_PROVIDERCODE","ACCTCUST","NAME_FIRST","NAME_LAST","SITEID","ADDRESS","ADDRESS2","CITY","STATE","ZIP","AUDIT_DATE","TYPE","QUANTITY","RMAKE","RSIZE","RSTYLE","TRANSMIT_DATE","EXISTING_FUEL_TYPE","EXISTING_EQUIP_AGE","EXISTING_HEAT_TYPE","OWNER_OCCUPIED","MODERATE_INCOME","DHW_FUEL_GAS","DRYER_FUEL_GAS","LEAD_VENDOR","REFRIG_SAVINGS","MULTI FAMILY","BUILDING_CATEGORY","ERCW");
					$efiFields = array("PRIMARY_PROVIDERCODE","ACCTCUST","NAME_FIRST","NAME_LAST","SITEID","EXT_ADDRESS","ADDRESS2","EXT_CITY","EXT_STATE","EXT_ZIP","ASSESSMENT_DT","TYPE","QUANTITY","RMAKE","RSIZE","RSTYLE","TRANSMIT_DATE","EXISTING_FUEL_TYPE","EXISTING_EQUIP_AGE","EXISTING_HEAT_TYPE","OWNER_OCCUPIED","MODERATE_INCOME","DHW_FUEL_GAS","DRYER_FUEL_GAS","LEAD_VENDOR","REFRIG_SAVINGS","MULTI FAMILY","IntakeSiteInfoBuildingCategory","ERCW");
			/*	}else{
					$efiHeader = array("PRIMARY_PROVIDERCODE","ACCTCUST","NAME_FIRST","NAME_LAST","SITEID","ADDRESS","ADDRESS2","CITY","STATE","ZIP","AUDIT_DATE","TYPE","QUANTITY","RMAKE","RSIZE","RSTYLE","TRANSMIT_DATE","EXISTING_FUEL_TYPE","EXISTING_EQUIP_AGE","EXISTING_HEAT_TYPE","OWNER_OCCUPIED","MODERATE_INCOME","DHW_FUEL_GAS","DRYER_FUEL_GAS","LEAD_VENDOR","REFRIG_SAVINGS","MULTI FAMILY");
					$efiFields = array("PRIMARY_PROVIDERCODE","ACCTCUST","NAME_FIRST","NAME_LAST","SITEID","EXT_ADDRESS","ADDRESS2","EXT_CITY","EXT_STATE","EXT_ZIP","ASSESSMENT_DT","TYPE","QUANTITY","RMAKE","RSIZE","RSTYLE","TRANSMIT_DATE","EXISTING_FUEL_TYPE","EXISTING_EQUIP_AGE","EXISTING_HEAT_TYPE","OWNER_OCCUPIED","MODERATE_INCOME","DHW_FUEL_GAS","DRYER_FUEL_GAS","LEAD_VENDOR","REFRIG_SAVINGS","MULTI FAMILY");
				}	*/			
				
				if (count($ezDocs)){
					$thisFileName = date("Ymd").".csv";
					$CompleteFileNameTxt = $path."residential/EFIExtracts/".$thisFileName;
					$fpCompleted = fopen($CompleteFileNameTxt, 'w');
					fputs($fpCompleted, implode("|",$efiHeader)."\n"); //add header row
				}
				foreach ($ezDocs as $docId=>$record){
					$SiteID = $record->siteId;
					$ProjectID = $record->projectId;
					$allFields = json_decode($record->dataByFieldName);
										
					$apptDate = date("Y-m-d",strtotime($allFields->APPTDATE));
					if ($apptDate == "1969-12-31"){$apptDate = (strpos($allFields->APPTDATE,"/") ? date("Y-m-d",strtotime($allFields->APPTDATE)) : date("Y-m-d",strtotime(exceldatetotimestamp($allFields->APPTDATE))));}

					$clientName = $allFields->NAME_FIRST." ".$allFields->NAME_LAST;
					//print_pre($allFields);
					
					//default fields
					$allFields->QUANTITY = 1;
					$allFields->TRANSMIT_DATE = date("m/d/Y");
					$allFields->OWNER_OCCUPIED = ($allFields->OWNER_OCCUPIED == "Owner" ? "Y" : "N");
					$allFields->MODERATE_INCOME = ($allFields->MODERATE_INCOME == "Yes" ? "Y" : "N");
					$allFields->LEAD_VENDOR = "CET";
					$allFields->STATE = "MA";
					$allFields->EXISTING_FUEL_TYPE = "Natural Gas";
					//fields that change
					$allFields->ACCTCUST_ORG = $allFields->ACCTCUST;
					
					$efiType["ERCAC"] = $allFields->EFI_TYPE_AC;
					$efiType["HEATING"] = $allFields->EFI_TYPE_HEATING;
					$efiType["FRIDGE"] = $allFields->EFI_TYPE_FRIDGE;
					$efiType["ERCW"] = $allFields->EFI_TYPE_WASHER; //
					
					if (strtotime(date("m/d/Y")) >= strtotime($EFINewFormatDate)){
						$efiTypes = array(1); // this is an override so that every available record is pulled, not just ones with EFI flagged items
					}else{
						$efiTypes = array(); //use this if only efi flagged items should be used.
					} 
					//$efiTypes = array(1);
					
					
					foreach ($efiType as $typeName => $typeValue){
						if (trim($typeValue) != ""){
							if ($typeName == "HEATING"){
								if(strpos(strtolower($typeValue),"furnace")){
									$typeName = "EFR";
								}else{
									$typeName = "EBR";
								}
							}
							$efiTypes[] = $typeName;
						}
					}
					//print_pre($allFields);
					
					
					
					foreach ($efiTypes as $typeName){
						$allFields->TYPE = $typeName;
							switch ($typeName){
								case "EBR":
									$allFields->EXISTING_EQUIP_AGE = "30+";
									break;
								case "EFR":
									$allFields->EXISTING_EQUIP_AGE = "12+";
									break;
								case "ERCAC":
									$allFields->EXISTING_EQUIP_AGE = "12-20";
									break;
								case "FRIDGE":
									$allFields->EXISTING_EQUIP_AGE = "12-20";
									break;
								case "ERCW": 
									$allFields->EXISTING_EQUIP_AGE = "12-20";
									$allFields->ERCW = "Y";
									break;
							}
						
						if ($typeName == "ERCAC" || $typeName == "FRIDGE" || $typeName == "ERCW"){
							$allFields->PRIMARY_PROVIDERCODE = $allFields->PROVIDERCODE2;
							$allFields->ACCTCUST = $allFields->MDW_ACT_ID;
							if ($typeName == "ERCW"){
								$allFields->DHW_FUEL_GAS = ucwords((trim($allFields->DHW_FUEL_GAS) ? : $allFields->DHWFUEL));
								
								//Lisa Kohler determined it was fine to have a default value of natural gas if left blank, but best to make this a required field
								if (!$allFields->DHW_FUEL_GAS){
									$allFields->DHW_FUEL_GAS = "Natural Gas";
									//$requiredFieldsMissing[$docId][] = $clientName." missing DHW_FUEL_GAS";
								}
								if (!$allFields->DRYER_FUEL_GAS){
									$allFields->DRYER_FUEL_GAS = "Natural Gas";
									//$requiredFieldsMissing[$docId][] = $clientName." missing DRYER_FUEL_GAS";
								}
								
								if (!$allFields->DHW_FUEL_GAS){
									$errors[] = $clientName." missing DHW_FUEL_GAS";
								}
								if (!$allFields->DRYER_FUEL_GAS){
									$errors[] = $clientName." missing DRYER_FUEL_GAS";
								}

							}
						}else{
							$allFields->PRIMARY_PROVIDERCODE = "BERKSHIR";
							$allFields->ACCTCUST = $allFields->ACCTCUST_ORG;
							
							
						}
						//print_pre($allFields);
						//standardize providercode
						if (trim(strtolower($allFields->PRIMARY_PROVIDERCODE)) == "eversource"){
							$allFields->PRIMARY_PROVIDERCODE = "WMECO";
						}
						if (strpos(strtolower($allFields->PRIMARY_PROVIDERCODE),"grid")){
							$allFields->PRIMARY_PROVIDERCODE = "NGRID";
						}
						 //remove dashes
						$allFields->ACCTCUST = str_replace("-","",$allFields->ACCTCUST);
						//standardize Building Type
						$allFields->IntakeSiteInfoBuildingCategory = str_replace("Low-Rise","Low_Rise",$allFields->IntakeSiteInfoBuildingCategory);
						$allFields->IntakeSiteInfoBuildingCategory = str_replace("High-Rise","High_Rise",$allFields->IntakeSiteInfoBuildingCategory);
						$allFields->IntakeSiteInfoBuildingCategory = str_replace("SF Detached","SF_Detached",$allFields->IntakeSiteInfoBuildingCategory);
						
						//If Building Type is blank, defaults to SF Detached
						if (strlen($allFields->IntakeSiteInfoBuildingCategory) < 1){
							$allFields->IntakeSiteInfoBuildingCategory = "SF_Detached";
						}
						
						//If ERCW is blank, defaults to N
						if (strlen($allFields->ERCW) < 1){
							$allFields->ERCW = "N";
						}
						
						//$allFields->IntakeSiteInfoBuildingCategory = str_replace("","SF_Detached",$allFields->IntakeSiteInfoBuildingCategory);
						//make sure zipcodes are only 5 digits
						if (strlen($allFields->EXT_ZIP) > 5){
							$errors[] = "zip code error: ".$clientName." ".$SiteID." ".$ProjectID." has more than five characters: ".$allFields->EXT_ZIP;
						}
						
						
						//print_pre($allFields);
						
						$thisRow = array();
						foreach ($efiFields as $fieldName){
							$thisValue = $allFields->$fieldName;
							if ($fieldName == "ASSESSMENT_DT"){
								$thisValue = date("m/d/Y",strtotime($apptDate));
							}
							$thisRow[] = $thisValue;
						}
						if (!count($requiredFieldsMissing)){
							$docIds[] = $docId;

							if (!$cronJob){
								echo "<tr>
										<td>".$SiteID." - ".$ProjectID."</td>
										<td>".date("m/d/Y",strtotime($apptDate))."</td>
										<td>".$clientName."</td>
										<td>".$typeName."</td>";
										
								echo "</tr>";
							}
							if (count($ezDocs)){
								fputs($fpCompleted, implode("|",$thisRow)."\n");
								//fputcsv($fpCompleted, $thisRow, "|");
							}
						}else{
							$allFieldsByDocId[$docId] = $allFields;
						}
					}
					
				}

				if (count($ezDocs)){
					fclose($fpCompleted);
				}
if (!$cronJob){				
			?>
			</tbody>
		
		</table>
		<?php
}//end if not cronjob		
			if (count($ezDocs)){
			$fileName = $CompleteFileNameTxt;
				if ($cronJob){
					if (count($errors)){
						echo "Errors Detected and file not sent\r\n";
						print_r($errors);
					}else{
						echo " marked ".count($docIds)." docIds as EFI Extracted<br>";
						$cronJobDocIds = "'".implode("','",$docIds)."'";
						$markEFIExtractDate = $residentialProvider->markEFIExtractDate($cronJobDocIds);
						//print_pre($markEFIExtractDate);
						$fileToUpload = $fileName;
						echo "Will upload: ".$fileToUpload."<bR>";
					}
				}else{
					if (count($errors)){
						echo "Errors were detected that must be fixed before extract file uploaded:";
						print_pre($errors);
					}else{
				?>
						<br><a href="<?php echo $CurrentServer.$adminFolder."residential/EFIExtracts/".$thisFileName;?>"><?php echo $thisFileName;?></a>
						<Br><a href="#" class="button-link markRecords">Mark Records as Exported</a>
				<?php	
					}
				}
			}else{ echo "No Extract To Create";}
		?>
<?php if (!$cronJob){?>
	</div>
	<div id='DataReview' class='tab-content'>
		<h1>Review Data</h1>
	</div>

</div>
<script type="text/javascript">
	$(function () {
		$(".markRecords").on('click',function(e){
			e.preventDefault();
			var docIds = "'<?php echo implode("','",$docIds);?>'";
			console.log(docIds);
			var	data = {};
				
				data["action"] = "markEFIExtractDate";
				data["docIds"] = docIds;
				$.ajax({
					url: "ApiResidentialManagement.php",
					type: "POST",
					data: JSON.stringify(data),
					success: function(resultdata){
						console.log(resultdata);
							window.location.href = '?nav=EFIExtractor';
//							$this.hide();
//							$(thisSpanId).show();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
			"scrollY": "600px",
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   true,
			"ordering": true,
			"scrollCollapse": true,
			"info":     false
		});
	
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
<?php
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
}//end if not cronjob
?>