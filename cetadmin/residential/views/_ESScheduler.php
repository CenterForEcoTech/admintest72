<?php
ini_set('memory_limit', '7048M'); //'7048M'
//phpinfo();
//error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
$TomorrowsDate = date("Y-m-d",strtotime($TodaysDate." + 1 day"));
$TestDate = date("Y-m-d",strtotime($TodaysDate." - 1 week"));
$oneWeekLater = date("Y-m-d",strtotime($TomorrowsDate." + 1 week"));
$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	set_include_path(".:/home/cetdash/pear/share/pear");
	$thispath = "";
	include_once($path."_config.php");
}else{
	$testingServer = true;
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes  12000000


include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");
$muniProvider = new MuniProvider($dataConn);
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);

//GetAlready Scheduled Data to maintain Confirmed Status
$criteria = new stdClass();
$criteria->status = "Confirmed";
$criteria->apptDate = date("Y-m-d",strtotime($TodaysDate." -1 day"));
$scheduledResults = $residentialProvider->getScheduledData($criteria);
$noEventTypes[3] = "MMWEC";
foreach ($scheduledResults->collection as $record){
	$dateOfAppt = date("Y-m-d",strtotime($record->apptDate));
	$projectId = $record->projectId;
	$confirmedAppts[$projectId][$dateOfAppt] = 1;
	$visitType = $record->visitType;
	 //workaround to get scheduled data instead of live data from Salesforce when the cronjob runs
	if ($creatingCRIScheduleFile){
		if (strpos(strtolower(" ".$visitType),"mmwec")){
			$thisRecord = new stdClass();
			$thisRecord->auditor = $record->energySpecialist;
			
			$ES = $record->energySpecialist;
			$thisStartDateTime = $record->apptDate;
			$StartDate = date("Y-m-d",strtotime($thisStartDateTime));
			$StartDateStr = strtotime(date("Y-m-d",strtotime($thisStartDateTime)));
			$StartDates[strtotime($StartDate)] = $StartDate;
			
			$thisRecord->accountName = $record->customerName;
			$thisRecord->email = $record->email;
			$thisRecord->addressFull = $record->address;
			$thisRecord->id = str_replace("SalesforceID","",$record->siteId);
			$thisRecord->startDateTime = $record->apptDate; // 2018-06-29T16:00:00.000Z
			$thisRecord->endDateTime = $record->endDate; //2018-06-29T18:00:00.000Z
			$thisRecord->phone = $record->phone;
			$thisRecord->auditNotes = $record->auditNotes;
			$thisRecord->MMWECType = $visitType;
			$thisRecord->energyCompany = $record->energyCompany;
			$electricProvider = $newRecord->electricProvider;
			
			$NotesForEnergyAudit = $MMWECEnergyCompanyByName[$electricProvider]->Notes_For_Energy_Audit__c;
			$WhatToExpect = $MMWECEnergyCompanyByName[$electricProvider]->What_to_Expect__c;
			//$NotesForEnergyAudit = $electricProvider->Notes_For_Energy_Audit__c;
			//$WhatToExpect = $electricProvider->What_to_Expect__c;
			
/*			
			$thisRecord->city = $record->Account->BillingAddress->city;
			$thisRecord->state = $record->Account->BillingAddress->state;
			$thisRecord->zipcode = $record->Account->BillingAddress->postalCode;
			$thisRecord->MMWECID = $record->MMWEC_ID__c; //MMWEC-180230
			$thisRecord->MMWECType = $record->MMWEC_Type__c;
			$thisRecord->opportunityName = $record->Name;
			$thisRecord->stageName = $record->StageName;
			$thisRecord->visitDetails = $record->Visit_details__c;
			$thisRecord->energyCompany = $MMWECEnergyCompany[$record->MMWEC_Energy_Company__c]->Name;
*/			
			$thisRecord->dataSource = "MMWEC";
	
			$thisRecord = (array)$thisRecord;
			
			
			
			
			$scheduledEvents["AllData_".$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
			
			if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
				$scheduledEvents[$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
				$noEventTypes[3] = false;
				//echo "test".$ES." ".date("l, m/d/Y",strtotime($StartDate))." < ".$oneWeekLater." >= ".$TomorrowsDate."<br>";
				//echo $thisStartDateTime."=".strtotime($thisStartDateTime)." = ".date("m/d/Y H:ia",strtotime($thisStartDateTime))."<br>";
			}
			
			
			
		}
	}
}
//Get the cancelled items and remove them first
include_once($thispath.$path.'sharepoint/SharePointAPIFiles_IntakeCancelled.php');
if ($SharepointArray["warning"] != "No data returned."){
	foreach ($SharepointArray as $id=>$record){
		$siteId = $record["site_x0020_id"];
		$projectId = $record["project_x0020_id"];
		$cancelled[] = array("siteId"=>$siteId,"projectId"=>$projectId,"customerName"=>$record["last_x0020_name"],"heaDate"=>$record["hea_x0020_date"]);
	}
}

if (!$creatingCRIScheduleFile){
	//Get Salesforce Data
	require_once '../gbs/salesforce/config.php';
	include_once('../gbs/salesforce/soap_connect.php');

	//get MMWEC Opportunity that are Cancelled Info
	$query = "SELECT Id, Name, Account.Name, Account.Phone, Account.Account_E_mail__c, Account.BillingAddress, MMWEC_Auditor__c, MMWEC_ID__c, MMWEC_Type__c, StageName,Audit_Date_Start__c, Audit_Date_Stop__c, Audit_Notes__c, Visit_Details__c FROM Opportunity WHERE RecordTypeId='0120P000000NI35QAG' AND (StageName='Cancelled' OR StageName='Audit Needs to be Scheduled')";
	//echo $query;
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	//print_pre($response);
	if (count($response->records)){
		foreach ($response->records as $record){
			$siteId = "SalesforceID".$record->Id;
			$projectId = "SalesforceID".$record->Id;
			$cancelled[] = array("siteId"=>$siteId,"projectId"=>$projectId);
		}
	}//end if records
	//print_pre($cancelled);
}

//mark them as cancelled
if (count ($cancelled)){
	foreach ($cancelled as $cancelledData){
		$criteria = new stdClass();
		$criteria->siteId = $cancelledData["siteId"];
		$criteria->projectId = $cancelledData["projectId"];
		$cancelled = $residentialProvider->cancelSharePointApt($criteria);
	}
}
//end removing cancelled items


$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;
//print_pre($allData["S00050203741"]);

if (!$creatingCRIScheduleFile){
	echo "<div id='ESSchedulerDataDisplay'><h1>ES Scheduler</h1>";
}

$SharepointArray = array();
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($path.'sharepoint/SharePointAPIFiles_ESScheduler.php');

//print_pre($SharepointArray);
$noEventTypes[0] = "HES";
//print_pre($SharepointArray);
if ($SharepointArray["warning"] == "No data returned."){
	$noEventTypes[0] = "HES";
}else{
	//print_pre($SharepointArray);
	foreach ($SharepointArray as $id=>$record){
		$esParts = explode(" ",$record["energy_x0020_specialist"]);
		$ES = $esParts[0]." ".substr($esParts[1],0,1);
		if (trim($ES) == ""){$ES = "Kacie D";}
		$StartDate = date("Y-m-d",strtotime($record["intakeauditstartdatetime"]));
		$StartDateStr = strtotime($StartDate);
		$StartDates[strtotime($StartDate)] = $StartDate;
		$record["dataSource"] = "HES";
		$scheduledEvents["AllData_".$ES][$StartDateStr][strtotime($record["intakeauditstartdatetime"])][] = $record;
		if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
			$scheduledEvents[$ES][$StartDateStr][strtotime($record["intakeauditstartdatetime"])][] = $record;
			$noEventTypes[0] = false;
		}
	}
}
//get WGE audit data
$criteria = new stdClass();
$criteria->auditScheduledFuture = true;
$criteria->orderBy = "auditor, auditScheduledDate";
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
$auditFields = array("auditFloorArea","auditHouseWidth","auditHouseLength","auditConditionedStories","auditHeatingEquipmentType","auditHeatingFuel","notes");
$CustomerIDWithAuditInfo = array();
$noEventTypes[1] = "WGE";
//print_pre($customerCollection);
if (count($customerCollection)){
	foreach ($customerCollection as $result=>$record){
		$ESFullName = $record->auditor;
		$ESNameParts = explode(" ",$ESFullName);
		$ES = $ESNameParts[0]." ".substr($ESNameParts[1],0,1);
		$startDate = $record->auditScheduledDate;
		$StartDate = $record->auditScheduledDate;
		$StartDateStr = strtotime($StartDate);
		$startTime = $record->auditScheduledTime;
		$StartingDateTime = strtotime(trim($startDate." ".$startTime));
		
		$CustomerByID[$record->id] = $record;
		foreach ($auditFields as $auditField){
			if ($record->{$auditField}){
				$CustomerIDAuditInfo[$record->id][str_replace("audit","",$auditField)] = $record->{$auditField};
			}
		}
		
		$record = (array)$record;
		$record["dataSource"] = "WGE";
		$scheduledEvents["AllData_".$ES][$StartDateStr][$StartingDateTime][] = $record;
		if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
			$scheduledEvents[$ES][$StartDateStr][$StartingDateTime][] = $record;
			$noEventTypes[1] = false;
		}
	}
}

if (!$creatingCRIScheduleFile){
	//get the auditors from Salesforce
	$query = "SELECT Id, Name, Email__c, Phone__c, Availability_Notes__c, Agency__c FROM MMWEC_Auditor__c WHERE Active__c=true";
	//echo $query;
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	//print_pre($response);
	foreach ($response->records as $record){
		$MMWECAuditor[$record->Id] = $record;
		$NameParts = explode(" ",$record->Name);
		
		$employeeInfo = new stdClass();
		//check to see if already a CET employeeInfo
		$notCETEmpolyee = true;
		foreach ($EmployeeByName as $id=>$eRecord){
			if (trim(strtolower($eRecord->fullName)) == trim(strtolower($record->Name))){$notCETEmpolyee = false;}
		}
		if ($notCETEmpolyee){
			$employeeInfo->id = $record->Id;
			$employeeInfo->firstName = $NameParts[0];
			$employeeInfo->lastName = $NameParts[1];
			$employeeInfo->fullName = $record->Name;
			$employeeInfo->email = $record->Email__c;
			$employeeInfo->phone = $record->Phone__c;
			$employeeInfo->availabilityNotes = $record->Availability_Notes__c;
			$employeeInfo->agency = $record->Agency__c;
			$EmployeeByName[$record->Id] = $employeeInfo;
		}
		
		
	}
	
	//get the Energy company from Salesforce
	$query = "SELECT Id, Name, Notes_For_Energy_Audit__c, What_to_Expect__c FROM MMWEC_Energy_Company__c";
	//echo $query;
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	//print_pre($response);
	foreach ($response->records as $record){
		$MMWECEnergyCompany[$record->Id] = $record;
		
	}
	$NotesForEnergyAudit = $record->Notes_For_Energy_Audit__c;
	$WhatToExpect = $record->What_to_Expect__c;
	//print_pre ($NotesForEnergyAudit);
	//echo "<br>Test Notes: ".$NotesForEnergyAudit."<br> Test Expect".$WhatToExpect;
	//print_pre($EmployeeByName);
	//var_dump $record[$electricProvider]; //spins forever

	//get MMWEC Opportunity Info

	$query = "SELECT Id, Name, Account.Name, Account.Phone, Account.Account_E_mail__c, Account.BillingAddress, MMWEC_Auditor__c, MMWEC_ID__c, MMWEC_Type__c, StageName,Audit_Date_Start__c, Audit_Date_Stop__c, Audit_Notes__c, Visit_Details__c,MMWEC_Energy_Company__c FROM Opportunity WHERE RecordTypeId='0120P000000NI35QAG' AND StageName='Audit Scheduled'";
	//echo $query;
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	//print_pre($response);
	if (!count($response->records)){
			$noEventTypes[3] = "MMWEC";
	}else{
		foreach ($response->records as $record){
			//print_pre($record);
			$thisRecord = new stdClass();
			$thisRecord->auditor = $MMWECAuditor[$record->MMWEC_Auditor__c]->Name;

			$esParts = explode(" ",$thisRecord->auditor);
			$ES = $esParts[0]." ".substr($esParts[1],0,1);
			if (trim($ES) == ""){$ES = "Kacie D";}
			
			$thisRecord->id = $record->Id;
			$thisRecord->accountName = $record->Account->Name;
			$thisRecord->email = $record->Account->Account_E_mail__c;
			$thisRecord->phone = $record->Account->Phone;
			$thisRecord->address = $record->Account->BillingAddress->street;
			$thisRecord->city = $record->Account->BillingAddress->city;
			$thisRecord->state = $record->Account->BillingAddress->state;
			$thisRecord->zipcode = $record->Account->BillingAddress->postalCode;
			$thisRecord->startDateTime = $record->Audit_Date_Start__c; // 2018-06-29T16:00:00.000Z
			$thisRecord->endDateTime = $record->Audit_Date_Stop__c; //2018-06-29T18:00:00.000Z
			$thisRecord->auditNotes = $record->Audit_Notes__c;
			$thisRecord->MMWECID = $record->MMWEC_ID__c; //MMWEC-180230
			$thisRecord->MMWECType = $record->MMWEC_Type__c;
			$thisRecord->opportunityName = $record->Name;
			$thisRecord->stageName = $record->StageName;
			$thisRecord->visitDetails = $record->Visit_details__c;
			$thisRecord->energyCompany = $MMWECEnergyCompany[$record->MMWEC_Energy_Company__c]->Name;
			$thisRecord->NotesForEnergyAudit = $MMWECEnergyCompanyByName[$electricProvider]->Notes_For_Energy_Audit__c; //added by Lauren
			$thisRecord->WhatToExpect = $MMWECEnergyCompanyByName[$electricProvider]->What_to_Expect__c; //added by Lauren
			$thisStartDateTime = $thisRecord->startDateTime;
			$StartDate = date("Y-m-d",strtotime($thisStartDateTime));
			$StartDateStr = strtotime($StartDate);
			$StartDates[strtotime($StartDate)] = $StartDate;
			$thisRecord->dataSource = "MMWEC";
			$thisRecord = (array)$thisRecord;
			$scheduledEvents["AllData_".$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
			
			
			if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
				$scheduledEvents[$ES][$StartDateStr][strtotime($thisStartDateTime)][] = $thisRecord;
				$noEventTypes[3] = false;
				//echo $ES." ".date("l, m/d/Y",strtotime($StartDate))." < ".$oneWeekLater." >= ".$TomorrowsDate."<br>";
				//echo $thisStartDateTime."=".strtotime($thisStartDateTime)." = ".date("m/d/Y H:ia",strtotime($thisStartDateTime))."<br>";
			}
		}
	}//end if records
	//print_pre($MMWECScheduledAudits);
	//print_pre($scheduledEvents);
}else{
	//work around for pulling from residential_schedule table instead
	
}//end if not create scheduler

//get blockedSchedules
$criteria = new stdClass();
$criteria->future = true;
$criteria->status = "active";
$criteria->orderBy = " ORDER BY ResidentialScheduledBlocked_ES, ResidentialScheduledBlocked_StartTimeStamp";
$blockedScheduleResults = $residentialProvider->getScheduleBlocked($criteria);
$blockedCollection = $blockedScheduleResults->collection;
//print_pre($customerCollection);
if (count($blockedCollection)){
	foreach ($blockedCollection as $result=>$record){
		$ESFullName = $record->auditor;
		$ESNameParts = explode(" ",$ESFullName);
		$esParts = explode(" ",$record->ResidentialScheduledBlocked_ES);
		$ES = $esParts[0]." ".substr($esParts[1],0,1);
		$StartingDateTime = strtotime($record->ResidentialScheduledBlocked_StartTimeStamp);
		$StartDate = date("Y-m-d",$StartingDateTime);
		$StartDateStr = strtotime($StartDate);
		
		$record = (array)$record;
		$record["dataSource"] = "BLOCKED";
		$scheduledEvents["AllData_".$ES][$StartDateStr][$StartingDateTime][] = $record;
		if (strtotime($StartDate) < strtotime($oneWeekLater) && strtotime($StartDate) >= strtotime($TomorrowsDate)){
			$scheduledEvents[$ES][$StartDateStr][$StartingDateTime][] = $record;
		}
	}
}




//print_pre($scheduledEvents["CJ H"]);
//print_pre("test".$scheduledEvents["Ben C"]);
include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->noLimit = true;
$employeeDataResults = $hrEmployeeProvider->get($criteria);
$employeeCollection = $employeeDataResults->collection;
foreach ($employeeCollection as $record){
	$ESName = $record->firstName." ".substr($record->lastName,0,1);
	$ESEmail[$ESName] = $record->fullName." <".$record->email.">";
}

$spacer = "-----------------------------------------------------<br>";
$border = "=====================================================<br>";

if (count($scheduledEvents)){
	
	//echo("test".$scheduledEvents["Alec P"]);
	//print_pre("test".$scheduledEvents["Ben C"]);
	//print_pre("test".$scheduledEvents["Kacie D"]["Renter"]);
	$thisSchedule = array();
	foreach ($scheduledEvents as $ES=>$StartDateRecord){
		//print_pre($StartDateRecord);
		ksort($StartDateRecord);
		$AllData = false;
		if (strpos(" ".$ES,"AllData")){
			$AllData = true;
		}
		//echo "<h3>".$ES."</h3>";
		foreach ($StartDateRecord as $StartDateStr=>$StrToTimeStartDate){
			$StartDate = date("Y-m-d",$StartDateStr);
			//echo "<hr>".$ES." ".date("l, m/d/Y",strtotime($StartDate))."<hr>";
			ksort($StrToTimeStartDate);
			//print_pre($StrToTimeStartDate);
			$priorAddress = "";
			foreach ($StrToTimeStartDate as $strtoTimeStartDate=>$records){
				foreach ($records as $record){
					
					//print_pre($record);
					$thisDisplay = "";
					if ($record["dataSource"] == "HES"){
						$Address = trim(str_replace("Upper Rear","",$record["street_x0020_address"])).", ".$record["town"]." MA ".$record["zip_x0020_code"];
						$startTimeStr = strtotime($record["intakeauditstartdatetime"]);						
						if (!$AllData){
							//CRIFields
							$record["cri_SchedDate"] = date("m/d/Y",$startTimeStr);
							$record["cri_ApptStart"] = date("g:i A",$startTimeStr);
							$record["cri_ProgramType"] = "Mass Save SF";
							$record["cri_Vendor1"] = "CET";
							$record["cri_State"] = "MA";
							//print_pre("Audit Type: ".$record["intakeaudittype"]);
							if ($record["intakeaudittype"] == "HEA" || $record["intakeaudittype"] == "Hybrid Visit" || $record["intakeaudittype"] == "HEAAU" || $record["intakeaudittype"] == "Renter" || $record["intakeaudittype"] == "Landlord" || $record["intakeaudittype"] == "Return Visit"|| $record["intakeaudittype"] == "Inspection"|| $record["intakeaudittype"] == "Virtual SHV" || $record["intakeaudittype"] == "Virtual HEA" || $record["intakeaudittype"] == "Post Virtual HEA"){
								$thisWeeklySchedule[$startTimeStr][$ES][] =$record;
							}

							$auditType = $record["intakeaudittype"];
							$thisDisplay = str_replace("<br>","",$spacer).$spacer
								.date("h:i a",strtotime($record["intakeauditstartdatetime"]))." - ".date("h:i a",strtotime($record["intakeauditenddatetime"]))."<br>"
								.$record["intakeaudittype"]."<br>"
								.$record["site_x0020_id"].(strtoupper($auditType) !="WIFI" ? " <a href='https://cetdashboard.info//admintest/cetadmin/residential/EZDocCreator.php?ProjectID=".$record["project_x0020_id"]."' target='".$record["project_x0020_id"]."'>Pre-Filled EZ-Doc</a><br>" : "<br>")
								."Appointment Contact Information:<br>"
								."<b>".$record["first_x0020_name"]." ".$record["last_x0020_name"]."<br>"
								.$record["street_x0020_address"]."<br>"
								.$record["town"].", MA ".$record["zip_x0020_code"]."</b><br>"
								."<a href='tel:".$record["phone_x0020_number"]."'>".$record["phone_x0020_number"]."</a><Br>"
								.$spacer;
							if ($priorAddress){
								$distanceFromPriorAddress = getDistance($priorAddress, $Address, "M");
								$milage = (strpos($distanceFromPriorAddress,"mi") ? trim(str_replace(" mi","",$distanceFromPriorAddress)) : 0);
								$thisDisplay .= "Distance from Prior Apt (".$priorAddress.") to ".$Address."<Br>";
								$thisDisplay .= ($milage > 30 ? "<span style='color:red;font-weight:bold;'>" : "")."Distance from Prior Apt: ".$distanceFromPriorAddress.($milage > 30 ? "</span>" : "")."<br>";
								/*If ($milage > 30){
									$alerts[] = $ES." on ".date("l, m/d/Y",strtotime($StartDate))." has travel distance of ".$distanceFromPriorAddress." between ".$priorAddress." and ".$Address;
								}*/
							}
							$priorAddress = $Address;
							$thisDisplay .= "For map and/or driving directions, click on the following link, or paste into a browser:<br>"
								."<b><a href='http://maps.google.com/maps?q=".urlencode($Address)."' target='_Blank'>".$Address."</a></b><br>"
								."<br>Intake Note: ".$record["intakeauditnotes"]."<br>"
								//Customer Notes: ".$record["customer_x0020_notes"]."<br>"
								."<br>Electric account:<br>"
								.$record["intakesiteinfoelectric"]."<br>"
								."Customer Account#: ".$record["intakesiteinfoelectricaccount"]."<br>"
								."<br>"
								."Gas account:<br>"
								.$record["intakesiteinfoauditsponsor"]."<br>"
								."Customer Account#: ".$record["intakesiteinfobgasaccount"]."<br>"
								."Service Account#: ".$record["intakesiteinfobgaslocationid"]."<br><br><br>";
						}else{
							$ESParts = explode("_",$ES);
							$scheduledData = new stdClass();
							$scheduledData->energySpecialist = $ESParts[1];
							$scheduledData->apptDate = date("Y-m-d H:i:s",strtotime($record["intakeauditstartdatetime"]));
							$scheduledData->endDate = date("Y-m-d H:i:s",strtotime($record["intakeauditenddatetime"]));
							$scheduledData->siteId = $record["site_x0020_id"];
							$scheduledData->projectId = $record["project_x0020_id"];
							$scheduledData->hesId = $record["id"];
							$scheduledData->electricProvider = $record["intakesiteinfoelectric"];

							$scheduledData->customerName = $record["first_x0020_name"]." ".$record["last_x0020_name"];
							$scheduledData->email = $record["customer_x0020_e_x002d_mail"];
							$scheduledData->address = $Address;
							$scheduledData->visitType = $record["intakeaudittype"];
							$scheduledData->status = ($confirmedAppts[$record["project_x0020_id"]][date("Y-m-d",strtotime($record["intakeauditstartdatetime"]))] ? "Confirmed" : "");
							$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
						}
						//echo "<br>Lauren: <br> Data source: ".$dataSource."  "." Visit Type: ".$visitType.$ES." ".date("l, m/d/Y",strtotime($StartDate))."=".$thisDisplay."<hr>";
						//echo $MMWECEnergyCompanyByName[$electricProvider]->What_to_Expect__c."<br>";
						//$NotesForEnergyAudit = $MMWECEnergyCompanyByName[$electricProvider]->Notes_For_Energy_Audit__c;
						//$NotesForEnergyAudit = $electricProvider->Notes_For_Energy_Audit__c;
						//$WhatToExpect = $electricProvider->What_to_Expect__c;
						//echo "<hr>YOSH: "." Data source: ".$record["dataSource"]."  ".$ES." ".date("l, m/d/Y",strtotime($StartDate))."=".$thisDisplay."<hr>";
						//print_pre($insertScheduleResults);
						
					}
					if ($record["dataSource"] == "WGE"){
						$Address = trim($record["installedAddress"]).", ".$record["installedCity"]." MA ".$record["installedZip"];
						if (!$AllData){
							$thisDisplay = str_replace("<br>","",$spacer).$spacer;
							if ($record["auditScheduledTime"]){
								$thisDisplay .= date("h:i a",strtotime($record["auditScheduledTime"]))." - ".date("h:i a",strtotime($record["auditScheduledTime"]." +1 hour"))."<br>";
							}else{
								$thisDisplay .= "<b>NO TIME SCHEDULED IN SYSTEM YET: Review Personal Calendar</b><br>";
							}
							$thisDisplay .= $record["dataSource"]."<br>"
								."Appointment Contact Information:<br>"
								."<b>".$record["accountHolderFullName"]."<br>"
								.$record["installedAddress"]."<br>"
								.$record["installedCity"].", MA ".$record["installedZip"]."</b><br>"
								.$record["customerNotes"]."<br>"
								."<a href='tel:".$record["installedPhone"]."'>".$record["installedPhone"]."</a><br>"."<a href='mailto:".$record["installedEmail"]."'>".$record["installedEmail"]."</a><br>"
								
								//."<a href='tel:".$record["installedPhone"]."'>".$record["installedPhone"]."</a><br>"."<a href='mailto:".$record["customer_x0020_e_x002d_mail"]."'>".$record["customer_x0020_e_x002d_mail"]."</a><br>"
								.$spacer;
								
							if ($priorAddress){
								$distanceFromPriorAddress = getDistance($priorAddress, $Address, "M");
								$milage = (strpos($distanceFromPriorAddress,"mi") ? trim(str_replace(" mi","",$distanceFromPriorAddress)) : 0);
								$thisDisplay .= "Distance from Prior Apt (".$priorAddress.") to ".$Address."<Br>";
								$thisDisplay .= ($milage > 20 ? "<span style='color:red;font-weight:bold;'>" : "")."Distance from Prior Apt: ".$distanceFromPriorAddress.($milage > 20 ? "</span>" : "")."<br>";
							/*	If ($milage > 20){
									$alerts[] = $ES." on ".date("l, m/d/Y",strtotime($StartDate))." has travel distance of ".$distanceFromPriorAddress." between ".$priorAddress." and ".$Address;
								} */
							}
							$priorAddress = $Address;
							
							$thisDisplay .= "For map and/or driving directions, click on the following link, or paste into a browser:<br>"
								."<b><a href='http://maps.google.com/maps?q=".urlencode($Address)."' target='_Blank'>".$Address."</a></b><br>"
								."For Audit Data Entry use the following link:<br>"
								."<a href='https://CETDashboard.info/admintest/cetstaff/?linkPage=Muni&CustomerID=".$record["id"]."' target='_Blank'>https://CETDashboard.info/admintest/cetstaff/?linkPage=Muni&CustomerID=".$record["id"]."</a><br>"
								."<br>";
							if (count($CustomerIDAuditInfo[$record["id"]])){
								$thisDisplay .="<br>Audit Intake Info:<br>";
								
								foreach ($CustomerIDAuditInfo[$record["id"]] as $field=>$value){
									$thisDisplay .= $field.": ".$value."<br>";
									
								}
							}
							
							//insert Into SchedulerData
						}else{
							//print_pre($record);
							$ESParts = explode("_",$ES);
							$scheduledData = new stdClass();
							$scheduledData->energySpecialist = $ESParts[1];
							$scheduledData->apptDate = date("Y-m-d H:i:s",strtotime($record["auditScheduledDate"]." ".$record["auditScheduledTime"]));
							$scheduledData->endDate = date("Y-m-d H:i:s",strtotime($record["auditScheduledDate"]." ".$record["auditScheduledTime"]." +1 hour"));
							$scheduledData->siteId = "CustomerID".$record["customerId"];
							$scheduledData->projectId = "CustomerID".$record["customerId"];
							$scheduledData->electricProvider = "WGE";
							$scheduledData->notes = $record["notes"];

							$scheduledData->customerName = $record["accountHolderFullName"];
							$scheduledData->email = $record["installedEmail"];
							$scheduledData->address = $Address;
							$scheduledData->visitType = "WGE";
							$scheduledData->status = ($confirmedAppts["CustomerID".$record["customerId"]][date("Y-m-d",strtotime($record["auditScheduledDate"]." ".$record["auditScheduledTime"]))] ? "Confirmed" : "");
							//print_pre($scheduledData);
							$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
						}
						
					}

					if ($record["dataSource"] == "MMWEC"){
						$useFullAddress = false;
						if ($record["addressFull"]){
							$Address = $record["addressFull"];
							$useFullAddress = true;
							$visitDetails  = $record["auditNotes"];
						}else{
							$visitDetails  = $record["visitDetails"];
							$Address = trim($record["address"]).", ".$record["city"]." ".$record["state"]." ".$record["zipcode"];
						}
						if (!$AllData){
							$thisDisplay = str_replace("<br>","",$spacer).$spacer;
							if ($record["startDateTime"]){
								$thisDisplay .= date("h:i a",strtotime($record["startDateTime"]))." - ".date("h:i a",strtotime($record["endDateTime"]))."<br>";
							}else{
								$thisDisplay .= "<b>NO TIME SCHEDULED IN SYSTEM YET: Review Personal Calendar</b><br>";
							}
							$thisDisplay .= $record["dataSource"]."<br>"
								."Appointment Contact Information:<br>"
								."<b>".$record["accountName"]."<br>";
							if ($useFullAddress){
								$thisDisplay .=$Address."</b><br>";
							}else{
								$thisDisplay .= $record["address"]."<br>"
									.$record["city"].", ".$record["state"]." ".$record["zipcode"]."</b><br>";
							}
							
							$thisDisplay .=	"<a href='tel:".$record["phone"]."'>".$record["phone"]."</a><br>"
								.$spacer;
							if ($priorAddress){
								$distanceFromPriorAddress = getDistance($priorAddress, $Address, "M");
								$milage = (strpos($distanceFromPriorAddress,"mi") ? trim(str_replace(" mi","",$distanceFromPriorAddress)) : 0);
								$thisDisplay .= "Distance from Prior Apt (".$priorAddress.") to ".$Address."<Br>";
								$thisDisplay .= ($milage > 20 ? "<span style='color:red;font-weight:bold;'>" : "")."Distance from Prior Apt: ".$distanceFromPriorAddress.($milage > 20 ? "</span>" : "")."<br>";
							/*	If ($milage > 20){
									$alerts[] = $ES." on ".date("l, m/d/Y",strtotime($StartDate))." has travel distance of ".$distanceFromPriorAddress." between ".$priorAddress." and ".$Address;
								} */
							}
							$priorAddress = $Address;
							
							$thisDisplay .= "For map and/or driving directions, click on the following link, or paste into a browser:<br>"
								."<b><a href='http://maps.google.com/maps?q=".urlencode($Address)."' target='_Blank'>".$Address."</a></b><br>"
								."For MMWEC Data Entry use the following link:<br>"
								."<a href='https://cet.my.salesforce.com/".$record["id"]."' target='_Blank'>https://cet.my.salesforce.com/".$record["id"]."</a><br>"
								."<br>";
								$thisDisplay .="<br>".$record["MMWECType"]." Visit Details: ".$visitDetails."<br>";
							
							//insert Into SchedulerData
						}else{
							//print_pre($record);
							$ESParts = explode("_",$ES);
							$scheduledData = new stdClass();
							$scheduledData->energySpecialist = $ESParts[1];
							$scheduledData->apptDate = date("Y-m-d H:i:s",strtotime($record["startDateTime"]));
							$scheduledData->endDate = date("Y-m-d H:i:s",strtotime($record["endDateTime"]));
							$scheduledData->siteId = "SalesforceID".$record["id"];
							$scheduledData->projectId = "SalesforceID".$record["id"];
							$scheduledData->electricProvider = $record["energyCompany"];

							$scheduledData->customerName = $record["accountName"];
							$scheduledData->email = $record["email"];
							$scheduledData->phone = $record["phone"];
							$scheduledData->address = $Address;
							$scheduledData->visitType = "MMWEC-".$record["MMWECType"];
							$scheduledData->auditNotes = $record["visitDetails"]." ".$record["auditNotes"];
							$scheduledData->status = ($confirmedAppts["SalesforceID".$record["id"]][date("Y-m-d",strtotime($record["startDateTime"]))] ? "Confirmed" : "");
							//print_pre($scheduledData);
							if (!$creatingCRIScheduleFile){
								$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
							}//don't create if from the scheduled data to begin with
						}
					}

					
					
					if ($record["dataSource"] == "BLOCKED"){
						if (!$AllData){
							$thisDisplay = str_replace("<br>","",$spacer).$spacer;
							$thisDisplay .= date("h:i a",strtotime($record["ResidentialScheduledBlocked_StartTimeStamp"]))." - ".date("h:i a",strtotime($record["ResidentialScheduledBlocked_EndTimeStamp"]))."<br>";
							$thisDisplay .= $record["dataSource"]." - ".$record["ResidentialScheduledBlocked_Reason"]."<br>".$spacer;
							//insert Into SchedulerData
						}else{
							//print_pre($record);
							$ESParts = explode("_",$ES);
							$scheduledData = new stdClass();
							$scheduledData->energySpecialist = $ESParts[1];
							$scheduledData->apptDate = $record["ResidentialScheduledBlocked_StartTimeStamp"];
							$scheduledData->endDate = $record["ResidentialScheduledBlocked_EndTimeStamp"];
							$scheduledData->siteId = "BlockedID".$record["ResidentialScheduledBlocked_ID"];
							$scheduledData->projectId = "BlockedID".$record["ResidentialScheduledBlocked_ID"];
							$scheduledData->electricProvider = "BLOCKED";

							$scheduledData->customerName = $record["ResidentialScheduledBlocked_Reason"];
							$scheduledData->email = NULL;
							$scheduledData->address = NULL;
							$scheduledData->visitType = "BLOCKED";
							$scheduledData->status = "";
							//print_pre($scheduledData);
							$insertScheduleResults = $residentialProvider->insertResScheduleData($scheduledData);
						}
					}
					if (!strpos(" ".$ES,"AllData")){
						$thisSchedule[$ES][date("l, m/d/Y",strtotime($StartDate))][] =$thisDisplay;
					}
					/*debugging*/
				//	if (!strpos(" ".$ES,"AllData") && $record["dataSource"] == "MMWEC"){
				//		echo "<hr>YOSH: ".$ES." ".date("l, m/d/Y",strtotime($StartDate))."=".$thisDisplay."<hr>";
				//	}
					/**/
					//echo $thisDisplay;
					
					/*debugging*/
					//if (!strpos(" ".$ES,"AllData") && $record["dataSource"] == "MMWEC"){
				//		echo "<hr>YOSH: "." Data source: ".$record["dataSource"]."  ".$ES." ".date("l, m/d/Y",strtotime($StartDate))."=".$thisDisplay."<hr>";
				//	}
					/**/
				}
			}
		}
		//echo "<hr><hr>";
	}	
}else{
	echo "No ".implode(" or ",$noEventTypes)." Audit Appointments Scheduled between ".$TomorrowsDate." and ".date("m/d/Y",strtotime($oneWeekLater))."<br>";
}

//print_pre($thisSchedule["Chad S"]["Tuesday, 10/09/2018"]);
if (!$creatingCRIScheduleFile){
/*	if (count($alerts)){
		echo "<div style='border:1pt solid red;background-color:pink;'>";
		foreach ($alerts as $alert){
			echo $alert."<hr>";
		}
		echo "</div>";
	} */
}
if (count($thisSchedule)){
	if (!$sendEmail && !$creatingCRIScheduleFile){
		echo "<h1>Emails Only Send via CronJob</h1>";
		
	}
	//send emails if there are some to send
	require_once "Mail.php";  
	require_once "Mail/mime.php";  
	
	$from = "Center For EcoTechnology <no-reply@cetonline.org>"; 
	$username = "no-reply@cetonline.org"; 
	$password = "Dark76wall!12";  
	
	/*$from = "Yosh Schulman <yosh.schulman@cetonline.org>";
	$username = "yosh.schulman@cetonline.org";
	$password = "YesAgain!23";
	*/
	$host = "smtp.office365.com"; 
	$port = "587"; 

	foreach ($thisSchedule as $ES=>$eventData){
		$sendThisEmail = true;
		//reasons not to send
		if (strtolower($ES) == "cancelled"){
			$sendThisEmail = false;
		}//end if not 'cancelled'
		if ($ES == "Matt Z"){
			$sendThisEmail = false;
			//Matt only wants it sent on Tuesday and Friday
			if (date("D",strtotime($TodaysDate))=="Tue" || date("D",strtotime($TodaysDate))=="Fri"){
				$sendThisEmail = true;
			}
		}//end if not 'cancelled'
		if ($ES == "Jamie O"){
			$sendThisEmail = false;
			//Jamie only wants it sent on Wednesday
			if (date("D",strtotime($TodaysDate))=="Mon" || date("D",strtotime($TodaysDate))=="Wed"){
				$sendThisEmail = true;
			}
		}//end if not 'cancelled'
			$to = $ESEmail[$ES];
			$recipients = $to.",Kacie Dean <kacie.dean@cetonline.org>,IT Admin <it@cetonline.org>";
			/*For Testing ES email receipt 
			$to = "Lauren Holway <lauren.holway@cetonline.org>";
			$recipients = $to.;
			End Testing*/
			$subject = "Schedule Information: ".date("m/d/Y",strtotime($TomorrowsDate))." to ".date("m/d/Y",strtotime($oneWeekLater)).($to == "Lauren Holway <lauren.holway@cetonline.org>" ? " for ".$ESEmail[$ES] : "");
			$msgHeader = "********************************************************************************<br>";
			$msgHeader .= "  Current schedule for ".$ES." (".date("m/d/Y",strtotime($TomorrowsDate))." - ".date("m/d/Y",strtotime($oneWeekLater)).")<Br>";
			$msgHeader .= "********************************************************************************<br>";

			$htmlBody = "<h3>".$ES."</h3>".$msgHeader;
			$txtBody = str_replace("<br>","\r\n",$msgHeader);
			$crlf = "\r\n";
			foreach ($eventData as $eventDate=>$eventInfo){
				$htmlBody .= $border.$eventDate."<br>".$border;
				$txtBody .= $eventDate."\r\n";
				foreach ($eventInfo as $id=>$event){
					$htmlBody .= $event;
					$txtBody .= str_replace("<br>","\r\n",$event);
				}
			}
			if ($sendEmail && $sendThisEmail){
				echo $htmlBody;
				$headers = array (
					'From' => $from,   
					'To' => $to, 
					'Reply-to' => $from,
					'Subject' => $subject
				); 
				$mime = new Mail_mime($crlf);
				$mime->setTXTBody($txtBody);
				$mime->setHTMLBody($htmlBody);
				$body = $mime->get();
				$headers = $mime->headers($headers);
				
				$smtp = Mail::factory(
					'smtp',   
					array (
						'host' => $host,     
						'port' => $port,     
						'auth' => true,     
						'username' => $username,     
						'password' => $password
					)
				);  
				
				$mail = $smtp->send($recipients, $headers, $body);
				print_pre($mail);
			}else{
				if (!$creatingCRIScheduleFile){
					echo $htmlBody;
					echo "<hr><hr>";
				}
			}
	}
	
}

if ($creatingCRIScheduleFile){
	//Create weekly schedule for transmiting to CRI
		ksort($thisWeeklySchedule);
		/* Deprecated 3/5 as instructed by Andrea McCormack from CMCEnergy.com
		$weeklySchedulerHeader = array("SiteId"=>"site_x0020_id","SchedDate"=>"cri_SchedDate","ApptStart"=>"cri_ApptStart",
		"SchedAppType"=>"intakeaudittype","ProgramType"=>"cri_ProgramType","ElectricUtilityName"=>"secondary_x0020_utility","ElectricAcctNum"=>"intakesiteinfoelectricaccount","GasUtilityName"=>"utility",
		"GasAcctNum"=>"intakesiteinfobgasaccount","HeatingSource"=>"","Vendor1"=>"cri_Vendor1","Vendor2"=>"","Vendor3"=>"energy_x0020_specialist","Age"=>"intakesiteinfoyearbuilt","SqFt"=>"","BldType"=>"intakesiteinfohousetype","Units"=>"","ContactName"=>"",
		"ContactPhone"=>"","FacilityName"=>"","CustI"=>"","Fname"=>"first_x0020_name","Lname"=>"last_x0020_name","Address"=>"street_x0020_address","City"=>"town","State"=>"cri_State","Zip"=>"zip_x0020_code","Phone"=>"phone_x0020_number","Cell"=>"","Email"=>"customer_x0020_e_x002d_mail",
		"Lowincome"=>"","Vendor2Type"=>"");
		*/
		//new column format for March
		$weeklySchedulerHeader = array("SiteId"=>"site_x0020_id","SchedDate"=>"cri_SchedDate","ApptStart"=>"cri_ApptStart",
		"SchedAppType"=>"intakeaudittype","ProgramType"=>"cri_ProgramType","ElectricUtilityName"=>"secondary_x0020_utility","ElectricAcctNum"=>"intakesiteinfoelectricaccount","GasUtilityName"=>"utility",
		"GasAcctNum"=>"intakesiteinfobgasaccount","HeatingSource"=>"","Vendor1"=>"cri_Vendor1","Vendor2"=>"cri_Vendor1","Vendor3"=>"energy_x0020_specialist",
		"Fname"=>"first_x0020_name","Lname"=>"last_x0020_name","Address"=>"street_x0020_address","City"=>"town","State"=>"cri_State","Zip"=>"zip_x0020_code","Phone"=>"phone_x0020_number","Cell"=>"","Email"=>"customer_x0020_e_x002d_mail",
		"Vendor2Type"=>"","BillingUtility"=>"");
		
		//print_pre($thisWeeklySchedule);
		foreach ($thisWeeklySchedule as $startTimeStr=>$ESInfo){
			foreach ($ESInfo as $ES=>$info){
				foreach ($info as $aptInfo){
					$weeklyRow = array();
					foreach ($weeklySchedulerHeader as $CRIField=>$SharePointField){
						$displayValue = $aptInfo[$SharePointField];
						switch ($CRIField){
							case "ElectricUtilityName":
								if ($displayValue == "National Grid"){$displayValue = "NGRID ELEC";}
								if ($displayValue == "Eversource"){$displayValue = "EVERSOURCE WEST";}
								break;
							case "GasUtilityName":
								$displayValue = "Berkshire Gas";
								break;
							case "Vendor2Type":
								$displayValue = "LEAD";
								break;
							case "BillingUtility":
								$displayValue = "Berkshire Gas";
								break;
							default:
								$displayValue = $displayValue;
						}
						
						$weeklyRow[$CRIField]=$displayValue;
					}
					$criWeeklyRows[] = $weeklyRow;
				}
			}
		}
		$fileNameToPut = 'CETWeeklySchedule_'.date("Y-m-d",strtotime(date("m/d/Y")." +1 day")).'.txt';
		$fileToUpload = $path.'residential/CRIData/'.$fileNameToPut;
		$fh = fopen($fileToUpload, 'w');

		# write out the headers
		fputcsv($fh, array_keys($weeklySchedulerHeader),"\t");

		# write out the data
		foreach ( $criWeeklyRows as $row ) {
			fwrite($fh, implode("\t", $row) . "\r\n");
			//fputcsv($fh, $row,"\t");
		}
		rewind($fh);
		//$csv = stream_get_contents($fh);
		fclose($fh);
		
		//this File will be sent via a cronjob file called CRISchedulerSendViaSFTP

		//return $csv;
		echo "today is a ".date("D")."\r\n";
		echo  "Created https://cetdashboard.info/admintest/cetadmin/residential/CRIData/CETWeeklySchedule_".date("Y-m-d",strtotime(date("m/d/Y")." +1 day")).".txt ";
}//end if $creatingCRIScheduleFile



$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo $execution_time;
?>
</div> <!--ESSchedulerDataDisplay-->