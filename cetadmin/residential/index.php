<?php
ini_set('max_execution_time',600);

include_once("../_config.php");
//$TodaysDate = "08/19/2018";
//echo "Todays date = ".$TodaysDate."<Br>";
//error_reporting(E_ALL);
$pageTitle = "Admin - Residential";
//print_pre($_SESSION);

$nav = (isset($_GET["nav"]) ? $_GET["nav"] : $sessionNav);
if ($nav){
    switch ($nav){
        case "tester":
            $displayPage = "views/_tester.php";
            break;
        case "inspection-schedule":
            $displayPage = "views/_inspectionSchedule.php";
            break;
        case "report-ClosureRate":
            $displayPage = "views/_report_ClosureRates.php";
            break;
		case "report-BGASRequisite":
			$displayPage = "views/_report_BGASRequisite.php";
			break;
        case "report-BGASExtractProcessor":
			$navDropDown = $nav;
			$displayPage = "views/_report-BGASExtractProcessor.php";
            break;
        case "HES-DataImporter":
			$navDropDown = $nav;
			$displayPage = "views/_HES-DataImporter.php";
            break;
        case "HES-DataReviewer":
			$navDropDown = $nav;
			$displayPage = "views/_HES-DataReviewer.php";
            break;
		case "ESScheduler":
			$navDropDown = $nav;
			$displayPage = "views/_ESSchedulerLayout.php";
            break;
		case "ESSchedulerDisplay":
			$navDropDown = $nav;
			$displayPage = "views/_ESScheduler.php";
            break;
		case "EZDocCreator":
			$navDropDown = $nav;
			$displayPage = "views/_EZDocCreator.php";
            break;
		case "BilledLastMonth":
			$navDropDown = $nav;
			$displayPage = "views/_BilledLastMonth.php";
            break;
		case "EFIExtractor":
			$navDropDown = $nav;
			$displayPage = "views/_EFIExtractor.php";
            break;
		case "Leads":
			$displayPage = "views/_Leads.php";
            break;
		case "HESStats":
			$displayPage = "views/_HESStats.php";
            break;
		case "SolarAccessConverter":
			$displayPage = "views/_SolarAccessConverter.php";
            break;
        case "ebrContract":
            echo '<link rel="stylesheet" href="../css/jquery.fileupload-ui.css">';
        	$displayPage = "views/_ebrContract.php";
        	break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Residential Administration</h1>';
include_once("_residentialMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Process Time: {$time}";
?>