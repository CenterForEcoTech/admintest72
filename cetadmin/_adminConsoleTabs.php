<?php $adminConsoleTabs = true;?>
<ul id="admin-tabs" class="tabs">
<!--    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Resources))){$setTab="Resources";?><li><a id="tab-Resources" href="#Resources" class="setTab">Resources</a></li><?php }?>
   -->
	 <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Inventory))){$setTab="Inventory";?><li><a id="tab-Inventory" href="#Inventory" class="setTab">Inventory</a></li><?php }?>
<!--    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GHS))){$setTab="GHS";?><li><a id="tab-GHS" href="#GHS" class="setTab">GHS</a></li><?php }?>-->
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Tools))){?><li><a id="tab-Tools" href="#Tools" class="setTab">Tools</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_HR))){$setTab="HR";?><li><a id="tab-HR" href="#HR" class="setTab">HR</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GBS))){$setTab="GBS";?><li><a id="tab-GBS" href="#GBS" class="setTab">Commercial (GBS)</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Residential)) || count(array_intersect($adminSecurityGroups,$Security_Tabs_GHS))){$setTab="Residential";?><li><a id="tab-Residential" href="#Residential" class="setTab">Residential (GHS)</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Muni))){$setTab="Muni";?><li><a id="tab-Muni" href="#Muni" class="setTab">Muni</a></li><?php }?>
<!--    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Invoicing))){$setTab="Invoicing";?><li><a id="tab-Invoicing" href="#Invoicing" class="setTab">Retail</a></li><?php }?> -->
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Hours))){$setTab="Hours";?><li><a id="tab-Hours" href="#Hours" class="setTab">Hours</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Forecast))){$setTab="Forecast";?><li><a id="tab-Forecast" href="#Forecast" class="setTab">Forecast</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Admin))){//$setTab="Admin";?><li><a id="tab-Admin" href="#Admin" class="setTab">Admin</a></li><?php }?>
</ul>
<?php $setTab = ($_SESSION['setTab'] ? : $setTab);?>

<ul class="tabs-content">
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Resources))){?><li id="Resources">Resources Menu:<?php $navMenuID="navMenuResources"; include("resources/_resourcesMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Inventory))){?><li id="Inventory" class="tab-content">Inventory Menu:<?php $navMenuID="navMenu1"; include("inventory/_inventoryMenu.php");include("_quickLinks.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Tools))){?><li id="Tools" class="tab-content">Tools Menu:<?php $navMenuID="navMenu2"; include("tools/_toolsMenu.php");include("_quickLinks.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_HR))){?><li id="HR">HR Menu:<?php $navMenuID="navMenu4"; include("hr/_hrMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GBS))){?><li id="GBS">Commercial Menu:<?php $navMenuID="navMenu6"; include("gbs/_gbsMenu.php");include("_quickLinks.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Muni))){?><li id="Muni">Muni Menu:<?php $navMenuID="navMenuMuni"; include("muni/_muniMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Hours))){?><li id="Hours" class="tab-content">Hours Menu:<?php $navMenuID="navMenuHours"; include("hours/_hoursMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Residential)) || count(array_intersect($adminSecurityGroups,$Security_Tabs_GHS))){?><li id="Residential" class="tab-content">Residential Menu:<?php $navMenuID="navMenuResidential"; include("residential/_residentialMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Invoicing))){?><li id="Invoicing" class="tab-content">Retail Menu:<?php $navMenuID="navMenuInvoicing"; include("invoicing/_invoicingMenu.php");include("_quickLinks.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Forecast))){?><li id="Forecast">Forecast Menu:<?php $navMenuID="navMenuForecast"; include("forecast/_forecastMenu.php"); include("tools/_toolsMenu.php");include("_quickLinks.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Admin))){?><li id="Admin">Admin Menu:<?php $navMenuID="navMenu5"; include("admin/_adminMenu.php");?></li><?php }?>

</ul>
<script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
    $(function(){
		$(".setTab").on('click',function(){
			var $this = $(this),
				thisTab = $this.attr('href').replace("#","");
				var Data = {};
					Data["setTab"] = thisTab;
					console.log(Data);
			$.ajax({
				url: "setTabSession.php",
				type: "GET",
				data: Data,
				success: function(data){
					console.log(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					console.log(message);
				}
			});				
		});
        var hash = window.location.hash,
            tabHashes = new Array("#Inventory","#Tools","#SiteConfig","#HR","#GBS","#MO","#Forecast","#Resources","#Admin","#Muni","#Invoicing","#Residential","#Hours"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.addClass("active").show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        } else if (isOnSiteConfigTab || !(hash.length)){
            setTab("<?php echo $setTab;?>");
        }
	$(".back-to-console").hide();
    <?php if ($scrollId) {
        // to support the site config tab, we want to scroll to AFTER the tab has received focus
        ?>

        $('html, body').animate({
            scrollTop: $("#<?php echo $scrollId;?>").offset().top - 250
        }, 500);
<?php } ?>
    });
</script>