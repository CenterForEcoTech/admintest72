<h2>Disburse Funds</h2>

<p>You are here because you want to manually disburse funds to nonprofits in one of the following ways:</p>
<ul style="list-style-type: disc;">
    <li>Generate a spreadsheet of non-fiscally-sponsored nonprofits with disbursements for FirstGiving.</li>
    <li>View a disbursement and manually mark it as disbursed (because you intend to have a check cut manually through some other process).</li>
</ul>

<fieldset>
    <h3>Disburse funds for FirstGiving to process</h3>
    <p>This process will only deal with disbursements that have reached the minimum threshold, AND are for the non profit registered with the IRS.</p>

    <input class="button-link" type="button" id="load-disbursements" value="Refresh List">
    <form action="ExportFirstGivingCSV.php" method="POST">
        <div id="export-disbursements"><?php // container populated by ajax ?></div>
    </form>
</fieldset>

<fieldset>
    <h3>Download previous disbursement spreadsheet</h3>
    <p>
        This process will allow you to download a previously generated spreadsheet. Understand that these disbursements have already been marked as disbursed.
        If you send this spreadsheet to FirstGiving and donations are processed again, this fact will not be registered in the system.
    </p>

    <input class="button-link" type="button" id="load-prev-disbursements" value="Refresh List">
    <form action="ExportFirstGivingCSV.php" method="POST">
        <div id="export-prev-disbursement"><?php // container populated by ajax ?></div>
    </form>
</fieldset>

<fieldset>
    <h3>Sync with bill.com</h3>
    <p>If you need to do a disbursement and know there are payments that haven't been pulled in, you can manually force a sync here.</p>
    <a href="?action=sync-bill-com" class="button-link">Sync with Bill.Com</a>
</fieldset>

<fieldset>
    <h3>Possible future features for this page</h3>
    <p>Ability to "preview" a spreadsheet before generating it. Will display data in html form first for review.</p>
    <p>Ability to view any pending disbursements regardless of minimum amounts met.</p>
</fieldset>

<script type="text/template" id="form-header-export">
    <h4>Generate a spreadsheet</h4>
    <p class="warning">NOTICE: Clicking Export will mark these disbursements as "disbursed" -- do not do this unless you really mean it!</p>
    <table class="hor-minimalist-b">
        <thead>
            <tr>
                <th><label><input type="checkbox" id="select-all-export" name="export_all" class="row-click-target"> Export All</label></th>
                <th>Amount</th>
                <th>EIN</th>
                <th>Name -- if available</th>
                <th>Is Fiscally Sponsored?</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <input type="submit" class="export-action" data-script="ExportFirstGivingCSV.php" value="Export for FirstGiving">
    <input type="submit" class="export-action" data-script="ExportDisbursementForDan.php" value="Export for Dan">
    <p>Tips:</p>
    <ul style="list-style-type: disc;">
        <li>Clicking "Export All" more efficiently exports all disbursements that are ready to go.</li>
        <li>There is nothing to assist you in individually selecting the entire list (because it is less efficient on the back end to run the whole list that way).</li>
        <li>If you really do wish to run an export of all EXCEPT a few items, you do have to select them by hand, and you are advised to keep the list relatively short (i.e., create several small exports and manually combine them in a separate process).</li>
    </ul>
</script>
<script type="text/template" id="disbursement-row">
    <tr>
        <td>
            <input type="checkbox" value="{id}" name="disbursementIds[]" class="row-click-target {fiscalSponsorClass}">
        </td>
        <td class="selectable">{amount}</td>
        <td class="selectable">{ein}</td>
        <td class="selectable">{name}</td>
        <td class="selectable">{isFiscallySponsored}</td>
    </tr>
</script>

<script type="text/template" id="form-header-exported">
    <h4>Download a spreadsheet</h4>
    <table class="hor-minimalist-b">
        <thead>
        <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Size</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <input type="hidden" name="filepath" id="re-export-file-path">
</script>
<script type="text/template" id="exported-disbursement-row">
    <tr>
        <td><a href="#" class="re-export" data-path="{path}">{name}</a></td>
        <td>{date}</td>
        <td>{size}</td>
    </tr>
</script>
<script type="text/javascript">
    $(function(){
        var exportListDiv = $("#export-disbursements"),
            loadCandidateDisbursements = function(){
                var noneText = "<p>Nothing qualified for disbursement at this time!</p>",
                    url = "getFGCandidateDisbursements.php",
                    formHeaderTemplate = $("#form-header-export").text(),
                    htmlRowTemplate = $("#disbursement-row").text();
                exportListDiv.html("<p class='alert'>Loading... Please please please wait...</p>");
                $.post(
                    url,
                    {
                        action: "get"
                    },
                    function(data){
                        exportListDiv.html("");
                        if (data.length){
                            exportListDiv.append(formHeaderTemplate);
                            $.each(data, function(index, item){
                                var escapedName = item.name.replace("'", '&apos;').replace('"', '&quot;'),
                                    textToAppend = htmlRowTemplate
                                        .replace("{id}", item.id)
                                        .replace("{amount}", item.amount)
                                        .replace("{ein}", item.ein)
                                        .replace("{name}", item.name),
                                    tableBody = exportListDiv.find("table tbody");
                                if (item.is_fiscally_sponsored){
                                    textToAppend = textToAppend
                                        .replace("{fiscalSponsorClass}", "child-org")
                                        .replace("{isFiscallySponsored}", "yes");
                                } else {
                                    textToAppend = textToAppend
                                        .replace("{fiscalSponsorClass}", "parent-org")
                                        .replace("{isFiscallySponsored}", "");
                                }
                                tableBody.append(textToAppend);
                            });
                        } else {
                            exportListDiv.append(noneText);
                        }
                    }
                );
            },
            exportedListDiv = $("#export-prev-disbursement"),
            loadExportedDisbursements = function(){
                var noneText = "<p>Nothing in the exported files repository.</p>",
                    url = "getExportedDisbursements.php",
                    formHeaderTemplate = $("#form-header-exported").text(),
                    htmlRowTemplate = $("#exported-disbursement-row").text();
                exportedListDiv.html("<p class='alert'>Loading... Please please please wait...</p>");
                $.post(
                    url,
                    {
                        action: "get"
                    },
                    function(data){
                        exportedListDiv.html("");
                        if (data.length){
                            exportedListDiv.append(formHeaderTemplate);
                            $.each(data, function(index, item){
                                var escapedName = item.name.replace("'", '&apos;').replace('"', '&quot;'),
                                    textToAppend = htmlRowTemplate
                                        .replace("{path}", item.path)
                                        .replace("{name}", item.name)
                                        .replace("{date}", item.date)
                                        .replace("{size}", item.size),
                                    tableBody = exportedListDiv.find("table tbody");
                                tableBody.append(textToAppend);
                            });
                        } else {
                            exportedListDiv.append(noneText);
                        }
                    }
                );
            };

        // init
        $("#load-disbursements").click(function(){
            loadCandidateDisbursements();
        });

        $("#load-prev-disbursements").click(function(){
            loadExportedDisbursements();
        });

        exportListDiv.on("click", ".export-action", function(){
            var clickedButton = $(this),
                form = clickedButton.closest("form"),
                scriptFile = clickedButton.attr("data-script");
            $(".export-action").prop("disabled", true);
            form.attr("action", scriptFile);
            form.attr("method", "POST");
            form.submit();
        });

        // if the "all" checkbox is selected, check all parent orgs and uncheck all child orgs
        exportListDiv.on("click", "table #select-all-export", function(){
            var targetElements = $(this).closest("table").find("tbody").find(".row-click-target:checkbox"),
                isChecked = $(this).prop('checked');
            targetElements.prop('disabled', isChecked);
            if (isChecked){
                targetElements.prop('checked', false);
                alert("Checking 'All' will only export disbursements for Parent organizations. In order to disburse to Child organizations, you must select them.");
            }
        });

        exportListDiv.on("click", "table tbody .selectable", function(e){
            var targetElement = $(this).parent().find(".row-click-target:checkbox");
            if (targetElement){
                targetElement.prop('checked', !targetElement.prop('checked'));
                e.stopPropagation();
            }
        });

        // if a child org is manually selected, uncheck the "all" checkbox.
        exportListDiv.on("change", "table tbody .row-click-target.child-org:checkbox", function(e){
            var targetElement = $(this);
            if (targetElement.prop('checked')){
                $("#select-all-export").prop('checked', false);
                e.stopPropagation();
            }
        });

        exportedListDiv.on("click", ".re-export", function(e){
            e.preventDefault();
            $("#re-export-file-path").val($(this).attr("data-path"));
            $(this).closest("form").submit();
            $("#re-export-file-path").val("");
        });
    });
</script>