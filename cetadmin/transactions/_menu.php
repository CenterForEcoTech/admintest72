<div id="nav-menu">
    <ul>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
        }
        if (activeTab.length){
            activeTab.addClass("ui-state-highlight");
        } else {
            $("#profiles").addClass("ui-state-highlight");
        }
    });
</script>