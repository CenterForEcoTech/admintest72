<style type="text/css">
    table {
        border:1px solid black;
        border-collapse: collapse;
    }
    table td, table th{
        border:1px solid black;
        padding:3px 15px 3px 5px;
    }
    table tbody th, table tbody td{
        text-align: left;
        color:#5C5C5C;
        min-width:230px;
    }
    table tbody td.Entered.By.Merchant{
        color:green;
    }
    table tbody td.Deleted{
        color:red;
    }
</style>
<h2>Review Transaction</h2>
<form id="get-transaction">
    <input type="hidden" name="nav" value="review">
    <fieldset>
        <label>
            <div>Event Transaction ID:</div>
            <input type="text" name="id" pattern="\d*" value="<?php echo $record->ID?>" class="auto-watermarked" title="Enter a number">
        </label>
        <input type="submit" value="Get Another" class="button-link">
    </fieldset>
</form>
<script type="text/javascript">
    $(function(){
        var formElement = $("#get-transaction"),
            idField = formElement.find("input[name='id']");

        formElement.on("click", "input[type='submit']", function(e){
            if (!idField.val()){
                e.preventDefault();
                alert("Please enter a transaction id first!");
            }
        });
    });
</script>
<table>
    <thead>
    <tr>
        <th>Column Name</th>
        <th>Value</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (get_object_vars($record) as $fieldName => $fieldValue) {
        if ($fieldName == 'canDelete') {
            break;
        } ?>
        <tr>
            <th><?php echo $fieldName; ?></th>
            <td class="<?php echo $fieldValue;?>"><?php echo $fieldValue;?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<fieldset>
    <legend>Admin Actions</legend>
    <p>You must enter the password to delete a transaction or impersonate this merchant.</p>
    <label>
        <input id="impersonate-pwd" type="password" class="auto-watermarked" title="password for admin actions">
    </label>
    <?php if (getMemberId() !== $record->memberId) { ?>
    <br>
    <input id="impersonate-button" type="button" class="button-link" value="Impersonate">
    <?php } ?>
    <div class="on-logged-in" style="display:none;">
    <?php if ($record->canDelete && $record->Status !== 'Deleted') { ?>
        <input id="delete-button" data-id="<?php echo $record->ID;?>" type="button" class="button-link ui-state-highlight" value="Delete Transaction">
        <script type="text/javascript">
            $(function(){
                $("#delete-button").on("click", function(e){
                    var self = $(this),
                        id = self.data("id");
                });
            });
        </script>

    <?php } ?>
        <br>

        <a href="<?php echo $CurrentServer."Merchant/EnterSales#".$record->eventId;?>" class="button-link" target="enter-sales-for-<?php echo $record->eventId;?>">Enter Sales for Event #<?php echo $record->eventId;?></a>
    </div>
</fieldset>
<script type="text/javascript">
    $(function(){
        var memberId = <?php echo $record->memberId;?>,
            pwdField = $("#impersonate-pwd"),
            impersonateButton = $("#impersonate-button");

        if (impersonateButton.length){
            impersonateButton.on("click", function(e){
                var self = $(this),
                    pwd = pwdField.val(),
                    postData = {
                        "action": "impersonate",
                        "memberId": memberId,
                        "password": pwd
                    };
                $.ajax({
                    url:'<?php echo $CurrentServer.$adminFolder;?>reports/ApiMemberRegistrations.php',
                    type: "POST",
                    data: JSON.stringify(postData),
                    success: function(data){
                        alert(data.message);
                        self.hide();
                        $(".on-logged-in").show();
                    },
                    error: function(response){
                        var data = JSON.parse(response.responseText);
                        alert(data.message);
                        pwdField.highlight();
                    }
                });
            });
        } else {
            $(".on-logged-in").show();
        }

        $("#delete-button").on("click", function(e){
            var pwd = pwdField.val(),
                postData = {
                    "id":<?php echo $record->ID;?>,
                    "memberId": memberId,
                    "password": pwd
                };

            if (!pwd){
                alert("Please enter the admin password to delete this transaction.");
            } else {
                if (confirm("Are you sure you want to delete this Transaction? There is no undo!")){

                    $.ajax({
                        url:'<?php echo $CurrentServer.$adminFolder;?>transactions/ApiTransactions.php',
                        type: "DELETE",
                        data: JSON.stringify(postData),
                        success: function(data){
                            window.location.reload();
                        },
                        error: function(response){
                            var data = JSON.parse(response.responseText);
                            alert(data.message);
                            pwdField.highlight();
                        }
                    });
                }
            }
        });
    });
</script>