<?php
include_once("../_config.php");
include_once($dbProviderFolder."EventTransactionProvider.php");
if ( isset($_SERVER["REMOTE_ADDR"]) ){
    $ip=$_SERVER["REMOTE_ADDR"];
} elseif (isset($_SERVER["SERVER_ADDR"])){
    $ip=$_SERVER["SERVER_ADDR"];
} else {
    $ip="Not Available";
}

$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $newRecord = $currentRequest->getAsRecord();
        $eventTransactionProvider = new EventTransactionProvider($mysqli);
        $eventTransactionRecord = new EventTransactionRecord($newRecord->id, "");
        $memberId = $newRecord->memberId;
        $username = "ct".$memberId;
        $password = $newRecord->password;
        $ip = "";
        $errorMessage = $eventTransactionProvider->deleteMerchantTransaction($eventTransactionRecord, $memberId, $username, $password, $ip);
        $transactionDeleted = strpos(strtolower($errorMessage), "incorrect password") === false;
        if ($transactionDeleted){
            header("HTTP/1.0 200 OK");
        } else {
            $results->message = $errorMessage;
            header("HTTP/1.0 409 Conflict");
        }
        break;
    case "GET":
        $results->message = "GET is not supported at this time.";
        break;
    case "POST":
        $results->message = "POST is not supported at this time.";
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
if ($results->message || count($results->validationMessages)){
    header("HTTP/1.0 409 Conflict");
}
output_json($results);
die();