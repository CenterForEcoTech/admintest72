<?php
include_once("../_config.php");
$pageTitle = "Admin - Transactions";
?>
<html>
<?php include("../_header.php");
?>
    <div class="container">
        <h1>Transaction Management</h1>
        <?php

        $nav = isset($_GET["nav"]) ? $_GET["nav"] : "error-logs";
        include_once("_menu.php");
        if ($nav){
            switch ($nav){
                case "review":
                default:
                    include("model/_reviewModel.php");
                    include("views/_review.php");
                    break;
            }
        }
        ?>
    </div> <!-- end container -->
<?php
include("../_footer.php");
?>