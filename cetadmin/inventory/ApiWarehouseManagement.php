<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");

include_once("model/_productTableRow.php");
include_once("model/_warehouseRow.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."InventoryProvider.php");
include_once($dbProviderFolder."MemberProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        echo "this is get";
        break;
    case "POST":
		echo "this is post";
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'warehouse_update'){
	            $warehouseProvider = new WarehouseProvider($dataConn);
				$response = $warehouseProvider->update($newRecord, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else if (trim($newRecord->action) == 'warehouse_add'){
	            $warehouseProvider = new WarehouseProvider($dataConn);
				$response = $warehouseProvider->add($newRecord, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else if (trim($newRecord->action) == 'commission_lock'){
	            $warehouseProvider = new WarehouseProvider($dataConn);
				$ESData = $newRecord->ESData;
				foreach ($ESData as $ES=>$ESInfo){
					$record = new stdClass();
					$record = $ESInfo;
					$response[] =$warehouseProvider->commissionLock($record, getAdminId()); 
				}
				$results = $response;
				header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == 'insert_commissionnote'){
	            $warehouseProvider = new WarehouseProvider($dataConn);
				$NoteDate = $newRecord->noteDate;
				$Note = $newRecord->note;
				$response =$warehouseProvider->commissionNoteInsert($NoteDate,$Note, getAdminId()); 
				$results = $response;
				header("HTTP/1.0 201 Created");
		}else{
            $warehouseProvider = new WarehouseProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $warehouseProvider->updateDisplayOrder($id,($displayID+1),1, getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $warehouseProvider->updateDisplayOrder($id,0,0, getAdminId());
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>