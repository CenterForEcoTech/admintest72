<?php

include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
include_once("_initImageManager.php"); // initializes imageManager, imageDataProvider, & options
$currentRequest = new Request();
//error_reporting(1); ini_set("display_errors", 1);
$results = new stdClass();

function getImageUrl(MediaRecord $object, $CurrentServer, $siteRoot){
    return ImageLibraryHelper::getSmallImageUrl($object->folderUrl, $object->fileName, $CurrentServer, $siteRoot);
}

function getLargeImageUrl(MediaRecord $object, $CurrentServer, $siteRoot){
    return ImageLibraryHelper::getLargeImageUrl($object->folderUrl, $object->fileName, $CurrentServer, $siteRoot);
}

if ($imageManager){
    switch(strtoupper($currentRequest->verb)){
        case "DELETE":
            $file_name = $_GET['file'];
            $results = $imageManager->deleteFile($file_name);
            break;
        case "GET":
            if ($_GET["action"] == "keyword_search" && $_GET["keyword"]){
                // this is used by the logo search; allows searching merchant images
                $criteria = new MediaCriteria();
                $criteria->keyword = $_GET["keyword"];
                $criteria->size = 0; // for now, get all records
                $result = $imageDataProvider->get($criteria);
                foreach($result->collection as $imageRecord){
                    $imageRecord->imageFullPath = getImageUrl($imageRecord, $CurrentServer, $siteRoot);
                    $imageRecord->largeImageFullPath = getLargeImageUrl($imageRecord, $CurrentServer, $siteRoot);
                }
                $results = $result;
            } else {
                $id = $_GET['id'];
                $files = $imageDataProvider->getImages($id);
                foreach($files as $fileRow){
                    $file = $options->getMetadata($fileRow->file_name);
                    if (!$file->name){
                        // try merchant library
                        $file = $merchantImageOptions->getMetadata($fileRow->file_name);
                        if ($file->name){
                            // don't include this one
                            continue;
                        }
                    }
                    $file->id = $fileRow->id;
                    $file->title = $fileRow->title;
                    $file->description = $fileRow->description;
                    $file->keywords = $fileRow->keywords;
                    $file->delete_type = 'DELETE';
                    $file->delete_url = "ApiImageLibrary.php?file=".rawurlencode($file->name);
                    $file->width = $fileRow->width;
                    $file->height = $fileRow->height;
                    $result[] = $file;
                }
                $results = $result;
            }
            break;
        case "POST":
            if (empty($_POST)){
                $newRecord = (array)json_decode($currentRequest->rawData);
                $id = $newRecord['id[]'];
                $response = $imageDataProvider->updateUserData($id, $newRecord['title[]'], $newRecord['description[]'], $newRecord['keywords[]']);
                if (!$response->success){
                    $results->message = $response->error;
                    header("HTTP/1.0 409 Conflict");
                } else {

                    $files = $imageDataProvider->getImages($id);
                    foreach($files as $fileRow){
                        $file = $options->getMetadata($fileRow->file_name);
                        $file->id = $fileRow->id;
                        $file->title = $fileRow->title;
                        $file->description = $fileRow->description;
                        $file->keywords = $fileRow->keywords;
                        $file->delete_type = 'DELETE';
                        $file->delete_url = "ApiImageLibrary.php?file=".rawurlencode($file->name);
                        $result[] = $file;
                    }
                    $results = $result;
                }
            } else {
                // this includes at least one uploaded file
                $postedFilesArray = JqueryFileUploadConverter::convertToMediaLibraryFile($_SERVER, $_FILES, $_POST, "files", "title");
                $result = array();
                foreach ($postedFilesArray as $pendingFile){
                    $file = $imageManager->upload($pendingFile);
                    if ($file->name){
                        foreach ($pendingFile->formValuesArray as $key => $value){
                            $file->{$key} = $value;
                        }
                        $file->delete_type = 'DELETE';
                        $file->delete_url = "ApiImageLibrary.php?file=".rawurlencode($file->name);
                    }
                    $result[] = $file;
                }
                $results = $result;
            }
            break;
        case "PUT":
            $results->message = "PUT is not supported at this time";
            break;
    }
} else {
    $results->message = "Access Denied";
}
output_json($results);
die();
