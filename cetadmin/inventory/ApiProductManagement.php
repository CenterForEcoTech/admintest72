<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");

include_once("model/_productTableRow.php");
include_once("model/_warehouseRow.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."InventoryProvider.php");
include_once($dbProviderFolder."MemberProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        if ($_GET['action'] == "transferhistory"){
            $productProvider = new ProductProvider($dataConn);
            $criteria = ProductMovementHistoryTableRow::getCriteria($_GET);
            $paginationResult = $productProvider->getProductMovmentHistory($criteria);
            $resultArray = $paginationResult->collection;
            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new ProductMovementHistoryTableRow($record);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
		}else if ($_GET['action'] == "transferleakage"){
			$warehouseProvider = new WarehouseProvider($dataConn);
			$paginationResult = $warehouseProvider->get(); 
			$resultArray = $paginationResult->collection;
			
			//get exclusion list
			foreach ($resultArray as $WarehouseID=>$WarehouseDetail){
				if ($WarehouseDetail->excludeFromCountId){
					$excludeList[]=$WarehouseDetail->name;
				}
				$orderWareHouseByID[$WarehouseDetail->name]=$WarehouseDetail->displayOrderId;
			}
		
			//get warehouse count information for all warehouses
			$criteria = new WarehouseCriteria();
			$criteria->excludelist = $excludeList;
			$criteria->mostrecent = true;
			$warehouseCount = $warehouseProvider->getAllWarehouseCount($criteria);
			foreach ($warehouseCount->collection as $warehouseCollection=>$warehouseCountDetails){
				$warehouseName = strtoupper($warehouseCountDetails->warehouseName);
				$EFICount[$warehouseCountDetails->efi] = $EFICount[$warehouseCountDetails->efi]+$warehouseCountDetails->qty;
				$EFIDetailCount[$warehouseCountDetails->efi][$warehouseName] = $EFICount[$warehouseCountDetails->efi][$warehouseName]+$warehouseCountDetails->qty;
				$WarehouseCountArray[$warehouseName]["EFI"][$warehouseCountDetails->efi] = $WarehouseCountArray[$warehouseName]["EFI"][$warehouseCountDetails->efi]+$warehouseCountDetails->qty;
				$WarehouseCountArray[$warehouseName]["TotalCount"] = $WarehouseCountArray[$warehouseName]["TotalCount"]+$warehouseCountDetails->qty;
			}
				
			//Reset the Order for the details
			foreach ($EFIDetailCount as $EFI=>$WarehouseList){
				uksort($WarehouseList, 'my_uksort');
				$EFIDetailCountOrdered[$EFI]=$WarehouseList;
				//print_pre($WarehouseList);

			}
	
            $productProvider = new ProductProvider($dataConn);
            $criteria = ProductTableRow::getCriteria($_GET);
            $paginationResult = $productProvider->get($criteria);
            $resultArray = $paginationResult->collection;
            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new ProductTableRow($record, $productImageUrlPath);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
			
			//look at sum by efi for movements 3 weeks
			$leakagePaginationResult = $productProvider->getProductMovmentLeakage($criteria);
			$leakageResults = $leakagePaginationResult->collection;
			foreach ($leakageResults as $ID=>$LeakageDetail){
				$Leakage3Weeks[$LeakageDetail->efi]["units"] = $LeakageDetail->units;
			}
			//look at sum by efi for movements 1 month prior
			$criteria->startDate = date("m/d/Y", strtotime($TodaysDate." - 1 month"));
			$criteria->endDate = $TodaysDate;
			$leakagePaginationResult = $productProvider->getProductMovmentLeakage($criteria);
			$leakageResults = $leakagePaginationResult->collection;
			foreach ($leakageResults as $ID=>$LeakageDetail){
				$Leakage1Month[$LeakageDetail->efi]["units"] = $LeakageDetail->units;
			}
			
			//inject Product count
			foreach ($results["aaData"] as $ID=>$ProductDetail){
				$results["aaData"][$ID]->totalCount = $EFICount[$ProductDetail->efi];
				$results["aaData"][$ID]->detailCount = $EFIDetailCountOrdered[$ProductDetail->efi];
				$results["aaData"][$ID]->excludelist = $excludeList;
				$results["aaData"][$ID]->units3weeks = $Leakage3Weeks[$ProductDetail->efi]["units"];
				$results["aaData"][$ID]->units1month = $Leakage1Month[$ProductDetail->efi]["units"];
			}
			
			

        }else if ($_GET['action'] == 'get-onhand' || $_GET['action'] == "list"){
			$warehouseProvider = new WarehouseProvider($dataConn);
			$paginationResult = $warehouseProvider->get(); 
			$resultArray = $paginationResult->collection;
			
			//get exclusion list
			foreach ($resultArray as $WarehouseID=>$WarehouseDetail){
				if ($WarehouseDetail->excludeFromCountId){
					$excludeList[]=$WarehouseDetail->name;
				}
				$orderWareHouseByID[$WarehouseDetail->name]=$WarehouseDetail->displayOrderId;
			}
		
			//get warehouse count information for all warehouses
			$criteria = new WarehouseCriteria();
			$criteria->excludelist = $excludeList;
			$criteria->mostrecent = true;
			$warehouseCount = $warehouseProvider->getAllWarehouseCount($criteria);
			foreach ($warehouseCount->collection as $warehouseCollection=>$warehouseCountDetails){
				$warehouseName = strtoupper($warehouseCountDetails->warehouseName);
				$EFICount[$warehouseCountDetails->efi] = $EFICount[$warehouseCountDetails->efi]+$warehouseCountDetails->qty;
				$EFIDetailCount[$warehouseCountDetails->efi][$warehouseName] = $EFICount[$warehouseCountDetails->efi][$warehouseName]+$warehouseCountDetails->qty;
				$WarehouseCountArray[$warehouseName]["EFI"][$warehouseCountDetails->efi] = $WarehouseCountArray[$warehouseName]["EFI"][$warehouseCountDetails->efi]+$warehouseCountDetails->qty;
				$WarehouseCountArray[$warehouseName]["TotalCount"] = $WarehouseCountArray[$warehouseName]["TotalCount"]+$warehouseCountDetails->qty;
			}
				
			//Reset the Order for the details
			foreach ($EFIDetailCount as $EFI=>$WarehouseList){
				uksort($WarehouseList, 'my_uksort');
				$EFIDetailCountOrdered[$EFI]=$WarehouseList;
				//print_pre($WarehouseList);

			}
	
            $productProvider = new ProductProvider($dataConn);
            $criteria = ProductTableRow::getCriteria($_GET);
            $paginationResult = $productProvider->get($criteria);
            $resultArray = $paginationResult->collection;
            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new ProductTableRow($record, $productImageUrlPath);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
			
			//inject Product count
			foreach ($results["aaData"] as $ID=>$ProductDetail){
				$results["aaData"][$ID]->totalCount = ($EFICount[$ProductDetail->efi] ? $EFICount[$ProductDetail->efi] : 0);
				$results["aaData"][$ID]->detailCount = ($EFIDetailCountOrdered[$ProductDetail->efi] ? $EFIDetailCountOrdered[$ProductDetail->efi] : 0);
				$results["aaData"][$ID]->excludelist = $excludeList;
			}
        } else {
            $tagId = $_GET["tagID"];
            $productResourceProvider = new ProductResourceProvider($dataConn);
            $results = $productResourceProvider->getTagReferences($tagId, false);
        }
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
            if ($_POST['action'] === 'product-transfer'){
				//warehouse provider used to update count
				$warehouseProvider = new WarehouseProvider($dataConn);
                $productProvider = new ProductProvider($dataConn);
				
				$WarehouseFromLocation = $_POST["WarehouseFromLocation"];
				$criteria = new stdClass();
				$criteria->name = $WarehouseFromLocation;
				$warehouseResults = $warehouseProvider->get($criteria);
				$warehouseInfo = $warehouseResults->collection;
				$FromLocationOutsideGHS = $warehouseInfo[0]->outsideGHS;

				
				$WarehouseToLocation = $_POST["WarehouseToLocation"];
				//get Email for WarehouseCountArray
				$criteria = new stdClass();
				$criteria->name = $WarehouseToLocation;
				$warehouseResults = $warehouseProvider->get($criteria);
				$warehouseInfo = $warehouseResults->collection;
				$EmployeeID = $warehouseInfo[0]->linkedToEmployeeId;
				$SecondEmployeeID = $warehouseInfo[0]->linkedToSecondEmployeeId;
				$ToLocationOutsideGHS = $warehouseInfo[0]->outsideGHS;

				
				//if the warehouse is linked to an employee get the email address
				if ($EmployeeID){
					include_once($dbProviderFolder."HREmployeeProvider.php");
					$hrEmployeeProvider = new HREmployeeProvider($dataConn);
					$criteria->id = $EmployeeID;
					$paginationResult = $hrEmployeeProvider->get($criteria);
					$resultArray = $paginationResult->collection;
					$EmailAddress = $resultArray[0]->email;
					$EmployeeName = $resultArray[0]->fullName;
					$sendEmail = true;

				}
				//only include second employee ID if one of the locations is outside GHS
				if ($FromLocationOutsideGHS || $ToLocationOutsideGHS){
					if ($SecondEmployeeID){
						include_once($dbProviderFolder."HREmployeeProvider.php");
						$hrEmployeeProvider = new HREmployeeProvider($dataConn);
						$criteria->id = $SecondEmployeeID;
						$paginationResult = $hrEmployeeProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						$SecondEmailAddress = $resultArray[0]->email;
						$SecondEmployeeName = $resultArray[0]->fullName;
						$sendEmail = true;
					}
				}
				
				$TransferResults = $productProvider->transfer($_POST, getAdminId());
				$results = $TransferResults;					
				
				//Now update the counts
				$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
				
				if ($sendEmail){
					$productProvider = new ProductProvider($dataConn);
					$criteria = new stdClass();
					$criteria->showAll = true;
					$criteria->noLimit = true;
					$paginationResult = $productProvider->get($criteria);
					$resultArray = $paginationResult->collection;
					foreach($resultArray as $id=>$record){
						$ProductByEFI[$record->efi] = $record->description;
					}
					$NewLevels = "";
					foreach ($updateResults["EFI"] as $EFI=>$EFIInfo){
						$ProductDescription = $ProductByEFI[$EFI];
						$NewLevels .= $ProductDescription." (".$EFI.") ".$EFIInfo."\n\r";
					}
					//if wanted to send notification from dreamhost server
					/*
					$path = getcwd();
					$pathSeparator = "/";
					if (!strpos($path,"xampp")){
						$thisPath = ".:/home/cetdash/pear/share/pear";
						set_include_path($thisPath);
					}else{
						$pathSeparator = "\\";
					}
					*/
					require_once "Mail.php";  
					$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
					$to = ($SecondEmailAddress ? $EmployeeName." <".$EmailAddress.">,".$SecondEmployeeName." <".$SecondEmailAddress.">" : $EmployeeName." <".$EmailAddress.">"); 
					$recipients = $to.",IT Admin <it@cetonline.org>";
					$subject = "Inventory added on ".$_POST["TransferDate"]; 
					$bodyName = $EmployeeName;
					$body = ":\n\r ".($_POST["PurchaseOrder"] ? " Purchase Order: ".$_POST["PurchaseOrder"]."\n\r" : "")." On ".$_POST["TransferDate"]." ".$_POST["WarehouseToLocation"]." warehouse storage location had the following item quantities adjusted:\n\r\n\r".$NewLevels;  
					$host = "smtp.office365.com"; 
					$port = "587"; 
					$username = $_SESSION['AdminEmail']; 
					$password = $_SESSION['AdminAuthenticationCode'];  
					$headers = array (
						'From' => $from,   
						'To' => $to, 
						'Reply-to' => $from,
						'Subject' => $subject
					); 
					$smtp = Mail::factory(
						'smtp',   
						array (
							'host' => $host,     
							'port' => $port,     
							'auth' => true,     
							'username' => $username,     
							'password' => $password
						)
					);  
					$bodyOrg=$body;
					$body = $bodyName.$body;
					$emailRecordId = $productProvider->movementRecordEmail($body,$to,$headers,$from,$username."QQQ".$password);
					if (strtotime($TodaysDate) >= strtotime("6/28/2015")){
						$body = $body."\n\r\n\rTo indicate shipment received: ".$CurrentServer."productReceipt/".$emailRecordId;					
						$body = $body."\n\r\n\rTo note any discrepancies: ".$CurrentServer."productDiscrepancy/".$emailRecordId. "\n\r";					
					}
					$mail = $smtp->send($recipients, $headers, $body); 
					$updateEmailRecordId = $productProvider->movementRecordEmailUpdate($emailRecordId,$mail);
					$body = "<b>".$bodyName."</b>".$bodyOrg."<br><br><br><br clear='all'><table width='100%'><tr><td width='40%' align='center' valign='top'>To indicate shipment received:<br><img src='https://chart.googleapis.com/chart?cht=qr&chs=177x177&choe=UTF-8&chl=https%3A%2F%2Fcetdashboard.info%2Fadmintest%2FproductReceipt%2F".$emailRecordId."'><br><img src='".$CurrentServer."images/thumbsup.png'></td>";
					$body = $body."<td width='60%' align='center' valign='top'>To note any discrepancies:<br><img src='https://chart.googleapis.com/chart?cht=qr&chs=177x177&choe=UTF-8&chl=https%3A%2F%2Fcetdashboard.info%2Fadmintest%2FproductDiscrepancy%2F".$emailRecordId."'><br><img src='".$CurrentServer."images/thumbsdown.png'></td></tr></table>";
					
				}
				$results[0]["mail"] = $body;
				$results[0]["From"] = $updateResults["From"];
				$results[0]["To"] = $updateResults["To"];
			}elseif ($_POST['action'] === 'product-countadjust'){
				//warehouse provider used to update count
				$warehouseProvider = new WarehouseProvider($dataConn);
				$adjustResults = $warehouseProvider->countadjust($_POST, getAdminId());
				foreach  ($adjustResults as $transferRecord){
					$transferRecords[] = $transferRecord["transferRecord"];
				}
                $productProvider = new ProductProvider($dataConn);
				$TransferResults = $productProvider->countadjust($transferRecords, getAdminId());
			
				//Now update the counts
				$updateResults = $warehouseProvider->resetWarehouseCount($TransferResults);
				$results = $updateResults;
            }elseif ($_POST['action'] === 'product-setfillqty'){
				//warehouse provider used to update count
				$warehouseProvider = new WarehouseProvider($dataConn);
				$adjustResults = $warehouseProvider->setfillqty($_POST, getAdminId());
				$results = $adjustResults;		
            }elseif ($_POST['action'] === 'product-physicalcount'){
				//warehouse provider used to update count
				$warehouseProvider = new WarehouseProvider($dataConn);
				$adjustResults = $warehouseProvider->physicalcount($_POST, getAdminId());
				$results = $adjustResults;		
            }elseif ($_POST['action'] === 'product-efimismatch'){
				//warehouse provider used to update count
				$id1 = $_POST['id1'];
				$efi = $_POST['efi'];
				$moveInventory = $_POST['moveInventory'];
				include_once($dbProviderFolder."CSGDailyDataProvider.php");
				$csgDailyDataProvider = new CSGDailyDataProvider($dataConn);
				$criteria = new stdClass();
				$criteria->id1 = $id1;
				$updateResults = $csgDailyDataProvider->getDailyData($criteria);
				$id = $updateResults->id;
				$results = $csgDailyDataProvider->updateCSGDataEFIMisMatch($efi,$id);
				if ($moveInventory=="yes"){
					//Prepare Transfer Data
					$installed_date = $updateResults->installed_dt;
					$installed = explode(" ",$installed_date);
					$installed_dt = date("m/d/Y",strtotime($installed[0]));
					$transferInfo["TransferDate"] = $installed_dt;
					$transferInfo["WarehouseFromLocation"] = $updateResults->crew;
					$transferInfo["WarehouseToLocation"] = "Installed";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$updateResults->siteId.",ProjectID: ".$updateResults->projectId.",Measure: ".$updateResults->meastable;
					$transferInfo["MovementType"] = "Installed - EFI MisMatch Adjusted";
					$transferInfo["Total_".str_replace(".","_",$efi)] = $updateResults->qty;
					
					$productProvider = new ProductProvider($dataConn);
					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					$warehouseProvider = new WarehouseProvider($dataConn);
					//Now update the counts
					$results = $warehouseProvider->updateWarehouseCount($TransferResults);
				}
				$results = $_POST;
				header("HTTP/1.0 201 Created");
			}else if ($_POST['action'] === 'product-update'){
				$newRecord = json_decode($currentRequest->rawData);
				$response->record = false;
				$response->message = "no good";
				if ($response->record){
		            $results = $response->record;
					$results = $newRecord;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->message;
					$results = $newRecord;
					header("HTTP/1.0 409 Conflict");
				}
            } else if ($_POST['action'] === 'clone-product'){
                $productProvider = new ProductProvider($dataConn);
                $productCriteria = new ProductCriteria();
                $productCriteria->id = $_POST['product_id'];
                $productResults = $productProvider->get($productCriteria);
                if (count($productResults->collection) == 1){
                    $productRecord = $productResults->collection[0];
                    $cloneCriteria = new ProductCloneCriteria();
                    $cloneCriteria->record = $productRecord;
                    $cloneCriteria->cloneReferences = $_POST['cloneReferences'];
                    $cloneCriteria->cloneSocialMessages = $_POST['cloneSocialMessages'];
                    $response = $productProvider->cloneProduct($cloneCriteria);
                    if ($response->success && $response->record){
                        $results = $response->record;
                    } else {
                        $results = $response;
                        header("HTTP/1.0 409 Conflict");
                    }
                } else {
                    $results = "Invalid id";
                    header("HTTP/1.0 400 Bad Request");
                }
			}
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'product_update'){
	            $productProvider = new ProductProvider($dataConn);
				$response = $productProvider->update($newRecord, getAdminId());
				include_once("..\cronjobs\FTPtoNeat.php");
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
				// $results ="file exists = ".file_exists("FTPtoNeat.csv");
		}else if (trim($newRecord->action) == 'product_add'){
	            $productProvider = new ProductProvider($dataConn);
				$response = $productProvider->add($newRecord, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else{
            $productProvider = new ProductProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$efi){
						$responseCollection[] = $productProvider->updateDisplayOrder($efi,($displayID+1), getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$efi){
						$responseCollection[] = $productProvider->updateDisplayOrder($efi,0, getAdminId());
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>