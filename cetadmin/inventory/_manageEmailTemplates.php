<?php
include_once($dbProviderFolder."ContentProvider.php");
if (!isset($emailTemplateProvider)){
    $emailTemplateProvider = new EmailTemplateProvider($mysqli);
}
$providerRequest = new GetTemplateRequest();
$providerRequest->getAll = true;
$result = $emailTemplateProvider->get($providerRequest);
?>

<div class="sixteen columns">
    <h3>Manage Email Templates</h3>

    <p>
        <a href="?nav=edit-email-template" id="add-new" class="button-link">Add New</a>
    </p>
    <div>
        <table id="email-templates-table" class="datatables" style="display:none;">
            <thead>
            <tr id="search-inputs">
                <td class="ui-state-default"><input class="auto-watermarked" title="Search Name"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Search Type"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Search Default"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Search Referral Codes"></td>
                <td class="ui-state-default">&nbsp;</td>
            </tr>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Default?</th>
                <th>Referral Codes</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($result->collection as $record){ ?>
                <tr data-id="<?php echo $record->id;?>">
                    <td><a href="?nav=edit-email-template&id=<?php echo $record->id;?>"><?php echo $record->name;?></a></td>
                    <td><?php echo $record->emailType;?></td>
                    <td><?php echo ($record->isDefault) ? "Yes" : "";?></td>
                    <td><?php echo implode(", ", $record->referralCodesArray);?></td>
                    <td><a href="delete-template" class="delete-link do-not-navigate" title="delete">Delete</a></td>
                </tr>
            <?php } // end foreach?>
            </tbody>
        </table>
    </div>
</div>
<div id="dialog-confirm-delete" title="Delete this template?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this template?</p>
</div>
<script type="text/javascript">
    $(function(){
        var tableElement = $("#email-templates-table"),
            asInitVals = new Array(),
            searchInputs = $("#search-inputs input"),
            oTable = tableElement.dataTable({
                "bJQueryUI": true,
                "iDisplayLength":100
            });

        searchInputs.keyup( function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter( this.value, searchInputs.index(this) );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        tableElement.on("click", ".delete-link", function(){
            var clicked = $(this),
                tr = clicked.closest("tr"),
                templateID = tr.attr("data-id"),
                allRows = tableElement.fnGetNodes(),
                selectedNode;

            for ( var i=0 ; i<allRows.length ; i++ )
            {
                if ( $(allRows[i]).attr('data-id') == templateID )
                {
                    selectedNode = allRows[i];
                }
            }
            if (selectedNode){
                $( "#dialog-confirm-delete" ).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Delete": function() {
                            var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEmailTemplates.php",
                                data = {
                                    id: templateID
                                };

                            // delete
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                data: JSON.stringify(data),
                                dataType: "json",
                                contentType: "application/json",
                                statusCode:{
                                    200: function(data){
                                        tableElement.fnDeleteRow(selectedNode);
                                    },
                                    409: function(jqXHR, textStatus, errorThrown){
                                        var data = $.parseJSON(jqXHR.responseText);
                                        if (data.message){
                                            alert(data.message);
                                        } else {
                                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                                        }
                                    }
                                }
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            }
        });

        tableElement.show();
    });
</script>
