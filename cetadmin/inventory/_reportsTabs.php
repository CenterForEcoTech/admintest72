<div id="<?php echo (!$showReportsDropDown ? "reportDropDownItems" : "reportItems");?>">
	<a id="product-transfer_history" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-transfer_history" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Product Transfer History</a><br>
	<a id="product-transfer_leakage" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-transfer_leakage" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Product Transfer Leakage</a><br>
	<a id="report-efimismatch" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-efimismatch" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">EFI MisMatch</a><br>
	<a id="report-inventoryusage" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-inventoryusage" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Inventory Usage</a><br>
	<a id="report-unacknowldgetransfer" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-unacknowledgetransfer" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">UnAcknowledged Transfers</a><br>
	<a id="report-warehousefillqty" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-warehousefillqty" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Warehouse Fill Qty</a><br>
	<?php if (count(array_intersect($adminSecurityGroups,$Security_CommissionsReport_Inventory))){?>
		<a id="report-closurerates" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-closurerates" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">ES Closure Rates</a><br>
		<a id="report-commissions" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=report-commissions" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Commissions Report</a>
	<?php }?>
</div>