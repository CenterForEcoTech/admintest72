<?php

include_once($siteRoot."functions.php");
include_once($dbProviderFolder."ContentProvider.php");
include_once($repositoryApiFolder."files/ImageFileManager.php");

// set up directory location and options (this makes it more portable without copying the class files around)
$urlRoot = $CurrentServer."media/images"; // example only
$directory = $siteRoot."media/images";
$enableThumbnail = true;
$enableMedium = false;
$options = new ImageFileManagerOptions($urlRoot, $directory, $enableThumbnail, $enableMedium);
$imageDataProvider = new ImageLibraryProvider($mysqli, getAdminId());

// get an ImageFileManager for this directory (if unable to read or write to the configured directory, returns null
$imageManager = ImageFileManager::getInstance($options, $imageDataProvider);

// setup merchant image options for convenience

$merchantImageUrlRoot = $CurrentServer."media/images/merchants"; // example only
$merchantImageDirectory = $siteRoot."media/images/merchants";
$enableThumbnail = true;
$enableMedium = false;
$merchantImageOptions = new ImageFileManagerOptions($merchantImageUrlRoot, $merchantImageDirectory, $enableThumbnail, $enableMedium);