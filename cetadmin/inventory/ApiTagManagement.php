<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
include_once("model/_campaignTableRow.php");
include_once("model/_campaignReferenceTableRow.php");
include_once("model/_campaignOrgTableRow.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."CampaignProvider.php");
include_once($dbProviderFolder."CampaignAssociationProvider.php");
include_once($dbProviderFolder."MemberProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        if ($_GET['action'] == "list"){
            $provider = new CampaignProvider($mysqli);
            $criteria = CampaignTableRow::getCriteria($_GET);
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new CampaignTableRow($record, $campaignImageUrlPath);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        } else if ($_GET['action'] == 'get_reference'){
            $campaignResourceProvider = new CampaignResourceProvider($mysqli);
            $criteria = CampaignReferenceTableRow::getCriteria($_GET);
            $paginationResult = $campaignResourceProvider->get($criteria);
            $resultArray = $paginationResult->collection;
            if ($_GET['id'] && count($resultArray)){
                $results = new CampaignReferenceTableRow($resultArray[0]);
            } else {

                $results = array(
                    "iTotalRecords" => $paginationResult->totalRecords,
                    "iTotalDisplayRecords" => $paginationResult->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($resultArray as $record){
                    $results["aaData"][] = new CampaignReferenceTableRow($record);
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
        }else if ($_GET['action'] == 'get_orgs' && $_GET['campaign_id'] > 0){
            $orgProvider = new OrganizationMemberProvider($mysqli);
            $criteria = CampaignOrgTableRow::getCriteria($_GET);
            $paginationResult = $orgProvider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new CampaignOrgTableRow($record);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        } else {
            $tagId = $_GET["tagID"];
            $campaignResourceProvider = new CampaignResourceProvider($mysqli);
            $results = $campaignResourceProvider->getTagReferences($tagId, false);
        }
        break;
    case "POST":
        if (empty($_POST)){
            // no image being uploaded
            $newRecord = json_decode($currentRequest->rawData);
            if ($newRecord->action === 'save-campaign-data'){
                $campaignRecord = CampaignRecord::castAs($newRecord);
                // handle deleting image
                $deleteImage = $newRecord->delete_image === "yes";
                if ($deleteImage){
                    $file_path = $campaignImageFilePath.$campaignRecord->imageFileName;
                    $success = is_file($file_path) && $campaignRecord->imageFileName[0] !== '.' && unlink($file_path);
                    $campaignRecord->imageFileName = "";
                    $campaignRecord->imageUrl = "";
                }

                $provider = new CampaignProvider($mysqli);
                if ($campaignRecord->id){
                    $response = $provider->update($campaignRecord, getAdminId());
                    $results->record = $campaignRecord;
                } else {
                    $response = $provider->add($campaignRecord, getAdminId());
                    $results->record = $response->record;
                }
                if (!$response->success){
                    $results->message = $response->error;
                    header("HTTP/1.0 409 Conflict");
                }
            } else if ($newRecord->action == 'add_org' && $newRecord->orgId && $newRecord->id) {
                $memberId = getMemberId();
                $campaignProvider = new CampaignProvider($mysqli);
                $campaignCriteria = new CampaignCriteria();
                $campaignCriteria->id = $newRecord->id;
                $campaignResults = $campaignProvider->get($campaignCriteria);

                $orgProvider = new OrganizationMemberProvider($mysqli);
                $orgCriteria = new OrganizationProfilesCriteria();
                $orgCriteria->id = $newRecord->orgId;
                $orgResults = $orgProvider->get($orgCriteria);

                if (count($campaignResults->collection) == 1 && count($orgResults->collection) == 1){
                    $provider = new CampaignAssociationProvider($mysqli);
                    $campaignRecord = $campaignResults->collection[0];
                    $orgRecord = $orgResults->collection[0];
                    $response = $provider->addOrganization($campaignRecord, $orgRecord);
                    if ($response->success && $response->affectedRows){
                        $results->numAdded = 1;
                    }
                } else {
                    $results->message = "No charities added";
                    header("HTTP/1.0 400 Bad Request");
                }
            } else if ($newRecord->action == 'remove_org' && $newRecord->orgId){
                $memberId = getMemberId();
                $provider = new CampaignAssociationProvider($mysqli);
                $response = $provider->removeOrganization($newRecord->id, $newRecord->orgId);
                if ($response->success && $response->affectedRows){
                    $results->success = true;
                } else {
                    $results->message = "Org was not removed;";
                }
            }
        } else {
            if ($_POST['action'] === 'save-map-defaults' && $_POST['tagName']){
                $results->success = false;
                $tagName = htmlspecialchars($_POST['tagName']);
                $zoomLevel = $_POST['zoom'];
                $centerLat = $_POST['lat'];
                $centerLong = $_POST['long'];

                $campaignProvider = new CampaignProvider($mysqli);
                $campaignCriteria = new CampaignCriteria();
                $campaignCriteria->setProductName(htmlspecialchars($_POST['tagName']));
                $campaignResults = $campaignProvider->get($campaignCriteria);

                $records = $campaignResults->collection;
                if (count($records) == 1){
                    /* @var $campaignRecord CampaignRecord */
                    $campaignRecord = $records[0];
                    $campaignRecord->mapZoomLevel = is_numeric($zoomLevel) ? $zoomLevel : "";
                    $campaignRecord->mapCenterLat = is_numeric($centerLat) ? $centerLat : "";
                    $campaignRecord->mapCenterLong = is_numeric($centerLong) ? $centerLong : "";
                    $response = $campaignProvider->update($campaignRecord, getAdminId());
                    $results->success = $response->success;
                }
            } else if ($_POST['action'] === 'save-campaign-data'){

                // this includes at least one uploaded file
                $campaignRecord = CampaignRecord::castAs((object)$_POST);
                $tmpFileName = $_FILES['files']['tmp_name'];
                $origFileName = $_FILES['files']['name'];
                if (!strlen($campaignRecord->imageFileName)){
                    // first time ever for this campaign; assign a name now
                    $campaignRecord->imageFileName = "Tag_".urlencode($campaignRecord->name)."_".$origFileName;
                }
                $uploadFilePath = $campaignImageFilePath.$campaignRecord->imageFileName;

                $provider = new CampaignProvider($mysqli);
                if ($campaignRecord->id){
                    $response = $provider->update($campaignRecord, getAdminId());
                    $results->record = $campaignRecord;
                } else {
                    $response = $provider->add($campaignRecord, getAdminId());
                    $results->record = $response->record;
                }
                if (!$response->success){
                    $results->message = $response->error;
                    header("HTTP/1.0 409 Conflict");
                } else {
                    if (!move_uploaded_file($tmpFileName, $uploadFilePath)){
                        trigger_error("Unable to move ".$tmpFileName." to ".$uploadFilePath." (orig file ".$origFileName.")", E_USER_ERROR);
                        $results->message = "There was an error uploading the image";
                    }
                }
            } else if ($_POST['action'] === 'clone-campaign'){
                $campaignProvider = new CampaignProvider($mysqli);
                $campaignCriteria = new CampaignCriteria();
                $campaignCriteria->id = $_POST['campaign_id'];
                $campaignResults = $campaignProvider->get($campaignCriteria);
                if (count($campaignResults->collection) == 1){
                    $campaignRecord = $campaignResults->collection[0];
                    $cloneCriteria = new CampaignCloneCriteria();
                    $cloneCriteria->record = $campaignRecord;
                    $cloneCriteria->cloneReferences = $_POST['cloneReferences'];
                    $cloneCriteria->cloneSocialMessages = $_POST['cloneSocialMessages'];
                    $response = $campaignProvider->cloneCampaign($cloneCriteria);
                    if ($response->success && $response->record){
                        $results = $response->record;
                    } else {
                        $results = $response;
                        header("HTTP/1.0 409 Conflict");
                    }
                } else {
                    $results = "Invalid id";
                    header("HTTP/1.0 400 Bad Request");
                }
            }
        }
        break;
    case "PUT":
        // add tag reference
        $newRecord = json_decode($currentRequest->rawData);
        $tagReference = new TagReference();
        $tagReference->id = $newRecord->id;
        $tagReference->tagID = $newRecord->tagID;
        $tagReference->sortOrder = $newRecord->sortOrder;
        $tagReference->displayText = $newRecord->displayText;
        $tagReference->hoverText = $newRecord->hoverText;
        $tagReference->url = $newRecord->url;
        $tagReference->targetID = $newRecord->targetID;
        $tagReference->isActive = isset($newRecord->status) && $newRecord->status;
        $tagReference->page = $newRecord->page;
        $campaignResourceProvider = new CampaignResourceProvider($mysqli);
        $response = $campaignResourceProvider->saveTagReference($tagReference);
        if ($response->record){
            $results = $response->record;
            header("HTTP/1.0 201 Created");
        } else {
            $results = $response->message;
            header("HTTP/1.0 409 Conflict");
        }
        break;
}
output_json($results);
die();
?>