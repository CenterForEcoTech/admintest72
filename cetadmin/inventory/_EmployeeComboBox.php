<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxEmployee<?php echo $employeeComboBoxSelectID;?>" ).combobox({
				select: function(event, ui){
					<?php if (!$employeeComboBoxFunction){?>
					window.location = "?nav=manage-employees&EmployeeID="+$(ui.item).val();
					<?php }else{
						echo $employeeComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($employeeComboBoxDivColumns ? $employeeComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$employeeComboBoxHideLabel){?>
				<label>Start typing employee name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxEmployee<?php echo $employeeComboBoxSelectID;?> parameters" id="SelectedEmployee<?php echo $employeeComboBoxSelectID;?>">
				<option value="">Select one...</option>
				<?php 
					foreach ($EmployeesActive as $employee=>$employeeInfo){
						if ($EmployeeID == $employeeInfo->id){$DisplayEmployeeName = $employeeInfo->fullName;}
						echo "<option value=\"".$employeeInfo->id."\"".($EmployeeID == $employeeInfo->id ? " selected" : "").">".$employeeInfo->firstName." ".$employeeInfo->lastName."</option>";
					}
				?>
			  </select>
			</div>
		</div>