<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
//error_reporting(1);
//ini_set("display_errors","on");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."EventProvider.php");

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        include_once($dbProviderFolder."MySqliEventManager.php");
        include_once($indexProviderFolder."IndexedEventManager.php");

        $IndexingProviderURL = $Config_IndexingURLPrivate;
        $IndexName = $Config_IndexingEventsIndex;

        $eventProvider = new EventProvider($mysqli);
        $mysqlEventManager = new MySqliEventManager($mysqli);
        $indexedEventManager = new IndexedEventManager($IndexingProviderURL, $IndexName);

        $newRecord = json_decode($currentRequest->rawData);
        $eventRecord = $eventProvider->getEventById($newRecord->id);
        $arrayOfVenues = $eventProvider->getMerchantsForEvent($newRecord->id);

        $deleteResponse = $mysqlEventManager->deleteEvent($eventRecord, $arrayOfVenues, 0, getAdminId());
        if ($deleteResponse->success){
            $deleteDocResponse = $indexedEventManager->deleteEvent($eventRecord, $arrayOfVenues, 0, getAdminId());
        }

        if ($deleteResponse->success){
            $results = $deleteResponse;
            header("HTTP/1.0 200 Success");
        } else {
            $results = $deleteResponse;
            header("HTTP/1.0 409 Conflict");
        }
        break;
    case "GET":
        echo "GET is not supported";
        break;
    case "POST":
        echo "POST is not supported";
        break;
    case "PUT":
        // add
        include_once($dbProviderFolder."MySqliEventManager.php");
        include_once($indexProviderFolder."IndexedEventManager.php");
        $eventProvider = new EventProvider($mysqli);
        $mysqlEventManager = new MySqliEventManager($mysqli);
        $indexedEventManager = new IndexedEventManager($IndexingProviderURL, $IndexName);

        $updateInfo = json_decode($currentRequest->rawData);
        $eventRecord = $eventProvider->getEventById($updateInfo->EventID);

        $eventRecord->title = $updateInfo->Event_Title;
        $eventRecord->setStartDate($updateInfo->Event_Date, $updateInfo->Event_StartTime);
        $eventRecord->setEndDate($updateInfo->Event_EndDate, $updateInfo->Event_EndTime);
        $eventRecord->venue->customerChoicePercentage = $updateInfo->Event_MerchantSecondaryPercentage;
        $eventRecord->expectedAttendance = $updateInfo->Event_OrgAnticipatedCount;
        $eventRecord->whyHostThisDescription = $updateInfo->Event_OrgWhyYou;
        $eventRecord->description = $updateInfo->Event_Description;
        $eventRecord->customAttendLink = $updateInfo->customAttendLink;
        $eventRecord->isNationalEvent = $updateInfo->isNational;
        $eventRecord->onlineLocationLabel = $updateInfo->onlineLocationLabel;
        $eventRecord->onlineLocationLink = $updateInfo->onlineLocationLink;
        $eventRecord->suppressCharitySelectionForm = $updateInfo->suppressCharitySelectionForm;
        $eventRecord->suppressPrintFlier = $updateInfo->suppressPrintFlier;

        if ($updateInfo->isFlatDonation){
            $eventRecord->venue->isFlatDonation = true;
            $eventRecord->venue->flatDonationActionPhrase = htmlspecialchars($updateInfo->donationActionPhrase);
            $eventRecord->venue->unitDescriptor = htmlspecialchars($updateInfo->unitDescriptor);
            $eventRecord->venue->donationPerUnit = is_numeric($updateInfo->donationPerUnit) ? $updateInfo->donationPerUnit : 0;
            $eventRecord->venue->customerChoicePercentage = 0;
        } else {
            $eventRecord->venue->isFlatDonation = false;
            $eventRecord->venue->flatDonationActionPhrase = "";
            $eventRecord->venue->unitDescriptor = "";
            $eventRecord->venue->donationPerUnit = "";
        }

        $updateResponse = $mysqlEventManager->updateEvent($eventRecord, 0, getAdminId());
        if ($updateResponse->success){
            $eventRecord->commaDelimitedTagNames = $updateInfo->tagNamesArray;
            $eventProvider->addTagAssociationByTagName($eventRecord);

            MySqliHelper::log_event_action($mysqli, $eventRecord->id,'Updated by Admin',0,getAdminId());
            $eventsToUpdate = $eventProvider->getEventByIdExploded($eventRecord->id);
            $logEntries = array();
            foreach ($eventsToUpdate as $eventToUpdate){
                /* @var $eventToUpdate EventRecord */
                $updateDocResponse = $indexedEventManager->updateEvent($eventToUpdate, 0, getAdminId());
                $logEntries[] = 'Indexed docid='.$updateDocResponse->insertedId;
            }
            foreach($logEntries as $logEntry){
                $logged = MySqliHelper::log_event_action($mysqli, $eventRecord->id, $logEntry, 0, getAdminId());
            }
        }

		
        if ($updateResponse->success){
            $results = "Success";
            header("HTTP/1.0 201 Created");
        } else {
            $results = $updateResponse->error;
            header("HTTP/1.0 409 Conflict");
        }
        break;
}
output_json($results);
die();
?>