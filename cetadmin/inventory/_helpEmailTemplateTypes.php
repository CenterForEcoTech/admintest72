<h4>Registration</h4>

<p>Variable names are static and should not expect to change much.</p>

<h4>TransactionReceiptCC (Customer Choice)</h4>
<p>Static Variables (that won't change much):</p>
<dl>
    <dt>{emailToName}</dt>
    <dd>e.g., Sally Smith, or "Friend"</dd>
    <dt>{eligibleAmount}</dt>
    <dd>The sale amount</dd>
    <dt>{generatedAmount}</dt>
    <dd>Donation amount</dd>
    <dt>{eventURL}</dt>
    <dd>URL to the detail page (Not available for the no-event template type)</dd>
    <dt>{loginURL}</dt>
    <dd>URL to the login page</dd>
    <dt>{SiteName}</dt>
    <dd>Configured site name, e.g., "CETDashboard" with a lowercase "t"</dd>
    <dt>{publicProfileURL}</dt>
    <dd>URL to merchant public profile</dd>
    <dt>{chooseCharityUrl}</dt>
    <dd>URL to the Choose Charity page (user does not need to be logged in)</dd>
    <dt>{donationForSaleSnippet}</dt>
    <dd>e.g., "$10.00 generated a donation of $2.50" or "2 beer(s) generated a donation of $2.50"</dd>
</dl>
<p>Dynamic Variables (available on the venue object, which on the back end is of type MerchantRecord). Note that these use "dot notation" to traverse nested objects. Thus, venue.name = the name property on the venue object, and venue.primaryContact.firstName refers to the first name of the primary contact of that venue.</p>
<dl>
    <dt>{venue.pid}</dt>
    <dd>the Permalink ID that goes to public profile (e.g., http://causetown.org/ct/{venue.pid})</dd>
    <dt>{venue.name}</dt>
    <dd>Merchant name</dd>
    <dt>{venue.address}</dt>
    <dd>Merchant street address</dd>
    <dt>{venue.city}</dt>
    <dd>Merchant City</dd>
    <dt>{venue.state}</dt>
    <dd>Merchant State</dd>
    <dt>{venue.postalCode}</dt>
    <dd>Merchant Zip</dd>
    <dt>{venue.phone}</dt>
    <dd>Merchant main phone</dd>
    <dt>{venue.website}</dt>
    <dd>Merchant website URL</dd>
    <dt>{venue.customTransactionEmailText}</dt>
    <dd>The custom text the merchant has entered that is to be added to the receipt when the user's email is supplied at point of sale.</dd>
</dl>
<p>There are more properties available on the MerchantRecord, but are omitted here because they seem irrelevant to the context.</p>

<h4>TransactionReceiptFC (Featured Cause)</h4>
<p>This type uses all the same variables as the Customer Choice option, with the addition of a "cause" object, whose properties are identical to those of venue with very rare exception.</p>
<dl>
    <dt>{cause.name}</dt>
    <dd>Nonprofit name</dd>
    <dt>{cause.address}</dt>
    <dd>Nonprofit street address</dd>
    <dt>{cause.city}</dt>
    <dd>Nonprofit City</dd>
    <dt>{cause.state}</dt>
    <dd>Nonprofit State</dd>
    <dt>{cause.postalCode}</dt>
    <dd>Nonprofit Zip</dd>
    <dt>{cause.phone}</dt>
    <dd>Nonprofit main phone</dd>
    <dt>{cause.website}</dt>
    <dd>Nonprofit website URL</dd>
</dl>

<h4>MerchantEventPublished (event created)</h4>
<p>This type has access to a wide variety of objects, and a handful of "handy" variables:</p>
<dl>
    <dt>{firstName}</dt>
    <dd>Same as {venue.primaryContact.firstname}</dd>
    <dt>{datePhrase}</dt>
    <dd>e.g., "on December 22" or "from December 29 through January 3"</dd>
    <dt>{startDate}</dt>
    <dd>The event start date in 9/28/2013 format</dd>
    <dt>{endDate}</dt>
    <dd>The event end date in 9/28/2013 format</dd>
    <dt>{donationPhrase}</dt>
    <dd>e.g., "15% of sales to Red Cross or to any charity customers choose"</dd>
    <dt>{eventUrl}</dt>
    <dd>The event's URL</dd>
    <dt>{eventLink}</dt>
    <dd>The event's URL enclosed in an anchor tag (the displayed text is the URL)</dd>
    <dt>{loginUrl}</dt>
    <dd>The login url for the website.</dd>
    <dt>{SiteName}</dt>
    <dd>The Site's name (e.g., "CETDashboard") -- to enable white labeling.</dd>
    <dt>{event.title}</dt>
    <dd>The event title. The entire EventRecord is accessible, as well as most of its sub-objects. Note that {event.venue} is NOT the same as the {venue} object, and is not guaranteed to be available.</dd>
    <dt>{venue.primaryContact.email}</dt>
    <dd>The merchant's primary contact email. The venue object's underlying model is the Merchant Record, whose properties as documented above are all available.</dd>
    <dt>{cause.name}</dt>
    <dd>The cause name. This is the same as the {event.cause} object, and is therefore limited to simple details like name, pid, and ein.</dd>
</dl>