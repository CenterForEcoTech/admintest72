<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
//error_reporting(1);
//ini_set("display_errors","on");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."EventTemplateProvider.php");
include_once($dbProviderFolder."EventTemplatePremiumProvider.php");
include_once("model/_eventTemplatePremiumTableRow.php");
include_once($dbProviderFolder."AdminEventProvider.php");
include_once($siteRoot."_setupEventByLocationProvider.php");
$provider = new EventTemplateProvider($mysqli);

function updateIndexedEvents($templateId){
    global $mysqli,$repositoryApiFolder, $Config_IndexingURLPrivate, $Config_IndexingEventsIndex, $isDevMode, $indexedEventProvider;
    if ($templateId && isset($indexedEventProvider)){
        $criteria = new AdminEventCriteria();
        $criteria->upcomingOnly = false;
        $criteria->eventTemplateId = $templateId;
        $criteria->size = 0; // get all of them

        /* @var $eventProvider EventProvider */
        $eventProvider = new AdminEventProvider($mysqli);
        $response = $eventProvider->get($criteria);

        $eventsToUpdate = $response->collection;

        if (count($eventsToUpdate)){
            $indexProviderFolder = $repositoryApiFolder."indexedSearchProvider/";
            include_once($indexProviderFolder."IndexedEventManager.php");
            $IndexingProviderURL = $Config_IndexingURLPrivate;
            $IndexName = $Config_IndexingEventsIndex;
            $indexedEventManager = new IndexedEventManager($IndexingProviderURL, $IndexName);

            $memberID = 0; // admin
            foreach ($eventsToUpdate as $eventToUpdate){
                /* @var $eventToUpdate EventRecord */
                if ($isDevMode){
                    getEventLogger()->log_action($eventToUpdate->id, 'Calling updateEvent on '.get_class($indexedEventManager), $memberID, getAdminId());
                }

                /* @var $eventManager EventManager */
                $indexedEventManager->updateEvent($eventToUpdate, $memberID, getAdminId());

            }
        }
    } else {
        trigger_error("ApiEventTemplates.php updateIndexedEvents called without valid templateId", E_USER_ERROR);
    }
}

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        /*
        $newRecord = json_decode($currentRequest->rawData);
        $response = $provider->deleteTemplate($newRecord->id);

        if ($response->success){
            $results = $response;
            header("HTTP/1.0 200 Success");
        } else {
            $results = $response;
            header("HTTP/1.0 409 Conflict");
        }*/
        break;
    case "GET":
        if ($_GET['action'] == 'get_premium'){
            $eventTemplatePremiumProvider = new EventTemplatePremiumProvider($mysqli);
            $criteria = EventTemplatePremiumTableRow::getCriteria($_GET);
            $paginationResult = $eventTemplatePremiumProvider->get($criteria);
            $resultArray = $paginationResult->collection;
            if ($_GET['id'] && count($resultArray)){
                $results = new EventTemplatePremiumTableRow($resultArray[0]);
            } else {

                $results = array(
                    "iTotalRecords" => $paginationResult->totalRecords,
                    "iTotalDisplayRecords" => $paginationResult->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($resultArray as $record){
                    $results["aaData"][] = new EventTemplatePremiumTableRow($record);
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
        } else {
            header("HTTP/1.0 400 Bad Request");
            echo "GET request not understood.";
        }
        break;
    case "POST":
        $newRecord = json_decode($currentRequest->rawData);
        if ($newRecord->action == 'make-default' && is_numeric($newRecord->id)){
            include_once($dbProviderFolder."DbConfigProvider.php");
            $configProvider = new DbConfigProvider($mysqli);
            $response = $configProvider->setConfig("Config_DefaultCreateEventTemplate", $newRecord->id, true, "Default Create Event Template", 5);
            if ($response->success){
                $results = $response;
                header("HTTP/1.0 200 Success");
            } else {
                $results = $response;
                header("HTTP/1.0 409 Conflict");
            }
        } else if ($newRecord->action == 'post-premium'){
            $eventTemplatePremiumProvider = new EventTemplatePremiumProvider($mysqli);
            $contract = new EventTemplatePremium($newRecord);
            $response = $eventTemplatePremiumProvider->save($contract);
            if ($response->record){
                $results = $response->record;
                header("HTTP/1.0 200 Success");
            } else {
                $results = $response->message;
                header("HTTP/1.0 409 Conflict");
            }
        }  else if ($newRecord->action === 'clone-template'){
            $eventTemplate = $provider->getTemplateByID($newRecord->template_id);
            if ($eventTemplate->id == $newRecord->template_id){
                $cloneCriteria = new EventTemplateCloneCriteria();
                $cloneCriteria->record = $eventTemplate;
                $cloneCriteria->clonePremiums = $newRecord->clonePremiums;
                $response = $provider->cloneTemplate($cloneCriteria);
                if ($response->success && $response->record){
                    $results = $response->record;
                } else {
                    $results = $response;
                    header("HTTP/1.0 409 Conflict");
                }
            } else {
                $results = "Invalid id";
                header("HTTP/1.0 400 Bad Request");
            }
        } else if ($_POST["id"] && isset($_POST["image_id"])){
            $templateId = $_POST["id"];
            $updateResponse = $provider->setImageId($templateId, $_POST["image_id"]);
            if ($updateResponse->success && $updateResponse->affectedRows > 0){
                $results->success = true;
            } else {
                $results->success = false;
                $results->message = "failed to update image";
            }
        } else if ($newRecord->action === 're-index-events' && $newRecord->id){
            updateIndexedEvents($newRecord->id);
        } else {
            header("HTTP/1.0 409 Conflict");
        }
        break;
    case "PUT":
        // add
        $newRecord = json_decode($currentRequest->rawData);
        $template = new EventTemplate($newRecord);
        $response = $provider->saveTemplate($template, getAdminId());

        if ($response->success){
            $results = $response->record;
            header("HTTP/1.0 201 Created");
        } else {
            $results = $response;
            header("HTTP/1.0 409 Conflict");
        }
        break;
}
output_json($results);
die();
?>