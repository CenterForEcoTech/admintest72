<?php 
								$deactiveYearMonth = date("Y-m",strtotime($WarehouseRecordByName[$CrewID]->endDate));
								//Don't give contract commission in month crewId left
								$deactiveDate = (int)strtotime($deactiveYearMonth."-01");
								$thisMonthYearDate = (int)strtotime($MonthYear."-01");
								$deactivatedMonth = false;
								if ($deactiveDate > 0 && $deactiveDate <= $thisMonthYearDate){
									$deactivatedMonth = true;
								}
							?>
							<tr>
								<td colspan="3" class="ReportHeaderName">
									<span style="font-size:14pt;"><?php echo $WarehouseNameByShortName[strtolower($ESName[1])]->description;?> <?php echo date("M Y",strtotime($MonthYear."-01"));?></span><br>
									Total Commission <b>$<?php echo money($PriorCommissionData[$ES][$MonthYear]["TotalCommission"]);?></b><br>
									<?php if (!$deactivatedMonth){?>
										<?php echo ($HEAJobDetailsPageNum > 1 ? "Page ".$HEAJobDetailsPageNum : "");?>
										<div <?php echo $HEAJobDetails;?>>
											<div style="border:0pt solid black;width:80px;text-align:center;float:left;">&nbsp;</div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><?php echo date("M",strtotime($MonthYear."-01"));?></div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;">YTD</div><br clear="all">
											<div style="border:0pt solid black;width:80px;text-align:center;float:left;">HEA</div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo $PriorCommissionData[$ES][$MonthYear]["HEACount"];?></div></div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"];?></div></div><br clear="all">
											<div style="border:0pt solid black;width:80px;text-align:center;float:left;">Billed Jobs</div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo $PriorCommissionData[$ES][$MonthYear]["BilledContracts"];?></div></div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["BilledCount"];?></div></div><br clear="all">
											<div style="border:0pt solid black;width:80px;text-align:center;float:left;">Closing Rate</div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo round(($PriorCommissionData[$ES][$MonthYear]["BilledContracts"]/$PriorCommissionData[$ES][$MonthYear]["HEACount"])*100);?>%</div></div>
											<div style="border:0pt solid black;width:60px;text-align:center;float:left;"><div class="disabled" style="width:60px;"><?php echo round(($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["BilledCount"]/$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"])*100);?>%</div></div><br clear="all">
										</div>
									<?php }?>
									<!--Start Date: <?php echo MySQLDate($WarehouseNameByShortName[strtolower($ESName[1])]->startDate);?>-->
								</td>
								<td colspan="3">
									<?php 
										if ($deactivatedMonth){
											$MonthYearInfoEmpty = true;
											$MonthYearInfoEmptyText = " ";
											echo "<div style'text-align:center;'>".$WarehouseNameByShortName[strtolower($ESName[1])]->description." last day ".date("m/d/Y",strtotime($WarehouseRecordByName[$CrewID]->endDate))."</div><br clear='all'>";
										}else{
											$MonthYearInfoEmptyText = "";
									?>
										<div class="disabled">$<?php echo money($PriorCommissionData[$ES][$MonthYear]["ContractsFinal"]);?></div><div class="disabledText"> Total Contracts Completed</div><br clear="all">
										<div class="disabled">$<?php echo money($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"]);?></div><div class="disabledText"> Contracts Commission</div><br clear="all">
										<div class="PriorCalculations">
											Prior Contracts Commission: $<?php echo money($PriorCommissionData[$ES][$MonthYear]["ContractsCommissionPrior"]);?><br>
										</div>
										<?php echo (!$PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibleByDate"] ? "<br><span class='CommissionNotEligible'>Less than 90 Days</span><br>" : "");?>
									<?php }
									?>
								</td>
								<td colspan="3">
									<div class="disabled"><?php echo $PriorCommissionData[$ES][$MonthYear]["BulbCount"];?></div><div class="disabledText"> Total Bulbs Installed x $<?php echo money($PriorCommissionData[$ES][$MonthYear]["BulbFactor"]);?></div><br clear="all">
									<div class="disabled">$<?php echo money($PriorCommissionData[$ES][$MonthYear]["BulbCommission"]);?></div><div class="disabledText"> Bulb Commission</div><br clear="all">
									<div class="PriorCalculations">
										Prior Bulb Factor: $<?php echo money($PriorCommissionData[$ES][$MonthYear]["BulbFactorPrior"]);?><br>
										Prior Bulb Commission: $<?php echo money($PriorCommissionData[$ES][$MonthYear]["BulbCommissionPrior"]);?>
									</div>
									<?php echo (!$PriorCommissionData[$ES][$MonthYear]["YTDBulbCommissionEligibleByBulb"] ? "<br><span class='CommissionNotEligible'>Less than 17 Bulbs/Visit</span><br>" : "");?>
									<?php echo (!$WarehouseRecordByName[$CrewID]->lead ? ($PriorCommissionData[$ES][$MonthYear]["BulbBonus"] ? "<br><span class='CommissionBulbBonus'>Qualifies for Bulb Commission Bonus!</span>" : "") : "");?>
								</td>
							</tr>
