<?php
ini_set("display_errors", "on"); error_reporting(1);

$MovementType = explode("-",$nav);
$WarehouseName = ($_GET['WarehouseName'] ? $_GET['WarehouseName'] : 'Residential');
$physicalcountdate = $_GET['physicalcountdate'];
include_once($siteRoot."_setupDataConnection.php");
include_once("model/_warehouseRow.php");

include_once($dbProviderFolder."InventoryProvider.php");

	$warehouseProvider = new WarehouseProvider($dataConn);
	$criteria = WarehouseRow::getCriteria($_GET);
	$paginationResult = $warehouseProvider->get($criteria); 
	$resultArray = $paginationResult->collection;
	//print_pre($resultArray);

	
	$criteria = new stdClass();
	$productProvider = new ProductProvider($dataConn);
	$products = $productProvider->get();
	$productsArray = $products->collection;
	foreach ($productsArray as $id=>$productDetail){
		$ProductDescription[$productDetail->efi] = $productDetail->description;
		$EFIList[] = $productDetail->efi;
	}
	
	$criteria = new stdClass();
	$autofillqtyResult = $warehouseProvider->getfillqty(); 
	$autofillqty = $autofillqtyResult->collection;
	
//	print_pre($autofillqty);
	foreach ($autofillqty as $autofilldata){
		$warehouse = $autofilldata->warehouseName;
		$efi = $autofilldata->efi;
		$qty = $autofilldata->qty;
		$AutoFillQty[$warehouse][$efi] = $qty;
	}
	//get exclusion list
	foreach ($resultArray as $WarehouseID=>$WarehouseDetail){
		if ($WarehouseDetail->excludeFromCountId){
			$excludeList[]=$WarehouseDetail->name; //used in javascript below
		}
		$orderWareHouseByID[$WarehouseDetail->name]=$WarehouseDetail->displayOrderId;
	}
	$PackOrCase = "Pack";
	if ($MovementType[1] == "received"){
		$FromWarehouseDefault = "EFI Shipment Received";
		$ToWarehouseDefault="Residential";
		$PackOrCase = "Case";

	}
	if ($MovementType[1] == "setfillqty"){
		$FromWarehouseDefault = $WarehouseName;
		$ToWarehouseDefault="Residential";
	}
	if ($MovementType[1] == "physicalcount"){
		$FromWarehouseDefault = $WarehouseName;
		$ToWarehouseDefault="Residential";
		$criteria = new stdClass();
		$criteria->warehouseName = $WarehouseName;
		//GetLastPhysicalCount
		$lastPhysicalCountResult = $warehouseProvider->getlastphysicalcount($criteria); 
		$lastphysicalCount = $lastPhysicalCountResult->collection;	
		foreach ($lastphysicalCount as $lastPhysicalCountData){
			$warehouse = $lastPhysicalCountData->warehouseName;
			$efi = $lastPhysicalCountData->efi;
			$qty = $lastPhysicalCountData->qty;
			$lastPhysicalCountAsOfDate = $lastPhysicalCountData->asOfDate;
			$PhysicalCount[$efi] = $qty;
		}
		
		//GetAddedCount since last physical count
		$criteria->warehouseTo = $WarehouseName;
		//$criteria->movementType = "Transfer";
		$criteria->transferDateGreaterThan = $lastPhysicalCountAsOfDate;
		$criteria->transferDateUpTo = MySQLDateUpdate($physicalcountdate);
		$criteria->notFrom = "Adjust Manual Count";
		$inventoryAddedResult = $productProvider->getProductMovmentHistory($criteria);
		$inventoryAdded = $inventoryAddedResult->collection;
		foreach ($inventoryAdded as $inventoryAddedData){
			$inventoryAddedCount[$inventoryAddedData->efi] = $inventoryAddedCount[$inventoryAddedData->efi]+$inventoryAddedData->units;
		}
		//print_pre($inventoryAdded);
		
		//Get installed count since last physical count
		$criteria = new stdClass();
		$criteria->warehouseFrom = $WarehouseName;
		$criteria->notTo = "Adjust Manual Count";
		//$criteria->movementType = "Transfer";
		$criteria->transferDateGreaterThan = $lastPhysicalCountAsOfDate;
		$criteria->transferDateUpTo = MySQLDateUpdate($physicalcountdate);
		$inventoryRemovedResult = $productProvider->getProductMovmentHistory($criteria);
		$inventoryRemoved = $inventoryRemovedResult->collection;
		foreach ($inventoryRemoved as $inventoryRemovedData){
			$inventoryRemovedCount[$inventoryRemovedData->efi] = $inventoryRemovedCount[$inventoryRemovedData->efi]+$inventoryRemovedData->units;
		}
		
		foreach ($PhysicalCount as $EFI=>$qty){
			$calculatedCount[$EFI] = ($qty+$inventoryAddedCount[$EFI]-$inventoryRemovedCount[$EFI]);
		}

	}
	
	$FromWarehouseExcludeList = array('Discontinued','Staff Purchase','Credit Issued');	
	if (strtolower($_SESSION["AdminUsername"]) == "heather.mccreary@cetonline.org"){
		$ToWarehouseExcludeList = array('EFI Shipment Received','New Construction');	//Allow Heather to move items into Installed
	}else{
		$ToWarehouseExcludeList = array('EFI Shipment Received','Installed','New Construction');	
	}
	$FromWarehouse = $warehouseProvider->createSelectPulldown($resultArray,"WarehouseFromLocation",$FromWarehouseDefault,$FromWarehouseExcludeList);
	$ToWarehouse = $warehouseProvider->createSelectPulldown($resultArray,"WarehouseToLocation",$ToWarehouseDefault,$ToWarehouseExcludeList);
?>
<script type="text/javascript">
function getOnHandForWarehouse(data,warehouse){
	var detailCount = data,
		warehouse = warehouse.toUpperCase(),
		data = 0;

	if (detailCount){
		data=detailCount[warehouse];
	}
	if(typeof data === 'undefined'){
		data = 0;
	}
	return "<span title='"+warehouse+"' class='warehouseCount'>"+data+"</span>";
}
function getAutoFillForWarehouse(warehouse,efi){
var detailFillQty = <?php echo json_encode($AutoFillQty);?>,
		warehouse = warehouse.toUpperCase(),
		efi = typeof efi !== 'undefined' ? efi : false;
		efi = efi,
		data = 0;
	if (efi){
		if (detailFillQty[warehouse]){
			data=detailFillQty[warehouse][efi];
		}
		if(typeof data === 'undefined'){
			data = 0;
		}
		return "<span title='"+warehouse+" fill qty' id=fillqty"+efi.replace(".","_")+" class='warehouseCount'>"+data+"</span>";
	}else{
		var efiList = <?php echo json_encode($EFIList);?>;
		$.each(efiList,function(key,efiVal){
			var efi = efiVal,
				fillqtyefi = "#fillqty"+efi.replace(".","_");
			if (detailFillQty[warehouse]){
				data=detailFillQty[warehouse][efi];
			}
			if(typeof data === 'undefined'){
				data = 0;
			}
			$(fillqtyefi).text(data);
		});
	}
}

function format ( d ) {
	// `d` is the original data object for the row
	var detailCount = d.detailCount,
		detailDisplayWarehouse = "",
		detailDisplayCount = "",
		detailDisplayRow = "",
		totalCountDisplayRow = "",		
		totalCount = 0,
		efi = d.efi;
	$.each( detailCount, function( key, value ) {
		detailDisplayRow =  detailDisplayRow+'<tr>'+
			'<td align="right">'+key+'</td>'+
			'<td>'+value+'</td>'+
			'</tr>';
		totalCount = (totalCount+parseInt(value));
		
	});
	if (totalCount > 0){
		totalCountDisplayRow = '<tr>'+
			'<td align="right">Total</td>'+
			'<td>'+totalCount+'</td>'+
		'</tr>';
	}
	
	return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
		'<colgroup><col><col><col></colgroup>'+
		detailDisplayRow+
		totalCountDisplayRow+
		'</table>';
}



    $(function(){
		<?php if ($nav == "product-received"){?>
			$("#warehouse-list .WarehouseSelectFrom").hide();	
		<?php }else{?>
			$("#warehouse-list .PurchaseOrder").hide();	
		<?php }?>
		var transferMessage = $("#transferMessage");
		transferMessage.dialog({
			autoOpen: false,
			height: 400,
			width:400,
			modal:true,
			beforeClose: function(){
				<?php if ($MovementType[1] == "received"){echo "location.reload();";}?>
				var data = $("#transferMessagePrint").html();
				<?php if (strtotime($TodaysDate) >= strtotime("6/28/2015")){?>
				if (data){
					var newWin = window.open();
					newWin.document.write(data);
					newWin.document.close();
					newWin.focus();
					newWin.print();
					newWin.close();
				}
				<?php }?>
			}
		});
		
		
		var CloseFunction = function(){
			<?php if ($MovementType[1] == "received"){?>	
			CreateDataTable('Residential');
			<?php }?>
		};
		
		var dateClass = $(".date");
		dateClass.datepicker({
			maxDate:0
			<?php if ($MovementType[1] == "physicalcount"){?>	
				,onSelect: function(date) {
					window.location.href='?nav=product-physicalcount&WarehouseName=<?php echo $WarehouseName;?>&physicalcountdate='+date;
				}
			<?php }?>
		});
		dateClass.width(80);
		
		var physicalCount = <?php echo (count($PhysicalCount) ? json_encode($PhysicalCount): "{}");?>;
		var calculatedCount = <?php echo (count($calculatedCount) ? json_encode($calculatedCount): "{}");?>;
	
        var productListTable = $("#product-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>inventory/ApiProductManagement.php",
            lastRequestedCriteria = null,
			
			CreateDataTable = function (warehouseName) {
				var warehouseText = $("#WarehouseFromLocation option[value='"+warehouseName+"']").text();
				$(".warehouseNameSpan").html(warehouseText.replace("CET_","#"));
				table = productListTable.DataTable({
					dom: 'T<"clear">lfrtip',
					tableTools: {
						"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
									{
										"sExtends": "print"
									},
								]						
					},				
					"bJQueryUI": true,
					"bAutoWidth": false,
					"sAjaxSource": apiUrl,
					"iDisplayLength": 50,
					"bSearchable":false,
					"bFilter":false,
					"bPaginate": false,
					"aaSorting": [ [0, "asc"]],
					"aoColumns" :[
						{
							"sClass": 'details-control',
							"mData": "displayOrderId",
							"mRender": function ( data, type, full ) {
								return '';
							},
							"sWidth": "5px"
						},
						{
							"mData": "efi",
							"mRender": function ( data, type, full ) {
								return '<a href="?nav=product-management&EFI='+data+'">'+data+'</a>';
							},
							"sWidth": "85px"
						},
						{
							"mData": "description",
							"sWidth": "20%"
						},
						{
							"mData": "unitsPer<?php echo $PackOrCase;?>",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								return '<span data-unitsperpack="'+data+'">'+data+'<span><span class="unitsIndicated" style="display:none;">0</span>';
						   }
						},
						{
							"mData": "casesId",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								return '<input style="width:30px;height:20px;" type="text" class="productCases" value="0">';
						   }
						},
						{
							"mData": "unitsId",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								return '<input style="width:50px;height:20px;" type="text" class="productUnits" value="0">';
						   }
						},
						{
							"mData": "totalUnitsId",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								var thisEfi = data.replace('_','.');
								return '<input style="width:30px;display:none;" type="text" name="Total_'+data+<?php if ($MovementType[1] == "physicalcount"){?>'QQQ'+(calculatedCount[thisEfi] ? calculatedCount[thisEfi] : 0)+<?php }?>'" class="totalUnits" value="0"><span class="totalUnits">'+(calculatedCount[thisEfi] ? calculatedCount[thisEfi] : 0)+'</span>';
							}
						}
						<?php if ($MovementType[1] != "countadjust" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
						,{
							"mData": "detailCount",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								return getOnHandForWarehouse(data,warehouseName);
							}
						},
						{
							"mData": "totalCount",
							"sWidth": "10%"
						}
						<?php }?>
						<?php if ($MovementType[1] == "setfillqty" || $MovementType[1] == "transfer"){?>
						,{
							"mData": "efi",
							"sWidth": "10%",
							"mRender": function ( data, type, full ) {
								return getAutoFillForWarehouse(warehouseName,data);
							}
						}
						<?php }?>
					],
	//                "aoColumnDefs": [
	//					{ "bVisible": false, "aTargets": [ 1 ] }
	 //               ],
					"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "action", "value": "list" } );
					},
					"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						lastRequestedCriteria = aoData;
						oSettings.jqXHR = $.ajax( {
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": lastRequestedCriteria,
							"success": fnCallback
						} );
					},
					"createdRow": function ( row, data, index ) {
						//console.log(data.efi);
						var minimumThreshold = Math.floor(data.minimumQty/3)
						<?php 
							if ($Config_ThresholdQtyCompare == 'Residential'){
								$countCompare = 'data.detailCount["RESIDENTIAL"]';
							}else{
								$countCompare = 'data.totalCount';							
							}
						?>
						var compareCount = parseInt(<?php echo $countCompare;?>);
						//console.log(NonotuckData);
						$.each( data, function( key, value ) {
							//console.log(key +":"+ value);
						});
						if (!data.canBeOrderedId){
							$(row).addClass( 'cannotbereordered' );
						}
						<?php if ($MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?> 
						if (compareCount < minimumThreshold){
							$(row).addClass( 'reorder' );
						}
						<?php }?>
					}
				});
			};
		
		var	casesClass = $(".productCases"),
			unitsClass = $(".productUnits");
	
		$('#product-list tbody').on('keyup', 'input', function () {
			var $this = $(this),
				thisEFI = $this.attr('data-productefi'),
				unitsperpack = parseInt($this.closest("tr").find('span').attr('data-unitsperpack')),
				unitsperpackspan = $this.closest("tr").find('span.unitsIndicated'),
				totalspan = $this.closest("tr").find('span.totalUnits'),
				warehouseSpan = $this.closest("tr").find('span.warehouseCount'),
				warehouseCount = warehouseSpan.html(),
				warehouseName = warehouseSpan.attr('title'),
				totalInput = $this.closest("tr").find('input.totalUnits'),
				thisClass = $this.attr('class'),
				casesInput = parseInt($this.closest("tr").find('input').val()),
				excludeList = ['<?php echo implode("','",$excludeList);?>'];
				
			if (thisClass=='productUnits'){
				unitsInput = parseInt($this.val());
				unitsperpackspan.html($this.val());
			}else{
				unitsInput = parseInt(unitsperpackspan.html());
			}

			//unitsInput = parseInt(unitsInput);

			if (isNaN(casesInput)){
				casesInput=0;
			}
			if (isNaN(unitsInput)){
				unitsInput=0;
			}
			totalUnits = (unitsperpack*casesInput)+unitsInput;
			totalspan.html(totalUnits);
			totalInput.val(totalUnits);
			<?php if ($MovementType[1] != "received" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
				if (totalUnits > warehouseCount && excludeList.indexOf(warehouseName)<1){
					alert('Request Transfers More Units Than Currently In '+warehouseName);
				}
			<?php }?>
		});		
		
		var AdjustmentResultsTable = $("#AdjustmentResultsTable");
		var AdjustmentResults = AdjustmentResultsTable.DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 20
		});
		var FromWarehouse = $("#WarehouseFromLocation"),
			ToWarehouse = $("#WarehouseToLocation");
		
		$('button').click( function() {
			//disable the button
			var $this = $(this),
				processTransfer = false,
				warehouseName = FromWarehouse.val(),
				warehouseText = $("#WarehouseFromLocation option[value='"+warehouseName+"']").text();
			$this.html("processing...");
			$this.attr("disabled", "disabled");
			<?php if ($MovementType[1] == "countadjust"){?>
			var r = confirm("Have you printed this out and confirmed the values?\nReminder: Any row set with Total Units of 0 will be set to an inventory level of 0\r\nYou are about to set the inventory levels for:\n"+warehouseText);
			if (r == true) {
				processTransfer = true;
			} else {
				processTransfer = false;
			}			
			<?php }else{?>
				processTransfer = true;
			<?php }?>
			
			if (FromWarehouse.val() == ToWarehouse.val()){
				var warehouseConfirm = confirm("Confirm to Transfer Items From and To the Same Location");
				processTransfer = false;
				$this.removeAttr("disabled"); 
				$this.html("Save");		
				if (warehouseConfirm == true){
					processTransfer = true;
				}
			}
			
			//check to see if explanation is required
			if ($("#AdjustMessage").css('display') == "block"){
				processTransfer = false;
				if ($("#AdjustMessageTextArea").val().trim() == ''){
					alert('you must provide an explanation');
				}else{
					processTransfer = true;
				}
			}
			
			if (processTransfer){
				var datastring = JSON.parse(JSON.stringify($('input, select').serialize()));
				//console.log(datastring);
				$.ajax({
					type: "POST",
					url: apiUrl,
					data: datastring,
					dataType: "json",
					success: function(data) {
						var resultText= "",
							alertClass = "info";
							
						if (data.NoProduct){
							resultText = "No Items were Transfered&nbsp;<br>";
							alertClass = "alert";
							ClassRemove = "info";
						}else{
							alertClass = "info";
							ClassRemove = "alert";
							<?php if ($MovementType[1] == "countadjust"){?>
								var index,
									AMCTotalCount =data.AMC,
									WarehouseTotalCount =data.Warehouse,
									SummaryRow = data.Summary,
									SummaryRows = SummaryRow.split(",");
									resultText = resultText+"<bR>The Warehouse now has the following on hand count:<br>"+WarehouseTotalCount+"<br>";
									$.each(SummaryRows,function(key,row){
										var rowdata = row.split("|");
											AdjustmentResults.rows.add( [[
												rowdata[0],
												rowdata[1],
												rowdata[2],
												rowdata[3],
												rowdata[4]
											]] ).draw();
									});
							<?php }elseif ($MovementType[1] == "setfillqty"){?>
								AdjustmentResults.clear().draw();

								var WarehouseTotalCount =data.warehouse,
									SummaryRow = data.summary;
									DetailRow = data.details;
									DetailRows = DetailRow.split(",");
									$.each(DetailRows,function(key,row){
										var rowdata = row.split("|"),
											fillqtyefi = "#fillqty"+rowdata[0].replace(".","_");
		
											$(fillqtyefi).text(rowdata[2]);
											AdjustmentResults.rows.add( [[
												rowdata[0],
												rowdata[1],
												rowdata[2]
											]] ).draw();
									});
								resultText = resultText+"<bR>"+WarehouseTotalCount+" has set the following autofill qty levels:<br>"+SummaryRow+"<br>";
							<?php }elseif ($MovementType[1] == "physicalcount"){?>
								AdjustmentResults.clear().draw();
								$("#mostRecentPhysicalCount").html($("#TransferDate").val());
								//console.log(data);
								var inventoryAddedCount = <?php echo (count($inventoryAddedCount) ? json_encode($inventoryAddedCount) : "{}");?>;
								var inventoryRemovedCount = <?php echo (count($inventoryAddedCount) ? json_encode($inventoryRemovedCount) : "{}");?>;

								var WarehouseName =data.warehouse,
									SummaryRow = data.summary;
									DetailRow = data.details;
									DetailRows = DetailRow.split(",");
									$.each(DetailRows,function(key,row){
										var rowdata = row.split("|"),
											thisEfi = rowdata[0],
											priorCount = (physicalCount[thisEfi] ? physicalCount[thisEfi] : 0),
											addedCount = (inventoryAddedCount[thisEfi] ? inventoryAddedCount[thisEfi] : 0),
											removedCount = (inventoryRemovedCount[thisEfi] ? inventoryRemovedCount[thisEfi] : 0),
											calculated = (calculatedCount[thisEfi] ? calculatedCount[thisEfi] : 0);

											AdjustmentResults.rows.add( [[
												WarehouseName,
												thisEfi,
												rowdata[1],
												priorCount,
												addedCount,
												removedCount,
												calculated,
												rowdata[2],
												rowdata[3]
											]] ).draw();
									});
								resultText = resultText+"<bR>"+WarehouseName+" has set the following autofill qty levels:<br>"+SummaryRow+"<br>";
							<?php }else{?>
								var index,
									fromTotalCount =data[0].From,
									toTotalCount =data[0].To;

									for (index = 0; index < data.length; ++index) {
										var fromWarehouse = data[index].warehouseFrom,
											toWarehouse = data[index].warehouseTo,
											transferDate = data[index].transferDate;
										resultText = resultText + data[index].units+" units of "+data[index].efi+"<Br>";
									}						
										resultText = "The following items were transfered from "+fromWarehouse+" to "+toWarehouse+" on "+transferDate+":<br>"+resultText;
									if (fromWarehouse != "EFI Shipment Received"){
										resultText = resultText+"<bR>This adjusts "+fromWarehouse+" to the following on hand count:<br>"+fromTotalCount+"<br>";
									}
									if (toWarehouse !="Staff Purchase"){
										resultText = resultText+"<bR>This adjusts "+toWarehouse+" to the following on hand count:<br>"+toTotalCount+"<br>";
									}
									//commented out to include printing for Residential
									//if ($("#WarehouseToLocation").val() != 'Residential'){
										if (data[0].mail){
											$("#transferMessagePrint").html(data[0].mail.replace(/\n/g,"<br>"));
										}
									//}
							<?php }?>
						}
						$("#transferMessageText").removeClass(ClassRemove).addClass(alertClass).html(resultText);
						transferMessage.dialog("open");
						//$("#general-validation-message").delay(2000).fadeOut(4000);
						//reenable button
						$this.removeAttr("disabled"); 
						$this.html("Save");
						//reset form
						$(".productCases").val("0");
						$(".productUnits").val("0");
						$(".totalUnits").html("0");
						$(".totalUnits").val("0");
						$(".unitsIndicated").html("0");
						$("#WarehouseFromLocation").val("<?php echo $WarehouseName;?>");
						$("#WarehouseToLocation").val("<?php echo $WarehouseName;?>");
						<?php if ($MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
							productListTable.dataTable().fnDestroy();
							CreateDataTable("<?php echo $WarehouseName;?>");
						<?php }?>
					},
					error: function(){
						  alert('error handing here');
						$this.removeAttr("disabled"); 
						$this.html("Save");
					}
				});	
			}else{
				$this.removeAttr("disabled"); 
				$this.html("Save");
			}//end process transfer
			return false;
		} );		
		
		// Add event listener for opening and closing details
		$('#product-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data();
				if (!dataObj.detailCount){
					dataObj.detailCount = {'Item not currently in any warehouse':''};
				}
				if (dataObj.detailCount){
					row.child( format(row.data()) ).show();
					tr.addClass('shown');
				}
			}
		} );
		
		ToWarehouse.on('change', function () {
				showExplanationText();
			var $this = $(this),
				warehouseName = $this.val();

			getAutoFillForWarehouse(warehouseName);

		});
		FromWarehouse.on('change', function () {
				showExplanationText();
			var $this = $(this);
			productListTable.dataTable().fnDestroy();
			var warehouseName = $this.val();

			<?php if ($MovementType[1] == "setfillqty"){?>
				AdjustmentResults.clear().draw();
				window.location.href='?nav=product-setfillqty&WarehouseName='+warehouseName;

			<?php }?>
			<?php if ($MovementType[1] == "physicalcount"){?>
				AdjustmentResults.clear().draw();
				window.location.href='?nav=product-physicalcount&WarehouseName='+warehouseName;

			<?php }?>
			CreateDataTable($this.val());
		});	
		
		var showExplanationText = function(){
			var fromWarehouseName = FromWarehouse.val(),
				toWarehouseName = ToWarehouse.val();
			if (fromWarehouseName == "Adjust Manual Count" || toWarehouseName == "Adjust Manual Count"){
				$("#AdjustMessage").show();
			}else{
				$("#AdjustMessage").hide();
				$("#PurchaseOrder").val('');
			}
		};
		
		$("#AdjustMessageTextArea").on('keyup',function(){
			var $this = $(this);
				$("#PurchaseOrder").val($this.val());
		});
		
		CreateDataTable('<?php echo $WarehouseName;?>');		
    });
</script>
<h3><?php if ($MovementType[1] == "countadjust"){echo "Count Adjustment";}elseif ($MovementType[1] == "setfillqty"){echo "Set Fill Qty";}elseif ($MovementType[1] == "physicalcount"){echo "Set Physical Count";}else{echo " Product ".ucfirst($MovementType[1]);}?></h3>
<div id="AdjustMessage" style="display:none;">
	You must include an explanation as to why you are manually adjusting the count.  Please provide enough detail that will be clear months from now:<br>
	<textarea id="AdjustMessageTextArea" class="ten columns" title="explanation goes here"></textarea>
</div>
<div id="transferMessage"><span id="transferMessageText"></span><span id="transferMessagePrint" style="display:none;"></span></div>
<table id="warehouse-list">
    <thead>
    <tr>
        <th class="WarehouseSelect WarehouseSelectFrom"><?php echo ($MovementType[1] != "countadjust" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount" ? "From " : "");?>Warehouse</th>
		<?php if ($MovementType[1] != "countadjust" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
			<th class="WarehouseSelect">To Warehouse</th>
			<th class="PurchaseOrder">Purchase Order</th>
		<?php }?>
		<?php if ($MovementType[1] != "setfillqty"){?>
			<th><?php echo ($MovementType[1] == "countadjust" ? "Day After Count" : ucfirst($MovementType[1]));?> Date</th>
		<?php }?>
    </tr>
    </thead>
    <tbody>
		<td class="WarehouseSelect WarehouseSelectFrom">
			<input type="hidden" name="action" value="<?php echo ($nav == "product-received" ? "product-transfer" : $nav);?>">
			<?php echo $FromWarehouse;?>
		</td>
		<?php if ($MovementType[1] != "countadjust" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
			<td class="WarehouseSelect">
				<?php echo $ToWarehouse;?>
			</td>
			<td class="PurchaseOrder">
				<input type="text" name="PurchaseOrder" id="PurchaseOrder" value="">
			</td>
		<?php }?>
		<td align="center"<?php if ($MovementType[1] == "setfillqty"){?> style="display:none;"<?php }?>>
			<input type="text" class="date" name="TransferDate" id="TransferDate" value="<?php echo ($MovementType[1] == "physicalcount" ? $physicalcountdate : $TodaysDate);?>">
		</td>
		
		<?php if (!$ReadOnlyTrue){?>
			<td><button id="SaveButton">Save</button></td>
		<?php }?>
	</tbody>
</table>
<table id="product-list" class="hover"<?php if ($MovementType[1] == "physicalcount" && !$physicalcountdate){echo " style='display:none;'";}?>>
    <thead>
    <tr>
		<th><div class="warehouseNameSpan" style="display:none;"></div>Storage</th>
        <th style="min-width:70px;">EFI</th>
        <th>Description</th>
        <th>Units/<?php echo $PackOrCase;?></th>
        <th><?php echo $PackOrCase;?>s</th>
        <th>Units</th>
        <th><?php echo ($MovementType[1] == "countadjust" || $MovementType[1] == "setfillqty" || $MovementType[1] == "physicalcount" ? "Total" : "Transfer");?> Units</th>
		<?php if ($MovementType[1] != "countadjust" && $MovementType[1] != "setfillqty" && $MovementType[1] != "physicalcount"){?>
			<th>Units In Warehouse</th>
			<th>Stock on Hand</th>
		<?php }?>
		<?php if ($MovementType[1] == "setfillqty"){?>
			<th>Current Fill To Qty</th>
		<?php }?>
		<?php if ($MovementType[1] == "transfer"){?>
			<th>Fill To Qty</th>
		<?php }?>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<?php if ($MovementType[1] == "countadjust"){?>
	<br clear="all">
	<hr>
	<h3>Adjustment Summary</h3>
	<table id="AdjustmentResultsTable">
		<thead>
			<tr><th>Warehouse</th><th>EFI</th><th>Description</th><th>Units Difference</th><th>Adjusted Date</th></tr>
		</thead>
		<tbody>
		
		</tbody>
	</table>
<?php }?>
<?php if ($MovementType[1] == "setfillqty"){?>
	<br clear="all">
	<hr>
	<h3>Auto Fill Qty</h3>
	<table id="AdjustmentResultsTable">
		<thead>
			<tr><th colspan="3"><span class="warehouseNameSpan"></span>  Updated: <?php echo $TodaysDate;?></th></tr>
			<tr><th>ITEM#</th><th>DESCRIPTION</th><th>FILL QTY</th></tr>
		</thead>
		<tbody>
			<?php
				foreach ($AutoFillQty[$WarehouseName] as $EFI=>$qty){
					echo "<tr><td>".$EFI."</td><td>".$ProductDescription[$EFI]."</td><td>".$qty."</td></tr>";
				}
			?>
		
		</tbody>
	</table>
<?php }?>
<?php if ($MovementType[1] == "physicalcount"){?>
	<br clear="all">
	<hr>
	<h3>Calculated vs Physical Summary</h3>
	<table id="AdjustmentResultsTable">
		<thead>
			<tr><th>Warehouse</th><th>EFI</th><th>Description</th><th>Prior Physical Count as of <?php echo MySQLDate($lastPhysicalCountAsOfDate);?></th><th>Added to Warehouse since <?php echo MySQLDate($lastPhysicalCountAsOfDate);?></th><th>Removed from Warehouse since <?php echo MySQLDate($lastPhysicalCountAsOfDate);?></th><th>Currently Calculated Count</th><th>This Physical Count <div id="mostRecentPhysicalCount">(Not Yet Entered)</div></th><th>Variance</th></tr>
		</thead>
		<tbody>
			<?php
				foreach ($PhysicalCount as $EFI=>$qty){
					echo "<tr><td>".$WarehouseName."</td><td>".$EFI."</td><td>".$ProductDescription[$EFI]."</td><td>".$qty."</td><td>".$inventoryAddedCount[$EFI]."</td><td>".$inventoryRemovedCount[$EFI]."</td><td>".$calculatedCount[$EFI]."</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
				}
			?>
		
		</tbody>
	</table>
<?php }?>
