<?php
if (isset($campaignRecord) && $campaignRecord->id > 0){
    /* @var $campaignRecord CampaignRecord */
    include_once($repositoryApiFolder."DataContracts.php");
?>
<h3>Reference Links</h3>
<p>
    <a href="#addReference" id="add-reference-button" class="button-link do-not-navigate">+ Add Tag Reference</a>
    <span class="help">Click on a row to edit.</span>
</p>
<table id="tag-reference-table">
    <thead>
    <tr>
        <th>Page</th>
        <th>Sort Order</th>
        <th>Display Text</th>
        <th>Hover Text</th>
        <th>URL</th>
        <th>Target ID</th>
        <th>Active?</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="hidden" id="edit-reference-form" title="Add/Edit Tag Reference">
    <form>
        <h4>Add or Edit Tag Reference</h4>
        <input type="hidden" name="id" class="editable" value="0" data-default-value="0">
        <input type="hidden" name="tagID" value="<?php echo $campaignRecord->id;?>">
        <fieldset style="background: #E5F9FF;font-size:12px;float:right;">
            <legend>Page Help</legend>
                <div><b>Event Template</b> = Event Template Resource</div>
                <div><b>Campaign</b> = Campaign Resource</div>
                <div><b>Campaign - Related</b> = Campaign Related Link</div>
        </fieldset>
        <label>
            <div>Page:</div>
            <select name="page">
                <?php foreach(CampaignResourceProvider::$PAGES_ARRAY as $page) { ?>
                    <option value="<?php echo $page;?>"><?php echo $page;?></option>
                <?php } ?>
            </select>
        </label>
        <label>
            <div>Sort Order:</div>
            <input type="text" title="Sort Order" class="auto-watermarked editable" name="sortOrder" value="0" data-default-value="0">
        </label>
        <label>
            <div>Display Text:</div>
            <input type="text" title="Display Text" class="auto-watermarked editable" name="displayText">
        </label>
        <label>
            <div>Hover Text:</div>
            <input type="text" title="Hover Text" class="auto-watermarked editable" name="hoverText">
        </label>
        <label>
            <div>URL:</div>
            <input type="text" title="http://www.domain.com" class="auto-watermarked editable" name="url">
        </label>
        <label>
            <div>Target ID: <span class="help">for the window/tab that the link opens</span></div>
            <input type="text" title="_blank" class="auto-watermarked editable" name="targetID">
        </label>
        <label>
            <input type="checkbox" name="status" value="1" data-default-value="1" checked> Is Active?
        </label>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        var referenceDataTable = $("#tag-reference-table"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiTagManagement.php",
            oTable = referenceDataTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aaSorting": [ ],
                "aoColumns" :[
                    { "mData": "page" },
                    { "mData": "sortOrder", "sWidth": "20px" },
                    { "mData": "displayText" },
                    { "mData": "hoverText" },
                    { "mData": "url" },
                    { "mData": "targetID", "sWidth": "20px" },
                    { "mData": "status", "sWidth": "20px" }
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 1 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "tagID", "value": "<?php echo $campaignRecord->id;?>" } );
                    aoData.push( { "name": "action", "value": "get_reference" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            }),
            editReferenceDataDialog = $("#edit-reference-form"),
            editReferenceDataForm = editReferenceDataDialog.find("form"),
            clearFormData = function(){
                editReferenceDataForm.find(".editable").each(function(i,item){
                    var element = $(item),
                        defaultValue = element.attr("data-default");
                    if (defaultValue){
                        element.val(defaultValue);
                    } else {
                        element.val("");
                    }
                });
                editReferenceDataForm.find("input[name='status']").prop("checked", true);
            };

        editReferenceDataDialog.dialog({
            autoOpen: false,
            height: 600,
            width:650,
            modal:true,
            buttons: {
                "Save": function(){
                    $.ajax({
                        url: apiUrl,
                        type: "PUT",
                        data: JSON.stringify(editReferenceDataForm.serializeObject()),
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data){
                            if (data.message){
                                alert(data.message);
                            }
                            oTable.fnReloadAjax();
                            clearFormData();
                            editReferenceDataDialog.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    editReferenceDataDialog.dialog("close");
                }
            },
            close: function(){
                clearFormData();
            }
        });

        referenceDataTable.on("click", "tr", function(e){
            var row = $(this),
                rowId = row.attr("id"),
                dataId = rowId.replace("reference-", "");
            editReferenceDataForm.find("input[name='id']").val(dataId);
            $.ajax({
                url: apiUrl,
                type: "GET",
                data: {
                    id: dataId,
                    action: "get_reference"
                },
                success: function(data){
                    editReferenceDataForm.find("select[name='page']").val(data.page);
                    editReferenceDataForm.find("input[name='sortOrder']").val(data.sortOrder);
                    editReferenceDataForm.find("input[name='displayText']").val(data.displayText);
                    editReferenceDataForm.find("input[name='hoverText']").val(data.hoverText);
                    editReferenceDataForm.find("input[name='url']").val(data.url);
                    editReferenceDataForm.find("input[name='targetID']").val(data.targetID);
                    editReferenceDataForm.find("input[name='status']").prop("checked", data.status);

                    editReferenceDataDialog.dialog("open");
                }
            });
        });

        $("#add-reference-button").click(function(e){
            e.preventDefault();
            editReferenceDataDialog.dialog("open");
        });
    });
</script>
<?php } // is relevant ?>