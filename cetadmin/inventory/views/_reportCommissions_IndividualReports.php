<?php if ($MonthForReport != ""){?>
	<style>
	.reportTable {border:1pt solid black;}
	.ReportHeaderName {text-align:center;}
	.HeaderLabel {background-color:#F5F5F5;width:100%;border:0pt solid black;color:#7A7A7A;}
	.HeaderValue {background-color:white;width:100%;border:0pt solid red;}
	.HeaderRow {border:0pt solid red;}
	table.dataTable td {padding:0pt !important}
	.ContractType {position:relative;float:left;border:1pt solid black;border-bottom: 0pt;border-right:0pt;font-size:8pt;box-shadow: 2px 2px 2px #000; width:15%;color:#7A7A7A;}
	.ContractDetailsDate {position:relative;float:right;width:75%;padding-right:5px;color:#7A7A7A;border:0pt solid red;}
	.ContractDetailsDiv {position:relative;float:left;width:45%;padding-right:5px;color:#7A7A7A;text-align:center;}
	.ContractValue {color:#000;}
	.Details_AS {background-color:#D6EAFF;border-bottom:1pt solid black;height:35px;margin-bottom:2pt;}
	.Details_INS {background-color:#EAD6FF;border-bottom:1pt solid black;height:35px;margin-bottom:2pt;}
	.Details_TSTAT {background-color:#DBFF94;border-bottom:1pt solid black;height:35px;margin-bottom:2pt;}
	.CommissionNotEligible {background-color:white;color:red;font-weight:bold;}
	.CommissionBulbBonus {background-color:white;color:green;font-weight:bold;}
	.disabled {
		background-color: #eee;
		border-spacing:2px;
		border:1pt solid gray;
		color:rgb(84, 84, 84);
		width:95px;
		height:18px;
		text-align:center;
		float:left;
		margin-right:2px;
	}	
	.disabledFormula {width:50px;}
	.disabledDetails {display:none;}
	.disabledText {float:left;font-size:10pt;}
	.PriorCalculations {display:none;}
	@media all {
		.page-break	{ display: none; }
	}

	@media print {
		.page-break	{ position:relative;display: block; page-break-before: always; }
		.DTTT_container {display:none;}
		.dataTables_info {display:none;}
	}
</style>
<button id="printAllIndividualReports">Print All Individual Reports</button>
<?php 
//print_pre($ESDataContractReport);
	ksort($ESDataContractReport);
	//Getlist of BulbData ES 
	foreach ($BulbData as $CrewID=>$MonthData){
		$ESWithBulbData[$CrewID]=1;
	}
	ksort($ESWithBulbData);
	//print_pre($ESWithBulbData);
	foreach ($ESDataContractReport as $ES=>$MonthData){
		$HEAJobDetailsPageNum = 1;
		$ESName = explode("QQQ",$ES);
		$CrewID = strtoupper(str_replace("XX","",$ESName[0]));
		$CrewParts = explode("_",$CrewID);
		$CrewIDSort = $CrewID;
		if (strlen($CrewParts[1]) < 3){
			$CrewIDSort = str_replace("_","_0",$CrewID);
		}
		$ESWithContractData[$CrewID] = 1;
		if ($ESWithBulbData[$CrewID]){unset($ESWithBulbData[$CrewID]);}
		$esDataFound = false;
		$MonthYearInfoEmpty = false;

		$OneMonthAfterWarehouseEndDate = date("Y-m",strtotime($WarehouseRecordByName[$CrewID]->endDate." + 1 month"));
		$OneMonthAfterEndDate = (int)strtotime($OneMonthAfterWarehouseEndDate."-01");
		foreach ($MonthData as $MonthYear=>$MonthYearInfo){
			$ShowReport = true;
			if ($OneMonthAfterEndDate > 1 && strtotime($MonthYear."-01") > $OneMonthAfterEndDate){
				$ShowReport = false;
			}
			if (strtotime($MonthYear."-01") == strtotime($MonthForReport) && $ShowReport){
				$esDataFound = true;
				include('views/_reportCommissions_IndividualReports_DetailReport.php');
			}//end if in timeframe
		}//end foreach MonthData
		if (!$esDataFound){
			$ShowReport = true;
			$MonthYear = date("Y-m",strtotime($MonthForReport));
			$MonthYearInfoEmpty = true;
			if ($OneMonthAfterEndDate > 1 && strtotime($MonthYear."-01") > $OneMonthAfterEndDate){
				$ShowReport = false;
			}
			if ($ShowReport){
				include('views/_reportCommissions_IndividualReports_DetailReport.php');
			}
		}

	}//end foreach ESDataContractReport
	//pick up ES that only do bulb and no contract work
	
	foreach ($ESWithBulbData as $CrewID=>$item){
		$ESFullName = $WarehouseRecordByName[$CrewID]->description;
		$ESFullNameParts = explode(" ",$ESFullName);
		$ES = "XX".$CrewID."QQQ".strtoupper($ESFullNameParts[0])." ".strtoupper(substr($ESFullNameParts[1],0,1));
		$HEAJobDetailsPageNum = 1;
		$ESName = explode("QQQ",$ES);
		$CrewParts = explode("_",$CrewID);
		$CrewIDSort = $CrewID;
		if (strlen($CrewParts[1]) < 3){
			$CrewIDSort = str_replace("_","_0",$CrewID);
		}
		$esDataFound = false;
		$OneMonthAfterWarehouseEndDate = date("Y-m",strtotime($WarehouseRecordByName[$CrewID]->endDate." + 1 month"));
		$OneMonthAfterEndDate = (int)strtotime($OneMonthAfterWarehouseEndDate."-01");
		
		
//		echo "<hr>".$CrewID."</hr>";
//		echo $ES;
//		print_pre($PriorCommissionData);
		$ShowReport = true;
		$MonthYear = date("Y-m",strtotime($MonthForReport));
		$MonthYearInfoEmpty = true;
		if ($OneMonthAfterEndDate > 1 && strtotime($MonthYear."-01") > $OneMonthAfterEndDate){
			$ShowReport = false;
		}
		if ($ShowReport){
			include('views/_reportCommissions_IndividualReports_DetailReport.php');
		}
	}
	//print_pre($ESWithContractData);
	//print_pre($ESWithBulbData);
}//end if $MonthForReport
?>