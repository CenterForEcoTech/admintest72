<h3>Manage Image Catalogs</h3>
<p>
    <a href="?nav=edit-catalog" id="add-catalog-button" class="button-link">+ Add Catalog</a>
</p>
<table id="catalog-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Title</th>
        <th># Merchants</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var catalogListTable = $("#catalog-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiImageCatalog.php",
            lastRequestedCriteria = null,
            oTable = catalogListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "iDisplayLength": 50,
                "aaSorting": [ [0, "desc"]],
                "aoColumns" :[
                    {
                        "mData": "id",
                        "mRender": function ( data, type, full ) {
                            return '<a href="?nav=edit-catalog&id='+data+'">'+data+'</a>';
                        },
                        "sWidth": "50px"
                    },
                    {
                        "mData": "name",
                        "sWidth": "150px"
                    },
                    {"mData": "title"},
                    {"mData": "numMerchants"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0, 3 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "action", "value": "list" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": lastRequestedCriteria,
                        "success": fnCallback
                    } );
                }
            });
    });
</script>