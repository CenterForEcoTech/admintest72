<?php
ini_set('memory_limit', '2048M');
error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."WarehouseProvider.php");
include_once($dbProviderFolder."ProductProvider.php");
include_once($dbProviderFolder."CSGDailyDataProvider.php");

$DateGrouping = ($_GET['DateGrouping'] ? $_GET['DateGrouping'] : "ByMonth");
$ColumnGrouping = ($_GET['ColumnGrouping'] ? $_GET['ColumnGrouping'] : "All");
$MonthForReport = ($_GET['MonthForReport'] ? $_GET['MonthForReport'] : "");
$ShowPriorCalculations = ($_GET['ShowPriorCalculations'] > 0 ? true : false);
$UsePriorCalculations = ($_GET['UsePriorCalculations'] > 0 ? true : false);
$UseCalendarYTD = true;
$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	$ProductsByDescription[$record->description] = $record->efi;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}
}
//echo "<br>MemoryUsage1:".memory_get_usage();
$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$NameParts = explode(" ",$record->description);
	$ShortName = $NameParts[0]." ".substr($NameParts[1],0,1);
	$WarehouseByName[strtoupper($record->name)] = $ShortName;
	$WarehouseByShortName[$ShortName] = $record->name;
	$WarehouseNameByShortName[strtolower($ShortName)] = $record;
	$WarehouseRecordByName[strtoupper($record->name)] = $record;
	if ($record->displayOrderId){
		$ActiveWarehouseByName[]=$ShortName;
		$ActiveWarehouseByID[]=strtoupper($record->name);
	}
}
//print_pre($ActiveWarehouseByName);

//Use Fiscal Year
$ThisMonth = (int)date("m");
$ThisYear = date("Y");
$ThisFiscalYear = ($_GET["FiscalYear"] ? $_GET["FiscalYear"] : 0);
//special case for November
if ($ThisMonth == 11){
	$ThisFiscalYear=1;	
	$FiscalYearStart = ($ThisYear);
	$FiscalYearEnd = $ThisYear+1;
}else{
	$FiscalYearStart = ($ThisMonth > 10 ? $ThisYear : ($ThisYear-1) );
	$FiscalYearEnd = ($ThisMonth <= 10 ? $ThisYear : ($ThisYear+1) );
}
$FiscalStart = "11/1/".($FiscalYearStart-$ThisFiscalYear);
$FiscalEnd = "10/31/".($FiscalYearEnd-$ThisFiscalYear);
$StartDate = $FiscalStart;
$EndDate = $FiscalEnd;
if ($ThisMonth < 2){$ThisYear = $ThisYear-1;}
$CalendarStartDate = "01/01/".$ThisYear;
$CalendarEndDate = "12/31/".$ThisYear;

//Get CommissionData
$criteria = new stdClass();
 //Commissions are based on Calendar
$EarliestStartDate = (strtotime($FiscalStart) < strtotime($CalendarStartDate) ? $FiscalStart : $CalendarStartDate);
$LatestEndDate = (strtotime($FiscalEnd) > strtotime($CalendarEndDate) ? $FiscalEnd : $CalendarEndDate);
$criteria->startDate = MySQLDateUpdate($EarliestStartDate);
$criteria->endDate = MySQLDateUpdate($LatestEndDate);
//print_pre($criteria);
$paginationResult = $warehouseProvider->getCommissions($criteria);
$resultArray = $paginationResult->collection;
$CurrentCalendarMonth = date("n");
$CurrentCalendarYear = ($CurrentCalendarMonth < 2 ? (date("Y")-1) : date("Y"));
$CurrentCalendarMonth = ($CurrentCalendarMonth < 2 ? 13 : $CurrentCalendarMonth);

foreach ($resultArray as $result=>$record){
	$ES = $record["GHSInventoryWareHouseCommissions_ES"];
	$thisCrewID = $record["GHSInventoryWareHouseCommissions_CrewID"];
	$LockedDateActual = $record["GHSInventoryWareHouseCommissions_LockedDate"];
	$LockedDate = date("Y-m",strtotime($record["GHSInventoryWareHouseCommissions_LockedDate"]));
	$LockedDateYear = date("Y",strtotime($record["GHSInventoryWareHouseCommissions_LockedDate"]));
	$LockedDates[$LockedDate] = 1;
	foreach ($record as $fieldName=>$value){
		$FieldName = str_replace("GHSInventoryWareHouseCommissions_","",$fieldName);
		$PriorCommissionData[$ES][$LockedDate][$FieldName] = $value;
		if ($LockedDateYear == $CurrentCalendarYear){
			$CalendarYTDTotals[$thisCrewID][$FieldName][$LockedDate] = $value;
		}
	}
	//$LockedDateChronology[$ES][strtotime($LockedDateActual)] = $LockedDate;
}
//print_pre($PriorCommissionData);
//echo array_sum($CalendarYTDTotals["CET_18QQQRENEE S"]["HEACount"]);
$CheckingPriorMonth = date("Y-m",strtotime(date()." -2 months"));
if (date("d") < 2 && array_key_exists($CheckingPriorMonth,$LockedDates)){
	$LockedDates[date("Y-m",strtotime(date()." -1 months"))] = 1;
}
//print_pre($LockedDates);

//Get CommissionNotes
$paginationResult = $warehouseProvider->getCommissionNotes($FiscalStart);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CommissionNote = $record["GHSInventoryWareHouseCommissionsNotes_Note"];
	$CommissionLastEditedBy = $record["GHSInventoryWareHouseCommissionsNotes_AddedByAdmin"];
}
//print_pre($PriorCommissionData);
$HEACount = 0;
$BulbVisit = 0;
$BilledContracts = 0;
$BulbCount = 0;
//echo "<br>MemoryUsage2:".memory_get_usage();
//Get BulbReport Data
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$criteria = new stdClass();
$EarliestStartDate = (strtotime($StartDate) < strtotime($CalendarStartDate) ? $StartDate : $CalendarStartDate);
$LatestEndDate = date("m/t/Y", strtotime(date("m/d/Y")." -1 month"));
//((strtotime($EndDate) > strtotime($CalendarEndDate) ? $EndDate : $CalendarEndDate);
$criteria->startDate = MySQLDateUpdate($EarliestStartDate);
$criteria->endDate = MySQLDateUpdate($LatestEndDate);
//print_pre($criteria);
$bulbData = $CSGDailyDataProvider->getDailyData($criteria);
//print_pre($bulbData);
foreach($bulbData as $id=>$record){
	$BulbMonthYear = date("Y-m",strtotime($record->installed_dt));
	$BulbMonthYears[$BulbMonthYear] = 1;
	$crewId = strtoupper($record->crew);
	$deactiveDate = (int)strtotime($WarehouseRecordByName[$crewId]->endDate);
	$actualInstallDate = (int)strtotime($record->installed_dt);
	$IncludeBulbsForBulbCount = true;
	//Don't count bulbs installed after the crewId endDate
	if ($deactiveDate > 0 && $deactiveDate < $actualInstallDate){
		$IncludeBulbsForBulbCount = false;
	}
	if ($IncludeBulbsForBulbCount){
		$partId = $record->partId;
		switch ($partId){
			case "WFP2a":
				$partId = "Aerator";
				break;
			case "AeratorFlip_22":
				$partId = "Aerator";
				break;
			case "TrickleStarPowerStrip":
				$partId = "PowerStrip";
				break;
			case "ShowerheadEarth_17":
				$partId = "Showerhead";
				break;
			case "HWellThermostat_Furnace":
				$partId = "Tstat";
				break;
			case "HWellThermostat_Combo":
				$partId = "Tstat";
				break;
			case "HWellThermostat_Boiler":
				$partId = "Tstat";
				break;
			default:
				if (substr($partId,0,3) == "Fee"){
					if ($partId == "FeeHEAVisit" || $partId == "FeeHEAAddUnitVisit"){
						$partId = "HEA";
					}
					if ($partId == "FeeShvVisit"){
						$partId = "SHV";
					}	
					if ($partId == "FeeThermostatInstall"){
						$partId = "TstatVisit";
					}	
				}else{
					$partId = "Bulbs";
				}
				break;
		}
		$BulbData[$crewId][$BulbMonthYear][$partId] = bcadd($BulbData[$crewId][$BulbMonthYear][$partId],$record->qty,0);
	}
}
//print_pre($BulbData["CET_114"]);
$CommissionISM = array("Bulbs","Aerator","Tstat","Showerhead","PowerStrip","HEA","SHV");
foreach ($BulbData as $CrewID=>$monthinfo){
	$lastMonthYTD = array();
	foreach ($monthinfo as $monthdate=>$fieldthing){
		foreach ($CommissionISM as $Measure){
			//echo $monthdate."<Br>";
			$lastMonthYTDYear = date("Y",strtotime($monthdate));
			$CalendarYTDBulbReportTotal[$CrewID][$monthdate][$Measure] = $lastMonthYTD[$lastMonthYTDYear][$Measure]+$fieldthing[$Measure];
			$CalendarYTDBulbReportTotal[$CrewID]["YTD"][$lastMonthYTDYear][$Measure] = $CalendarYTDBulbReportTotal[$CrewID]["YTD"][$lastMonthYTDYear][$Measure]+$fieldthing[$Measure];
			$lastMonthYTD[$lastMonthYTDYear][$Measure] = $lastMonthYTD[$lastMonthYTDYear][$Measure]+$fieldthing[$Measure];
		}
	}
}
//print_pre($CalendarYTDBulbReportTotal["CET_18"]);
foreach ($BulbMonthYears as $BulbMonthYear=>$count){
	$BulbMonthYearsTrim[] = $BulbMonthYear;
}
$Notes[] = "HEA Visit Count is compiled from Bulb Report Data and includes HEA Additional Unit count.  All other information is from Sharepoint";
$includeCommissions = true;
$path = ($path ? $path : $siteRoot.$adminFolder);
include_once($path.'sharepoint/SharePointAPIFiles.php');

$dateEvents = array("AirSealing","Insulation");


$ignoreContractor = array("Ron Desellier Electric");
$ignoreES = array("HP","Rtl_");
$Notes[] = "Only displaying Active Energy Specialists";
$Notes[] = "Excluding 'A Mathews' and 'Jeanne C'";
$Notes[] = "Excluding 'SHV' records";
$Notes[] = "Row shows totals amount or count for each month";
$Notes[] = "If both Airsealing and Insulation contracts both billed then the count is only 1 unless they were billed in different years.";
$Notes[] = "Tstat Install Billed amounts are excluded from Utility Amounts but included for ES Commissions";
$Notes[] = "If contract has Billed Date, but Final Amount is blank, Original Amount is used";
echo "<span class='memoryusage'>Share Point Data Split into ".count($Views)." parts due to record count<br>Processing...<br></span>";
ob_flush();
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	echo "<span class='memoryusage'>Processing Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	ob_flush();
	foreach (${$ArrayName} as $SPData){
		$ES = strtoupper(trim($SPData["energy_x0020_specialist"]));
		if (substr($ES,0,2) != "HP" && substr($ES,0,4) != "RTL_" && $ES != "A MATHEWS" && $ES != "JEANNE C" && $ES != ""){
			if (strtoupper(substr($ES,0,3)) == "CET"){
				$ES = $WarehouseByName[strtoupper($ES)];
			}
			$ES = ($ES != "CJ H" ? ucwords(strtolower(trim($ES))) : $ES);
			$Status = $SPData["status"];
			//only use active ES and exclude SHV
			if (in_array($ES,$ActiveWarehouseByName)){
				$SiteID = $SPData["site_x0020_id"];
				$ESPrefix = ($WarehouseByShortName[$ES]{strlen($WarehouseByShortName[$ES])-3}=="1" ? "XX" : "").$WarehouseByShortName[$ES];
				$ES = strtoupper($ESPrefix."QQQ".$ES);
				$ESListAll[$ES] = 1;
				$Utility = trim($SPData["utility"]);
				$Utilties[$Utility] = 1;
				switch($Utility){
					case "Berkshire Gas":
						$UtilityShort = "BGAS";
						break;
	//				case "National Grid":
	//					$UtilityShort = "NATGRID";
					case "WMECO":
						$UtilityShort = "WMECO";
						break;
					default:
						$UtilityShort = $Utility;
						break;
				}
				$Utility = $UtilityShort;
				$HEADate = (date("Y",strtotime($SPData["hea_x0020_date"])) == 1969 ? "0000-00-00" : date("Y-m-d",strtotime($SPData["hea_x0020_date"])));
				$MonthYear = date("Y-m",strtotime($HEADate));
				
				$SiteID = $SPData["site_x0020_id"];
				$SiteIDs[$SiteID]["Name"] = $SPData["first_x0020_name"]." ".$SPData["last_x0020_name"];
				$SiteIDs[$SiteID]["HEADate"] = $HEADate;
				$SiteIDs[$SiteID]["Utility"] = $SPData["utility"];
				$InsulationContractSignedDate = ($SPData["insulation_x0020_date_x0020_sign"] ? date("Y-m-d",strtotime($SPData["insulation_x0020_date_x0020_sign"])) : "0000-00-00");
				$InsulationContractBilledDate = ($SPData["insulation_x0020_billing_x0020_d"] ? date("Y-m-d",strtotime($SPData["insulation_x0020_billing_x0020_d"])) : "0000-00-00");
				$InsulationFinalContractAmount = round($SPData["insulation_x0020_final_x0020_con"],2);
				$InsulationOriginalContractAmount = round($SPData["insulation_x0020_original_x0020_"],2);
				$AirSealingContractSignedDate = ($SPData["a_x002f_s_x0020_date_x0020_signe"] ? date("Y-m-d",strtotime($SPData["a_x002f_s_x0020_date_x0020_signe"])) : "0000-00-00");
				$AirSealingContractBilledDate = ($SPData["utility_x0020_billing_x0020_date"] ? date("Y-m-d",strtotime($SPData["utility_x0020_billing_x0020_date"])) : "0000-00-00");
				$AirSealingFinalContractAmount = round($SPData["final_x0020__x0024_"],2);
				$AirSealingOriginalContractAmount = round($SPData["a_x002f_s_x0020_original_x0020_c"],2);
				$TstatContractSignedDate = ($SPData["tstats_x0020_date_x0020_signed"] ? date("Y-m-d",strtotime($SPData["tstats_x0020_date_x0020_signed"])) : "0000-00-00");
				$TstatContractBilledDate = ($SPData["tstat_x0020_billing_x0020_date"] ? date("Y-m-d",strtotime($SPData["tstat_x0020_billing_x0020_date"])) : "0000-00-00");
				$TstatFinalContractAmount = round($SPData["tstat_x0020_final_x0020_contract"],2);
				$TstatOriginalContractAmount = round($SPData["tstat_x0020_original_x0020_contr"],2);
				erase_val($SPData);
				$SPData = null;
				$BilledCount = 0;
				$BillingYear1 = 0;
				$BillingYear2 = 0;
				$BillingMonthYear = null;
				//$SiteIDList[$SiteID][] = $ES;
				if (MySQLDate($AirSealingContractBilledDate)){
					$BillingMonthYear = date("Y-m",strtotime($AirSealingContractBilledDate));
					$BillingYear1 = date("Y",strtotime($AirSealingContractBilledDate));
					$ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear][$SiteID]["AirSealing"][MySQLDate($AirSealingContractBilledDate)][] = ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear]["AirSealing"] = $ESBilledSite[$ES][$BillingMonthYear]["AirSealing"] + ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] = $ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] + ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"]+$AirSealingOriginalContractAmount;
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["AS"]["BilledDate"] = date("m/d/Y",strtotime($AirSealingContractBilledDate));
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["AS"]["ContractAmountOriginal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["AS"]["ContractAmountOriginal"]+$AirSealingOriginalContractAmount;
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["AS"]["ContractAmountFinal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["AS"]["ContractAmountFinal"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$SiteIDContractReport[$SiteID]["AS"]["BilledDate"] = date("m/d/Y",strtotime($AirSealingContractBilledDate));
					$SiteIDContractReport[$SiteID]["AS"]["ContractAmountOriginal"] = $SiteIDContractReport[$SiteID]["AS"]["ContractAmountOriginal"]+$AirSealingOriginalContractAmount;
					$SiteIDContractReport[$SiteID]["AS"]["ContractAmountFinal"] = $SiteIDContractReport[$SiteID]["AS"]["ContractAmountFinal"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);

					if ($Status!="SHV"){
						$BilledCount = 1;
						$ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledCount"] = $ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledCount"]+$BilledCount;
						$ESDataBilled[$ES][$BillingMonthYear]["BilledCount"] = $ESDataBilled[$ES][$BillingMonthYear]["BilledCount"]+$BilledCount;
					}
				}
				$BillingMonthYear = null;
				if (MySQLDate($InsulationContractBilledDate)){
					$BillingMonthYear = date("Y-m",strtotime($InsulationContractBilledDate));
					$BillingYear2 = date("Y",strtotime($InsulationContractBilledDate));
					$ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear][$SiteID]["Insulation"][MySQLDate($InsulationContractBilledDate)][] = ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear]["Insulation"] = $ESBilledSite[$ES][$BillingMonthYear]["Insulation"] + ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] = $ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] + ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"]+$InsulationOriginalContractAmount;
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["INS"]["BilledDate"] = date("m/d/Y",strtotime($InsulationContractBilledDate));
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["INS"]["ContractAmountOriginal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["INS"]["ContractAmountOriginal"]+$InsulationOriginalContractAmount;
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["INS"]["ContractAmountFinal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["INS"]["ContractAmountFinal"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$SiteIDContractReport[$SiteID]["INS"]["BilledDate"] = date("m/d/Y",strtotime($InsulationContractBilledDate));
					$SiteIDContractReport[$SiteID]["INS"]["ContractAmountOriginal"] = $SiteIDContractReport[$SiteID]["INS"]["ContractAmountOriginal"]+$InsulationOriginalContractAmount;
					$SiteIDContractReport[$SiteID]["INS"]["ContractAmountFinal"] = $SiteIDContractReport[$SiteID]["INS"]["ContractAmountFinal"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);

					if ($Status!="SHV"){
						if (!$BilledCount || $BillingYear1 != $BillingYear2){
							$BilledCount = 1;
							$ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledCount"] = $ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledCount"]+$BilledCount;
							$ESDataBilled[$ES][$BillingMonthYear]["BilledCount"] = $ESDataBilled[$ES][$BillingMonthYear]["BilledCount"]+$BilledCount;
						}
					}
				}
				$BillingMonthYear = null;
				if (MySQLDate($TstatContractBilledDate)){
					$BillingMonthYear = date("Y-m",strtotime($TstatContractBilledDate));
					$ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear]["BilledAmount"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear][$SiteID]["Tstat"][MySQLDate($TstatContractBilledDate)][] = ($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$ESBilledSite[$ES][$BillingMonthYear]["Tstat"] = $ESBilledSite[$ES][$BillingMonthYear]["Tstat"] + ($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);					
					$ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] = $ESBilledSite[$ES][$BillingMonthYear]["TotalBilled"] + ($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESDataBilled[$ES][$BillingMonthYear][$Utility]["BilledAmount"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountOriginal"]+$TstatOriginalContractAmount;
					$ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"] = $ESDataContract[$ES][$BillingMonthYear]["ContractAmountFinal"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["TSTAT"]["BilledDate"] = date("m/d/Y",strtotime($TstatContractBilledDate));
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["TSTAT"]["ContractAmountOriginal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["TSTAT"]["ContractAmountOriginal"]+$TstatOriginalContractAmount;
					$ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["TSTAT"]["ContractAmountFinal"] = $ESDataContractReport[$ES][$BillingMonthYear][$SiteID]["TSTAT"]["ContractAmountFinal"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$SiteIDContractReport[$SiteID]["TSTAT"]["BilledDate"] = date("m/d/Y",strtotime($TstatContractBilledDate));
					$SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountOriginal"] = $SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountOriginal"]+$TstatOriginalContractAmount;
					$SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountFinal"] = $SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountFinal"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
				}
				
				//if visit is in time frame
	//			if (MySQLDate($HEADate) && (strtotime($HEADate) >= strtotime($StartDate) && strtotime($HEADate) <= strtotime($EndDate))){

					$ESData[$ES][$MonthYear]["SiteCount"] = $ESData[$ES][$MonthYear]["SiteCount"]+1;
					$TotalOriginalContractAmount = 0;
					$TotalFinalContractAmount = 0;
					//is there a contract signed in the same month as visit
					$ContractSigned = false;
					if (MySQLDate($AirSealingContractSignedDate)){
						$AirSealingMonthYear = date("Y-m",strtotime($AirSealingContractSignedDate));
						if ($AirSealingMonthYear == $MonthYear){
							$ContractSigned = true;
						}
					}	
					if (MySQLDate($InsulationContractSignedDate)){
						$InsulationMonthYear = date("Y-m",strtotime($AirSealingContractSignedDate));
						if ($InsulationMonthYear == $MonthYear){
							$ContractSigned = true;
						}
					}
					if ($ContractSigned){
						$ESData[$ES][$MonthYear]["SignedCount"] = $ESData[$ES][$MonthYear]["SignedCount"]+1;
						$ESData[$ES][$MonthYear][$Utility]["SignedCount"] = $ESData[$ES][$MonthYear][$Utility]["SignedCount"]+1;
					}

					
	//			}//end only HEADates in timeframe
			}//end using only active specialists
		}//end ignoring other EnergySpecialist
	}//end foreach sprecord
	${$ArrayName} = null;
	echo "<span class='memoryusage'>Completed Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	ob_flush();

	//echo "<br>MemoryUsage:".$ArrayName." ".memory_get_usage();
}//end foreach view
//ksort($ESBilledSite);
//print_pre($ESBilledSite["CET_92QQQAMY W"]["2016-01"]);
//get YTDTotal for BilledCount 
foreach ($ESDataBilled as $ES => $MonthYearInfo){
	$ESName = explode("QQQ",$ES);
	$CrewID = strtoupper(str_replace("XX","",$ESName[0]));	
	$lastMonthYTD = 0;
	$x = 1;
	while ($x < 13){
		$MonthYear = $CurrentCalendarYear."-".($x < 10 ? "0".$x : $x);
		$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["BilledCount"] = $lastMonthYTD+$MonthYearInfo[$MonthYear]["BilledCount"];
		$CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["BilledCount"] = $CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["BilledCount"]+$MonthYearInfo[$MonthYear]["BilledCount"];
		$lastMonthYTD = $lastMonthYTD+$MonthYearInfo[$MonthYear]["BilledCount"];
		$x++;
	}
}
//print_pre($CalendarYTDBulbReportTotal["CET_18"]);

//echo "<br>MemoryUsage3:".memory_get_usage();
echo "<span class='memoryusage'><Br>Processing Complete.  Please stand by while page finishes rendering: Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
ob_flush();
//print_pre($ESDataBilled["CET_18QQQRENEE S"]);
//empty $SPRecords to free up memory
$SPRecords = array();
//print_pre($Utilties);
//print_pre($ESDataBilled["CET_74QQQMATT V"]);
//get bulb commissions
$Notes[]= "ES with less than 90 days active are not eligible for bulb commissions OR for contract commissions";
$Notes[] = "Eligible ES get 2% of all contract work after 90 days of employment as an energy specialist";
$Notes[] = "Eligible ES must have a fiscal year to date average bulb per HEA install of 17 or higher to qualify for bulb commissions";
$Notes[] = "Bulb commission is based on year to date Conversion rate";
$Notes[] = "Bulb commissions: YTD Conversion rate >= 20% $0.50/bulb, >= 25% $0.75/bulb, >= 30% $1.00/bulb, >= 40% $1.25/bulb";
$Notes[] = "Bulb Bonus: If YTD quarterly closing rate >40% AND YTD bulbs installed per visit >22 $1.50/bulb";

foreach ($ESListAll as $ES=>$count){
	$CrewIDParts = explode("QQQ",$ES);
	$ESList[] = $ES;
	$CrewIDList[] = str_replace("XX","",$CrewIDParts[0]);
}

sort($ESList);
sort($CrewIDList);
//print_pre($BulbData);

//look for months that have billed data but no bulb data
$MonthCycleStart = $StartDate;
do {
	$MonthsToCycleThrough[] = date("Y-m",strtotime($MonthCycleStart));
	$MonthCycleStart = date("m/d/Y",strtotime($MonthCycleStart." +1 month"));
} while (strtotime($MonthCycleStart) <= strtotime("12/1/".date("Y")));
foreach ($BulbData as $CrewID=>$MonthYearInfo){
	foreach ($MonthsToCycleThrough as $MonthYear){
		$CrewWithBulbMonths[$CrewID][] = $MonthYear;
	}
}
foreach ($BulbData as $CrewID=>$MonthYearInfo){
	$ESPrefix = ($CrewID{strlen($CrewID)-3}=="1" ? "XX" : "").$CrewID;
	$ES = strtoupper($ESPrefix."QQQ".$WarehouseByName[strtoupper($CrewID)]);
	if (in_array($ES,$ESList)){
		$CrewStartDate = $WarehouseRecordByName[strtoupper($CrewID)]->startDate;
		$IsLeadSpecialist = $WarehouseRecordByName[strtoupper($CrewID)]->lead;
		$IsBulbRateNotTiedToClosingRate = $WarehouseRecordByName[strtoupper($CrewID)]->bulbRateNotTiedToClosingRate;
		$YTDBulbCount[$CrewID] = 0;
		$YTDBulbVisit[$CrewID] = 0;
		$YTDAerator[$CrewID] = 0;
		$YTDTstat[$CrewID] = 0;
		$YTDPowerStrip[$CrewID] = 0;
		$YTDShowerhead[$CrewID] = 0;
		$YTDBilledContracts[$CrewID] = 0;
		$YTDHEACount[$CrewID] = 0;
		foreach ($CrewWithBulbMonths[$CrewID] as $MonthYear){
			$BulbData[$CrewID][$MonthYear]["BilledCount"] = $ESDataBilled[$ES][$MonthYear]["BilledCount"];
		//foreach ($MonthYearInfo as $MonthYear=>$RecordType){
			$DateParts = explode("-",$MonthYear);
			$visitCount =  ($MonthYearInfo[$MonthYear]["HEA"]+$MonthYearInfo[$MonthYear]["SHV"]);
			$BulbCount = $MonthYearInfo[$MonthYear]["Bulbs"];
			$AeratorCount = $MonthYearInfo[$MonthYear]["Aerator"];
			$TstatCount = $MonthYearInfo[$MonthYear]["Tstat"];
			$PowerStripCount = $MonthYearInfo[$MonthYear]["PowerStrip"];
			$ShowerheadCount = $MonthYearInfo[$MonthYear]["Showerhead"];
			$diff = date_diff(date_create($CrewStartDate),date_create($MonthYear."-01"));
			$daysDiff = $diff->format("%R%a");
			$monthsDiff = ($diff->m + ($diff->y * 12));
			//echo $CrewID." ".$CrewStartDate." ".$MonthYear."-01 ".$daysDiff." ".$monthsDiff." months<br>";
			$EligibleByDate = ($daysDiff >0 && $monthsDiff >= 3 ? true : false);
			if ($UseCalendarYTD && $DateParts[0] == $CurrentCalendarYear && $DateParts[1] < $CurrentCalendarMonth){
				$YTDBulbCount[$CrewID] = ($YTDBulbCount[$CrewID]+$BulbCount);
				$YTDAerator[$CrewID] = bcadd($YTDAerator[$CrewID],$AeratorCount,0);
				$YTDTstat[$CrewID] = ($YTDTstat[$CrewID]+$TstatCount);
				$YTDPowerStrip[$CrewID] = ($YTDPowerStrip[$CrewID]+$PowerStripCount);
				$YTDShowerhead[$CrewID] = ($YTDShowerhead[$CrewID]+$ShowerheadCount);
				$YTDBulbVisit[$CrewID] = ($YTDBulbVisit[$CrewID]+$visitCount);
				$YTDBilledContracts[$CrewID] = ($YTDBilledContracts[$CrewID]+$ESDataBilled[$ES][$MonthYear]["BilledCount"]);
				$YTDHEACount[$CrewID] = bcadd($YTDHEACount[$CrewID],$MonthYearInfo[$MonthYear]["HEA"],0); //($YTDHEACount[$CrewID]+$MonthYearInfo[$MonthYear]["HEA"]);
				$BulbData[$CrewID][$MonthYear]["YTDCommissionEligibleByDate"] = $EligibleByDate;
				$BulbData[$CrewID][$MonthYear]["YTDAerator"] = $YTDAerator[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDTstat"] = $YTDTstat[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDPowerStrip"] = $YTDPowerStrip[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDShowerhead"] = $YTDShowerhead[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDBulbCount"] = $YTDBulbCount[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"] = $YTDBulbVisit[$CrewID];
				$YTDBulbPerVisit = round($BulbData[$CrewID][$MonthYear]["YTDBulbCount"]/$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"]);
				/*testing
				if ($CrewID == "CET_18"){
					echo $MonthYear."=".$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"]."/(".$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]."+".$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["SHV"].")<br>";
				}
				*/
				$EligibleByInstallPerVisit = (round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"]/($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["SHV"])) >= 17 ? true : false);
				$BulbData[$CrewID][$MonthYear]["YTDBulbPerVisit"] = $YTDBulbPerVisit;
				$BulbData[$CrewID][$MonthYear]["YTDBulbCommissionEligibleByBulb"] = $EligibleByInstallPerVisit;
				$BulbData[$CrewID][$MonthYear]["YTDCommissionEligibilty"] = (!$EligibleByDate ? "Less than 90 Days " : "").(!$EligibleByInstallPerVisit ? "Less than 17 YTD bulbs per visit" : "");
				$BulbData[$CrewID][$MonthYear]["YTDBilledContracts"] = $YTDBilledContracts[$CrewID];
				$BulbData[$CrewID][$MonthYear]["YTDHEACount"] = $YTDHEACount[$CrewID];
				//ClosingRate based on Calendar Year, Not Fiscal Year
				$YTDClosingRate = round(($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["BilledCount"]/$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]*10000)/100);
				//$YTDClosingRate = (round(($BulbData[$CrewID][$MonthYear]["YTDBilledContracts"]/$BulbData[$CrewID][$MonthYear]["YTDHEACount"])*10000)/100);
				$BulbBonus = ($YTDClosingRate >= 40 && round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"]/($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["SHV"])) >= 22 ? true : false);
				$BulbData[$CrewID][$MonthYear]["YTDClosingRate"] = $YTDClosingRate;
			}
			$BulbData[$CrewID][$MonthYear]["BulbBonus"] = $BulbBonus;
			$BulbData[$CrewID][$MonthYear]["BulbVisitCount"] = $visitCount;
			
			//new for June 2015
			$BulbFactor=0;
			if ($YTDClosingRate >=20){
				$BulbFactor = 0.50;
			}
			if ($YTDClosingRate >=25){
				$BulbFactor = 0.75;
			}
			if ($YTDClosingRate >=30){
				$BulbFactor = 1;
			}
			if ($YTDClosingRate >=40){
				$BulbFactor = 1.25;
			}
			if ($BulbBonus){
				$BulbFactor = 1.50;
			}
			//structure for less than 1 year
			if ($EligibleByDate && $monthsDiff < 12){
//			echo $CrewID." ".$YTDClosingRate." ".$EligibleByDate." && ".$monthsDiff." ";
				$BulbFactor = 0.50;
				if ($YTDClosingRate >=25){
					$BulbFactor = 0.75;
				}
				if ($YTDClosingRate >=30){
					$BulbFactor = 1.00;
				}
				if ($YTDClosingRate >=40){
					$BulbFactor = 1.25;
				}
				if ($BulbBonus){
					$BulbFactor = 1.50;
				}
			}
//			echo $BulbFactor."<br>";
			//structure for LeadSpecialist or NotTiedtoClosingRate
			if ($IsLeadSpecialist || $IsBulbRateNotTiedToClosingRate){
				$BulbFactor = 1.00;
			}
			if (!$EligibleByInstallPerVisit){
				$BulbCommission = 0.00;
			}else{
				$BulbCommission = bcmul($BulbCount,$BulbFactor,2);
			}
			$BulbData[$CrewID][$MonthYear]["BulbFactor"] = $BulbData[$CrewID][$MonthYear]["BulbFactorProposed"] = $BulbFactor;
			$BulbData[$CrewID][$MonthYear]["BulbCommission"] = $BulbData[$CrewID][$MonthYear]["BulbCommissionProposed"] = $BulbCommission;
			
			//prior to June 2015
			$BulbFactor=0;
			$BulbAvg = round($BulbCount/$visitCount);
			if ($BulbAvg >=13){
				$BulbFactor = 0.25;
			}
			if ($BulbAvg >=16){
				$BulbFactor = 1;
			}
			$BulbCommission = bcmul($BulbCount,$BulbFactor,2);
			$BulbData[$CrewID][$MonthYear]["BulbFactorPrior"] = $BulbFactor;
			$BulbData[$CrewID][$MonthYear]["BulbCommissionPrior"] = $BulbCommission;
			if ($UsePriorCalculations){
				$BulbData[$CrewID][$MonthYear]["BulbFactor"] = $BulbFactor;
				$BulbData[$CrewID][$MonthYear]["BulbCommission"] = $BulbCommission;
			}
		}//end Foreach MonthYearInfo
	}//end if in $ESList
	ksort($BulbData[$CrewID]);
}
//echo "<br>MemoryUsage4:".memory_get_usage();'
ksort($BulbData);
//print_pre($BulbData["CET_18"]);
/*
foreach($ESData as $ES=>$MonthYearInfo){
	$ESName = explode("QQQ",$ES);
	$CrewID = str_replace("XX","",$ESName[0]);
	//check to see if CrewID in BulbData...possibly had no ISM installs this month
	ksort($MonthYearInfo);
	foreach ($MonthYearInfo as $MonthYear=>$Category){
		//check to see if CrewID in BulbData...possibly had no ISM installs this month
		if (in_array($MonthYear,$BulbMonthYearsTrim)){
			$HasBulbData[$CrewID][]=$MonthYear;
		}
		$BilledAmount = $ESDataBilled[$ES][$MonthYear]["BilledAmount"];
		$BilledCount = $ESDataBilled[$ES][$MonthYear]["BilledCount"];
		//$SiteCount = $Category["SiteCount"];
		$SiteCount = $BulbData[$CrewID][$MonthYear]["HEA"]; //using data from bulb report
		$SignedCount = $Category["SignedCount"];
		
		$ESData[$ES][$MonthYear]["SignedRate"] = floor(($SignedCount/$SiteCount)*100);
		$Formulas["ES"]["Signed Rate"] = "(Number of HEA Visits this Month that get Contracts Signed)/(Number of HEA Visits this Month)";
		
		$ESData[$ES][$MonthYear]["ConversionRate"] = floor(($BilledCount/$SiteCount)*100);
		$Formulas["ES"]["Conversion Rate"] = "(Number of Contracts with Billed Date in this Month)/(Number of HEA Visits this Month)";

		$ESDataContract[$ES][$MonthYear]["ContractCommission"] = bcmul($BilledAmount,0.02,2);
//		echo $counter.") ".$ES. " ".$MonthYear." Contract Commission = ".bcmul($BilledAmount,0.02,2)." x.02= ".$ESDataContract["CET_74QQQMATT V"]["2015-03"]["ContractCommission"]."<br>";
		$Formulas["ES"]["Contract Commission"] = "(Billed Amount including Tstat)*0.02";
	}
}
*/
foreach ($BulbMonthYears as $MonthYear=>$count){
	foreach ($ESList as $ES){
		$ESName = explode("QQQ",$ES);
		$CrewID = str_replace("XX","",$ESName[0]);
		//echo $CrewID."<br>";
		
		$BilledAmount = $ESDataBilled[$ES][$MonthYear]["BilledAmount"];
		$BilledCount = $ESDataBilled[$ES][$MonthYear]["BilledCount"];
//		$ESDataBilled[$ES][$MonthYear]["BilledCount"];
		$YTDBilledCount[$CrewID] = $YTDBilledCount[$CrewID]+$BilledCount;
		//$SiteCount = $Category["SiteCount"];
		$SiteCount = ($BulbData[$CrewID][$MonthYear]["HEA"] ? $BulbData[$CrewID][$MonthYear]["HEA"] : 0); //using data from bulb report
//		$SiteCount = ($BulbData[$CrewID][$MonthYear]["HEA"] ? $BulbData[$CrewID][$MonthYear]["HEA"] : 0); //using data from bulb report
		$YTDSiteCount[$CrewID] = $YTDSiteCount[$CrewID]+$SiteCount;
		$SignedCount = $Category["SignedCount"];
		

		//Don't give contract commission in month crewId left
		$deactiveYearMonth = (MySQLDate($WarehouseRecordByName[$CrewID]->endDate) ? date("Y-m",strtotime($WarehouseRecordByName[$CrewID]->endDate)) : "1969-12");
		$deactiveDate = (int)strtotime($deactiveYearMonth."-01");
		/*
			if ($deactiveYearMonth !="1969-12"){
					echo $WarehouseRecordByName[$CrewID]->endDate." ".date("Y-m-d",$deactiveDate)."=".date("Y-m-d",$thisMonthYearDate)."<br>";
					echo $deactiveDate."=".$thisMonthYearDate." ".$ES." DecativeYearMonth=".$deactiveYearMonth." ".bcmul($BilledAmount,0.02,2)."<br>";
			}
		*/
		
		$thisMonthYearDate = (int)strtotime($MonthYear."-01");
		if ($deactiveYearMonth !="1969-12" && $deactiveDate <= $thisMonthYearDate){
			$ESData[$ES][$MonthYear]["SignedRate"] = 0;
			$ESData[$ES][$MonthYear]["ConversionRate"] = 0;
			$ESDataContract[$ES][$MonthYear]["ContractCommission"] = 0.00;
		}else{
			$ESData[$ES][$MonthYear]["SignedRate"] = round(($SignedCount/$SiteCount)*100);
			$ESData[$ES][$MonthYear]["ConversionRate"] = round(($BilledCount/$SiteCount)*100);
			$ESDataContract[$ES][$MonthYear]["ContractCommission"] = bcmul($BilledAmount,0.02,2);
		}
		
		if (!array_key_exists($MonthYear,$BulbData[$CrewID]) && strtotime($MonthYear."-01") > strtotime($FiscalStart)){
			//echo $CrewID ." Missing Bulb Data for ".$MonthYear." '".array_key_exists($MonthYear,$BulbData[$CrewID])."' <br>";
			$CrewStartDate = $WarehouseRecordByName[strtoupper($CrewID)]->startDate;
			//print_pre($BulbData[$CrewID]);
			$SearchingMonthYear = $MonthYear;
			do {
				$diff = date_diff(date_create($CrewStartDate),date_create($SearchingMonthYear."-01"));
				$daysDiff = $diff->format("%R%a");
				$monthsDiff = ($diff->m + ($diff->y * 12));
				$EligibleByDate = ($daysDiff >0 && $monthsDiff >= 3 ? true : false);
//				echo $CrewID ." ".$EligibleByDate." Missing Bulb Data for ".$SearchingMonthYear." '".array_key_exists($SearchingMonthYear,$BulbData[$CrewID])."' <br>";
				$BulbData[$CrewID][$SearchingMonthYear]["YTDCommissionEligibleByDate"] = $EligibleByDate;
				$BulbData[$CrewID][$SearchingMonthYear]["YTDCommissionEligibilty"] = (!$EligibleByDate ? "Less than 90 Days " : "");
				$PriorSearchingMonthYear = date("Y-m",strtotime($SearchingMonthYear."-01 -1 month"));
				$BulbData[$CrewID][$SearchingMonthYear]["YTDAerator"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDAerator"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDTstat"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDTstat"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDPowerStrip"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDPowerStrip"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDShowerhead"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDShowerhead"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBulbCount"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBulbCount"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBulbVisit"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBulbVisit"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBulbPerVisit"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBulbPerVisit"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBulbCommissionEligibleByBulb"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBulbCommissionEligibleByBulb"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDCommissionEligibilty"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDCommissionEligibilty"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBilledContracts"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBilledContracts"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDHEACount"] = $BulbData[$CrewID][$PriorSearchingMonthYear]["YTDHEACount"];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDClosingRate"] = (bcdiv($BulbData[$CrewID][$PriorSearchingMonthYear]["YTDBilledContracts"],$BulbData[$CrewID][$PriorSearchingMonthYear]["YTDHEACount"],3)*100);
				/*
				$BulbData[$CrewID][$SearchingMonthYear]["YTDBilledContracts"] = $YTDBilledCount[$CrewID];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDHEACount"] = $YTDSiteCount[$CrewID];
				$BulbData[$CrewID][$SearchingMonthYear]["YTDClosingRate"] = (bcdiv($YTDBilledCount[$CrewID],$YTDSiteCount[$CrewID],3)*100);
				*/
				$SearchingMonthYear = $PriorSearchingMonthYear;
			} while (!array_key_exists($SearchingMonthYear,$BulbData[$CrewID]) && strtotime($SearchingMonthYear."-01") >= strtotime($FiscalStart));
		}
		
	}
}
		$Formulas["ES"]["Contract Commission"] = "(Billed Amount including Tstat)*0.02";
		$Formulas["ES"]["Signed Rate"] = "(Number of HEA Visits this Month that get Contracts Signed)/(Number of HEA Visits this Month)";
		$Formulas["ES"]["Conversion Rate"] = "(Number of Contracts with Billed Date in this Month)/(Number of HEA Visits this Month)";
//print_pre($BulbData["CET_107"]);
//print_pre($BulbData["CET_74"]);
//print_pre($ESDataBilled["CET_74QQQMATT V"]["2015-03"]);
//print_pre($ESDataContract["CET_74QQQMATT V"]["2015-03"]);
ksort($ESData);
//print_pre($YTDBulbCount);
//print_pre($ESData["CET_18QQQRENEE S"]["2015-02"]);
//echo "Looking at HEA Visits Starting ".$StartDate." Through ".$EndDate."<br>";
//echo "<br>MemoryUsage5:".memory_get_usage();
?>
<style>
#allCount th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
.esName {cursor: pointer;text-decoration:underline;}
.changedData {color:red;font-weight:bold;}
</style>
<div id="topHalfOfReports">
<h3>Commissions Report<span style="font-weight:normal;font-size:10pt;">(<a href="#Notes">Notes</a>)</span></h3>
<select name="FiscalYear" id="ThisFiscalYear" class="parameters" style="width:175px;">
	<?php
		$FYear = 0;
		while ($FYear < 3){
			echo '<option value="'.$FYear.'"'.($FYear==$ThisFiscalYear ? " selected" : "").'>Fiscal Year '.($FiscalYearStart-$FYear).'-'.($FiscalYearEnd-$FYear).'</option>';
			$FYear++;
		}
	?>
</select>
<select name="DateGrouping" id="DateGrouping" class="parameters" style="width:100px;">
	<option value="ByMonth"<?php echo ($DateGrouping == "ByMonth" ? " selected" : "");?>>By Month</option>
	<option value="ByQuarter"<?php echo ($DateGrouping == "ByQuarter" ? " selected" : "");?>>By Quarter</option>
	<option value="ByYTD"<?php echo ($DateGrouping == "ByYTD" ? " selected" : "");?>>By YTD</option>
</select>
<select name="ColumnGrouping" id="ColumnGrouping" class="parameters" style="width:120px;">
	<option value="All"<?php echo ($ColumnGrouping == "All" ? " selected" : "");?>>All Columns</option>
	<option value="Conversion"<?php echo ($ColumnGrouping == "Conversion" ? " selected" : "");?>>Conversion Only</option>
	<option value="Commission"<?php echo ($ColumnGrouping == "Commission" ? " selected" : "");?>>Commission Only</option>
	<option value="Bulb"<?php echo ($ColumnGrouping == "Bulb" ? " selected" : "");?>>Bulb Only</option>
</select>
<select name="MonthForReport" id="MonthForReport" class="parameters" style="width:175px;">
	<option value="">Itemized Report Month</option>
<?php
	//include Oct of Last Fiscal Year
	echo "<option class=\"MonthForReportOption\" value=\"" . date("Y-m-01",strtotime("October 1, ".$FiscalYearStart))."\"".($MonthForReport == date("Y-m-01",strtotime("October 1, ".$FiscalYearStart)) ? " selected" : "").">2015 Oct</option>";
	$months = array("Nov","Dec","Jan", "Feb", "Mar", "Apr","May","Jun","Jul","Aug","Sep","Oct",);
	foreach ($months as $month) {
		echo "<option class=\"MonthForReportOption\" value=\"" . date("Y-m-01",strtotime($month." 1, ".($month == "Nov" || $month == "Dec" ? $FiscalYearStart : $FiscalYearEnd)))."\"".($MonthForReport == date("Y-m-01",strtotime($month." 1, ".($month == "Nov" || $month == "Dec" ? $FiscalYearStart : $FiscalYearEnd))) ? " selected" : "").">" .($month == "Nov" || $month == "Dec" ? $FiscalYearStart : $FiscalYearEnd)." ". $month . "</option>";
	}
?>
	<option value="">The Data Must be Locked prior to Creating Itemized Reports</option>
</select>
<br>
<span><input type="checkbox" id="ShowPriorCalculations" name="ShowPriorCalculations"<?php echo ($ShowPriorCalculations ? " checked" : "");?>> Show Both Commission Calculations on Reports</span>
<span id="UsePriorCalculationsSpan"><input type="checkbox" id="UsePriorCalculations" name="UsePriorCalculations" class="parameters"<?php echo ($UsePriorCalculations ? " checked" : "");?>> Use Prior Commission Calculations</span>
<style>
.energySpecialist {min-width:100px;}
</style>
<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2"></th>
			<th class="energySpecialist" rowspan="2">Energy Specialist</th>
			<?php
				$ColumsForGrouping["Conversion"] = array('HEASiteVisits','BilledContracts','ConvRate');
				$ColumsForGrouping["Commission"] = array('ContractsOriginal','ContractsFinal','ContractsCommission','BulbCount','BulbFactor','BulbCommission','YTDCommissionEligibilty','TotalCommission');
				$ColumsForGrouping["Bulb"] = array('BulbVisit','BulbCount','BulbCommission');
				//$ColumnsArray["HEASiteSrcs"] = '<th>HEA Site Visits<br><div title="Sp=Sharepoint Sc=Schedule Br=BulbReport" style="font-size:8pt;">(Sp/Sc/Br)</div></th>';
				$ColumnsArray["HEASiteVisits"] = '<th>HEA Site Visits<br><div style="font-size:8pt;">(Br)</div></th>';
				$ColumnsArray["BilledContracts"] = '<th>Billed Contracts<br><div style="font-size:8pt;">(without Tstats)</div></th>';
				$ColumnsArray["ConvRate"] = '<th>Conv Rate<Br><div title="Using BulbReport HEA and HEAAddU count" style="font-size:8pt;">(Br)</div></th>';
				$ColumnsArray["ContractsOriginal"] = '<th>Contracts Original Amount<br><div style="font-size:8pt;">(with Tstats)</div></th>';
				$ColumnsArray["ContractsFinal"] = '<th>Contracts Final Amount<br><div style="font-size:8pt;">(with Tstats)</div></th>';
				$ColumnsArray["ContractsCommission"] = '<th>Contract Commission</th>';
				$ColumnsArray["NatGridBilled"] = '<th>NATGRID Billed Contracts</th>';
				$ColumnsArray["WMECOBilled"] = '<th>ESOURCE Billed  Contracts</th>';
				$ColumnsArray["BGASBilled"] = '<th>BGAS Billed  Contracts</th>';
				$ColumnsArray["BGASAmount"] = '<th>BGAS Amount</th>';
				$ColumnsArray["BulbVisit"] = '<th>Bulb Visit</th>';
				$ColumnsArray["BulbCount"] = '<th>Bulb Count</th>';
				$ColumnsArray["BulbAvg"] = '<th>Bulb Avg</th>';
				$ColumnsArray["BulbFactor"] = '<th>Bulb Factor</th>';
				$ColumnsArray["BulbCommission"] = '<th>Bulb Commission</th>';
				$ColumnsArray["YTDCommissionEligibilty"] = '<th>Ineligible Reason</th>';
				$ColumnsArray["TotalCommission"] = '<th>Total Commission</th>';
				//print_pre($ColumnsArray);
				$spanColumnCount = count($ColumnsArray);
				if (count($ColumsForGrouping[$ColumnGrouping])){
					$spanColumnCount = count($ColumsForGrouping[$ColumnGrouping]);
					foreach ($ColumnsArray as $ColumnName=>$ColumnDisplay){
						if (in_array($ColumnName,$ColumsForGrouping[$ColumnGrouping])){
							$ColumnsForDisplay[$ColumnName] = $ColumnDisplay;
							$ColumnList[]=$ColumnName;
						}
					}
				}else{
					foreach ($ColumnsArray as $ColumnName=>$ColumnDisplay){
							$ColumnsForDisplay[$ColumnName] = $ColumnDisplay;
							$ColumnList[]=$ColumnName;
					}
				}

				$StartMonth = date("m",strtotime($StartDate));
				$StartYear = date("Y",strtotime($StartDate));
				$EndMonth = date("m",strtotime($EndDate));
				$EndYear = date("Y",strtotime($EndDate));
				$ThisMonth = (int)($StartMonth);
				$ThisMonthYear = (int)($StartYear.$StartMonth);
				$EndMonthYear = (int)($EndYear.$EndMonth);
				$calendarCount = 0;
				$RowData = null;
				if ($DateGrouping == "ByQuarter"){
					for ($i = 1; $i <= 4; $i++) {
						$RowData .= "<th colspan=\"".$spanColumnCount."\" style=\"text-align:left;\">".addOrdinalNumberSuffix($i)." Quarter</th>";
					}
				}else{
					do {
						$RowData .= "<th colspan=\"".$spanColumnCount."\" style=\"text-align:left;\">".date("M Y",strtotime($StartDate." + ".$calendarCount." months"))."</th>";
						$ThisMonth = (int)($ThisMonth+1);
						if ($ThisMonth == 13){
							$ThisMonth = 1;
							$ThisMonthYear = (($ThisMonthYear+100)-11);
						}else{
							$ThisMonthYear = ($ThisMonthYear+1);
						}
						$calendarCount++;
					} while ($ThisMonthYear <= $EndMonthYear);
				}//end if ByQuarter
				$RowData .= "<th colspan=\"".$spanColumnCount."\" style=\"text-align:left;\">YTD (Fiscal Year)</th>";
				if ($DateGrouping != "ByYTD"){
					echo $RowData;
				}

			?>
		<tr>
			<?php
				$StartMonth = date("m",strtotime($StartDate));
				$StartYear = date("Y",strtotime($StartDate));
				$EndMonth = date("m",strtotime($EndDate));
				$EndYear = date("Y",strtotime($EndDate));
				$ThisMonth = (int)($StartMonth);
				$ThisMonthYear = (int)($StartYear.$StartMonth);
				$EndMonthYear = (int)($EndYear.$EndMonth);
				$RowData = null;
				
				$RowDataTemplate = implode(" ",$ColumnsForDisplay);
						
				if ($DateGrouping == "ByQuarter"){
					for ($i = 1; $i <= 4; $i++) {
						$RowData .= $RowDataTemplate;
					}
				}else{
					do {
						$RowData .= $RowDataTemplate;
						$ThisMonth = (int)($ThisMonth+1);
						if ($ThisMonth == 13){
							$ThisMonth = 1;
							$ThisMonthYear = (($ThisMonthYear+100)-11);
						}else{
							$ThisMonthYear = ($ThisMonthYear+1);
						}
					} while ($ThisMonthYear <= $EndMonthYear);
				}//end ByQuarter
				if ($DateGrouping != "ByYTD"){
					echo $RowData;
				}
				$RowDataTemplate = null;
				if (in_array("HEASiteVisits",$ColumnList)){
					$RowDataTemplate .= '<th>YTD HEA Site Visits</th>';
				}
				if (in_array("BilledContracts",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Billed Contracts</th>';
				}
				if (in_array("ConvRate",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Conv Rate</th>';
				}
				if (in_array("ContractsOriginal",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Contracts Original Amount</th>';
				}
				if (in_array("ContractsFinal",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Contracts Final Amount</th>';
				}
				if (in_array("ContractsCommission",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Contract Commission</th>';
				}
				if (in_array("NatGridBilled",$ColumnList)){
					$RowDataTemplate .= '<th>YTD NATGRID Billed Contracts</th>';
				}
				if (in_array("WMECOBilled",$ColumnList)){
					$RowDataTemplate .= '<th>YTD ESOURCE Billed  Contracts</th>';
				}
				if (in_array("BGASBilled",$ColumnList)){
					$RowDataTemplate .= '<th>YTD BGAS Billed  Contracts</th>';
				}
				if (in_array("BGASAmount",$ColumnList)){
					$RowDataTemplate .= '<th>YTD BGAS Amount</th>';
				}
				if (in_array("BulbVisit",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Bulb Visit</th>';
				}
				if (in_array("BulbCount",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Bulb Count</th>';
				}
				if (in_array("BulbAvg",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Bulb Avg</th>';
				}
				if (in_array("BulbCommission",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Bulb Commission</th>';
				}
				if (in_array("TotalCommission",$ColumnList)){
					$RowDataTemplate .= '<th>YTD Total Commission</th>';
				}
				echo $RowDataTemplate;
			?>
		</tr>
	</thead>
	<tbody>
		<?php
			$DisplayHideExceptions = array("CET_85");
			foreach ($ESData as $ES=>$MonthYearInfo){
				$ESName = explode("QQQ",$ES);
				$CrewID = strtoupper(str_replace("XX","",$ESName[0]));
				$CrewParts = explode("_",$CrewID);
				$CrewIDSort = $CrewID;
				if (strlen($CrewParts[1]) < 3){
					$CrewIDSort = str_replace("_","_0",$CrewID);
				}
				$ESNameFormated = ucwords(strtolower($ESName[1]));
				$ESNameFormated = ($ESNameFormated == "Cj H" ? "CJ H" : $ESNameFormated);
				$Title = " title=\"".$ESNameFormated."\"";
				//hide this row if ended prior to fiscal start
				$trStyle ="";
				if (!in_array($CrewID,$DisplayHideExceptions) && MySQLDate($WarehouseRecordByName[$CrewID]->endDate) && strtotime($WarehouseRecordByName[$CrewID]->endDate) < strtotime($StartDate)){
					$trStyle = " style='display:none;border:1pt solid red;'";
				}
				$RowDataStart = '<tr'.$trStyle.'>
						<td class="data-control" data-es="'.$ES.'">'.$CrewIDSort.'</td>
						<td class="energySpecialist">'.$ESNameFormated.'</td>';
				$RowData = null;
				$QuarterData = null;
				$StartMonth = date("m",strtotime($StartDate));
				$StartYear = date("Y",strtotime($StartDate));
				$EndMonth = date("m",strtotime($EndDate));
				$EndYear = date("Y",strtotime($EndDate));
				$ThisMonth = (int)($StartMonth);
				$ThisMonthYear = (int)($StartYear.$StartMonth);
				$ThisQuarter = 1;
				$EndMonthYear = (int)($EndYear.$EndMonth);
				$MonthYear = date("Y-m",strtotime($StartDate));
				$ThisQuarterStyle = ($DateGrouping == "ByQuarter" ? ' style="display:none;"' : '');
				$QuarterMonthsToHide = array(1,2,4,5,7,8,10,11);
				$QuarterMonthsToReset = array(1,4,7,10);
				$PriorCurrentMonth = date("Y-m",strtotime(date("Y-m-d")." -1 month"));
					//echo "<hr>".$PriorCurrentMonth.":<br>";
					do {
						//echo $MonthYear." <= ".$PriorCurrentMonth." ";
						$LockDataValid = (strtotime($MonthYear) <= strtotime($PriorCurrentMonth) ? true : false);	
						//echo $LockDataValid."<br>";
						if ($DateGrouping == "ByQuarter"){
							//reset the quarter sum if the start of a new quarter
							if (in_array($ThisQuarter,$QuarterMonthsToReset)){
								erase_val($QuarterData);
//								$QuarterData = array();								
							}
							$QuarterData["HEA"] = ($QuarterData["HEA"]+$BulbData[$CrewID][$MonthYear]["HEA"]);
							$QuarterData["SHV"] = ($QuarterData["SHV"]+$BulbData[$CrewID][$MonthYear]["SHV"]);
							$QuarterData["BulbCount"] = ($QuarterData["BulbCount"]+$BulbData[$CrewID][$MonthYear]["Bulbs"]);
							$QuarterData["BulbCommission"] = ($QuarterData["BulbCommission"]+$BulbData[$CrewID][$MonthYear]["BulbCommission"]);
							$QuarterData["BilledCount"] = ($QuarterData["BilledCount"]+$ESDataBilled[$ES][$MonthYear]["BilledCount"]);
							$QuarterData["ConversionRate"] = round(bcdiv($QuarterData["BilledCount"],$QuarterData["HEA"],3)*100);
							$QuarterData["ContractAmountOriginal"] = ($QuarterData["ContractAmountOriginal"]+$ESDataContract[$ES][$MonthYear]["ContractAmountOriginal"]);
							$QuarterData["ContractAmountFinal"] = ($QuarterData["ContractAmountFinal"]+$ESDataContract[$ES][$MonthYear]["ContractAmountFinal"]);
							$QuarterData["ContractCommission"] = ($QuarterData["ContractCommission"]+$ESDataContract[$ES][$MonthYear]["ContractCommission"]);
							$QuarterData["NatGridBilledCount"] = ($QuarterData["NatGridBilledCount"]+$ESDataBilled[$ES][$MonthYear]["National Grid"]["BilledCount"]);
							$QuarterData["WMECOBilledCount"] = ($QuarterData["WMECOBilledCount"]+$ESDataBilled[$ES][$MonthYear]["WMECO"]["BilledCount"]);
							$QuarterData["BGASBilledCount"] = ($QuarterData["BGASBilledCount"]+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledCount"]);
							$QuarterData["BGASBilledAmount"] = ($QuarterData["BGASBilledAmount"]+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledAmount"]);
							//hide columns if not the end of quarter
						}
						$HEACount = ($DateGrouping == "ByQuarter" ? $QuarterData["HEA"] : $BulbData[$CrewID][$MonthYear]["HEA"]);
						$HEACount = ($HEACount ? $HEACount : 0);
						$SHVCount = ($DateGrouping == "ByQuarter" ? $QuarterData["SHV"] : $BulbData[$CrewID][$MonthYear]["SHV"]);
						$SHVCount = ($SHVCount ? $SHVCount : 0);
						$BulbVisitCount = ($HEACount+$SHVCount);
						
						$BilledCount = ($DateGrouping == "ByQuarter" ? $QuarterData["BilledCount"] : $ESDataBilled[$ES][$MonthYear]["BilledCount"]);
						//$YTDBilledContracts = $BulbData[$CrewID][$MonthYear]["YTDBilledContracts"];
						$YTD_HEACount = $BulbData[$CrewID][$MonthYear]["YTDHEACount"];
//						echo "<br>YTD_HEACount ".$MonthYear."=$TotalHEACount[$MonthYear]+".$YTD_HEACount;
						$ConversionRate = ($DateGrouping == "ByQuarter" ? $QuarterData["ConversionRate"] : $ESData[$ES][$MonthYear]["ConversionRate"]);
						$YTDClosingRate  = $BulbData[$CrewID][$MonthYear]["YTDClosingRate"];
						$TotalHEACount[$MonthYear] = bcadd($TotalHEACount[$MonthYear],$YTD_HEACount,0);
//						echo "=".$TotalHEACount[$MonthYear];
						$YTDTotalHEACount = ($YTDTotalHEACount+$BulbData[$CrewID][$MonthYear]["HEA"]);
						$YTDHEACounts[$ES] = bcadd($YTDHEACounts[$ES],$HEACount,0);
						$ContractAmountOriginal = ($DateGrouping == "ByQuarter" ? $QuarterData["ContractAmountOriginal"] : $ESDataContract[$ES][$MonthYear]["ContractAmountOriginal"]);
						$ContractAmountFinal = ($DateGrouping == "ByQuarter" ? $QuarterData["ContractAmountFinal"] : $ESDataContract[$ES][$MonthYear]["ContractAmountFinal"]);
						$ContractCommission = ($DateGrouping == "ByQuarter" ? $QuarterData["ContractCommission"] : $ESDataContract[$ES][$MonthYear]["ContractCommission"]);						
						
						$NatGridBilledCount = ($DateGrouping == "ByQuarter" ? $QuarterData["NatGridBilledCount"] : $ESDataBilled[$ES][$MonthYear]["National Grid"]["BilledCount"]);
						$WMECOBilledCount = ($DateGrouping == "ByQuarter" ? $QuarterData["WMECOBilledCount"] : $ESDataBilled[$ES][$MonthYear]["WMECO"]["BilledCount"]);
						$BGASBilledCount = ($DateGrouping == "ByQuarter" ? $QuarterData["BGASBilledCount"] : $ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledCount"]);
						$BGASBilledAmount = ($DateGrouping == "ByQuarter" ? $QuarterData["BGASBilledAmount"] : $ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledAmount"]);
						
						$BulbCount = ($DateGrouping == "ByQuarter" ? $QuarterData["BulbCount"] : $BulbData[$CrewID][$MonthYear]["Bulbs"]);
						$BulbAvg = ($BulbVisitCount > 0 ? round(bcdiv($BulbCount,$BulbVisitCount,1)) : 0);
						$BulbFactor = $BulbData[$CrewID][$MonthYear]["BulbFactor"];
						$BulbCommission = ($DateGrouping == "ByQuarter" ? $QuarterData["BulbCommission"] : $BulbData[$CrewID][$MonthYear]["BulbCommission"]);
						$YTDCommissionEligibilty = $BulbData[$CrewID][$MonthYear]["YTDCommissionEligibilty"];
						$YTDCommissionEligibleByDate = $BulbData[$CrewID][$MonthYear]["YTDCommissionEligibleByDate"];
						$YTDBulbCommissionEligibleByBulb = $BulbData[$CrewID][$MonthYear]["YTDBulbCommissionEligibleByBulb"];
						//$YTDBulbCount = $BulbData[$CrewID][$MonthYear]["YTDBulbCount"];
						//$YTDBulbVisit = $BulbData[$CrewID][$MonthYear]["YTDBulbVisit"];
						$YTDBulbPerVisit = $BulbData[$CrewID][$MonthYear]["YTDBulbPerVisit"];
						$BulbBonus = $BulbData[$CrewID][$MonthYear]["BulbBonus"];
						$BulbFactorProposed = $BulbData[$CrewID][$MonthYear]["BulbFactorProposed"];
						$BulbCommissionProposed = $BulbData[$CrewID][$MonthYear]["BulbCommissionProposed"];
						$BulbFactorPrior = $BulbData[$CrewID][$MonthYear]["BulbFactorPrior"];
						$BulbCommissionPrior = $BulbData[$CrewID][$MonthYear]["BulbCommissionPrior"];
						
						$TotalBilledCount[$MonthYear] = ($TotalBilledCount[$MonthYear]+$BilledCount);
						$YTDTotalBilledCount = ($YTDTotalBilledCount+$ESDataBilled[$ES][$MonthYear]["BilledCount"]);
						$YTDBilledCount[$ES] = ($YTDBilledCount[$ES]+$ESDataBilled[$ES][$MonthYear]["BilledCount"]);

						$YTDContractOriginal[$ES] = ($YTDContractOriginal[$ES]+$ESDataContract[$ES][$MonthYear]["ContractAmountOriginal"]);
						$YTDTotalContractOriginal = ($YTDTotalContractOriginal+$ESDataContract[$ES][$MonthYear]["ContractAmountOriginal"]);
						$TotalContractOriginal[$MonthYear] = bcadd($TotalContractOriginal[$MonthYear],$ContractAmountOriginal,2);
						
						$YTDContractFinal[$ES] = ($YTDContractFinal[$ES]+$ESDataContract[$ES][$MonthYear]["ContractAmountFinal"]);
						$YTDTotalContractFinal = ($YTDTotalContractFinal+$ESDataContract[$ES][$MonthYear]["ContractAmountFinal"]);
						$TotalContractFinal[$MonthYear] = bcadd($TotalContractFinal[$MonthYear],$ContractAmountFinal,2);

						$YTDContractCommission[$ES] = ($YTDContractCommission[$ES]+$ESDataContract[$ES][$MonthYear]["ContractCommission"]);
						$YTDTotalContractCommission = ($YTDTotalContractCommission+$ESDataContract[$ES][$MonthYear]["ContractCommission"]);
						$TotalContractCommission[$MonthYear] = bcadd($TotalContractCommission[$MonthYear],$ContractCommission,2);

						$YTDNatGrid[$ES] = ($YTDNatGrid[$ES]+$ESDataBilled[$ES][$MonthYear]["National Grid"]["BilledCount"]);
						$YTDTotalNatGrid = ($YTDTotalNatGrid+$ESDataBilled[$ES][$MonthYear]["National Grid"]["BilledCount"]);
						$TotalNatGrid[$MonthYear] = ($TotalNatGrid[$MonthYear]+$NatGridBilledCount);

						$YTDWMECO[$ES] = ($YTDWMECO[$ES]+$ESDataBilled[$ES][$MonthYear]["WMECO"]["BilledCount"]);
						$YTDTotalWMECO = ($YTDTotalWMECO+$ESDataBilled[$ES][$MonthYear]["WMECO"]["BilledCount"]);
						$TotalWMECO[$MonthYear] = ($TotalWMECO[$MonthYear]+$WMECOBilledCount);
						
						$YTDBGAS[$ES] = ($YTDBGAS[$ES]+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledCount"]);
						$YTDTotalBGAS = ($YTDTotalBGAS+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledCount"]);
						$TotalBGAS[$MonthYear] = ($TotalBGAS[$MonthYear]+$BGASBilledCount);

						$YTDBGASAmount[$ES] = ($YTDBGASAmount[$ES]+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledAmount"]);
						$YTDTotalBGASAmount = ($YTDTotalBGASAmount+$ESDataBilled[$ES][$MonthYear]["BGAS"]["BilledAmount"]);
						$BGASAmount[$MonthYear] = bcadd($BGASAmount[$MonthYear],$BGASBilledAmount,2);

						$YTDBulbCount[$ES] = $YTDBulbCount[$CrewID];
						$YTDTotalBulbCount = bcadd($YTDTotalBulbCount,$BulbData[$CrewID][$MonthYear]["Bulbs"],0);
						$TotalBulbs[$MonthYear] = ($TotalBulbs[$MonthYear]+$BulbCount);
						$YTDBulbVisitCount[$ES] = ($YTDBulbVisitCount[$ES]+$BulbData[$CrewID][$MonthYear]["BulbVisitCount"]);
						$YTDTotalBulbVisitCount = ($YTDTotalBulbVisitCount+$BulbData[$CrewID][$MonthYear]["BulbVisitCount"]);
						$TotalBulbVisitCount[$MonthYear] = ($TotalBulbVisitCount[$MonthYear]+$BulbVisitCount);

						$YTDBulbCommission[$ES] = ($YTDBulbCommission[$ES]+$BulbData[$CrewID][$MonthYear]["BulbCommission"]);
						$YTDTotalBulbCommission = ($YTDTotalBulbCommission+$BulbData[$CrewID][$MonthYear]["BulbCommission"]);
						$TotalBulbCommission[$MonthYear] = bcadd($TotalBulbCommission[$MonthYear],$BulbCommission,2);

						$TotalCommission = bcadd($ContractCommission,$BulbCommission,2);
						$YTDTotalCommission[$ES] = bcadd($YTDTotalCommission[$ES],$ESDataContract[$ES][$MonthYear]["ContractCommission"],2);
						$YTDTotalCommission[$ES] = bcadd($YTDTotalCommission[$ES],$BulbData[$CrewID][$MonthYear]["BulbCommission"],2);
						$TotalYTDCommission = bcadd($TotalYTDCommission,$ESDataContract[$ES][$MonthYear]["ContractCommission"],2);
						$TotalYTDCommission = bcadd($TotalYTDCommission,$BulbData[$CrewID][$MonthYear]["BulbCommission"],2);
						$TotalTotalCommission[$MonthYear] = bcadd($TotalTotalCommission[$MonthYear],$TotalCommission,2);
						$RowDataTemplate = null;
						
						$lockDataArray["action"] = "commission_lock";
						$lockDataArray["ESData"][$ES]["CrewID"] = $CrewID;
						$lockDataArray["ESData"][$ES]["ES"] = $ES;

						/*
						if (in_array("HEASiteSrcs",$ColumnList)){
							$RowDataTemplate .= '<td'.$Title.'>'.($ESData[$ES][$MonthYear]["SiteCount"] ? $ESData[$ES][$MonthYear]["SiteCount"] : "0").'_'.($ScheduledData[$CrewID][$MonthYear]["HEA"] ? $ScheduledData[$CrewID][$MonthYear]["HEA"] : "0").'_'.($HEACount ? $HEACount : "0").'</td>';
						}
						*/
						if (in_array("HEASiteVisits",$ColumnList)){
							$DisplayVal = ($HEACount ? $HEACount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["HEACount"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["HEACount"] = $DisplayVal;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDHEACount"] = $CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["HEA"];
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["HEACount"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["HEACount"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["HEACount"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["HEACount"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}							
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BilledContracts",$ColumnList)){
							$DisplayVal = ($BilledCount ? $BilledCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BilledContracts"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BilledContracts"] = $DisplayVal;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDBilledContracts"] = $YTDBilledContracts[$CrewID];
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["BilledContracts"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BilledContracts"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BilledContracts"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BilledContracts"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("ConvRate",$ColumnList)){
							$DisplayVal = ($ConversionRate ? $ConversionRate : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["ConvRate"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["ConvRate"] = $DisplayVal;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDClosingRate"] = round(bcdiv($CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["BilledCount"],$CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["HEA"],3)*100);
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["ConvRate"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["ConvRate"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["ConvRate"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["ConvRate"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'%</td>';
						}
						if (in_array("ContractsOriginal",$ColumnList)){
							$DisplayVal = ($ContractAmountOriginal ? $ContractAmountOriginal : "0.00");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["ContractsOriginal"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["ContractsOriginal"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["ContractsOriginal"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["ContractsOriginal"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["ContractsOriginal"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["ContractsOriginal"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						if (in_array("ContractsFinal",$ColumnList)){
							$DisplayVal = ($ContractAmountFinal ? $ContractAmountFinal : "0.00");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["ContractsFinal"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["ContractsFinal"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["ContractsFinal"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["ContractsFinal"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["ContractsFinal"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["ContractsFinal"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						if (in_array("ContractsCommission",$ColumnList)){
							$isThisActive = true;
							if (MySqlDate($WarehouseRecordByName[$CrewID]->endDate)){
								$deactiveDate = (int)strtotime($WarehouseRecordByName[$CrewID]->endDate);
								$thisMonthYearDate = (int)strtotime($MonthYear."-01");
								$isThisActive = ($deactiveDate>$thisMonthYearDate ? true : false);
							}
							if (!$isThisActive){$ContractCommission = "0.00";}
							
							$DisplayValPrior = $DisplayVal = ($ContractCommission ? $ContractCommission : "0.00");
							if (!$YTDCommissionEligibleByDate){$DisplayVal = "0.00";}
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["ContractsCommission"] = $DisplayVal;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["ContractsCommissionPrior"] = $DisplayValPrior;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["ContractsCommission"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["ContractsCommission"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["ContractsCommission"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						if (in_array("NatGridBilled",$ColumnList)){
							$DisplayVal = ($NatGridBilledCount ? $NatGridBilledCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["NatGridBilled"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["NatGridBilled"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["NatGridBilled"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["NatGridBilled"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["NatGridBilled"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["NatGridBilled"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("WMECOBilled",$ColumnList)){
							$DisplayVal = ($WMECOBilledCount ? $WMECOBilledCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["WMECOBilled"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["WMECOBilled"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["WMECOBilled"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["WMECOBilled"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["WMECOBilled"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["WMECOBilled"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BGASBilled",$ColumnList)){
							$DisplayVal = ($BGASBilledCount ? $BGASBilledCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BGASBilled"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BGASBilled"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["BGASBilled"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BGASBilled"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BGASBilled"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BGASBilled"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BGASAmount",$ColumnList)){
							$DisplayVal = ($BGASBilledAmount ? $BGASBilledAmount : "0.00");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BGASAmount"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BGASAmount"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["BGASAmount"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BGASAmount"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BGASAmount"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BGASAmount"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						if (in_array("BulbVisit",$ColumnList)){
							$DisplayVal = ($BulbVisitCount ? $BulbVisitCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BulbVisit"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbVisit"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["BulbVisit"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BulbVisit"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BulbVisit"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BulbVisit"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BulbCount",$ColumnList)){
							$DisplayVal = ($BulbCount ? $BulbCount : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BulbCount"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbCount"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["BulbCount"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BulbCount"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BulbCount"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BulbCount"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BulbAvg",$ColumnList)){
							$DisplayVal = ($BulbAvg ? $BulbAvg : "0");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BulbAvg"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbAvg"] = $DisplayVal;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["BulbAvg"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BulbAvg"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BulbAvg"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BulbAvg"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BulbFactor",$ColumnList)){
							$DisplayVal = ($BulbFactor ? $BulbFactor : "0.00");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BulbFactor"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbFactor"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["BulbFactor"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BulbFactor"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BulbFactor"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BulbFactor"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("BulbCommission",$ColumnList)){
							if (!$isThisActive){$BulbCommission = "0.00";}
							$DisplayVal = ($BulbCommission ? $BulbCommission : "0.00");
							if (!$YTDCommissionEligibleByDate){$DisplayVal = "0.00";}
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["BulbCommission"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbCommission"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["BulbCommission"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["BulbCommission"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["BulbCommission"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["BulbCommission"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						if (in_array("YTDCommissionEligibilty",$ColumnList)){
							$DisplayVal = ($YTDCommissionEligibilty ? $YTDCommissionEligibilty : "");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibilty"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDCommissionEligibilty"] = $DisplayVal;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDCommissionEligibleByDate"] = $YTDCommissionEligibleByDate;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDBulbCommissionEligibleByBulb"] = $YTDBulbCommissionEligibleByBulb;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDBulbCount"] = $CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["Bulbs"];
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDBulbVisit"] = ($CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["SHV"]);
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["YTDBulbPerVisit"] = bcdiv($CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["Bulbs"],($CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID]["YTD"][date("Y",strtotime($MonthYear))]["SHV"]),0);
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbBonus"] = $BulbBonus;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbFactorProposed"] = $BulbFactorProposed;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbCommissionProposed"] = $BulbCommissionProposed;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbFactorPrior"] = $BulbFactorPrior;
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["BulbCommissionPrior"] = $BulbCommissionPrior;
								}else{
									if ($PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibilty"] != $DisplayVal){
										$PriorCommssionDataChanged[$ES][$MonthYear]["YTDCommissionEligibilty"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["YTDCommissionEligibilty"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibilty"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>'.$DisplayVal.'</td>';
						}
						if (in_array("TotalCommission",$ColumnList)){
							if (!$isThisActive){$TotalCommission = "0.00";}
							$DisplayVal = ($TotalCommission ? $TotalCommission : "0.00");
							$DisplayClass = '';
							if ($LockDataValid){
								if (trim($PriorCommissionData[$ES][$MonthYear]["TotalCommission"]) == ''){
									$lockDataArray["ESData"][$ES]["DateData"][$MonthYear]["TotalCommission"] = $DisplayVal;
								}else{
									if (bccomp($PriorCommissionData[$ES][$MonthYear]["TotalCommission"],$DisplayVal,2)){
										$PriorCommssionDataChanged[$ES][$MonthYear]["TotalCommission"]["Is"] = $DisplayVal;
										$DisplayVal = $PriorCommssionDataChanged[$ES][$MonthYear]["TotalCommission"]["Was"] = $PriorCommissionData[$ES][$MonthYear]["TotalCommission"];
										$DisplayClass = ' class="changedData"';
									}
								}
							}
							$RowDataTemplate .= '<td'.$Title.$DisplayClass.'>$'.money($DisplayVal).'</td>';
						}
						$ThisMonth = (int)($ThisMonth+1);
						if ($ThisMonth == 13){
							$ThisMonth = 1;
							$ThisMonthYear = (($ThisMonthYear+100)-11);
						}else{
							$ThisMonthYear = ($ThisMonthYear+1);
						}
						if ($DateGrouping == "ByQuarter"){
							if (!in_array($ThisQuarter,$QuarterMonthsToHide)){
								$RowData .=$RowDataTemplate;
							}
						}else{
							$RowData .=$RowDataTemplate;
						}
						
						
						$MonthYear = date("Y-m",strtotime($MonthYear."-01 + 1 month"));
						$ThisQuarter++;
					} while ($ThisMonthYear <= $EndMonthYear);					
					echo $RowDataStart;
					if ($DateGrouping != "ByYTD"){
						echo $RowData;
					}
				//add YTD at the end
				$RowDataTemplate = null;
				if (in_array("HEASiteVisits",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDHEACounts[$ES].'</td>';
				}
				if (in_array("BilledContracts",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDBilledCount[$ES].'</td>';
				}
				if (in_array("ConvRate",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.round(bcdiv($YTDBilledCount[$ES],$YTDHEACounts[$ES],3)*100).'%</td>';
				}
				if (in_array("ContractsOriginal",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDContractOriginal[$ES]).'</td>';
				}
				if (in_array("ContractsFinal",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDContractFinal[$ES]).'</td>';
				}
				if (in_array("ContractsCommission",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDContractCommission[$ES]).'</td>';
				}
				if (in_array("NatGridBilled",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDNatGrid[$ES].'</td>';
				}
				if (in_array("WMECOBilled",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDWMECO[$ES].'</td>';
				}
				if (in_array("BGASBilled",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDBGAS[$ES].'</td>';
				}
				if (in_array("BGASAmount",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDBGASAmount[$ES]).'</td>';
				}
				if (in_array("BulbVisit",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDBulbVisitCount[$ES].'</td>';
				}
				if (in_array("BulbCount",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.$YTDBulbCount[$ES].'</td>';
				}
				if (in_array("BulbAvg",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>'.bcdiv($YTDBulbCount[$ES],$YTDBulbVisitCount[$ES],0).'</td>';
				}
				if (in_array("BulbCommission",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDBulbCommission[$ES]).'</td>';
				}
				if (in_array("TotalCommission",$ColumnList)){
					$RowDataTemplate .= '<td'.$Title.'>$'.money($YTDTotalCommission[$ES]).'</td>';
				}
				echo $RowDataTemplate;
				echo '</tr>';
			}
			echo "</tbody>";
			echo "<tbody>";
			//empty $ESData to free memory
			$ESData = array();
			//Add Totals Row
				$StartMonth = date("m",strtotime($StartDate));
				$StartYear = date("Y",strtotime($StartDate));
				$EndMonth = date("m",strtotime($EndDate));
				$EndYear = date("Y",strtotime($EndDate));
				$ThisMonth = (int)($StartMonth);
				$ThisMonthYear = (int)($StartYear.$StartMonth);
				$EndMonthYear = (int)($EndYear.$EndMonth);
				$MonthYear = date("Y-m",strtotime($StartDate));
				$ThisQuarter = 1;
				$RowDataStart = '<tr class="totalsRow">
						<td class="data-control totalsRow">Totals</td>
						<td class="energySpecialist totalsRow"> </td>';
				$RowData = null;
				do {
					if (count($ColumsForGrouping[$ColumnGrouping])){
						$RowDataTemplate = '';
					}else{
						$RowDataTemplate = '';
						//$RowDataTemplate = '<td> </td>';
					}
					if (in_array("HEASiteVisits",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalHEACount[$MonthYear].'</td>';
					}
					if (in_array("BilledContracts",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalBilledCount[$MonthYear].'</td>';
					}
					if (in_array("ConvRate",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.round(bcdiv($TotalBilledCount[$MonthYear],$TotalHEACount[$MonthYear],3)*100).'%</td>';
					}
					if (in_array("ContractsOriginal",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalContractOriginal[$MonthYear]).'</td>';
					}
					if (in_array("ContractsFinal",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalContractFinal[$MonthYear]).'</td>';
					}
					if (in_array("ContractsCommission",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalContractCommission[$MonthYear]).'</td>';
					}
					if (in_array("NatGridBilled",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalNatGrid[$MonthYear].'</td>';
					}
					if (in_array("WMECOBilled",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalWMECO[$MonthYear].'</td>';
					}
					if (in_array("BGASBilled",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalBGAS[$MonthYear].'</td>';
					}
					if (in_array("BGASAmount",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($BGASAmount[$MonthYear]).'</td>';
					}
					if (in_array("BulbVisit",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalBulbVisitCount[$MonthYear].'</td>';
					}
					if (in_array("BulbCount",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.$TotalBulbs[$MonthYear].'</td>';
					}
					if (in_array("BulbAvg",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">'.bcdiv($TotalBulbs[$MonthYear],$TotalHEACount[$MonthYear],0).'</td>';
					}
					if (in_array("BulbFactor",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">&nbsp;</td>';
					}
					if (in_array("BulbCommission",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalBulbCommission[$MonthYear]).'</td>';
					}
					if (in_array("YTDCommissionEligibilty",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">&nbsp;</td>';
					}
					if (in_array("TotalCommission",$ColumnList)){
						$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalTotalCommission[$MonthYear]).'</td>';
					}
						if ($DateGrouping == "ByQuarter"){
							if (!in_array($ThisQuarter,$QuarterMonthsToHide)){
								$RowData .=$RowDataTemplate;
							}
						}else{
							$RowData .=$RowDataTemplate;
						}
					$ThisMonth = (int)($ThisMonth+1);
					if ($ThisMonth == 13){
						$ThisMonth = 1;
						$ThisMonthYear = (($ThisMonthYear+100)-11);
					}else{
						$ThisMonthYear = ($ThisMonthYear+1);
					}
					$ThisQuarter++;

					$MonthYear = date("Y-m",strtotime($MonthYear."-01 + 1 month"));
				} while ($ThisMonthYear <= $EndMonthYear);					
				//add YTD at the end
				echo $RowDataStart;
				if ($DateGrouping != "ByYTD"){
					echo $RowData;
				}
				$RowDataTemplate = null;
				if (in_array("HEASiteVisits",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalHEACount.'</td>';
				}
				if (in_array("BilledContracts",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalBilledCount.'</td>';
				}
				if (in_array("ConvRate",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.round(bcdiv($YTDTotalBilledCount,$YTDTotalHEACount,3)*100).'%</td>';
				}
				if (in_array("ContractsOriginal",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($YTDTotalContractOriginal).'</td>';
				}
				if (in_array("ContractsFinal",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($YTDTotalContractFinal).'</td>';
				}
				if (in_array("ContractsCommission",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($YTDTotalContractCommission).'</td>';
				}
				if (in_array("NatGridBilled",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalNatGrid.'</td>';
				}
				if (in_array("WMECOBilled",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalWMECO.'</td>';
				}
				if (in_array("BGASBilled",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalBGAS.'</td>';
				}
				if (in_array("BGASAmount",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($YTDTotalBGASAmount).'</td>';
				}
				if (in_array("BulbVisit",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalBulbVisitCount.'</td>';
				}
				if (in_array("BulbCount",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.$YTDTotalBulbCount.'</td>';
				}
				if (in_array("BulbAvg",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">'.bcdiv($YTDTotalBulbCount,$YTDTotalBulbVisitCount,0).'</td>';
				}
				if (in_array("BulbCommission",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($YTDTotalBulbCommission).'</td>';
				}
				if (in_array("TotalCommission",$ColumnList)){
					$RowDataTemplate .= '<td class="totalsRow">$'.money($TotalYTDCommission).'</td>';
				}
				echo $RowDataTemplate;
				echo '</tr>';
		?>
	</tbody>

</table>
<?php //print_pre($lockDataArray["ESData"]["CET_18QQQRENEE S"]["DateData"]["2015-06"]);?>
<br clear="all">
<br>
</div><!--topHalfOfReports-->
<ul id="admin-tabs" class="tabs">
	<?php if ($MonthForReport){?>
		<li><a id="tab-FiscalReport" href="#FiscalReport"><?php echo date("M", strtotime($MonthForReport));?> Fiscal Report</a></li>
		<li><a id="tab-ISMInstalls" href="#ISMInstalls">ISM Installs</a></li>
<!--		<li><a id="tab-PerformanceReports" href="#PerformanceReports">Performance Reports</a></li>-->
		<li><a id="tab-IndividualReports" href="#IndividualReports">Individual Reports</a></li>
	<?php }?>
	<li><a id="tab-RawData" href="#RawData">Raw Contract Data</a></li>
	<li><a id="tab-RawDataCurrent" href="#RawDataCurrent">Raw Contract Data <?php echo date("M");?></a></li>
	<li><a id="tab-Changes" href="#Changes">Changed Data</a></li>
    <li><a id="tab-Notes" href="#Notes">Notes</a></li>
</ul>
<div class="tabs-content">
	<?php if ($MonthForReport){?>
		<div id="FiscalReport" class="tab-content"><?php include("views/_reportCommissions_FiscalReport.php");?></div>
		<div id="IndividualReports" class="tab-content"><?php include("views/_reportCommissions_IndividualReports.php");?></div>
<!--		<div id="PerformanceReports" class="tab-content"><?php //include("views/_reportCommissions_PerformanceReports.php");?></div>-->
		<div id="ChangedCommission" class="tab-content"><?php include("views/_reportCommissions_ChangedCommission.php");?></div>
		<div id="ISMInstalls" class="tab-content"><?php include("views/_reportCommissions_ISMInstalls.php");?></div>
<?php }?>
	<div id="RawData" class="tab-content"><?php include("views/_reportCommissions_RawData.php");?></div>
	<div id="RawDataCurrent" class="tab-content"><?php include("views/_reportCommissions_RawDataCurrentMonth.php");?></div>
	<div id="Changes" class="tab-content"><?php include("views/_reportCommissions_ChangedData.php");?></div>
    <div id="Notes" class="tab-content"><?php include("views/_reportCommissions_Notes.php");?></div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		$("#printAllIndividualReports").on('click',function(){
			$("#topHalfOfReports").hide();
			window.print();
		});
		var	jsondataESNames = <?php echo json_encode($WarehouseByShortName);?>;

		function format ( d, DetailType ) {
			// `d` is the original data object for the row
			var thisItem = d,
				DetailType = DetailType,
				jsondataES = <?php echo json_encode($DetailTableRows);?>,
				jsondataContractor = <?php echo json_encode($DetailContractorTableRows);?>,
				detailDisplayRow = "";
				if (DetailType == "EnergySpecialist"){
					var ESName = thisItem.split("&nbsp;"),
						thisItem = ESName[1],
						jsondata = jsondataES;
					var TableHeader = '<tr style="font-size:8pt;"><td><a href="?nav=warehouse-management&WarehouseName='+jsondataESNames[thisItem]+'" target="_blank">'+jsondataESNames[thisItem]+'</a></td><td style="font-size:8pt;">Site Visits</td><td style="font-size:8pt;">No Work</td><td style="font-size:8pt;">Road Blocked</td><td style="font-size:8pt;">Pending Signature</td><td style="font-size:8pt;">Contracts Signed</td><td style="font-size:8pt;">Signed Rate</td><td style="font-size:8pt;">Conv Rate</td>'+
						'<td style="font-size:8pt;">Days Issue</td><td style="font-size:8pt;">Days Sign</td><td style="font-size:8pt;">Days Bill</td><td style="font-size:8pt;">Org Contract Amt</td><td style="font-size:8pt;">Final Contract Amt</td><td style="font-size:8pt;">Changed Amt</td><td style="font-size:8pt;">Avg Contract Amt</td><td style="font-size:8pt;">Billed Amt</td><td style="font-size:8pt;">Commission</td><td style="font-size:8pt;">NATGRID Contracts</td><td style="font-size:8pt;">WMECO Contracts</td><td style="font-size:8pt;">BGAS Contracts</td><td style="font-size:8pt;">BGAS Amount</td></tr>';

				}else{
					jsondata = jsondataContractor
					var TableHeader = '<tr style="font-size:8pt;"><td></td><td style="font-size:8pt;">Days Accept</td><td style="font-size:8pt;">Days Install</td><td style="font-size:8pt;">Contract Amt</td></tr>';
				}
			var thisItem = (thisItem == "Contractor Averages" ? "ZZZContractor Averages" : d.replace("&nbsp;","QQQ"));
			var	detailDisplayRow = jsondata[thisItem];
		
			return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:40px;">'+
					'<colgroup><col><col><col></colgroup>'+TableHeader+detailDisplayRow+
					'</table>';
		}
	
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var tableCommission = $('.performanceReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"bPaginate": false,
			"iDisplayLength": 200
		});
		var tableRawData = $('.rawDataReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"bPaginate": false,
			"iDisplayLength": 200
		});
		var tableCommission = $('.commissionReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"bPaginate": false,
			"iDisplayLength": 200
		});
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 50
		});
		var tableReport = $('.reportTable').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
							{
								"sExtends":    "print",
								"fnComplete": function ( nButton, oConfig, oFlash, sFlash ) {
									$(".page-break").hide();
								}
							}
				]				
			},				
			"scrollX": false,
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bSort": false,
			"bPaginate": false,
			"iDisplayLength": 200
		});
		<?php
			$LockButtonHTML	= (array_key_exists($PriorCurrentMonth,$LockedDates) ? '&nbsp;' : '<span id="lockDataResults"><a href="#" id="lockData" class="button-link do-not-navigate">Lock Prior Data</a></span>');				
			$LockButtonDisplay = ".hide()";
			if ($DateGrouping == "ByMonth" && $ColumnGrouping == "All"){
				if ($LockButtonHTML != "&nbsp;"){
					$LockButtonDisplay = ".show()";
					echo '$(".MonthForReportOption").hide();';
					echo '$("#UsePriorCalculationsSpan").show();';
				}else{
					echo '$(".MonthForReportOption").show();';
					echo '$("#UsePriorCalculationsSpan").hide();';
				}
			}
		?>
		//hide the records showing for all other tables
		var dataTables_length = $(".dataTables_length");
		$.each(dataTables_length,function(key,val){
			if (key > 0){
				$(val).hide();
			}
		});
		$("#DataTables_Table_<?php echo $DataTablesRecord;?>_length").html('<?php echo $LockButtonHTML;?>')<?php echo $LockButtonDisplay;?>;

		$("#lockData").on('click',function(e){
			e.preventDefault();
			var lockDataResults = $('#lockDataResults');
			lockDataResults.show().html("Processing...").removeClass("info").removeClass("alert").addClass("alert");

			var reportData = <?php echo json_encode($lockDataArray);?>,
				data = JSON.stringify(reportData);
				//console.log(data);
			$.ajax({
				url: "ApiWarehouseManagement.php",
				type: "PUT",
				data: data,
				success: function(data){
					//console.log(data);
					if (data.error == '' || !data.error){
						lockDataResults.show().html("Data Prior to <?php echo date("m/1/Y");?> Locked").removeClass("info").removeClass("alert").addClass("alert").addClass("info");
						$(".MonthForReportOption").show();
					}else{
						lockDataResults.show().html(data.error).removeClass("info").removeClass("alert").addClass("alert");
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					lockDataResults.show().html(message).removeClass("info").removeClass("alert").addClass("alert");
				}
			});				
			
		});
		// Add event listener for opening and closing details
		$('.reportResults tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data(),
					efi = (dataObj[1]),
					type = (dataObj[5]);
					row.child( format(efi,type) ).show();
					tr.addClass('shown');
			}
		} );
		
		var getFilters = function(){
			var fiscalYear = $("#ThisFiscalYear").val(),
				dateGrouping = $("#DateGrouping").val(),
				columnGrouping = $("#ColumnGrouping").val(),
				monthForReport = $("#MonthForReport").val(),
				showPriorCalculations = ($("#ShowPriorCalculations").prop("checked") ? 1 : 0), 
				usePriorCalculations = ($("#UsePriorCalculations").prop("checked") ? 1 : 0),
				filterValues = new Array(fiscalYear,dateGrouping,columnGrouping,monthForReport,showPriorCalculations,usePriorCalculations);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();

			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>inventory/?FiscalYear="+Filters[0]+"&DateGrouping="+Filters[1]+"&ColumnGrouping="+Filters[2]+"&MonthForReport="+Filters[3]+"&ShowPriorCalculations="+Filters[4]+"&UsePriorCalculations="+Filters[5]+"&nav=report-commissions#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		var OpenInNewTab = function(url) {
		  var win = window.open(url, '_blank');
		  win.focus();
		};
		$(".esName").on("click",function(){
			var $this = $(this),
				esName = $this.attr('data-esname'),
				url = '<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&WarehouseName='+jsondataESNames[esName];
				OpenInNewTab(url);
		});
		
		var timer = null;
		var notes = $("#notes");
		notes.on('keyup',function(){
			clearTimeout(timer);
			timer = setTimeout(updateComments, 1000);
		});
		var updateComments = function(){
			var thisVal = notes.val(),
				thisDate = notes.attr('data-fiscalyear');
			
			var Comments = {};
				Comments["action"] = "insert_commissionnote";
				Comments["noteDate"] = thisDate;
				Comments["note"] = thisVal;
				data = Comments;
				//console.log(data);
			$.ajax({
				url: "ApiWarehouseManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					//console.log(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					notes.val(message);
				}
			});
		};

		$(".disabledFormula").on("click",function(){
			var $this = $(this);
			if ($this.css("width") == "50px"){
				$this.css("width","95px");
			}else{
				$this.css("width","50px");
			}
			$this.children().toggle();
		});
		$("#ShowPriorCalculations").on('click',function(){
			$(".PriorCalculations").toggle();
		});
	} );	
</script>
<script type="text/javascript">
    $(function(){
		var tabs = $('ul.tabs');

		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
		$("#tab-RawData").click(function(){
			$("#RawDataDiv").show();
		});
        var hash = window.location.hash,
            tabHashes = new Array("#FiscalReport","#Changes","#IndividualReports","#ChangedCommission","#Notes","#ISMInstalls","#PerformanceReports","#RawData","#RawDataCurrent"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo ($MonthForReport ? "FiscalReport" : "RawData");?>");
		}
		$(".memoryusage").hide();
    });
</script>