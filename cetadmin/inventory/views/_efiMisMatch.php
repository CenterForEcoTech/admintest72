<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");

$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : "1/1/2001");
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $TodaysDate);

$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$distinctEfiByES = $CSGDailyDataProvider->getDistinctEFIByCrew($StartDate,$EndDate);
$distinctEFICollection = $distinctEfiByES->collection;
foreach ($distinctEFICollection as $id=>$efiObj){
	$crew = strtoupper($efiObj->crew);
	if ($efiObj->efiPart){
		if (!in_array(trim($efiObj->efiPart),$DistinctEFI[$crew])){
			$DistinctEFIByCrew[$crew][] = trim($efiObj->efiPart);
		}
	}
}

$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	$ProductDescriptionByEFI[$record->efi] = $record->description;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
		$DistinctEFIParts[] = $record->efi;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}
}
ksort($ProductsWithDisplay);
//resort the products without display order by EFI
foreach ($DistinctEFIByCrew as $Crew=>$EFI){
	foreach ($EFI as $id=>$efiPart){
		if (trim($efiPart) &&!in_array($efiPart,$DistinctEFIParts)){
			$EFIPartsNotInList[$Crew][]=$efiPart;
			$criteria = new stdClass();
			$criteria->efi = $efiPart;
			$criteria->crew = $Crew;
			$criteria->efiMisMatch = true;
			$partHistory = $CSGDailyDataProvider->getDailyDataByEFI($criteria);
			if (count($partHistory)){
				foreach ($partHistory as $id=>$obj){
					$efiPartHistory[$Crew][$obj->efiPart][] = $obj;
				}
			}
		}
	}
}

$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByName[$record->name] = $record->description;
}


?>
<script type="text/javascript" language="javascript" class="init">
	var removeMisMatch = function(element){
		var $this = $(element),
			id1 = $this.attr('data-id1'),
			crew = $this.attr('data-crew'),
			qty = parseInt($this.attr('data-qty')),
			thisVal = $this.val(),
			TotalCrewQty = "#TotalCount"+crew,
			TotalQty = parseInt($(TotalCrewQty).text()),
			TRid1 = ".id1"+id1,
			TRcrew = ".TR"+crew,
			DetailTable = "#DetailTable"+crew,
			NewQty = TotalQty-qty;
			
		$.ajax({
			url: "ApiProductManagement.php",
			type: "POST",
			data: {
				action : "product-efimismatch",
				id1 : id1,
				efi : thisVal,
				moveInventory : $("#MoveInventory").val()
			},
			success: function(data){
				console.log(data);
				$(TotalCrewQty).fadeOut().text(NewQty).fadeIn();
				$(TRid1).fadeOut();
				if (NewQty < 1){
					$(TRcrew).fadeOut();		
					$(DetailTable).fadeOut();
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				console.log(message);
			}
		});
		
		
		
	};
		
	$(document).ready(function() {
		function format ( d,crew ) {
			// `d` is the original data object for the row
			var efi = d,
				crew = crew,
				jsondata = <?php echo json_encode($efiPartHistory);?>,
				productdata = <?php echo json_encode($ProductDescriptionByEFI);?>,
				detailDisplayRow = "",
				totalCount = 0;
			var efiPicker = "<option value=''>Choose Correct efiPart</option><?php 
				foreach ($ProductsWithDisplay as $DisplayID=>$record){
						echo "<option value='".$record->efi."'>".$record->efi." ".$record->description."</option>";
				}?>";
			$.each(jsondata[crew], function( key, jdata ) {
				$.each(jdata, function( key, valueObj ) {
					if (valueObj.efiPart == efi){
						if (key < (jdata.length)){ 
							detailDisplayRow =  detailDisplayRow+'<tr class="id1'+valueObj.id1+'"><td>Details</td><td></td></tr>';
						}
						var efiSelector = ' <select class="RemoveMisMatch" data-crew="'+crew+'" data-qty="'+valueObj.qty+'" data-id1="'+valueObj.id1+'" onchange="removeMisMatch(this);">'+efiPicker+'</select>';
						$.each(valueObj, function( Objkey, Objvalue ) {
							detailDisplayRow =  detailDisplayRow+'<tr class="id1'+valueObj.id1+'">'+
							'<td align="right">'+Objkey+'</td>'+
							'<td>'+Objvalue;
							if (Objkey == "efiPart"){
								detailDisplayRow =  detailDisplayRow+' '+productdata[Objvalue]+'<br>'+efiSelector;
							}
							detailDisplayRow =  detailDisplayRow+'</td>'+
							'</tr>';
							
							if (Objkey == "qty"){
								totalCount = (totalCount+parseInt(Objvalue));
							}
						});
					}//end if efi
				});
			});
			if (totalCount > 0){
				totalCountDisplayRow = '<tr><td>Total Qty: <span id="TotalCount'+crew+'" data-qty="'+totalCount+'">'+totalCount+'</span></td><td></td></tr>';
			}
			
			return '<table id="DetailTable'+crew+'" class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
				'<colgroup><col><col><col></colgroup>'+
				totalCountDisplayRow+detailDisplayRow+
				'</table>';
		}
	
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		


		var table = $('#efiMisMatch').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
		
		// Add event listener for opening and closing details
		$('#efiMisMatch tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data(),
					efi = (dataObj[3]);
					crew = (dataObj[1]);
					row.child( format(efi,crew) ).show();
					tr.addClass('shown');
			}
		} );
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>inventory/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=report-efimismatch#";
		}
		$("button").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});

	} );	
</script>
<style>
#allCount th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>

<h3>EFI Recorded in CSG Data but not currently used by CET</h3>
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : "");?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : "");?>" title="End Date">
<button>reset filters</button>
<select id="MoveInventory"><option value="yes">Move Inventory Count</option><option value="">Do Not Move Inventory Count</option></select>
<table id="efiMisMatch" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th></th>
			<th>ES ID</th>
			<th>Energy Specialist</th>
			<th>EFI</th>
			<th>Status</th>
		</tr>
	</thead>

	<tbody>
		<?php
			foreach ($EFIPartsNotInList as $Crew=>$EfiPart){
				foreach ($EfiPart as $id=>$efiPart){
					echo "<tr class='TR".$Crew."'>";
					echo "<td class=\"details-control\">";
					echo "<td>".$Crew."</td>";
					echo "<td><a href=\"?nav=warehouse-management&WarehouseName=".$Crew."\">".$Crew."</a>".($WarehouseByName[$Crew] != $Crew ? " ".$WarehouseByName[$Crew] : "")."</td>";
					echo "<td>".$efiPart."</td>";
					echo "<td><a href=\"?nav=product-management&EFI=".$efiPart."\">".$efiPart.($ProductsWithOutDisplay[$efiPart] ? " not active" : " not found")."</a></td>";
				}
				echo "</tr>";
			}
		?>
	</tbody>
</table>
<input type="hidden" id="javascriptinserter" value="">