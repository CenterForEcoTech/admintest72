<div class="page-break">&nbsp;</div>
			<div class="row">
				<div class="fifteen columns">
					<table class="reportTable display" cellspacing="0" width="100%">
						<thead style="background-color:#D6EAFF;">
							<?php 
								$PageBreakStyle = "";
								$HEAJobDetails = "style='display:block;'";
								include('views/_reportCommissions_IndividualReports_Detail_HeaderTop.php');
								include('views/_reportCommissions_IndividualReports_Detail_HeaderTotals.php');
							?>
							<?php
								$oldTotalCommission = bcadd($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"],$PriorCommissionData[$ES][$MonthYear]["BulbCommission"],2);
								$newTotalCommission = bcadd(($PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibleByDate"] ? $PriorCommissionData[$ES][$MonthYear]["ContractsCommission"] : 0.00),$PriorCommissionData[$ES][$MonthYear]["BulbCommissionProposed"],2);
								if (bccomp($oldTotalCommission,$newTotalCommission,2)){
									$CommissionChangeReport[$ES][$MonthYear]["WAS"] = $oldTotalCommission;
									$CommissionChangeReport[$ES][$MonthYear]["IS"] = $newTotalCommission;
									$CommissionChangeReport[$ES][$MonthYear]["Diff"] = bcsub($oldTotalCommission,$newTotalCommission,2);
								}
								include('views/_reportCommissions_IndividualReports_Detail_HeaderHidden.php');

							?>
						</thead>
						<?php
							$rowCounter = 0;
							if (!$MonthYearInfoEmpty){
								//resort by SiteID
								ksort($MonthYearInfo);
								foreach ($MonthYearInfo as $SiteID=>$SiteData){
									$PageBreakStyle = "";
									$HEAJobDetails = "style='display:block;'";
									if ($rowCounter == 7){
										$HEAJobDetailsPageNum++;
										$PageBreakStyle = "border-top: 3pt solid red;page-break-before: always;";
										$HEAJobDetails = "style='display:none;'";
										$rowCounter = 0;
									?>	
												</table>
											</div><!--fifteen col-->
										</div><!--row-->
										<div class="page-break">&nbsp;</div>
										<div class="row">
											<div class="fifteen columns">
												<table class="reportTable display" cellspacing="0" width="100%">
													<thead style="background-color:#D6EAFF;">
									<?php	
														include('views/_reportCommissions_IndividualReports_Detail_HeaderTop.php');
														include('views/_reportCommissions_IndividualReports_Detail_HeaderHidden.php');
									}	
									$rowCounter++;

							?>
								<tbody style="<?php echo $PageBreakStyle;?>">
									<tr style="background-color:white;<?php //echo $PageBreakStyle;?>">
										<td class="HeaderRow">
											<div class="HeaderLabel">Site ID</div>
											<div class="HeaderValue"><?php echo $SiteID;?></div>
											<div class="HeaderDetails Details_AS">
												<div class="ContractType">AS</div>
												<div class="ContractDetailsDate">
													Billing Date<br>
													<span class="ContractValue"><?php echo ($SiteData["AS"]["BilledDate"] ? $SiteData["AS"]["BilledDate"] : $SiteIDContractReport[$SiteID]["AS"]["BilledDate"]);?></span>
												</div>
											</div>
										</td>
										
										<td class="HeaderRow" colspan="2">
											<div class="HeaderLabel">Name</div>
											<div class="HeaderValue"><?php echo $SiteIDs[$SiteID]["Name"];?></div>
											<div class="HeaderDetails Details_AS">
												<div class="ContractDetailsDiv">
													Original Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["AS"]["ContractAmountOriginal"] ? money($SiteData["AS"]["ContractAmountOriginal"]) : money($SiteIDContractReport[$SiteID]["AS"]["ContractAmountOriginal"]));?></span>
												</div>
												<div class="ContractDetailsDiv" style="float:right;">
													Final Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["AS"]["ContractAmountFinal"] ? money($SiteData["AS"]["ContractAmountFinal"]) : money($SiteIDContractReport[$SiteID]["AS"]["ContractAmountFinal"]));?></span>
												</div>
											</div>
										</td>
										<td style="display:none;"></td>
										
										<td class="HeaderRow">
											<div class="HeaderLabel">Utility</div>
											<div class="HeaderValue"><?php echo $SiteIDs[$SiteID]["Utility"];?></div>
											<div class="HeaderDetails Details_INS">
												<div class="ContractType">INS</div>
												<div class="ContractDetailsDate">
													Billing Date<br>
													<span class="ContractValue"><?php echo ($SiteData["INS"]["BilledDate"] ? $SiteData["INS"]["BilledDate"] : $SiteIDContractReport[$SiteID]["INS"]["BilledDate"]);?></span>
												</div>
											</div>
										</td>
										<td class="HeaderRow">
											<div class="HeaderLabel">&nbsp;</div>
											<div class="HeaderValue">&nbsp;</div>
											<div class="HeaderDetails Details_INS">
												<div class="ContractDetailsDiv" style="width:100%;">
													Original Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["INS"]["ContractAmountOriginal"] ? money($SiteData["INS"]["ContractAmountOriginal"]) : money($SiteIDContractReport[$SiteID]["INS"]["ContractAmountOriginal"]));?></span>
												</div>
											</div>
										</td>
										<td class="HeaderRow">
											<div class="HeaderLabel">HEA Date</div>
											<div class="HeaderValue"><?php echo MySQLDate($SiteIDs[$SiteID]["HEADate"]);?></div>
											<div class="HeaderDetails Details_INS">
												<div class="ContractDetailsDiv" style="width:100%;">
													Final Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["INS"]["ContractAmountFinal"] ? money($SiteData["INS"]["ContractAmountFinal"]) : money($SiteIDContractReport[$SiteID]["INS"]["ContractAmountFinal"]));?></span>
												</div>
											</div>
										</td>
										
										<td class="HeaderRow">
											<div class="HeaderLabel">&nbsp;</div>
											<div class="HeaderValue">&nbsp;</div>
											<div class="HeaderDetails Details_TSTAT">
												<div class="ContractType" style="width:30%;">TSTAT</div>
												<div class="ContractDetailsDate" style="width:60%;">
													Billing Date<br>
													<span class="ContractValue"><?php echo ($SiteData["TSTAT"]["BilledDate"] ? $SiteData["TSTAT"]["BilledDate"] : $SiteIDContractReport[$SiteID]["TSTAT"]["BilledDate"]);?></span>
												</div>
											</div>
										</td>
										<td class="HeaderRow">
											<div class="HeaderLabel">&nbsp;</div>
											<div class="HeaderValue">&nbsp;</div>
											<div class="HeaderDetails Details_TSTAT">
												<div class="ContractDetailsDiv" style="width:100%;">
													Original Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["TSTAT"]["ContractAmountOriginal"] ? money($SiteData["TSTAT"]["ContractAmountOriginal"]) : money($SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountOriginal"]));?></span>
												</div>
											</div>
										</td>
										<td class="HeaderRow">
											<div class="HeaderLabel">&nbsp;</div>
											<div class="HeaderValue">&nbsp;</div>
											<div class="HeaderDetails Details_TSTAT">
												<div class="ContractDetailsDiv" style="width:100%;">
													Final Amt<br>
													<span class="ContractValue">$<?php echo ($SiteData["TSTAT"]["ContractAmountFinal"] ? money($SiteData["TSTAT"]["ContractAmountFinal"]) : money($SiteIDContractReport[$SiteID]["TSTAT"]["ContractAmountFinal"]));?></span>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
<?php 
								}//end foreach MonthYearInfo 
							}else{
						?>
								<tbody>
									<tr style="background-color:white;<?php echo $PageBreakStyle;?>">
											<td class="HeaderRow">&nbsp;</td>
											<td class="HeaderRow" colspan="2">
											<td style="display:none;"></td>
											<td class="HeaderRow">&nbsp;</td>
											<td class="HeaderRow" style="border:1pt solid black;font-weight:bold;font-size:+1em;width:300px;text-align:center;"><?php echo ($MonthYearInfoEmptyText ? $MonthYearInfoEmptyText : "No Contracts Billed This Month for ".$CrewID);?></td>
											<td class="HeaderRow">&nbsp;</td>
											<td class="HeaderRow">&nbsp;</td>
											<td class="HeaderRow">&nbsp;</td>
											<td class="HeaderRow">&nbsp;</td>
									</tr>
								</tbody>

						<?php
							}//end if MonthYearInfoEmpty
						?>
						<!--
						<tfoot>
							<tr>
								<th colspan="3" valign="top">
									<?php echo $WarehouseNameByShortName[strtolower($ESName[1])]->description;?>
								</th>
								<td colspan="3" valign="top">
									<?php $TotalCommission = bcadd($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"],$PriorCommissionData[$ES][$MonthYear]["BulbCommission"],2);?>
									<?php $TotalCommissionProposed = bcadd(($PriorCommissionData[$ES][$MonthYear]["YTDCommissionEligibleByDate"] ? $PriorCommissionData[$ES][$MonthYear]["ContractsCommission"] : 0.00),$PriorCommissionData[$ES][$MonthYear]["BulbCommissionProposed"],2);?>
									Total Commission: $<?php echo money($TotalCommission);?><br>
									Total Commission Proposed: $<?php echo money($TotalCommissionProposed);?> 
								</td>
								<th colspan="3" valign="top">
									<?php echo date("M Y",strtotime($MonthYear."-01"));?>
								</th>
							</tr>
						</tfoot>
						-->
					</table>
				</div>
			</div>
