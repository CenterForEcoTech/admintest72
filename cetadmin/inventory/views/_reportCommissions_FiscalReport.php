<?php
if (count($PriorCommissionData)){
	echo "<style>.ESName {min-width: 150px;}</style>";
	echo "<h4>".date("F Y",strtotime($MonthForReport))." Fiscal Report</h4>";
	echo '<div class="row"><div class="twelve columns">';
	echo '<table class="commissionReport display" cellspacing="0" width="100%">
			<thead style="background-color:#D6EAFF;">
				<tr>
					<th class="ESName">After June 2015</th>
					<th colspan="3">'.date("M-Y",strtotime($MonthForReport)).'</th>
					<th colspan="3">YTD Cumulative</th>
					<th colspan="3">Prior June 2015</th>
					<th colspan="3">Prior YTD Cumulative</th>
				</tr>
				<tr>
					<th class="ESName">Energy Specialist</th>
					<th>0.02</th><th>Bulb</th><th>Total</th>
					<th>0.02</th><th>Bulb</th><th>Total</th>
					<th>Prior Contract</th><th>Prior Bulb</th><th>Prior Total</th>
					<th>YTD Prior Contract</th><th>YTD Prior Bulb</th><th>YTD Prior Total</th>
				</tr>
			</thead>
			<tbody>';
	$MonthYear = date("Y-m",strtotime($MonthForReport));
		foreach ($PriorCommissionData as $ES=>$MonthYearData){
			$ESName = explode("QQQ",$ES);
			$CrewID = strtoupper(str_replace("XX","",$ESName[0]));
			$CrewIDSort = $CrewID;
			$CrewParts = explode("_",$CrewID);
			if (strlen($CrewParts[1]) < 3){
				$CrewIDSort = str_replace("_","_0",$CrewID);
			}
			$trStyle = "";
			if (!in_array($CrewID,$DisplayHideExceptions) && MySQLDate($WarehouseRecordByName[$CrewID]->endDate) && strtotime($WarehouseRecordByName[$CrewID]->endDate) < strtotime($StartDate)){
				$trStyle = " style='display:none;border:1pt solid red;'";
			}

			echo "<tr".$trStyle."><td valign=\"top\">".$CrewIDSort." ".$ESName[1]."</td>";
					$totalCommission = bcadd($MonthYearData[$MonthYear]["ContractsCommission"],$MonthYearData[$MonthYear]["BulbCommission"],2);
					$totalCommissionPrior = bcadd($MonthYearData[$MonthYear]["ContractsCommissionPrior"],$MonthYearData[$MonthYear]["BulbCommissionPrior"],2);
					echo "<td>$".money($MonthYearData[$MonthYear]["ContractsCommission"])."</td>";
					echo "<td>$".money($MonthYearData[$MonthYear]["BulbCommission"])."</td>";
					echo "<td>$".money($totalCommission)."</td>";
					$totalContract = bcadd($totalContract,$MonthYearData[$MonthYear]["ContractsCommission"],2);
					$totalBulb = bcadd($totalBulb,$MonthYearData[$MonthYear]["BulbCommission"],2);
					$totalTotal = bcadd($totalTotal,$totalCommission,2);
					$YTDContractsCommission = 0;
					$YTDBulbCommission = 0;
					$TotalCommission = 0;
					foreach ($MonthYearData as $ThisMonthYear=>$MonthYearInfo){
						if (strtotime($ThisMonthYear."-01") <= strtotime($MonthForReport)){
							$YTDContractsCommission = bcadd($YTDContractsCommission,$MonthYearInfo["ContractsCommission"],2);
							$YTDBulbCommission = bcadd($YTDBulbCommission,$MonthYearInfo["BulbCommission"],2);
							$YTDTotalCommission = bcadd($YTDContractsCommission,$YTDBulbCommission,2);
						}
					}
					echo "<td>$".money($YTDContractsCommission)."</td>";
					echo "<td>$".money($YTDBulbCommission)."</td>";
					echo "<td>$".money($YTDTotalCommission)."</td>";
					$totalContractYTD = bcadd($totalContractYTD,$YTDContractsCommission,2);
					$totalBulbYTD = bcadd($totalBulbYTD,$YTDBulbCommission,2);
					$totalTotalYTD = bcadd($totalTotalYTD,$YTDTotalCommission,2);
					
					echo "<td>$".money($MonthYearData[$MonthYear]["ContractsCommissionPrior"])."</td>";
					echo "<td>$".money($MonthYearData[$MonthYear]["BulbCommissionPrior"])."</td>";
					echo "<td>$".money($totalCommissionPrior)."</td>";
					$totalContractPrior = bcadd($totalContractPrior,$MonthYearData[$MonthYear]["ContractsCommissionPrior"],2);
					$totalBulbPrior = bcadd($totalBulbPrior,$MonthYearData[$MonthYear]["BulbCommissionPrior"],2);
					$totalTotalPrior = bcadd($totalTotalPrior,$totalCommissionPrior,2);
					$YTDContractsCommissionPrior = 0;
					$YTDBulbCommissionPrior = 0;
					$YTDTotalCommissionPrior = 0;
					foreach ($MonthYearData as $ThisMonthYear=>$MonthYearInfo){
						if (strtotime($ThisMonthYear."-01") <= strtotime($MonthForReport)){
							$YTDContractsCommissionPrior = bcadd($YTDContractsCommissionPrior,$MonthYearInfo["ContractsCommissionPrior"],2);
							$YTDBulbCommissionPrior = bcadd($YTDBulbCommissionPrior,$MonthYearInfo["BulbCommissionPrior"],2);
							$YTDTotalCommissionPrior = bcadd($YTDContractsCommissionPrior,$YTDBulbCommissionPrior,2);
						}
					}
					echo "<td>$".money($YTDContractsCommissionPrior)."</td>";
					echo "<td>$".money($YTDBulbCommissionPrior)."</td>";
					echo "<td>$".money($YTDTotalCommissionPrior)."</td>";
					$totalContractPriorYTD = bcadd($totalContractPriorYTD,$YTDContractsCommissionPrior,2);
					$totalBulbPriorYTD = bcadd($totalBulbPriorYTD,$YTDBulbCommissionPrior,2);
					$totalTotalPriorYTD = bcadd($totalTotalPriorYTD,$YTDTotalCommissionPrior,2);
			echo "</tr>";
		}
		echo "<tfoot>
			<tr>
				<td class=\"ESName\">Total Commissions</td>
				<td>$".money($totalContract)."</td><td>$".money($totalBulb)."</td><td>$".money($totalTotal)."</td>
				<td>$".money($totalContractYTD)."</td><td>$".money($totalBulbYTD)."</td><td>$".money($totalTotalYTD)."</td>
				<td>$".money($totalContractPrior)."</td><td>$".money($totalBulbPrior)."</td><td>$".money($totalTotalPrior)."</td>
				<td>$".money($totalContractPriorYTD)."</td><td>$".money($totalBulbPriorYTD)."</td><td>$".money($totalTotalPriorYTD)."</td>
			</tr>
		</tfoot>";
	echo "</tbody></table>";
	echo '</div></div>';
}
?>