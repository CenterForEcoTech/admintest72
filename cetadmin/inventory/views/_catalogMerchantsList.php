<?php if ($record->id) { ?>
    <h3>Merchants</h3>
    <div class="ten columns">
        <table id="merchant-table">
            <thead>
            <tr>
                <th>Merchant ID</th>
                <th>Merchant</th>
                <th>Num Images</th>
                <th></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <fieldset class="five columns" id="add-merchant-widget">
        <legend>Add Merchants</legend>
        <label>
            <div>By Merchant ID:</div>
            <input type="text" id="merchant-id-selector">
        </label>
        <input type="button" class="button-link" value="Add">
        <label>
            <div>Filter on Campaign:</div>
            <select id="campaign-selector">
            </select>
        </label>
        <div id="merchant-selector"></div>
    </fieldset>
    <script type="text/javascript">
        $(function(){
            var table = $("#merchant-table"),
                apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiImageCatalog.php",
                lastRequestedCriteria = null,
                oTable = table.dataTable({
                    "bJQueryUI": true,
                    "bServerSide":true,
                    "sAjaxSource": apiUrl,
                    "iDisplayLength": 50,
                    "aaSorting": [ [0, "desc"]],
                    "aoColumns" :[
                        { "mData": "merchantIdDisplay", "sWidth": "50px" },
                        { "mData": "contactInfo" },
                        { "mData": "numImages", "sWidth": "50px" },
                        { "mData": "id",
                            "mRender": function ( data, type, full ) {
                            return '<a href="delete-template" class="delete-link do-not-navigate" title="delete">Delete</a>';
                        }}
                    ],
                    "aoColumnDefs": [
                        { "bSearchable": false, "aTargets": [ 3 ] },
                        { "bSortable": false, "aTargets": [ 3 ] }
                    ],
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "action", "value": "get_merchants" } );
                        aoData.push( { "name": "catalog_id", "value": "<?php echo $record->id;?>" } );
                    },
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        lastRequestedCriteria = aoData;
                        oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": lastRequestedCriteria,
                            "success": fnCallback
                        } );
                    }
                }),

                campaignSelector = $("#campaign-selector"),
                merchantSelector = $("#merchant-selector"),
                merchantIdField = $("#merchant-id-selector"),
                resetSection = function(){
                    oTable.fnDraw();
                    campaignSelector.val("");
                    merchantIdField.val("");
                    merchantSelector.empty();
                };

            // load campaign selector
            $.ajax({
                url: apiUrl,
                type: "GET",
                data: {
                    action: "get_campaigns"
                },
                success: function(data){
                    var record;
                    campaignSelector.append("<option value=''>-- none --</option>")
                    if ($.isArray(data) && data.length){
                        for (var i = 0; i < data.length; i++){
                            record = data[i];
                            campaignSelector.append("<option value='" + record.id + "'>" + record.name + "</option>");
                        }
                    }
                }
            });

            campaignSelector.on("change", function(e){
                var campaignId = campaignSelector.find(":selected").val();
                if (campaignId){
                    $.ajax({
                        url: apiUrl,
                        type: "GET",
                        data: {
                            action: "find_merchants",
                            filter: "campaign",
                            campaign_id: campaignId,
                            catalog_id: <?php echo $record->id;?>
                        },
                        success: function(data){
                            var record,
                                template = "<div><label><input type='checkbox' class='merchant-select' value='{merchantId}'> {merchantName}</label></div>",
                                element;
                            if ($.isArray(data) && data.length){
                                merchantSelector.empty();
                                merchantSelector.append("<div><label><input type='checkbox' class='select-all-merchants'> Select All</label></div>");
                                merchantSelector.append("<hr>");
                                for (var i = 0; i < data.length; i++){
                                    record = data[i];
                                    element = template.replace(/{merchantId}/g, record.merchantId).replace(/{merchantName}/g, record.name);
                                    merchantSelector.append(element);
                                }
                            } else {
                                alert("no new merchants found");
                            }
                        }
                    });
                }
            });

            merchantSelector.on("change", ".select-all-merchants", function(e){
                var self = $(this);
                if (self.is(":checked")){
                    merchantSelector.find(".merchant-select").prop("checked", true);
                }
            });

            merchantSelector.on("change", ".merchant-select", function(e){
                var self = $(this);
                if (self.not(":checked")){
                    merchantSelector.find(".select-all-merchants").prop("checked", false);
                }
            });

            $("#add-merchant-widget").on("click", ".button-link", function(e){
                var merchantIds = $.map(merchantSelector.find(".merchant-select:checked"), function(element, index){
                        return $(element).val();
                    }),
                    manualId = merchantIdField.val(),
                    data = {
                        action: "add_merchants",
                        id: <?php echo $record->id;?>,
                        merchantIds: merchantIds
                    };
                if (manualId){
                    merchantIds.push(manualId);
                }
                if (merchantIds.length){
                    $.ajax({
                        url: apiUrl,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function(data){
                            if (data.numAdded){
                                resetSection()
                            } else {
                                alert("No merchants added.");
                            }
                        }
                    });
                }
            });

            table.on("click", ".delete-link", function(e){
                var row = $(this),
                    rowId = row.closest("tr").attr("id"),
                    dataId = rowId.replace("merchant-", "");
                e.preventDefault();
                $.ajax({
                    url: apiUrl,
                    type: "POST",
                    data: JSON.stringify({
                        action: "remove_merchant",
                        id: <?php echo $record->id;?>,
                        merchantId: dataId
                    }),
                    success: function(data){
                        resetSection();
                    }
                });
            });
        });
    </script>
<?php } ?>