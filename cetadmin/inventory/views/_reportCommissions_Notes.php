<Br>
<a name="Notes">Notes:</a>
<textarea style="width:100%;" id="notes" data-fiscalyear="<?php echo $FiscalStart;?>"><?php echo $CommissionNote;?></textarea>
<?php echo ($CommissionLastEditedBy ? "Last Edited By: ".$CommissionLastEditedBy : "");?>
<ul>
<?php 
$Note = array_unique($Notes);
ksort($Note);
foreach($Note as $note){
	echo "<li>".$note.".</li>";
}
?>
</ul>
