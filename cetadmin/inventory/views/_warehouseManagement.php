<?php
ini_set("display_errors", "on"); error_reporting(1);

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
		$EmployeesByManager[$record->manager][]=$record;
		$EmployeesByFullName[$record->fullName]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
	if ($record->isManager){
		$Managers[] = $record->firstName." ".$record->lastName;
	}

}
//resort the products without display order by EmployeeID
ksort($EmployeesInActive);


include_once($dbProviderFolder."InventoryProvider.php");

$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByID[$record->id] = $record;
	$WarehouseByName[$record->name] = $record->id;
	if ($record->activeId){
		$WarehouseActiveDisplay[$record->displayOrderId]=$record;
	}else{
		$WarehouseNotActiveDisplay[$record->id]=$record;
	}

}
//resort the warehouses without display order by Name
ksort($WarehouseNotActiveDisplay);
$SelectedWarehouseName = $_GET['WarehouseName'];
$SelectedWarehouse = $_GET['WarehouseID'];
if ($SelectedWarehouseName && $WarehouseByName[$SelectedWarehouseName]){
	$SelectedWarehouse = $WarehouseByName[$SelectedWarehouseName];
}
$addWarehouse = $_GET['addWarehouse'];


$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	$ProductDescription[$record->efi] = $record->description;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
		$ProductList[] = $record->efi;
	}
}

$criteria = new stdClass();
$autofillqtyResult = $warehouseProvider->getfillqty(); 
$autofillqty = $autofillqtyResult->collection;

//	print_pre($autofillqty);
foreach ($autofillqty as $autofilldata){
	$warehouse = $autofilldata->warehouseName;
	$efi = $autofilldata->efi;
	$qty = $autofilldata->qty;
	$AutoFillQty[$warehouse][$efi] = $qty;
}

?>
<?php if (!$addWarehouse){?>
  <style>
  #sortable1, #sortable2 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:300px;
	overflow-y: auto;
	overflow-x: hidden;
	
  }
  #sortable1 li, #sortable2 li {
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .efiDescription {font-weight:normal;}
  .efi {font-weight:bold;}

  </style><br>
  	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&addWarehouse=true" class="button-link">+ Add New Warehouse Location</a>
		</div>
	<?php }// end if not readonly ?>
	<div class="fifteen columns"><bR>
		<button id="showWarehouseSelectForm">Choose A Different Warehouse</button>
	</div>
	<fieldset id="warehouseSelectForm">
		<legend>Choose A Warehouse In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Active Locations</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($WarehouseActiveDisplay as $warehouse=>$warehouseInfo){
						echo "<li id=\"".$warehouseInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=warehouse-management&WarehouseID=".$warehouseInfo->id."\">".$warehouseInfo->name." <span class=\"efiDescription\">".($warehouseInfo->name != $warehouseInfo->description ? $warehouseInfo->description : '')."</span></a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Inactive Locations</b></div>
				<ul id="sortable2" class="connectedSortable">
				<?php 
					foreach ($WarehouseNotActiveDisplay as $warehouse=>$warehouseInfo){
						echo "<li id=\"".$warehouseInfo->id."\" class=\"ui-state-disabled\"><a href=\"?nav=warehouse-management&WarehouseID=".$warehouseInfo->id."\" style=\"text-decoration:none;\"><span class=\"efi\">".$warehouseInfo->name."</span> &nbsp;&nbsp;".($warehouseInfo->name != $warehouseInfo->description ? $warehouseInfo->description : '')."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedWarehouse || $addWarehouse){?>
	<form id="WarehouseUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Warehouse Location Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<input type="text" id="displayOrderId" name="displayOrderId" value="<?php echo ($addWarehouse ? (count($WarehouseActiveDisplay)+1) : $WarehouseByID[$SelectedWarehouse]->displayOrderId);?>">
					<input type="text" name="activeId" value="<?php echo ($addWarehouse ? 1 : $WarehouseByID[$SelectedWarehouse]->activeId);?>">
					<?php if (!$addWarehouse){?><input type="text" name="id" value="<?php echo $WarehouseByID[$SelectedWarehouse]->id;?>"><?php }?>
				</div>
			
				<div class="fourteen columns">
					<fieldset>
						<legend>Identifiers</legend>
						<div class="ten columns">
							<div class="three columns">Warehouse Name:</div><div class="four columns"><input type="text" name="name" value="<?php echo $WarehouseByID[$SelectedWarehouse]->name;?>"></div><div class="two columns">&nbsp;</div>
							<div class="three columns">Description:</div><div class="six columns">
								<input type="text" name="description" value="<?php echo $WarehouseByID[$SelectedWarehouse]->description;?>"><br>
								&nbsp;&nbsp;Linked to Primary Employee:<Br>
								<input type="hidden" name="linkedToEmployeeId" id="linkedToEmployeeId" value="<?php echo $WarehouseByID[$SelectedWarehouse]->linkedToEmployeeId;?>">
								<?php
									$employeeComboBoxHideLabel = true;
									$EmployeeID = $WarehouseByID[$SelectedWarehouse]->linkedToEmployeeId;
									$employeeComboBoxFunction = "$('#linkedToEmployeeId').val($(ui.item).val());";
									include('_EmployeeComboBox.php');
								?>
								<br><Br>
								&nbsp;&nbsp;Linked to Secondary Employee:<Br>
								<input type="hidden" name="linkedToSecondEmployeeId" id="linkedToSecondEmployeeId" value="<?php echo $WarehouseByID[$SelectedWarehouse]->linkedToSecondEmployeeId;?>">
								<?php
									$employeeComboBoxHideLabel = true;
									$employeeComboBoxSelectID = "Second";
									$EmployeeID = $WarehouseByID[$SelectedWarehouse]->linkedToSecondEmployeeId;
									$employeeComboBoxFunction = "$('#linkedToSecondEmployeeId').val($(ui.item).val());";
									include('_EmployeeComboBox.php');
								?><br>
								(Should be who is tracking in accounting)

							</div><div class="one columns">&nbsp;</div>
							<Br clear="all"><br>
							<div class="three columns">Start Date:</div><div class="four columns"><input type="text" class="date" name="startDate" value="<?php echo MySQLDate($WarehouseByID[$SelectedWarehouse]->startDate);?>"></div><div class="two columns">&nbsp;</div>
							<div class="three columns">End Date:</div><div class="four columns"><input type="text" class="date" name="endDate" value="<?php echo MySQLDate($WarehouseByID[$SelectedWarehouse]->endDate);?>"></div><div class="two columns">&nbsp;</div>
							<div class="four columns" title="Check this to keep item moved to this warehouse from being counted in Total Count">Exclude From On Hand Count:</div>
							<div class="four columns">
								<input type="checkbox" id="warehouse_ExcludeID_checkbox"<?php if($WarehouseByID[$SelectedWarehouse]->excludeFromCountId){ echo " checked";}?>>
								<input type="hidden" name="excludeFromCountId" id="warehouse_ExcludeID_input" value="<?php echo $WarehouseByID[$SelectedWarehouse]->excludeFromCountId;?>">
							</div>
							<br clear="all">
							<div class="four columns" title="Check this to apply different commission rate to Lead Specialist">Lead Specialist:</div>
							<div class="four columns">
								<input type="checkbox" id="warehouse_Lead_checkbox"<?php if($WarehouseByID[$SelectedWarehouse]->lead){ echo " checked";}?>>
								<input type="hidden" name="lead" id="warehouse_Lead_input" value="<?php echo $WarehouseByID[$SelectedWarehouse]->lead;?>">
							</div>
							<br clear="all">
							<div class="four columns" title="Check this if bulb commission rate not tied to closing rate">Bulb Commission not tied to Closing Rate:</div>
							<div class="four columns">
								<input type="checkbox" id="warehouse_BulbRate_checkbox"<?php if($WarehouseByID[$SelectedWarehouse]->bulbRateNotTiedToClosingRate){ echo " checked";}?>>
								<input type="hidden" name="bulbRateNotTiedToClosingRate" id="warehouse_BulbRate_input" value="<?php echo $WarehouseByID[$SelectedWarehouse]->bulbRateNotTiedToClosingRate;?>">
							</div>
							<br clear="all">
							<div class="four columns" title="Check this if Warehouse is not part of GHS">Warehouse outside of GHS:</div>
							<div class="four columns">
								<input type="checkbox" id="warehouse_OutsideGHS_checkbox"<?php if($WarehouseByID[$SelectedWarehouse]->outsideGHS){ echo " checked";}?>>
								<input type="hidden" name="outsideGHS" id="warehouse_OutsideGHS_input" value="<?php echo $WarehouseByID[$SelectedWarehouse]->outsideGHS;?>">
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="warehouseResults" class="fifteen columns"></div>
	  	<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&WarehouseID=<?php echo $SelectedWarehouse;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addWarehouse){?>
					<a href="save-warehouse-add" id="save-warehouse-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="warehouse_add">
				<?php }else{?>
					<a href="save-warehouse-update" id="save-warehouse-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="warehouse_update">
				<?php }?>

			</div>
		<?php }//end if not read only ?>
	</form>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&addWarehouse=true" class="button-link">+ Add New Warehouse Location</a>
		</div>
	<?php }//end if not readonly ?>
<?php }?>
<script>
$(function(){
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});
	
	$("#showWarehouseSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedWarehouse){?>
		$("#warehouseSelectForm").hide();
		$("#showWarehouseSelectForm").show();
	<?php }?>
	$("#showWarehouseSelectForm").click(function(){
		$("#warehouseSelectForm").show();		
		$(this).hide();
	});

	$("#warehouseSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	$("#warehouse_ExcludeID_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#warehouse_ExcludeID_input").val(1);
		}else{
			$("#warehouse_ExcludeID_input").val(0);
		}
	});
	$("#warehouse_Lead_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#warehouse_Lead_input").val(1);
		}else{
			$("#warehouse_Lead_input").val(0);
		}
	});
	$("#warehouse_BulbRate_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#warehouse_BulbRate_input").val(1);
		}else{
			$("#warehouse_BulbRate_input").val(0);
		}
	});
	$("#warehouse_OutsideGHS_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#warehouse_OutsideGHS_input").val(1);
		}else{
			$("#warehouse_OutsideGHS_input").val(0);
		}
	});
	
	var formElement = $("#WarehouseUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
			description: {
				required: true
            }
		},
        messages: {
            name: {
                required: "Location must have a name. If Energy Specialist use format of CET_???"
            },
            description: {
                required: "Can be the same as name but must be included."
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-warehouse-update").click(function(e){

        $('#warehouseResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiWarehouseManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#warehouseResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#warehouseResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-warehouse-add").click(function(e){

        $('#warehouseResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiWarehouseManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					//console.log(data);
                    $('#warehouseResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));
                },
                error: function(jqXHR, textStatus, errorThrown){
					//console.log(jqXHR);
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#warehouseResults').html(message).addClass("alert");
                }
            });
        }
    });
});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
				if ($(ui.item).parents('#sortable1').length > 0) {
					$(ui.item).switchClass('ui-state-disabled', 'ui-state-default');
				} else {
					$(ui.item).switchClass('ui-state-default', 'ui-state-disabled');
				}					
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiWarehouseManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
	<?php }//not in readonly ?>
});



</script>
<?php if ($SelectedWarehouse){?>
	<script type="text/javascript">
	$(function () {
		var seriesOptions = [],
			seriesCounter = 0,
			names = ['<?php echo implode("','",$ProductList);?>'],
			// create the chart when all data is loaded
			createChart = function () {

				$('#containerChart').highcharts('StockChart', {
					chart: {
						height: 430
					},
					plotOptions: {
						series: {
							marker: {
								enabled: true
							}
						}
					},
					title:{
						text:'Item Count History'
					},
					credits:{enabled:false},
					rangeSelector: {
						inputEnabled: $('#containerChart').width() > 480,
						selected: 4
					},
					legend:{
						enabled: true,
						layout:'vertical',
						align:'left',
						verticalAlign:'top',
						backgroundColor:'#fff',
						borderColor:'#ccc',
						borderWidth:.5,
						y:30,
						x:0,
						itemWidth:135,
						itemStyle:{
							fontWeight:'bold'
						},
						itemHiddenStyle:{
							fontWeight:'bold'
						},
						title: {
							text: 'EFI<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
							style: {
								fontStyle: 'italic'
							}
						}
					},
					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						plotLines: [{
							value: 0,
							width: 2,
							color: 'silver'
						}]
					},


					tooltip: {
						pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
						valueDecimals: 2
					},
					series: seriesOptions
				});
			};

		$.each(names, function (i, name) {
			var url = '../stockchart/data.php?type=warehouse&warehouse=<?php echo $WarehouseByID[$SelectedWarehouse]->name;?>&EFI='+name+'&callback=?';
			$.getJSON(url,    function (data) {
				//console.log(data)

				seriesOptions[i] = {
					name: name,
					data: data
				};

				// As we're loading the data asynchronously, we don't know what order it will arrive. So
				// we keep a counter and create the chart when all the data is loaded.
				seriesCounter += 1;

				if (seriesCounter === names.length) {
					createChart();
				}
			});
		});
		var AdjustmentResultsTable = $(".AdjustmentResultsTable");
		var AdjustmentResults = AdjustmentResultsTable.DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 20
		});

	});
	</script>
	<script src="../js/highstock/highstock.js"></script>
	<script src="../js/highstock/modules/exporting.js"></script>
	<br clear="all">
	<div id="containerChart"></div>
	<?php include_once('views/_warehouseManagementFillQty.php');?>
<?php }//end if $SelectedWarehouse ?>