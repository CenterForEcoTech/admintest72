<tr>
								<td colspan="9">
									<table style="width:100%;border:1pt solid black;">
										<tr style="border-top:1pt solid black;">
											<td></td>
											<td>HEA & SHV</td>
											<td>Aerators / avg</td>
											<td>Showerheads / avg</td>
											<td>Powerstrips / avg</td>
											<td>Tstat / avg</td>
											<td>Bulbs</td>
											<td>Bulbs/Visit</td>
											<td></td>
										</tr>
										<tr>
											<?php $BulbVisit = ($BulbData[$CrewID][$MonthYear]["HEA"]+$BulbData[$CrewID][$MonthYear]["SHV"]); ?>

											<td>Total for <?php echo date("M",strtotime($MonthYear."-01"));?></td>
											<td><?php echo ($BulbVisit ? $BulbVisit : "0");?></td>
											<td><?php echo ($BulbData[$CrewID][$MonthYear]["Aerator"] ? $BulbData[$CrewID][$MonthYear]["Aerator"] : "0");?> / <?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Aerator"],$BulbVisit,2);?></td>
											<td><?php echo ($BulbData[$CrewID][$MonthYear]["Showerhead"] ? $BulbData[$CrewID][$MonthYear]["Showerhead"] : "0");?> / <?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Showerhead"],$BulbVisit,2);?></td>
											<td><?php echo ($BulbData[$CrewID][$MonthYear]["PowerStrip"] ? $BulbData[$CrewID][$MonthYear]["PowerStrip"] : "0");?> / <?php echo bcdiv($BulbData[$CrewID][$MonthYear]["PowerStrip"],$BulbVisit,2);?></td>
											<td><?php echo ($BulbData[$CrewID][$MonthYear]["Tstat"] ? $BulbData[$CrewID][$MonthYear]["Tstat"] : "0");?> / <?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Tstat"],$BulbVisit,2);?></td>
											<td><?php echo ($BulbData[$CrewID][$MonthYear]["Bulbs"] ? $BulbData[$CrewID][$MonthYear]["Bulbs"] : "0");?></td>
											<td><div class="disabled" style="width:60px;"><?php echo round($PriorCommissionData[$ES][$MonthYear]["BulbCount"]/$PriorCommissionData[$ES][$MonthYear]["BulbVisit"]);?></div></td>
											<td></td>
										</tr>
										<tr>
											<?php $YTDBulbVisit = ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["SHV"]);?>
											<td>YTD</td>					
											<td><?php echo $YTDBulbVisit;?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"] : "0");?> / <?php echo bcdiv($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"],$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"] : "0");?> / <?php echo bcdiv($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"],$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"] : "0");?> / <?php echo bcdiv($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"],$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"] : "0");?> / <?php echo bcdiv($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"],$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"] : "0");?></td>
											<td><div class="disabled" style="width:60px;"><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"]/$YTDBulbVisit);?></div></td>											
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
