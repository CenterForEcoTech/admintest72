<?php
ini_set("display_errors", "on"); error_reporting(1);

$MovementType = explode("-",$nav);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");

	$warehouseProvider = new WarehouseProvider($dataConn);
	$paginationResult = $warehouseProvider->get(); 
	$resultArray = $paginationResult->collection;
	//print_pre($resultArray);
	$excludeList = array();
	$includeBlankDefault = true;
	$classList = "historyFilter";
	$FromWarehouse = $warehouseProvider->createSelectPulldown($resultArray,"Warehouse",$FromWarehouseDefault,$excludeList,$includeBlankDefault,$classList);

?>
<script type="text/javascript">
function format ( d ) {
	// `d` is the original data object for the row
	var detailData = d.purchaseOrder,
		detailType = "PurchaseOrder:",
		detailDisplayRow = "";
		if (detailData.indexOf('ProjectID:') > 0){
			detailType = " &nbsp; ";
			detailData = detailData.replace(/,/g,"<br>");
		}
		detailDisplayRow =  detailDisplayRow+'<tr>'+
			'<td align="right">'+detailType+'</td>'+
			'<td>'+detailData+'</td>'+
			'</tr>';
	return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
		'<colgroup><col><col><col></colgroup>'+
		detailDisplayRow+
		'</table>';
}



    $(function(){
		
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 1);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		
	
        var productListTable = $("#product-list"),
            //apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiTagManagement.php",
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>inventory/ApiProductManagement.php",
            lastRequestedCriteria = null,
			
			CreateDataTable = function (movementType,warehouseName,startDate,endDate) {
				table = productListTable.DataTable({
					dom: 'T<"clear">lfrtip',
					tableTools: {
						"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					},				
					"bJQueryUI": true,
					"bAutoWidth": false,
					"sAjaxSource": apiUrl,
					"iDisplayLength": 50,
					"aaSorting": [ [0, "asc"]],
					"aoColumns" :[
						{
							"sClass": 'details-control',
							"mData": "efi",
							"mRender": function ( data, type, full ) {
								return '';
							},
							"sWidth": "5px"
						},
						{
							"mData": "movementType",
							"sWidth": "5px"
						},
						{
							"mData": "warehouseFrom",
							"mRender": function ( data, type, full ) {
								return '<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&WarehouseName='+data+'">'+data+'</a>';
							},
							"sWidth": "100px"
						},
						{
							"mData": "warehouseTo",
							"mRender": function ( data, type, full ) {
								return '<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&WarehouseName='+data+'">'+data+'</a>';
							},
							"sWidth": "85px"
						},
						{
							"mData": "transferDate",
							"sWidth": "75px"
						},
						{
							"mData": "efi",
							"sWidth": "10%"
						},
						{
							"mData": "units",
							"sWidth": "10%"
						},
						{
							"mData": "createdBy",
							"sWidth": "10%"
						},
						{
							"mData": "purchaseOrder",
							"sWidth": "10%"
						}
					],
					"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "action", "value": "transferhistory" } );
						aoData.push( { "name": "criteria[movementType]", "value": movementType } );
						aoData.push( { "name": "criteria[warehouseAny]", "value": warehouseName } );
						aoData.push( { "name": "criteria[startDate]", "value": startDate } );
						aoData.push( { "name": "criteria[endDate]", "value": endDate } );
					},
					"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						lastRequestedCriteria = aoData;
						oSettings.jqXHR = $.ajax( {
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": lastRequestedCriteria,
							"success": fnCallback
						} );
					}
				});
			};
		
	
		// Add event listener for opening and closing details
		$('#product-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data();
				if (!dataObj.purchaseOrder){
					dataObj.purchaseOrder = 'No Purchase Order';
				}
				if (dataObj.purchaseOrder){
					row.child( format(row.data()) ).show();
					tr.addClass('shown');
				}
			}
		} );
		var getFilters = function(){
			var movementType = $("#movementTypeSelect").val(),
				warehouseName = $("#Warehouse").val(),
				startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(movementType,warehouseName,startDate,endDate);
				
				return filterValues;
		};
		var updateFilters = function(){
			var Filters=getFilters();
			productListTable.dataTable().fnDestroy();
			CreateDataTable(Filters[0],Filters[1],Filters[2],Filters[3]);
		}
		
		$(".historyFilter").on('change', function () {
			updateFilters();
		});	
		$("button").on('click',function(){
			$("#movementTypeSelect").val('Transfer');
			$("#Warehouse").val('');
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		CreateDataTable('Transfer','');		
    });
</script>
<h3>Product Transfer History</h3>
Filters:<br>
<select id="movementTypeSelect" class="historyFilter"><option value="Transfer">Show Only Transfers</option><option value="Installed">Show Only Installed</option><option value="Bulb">Show Only Bulb Report Corrections</option><option value="">Show All Movement Types</option></select>
<?php echo $FromWarehouse;?>
&nbsp;
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="" title="End Date">
<button>reset filters</button>
<table id="product-list" class="hover">
    <thead>
    <tr>
		<th></th>
		<th>Type</th>
		<th>From</th>
		<th>To</th>
		<th>Date</th>
        <th>EFI</th>
        <th>Qty</th>
        <th>By</th>
        <th>Info</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
