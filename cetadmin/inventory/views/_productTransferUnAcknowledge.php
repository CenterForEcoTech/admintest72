<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$productProvider = new ProductProvider($dataConn);
$productReceiptDetails = $productProvider->getUnAcknowledgedMovementRecordEmail();
//print_pre($productReceiptDetails);
echo "<h2>Product Transfers prior to ".date("m/d/Y",strtotime(date()." -7 days")). " Not Yet Acknowledged</h2>";
if (count($productReceiptDetails)){
	foreach($productReceiptDetails as $receiptDetail){
		$thisID = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_ID"];
	?>
	<div class="row">
		<div class="twelve columns">
			<?php echo nl2br($receiptDetail["GHSInventoryProductsMovementHistoryRecord_Text"]);?>
			Days Overdue: <?php echo $receiptDetail["Days"];?>
			<div id="messageResult<?php echo $thisID;?>">
				<div id="thumbtypes<?php echo $thisID;?>">
					All Good: <span style="cursor:pointer;" class="thumb" data-id="<?php echo $thisID;?>"><img src="<?php echo $CurrentServer;?>images/thumbsup.png" style="height:20px;"></span>
					Something Wrong: <span style="cursor:pointer;" class="thumb thumbsdown" data-id="<?php echo $thisID;?>"><img src="<?php echo $CurrentServer;?>images/thumbsdown.png" style="height:20px;"></span>
				</div>
				<div id="note<?php echo $thisID;?>" class="twelve columns" style="display:none;">
					Include a brief note describing what was wrong:<br>
					<textarea id="discrepancy<?php echo $thisID;?>"></textarea><br>
					<button class="sendNote" data-id="<?php echo $thisID;?>">Send Note</button>
				</div>

			</div>
			<hr>
		</div>	
	</div>
	<?php		
	}
}else{
	echo "All Product Transfers prior to ".date("m/d/Y",strtotime(date()." -7 days"))." have been acknowledged";	
}
?>
<script>
    $(function(){
		var thumbs = $(".thumb");
		thumbs.on('click',function(){
			var $this = $(this),
				thisID = $this.attr('data-id'),
				thisMessageID = "#messageResult"+thisID,
				thisThumbType = "#thumbtypes"+thisID;
			$this.html('processing...');
			data = {};
			data["id"] = thisID;
			data["action"] = "productmovementacknowledged";
			datastring = JSON.parse(JSON.stringify(data));
				$.ajax({
					type: "POST",
					url: "<?php echo $CurrentServer;?>productDiscrepancySubmit",
					data: datastring,
					dataType: "json",
					success: function(data) {		
						$(thisThumbType).html('Marked Acknowledged on <?php echo $TodaysDate;?>');
					}
				});
		});
		$(".thumbsdown").on('click',function(){
			var $this = $(this),
				thisID = $this.attr('data-id'),
				thisNote = "#note"+thisID;
				$(thisNote).toggle();
		});
		$(".sendNote").on('click',function(){
			var $this = $(this),
				thisID = $this.attr('data-id'),
				thisMessageID = "#messageResult"+thisID,
				thisDiscrepancyID = "#discrepancy"+thisID;
			$this.html('processing...');
			$this.attr('disabled',true);
			data = {};
			data["id"] = $this.attr('data-id');
			data["action"] = "productmovementdiscrepancy";
			data["discrepancy"] = $(thisDiscrepancyID).val();
			datastring = JSON.parse(JSON.stringify(data));
				$.ajax({
					type: "POST",
					url: "<?php echo $CurrentServer;?>productDiscrepancySubmit",
					data: datastring,
					dataType: "json",
					success: function(data) {
						$this.html('message sent');
						$(thisMessageID).html('Your note has been included.');
					}
				});
		});
	});
</script>