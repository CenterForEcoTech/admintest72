<h4><?php echo date("F Y",strtotime($MonthForReport));?> Performance Reports</h4>
<?php 
	if ($MonthForReport != ""){?>
		<style>
			.rowSpacer {height:20px;}
		</style>
		<div class="page-break">&nbsp;</div>
		<div class="row">
			<div class="fifteen columns">
				<table class="performanceReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php	
							//print_pre($PriorCommissionData);
							foreach ($PriorCommissionData as $ES=>$MonthData){
								$ESName = explode("QQQ",$ES);
								$CrewID = strtoupper(str_replace("XX","",$ESName[0]));
								$CrewParts = explode("_",$CrewID);
								$CrewIDSort = $CrewID;
								if (strlen($CrewParts[1]) < 3){
									$CrewIDSort = str_replace("_","_0",$CrewID);
								}
								$MonthYear = date("Y-m",strtotime($MonthForReport));
								$BulbVisit = ($BulbData[$CrewID][$MonthYear]["HEA"]+$BulbData[$CrewID][$MonthYear]["SHV"]);
								include('views/_reportCommissions_PerformanceReports_details.php');
							}//end foreach PriorCommissionData
						?>
					
					</tbody>
				</table>
			</div>
		</div>
<?php
	}else{//end if $MonthForReport
	 echo "Choose a Month to Display";
	}
?>