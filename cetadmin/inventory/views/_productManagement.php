<?php
ini_set("display_errors", "on"); error_reporting(1);

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."ProductProvider.php");

$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}

}
//resort the products without display order by EFI
ksort($ProductsWithOutDisplay);
$SelectedEFI = $_GET['EFI'];
$addProduct = $_GET['addproduct'];
if ($SelectedEFI && !$ProductsByEFI[$SelectedEFI]->id){
	$SelectedEFI = false;
	$addProduct=true;
	$EFINotFound = true;
}
?>
<?php if (!$addProduct){?>
  <style>
  #sortable1, #sortable2 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:380px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li {
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .efiDescription {font-weight:normal;}
  .efi {font-weight:bold;}

  </style>
	<div class="fifteen columns"><bR>
		<button id="showProductSelectForm">Choose A Different Product</button>
	</div>
	<fieldset id="productSelectForm">
		<legend>Choose A Product In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Displayed Items</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($ProductsWithDisplay as $product=>$productInfo){
						echo "<li id=\"".$productInfo->efi."\" class=\"ui-state-default\"><a href=\"?nav=product-management&EFI=".$productInfo->efi."\">".$productInfo->efi." <span class=\"efiDescription\">".$productInfo->description."</span></a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Hidden Items</b></div>
				<ul id="sortable2" class="connectedSortable">
				<?php 
					foreach ($ProductsWithOutDisplay as $product=>$productInfo){
						echo "<li id=\"".$productInfo->efi."\" class=\"ui-state-disabled\"><a href=\"?nav=product-management&EFI=".$productInfo->efi."\" style=\"text-decoration:none;\"><span class=\"efi\">".$productInfo->efi."</span> &nbsp;&nbsp;".$productInfo->description."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedEFI || $addProduct){?>
	<div id="productResultsNotFound" class="fifteen columns alert"><?php if ($EFINotFound){echo "EFI was not found in our current product list";}?></div>
	<form id="ProductUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Products Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<?php if (!$addProduct){?><input type="text" name="id" value="<?php echo $ProductsByEFI[$SelectedEFI]->id;?>"><?php }?>
				</div>

				<div class="fourteen columns">
					<fieldset>
						<legend>Identifiers</legend>
						<div class="eight columns">
							<div class="eight columns">
								<div class="two columns">EFI SKU:</div><input type="text" class="two columns" name="efi" value="<?php echo $ProductsByEFI[$SelectedEFI]->efi;?>">
							</div>
							<div class="eight columns">
								<div class="two columns">Description:</div><input type="text" class="four columns" name="description" value="<?php echo $ProductsByEFI[$SelectedEFI]->description;?>">
							</div>
							<div class="eight columns">
								<div class="two columns">CSG Code(s):</div><input type="text" class="five columns" name="csgCode" value="<?php echo $ProductsByEFI[$SelectedEFI]->csgCode;?>">
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Bulb Details</legend>
						<div class="four columns">
							<?php 
								$BulbStyles = array('A-Lamps','Decorative','Globes','Reflector','Spirals','WIFI');
								$BulbTypes = array('CFL','LED');
								$BulbClasses = array('','_REFLECTOR','_EISA EXEMPT');
							?>
							Bulb Style:
								<select name="bulbStyle">
									<option value="">Choose a Style</option>
									<?php
										foreach ($BulbStyles as $BulbStyle){
											echo "<option value='".$BulbStyle."'".($BulbStyle==$ProductsByEFI[$SelectedEFI]->bulbStyle ? " selected":"").">".$BulbStyle."</option>";
										}
									?>
								</select>
							<br>
							Bulb Type:
									<select name="bulbType">
										<option value="">Choose a Type</option>
									<?php
										foreach ($BulbTypes as $BulbType){
											echo "<option value='".$BulbType."'".($BulbType==$ProductsByEFI[$SelectedEFI]->bulbType ? " selected":"").">".$BulbType."</option>";
										}
									?>
								</select>
							Bulb Class:
									<select name="bulbClass">
									<?php
										foreach ($BulbClasses as $BulbClass){
											echo "<option value='".$BulbClass."'".($BulbClass==$ProductsByEFI[$SelectedEFI]->bulbClass ? " selected":"").">".$BulbClass."</option>";
										}
									?>
								</select><br>
								<span style='font-size:8pt;'>(for eversource export tracking)</span>
						</div>
					</fieldset>
				</div>
				<div class="fourteen columns">
					<fieldset>
						<legend>Display Options</legend>
						<div class="thirteen columns">
							<div class="two columns">
								<input type="checkbox" id="product_ActiveID_checkbox"<?php if($ProductsByEFI[$SelectedEFI]->activeId){ echo " checked";}?>> Is Active
								<input type="hidden" name="activeId" id="product_ActiveID_input" value="<?php echo $ProductsByEFI[$SelectedEFI]->activeId;?>">
							</div>
							<div class="four columns">
								<input type="checkbox" id="product_CanBeOrderedID_checkbox"<?php if($ProductsByEFI[$SelectedEFI]->canBeOrderedId){ echo " checked";}?>> Include on Order List
								<input type="hidden" name="canBeOrderedId" id="product_CanBeOrderedID_input" value="<?php echo $ProductsByEFI[$SelectedEFI]->canBeOrderedId;?>">
							</div>
							<div class="four columns">
								<?php if(!$ProductsByEFI[$SelectedEFI]->displayOrderId){?>
									<input type="checkbox" id="product_NoDisplayID_checkbox" checked disabled> Hidden From Display
								<?php }else{
									echo "Display Order: ".$ProductsByEFI[$SelectedEFI]->displayOrderId;
									}
								?>
							</div>
							<input type="hidden" name="displayOrderId" value="<?php echo $ProductsByEFI[$SelectedEFI]->displayOrderId;?>">
						</div>
					</fieldset>
				</div>
				<div class="fourteen columns">
					<fieldset>
						<legend>Order Details</legend>
						<div class="three columns">
							Units Per Pack:<input type="text" name="unitsPerPack" value="<?php echo $ProductsByEFI[$SelectedEFI]->unitsPerPack;?>" class="input_text_tiny"><br>
							Units Per Case:<input type="text" name="unitsPerCase" value="<?php echo $ProductsByEFI[$SelectedEFI]->unitsPerCase;?>" class="input_text_tiny">
						</div>
						<div class="four columns">
							<div class="five columns">
								<div class="three columns">Cost Per Unit:</div>$<input type="text" class="input_text_small" name="cost" value="<?php echo $ProductsByEFI[$SelectedEFI]->cost;?>">
							</div>
							<div class="five columns">
								<div class="three columns">Retail Cost Per Unit:</div>$<input type="text" class="input_text_small" name="retail" value="<?php echo $ProductsByEFI[$SelectedEFI]->retail;?>">
							</div>
						</div>
						<div class="six columns">
							<div class="six columns">
								<div class="four columns"><span title="Auto-Calculated based on data from CSG for the Prior # of Days">Min Qty to keep on Hand:</div><input type="text" class="input_text_small" name="minimumQty" value="<?php echo $ProductsByEFI[$SelectedEFI]->minimumQty;?>">
							</div>
							<div class="six columns" title="As Set in System Configs">
								<div class="four columns"><span>Days to Determine Min Qty:</div><?php echo $ProductsByEFI[$SelectedEFI]->minimumDays;?>
								<?php if ($addProduct){?>
									<?php echo $Config_DaysForInstallCount;?>
									<input type="hidden" name="minimumDays" value="<?php echo $Config_DaysForInstallCount;?>">
								<?php }else{?>
									<input type="hidden" name="minimumDays" value="<?php echo $ProductsByEFI[$SelectedEFI]->minimumDays;?>">
								<?php }?>
							</div>
							<div class="six columns">
								<div class="four columns"><span title="Override Amount to order instead of based on 3 month usage average">Override Qty to Order: <?php echo ($Config_OverrideOrderQty && $Config_OverrideOrderQty != 'false' ? '(active)' : '(inactive)');?></div><input type="text" class="input_text_small" name="orderQty" value="<?php echo $ProductsByEFI[$SelectedEFI]->orderQty;?>">
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="productResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-management&EFI=<?php echo $SelectedEFI;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addProduct){?>
					<a href="save-product-add" id="save-product-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="product_add">
				<?php }else{?>
					<a href="save-product-update" id="save-product-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="product_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-management&addproduct=true" class="button-link">+ Add New Product</a>
		</div>
	<?php }?>
<?php }?>
<script>
$(function(){
	$("#showProductSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedEFI){?>
		$("#productSelectForm").hide();
		$("#showProductSelectForm").show();
	<?php }?>
	$("#showProductSelectForm").click(function(){
		$("#productSelectForm").show();		
		$(this).hide();
	});

	$("#productSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	$("#product_ActiveID_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#product_ActiveID_input").val(1);
		}else{
			$("#product_ActiveID_input").val(0);
		}
	});
	$("#product_CanBeOrderedID_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#product_CanBeOrderedID_input").val(1);
		}else{
			$("#product_CanBeOrderedID_input").val(0);
		}
	});
	 var formElement = $("#ProductUpdateForm");
	 
	 formElement.validate({
        rules: {
            efi: {
                required: true
            },
			unitsPerPack: {number: true},
			unitsPerCase: {number: true},
			cost: {number: true},
			retail: {number: true},
			minimumQty: {number: true}
        },
        messages: {
            efi: {
                required: "Product must have a SKU number for EFI.",
                maxlength: "Title must be 75 characters or less."
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-product-update").click(function(e){

        $('#productResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiProductManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					console.log(data);
                    $('#productResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#productResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-product-add").click(function(e){

        $('#productResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiProductManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					//console.log(data);
                    $('#productResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
					//console.log(jqXHR);
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#productResults').html(message).addClass("alert");
                }
            });
        }
    });
});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
				if ($(ui.item).parents('#sortable1').length > 0) {
					$(ui.item).switchClass('ui-state-disabled', 'ui-state-default');
				} else {
					$(ui.item).switchClass('ui-state-default', 'ui-state-disabled');
				}					
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiProductManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
	<?php }//not in readonly?>
});
</script>
<?php if ($SelectedEFI){?>
	<script type="text/javascript">
	$(function () {
		var seriesOptions = [],
			seriesCounter = 0,
			seriesOptionsCount = [],
			seriesCounterCount = 0,
			names = ['<?php echo $SelectedEFI;?>'],
			// create the chart when all data is loaded
			createChart = function () {

				$('#containerChart').highcharts('StockChart', {
					title:{
						text:'Minimum On Hand History'
					},

					rangeSelector: {
						inputEnabled: $('#containerChart').width() > 480,
						selected: 4
					},

					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						plotLines: [{
							value: 0,
							width: 2,
							color: 'silver'
						}]
					},


					tooltip: {
						pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
						valueDecimals: 2
					},

					series: seriesOptions
				});
			};
			createChartCount = function () {

				$('#containerChartCount').highcharts('StockChart', {
					title:{
						text:'On Hand History'
					},

					rangeSelector: {
						inputEnabled: $('#containerChartCount').width() > 480,
						selected: 4
					},

					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						plotLines: [{
							value: 0,
							width: 2,
							color: 'silver'
						}]
					},


					tooltip: {
						pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
						valueDecimals: 2
					},

					series: seriesOptionsCount
				});
			};

		$.each(names, function (i, name) {
			var url = '../stockchart/data.php?type=product&EFI='+name+'&callback=?';
			$.getJSON(url,    function (data) {
				//console.log(data)

				seriesOptions[i] = {
					name: name,
					data: data
				};

				// As we're loading the data asynchronously, we don't know what order it will arrive. So
				// we keep a counter and create the chart when all the data is loaded.
				seriesCounter += 1;

				if (seriesCounter === names.length) {
					createChart();
				}
			});
		});
		$.each(names, function (i, name) {
			var url = '../stockchart/data.php?type=productOnHand&EFI='+name+'&callback=?';
			$.getJSON(url,    function (data) {
				//console.log(data)

				seriesOptionsCount[i] = {
					name: name,
					data: data
				};

				// As we're loading the data asynchronously, we don't know what order it will arrive. So
				// we keep a counter and create the chart when all the data is loaded.
				seriesCounterCount += 1;

				if (seriesCounterCount === names.length) {
					createChartCount();
				}
			});
		});
		
		
	});
	</script>
	<script src="../js/highstock/highstock.js"></script>
	<script src="../js/highstock/modules/exporting.js"></script>
	<br clear="all">
	<div id="containerChart"></div>
	<hr>
	<div id="containerChartCount"></div>
<?php }//end if $SelectedEFI ?>
