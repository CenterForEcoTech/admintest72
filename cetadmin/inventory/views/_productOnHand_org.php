<?php
ini_set("display_errors", "on"); error_reporting(1);

if ($Config_ThresholdQtyCompare == 'Nonotuck'){
	$countCompareData = 'data.detailCount["NONOTUCK"]';
	$countCompareRow = 'row.detailCount["NONOTUCK"]';
}else{
	$countCompareData = 'data.totalCount';							
	$countCompareRow = 'row.totalCount';							
}



include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");


$warehouseProvider = new WarehouseProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$paginationResult = $warehouseProvider->get($criteria); 
$resultArray = $paginationResult->collection;
$criteria = new WarehouseCriteria();
$criteria->mostrecent = true;
$warehouseCount = $warehouseProvider->getAllWarehouseCount($criteria);
$warehouseList = array();

foreach ($resultArray as $WarehouseID=>$WarehouseDetail){
	$orderWareHouseByID[$WarehouseDetail->name]=$WarehouseDetail->displayOrderId;
}
//print_pre($orderWareHouseByID);
foreach ($warehouseCount->collection as $warehouseCollection=>$warehouseCountDetails){
	$warehouseName = strtoupper($warehouseCountDetails->warehouseName);
	$EFIDetailCount[$warehouseCountDetails->efi][$warehouseName] = $EFICount[$warehouseCountDetails->efi][$warehouseName]+$warehouseCountDetails->qty;
}
//print_pre($EFIDetailCount);
$productProvider = new ProductProvider($dataConn);
$paginationResult = $productProvider->get();
$resultArray = $paginationResult->collection;

$csv_hdr ="Warehouse";
$csvhdr[] = "Warehouse";
foreach ($resultArray as $ID=>$productInfo){
			$EFI = $productInfo->efi;
			$csv_hdr .= ",|".$EFI."|";
			$csvhdr[] = $EFI;
			$reorderQty[$EFI]["reorder"] = $productInfo->orderQty;
			$reorderQty[$EFI]["trigger"] = $productInfo->minimumQty;
}
$csv_hdr .="NEWLINE<br>";

foreach ($orderWareHouseByID as $warehouseName=>$displayOrderId){
	$csv_row .= $warehouseName;
	$csvrow[$warehouseName][] = $warehouseName;
		foreach ($resultArray as $ID=>$productInfo){
			$EFI = $productInfo->efi;
			$csv_row .= ",".($EFIDetailCount[$EFI][strtoupper($warehouseName)] ? $EFIDetailCount[$EFI][strtoupper($warehouseName)] : 0);
			$csvrow[$warehouseName][] = ($EFIDetailCount[$EFI][strtoupper($warehouseName)] ? $EFIDetailCount[$EFI][strtoupper($warehouseName)] : 0);
			if ($warehouseName =="Nonotuck" || substr($warehouseName,0,3)=="CET" || $warehouseName=="Broken" || $warehouseName=="Defective"){
				$csvrowTotal[$EFI] = $csvrowTotal[$EFI]+($EFIDetailCount[$EFI][strtoupper($warehouseName)] ? $EFIDetailCount[$EFI][strtoupper($warehouseName)] : 0);
			}
		}
	$csv_row .= "NEWLINE<Br>";
}




?>
<script type="text/javascript" language="javascript" class="init">
	function format ( d ) {
		// `d` is the original data object for the row
		var detailCount = d.detailCount,
			detailDisplayWarehouse = "",
			detailDisplayCount = "",
			detailDisplayRow = "",
			totalCountDisplayRow = "",		
			totalCount = 0;
		$.each( detailCount, function( key, value ) {
			detailDisplayRow =  detailDisplayRow+'<tr>'+
				'<td align="right">'+key+'</td>'+
				'<td>'+value+'</td>'+
				'</tr>';
			totalCount = (totalCount+parseInt(value));
			
		});
		if (totalCount > 0){
			totalCountDisplayRow = '<tr>'+
				'<td align="right">Total</td>'+
				'<td>'+totalCount+'</td>'+
			'</tr>';
		}
		
		return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
			'<colgroup><col><col><col></colgroup>'+
			detailDisplayRow+
			totalCountDisplayRow+
			'</table>';
	}



	$(document).ready(function() {
		var TotalNonotuck=0;
		var TotalOnHand=0;
		var apiUrl = "ApiProductManagement.php?action=get-onhand";
		var table = $('#product-list').DataTable( {
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
			"ajax": {
						"type": "GET",
						"url": apiUrl,
						"dataSrc": "aaData"			
					},
			"columns": [
				{
					"class": 'details-control',
					"orderable":      false,
					"data":  "",
					"defaultContent": '',
					"searchable": false
				},
				{ 
					"data": "efi",
					"width": "85px",
					"searchable": true
				},
				{ 	
					"data": "description",
					"width": "185px",
					"searchable": true
				},
				{ 
					"data": "detailCount",
					"render": function ( data, type, row ) {
						var totalCount = parseInt(row.totalCount),
							NonotuckCount = parseInt(data["NONOTUCK"]),
							remainingCount = (totalCount-NonotuckCount);
						return remainingCount;
					},
					"searchable": false
				},
				{ 
					"data": "detailCount",
					"render": function ( data, type, row ) {
						if (!data){data = 0;}
						return data["NONOTUCK"];
					},
					"searchable": false
				},
				{ 
					"data": "cost",
					"render": function ( data, type, row ) {
						if (!data){data = 0;}
						var perItemCost = data;
						var cost = Math.ceil(data*row.detailCount["NONOTUCK"]);
//						TotalNonotuck = TotalNonotuck+cost;
//						$("#TotalNonotuck").html('\$'+(TotalNonotuck + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
						return "<span title='\$"+perItemCost+" per item' class='totalNonotuck'>$"+(cost + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+"</span>";
					},
					"searchable": false
				},
				{ 
					"data": "totalCount",
					"render": function ( data, type, row ) {
						if (!data){data = 0;}
						return data;
					},
					"searchable": false
				},
				{ 
					"data": "cost",
					"render": function ( data, type, row ) {
						if (!data){data = 0;}
						var perItemCost = data;
						var cost = Math.ceil(data*row.totalCount);
//						TotalOnHand = TotalOnHand+cost;
//						$("#TotalOnHand").html('\$'+(TotalOnHand + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
						return "<span title='\$"+perItemCost+" per item' class='totalOnHand'>$"+(cost + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+"</span>";
					},
					"searchable": false
				},
				<?php if ($Config_OverrideOrderQty && $Config_OverrideOrderQty !='false'){?>
				{ 
					"data": "minimumQty",
					"render": function ( data, type, row ) {
						//console.log(data);
						return data;
					},
					"searchable": false
				},
				{ 
					"data": "efi",
					"render": function ( data, type, row ) {
						var reorderQty = <?php echo json_encode($reorderQty);?>;

						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = reorderQty[data].trigger;
							
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = reorderQty[data].reorder,
							reorderPercentage = 0,
							reorder = false;
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								reorderCount = "";
							} else{
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								//console.log(reorderThreshold+"="+reorderPercentage);
								reorderCount = reorderQty[data].reorder;
							}
						return reorderCount;
					},
					"searchable": false
				},	
				{
					"data": "efi",
					"render": function(data,type,row){
						var reorderQty = <?php echo json_encode($reorderQty);?>;
						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = reorderQty[data].trigger,
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = reorderQty[data].reorder,
							reorderPercentage = 0,
							reorder = false,
							reorderCases = "",
							unitsPerCase=0;
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								reorderCount = "";
							} else{
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								//console.log(reorderThreshold+"="+reorderPercentage);
								reorderCount = reorderQty[data].reorder;
							}
							if (reorderCount > 0){
								unitsPerCase = row.unitsPerCase;
								reorderCases = "<span title='"+unitsPerCase+" units per case'>"+Math.ceil(reorderCount/unitsPerCase)+"</span>";
							}
								
						return reorderCases;
					}
				},
				{ 
					"data": "efi",
					"render": function ( data, type, row ) {
						var reorderQty = <?php echo json_encode($reorderQty);?>;
						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = reorderQty[data].trigger,
							currentDaysOnHandQty = row.currentDaysOnHandQty,
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = parseInt(reorderQty[data].reorder-compareCount),
							reorderPercentage = 0,
							reorderStatusDisplay = "",
							currentDaysOnHandDisplay = "",
							reorder = false;
							
							currentDaysOnHandDisplay = Math.floor(compareCount/currentDaysOnHandQty);
							if (currentDaysOnHandDisplay != "Infinity"){
								currentDaysOnHandDisplay += " days on hand";
							}
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								if (reorderPercentage < 50){
									reorderStatusDisplay = "<span title='% of stock left before reaching reorder trigger'>" +reorderPercentage+ "% left</span>";
								}
								
								reorderCount = "";
							} else{
								reorderStatusDisplay = " Reorder";
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								reorderStatusDisplay = " <span title='Within 10% of stock left before reaching reorder trigger'>Possibly Reorder</span>";
							}
						return reorderStatusDisplay+"\r\n"+currentDaysOnHandDisplay;
					},
					"searchable": false
				}
				<?php }else{?>
				{ 
					"data": "minimumQty",
					"render": function ( data, type, row ) {
						//console.log(data);
						return Math.floor(data/3);
					},
					"searchable": false
				},
				{
					"data": "minimumQty",
					"render": function ( data, type, row ) {
						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = Math.floor(data/3),
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = parseInt(data-compareCount),
							reorderPercentage = 0,
							reorder = false;
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								reorderCount = "";
							} else{
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								//console.log(reorderThreshold+"="+reorderPercentage);
								reorderCount = Math.abs(parseInt(data-compareCount));
							}
						return reorderCount;
					},
					"searchable": false
				},
				{
					"data": "minimumQty",
					"render": function(data,type,row){
						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = Math.floor(data/3),
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = parseInt(data-compareCount),
							reorderPercentage = 0,
							reorder = false,
							reorderCases = "",
							unitsPerCase=0;
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								reorderCount = "";
							} else{
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								//console.log(reorderThreshold+"="+reorderPercentage);
								reorderCount = Math.abs(parseInt(data-compareCount));
							}
							if (reorderCount > 0){
								unitsPerCase = row.unitsPerCase;
								reorderCases = "<span title='"+unitsPerCase+" units per case'>"+Math.ceil(reorderCount/unitsPerCase)+"</span>";
							}
								
						return reorderCases;
					}
				},
				{ 
					"data": "minimumQty",
					"render": function ( data, type, row ) {
						var compareCount = parseInt(<?php echo $countCompareRow;?>),
							minimumThreshold = Math.floor(data/3),
							currentDaysOnHandQty = row.currentDaysOnHandQty,
							reorderThreshold = parseInt(minimumThreshold-compareCount),
							reorderCount = parseInt(data-compareCount),
							reorderPercentage = 0,
							reorderStatusDisplay = "",
							currentDaysOnHandDisplay = "",
							reorder = false;
							
							currentDaysOnHandDisplay = Math.floor(compareCount/currentDaysOnHandQty);
							if (currentDaysOnHandDisplay != "Infinity"){
								currentDaysOnHandDisplay += " days on hand";
							}
							if (reorderThreshold < 0){
								//determine the percentage from the min threshold
								reorderPercentageActual = (Math.abs(reorderThreshold)/minimumThreshold);
								reorderPercentage = Math.floor(reorderPercentageActual * 100);
								if (reorderPercentage < 50){
									reorderStatusDisplay = "<span title='% of stock left before reaching reorder trigger'>" +reorderPercentage+ "% left</span>";
								}
								
								reorderCount = "";
							} else{
								reorderStatusDisplay = " Reorder";
								reorder = true;
							}
							if (reorderPercentage < 10 && !reorder){
								reorderStatusDisplay = " <span title='Within 10% of stock left before reaching reorder trigger'>Possibly Reorder</span>";
							}
						return reorderStatusDisplay+"\r\n"+currentDaysOnHandDisplay;
					},
					"searchable": false
				}
				<?php }?>
			],
			"createdRow": function ( row, data, index ) {
				var minimumThreshold = Math.floor(data.minimumQty/3)
				var compareCount = parseInt(<?php echo $countCompareData;?>);
				//console.log(NonotuckData);
				$.each( data, function( key, value ) {
					//console.log(key +":"+ value);
				});
				if (!data.canBeOrderedId){
					$(row).addClass( 'cannotbereordered' );
				}
				if (compareCount < minimumThreshold){
					$(row).addClass( 'reorder' );
				}
				var perItemCost = data.cost;
				var totalOnHandCost = Math.ceil(perItemCost*data.totalCount);
				var totalNonotuckCost = Math.ceil(perItemCost*data.detailCount["NONOTUCK"]);
				TotalOnHand = TotalOnHand+totalOnHandCost;
				$("#TotalOnHand").html('\$'+(TotalOnHand + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
				TotalNonotuck = TotalNonotuck+totalNonotuckCost;
				$("#TotalNonotuck").html('\$'+(TotalNonotuck + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
			},
			"order": [[1, 'asc']]
		} );
		
		// Add event listener for opening and closing details
		$('#product-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data();
				if (!dataObj.detailCount){
					dataObj.detailCount = {'Item not currently in any warehouse':''};
				}
				if (dataObj.detailCount){
					row.child( format(row.data()) ).show();
					tr.addClass('shown');
				}
			}
		} );
		
	});
	
	$(document).ready(function() {
		$('#allCount').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
	} );	
</script>
<style>
#allCount th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>

<table id="product-list" class="hover">
    <thead>
    <tr>
		<th></th>
        <th>EFI</th>
        <th>Description</th>
        <th title="Total Units in all other Warehouses">Energy Specialists Qty</th>
        <th title="Units in Nonotuck">Nonotuck Qty</th>
        <th title="Cost of Units in Nonotuck" class="sum">Nonotuck Cost</th>
        <th title="Total Units in all Warehouse Locations">Total On Hand Qty</th>
        <th title="Cost of Units in all Warehouse Locations">On Hand Cost</th>
        <th title="<?php echo ($Config_RestockTriggerOverride && $Config_RestockTriggerOverride != 'false' ? 'Use Manual Minimum Qty' : '1/3 of last 3 week consumption');?>">Restock Trigger</th>
        <th title="To bring <?php echo $Config_ThresholdQtyCompare;?> qty up to 3 week stock">Reorder Qty</th>
        <th title="Number of Cases based off of Reorder Qty">Case Qty</th>
        <th title="Suggested Action based on Reorder Qty. Days on Hand based on the average used per day from the last <?php echo $Config_DaysForDaysOnHandCount;?> days">Status</th>
    </tr>
    </thead>
	<tfoot style="font-size:small;">
		<tr>
			<th colspan="5" style="text-align:right"></th>
			<th id="TotalNonotuck" title="Total in Nonotuck" style="text-align:left"></th>
			<th style="text-align:right"></th>
			<th id="TotalOnHand" title="Total in All Warehouse Locations" style="text-align:left"></th>
			<th colspan="4"></th>
		</tr>
	</tfoot>	
    <tbody></tbody>
</tfoot>
</table>
<br><br><hr>
<h3>All Location Count</h3>
<table id="allCount" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<?php
				foreach ($csvhdr as $hdr){
					echo "<th>".$hdr."</th>";
				}
			?>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<?php
				foreach ($csvhdr as $hdr){
					echo "<th>".$hdr."</th>";
				}
			?>
		</tr>
	</tfoot>
	<tbody>
		<?php
			foreach ($csvrow as $warehouse=>$row){
				echo "<tr>";
				foreach ($row as $rowid=>$rowinfo){
					echo "<td>".$rowinfo."</td>";
				}
				echo "</tr>";
			}
		?>
		<tr><td>OnHand Totals</td>
			<?php
				foreach ($csvrowTotal as $EFI=>$total){
					echo "<td>".$total."</td>";
				}
			?>
		</tr>
	</tbody>
</table>