<?php
$ThreeFullMonthsAgoEnd = date("m/t/Y",strtotime($TodaysDate." -2 month"));
$ThreeFullMonthsAgoStart = date("m/1/Y",strtotime($ThreeFullMonthsAgoEnd." -3 month"));
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $ThreeFullMonthsAgoStart);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $ThreeFullMonthsAgoEnd);
$includeCommissions = true;
include_once($siteRoot.$adminFolder.'forecast/_closureRatesCalculator.php');
?>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var	jsondataESNames = <?php echo json_encode($WarehouseByShortName);?>;

		function format ( d, DetailType ) {
			// `d` is the original data object for the row
			var thisItem = d,
				DetailType = DetailType,
				jsondataES = <?php echo json_encode($DetailTableRows);?>,
				jsondataContractor = <?php echo json_encode($DetailContractorTableRows);?>,
				detailDisplayRow = "";
				if (DetailType == "EnergySpecialist"){
					var ESName = thisItem.split("&nbsp;"),
						thisItem = ESName[1],
						jsondata = jsondataES;
					var TableHeader = '<tr style="font-size:8pt;"><td><a href="?nav=warehouse-management&WarehouseName='+jsondataESNames[thisItem]+'" target="_blank">'+jsondataESNames[thisItem]+'</a></td><td style="font-size:8pt;">Site Visits</td><td style="font-size:8pt;">No Work</td><td style="font-size:8pt;">Road Blocked</td><td style="font-size:8pt;">Pending Signature</td><td style="font-size:8pt;">Contracts Signed</td><td style="font-size:8pt;">Contracts Billed</td>'+ //<td style="font-size:8pt;">Signed Rate</td>
						'<td style="font-size:8pt;">Conv Rate</td><td style="font-size:8pt;">Days Issue</td><td style="font-size:8pt;">Days Sign</td><td style="font-size:8pt;">Days Bill</td><td style="font-size:8pt;">Org Contract Amt</td><td style="font-size:8pt;">Final Contract Amt</td><td style="font-size:8pt;">Changed Amt</td><td style="font-size:8pt;">Avg Contract Amt</td><td style="font-size:8pt;">NATGRID Contracts</td><td style="font-size:8pt;">WMECO Contracts</td><td style="font-size:8pt;">BGAS Contracts</td></tr>'; //<td style="font-size:8pt;">Billed Amt</td><td style="font-size:8pt;">Commission</td><td style="font-size:8pt;">BGAS Amount</td>

				}else{
					jsondata = jsondataContractor
					var TableHeader = '<tr style="font-size:8pt;"><td></td><td style="font-size:8pt;">Days Accept</td><td style="font-size:8pt;">Days Install</td><td style="font-size:8pt;">Contract Amt</td></tr>';
				}
			var thisItem = (thisItem == "Contractor Averages" ? "ZZZContractor Averages" : d.replace("&nbsp;","QQQ"));
			var	detailDisplayRow = jsondata[thisItem];
		
			return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:40px;">'+
					'<colgroup><col><col><col></colgroup>'+TableHeader+detailDisplayRow+
					'</table>';
		}
	
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		


		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
		
		// Add event listener for opening and closing details
		$('.reportResults tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data(),
					efi = (dataObj[1]),
					type = (dataObj[5]);
					row.child( format(efi,type) ).show();
					tr.addClass('shown');
			}
		} );
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>inventory/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=<?php echo $nav;?>#";
		}
		$("button").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		var OpenInNewTab = function(url) {
		  var win = window.open(url, '_blank');
		  win.focus();
		};
		$(".esName").on("click",function(){
			var $this = $(this),
				esName = $this.attr('data-esname'),
				url = '<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management&WarehouseName='+jsondataESNames[esName];
				OpenInNewTab(url);
		});

		
	} );	
</script>
<style>
#allCount th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
.esName {cursor: pointer;text-decoration:underline;}
</style>

<h3>Performance Information</h3>
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
<button>reset filters</button>
<?php include_once($siteRoot.$adminFolder."forecast/_esContractAverages.php");?>
<Br><Br>
<?php include_once($siteRoot.$adminFolder."forecast/_esISMAverages.php");?>
<Br><Br>
<?php include_once($siteRoot.$adminFolder."forecast/_esContractorAverages.php");?>
<Br><Br>
<hr>
<a name="Notes">Notes:</a>
<ul>
<?php 
$Note = array_unique($Notes);
ksort($Note);
foreach($Note as $note){
	echo "<li>".$note.".</li>";
}
?>
</ul>