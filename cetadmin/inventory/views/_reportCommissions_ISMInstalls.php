<h4><?php echo date("F Y",strtotime($MonthForReport));?> ISM Installs</h4>
<?php 
	if ($MonthForReport != ""){?>
		<div class="page-break">&nbsp;</div>
		<div class="row">
			<div class="fifteen columns">
				<table class="commissionReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th colspan="13" style="text-align:left;"><?php echo date("M-Y",strtotime($MonthForReport));?></th>
							<th colspan="11" style="text-align:left;">YTD</th>
						</tr>
						<tr>
							<th>Crew</th>
							<th>Energy Specialist</th>
							<th>HEAs & SHVs</th>
							<th>Aerators</th>
							<th>Avg</th>
							<th>Showerheads</th>
							<th>Avg</th>							
							<th>Powerstrips</th>
							<th>Avg</th>
							<th>Tstats</th>
							<th>Avg</th>
							<th>Bulbs</th>
							<th>Bulb Avg</th>
							<th>HEAs & SHVs</th>
							<th>Aerators</th>
							<th>Avg</th>
							<th>Showerheads</th>
							<th>Avg</th>
							<th>Powerstrips</th>
							<th>Avg</th>
							<th>Tstats</th>
							<th>Avg</th>
							<th>Bulbs</th>
							<th>BulbAvg</th>
						</tr>
					</thead>
					<tbody>
						<?php	
							$TotalBulbs = 0;
							foreach ($BulbData as $CrewID=>$MonthData){
								$CrewParts = explode("_",$CrewID);
								$CrewIDSort = $CrewID;
								if (strlen($CrewParts[1]) < 3){
									$CrewIDSort = str_replace("_","_0",$CrewID);
								}

								foreach ($MonthData as $MonthYear=>$PartInfo){
					//				if (strtotime($MonthYear."-01") >= strtotime($StartDate) && strtotime($MonthYear."-01") <= strtotime($EndDate) && $ESName[1]=="CEDAR B"){
									if (strtotime($MonthYear."-01") == strtotime($MonthForReport)){
										$BulbVisit = ($PartInfo["HEA"]+$PartInfo["SHV"]);
										$trStyle = "";
										if (!in_array($CrewID,$DisplayHideExceptions) && MySQLDate($WarehouseRecordByName[$CrewID]->endDate) && strtotime($WarehouseRecordByName[$CrewID]->endDate) < strtotime($StartDate)){
											$trStyle = " style='display:none;border:1pt solid red;'";
										}

									?>
										<tr<?php echo $trStyle;?>>
											<td><?php echo $CrewIDSort;?></td>
											<td><?php echo $WarehouseRecordByName[$CrewID]->description;?></td>
											<td><?php echo ($BulbVisit ? $BulbVisit : "0");$TotalBulbVisit = $TotalBulbVisit+$BulbVisit;?></td>
											<td><?php echo ($PartInfo["Aerator"] ? $PartInfo["Aerator"] : "0");$TotalAerator = $TotalAerator+$PartInfo["Aerator"];?></td>
											<td><?php echo round($PartInfo["Aerator"]/$BulbVisit,2);?></td>
											<td><?php echo ($PartInfo["Showerhead"] ? $PartInfo["Showerhead"] : "0");$TotalShowerhead = $TotalShowerhead+$PartInfo["Showerhead"];?></td>
											<td><?php echo round($PartInfo["Showerhead"]/$BulbVisit,2);?></td>
											<td><?php echo ($PartInfo["PowerStrip"] ? $PartInfo["PowerStrip"] : "0");$TotalPowerStrip = $TotalPowerStrip+$PartInfo["PowerStrip"];?></td>
											<td><?php echo round($PartInfo["PowerStrip"]/$BulbVisit,2);?></td>
											<td><?php echo ($PartInfo["Tstat"] ? $PartInfo["Tstat"] : "0");$TotalTstat = $TotalTstat+$PartInfo["Tstat"];?></td>
											<td><?php echo round($PartInfo["Tstat"]/$BulbVisit,2);?></td>
											<td><?php echo ($PartInfo["Bulbs"] ? $PartInfo["Bulbs"] : "0");$TotalBulbs = $TotalBulbs+$PartInfo["Bulbs"];?></td>
											<td><?php echo round($PartInfo["Bulbs"]/$BulbVisit);?></td>
											<?php $YTDBulbVisit = ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["HEA"]+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["SHV"]);?>
											<td><?php echo ($YTDBulbVisit ? $YTDBulbVisit : "0");$TotalBulbVisitYTD = $TotalBulbVisitYTD+$YTDBulbVisit;?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"] : "0");$TotalAeratorYTD = $TotalAeratorYTD+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"];?></td>
											<td><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Aerator"]/$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"] : "0");$TotalShowerheadYTD = $TotalShowerheadYTD+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"];?></td>
											<td><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Showerhead"]/$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"] : "0");$TotalPowerStripYTD = $TotalPowerStripYTD+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"];?></td>
											<td><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["PowerStrip"]/$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"] : "0");$TotalTstatYTD = $TotalTstatYTD+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"];?></td>
											<td><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Tstat"]/$YTDBulbVisit,2);?></td>
											<td><?php echo ($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"] ? $CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"] : "0");$TotalBulbCountYTD = $TotalBulbCountYTD+$CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"];?></td>
											<td><?php echo round($CalendarYTDBulbReportTotal[$CrewID][$MonthYear]["Bulbs"]/$YTDBulbVisit,2);?></td>
										</tr>
									<?php 
									}//end if in timeframe
								}//end foreach MonthData
							}//end foreach BulbData
						?>
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td>Totals</td>
							<td><?php echo ($TotalBulbVisit ? $TotalBulbVisit : "0");?></td>
							<td><?php echo ($TotalAerator ? $TotalAerator : "0");?></td>
							<td><?php echo round($TotalAerator/$TotalBulbVisit,2);?></td>
							<td><?php echo ($TotalShowerhead ? $TotalShowerhead : "0");?></td>
							<td><?php echo round($TotalShowerhead/$TotalBulbVisit,2);?></td>
							<td><?php echo ($TotalPowerStrip ? $TotalPowerStrip : "0");?></td>
							<td><?php echo round($TotalPowerStrip/$TotalBulbVisit,2);?></td>
							<td><?php echo ($TotalTstat ? $TotalTstat : "0");?></td>
							<td><?php echo round($TotalTstat/$TotalBulbVisit,2);?></td>
							<td><?php echo ($TotalBulbs ? $TotalBulbs : "0");?></td>
							<td><?php echo round($TotalBulbs/$TotalBulbVisit);?></td>
							<td><?php echo ($TotalBulbVisitYTD ? $TotalBulbVisitYTD : "0");?></td>
							<td><?php echo ($TotalAeratorYTD ? $TotalAeratorYTD : "0");?></td>
							<td><?php echo round($TotalAeratorYTD/$TotalBulbVisitYTD,2);?></td>
							<td><?php echo ($TotalShowerheadYTD ? $TotalShowerheadYTD : "0");?></td>
							<td><?php echo round($TotalShowerheadYTD/$TotalBulbVisitYTD,2);?></td>
							<td><?php echo ($TotalPowerStripYTD ? $TotalPowerStripYTD : "0");?></td>
							<td><?php echo round($TotalPowerStripYTD/$TotalBulbVisitYTD,2);?></td>
							<td><?php echo ($TotalTstatYTD ? $TotalTstatYTD : "0");?></td>
							<td><?php echo round($TotalTstatYTD/$TotalBulbVisitYTD,2);?></td>
							<td><?php echo ($TotalBulbCountYTD ? $TotalBulbCountYTD : "0");?></td>
							<td><?php echo round($TotalBulbCountYTD/$TotalBulbVisitYTD);?></td>
						</tr>					
					</tfoot>
				</table>
			</div>
		</div>
<?php
	}else{//end if $MonthForReport
	 echo "Choose a Month to Display";
	}
?>