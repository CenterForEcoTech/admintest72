<?php
if (count($PriorCommssionDataChanged)){
	echo "<h4>The Following Changes Were Detected:</h4>";
	echo '<div class="row"><div class="twelve columns">';
	echo "<table border=1 cellspacing=0 cellpadding=5>";
		foreach ($PriorCommssionDataChanged as $ES=>$MonthYearData){
			$ESName = explode("QQQ",$ES);
			echo "<tr><td valign=\"top\">".str_replace("XX","",$ESName[0])." ".$ESName[1]."</td>";
			echo "<td valign=\"top\">";
				foreach ($MonthYearData as $MonthYear=>$MonthYearInfo){
					echo date("M Y",strtotime($MonthYear."-01"))."<br>";
					echo "<ul>";
					foreach ($MonthYearInfo as $FieldName=>$Value){
						echo "<li>".$FieldName." locked at ".$Value["Was"]." but now calculated at ".$Value["Is"]."</li>";
					}
					echo "</ul>";
				}
			echo "</td>";
		}
	echo "</table>";
	echo '</div></div>';
	
}else{
?>
	<div class="row">
		<div class="twelve columns">
			Nothing detected as changed
		</div>
	</div>

<?php }?>