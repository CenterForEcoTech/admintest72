<h4>Raw Commission Data</h4>
<?php 
	$MonthForReportRawData = date("Y-m-01");
	if ($MonthForReportRawData != ""){
	?>
		<div class="page-break">&nbsp;</div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Crew</th>
							<th>Energy Specialist</th>
							<th>Site ID</th>
							<th>Name</th>
							<th>Utility</th>
							<th>HEA Date</th>
							<th>AS BilledDate</th>
							<th>AS Final Amount</th>
							<th>INS BilledDate</th>
							<th>INS Final Amount</th>
							<th>TSTAT BilledDate</th>
							<th>TSTAT Final Amount</th>
							<th>Combined Final Amount</th>
						</tr>
					</thead>
					<tbody>
						<?php	
							foreach ($ESDataContractReport as $ES=>$MonthData){
								$ESName = explode("QQQ",$ES);
								$CrewID = strtoupper(str_replace("XX","",$ESName[0]));
								$CrewParts = explode("_",$CrewID);
								$CrewIDSort = $CrewID;
								if (strlen($CrewParts[1]) < 3){
									$CrewIDSort = str_replace("_","_0",$CrewID);
								}

								foreach ($MonthData as $MonthYear=>$MonthYearInfo){
					//				if (strtotime($MonthYear."-01") >= strtotime($StartDate) && strtotime($MonthYear."-01") <= strtotime($EndDate) && $ESName[1]=="CEDAR B"){
									if (strtotime($MonthYear."-01") == strtotime($MonthForReportRawData)){
										$rowCounter = 0;
										//resort by SiteID
										ksort($MonthYearInfo);
										foreach ($MonthYearInfo as $SiteID=>$SiteData){
									?>
											<tr>
												<td><?php echo $CrewIDSort;?></td>
												<td><?php echo $ESName[1];?></td>
												<td><?php echo $SiteID;?></td>
												<td><?php echo $SiteIDs[$SiteID]["Name"];?></td>
												<td><?php echo $SiteIDs[$SiteID]["Utility"];?></td>
												<td><?php echo MySQLDate($SiteIDs[$SiteID]["HEADate"]);?></td>
												<td><?php echo $SiteData["AS"]["BilledDate"];?></td>
												<td>$<?php echo money($SiteData["AS"]["ContractAmountFinal"]);?></td>
												<td><?php echo $SiteData["INS"]["BilledDate"];?></td>
												<td>$<?php echo money($SiteData["INS"]["ContractAmountFinal"]);?></td>
												<td><?php echo $SiteData["TSTAT"]["BilledDate"];?></td>
												<td>$<?php echo money($SiteData["TSTAT"]["ContractAmountFinal"]);?></td>
												<?php $ContractCombined = 0;
													$ContractCombined = bcadd($SiteData["AS"]["ContractAmountFinal"],$SiteData["INS"]["ContractAmountFinal"],2);
													$ContractCombined = bcadd($ContractCombined,$SiteData["TSTAT"]["ContractAmountFinal"],2);
												?>
												<td>$<?php echo money($ContractCombined);?></td>
											</tr>
									<?php 
										}//end foreach MonthYearInfo 
									}//end if in timeframe
								}//end foreach MonthData
							}//end foreach ESDataContractReport
						?>
					</tbody>
				</table>
			</div>
		</div>
<?php
	}else{//end if $MonthForReport
	 echo "Choose a Month to Display first";
	}
?>