<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");

$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : "11/1/2014");
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $TodaysDate);
$TotalDays = DateDiff($StartDate,$EndDate);

$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByName[$record->name] = $record->description;
//	$WarehouseByDisplayID[$record->displayOrderId] = $record->name;
	$WarehouseByDisplayID[$record->name] = $record->displayOrderId;
}

$productProvider = new ProductProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	if ($record->displayOrderId){
		$ProductsDisplayOrder[$record->displayOrderId] = $record->efi;
		$ProductsWithDisplay[$record->displayOrderId]=$record;
		$DistinctEFIParts[] = $record->efi;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}
}
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$criteria = new stdClass();
$criteria->startDate = MySQLDateUpdate($StartDate);
$criteria->endDate = MySQLDateUpdate($EndDate);
$CSGData = $CSGDailyDataProvider->getDailyData($criteria); //INCOMPATIBLE WITH PHP 5.6
foreach ($CSGData as $ID=>$DataRecord){
	$auditorName = $DataRecord->auditor_name;
	$crew = strtoupper($DataRecord->crew);
	$efi = str_replace("\r","",trim($DataRecord->efiPart));
	//manual fix
	if ($efi == "3010.10"){$efi = "3010.100";}
	if ($efi == "3010.02"){$efi = "3010.020";}
	$qty = $DataRecord->qty;
	if ($efi){
		$crewOrder = ($WarehouseByDisplayID[$crew] ? $WarehouseByDisplayID[$crew] : 1001);
		$EFICount[$efi]["count"] = $EFICount[$efi]["count"]+$qty;
		$EFICount[$efi]["average"] = ceil($EFICount[$efi]["count"]/$TotalDays);
		$EFICount[$efi]["partId"] = $DataRecord->partId;
		$EFICount[$efi]["EFI"] = $efi;
		
		$CrewCount[$efi][$crew]["count"] = $CrewCount[$efi][$crew]["count"]+$qty;
		$CrewCount[$efi][$crew]["average"] = ceil($CrewCount[$efi][$crew]["count"]/$TotalDays);
		$CrewCount[$efi][$crew]["name"] = $WarehouseByName[$crew];
		$CrewCount[$efi][$crew]["crewOrder"] = $crewOrder;
		
		$CrewCountByCrew[$crew]["name"] = $WarehouseByName[$crew];
		$CrewCountByCrew[$crew]["crewOrder"] = $crewOrder;
		$CrewCountByCrew[$crew]["count"] = $CrewCountByCrew[$crew]["count"]+$qty;
		$CrewCountByCrew[$crew]["average"] = ceil($CrewCountByCrew[$crew]["count"]/$TotalDays);
		
		$CrewCountByCrew[$crew]["EFIs"][$efi]["count"] = $CrewCountByCrew[$crew]["EFIs"][$efi]["count"]+$qty;
		$CrewCountByCrew[$crew]["EFIs"][$efi]["average"] = ceil($CrewCountByCrew[$crew]["EFIs"][$efi]["count"]/$TotalDays);
		$CrewCountByCrew[$crew]["EFIs"][$efi]["name"] = $DataRecord->partId;
		$TotalCount = $TotalCount+$qty;
	}
}
//There are some EFI's that either are not displayed or not found any more.  Add them to a separate recordset so they can be added back at the end
foreach ($EFICount as $EFI=>$CountDetails){
	if (!in_array($EFI,$ProductsDisplayOrder)){
		$EFIWithNoDisplay[$EFI] = $EFICount[$EFI];
	}
}

$sortedEFICount = [];
//loop through the Product Display order so that the sorted EFICount array will match the displayID order for the EFI
foreach ($ProductsDisplayOrder as $id) {
    $sortedEFICount[$id] = $EFICount[$id];
}
$EFICount = $sortedEFICount;

//add in the EFI's with no displayID
foreach ($EFIWithNoDisplay as $EFI=>$CountDetails){
	$EFICount[$EFI] = $CountDetails;
}

//reorder crewcount
function sortByCrewOrder($a, $b) {
    return $a['crewOrder'] - $b['crewOrder'];
}
foreach ($CrewCount as $EFI=>$CrewDetails){
	uasort($CrewDetails, 'sortByCrewOrder');
	$CrewCountOrdered[$EFI] = $CrewDetails;
}
$CrewCount = $CrewCountOrdered;

//reorder the display of CrewCountByCrew
uasort($CrewCountByCrew, 'sortByCrewOrder');

//Reorder the details of the CrewCountByCrew EFI's to match the displayOrderID for the EFI'sSwfPath
//print_pre($CrewCountByCrew);
foreach ($CrewCountByCrew as $crewId=>$crew){
	$EFIWithNoDisplay = array();
	$sortedEFIs = array();
	foreach ($crew["EFIs"] as $EFI=>$efiDetails){
		if (!in_array($EFI,$ProductsDisplayOrder)){
			$EFIWithNoDisplay[$EFI] = $crew["EFIs"][$EFI];
		}
	}

	foreach ($ProductsDisplayOrder as $id) {
		if (array_key_exists($id,$crew["EFIs"])){
			$sortedEFIs[$id] = $crew["EFIs"][$id];
		}
	}
	$CrewCountByCrew[$crewId]["EFIs"] = $sortedEFIs;
	//add in the EFI's with no displayID
	if (count($EFIWithNoDisplay)){
		foreach ($EFIWithNoDisplay as $EFI=>$CountDetails){
			$CrewCountByCrew[$crewId]["EFIs"][$EFI] = $CountDetails;
		}	
	}
}
?>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		function format ( d, DetailType ) {
			// `d` is the original data object for the row
			var thisItem = d,
				DetailType = DetailType,
				jsondataEfi = <?php echo json_encode($CrewCount);?>,
				jsondataCrew = <?php echo json_encode($CrewCountByCrew);?>,
				detailDisplayRow = "";
				if (DetailType == "Auditor"){jsondata = jsondataEfi;}else{jsondata = jsondataCrew}
			var
				jsonlength = jsondata[thisItem].length,
				totalCount = 0,
				totalKeyCount = 0,
				totalCountDisplayRow = "";
			$.each(jsondata[thisItem], function( key, valueObj ) {
				if (totalKeyCount < 1){ 
					detailDisplayRow =  detailDisplayRow+'<tr><td>Details</td><td>'+DetailType+'</td><td>Count</td><td>Average</td></tr>';
				}
				totalKeyCount++;
				if (DetailType == "EFI"){
				totalCount = 0;
				/*
					if (key != 'name' && key !='count' && key !='average' && key !='crewOrder'){
						detailDisplayRow =  detailDisplayRow+'<tr><td></td><td>'+key+' '+valueObj["name"]+'</td>'+
							'<td align="left">'+valueObj["count"]+'</td>'+
							'<td>'+valueObj["average"]+'</td>'+
							'</tr>';
						totalCount = (totalCount+parseInt(valueObj["count"]));
					}
				*/
					if (key == 'EFIs'){
						$.each(valueObj, function( ObjKey, ObjValue ) {
							detailDisplayRow =  detailDisplayRow+'<tr><td></td><td>'+ObjKey+' '+ObjValue["name"]+'</td>'+
								'<td align="left">'+ObjValue["count"]+'</td>'+
								'<td>'+ObjValue["average"]+'</td>'+
								'</tr>';
							totalCount = (totalCount+parseInt(ObjValue["count"]));
						});
					}
				}else{
					detailDisplayRow =  detailDisplayRow+'<tr><td></td><td>'+key+' '+valueObj["name"]+'</td>'+
						'<td align="left">'+valueObj["count"]+'</td>'+
						'<td>'+valueObj["average"]+'</td>'+
						'</tr>';
					totalCount = (totalCount+parseInt(valueObj["count"]));
				}

			});
			
			totalCountDisplayRow = '<tr><td></td><td align="right">Total Count:</td><td>'+totalCount+'</td><td></td></tr>';
		
			
			return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
				'<colgroup><col><col><col></colgroup>'+
				detailDisplayRow+totalCountDisplayRow+
				'</table>';
		}
	
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		


		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
		
		// Add event listener for opening and closing details
		$('.reportResults tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data(),
					efi = (dataObj[1]),
					type = (dataObj[5]);
					row.child( format(efi,type) ).show();
					tr.addClass('shown');
			}
		} );
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>inventory/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=<?php echo $nav;?>#";
		}
		$("button").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});

		
	} );	
</script>
<style>
#allCount th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>

<h3>Inventory Usage By Type Per Period</h3>
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
<button>reset filters</button>
<?php 
echo "<bR>Total Days in this Period: ".$TotalDays."<br>";
echo "Total QTY Count=".$TotalCount."<br>";
?>

<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th></th>
			<th>EFI</th>
			<th>Description</th>
			<th>Qty / <?php echo $TotalDays;?> days</th>
			<th>Avg / day</th>
			<th style="display:none;"></th>
		</tr>
	</thead>

	<tbody>
		<?php
		//print_pre($EFICount);
//print_pre($CrewCount);
			foreach ($EFICount as $EFI=>$CountDetails){
				if (in_array($EFI,$DistinctEFIParts,true)){
					$ProductStatus = "";
				}else{
					if($ProductsWithOutDisplay[$EFI]){
						$ProductStatus = " not active";
					}else{
						$ProductStatus=" ".$CountDetails["partId"]." not found";
					}
				}
				echo "<tr>";
				echo "<td class=\"details-control\"></td>";
				echo "<td>".$EFI."</td>";
				echo "<td><a href=\"?nav=product-management&EFI=".$EFI."\">".$ProductsByEFI[$EFI]->description."</a>".$ProductStatus."</td>";
				echo "<td>".$CountDetails["count"]."</td>";
				echo "<td>".$CountDetails["average"]."</td>";
				echo "<td style=\"display:none;\">Auditor</td>";
				echo "</tr>";
			}
		?>
	</tbody>
</table>

<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th></th>
			<th>Auditor</th>
			<th>Name</th>
			<th>Qty / <?php echo $TotalDays;?> days</th>
			<th>Avg / day</th>
			<th style="display:none;"></th>
		</tr>
	</thead>

	<tbody>
		<?php
			foreach ($CrewCountByCrew as $CET=>$CountDetails){
				$CrewStatus = ($CountDetails["crewOrder"] == 1001 ? " not currently active" : "");
				echo "<tr>";
				echo "<td class=\"details-control\">";
				echo "<td>".$CET."</td>";
				echo "<td><a href=\"?nav=warehouse-management&WarehouseName==".$CET."\">".$CountDetails["name"]."</a> ".$CrewStatus."</td>";
				echo "<td>".$CountDetails["count"]."</td>";
				echo "<td>".$CountDetails["average"]."</td>";
				echo "<td style=\"display:none;\">EFI</td>";
				echo "</tr>";
			}
		?>
	</tbody>
</table>