<tr>
									<td><?php echo $WarehouseRecordByName[$CrewID]->description;?></td>
									<td><?php echo date("M-Y",strtotime($MonthForReport));?></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>Commissions</td><td>2%</td><td>Bulb</td><td>Total</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>$<?php echo money($PriorCommissionData[$ES][$MonthYear]["ContractsCommission"]);?></td>
									<td>$<?php echo money($PriorCommissionData[$ES][$MonthYear]["BulbCommission"]);?></td>
									<td>$<?php echo money($PriorCommissionData[$ES][$MonthYear]["TotalCommission"]);?></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								<tr>
									<td class="rowSpacer"> </td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><?php echo date("M-Y",strtotime($MonthForReport));?></td>
									<td></td>
									<td></td>
									<td>YTD</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>Closing Rates</td>
									<td>HEA</td>
									<td>Billed Jobs</td>
									<td>Rate</td>
									<td>HEA</td>
									<td>Billed Jobs</td>
									<td>Rate</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><?php echo $PriorCommissionData[$ES][$MonthYear]["HEACount"];?></td>
									<td><?php echo $PriorCommissionData[$ES][$MonthYear]["BilledContracts"];?></td>
									<td><?php echo $PriorCommissionData[$ES][$MonthYear]["ConvRate"];?>%</td>
									<td><?php echo $PriorCommissionData[$ES][$MonthYear]["YTDHEACount"];?></td>
									<td><?php echo $PriorCommissionData[$ES][$MonthYear]["YTDBilledContracts"];?></td>
									<td><?php echo floor(($PriorCommissionData[$ES][$MonthYear]["YTDBilledContracts"]/$PriorCommissionData[$ES][$MonthYear]["YTDHEACount"])*100);?>%</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td class="rowSpacer"> </td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>ISM</td>
									<td>HEA & SHV</td>
									<td>Aerators</td>
									<td>Avg</td>
									<td>Showerheads</td>
									<td>Avg</td>
									<td>Powerstrips</td>
									<td>Avg</td>
									<td>Tstat</td>
									<td>Avg</td>
									<td>Bulbs</td>
									<td>Bulb Avg</td>
								</tr>
								<tr>
									<td>Total for Month</td>
									<td><?php echo ($BulbVisit ? $BulbVisit : "0");?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["Aerator"] ? $BulbData[$CrewID][$MonthYear]["Aerator"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Aerator"],$BulbVisit,2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["Showerhead"] ? $BulbData[$CrewID][$MonthYear]["Showerhead"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Showerhead"],$BulbVisit,2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["PowerStrip"] ? $BulbData[$CrewID][$MonthYear]["PowerStrip"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["PowerStrip"],$BulbVisit,2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["Tstat"] ? $BulbData[$CrewID][$MonthYear]["Tstat"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["Tstat"],$BulbVisit,2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["Bulbs"] ? $BulbData[$CrewID][$MonthYear]["Bulbs"] : "0");?></td>
									<td><?php echo round($BulbData[$CrewID][$MonthYear]["Bulbs"]/$BulbVisit);?></td>
								</tr>
								<tr>
									<td>YTD</td>					
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDBulbVisit"] ? $BulbData[$CrewID][$MonthYear]["YTDBulbVisit"] : "0");?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDAerator"] ? $BulbData[$CrewID][$MonthYear]["YTDAerator"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["YTDAerator"],$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"],2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDShowerhead"] ? $BulbData[$CrewID][$MonthYear]["YTDShowerhead"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["YTDShowerhead"],$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"],2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDPowerStrip"] ? $BulbData[$CrewID][$MonthYear]["YTDPowerStrip"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["YTDPowerStrip"],$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"],2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDTstat"] ? $BulbData[$CrewID][$MonthYear]["YTDTstat"] : "0");?></td>
									<td><?php echo bcdiv($BulbData[$CrewID][$MonthYear]["YTDTstat"],$BulbData[$CrewID][$MonthYear]["YTDBulbVisit"],2);?></td>
									<td><?php echo ($BulbData[$CrewID][$MonthYear]["YTDBulbCount"] ? $BulbData[$CrewID][$MonthYear]["YTDBulbCount"] : "0");?></td>
									<td><?php echo $BulbData[$CrewID][$MonthYear]["YTDBulbPerVisit"];?></td>
								</tr>
								<tr>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
									<td style="border-bottom:1pt solid black;"></td>
								</tr>
								<tr>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
									<td class="rowSpacer"> </td>
								</tr>