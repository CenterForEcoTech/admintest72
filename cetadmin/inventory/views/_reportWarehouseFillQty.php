<?php
ini_set("display_errors", "on"); error_reporting(1);

include_once($siteRoot."_setupDataConnection.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
		$EmployeesByManager[$record->manager][]=$record;
		$EmployeesByFullName[$record->fullName]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
	if ($record->isManager){
		$Managers[] = $record->firstName." ".$record->lastName;
	}

}
//resort the products without display order by EmployeeID
ksort($EmployeesInActive);


include_once($dbProviderFolder."InventoryProvider.php");

$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByID[$record->id] = $record;
	$WarehouseByName[$record->name] = $record->id;
	if ($record->activeId){
		$WarehouseActiveDisplay[$record->displayOrderId]=$record;
	}else{
		$WarehouseNotActiveDisplay[$record->id]=$record;
	}

}


$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	$ProductDescription[$record->efi] = $record->description;
}

$criteria = new stdClass();
$autofillqtyResult = $warehouseProvider->getfillqty(); 
$autofillqty = $autofillqtyResult->collection;

//	print_pre($autofillqty);
foreach ($autofillqty as $autofilldata){
	$warehouse = $autofilldata->warehouseName;
	$efi = $autofilldata->efi;
	$qty = $autofilldata->qty;
	$AutoFillQty[$warehouse][$efi] = $qty;
}

?>
<style type="text/css" media="print">
	div.page
		{
		position:relative;
		page-break-after: always;
		page-break-inside: avoid;
		}
</style>
<script type="text/javascript">
	$(function () {
		var AdjustmentResultsTable = $(".AdjustmentResultsTable");
		var AdjustmentResults = AdjustmentResultsTable.DataTable({
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 20
		});

	});
</script>
<script src="../js/highstock/highstock.js"></script>
<script src="../js/highstock/modules/exporting.js"></script>
<div class="page">&nbsp;</div>
<?php 
	//print_pre($WarehouseActiveDisplay);
	ksort($WarehouseActiveDisplay);
	foreach ($WarehouseActiveDisplay as $id=>$record){
		if (!$record->excludeFromCountId && $record->name !='Nonotuck'){
			$SelectedWarehouse = $record->id;
			//echo $SelectedWarehouse."<br>";
			include('views/_warehouseManagementFillQty.php');
		}
	}
?>
