<?php
if (count($CommissionChangeReport)){
	echo "<br><Br><h4>Switching Commission Structure Changes:</h4>";
	echo '<div class="row"><div class="twelve columns">';
	echo '<table class="commissionReport display" cellspacing="0" width="100%">
			<thead style="background-color:#D6EAFF;">
				<tr><td>Energy Specialist</td><td>Month</td><td>Old Commission</td><td>New Commission</td><td>Change</td></tr>
			</thead>
			<tbody>';
		foreach ($CommissionChangeReport as $ES=>$MonthYearData){
			$ESName = explode("QQQ",$ES);
			echo "<tr><td valign=\"top\">".$ESName[1]."</td>";
			foreach ($MonthYearData as $MonthYear=>$MonthYearInfo){
					echo "<td valign=\"top\">".date("M Y",strtotime($MonthYear."-01"))."</td>";
					echo "<td>$".money($MonthYearInfo["WAS"])."</td>";
					echo "<td>$".money($MonthYearInfo["IS"])."</td>";
					echo "<td>$".money($MonthYearInfo["Diff"])."</td>";
					$totalWas = bcadd($totalWas,$MonthYearInfo["WAS"],2);
					$totalIs = bcadd($totalIs,$MonthYearInfo["IS"],2);
					$totalDiff = bcadd($totalDiff,$MonthYearInfo["Diff"],2);
			}
			echo "</tr>";
		}
		echo "<tfoot><tr><td colspan=\"2\">Total</td><td>$".money($totalWas)."</td><td>$".money($totalIs)."</td><td>$".money($totalDiff)."</td></tr></tfoot>";
	echo "</tbody></table>";
	echo "<br><br>Switching to the New Commission Structure will ".($totalDiff > 0 ? "save" : "cost")." CET $".money($totalDiff)."<br>";
	echo '</div></div>';
}else{
?>
	<div class="row">
		<div class="twelve columns">
			Nothing detected as changed
		</div>
	</div>
<?php }
?>