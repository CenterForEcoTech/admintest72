<?php
ini_set("display_errors", "on"); error_reporting(1);

$MovementType = explode("-",$nav);
$StartDate = date("m/d/Y", strtotime($TodaysDate." - 3 weeks"));
$EndDate = $TodaysDate;
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");

	$warehouseProvider = new WarehouseProvider($dataConn);
	$paginationResult = $warehouseProvider->get(); 
	$resultArray = $paginationResult->collection;
	//print_pre($resultArray);
	$excludeList = array();
	$includeBlankDefault = true;
	$classList = "historyFilter";
	$FromWarehouseDefault = "Nonotuck";
	$FromWarehouse = $warehouseProvider->createSelectPulldown($resultArray,"Warehouse",$FromWarehouseDefault,$excludeList,$includeBlankDefault,$classList);

?>
<script type="text/javascript">
function getOnHandForWarehouse(data,warehouse){
	var detailCount = data,
		warehouse = warehouse.toUpperCase(),
		data = 0;

	if (detailCount){
		data=detailCount[warehouse];
	}
	if(typeof data === 'undefined'){
		data = 0;
	}
	return "<span title='"+warehouse+"' class='warehouseCount'>"+data+"</span>";
}


function format ( d ) {
	// `d` is the original data object for the row
	var detailData = d.purchaseOrder,
		detailType = "PurchaseOrder:",
		detailDisplayRow = "";
		if (detailData.indexOf('ProjectID:') > 0){
			detailType = " &nbsp; ";
			detailData = detailData.replace(/,/g,"<br>");
		}
		detailDisplayRow =  detailDisplayRow+'<tr>'+
			'<td align="right">'+detailType+'</td>'+
			'<td>'+detailData+'</td>'+
			'</tr>';
	return '<table class="detailCountDisplay" border="0" cellpadding="5" cellspacing="0" width="100%" style="padding-left:50px;">'+
		'<colgroup><col><col><col></colgroup>'+
		detailDisplayRow+
		'</table>';
}


    $(function(){

		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 1);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				var diff = Math.floor((dt2.getTime() - dt1.getTime()) / 86400000); // ms per day
				var weeks = Math.round((diff/7) * 10) / 10
				$("#weeks").html(" "+weeks+" weeks");
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			maxDate:0,
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				var diff = Math.floor((dt2.getTime() - dt1.getTime()) / 86400000); // ms per day
				var weeks = Math.round((diff/7) * 10) / 10
				$("#weeks").html(" "+weeks+" weeks");
				updateFilters();
			}
		});		
		
	
        var productListTable = $("#product-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>inventory/ApiProductManagement.php",
            lastRequestedCriteria = null,
			
			CreateDataTable = function (movementType,warehouseName,startDate,endDate) {
				table = productListTable.DataTable({
					"bJQueryUI": true,
					"bAutoWidth": false,
					"sAjaxSource": apiUrl,
					"iDisplayLength": 50,
					"aaSorting": [ [0, "asc"]],
					"aoColumns" :[
						{
							"mData": "efi",
							"mRender": function ( data, type, full ) {
								return '<a href="?nav=product-management&EFI='+data+'">'+data+'</a>';
							},
							"sWidth": "20%"
						},
						{
							"mData": "description",
							"sWidth": "20%"
						},
						{
							"mData": "unitsPerPack",
							"sWidth": "20%",
							"mRender": function ( data, type, full ) {
								return '<span data-unitsperpack="'+data+'">'+data+'<span><span class="unitsIndicated" style="display:none;">0</span>';
						   }
						},
						{
							"mData": "units3weeks",
							"sWidth": "20%",
							"mRender": function ( data, type, full ) {
								return data;
						   }
						},
						{
							"mData": "units1month",
							"sWidth": "20%",
							"mRender": function ( data, type, full ) {
								return data;
						   }
						},
					],
					"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "action", "value": "transferleakage" } );
						aoData.push( { "name": "criteria[movementType]", "value": movementType } );
						aoData.push( { "name": "criteria[warehouseFrom]", "value": warehouseName } );
						aoData.push( { "name": "criteria[startDate]", "value": startDate } );
						aoData.push( { "name": "criteria[endDate]", "value": endDate } );
						
					},
					"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						lastRequestedCriteria = aoData;
						oSettings.jqXHR = $.ajax( {
							"dataType": 'json',
							"type": "GET",
							"url": sSource,
							"data": lastRequestedCriteria,
							"success": fnCallback
						} );
					},
					"createdRow": function ( row, data, index ) {
						//console.log(data.efi);
						var minimumThreshold = Math.floor(data.minimumQty/3)
//						if (data.detailCount){
						<?php 
							if ($Config_ThresholdQtyCompare == 'Nonotuck'){
								$countCompare = 'data.detailCount["NONOTUCK"]';
							}else{
								$countCompare = 'data.totalCount';							
							}
						?>
							var compareCount = parseInt(<?php echo $countCompare;?>);
//						}else{
//							var compareCount = 0;
//						}
						//console.log(NonotuckData);
						$.each( data, function( key, value ) {
							//console.log(key +":"+ value);
						});
						if (!data.canBeOrderedId){
							$(row).addClass( 'cannotbereordered' );
						}
						if (compareCount < minimumThreshold){
							$(row).addClass( 'reorder' );
						}
					}
				});
			};
	
		// Add event listener for opening and closing details
		$('#product-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).parents('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				var dataObj = row.data();
				if (!dataObj.purchaseOrder){
					dataObj.purchaseOrder = 'No Purchase Order';
				}
				if (dataObj.purchaseOrder){
					row.child( format(row.data()) ).show();
					tr.addClass('shown');
				}
			}
		} );
		var getFilters = function(){
			var movementType = $("#movementTypeSelect").val(),
				warehouseName = $("#Warehouse").val(),
				startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(movementType,warehouseName,startDate,endDate);
				
				return filterValues;
		};
		var updateFilters = function(){
			var Filters=getFilters();
			productListTable.dataTable().fnDestroy();
			CreateDataTable(Filters[0],Filters[1],Filters[2],Filters[3]);
		}
		
		$(".historyFilter").on('change', function () {
			updateFilters();
		});	
		$("button").on('click',function(){
			$("#movementTypeSelect").val('Transfer');
			$("#Warehouse").val('');
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		updateFilters();
//		CreateDataTable('Transfer','');		

    });
</script>
<h3>Product Transfer Leakage</h3>
Filters:<br>
<select id="movementTypeSelect" class="historyFilter"><option value="Transfer">Show Only Transfers</option><option value="Installed">Show Only Installed</option></select>
<?php echo $FromWarehouse;?>
&nbsp;
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo $StartDate;?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo $EndDate;?>" title="End Date">
<?php 
$periodUnits = 'day';
$periodDays = datediff($StartDate,$EndDate,$periodUnits);
$weekTest = ($periodDays/7)." weeks";
$periodAmount = $periodDays;
?>
<button>reset filters</button>
<table id="product-list" class="hover">
    <thead>
    <tr>
        <th>EFI</th>
        <th>Description</th>
        <th>Units/Pack</th>
        <th>Transfers for <div id="weeks" style="position:relative;float:right;"><?php echo $weekTest;?></div></th>
        <th>Transfers 30 Days</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>