<div class="page">
		<br clear="all">
		<hr>
		<h3>Auto Fill Qty <a href="?nav=product-setfillqty&WarehouseName=<?php echo $WarehouseByID[$SelectedWarehouse]->name;?>" id="set-fillqty" class="button-link">Set Fill Qty For <?php echo $WarehouseByID[$SelectedWarehouse]->name;?> <?php echo $WarehouseByID[$SelectedWarehouse]->description;?></a></h3>
		<table class="AdjustmentResultsTable">
			<thead>
				<tr><th colspan="3"><span class="warehouseNameSpan"></span>  Updated: <?php echo $TodaysDate;?></th></tr>
				<tr><th>ITEM#</th><th>DESCRIPTION</th><th>FILL QTY</th></tr>
			</thead>
			<tbody>
				<?php
					foreach ($AutoFillQty[$WarehouseByID[$SelectedWarehouse]->name] as $EFI=>$qty){
						echo "<tr><td>".$EFI."</td><td>".$ProductDescription[$EFI]."</td><td>".$qty."</td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>