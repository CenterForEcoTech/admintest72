<?php
/* @var $template EventTemplate */

$editPageHeader = "Create Event Template";
if ($template->name){
    $editPageHeader = "Edit Event Template";
}
?>
<style type="text/css">
    ul.tagit{margin:0;}
    .ui-state-preferred { border: 1px solid #2694e8; background: #3baae3 url(../css/images/ui-bg_glass_50_3baae3_1x400.png) 50% 50% repeat-x; font-weight: bold; color: #ffffff; }
    textarea:disabled {background-color: lightgray;}
    .wmd-preview ul{list-style-type: disc;}
    .wmd-preview ol{list-style-type: decimal;}
    fieldset.merge-tags ul li span{font-weight:bold;}
    fieldset.merge-tags ul li {margin:5px 0;}
    #admin fieldset.merge-tags{background: #E5F9FF;}

    #re-index-spinner{position:absolute;background:transparent url(../images/loadingSearch.gif) no-repeat 0 0;width:30px;height:30px;}
</style>

<form id="edit-event-template" class="basic-form sixteen columns override_skel" xmlns="http://www.w3.org/1999/html">
    <input type="hidden" name="id" value="<?php echo $template->id;?>">
    <h3><?php echo $editPageHeader;?></h3>
    <?php if ($template->id) {?>
        <a href="<?php echo $CurrentServer."template/".$template->url;?>" class="button-link" target="_blank" style="float:right">View Published Template</a>
    <?php } ?>
    <fieldset>
        <legend>Template Information</legend>
    <div class="fifteen columns">
        <div class="four columns">
            <label for="EventTemplate_Name">Template Name:</label>
                <input type="text" id="EventTemplate_Name" data-title="Template Name" name="name" value="<?php echo $template->name;?>" class="four columns" >
                <input type="hidden" id="EventTemplate_Name_Tester" value="<?php echo $template->name;?>">
            <div class="alert" id="EventTemplate_NameAlert"></div>
        </div>

        <div class="four columns ">
            <label for="EventTemplate_URL">Template URL:</label>
                <input type="text" id="EventTemplate_URL" name="url" value="<?php echo $template->url;?>" class="four columns" >
                <input type="hidden" id="EventTemplate_URL_Tester" value="<?php echo $template->url;?>">
            <div class="alert" id="EventTemplate_URLAlert"></div>
        </div>

        <div class="five columns ">
            <label for="tags">Tags:</label>
            <input id="tags" name="tagNamesArray" value="<?php echo implode(',', $template->tagNamesArray);?>">
        </div>
    </div>

    <div class="fifteen columns">
        <div class="fourteen columns">
            <label>
                <div>Template Heading (public)</div>
                <input type="text" id="EventTemplate_PageHeader" name="pageHeader" class="eleven columns" value="<?php echo $template->pageHeader;?>">
                <div class="alert eleven columns"></div>
            </label>
        </div>
    </div>

    <div class="fifteen columns">
        <div class="fourteen columns">
            <label>
                <div>Template Subheading (public)</div>
                <input type="text" name="pageSubtitle" class="eleven columns" value="<?php echo $template->pageSubtitle;?>">
            </label>
        </div>
    </div>

<?php if (strlen($template->instructionsMarkdownSource) == 0 && strlen($template->getOldInstructions()) > 0) {?>
    <div class="fifteen columns">
        <div class="fourteen columns">
            <label>
                <div>Old Template Instructions <a href="template-instructions-help" data-target="template-instructions" class="help-modal do-not-navigate">Help on writing template instructions</a></div>
                <textarea id="template-instructions" name="instructions" class="eleven columns" style="height:200px;" disabled><?php echo $template->instructions;?></textarea>
            </label>
        </div>
        <div style="display:none;">
            <div id="template-instructions-help" title="Template Instructions">
                <h4>First, persuade user to click &ldquo;Count Me In&rdquo;</h4>
                <p>
                    <em>Whatever you enter first must persuade the user to click the &ldquo;Count Me In&rdquo; button</em> in order to proceed.

                    When the user lands on the Template page for the first time, they will see the banner and text associated with the template&rsquo;s tags, then the
                    instructions you type here.
                </p>
                <h4>Next, embellish with additional details that might assist them</h4>
                <p>
                    If your users need help with the template form, you can add those additional details, but remember, <em>the more you add, the further away the
                    &ldquo;Count Me In&rdquo; button gets pushed down</em> the screen. Don&rsquo;t bore their pants off!
                </p>
            </div>
        </div>
    </div>
<?php } ?>
    </fieldset>

    <fieldset class="sixteen columns alpha omega">
        <legend>Template Instructions</legend>
        <div class="markdown-editor fifteen columns alpha" >

            <label for="instructions-markdown-source">Editor</label>
            <div id="instructions-button-bar" class="wmd-button-bar"></div>
            <textarea id="instructions-markdown-source" name="instructionsMarkdownSource" class="wmd-input fifteen columns alpha omega" cols="92" rows="15"><?php echo $template->instructionsMarkdownSource;?></textarea>

            <label for="instructions-html-source">Preview</label>
            <div id="instructions-preview" class="wmd-preview fifteen columns alpha omega"></div>
            <textarea id="instructions-html-source" name="instructionsHtmlSource" style="display:none;"></textarea>
        </div>
    </fieldset>
    <fieldset>
        <legend>Event Information</legend>
    <div class="fifteen columns">
        <div class="four columns">
            <label for="EventTemplate_EventName">Event Name:</label>
                <input type="text" id="EventTemplate_EventName" name="eventName" value="<?php echo $template->eventName;?>" class="four columns" >
            <div class="alert"></div>
        </div>

        <div class="paired fields five columns">
            <label for="EventTemplate_EventStartDate">Event Start Date/Time:</label>
                <input type="text" class="auto-watermarked three columns" title="date" name="startDate" id="EventTemplate_EventStartDate" value="<?php echo $template->startDate ? date("m/d/Y", strtotime($template->startDate)) : "";?>">
                <select name="startTime" id="EventTemplate_EventStartTime" class="two columns">
                    <option value=""></option>
                    <?php
                    $selectedTime = strtotime($template->startTime);
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            <div id="EventTemplate_EventStartDate_Error" class="alert"></div>
        </div>
        <div class="paired fields five columns">
            <label for="EventTemplate_EventEndDate">Event End Date/Time:</label>
                <input type="text" class="auto-watermarked three columns" title="date" name="endDate" id="EventTemplate_EventEndDate" value="<?php echo ($template->endDate) ? date("m/d/Y", strtotime($template->endDate)) : "";?>">
                <select name="endTime" id="EventTemplate_EventEndTime" class="two columns">
                    <option value=""></option>
                    <?php
                    $selectedTime = strtotime($template->endTime);
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            <div id="EventTemplate_EventEndDate_Error" class="alert"></div>
        </div>
    </div>

    <div class="fifteen columns">
        <label class="eleven columns alpha omega">
            <div>Event Description (Public):</div>
                <textarea name="description" class="eleven columns alpha omega" style="height:150px;"><?php echo $template->description;?></textarea>
            <div class="alert eleven columns"></div>
        </label>

        <fieldset class="merge-tags eleven columns alpha">
            <legend>Merge Tags Help</legend>
            <ul style="margin:0;padding:0;font-size:13px;">
                <li><span>*|BUSINESS_NAME|*</span> = venue.name</li>
                <li><span>*|CHARITY_AMOUNT|*</span> = 15% of purchases, or $1 for each item</li>
            </ul>
        </fieldset>
    </div>

    <div class="fifteen columns">
        <div class="inline-field slider-container thirteen columns">
            <label>
                <div>Default percentage of event sales to donate: </div>
                    <select id="customer-choice-select" name="percentageID" class="three columns slider-value-input">
                        <?php
                        $selectOptions = "";
                        for ($i = 1; $i <= 100; $i++){
                            $selected = ($template->percentageID == $i) ? " selected" : "";
                            $selectOptions .= "<option value='".$i."'".$selected.">".$i."%</option>\n";
                        }
                        ?>
                        <?php echo $selectOptions;?>
                    </select>
                <div class="alert"></div>
                <div id="customer-choice-slider" class="percentage-slider fat-slider nine columns no-mobile"></div>
            </label>
        </div>
    </div>
        <script>
            $(function() {
                $( "#customer-choice-slider" ).slider({
                    range: "min",
                    value: <?php if($template->percentageID){echo $template->percentageID;}else{echo "20";}?>,
                    min: 1,
                    max: 100,
                    slide: function( event, ui ) {
                        $( "#customer-choice-select" ).val(ui.value);
                    }
                });
                $( "#customer-choice-select" ).val($( "#customer-choice-slider" ).slider( "value" ));

                $("#customer-choice-select").focus(function(){
                    $("#customer-choice-slider").addClass("ui-state-hover");
                });

                $("#customer-choice-select").change(function(){
                    $("#customer-choice-slider").slider("value",  $(this).val());
                }).change();
            });
        </script>
    </fieldset>
    <div id="templateResults" class="fifteen columns"></div>
    <div class="fifteen columns">
        <a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-event-templates" class="button-link">Cancel</a>
        <a href="save-event-template" id="save-event-template" class="button-link do-not-navigate">Save</a>
        <a href="clone-event-template" id="clone-event-template" class="button-link do-not-navigate ui-state-highlight" title="Copy this template">Clone</a>
        <a href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=add-event&template=<?php echo $template->url;?>" target="_blank" class="button-link ui-state-preferred" title="Create event using this template" style="float:right;">Create Event</a>
    </div>
</form>
<div class="hidden" id="clone-template-form" title="Clone Event Template">
    <p>Please select the options to include with this process.</p>
    <label>
        <input type="checkbox" name="clonePremiums" >
        <span>Include Premiums</span>
    </label>
    <input type="hidden" name="template_id" value="<?php echo $template->id;?>">
</div>

<style type="text/css">
    #calendar-icon-fieldset{
        padding:20px;
    }
    #calendar-icon{
        float:left;
        margin-right:20px;
        display:block;
        margin-bottom:10px;
        color:#fafff5;
        background: transparent url(<?php echo $CurrentServer;?>images/calendar_box_sprite.png) no-repeat left top;
        height:81px;
        width:92px;
        position:relative;
        font:45px 'Yanone Kaffeesatz', sans-serif;
    }
    #calendar-icon span{
        position:absolute;
        top:18px;
        left:21px;
    }
    #calendar-icon-fieldset .event_text{
        float:left;
        min-width:480px;
        min-height:75px;
        background-color:#ddd;
        padding:30px 50px
    }
    #calendar-icon-fieldset .event_text:after{
        content:"Event Brief Area";
        font-size:40px;
    }
</style>
<?php if ($template->id) {
    if ($template->calendarIconFile) {
        $imageUrl = $template->getCalendarIconUrl($CurrentServer, $siteRoot);
        ?>
        <style type="text/css">
            #calendar-icon{
                background: transparent url(<?php echo $imageUrl;?>) no-repeat left center;
                height:100px;
                background-size:contain;
            }
            #calendar-icon span{
                visibility: hidden;
            }
        </style>
    <?php
    }
    ?>
    <fieldset id="calendar-icon-fieldset" class="fifteen columns">
        <legend>Event Brief Calendar Icon</legend>
        <div id="calendar-icon">
            <span>00</span>
        </div>
        <div class="event_text">

        </div>
        <div style="clear:both;padding-top:10px;min-height:20px;">
            <a id="re-index-events" class="button-link">Re-index Related Events</a>
            <span id="re-index-spinner" class="loading-spinner hidden"></span>
        </div>
    </fieldset>
<?php } ?>
<div class="hidden" id="pick-calendar-icon" title="Add/Delete Calendar Icon">
    <p>Use the autocomplete box to search for the uploaded image from the library <em>(save without selecting to delete the current icon)</em>.</p>
    <input type="text" id="image-search-box" title="search for image" class="auto-watermarked">
    <input type="hidden" id="image_id">
    <input type="hidden" id="template_id" value="<?php echo $template->id;?>">
    <p>
        <a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=image-library" class="button-link" target="_blank">Go to Image Library</a>
    </p>
</div>
<script>
$(function() {
    var formElement = $("#edit-event-template"),
        idField = formElement.find("input[name='id']"),
        nameField = formElement.find("input[name='name']"),
        urlField = formElement.find("input[name='url']"),
        titleEl = $("#EventTemplate_Name"),
        titleElTester = $("#EventTemplate_Name_Tester"),
        alertTitle = $("#EventTemplate_NameAlert"),
        templateURL = $("#EventTemplate_URL"),
        templateURLTester = $("#EventTemplate_URL_Tester"),
        alertURL = $("#EventTemplate_URLAlert"),
        startDateEl = $("#EventTemplate_EventStartDate"),
        endDateEl = $("#EventTemplate_EventEndDate"),
        startTimeEl = $("#EventTemplate_EventStartTime"),
        endTimeEl = $("#EventTemplate_EventEndTime"),
        areDatesValid = function(validatingStart){
            var startDateVal = startDateEl.val(),
                startTimeVal = startTimeEl.val(),
                endDateVal = endDateEl.val(),
                endTimeVal = endTimeEl.val(),
                isValid = true;
            if (startDateVal && endDateVal){

                var	startDate = new Date(startDateVal + " " + startTimeVal),
                    endDate = new Date(endDateVal + " " + endTimeVal),
                    startalertElement = $("#EventTemplate_EventStartDate_Error"),
                    endAlertElement = $("#EventTemplate_EventEndDate_Error");

                startalertElement.html("");
                endAlertElement.html("");
                if (endDate < startDate && (startTimeVal && endTimeVal)){
                    if (validatingStart){
                        startalertElement.html("Must be earlier than End Time");
                    } else {
                        endAlertElement.html("Must be later than Start Time");
                    }
                    isValid = false;
                }
            }
            return isValid;
        },
        pickIconForm = $("#pick-calendar-icon"),
        autoCompleteField = pickIconForm.find("#image-search-box"),
        imageIdField = pickIconForm.find("#image_id");


    $("#tags").tagit({
        availableTags:<?php echo json_encode($tagNames);?>
    });

    $("#save-event-template").click(function(e){

        $('#templateResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (areDatesValid() && formElement.valid()){
            //run ajax process
            $.ajax({
                url: "ApiEventTemplates.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    var newlyAdded = idField.val() != data.id;
                    if (newlyAdded){
                        window.location.href = '<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-event-template&id=' + data.id;
                    } else {
                        window.location.href = window.location.href;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var data = $.parseJSON(jqXHR.responseText);
                    $('#templateResults').html(data.message).addClass("alert");
                }
            });
        }
    });

    titleEl.focus(function(){
        alertTitle.html('');
    });

    titleEl.blur(function(e){
        if (titleEl.val() && titleEl.val().toLowerCase()!=titleElTester.val().toLowerCase()){
            //run ajax process
            $.ajax({
                url: "eventTemplatesErrorCheck.php",
                type: "POST",
                data: {
                    term : titleEl.val(),
                    FieldName : 'EventTemplate_Name'
                },
                success: function(data){
                    if (data > 0){
                        alertTitle.html('\''+titleEl.val()+'\' already exists');
                        titleEl.val('');
                    }
                }
            });

        }
    });
    templateURL.focus(function(){
        alertURL.html('');
    });
    templateURL.blur(function(e){
        var UrlVal = templateURL.val();
        templateURL.val(UrlVal.replace(/[^a-zA-Z0-9\-\s]/gi, '').replace(/[\s]/g, '-'));
        if (templateURL.val() && templateURL.val().toLowerCase()!=templateURLTester.val().toLowerCase()){
            //run ajax process
            $.ajax({
                url: "eventTemplatesErrorCheck.php",
                type: "POST",
                data: {
                    term : templateURL.val(),
                    FieldName : 'EventTemplate_URL'
                },
                success: function(data){
                    if (data > 0){
                        alertURL.html('\''+templateURL.val()+'\' already exists');
                        templateURL.val('');
                    }
                }
            });

        }
    });


    startDateEl.datepicker({
        minDate: 0,
        onSelect: function(dateText, inst) {
            if (endDateEl.val()){
                if (!areDatesValid(true)) {
                    endDateEl.val(dateText);
                }
            } else {
                endDateEl.val(dateText);
            }
        }
    });
    endDateEl.datepicker({
        minDate: 0,
        onSelect: function(dateText, inst) {
            areDatesValid();
        }
    });
    startTimeEl.change(function(){
        areDatesValid(true);
    });
    endTimeEl.change(function(){
        areDatesValid();
    });
    startDateEl.blur(function(){
        areDatesValid();
    });
    endDateEl.blur(function(){
        areDatesValid();
    });

    formElement.validate({
        rules: {
            name:{
                required: true
            },
            url:{
                required: true
            },
            pageHeader:{
                required: true
            },
            eventName: {
                required: true,
                maxlength: 75
            },
            description: {
                required: true
            },
            instructions: {
                required: true
            }
        },
        messages: {
            name:{
                required: "Template Name is used for internal purposes."
            },
            url:{
                required: "URL is used for linking directly to the template."
            },
            pageHeader:{
                required: "Please enter a page header"
            },
            eventName: {
                required: "Please give a title to your event.",
                maxlength: "Title must be 75 characters or less."
            },
            description: {
                required: "Please give a Description."
            },
            instructions: {
                required: "Please provide instructions."
            }
        }
    });
    titleEl.blur(function(){
        formElement.validate().element(titleEl);
    });

    titleEl.focus();
    $("#nav-menu").find("#manage-event-templates").addClass("ui-state-highlight");

    $("#instructions-markdown-source").wmd({
        "helpLink": "http://daringfireball.net/projects/markdown/",
        "helpHoverTitle": "Markdown Help",
        "button_bar": "instructions-button-bar",
        "preview": "instructions-preview",
        "output": "instructions-html-source"
    });

    var cloneForm = $("#clone-template-form"),
        templateIdField = cloneForm.find("input[name='template_id']"),
        includePremiumsCheckbox = cloneForm.find("input[name='clonePremiums']");

    formElement.on("click", "#clone-event-template", function(e){
        e.preventDefault();
        cloneForm.dialog("open");
    });

    cloneForm.dialog({
        autoOpen: false,
        height: 300,
        width:450,
        modal:true,
        buttons: {
            "Save": function(){
                $.ajax({
                    url: "ApiEventTemplates.php",
                    type: "POST",
                    data: JSON.stringify({
                        "action":"clone-template",
                        "template_id":templateIdField.val(),
                        "clonePremiums":includePremiumsCheckbox.is(":checked") ? 1 : 0
                    }),
                    success: function(data){
                        if (data.id){
                            window.location.href = "<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-event-template&id=" + data.id;
                        } else {
                            alert("unexpected result from clone operation");
                        }
                        cloneForm.dialog("close");
                    }
                });
            },
            "Cancel": function(){
                cloneForm.dialog("close");
            }
        },
        close: function(){
            includePremiumsCheckbox.prop("checked", false);
        }
    });

    autoCompleteField.autocomplete({
        delay:100,
        minLength: 0,
        source: function (request, response) {
            $.ajax({
                url: "<?php echo $CurrentServer;?>mayor/cms/ApiImageLibrary.php",
                data: {
                    action: "keyword_search",
                    keyword: request.term
                },
                success: function(data) {
                    response($.map(data.collection, function(el, index) {
                        return {
                            value: el.file_name,
                            id: el.id,
                            url: el.imageFullPath,
                            label: el.title
                        };
                    }));
                }
            });
        },
        select: function(e, ui){
            imageIdField.val(ui.item.id);
        }
    }).data("autocomplete")._renderItem = function(ul, item){
        return $("<li />")
            .data("item.autocomplete", item)
            .append("<a><img src='"+ item.url +"' class='small-logo-image'/> "+ item.label +"</a>")
            .appendTo(ul);
    };

    pickIconForm.dialog({
        autoOpen: false,
        height: 350,
        width:450,
        modal:true,
        buttons: {
            "Save": function(){
                var imageId = imageIdField.val();

                $.ajax({
                    url: "ApiEventTemplates.php",
                    type: "POST",
                    data: {
                        "id":'<?php echo $template->id;?>',
                        "image_id":imageId
                    },
                    success: function(data){
                        var imgTagTemplate = "<img src='{srcUrl}' height='61px'>";
                        if (data.record){
                            $("#calendar-icon").html(imgTagTemplate.replace(/{srcUrl}/g, data.record.logoFullPath));
                        }
                        pickIconForm.dialog("close");
                        window.location.reload();
                    }
                });
            },
            "Cancel": function(){
                pickIconForm.dialog("close");
            }
        },
        close: function(){
            autoCompleteField.val("");
            imageIdField.val("");
        }
    });

    $(document).on("click", "#calendar-icon-fieldset", function(e){
        pickIconForm.dialog("open");
    });
    $(document).on("click", "#re-index-events", function(e){
        var button = $(this),
            spinner = $("#re-index-spinner");
        e.preventDefault();
        e.stopPropagation();
        if (confirm("Are you sure you want to re-index related events? This may take a while!")){
            button.hide();
            spinner.show();
            $.ajax({
                url: "ApiEventTemplates.php",
                type: "POST",
                data: JSON.stringify({
                    "id":'<?php echo $template->id;?>',
                    "action":"re-index-events"
                }),
                success: function(data){
                    spinner.hide();
                    button.show();
                    alert("re-indexing completed!");
                }
            });
        }
    });
});
</script>
<?php
include_once("_eventTemplatePremiums.php");
?>