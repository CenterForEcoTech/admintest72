<?php
/* @var $record MerchantCatalogRecord */
/* @var $catalogMerchant MerchantCatalogMerchantRecord */
?>
<style type="text/css">
    .btn.hidden{display:none;}
    form label {font-weight:bold; font-size:16px;}
</style>
<h2>Edit Catalog</h2>
<form id="edit-catalog" class="sixteen columns normal">
    <div class="sixteen columns alpha omega">
        <fieldset>
            <legend>Basics</legend>
            <label>
                <div>Name:</div>
                <input type="text" name="name" title="Name: used as the URL" class="auto-watermarked" value="<?php echo $record->name;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>Page Title:</div>
                <input type="text" name="title" title="Page Title" class="auto-watermarked" value="<?php echo $record->title;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>Page Subtitle</div>
                <input type="text" name="subtitle" title="Page Subtitle" class="auto-watermarked" value="<?php echo $record->subtitle;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>
                    Description:
                    <a class="help-modal do-not-navigate" href="help-description">Help on Description</a>
                </div>
                <textarea name="description" title="Description: information about this catalog" class="auto-watermarked wide"><?php echo $record->description;?></textarea>
                <div class="alert"></div>
            </label>
            <label>
                <div>Status:</div>
                <select name="status">
                    <option value="ACTIVE" <?php echo $record->status === 'ACTIVE' ? "selected" : "";?>>ACTIVE</option>
                    <option value="DELETED"<?php echo $record->status === 'DELETED' ? "selected" : "";?>>DELETED</option>
                </select>
            </label>
        </fieldset>
    </div>
    <div class="sixteen columns">
        <input type="hidden" name="id" value="<?php echo $record->id;?>">
        <input type="hidden" name="action" value="save_catalog">
        <input type="submit" name="submit" value="Save" class="button-link">
    </div>
</form>
<div style="display:none;">
    <div id="help-description" title="Catalog Description">
        <p>Type an optional description of this catalog.</p>
        <p>At this time, the catalog does not have any additional text appearing with it as an introduction. So, if desired, this field may be treated as an "internal notes" field.</p>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var formElement = $("#edit-catalog"),
            apiUrl = '<?php echo $CurrentServer.$adminFolder;?>cms/ApiImageCatalog.php';

        formElement.on("submit", function(e){
            var formData = formElement.serializeObject();
            e.preventDefault();
            $.ajax({
                url: apiUrl,
                type: "POST",
                data: JSON.stringify(formData),
                success: function(data){
                    if (formData.id){
                        window.location.reload();
                    } else {
                        window.location.href = window.location.href + "&id=" + data.record.id;
                    }
                    //window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var data = $.parseJSON(jqXHR.responseText);
                    if (data.message){
                        alert(data.message);
                    }
                }
            });
        });
    });
</script>
<?php
if ($record->id){
    include("_catalogMerchantsList.php");
}
?>