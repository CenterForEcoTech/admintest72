<?php
include_once($repositoryApiFolder."DataContracts.php");
$supportedTypes = SocialMessageSampleFactory::getSupportedTypes();
$availableStatuses = SocialMessageSample::getAvailableStatuses();
if (!isset($relevantEntity)){
    $relevantEntity = SocialMessageSample::EVENT_ENTITY;
}
if (!isset($relevantEntityId)){
    $relevantEntityId = 0;
}
if ($relevantEntityId){
    $pageTitle = "Sample tweets and blurbs for ".$relevantEntity." with id=".$relevantEntityId;
    $formTitle = "Add or Edit social messaging for the ".$relevantEntity." with id=".$relevantEntityId.".";
} else {
    $pageTitle = "Sample tweets and blurbs for any ".$relevantEntity;
    $formTitle = "Add or Edit social messaging for any ".$relevantEntity.".";
}
?>
<h3><?php echo $pageTitle;?></h3>
<p>
    <a href="#addSample" id="add-sample-button" class="button-link do-not-navigate">+ Add Sample</a>
    <span class="help">Click on a row to edit.</span>
</p>
<table id="social-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>Type</th>
        <th>Message</th>
        <th>Sort Order</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="hidden" id="edit-social-data-form" title="Add/Edit Social Messaging">
    <p><?php echo $formTitle;?></p>
    <form>
        <label>
            <div>Type:</div>
            <select class="editable" name="type" data-default="<?php echo SocialMessageSample::TWEET_TYPE;?>"><?php
                foreach ($supportedTypes as $type){
                ?><option value="<?php echo $type;?>"><?php echo $type;?></option><?php
                }
                ?></select>
        </label>
        <label>
            <div>Message: <a class="help-modal do-not-navigate" href="help-message-text">Help on Message</a></div>
            <textarea class="editable" name="message"></textarea>
        </label>
        <label>
            <div>Sort Order (optional):</div>
            <input class="editable" name="sort_order" pattern="\d*" value="0" data-default="0">
        </label>
        <label>
            <div>Status:</div>
            <select class="editable" name="status"><?php
                foreach ($availableStatuses as $status){
                ?><option value="<?php echo $status;?>"><?php echo $status;?></option><?php
                }
                ?></select>
        </label>
        <input class="editable" type="hidden" name="id" value="0" data-default="0">
        <input type="hidden" name="relevant_entity" value="<?php echo $relevantEntity;?>">
        <input type="hidden" name="relevant_entity_id" value="<?php echo $relevantEntityId;?>">
    </form>
</div>
<div style="display:none;">
    <div id="help-message-text" title="Messages">
        <p>Some rules (subject to modification):</p>
        <ul>
            <li>- Use {{url}} to indicate the url of the current page.</li>
            <li>- HTML is not explicitly supported</li>
            <li>- Length limits are not imposed, but a validation warning will be displayed if the estimated length is too large.</li>
            <li>- Mark a message 'DRAFT' or 'DELETED' to remove it from live display.</li>
            <li>- You may change the message type (e.g., from 'tweet' to 'blurb') arbitrarily.</li>
        </ul>
        <p>'tweet' is very short. 140 chars. urls are shortened to about 20 chars automatically. The length estimator always counts all urls as 20 chars.</p>
        <p>'share' is a generic term to cover Facebook Posts and similar "medium to large" posts. The text limit on these is fairly large (slightly over 30K).</p>
        <p>'email', as in 'newsletter email blurb' is an unlimited text snippet.</p>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var socialListTable = $("#social-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiSocialMessaging.php",
            oTable = socialListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aaSorting": [ ],
                "aoColumns" :[
                    {"mData": "id", "sWidth": "20px"},
                    {"mData": "type", "sWidth": "20px"},
                    {"mData": "message"},
                    {"mData": "sortOrder", "sWidth": "20px"},
                    {"mData": "status", "sWidth": "20px"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0, 3 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "relevantEntity", "value": "<?php echo $relevantEntity;?>" } );
                    aoData.push( { "name": "relevantEntityId", "value": "<?php echo $relevantEntityId;?>" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            }),
            editSocialDataDialog = $("#edit-social-data-form"),
            editSocialDataForm = editSocialDataDialog.find("form"),
            clearFormData = function(){
                editSocialDataForm.find(".editable").each(function(i,item){
                    var element = $(item),
                        defaultValue = element.attr("data-default");
                    if (defaultValue){
                        element.val(defaultValue);
                    } else {
                        element.val("");
                    }
                });
            };

        editSocialDataDialog.dialog({
            autoOpen: false,
            height: 500,
            width:650,
            modal:true,
            buttons: {
                "Save": function(){
                    $.ajax({
                        url: apiUrl,
                        type: "POST",
                        data: editSocialDataForm.serialize(),
                        success: function(data){
                            if (data.message){
                                alert(data.message);
                            }
                            oTable.fnReloadAjax();
                            clearFormData();
                            editSocialDataDialog.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    editSocialDataDialog.dialog("close");
                }
            },
            close: function(){
                clearFormData();
            }
        });

        socialListTable.on("click", "tr", function(e){
            var row = $(this),
                rowId = row.attr("id"),
                dataId = rowId.replace("social-", "");
            editSocialDataForm.find("input[name='id']").val(dataId);
            $.ajax({
                url: apiUrl,
                type: "GET",
                data: {
                    id: dataId
                },
                success: function(data){
                    editSocialDataForm.find("textarea[name='message']").val(data.message);
                    editSocialDataForm.find("select[name='type']").val(data.type);
                    editSocialDataForm.find("select[name='status']").val(data.status);
                    editSocialDataForm.find("input[name='sort_order']").val(data.sortOrder);

                    editSocialDataDialog.dialog("open");
                }
            });
        });

        $("#add-sample-button").click(function(e){
            e.preventDefault();
            editSocialDataDialog.dialog("open");
        });
    });
</script>