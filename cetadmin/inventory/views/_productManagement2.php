<?php
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."ProductProvider.php");

$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	$ProductsByEFI[$record->efi] = $record;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}

}
//print_pre($ProductsByEFI);
//resort the products without display order by EFI
ksort($ProductsWithOutDisplay);
$ProductWithDisplaySelectDefault = $_GET['ProductsWithDisplayOrder'];
$ProductWithOutDisplaySelectDefault = $_GET['ProductsWithOutDisplayOrder'];
$QStringEFI = $_GET['EFI'];
	$ProductWithDisplaySelect = $productProvider->createSelectPulldown($ProductsWithDisplay,"ProductsWithDisplayOrder",$ProductWithDisplaySelectDefault.$QStringEFI);
	$ProductWithOutDisplaySelect = $productProvider->createSelectPulldown($ProductsWithOutDisplay,"ProductsWithOutDisplayOrder",$ProductWithOutDisplaySelectDefault.$QStringEFI);
	//$ToWarehouse = $warehouseProvider->createSelectPulldown($resultArray,"WarehouseToLocation",$ToWarehouseDefault);	
$SelectedEFI = ($QStringEFI ? $QStringEFI : $ProductWithDisplaySelectDefault.$ProductWithOutDisplaySelectDefault);
$addProduct = $_GET['addproduct'];
?>

  <style>
  #sortable1, #sortable2 {
    border: 1px solid #eee;
    width: 400px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
  }
  #sortable1 li, #sortable2 li {
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 380px;
	font-size:small;
	height:12px;
  }
  .efiDescription {font-weight:normal;}
  .efi {font-weight:bold;}

  </style>
  <script>
  $(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
    $( "#sortable1, #sortable2" ).sortable({
		items: 'li',
		connectWith: ".connectedSortable",
		stop: function(event, ui) {
			var myOrderSortable1 = new Array();
			var myOrderSortable2 = new Array();
			var DisplayArray = new Array();
				
			sortable1.each(function() {
				myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
			});
			sortable2.each(function() {
				myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
			});
			DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
			DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
			//process the change for displayedID
			if (myOrderSortable1.length){
				$.ajax({
					url: "ApiProductManagement.php",
					type: "PUT",
					data: JSON.stringify(DisplayArray),
					success: function(data){
						$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
					}
				});
			}
		}	  
    }).disableSelection();
  });
  </script>
<hr>
<div class="fifteen columns"> 
	<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
	<div class="six columns"> 
		<div class="three columns"><b>Displayed Items</b></div>
		<ul id="sortable1" class="connectedSortable">
		<?php 
			foreach ($ProductsWithDisplay as $product=>$productInfo){
				echo "<li id=\"".$productInfo->efi."\" class=\"ui-state-default\"><a href=\"?nav=product-management&EFI=".$productInfo->efi."\">".$productInfo->efi." <span class=\"efiDescription\">".$productInfo->description."</span></a></li>";
			}
		?>
		</ul>
	</div>
	<div class="one columns">&nbsp;</div>
	<div class="six columns"> 
		<div class="three columns"><b>Hidden Items</b></div>
		<ul id="sortable2" class="connectedSortable">
		<?php 
			foreach ($ProductsWithOutDisplay as $product=>$productInfo){
				echo "<li id=\"".$productInfo->efi."\" class=\"ui-state-disabled\"><a href=\"?nav=product-management&EFI=".$productInfo->efi."\" style=\"text-decoration:none;\"><span class=\"efi\">".$productInfo->efi."</span> &nbsp;&nbsp;".$productInfo->description."</a></li>";
			}
		?>
		</ul>
	</div>
</div> 
 
</body>
</html>