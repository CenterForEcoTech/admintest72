<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - Inventory";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "product-transfer":
            $displayPage = "views/_productTransfer.php";
            break;
        case "product-transfer_history":
            $displayPage = "views/_productTransferHistory.php";
            break;
        case "product-transfer_leakage":
            $displayPage = "views/_productTransferLeakage.php";
            break;
        case "product-received":
            $displayPage = "views/_productTransfer.php";
            break;
        case "product-countadjust":
            $displayPage = "views/_productTransfer.php";
            break;
        case "product-setfillqty":
            $displayPage = "views/_productTransfer.php";
            break;
        case "product-physicalcount":
            $displayPage = "views/_productTransfer.php";
            break;
        case "product-onhand":
            $displayPage = "views/_productOnHand.php";
            break;
        case "product-management":
            $displayPage = "views/_productManagement.php";
            break;
        case "warehouse-management":
            $displayPage = "views/_warehouseManagement.php";
            break;
		case "reports":
			$displayPage = "_reportsTabs.php";
			break;
		case "report-efimismatch":
			$displayPage = "views/_efiMisMatch.php";
			break;
		case "report-inventoryusage":
			$displayPage = "views/_reportInventoryUsage.php";
			break;
		case "report-closurerates":
			$displayPage = "views/_reportClosureRates.php";
			break;
		case "report-commissions":
			$displayPage = "views/_reportCommissions.php";
			break;
		case "report-unacknowledgetransfer":
			$displayPage = "views/_productTransferUnAcknowledge.php";
			break;
		case "report-warehousefillqty":
			$displayPage = "views/_reportWarehouseFillQty.php";
			break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Inventory Administration</h1>';
include_once("_inventoryMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);
echo '
</div> <!-- end container -->';
include("../_footer.php");
?>