<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Inventory)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>" style="position:relative;">
	<ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Inventory))){?>
		<li><a id="product-management" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-management" class="button-link">Product Management</a></li>	
<!--	
		<li><a id="product-received" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-received" class="button-link">Product Received</a></li>
			<li><a id="product-transfer" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-transfer" class="button-link">Product Transfer</a></li>
			<li><a id="product-countadjust" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-countadjust" class="button-link">Product Count Adjust</a></li>
			<li><a id="product-onhand" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=product-onhand" class="button-link">Product On Hand</a></li>
			
		<?php }?>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Warehouse))){?>
			<li><a id="warehouse-management" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=warehouse-management" class="button-link">Warehouse Management</a></li>
		<?php }?>
			<li id="ReportMenuDropDown"><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>inventory/?nav=reports" class="button-link">Reports</a>
				<?php ($nav != "reports" ? include('_reportsTabs.php') : "");?>
			</li>
        <li style="margin-left:10px;"><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
-->   
 </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "product-transfer_history":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-efimismatch":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-commissions":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-unacknowledgetransfer":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
			if (currentPage == "reports"){
				var currentDropDown = '<?php echo $navDropDown;?>',
				activeDropDown;
				if (currentDropDown){
					activeDropDown = $("#reportDropDownItems").find("#"+currentDropDown);
					if (activeDropDown.length){
						activeDropDown.addClass("ui-state-highlight");
					}
				}
			
			}
        }
    });
</script>
<?php }?>