<?php
include_once($dbProviderFolder."ContentProvider.php");
include_once($dbProviderFolder."ReferralCodeHelper.php");
if (!isset($emailTemplateProvider)){
    $emailTemplateProvider = new EmailTemplateProvider($mysqli);
}
$template = new EmailTemplate(); // default
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $getTemplateRequest = new GetTemplateRequest();
    $getTemplateRequest->id = $_GET["id"];
    $getTemplateResponse = $emailTemplateProvider->get($getTemplateRequest);
    if (count($getTemplateResponse->collection)){
        $template = $getTemplateResponse->collection[0];
    }
}
$editPageHeader = "Create Email Template";
if ($template->name){
    $editPageHeader = "Edit Email Template";
}
$template->init();

// get static values
$emailTypes = $emailTemplateProvider->getTypes();
$tags = ReferralCodeHelper::getAllReferralCodes($mysqli);
$tagNames = array();
foreach($tags as $tag){
    $tagNames[] = $tag['code'];
}
?>
<form id="edit-email-template" class="basic-form sixteen columns" xmlns="http://www.w3.org/1999/html">
    <h3><?php echo $editPageHeader;?></h3>
    <input type="hidden" name="id" id="template-id" value="<?php echo $template->id;?>">

    <p>
        <a href="save-email-template" id="save-email-template" class="button-link do-not-navigate">Save</a>
        <a href="clone-email-template" id="clone-email-template" class="button-link do-not-navigate">Clone</a>
    </p>

    <label>
        <div>Email Subject:</div>
        <input type="text" name="emailSubject" value="<?php echo $template->emailSubject;?>">
    </label>

    <div class="markdown-editor eight columns" >

        <label for="markdown-source">Editor</label>
        <div id="notes-button-bar" class="wmd-button-bar"></div>
        <textarea id="markdown-source" class="wmd-input" cols="92" rows="15"><?php echo $template->markdownSource;?></textarea>

        <label for="html-source">Preview</label>
        <div id="notes-preview" class="wmd-preview"></div>
        <textarea id="html-source" style="display:none;"></textarea>
    </div>
    <div class="seven columns">
        <fieldset>
            <legend>Identifiers</legend>
            <label>
                <span>Template Name:</span>
                <input type="text" id="template-name" name="name" value="<?php echo $template->name;?>">
            </label>
            <label>
                <span>Type:</span>
                <select id="email-type">
                    <?php foreach($emailTypes as $emailTypes){
                        $value = $emailTypes->id;
                        $display = $emailTypes->name;
                        $selected = ($template->emailTypeId == $value) ? "selected" : "";
                    echo "<option value='".$value."' ".$selected.">".$display."</option>";

                }?>
                </select>
            </label>
            <label>
               <span>Referral Codes: <a class="help-modal do-not-navigate" href="help-referral-codes">Help on Referral Codes</a></span>
                <input id="referral-codes" name="referralCodes" value="<?php echo implode(',', $template->referralCodesArray);?>">
            </label>

        </fieldset>
        <fieldset>
            <legend>Test Values</legend>
            <b><em>Need help on variable names?</em></b> --&gt; <a class="help-modal-wide do-not-navigate" href="help-template-type">Help on Variables by template type</a>
            <?php
            $fieldsArray = $template->getFields();
            if (count($fieldsArray)){?>
                <table id="custom-values">
                    <thead style="display:none;">
                    <tr>
                        <th>variable</th>
                        <th>test value</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($fieldsArray as $templateField) {
                        $variableName = $templateField->variableName;
                        $sampleValue = $templateField->getSampleValue();
                        ?>
                    <tr>
                        <td>{<?php echo $variableName; ?>}</td>
                        <td><input type="text" id="<?php echo $variableName; ?>" value="<?php echo $sampleValue; ?>"></td>
                        <td><a href="delete-field" class="delete-link do-not-navigate" title="delete">Delete</a></td>
                    </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <input type="button" id="apply-variables" value="Apply Test Values" class="button-link">
                <a href="help-apply-variables" class="help-modal do-not-navigate">Help on Applying Variables</a>
                    <hr solid>
                <input type="text" id="test-email-recipient" class="auto-watermarked" title="email@domain.com">
                <input type="button" id="send-test-email" value="Test Email" class="button-link">
                <a href="help-test-email" class="help-modal do-not-navigate">Help on Sending Test Email</a>
                <?php } else { ?>
                <p>Create a template with some valid variables inside curly braces, then save. Return to this form and some sample values should be available.</p>
                <?php } ?>
        </fieldset>
    </div>
</form>

<div style="display:none;">
    <div id="dialog-confirm-delete" title="Delete this template?">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this variable?</p>
    </div>
    <div id="help-referral-codes" title="Referral Codes">
        <p>Start typing a referral code and select your value from the autocomplete options.</p>
        <p>If you are creating a clone of the default template for a specific referral code, specify that code here. Invalid codes will not be applied.</p>
        <p>Do not apply any referral codes to the default template. If you do, then the default process will no longer have a template!</p>
    </div>
    <div id="help-apply-variables" title="Testing with Sample Data">
        <p>Use the sample data fields to test how the email will look with real data.</p>

        <p>Note that variable names MUST match the name structure for the context of the email in question. When in doubt, a programmer should create the original template
            with the original set of variables, and subsequent copies can be cloned from that template.</p>
    </div>
    <div id="help-test-email" title="Sending a Test Email">
        <p>Use the "Test Email" button to send yourself a test of the email output.</p>

        <p>There is no need to use the "Apply Test Values" before sending a test email.</p>

        <p><span class="alert warning">WARNING:</span> The program that actually sends email using this template may use different subject lines, and may append other headers and footers to the email. These additional embellishments will not be included in the test email.</p>

        <p><span class="alert warning">WARNING: </span> Clicking "Test Email" does not save the record!</p>
    </div>
    <div id="help-template-type" title="Template Types and what you can do with them">
        <?php include("_helpEmailTemplateTypes.php");?>
    </div>
</div>
<?php // using https://github.com/aehlke/tag-it ?>
<script type="text/javascript">
    $(function(){
        var form = $("#edit-email-template"),
            idField = $("#template-id"),
            nameField = $("#template-name"),
            editorField = $("#markdown-source"),
            rawPreviewField = $("#html-source"),
            previewField = $("#notes-preview"),
            originalTemplateData = <?php echo json_encode($template);?>,
            originalFieldArray = <?php echo json_encode($template->getFields());?>,
            referralCodeField = $('#referral-codes'),
            customValuesTable = $("#custom-values"),
            emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,
            getFormData = function(){
                var data = {
                    "id": idField.val(),
                    "name": nameField.val(),
                    "emailSubject": form.find("input[name='emailSubject']").val(),
                    "emailTypeId": $("#email-type").val(),
                    "markdownSource" : editorField.val(),
                    "htmlSource" : rawPreviewField.val(),
                    "referralCodesArray": referralCodeField.tagit('assignedTags'),
                    "fieldArray" : []
                },
                    fieldRows = customValuesTable.find("input");

                // add sample values
                fieldRows.each(function(index, el){
                    var element = $(el),
                        fieldData = {
                            "variableName": element.attr("id"),
                            "sampleValue": element.val()
                        };
                    data.fieldArray.push(fieldData);
                });
                return data;
            };

        editorField.wmd({
            "helpLink": "http://daringfireball.net/projects/markdown/",
            "helpHoverTitle": "Markdown Help",
            "button_bar": "notes-button-bar",
            "preview": "notes-preview",
            "output": "html-source"
        });

        $("#apply-variables").click(function(){
            var text = rawPreviewField.val(),
                rows = customValuesTable.find("input");

            rows.each(function(index, el){
                var element = $(el),
                    mergeTag = "{" + element.attr("id") + "}",
                    mergeValue = element.val(),
                    pattern = new RegExp(mergeTag, "g");
                text = text.replace(pattern, mergeValue);
                previewField.html(text);
            });
        });

        referralCodeField.tagit({
            availableTags:<?php echo json_encode($tagNames);?>
        });

        $("#save-email-template").click(function(){
            // TODO: some validation maybe?
            var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEmailTemplates.php",
                data = getFormData();

            // save
            $.ajax({
                url: url,
                type: "PUT",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                statusCode:{
                    201: function(data){
                        var newlyAdded = idField.val() != data.id;
                        if (newlyAdded){
                            window.location.href = '<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-email-template&id=' + data.id;
                        } else {
                            window.location.reload();
                        }
                    },
                    409: function(jqXHR, textStatus, errorThrown){
                        var data = $.parseJSON(jqXHR.responseText),
                            record = data.record,
                            dupeExists = idField.val() != record.id;
                        if (dupeExists){
                            alert(data.message);
                        } else {
                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                        }
                    }
                }
            });
        });

        $("#send-test-email").click(function(){
            // TODO: some validation maybe?
            var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEmailTemplates.php",
                template = getFormData(),
                recipient = $("#test-email-recipient").val(),
                data = {
                    template: template,
                    recipient: recipient
                };

            if ($.trim(recipient) && emailReg.test(recipient)){
                // send
                $.ajax({
                    url: url,
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "json",
                    contentType: "application/json",
                    statusCode:{
                        200: function(data){
                            alert("email sent");
                        },
                        409: function(jqXHR, textStatus, errorThrown){
                            var data = $.parseJSON(jqXHR.responseText);
                            if (data.message){
                                alert(data.message);
                            } else {
                                alert("Unexpected condition encountered. Please refresh the page and try again.")
                            }
                        }
                    }
                });
            } else {
                alert("Please enter a valid email.");
            }
        });

        $("#clone-email-template").click(function(){
            idField.val(0);
            nameField.val("Copy of " + nameField.val());
        });

        $("#custom-values").on("click", ".delete-link", function(){
            var rowToDelete = $(this).closest("tr");
            $( "#dialog-confirm-delete" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Delete": function() {
                        rowToDelete.remove();
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });
        });

        $("#nav-menu").find("#manage-email-templates").addClass("ui-state-highlight");
    });
</script>