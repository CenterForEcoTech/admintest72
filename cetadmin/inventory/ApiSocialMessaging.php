<?php
include_once("../_config.php");
include_once($dbProviderFolder."ContentProvider.php");
include_once($rootFolder.$adminFolder."cms/model/_socialMessageTableRow.php");

$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        if ($_GET["id"]){
            $provider = new SocialMessagingSampleProvider($mysqli);
            $criteria = SocialMessageTableRow::getCriteria($_GET);
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;
            if (count($resultArray)){
                $results = new SocialMessageTableRow($resultArray[0]);
            }
        } else {

            $provider = new SocialMessagingSampleProvider($mysqli);
            $criteria = SocialMessageTableRow::getCriteria($_GET);
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new SocialMessageTableRow($record);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        }
        break;
    case "POST":
        if ($_POST["id"]){
            $record = SocialMessageSampleFactory::buildFrom((object)$_POST);
            $provider = new SocialMessagingSampleProvider($mysqli);
            $updateResponse = $provider->update($record, getAdminId());
            if ($updateResponse->success && $updateResponse->affectedRows > 0){
                $results->success = true;
                $results->record = $updateResponse->record;
                $results->message = $updateResponse->message;
            } else {
                $results->success = false;
                $results->message = "failed to update message";
            }
        } else {
            $record = SocialMessageSampleFactory::buildFrom((object)$_POST);
            $provider = new SocialMessagingSampleProvider($mysqli);
            $insertResponse = $provider->add($record, getAdminId());

            if ($insertResponse->success && $insertResponse->affectedRows > 0){
                $results->success = true;
                $results->record = $insertResponse->record;
                $results->message = $insertResponse->message;
            } else {
                $results->success = false;
                $results->message = "failed to insert message";
            }
        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();