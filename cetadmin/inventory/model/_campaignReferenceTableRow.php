<?php

class CampaignReferenceTableRow{
    public $DT_RowId;
    public $id;
    public $page;
    public $sortOrder;
    public $displayText;
    public $hoverText;
    public $url;
    public $targetID;
    public $status;

    public function __construct(TagReference $record){
        $this->DT_RowId = "reference-".$record->id;
        $this->id = $record->id;
        $this->page = $record->page;
        $this->sortOrder = $record->sortOrder;
        $this->displayText = $record->displayText;
        $this->hoverText = $record->hoverText;
        $this->url = $record->url;
        $this->targetID = $record->targetID;
        $this->status = $record->isActive;
    }

    private static $columnMapping = array(
        0 => "page",
        1 => "sortOrder",
        2 => "displayText",
        3 => "hoverText",
        4 => "url",
        5 => "targetID",
        6 => "status"
    );

    public static function getCriteria($datatablePost){
        $criteria = new CampaignResourceCriteria();
        if ($datatablePost['tagID']){
            $criteria->tagId = $datatablePost['tagID'];
        }
        if ($datatablePost['id']){
            $criteria->id = $datatablePost['id'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}