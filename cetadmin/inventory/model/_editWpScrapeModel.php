<?php
include_once($dbProviderFolder."ContentProvider.php");
if (isset($_GET['id']) && is_numeric($_GET['id'])){
    include_once($dbProviderFolder."ContentProvider.php");
    $provider = new WordPressContentScrapeConfigProvider($mysqli);
    $criteria = new WordPressContentScrapeCriteria();
    $criteria->id = $_GET['id'];
    $paginationResult = $provider->get($criteria);
    $resultArray = $paginationResult->collection;
    if (count($resultArray)){
        /* @var $record WordPressContentScrapeConfig */
        $record = $resultArray[0];
    }
}
if (!isset($record)){
    $record = new WordPressContentScrapeConfig();
}