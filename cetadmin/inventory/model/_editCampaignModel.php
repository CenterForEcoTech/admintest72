<?php
if (isset($_GET['id']) && is_numeric($_GET['id'])){
    include_once($dbProviderFolder."CampaignProvider.php");
    $provider = new CampaignProvider($mysqli);
    $criteria = new CampaignCriteria();
    $criteria->id = $_GET['id'];
    $paginationResult = $provider->get($criteria);
    $resultArray = $paginationResult->collection;
    if (count($resultArray)){
        /* @var $campaignRecord CampaignRecord */
        $campaignRecord = $resultArray[0];
        $campaignRecord->setImageUrl($campaignImageUrlPath);
    }
}
if (!isset($campaignRecord)){
    $campaignRecord = new CampaignRecord();
}