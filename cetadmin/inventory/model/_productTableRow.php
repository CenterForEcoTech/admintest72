<?php
class ProductTableRow{
    public $displayOrderId;
    public $efi;
    public $description;
    public $unitsPerPack;
    public $unitsPerCase;
	public $minimumQty;
	public $orderQty;
	public $minimumDays;
	public $currentDaysOnHandQty;
	public $currentDaysOnHandDays;
	public $cost;
    public $bulbStyle;
    public $bulbType;
	public $casesId;
	public $unitsId;
	public $totalUnitsId;
	public $canBeOrderedId;

    public function __construct(ProductRecord $record, $serverImgPath){
        $this->displayOrderId = $record->displayOrderId;
        $this->efi = $record->efi;
        $this->description = $record->description;
        $this->unitsPerPack = $record->unitsPerPack;
        $this->unitsPerCase = $record->unitsPerCase;
        $this->minimumQty = $record->minimumQty;
        $this->orderQty = $record->orderQty;
        $this->minimumDays = $record->minimumDays;
        $this->currentDaysOnHandQty = $record->currentDaysOnHandQty;
        $this->currentDaysOnHandDays = $record->currentDaysOnHandDays;
        $this->cost = $record->cost;
		$this->casesId = "";
		$this->unitsId = "";
		$this->totalUnitsId = str_replace(".","_",$record->efi);
		$this->canBeOrderedId = $record->canBeOrderedId;
	}

    private static $columnMapping = array(
		0 => "displayOrderId",
        1 => "efi",
        2 => "description",
        3 => "unitsPerPack",
        4 => "unitsPerCase",
        5 => "minimumQty",
        5 => "minimumDays",
        6 => "cost",
		7 => "casesId",
		8 => "unitsId",
		9 => "totalUnitsId",
		9 => "canBeOrderedId"
    );

    public static function getCriteria($datatablePost){
        $criteria = new ProductCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
		
		//see if criteria object actually set
		if (count($datatablePost["criteria"])){
			foreach ($datatablePost["criteria"] as $CriteriaName=>$CriteriaValue){
				if (trim($CriteriaValue)){
					$criteria->$CriteriaName=$CriteriaValue;
				}
			}
		}
		
		
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }
        return $criteria;
    }
}

class ProductMovementHistoryTableRow{
    public $efi;
    public $transferDate;
    public $units;
	public $movementType;
    public $warehouseFrom;
    public $warehouseTo;
	public $createdBy;
    public $purchaseOrder;

    public function __construct(ProductTransferRecord $record){
        $this->efi = $record->efi;
		$this->transferDate = MySQLDate($record->transferDate);
		$this->units = $record->units;
		$this->movementType = $record->movementType;
		$this->warehouseFrom = $record->warehouseFrom;
		$this->warehouseTo = $record->warehouseTo;
		$this->createdBy = $record->createdBy;
		$this->purchaseOrder = $record->purchaseOrder;
	}

    private static $columnMapping = array(
		0 => "warehouseFrom",
        1 => "warehouseTo",
        2 => "transferDate",
        3 => "efi",
        4 => "units",
        5 => "createdBy",
        6 => "purchaseOrder"
    );

    public static function getCriteria($datatablePost){
        $criteria = new ProductCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
		
		//see if criteria object actually set
		if (count($datatablePost["criteria"])){
			foreach ($datatablePost["criteria"] as $CriteriaName=>$CriteriaValue){
				if (trim($CriteriaValue)){
					$criteria->$CriteriaName=$CriteriaValue;
				}
			}
		}
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }
        return $criteria;
    }
}