<?php
include_once($dbProviderFolder."EventTemplateProvider.php");
if (!isset($eventTemplateProvider)){
    $eventTemplateProvider = new EventTemplateProvider($mysqli);
}
if (!isset($tagProvider)){
    $tagProvider = new TagProvider($mysqli);
}
$template = new EventTemplate(); // default
if (isset($_GET["id"]) && strlen($_GET["id"])){
    $template = $eventTemplateProvider->getTemplateByID($_GET["id"]);
}

// get static values
$tags = $tagProvider->getAllTags();
$tagNames = array();
foreach($tags as $tag){
    $tagNames[] = $tag->TagList_Name;
}
?>