<?php
include_once($dbProviderFolder."EventProvider.php");
include_once($dbProviderFolder."EventTransactionProvider.php");
include_once($dbProviderFolder."EventTemplateProvider.php");
$eventProvider = new EventProvider($mysqli);
if (!isset($tagProvider)){
	$tagProvider = new TagProvider($mysqli);
}
if (isset($_GET["id"]) && strlen($_GET["id"])){
    /* $event EventRecord */
    $event = $eventProvider->getEventById($_GET["id"]);
    if ($event->id){
        $transactionProvider = new EventTransactionProvider($mysqli);
        $hasTransactions = $transactionProvider->hasTransactions($event->id);
    }
}
$editPageHeader = "Edit Event Info";

// get static values
$tags = $tagProvider->getAllTags();
$tagNames = array();
foreach($tags as $tag){
    $tagNames[] = $tag->TagList_Name;
}
?>
<style type="text/css">
    ul.tagit{margin:0;}
</style>
    <h3><?php echo $editPageHeader;?></h3>
	<?php if (!$event->id){?><div class="alert">You have entered an invalid Event ID</div><?php }?>
	EventID:<input type="text" id="eventLookupId" value="<?php echo $event->id;?>" style="width:50px;">
    <a href="?nav=<?php echo $nav;?>" class="button-link" id="eventLookupLink">Load Event Info</a>
<form id="edit-event-infoForm" class="basic-form sixteen columns override_skel" xmlns="http://www.w3.org/1999/html">
	<div style="display:none;">
		<input type="text" name="EventID" value="<?php echo $event->id;?>">
	</div>
	<?php if ($event->id){?>
    <fieldset>
        <legend>Event Information</legend>
    <div class="fifteen columns">
        <?php if (!$hasTransactions) {?>
        <a href="DeleteEvent" class="button-link do-not-navigate delete-link" title="Delete this event">Delete Event</a>
    <?php } ?>
        <?php if ($event->isFeaturedCause()){?>
        <span class="alert notice">This is a FEATURED CAUSE event!</span>
        <?php } ?>
    </div>
        <div class="fifteen columns">
        <div class="four columns">
            <label for="Event_Title">Event Name:</label>
                <input type="text" id="Event_Title" name="Event_Title" value="<?php echo $event->title;?>" class="four columns" >
            <div class="alert"></div>
        </div>

        <div class="paired fields five columns">
            <label for="Event_Date">Event Start Date/Time:</label>
                <input type="text" class="auto-watermarked three columns nodelete" title="date" name="Event_Date" id="Event_Date" value="<?php echo $event->startDate ? date("m/d/Y", $event->startDate) : "";?>">
                <select name="Event_StartTime" id="Event_StartTime" class="two columns">
                    <?php
                    $selectedTime = strtotime(date("H:i", $event->startDate));
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            <div id="Event_Date_Error" class="alert"></div>
        </div>
        <div class="paired fields five columns">
            <label for="Event_EndDate">Event End Date/Time:</label>
                <input type="text" class="auto-watermarked three columns nodelete" title="date" name="Event_EndDate" id="Event_EndDate" value="<?php echo ($event->endDate) ? date("m/d/Y", $event->endDate) : "";?>">
                <select name="Event_EndTime" id="Event_EndTime" class="two columns">
                    <?php
                    $selectedTime = strtotime(date("H:i", $event->endDate));
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            <div id="Event_EndDate_Error" class="alert" style="clear:both;"></div>
        </div>
    </div>

    <div class="fifteen columns">
        <label class="fourteen columns">
            <div>Event Description (Public):</div>
                <textarea name="Event_Description" class="eleven columns"><?php echo $event->description;?></textarea>
            <div class="alert eleven columns"></div>
        </label>
    </div>

    <div class="fifteen columns">
        <label class="fourteen columns">
            <div>Private Message to Local Businesses (Optional):</div>
                <textarea name="Event_OrgWhyYou" class="eleven columns"><?php echo $event->whyHostThisDescription;?></textarea>
            <div class="alert eleven columns"></div>
        </label>
    </div>


    <div class="fifteen columns">
		<div class="six columns">
			<label for="Event_OrgAnticipatedCount">Expected Attendance (Optional):</label>
				<input type="text" id="Event_OrgAnticipatedCount" name="Event_OrgAnticipatedCount" value="<?php echo $event->expectedAttendance;?>" class="two columns" >
			<div class="alert"></div>
		</div>
    </div>
    <div class="fifteen columns">
        <h3>Select Percentage, or fill out the flat donation information</h3>
		<div class="inline-field slider-container thirteen columns">
            <label>
                <div>Percentage of event sales to donate: </div>
                    <select id="customer-choice-select" name="Event_MerchantSecondaryPercentage" class="three columns slider-value-input">
                        <?php
                        $selectOptions = "";
                        for ($i = 0; $i <= 100; $i++){
                            $selected = ($event->venue->customerChoicePercentage == $i) ? " selected" : "";
                            $selectOptions .= "<option value='".$i."'".$selected.">".$i."%</option>\n";
                        }
                        ?>
                        <?php echo $selectOptions;?>
                    </select>
                <div class="alert"></div>
                <div id="customer-choice-slider" class="percentage-slider fat-slider nine columns no-mobile"></div>
            </label>
		</div>
       <script>
            $(function() {
                $( "#customer-choice-slider" ).slider({
                    range: "min",
                    value: <?php if($event->venue->customerChoicePercentage){echo $event->venue->customerChoicePercentage;}else{echo "0";}?>,
                    min: 0,
                    max: 100,
                    slide: function( event, ui ) {
                        $( "#customer-choice-select" ).val(ui.value);
                    }
                });
                $( "#customer-choice-select" ).val($( "#customer-choice-slider" ).slider( "value" ));

                $("#customer-choice-select").focus(function(){
                    $("#customer-choice-slider").addClass("ui-state-hover");
                });

                $("#customer-choice-select").change(function(){
                    $("#customer-choice-slider").slider("value",  $(this).val());
                }).change();
				
            });
        </script>
        <fieldset>
            <legend>Flat Donation</legend>
            <label class="fourteen columns">
                <input type="checkbox" name="isFlatDonation" value="1" <?php echo ($event->venue->isFlatDonation) ? "checked" : "";?>> <span>Is Flat Donation?</span>
            </label>
            <label class="fourteen columns">
                <div>Donation per Unit:</div>
                $<input type="text" name="donationPerUnit" value="<?php echo $event->venue->donationPerUnit?>" class="auto-watermarked" title="1.00">
            </label>
            <label class="fourteen columns">
                <div>Action Phrase:</div>
                <input type="text" name="donationActionPhrase" value="<?php echo $event->venue->flatDonationActionPhrase?>" class="auto-watermarked" title="for every item sold">
            </label>
            <label class="fourteen columns">
                <div>Unit Descriptor:</div>
                <input type="text" name="unitDescriptor" value="<?php echo $event->venue->unitDescriptor?>" class="auto-watermarked" title="item(s)">
            </label>
        </fieldset>
    </div>
        <div class="five columns ">
            <label for="tags">Tags:</label>
            <input id="tags" name="tagNamesArray" value="<?php echo $event->commaDelimitedTagNames;?>">
        </div>
    </fieldset>
    <fieldset>
        <legend>Secret Admin Stuff</legend>
        <div class="fifteen columns">
            <label class="fourteen columns">
                <div>Custom Eventbrite Link:</div>
                <input type="text" name="customAttendLink" class="eleven columns auto-watermarked" title="https://www.eventbrite.com/event/1234?access=cetonline" value="<?php echo $event->customAttendLink;?>">
                <div class="alert eleven columns"></div>
            </label>
            <?php if ($event->isFeaturedCause()){?>
            <span class="alert notice fourteen columns">It is not wise to set a custom link on a Featured Cause Event!</span>
            <?php } ?>

            <label class="fourteen columns">
                <input type="checkbox" name="isNational" value="1" <?php echo ($event->isNationalEvent) ? "checked" : "";?>> <span>Is National Event?</span>
            </label>
            <label class="fourteen columns">
                <div>Order Online Label:</div>
                <input type="text" name="onlineLocationLabel" class="eleven columns auto-watermarked" title="Order Online" value="<?php echo $event->onlineLocationLabel;?>">
                <div class="alert eleven columns"></div>
            </label>
            <label class="fourteen columns">
                <div>Order Online Link:</div>
                <input type="text" name="onlineLocationLink" class="eleven columns auto-watermarked" title="https://merchant-website.com/store" value="<?php echo $event->onlineLocationLink;?>">
                <div class="alert eleven columns"></div>
            </label>

            <label class="fourteen columns">
                <input type="checkbox" name="suppressCharitySelectionForm" value="1" <?php echo ($event->suppressCharitySelectionForm) ? "checked" : "";?>> <span>Suppress Charity Selection Form?</span>
            </label>
            <label class="fourteen columns">
                <input type="checkbox" name="suppressPrintFlier" value="1" <?php echo ($event->suppressPrintFlier) ? "checked" : "";?>> <span>Suppress Print Flier?</span>
            </label>
        </div>
    </fieldset>
    <div id="templateResults" class="fifteen columns"></div>
    <div class="fifteen columns">
        <a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-event-info&id=<?php echo $_GET['id'];?>" class="button-link">Cancel</a>
        <a href="save-event-template" id="save-event-template" class="button-link do-not-navigate">Save</a>
    </div>
	<?php }?>
</form>
<div id="dialog-confirm-delete" title="Delete this event?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this event?</p>
</div>
<script>
$(function() {
    var formElement = $("#edit-event-infoForm"),
        titleEl = $("#Event_Title"),
        startDateEl = $("#Event_Date"),
        endDateEl = $("#Event_EndDate"),
        startTimeEl = $("#Event_StartTime"),
        endTimeEl = $("#Event_EndTime"),
		eventLookupId = $("#eventLookupId"),
		eventLookupLink = $("#eventLookupLink"),
        areDatesValid = function(validatingStart){
            var startDateVal = startDateEl.val(),
                startTimeVal = startTimeEl.val(),
                endDateVal = endDateEl.val(),
                endTimeVal = endTimeEl.val(),
                isValid = true,
                startalertElement = $("#Event_Date_Error"),
                endAlertElement = $("#Event_EndDate_Error");

            if (startDateVal && endDateVal){

                var	startDate = new Date(startDateVal + " " + startTimeVal),
                    endDate = new Date(endDateVal + " " + endTimeVal);

                startalertElement.html("");
                endAlertElement.html("");
                if (endDate < startDate && (startTimeVal && endTimeVal)){
                    if (validatingStart){
                        startalertElement.html("Must be earlier than End Time");
                    } else {
                        endAlertElement.html("Must be later than Start Time");
                    }
                    isValid = false;
                }
            } else{
				if (!startDateVal){startalertElement.html("Start Date cannot be blank");}
                if (!endDateVal){endAlertElement.html("End Date cannot be blank");}
				isValid = false;
			}
            return isValid;
        };
		
	eventLookupId.blur(function(){
		eventLookupLink.attr("href","?nav=<?php echo $nav;?>&id="+eventLookupId.val());
	});

    $("#tags").tagit({
        availableTags:<?php echo json_encode($tagNames);?>
    });

    $("#save-event-template").click(function(e){

        $('#templateResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (areDatesValid() && formElement.valid()){
            //run ajax process
            $.ajax({
                url: "ApiEventInfo.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#templateResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#templateResults').html(message).addClass("alert");
                }
            });
        }
    });

    startDateEl.datepicker({
        minDate: 0,
        onSelect: function(dateText, inst) {
            if (endDateEl.val()){
                if (!areDatesValid(true)) {
                    endDateEl.val(dateText);
                }
            } else {
                endDateEl.val(dateText);
            }
        }
    });
    endDateEl.datepicker({
        minDate: 0,
        onSelect: function(dateText, inst) {
            areDatesValid();
        }
    });
    startTimeEl.change(function(){
        areDatesValid(true);
    });
    endTimeEl.change(function(){
        areDatesValid();
    });
    startDateEl.blur(function(){
        areDatesValid();
    });
    endDateEl.blur(function(){
        areDatesValid();
    });

    formElement.validate({
        rules: {
            Event_Title: {
                required: true,
                maxlength: 75
            }
        },
        messages: {
            Event_Title: {
                required: "Please give a title to your event.",
                maxlength: "Title must be 75 characters or less."
            }
        }
    });
    titleEl.blur(function(){
        formElement.validate().element(titleEl);
    });

    titleEl.focus();
    $("#nav-menu").find("#edit-event-info").addClass("ui-state-highlight");

    formElement.on("click", ".delete-link", function(){

        $( "#dialog-confirm-delete" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Delete": function() {
                    var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEventInfo.php",
                        data = {
                            id: '<?php echo $event->id;?>'
                        };

                    // delete
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: JSON.stringify(data),
                        dataType: "json",
                        contentType: "application/json",
                        statusCode:{
                            200: function(data){
                                alert("Event has been deleted. The page will be refreshed");
                                window.location = "?nav=edit-event-info";
                            },
                            409: function(jqXHR, textStatus, errorThrown){
                                var data = $.parseJSON(jqXHR.responseText);
                                if (data.message){
                                    alert(data.message);
                                } else {
                                    alert("Unexpected condition encountered. Please refresh the page and try again.")
                                }
                            }
                        }
                    });
                    $( this ).dialog( "close" );
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
});
</script>

<?php
if ($event->id){
    $relevantEntity = SocialMessageSample::EVENT_ENTITY;
    $relevantEntityId = $event->id;
    include($rootFolder.$adminFolder."cms/views/_socialMessagesList.php");
}
?>