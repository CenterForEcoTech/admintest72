<?php
include_once("../_config.php");
$pageTitle = "Admin - Prototypes";
?>
<html>
<?php include("../_header.php");
?>
<div class="container">
    <h1>Prototypes</h1>
    <?php

    $nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
    include_once("_prototypeMenu.php");
    if ($nav){
        switch ($nav){
            case "mailchimp-subscribe":
                include("_mailchimpSubscribe.php");
                break;
            default:
                break;
        }
    }
    ?>
</div> <!-- end container -->
<?php
include("../_footer.php");
?>