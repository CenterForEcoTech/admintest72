<div id="nav-menu">
    <ul>
        <li><a id="mailchimp-subscribe" href="<?php echo $CurrentServer.$adminFolder;?>prototypes/?nav=mailchimp-subscribe" class="button-link">Mailchimp Subscribe</a></li>
        <?php if ($_SERVER['REQUEST_URI'] != '/'.$adminFolder){ ?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link">Admin Console</a></li>
        <?php } // if on admin main?>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>