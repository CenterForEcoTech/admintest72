<h3>Sign up for LostBikingCats Newsletter!</h3>
<p>Development log:</p>
<ol style="list-style-type: decimal;">
    <li>Embed the generated code from mailchimp (turns out due to SSL as well as cross-domain javascript, it isn't really worth using their code, but I kept their form layout).</li>
    <li>Add ajax against our proxy API to subscribe (this is more or less what our subscribe form will do, of course, we'll be fancier about it in case of errors.</li>
    <li>Add ajax to mimic the second opt-in "confirmation" page; This is the URL we send the user in the initial email, which comes to our server. They are presented with a page that has all the info needed to subscribe (because we stored that information in our database and added our own fun verification code (just like registration activation). In fact, we can probably piggy-back on the same concept.</li>
</ol>

<p>General findings</p>
<ul style="list-style-type: disc;">
    <li>API is super easy to use. Just need an API key (on your Mailchimp account, you just make one--and if we catch anyone using it, we inactivate it and generate a new one).</li>
    <li>We don't have to use double-opt-in through mailchimp (i.e., if we combine newsletter with registration with checkboxes, we simply send the confirm and welcome message). All that matters is that WE do double-opt in somehow.</li>
</ul>

<p>Notes about the prototype</p>
<ul style="list-style-type: disc;">
    <li>The link in the email goes to mailchimps servers because I didn't configure the url in my account. In practice, we will specify a URL and send the user to our site.</li>
    <li>The error message if you try to register yourself twice is coming from their server unmodified. We will of course modify this to suit our own use case.</li>
</ul>

<!-- Begin MailChimp Signup Form -->
<?php
/* blocked due to SSL -- imported and stored locally
<link href="http://cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
// we also removed embedded javascript that was doing a cross-domain call
// http://blogspot.us6.list-manage.com/subscribe/post?u=ab4b4929c7157837b0c918633&amp;id=a893d6519d
 */
?>

<link href="mailchimp-embed-classic-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
         We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */

    /* override skeleton a bit*/
    #mc_embed_signup .clear{
        visibility: visible;
        width:inherit;
        height:inherit;
    }
</style>
<div id="mc_embed_signup">
    <form id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
        <div class="mc-field-group">
            <label for="mce-API">Mailchimp API key </label>
            <input type="text" value="" name="apikey" id="mce-API"> <span class="help">Optional, but you can use it to point to some other Mailchimp acct (go to account settings and choose API key).</span>
        </div>
        <div class="mc-field-group">
            <label for="mce-LIST">Mailchimp List ID </label>
            <input type="text" value="" name="list" id="mce-LIST"> <span class="help">Optional, but you can use it to point to a specific list (go to List Settings and scroll to the bottom to get this value).</span>
        </div>
        <div class="mc-field-group">
            <label for="mce-EMAIL">Email Address </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
        </div>
        <div class="mc-field-group">
            <label for="mce-FNAME">First Name </label>
            <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
        </div>
        <div class="mc-field-group">
            <label for="mce-LNAME">Last Name </label>
            <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
        </div>
        <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" ></div>
            <div class="response" id="mce-success-response" ></div>
        </div>
        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"><span class="help">Click this to simulate the initial subscribe.</span></div>
        <div class="clear"><input type="submit" value="Confirm" name="subscribe" id="mc-embedded-subscribe-confirm" class="button"><span class="help">Click this to simulate the confirmation that you do want to join this list (the email link will send you to a page that has a button that does this).</span></div>
    </form>
</div>
<!--End mc_embed_signup-->
<script type="text/javascript">
    $(function(){
        var form = $("#mc-embedded-subscribe-form"),
            errorDiv = $("#mce-error-response"),
            successDiv = $("#mce-success-response"),
            apiUrl = "ApiMailchimp.php",
            submitForm = function(action){
                var formDataArray = form.serializeArray();
                formDataArray.push({
                    name: "action",
                    value: action
                });
                $.ajax({
                    url: apiUrl,
                    type: "POST",
                    data: $.param(formDataArray),
                    success: function(data){
                        errorDiv.html("");
                        successDiv.html("Hurray! Check your email for a "+ action +" message now!");
                        successDiv.show();
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        var response = $.parseJSON(jqXHR.responseText);
                        errorDiv.html(response.message);
                        successDiv.html("");
                        errorDiv.show();
                    }
                });
            };

        $("#mc-embedded-subscribe").click(function(e){
            e.preventDefault();
            submitForm("subscribe");
        });

        $("#mc-embedded-subscribe-confirm").click(function(e){
            e.preventDefault();
            submitForm("confirm");
        });

        form.submit(function(e){
            e.preventDefault();
        });
    });
</script>
