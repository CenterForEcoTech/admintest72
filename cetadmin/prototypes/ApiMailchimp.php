<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
//error_reporting(1);
//ini_set("display_errors","on");

include_once($siteRoot."functions.php");
include_once($siteRoot."Library/MCAPI.mini.class.php");
$lostbikingcatApiKey = "394b17f466ecb7305ece5e3dbc6320e4-us6";
$lostbikingcatListID = "a893d6519d";
$useSecureConnection = true;
$currentRequest = new Request();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "DELETE is not supported";
        break;
    case "GET":
        echo "GET is not supported";
        break;
    case "POST":
        // No validation for prototype
        // auto add merge vars
        $merge_vars = array();
        foreach($_POST as $key =>$value){
            // for prototype, we take every key that is in allcaps!
            if ($key === strtoupper($key)){
                $merge_vars[$key] = $value;
            }
        }

        // grab api key
        $apiKey = $lostbikingcatApiKey;
        if (isset($_POST['apikey']) && strlen($_POST['apikey'])){
            $apiKey = $_POST['apikey'];
        }
        // grab listid
        $listID = $lostbikingcatListID;
        if (isset($_POST['list']) && strlen($_POST['list'])){
            $listID = $_POST['list'];
        }

        // create the api object
        $mcapi = new MCAPI($apiKey, $useSecureConnection);

        // the subscribe action with sends the opt in email
        if ($_POST['action'] == 'subscribe'){
            $response = $mcapi->listSubscribe($listID, $_POST["EMAIL"], $merge_vars);
            if (!$response){
                $results->error = $mcapi->errorCode;
                $results->message = $mcapi->errorMessage;
                header("HTTP/1.0 409 Conflict");
            } else {
                $results->message = "Success";
            }
        }

        // the subscribe action which directly subscribes and sends welcome email
        if ($_POST['action'] == 'confirm'){
            $email_type = 'html';
            $double_optin = false;
            $update_existing = false;
            $replace_interests = true;
            $send_welcome = true;
            $response = $mcapi->listSubscribe($listID, $_POST["EMAIL"], $merge_vars, $email_type, $double_optin, $update_existing, $replace_interests, $send_welcome);
            if (!$response){
                $results->error = $mcapi->errorCode;
                $results->message = $mcapi->errorMessage;
                header("HTTP/1.0 409 Conflict");
            } else {
                $results->message = "Success";
            }
        }

        break;
    case "PUT":
        echo "PUT is not supported";
        break;
}
output_json($results);
die();
?>