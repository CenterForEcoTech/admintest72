<?php
/**
 * Based on the batch id (of a previously exported batch) or a set of disbursement ids (for specific nonprofits)
 * This generates a csv export of the resulting data in a format compatible with FirstGiving's batch api.
 *
 * See http://developers.firstgiving.com/documentation/disbursement-only/
 */

unset($disbursement_data); // we don't allow an external template to set this and override our checks

$exportPath = $_POST["filepath"];
$exportAll = isset($_POST["export_all"]);
$disbursementIds = $_POST["disbursementIds"];
$processName = "DisbursementBatchExport";
$processIdentifier = $_REQUEST["PHPSESSID"]."__".time();
if (!isset($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = "null";
}
if (!is_numeric($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = "null"; // "null" is the only valid non numeric value
}

include_once("_config.php");
include_once("../repository/mySqlProvider/Helper.php");
include_once("../functions.php");
//
// Verify that db connection configuration is set up
//
if (!isset($dbConfig)){
    echo "This script requires the database configuration file in siteconf.";
    die(); // no db config for source db.
}
if (!isset($dbConfigDisbursement)){
    echo "This script requires the database configuration for the disbursement database in siteconf.";
    die(); // no db config for the target db.
}

include_once($siteRoot."_setupDisbursementConnection.php");

if (isset($exportPath)){
    $shortname = basename($exportPath);
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".$shortname."\";" );
    header("Content-Transfer-Encoding: binary");

    $fileContents = file_get_contents($exportPath);
    echo $fileContents;
    die();
}
if ($exportAll){
    unset($disbursementBatchId); // start fresh
    $sp_query = "CALL sp_mark_disbursed('".$processName."','".$processIdentifier."','all', ".$Config_MinDisbursementAmt.",@outBatchId)";
} else if (isset($disbursementIds) && count($disbursementIds) > 0){
    unset($disbursementBatchId); // start fresh
    $isValid = true;
    $paramIds = "";
    foreach ($disbursementIds as $disbursementId){
        if (is_numeric($disbursementId)){
            if (strlen($paramIds)){
                $paramIds .= ",";
            }
            $paramIds .= $disbursementId;
        }
    }
    $sp_query = "CALL sp_mark_disbursed('".$processName."','".$processIdentifier."','".$paramIds."',".$Config_MinDisbursementAmt.",@outBatchId)";
}

if ($sp_query){
    $result = $mysqli_disb->query($sp_query);
    $batchIdSql = "
        SELECT id
        FROM disbursement_batches
        WHERE processor_name = ?
        AND process_identifier = ?";
    $batchSqlResult = MySqliHelper::get_result($mysqli_disb, $batchIdSql, "ss", array($processName, $processIdentifier));
    if (count($batchSqlResult)){
        $disbursementBatchId = $batchSqlResult[0]["id"];
    }
}
if (isset($disbursementBatchId) && is_numeric($disbursementBatchId)){
    $defaultSettings = array(
        $Config_FGDonorEmailAddress => "mayor@causetown.org",
        $Config_FGDonorFirstName => $SiteName,
        $Config_FGDonorLastName => "",
        $Config_FGDonorTitle => "",
        $Config_FGDonorAddress1 => "107 S West St #548",
        $Config_FGDonorAddress2 => "",
        $Config_FGDonorCity => "Alexandria",
        $Config_FGDonorState => "VA",
        $Config_FGDonorCountry => "US",
        $Config_FGDonorPostcode => "22314",
        $Config_FirstGiving_ApiKey => ""
    );
    foreach($defaultSettings as $key => $value){
        if (!isset(${$key})){
            ${$key} = $value;
        }
    }
    //
    // HACK: using db instance name to do join across dbs!!
    //

    $causetownDbInst = $dbConfig->inst;
    $disbursementInst = $dbConfigDisbursement->inst;

    $selectSql = "

    SELECT
            d.id as disbursement_id,
            d.amount as donation_amount,
            o.OrganizationInfo_EIN as org_EIN,
            dorg.name as org_name,
            IF(dorg.is_fiscally_sponsored, 'yes','') as is_child,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_EIN, o.OrganizationInfo_EIN) as official_ein,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_Name, o.OrganizationInfo_Name) as official_name,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_Address, o.OrganizationInfo_Address) as official_address,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_City, o.OrganizationInfo_City) as official_city,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_State, o.OrganizationInfo_State) as official_state,
            IF (parent.OrganizationInfo_ID, parent.OrganizationInfo_Zip, o.OrganizationInfo_Zip) as official_zip,
            o.OrganizationInfo_ContactFirstName as contact_firstname,
            o.OrganizationInfo_ContactLastName as contact_lastname,
            o.OrganizationInfo_ContactEmail as contact_email,
            o.OrganizationInfo_Website as website,
            o.OrganizationInfo_ID as organization_info_id,
			(
	            SELECT pid
	            FROM ".$causetownDbInst.".permalinks
	            WHERE entity_name = 'organization'
	            AND entity_id = o.OrganizationInfo_ID
                AND status = 'ACTIVE'
	            ORDER BY weight DESC
	            LIMIT 1
			) as pid,
            d.batch_id as disbursement_batch_id
        FROM ".$disbursementInst.".disbursements d
        JOIN ".$disbursementInst.".disbursement_orgs dorg ON d.disbursement_org_id = dorg.id
        JOIN ".$causetownDbInst.".organization_info o ON dorg.organization_info_id = o.OrganizationInfo_ID
        LEFT JOIN ".$causetownDbInst.".organization_info parent
            ON o.OrganizationInfo_FiscalSponsorEIN = parent.OrganizationInfo_EIN
            AND o.OrganizationInfo_FiscalSponsorEIN <> ''
        WHERE d.batch_id = ?";

    $colHeaders = array(
        'disbursement_id',
        'amount',
        'EIN',
        'org_name',
        'isChild',
        'official_ein',
        'official_name',
        'official_address',
        'official_city',
        'official_state',
        'official_zip',
        'contact_firstname',
        'contact_lastname',
        'contact_email',
        'website',
        'organization_info_id',
        'pid',
        'batch_id'
    );

    $disbursement_data = MySqliHelper::get_result($mysqli_disb, $selectSql, "i", array($disbursementBatchId));
}

if (isset($disbursement_data) && count($disbursement_data)){
    $currentDate = date("Y-m-d");
    $filebase = $processName."_".$currentDate."_".$disbursementBatchId;
    $extension = ".csv";
    $filename = $filebase.$extension;
    $fileExists = true;
    $attempts = 0;
    while ($fileExists){
        $savePath = "disbursements/".$filename;
        if (file_exists($savePath)){
            $attempts++;
            $filename = $filebase." (".$attempts.")".$extension;
        } else {
            $fileExists = false;
        }
    }
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".$filename."\";" );
    header("Content-Transfer-Encoding: binary");

    $fileContents = exportCSV($disbursement_data, $colHeaders, true);
    file_put_contents($savePath, $fileContents);
    echo $fileContents;
} else {
    ?>
<h2>Oops!</h2>
<p>
    You are here due to a disbursement spreadsheet download that managed to find nothing to export!
    This might happen if you and some other person simultaneously launched a download of the same dataset,
    and if you are here, chances are the other person got the download.
</p>
<p>The other possibility is that you did not select any disbursements to export.</p>
<p>Use the browser's back button, refresh the page, and try again.</p>
<?php
}
?>