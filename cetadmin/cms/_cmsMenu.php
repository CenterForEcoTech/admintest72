<div id="nav-menu">
    <ul>
        <li><a id="image-library" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=image-library" class="button-link">Image Library</a></li>
        <li><a id="manage-catalogs" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-catalogs" class="button-link">Image Catalogs</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="file-library" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=file-library" class="button-link">File Library (pdfs)</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="manage-campaigns" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-campaigns" class="button-link">Campaigns</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="manage-event-templates" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-event-templates" class="button-link">Event Templates</a></li>
        <li><a id="manage-email-templates" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-email-templates" class="button-link">Email Templates</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="edit-event-info" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-event-info" class="button-link deprecated">Event Edits</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="sample-social" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=sample-social" class="button-link">Sample Social Messaging</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="wordpress-scrapes" href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=wordpress-scrapes" class="button-link">Custom Pages (WP Scrapes)</a></li>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            } else {
                if (currentPage === "edit-image"){
                    $("#image-library").addClass("ui-state-highlight");
                } else if (currentPage == 'edit-catalog'){
                    $("#manage-catalogs").addClass("ui-state-highlight");
                } else if (currentPage == 'edit-campaign'){
                    $("#manage-campaigns").addClass("ui-state-highlight");
                } else if (currentPage == 'edit-file'){
                    $("#file-library").addClass("ui-state-highlight");
                } else if (currentPage === "edit-wordpress-scrape"){
                    $("#wordpress-scrapes").addClass("ui-state-highlight");
                }
            }
        }
    });
</script>