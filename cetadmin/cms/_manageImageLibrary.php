<div class="sixteen columns">
    <h3>Manage Image Library</h3>
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="eight columns">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <span>Cancel upload</span>
                </button>
                <?php /*
                <button type="button" class="btn btn-danger delete">
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
 */ ?>
            </div>
            <!-- The global progress information -->
            <div class="five columns fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar" style="width:0%;"></div>
                </div>
                <!-- The extended global progress information -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The loading indicator is shown during file processing -->
        <div class="fileupload-loading"></div>
        <br>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
    </form>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="form">
            <label>*Title: <input type="text" name="title[]" required title="must be unique and will be used as the filename"></label>
            <label>Description: <input type="text" name="description[]" title="optional"></label>
            <label>Keywords: <input type="text" name="keywords[]" title="space delimited words"></label>
        </td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
        </td>
        <td class="start">{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary">
                <span>Start</span>
            </button>
            {% } %}</td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <span>Cancel</span>
            </button>
            {% } %}</td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
        <td></td>
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="Click to view original image" target="image-preview" ><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="form" data-id="{%=file.id%}">
            <label>Title: {%=file.title%}</label>
            <label>Description: {%=file.description%}</label>
            <label>Keywords: {%=file.keywords%}</label>
        </td>
        <td class="name">
            <a href="?nav=edit-image&id={%=file.id%}" title="{%=file.name%}">{%=file.name%}</a>
        </td>
        <td class="size">
            {%=o.formatFileSize(file.size)%}
            <br>{%=file.width + ''%} x {%=file.height + ''%}
        </td>
        <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <?php /*
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>

            <span>Delete</span>
            </button>
            <input type="checkbox" name="delete" value="1">
 */ ?>
        </td>
    </tr>
    {% } %}
</script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>load-image.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script type="text/javascript">
    $(function () {
        'use strict';
        var uploadForm = $('#fileupload');

        // Initialize the jQuery File Upload widget:
        uploadForm.fileupload({
            url: 'ApiImageLibrary.php',
            dropZone: uploadForm,
            pasteZone: uploadForm,
            prependFiles: true
        });

        uploadForm.bind('fileuploadsubmit', function (e, data) {
            var inputs = data.context.find(':input');
            if (inputs.filter('[required][value=""]').first().focus().length) {
                return false;
            }
            data.formData = inputs.serializeArray();
        });

        // Load existing files:
        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).done(function (result) {
                if (result && result.length) {
                    $(this).fileupload('option', 'done')
                        .call(this, null, {result: result});
                }
            });

    });
</script>