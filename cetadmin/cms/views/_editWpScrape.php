<?php
/* @var $record WordPressContentScrapeConfig */
?>
<style type="text/css">
    .required:after{color:red;content:"*";}
    .required{color:inherit;}
</style>
<h2>Edit Custom Page (scraped from WordPress)</h2>
<form id="edit-form" class="sixteen columns normal template-upload">
    <div class="sixteen columns alpha omega">
        <?php if ($record->isValid()){?>
            <p>
                When creating a new page, please remember that the only URL you can view the page at is the test url.
                If you want this custom page to be viewed by another URL, you must get a dev to add it to .htaccess file.

                <a class="help-modal do-not-navigate" href="help-add-url">Help on Adding Url</a>
            </p>
            <a href="<?php echo $CurrentServer."custompages/".$record->urlName;?>" class="button-link" target="_blank" style="float:right;">View Test Page</a>
            <a href="<?php echo $CurrentServer.$record->urlName;?>" class="button-link" target="_blank" style="float:right;">View Published Page</a>
        <?php } else { ?>
            <p class="alert">There are some bad data in this record preventing it from being displayed.</p>
        <?php } ?>
        <fieldset class="fifteen columns alpha omega">
            <legend>Basics</legend>
            <label>
                <div>
                    <span class="required">Url Name:</span>
                    <a class="help-modal do-not-navigate" href="help-url-name">Help on Url Name</a>
                </div>
                <input type="text" name="urlName" title="Url Name: used as the URL" class="auto-watermarked" value="<?php echo $record->urlName;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>
                    <span class="required">Page Title:</span>
                </div>
                <input type="text" name="pageTitle" title="Page Title: Text that will display in the browser title bar." class="auto-watermarked wide" value="<?php echo $record->pageTitle;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>
                    <span class="required">Page Header:</span>
                </div>
                <input type="text" name="pageHeader" title="Page Heading: used in the page header" class="auto-watermarked" value="<?php echo $record->pageHeader;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>
                    Page Sub Header:
                </div>
                <input type="text" name="pageSubheader" title="Page Sub Header: used in the page header" class="auto-watermarked" value="<?php echo $record->pageSubheader;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div>
                    <span class="required">WordPress URL to scrape:</span>
                </div>
                <input type="text" name="urlToScrape" title="URL to scrape: A valid full URL that points to a WordPress site that can be scraped." class="auto-watermarked wide" value="<?php echo $record->urlToScrape;?>">
                <div class="alert"></div>
            </label>
            <label>
                <input type="checkbox" value="1" name="useCETDashCmsCredentials" <?php echo $record->useCETDashCmsCredentials ? "checked" : "";?>>
                <span>Use CETDash CMS Credentials</span>
            </label>
        </fieldset>
    </div>
    <fieldset class="fifteen columns alpha">
        <input type="hidden" name="id" value="<?php echo $record->id;?>">
        <input type="submit" name="submit" value="Save" class="button-link">
    </fieldset>
</form>
<div style="display:none;">
    <div id="help-url-name" title="Url Name">
        <p>Please enter a value that you expect to append to the site root.</p>
        <p>Example:</p>
        <p>"sixdegrees" => https://causetown.org/sixdegrees</p>
        <p>Note that this name will not automatically work at the root. It is only available for testing using the link in the form. Once you want to publish this, it must be added to .htaccess by a developer.</p>
    </div>
    <div id="help-add-url" title="Adding to .htaccess">
        <h3>How the developer should add this to the .htaccess file</h3>
        <ol>
            <li>Find the "custompages" section</li>
            <li>Add a line like this:</li>
        </ol>
        RewriteRule ^sixdegrees$ custompages/sixdegrees [NC,PT,QSA]
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiWpScrapesManagement.php",
            formElement = $("#edit-form"),
            idField = formElement.find("input[name='id']");
        formElement.submit(function(e){
            e.preventDefault();
            // TODO: add some validation
            $.ajax({
                url: url,
                data: JSON.stringify(formElement.serializeObject()),
                type: "PUT",
                statusCode:{
                    201: function(data){
                        var record = data.record,
                            newlyAdded = idField.val() != record.id;
                        window.location.href = '<?php echo $CurrentServer.$adminFolder;?>cms/?nav=wordpress-scrapes';
                    },
                    409: function(jqXHR, textStatus, errorThrown){
                        var data = $.parseJSON(jqXHR.responseText);
                        if (data.message){
                            alert(data.message);
                        } else {
                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                        }
                    }
                }
            });
        });
    });
</script>