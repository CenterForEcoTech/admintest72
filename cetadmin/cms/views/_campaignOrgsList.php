<?php if ($record->id) { ?>
    <h3>Supporting Organizations</h3>
    <fieldset class="sixteen columns" id="add-org-widget">
        <legend>Add Organizations</legend>
        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.CampaignOrgList = {
                autocompleteOnSelect: function( event, ui ){}
            };
            CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                flavor: "default",
                showState: true,
                limitToRegisteredOrgs: true,
                dropOnFocus: true,
                sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                sourceType : "POST",
                minLength: 3,
                pageLength: 10,
                onSelect: function( event, ui ) {
                    CETDASH.CampaignOrgList.autocompleteOnSelect(event, ui);
                }
            };
        </script>
        <style type="text/css">
            .ui-widget-content .ui-icon.btn{
                padding:0;
            }
        </style>
        <?php include $siteRoot."views/_charityPickerWidget.php";?>
    </fieldset>
    <div class="sixteen columns">
        <table id="organization-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Organization</th>
                <th>Member Id</th>
                <th></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(function(){
            var table = $("#organization-table"),
                apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiTagManagement.php",
                lastRequestedCriteria = null,
                oTable = table.dataTable({
                    "bJQueryUI": true,
                    "bServerSide":true,
                    "sAjaxSource": apiUrl,
                    "iDisplayLength": 50,
                    "aaSorting": [ [0, "desc"]],
                    "aoColumns" :[
                        { "mData": "orgId", "sWidth": "50px" },
                        { "mData": "contactInfo" },
                        { "mData": "memberId", "sWidth": "50px" },
                        { "mData": "id",
                            "mRender": function ( data, type, full ) {
                                return '<a href="delete-template" class="delete-link do-not-navigate" title="delete">Delete</a>';
                            }}
                    ],
                    "aoColumnDefs": [
                        { "bSearchable": false, "aTargets": [ 3 ] },
                        { "bSortable": false, "aTargets": [ 3 ] }
                    ],
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "action", "value": "get_orgs" } );
                        aoData.push( { "name": "campaign_id", "value": "<?php echo $record->id;?>" } );
                    },
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        lastRequestedCriteria = aoData;
                        oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": lastRequestedCriteria,
                            "success": fnCallback
                        } );
                    }
                }),
                resetSection = function(){
                    oTable.fnDraw();
                    $("#add-org-widget").find("input#charity-search-box").val("");
                };

            CETDASH.CampaignOrgList.autocompleteOnSelect = function(event, ui){
                var record = ui.item.record;
                $.ajax({
                    url: apiUrl,
                    type: "POST",
                    data: JSON.stringify({
                        action: "add_org",
                        id: <?php echo $record->id;?>,
                        orgId: record.orgId
                    }),
                    success: function(data){
                        if (data.numAdded){
                            resetSection();
                        } else {
                            alert("No Org added.");
                        }
                    }
                });
            };

            table.on("click", ".delete-link", function(e){
                var row = $(this),
                    rowId = row.closest("tr").attr("id"),
                    dataId = rowId.replace("org-", "");
                e.preventDefault();
                $.ajax({
                    url: apiUrl,
                    type: "POST",
                    data: JSON.stringify({
                        action: "remove_org",
                        id: <?php echo $record->id;?>,
                        orgId: dataId
                    }),
                    success: function(data){
                        resetSection();
                    }
                });
            });
        });
    </script>
<?php } ?>