<h3>Manage Custom Pages (WordPress Scrapes)</h3>
<p>
    <a href="?nav=edit-wordpress-scrape" id="add-item-button" class="button-link">+ Add Custom Page</a>
</p>
<table id="item-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>Url Name</th>
        <th>Title</th>
        <th>Header</th>
        <th>Url To Scrape</th>
        <th>Created Date</th>
        <th>Updated Date</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var datatable = $("#item-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiWpScrapesManagement.php",
            lastRequestedCriteria = null,
            oTable = datatable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "iDisplayLength": 50,
                "aaSorting": [ [0, "desc"]],
                "aoColumns" :[
                    {
                        "mData": "id",
                        "mRender": function ( data, type, full ) {
                            return '<a href="?nav=edit-wordpress-scrape&id='+data+'">'+data+'</a>';
                        },
                        "sWidth": "30px"
                    },
                    {
                        "mData": "urlName",
                        "mRender": function(data, type, full){
                            if (full.isValid){
                                return '<a href="<?php echo $CurrentServer;?>custompages/'+data+'" target="_blank">'+data+'</a>';
                            } else {
                                return '<span class="alert" title="Incomplete!">'+data+'</span>';
                            }
                        },
                        "sWidth": "150px"
                    },
                    { "mData": "pageTitle" },
                    { "mData": "pageHeader" },
                    { "mData": "urlToScrape" },
                    { "mData": "createdDate", "sWidth": "50px" },
                    { "mData": "updatedDate", "sWidth": "50px" }
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "action", "value": "list" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": lastRequestedCriteria,
                        "success": fnCallback
                    } );
                }
            });
    });
</script>