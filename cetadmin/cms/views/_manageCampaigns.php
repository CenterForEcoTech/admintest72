<h3>Manage Campaigns</h3>
<p>
    <a href="?nav=edit-campaign" id="add-campaign-button" class="button-link">+ Add Campaign</a>
</p>
<table id="campaign-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Snippet</th>
        <th>Image</th>
        <th>Cache</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var campaignListTable = $("#campaign-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiTagManagement.php",
            lastRequestedCriteria = null,
            oTable = campaignListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "iDisplayLength": 50,
                "aaSorting": [ [0, "desc"]],
                "aoColumns" :[
                    {
                        "mData": "id",
                        "mRender": function ( data, type, full ) {
                            return '<a href="?nav=edit-campaign&id='+data+'">'+data+'</a>';
                        },
                        "sWidth": "50px"
                    },
                    {
                        "mData": "name",
                        "sWidth": "150px"
                    },
                    {"mData": "snippet"},
                    {
                        "mData": "imageUrl",
                        "mRender": function ( data, type, full ) {
                            if (data){
                                return '<img src="'+data+'">';
                            }
                            return "";
                        },
                        "sClass": "shrink-to-60",
                        "sWidth": "200px"
                    },
                    {
                        "mData": "name",
                        "mRender": function(data, type, full){
                            return '<a target="_blank" href="<?php echo $CurrentServer;?>CampaignMap?tagName='+data+'&forceRefresh=1">Refresh</a>';
                        },
                        "sWidth":"50px"
                    }
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "action", "value": "list" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": lastRequestedCriteria,
                        "success": fnCallback
                    } );
                }
            });
    });
</script>