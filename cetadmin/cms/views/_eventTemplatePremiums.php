<?php
/* @var $template EventTemplate */
if (isset($template) && $template->id > 0){
    include_once($repositoryApiFolder."DataContracts.php");
    ?>
    <div style="clear:both;"></div>
    <style type="text/css">
        .required:after{color:red;content:"*";}
        .required{color:inherit;}
    </style>
    <h3>Premium Levels</h3>
    <p>
        <a href="#addPremium" id="add-premium-button" class="button-link do-not-navigate">+ Add Premium</a>
        <span class="help">Click on a row to edit.</span>
    </p>
    <table id="premium-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Sort Order</th>
            <th>Active?</th>
            <th>Default?</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <div class="hidden" id="edit-premium-form" title="Add/Edit Premium">
        <form>
            <h4>Add or Edit Premium</h4>
            <input type="hidden" name="action" value="post-premium">
            <input type="hidden" name="id" class="editable" value="0" data-default="0">
            <input type="hidden" name="eventTemplateId" value="<?php echo $template->id;?>">
            <label>
                <div class="required">Name:</div>
                <input type="text" title="Name" class="auto-watermarked editable" name="name">
            </label>
            <label>
                <div class="required">Price:</div>
                <input type="text" title="Price" class="auto-watermarked editable" name="price">
            </label>
            <div class="markdown-editor eight columns" >

                <label for="markdown-source">Editor</label>
                <div id="notes-button-bar" class="wmd-button-bar"></div>
                <textarea id="markdown-source" name="displayTextMarkdownSource" class="wmd-input editable" cols="92" rows="15"></textarea>

                <label for="html-source">Preview</label>
                <div id="notes-preview" class="wmd-preview"></div>
                <textarea id="html-source"  name="displayText" style="display:none;" class="editable"></textarea>
            </div>
            <label>
                <div>Sort Order:</div>
                <input type="text" title="Sort Order" class="auto-watermarked editable" name="sortOrder" value="0" data-default="0">
            </label>
            <label>
                <input type="checkbox" name="status" value="1" checked> Is Active?
            </label>
            <label title="Mark this item as the default selection when the user is first presented with these options.">
                <input type="checkbox" name="isDefault" value="1" > Is Default?
            </label>
        </form>
    </div>

    <script type="text/javascript">
        $(function(){
            var premiumDataTable = $("#premium-table"),
                apiUrl = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEventTemplates.php",
                oTable = premiumDataTable.dataTable({
                    "bJQueryUI": true,
                    "bServerSide":true,
                    "sAjaxSource": apiUrl,
                    "aaSorting": [ ],
                    "aoColumns" :[
                        { "mData": "name" },
                        { "mData": "price" },
                        { "mData": "sortOrder", "sWidth": "20px" },
                        { "mData": "status", "sWidth": "20px" },
                        { "mData": "isDefault", "sWidth": "20px" }
                    ],
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "templateID", "value": "<?php echo $template->id;?>" } );
                        aoData.push( { "name": "action", "value": "get_premium" } );
                    },
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        } );
                    }
                }),
                editPremiumDataDialog = $("#edit-premium-form"),
                editPremiumDataForm = editPremiumDataDialog.find("form"),
                clearFormData = function(){
                    editPremiumDataForm.find(".editable").each(function(i,item){
                        var element = $(item),
                            defaultValue = element.attr("data-default");
                        if (typeof defaultValue === "string" && defaultValue.length){
                            element.val(defaultValue);
                        } else {
                            element.val("");
                        }
                    });
                    editPremiumDataForm.find("input[name='status']").prop("checked", true);
                    editPremiumDataForm.find("input[name='isDefault']").prop("checked", false);
                    editPremiumDataForm.find("#notes-preview").html("");
                },
                editorField = $("#markdown-source");

            editorField.wmd({
                "helpLink": "http://daringfireball.net/projects/markdown/",
                "helpHoverTitle": "Markdown Help",
                "button_bar": "notes-button-bar",
                "preview": "notes-preview",
                "output": "html-source"
            });

            editPremiumDataDialog.dialog({
                autoOpen: false,
                height: 600,
                width:650,
                modal:true,
                buttons: {
                    "Save": function(){
                        $.ajax({
                            url: apiUrl,
                            type: "POST",
                            data: JSON.stringify(editPremiumDataForm.serializeObject()),
                            dataType: "json",
                            contentType: "application/json",
                            success: function(data){
                                if (data.message){
                                    alert(data.message);
                                }
                                oTable.fnReloadAjax();
                                clearFormData();
                                editPremiumDataDialog.dialog("close");
                            }
                        });
                    },
                    "Cancel": function(){
                        editPremiumDataDialog.dialog("close");
                    }
                },
                close: function(){
                    clearFormData();
                }
            });

            premiumDataTable.on("click", "tr", function(e){
                var row = $(this),
                    rowId = row.attr("id"),
                    dataId = rowId.replace("premium-", "");
                editPremiumDataForm.find("input[name='id']").val(dataId);
                $.ajax({
                    url: apiUrl,
                    type: "GET",
                    data: {
                        id: dataId,
                        action: "get_premium"
                    },
                    success: function(data){
                        editPremiumDataForm.find("input[name='id']").val(data.id);
                        editPremiumDataForm.find("input[name='name']").val(data.name);
                        editPremiumDataForm.find("input[name='price']").val(data.price);
                        editPremiumDataForm.find("input[name='sortOrder']").val(data.sortOrder);
                        editPremiumDataForm.find("textarea[name='displayText']").val(data.displayText);
                        editPremiumDataForm.find("textarea[name='displayTextMarkdownSource']").val(data.displayTextMarkdownSource);
                        editPremiumDataForm.find("input[name='status']").prop("checked", data.status);
                        editPremiumDataForm.find("input[name='isDefault']").prop("checked", data.isDefault);

                        editPremiumDataDialog.dialog("open");
                    }
                });
            });

            $("#add-premium-button").click(function(e){
                e.preventDefault();
                editPremiumDataDialog.dialog("open");
            });
        });
    </script>
<?php } // is relevant ?>