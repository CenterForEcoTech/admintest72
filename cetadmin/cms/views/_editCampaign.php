<?php
/* @var $campaignRecord CampaignRecord */
?>
<style type="text/css">
    .btn.hidden{display:none;}
    form label {font-weight:bold; font-size:16px;}
    textarea:disabled {background-color: lightgray;}
    .wmd-preview {
        padding-top:20px;
    }
    .wmd-preview ul{list-style-type: disc;}
    .wmd-preview ol{list-style-type: decimal;}
    .wmd-preview li{margin-bottom:0.7em;}
    .wmd-preview ol, .wmd-preview ul{
        margin-top:0px;
        margin-left:5px;
    }
    .wmd-preview ol:first-child, .wmd-preview ul:first-child{
        margin-left:-15px;
    }
    .wmd-preview p{
        margin:0 0 20px 0;
    }
</style>
<h2>Edit Campaign</h2>
<form id="fileupload" class="sixteen columns normal template-upload">
    <div class="sixteen columns alpha omega">
    <fieldset class="nine columns alpha omega">
        <legend>Basics</legend>
        <label title="TagList_Name">
            <div>
                Name:
                <a class="help-modal do-not-navigate" href="help-tag-name">Help on Tag Names</a>
            </div>
            <input type="text" name="name" title="Name: used as the URL" class="auto-watermarked" value="<?php echo $campaignRecord->name;?>">
            <div class="alert"></div>
        </label>
        <label title="TagList_Description">
            <div>
                Snippet:
            </div>
            <textarea name="snippet" title="Snippet: Text that will display for things tagged with this campaign." class="auto-watermarked wide"><?php echo $campaignRecord->snippet;?></textarea>
            <div class="alert"></div>
        </label>
        <label title="TagList_CampaignHeading">
            <div>
                Heading:
            </div>
            <input type="text" name="heading" title="Heading: used in the page header" class="auto-watermarked" value="<?php echo $campaignRecord->heading;?>">
            <div class="alert"></div>
        </label>
        <label title="TagList_CampaignSubHeading">
            <div>
                Sub Heading:
            </div>
            <input type="text" name="subHeading" title="SubHeading: used in the page header" class="auto-watermarked" value="<?php echo $campaignRecord->subHeading;?>">
            <div class="alert"></div>
        </label>
        <?php if (strlen($campaignRecord->descriptionMarkdownSource) == 0 && strlen($campaignRecord->getOldDescription()) > 0) {?>
        <label title="TagList_CampaignDescription">
            <div>
                Old Description Data:
            </div>
            <textarea name="description" title="READ ONLY: please use markdown fields!" class="auto-watermarked wide" disabled><?php echo $campaignRecord->getOldDescription();?></textarea>
            <div class="alert"></div>
        </label>
        <?php } ?>
    </fieldset>
    <?php if ($campaignRecord->name){?>
        <a href="<?php echo $CurrentServer."campaign/".$campaignRecord->name;?>" class="button-link" target="_blank" style="float:right;">View Published Campaign</a>
    <?php } ?>

        <fieldset class="six columns  omega">
            <legend>Call to Action</legend>
            <label>
                <div>Call to action text</div>
                <textarea name="callToAction" title="Text that goes in the orange box" style="width:100%;"><?php echo $campaignRecord->callToAction;?></textarea>
            </label>
        </fieldset>
        <fieldset class="six columns omega" style="background: #E5F9FF;font-size:15px;">
            <legend>VIDEO EMBED HELP</legend>
            Enclose the embedded iframe inside this div tag:
            <pre style="margin:0;font-size:13px;">
&lt;div class='video-container'>
  &lt;iframe ... >&lt;/iframe>
&lt;/div>
            </pre>
        </fieldset>
    </div>
	<fieldset class="sixteen columns alpha omega">
		<legend>What to Display</legend>
		<label>
			<span>Show Map:</span>
		</label>
			<input type="radio" name="showMap" value="WithBizCountAndPins"<?php if (!$campaignRecord->showMap || $campaignRecord->showMap=='WithBizCountAndPins'){echo " checked";}?>>With Biz Count and Pins &nbsp;&nbsp;<input type="radio" name="showMap" value="BlankMap"<?php if ($campaignRecord->showMap=='BlankMap'){echo " checked";}?>>Blank Map &nbsp;&nbsp;<input type="radio" name="showMap" value="NoMap"<?php if ($campaignRecord->showMap=='NoMap'){echo " checked";}?>>No Map
		<label>
			<span>Show Biz List:</span>
		</label>
			<input type="radio" name="showBizList" value="Yes"<?php if (!$campaignRecord->showBizList || $campaignRecord->showBizList=='Yes'){echo " checked";}?>>Yes &nbsp;&nbsp;<input type="radio" name="showBizList" value="No"<?php if ($campaignRecord->showBizList=='No'){echo " checked";}?>>No
		<label>
			<div>Not Showing Biz List Blurb:</div>
		</label>
			<textarea name="notShowingBizListBlurb" title="Text that displays if not showing biz list.  If left blank default from Site Configs will be used." style="width:100%;" class="auto-watermarked wide"><?php echo $campaignRecord->notShowingBizListBlurb;?></textarea>
	</fieldset>

    <fieldset class="sixteen columns alpha omega">
        <legend>Description</legend>
        <div class="markdown-editor fifteen columns alpha" >

            <label for="markdown-source">Editor</label>
            <div id="notes-button-bar" class="wmd-button-bar"></div>
            <textarea id="markdown-source" name="descriptionMarkdownSource" class="wmd-input fifteen columns alpha omega" cols="92" rows="15"><?php echo $campaignRecord->descriptionMarkdownSource;?></textarea>

            <label for="html-source">Preview</label>
            <div id="notes-preview" class="wmd-preview fifteen columns alpha omega"></div>
            <textarea id="html-source" name="descriptionHtmlSource" style="display:none;"></textarea>
        </div>
    </fieldset>

    <div class="sixteen columns alpha omega">
        <fieldset class="nine columns alpha omega">
            <legend>Image</legend>
            <button class="btn btn-danger delete do-not-navigate <?php echo ($campaignRecord->imageUrl) ? "" : "hidden";?>" >
                <span>Delete</span>
            </button>
            <div class="preview two columns">
                <span>&nbsp;<a href="<?php echo $campaignRecord->imageUrl;?>" target="campaign_image" title="click to view image"><img src="<?php echo $campaignRecord->imageUrl;?>" width="90"></a></span>
            </div>
            <div class="five columns">
                <div class="metadata hidden">
                    <label class="name"><strong>Name: </strong><span><?php echo $campaignRecord->imageFileName;?></span></label>
                    <label class="size"><strong>Size: </strong><span><span></label>
                    <label class="type"><strong>Type: </strong><span><span></label>
                    <label class="xy"><strong>Dimensions: </strong><span><span></label>
                </div>
                <div class="btn btn-success fileinput-button">
                    <span>Select file...</span>
                    <input type="file" name="files" multiple>
                </div>
            </div>
            <br>
            <div class="eight columns">
                <label title="TagList_AddWRap: Add grayish border around image to make it stand out?">
                    <input type="checkbox" name="addWrap" value="1" <?php echo $campaignRecord->addWrap ? "checked" : "";?>>
                    Add Wrap?
                </label>
                <label title="TagList_URL">
                    <div>URL: <span class="help">(optional)</span></div>
                    <input type="text" name="url" title="http://www.domain.com" class="auto-watermarked" value="<?php echo $campaignRecord->url;?>">
                </label>
                <label title="TagList_EventURLOverride">
                    <div>URL for Event Detail Pages: <span class="help">(optional)</span></div>
                    <input type="text" name="eventUrlOverride" title="http://www.domain.com" class="auto-watermarked" value="<?php echo $campaignRecord->eventUrlOverride;?>">
                </label>
            </div>
        </fieldset>
        <fieldset class="six columns">
            <legend>Map Defaults</legend>
            <label title="TagList_MapZoomLevel">
                <div>Zoom Level:</div>
                <input type="text" name="mapZoomLevel" pattern="\d*" value="<?php echo $campaignRecord->mapZoomLevel;?>">
            </label>
            <label title="TagList_MapCenterLat">
                <div>Center Latitude:</div>
                <input type="text" name="mapCenterLat" pattern="(\d{1,3})([\.])(\d{1,8})" value="<?php echo $campaignRecord->mapCenterLat;?>">
            </label>
            <label title="TagList_MapCenterLong">
                <div>Center Longitude:</div>
                <input type="text" name="mapCenterLong" pattern="[\-]?(\d{1,3})([\.])(\d{1,8})" value="<?php echo $campaignRecord->mapCenterLong;?>">
            </label>
            <a href="<?php echo $CurrentServer."CampaignMap?tagName=".$campaignRecord->name."&forceRefresh=1";?>" class="button-link" target="_blank">Refresh Map Cache</a>
        </fieldset>
    </div>
    <fieldset class="fifteen columns alpha">
        <legend>Poster</legend>
        <label title="This is the image that the user sees on the campaign page that motivates them to click it to download the poster.">
            <div>Image URL:</div>
            <input class="wide" type="text" name="posterImageUrl" value="<?php echo $campaignRecord->posterImageUrl;?>">
        </label>
        <label title="This is the actual poster that is linked to from the image (and opened in a new window). It can be the same as the image.">
            <div>Poster URL:</div>
            <input class="wide" type="text" name="posterUrl" value="<?php echo $campaignRecord->posterUrl;?>">
        </label>
    </fieldset>
    <fieldset class="fifteen columns alpha">
        <input type="hidden" name="delete_image" value="">
        <input type="hidden" name="id" value="<?php echo $campaignRecord->id;?>">
        <input type="hidden" name="imageFileName" value="<?php echo $campaignRecord->imageFileName;?>">
        <input type="hidden" name="action" value="save-campaign-data">
        <input type="submit" name="submit" value="Save" class="button-link">

        <?php if ($campaignRecord->id){?>
        <a href="clone-campaign" id="clone-campaign" data-id="<?php echo $campaignRecord->id;?>" class="button-link do-not-navigate ui-state-highlight" title="Copy this campaign">Clone</a>
        <?php } ?>
    </fieldset>
</form>
<div class="hidden" id="clone-campaign-form" title="Clone Campaign">
    <p>Please select the options to include with this process.</p>
    <label>
        <input type="checkbox" name="cloneReferences" checked>
        <span>Include Reference Links</span>
    </label>
    <br>
    <label>
        <input type="checkbox" name="cloneSocialMessages" >
        <span>Include Sample Social Messages</span>
    </label>
    <input type="hidden" name="campaign_id" value="<?php echo $campaignRecord->id;?>">
</div>
<div style="display:none;">
    <div id="help-tag-name" title="Tag Names">
        <p>Business Rule: Tag names cannot contain hyphens.</p>
        <p>If you add a hyphen to a name here, it will be removed before it is saved to the database.</p>
        <p>Nevertheless, in a URL, if you replace spaces with hyphens or "%20" (urlencoding for space), then the campaign will display as expected.</p>
    </div>
</div>
<div id="dialog-confirm-delete" title="Delete this image?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this Image?</p>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
</script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>load-image.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script type="text/javascript">
    $(function () {
        'use strict';
        var uploadForm = $('#fileupload'),
            previewNode = uploadForm.find(".preview span"),
            posterImageField = uploadForm.find("input[name='posterImageUrl']"),
            posterField = uploadForm.find("input[name='posterUrl']"),
            deleteImage = function(){
                $("form input[name='delete_image']").val("yes");
                $("form input[type='submit']").addClass("btn-danger");
            },
            undeleteImage = function(){
                $("form input[name='delete_image']").val("");
                $("form input[type='submit']").removeClass("btn-danger");
            },
            renderPreview = function (file) {
                var node = previewNode,
                    metadataContainer = uploadForm.find(".metadata"),
                    nameNode = metadataContainer.find("label.name span"),
                    typeNode = metadataContainer.find("label.type span"),
                    sizeNode = metadataContainer.find("label.size span"),
                    dimensionNode = metadataContainer.find("label.xy span");
                return (loadImage && loadImage(
                    file,
                    function (img) {
                        node.html(img);
                        nameNode.html(file.name);
                        typeNode.html(file.type);
                        sizeNode.html(file.size);
                        dimensionNode.html("Pending . . .").addClass("warning");
                        metadataContainer.show();
                        undeleteImage();
                        $("form input[type='submit']").addClass("btn-success");
                    },
                    {
                        maxWidth: 80,
                        maxHeight: 80,
                        canvas: true
                    }
                ));
            },
            dataObject;

        uploadForm.on("submit", function (e) {
            e.preventDefault();
            var button = $(e.currentTarget),
                data = dataObject;
            if (data && data.submit && !data.jqXHR && data.submit().success(function (result, textStatus, jqXHR) {
                location.href = "?nav=edit-campaign&id=" + result.record.id;
            })) {
                button.prop('disabled', true);
            } else {
                // do a standard submit
                $.ajax({
                    url: "ApiTagManagement.php",
                    type: "POST",
                    data: JSON.stringify(uploadForm.serializeObject()),
                    dataType: "json",
                    contentType: "application/json",
                    statusCode:{
                        200: function(data){
                            location.href = "?nav=edit-campaign&id=" + data.record.id;
                        },
                        409: function(jqXHR, textStatus, errorThrown){
                            var data = $.parseJSON(jqXHR.responseText),
                                record = data.record;
                            if (data.message){
                                alert(data.message);
                            } else {
                                alert("Unexpected condition encountered. Please refresh the page and try again.")
                            }
                        }
                    }
                });
            }
        });

        uploadForm.on("click", "button.delete", function(e){
            deleteImage();
            $(this).hide();
        });

        // Initialize the jQuery File Upload widget:
        uploadForm.fileupload({
            url: 'ApiTagManagement.php',
            uploadTemplateId: null,
            change: function (e, data) {
                $.each(data.files, function (index, file) {
                    renderPreview(file);
                });
            },
            add: function (e, data) {
                dataObject = data;
            }
        });

        uploadForm.bind('fileuploadsubmit', function (e, data) {
            data.formData = uploadForm.serializeArray();
        });

        var cloneForm = $("#clone-campaign-form"),
            campaignIdField = cloneForm.find("input[name='campaign_id']"),
            includeReferencesCheckbox = cloneForm.find("input[name='cloneReferences']"),
            includeSocialCheckbox = cloneForm.find("input[name='cloneSocialMessages']");

        uploadForm.on("click", "#clone-campaign", function(e){
            e.preventDefault();
            cloneForm.dialog("open");
        });

        cloneForm.dialog({
            autoOpen: false,
            height: 300,
            width:450,
            modal:true,
            buttons: {
                "Save": function(){
                    $.ajax({
                        url: "ApiTagManagement.php",
                        type: "POST",
                        data: {
                            "action":"clone-campaign",
                            "campaign_id":campaignIdField.val(),
                            "cloneReferences":includeReferencesCheckbox.is(":checked") ? 1 : 0,
                            "cloneSocialMessages":includeSocialCheckbox.is(":checked") ? 1 : 0
                        },
                        success: function(data){
                            if (data.id){
                                window.location.href = "<?php echo $CurrentServer.$adminFolder;?>cms/?nav=edit-campaign&id=" + data.id;
                            } else {
                                alert("unexpected result from clone operation");
                            }
                            cloneForm.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    cloneForm.dialog("close");
                }
            },
            close: function(){
                includeReferencesCheckbox.prop("checked", true);
                includeSocialCheckbox.prop("checked", false);
            }
        });

        $("#markdown-source").wmd({
            "helpLink": "http://daringfireball.net/projects/markdown/",
            "helpHoverTitle": "Markdown Help",
            "button_bar": "notes-button-bar",
            "preview": "notes-preview",
            "output": "html-source"
        });

        posterImageField.autocomplete({
            delay:100,
            minLength: 0,
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $CurrentServer;?>mayor/cms/ApiImageLibrary.php",
                    data: {
                        action: "keyword_search",
                        keyword: request.term
                    },
                    success: function(data) {
                        response($.map(data.collection, function(el, index) {
                            return {
                                value: el.largeImageFullPath,
                                id: el.id,
                                url: el.imageFullPath,
                                label: el.title
                            };
                        }));
                    }
                });
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item){
            return $("<li />")
                .data("item.autocomplete", item)
                .append("<a><img src='"+ item.url +"' class='small-logo-image'/> "+ item.label +"</a>")
                .appendTo(ul);
        };

        posterField.autocomplete({
            delay:100,
            minLength: 0,
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $CurrentServer;?>mayor/cms/ApiFileLibrary.php",
                    data: {
                        action: "keyword_search",
                        keyword: request.term
                    },
                    success: function(data) {
                        response($.map(data.collection, function(el, index) {
                            return {
                                value: el.fullPath,
                                id: el.id,
                                url: el.fullPath,
                                label: el.title,
                                fileType: el.fileType
                            };
                        }));
                    }
                });
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item){
            if (/^image\/.*$/i.test(item.fileType)) {
                return $("<li />")
                    .data("item.autocomplete", item)
                    .append("<a><img src='"+ item.url +"' class='small-logo-image'/> "+ item.label +"</a>")
                    .appendTo(ul);
            } else if (/^application\/pdf$/i.test(item.fileType)){
                return $("<li />")
                    .data("item.autocomplete", item)
                    .append("<a><img src='<?php echo $CurrentServer;?>images/file_pdf.png' class='small-logo-image'/> "+ item.label +"</a>")
                    .appendTo(ul);
            } else {
                return $("<li />")
                    .data("item.autocomplete", item)
                    .append("<a><img src='<?php echo $CurrentServer;?>images/document.png' class='small-logo-image'/> "+ item.label +"</a>")
                    .appendTo(ul);
            }
        };
    });
</script>
<?php
if ($campaignRecord->id){
    include($rootFolder.$adminFolder."cms/views/_tagReferencesList.php");

    $relevantEntity = SocialMessageSample::CAMPAIGN_ENTITY;
    $relevantEntityId = $campaignRecord->id;
    include($rootFolder.$adminFolder."cms/views/_socialMessagesList.php");

    $record = $campaignRecord;
    include($rootFolder.$adminFolder."cms/views/_campaignOrgsList.php");
}
?>