<?php
include_once($dbProviderFolder."EventTemplateProvider.php");
if (!isset($eventTemplateProvider)){
    $eventTemplateProvider = new EventTemplateProvider($mysqli);
}
$templates = $eventTemplateProvider->getAll();
if (!isset($Config_DefaultCreateEventTemplate)){
    $Config_DefaultCreateEventTemplate = 0;
}
?>

<div class="sixteen columns">
    <h3>Manage Event Templates</h3>

    <p>
        <a href="?nav=edit-event-template" id="add-new" class="button-link">Add New</a>
        ( <span class="make-default-link">&nbsp;</span> Click this to make a template the default template)
        ( <span class="is-default">&nbsp;</span> This is the default template)
    </p>
    <div>
        <table id="event-templates-table" class="datatables" style="display:none;">
            <thead>
            <tr id="search-inputs">
                <td class="ui-state-default"><input class="auto-watermarked" title="Name"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="URL"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Event Name"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Start"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="End"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="%"></td>
                <td class="ui-state-default"><input class="auto-watermarked" title="Tags"></td>
                <td class="ui-state-default">&nbsp;</td>
            </tr>
            <tr>
                <th>Name</th>
                <th>URL</th>
                <th>Event</th>
                <th>Start</th>
                <th>End</th>
                <th>%</th>
                <th>Tags</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($templates as $record){
                $startDisplay = ($record->startDate) ? date("n/j/Y", strtotime($record->startDate)) : "--/--/--";
                $startDisplay .= "<br>". (($record->startTime) ? date("g:i a", strtotime($record->startTime)) : "--:--");
                $endDisplay = ($record->endDate) ? date("n/j/Y", strtotime($record->endDate)) : "--/--/--";
                $endDisplay .= "<br>". (($record->endTime) ? date("g:i a", strtotime($record->endTime)) : "--:--");
                $defaultIndicatorClass = ($record->id == $Config_DefaultCreateEventTemplate) ? "is-default" : "make-default-link";
                ?>
            <tr data-id="<?php echo $record->url;?>"  title="<?php echo htmlentities($record->description, ENT_QUOTES);?>">
                <td>
                    <a href="make-default" class="<?php echo $defaultIndicatorClass;?> do-not-navigate" title="Make this the default template for create event" data-id="<?php echo $record->id;?>">Make Default</a>
                    <a href="?nav=edit-event-template&id=<?php echo $record->id;?>"><?php echo $record->name;?></a>
                </td>
                <td><?php echo $record->url;?></td>
                <td><?php echo $record->eventName;?></td>
                <td><?php echo $startDisplay;?></td>
                <td><?php echo $endDisplay;?></td>
                <td><?php echo $record->percentageID;?></td>
                <td><?php echo implode(", ", $record->tagNamesArray);?></td>
                <td><a href="delete-template" class="delete-link do-not-navigate" title="delete">Delete</a></td>
            </tr>
                <?php } // end foreach?>
            </tbody>
        </table>
    </div>
</div>
<div id="dialog-confirm-delete" title="Delete this template?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this template?</p>
</div>
<div id="dialog-make-default" title="Make this the default template?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to make this the default template?</p>
</div>
<script type="text/javascript">
    $(function(){
        var tableElement = $("#event-templates-table"),
            asInitVals = new Array(),
            searchInputs = $("#search-inputs input"),
            oTable = tableElement.dataTable({
                "bJQueryUI": true,
                "iDisplayLength":100
            });

        searchInputs.keyup( function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter( this.value, searchInputs.index(this) );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        tableElement.on("click", ".delete-link", function(){
            alert("Not yet implemented!");
            return;
            // TODO: implement delete on the back end??
            var clicked = $(this),
                tr = clicked.closest("tr"),
                templateID = tr.attr("data-id"),
                allRows = tableElement.fnGetNodes(),
                selectedNode;

            for ( var i=0 ; i<allRows.length ; i++ )
            {
                if ( $(allRows[i]).attr('data-id') == templateID )
                {
                    selectedNode = allRows[i];
                }
            }
            if (selectedNode){
                $( "#dialog-confirm-delete" ).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Delete": function() {
                            var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEventTemplates.php",
                                data = {
                                    id: templateID
                                };

                            // delete
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                data: JSON.stringify(data),
                                dataType: "json",
                                contentType: "application/json",
                                statusCode:{
                                    200: function(data){
                                        tableElement.fnDeleteRow(selectedNode);
                                    },
                                    409: function(jqXHR, textStatus, errorThrown){
                                        var data = $.parseJSON(jqXHR.responseText);
                                        if (data.message){
                                            alert(data.message);
                                        } else {
                                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                                        }
                                    }
                                }
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            }
        });

        tableElement.on("click", ".make-default-link", function(){
            var clicked = $(this),
                templateID = clicked.attr("data-id");
            $( "#dialog-make-default" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Make Default": function() {
                        var url = "<?php echo $CurrentServer.$adminFolder;?>cms/ApiEventTemplates.php",
                            data = {
                                id: templateID,
                                action: 'make-default'
                            };

                        // delete
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: JSON.stringify(data),
                            dataType: "json",
                            contentType: "application/json",
                            statusCode:{
                                200: function(data){
                                    tableElement.find(".is-default").removeClass("is-default").addClass("make-default-link");
                                    clicked.removeClass("make-default-link").addClass("is-default");
                                },
                                409: function(jqXHR, textStatus, errorThrown){
                                    var data = $.parseJSON(jqXHR.responseText);
                                    if (data.message){
                                        alert(data.message);
                                    } else {
                                        alert("Unexpected condition encountered. Please refresh the page and try again.")
                                    }
                                }
                            }
                        });
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });
        });

        tableElement.show();
    });
</script>
