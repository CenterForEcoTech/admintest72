<?php

include_once("../_config.php");
include_once("_initImageManager.php"); // initializes imageManager, imageDataProvider, & options

$id = $_GET['id'];
if ($imageManager && $id){
    $imageResults = $imageDataProvider->getImages($id);
    $imageRecord = $imageResults[0];
    $file = $imageManager->getMetadata($imageRecord->file_name);
    //var_dump($imageRecord);
    //var_dump($file);
?>
<h3>Edit Image</h3>
<form id="fileupload" action="ApiImageLibrary.php" method="POST" enctype="multipart/form-data" class="sixteen columns normal template-upload">
    <button class="btn btn-danger delete do-not-navigate" >
        <span>Delete</span>
    </button>
    <input type="hidden" name="id[]" value="<?php echo $imageRecord->id?>">
    <div class="preview two columns">
        <span><a href="<?php echo $file->url;?>" target="image-preview" title="Click to view original image"><img src="<?php echo $file->thumbnail_url;?>"></a></span>
    </div>
    <div class="eleven columns">
        <div class="metadata">
            <label class="name"><strong>Name: </strong><span><?php echo $file->name;?></span></label>
            <label class="size"><strong>Size: </strong><span><?php echo $file->size;?><span></label>
            <label class="type"><strong>Type: </strong><span><?php echo $imageRecord->file_type;?><span></label>
            <label class="xy"><strong>Dimensions: </strong><span><?php echo $imageRecord->width;?> x <?php echo $imageRecord->height;?><span></label>
        </div>
        <div class="btn btn-success fileinput-button">
            <span>Select file...</span>
            <input type="file" name="files[]" multiple>
        </div>
    </div>
    <br>
    <div class="sixteen columns">
    <label>
        <div>*Title:</div>
        <input type="text" id="title" name="title[]" required value="<?php echo $imageRecord->title;?>">
    </label>
    <label>
        <div>Description:</div>
        <textarea name="description[]"><?php echo $imageRecord->description;?></textarea>
    </label>

    <label>
        <div>Keywords:</div>
        <input type="text" name="keywords[]" value="<?php echo $imageRecord->keywords?>">
    </label>
        <button type="submit" class="btn btn-primary start">
            <span>Save</span>
        </button>
        <button class="btn btn-warning cancel do-not-navigate">
            <span>Cancel</span>
        </button>
    </div>
</form>
<div id="dialog-confirm-delete" title="Delete this image?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this Image?</p>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
</script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>load-image.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script type="text/javascript">
    $(function () {
        'use strict';
        var uploadForm = $('#fileupload'),
            previewNode = uploadForm.find(".preview span"),
            renderPreview = function (file) {
                var node = previewNode,
                    metadataContainer = uploadForm.find(".metadata"),
                    nameNode = metadataContainer.find("label.name span"),
                    typeNode = metadataContainer.find("label.type span"),
                    sizeNode = metadataContainer.find("label.size span"),
                    dimensionNode = metadataContainer.find("label.xy span");
                return (loadImage && loadImage(
                    file,
                    function (img) {
                        node.html(img);
                        nameNode.html(file.name);
                        typeNode.html(file.type);
                        sizeNode.html(file.size);
                        dimensionNode.html("Pending . . .").addClass("warning");
                    },
                    {
                        maxWidth: 80,
                        maxHeight: 80,
                        canvas: true
                    }
                ));
            },
            dataObject;

        uploadForm.on("submit", function (e) {
            e.preventDefault();
            var button = $(e.currentTarget),
                data = dataObject;
            if (data && data.submit && !data.jqXHR && data.submit().success(function (result, textStatus, jqXHR) {window.location.reload()})) {
                button.prop('disabled', true);
            } else {
                // do a standard submit
                $.ajax({
                    url: "ApiImageLibrary.php",
                    type: "POST",
                    data: JSON.stringify(uploadForm.serializeObject()),
                    dataType: "json",
                    contentType: "application/json",
                    statusCode:{
                        200: function(data){
                            window.location.reload();
                        },
                        409: function(jqXHR, textStatus, errorThrown){
                            var data = $.parseJSON(jqXHR.responseText),
                                record = data.record;
                            if (data.message){
                                alert(data.message);
                            } else {
                                alert("Unexpected condition encountered. Please refresh the page and try again.")
                            }
                        }
                    }
                });
            }
        });

        uploadForm.on("click", "button.cancel", function(e){
            e.preventDefault();
            window.location = "?nav=image-library";
        });

        uploadForm.on("click", "button.delete", function(e){
            $( "#dialog-confirm-delete" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Delete": function() {
                        var url = "ApiImageLibrary.php?file=<?php echo urlencode($file->name);?>";

                        // delete
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            contentType: "application/json",
                            statusCode:{
                                200: function(data){
                                    alert("Image has been deleted. The page will be refreshed");
                                    window.location = "?nav=image-library";
                                },
                                409: function(jqXHR, textStatus, errorThrown){
                                    var data = $.parseJSON(jqXHR.responseText);
                                    if (data.message){
                                        alert(data.message);
                                    } else {
                                        alert("Unexpected condition encountered. Please refresh the page and try again.")
                                    }
                                }
                            }
                        });
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });
        });

        // Initialize the jQuery File Upload widget:
        uploadForm.fileupload({
            url: 'ApiImageLibrary.php',
            dropZone: uploadForm,
            pasteZone: uploadForm,
            uploadTemplateId: null,
            drop: function (e, data) {
                $.each(data.files, function (index, file) {
                    renderPreview(file);
                });
            },
            change: function (e, data) {
                $.each(data.files, function (index, file) {
                    renderPreview(file);
                });
            },
            add: function (e, data) {
                dataObject = data;
            }
        });

        uploadForm.bind('fileuploadsubmit', function (e, data) {
            data.formData = uploadForm.serializeArray();
        });

    });
</script>
<?php
}
?>