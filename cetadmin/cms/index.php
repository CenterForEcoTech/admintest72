<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");
$pageTitle = "Admin - CMS";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
$fileuploadpage=false;

if ($nav){
    switch ($nav){
        case "image-library":
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $displayPage = "_manageImageLibrary.php";
            break;
        case "edit-image":
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $displayPage = "_editImage.php";
            break;
        case "manage-catalogs":
            $bodyCssClass = "bootstrap";
            $displayPage = "views/_manageCatalogs.php";
            break;
        case "edit-catalog":
            $modelTemplate = "model/_editCatalogModel.php";
            $displayPage = "views/_editCatalog.php";
            break;
        case "manage-email-templates":
            $displayPage = "_manageEmailTemplates.php";
            break;
        case "edit-email-template":
            $displayPage = "_editEmailTemplate.php";
            break;
        case "manage-event-templates":
            $displayPage = "_manageEventTemplates.php";
            break;
        case "edit-event-template":
            $modelTemplate = "model/_editEventTemplateModel.php";
            $displayPage = "views/_editEventTemplate.php";
            break;
        case "manage-campaigns":
            $displayPage = "views/_manageCampaigns.php";
            break;
        case "edit-campaign":
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $modelTemplate = "model/_editCampaignModel.php";
            $displayPage = "views/_editCampaign.php";
            break;
        case "edit-event-info":
            $displayPage = "_editEventInfo.php";
            break;
        case "sample-social":
            $displayPage = "views/_socialMessagesList.php";
            break;
        case "file-library":
            $displayPage = "views/_manageUploadedFiles.php";
            break;
        case "edit-file":
			$fileuploadpage=true;
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $modelTemplate = "model/_editFileModel.php";
            $displayPage = "views/_editFile.php";
            break;
        case "wordpress-scrapes":
            $displayPage = "views/_manageWordpressScrapes.php";
            break;
        case "edit-wordpress-scrape":
            $modelTemplate = "model/_editWpScrapeModel.php";
            $displayPage = "views/_editWpScrape.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>CMS Administration</h1>';

include_once("_cmsMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>