<?php

include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
include_once("model/_fileLibraryTableRow.php");
include_once("_initFileManager.php"); // initializes imageManager, imageDataProvider, & options
$currentRequest = new Request();
//$currentRequest->verb = "POST";
//print_pre($currentRequest);
//error_reporting(1); ini_set("display_errors", 1);
$results = new stdClass();

function getFileUrl(MediaRecord $object, $CurrentServer, $siteRoot){
    $folderUrl = rtrim($object->folderUrl, '/') . '/';
    $fileName = $object->fileName;
    if ($folderUrl && $fileName){
        $fileUrls = array(
            $folderUrl.$fileName
        );
        if (strpos($folderUrl, $CurrentServer) === false){
            // folder url points somewhere else; look on current server as well
            // making the assumption that all image library files are in the /media/ path
            $currentServerUrl = preg_replace("/(.*)(\/)(media\/.*)/", $CurrentServer."$3", $folderUrl);
            $fileUrls[] = $currentServerUrl.$fileName;
        }
        foreach($fileUrls as $fileUrl){
            $filePath = str_replace($CurrentServer, $siteRoot, $fileUrl);
            if (file_exists($filePath)){
                return $fileUrl;
                break;
            }
        }
    }
    return "";
}

if ($fileManager){
    switch(strtoupper($currentRequest->verb)){
        case "DELETE":
            $file_name = $_GET['file'];
            $results = $fileManager->deleteFile($file_name);
            break;
        case "GET":
            if ($_GET["action"] == "keyword_search" && $_GET["keyword"]){
                $criteria = new PaginationCriteria();
                $criteria->keyword = $_GET["keyword"];
                $criteria->size = 0; // for now, get all records
                $result = $fileDataProvider->get($criteria);
                foreach($result->collection as $mediaRecord){
                    $mediaRecord->fullPath = getFileUrl($mediaRecord, $CurrentServer, $siteRoot);
                }
                $results = $result;
            } else {
                $criteria = FileLibraryTableRow::getCriteria($_GET);
                $paginationResult = $fileDataProvider->get($criteria);
                $resultArray = $paginationResult->collection;

                $results = array(
                    "iTotalRecords" => $paginationResult->totalRecords,
                    "iTotalDisplayRecords" => $paginationResult->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($resultArray as $record){
                    $results["aaData"][] = new FileLibraryTableRow($record);
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
            break;
        case "POST":
            if (empty($_POST)){
                $newRecord = (array)json_decode($currentRequest->rawData);
                $id = $newRecord['id[]'];
                $response = $fileDataProvider->updateUserData($id, $newRecord['title[]'], $newRecord['description[]'], $newRecord['keywords[]']);
                if (!$response->success){
                    $results->message = $response->error;
                    header("HTTP/1.0 409 Conflict");
                } else {
                    $files = $fileDataProvider->getFiles($id);

                    foreach($files as $fileRow){
                        $file = $options->getMetadata($fileRow->file_name);
                        $file->id = $fileRow->id;
                        $file->title = $fileRow->title;
                        $file->description = $fileRow->description;
                        $file->keywords = $fileRow->keywords;
                        $file->delete_type = 'DELETE';
                        $file->delete_url = "ApiFileLibrary.php?file=".rawurlencode($file->name);
                        $result[] = $file;
                    }
                    $results = $result;
                }
            } else {
                // this includes at least one uploaded file
                $postedFilesArray = JqueryFileUploadConverter::convertToMediaLibraryFile($_SERVER, $_FILES, $_POST, "files", "title");
                $result = array();
                foreach ($postedFilesArray as $pendingFile){
                    $file = $fileManager->upload($pendingFile);

                    if ($file->name){
                        foreach ($pendingFile->formValuesArray as $key => $value){
                            $file->{$key} = $value;
                        }
                        $file->delete_type = 'DELETE';
                        $file->delete_url = "ApiIFileLibrary.php?file=".rawurlencode($file->name);
                    }
                    $result[] = $file;
                }
                $results = $result;
            }
            break;
        case "PUT":
            $results->message = "PUT is not supported at this time";
            break;
    }
} else {
    $results->message = "Access Denied";
}
output_json($results);
die();
