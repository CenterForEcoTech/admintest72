<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
include_once("model/_catalogTableRow.php");
include_once("model/_catalogMerchantTableRow.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."MerchantCatalogProvider.php");
include_once($dbProviderFolder."CampaignProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        if ($_GET['action'] == "list"){
            $provider = new MerchantCatalogProvider($mysqli);
            $criteria = CatalogTableRow::getCriteria($_GET);
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new CatalogTableRow($record, $campaignImageUrlPath);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        } else if ($_GET['action'] == 'get_merchants' && $_GET['catalog_id'] > 0){
            $catalogId = $_GET['catalog_id'];
            $campaignResourceProvider = new MerchantCatalogMerchantProvider($mysqli, $catalogId);
            $criteria = CatalogMerchantTableRow::getCriteria($_GET);
            $paginationResult = $campaignResourceProvider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new CatalogMerchantTableRow($record);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        } else if ($_GET['action'] == 'find_merchants'){
            if ($_GET['filter'] == 'campaign' && $_GET['campaign_id']){
                $provider = new MerchantCatalogMerchantFinder($mysqli);
                $results = $provider->findMerchantsByCampaign($_GET['campaign_id'], $_GET['catalog_id']);
            }
        } else if ($_GET['action'] == 'get_campaigns'){
            $provider = new CampaignProvider($mysqli);
            $criteria = new CampaignCriteria();
            $criteria->hasEvents = true;
            $criteria->size = 0;
            $pagedResult = $provider->get($criteria);
            $results = $pagedResult->collection;
        }
        break;
    case "POST":
        $newRecord = $currentRequest->getAsRecord();
        if ($newRecord->action == 'save_catalog'){
            $record = new MerchantCatalogRecord();
            $record->id = $newRecord->id;
            $record->name = $newRecord->name;
            $record->title = $newRecord->title;
            $record->subtitle = $newRecord->subtitle;
            $record->description = $newRecord->description;
            $record->status = $newRecord->status;

            $provider = new MerchantCatalogProvider($mysqli);
            $response = $provider->put($record, getAdminId());
            if ($response->success){
                $results->record = $record;
                if ($response->insertedId){
                    $results->record->id = $response->insertedId;
                }
            } else {
                $results->message = $response->error;
            }
        } else if ($newRecord->action == 'add_merchants' && is_array($newRecord->merchantIds)){
            if (count($newRecord->merchantIds)){
                $provider = new MerchantCatalogProvider($mysqli);
                $criteria = new MerchantCatalogCriteria();
                $criteria->id = $newRecord->id;
                $response = $provider->get($criteria);
                $record = $response->collection[0];
                $recordsAdded = 0;
                foreach ($newRecord->merchantIds as $merchantId){
                    $response = $provider->addMerchant($record, $merchantId, getAdminId());
                    if ($response->success && $response->affectedRows){
                        $recordsAdded++;
                    }
                }
                $results->numAdded = $recordsAdded;
            } else {
                $results->message = "No merchants added";
                header("HTTP/1.0 400 Bad Request");
            }
        } else if ($newRecord->action == 'remove_merchant' && $newRecord->merchantId){
            $provider = new MerchantCatalogProvider($mysqli);
            $criteria = new MerchantCatalogCriteria();
            $criteria->id = $newRecord->id;
            $response = $provider->get($criteria);
            $record = $response->collection[0];
            $response = $provider->removeMerchant($record, $newRecord->merchantId, getAdminId());
            if ($response->success && $response->affectedRows){
                $results->success = true;
            } else {
                $results->message = "Merchant was not removed;";
            }
        } else {
            $results->message = "Not understood";
            header("HTTP/1.0 400 Bad Request");
        }
        break;
    case "PUT":
        break;
}
output_json($results);
die();
?>