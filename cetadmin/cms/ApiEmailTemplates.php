<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
//error_reporting(1);
//ini_set("display_errors","on");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."ContentProvider.php");
$provider = new AdminEmailTemplateProvider($mysqli);

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $newRecord = json_decode($currentRequest->rawData);
        $response = $provider->delete($newRecord->id);

        if ($response->success){
            $results = $response;
            header("HTTP/1.0 200 Success");
        } else {
            $results = $response;
            header("HTTP/1.0 409 Conflict");
        }
        break;
    case "GET":
        echo "get is not supported";
        break;
    case "POST":
        // Test email
        $newRecord = json_decode($currentRequest->rawData);
        $emailTemplate = new EmailTemplate($newRecord->template);
        $context = $emailTemplate->getSampleContext();
        $body = $emailTemplate->merge($context);
        $recipient = $newRecord->recipient;

        include_once($dbProviderFolder."MySqliEmailLogger.php");
        include_once($repositoryApiFolder."EmailHelper.php");
        $emailMessage = new EmailMessage();
        $emailMessage->addAddress($newRecord->recipient, "");
        $emailMessage->setFrom($SMTPFromEmail, $SMTPFromEmailName."");
        $emailMessage->Subject = "Testing Template: ".$emailTemplate->name;
        $emailMessage->setHtmlBody($body);

        $throwErrors = true;
        $emailLogger = new MySqliEmailLogger($mysqli);
        $emailProvider = new EmailProvider($emailLogger, $SMTPEmailhost, $SMTPEmailport, $SMTPEmailusername, $SMTPEmailpassword, $throwErrors);
        $emailResponse = $emailProvider->smtpSend($emailMessage, $isDevMode_Email);

        if ($emailResponse->emailSent || $isDevMode_Email){
            $results = $emailResponse;
            header("HTTP/1.0 200 Success");
        }
        if ($emailResponse->message){
            $results = $emailResponse;
            header("HTTP/1.0 409 Conflict");
        }
        break;
    case "PUT":
        // add
        $newRecord = json_decode($currentRequest->rawData);
        $emailTemplate = new EmailTemplate($newRecord);
        $response = $provider->save($emailTemplate, getAdminId());

        if ($response->success){
            $results = $response->record;
            header("HTTP/1.0 201 Created");
        } else {
            $results = $response;
            header("HTTP/1.0 409 Conflict");
        }
        break;
}
output_json($results);
die();
?>