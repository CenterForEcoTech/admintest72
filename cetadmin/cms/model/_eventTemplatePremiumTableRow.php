<?php

class EventTemplatePremiumTableRow{
    public $DT_RowId;
    public $id;
    public $name;
    public $price;
    public $sortOrder;
    public $displayText;
    public $displayTextMarkdownSource;
    public $status;
    public $isDefault;

    public function __construct(EventTemplatePremium $record){
        $this->DT_RowId = "premium-".$record->id;
        $this->id = $record->id;
        $this->name = $record->name;
        $this->price = $record->price;
        $this->sortOrder = $record->sortOrder;
        $this->displayText = $record->displayText;
        $this->displayTextMarkdownSource = $record->displayTextMarkdownSource;
        $this->status = $record->isActive;
        $this->isDefault = $record->isDefault ? "Y" : "";
    }

    private static $columnMapping = array(
        0 => "name",
        1 => "price",
        2 => "sortOrder",
        3 => "status",
        4 => "default"
    );

    public static function getCriteria($datatablePost){
        $criteria = new EventTemplatePremiumCriteria();
        if ($datatablePost['templateID']){
            $criteria->templateID = $datatablePost['templateID'];
        }
        if ($datatablePost['id']){
            $criteria->id = $datatablePost['id'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}