<?php

class SocialMessageTableRow{
    public $DT_RowId;
    public $id;
    public $type;
    public $message;
    public $sortOrder;
    public $status;

    public function __construct(SocialMessageSample $record){
        $this->DT_RowId = "social-".$record->id;
        $this->id = $record->id;
        $this->type = $record->type;
        $this->message = $record->message;
        $this->sortOrder = $record->sortOrder;
        $this->status = $record->status;
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "type",
        2 => "message",
        3 => "sortOrder",
        4 => "status"
    );

    public static function getCriteria($datatablePost){
        $criteria = new SocialMessageCriteria();
        if (isset($datatablePost['id'])){
            $criteria->id = $datatablePost['id'];
        }
        if (isset($datatablePost['relevantEntity'])){
            $criteria->relevantEntity = $datatablePost['relevantEntity'];
        } else {
            $criteria->relevantEntity = SocialMessageSample::EVENT_ENTITY;
        }
        if (isset($datatablePost['relevantEntityId'])){
            $criteria->relevantEntityId = $datatablePost['relevantEntityId'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}