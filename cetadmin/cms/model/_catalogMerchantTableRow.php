<?php

class CatalogMerchantTableRow{
    public $DT_RowId;
    public $merchantIdDisplay;
    public $contactInfo;
    public $numImages;
    public $id;

    public function __construct(MerchantCatalogMerchantRecord $record){
        global $CurrentServer, $adminFolder;
        $this->DT_RowId = "merchant-".$record->merchantId;
        $this->id = $record->id;
        $this->numImages = $record->numImages;

        $merchantEditHref = $CurrentServer.$adminFolder."merchants/?nav=profile-edit&id=".$record->merchantId;
        $this->merchantIdDisplay = "<a href='".$merchantEditHref."' target='_blank'>".$record->merchantId."</a>";
        $contactInfoHtml = $record->name;
        if ($record->address){
            $contactInfoHtml .= "<br>".$record->address;
        }
        if ($record->city){
            $contactInfoHtml .= "<br>".$record->city.", ".$record->state." ".$record->zip;
        }
        if ($record->firstName || $record->lastName){
            $contactInfoHtml .= "<br>".$record->firstName." ".$record->lastName;
        }
        if ($record->email){
            $contactInfoHtml .= "<br>".$record->email;
        }
        if ($record->phone){
            $contactInfoHtml .= "<br>".$record->phone;
        }
        if ($record->pid){
            $contactInfoHtml .= "<br> PID: ".$record->pid;
        }
        $this->contactInfo = $contactInfoHtml;
    }

    private static $columnMapping = array(
        0 => "merchantIdDisplay",
        1 => "contactInfo",
        2 => "numImages",
        3 => "id"
    );

    public static function getCriteria($datatablePost){
        $criteria = new PaginationCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 0){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}