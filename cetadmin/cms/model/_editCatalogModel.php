<?php
if (isset($_GET['id']) && is_numeric($_GET['id'])){
    include_once($dbProviderFolder."MerchantCatalogProvider.php");
    $provider = new MerchantCatalogProvider($mysqli);
    $criteria = new MerchantCatalogCriteria();
    $criteria->id = $_GET['id'];
    $paginationResult = $provider->get($criteria);
    $resultArray = $paginationResult->collection;
    if (count($resultArray)){
        /* @var $record MerchantCatalogRecord */
        $record = $resultArray[0];
    }
}
if (!isset($record)){
    $record = new MerchantCatalogRecord();
}