<?php

class WpScrapeTableRow extends DatatableModelBase{
    public $DT_RowId;
    public $id;
    public $urlName;
    public $pageTitle;
    public $pageHeader;
    public $urlToScrape;
    public $createdDate;
    public $updatedDate;
    public $isValid;

    public function __construct(WordPressContentScrapeConfig $record = null){
        if ($record){
            $this->DT_RowId = "social-".$record->id;
            $this->id = $record->id;
            $this->urlName = $record->urlName;
            $this->pageTitle = $record->pageTitle;
            $this->pageHeader = $record->pageHeader;
            $this->urlToScrape = $record->urlToScrape;
            $this->createdDate = date("Y-m-d H:i", strtotime($record->createdDate));
            $this->updatedDate = $record->updatedDate ? date("Y-m-d H:i", strtotime($record->updatedDate)) : "";
            $this->isValid = $record->isValid() ? 1 : 0;
        }
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "urlName",
        2 => "pageTitle",
        3 => "pageHeader",
        4 => "urlToScrape",
        5 => "createdDate",
        6 => "updatedDate"
    );

    protected function getCriteriaObject(array $datatablePost)
    {
        $criteria = new WordPressContentScrapeCriteria();
        if (is_numeric($datatablePost['id'])){
            $criteria->id = $datatablePost['id'];
        }
        if ($datatablePost['urlName']){
            $criteria->urlName = $datatablePost['urlName'];
        }
        return $criteria;
    }

    protected function getColumnMapping(){
        return self::$columnMapping;
    }
}