<?php
include_once("_initFileManager.php"); // initializes fileManager, fileDataProvider, & options
$id = $_GET['id'];
if ($fileManager){
    /* @var $fileRecord MediaRecord */
    if ($id){
        $criteria = new MediaCriteria();
        $criteria->id = $id;
        $paginationResult = $fileDataProvider->get($criteria);
        $fileResults = $paginationResult->collection;
        $fileRecord = $fileResults[0];
    }
}