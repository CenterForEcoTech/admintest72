<?php

include_once($siteRoot."functions.php");
include_once($dbProviderFolder."ContentProvider.php");
include_once($repositoryApiFolder."files/ImageFileManager.php");

// set up directory location and options (this makes it more portable without copying the class files around)
$urlRoot = $CurrentServer."media/documents"; // example only
$directory = $siteRoot."media/documents";
$options = new UploadFileManagerOptions($urlRoot, $directory);
$fileDataProvider = new FileLibraryProvider($mysqli, getAdminId());

// get an ImageFileManager for this directory (if unable to read or write to the configured directory, returns null
$fileManager = GeneralFileManager::getInstance($options, $fileDataProvider);
