<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");
include_once("../_datatableModelBase.php");
include_once("model/_wpScrapeTableRow.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."ContentProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        if ($_GET['action'] == "list"){
            $provider = new WordPressContentScrapeConfigProvider($mysqli);
            $modelBase = new WpScrapeTableRow();
            $criteria = $modelBase->getCriteria($_GET);
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;

            $results = array(
                "iTotalRecords" => $paginationResult->totalRecords,
                "iTotalDisplayRecords" => $paginationResult->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($resultArray as $record){
                $results["aaData"][] = new WpScrapeTableRow($record);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
        }
        break;
    case "POST":
        echo "this is a post";
        break;
    case "PUT":
        // add
        $newRecord = json_decode($currentRequest->rawData);
        $record = new WordPressContentScrapeConfig();
        $record->id = $newRecord->id;
        $record->urlName = $newRecord->urlName;
        $record->pageTitle = $newRecord->pageTitle;
        $record->pageHeader = $newRecord->pageHeader;
        $record->pageSubheader = $newRecord->pageSubheader;
        $record->urlToScrape = $newRecord->urlToScrape;
        $provider = new WordPressContentScrapeConfigProvider($mysqli);
        $response = $provider->save($newRecord);
        if ($response->success){
            if ($response->insertedId){
                $record->id = $response->insertedId;
            }
            $results = (object)array(
                "record" => $record
            );
            header("HTTP/1.0 201 Created");
        } else {
            $results = (object)array(
                "record" => $record,
                "message" => $response->error
            );
            header("HTTP/1.0 409 Conflict");
        }
        break;
}
output_json($results);
die();
?>