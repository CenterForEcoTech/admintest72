<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Green Team Hours Invoice file</h3>
<?php
$_SESSION['SalesForceReport'] = "GreenTeam";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}


$GBSProvider = new GBSProvider($dataConn);

//Get Billing Rates
$criteria = new stdClass();
$criteria->invoiceName = "GreenTeam";
$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	$BillingRateByCategory[$record->GBSBillingRate_Name]=$record;
	foreach ($EmployeeMembers as $Employee){
		$BillingRateByEmployee[$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		$BillingRateByEmployeeJustRate[$Employee] = $record->GBSBillingRate_Rate;
	}
}
//print_pre($BillingRateByCategory);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "Green Team";
			$SelectedGroupsID = 27; //GreenTeam
			include('views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			foreach ($InvoiceData as $Employee_Name=>$hours){
				if (!$EmployeesRevenueCodeByName[$Employee_Name]){
					$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
				}
				
	//				echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
	//				print_pre($hours);
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingCategory=>$RateInfo){
				foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
					$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
					foreach ($EmployeeHours as $Employee_Name=>$hours){
						if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingCategory]){
							if ($hours && $hours != "0.00"){
								$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
								$IncludedEmployees[] = $Employee_Name;
							}
						}
						if ($BillingCategory == "EcoFellow" && !in_array($Employee_Name,$IncludedEmployees)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							$Adjustments["Alert"]["Staff missing Billing Rate"][]=str_replace(", ","_",$Employee_Name);
						}
					}
				}
				
			}
			//print_pre($InvoiceDataHours);
			
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
			require_once('salesforce/config.php');
			require_once('salesforce/soap_connect.php');
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_token'];
			$instance_url = $_SESSION['instance_url'];
			$reportId = "00OU0000003eDp2"; //Invoicing/518 GT Receipts

			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				session_start();
				$_SESSION['SalesForceReport'] = "GreenTeam";
				$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}else{					
				ob_start();
				
				if (!isset($access_token) || $access_token == "") {
					die("Error - access token missing from session!");
				}
				if (!isset($instance_url) || $instance_url == "") {
					die("Error - instance URL missing from session!");
				}
			}//end if checkSession
			$reportId = "00OU0000003eDp2"; //Invoicing/518 GT Receipts
			$reportName = "518 GT Receipts";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$parentIds[] = $GPNameLink;
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];

				/*New Method for Checking Attachments */
				$query = "SELECT Id,LinkedEntityId,ContentDocumentId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId='".$idLookup."'";
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->LinkedEntityId;
						$contentDocumentId = $record->ContentDocumentId;
						$query2 = "SELECT Id,Title,Description,FileType,FileExtension,VersionData FROM ContentVersion WHERE ContentDocumentId='".$contentDocumentId."'";
						$response2 = $mySforceConnection->query($query2); //sending request and getting response using SOAP
						foreach ($response2->records as $record2) {
							$name = $record2->Title;
							$fileType = $record2->FileType;
							$fileExtension = $record2->FileExtension;
							$versionData = $record2->VersionData;
							
							$nameParts = explode("_",$name);
							$vendorName = $nameParts[0];
							$receiptDescription = $nameParts[1];
							$receiptDate = str_replace(".pdf","",$nameParts[2]);
							$receiptDate = date("n/d/Y",strtotime($receiptDate));
							//print_pre($nameParts);
							
							
							if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
								$thisAttachmentInfo = array("accountId"=>$accountId,"Id"=>$contentDocumentId,"code"=>$code,"Name"=>$name,"FileExtension"=>$fileExtension,"AccountName"=>$accountName,"ContractorName"=>$ContractorLookup[$idLookup]["Name"],"ContractorInfo"=>$ContractorLookup[$idLookup]["Info"],"Report"=>$reportFolder,"Folder"=>"GreenTeam","parentId"=>$parentId);
								$attachmentName = retrieve_attachmentName($thisAttachmentInfo);
								$thisAttachmentInfo["SavedName"] = $attachmentName;
								
								$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $thisAttachmentInfo["SavedName"];
								$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $thisAttachmentInfo["AccountName"];
								$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $name;
								$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $fileExtension;
								
								
								$destination = "salesforce/".$attachmentFolder."/".$attachmentName;
								//echo $destination."<br>";
								$file = fopen($destination, "w+");
								fputs($file, $versionData);
								fclose($file);
								$Attachments[$parentId][] = $thisAttachmentInfo;
								
								$itemCheck = "518A".date("Y-m-d",strtotime($completedDate)).$nameParts[0];
								$ReceiptInfo = new stdClass();
								$ReceiptInfo->VendorName = $nameParts[0];
								$ReceiptInfo->Description = $receiptDescription;
								$ReceiptInfo->CompletedDate = $completedDate;
								$ReceiptInfo->Number = $number;
								$ReceiptInfo->Unit = $unit;
								$ReceiptInfo->itemCheck = $itemCheck;
	//							print_pre($ReceiptInfo);
								$attachmentsByJobID["518A"][] = $ReceiptInfo;	
								$attachmentsByItem[$itemCheck] = $ReceiptInfo;
								
								
								//echo $name.": ".$instance_url."/sfc/servlet.shepherd/version/download/".$latestPublishedVersionId."<Br>";
								
							}
							
						}
						//print_pre($Attachments); //ARRAY DESCRIBE parentIds and info used for gathering attachments and renaming them
					}
				}else{
					//$Attachments[$ids][] = array("NoAttachments"=>true,"Name"=>$accountName."-Audit","AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
					//$Adjustments["Attachments"][$accountName][$greenProspectsAccountId[$accountName][$ids][0]] = " missing attachment";
					$GPLink = "";
					foreach ($parentIds as $GPLinks){
						$GPLink .= $GPNameLinksArray[$GPLinks];
					}
					$Adjustments["Alert"]["Missing Attachment"][] = $accountName." ".$GPLink;
					//echo $GPLink." missing Attachment<br>";
				}
				/*End New Method for Checking Attachments */
				
				
				/* Old Method for checking attachments*/
				/*
				//check for attachments
				$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId ='".$GPNameLink."'";
				//echo $query;
				$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
//				print_r($response);
				if (count($response->records)){
					foreach ($response->records as $record) {
						$parentId = $record->ParentId;
						$name = $record->Name;
						$id = $record->Id;
						$fileParts = explode(".",$name);
						$attachmentInfo = array("Id"=>$id,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts,"Folder"=>"GreenTeam");
						$nameParts = explode("_",$name);
						$vendorName = $nameParts[0];
						$receiptDescription = $nameParts[1];
						$receiptDate = str_replace(".pdf","",$nameParts[2]);
						$receiptDate = date("n/d/Y",strtotime($receiptDate));
						//print_pre($nameParts);
						$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
						$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $attachment;
						$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
						$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $attachmentInfo["FileParts"][0];
						$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
						
						
						if (strtotime($completedDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
							$itemCheck = "518A".date("Y-m-d",strtotime($completedDate)).$nameParts[0];
							$ReceiptInfo = new stdClass();
							$ReceiptInfo->VendorName = $nameParts[0];
							$ReceiptInfo->Description = $receiptDescription;
							$ReceiptInfo->CompletedDate = $completedDate;
							$ReceiptInfo->Number = $number;
							$ReceiptInfo->Unit = $unit;
							$ReceiptInfo->itemCheck = $itemCheck;
//							print_pre($ReceiptInfo);
							$attachmentsByJobID["518A"][] = $ReceiptInfo;	
							$attachmentsByItem[$itemCheck] = $ReceiptInfo;
						}
						
					}
				}else{
					$Adjustments["Alert"]["Missing Receipt Attachment"][] = $GPNameHref." missing attachment for $".money($number)." ".$otherExplain;
				}
				*/
				/*End old method for checking attachments*/
			}
		
			include_once('salesforce/reports/reportGreenTeamHours.php');
			echo $ReportsFileLink;
			
		echo "</div>";
		echo "<div id='Report' class='tab-content'>";
		if(count($_FILES['filesToUpload1'])) {
			$fileReceived = true;
			foreach ($_FILES['filesToUpload1']["tmp_name"] as $fileKey=>$tmpFileName) {
				if ($_GET['type'] == "databaseFile"){
					$fileName = strtolower($_FILES['filesToUpload1']["name"][$fileKey]);
					if (strpos($fileName,"reg query")){$fileName = "01RegQuery";}
					if (strpos($fileName,"equipment app")){$fileName = "02EquipmentApp";}
					if (strpos($fileName,"equip eval")){$fileName = "03EquipEval";}
					if (strpos($fileName,"idle reduction")){$fileName = "04IdleReduction";}
					if (strpos($fileName,"teacher response")){$fileName = "05TeacherResponse";}
					if (strpos($fileName,"technical assistance")){$fileName = "06TechnicalAssistance";}
					if (strpos($fileName,"outreach efforts")){$fileName = "07OutreachEfforts";}
					$fileName = date("Y_m",strtotime($invoiceDate))."_".$fileName.".xlsx";
					$databaseFileReceived = true;
					$databaseFileName = "importfiles/GreenTeam/".$fileName;
					$target_file = $siteRoot.$adminFolder."invoicing/".$databaseFileName;
					move_uploaded_file($tmpFileName, $target_file);
				}
			
				
			}
		}
		
		
		
?>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>invoicing/?nav=invoicingTool-GreenTeam&type=databaseFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload1[]" id="filesToUpload1" multiple="" onChange="makeFileList1();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList1"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList1() {
								var input = document.getElementById("filesToUpload1");
								var ul = document.getElementById("fileList1");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
						</script>
					</form>
<?php		
		
		echo "Currently not loading Google Docs but if this line was removed could";		
		/*
		
			$url="https://docs.google.com/spreadsheets/d/1kmj9XxOBcnMroGydPwlWs6E1BD5GO9FPK1Ie3fOeUII/pub?gid=366106165&single=true&output=csv";
			$rowCount = 0;
			if (($handle = fopen($url, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						if ($rowCount < 1){
							$headerRow = $data;
						}else{
							$spreadsheet_data[]=$data;
						}
						$rowCount++;
					}
				fclose($handle);
			}else{
				die("Problem reading csv");	
			}
			foreach ($spreadsheet_data as $row=>$data){
				$googleDataRow = array();
				foreach ($data as $key=>$val){
					$googleDataRow[$headerRow[$key]] = $val;
				}
				$GreenTeamOutreachEfforts[] = $googleDataRow;
			};
			echo "There are ".count($GreenTeamOutreachEfforts)." Green Team Outreach Effort rows<br>";
//			print_pre($GreenTeamOutreachEfforts);
			
			$url = "https://docs.google.com/spreadsheets/d/1ZSorN3dXRxefT48MgTVLBiaPfk6XMCs7QA5PrjxvFUE/pub?gid=270658893&single=true&output=csv";
			$rowCount = 0;
			if (($handle = fopen($url, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						if ($rowCount < 1){
							$headerRow = $data;
						}else{
							$spreadsheet_data[]=$data;
						}
						$rowCount++;
					}
				fclose($handle);
			}else{
				die("Problem reading csv");	
			}
			foreach ($spreadsheet_data as $row=>$data){
				$googleDataRow = array();
				foreach ($data as $key=>$val){
					$googleDataRow[$headerRow[$key]] = $val;
				}
				$GreenTeamTechnicalAssistance[] = $googleDataRow;
			};
			echo "There are ".count($GreenTeamTechnicalAssistance)." Green Team Technical Assistance rows<br>";
//			print_pre($GreenTeamTechnicalAssistance);
		*/		
		echo "</div>";
		
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "GreenTeam");
		include_once('_notesTab.php');
		
	echo "</div>";
?>	
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the <?php echo $ReportsFileLink;?> File.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>