<?php
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");
include_once($dbProviderFolder."ProductProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$InvoicingProvider = new InvoicingProvider($dataConn);
$ProductProvider = new ProductProvider($dataConn);

$taxRate = 1.0625; //used for calculating HPC UnitRate

$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $ProductProvider->get($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);
foreach ($resultArray as $result=>$record){
	$efiParts = explode(".",$record->efi);
	$bulbType = ($record->bulbType ? : "LED");
	$CSGCode = $record->csgCode;
	$CSGCodes = array();
	if (strpos($CSGCode,",")){
		$CSGCodes = explode(",",$CSGCode);
	}else{
		$CSGCodes = array($CSGCode);
	}
	$clearResultsCode = $bulbType.$efiParts[1];
	$ProductsByEFI[$record->efi] = $record;
	$ProductsByDescription[$clearResultsCode] = $record;
	foreach ($CSGCodes as $code){
		$ProductsByCSGCodes[$code] = $record;
	}
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}

}
//print_pre($ProductsByCSGCodes["005_TCP_TORP_LED"]);

$recordType = ($_GET['recordType'] ? : "Jobs");
$destination = ($_GET['destination'] ? : "NGRID");
$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

//NGRID update invoice number
if ($destination == "NGRID"){
	$startInvoiceNumberFromNov2016 = 220;	
	$monthsFromNov2016 = datediff("2016-11-01", $invoiceDate, "month");
	$invoiceNumber = $startInvoiceNumberFromNov2016+$monthsFromNov2016;
}
//Eversource update invoice number
if ($destination == "Eversource"){
	$startInvoiceNumberFromNov2016 = 104;	
	$monthsFromNov2016 = datediff("2016-11-01", $invoiceDate, "month");
	$invoiceNumber = $startInvoiceNumberFromNov2016+$monthsFromNov2016;
}

$criteria = new stdClass();
$criteria->recordType = $recordType;
$criteria->destination = $destination;
$FileConverterFields = $InvoicingProvider->getInvoicingFileConverterFields($criteria);
foreach ($FileConverterFields->collection as $result){
	$field[$result->FieldName] = $result->FieldType;
	$fields[$result->OrderID] = $result;
}
ksort($fields);
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
</style>

<h1>Convert ClearResult File to Pipe Delimited data for Energy Providers</h2>
<div class="row">
	<div class="three columns">
		<h3>
			<?php echo $monthYear;?>
			<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$monthYear)));?>">
		</h3>
	</div>
	<div class="four columns">
		Energy Provider:<br>
		<select id="destination" class="parameters four columns">
			<option value="NGRID"<?php echo ($recordType=="NGRID" ? " selected" : "");?>>NGRID</option>
			<option value="Eversource"<?php echo ($destination=="Eversource" ? " selected" : "");?>>Eversource</option>
		</select>
	</div>
	<div class="four columns">
		File Type:<br>
		<select id="recordType" class="parameters four columns">
			<option value="Jobs"<?php echo ($recordType=="Jobs" ? " selected" : "");?>>Jobs</option>
			<option value="Measures"<?php echo ($recordType=="Measures" ? " selected" : "");?>>Measures/Parts</option>
		</select>
	</div>
</div>
<br clear="all">
<?php
$ImportFileName = "importfiles/fileconverter/uploadFile_".$destination.$recordType."_".$yearMonth.".xls";
$ExportFileName = "importfiles/fileconverter/uploadFile_".$destination.$recordType."_".$yearMonth.".csv";
//echo "ImportFileName ".$ImportFileName."<br>";
$target_file = $siteRoot.$adminFolder."invoicing/".$ImportFileName;
if(count($_FILES['fileToUpload1'])) {
	$tmpFileName = $_FILES["fileToUpload1"]["tmp_name"][0];
	if ($_GET['type'] == "uploadFile"){
		$ImportFileReceived = true;
		move_uploaded_file($tmpFileName, $target_file);
	}
}
if ($ImportFileReceived || $_GET['useUploadedFile']){
	//new import that data and parse it out 
	$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
	$_SESSION['ImportFileName'] = $ImportFileName;
	echo "Using File just uploaded<br>";
	ob_flush();
	include('salesforce/read_rawFileReader.php');
	//print_pre($rowsRaw);
}
if (!$ImportFileReceived){
	if (file_exists($target_file)){
		$lastUploadedDateParts = explode("File",$ImportFileName);
		$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
		
		echo "<br><br><a href='".$ImportFileName."'>View Existing ".$destination." ".$recordType." Uploaded File</a> last uploaded ".date("F d, Y H:ia",filemtime($target_file))."<br>";
		$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/".$ImportFileName : $ImportFileName);
		$trackingFile = $ImportFileName;
	}
}
?>


	<a id='uploadFile' href="#" class='button-link do-not-navigate'>Upload File</a><br>
	<div id="uploadFileForm" style='display:none;'>
		<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>invoicing/?type=uploadFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
			<div class="row">
				<div class="eight columns">
					<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
				</div>
				<div class="eight columns">
					<strong>File You Selected:</strong>
					<ul id="fileList1"><li>No Files Selected</li></ul>
					<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
				</div>
			</div>
			
			<script type="text/javascript">
				function makeFileList1() {
					var input = document.getElementById("fileToUpload1");
					var ul = document.getElementById("fileList1");
					while (ul.hasChildNodes()) {
						ul.removeChild(ul.firstChild);
					}
					for (var i = 0; i < input.files.length; i++) {
						var li = document.createElement("li"),
							fileName = input.files[i].name,
							fileLength = fileName.length,
							fileExt = fileName.substr(fileLength-4);
							console.log(fileExt);
						if (fileExt == ".xls" || fileExt == "xlsx"){
							li.innerHTML = input.files[i].name;
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'block';
						}else{
							li.innerHTML = 'You must save '+fileName+' as a .xls or .xlsx file first';
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'none';
						}
					}
					if(!ul.hasChildNodes()) {
						var li = document.createElement("li");
						li.innerHTML = 'No Files Selected';
						ul.appendChild(li);
						document.getElementById('submitButton1').style.display = 'none';
					}
				}
			</script>
		</form>
	</div>
<?php
if ($recordType == "Measures"){
	$measureRowsRaw = $rowsRaw; //to avoid data being overwritten
	$measureHeaderNames = $headerNames;
	//read Jobs file for reconciliation

	$ImportFileName = "importfiles/fileconverter/uploadFile_".$destination."Jobs_".$yearMonth.".xls";
	include('salesforce/read_rawFileReader.php');
	$HPCAuditors = array('American Installations LLC');
	foreach ($rowsRaw as $rowInfo){
		$JobId = substr($rowInfo["EXT_JOB_ID"],0,12);
		$JobInfo[$JobId] = trim((in_array($rowInfo["AUDITOR"],$HPCAuditors) ? "HPC" : "").($rowInfo["OWN_RENT"] == "R" ? " R" : ""));
		$RenterInfo[$JobId] = trim($rowInfo["OWN_RENT"]);
		$JobSiteIds[$JobId] = "Jobs";
	}
	$rateTypeID = array(0=>"",1=>"R",3=>"HPC",5=>"HPC R");
	$rowsRaw = $measureRowsRaw; //to restore data
	$headerNames = $measureHeaderNames;

}
?>			
	<?php 
		if ($recordType == "Measures"){
			foreach ($rowsRaw as $rowId=>$rowInfo){
				//clean up Job_ID
				$JobId = substr($rowInfo["EXT_JOB_ID"],0,12);
				$MeasuresSiteIds[$JobId] = "Measures";
				$InvoiceBreakdownID = ($JobInfo[$JobId] ? : "");
				$InvoiceBreakdownID = strlen($InvoiceBreakdownID);
				$InvoiceBreakdownID = ($destination == "NGRID" ? 0 : $InvoiceBreakdownID);
				$rowInfo["EXT_JOB_ID"] = $JobId;
				$thisMeasureCode = ($destination == "NGRID" ? $rowInfo["MEASURE_CODE"] : $rowInfo["PART_ID"]);
				$recordIdentifier = $JobId."_".$thisMeasureCode."_".str_replace(" ","_",$rowInfo["DESCRIPTION"])."_".strtotime($rowInfo["INSTALLED_DATE"]);
				
				//get aggregate for invoice comparison
				$LaborCost = ($destination == "NGRID" ? 4.25 : 4.50);
				$itemCost = ($ProductsByDescription[$thisMeasureCode]->cost ? : $ProductsByCSGCodes[$thisMeasureCode]->cost);
				if ($destination == "Eversource" && $InvoiceBreakdownID >=3){
					$itemCost = round(bcmul($itemCost,$taxRate,3),2,PHP_ROUND_HALF_UP);
				}
				$UnitCost = bcadd($itemCost,$LaborCost,2);

				//exception for PowerStrip
				if ($thisMeasureCode == "TrickleStarPowerStrip"){
					$UnitCost = $ProductsByCSGCodes[$thisMeasureCode]->retail;
					$LaborCost = bcsub($UnitCost,$itemCost,2);
				}
				//exception for Piggyback Fee
				if ($thisMeasureCode == "PIGGYBACK_FEE"){
					$UnitCost = 15.00;
					$itemCost = 0;
					$LaborCost = bcsub($UnitCost,$itemCost,2);
				}
				
				//Eversource was MEA_ID appended with _R for renter units
				if ($destination == "Eversource" && $RenterInfo[$JobId] == "R"){
					$rowInfo["MEA_ID"] = $rowInfo["MEA_ID"]."_R";
				}
				
				$EFI = ($ProductsByDescription[$thisMeasureCode]->efi ? : $ProductsByCSGCodes[$thisMeasureCode]->efi);
				$rowInfo["AUTH_INCENTIVE_AMT"] = $UnitCost;
				$rowInfo["TOTAL_INSTALLED_PRICE"] = bcmul($UnitCost,$rowInfo["QTY"],2);
				$rowInfo["CUSTOMER_PRICE"] = bcmul($UnitCost,$rowInfo["QTY"],2);
				$InvoiceData[$rowInfo["DESCRIPTION"]][$InvoiceBreakdownID]["QTY"] = ($InvoiceData[$rowInfo["DESCRIPTION"]][$InvoiceBreakdownID]["QTY"]+$rowInfo["QTY"]);
				$InvoiceData[$rowInfo["DESCRIPTION"]][$InvoiceBreakdownID]["UnitCost"] = $UnitCost;
				$InvoiceData[$rowInfo["DESCRIPTION"]][$InvoiceBreakdownID]["EFI"] = $EFI;
				$InvoiceData[$rowInfo["DESCRIPTION"]][$InvoiceBreakdownID]["LaborCost"] = $LaborCost;
				
				//$rowInfo["AUTH_INCENTIVE_AMT"];
				
				
				if (is_array($groupedRows[$recordIdentifier])){
					$condensedJobs[$JobId][$thisMeasureCode." ".$rowInfo["DESCRIPTION"]]++;
					$totalLessRowsFromRawData++;
				}
				foreach ($fields as $fieldorderId=>$fieldInfo){
					$fieldName = $fieldInfo->FieldName;
					$fieldValue = trim($rowInfo[$fieldName]);
					$fieldType = $fieldInfo->FieldType;
					if (strpos(" ".$fieldType,"NUMBER")){
						//special exception for AUTH_INCENTIVE_AMT for NGRID which should be AUTH_INCENTIVE_AMT*QTY
						//as indicated from an email from Sumit at NGRID "There is no field for per-unit incentive amount in this layout. AUTH_INCENTIVE_AMT field is the Total Incentive Amount."
						if ($destination == "NGRID" && $fieldName == "AUTH_INCENTIVE_AMT"){
							$fieldValue = bcmul($rowInfo["QTY"],$rowInfo["AUTH_INCENTIVE_AMT"],2);
						}
						if ($destination == "Eversource" && $fieldName == "AUTH_INCENTIVE_AMT"){
							$fieldValue = $rowInfo["TOTAL"];
						}
						$groupedRows[$recordIdentifier][$fieldName] = bcadd($groupedRows[$recordIdentifier][$fieldName],$fieldValue,2);
					}else{
						$groupedRows[$recordIdentifier][$fieldName] = $fieldValue;
					}
					
				}
			}
		}else{
			$groupedRows = $rowsRaw;
		}
	?>
<hr>
Data Imported <?php echo count($rowsRaw)." rows";?>:
			<table class="simpleTableLimited">
				<thead>
					<tr>
						<th>Row</th>
						<?php 
							foreach ($headerNames as $columnLetter=>$value){
								echo "<th>".$value."</th>";
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($rowsRaw as $rowId=>$rowInfo){
							echo "<tr><td>".($rowId+1)."</td>";
							foreach ($rowInfo as $rowHeader=>$rowValue){
								echo "<td nowrap>".$rowValue."</td>";
							}
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
			
Data Converted to <?php echo count($groupedRows)." rows";?><?php echo ($totalLessRowsFromRawData ? " (Data Imported minus ".$totalLessRowsFromRawData." combined rows)" : "");?>:
			<table class="simpleTableLimited">
				<thead>
					<tr>
						<th>Row</th>
						<?php 
							foreach ($fields as $fieldorderId=>$fieldInfo){
								$fieldHeaders[] = $fieldInfo->FieldName;
								echo "<th>".$fieldInfo->FieldName."</th>";
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php 
						$thisRow = 0;
						foreach ($groupedRows as $rowId=>$rowInfo){
							//clean up Job_ID
							$rowInfo["EXT_JOB_ID"] = substr($rowInfo["EXT_JOB_ID"],0,12);
							$thisRow++;
							echo "<tr><td>".$thisRow."</td>";
							
							
							
							foreach ($fields as $fieldorderId=>$fieldInfo){
								$fieldName = $fieldInfo->FieldName;
								$fieldType = $fieldInfo->FieldType;
								$fieldValdationRule = $fieldType;
								$fieldValue = trim($rowInfo[$fieldName]);
								$mandatoryStatus = $fieldInfo->MandatoryStatus;
								$errorMessage = "";
								if ($mandatoryStatus == "Mandatory" && !$fieldValue){
									$errorMessage = "is required but value is blank";
								}
								if (strpos(" ".$fieldType,"VARCHAR2")){
									$strLimit = str_replace("VARCHAR2","",$fieldType);
									$strLimit = str_replace("(","",$strLimit);
									$strLimit = (int)str_replace(")","",$strLimit);
									$fieldValdationRule = " string limit ".$strLimit."> ".strlen($fieldValue);
									if (strlen($fieldValue) > $strLimit){
										$errorMessage = "'".$fieldValue."' length ".strlen($fieldValue)." characters > limit of ".$strLimit." characters";
									}
									
								}
								if ($fieldType == "DATE"){
									if ($fieldName == "YEAR_BUILT"){
										$thisDateYear = (int)$fieldValue;
										$dateFormat = "YYYY";
										if ($thisDateYear > 1971){
											$convertedDate = date("Y",strtotime("01/01/".$thisDateYear));
										}else{
                                            try {
                                                $date = new DateTime("01/01/" . $thisDateYear);
                                            } catch (Exception $e) {
                                            }
                                            $convertedDate = $date->format("Y");
											if(!$thisDateYear){$convertedDate = "and is blank";}
										}
									}else{
										$dateFormat = "mm/dd/YYYY";
										$convertedDate = date("m/d/Y",strtotime($fieldValue));
										if ($convertedDate == "12/31/1969" && strlen($fieldValue) == 5){
											$fieldValue = exceldatetotimestamp($fieldValue,$dateFormat);
											$convertedDate = date("m/d/Y",strtotime($fieldValue));
										}
									}
									$fieldValdationRule = $convertedDate;
									if ($fieldValue != $convertedDate){
										$errorMessage = $fieldValue." not in ".$dateFormat." format ".$convertedDate;
									}
								}
								if (strpos(" ".$fieldType,"NUMBER")){
									$strLimit = str_replace("NUMBER","",$fieldType);
									$strLimit = str_replace("(","",$strLimit);
									$strLimit = (int)str_replace(")","",$strLimit);
									$fieldValdationRule = " number limit ".$strLimit."> ".strlen($fieldValue);
									if (strlen($fieldValue) > $strLimit){
										$errorMessage = "length of ".strlen($fieldValue)." digits > limit of ".$strLimit." digits";
									}
									
								}
								if ($mandatoryStatus == "Mandatory" && $errorMessage){
									$errors[$thisRow][$fieldName][$mandatoryStatus] = $errorMessage;
								}else{
									if ($fieldValue && $errorMessage){
										$errors[$thisRow][$fieldName][$mandatoryStatus] = $errorMessage;
									}
								}
								
								echo "<td nowrap>".$fieldValue."</td>";
								//convert QTY to whole number
								if ($fieldName == "QTY"){$fieldValue = round($fieldValue);}
								$cleanRow[$thisRow][] = $fieldValue;
							}
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
			<style>
				.Mandatory {color:red;}
				.Not {color:black;}
			</style>
<?php
	if ($recordType == "Measures"){
		//look for orphan records (JobId in Jobs but not in Measures and vice versa)
		$JobOrphans = array_diff_key($JobSiteIds,$MeasuresSiteIds);
		$MeasureOrphans = array_diff_key($MeasuresSiteIds,$JobSiteIds);
		if (count($JobOrphans) || count($MeasureOrphans)){
			echo "<br><div style='background-color:pink;padding:10px;border:1pt solid red;'>";
			echo "<h3>Orphan Site IDs</h3>";
			echo "<table class='simpleTable'>
					<thead>
						<tr>
							<th style='width:150px;'>Record Type</th>
							<th>SiteID</th>
						</tr>
					</thead>
					<tbody>";
			if (count($JobOrphans)){
				foreach ($JobOrphans as $SiteID=>$type){
					echo "<tr><td>Job</td><td>".$SiteID."</td></tr>";
				}
			}
			if (count($MeasureOrphans)){
				foreach ($MeasureOrphans as $SiteID=>$type){
					echo "<tr><td>Measure</td><td>".$SiteID."</td></tr>";
				}
			}
			echo 	"</tbody>
				</table>
			</div><br>";
		}
	}
	
	if (count($errors)){
		echo "<div style='background-color:pink;padding:10px;border:1pt solid red;'>";
		echo "<h3>Errors</h3>";
		echo "<table class='simpleTable'>
				<thead>
					<tr>
						<th>Row</th>
						<th>Error Message</th>
					</tr>
				</thead>
				<tbody>";
					foreach ($errors as $rowId=>$fieldInfo){
						$thisErrorMessage = "";
						ksort($fieldInfo);
						foreach ($fieldInfo as $fieldName=>$errorInfo){
							foreach ($errorInfo as $mandatoryStatus=>$errorMessage){
								$thisErrorMessage .= "<span class='".$mandatoryStatus."'>".$fieldName."</span> ".$errorMessage."<Br>";
							}
						}
						echo "<tr>
							<td nowrap>".$rowId."</td>
							<td nowrap>".$thisErrorMessage."</td>
						</tr>";
					}
		echo "	</tbody>
			</table>
		</div><hr>";
	}
	if (count($condensedJobs)){
		echo "<h3>Combined Rows</h3>";
		echo "<table class='simpleTable'>
				<thead>
					<tr>
						<th>Job_ID</th>
						<th>MEASURE_CODE</th>
					</tr>
				</thead>
				<tbody>";
					foreach ($condensedJobs as $JobID=>$MeasureCodeInfo){
						foreach ($MeasureCodeInfo as $MeasureCode=>$count){
							echo "<tr>
								<td nowrap>".$JobID."</td>
								<td nowrap>".$MeasureCode." [".($count+1)." rows]</td>
							</tr>";
						}
					}
		echo "	</tbody>
			</table><hr>";
	}
	//display summary of items for invoice comparison
	if ($recordType == "Measures"){
			echo "<h3>Invoice Comparison</h3>";
			echo "<table class='simpleTable'>
					<thead>
						<tr>".($destination == "Eversource" ? "<th>RateType</th>" : "")."
							<th>Number</th>
							<th>Description</th>
							<th>UnitCost</th>
							<th>Totals</th>
						</tr>
					</thead>
					<tbody>";
					ksort($InvoiceData);
//					print_pre($InvoiceData);
					foreach ($InvoiceData as $Description=>$DescriptionInfo){
						if ($destination == "Eversource"){
							ksort($DescriptionInfo);
							foreach ($DescriptionInfo as $rateType=>$RateTypeInfo){
								$thisTotal = bcmul($RateTypeInfo["QTY"],$RateTypeInfo["UnitCost"],2);
								$InvoiceTotal = bcadd($InvoiceTotal,$thisTotal,2);
								$type = (trim($Description) == "Piggyback Fee" ? "CLCCC" : "CLINC");
								echo "<tr>
										<td><span style='display:none;'>".$Description.$rateType."</span>".$rateTypeID[$rateType]."</td>
										<td>".$RateTypeInfo["QTY"]."</td>
										<td>".$Description."</td>
										<td>$".money($RateTypeInfo["UnitCost"])."</td>
										<td>$".money($thisTotal)."</td>
									</tr>";
								$InvoiceRowData[$Description."_".$rateType] = array("RateType"=>$rateType,"Type"=>$type,"UnitCost"=>$RateTypeInfo["UnitCost"],"LaborCost"=>$RateTypeInfo["LaborCost"],"EFI"=>$RateTypeInfo["EFI"],"Description"=>$Description,"QTY"=>$RateTypeInfo["QTY"]);
								$TypeAmount[$type] = bcadd($TypeAmount[$type],$thisTotal,2);
							}

						}else{
							foreach ($DescriptionInfo as $rateType=>$RateTypeInfo){
								$thisTotal = bcmul($RateTypeInfo["QTY"],$RateTypeInfo["UnitCost"],2);
								$InvoiceTotal = bcadd($InvoiceTotal,$thisTotal,2);
								echo "<tr>
										<td><span style='display:none;'>".$Description."</span>".$RateTypeInfo["QTY"]."</td>
										<td>".$Description."</td>
										<td>$".money($RateTypeInfo["UnitCost"])."</td>
										<td>$".money($thisTotal)."</td>
									</tr>";
							}
						}
					}
			echo "</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>$".money($InvoiceTotal)."</td>
					</tr>
				</tfoot>
				</table>";
				
	}
?>
<?php 
	if ($ImportFileReceived){
		//print_pre($cleanRow);
		//create pipedelimited file
		$fp = fopen( $ExportFileName,'w');
		fwrite($fp, implode('|',$fieldHeaders) . "\r\n");
	//	fputs($fp,implode("|",$fieldHeaders),"\n");
		foreach ($cleanRow as $rowId=>$row){
			fwrite($fp, implode('|',$row) . "\r\n");
			//fputs($fp, implode("|",$row),"\n");
		}
		fclose($fp);
		echo "<a href='".$ExportFileName."'>Export ".$destination." ".$recordType." ".$yearMonth." File</a>";

		//create Invoice
		if ($recordType == "Measures"){
			include_once('salesforce/reports/'.$destination.'Residential.php');
			echo "<Br>".$ReportsFileLink;
		}
	}

?>	

<Br><BR>
Field Template for Export:
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Order</th>
						<th>FieldName</th>
						<th>FieldType</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($fields as $fieldInfo){
							echo "<tr>
								<td nowrap>".$fieldInfo->OrderID."</td>
								<td nowrap><span class='".$fieldInfo->MandatoryStatus."'>".$fieldInfo->FieldName."</span></td>
								<td nowrap>".$fieldInfo->FieldType."</td>
								<td>".$fieldInfo->Description."</td>
							</tr>";
						}
					?>
				</tbody>
			</table>

<script type="text/javascript">
	$(function () {
		$("#uploadFile").on('click',function(){
			$("#uploadFileForm").toggle();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		var tableLimited = $('.simpleTableLimited').DataTable({
			"scrollX": true,
			"scrollY": "240px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 10,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
		
		var getFilters = function(){
			var recordType = $("#recordType").val(),
				destination = $("#destination").val(),
				monthYear = $("#MonthPicker").val(),
				filterValues = new Array(recordType,destination,monthYear);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>invoicing/?recordType="+Filters[0]+"&destination="+Filters[1]+"&monthYear="+Filters[2]+"&nav=<?php echo $navDropDown;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		$('.date-picker').datepicker({
			showOn: "button",
			buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
			buttonImageOnly: true,
			buttonText: 'Click to change month',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			minDate: '01/01/14',
			maxDate: '<?php echo date("m/t/Y");?>',
			onClose:
				function(dateText, inst){
					var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, month, 1));
					$("#StartDate").datepicker('setDate', new Date(year, month, 1));
					var date2 = $(this).datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$("#EndDate").datepicker('setDate', date2);
					$("#weekPicker").val('');
					updateFilters();
				}
		
		});

		
	});
</script>