<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
$adminFolder = ($_SESSION['isStaff'] ? "cetstaff" : $adminFolder);
$GBSProvider = new GBSProvider($dataConn);
switch ($_SESSION['SalesForceReport']){
	case "MFEP":
		$mfepQueryString = $_SESSION['MFEPQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$mfepQueryString."&useUploadedFile=".$_SESSION['MFEPBillableHoursFileName'];
		//echo $redirectString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
	case "BGAS":
		$salesForceQueryString = $_SESSION['SalesForceQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$salesForceQueryString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
	case "RWTAPipeline":
		$salesForceQueryString = $_SESSION['SalesForceQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$salesForceQueryString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
	case "GreenTeam":
		$salesForceQueryString = $_SESSION['SalesForceQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$salesForceQueryString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
	case "ColumbiaGas":
		$salesForceQueryString = $_SESSION['SalesForceQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$salesForceQueryString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
	case "Eversource":
		$_SESSION['SalesForceReport'] = "Eversource";
	default:
		$salesForceQueryString = $_SESSION['SalesForceQueryString'];
		$redirectString = $CurrentServer.$adminFolder."/invoicing/?".$salesForceQueryString;
		echo "<script>window.location.replace('".$redirectString."');</script>";
		break;
}
?>