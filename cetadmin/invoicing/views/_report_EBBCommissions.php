<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the EBB Commissions Report</h3>
<?php
$_SESSION['SalesForceReport'] = "EBBCommissions";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);


$StartOfThisMonth = date("m/01/Y",strtotime(date("m/01/Y")." -1 month"));
$EndOfThisMonth = date("m/t/Y",strtotime(date("m/01/Y")." -1 month"));
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $StartOfThisMonth);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $EndOfThisMonth);

$invoiceStartDate = $invoiceDate = $StartDate;
$invoiceEndDate = $EndDate;
$month = strtolower(date("M",strtotime($invoiceDate)));
$FiscalYear = "2018-2019";
$YearMonth = $FiscalYear."_".$month;

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);

//check if this month has already been locked data;
$checkedResults = $GBSProvider->getLockedEcoBBCommissions();
$results = $checkedResults->collection;
foreach ($results as $record){
	$lockedData[$record->FiscalYear."_".$record->Month][$record->Owner][$record->Type] = $record->Amt;
}
//print_pre($lockedData);
if (count($lockedData[$YearMonth])){
	$thisYearMonthLocked = true;
}

$setTab = "Data";
	$TabCodes = array("Data"=>"Data","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Data' class='tab-content'>";
		?>
			<div class="twenty columns">
				<div class="two columns">
					Start Date<bR>
					<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
				</div>
				<div class="two columns">
					End Date<br>
					<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
				</div>
			</div>
			<br clear="all">
		<?php
			$extraSFID = "00kG0000"; //this is the leading code cut off on pos lookup codes because of length.
			$extraSFID = "00k0f000";
			require_once 'salesforce/config.php';
			require_once('salesforce/rest_functions.php');
			$access_token = $_SESSION['access_tokenEBB'];
			$instance_url = $_SESSION['instance_urlEBB'];
			$reportId = "00OG0000007kYfc"; //Commissions/Commission Items Over 5k-Testing
			//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
			if (!checkSession($instance_url, $reportId, $access_token)){
				echo "<script>window.location.replace('".AUTH_URL."');</script>";
				//echo AUTH_URL;
				//header('Location: '.AUTH_URL);
			}		
			if (!isset($access_token) || $access_token == "") {
				die("Error - access token missing from session!");
			}
			if (!isset($instance_url) || $instance_url == "") {
				die("Error - instance URL missing from session!");
			}
			
			
			$reportId = "00O0f0000077HSDEA2"; //Commissions/Acquisition Expense
			$reportName = "Dash - Closed Won for Commissions";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportResults);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$owner = $rowDetail["FULL_NAME"];
				$amount = $rowDetail["AMOUNT"];
				$amountInt = str_replace(",","",str_replace("$","",$amount));
				$closeDate = $rowDetail["CLOSE_DATE"];
				if (strtotime($closeDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($closeDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceEndDate)))){
					$monthlyClosedWon[$month][$owner] = bcadd($monthlyClosedWon[$month][$owner],$amountInt,2);
					$lockCommissionData[$YearMonth][$owner]["ClosedWon"] = bcadd($lockCommissionData[$YearMonth][$owner]["ClosedWon"],$amountInt,2);
				}
			}
			$reportId = "00OG0000007kOjF"; //Commissions/Acquisition Expense
			$reportName = "Acquisition Expense";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportResults);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				
				$owner = $rowDetail["Opportunity.Owner.Name"];
				$amount = $rowDetail["Opportunity.Acquisition_Expense__c"];
				$amountInt = str_replace(",","",str_replace("$","",$amount));
				$closeDate = $rowDetail["Opportunity.CloseDate"];
				if (strtotime($closeDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($closeDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceEndDate)))){
					$monthlyAcquisitionsExpenses[$month][$owner] = bcadd($monthlyAcquisitionsExpenses[$month][$owner],$amountInt,2);
					$lockCommissionData[$YearMonth][$owner]["AcquisitionExpense"] = bcadd($lockCommissionData[$YearMonth][$owner]["AcquisitionExpense"],$amountInt,2);
				}

			}
	
			//00OG0000007kbtF
			$reportId = "00OG0000007kbtF"; //Commissions/Commissions Details For Items Under 5K
			$reportName = "Commissions Details For Items Under 3K";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportResults);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$closeDate = $rowDetail["CLOSE_DATE"];
				$stageName = $rowDetail["STAGE_NAME"];
				$owner = $rowDetail["FULL_NAME"];
				
				$opportunityName = $rowDetail["OPPORTUNITY_NAME"];
				$opportunityNameLink = $rowDetail["OpportunityNameLinkValue"];
				$opportunityNameHref = "<a href='".$instance_url."/".$opportunityNameLink."' target='".$opportunityNameLink."'>".$opportunityName."</a>";
				
				$productName = $rowDetail["PRODUCT_NAME"];
				$productNameLink = $rowDetail["ItemNameLinkValue"];
				$productNameHref = "<a href='".$instance_url."/".$productNameLink."' target='".$productNameLink."'>".$productName."</a>";
				
				$itemName = $rowDetail["LINEITEM_DESCRIPTION"];
				$itemNameLink = $rowDetail["OpportunityLineItem.LineItemID__c"];
				$itemNameHref = "<a href='".$instance_url."/".$itemNameLink."' target='".$itemNameLink."'>".$itemName."</a>";
				
				$rowDetail["ITEMID"] = $itemNameLink;
				$rowDetail["ITEMLINK"] = $instance_url."/".$itemNameLink;
				$rowDetail["ITEMLINKHREF"] = $itemNameHref;
				$quantity = $rowDetail["QUANTITY"];
				$unitPrice = $rowDetail["UNIT_PRICE"];
				$totalPrice = $rowDetail["TOTAL_PRICE"];
				$totalPriceInt = str_replace(",","",str_replace("$","",$totalPrice));
				$productPrice = $rowDetail["PRODUCT_PRICE"];
				
				$commissionDetailsByOwner[$month][$owner][$opportunityName][] = $rowDetail;
				$commissionSummaryByOwner[$month][$owner] = bcadd($commissionSummaryByOwner[$month][$owner],$totalPriceInt,2);
				$lockCommissionData[$YearMonth][$owner]["Under5K"] = bcadd($lockCommissionData[$YearMonth][$owner]["Under5K"],$totalPriceInt,2);
			}
			//print_pre($commissionSummaryByOwner);
			ksort($commissionDetailsByOwner[$month]);
			
			
			$reportId = "00OG0000007kYfc"; //Commissions/Commissions Item Over 5k-Testing
			$reportName = "Commissions Item Over 5k-Testing";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportResults);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$closeDate = $rowDetail["CLOSE_DATE"];
				$stageName = $rowDetail["STAGE_NAME"];
				$owner = $rowDetail["FULL_NAME"];
				
				$opportunityName = $rowDetail["OPPORTUNITY_NAME"];
				$opportunityNameLink = $rowDetail["OpportunityNameLinkValue"];
				$opportunityNameHref = "<a href='".$instance_url."/".$opportunityNameLink."' target='".$opportunityNameLink."'>".$opportunityName."</a>";

				$productName = $rowDetail["PRODUCT_NAME"];
				$productNameLink = $rowDetail["ItemNameLinkValue"];
				$productNameHref = "<a href='".$instance_url."/".$productNameLink."' target='".$productNameLink."'>".$productName."</a>";
				
				$itemName = $rowDetail["LINEITEM_DESCRIPTION"];
				$itemNameLink = $rowDetail["OpportunityLineItem.LineItemID__c"];
				$itemNameHref = "<a href='".$instance_url."/".$itemNameLink."' target='".$itemNameLink."'>".$itemName."</a>";
				$rowDetail["ITEMLINK"] = $instance_url."/".$itemNameLink;
				$rowDetail["ITEMLINKHREF"] = $itemNameHref;

				$quantity = $rowDetail["QUANTITY"];
				$unitPrice = $rowDetail["UNIT_PRICE"];
				$totalPrice = $rowDetail["TOTAL_PRICE"];
				$productPrice = $rowDetail["PRODUCT_PRICE"];
				
				$soldDate = $rowDetail["OpportunityLineItem.Date_Sold__c"];
				$soldPrice = $rowDetail["OpportunityLineItem.Actual_Sold_Price__c"];
				
				$lastModifiedBy = $rowDetail["LINEITEM_LAST_UPDATE_BY"];
				$sfIdCloseDate = strtotime($closeDate).$itemNameLink;
				if (trim($lastModifiedBy) == "Heather McCreary" && trim($soldPrice) != "-"){
					$SFOverideSoldItem[$itemNameLink] = $rowDetail;
				}

				
				$reportItems[$itemNameLink] = $rowDetail;
				$itemIDs[] = $itemNameLink;
				$itemIDsShort[] = substr($itemNameLink,-10);
				$opportunityIDs[$opportunityNameLink] = 1;

			}
			//print_pre($SFOverideSoldItem);
			
			//lookup pos item information to get ItemId
			$criteria = new stdClass();
			$criteria->SFItemIDs = $itemIDsShort;
			$paginationResult = $GBSProvider->pos_getItems($criteria,$extraSFID);
			$resultArray = $paginationResult->collection;
			//print_pre($criteria);
			//print_pre($resultArray);
			foreach ($resultArray as $result=>$data){
				$SFlookupCode = $extraSFID.str_replace($extraSFID,"",$data->SubDescription2);//standardize all SubDescriptions to match SalesForce
				$ItemsToCheckIfSoldBySFCode[$SFlookupCode][] = $data->ID;
				$ItemsToCheckIfSoldByPosID[] = $data->ID;
				$originalItemDetailsBySFCode[$SFlookupCode][] = $data;
				$originalItemDetailsByPosID[$data->ID] = $data;
				$convertposItemIDToSFItemID[$data->ID] = $SFlookupCode;
//				print_pre($data);
//				echo $data->ID;
				//now find any items that have a parent Id that of this Id
				$criteria = new stdClass();
				$criteria->posParentID = $data->ID;
				$paginationParentResult = $GBSProvider->pos_getItems($criteria,$extraSFID);
				$resultParentArray = $paginationParentResult->collection;
				foreach ($resultParentArray as $resultParent=>$record){
					$ItemsToCheckIfSoldBySFCode[$SFlookupCode][] = $record->ID;
					$ItemsToCheckIfSoldByPosID[] = $record->ID;
					$originalItemDetailsBySFCode[$SFlookupCode][] = $record;
					$originalItemDetailsByPosID[$record->ID] = $record;
					$convertposItemIDToSFItemID[$record->ID] = $SFlookupCode;
				}
			}
			//print_pre($ItemsToCheckIfSoldBySFCode);
			foreach ($ItemsToCheckIfSoldBySFCode as $sfItemCode=>$posItems){
				$criteria = new stdClass();
				$criteria->posItemIDs = $posItems;
				$paginationResult = $GBSProvider->pos_checkSoldItems($criteria);
				$resultArray = $paginationResult->collection;
				foreach ($resultArray as $result=>$record){
					$soldItemsBySFID[$sfItemCode][] = $record;
					//$soldItemsByPosID[$record->ItemID] = $record;
					$itemsToInExclude[$record->ItemID] = $itemsToInExclude[$record->ItemID]+$record->Quantity; //to eliminate voided transactions which create a quantity of negative value
				}
				
			}
			//print_pre($soldItemsBySFID);
			//print_pre($itemsToInExclude);
			foreach ($itemIDs as $itemId){
				//echo $itemId."<hr>";
				$owner = $reportItems[$itemId]["FULL_NAME"];
				$sfIdCloseDate = strtotime($reportItems[$itemId]["CLOSE_DATE"]).$itemId;
				$displayitem = array(
					"sfItemId"=>$itemId,
					"sfLinkHref"=>$reportItems[$itemId]["ITEMLINKHREF"],
					"sfLink"=>$reportItems[$itemId]["ITEMLINK"],
					"closeDate"=>$reportItems[$itemId]["CLOSE_DATE"],
					"opportunityName"=>$reportItems[$itemId]["OPPORTUNITY_NAME"],
					"owner"=>$reportItems[$itemId]["FULL_NAME"],
					"itemDescription"=>$reportItems[$itemId]["LINEITEM_DESCRIPTION"],
					"sfTotalListPrice"=>$reportItems[$itemId]["TOTAL_PRICE"]
				);
				$displayrow[$itemId]["ItemInfo"] = $displayitem;
				$over5KByOwner[$month][$owner][$sfIdCloseDate]["ItemInfo"] = $displayitem;
				if (count($SFOverideSoldItem[$itemId])){
					//print_pre($SFOverideSoldItem[$itemId]);
					$overRideData = $SFOverideSoldItem[$itemId];
					$soldPriceOverRide = trim($overRideData["OpportunityLineItem.Actual_Sold_Price__c"]);
					$soldDateOverRide = $overRideData["OpportunityLineItem.Date_Sold__c"];
					$displaysoldinfo = array(
						"description"=>$overRideData["LINEITEM_DESCRIPTION"],
						"soldPrice"=>$soldPriceOverRide,
						"transactionDate"=>$soldDateOverRide
					);
					//print_pre($displaysoldinfo);
					$displayrow[$itemId]["SoldInfo"][] = $displaysoldinfo;
					$over5KByOwner[$month][$owner][$sfIdCloseDate]["SoldInfo"][] = $displaysoldinfo;
					$soldPriceInt = str_replace(",","",str_replace("$","",$soldPriceOverRide));
					
					if (strtotime($soldDateOverRide) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($soldDateOverRide) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceEndDate)))){
						$commissionSummary5KByOwner[$month][$owner] = bcadd($commissionSummary5KByOwner[$month][$owner],$soldPriceInt,2);
						$lockCommissionData[$YearMonth][$owner]["Over5K"] = bcadd($lockCommissionData[$YearMonth][$owner]["Over5K"],$soldPriceInt,2); 
					}
				}
				if (count($soldItemsBySFID[$itemId])){
					foreach ($soldItemsBySFID[$itemId] as $soldDetails){
						$positemId = $soldDetails->ItemID;
						if ($itemsToInExclude[$positemId]){
							$thisDescription = $originalItemDetailsByPosID[$positemId]->Description;
							$thisParent = $originalItemDetailsByPosID[$positemId]->ParentItem;
							$displaysoldinfo = array(
								"description"=>$thisDescription,
								"soldPrice"=>$soldDetails->Price,
								"transactionDate"=>$soldDetails->TransactionTime,
								"parentItem"=>$thisParent
							);
							$displayrow[$itemId]["SoldInfo"][] = $displaysoldinfo;
							$over5KByOwner[$month][$owner][$sfIdCloseDate]["SoldInfo"][] = $displaysoldinfo;
							$soldPriceInt = str_replace(",","",str_replace("$","",$soldDetails->Price));
							
							if (strtotime($soldDetails->TransactionTime) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($soldDetails->TransactionTime) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceEndDate)))){
								$commissionSummary5KByOwner[$month][$owner] = bcadd($commissionSummary5KByOwner[$month][$owner],$soldPriceInt,2);
								$lockCommissionData[$YearMonth][$owner]["Over5K"] = bcadd($lockCommissionData[$YearMonth][$owner]["Over5K"],$soldPriceInt,2); 
							}
						}
						
					}
				}
				//print_pre($soldItemsBySFID[$itemId]);
				
			}
			echo "<hr><h3>Over 3K Threshold Items</h3>";
			echo "<table class='simpleTable'>
					<thead>
						<tr>
							<th>Closed Date</th>
							<th>Owner</th>
							<th>Opportunity Name</th>
							<th>Salesforce Price</th>
							<th>Salesforce Link</th>
							<th>Description</th>
							<th>Sold Date(s)</th>
							<th nowrap=nowrap>Sold Price(s)</th>
						</tr>
					</thead>
					<tbody>";
				foreach ($displayrow as $sfId=>$rowDetails){
					echo "<tr>
							<td nowrap=nowrap><span style='display:none'>".strtotime($rowDetails["ItemInfo"]["closeDate"]).$sfId."</span>".$rowDetails["ItemInfo"]["closeDate"]."</td>
							<td nowrap=nowrap>".$rowDetails["ItemInfo"]["owner"]."</td>
							<td>".$rowDetails["ItemInfo"]["opportunityName"]."</td>
							<td>".$rowDetails["ItemInfo"]["sfTotalListPrice"]."</td>
							<td>".$rowDetails["ItemInfo"]["sfLinkHref"]."</td>
							<td>".$rowDetails["ItemInfo"]["itemDescription"]."</td>
							<td>&nbsp;</td><td>&nbsp;</td>
						</tr>";
					if (count(	$rowDetails["SoldInfo"])){
						foreach ($rowDetails["SoldInfo"] as $soldDetails){
								echo "<tr>
										<td><span style='display:none'>".strtotime($rowDetails["ItemInfo"]["closeDate"]).$sfId."</span>&nbsp;</td>
										<td nowrap=nowrap>".$rowDetails["ItemInfo"]["owner"]."</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>".$rowDetails["ItemInfo"]["sfLinkHref"]."</td>
										<td>".$soldDetails["description"].((int)$soldDetails["parentItem"] > 0 ? " [piecemeal]" : "")."</td>
										<td>".date("m/d/y",strtotime($soldDetails["transactionDate"]))."<br>".date("G:i a",strtotime($soldDetails["transactionDate"]))."</td>
										<td nowrap=nowrap>$".money(str_replace(",","",str_replace("$","",$soldDetails["soldPrice"])))."</td>
									</tr>";
						}
					}
				}
					
			echo "	</tbody>
				</table><hr>";
			//print_pre($displayrow);
			
			if (!$thisYearMonthLocked){
				include_once('salesforce/reports/reportEBBComissions.php');
				echo $ReportsFileLink;
				echo "<div id='lockCommissionDataSpan'><button id='lockCommissionData'>Lock Commission Data for ".$YearMonth."</button></div>";
			}else{
				$path = getcwd();
				$pathSeparator = "/";
				$webpathSeparator = "/";
				if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}
				
				$ReportsFile = "EBB".$pathSeparator."EcoBBComissionTracking_".date("y-m", strtotime($invoiceEndDate)).".xlsx";
				$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
				
				echo "Commissions already locked for ".$YearMonth."<bR>";
				echo "<a href='".$webpathExtension."'>Download Commissions file for ".$YearMonth."</a><bR>";
			}

			echo "<h3>Detail Under 5K Threshold Items</h3>";
			
			echo "Summary:<bR>";
			foreach ($commissionSummaryByOwner as $month=>$ownerInfo){
				foreach ($ownerInfo as $owner=>$amount){
					echo $owner.": $".money($amount).($monthlyAcquisitionsExpenses[$month][$owner] > 0 ? " less $".money($monthlyAcquisitionsExpenses[$month][$owner])." acquisition expenses" : "")."<br>";
				}
			}
			//print_pre($commissionDetailsByOwner);
			echo "<table class='simpleTable'>
					<thead>
						<tr>
							<th>Owner</th>
							<th>Opportunity Name</th>
							<th>Closed Date</th>
							<th>Salesforce Link</th>
							<th>Description</th>
							<th>Salesforce Price</th>
						</tr>
					</thead>
					<tbody>";
				foreach ($commissionDetailsByOwner as $month=>$ownerInfo){
					foreach ($ownerInfo as $ownerName=>$opportunityInfo){
						foreach ($opportunityInfo as $opportunityName=>$rowInfo){
							foreach ($rowInfo as $count=>$rowDetails){
								echo "<tr>
										<td nowrap=nowrap>".$rowDetails["FULL_NAME"]."</td>
										<td>".$rowDetails["OPPORTUNITY_NAME"]."</td>
										<td nowrap=nowrap>".$rowDetails["CLOSE_DATE"]."</td>
										<td>".$rowDetails["ITEMLINKHREF"]."</td>
										<td>".$rowDetails["LINEITEM_DESCRIPTION"]."</td>
										<td>".$rowDetails["TOTAL_PRICE"]."</td>
									</tr>";
							}	
						}
					}
				}
					
			echo "	</tbody>
				</table><hr>";

		echo "</div>";
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = ($_SESSION['SalesForceReport'] ? : "EBBCommissions");
		include_once('_notesTab.php');

	echo "</div>";
?>	
<br clear="all">
The following Salesforce Reports were used:
<?php foreach ($reportLinks as $reportLink){
	echo "<br>".$reportLink;
}
?>
<br clear="all">
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. Set the date range you are looking to capture.
</ul>
<script type="text/javascript">
	$(function () {
		
		$("#lockCommissionData").on('click',function(e){
			e.preventDefault();
			var data = {};
			$(this).hide();
			$("#lockCommissionDataSpan").html('Locking Data...').addClass('alert');
			data["action"] = "LockCommissionData";
			data["lockedData"] = '[<?php echo json_encode($lockCommissionData);?>]';
			$.ajax({
				url: "ApiInvoicingManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data);
					$("#lockCommissionDataSpan").html('Locked!').addClass('info');
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$("#lockCommissionDataSpan").html(message);
				}
			});
		});
	
		
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>invoicing/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=<?php echo $nav;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>