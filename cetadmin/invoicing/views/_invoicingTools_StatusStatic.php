<h2>Invoice Status Report for August</h2>
<b>Passthrough File Status:</b> Last modified: September 16, 2016 08:24am<Br><BR>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Invoice Name/Codes</th>
						<th>Created</th>
						<th>Reviewed</th>
						<th>Approved</th>
						<th>Sent</th>
					</tr>
				</thead>
				<tbody>
					<tr>
								<td nowrap><a id="report-bgas" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-BGAS" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">BGAS 523,810,811</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/10/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/11/2016 2:29pm<br>By Mike Kania</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/13/2016 2:29pm<br>By Lorenzo Macaluso</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/14/2016 2:29pm<br>By Yosh Schulman</td>
							</tr><tr>
								<td nowrap><a id="report-bigy" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-BigY" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">BigY 540</a><Br><textarea style='width:200px;height:50px;'>No Hours this month</textarea></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><Br><i>System</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><Br><br><i>Liz Budd</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><Br><br><i>Lorenzo Macaluso</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><Br><br><i>Yosh Schulman</i></td>
							</tr><tr>
								<td nowrap><a id="report-columbiagas" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-ColumbiaGas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">Columbia Gas 525, 525D</a><br><textarea style='width:200px;height:50px;'>Removed 6 hours from Admin invoice</textarea></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Mike Kania</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Jae McAuley</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
							</tr><tr>
								<td nowrap><a id="report-covanta" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-Covanta" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">Covanta 516, 519</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By System</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Liz Budd</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Jae McAuley</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Quinn Lester</td>
							</tr><tr>
								<td nowrap><a id="report-greenteam" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-GreenTeam" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">Green Team 518</a><br><textarea style='width:200px;height:50px;'>Waiting on change of layout approval</textarea></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Yosh Schulman</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Emily Fable</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Jae McAuley</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Yosh Schulman</i></td>
							</tr><tr>
								<td nowrap><a id="report-mfep" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-MFEP" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">MFEP 560</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
							</tr><tr>
								<td nowrap><a id="report-rw" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-RWTAPipeline" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;"><?php echo ($isStaff ? "RW TA Pipeline" : "Recycling Works");?> 531-539</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/13/2016 10:29pm<br>By Emily Fabel</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/14/2016 1:45pm<br>By Mike Kania</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Yosh Schulman</i></td>
							</tr><tr>
								<td nowrap><a id="report-smallcontracthours" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-SmallContractHours" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;height:30px;">Small Contract Hours 550, 552, 570<br>591, 592, 620, 630</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/10/2016 2:29pm<br>By System</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/12/2016 2:29pm<br>By Liz Budd</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Lorenzo Macaluso</i></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/notdone.png'><br><br><br><i>Yosh Schulman</i></td>
							</tr><tr>
								<td nowrap><a id="report-trc" href="https://cetdashboard.info/cetadmininvoicing/?nav=invoicingTool-TRC" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini" style="border-color:green;">TRC 515V</a></td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/1/2016 9:19am<br>By Heather McCreary</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/2/2016 2:29pm<br>By Denice Hallstein</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/2/2016 2:35pm<br>By Denice Hallstein</td>
								<td nowrap><img src='https://cetdashboard.info/admintest/cetadmin/images/done.png'><br>9/3/2016 10:20am<br>By Heather McCreary</td>
							</tr>				</tbody>
			</table>
			<h3>Remaining Staff Tasks</h3>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Staff</th>
						<th>Tasks</th>
					</tr>
				</thead>
				<tbody>
					<tr><td nowrap>Jae McAuley</td><td><br>Approve Green Team</td></tr>
					<tr><td nowrap>Lorenzo Macaluso</td><td>Review BigY<br>Approve Small Contract Hours</td></tr>
					<tr><td nowrap>Liz Budd</td><td>Review BigY<br></td></tr>
					<tr><td nowrap>Yosh Schulman</td><td>Create Green Team<br>Send BigY<br>Send Green Team<br>Send Recycling Works<br>Send Small Contract Hours</td></tr>
					<tr><td nowrap>System</td><td>Create BigY<br></td></tr>
				</tbody>
			</table>

<script type="text/javascript">
	$(function () {
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 25,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
	});
</script>