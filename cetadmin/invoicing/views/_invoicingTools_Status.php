<?php
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$InvoicingProvider = new InvoicingProvider($dataConn);
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));

$invoicingCodes = $InvoicingProvider->getInvoicingCodes();
foreach ($invoicingCodes->collection as $result){
	$allCodes = explode(",",$result->codes);
	foreach ($allCodes as $code){
		$invoicingByCode[$code] = $result;
	}
	$invoicingByAllCodes[$result->codes] = $result;
	$invoicingByDisplayName[$result->displayName] = $result;
}
//print_pre($invoicingByAllCodes);
//print_pre($invoicingByCode);

$ImportFileName = "importfiles/PassthroughFile".date("Y_m",strtotime($invoiceDate)).".xlsx";
//echo "ImportFileName ".$ImportFileName."<br>";
$target_file = $siteRoot.$adminFolder."invoicing/".$ImportFileName;
$passThroughFile = "Not Yet Uploaded for ".date("F Y",strtotime($invoiceDate));
$passThroughColorTrue = "green";
$passThroughColorFalse = "orange";
$passThroughColor = $passThroughColorFalse;
if (file_exists($target_file)){
	$passThroughFile = "Last modified: ".date("F d, Y H:ia",filemtime($target_file));
	$passThroughColor = $passThroughColorTrue;
}


//print_pre($passThroughsByJobID);
echo "<br><br><b>Passthrough File Status:</b> ".$passThroughFile;
?>
<Br><BR>
			<table class="simpleTable">
				<thead>
					<tr>
						<th>Invoice Name/Codes</th>
						<th>Created</th>
						<th>Reviewed</th>
						<th>Approved</th>
						<th>Sent</th>
						<th>Notes</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($invoicingByDisplayName as $displayName=>$results){
							echo "<tr>
								<td nowrap>".$displayName."</td>
								<td nowrap><img src='".$CurrentServer.$adminFolder."images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='".$CurrentServer.$adminFolder."images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='".$CurrentServer.$adminFolder."images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><img src='".$CurrentServer.$adminFolder."images/done.png'><br>9/12/2016 2:29pm<br>By Yosh Schulman</td>
								<td nowrap><textarea style='width:280px;'>Yosh Creates->Mike Reviews->Jae Approves</textarea></td>
							</tr>";
						}
					?>
				</tbody>
			</table>

<script type="text/javascript">
	$(function () {
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
	});
</script>