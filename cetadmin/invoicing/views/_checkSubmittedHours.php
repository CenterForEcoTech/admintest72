<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
//include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");


//Get GBSSupervised
$gbsProvider = new HoursProvider($dataConn);
$hrEmployeeProvider = new HREmployeeProvider($dataConn);



$paginationResult = $hrEmployeeProvider->getStaffHoursMatrixEmployeeIDs();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$employeesWithHoursCodes[] = $record->EmployeeIDs;
}
//print_pre($employeesWithHoursCodes);

$criteria = new stdClass();
$criteria->withIDs = implode(",",$employeesWithHoursCodes);
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);

foreach ($resultArray as $result=>$record){
	if ($record->displayOrderId){
		$EmployeeByID[$record->id] = $record;
		$EmployeeEmailByID[$record->id] = $record->fullName." <".$record->email.">";
		
		$EmployeesByManager[]=$record;
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
//print_pre($EmployeesByManager);
$yearStartMonday = "2016-01-04";
$todaysDate = ($_GET['StartDate'] ? : $TodaysDate);
$today = date("D",strtotime($todaysDate));
$isTodayMonday = ($today == "Mon" ? true : false);
if ($isTodayMonday){
	$weeksSinceStart = datediff($yearStartMonday,$todaysDate,"week");
	$isPayPeriodStart = ($weeksSinceStart & 1 ? false : true);
	echo "<Br>".$todaysDate." is ".$weeksSinceStart." weeks since start period which is ".($isPayPeriodStart ? "a" : "not a")." pay period start date";
	if ($isPayPeriodStart){
		echo "<br>There are ".count($EmployeesByManager)." employees who should have submitted hours<hr>";
		//print_pre($EmployeesByManager);
					$WeekStart = strtotime($todaysDate." - 2 weeks");
					$WeekStartOrg = $WeekStart;
					$WeekEnd = strtotime($todaysDate." - 1 day");
					
					$criteria = new stdClass();
					$criteria->startDate = date("Y-m-d",$WeekStart);
					$criteria->endDate = date("Y-m-d",$WeekEnd);
					$criteria->statusOr = "SubmittedORApproved";
					$paginationResult = $gbsProvider->getHours($criteria);
					$resultArray = $paginationResult->collection;
					foreach ($resultArray as $Hours){
						$HoursDataTotals[$Hours->hoursDate][$Hours->employeeId][$Hours->status]=bcadd($HoursDataTotals[$Hours->hoursDate][$Hours->employeeId][$Hours->status],$Hours->hours,2);
					}
					foreach ($EmployeesByManager as $recordId=>$EmployeeInfo){
						$EmployeeID = $EmployeeInfo->id;
						$WeekStart = $WeekStartOrg;
						while ($WeekStart <=  $WeekEnd){
							$hoursDate = date("Y-m-d",$WeekStart);
							$hoursAmountSubmitted = $HoursDataTotals[$hoursDate][$EmployeeID]["Submitted"];
							$hoursAmountApproved = $HoursDataTotals[$hoursDate][$EmployeeID]["Approved"];
							if ($hoursAmountApproved || $hoursAmountSubmitted){
								if (datediff(date("m/d/Y",$WeekStart),date("m/d/Y",$WeekStartOrg)) < 7){
									$employeeWithHoursSubmitted["Week1"][$EmployeeID] = $EmployeeInfo->fullName;
								}else{
									$employeeWithHoursSubmitted["Week2"][$EmployeeID] = $EmployeeInfo->fullName;
								}
							}
							$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
						}
					}		
//		print_pre($employeeWithHoursSubmitted);
		foreach ($employeeWithHoursSubmitted as $weekCount=>$EmployeeInfo){
			foreach ($EmployeeEmailByID as $employeeId=>$emailName){
				if (!$EmployeeInfo[$employeeId]){
					$employeesNotSubmitted[$weekCount][$employeeId] = $emailName;
				}
			}
		}
		//print_pre($employeesNotSubmitted);
		foreach ($employeesNotSubmitted as $weekCount=>$employeeInfo){
			$weekAdd = ($weekCount == "Week2" ? " +1 week" : "");
			$weekAddVal = ($weekCount == "Week2" ? 1 : 0);
			$weekStartDate = strtotime(date("m/d/Y",$WeekStartOrg).$weekAdd);
			$weekEndDate = strtotime(date("m/d/Y",$weekStartDate)." + 6 days");
			$emailAddresses = array();
			foreach ($employeeInfo as $employeeId=>$emailName){
				$emailAddresses[] = $EmployeeByID[$employeeId]->email;
				$emailList[$employeeId] = ($emailList[$employeeId]+1+$weekAddVal);
			}
		}
		foreach ($emailList as $employeeId=>$missedCount){
			$week2StartDate = strtotime(date("m/d/Y",$WeekStartOrg)." +1 week");
			switch ($missedCount){
				case 1:
					$dateRange = date("m/d/Y",$WeekStartOrg)." - ".date("m/d/Y",strtotime(date("m/d/Y",$week2StartDate)." -1 day"));
					$zeroHourText = "this week, then enter 0 in any one of the days";
					break;
				case 2:
					$dateRange = date("m/d/Y",$week2StartDate)." - ".date("m/d/Y",$WeekEnd);
					$zeroHourText = "this week, then enter 0 in any one of the days";
					break;
				case 3:
					$dateRange = date("m/d/Y",$WeekStartOrg)." - ".date("m/d/Y",$WeekEnd);
					$zeroHourText = "both weeks, then enter 0 in any one of the days for each week";
					break;
			}
			$emailsByRange[$dateRange]["ids"][]= $employeeId;
			$emailsByRange[$dateRange]["emails"][]= $EmployeeByID[$employeeId]->email;
			$emailsByRange[$dateRange]["fullName"][]= $EmployeeByID[$employeeId]->fullName;
			$emailsByRange[$dateRange]["zeroHoursText"]= $zeroHourText;
		}
		ksort($emailsByRange);
		$emailOutput = "";
		foreach ($emailsByRange as $dateRange=>$employeeInfo){
			$thisMessage = "Good Morning<br>\r\nYou are receiving this email because you have not submitted hours through ".$dateRange." in the dashboard.<br>\r\nPlease log on and submit your hours as soon as possible.  If you need access to new codes or are having trouble logging in please contact Liz Budd.<br>\r\nIf you had zero hours for ".$employeeInfo["zeroHoursText"]." for a code and then save/submit<br>\r\n";
			echo "<hr>";
			echo "<br>Subject: Hours Not Yet Submitted ".$dateRange;
			$emailOutput .= "\r\n\r\nSubject: Hours Not Yet Submitted ".$dateRange."\r\n";
			echo "<br>bcc:<br><div style='border:1pt solid black;margin-left:20px;padding:5px;'>".implode("; ",$employeeInfo["emails"])."</div>";
			$emailOutput .= implode("; ",$employeeInfo["emails"])."\r\n\r\n";
			//echo "<div style='border:1pt solid black;'><span>".implode(",&nbsp;&nbsp;&nbsp;</span><span>",$employeeInfo["fullName"])."&nbsp;&nbsp;&nbsp;</span></div>";
			echo "message:<div style='border:1pt solid black;margin-left:20px;padding:5px;'>".$thisMessage."<br>Sincerely,<br>The billing team.</div>"; 
			$emailOutput .= $thisMessage;
		}
		
		if (count($emailsByRange)){
			$path = getcwd();
			$pathSeparator = "/";
			if (!strpos($path,"xampp")){
				$thisPath = ".:/home/cetdash/pear/share/pear";
				set_include_path($thisPath);
			}
			require_once "Mail.php";  
			$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
			$to = "Yosh Schulman <yschulman@gmail.com>"; 
			$recipients = $to.",Yosh Schulman <yosh.schulman@cetonline.org>";
			$subject = "CET Hours Entry Reminder"; 
			$host = "smtp.office365.com"; 
			$port = "587"; 
			$username = $_SESSION['AdminEmail']; 
			$password = $_SESSION['AdminAuthenticationCode'];  
			$headers = array (
				'From' => $from,   
				'To' => $to, 
				'Reply-to' => $from,
				'Subject' => $subject
			); 
			$smtp = Mail::factory(
				'smtp',   
				array (
					'host' => $host,     
					'port' => $port,     
					'auth' => true,     
					'username' => $username,     
					'password' => $password
				)
			);  
			$body = "https://cetdashboard.info//admintest/cetadmin/invoicing/?nav=check-submittedhours \r\n\r\n".str_replace("<br>","",$emailOutput);
			$mail = $smtp->send($recipients, $headers, $body); 
			//print_pre($mail);
		}
	}
}else{
	echo "<br>Today: ".$todaysDate." is Not a Monday<br>To force a specific date add querystring variable StartDate=mm/dd/YYYY format to URL<Br>(example ?nav=check-submittedhours&StartDate=10/24/2016";
}
?>