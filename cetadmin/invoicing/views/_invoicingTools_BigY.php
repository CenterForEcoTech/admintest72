<style>
	table {font-family: "Calibri";font-size:11pt;}
</style>
<h3>This tool will create the Big Y Invoice file</h3>
<?php
$_SESSION['SalesForceReport'] = "BigY";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date()." -1 month")));
$invoiceStartDate = $invoiceDate;
$invoiceEndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime(date()." -1 month")));

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");

$GBSProvider = new GBSProvider($dataConn);

//Get Billing Rates
$billingRatesArray = array("BigY"=>array("540"));
foreach ($billingRatesArray as $rateType=>$rateCodes){
	//Get Billing Rates
	$criteria = new stdClass();
	$criteria->invoiceName = $rateType;
	$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
		$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
		foreach ($EmployeeMembers as $Employee){
			$BillingRateByEmployee[$Employee][$rateType][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		}
	}
}
//print_pre($billingRatesArray);

$setTab = "Hours";
	$TabCodes = array("Hours"=>"Hours","Invoice"=>"Invoice","Adjustments"=>"Adjustments","Notes"=>"Notes");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
		echo "<div id='Hours' class='tab-content'>";
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "BigY";
			$SelectedGroupsID = 13;
			include('views/_reportByCode.php');
			//print_pre($CodesByGroupName);
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
//			print_pre($InvoiceYearMonths);
//			print_pre($InvoiceDataByMonth);
//			print_pre($InvoiceData);
			foreach ($InvoiceData as $Employee_Name=>$hours){
	//				echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
	//				print_pre($hours);
				foreach ($hours as $code=>$hour){
					if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
						$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
						$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
					}
				}
			}
			ksort($InvoiceHourData);
			foreach ($BillingRateByCategory as $BillingRateType=>$BillingRateTypeInfo){
				foreach ($BillingRateTypeInfo as $BillingCategory=>$RateInfo){
					foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
						if (in_array($InvoiceCode,$billingRatesArray[$BillingRateType])){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
							foreach ($EmployeeHours as $Employee_Name=>$hours){
								if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingRateType][$BillingCategory]){
									if ($hours && $hours != "0.00"){
										$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
										$IncludedEmployees[] = $Employee_Name;
									}
								}
								if ($BillingCategory == "Staff" && !in_array($Employee_Name,$IncludedEmployees)){
									$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
									$Adjustments["Alert"]["Staff missing Billing Rate"][]=str_replace(", ","_",$Employee_Name);
								}
							}
						}
					}
				}
			}
			ksort($InvoiceDataHours);
			//print_pre($InvoiceDataHours);
		echo "</div>";
		echo "<div id='Invoice' class='tab-content'>";
				$invoiceDate550A = $invoiceDate;
				include_once('salesforce/reports/reportBigY.php');
				echo $ReportsFileLink;
		echo "</div>";
		include_once('_adjustmentsTab.php');
		$invoiceCodeName = "BigY";
		include_once('_notesTab.php');
	echo "</div>";
?>	
	</div> <!--end tabs-->
<br clear="all">	
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">	
<b>Instructions</b>
<ul>
	<li<?php echo ($_GET['GroupID'] ? " class='alert info'" : "");?>>1. In the Hours tab set the date range and Status you are looking to capture.
	<li<?php echo ($billableHoursFileReceived ? " class='alert info'" : "");?>>3. In the Invoice tab, download the <?php echo $ReportsFileLink;?> File.
</ul>
<script type="text/javascript">
	$(function () {
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
	});
</script>