<?php
//error_reporting(E_ALL);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$_SESSION['SalesForceReport'] = "RWTAPipeline";
foreach($_GET as $key=>$val){$salesForceQueryString .= "&".$key."=".$val;}
$_SESSION['SalesForceQueryString'] = $salesForceQueryString;

include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->revenueCode){
		$EmployeesRevenueCodeByName[$record->lastName."_".$record->firstName]=$record->revenueCode;
	}
}


include_once($dbProviderFolder."GBSProvider.php");

$GBSProvider = new GBSProvider($dataConn);
$invoiceDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/01/Y",strtotime(date("m/01/Y")." -1 month")));
//echo $invoiceDate;
$lastDateofInvoiceDate = date("m/t/Y",strtotime($invoiceDate));

$NONQuartersStart = (date("m") < 7 ? (date("Y")-1) : date("Y"))."-07-01";
$invoiceQuarter = $NONQuartersStart;
while (strtotime(date("Y-m-d",strtotime(date()."-1 month"))) >= strtotime($NONQuartersStart)){
	$invoiceQuarter = $NONQuartersStart;
	$NONQuartersStart = date("Y-m-d",strtotime($NONQuartersStart."+3 Months"));
}

$CurrentYear = (date("n") > 7 ? date("Y") : (date("Y")-1));
if (date("m") == "01"){
	$CurrentYear = $CurrentYear-1;
}
$NextYear = $CurrentYear+1;
$CurrentFiscalYear = (date("n") > 7 ? $CurrentYear : $NextYear);
$CurrentFiscalYear = $CurrentYear;
$CurrentFY = $CurrentFiscalYear."-07-01";
echo "Current FY: ".$CurrentFY;
if(count($_FILES['filesToUpload'])) {
	$fileReceived = true;
	foreach ($_FILES['filesToUpload']["tmp_name"] as $fileKey=>$fileValue) {
		$folderLocationToGet = $fileValue;
		$fileLocations[] = $folderLocationToGet;
	}
}

//Get Billing Rates
$criteria = new stdClass();
$criteria->invoiceName = "RecyclingWorks";
$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	$BillingRateByCategory[$record->GBSBillingRate_Name]=$record;
	foreach ($EmployeeMembers as $Employee){
		$BillingRateByEmployee[$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
		$BillingRateByEmployeeJustRate[$Employee] = $record->GBSBillingRate_Rate;
	}
}

function csv_to_array($string='', $row_delimiter=PHP_EOL, $delimiter = "," , $enclosure = '"' , $escape = "\\" )
{
    $rows = array_filter(explode($row_delimiter, $string));
    $header = NULL;
    $data = array();

    foreach($rows as $row)
    {
        $row = str_getcsv ($row, $delimiter, $enclosure , $escape);
		
        if(!$header)
            $header = $row;
        else
            $data[] = array_combine($header, $row);
    }

    return $data;
}

$csvRows = array();
foreach ($fileLocations as $file){
		$fileLocation = str_replace("\\","/",$file);
		$csvstring = file_get_contents($fileLocation,"r");
		$csv[] = csv_to_array($csvstring,"\r\n");
		$csvRows = $csvRows + $csv;
}
//print_pre($csv);
//now combine each of the csv into one array
foreach ($csv as $id=>$csvRowData){
	foreach ($csvRowData as $row=>$rowInfo){
		$csvCombined[] = $rowInfo;
	}
}
//check for consistancy
foreach ($csvCombined as $rowId=>$rowContent){
	if (!is_array($rowContent)){
		echo "row ".$rowId." has bad formating<br>";
	}
}
//print_pre($csvCombined);
if ($isStaff){
	echo "<h3>This tool will create RW Report, NON Pipeline, and TA Pipeline Report</h3>";
	$setMasterTab = "NON";
	$masterTabCodes = array("NON Pipeline Report"=>"NON","TA Pipeline Report"=>"RWReport","RW Report"=>"Bullets","Adjustments"=>"Adjustments","Notes"=>"Notes");
}else{
	echo "<h3>This tool will create RW Invoice</h3>";
	$setMasterTab = "Hours";
	$masterTabCodes = array("Hours"=>"Hours","RW Invoice"=>"Bullets","Adjustments"=>"Adjustments","Notes"=>"Notes");
}
foreach ($masterTabCodes as $TabName=>$TabCode){
	$masterTabHashes[] = $TabCode;
	$masterTabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'">'.$TabName.'</a></li>';
}
echo '<ul id="admin-tabs" class="tabs">';
		foreach ($masterTabListItems as $TabItem){
			echo $TabItem;
		}
echo '</ul>';		
echo '<div class="tabs-content">';
if (!$isStaff){
	echo "<div id='Hours' class='tab-content'>";
		$InvoiceTool = true;
		$InvoicingToolCodeGroup = "Recycling Works";
		$SelectedGroupsID = 12; //Recycling Works
		include('views/_reportByCode.php');
		//echo "<br clear='all'>";
		$IncludedEmployees = array();
		$ExcludedEmployees = array();
		foreach ($InvoiceData as $Employee_Name=>$hours){
//				echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
//				print_pre($hours);
			foreach ($hours as $code=>$hour){
				if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
					$code = ($code == "534" || $code == "539" || $code == "537A" || $code == "537B" ? "534/539/537AB" : $code);
					
					$InvoiceHourData[$code][$Employee_Name] = $InvoiceHourData[$code][$Employee_Name]+$hour;
					$InvoiceHourDataByCode[$code] = $InvoiceHourDataByCode[$code]+$hour;
				}
			}
		}
		ksort($InvoiceHourData);
		//print_pre($InvoiceHourData);
		foreach ($BillingRateByCategory as $BillingCategory=>$RateInfo){
			foreach ($InvoiceHourData as $InvoiceCode=>$EmployeeHours){
				//echo "InvoiceCode ".$InvoiceCode."=";
				//echo $InvoiceCode."<br>";
				$InvoiceDataHours[$InvoiceCode][$BillingCategory] = array();
				foreach ($EmployeeHours as $Employee_Name=>$hours){
					if (!$EmployeesRevenueCodeByName[$Employee_Name]){
						$Adjustments["Alert"]["Staff missing Revenue Code"][]=str_replace("_",", ",$Employee_Name);
					}
					if ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$BillingCategory]){
						if ($hours && $hours != "0.00"){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							$IncludedEmployees[] = $Employee_Name;
						}
					}
					if ($Employee_Name != "Cook_Josh"){ //ignore Josh Cook because billed at flat rate of hours
						if ($BillingCategory == "EcoFellow" && !in_array($Employee_Name,$IncludedEmployees)){
							$InvoiceDataHours[$InvoiceCode][$BillingCategory][$Employee_Name]=$hours;
							$Adjustments["Alert"]["Staff missing Billing Rate"][]=str_replace(", ","_",$Employee_Name);
						}
					}
				}
			}
			
		}
		$InvoiceDataHours["DEP"]["NoCategory"]["Macaluso_Lorenzo"] = 2.50; //updated 11/14/2016
		$InvoiceDataHours["DEP"]["NoCategory"]["Cook_Josh"] = 82.00; //updated 11/14/2016
		//print_pre($InvoiceDataHours);
		
		
		//print_pre($InvoiceHourData);
	
	echo "</div>";
}//end if !isStaff
require_once 'salesforce/config.php';
require_once('salesforce/rest_functions.php');
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
$reportId = "00OU0000003e4x6"; //Invoicing/RW TA In-Progress

//echo "checking Session:".checkSession($instance_url, $reportId, $access_token);
if (!checkSession($instance_url, $reportId, $access_token)){
	session_start();
	$_SESSION['SalesForceReport'] = "BGAS";
	$_SESSION['SalesForceQueryString'] = $salesForceQueryString;
	echo "<script>window.location.replace('".AUTH_URL."');</script>";
	//echo AUTH_URL;
	//header('Location: '.AUTH_URL);
}else{					
	ob_start();
	
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from session!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from session!");
	}
}//end if checkSession
function normalizeAccountName($name){
	$name = str_replace(".","",$name);
	$name = str_replace(" ","",$name);
	$name = str_replace("'","",$name);
	$name = str_replace(",","",$name);
	$name = strtolower($name);
	return $name;
}


if ($isStaff){
	echo "<div id='NON' class='tab-content'>";
			$ImportFileName = "importfiles/NONFile".date("Y_m",strtotime($invoiceQuarter)).".xlsx";
			//echo "ImportFileName ".$ImportFileName."<br>";
			$target_file = $siteRoot.$adminFolder."invoicing/".$ImportFileName;
			$link_file = $CurrentServer.$adminFolder."invoicing/".$ImportFileName;
			if(count($_FILES['filesToUpload1'])) {
				$tmpFileName = $_FILES["filesToUpload1"]["tmp_name"][0];
				if ($_GET['type'] == "nonFile"){
					$ImportFileReceived = true;
					move_uploaded_file($tmpFileName, $target_file);
					//$results = $GBSProvider->invoiceTool_loadExternalData($target_file,"gbs_bgas_customer_accounts");
					//print_pre($results);
				}
			}
			if ($ImportFileReceived || $_GET['useUploadedFile']){
				//new import that data and parse it out 
				$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
				$_SESSION['ImportFileName'] = $ImportFileName;
				echo "Reading <a href='".$link_file."'>uploaded NON file</a><br>";
				ob_flush();
				include_once('salesforce/read_NONFile.php');
			}
			if (!$ImportFileReceived){
				if (file_exists($target_file)){
					$lastUploadedDateParts = explode("File",$ImportFileName);
					$lastNONUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
					echo "Using <a href='".$link_file."'>Existing Uploaded NON File</a> last uploaded ".date("F Y",strtotime($lastNONUploadedDate))."<br>";
					$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/".$ImportFileName : $ImportFileName);
					$trackingFile = $ImportFileName;
					include_once('salesforce/read_NONFile.php');
				}else{
					echo "No Prior NON File uploaded.  Please upload the most recent NON File<br>";
				}
			}
		
	?>
				<a id='updateNONFile' href="#" class='button-link do-not-navigate'>Update NON File</a><br>
				<div id="updateNONFileForm" style='display:none;'>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>invoicing/?nav=invoicingTool-RWTAPipeline&type=nonFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload1[]" id="filesToUpload1" multiple="" onChange="makeFileList1();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList1"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList1() {
								var input = document.getElementById("filesToUpload1");
								var ul = document.getElementById("fileList1");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton1').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton1').style.display = 'none';
								}
							}
						</script>
					</form>
				</div>
		<?php
			$reportId = "00OU0000003e4x6"; //Invoicing/RW TA In-Progress
			$reportName = "RW TA In-Progress";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignNameParts = explode(" ",$primaryCampaignName);
				$primaryCampaignCode = ($primaryCampaignNameParts[0] == "534" || $primaryCampaignNameParts[0] == "537A" || $primaryCampaignNameParts[0] == "537B" ? "534/537AB" : $primaryCampaignNameParts[0]); 
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$DEPReportSubmitted = $rowDetail["Upgrade__c.RW_DEP_Summary_Report_Submitted__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$billingAddress = str_replacE("<br>","",$rowDetail["Account.BillingAddress"]);
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				$RWTAInProgress[$AccountNameLink] = $rowDetail;
				$TAInfo = array("Account Name"=> $accountName,
					"Initiation Date" => date("m/d/Y",strtotime($initiationDate)),
                    "Last Action Date" => date("m/d/Y",strtotime($lastActionDate)),
					"Green Prospect Phase" => $greenProspectPhase,
					"Pipeline" => $pipeline,
					"RW DEP Summary Report Submitted" => $DEPReportSubmitted,
                    "Owner: Full Name" => $owner,
					"Green Prospect Name" => $greenProspectName,
					"Green Prospect Type" => $prospectType,
					"Number" => $number,
                    "Unit" => $unit,
                    "Material" => $material,
                    "Other: Explain" => $otherExplain,
					"Current Status Detail" => $statusDetail,
                    "Address" => $billingAddress,
					"Primary Campaign: Campaign Name" => $primaryCampaignName,
					"PrimaryCampaignCode" => $primaryCampaignCode
					);
				$TAPipelineItems[] = $TAInfo;	
			}
			//echo $reportLink;
			//print_pre($TAPipelineItems);
			
			$reportId = "00OU0000003e4xz"; //Invoicing/RW TA Completed 2015-2016
			$reportName = "RW TA Completed 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//			print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignNameParts = explode(" " ,$primaryCampaignName);
				$primaryCampaignCode = ($primaryCampaignNameParts[0] == "534" || $primaryCampaignNameParts[0] == "537A" || $primaryCampaignNameParts[0] == "537B" ? "534/537AB" : $primaryCampaignNameParts[0]); 
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$DEPReportSubmitted = $rowDetail["Upgrade__c.RW_DEP_Summary_Report_Submitted__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$number = $rowDetail["Metric__c.Number__c"];
				$unit = $rowDetail["Metric__c.Unit__c"];
				$material = $rowDetail["Metric__c.Material__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$billingAddress = str_replace("<br>","",$rowDetail["Account.BillingAddress"]);
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$otherExplain = $rowDetail["Metric__c.Other_Explain__c"];
				if ($primaryCampaignName != "539 Mini TA"){
					$RWTACompleted[$AccountNameLink] = $rowDetail;
				}
				$TAInfo = array("Account Name"=> $accountName,
					"Initiation Date" => date("m/d/Y",strtotime($initiationDate)),
                    "Last Action Date" => date("m/d/Y",strtotime($lastActionDate)),
					"Green Prospect Phase" => $greenProspectPhase,
					"Pipeline" => $pipeline,
					"RW DEP Summary Report Submitted" => $DEPReportSubmitted,
                    "Owner: Full Name" => $owner,
					"Green Prospect Name" => $greenProspectName,
					"Green Prospect Type" => $prospectType,
					"Number" => $number,
                    "Unit" => $unit,
                    "Material" => $material,
                    "Other: Explain" => $otherExplain,
					"Current Status Detail" => $statusDetail,
                    "Address" => $billingAddress,
					"Primary Campaign: Campaign Name" => $primaryCampaignName,
					"PrimaryCampaignCode" => $primaryCampaignCode
					);
				$TAPipelineItems[] = $TAInfo;	
			}
			//echo $reportLink;
			//print_pre($TAPipelineItems);
			
			//find any outliers who had previous closed TA's so that if they  have a current TA they get 'Yes'
			$reportId = "00O0P000002yS7X"; //Invoicing/RW NON Completed
			$reportName = "RW TA Complete Prior to Current FY";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$RWTACompletedOutlier[$AccountNameLink] = $accountName;
			}

			//print_pre($RWTACompletedOutlier);
			
			$reportId = "00OU0000003e1Sb"; //Invoicing/RW NON Completed
			$reportName = "RW NON Completed 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$NONCompletedReport[$AccountNameLink] = 1;
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$stageCompleted = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				//echo strtotime($lastNONUploadedDate)." > ".strtotime($lastActionDate)."<br>";
				$inTAPipeline = ($RWTACompleted[$AccountNameLink] || $RWTAInProgress[$AccountNameLink] || $RWTACompletedOutlier[$AccountNameLink] ? true : false);
				$NONType = ($NONDataNormalized[normalizeAccountName($accountName)] ? "outreach" : "completed");
				$NONType = (strtotime($lastNONUploadedDate) > strtotime($lastActionDate) ? $NONType : "completed");
				$NONType = ($NONType=="outreach" && !strpos(" ".$primaryCampaignName,"533") ? "TA" : $NONType);
				$TA = ($inTAPipeline ? "Yes" : "");
				$TA = ($inTAPipeline && strpos(" ".$primaryCampaignName,"539") && $NONType == "TA" ? "" : $TA);
				if ($greenProspectPhase == "Completed-Measure Not Implemented"){$NONType = "completed";$TA = "";}
				$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase);
//				echo "completed '".normalizeAccountName($accountName)."'<br>";
				$NONAccounts[normalizeAccountName($accountName)][$NONType][$greenProspectPhase][$lastActionDate][] = $infoArray;
				$NONOutReach[normalizeAccountName($accountName)] = $infoArray;
				$NONOutReachByStatus[$NONType][$AccountNameLink] = $infoArray;
				$NONOutReachByAddress[$normalizedAddress] = normalizeAccountName($accountName);
				$NONOutReachByAccount[$AccountNameLink] = $infoArray;
				//$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink);
			}


			
			$reportId = "00OU0000003e1SM"; //Invoicing/RW NON Outreach GPs
			$reportName = "RW NON Outreach GPs 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingStreet"]);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				$addressToLower = trim(strtolower($address));
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$pipeline = $rowDetail["Upgrade__c.Pipeline__c"];
				$opportunityLead = $rowDetail["Upgrade__c.Opportunity_Lead_Origination__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$stageCompleted = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$NONType = (strpos(" ".$greenProspectPhase,"Completed") ? "completed" : "outreach");
				$inTAPipeline = ($RWTACompleted[$AccountNameLink] || $RWTAInProgress[$AccountNameLink] ? true : false);
				$NONType = ($NONType=="outreach" && !strpos(" ".$primaryCampaignName,"533") ? "TA" : $NONType);  //disabled August 2016 to combine with outreach
				$TA = ($inTAPipeline ? "Yes" : "");
				$TA = ($inTAPipeline && strpos(" ".$primaryCampaignName,"539") && $NONType == "TA" ? "" : $TA);  //disabled August 2016 to combine with outreach
				$NONType = (strpos(" ".$greenProspectPhase,"Completed") ? "completed" : "outreach"); //added August 2016 to only have two groups ie eliminate TA type
				if ($greenProspectPhase == "Completed-Measure Not Implemented"){$NONType = "completed";$TA = "";}
				$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase);
				if ($NONType == "completed" && $NONAccounts[normalizeAccountName($accountName)]["TA"]){
					unset($NONAccounts[normalizeAccountName($accountName)]["TA"]);
				}
				$includeThisItem = true;
				//have only completed items for current FY
				$withinFY = ($NONType == "completed" && strtotime($lastActionDate) >= strtotime($CurrentFY) ? true : false);
				if ($NONType == "completed"){
					//echo $accountName." ".$NONType." ".$lastActionDate." >=".$CurrentFY." '".$withinFY."'<br>";
					if ($withinFY){
						$includeThisItem = true;
					}else{
						$includeThisItem = false;
					}
				}
				if ($includeThisItem){
					$NONAccounts[normalizeAccountName($accountName)][$NONType][$greenProspectPhase][$lastActionDate][] = $infoArray;
				
					//echo "outreach '".normalizeAccountName($accountName)."'<br>";
					$Added[] = normalizeAccountName($accountName);
					$NONOutReach[normalizeAccountName($accountName)] = $infoArray;
					$NONOutReachByStatus[$NONType][$AccountNameLink] = $infoArray;
					$NONOutReachByAddress[$normalizedAddress] = normalizeAccountName($accountName);
					$NONOutReachByAccount[$AccountNameLink] = $infoArray;
					//$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink);
				}
			}
			
			ksort($NONAccounts);
			
			//print_pre($NONOutReachByAccount);


			$reportId = "00OU0000003e10X"; //Invoicing/Business Accounts
			$reportName = "Business Accounts";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//			print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$address = str_replace("<br>","",$rowDetail["Account.BillingAddress"]);
				$addressParts = explode(",",$address);
				$city = strtolower($rowDetail["Account.BillingCity"]);
				//$address = $street.", ".$city;
				$addressToLower = trim(strtolower($addressParts[0]));
				//$normalizedAddress = rtrim(str_replace(" street","",trim(strtolower($addressParts[0])))," st");
				$has_street = (substr($addressToLower,-7)==" street" ? true : false);
				$has_st = (substr($addressToLower,-3)==" st" ? true : false);
				$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
				$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$greenProspectStage = $rowDetail["Upgrade__c.Green_Prospect_Stage__c"];
				$greenProspectStatus = $rowDetail["Upgrade__c.Green_Prospect_Status__c"];
				$greenProspectDetails = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$gpDetails = array("stage"=>$greenProspectStage,"status"=>$greenProspectStatus,"details"=>$greenProspectDetails);
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$SFAccounts[normalizeAccountName($accountName)][$GPNameLink] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"GPInfo"=>$gpDetails,"reportLink"=>$reportLink);
				$SFAccounts[normalizeAccountName($accountName)]["accountInfo"] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"reportLink"=>$reportLink);
				$SFAccountsByGPId[$GPNameLink][] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"GPInfo"=>$gpDetails,"reportLink"=>$reportLink);
				$SFAccountsByAddress[$normalizedAddress] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"addressStrToLower"=>strtolower($addressParts[0]),"addressStrReplace_street"=>str_replace(" street","",strtolower($addressParts[0])),"has_street"=>$has_street,"has_st"=>$has_st,"city"=>$city);
				$SFAccountsByAccountLink[$AccountNameLink] = array("name"=>$accountName,"accountNameLink"=>$AccountNameLink,"accountNameHref"=>$AccountNameHref,"address"=>$address,"addressStrToLower"=>strtolower($addressParts[0]),"addressStrReplace_street"=>str_replace(" street","",strtolower($addressParts[0])),"has_street"=>$has_street,"has_st"=>$has_st,"city"=>$city);
			}
			//print_pre($SFAccountsByAddress);
			//print_pre($NONData);
			if (count($NONData)){
				foreach ($NONData as $NONRow){
					$accountName = trim($NONRow["reg_obj_name"]);
					$billingAddress = trim($NONRow["reg_obj_mail_addr"]);
					$billingCity = trim($NONRow["town_name"]);
					
					$addressToLower = strtolower($billingAddress);
					$has_street = (substr($addressToLower,-7)==" street" ? true : false);
					$has_st = (substr($addressToLower,-3)==" st" ? true : false);
					$normalizedAddress = ($has_street ? str_replace_last(" street","",$addressToLower) : $addressToLower);
					$normalizedAddress = ($has_st ? str_replace_last(" st","",$normalizedAddress) : $normalizedAddress);
					if ($accountName){
						$matchFound = false;
						if (count($SFAccounts[normalizeAccountName($accountName)])){
							$matchFound = true;
							if ($SFAccountsByAddress[$normalizedAddress]){
								$Adjustments["Review"]["NON Accounts found via Name and Address exact match"][] = $SFAccountsByAddress[$normalizedAddress]["accountNameHref"];
								$accountNameLink = $SFAccountsByAddress[$normalizedAddress]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
							}else{
								$Adjustments["Alert"]["NON Accounts found via Name exact match but with mismatched Address"][] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["accountNameHref"]."<Br>Data in Sales Force:&nbsp;&nbsp;".$SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"]."<Br>Data in NON Report: ". $billingAddress.", ".$billingCity;
								$accountNameLink = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
							}
							$AccountNamesLookup[trim($accountName)] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"];
							$AccountNamesLookup[$SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["name"]] = $SFAccounts[normalizeAccountName($accountName)]["accountInfo"]["address"];
							
						}else{
							if ($SFAccountsByAddress[$normalizedAddress]){
								$matchFound = true;
								$Adjustments["Alert"]["NON Accounts only found by Address exact match but with mismatched Name"][] = strtoupper($SFAccountsByAddress[$normalizedAddress]["accountNameHref"])." vs NON Data: ".$accountName;
								$accountNameLink = $SFAccountsByAddress[$normalizedAddress]["accountNameLink"];
								$NONType = $NONOutReachByAccount[$accountNameLink]["NONType"];
								$AccountNamesLookup[trim($accountName)] = $normalizedAddress;
								$AccountNamesLookup[$SFAccountsByAddress[$normalizedAddress]["name"]] = $normalizedAddress;
							}
						}
						$NONType = ($NONType ? : "outreach");
						if (!$matchFound){
							//last check 
							if ($NONAccounts[normalizeAccountName($accountName)]){
								$Adjustments["Review"]["NON Accounts found in NONAccounts"][] = $accountName;
								foreach ($NONAccounts[normalizeAccountName($accountName)] as $NONType=>$info){
									$NONType = $NONType;
								}
								$AccountNamesLookup[trim($accountName)] = $normalizedAddress;
								$NONAccountsFound[$NONType][] = $NONOutReach[normalizeAccountName($accountName)]["accountNameLink"];
							}else{
								$thisZip = retrieve_zipcode($billingAddress.", ".$billingCity." MA ",GOOGLE_APIKEY);
								$nameParts = explode(" ",$accountName);
								$addLink = "<a title='click to prepopulate Add Account in SalesForce' target='SalesForceAddAccount' href='".$instance_url."/001/e?retURL=%2F001%2Fo&RecordType=012U00000009QUy&ent=Account&acc2=".urlencode($accountName)."&acc17street=".urlencode($billingAddress)."&acc17city=".urlencode($billingCity)."&acc17state=MA&acc17zip=".$thisZip."'>".$accountName."</a>";
								$searchLink = "<a title='click to search in SalesForce' target='SalesForceSearch' href='".$instance_url."//_ui/search/ui/UnifiedSearchResults?searchType=2&sen=001&sen=a01&sen=00O&str=".$nameParts[0]."'>".$accountName."</a>";
								$Adjustments["Review"]["NON New Accounts (not found in SF via Name or Address)<br><a href='".$instance_url."/001/o' target='salesForce'>Add New Account To SalesForce</a>"][] = $searchLink." ".$billingAddress.", ".$billingCity." MA ".$thisZip;
								$AccountNamesLookup[trim($accountName)] = "NEW";
								$NONAccountsNotFound[] = $searchLink." ".$billingAddress.", ".$billingCity." MA ".$thisZip;
								$accountInfoArray = array("name"=>"NEW ".ucwords($accountName),"city"=>$billingCity,"GPName"=>"NON Outreach","TA"=>"No","initiationDate"=>date("m/d/Y",strtotime($NONRow["perf_date"])),"lastActionDate"=>date("m/d/Y",strtotime($invoiceQuarter)));
								
								//$NONAccounts[ucwords($accountName)]["outreach"]["Contact-In-Progress"][$invoiceQuarter][] = $accountInfoArray;
							}
						}else{
							$NONAccountsFound[$NONType][] = $accountNameLink;
							$NONAccountsFound[$accountNameLink] = 1;
//							$Phase = $NONOutReachByStatus[$NONType][$accountNameLink]["greenProspectPhase"];
							//$infoArray = array("TA"=>$TA,"NONType"=>$NONType,"name"=>$accountName,"accountNameHref"=>$AccountNameHref,"accountNameLink"=>$AccountNameLink,"address"=>$address,"city"=>$city,"GPName"=>$greenProspectName,"GPNameLink"=>$GPNameHref,"reportLink"=>$reportLink,"lastActionDate"=>$lastActionDate,"initiationDate"=>$initiationDate,"greenProspectPhase"=>$greenProspectPhase);
							//print_pre($NONOutReachByStatus[$NONType][$accountNameLink]);
//							$NONReport[$NONType][$Phase][] = $NONOutReachByStatus[$NONType][$accountNameLink];
//							$NONReportTypeCount[$NONType]++;
						}
					}
				}
//				print_pre($NONAccountsFound);
//				print_pre($NONAccountsNotFound);
//				print_pre($NONAccounts);
				foreach ($NONAccounts as $normalizedAccountName=>$NONTypeInfo){
					$accountName = ($NONOutReach[$normalizedAccountName]["name"] ? : $accountName);
					foreach ($NONTypeInfo as $NONType=>$PhaseInfo){
						foreach ($PhaseInfo as $Phase=>$lastActionDateInfo){
							foreach ($lastActionDateInfo as $lastActionDate=>$accountInfo){
								$NONReport[$NONType][$Phase][] = $accountInfo[0];
								$NONReportTypeCount[$NONType]++;
								if (strtotime($lastActionDate) >= strtotime(date("m/01/Y",strtotime($invoiceDate))) && strtotime($lastActionDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
								$NONReportTypeCountThisMonth[$NONType]++;
								}
							}
						}
					}
				}
				include_once('salesforce/reports/RW_NON_Pipeline.php');
				echo "<Br>Download NON Pipeline Report: ".$ReportsFileLink."<br>";
				
				
				//print_pre($NONReport);
				echo "<table class='simpleTable'>";
				echo "<thead>
						<tr>
							<th>Type</th>
							<th>Account Name</th>
							<th>City</th>
							<th>Project</th>
							<th>Status</th>
							<th>Referred to TA?</th>
							<th>Initiation Date</th>
							<th>Last Action Date</th>
						</tr>
					  </thead>
					  <tbody>";
				foreach ($NONReport["completed"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						//check if date is within current FY for completed items only
						$withinFY = (strtotime($thisNONAccount["lastActionDate"]) >= strtotime($CurrentFY) ? true : false);
						if ($withinFY){
							echo "<tr>
									<td>Completed</td>
									<td>".$thisNONAccount["name"]."</td>
									<td>".ucwords($thisNONAccount["city"])."</td>
									<td>".$thisNONAccount["GPName"]."</td>
									<td>".$ProjectStatus."</td>
									<td>".$thisNONAccount["TA"]."</td>
									<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
									<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
								</tr>";
						}
					}
				}
				echo "<tr><td colspan=3>Completed: Total</td><td>".$NONReportTypeCount["completed"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				foreach ($NONReport["TA"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						echo "<tr>
								<td>Outreach to TA</td>
								<td>".$thisNONAccount["name"]."</td>
								<td>".ucwords($thisNONAccount["city"])."</td>
								<td>".$thisNONAccount["GPName"]."</td>
								<td>".$ProjectStatus."</td>
								<td>".$thisNONAccount[""]."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
							</tr>";
					}
				}
				echo "<tr><td colspan=3>Outreach to TA: Total</td><td>".$NONReportTypeCount["TA"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				foreach ($NONReport["outreach"] as $ProjectStatus=>$nonAccounts){
					foreach ($nonAccounts as $thisNONAccount){
						echo "<tr>
								<td>Projects In-Progress</td>
								<td>".$thisNONAccount["name"]."</td>
								<td>".ucwords($thisNONAccount["city"])."</td>
								<td>".$thisNONAccount["GPName"]."</td>
								<td>".$ProjectStatus."</td>
								<td>".$thisNONAccount[""]."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["initiationDate"]))."</td>
								<td>".date("n/j/y",strtotime($thisNONAccount["lastActionDate"]))."</td>
							</tr>";
					}
				}
				echo "<tr><td colspan=3>Projects In-Progress: Total</td><td>".$NONReportTypeCount["outreach"]."</td><td style='display:none;'>&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td><td style='display:none;'&nbsp;</td></tr>";
				echo "</tbody></table>";

					
				echo "<h4>Data from DEP NON file</h4>";
				echo "<table class='simpleTable'>";
				echo "<thead>
						<tr>
							<th>Name</th>
							<th>Address</th>
							<th>Town</th>
						</tr>
					  </thead>
					  <tbody>";
				foreach ($NONData as $NONRow){
					$accountName = $NONRow["reg_obj_name"];
					if ($accountName){
						echo "<tr>
								<td>".$accountName."</td>
								<td>".$NONRow["reg_obj_mail_addr"]."</td>
								<td>".$NONRow["town_name"]."</td>
							  </tr>";
					}
				}
				echo "</tbody></table>";
			}
?>
		<b>Instructions for NON Pipeline</b>
		<ul>
			<li>1. If new quarterly file from DEP is available, use the upload feature to add the new file.
			<li>2. Review the Adjustments tab to insure Accounts that are were not matched can be matched.  This may require making changes in Salesforce or ignoring the alerts/reviews.
			<li>3. If you made any changes in Salesforce refresh this page.
			<li>4. Download NON Pipeline report: <?php echo $ReportsFileLink;?>.
		</ul>

<?php
	echo "</div>";
}//end if isStaff
	echo "<div id='Bullets' class='tab-content'>";
		if (!$isStaff){
			$ImportFileName = "importfiles/DEPInternalReport".date("Y_m",strtotime($invoiceDate))."1.xlsx";
			$link_file = $CurrentServer.$adminFolder."invoicing/".$ImportFileName;
			//echo "ImportFileName ".$ImportFileName."<br>";
			$target_file = $siteRoot.$adminFolder."invoicing/".$ImportFileName;
			if(count($_FILES['filesToUpload2'])) {
				$tmpFileName = $_FILES["filesToUpload2"]["tmp_name"][0];
				if ($_GET['type'] == "DEPFile"){
					$ImportFileReceived = true;
					move_uploaded_file($tmpFileName, $target_file);
					//$results = $GBSProvider->invoiceTool_loadExternalData($target_file,"gbs_bgas_customer_accounts");
					//print_pre($results);
				}
			}
			if ($ImportFileReceived || $_GET['useUploadedFile']){
				//new import that data and parse it out 
				$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
				$_SESSION['ImportFileName'] = $ImportFileName;
				echo "Using <a href='".$link_file."'>DEP Internal Report</a> just uploaded<br>";
				ob_flush();
			}
			if (!$ImportFileReceived){
				if (file_exists($target_file)){
					$lastUploadedDateParts = explode("File",$ImportFileName);
					$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
					echo "Using <a href='".$link_file."'>Existing Uploaded DEP File</a> last uploaded ".date("F Y",strtotime($lastDEPUploadedDate))."<br>";
					$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/".$ImportFileName : $ImportFileName);
					$trackingFile = $ImportFileName;
				}else{
					echo "No Prior DEP File uploaded.  Please upload the most recent DEP Internal Report File.  This file is used to re-allocate Lorenzo's DEP hours to other billing categories.<br>";
					$ImportFileName = "importfiles/DEPInternalReport.xlsx";

				}
			}
			$DEPInternalReportFileName = $ImportFileName;
		
	?>
			<b>Instructions</b>
			<ul>
				<li>1. Update the DEP Internal File by clicking the button below and choosing the appropriate FY file from L:\Green Business\Internal Reports\DEP, then clicking the 'Begin Processing' button once the file has been chosen.
				<li>2. Download RW Invoice file from the link below.
				<li>3. Adjust layout in the RW Invoice file to insert rows so that sections are not broken between pages.
				<li>4. Verify the passthrough items have documentation saved to the L drive.
				<li>5. To retrieve the NON Pipeline Report, RW TA Pipeline Report, and the RW Report, logout of the Admin Dashboard and login to the Staff Dashboard to run this same Toolset.
			</ul>
				<a id='updateDEPFile' href="#" class='button-link do-not-navigate'>Update DEP Internal File</a><br>
				<div id="updateDEPFileForm" style='display:none;'>
					<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>invoicing/?nav=invoicingTool-RWTAPipeline&type=DEPFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
						<div class="row">
							<div class="eight columns">
								<input type="file" name="filesToUpload2[]" id="filesToUpload2" multiple="" onChange="makeFileList2();" /><Br>
							</div>
							<div class="eight columns">
								<strong>File You Selected:</strong>
								<ul id="fileList2"><li>No Files Selected</li></ul>
								<input type="submit" value="Begin Processing" id="submitButton2" style="display:none;">
							</div>
						</div>
						
						<script type="text/javascript">
							function makeFileList2() {
								var input = document.getElementById("filesToUpload2");
								var ul = document.getElementById("fileList2");
								while (ul.hasChildNodes()) {
									ul.removeChild(ul.firstChild);
								}
								for (var i = 0; i < input.files.length; i++) {
									var li = document.createElement("li"),
										fileName = input.files[i].name,
										fileLength = fileName.length,
										fileExt = fileName.substr(fileLength-4);
										console.log(fileExt);
									if (fileExt != "xlsx"){
										li.innerHTML = 'You must save '+fileName+' as a .xlsx file first';
										ul.appendChild(li);
										document.getElementById('submitButton2').style.display = 'none';
									}else{
										li.innerHTML = input.files[i].name;
										ul.appendChild(li);
										document.getElementById('submitButton2').style.display = 'block';
									}
								}
								if(!ul.hasChildNodes()) {
									var li = document.createElement("li");
									li.innerHTML = 'No Files Selected';
									ul.appendChild(li);
									document.getElementById('submitButton2').style.display = 'none';
								}
							}
						</script>
					</form>
				</div>
		<?php
		}//end if !isStaff
			$reportId = "00OU0000003e6wK"; //Invoicing/531 Diversion Report Com,Rec,Reu,Redu
			$reportName = "531 Diversion Report Composted,Recycled,Reused,Reduced 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$GPType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignParts = explode(" ",$primaryCampaignName);
				$secondaryCampaignName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$secondaryCampaignParts = explode(" ",$secondaryCampaignName);
				$createdDate = $rowDetail["Metric__c.CreatedDate"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$metricType = $rowDetail["Metric__c.Type__c"];
				$metricName = $rowDetail["Metric__c.Name"];
				$metricTons = floatval(str_replace(",","",trim($rowDetail["Metric__c.Weight_in_Tons__c"])));
				//$metricTons = floatval($metricTons);
				if (strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$diversionReportYTD[$metricType] = ($diversionReportYTD[$metricType]+$metricTons);
					if (strtotime($completedDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate))) && strtotime($completedDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$thisAddedValue = bcadd($diversionReportThisMonth[$metricType],$metricTons,2);
						$diversionReportThisMonth[$metricType] = $thisAddedValue;
					}
				}
			}
			ksort($diversionReportThisMonth);
			ksort($diversionReportYTD);
			
			$reportId = "00OU0000003e73z"; //Invoicing/RW Lamps Recycled YTD
			$reportName = "RW Lamps Recycled YTD 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$YTDLampReport = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$greenProspectPhase = $rowDetail["Upgrade__c.Green_Prospect_Phase__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];

				$createdDate = $rowDetail["Metric__c.CreatedDate"];
				$metricName = $rowDetail["Metric__c.Name"];
				$metricMaterial = $rowDetail["Metric__c.Material__c"];
				$metricUnit = $rowDetail["Metric__c.Unit__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$units = ($metricName == "unit" ? $metricNumber : floatval(str_replace(" units","",$rowDetail["Metric__c.Other_Explain__c"])));
				$metricType = "Lamps";
				if (strtotime($createdDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$diversionReportYTD[$metricType] = ($diversionReportYTD[$metricType]+$units);
					if (strtotime($createdDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
						$diversionReportThisMonth[$metricType] = ($diversionReportThisMonth[$metricType]+$units);
					}
				}
			}
			
			$reportId = "00OU0000003e74E"; //Invoicing/RW Lamps Recycled Last Month
			$reportName = "RW Lamps Recycled Last Month";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			$lastMonthLamps = 0;
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$units = floatval(str_replace(" units","",$rowDetail["Metric__c.Other_Explain__c"]));
				$lastMonthLamps = $lastMonthLamps+$units;
			}
			if ($lastMonthLamps != $diversionReportThisMonth["Lamps"]){
				$Adjustments["Alert"]["Last Month Lamp Calculation Error<br>".$YTDLampReport."<br>".$reportLink][] = "YTD Lamp units shown as ".$diversionReportThisMonth["Lamps"]." but Last Month Lamp Units shown as ".$lastMonthLamps;
			}
	
/*	Old method for getting dataAnalysis information, but now using report 00O0P000003eWFA which uses hardcoded reports as of August 2016
			$reportId = "00OU0000003e74Y"; //Invoicing/Hotline GP Created - 531 YTD
			$reportName = "Hotline GP Created - 531 YTD 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$businessType = $rowDetail["FK_Upgrade__c.Business_Type__c"];
				$evaluationDate = $rowDetail["FK_Upgrade__c.Initiation_Date__c"];
				$evalType = ($businessType == "Hauler" || $businessType == "Processor" ? "CreatedHP" : "CreatedBI");
				if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$dataAnalysisYTD[$evalType]++;
					if (strtotime($evaluationDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
						$dataAnalysisThisMonth[$evalType]++;
					}
				}
			}
			//print_pre($dataAnalysisThisMonth);
			
			$reportId = "00OU0000003e7LA"; //Invoicing/Hotline GP Completed - 531 YTD
			$reportName = "Hotline GP Completed - 531 YTD 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$businessType = $rowDetail["FK_Upgrade__c.Business_Type__c"];
				$evaluationDate = $rowDetail["FK_Upgrade__c.GP_Stage_Completed__c"];
				$evalType = ($businessType == "Hauler" || $businessType == "Processor" ? "CompletedHP" : "CompletedBI");
				
				if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$dataAnalysisYTD[$evalType]++;
					if (strtotime($evaluationDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
						$dataAnalysisThisMonth[$evalType]++;
					}
				}
				
			}

			$reportId = "00OU0000003e7MX"; //Invoicing/531 YTD Received Assistance
			$reportName = "531 YTD Received Assistance";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;			
			$dataAnalysisThisMonth["YTDReceivedAssistCount"] = count($reportRowsUnSorted);
			//print_pre($reportRowsUnSorted);
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$evaluationDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$evalType = "RequestedAssistance";
				
				if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					$dataAnalysisYTD[$evalType]++;
					if (strtotime($evaluationDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
						if (strtotime($initiationDate) < strtotime(date("m/1/Y",strtotime($invoiceDate)))){
							$dataAnalysisThisMonth[$evalType]++;
						}
					}
				}
				
			}
*/
			$reportId = "00O0P000003eWFA"; //Invoicing/531 YTD Modified or Created
			$reportName = "531 YTD Modified or Created";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportRowsSorts = array_orderby($reportRowsUnSorted, 'field_key', SORT_ASC);
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;			
			//print_pre($reportRowsSorts);
			$gpsReceivedAssistanceYTD = array();
			$gpsReceivedAssistanceThisMonth = array();
			$invoiceYearMonth = date("Y-m",strtotime($invoiceDate));
			foreach ($reportRowsSorts as $rowId=>$rowDetail){
				$modifiedDate = explode(" ",$rowDetail["CREATED_DATE"]);
				$evaluationDate = $modifiedDate[0];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				if (trim($completedDate) != "-"){
					$completedDateParts = explode(" ",$completedDate);
					$completedDate = $completedDateParts[0];
				}
				$yearMonthEvalDate = date("Y-m",strtotime($evaluationDate));
				$yearMonthCompletedDate = date("Y-m",strtotime($completedDate));
				$gpId = $rowDetail["CUST_ID"];
				$businessType = $rowDetail["Upgrade__c.Business_Type__c"];
				$bizType = ($businessType == "Hauler" || $businessType == "Processor" ? "HP" : "BI");
				$fieldKey = $rowDetail["field_key"];
				if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
					if (trim($fieldKey) == "Created."){
						$evalTypes["Created".$bizType][$yearMonthEvalDate][$gpId] = 1;
					}else{
						//only count open if it wasn't actually created in the same period
						if (!$evalTypes["Created".$bizType][$yearMonthEvalDate][$gpId]){
							$evalTypes["Open"][$yearMonthEvalDate][$gpId] = 1;
						}
					}
				}
				if (trim($completedDate) != "-"){
					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate))) && strtotime($completedDate) >= strtotime($CurrentFY)){
//					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate))) && strtotime($completedDate) >= strtotime($invoiceDate)){
						$evalTypes["Completed".$bizType][$yearMonthCompletedDate][$gpId]=1;
					}
				}
			}
//			print_pre($evalTypes);
			foreach ($evalTypes as $evalType=>$yearMonthInfo){
				foreach ($yearMonthInfo as $evalYearMonth=>$requests){
					//weed out next month items
					if (strtotime($evalYearMonth."-01") <= strtotime($invoiceYearMonth."-01")){ 
						$dataAnalysisYTD[$evalType] = $dataAnalysisYTD[$evalType]+count($requests);
						if ($evalYearMonth == $invoiceYearMonth){
							$dataAnalysisThisMonth[$evalType] = count($requests);
						}
					}
				}
			}
//			print_pre($evalTypes);
//			print_pre($dataAnalysisYTD);
//			print_pre($dataAnalysisThisMonth);
//			$dataAnalysisYTD["RequestedAssistance"] = $gpsReceivedAssistanceYTDCount;
//			$dataAnalysisThisMonth["RequestedAssistance"] = $gpsReceivedAssistanceThisMonthCount;
			
			
			$reportId = "00OU0000003e7OO"; //Invoicing/RW Marketing Report
			$reportName = "RW Marketing Report 2016-2017";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;			
			//$dataAnalysisThisMonth["YTDReceivedAssistCount"] = count($reportRowsUnSorted);
			//print_pre($reportRowsUnSorted);
			
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$prospectType = $rowDetail["Upgrade__c.Green_Prospect_Type__c"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$completedDate = $rowDetail["Upgrade__c.GP_Stage_Completed__c"];
				$initiationDate = $rowDetail["Upgrade__c.Initiation_Date__c"];
				$lastActionDate = $rowDetail["Upgrade__c.Last_Action_Date__c"];
				$metricType = $rowDetail["Metric__c.Type__c"];
				$metricNumber = $rowDetail["Metric__c.Number__c"];
				$evaluationDate = $completedDate;

				$evalType = "MarketingEvents";
				
				$possiblePresentation = false;
				$definitePresentation = false;
				//only counting presentations
				if (strpos(" ".strtolower($statusDetail),"presentation")){
					$possiblePresentation = true;
				}
				if (strpos($prospectType,"Presentation")){
					$definitePresentation = true;
					if (strtotime($evaluationDate) <= strtotime(date("m/t/Y 23:59:59",strtotime($invoiceDate)))){
						$dataAnalysisYTD[$evalType]++;
						if (strtotime($evaluationDate) >= strtotime(date("m/1/Y",strtotime($invoiceDate)))){
							$dataAnalysisThisMonth[$evalType]++;
							$marketingEvents[] = $statusDetail;
							$infoArray = array(
								"name"=>$metricType." ".(integer)$metricNumber,
								"primaryCampaignCode"=>"Marketing",
								"secondaryCampaignCode"=>"Marketing",
								"status"=>"Completed",
								"statusDetail"=>$statusDetail
								);
							$bulletsByCampaignCode["Marketing"][$greenProspectPhase][] = $infoArray;
						}
					}
				}
				if ($possiblePresentation && !$definitePresentation){
					$Adjustments["Review"]["Possible Marketing Presentation but not Prospect Type of Presentation<br>".$reportLink][] = $GPNameHref." Last Action Date:". $lastActionDate." Details: ".$metricType." ".$metricNumber." ".$statusDetail;
				}
			}
			//Get Google Analytics
			//https://developers.google.com/analytics/devguides/reporting/core/dimsmets
			$metricsToUse = array("sessions"=>"totalVisits","pageviews"=>"pageviews","pageviewsPerSession"=>"pageviewsPerSession","avgSessionDuration"=>"avgSessionDuration","percentNewSessions"=>"percentNewSession");
			$startDate = date("Y-m-d",strtotime($invoiceDate));
			$endDate = date("Y-m-t",strtotime($invoiceDate));
			include_once('../../cetadmin/invoicing/_googleAnalytics.php');

	
			$reportId = "00OU0000003e65B"; //Invoicing/Monthly Report Bullets
			$reportName = "Monthly Report Bullets";
			$reportResults = retrieve_report_asCSV($instance_url, $reportId, $access_token);
			$reportRowsUnSorted = $reportResults["rows"];
			$reportLink = "<a href='".$instance_url."/".$reportId."' target='".$reportId."'>".$reportName."</a>";
			$reportLinks[] = $reportLink;
			//print_pre($reportRowsUnSorted);
			//$bulletCodes = array();
			$b= 1; //RW Report bullets are codes 531-538
			while ($b < 9){
				$RWReportBulletCodes[] = "53".$b;
				$b++;
			}
			foreach ($reportRowsUnSorted as $rowId=>$rowDetail){
				$accountName = str_replace("Zaccheo ","",$rowDetail["Account.Name"]);
				$AccountNameLink = $rowDetail["AccountNameLinkValue"];
				$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";
				$greenProspectName = $rowDetail["Upgrade__c.Name"];
				$GPNameLink = $rowDetail["GPNameLinkValue"];
				$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
				$primaryCampaignName = $rowDetail["Upgrade__c.Primary_Campaign__c.Name"];
				$primaryCampaignParts = explode(" ",$primaryCampaignName);
				$primaryCampaign = trim($primaryCampaignParts[0]);
				$primaryCampaign = ($primaryCampaign == "534" || $primaryCampaign == "537A" || $primaryCampaign == "537B" ? "534/537AB" : $primaryCampaign);
				$secondaryCampaignName = $rowDetail["Upgrade__c.Secondary_Campaign_Association__c"];
				$secondaryCampaignParts = explode(" ",$secondaryCampaignName);
				$secondaryCampaign = trim($secondaryCampaignParts[0]);
				$secondaryCampaign = ($secondaryCampaign == "534" || $secondaryCampaign == "537A" || $secondaryCampaign == "537B" ? "534/537AB" : $secondaryCampaign);
				$greenProspectPhase = (strpos(" ".$rowDetail["Upgrade__c.Green_Prospect_Phase__c"],"Completed") ? "Completed" : "DDD".$rowDetail["Upgrade__c.Green_Prospect_Phase__c"]);
				
				$owner = $rowDetail["Upgrade__c.Owner.Name"];
				$statusDetail = $rowDetail["Upgrade__c.Current_Status_Detail__c"];
				$infoArray = array(
					"name"=>$accountName,
					"accountNameHref"=>$AccountNameHref,
					"accountNameLink"=>$AccountNameLink,
					"GPName"=>$greenProspectName,
					"GPNameLink"=>$GPNameHref,
					"reportLink"=>$reportLink,
					"primaryCampaign"=>$primaryCampaignName,
					"primaryCampaignCode"=>$primaryCampaign,
					"secondaryCampaignName"=>$secondaryCampaignName,
					"secondaryCampaignCode"=>$secondaryCampaign,
					"status"=>$greenProspectPhase,
					"statusDetail"=>$statusDetail
					);
				
				$bulletsByCampaignCode[$primaryCampaign][$greenProspectPhase][] = $infoArray;
				/*
				if (!in_array($primaryCampaignName,$bulletCodes)){
					$bulletCodes[] = $primaryCampaignName;
				}
				*/
				$codesByAccount[$AccountNameLink][] = $primaryCampaignName;
				if (trim($secondaryCampaignName) != "-"){
					$codesByAccount[$AccountNameLink][] = $secondaryCampaignName;
					if ($secondaryCampaign != "" && $secondaryCampaign != $primaryCampaign){
						$bulletsByCampaignCode[$secondaryCampaign][$greenProspectPhase][] = $infoArray;
					}
				}
			}
			
			//Hotline (531) Bullets are completely static, so reset them to only use database static bullets
			$bulletsByCampaignCode["531"] = array();
			
			//get static bullets 
			$criteria = new stdClass();
			$criteria->reportName = "RecyclingWorks"; 
			$staticBulletResults = $GBSProvider->invoiceTool_getStaticBullets($criteria);
			foreach ($staticBulletResults->collection as $results){
				$infoArray = array(
					"name"=>null,
					"primaryCampaignCode"=>$results->GBSBullet_InvoiceCode." Ongoing",
					"status"=>"Ongoing",
					"statusDetail"=>$results->GBSBullet_Content
					);
				$bulletsByCampaignCode[$results->GBSBullet_InvoiceCode]["Ongoing"][] = $infoArray;
			}

			//sort($bulletCodes);
			ksort($bulletsByCampaignCode);
			foreach ($bulletsByCampaignCode as $code=>$statusInfo){
				ksort($statusInfo);
				$bulletsByCampaignCodeSorted[$code] = $statusInfo;
				$codeCount = 0;
				foreach ($statusInfo as $status=>$statusRows){
					$codeCount = $codeCount+count($statusRows);
				}
				$bulletsCodeCount[$code] = $codeCount;
			}

			
			include_once('salesforce/reports/reportRW.php');
			echo $ReportsFileLink;

			//print_pre($bulletsByCampaignCodeSorted);
			

	echo "</div>";
	echo "<div id='RWReport' class='tab-content'>";
	?>
		<style>
			table {font-family: "Calibri";font-size:11pt;}
		</style>
		<h3>This tool will help group the Salesforce Dashboard reports for:<br>RW TA Completed - <?php echo $CurrentYear."-".$NextYear;?><br>RW TA In-Progress</h3>
		<?php
			$mergedFieldOrder = array(
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"RW DEP Summary Report Submitted"=>"85",
				"Green Prospect Phase"=>"71",
				"Owner: Full Name"=>"60",
				"Green Prospect Type"=>"71",
				"Number"=>"65",
				"Unit"=>"34",
				"Material"=>"67",
				"Current Status Detail"=>"62");
				
			$miniTAFieldOrder = array(
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"Green Prospect Phase"=>"71",
				"Owner: Full Name"=>"60",
				"Green Prospect Type"=>"71",
				"Number"=>"65",
				"Unit"=>"34",
				"Material"=>"67",
				"Current Status Detail"=>"62");

			$mapFieldOrder = array(
				"Green Prospect Name"=>"71",
				"Primary Campaign: Campaign Name"=>"90",
				"Account Name"=>"65",
				"Initiation Date"=>"80",
				"Last Action Date"=>"51",
				"RW DEP Summary Report Submitted"=>"85",
				"Green Prospect Phase"=>"71",
				"Green Prospect Type"=>"71",
				"Materials Diverted"=>"71",
				"Address"=>"64",
				"Pipeline"=>"65");
				
				
			$extraFields = array(
				"Address"=>"64",
				"Pipeline"=>"65",
				);
			$PipelineSortOrder = array("Does Not Apply","1-30 Days","31-60 Days","61-90 Days","90+ Days");
			if (count($TAPipelineItems)){
				//$RWTabCodes = array("Hospitality"=>"537A","FW Generators"=>"537B","State Facilities"=>"537C","General Recycling"=>"534","Mini TA"=>"539");
				//$RWTabCodes = array("Technical Assistance"=>"534/539/537AB","State Facilities"=>"537C");
				$RWTabCodes = array("Technical Assistance"=>"534/537AB","State Facilities"=>"537C","Mini TA"=>"539");
				$csvCombined = array();
				$csvCombined = $TAPipelineItems;
				echo "Instructions:<br>If Pipeline Errors tab is available, double click tab to display<br>";
				echo "Follow instructions in Impact Map tab to create Impact Map URL to be added to the report.";
//				echo "There are ".count($csvCombined)." rows<br>";
//				echo "There are ".count($TAPipelineItems)." TAPipeline rows<br>";
				//$mergedFile = convert_to_csv($csvCombined, 'Merged.csv', ',');
					$TableHeaderTop = "<thead><tr>";
					foreach ($mergedFieldOrder as $field=>$width){
						$TableColumns .= "<th valign=\"top\" width=\"".$width."\">".str_replace("Primary Campaign:","",$field)."</th>";
					}
					$TableHeaderBottom = "</tr></thead>";		
					$TableHeaderFull = $TableHeaderTop.$TableColumns.$TableHeaderBottom;

					$TableHeaderTopMiniTA = "<thead><tr>";
					foreach ($miniTAFieldOrder as $field=>$width){
						$TableColumnsMiniTA .= "<th valign=\"top\" width=\"".$width."\">".str_replace("Primary Campaign:","",$field)."</th>";
					}
					$TableHeaderBottomMiniTA = "</tr></thead>";		
					$TableHeaderFullMiniTA = $TableHeaderTopMiniTA.$TableColumnsMiniTA.$TableHeaderBottomMiniTA;

					$TableHeaderTopImpactMap = "<thead><tr>";
					foreach ($mapFieldOrder as $field=>$width){
						$TableColumnsImpactMap .= "<th valign=\"top\" width=\"".$width."\">".$field."</th>";
					}
					$TableHeaderBottomImpactMap = "</tr></thead>";		
					$TableHeaderFullImpactMap = $TableHeaderTopImpactMap.$TableColumnsImpactMap.$TableHeaderBottomImpactMap;
					
				foreach ($RWTabCodes as $TabName=>$TabCode){
					$RWHashes[] = str_replace("/","_",$TabCode);
					$TabListItems[] .= '<li><a id="tab-'.str_replace("/","_",$TabCode).'" href="#'.str_replace("/","_",$TabCode).'">'.$TabName.'</a></li>';
					//sort csvCombined by Pipeline Order
					$RWTabRowContent = "";
					$mergedFieldOrderToUse = ($TabCode == "539" ? $miniTAFieldOrder : $mergedFieldOrder);
					$tableHeaderToUse = ($TabCode == "539" ? $TableHeaderFullMiniTA : $TableHeaderFull);
					foreach ($PipelineSortOrder as $Pipeline){
						$pipelineRecordsfound = false;
						$rowDisplay = array();
						$rowToDisplay = array();
						foreach ($csvCombined as $id=>$rowInfo){
							if (strpos(" ".$rowInfo["PrimaryCampaignCode"],$TabCode)){
								$pipelineErrorFound = (strpos(" ".$rowInfo["Green Prospect Phase"],"Completed") && $rowInfo["Pipeline"] != "Does Not Apply" ? true : false);
								if ($pipelineErrorFound){
									//print_pre($rowInfo);
									$pipelineErrors[$id] = $rowInfo;
									//now correct the pipeline for display
									$rowInfo["Pipeline"] = "Does Not Apply";
								}
								if ($rowInfo["Pipeline"] == $Pipeline){
									//echo $rowInfo["Account Name"]." ".$rowInfo["Pipeline"]."<br>";
									$pipelineRecordsfound = true;
									$rowValues = array();
									$rowValueData = array();
									//cleanup
									if ($rowInfo["Material"] == "Other"){$rowInfo["Material"] = $rowInfo["Other: Explain"];}
									/*Currently calculated in Template via formula
									$DivertedMaterialsByType[$rowInfo["Material"]]["count"]++;
									if (strtolower(trim($rowInfo["Unit"])) == "tons"){
										$DivertedMaterialsByType[$rowInfo["Material"]]["tons"] = $DivertedMaterialsByType[$rowInfo["Material"]]["tons"]+$rowInfo["Number"];
									}
									*/
									foreach ($mergedFieldOrderToUse as $field=>$width){
										$rowValues[] = array($rowInfo[$field],$width);
										$rowValueData[$field] = $rowInfo[$field];
										//echo $field."=".$rowInfo[$field].", ".$width;
									}//end foreach mergedFieldOrder
									if (count($rowValues)){
										$rowDisplay[] = $rowValues;
										$rowToDisplay[] = $rowValueData;
									}
								}
							}
						}//end foreach csvCombined
						$rowToDisplayOrdered = array_orderby($rowToDisplay,"Account Name",SORT_ASC);

						$ExcelData[$TabName][$Pipeline] = $rowToDisplayOrdered;
						if (count($rowDisplay)){
							//show fields
							foreach ($rowDisplay as $rowID=>$rowValues){
								$RWTabRowContent .= "<tr>";
								foreach ($rowValues as $value){
									$thisValue = $value;
									$value = ($thisValue[0] ? $thisValue[0] : "&nbsp;");
									$width = ($thisValue[1] ? $thisValue[1] : "10");
									$RWTabRowContent .= "<td valign=\"top\" width=\"".$width."\">".$value."</td>";
								}
								$RWTabRowContent .= "</tr>";
							}
							$PipelineDisplayText = ($Pipeline == "Does Not Apply" ? "Total Completed ".$TabName." TA" : "Total ".$TabName." TA In-Progress, Expected Completed in ".$Pipeline);
							$PipelineDisplayColor = ($Pipeline == "Does Not Apply" ? "lightgreen" : "yellow");
							$RWTabRowContent .= "<tr style='background-color:".$PipelineDisplayColor.";'><td colspan='".count($mergedFieldOrderToUse)."'>".$PipelineDisplayText."</td></tr>";
						}
					}//end foreach PipelineSortOrder
					$RWTabContent ="<h3>".$TabName." (".$TabCode."):</h3><button class='selectAll' data-tableid='Table".str_replace("/","_",$TabCode)."' onclick=\"selectElementContents(document.getElementById('Table".str_replace("/","_",$TabCode)."'))\" >Select All</button>";
					$RWTabContent .= "<table border='1' cellspacing='0' cellpadding='2' width='100%'>".$tableHeaderToUse;
					$RWTabContent .= "</table>";
					$RWTabContent .= "<div id='Table".str_replace("/","_",$TabCode)."'><table border='1' cellspacing='0' cellpadding='2' width='100%'>";
					$RWTabContent .= $RWTabRowContent."</table></div>";
					$RWTabContentArray[$TabCode] = $RWTabContent;
				}//end foreach RWTabCodes
				//add Impact Map Info
					$RWHashes[] = "ImpactMap";
					$TabListItems[] .= '<li><a id="tab-ImpactMap" href="#ImpactMap">Impact Map</a></li>';
					//sort csvCombined by Pipeline Order
					$RWTabRowContent = "";
					$mergedFieldOrderToUse = $mapFieldOrder;
					$tableHeaderToUse = $TableHeaderFullImpactMap;
					$rowDisplay = array();
					foreach ($csvCombined as $id=>$rowInfo){
						$rowValues = array();
						//cleanup
						if ($rowInfo["Material"] == "Other"){$rowInfo["Material"] = $rowInfo["Other: Explain"];}
						$rowInfo["Materials Diverted"] = trim($rowInfo["Number"]." ".$rowInfo["Unit"]." of ".$rowInfo["Material"]);
						$rowInfo["Materials Diverted"] = ($rowInfo["Materials Diverted"] == "of" ? "" : $rowInfo["Materials Diverted"]);

						foreach ($mergedFieldOrderToUse as $field=>$width){
							$rowValues[] = array($rowInfo[$field],$width);
							//echo $field."=".$rowInfo[$field].", ".$width;
						}//end foreach mergedFieldOrder
						if (count($rowValues)){
							$rowDisplay[] = $rowValues;
						}
					}//end foreach csvCombined
					if (count($rowDisplay)){
						//show fields
						foreach ($rowDisplay as $rowID=>$rowValues){
							$RWTabRowContent .= "<tr>";
							foreach ($rowValues as $value){
								$thisValue = $value;
								$value = ($thisValue[0] ? $thisValue[0] : "&nbsp;");
								$width = ($thisValue[1] ? $thisValue[1] : "10");
								$RWTabRowContent .= "<td valign=\"top\" width=\"".$width."\">".$value."</td>";
							}
							$RWTabRowContent .= "</tr>";
						}
					}
					$RWTabContent ="<h3>Impact Map:</h3>
						<ul><li>1. Select All data and paste into <a href='https://batchgeo.com/' target='batchGeo'>https://batchgeo.com/</a> box.
							<li>2. Click 'Validate and Set Options' button.
							<li>3. Click the 'Advanced Options' and set Title = Account Name, Group by = Primary Campaign.
							<li>4. Click the Make Map button.  It will take a few minutes to process all records and create the map.
							<li>5. Zoom out and delete pins outside MA service area, center, save and copy link URL.
							<li>6. Add link URL to final tab of the RW TA Tracking Pipeline Report
						</ul>
					<button class='selectAll' data-tableid='TableImpactMap' onclick=\"selectElementContents(document.getElementById('TableImpactMap'))\" >Select All</button>";
					$RWTabContent .= "<div id='TableImpactMap'><table border='1' cellspacing='0' cellpadding='2' width='100%'>".$tableHeaderToUse.$RWTabRowContent;
					$RWTabContent .= "</table></div>";
					$RWTabContentArray["ImpactMap"] = $RWTabContent;
				//end Impact Map

				
				
				if (count($pipelineErrors)){
					$RWTabContent = "<h5>Update the Salesforce Dashboard to correct the Pipeline value to 'Does Not Apply.'<br>These errors were detected and corrected in the TA Pipeline download file, but should still be corrected in Salesforce.</h5>";
					$RWTabContent .= "<table id='ErrorTable'>";
					foreach ($pipelineErrors as $errorId=>$errorValue){
						$columns = array_keys($errorValue);
						if (!$columnsIncluded){
							$RWTabContent .="<thead><tr>";
							foreach ($columns as $column){
								$RWTabContent .="<th valign-'top'>".$column."</th>";
							}
							$RWTabContent .="</tr></thead>";
							$columnsIncluded = true;
						}
						$RWTabContent .="<tbody><tr>";
						foreach ($errorValue as $columnName=>$data){
							$dataDisplay = (strlen($data) > 50 ? "<span title='".$data."'>".substr($data,0,50)."...</span>" : $data);
							$fontcolor = ($columnName == "Pipeline" ? " style='color:red;'" : "");
							$RWTabContent .="<td valign-'top' nowrap='nowrap'".$fontcolor.">".$dataDisplay."</td>";
						}
						$RWTabContent .="</tr></tbody>";
					}
					$RWTabContent .= "</table>";
					$RWTabContentArray["Error"] = $RWTabContent;
					
					$TabListItems[] = '<li><a id="tab-Error" href="#Error" style="color:red;" class="active">Pipeline Errors</a></li>';
				}
				//print_pre($ExcelData);
				include_once('salesforce/reports/RW_TA_Pipeline.php');
				echo "<Br>Download TA Pipeline Report: ".$ReportsFileLink."<br>";
				
				echo '<ul id="admin-tabs" class="tabs">';
						foreach ($TabListItems as $TabItem){
							echo $TabItem;
						}
				echo '</ul>';
				echo '<div class="tabs-content">';
				foreach ($RWTabContentArray as $TabCode=>$RWTabContent){
					echo '<div id="'.str_replace("/","_",$TabCode).'" class="tab-content">'.$RWTabContent.'</div>';
				}
				echo '</div>';
			

			
			}//end Files not uploaded
		?>

		<script type="text/javascript">
			function selectElementContents(el) {
				var body = document.body, range, sel;
				if (document.createRange && window.getSelection) {
					range = document.createRange();
					sel = window.getSelection();
					sel.removeAllRanges();
					try {
						range.selectNodeContents(el);
						sel.addRange(range);
					} catch (e) {
						range.selectNode(el);
						sel.addRange(range);
					}
				} else if (body.createTextRange) {
					range = body.createTextRange();
					range.moveToElementText(el);
					range.select();
				}
			}
			
			$(function(){
				var tabs = $('ul.tabs'),
					selectAllClass = $(".selectAll");
					
				

				tabs.each(function(i) {

					//Get all tabs
					var tab = $(this).find('> li > a');
					tab.click(function(e) {

						//Get Location of tab's content
						var contentLocation = $(this).attr('href');

						//Let go if not a hashed one
						if(contentLocation.charAt(0)=="#") {

							e.preventDefault();

							//Make Tab Active
							tab.removeClass('active');
							$(this).addClass('active');

							//Show Tab Content & add active class
							$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
							ttInstances = TableTools.fnGetMasters();
							for (i in ttInstances) {
								if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
							}

						}
					});
				});		
				$(".tab-content").hide();
				$("#tab-RawData").click(function(){
					$("#RawDataDiv").show();
				});
				var hash = window.location.hash,
					tabHashes = new Array("#<?php echo implode("\",\"#",$RWHashes);?>"),
					isTabHash = $.inArray(hash, tabHashes) !== -1,
					isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
					setTabInner = function(tabid){
						var tab = $("#tab-" + tabid),
							content = $("#" + tabid);
						tab.addClass("active");
						content.show();
					};
				$("#tab-Error").click();
				setTabInner("<?php echo (count($pipelineErrors) ? "Error" : "537A");?>");
				$("#Error").show();
				
				$('#ErrorTable').DataTable({
					"fnInitComplete": function() {
							$('#ErrorTable tbody tr').each(function(){
									$(this).find('td:eq(0)').attr('nowrap', 'nowrap');
							});
						},			
					"scrollX": true,
					"bJQueryUI": true,
					"bSearchable":false,
					"bFilter":false,
					"bAutoWidth": false,
					"bSort": false,
					"order": [[0, "dec"]],
					"bPaginate": false,
					"iDisplayLength": 100
				});
				
			});
			
		</script>
<?php
	echo "</div>";
	include_once('_adjustmentsTab.php');
	$invoiceCodeName = "RecyclingWorks";
	include_once('_notesTab.php');
echo "</div>"; //end tabs-content
?>
<br clear="all">
<?php 
	if (count($reportLinks)){
		echo "The following Salesforce Reports were used:";
		foreach ($reportLinks as $reportLink){
			echo "<br>".$reportLink;
		}
	}//end if reportLinks
?>
<br clear="all">
<b>Instructions</b>
<ul>
	<li>1. In the Hours tab set the date range and Status you are looking to capture.
	<li>2. Review the Adjustments Tab
	<li>3. If you are logged in via the Admin Portal: Follow the instructions in the RW Invoice tab, download the File.
	<li>4. If you are logged in vai the Staff Portal: Follow instructions in each Tab.  Download NON Pipeline, TA Pipeline, and RW Invoice
</ul>
<script type="text/javascript">
	$(function () {
		$("#updateNONFile").on('click',function(){
			$("#updateNONFileForm").toggle();
		});
		$("#updateDEPFile").on('click',function(){
			$("#updateDEPFileForm").toggle();
		});
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
//			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
//			"iDisplayLength": 5,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
		
		var tabs = $('ul.tabs');
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {

					e.preventDefault();

					//Make Tab Active
					tab.removeClass('active');
					$(this).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$masterTabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setMasterTab;?>");
		}
		
	});
</script>