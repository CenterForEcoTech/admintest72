<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxMeasures" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "$('#measure').val($(ui.item).val());";
							// echo "window.location = '?nav=manage-codes&CodeID='+$(ui.item).val();";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxMeasures parameters" id="SelectedMeasure" style="border:1pt solid blue;" data-hiddenid="measure">
				<option value="">Select one...</option>
				<?php 
					foreach ($Measures as $Measure){
						echo "<option value=\"".$Measure."\"".($SelectedMeasure==$Measure ? " selected" : "").">".$Measure."</option>";
					}
				?>
			  </select>
			</div>
		</div>