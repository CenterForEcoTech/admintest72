<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."GBSProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		$action = $newRecord->action;
		if ($action == "get_measuretext"){
			$record = new stdClass();
			$record->primaryUtility = $newRecord->primaryUtility;
			$record->secondaryUtility = $newRecord->secondaryUtility;
			$record->measure = $newRecord->measure;
			$gbsProvider = new GBSProvider($dataConn);
			$resultArray = $gbsProvider->getEAPTemplates($record);
			$resultRecord = $resultArray->collection;
			$displayText = $resultRecord[0];
			$DisplayText = $displayText->displayText;
			$results->success = true;
			$results->displayText = $DisplayText;
			
		}
		header("HTTP/1.0 201 Created");
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'eap_update'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->updateEAPTemplate($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'eap_add'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->addEAPTemplate($newRecord, getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif (trim($newRecord->action) == 'group_update'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->updateGroups($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'group_add'){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->addGroups($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == "insert_hours"){
			$gbsProvider = new GBSProvider($dataConn);
			$response = $gbsProvider->validateHours($newRecord);
			if ($response->Errors){
				$results = $response;
			}else{
				$insertResponse = $gbsProvider->insertHours($response, getAdminId());
				$results = $insertResponse;
			}
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "insert_note"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->insertNotes($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hours"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hourid"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHourId($newRecord, getAdminId());
			if ($insertResponse->AddedByAdmin){
				$insertResponse->error = "Only ".$insertResponse->AddedByAdmin." can remove this entry.";
			}
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_hours"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->submitHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_remove"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_approve"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "approve_remove"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_approve"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_reject"){
			$gbsProvider = new GBSProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else{
            $gbsProvider = new GBSProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $gbsProvider->updateCodesDisplayOrder($id,($displayID+1));
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $gbsProvider->updateCodesDisplayOrder($id,0);
					}
				}
				if ($action == 'Insert_CodesMatrix'){
					include_once($dbProviderFolder."GBSProvider.php");
					$gbsProvider = new GBSProvider($dataConn);
					$record->codesId = $item->codesId;
					foreach ($items as $displayID=>$id){
						$record->employeeId = (int)$id;
						$responseCollection[] = $gbsProvider->addCodesMatrix($record,getAdminId());
						if (count($responseCollection)){
							$responseCollection[0] = "Added";
						}
					}
				}
				if ($action == 'Remove_CodesMatrix'){
					include_once($dbProviderFolder."GBSProvider.php");
					$gbsProvider = new GBSProvider($dataConn);
					$record->codesId = $item->codesId;
					foreach ($items as $displayID=>$id){
						$matrixId = (int)$id;
						$responseCollection[] = $gbsProvider->removeCodesMatrix($matrixId);
						if (count($responseCollection)){
							$responseCollection[0] = "Removed";
						}
					}
				}
				
				
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = $action."unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>