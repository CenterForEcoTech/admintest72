<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
//include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == "insert_hours"){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->validateHours($newRecord);
			if ($response->Errors){
				$results = $response;
			}else{
				$insertResponse = $gbsProvider->insertHours($response, getAdminId());
				$results = $insertResponse;
			}
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "invoicing_note"){
			$InvoicingProvider = new InvoicingProvider($dataConn);
			$insertResponse = $InvoicingProvider->updateInvoicingNotes($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "insert_note"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->insertNotes($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_hours"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->submitHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_approve"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hours"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hourid"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHourId($newRecord, getAdminId());
			if ($insertResponse->AddedByAdmin){
				$insertResponse->error = "Only ".$insertResponse->AddedByAdmin.", their supervisor, or Liz Budd can remove this entry.";
			}
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
        } else {
			header("HTTP/1.0 201 Created");
		
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'code_update'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->updateCodes($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'code_add'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->addCodes($newRecord, getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif (trim($newRecord->action) == 'group_update'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->updateGroups($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'group_add'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->addGroups($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif (trim($newRecord->action) == 'farm_update'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->updateFarms($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'farm_add'){
			$gbsProvider = new HoursProvider($dataConn);
			$response = $gbsProvider->addFarms($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == "delete_hours"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hourid"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->deleteHourId($newRecord, getAdminId());
			if ($insertResponse->AddedByAdmin){
				$insertResponse->error = "Only ".$insertResponse->AddedByAdmin." can remove this entry.";
			}
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		/* MOVED TO POST
		}else if (trim($newRecord->action) == "submit_hours"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->submitHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		*/
		}else if (trim($newRecord->action) == "submit_remove"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_approve"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "approve_remove"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_approve"){
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_reject"){
			//if wanted to send notification from dreamhost server
			/*
			$path = getcwd();
			$pathSeparator = "/";
			if (!strpos($path,"xampp")){
				$thisPath = ".:/home/cetdash/pear/share/pear";
				set_include_path($thisPath);
			}else{
				$pathSeparator = "\\";
			}
			*/
			require_once "Mail.php";  
			$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
			$Emails = $newRecord->employeeEmails;
			$rejectionMessage = $newRecord->rejectionMessage;
			$timePeriod = $newRecord->timeperiod;
			foreach ($Emails as $Email){
				$insertResponse = $timePeriod;
					$to = $Email; 
					$recipients = $to.",".$from;
					$subject = "Submitted Hours Rejected"; 
					$body = "Your submitted Hours for ".$timePeriod." were rejected.\n\r  ".$rejectionMessage;  
					$host = "smtp.office365.com"; 
					$port = "587"; 
					$username = $_SESSION['AdminEmail']; 
					$password = $_SESSION['AdminAuthenticationCode'];  
					$headers = array (
						'From' => $from,   
						'To' => $to, 
						'Subject' => $subject
					); 
					$smtp = Mail::factory(
						'smtp',   
						array (
							'host' => $host,     
							'port' => $port,     
							'auth' => true,     
							'username' => $username,     
							'password' => $password
						)
					);  
					$mail = $smtp->send($recipients, $headers, $body);  
			}
			$gbsProvider = new HoursProvider($dataConn);
			$insertResponse = $gbsProvider->submitRemove($newRecord, getAdminId());
	
			$results = print_r($mail,true);
			header("HTTP/1.0 201 Created");
		}else{
            $gbsProvider = new HoursProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $gbsProvider->updateCodesDisplayOrder($id,($displayID+1));
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $gbsProvider->updateCodesDisplayOrder($id,0);
					}
				}
				if ($action == 'Insert_CodesMatrix'){
					include_once($dbProviderFolder."HoursProvider.php");
					$gbsProvider = new HoursProvider($dataConn);
					$record->codesId = $item->codesId;
					$record->department = $item->department;
					foreach ($items as $displayID=>$id){
						$record->employeeId = (int)$id;
						$responseCollection[] = $gbsProvider->addCodesMatrix($record,getAdminId());
						$results = $responseCollection;
						if (count($responseCollection)){
							$responseCollection[0] = "Added";
						}
					}
				}
				if ($action == 'Remove_CodesMatrix'){
					include_once($dbProviderFolder."HoursProvider.php");
					$gbsProvider = new HoursProvider($dataConn);
					$record->codesId = $item->codesId;
					$record->department = $item->department;
					foreach ($items as $displayID=>$id){
						$matrixId = (int)$id;
						$responseCollection[] = $gbsProvider->removeCodesMatrix($matrixId);
						if (count($responseCollection)){
							$responseCollection[0] = "Removed";
						}
					}
				}
				
				
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = $action."unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>