<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."GBSProvider.php");
$GBSProvider = new GBSProvider($dataConn);


$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		$results = "Empty GET";
        header("HTTP/1.0 200 Success");
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = trim($newRecord->action);
		if ($action == "LockCommissionData"){
			$lockedData = json_decode($newRecord->lockedData);
			foreach ($lockedData[0] as $FiscalYearMonth=>$owners){
				$FiscalYearMonthParts = explode("_",$FiscalYearMonth);
				$FiscalYear = $FiscalYearMonthParts[0];
				$Month = $FiscalYearMonthParts[1];
				foreach ($owners as $owner=>$typeInfo){
					foreach ($typeInfo as $type=>$amt){
						$thisRecord = array("FiscalYear"=>$FiscalYear,"Month"=>$Month,"Owner"=>$owner,"Type"=>$type,"Amt"=>$amt);
						$responses[] = $GBSProvider->insertEcoBBCommissions($thisRecord,getAdminId());
					}
				}
			}
			$results = $responses;
			header("HTTP/1.0 201 Created");
		}
        break;
}
output_json($results);
die();
?>