<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("MFEP Billable Hours Allocation")
							 ->setSubject("MFEP Billable Hours Allocation")
							 ->setDescription("MFEP Billable Hours Allocation");
//START CurrentMonth
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($allocationCodeHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($headerData["width"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($column . "1")->applyFromArray(
            array('fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FF' . $headerData["color"])
            ),
                'font' => array('size' => 11)
            )
        );
    } catch (PHPExcel_Exception $e) {
    }
    if ($headerData["object"] != "space" && $headerData["object"] != "561" && $headerData["object"] != "562"){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . ($LastRow + 1), "=SUM(" . $column . "3:" . $column . $LastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . ($LastRow + 3), "=" . $column . ($LastRow + 1) . "-" . $column . ($LastRow + 2));
        } catch (PHPExcel_Exception $e) {
        }
    }
}
foreach ($SubtractOriginalAmounts as $column=>$cells){
	$allCells = implode("+",$cells);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . ($LastRow + 2), "=SUM(" . $allCells . ")");
    } catch (PHPExcel_Exception $e) {
    }
}

foreach ($reportRows as $rowId=>$cellInfo){
	foreach ($cellInfo as $cell=>$dataDisplay){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B2", "Name");
} catch (PHPExcel_Exception $e) {
}

//style sheet
try {
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A4:A" . $LastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["peach"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("C4:C" . $LastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["silver"])
        )

        )
    );
} catch (PHPExcel_Exception $e) {
}
foreach($YellowCells as $cell){
        try {
            $objPHPExcel->getActiveSheet()->getStyle($cell)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["yellow"])
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }
	foreach($NameRows as $rowId){
        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId . ":M" . $rowId)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["lightblue"])
                ),
                    'borders' => array(
                        'outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
                    )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["white"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("M" . $rowId)->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => $hexColors["salmon"])
                )

                )
            );
        } catch (PHPExcel_Exception $e) {
        }

    }
try {
    $objPHPExcel->getActiveSheet()->getStyle("B3:B" . $LastRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["olive"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("B" . ($LastRow + 1) . ":M" . ($LastRow + 3))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["summaryorange"])
        ),
            'borders' => array(
                'outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("B" . ($LastRow + 2) . ":M" . ($LastRow + 2))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_NONE
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("T2:AA2")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["darkblue"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("T3:Z3")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["lightblue"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("T4:U" . $predictRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["olive"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("T5:T" . ($predictRow - 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["peach"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("U5:U" . ($predictRow - 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["silver"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("V" . $predictRow . ":AA" . $predictRow)->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["summaryorange"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("AA5:AA" . ($predictRow - 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["pink"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("AC4:AC" . ($predictRow - 1))->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["silver"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("AA3")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["salmon"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("AC3")->applyFromArray(
        array('fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('argb' => $hexColors["lightblue"])
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("U" . ($predictRow + 3) . ":V" . ($predictRow + 6))->applyFromArray(
        array('borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
        )
    );
} catch (PHPExcel_Exception $e) {
}


// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle(date("F", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
//END CurrentMonth
							 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}


$allocationFile = "MFEPBillableAllocationFile_".date("Y_m_d").".xlsx";

$pathExtension = "invoicing".$pathSeparator."MFEP".$pathSeparator.$allocationFile;
$webpathExtension = "invoicing".$pathSeparator."MFEP".$webpathSeparator.$allocationFile;
$saveLocation = $path.$pathExtension;


//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$allocationFileLink = "<a id='allocationFile' href='".$webtrackingLink."'>".$allocationFile."</a><br>";
?>