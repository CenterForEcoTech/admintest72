<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/02_Invoice_Summary_Chart_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}


$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$invoiceFile02 = "02_Invoice_Summary_Chart_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."exports".$pathSeparator.$invoiceFile02;
$webpathExtension = "salesforce".$pathSeparator."exports".$webpathSeparator.$invoiceFile02;

$saveLocation = $path.$pathExtension;
$newRowsToAdd = (count($projectAddress)-1);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(5, $newRowsToAdd);
} catch (PHPExcel_Exception $e) {
}

//add the data
	foreach ($MFInvoiceSummaryRowData as $cell=>$dataDisplay){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
//set print area
/*
$objPHPExcel->getActiveSheet()->getPageSetup()->setPrintArea('A1:G55');	
$objPHPExcel->getActiveSheet()->getStyle("F1:F4")->applyFromArray(
array(	'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)

	)
);
*/
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$InvoiceFile02Link = "<a id='EverSourceTrackingFile' href='".$webtrackingLink."'>".$invoiceFile02."</a><br>";
?>