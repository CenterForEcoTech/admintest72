<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/ColumbiaGas_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "ColumbiaGas".$pathSeparator."525_Report_Pipeline_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$SectionStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
	'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
	'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);
$RecordsStyle = array(
	'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

//pipeline
try {
    $objPHPExcel->setActiveSheetIndex(4);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A1", date("n/d/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "525 - Columbia Gas Pipeline Report");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A2", "Account Name");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B2", "Account Number");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C2", "VisionID");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D2", "City");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E2", "Project");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F2", "Status");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G2", "Therms Saved");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H2", "Completion Date");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:H2")->applyFromArray($SectionStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("C1")->applyFromArray(array('font' => array('size' => 14)));
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(54);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(23.71);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23.71);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(21.28);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13.42);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
} catch (PHPExcel_Exception $e) {
}

$PhaseArray["Completed-Measure Implemented"]["TotalName"]="Completed Projects with Paperwork";
$PhaseArray["Completed-Measure Implemented"]["Color"]="FF92D050";
$PhaseArray["Installed-Final Paperwork In-Progress"]["TotalName"]="Completed Projects Acquiring Paperwork";
$PhaseArray["Installed-Final Paperwork In-Progress"]["Color"]="FF92D050";
$PhaseArray["Total_ThermsYTD"]["TotalName"] = "Projects Completed";
$PhaseArray["Total_ThermsYTD"]["Color"] = "FF00B050";
$PhaseArray["Total_ThermsYTD"]["SumOf"] = array("Completed-Measure Implemented","Installed-Final Paperwork In-Progress");

$PhaseArray["Installation-In-Progress"]["TotalName"]="Projects with Installation In Progress";
$PhaseArray["Installation-In-Progress"]["Color"]="FFFFFF00";
$PhaseArray["Install Agreement Signed"]["TotalName"]="Projects with Install Agreement Signed";
$PhaseArray["Install Agreement Signed"]["Color"]="FFFFFF00";
$PhaseArray["Total_ThermsSigned"]["TotalName"] = "Projects with Signed Installation Agreements";
$PhaseArray["Total_ThermsSigned"]["Color"] = "FFFFC000";
$PhaseArray["Total_ThermsSigned"]["SumOf"] = array("Installation-In-Progress","Install Agreement Signed");

$PhaseArray["Install Agreement In-Progress"]["TotalName"]="Projects with Install Agreement In-Process";
$PhaseArray["Install Agreement In-Progress"]["Color"]="FFFCE4D6";
$PhaseArray["Vendor Proposal-In-Progress"]["TotalName"]="Projects with Vendor Proposal In-Progress";
$PhaseArray["Vendor Proposal-In-Progress"]["Color"]="FFFCE4D6";
$PhaseArray["Engineering Study-In-Progress"]["TotalName"]="Projects with Engineering Study In-Progress";
$PhaseArray["Engineering Study-In-Progress"]["Color"]="FFFCE4D6";
$PhaseArray["Engineering Study-Proposal"]["TotalName"]="Projects with Engineering Study Proposed";
$PhaseArray["Engineering Study-Proposal"]["Color"]="FFFCE4D6";
$PhaseArray["Proposal Sent to Customer"]["TotalName"]="Projects with Proposal Sent to Customer";
$PhaseArray["Proposal Sent to Customer"]["Color"]="FFFCE4D6";
$PhaseArray["Assessment-In-Progress"]["TotalName"]="Projects with Assessment In-Progress";
$PhaseArray["Assessment-In-Progress"]["Color"]="FFFCE4D6";
$PhaseArray["Contact-In-Progress"]["TotalName"]="Projects with Contact In-Progress";
$PhaseArray["Contact-In-Progress"]["Color"]="FFFCE4D6";
$PhaseArray["Potential"]["TotalName"]="Potential Projects";
$PhaseArray["Potential"]["Color"]="FFFCE4D6";
$PhaseArray["2017 Follow-Up"]["TotalName"]="2017 Follow-Up Projects";
$PhaseArray["2017 Follow-Up"]["Color"]="FFFCE4D6";
$PhaseArray["Total_ThermsInProgress"]["TotalName"] = "Total Projects In Progress";
$PhaseArray["Total_ThermsInProgress"]["Color"] = "FFF4B084";
$PhaseArray["Total_ThermsInProgress"]["SumOf"] = array("Install Agreement In-Progress","Vendor Proposal-In-Progress","Engineering Study-In-Progress","Engineering Study-Proposal","Proposal Sent to Customer","Assessment-In-Progress","Contact-In-Progress","Potential","2017 Follow-Up");
$thisRowId = 3;
foreach ($PhaseArray as $GPPhase=>$PhaseInfo){
	echo $GPPhase."=".count($pipelineItems[$GPPhase])."<br><hr>";
	if (strpos(" ".$GPPhase,"Total_")){

		$SumOfArray = $PhaseInfo["SumOf"];
		$sectionTotal = 0.00;
		$sumOfTherms = array();
		foreach ($SumOfArray as $gpPhase){
			$sectionTotal = bcadd($sectionTotal,count($pipelineItems[$gpPhase]),2);
			$sumOfTherms[] = $SummaryRow[$gpPhase];
		}
		$GrandTotalRows[] = $thisRowId;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Total " . $PhaseInfo["TotalName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $sectionTotal);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "=G" . implode("+G", $sumOfTherms));
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray(
                array(
                    'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => $PhaseInfo["Color"])),
                    'font' => array('bold' => true),
                    'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("F" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } catch (PHPExcel_Exception $e) {
        }
        $ThermsSaved[$GPPhase] = $thisRowId;
		$thisRowId++;
		
	}else{
		$recordStart = $thisRowId;
		$recordEnd = $thisRowId;
		//resort completed by date
		if ($GPPhase == "Completed-Measure Implemented"){
			foreach ($pipelineItems[$GPPhase] as $PhaseData){
				$stageCompleted = $PhaseData["stageCompleted"];
				$GPLink = $PhaseData["GPLink"];
				$thisPipeline[$stageCompleted."_".$GPLink] = $PhaseData;
			}
			ksort($thisPipeline);
			$pipelineItems[$GPPhase] = array();
			foreach ($thisPipeline as $PhaseData){
				$pipelineItems[$GPPhase][] = $PhaseData;
			}
		}
		$rowSplit0 = 0;
		foreach ($pipelineItems[$GPPhase] as $PhaseData){
			if ($GPPhase == "Completed-Measure Implemented"){
				$totalThermsCompleted = bcadd($totalThermsCompleted,$PhaseData["Number"],2);
				if ($previousTotal < 130000 && $totalThermsCompleted >= 130000){
					$rowSplit0 = (130000-$previousTotal);
					$rowSplit1 = ($totalThermsCompleted-130000);
				}
				$previousTotal  = $totalThermsCompleted;
				if (!$rowSplit0){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, date("m/d/Y", $PhaseData["stageCompleted"]));
                    } catch (PHPExcel_Exception $e) {
                    }
                    //$objPHPExcel->getActiveSheet()->setCellValue("I".$thisRowId,$totalThermsCompleted);
					if ($PhaseData["stageCompleted"] >= strtotime($invoiceDate) && $totalThermsCompleted > 130000){
						$incentiveRows[] = $thisRowId;
					}
				}
			}
			
			if ($rowSplit0){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $PhaseData["Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $PhaseData["AccountNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $PhaseData["ApplicationNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $PhaseData["City"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $PhaseData["GPName"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $PhaseData["Phase"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $rowSplit0);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, date("m/d/Y", $PhaseData["stageCompleted"]));
                } catch (PHPExcel_Exception $e) {
                }
                //$objPHPExcel->getActiveSheet()->setCellValue("I".$thisRowId,130000);
				$rowSplitID = $thisRowId;
				$thisRowId++;

                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $PhaseData["Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $PhaseData["AccountNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $PhaseData["ApplicationNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $PhaseData["City"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $PhaseData["GPName"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $PhaseData["Phase"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $rowSplit1);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, date("m/d/Y", $PhaseData["stageCompleted"]));
                } catch (PHPExcel_Exception $e) {
                }
                if ($PhaseData["stageCompleted"] >= strtotime($invoiceDate)){
					$incentiveRows[] = $thisRowId;
				}
				//$objPHPExcel->getActiveSheet()->setCellValue("I".$thisRowId,(130000+$rowSplit1));
				$rowSplit0 = 0;				
			}else{
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $PhaseData["Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $PhaseData["AccountNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $PhaseData["ApplicationNumber"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $PhaseData["City"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $PhaseData["GPName"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $PhaseData["Phase"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $PhaseData["Number"]);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$recordEnd = $thisRowId;
			$thisRowId++;
		}
		if ($GPPhase == "Completed-Measure Implemented"){
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $recordStart . ":H" . $recordEnd)->applyFromArray($RecordsStyle);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $recordStart . ":G" . $recordEnd)->applyFromArray($RecordsStyle);
            } catch (PHPExcel_Exception $e) {
            }
        }
		if ($rowSplitID){
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $rowSplitID . ":H" . $rowSplitID)->applyFromArray(
                    array(
                        'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK), 'borderColor' => array('argb' => $PhaseInfo["Color"]))
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
        }

        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Total " . $PhaseInfo["TotalName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, count($pipelineItems[$GPPhase]));
        } catch (PHPExcel_Exception $e) {
        }
        if (count($pipelineItems[$GPPhase])){
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $recordStart . ":H" . $recordEnd)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "=SUM(G" . $recordStart . ":G" . $recordEnd . ")");
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray(
                    array(
                        'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => $PhaseInfo["Color"])),
                        'borders' => array('none' => array('style' => PHPExcel_Style_Border::BORDER_NONE))
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, '0.00');
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray(
                array(
                    'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => $PhaseInfo["Color"])),
                    'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("F" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } catch (PHPExcel_Exception $e) {
        }
        $SummaryRow[$GPPhase] = $thisRowId;
		$thisRowId++;
	}
}
//Grand Total for Pipeline
$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Grand Total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=F" . implode("+F", $GrandTotalRows));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "=G" . implode("+G", $GrandTotalRows));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":G" . $thisRowId)->applyFromArray(
        array(
            'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FFFFC000')),
            'font' => array('bold' => true),
            'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
        )
    );
} catch (PHPExcel_Exception $e) {
}
$ThermsSaved["GrandTotal"] = $thisRowId;
try {
    $objPHPExcel->getActiveSheet()->getStyle("E3:E" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("F" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->getStyle("G3:G" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("G3:G" . $thisRowId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
} catch (PHPExcel_Exception $e) {
}

//525 Report
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("n/1/y", strtotime($invoiceDate)) . "-" . date("n/t/y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "525-" . date("y-n", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G19", count($CustomerReport["ContactedYTD"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G20", count($CustomerReport["Walkthrough"]));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G21", count($CustomerReport["NoInterest"]));
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B25", "='525 Pipeline'!G" . $SummaryRow["Completed-Measure Implemented"]);
} catch (PHPExcel_Exception $e) {
}
//$objPHPExcel->getActiveSheet()->setCellValue("B25","='525 Pipeline'!F".$ThermsSaved["Total_ThermsYTD"]);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B26", "='525 Pipeline'!G" . $SummaryRow["Installed-Final Paperwork In-Progress"]);
} catch (PHPExcel_Exception $e) {
}
//$objPHPExcel->getActiveSheet()->setCellValue("B26","='525 Pipeline'!F".$ThermsSaved["Total_ThermsInProgress"]);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B27", "='525 Pipeline'!G" . $SummaryRow["Installation-In-Progress"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B28", "='525 Pipeline'!G" . $SummaryRow["Install Agreement Signed"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B29", "='525 Pipeline'!G" . $ThermsSaved["Total_ThermsInProgress"]);
} catch (PHPExcel_Exception $e) {
}
//$objPHPExcel->getActiveSheet()->setCellValue("B28","='525 Pipeline'!F".$ThermsSaved["GrandTotal"]);

//add bullets
$thisRowId = 32;
if (count($Bullets)){
	$rowShift = count($Bullets);
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $bulletPoint = $objPHPExcel->getActiveSheet()->getCell("A32")->getValue();
    } catch (PHPExcel_Exception $e) {
    }

    foreach ($Bullets as $bullet){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $bulletPoint);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $bullet["accountName"] . ":  " . $bullet["status"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("B" . $thisRowId . ":H" . $thisRowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $value = $objPHPExcel->getActiveSheet()->getCell("B" . $thisRowId)->getValue();
        } catch (PHPExcel_Exception $e) {
        }
        $width = mb_strwidth ($value); //Return the width of the string
		if ($width > 80){
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($thisRowId)->setRowHeight((16 * ceil($width / 80)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($thisRowId)->setRowHeight(15);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		$thisRowId++;
	}
}

//525 Fee Invoice
$InvoiceNumber["525Fee"] = "525-".date("y-m",strtotime($invoiceDate))."-Fee";
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["525Fee"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodesByRatioTotalAmount["525Fee"] = $objPHPExcel->getActiveSheet()->getCell("H26")->getCalculatedValue();
} catch (PHPExcel_Exception $e) {
}
$revenueCodesByRatioTotalCell["525Fee"] = "'525 Fee Invoice'!H26";


//525 Engineering Invoice
$InvoiceNumber["525Engineer"] = "525-".date("y-m",strtotime($invoiceDate))."-Eng";
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["525Engineer"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E19", $InvoiceHourDataByCode["525J"]);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 19;
if (count($timeCardsHoursByRate)){
	$rowShift = count($timeCardsHoursCount);
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
    ksort($timeCardsHoursByRate);
	foreach ($timeCardsHoursByRate as $rate=>$accountInfo){
		ksort($accountInfo);
		foreach ($accountInfo as $accountName=>$hours){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $accountName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $rate);
            } catch (PHPExcel_Exception $e) {
            }
            $revenueCodes["525Engineer"]["40205"] = $revenueCodes["525Engineer"]["40205"]+bcmul($hours,$rate,2);
			$revenueCodesByEmployee["525Engineer"]["40205"][$accountName] = $revenueCodesByEmployee["525Engineer"]["40205"][$accountName]+bcmul($hours,$rate,2);
			$thisRowId++;
		}
	}
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=SUM(H19:H" . ($thisRowId - 1) . ")");
    } catch (PHPExcel_Exception $e) {
    }
}

//525 Incentive Invoice
$InvoiceNumber["525Incentive"] = "525-".date("y-m",strtotime($invoiceDate))."-Inc";
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["525Incentive"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
$thisRowId = 19;
if (count($incentiveRows)){
	foreach ($incentiveRows as $rowId){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "='525 Pipeline'!B" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "='525 Pipeline'!A" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "='525 Pipeline'!E" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "='525 Pipeline'!G" . $rowId);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "0.60");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=E" . $thisRowId . "*F" . $thisRowId);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
	}
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=SUM(H19:H" . ($thisRowId - 1) . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $revenueCodesByRatioTotalAmount["525Incentive"] = $objPHPExcel->getActiveSheet()->getCell("H" . $thisRowId)->getCalculatedValue();
    } catch (PHPExcel_Exception $e) {
    }
    $revenueCodes["525Incentive"]["40205"] = $revenueCodesByRatioTotalAmount["525Incentive"];
	$revenueCodesByRatioTotalCell["525Incentive"] = "'525 Incentive Invoice'!H".$thisRowId;
	//$revenueCodesByEmployee["525Incentive"]["40205"]["Incentives"] = $revenueCodesByRatioTotalAmount["525Incentive"];
}

$CETInternalFiscalRevenueSheetIndex = 5;	
$CETInternalFiscalRevenueSheetName = "CMA";
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>