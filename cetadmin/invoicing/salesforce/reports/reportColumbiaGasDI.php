<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/ColumbiaGasDI_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "ColumbiaGas".$pathSeparator."525DI_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$SectionStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
	'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
	'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);
$RecordsStyle = array(
	'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);


//525DI Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 3;
$rowShift = count($items);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
} catch (PHPExcel_Exception $e) {
}
if (count($items)){
	foreach ($items as $AccountName=>$itemInfo){
			$AccountNameParts = explode("_",$AccountName);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $itemInfo["accountNumber"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $AccountNameParts[1]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $itemInfo["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $itemInfo["city"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "MA");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $itemInfo["zip"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $itemInfo["SPRY"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, ($itemInfo["SPRY"] ? "=G" . $thisRowId . "*108" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, ($itemInfo["SPRY"] ? "=G" . $thisRowId . "*114" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, $itemInfo["A"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, ($itemInfo["A"] ? "=J" . $thisRowId . "*7" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, ($itemInfo["A"] ? "=J" . $thisRowId . "*17" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $itemInfo["SH"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $itemInfo["HH"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, ($itemInfo["SH"] || $itemInfo["HH"] ? "=(27*M" . $thisRowId . ")+(47.5*N" . $thisRowId . ")" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, ($itemInfo["SH"] || $itemInfo["HH"] ? "=(26.5*M" . $thisRowId . ")+(26.5*N" . $thisRowId . ")" : ""));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $itemInfo["installDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
	}
    try {
        $objPHPExcel->getActiveSheet()->removeRow($thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->removeRow($thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
}//end if count Items

//525DI Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["CMA DI"] = "525D-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["CMA DI"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E18", "=SUM('525D Report'!J3:J" . ($thisRowId + 2) . ")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E19", "=SUM('525D Report'!M3:M" . ($thisRowId + 2) . ")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E20", "=SUM('525D Report'!N3:N" . ($thisRowId + 2) . ")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A21", "=IF(SUM('525D Report'!G3:G" . ($thisRowId + 2) . "),\"Spray Valves\",\"\")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E21", "=IF(SUM('525D Report'!G3:G" . ($thisRowId + 2) . "),SUM('525D Report'!G3:G" . ($thisRowId + 2) . "),\"\")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F21", "=IF(SUM('525D Report'!G3:G" . ($thisRowId + 2) . "),108,\"\")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H21", "=IF(SUM('525D Report'!G3:G" . ($thisRowId + 2) . "),E21*F21,\"\")");
} catch (PHPExcel_Exception $e) {
}

$DollarTotalPerItem["3010.020"] = "H18"; //A
$DollarTotalPerItem["3030.5"] = "H21"; //SPRY
$DollarTotalPerItem["3000.161"] = "H19"; //SH
$DollarTotalPerItem["3000.803"] = "H20"; //HH

$thisRowId = 30;
$revenueCodes = array();
$revenueCodesByEmployee = array();
$revenueCodesByRatioTotalCell = array();
if (count($itemsByEFI)){
	foreach ($itemsByEFI as $source=>$efiInfo){
		foreach ($efiInfo as $EFI=>$qty){
			$cost = $standardizedEFI[$EFI]["Cost"];
			$measureName = $standardizedEFI[$EFI]["MeasureName"];
			$description = $standardizedEFI[$EFI]["Description"];
			// 40222 = ISM Materials
			$dollarAmount = bcmul($cost,$qty,2);
			$revenueCodes[$source][40222] = bcadd($revenueCodes[$source][40222],$dollarAmount,2);
			$revenueCodesByEmployee[$source][40222][$EFI."|".$description."|".$qty] = $dollarAmount;

            try {
                $invoiceAmount = $objPHPExcel->getActiveSheet()->getCell($DollarTotalPerItem[$EFI])->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            // 40220 = ISM Labor
			$laborAmount = bcsub($invoiceAmount,$dollarAmount,2);
			$revenueCodes[$source][40220] = bcadd($revenueCodes[$source][40220],$laborAmount,2);
			$revenueCodesByEmployee[$source][40220][$EFI."|".$description."|".$qty] = $laborAmount;
			
			/* USED FOR CHECKING
			$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,$source);
			$objPHPExcel->getActiveSheet()->setCellValue("B".$thisRowId,$standardizedEFI[$EFI]["Description"]);
			$objPHPExcel->getActiveSheet()->setCellValue("C".$thisRowId,$EFI);
			$objPHPExcel->getActiveSheet()->setCellValue("D".$thisRowId,$qty);
			$objPHPExcel->getActiveSheet()->setCellValue("E".$thisRowId,$cost);
			$objPHPExcel->getActiveSheet()->setCellValue("F".$thisRowId,$dollarAmount);
			$objPHPExcel->getActiveSheet()->setCellValue("G".$thisRowId,"='525D Invoice'!".$DollarTotalPerItem[$EFI]);
			$objPHPExcel->getActiveSheet()->setCellValue("H".$thisRowId,"=(G".$thisRowId."-F".$thisRowId.")");
			$thisRowId++;
			*/

		}
	}
}

$CETInternalFiscalRevenueSheetIndex = 2;	
$CETInternalFiscalRevenueSheetName = "CMA DI";
//Create CET Internal Fiscal Revenue Sheet
include('CETInternalRevenueCodeSheet.php');

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}


//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>