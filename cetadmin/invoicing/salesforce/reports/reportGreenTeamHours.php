<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/GreenTeamHours_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$ReceiptInfo = new stdClass();
	$itemCheck = $results->JobID.$results->Date.$results->Memo;
	$ReceiptInfo->VendorName = $results->SourceName;
	$ReceiptInfo->Description = $results->Memo;
	$ReceiptInfo->CompletedDate = $results->Date;
	$ReceiptInfo->Number = $results->Debit;
	$ReceiptInfo->itemCheck = $itemCheck;
	$passThroughsByJobID[$results->JobID][] = $ReceiptInfo;
	$passThroughsByItem[$itemCheck] = $ReceiptInfo;
}
//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "GreenTeam".$pathSeparator."GreenTeamHours_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowsAdded = 0;

	$InvoiceStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$FooterStyle = array(
		'font'=>array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$SectionStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Fineprint = array(
		'font'  => array('bold'=>true,'size'=>8,'name'=>'Calibri'),
		'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$AmountDueStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
$InvoiceNumber["Green Team"] = "518-".date("y-m",strtotime($invoiceDate));
	$rowsAdded = 18;
	$SubTotalRows = array();
	//inactive code
	//"518N"=>"Green Team - Newsletter Update"
	$InvoiceCodeArray = array("518A"=>"Green Team - Program Administration","518C"=>"Green Team - MCAS Frameworks");
	foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceText){
		$thisRowId = $rowsAdded;
		//create Invoice Code header row
		//echo $InvoiceText." at row".$thisRowId."<br>";
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $InvoiceText);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($InvoiceStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
		$InvoiceCodeSubtotalRows = array();
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			if ($categoryEmployeeCount > 1){
				$rowShift = $categoryEmployeeCount;
				//echo "will add ".$rowShift." before row ".($thisRowId+1)."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                $revenueCodes["Green Team"][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes["Green Team"][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
				$revenueCodesByEmployee["Green Team"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee["Green Team"][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);

				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
				$thisSum = round($theseHours*$categoryRate,2);
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
            } catch (PHPExcel_Exception $e) {
            }
            if ($thisSum){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=SUM(F" . $categoryStartRow . ":F" . $thisLastRow . ")");
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "0");
                    } catch (PHPExcel_Exception $e) {
                    }
                }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisSum);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
            } catch (PHPExcel_Exception $e) {
            }
            $InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
		}
		
		
		//add spacing row between invoice codes
		if (count($InvoiceCodeSubtotalRows)){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Subtotal:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $InvoiceCodeSubtotalRows));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SubtotalStyle);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
            } catch (PHPExcel_Exception $e) {
            }
            $SubTotalRows[] = $thisRowId;
		}else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "No Recorded Hours");
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 2), 3);
        } catch (PHPExcel_Exception $e) {
        }
        $rowsAdded = $rowsAdded+3;
	}
	$thisRowId++;
	$thisRowId++;

try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "Services Subtotal:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $SubTotalRows));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D" . $thisRowId . ":H" . $thisRowId)->applyFromArray($FinalSubtotalStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
$ServicesSubtotalRow = $thisRowId;
	
	//add Receipt Expenses section
	$thisRowId = $thisRowId+2;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Description of Expenses (Receipts Attached)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "Cost");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SectionStyle);
} catch (PHPExcel_Exception $e) {
}
$thisRowId++;
	$ExpensesSectionStart  = $thisRowId;
	//loop through expenses?
	foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceText){
		if (count($passThroughsByJobID[$InvoiceCode])){
			foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
				$itemCheck = $thisPassThrough->itemCheck;
				$itemsToInclude[$itemCheck] = "passThroughsByItem";
				if ($attachmentsByItem[$itemCheck]){
					$itemsInBoth[$itemCheck] = 1;
				}else{
					$Adjustments["Alert"]["Passthrough match not found as attachments"][] = $thisPassThrough->VendorName." ".$thisPassThrough->Description." ".date("m/d/Y",strtotime($thisPassThrough->CompletedDate))." $".$thisPassThrough->Number;
				}
			}
		}
		if (count($attachmentsByJobID[$InvoiceCode])){
			foreach ($attachmentsByJobID[$InvoiceCode] as $thisPassThrough){
				$itemCheck = $thisPassThrough->itemCheck;
				$itemsToInclude[$itemCheck] = "attachmentsByItem";
				if ($passThroughsByItem[$itemCheck]){
					$itemsInBoth[$itemCheck] = 1;
				}else{
					$Adjustments["Alert"]["Attachment match not found in Passthrough file"][] = $thisPassThrough->VendorName." ".$thisPassThrough->Description." ".date("m/d/Y",strtotime($thisPassThrough->CompletedDate))." $".$thisPassThrough->Number;
				}
			}
		}
	}
	foreach ($itemsToInclude as $itemCheck=>$source){
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
        } catch (PHPExcel_Exception $e) {
        }
        $thisPassThrough = ${$source}[$itemCheck];
			//print_pre($thisPassThrough);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $thisPassThrough->VendorName);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $thisPassThrough->Description);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, date("m/d/Y", strtotime($thisPassThrough->CompletedDate)));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisPassThrough->Number);
        } catch (PHPExcel_Exception $e) {
        }
//				$objPHPExcel->getActiveSheet()->getStyle("A".$thisRowId.":H".$thisRowId)->applyFromArray($SummaryStyle);
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        $revenueCodes["Green Team"][40952] = bcadd($revenueCodes["Green Team"][40952],$thisPassThrough->Number,2);
			$revenueCodesByEmployee["Green Team"][40952][$thisPassThrough->VendorName] = bcadd($revenueCodesByEmployee["Green Team"][40952][$thisPassThrough->VendorName],$thisPassThrough->Number,2);
			$InvoiceCodeSubtotalRows[] = $thisRowId;
			$thisRowId++;
			$rowsAdded++;
		$ExpensesSectionEnd = $thisRowId;
	}
	//end of loop
	
	$thisRowId++;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "Expenses Subtotal:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=SUM(H" . $ExpensesSectionStart . ":H" . $ExpensesSectionEnd . ")");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D" . $thisRowId . ":H" . $thisRowId)->applyFromArray($FinalSubtotalStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("H" . $ExpensesSectionStart . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
$SubTotalRows[] = $thisRowId;
	$thisRowId++;
	//$objPHPExcel->getActiveSheet()->setCellValue("A".$thisRowId,"*Expense occurred in previous billing period.");
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":D" . $thisRowId)->applyFromArray($Fineprint);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . ($thisRowId + 2), "Amount Due:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . ($thisRowId + 2), "=H" . implode("+H", $SubTotalRows));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("F" . ($thisRowId + 2) . ":H" . ($thisRowId + 2))->applyFromArray($AmountDueStyle);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("H" . ($thisRowId + 2))->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
$thisRowId = $thisRowId+2;
	if ($thisRowId <= 47){ $thisRowId = 47;}
	//footer
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . ($thisRowId + 2), "Questions about this invoice:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . ($thisRowId + 3), "Contact: Emily Fabel");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . ($thisRowId + 4), "Phone: 413-586-7350 x 290");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . ($thisRowId + 4), "www.CETOnline.org");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 4) . ":G" . ($thisRowId + 4))->applyFromArray($FooterStyle);
} catch (PHPExcel_Exception $e) {
}


try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["Green Team"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}

$CETInternalFiscalRevenueSheetIndex = 1;	
$CETInternalFiscalRevenueSheetName = "GreenTeam";
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>