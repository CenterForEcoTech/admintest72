<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/NGRIDInspections_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "NGRID".$pathSeparator."524_Invoice_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$AmountDueStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FD8D8D8')),
	'font'  => array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)

);
$JustBoldStyle = array(
	'font'  => array('bold'=>true)
);

$SubTotalStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
);

//524 Report
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 3;
$rowShift = count($summaryData);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}
ksort($summaryData);
foreach ($summaryData as $rowInfo=>$workCategory){
		$rowData = explode("_",$rowInfo);
		$siteWorkHrs = ($workCategory["SiteWork"] ? : 0);
		$travelHrs = ($workCategory["Travel"] ? : 0);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $rowData[0]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $rowData[1]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $rowData[2]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $rowData[3]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rowData[4]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $rowData[5]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $siteWorkHrs);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=F" . $thisRowId . "*G" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $travelHrs);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRowId, "=(F" . $thisRowId . "/2)*I" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, "=H" . $thisRowId . "+J" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId++;
}
	//total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("K" . ($thisRowId + 1), "=SUM(K3:K" . ($thisRowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}


//524 Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["NGRIDInspections"] = "524-".date("y-m",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", $InvoiceNumber["NGRIDInspections"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 19;
foreach($summaryRowData as $rowId=>$columnData){
	foreach ($columnData as $columnId=>$data){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($columnId, $data);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$thisRowId++;
}
foreach ($subTotals as $Hcell){
	$Gcell = str_replace("H","G",$Hcell);
    try {
        $objPHPExcel->getActiveSheet()->getStyle($Gcell . ":" . $Hcell)->applyFromArray($SubTotalStyle);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($Gcell)->applyFromArray($JustBoldStyle);
    } catch (PHPExcel_Exception $e) {
    }
}
	$thisRowId++;
	//Total Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Amount Due:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=" . implode("+", $subTotals));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($AmountDueStyle);
} catch (PHPExcel_Exception $e) {
}


$CETInternalFiscalRevenueSheetIndex = 2;
$CETInternalFiscalRevenueSheetName = "NGRID524";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>