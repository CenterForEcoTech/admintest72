<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/RW_NON_Pipeline_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RW".$pathSeparator."RW_NON_Pipeline_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A1", $lastDateofInvoiceDate);
} catch (PHPExcel_Exception $e) {
}
$completedCount = $NONReportTypeCount["completed"];
$completedSummaryRow = 4;
if ($completedCount > 1){
	$rowShift = $completedCount-1;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($completedSummaryRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowId = 3;
foreach ($NONReport["completed"] as $ProjectStatus=>$nonAccounts){
	foreach ($nonAccounts as $thisNONAccount){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $thisNONAccount["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, ucwords($thisNONAccount["city"]));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisNONAccount["GPName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $ProjectStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $thisNONAccount["TA"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $thisNONAccount["initiationDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisNONAccount["lastActionDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        if (strlen($thisNONAccount["name"]) > 35){
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight((15 * ceil(strlen($thisNONAccount["name"]) / 35)));
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight(15);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		$rowId++;
	}
}
//add SummaryRow
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=COUNTA(D3:D" . ($rowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}
$rowId++;

/* combined with Outreach for August 2016
$taCount = $NONReportTypeCount["TA"];
$taSummaryRow = $rowId+1;
if ($taCount > 1){
	$rowShift = $taCount-1;
	$objPHPExcel->getActiveSheet()->insertNewRowBefore($taSummaryRow,$rowShift);
}
$nonTACounter = 0;
foreach ($NONReport["TA"] as $ProjectStatus=>$nonAccounts){
	foreach ($nonAccounts as $thisNONAccount){
		$nonTACounter++;
		$NONReport["TAOrdered"][$thisNONAccount["name"].$nonTACounter] = $thisNONAccount;
	}
}

ksort($NONReport["TAOrdered"]);
foreach ($NONReport["TAOrdered"] as $accountNameCounter=>$thisNONAccount){
		$objPHPExcel->getActiveSheet()->setCellValue("A".$rowId,$thisNONAccount["name"]);
		$objPHPExcel->getActiveSheet()->setCellValue("B".$rowId,ucwords($thisNONAccount["city"]));
		$objPHPExcel->getActiveSheet()->setCellValue("C".$rowId,$thisNONAccount["GPName"]);
		$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,$ProjectStatus);
		$objPHPExcel->getActiveSheet()->setCellValue("E".$rowId,$thisNONAccount["TA"]);
		$objPHPExcel->getActiveSheet()->setCellValue("F".$rowId,$thisNONAccount["initiationDate"]);
		$objPHPExcel->getActiveSheet()->setCellValue("G".$rowId,$thisNONAccount["lastActionDate"]);
		if (strlen($thisNONAccount["name"]) > 35){
			$objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight((15*ceil(strlen($thisNONAccount["name"])/35)));
		}else{
			$objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight(15);
		}
		$rowId++;
	
}
//add SummaryRow
$objPHPExcel->getActiveSheet()->setCellValue("D".$rowId,"=COUNTA(D".($taSummaryRow-1).":D".($rowId-1).")");
$rowId++;
*/


$outreachCount = $NONReportTypeCount["outreach"];
$outreachSummaryRow = $rowId+1;
if ($outreachCount > 1){
	$rowShift = $outreachCount-1;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($outreachSummaryRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}
foreach ($NONReport["outreach"] as $ProjectStatus=>$nonAccounts){
	foreach ($nonAccounts as $thisNONAccount){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $thisNONAccount["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, ucwords($thisNONAccount["city"]));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $thisNONAccount["GPName"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $ProjectStatus);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $thisNONAccount["TA"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $thisNONAccount["initiationDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $thisNONAccount["lastActionDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        if (strlen($thisNONAccount["name"]) > 35){
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight((15 * ceil(strlen($thisNONAccount["name"]) / 35)));
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight(15);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowId++;
	}
}
//add SummaryRow
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, "=COUNTA(D" . ($outreachSummaryRow - 1) . ":D" . ($rowId - 1) . ")");
} catch (PHPExcel_Exception $e) {
}
$rowId++;


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>