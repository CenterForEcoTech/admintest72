<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

//  Read your Excel workbook
$objPHPExcel = new PHPExcel();

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "ReportBullets".$pathSeparator."Monthly_Bullets".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;
$saveLocation = ($isStaff ? str_replace("cetstaff","cetadmin",$saveLocation) : $saveLocation);
$webpathExtension = ($isStaff ? "../../cetadmin/invoicing/" : "").$webpathExtension;
//echo $saveLocation;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Total = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF4B084')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
$headerRow = array(
	"Owner"=>array("A","owner",14),
	"Account Name"=>array("B","accountName",20),
	"Primary Campaign"=>array("C","primaryCampaign",18),
	"Secondary Campaign"=>array("D","secondaryCampaign",18),
	"Green Prospect Name"=>array("E","greenProspectName",20),
	"InitiationDate"=>array("F","initiationDate",14),
	"Current Status Detail"=>array("G","statusDetail",40)
//	,"Phase"=>array("G","phase"),
//	"Type"=>array("H","type"),
//	"Stage Completed"=>array("I","stageCompletedDate")
);
$activeSheet = 0;
try {
    $objPHPExcel->setActiveSheetIndex($activeSheet);
} catch (PHPExcel_Exception $e) {
}
foreach ($bulletsByCodeAccount as $code=>$accountInfo){
	$code = (string)$code;
	if ($activeSheet > 0){
        try {
            $objPHPExcel->createSheet($activeSheet);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->setActiveSheetIndex($activeSheet);
        } catch (PHPExcel_Exception $e) {
        }
    }
    try {
        $objPHPExcel->getActiveSheet()->setTitle($code);
    } catch (PHPExcel_Exception $e) {
    }
    $thisRowId = 1;
	foreach ($headerRow as $title=>$columnInfo){
		$column = $columnInfo[0];
		$columnField = $columnInfo[1];
		$columnWidth = $columnInfo[2];
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $thisRowId, $title);
        } catch (PHPExcel_Exception $e) {
        }
        //set columnwidth
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($columnWidth);
        } catch (PHPExcel_Exception $e) {
        }
        $lastColumn = $column;
	}
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":" . $lastColumn . $thisRowId)->applyFromArray($BoldStyle);
    } catch (PHPExcel_Exception $e) {
    }

    $thisRowId = 2;
	foreach ($accountInfo as $accountId=>$gpInfo){
		foreach ($gpInfo as $gpId=>$detail){
			foreach ($headerRow as $title=>$columnInfo){
				$column = $columnInfo[0];
				$columnField = $columnInfo[1];
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $thisRowId, $detail[$columnField]);
                } catch (PHPExcel_Exception $e) {
                }
                $lastColumn = $column;
			}
			$lastRow = $thisRowId;
			$thisRowId++;
		}
	}
	$activeSheet++;
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";

?>