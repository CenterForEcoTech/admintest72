<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

if (!$isStaff){
	//GET DEPInternal Data
	$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "").$DEPInternalReportFileName;
	//  Read your Excel workbook
	try {
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
	} catch (Exception $e) {
		die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
		. '": ' . $e->getMessage());
	}

	//  Get DEP data
    try {
        $sheet = $objPHPExcel->getSheet(0);
    } catch (PHPExcel_Exception $e) {
    }
    $columnsArray = array("C","D","F");
	foreach ($columnsArray as $column){
		$rowId = 18;
		while ($rowId < 28){
            try {
                $code = $objPHPExcel->getActiveSheet()->getCell("A" . $rowId)->getValue();
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $DEPInternalValues[$code][$column] = $objPHPExcel->getActiveSheet()->getCell($column . $rowId)->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            $rowId++;
		}
	}
	//print_pre($DEPInternalValues);
}

//Get Passthrough values
$criteria = new stdClass();
$criteria->recordedDate = date("m/01/Y",strtotime($invoiceDate." +1 month")); 
$passThroughResults = $GBSProvider->invoiceTool_getPassthrough($criteria);
foreach ($passThroughResults->collection as $results){
	$passThroughs[] = $results;
	$passThroughJobId = $results->JobID;
	$passThroughJobId = ($passThroughJobId == "537A" || $passThroughJobId == "537B" ? "534/539/537AB" : $passThroughJobId);
	
	$passThroughsByJobID[$passThroughJobId][] = $results;
	foreach ($results as $key=>$value){
		if ($key != "GBSPassthrough_ID"){
			$passThroughHeaders[$key] = 1;
		}
	}
}
$reportFileName = ($isStaff ? "RW_Report_Template.xlsx" : "RW_FullReport_Template.xlsx");
$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/".$reportFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RW".$pathSeparator."RW_".($isStaff ? "Report_" : "Invoice_").date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;

$TopBorder = array(
	'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

if ($isStaff){
    try {
        $objPHPExcel->setActiveSheetIndex(0);
    } catch (PHPExcel_Exception $e) {
    }
    $diversionThisMonthRecycled = ($diversionReportThisMonth["Recycled"] < 100 && $diversionReportThisMonth["Recycled"] >= 1 ? round($diversionReportThisMonth["Recycled"]) : $diversionReportThisMonth["Recycled"]);
	$diversionThisMonthReused = ($diversionReportThisMonth["Reused"] < 100 && $diversionReportThisMonth["Reused"] >= 1 ? round($diversionReportThisMonth["Reused"]) : $diversionReportThisMonth["Reused"]);
	$diversionThisMonthComposted = ($diversionReportThisMonth["Composted"] < 100 && $diversionReportThisMonth["Composted"] >= 1 ? round($diversionReportThisMonth["Composted"]) : $diversionReportThisMonth["Composted"]);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/Y", strtotime($invoiceDate)) . "-" . date("m/t/Y", strtotime($invoiceDate)));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G8", "RW-" . date("Y-m", strtotime($invoiceDate)));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D13", date("F", strtotime($invoiceDate)) . " Diversion");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D14", ($diversionThisMonthRecycled ?: "0") . " tons");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F14", round($diversionReportYTD["Recycled"]) . " tons");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D15", ($diversionReportThisMonth["Lamps"] ?: "0") . " units");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F15", $diversionReportYTD["Lamps"] . " units");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D16", ($diversionThisMonthReused ?: "0") . " tons");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F16", round($diversionReportYTD["Reused"]) . " tons");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D17", ($diversionThisMonthComposted ?: "0") . " tons");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F17", round($diversionReportYTD["Composted"]) . " tons");
    } catch (PHPExcel_Exception $e) {
    }

    $HighlightStyle = array('fill'=> array('type'=> PHPExcel_Style_Fill::FILL_SOLID,'color'=> array('argb' => 'FFFFF00')));

	$CodeOrderArray = array("531"=>27,"532"=>45,"Marketing"=>54,"533"=>60,"534"=>68,"535"=>82,"538C"=>107,"536D"=>114,"536"=>115);
	$rowsAdded = 0;
	foreach ($CodeOrderArray as $InvoiceCode=>$DetailRow){
		$rowId = $DetailRow+$rowsAdded;
		$statusRowStart = $rowId;
		$countRowsToDisplay = $bulletsCodeCount[$InvoiceCode];
		
		if ($InvoiceCode == "Marketing"){
			$DataAnalyistRow = 49+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, ($dataAnalysisThisMonth["MarketingEvents"] ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), ($dataAnalysisYTD["MarketingEvents"] ?: "0"));
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowShift = 0;
		if ($countRowsToDisplay > 1){
			$rowShift = $countRowsToDisplay;
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore($statusRowStart, $rowShift);
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		if ($InvoiceCode == "536"){
			//resort based on Primary Campaign Name
			foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
				$status = ($Status == "Completed" ? "Completed" : "Not Completed");
				foreach ($rowsToDisplay as $rowToDisplay){
					$primaryCampaignName = $rowToDisplay["primaryCampaign"];
					$secondaryCampaignName = ($rowToDisplay["secondaryCampaignName"] ? : "Ongoing");
					$campaignName = (substr($primaryCampaignName,0,3) == "536" ? $primaryCampaignName : $secondaryCampaignName);
					$campaignName = ($campaignName == "536 Best Management Practices" ? "536 The Best Management Practices" : $campaignName);
					$bulletsResorted[$status."_".$campaignName][] = $rowToDisplay;
				}
			}
			ksort($bulletsResorted);
			$bulletsByCampaignCodeSorted[$InvoiceCode] = $bulletsResorted;
		}
		
		foreach ($bulletsByCampaignCodeSorted[$InvoiceCode] as $Status=>$rowsToDisplay){
			if ($InvoiceCode == "536"){
				$statusparts = explode("_",$Status);
				$Status = $statusparts[0];
				$campaignName = $statusparts[1];
			}else{
				$campaignName = "";
			}
			foreach ($rowsToDisplay as $rowToDisplay){
				$statusDetails = nl2br($rowToDisplay["statusDetail"],false);
				$accountName = ($rowToDisplay["name"] ? "[".$rowToDisplay["name"]."] " : "");
				$statusDetails = str_replace("<br>"," ",$statusDetails);
				$statusDetails = str_replace("\r\n"," ",$statusDetails);
				$statusDetails = str_replace("\n\r"," ",$statusDetails);
				$statusDetails = str_replace("\r"," ",$statusDetails);
				$statusDetails = str_replace("\n"," ",$statusDetails);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, str_replace("DDD", "", $rowToDisplay["status"]));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $accountName . $statusDetails);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $rowId . ":H" . $rowId);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $value = $objPHPExcel->getActiveSheet()->getCell("B" . $rowId)->getValue();
                } catch (PHPExcel_Exception $e) {
                }
                $width = mb_strwidth ($value); //Return the width of the string
				if ($InvoiceCode == "536"){
					//add border between campaigns
					if ($oldCampaignName && $campaignName != $oldCampaignName){
                        try {
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":H" . $rowId)->applyFromArray($TopBorder);
                        } catch (PHPExcel_Exception $e) {
                        }
                    }
					$oldCampaignName = $campaignName;
				}

				$rowHeight = 15;
				if ($width > 80){
					$rowHeight = ($rowHeight*ceil($width/80));
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $rowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->getRowDimension($rowId)->setRowHeight($rowHeight);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				if ($rowToDisplay["primaryCampaignCode"] !=$InvoiceCode){
					//echo $rowToDisplay["primaryCampaignCode"]."!=".$InvoiceCode."<Br>";
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":B" . $rowId)->applyFromArray($HighlightStyle);
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				
				$statusRowEnd = $rowId;
				$rowId++;
				if ($rowShift){
					$rowsAdded++;
				}
			}
		}

        try {
            $objPHPExcel->getActiveSheet()->getStyle("B" . $statusRowStart . ":B" . $statusRowEnd)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }
        if ($InvoiceCode == "531"){
			$DataAnalyistRow = 30+$rowsAdded;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $DataAnalyistRow, $dataAnalysisThisMonth["CreatedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $DataAnalyistRow, $dataAnalysisYTD["CreatedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 1), $dataAnalysisThisMonth["CompletedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 1), $dataAnalysisYTD["CompletedBI"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 2), $dataAnalysisThisMonth["CreatedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 2), $dataAnalysisYTD["CreatedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 3), $dataAnalysisThisMonth["CompletedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 3), $dataAnalysisYTD["CompletedHP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 4), "=F" . $DataAnalyistRow . "+F" . ($DataAnalyistRow + 2));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 4), "=G" . $DataAnalyistRow . "+G" . ($DataAnalyistRow + 2));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 5), $dataAnalysisThisMonth["Open"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 6), "=F" . ($DataAnalyistRow + 1) . "+F" . ($DataAnalyistRow + 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 6), "=G" . ($DataAnalyistRow + 1) . "+G" . ($DataAnalyistRow + 3));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . ($DataAnalyistRow + 7), "=F" . ($DataAnalyistRow + 4) . "+F" . ($DataAnalyistRow + 5));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . ($DataAnalyistRow + 7), "=G" . ($DataAnalyistRow + 4) . "+F" . ($DataAnalyistRow + 5));
            } catch (PHPExcel_Exception $e) {
            }
        }
		if ($InvoiceCode == "533"){
			$DataAnalyistRow = 72+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $DataAnalyistRow, $NONReportTypeCountThisMonth["completed"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), $NONReportTypeCount["completed"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), ($NONReportTypeCount["TA"] + $NONReportTypeCount["outreach"]));
            } catch (PHPExcel_Exception $e) {
            }
        }
		if ($InvoiceCode == "535"){
			$DataAnalyistRow = 84+$rowsAdded;

            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $DataAnalyistRow, date("F", strtotime($invoiceDate)) . " Website Stats:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 1), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 1), money($analyticsResults["totalVisits"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($DataAnalyistRow + 2), date("F", strtotime($invoiceDate)));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 2), money($analyticsResults["pageviews"], true, true));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 3), $analyticsResults["pageviewsPerSession"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 4), $analyticsResults["avgSessionDuration"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($DataAnalyistRow + 5), $analyticsResults["percentNewSession"]);
            } catch (PHPExcel_Exception $e) {
            }
        }

		
		//$BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		//$objPHPExcel->getActiveSheet()->getStyle("A".$statusRowStart.":".$LastColumn.$statusRowEnd)->applyFromArray($BStyle);		
		
	}//end foreach CodeOrderArray
}// end if isStaff

if (!$isStaff){
	//$InvoiceCodeArray = array("531"=>array("Hotline","E"),"532"=>array("WasteWise","F"),"533"=>array("Marketing","G"),"534/539"=>array("Technical Assistance","H"),"535"=>array("Website","I"),"537A"=>array("Hospitality","J"),"537B"=>array("Food Waste Generators Subject to Ban","K"),"537C"=>array("State Facilities","L"),"536"=>array("Best Management Practices","M"),"538C"=>array("Compost Site Technical Assistance","N"),"DEP"=>array("DEP: Green Business Specialist",100));
	$InvoiceCodeArray = array("531"=>array("Hotline","E"),"532"=>array("WasteWise","F"),"533"=>array("Marketing","G"),
		"534/539/537AB"=>array("Technical Assistance","H"),
		"537C"=>array("State Facilities","L"),"535"=>array("Website","I"),"536"=>array("Best Management Practices","M"),"538C"=>array("Compost Site Technical Assistance","N"),"DEP"=>array("DEP: Green Business Specialist",100)
	);
	//Create Allocation data
    try {
        $objPHPExcel->setActiveSheetIndex(2);
    } catch (PHPExcel_Exception $e) {
    }
    //
	$ActualHours = $InvoiceDataHours["530B"]["Program Manager"]["Macaluso_Lorenzo"]+$InvoiceDataHours["530L"]["Program Manager"]["Macaluso_Lorenzo"];
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C5", $ActualHours);
    } catch (PHPExcel_Exception $e) {
    }
    foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){

		if ($InvoiceCode != "DEP"){
			$InvoiceCodeOriginal = $InvoiceCode;
			$InvoiceCode = ($InvoiceCode == "534/539/537AB" ? "534" : $InvoiceCode);
			$InvoiceCode = ($InvoiceCode == "536" ? "536D" : $InvoiceCode);
			$thisColumn = $InvoiceItem[1];
			$DEPValueContract = $DEPInternalValues[$InvoiceCode]["C"];
			$DEPValueMonth = $DEPInternalValues[$InvoiceCode]["D"];
			$DEPValueBilled = $DEPInternalValues[$InvoiceCode]["F"];
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "9", $DEPValueContract);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "11", $DEPValueMonth);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "12", $DEPValueBilled);
            } catch (PHPExcel_Exception $e) {
            }
            $hoursOriginalLorenzo = $InvoiceDataHours[$InvoiceCodeOriginal]["Program Manager"]["Macaluso_Lorenzo"];
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "6", $hoursOriginalLorenzo);
            } catch (PHPExcel_Exception $e) {
            }
            $hoursTotalLorenzo = $hoursTotalLorenzo+$hoursOriginalLorenzo;
            try {
                $calculatedDifference = $objPHPExcel->getActiveSheet()->getCell($thisColumn . "13")->getCalculatedValue();
            } catch (PHPExcel_Exception $e) {
            }
            if ($calculatedDifference > 0){
				$codesToReallocated[] = $InvoiceCode;
			}
		}
	}
	//now reallocate the actual hours based on weight of originalhours
    try {
        $adjustmentRequiredTotal = $objPHPExcel->getActiveSheet()->getCell("D5")->getCalculatedValue();
    } catch (PHPExcel_Exception $e) {
    }
    $allocatedhoursTotal = 0;
	while ($allocatedhoursTotal < $adjustmentRequiredTotal){
		foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){
			//$InvoiceCode = ($InvoiceCode == "534/539" ? "534" : $InvoiceCode);
			if ($InvoiceCode != "DEP"){
				if (in_array($InvoiceCode,$codesToReallocated)){ //only reallocate to codes with positive balance
					$thisColumn = $InvoiceItem[1];
                    try {
                        $previouslyAllocatedHours = $objPHPExcel->getActiveSheet()->getCell($thisColumn . "5")->getCalculatedValue();
                    } catch (PHPExcel_Exception $e) {
                    }
                    if (!$hoursOriginalLorenzoByCode[$InvoiceCode]){
						$hoursOriginalLorenzoByCode[$InvoiceCode] = ($InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] ? : 0.5);
					}
//					$hoursOriginalLorenzoByCode[$InvoiceCode] = ($hoursOriginalLorenzoByCode[$InvoiceCode] ? $hoursOriginalLorenzo[$InvoiceCode] : ($InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] ? : 0.5));
					$hoursWeight = ($hoursOriginalLorenzoByCode[$InvoiceCode]/$hoursTotalLorenzo);
					$valueTimes4 = ($ActualHours*$hoursWeight)*4; //used to keep things to the closes quarter or half
					$allocatedHoursCeil = ceil($valueTimes4)/4;
					$allocatedHoursFloor = floor($valueTimes4)/4;
					$allocated = false;
					$allocatedHours = 0.25;
					if (!$allocated && ($allocatedhoursTotal+$allocatedHoursCeil) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using Ceil: ".$allocatedhoursTotal."+".$allocatedHoursCeil.". is less than or equal to ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = $allocatedHoursCeil;
						$allocated = true;
						
					}
					if (!$allocated && ($allocatedhoursTotal+$allocatedHoursFloor) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using Floor: ".$allocatedhoursTotal."+".$allocatedHoursFloor." is less than or equal to ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = $allocatedHoursFloor;
						$allocated = true;
					}
					if (!$allocated && ($allocatedhoursTotal+0.25) <= $adjustmentRequiredTotal){
						//echo $InvoiceCode." using 0.25: ".$allocatedhoursTotal."+".$allocatedHoursFloor." is greater than ".$adjustmentRequiredTotal."<Br>";
						$allocatedHours = 0.25;
						$allocated = true;
					}
					if ($allocated){
						//echo $InvoiceCode." has previously allocated ".$previouslyAllocatedHours." and will now use ".($allocatedHours+$previouslyAllocatedHours)." as allocated hours<br>";
						$InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"] = $InvoiceDataHours[$InvoiceCode]["Program Manager"]["Macaluso_Lorenzo"]+$allocatedHours;
                        try {
                            $objPHPExcel->getActiveSheet()->setCellValue($thisColumn . "5", ($allocatedHours + $previouslyAllocatedHours));
                        } catch (PHPExcel_Exception $e) {
                        }
                        $allocatedhoursTotal = $allocatedhoursTotal+$allocatedHours;
					}
				}
			}
		}
	}//end while $allocatedhoursTotal <= $adjustmentRequiredTotal

	//Populate RW Invoice Sheet
    try {
        $objPHPExcel->setActiveSheetIndex(1);
    } catch (PHPExcel_Exception $e) {
    }
    $InvoiceCodeRow = 15;
	$rowsAdded = 0;

	$InvoiceStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'italic'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SubtotalStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$SummaryStyle = array(
		'font'=>array('bold'=>true,'size'=>9,'name'=>'Calibri')
	);
	$StaffStyle = array(
		'font'=>array('bold'=>false,'size'=>9,'name'=>'Calibri')
	);
	$FinalSubtotalStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFD9D9D9')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri')
	);
	$AmountDueStyle = array(
		'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFBFBFBF')),
		'font'  => array('bold'=>true,'size'=>11,'name'=>'Calibri'),
		'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
	$rowsAdded = 15;
	$SubTotalRows = array();
	//print_pre($InvoiceDataHours);
	foreach ($InvoiceCodeArray as $InvoiceCode=>$InvoiceItem){
		$InvoiceText = $InvoiceItem[0];
		$thisRowId = $rowsAdded;
		//create Invoice Code header row
		//echo $InvoiceText." at row".$thisRowId."<br>";
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $InvoiceText);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($InvoiceStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
		$InvoiceCodeSubtotalRows = array();
		//echo $InvoiceCode;
		//print_pre($InvoiceDataHours[$InvoiceCode]);
		foreach ($InvoiceDataHours[$InvoiceCode] as $CategoryName=>$EmployeeInfo){
			$BillingRateType = "RW";
			$categoryEmployeeCount = count($EmployeeInfo);
			$categoryRate = $BillingRateByCategory[$CategoryName]->GBSBillingRate_Rate;
			//if Employees in category for invoice code, add appropriate number of rows
			if ($categoryEmployeeCount > 1){
				$rowShift = $categoryEmployeeCount;
				//echo "will add ".$rowShift." before row ".($thisRowId+1)."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), $rowShift);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$categoryStartRow = $thisRowId;
			//Now fill in Employee info
			$theseHours = 0;
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, str_replace("_", ", ", $Employee_Name));
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($StaffStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $hours);
                } catch (PHPExcel_Exception $e) {
                }
                if ($InvoiceCode == "DEP"){
					//rates last updated 7/1/2016
					//$categoryRate = ($Employee_Name == "Macaluso_Lorenzo" ? 99.00 : 82.50); //other case is for Josh Cook 
					//$totalHours = ($Employee_Name == "Macaluso_Lorenzo" ? 297.00 : 7775.43); //other case is for Josh Cook
					
					//rates last updated 11/14/2016 (also need to update _invoiceTools_RWTAPipeline.php lines 154-155
					$categoryRate = ($Employee_Name == "Macaluso_Lorenzo" ? 115.00 : 95.00); //other case is for Josh Cook 
					$totalHours = ($Employee_Name == "Macaluso_Lorenzo" ? 297 : 7775.43); //other case is for Josh Cook

                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $totalHours);
                    } catch (PHPExcel_Exception $e) {
                    }

                    $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = bcadd($revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]],$totalHours,2);
					$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = bcadd($revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name],$totalHours,2);

                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("F" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
                    } catch (PHPExcel_Exception $e) {
                    }
                    $InvoiceCodeSubtotalRows[] = $thisRowId;
				}else{
					$revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]]+bcmul($hours,$categoryRate,2);
					$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$Employee_Name]][$Employee_Name]+bcmul($hours,$categoryRate,2);
				}
				
				
				$thisLastRow = $thisRowId;
				$theseHours = $theseHours+$hours;
				$thisRowId++;
				$rowsAdded++;
			}
			if ($InvoiceCode != "DEP"){
				$thisSum = round($theseHours*$categoryRate,2);
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $CategoryName . " Subtotal");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
                } catch (PHPExcel_Exception $e) {
                }
                if ($thisSum){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "=SUM(F" . $categoryStartRow . ":F" . $thisLastRow . ")");
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "0");
                    } catch (PHPExcel_Exception $e) {
                    }
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $categoryRate);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisSum);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
                } catch (PHPExcel_Exception $e) {
                }
                $InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
			}
		}
		//add any passthrough items
		if (count($passThroughsByJobID[$InvoiceCode])){
			$BillingRateType = "RW";
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), 1);
            } catch (PHPExcel_Exception $e) {
            }
            $thisRowId++;
			$rowsAdded++;
			foreach ($passThroughsByJobID[$InvoiceCode] as $thisPassThrough){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, MySQLDate($thisPassThrough->Date) . " " . $thisPassThrough->SourceName);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $thisPassThrough->Debit);
                } catch (PHPExcel_Exception $e) {
                }

                $revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$thisPassThrough->Debit,2);
				$revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"][$thisPassThrough->SourceName],$thisPassThrough->Debit,2);

                try {
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SummaryStyle);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->getStyle("H" . $thisRowId . ":H" . $thisRowId)->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""0.00"??_);_(@_)');
                } catch (PHPExcel_Exception $e) {
                }
                $InvoiceCodeSubtotalRows[] = $thisRowId;
				$thisRowId++;
				$rowsAdded++;
			}
		};

		
		
		//add spacing row between invoice codes
		if (count($InvoiceCodeSubtotalRows)){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Subtotal:");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $InvoiceCodeSubtotalRows));
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->applyFromArray($SubtotalStyle);
            } catch (PHPExcel_Exception $e) {
            }
            $SubTotalRows[] = $thisRowId;
		}else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "No Recorded Hours");
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 2), 3);
        } catch (PHPExcel_Exception $e) {
        }
        $rowsAdded = $rowsAdded+3;
	}
	$thisRowId++;
	$thisRowId++;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, "Subtotal:");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=H" . implode("+H", $SubTotalRows));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRowId . ":H" . $thisRowId)->applyFromArray($FinalSubtotalStyle);
    } catch (PHPExcel_Exception $e) {
    }

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . ($thisRowId + 2), "Amount Due:");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . ($thisRowId + 2), "=H" . $thisRowId);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 2) . ":H" . ($thisRowId + 2))->applyFromArray($AmountDueStyle);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H8", "=H" . ($thisRowId + 2));
    } catch (PHPExcel_Exception $e) {
    }

    $InvoiceNumber["RW"] = "RW-".date("y-m",strtotime($invoiceDate));
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H6", $InvoiceNumber["RW"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A7", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
    } catch (PHPExcel_Exception $e) {
    }

    $CETInternalFiscalRevenueSheetIndex = 3;
	$CETInternalFiscalRevenueSheetName = "RW";	
	//Create CET Internal Fiscal Revenue Sheet
	include_once('CETInternalRevenueCodeSheet.php');


    try {
        $objPHPExcel->setActiveSheetIndex(1);
    } catch (PHPExcel_Exception $e) {
    }
}else{
    try {
        $objPHPExcel->setActiveSheetIndex(0);
    } catch (PHPExcel_Exception $e) {
    }
} //end if isStaff
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>