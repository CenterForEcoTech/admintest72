<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/EBBCommissionTracking_Templatev3.xlsx";
//echo "test ".$inputFileName;
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "EBB".$pathSeparator."EcoBBComissionTracking_".date("y-m", strtotime($invoiceEndDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Total = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF4B084')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);

//Add additional sheets here
$sheetIndexArray = array("Amy Weber"=>0,"Paulina Alenkina"=>2,"Freya Bromwich"=>4,"EcoBuilding Bargains"=>6,"Receiving"=>8);	
$monthColumnArray = array("nov"=>"C","dec"=>"D","jan"=>"E","feb"=>"G","mar"=>"H","apr"=>"I","may"=>"K","jun"=>"L","jul"=>"M","aug"=>"O","sep"=>"P","oct"=>"Q");
//print_pre($sheetIndexArray);
//echo "test ".$inputFileName;
foreach ($sheetIndexArray as $owner=>$indexNumber){
	//echo $owner;
    try {
        $objPHPExcel->setActiveSheetIndex($indexNumber);
    } catch (PHPExcel_Exception $e) {
    }
    $startRow = 7;
	//cycle through the months to display any locked data
	foreach ($monthColumnArray as $monthName=>$columnLetter){
		//echo "test ".$inputFileName;
		//echo "[".$FiscalYear."_".$monthName."][".$owner."][Under5K] = ".$lockedData[$FiscalYear."_".$monthName][$owner]["Under5K"]."<br>";
		$dataToDisplay = ($lockedData[$FiscalYear."_".$monthName][$owner]["ClosedWon"] ? $lockedData[$FiscalYear."_".$monthName][$owner]["ClosedWon"] : $monthlyClosedWon[$monthName][$owner]); //Closed Won amounts
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($monthColumnArray[$monthName] . $startRow, $dataToDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$startRow++;
	foreach ($monthColumnArray as $monthName=>$columnLetter){
		//echo "test ".$inputFileName;
		//echo "[".$FiscalYear."_".$monthName."][".$owner."][Under5K] = ".$lockedData[$FiscalYear."_".$monthName][$owner]["Under5K"]."<br>";
		$dataToDisplay = ($lockedData[$FiscalYear."_".$monthName][$owner]["Under5K"] ? $lockedData[$FiscalYear."_".$monthName][$owner]["Under5K"] : $commissionSummaryByOwner[$monthName][$owner]); //Under 5K threshold amounts
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($monthColumnArray[$monthName] . $startRow, $dataToDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$startRow++;
	foreach ($monthColumnArray as $monthName=>$columnLetter){
		//echo "[".$FiscalYear."_".$monthName."][".$owner."][Over5K] = ".$lockedData[$FiscalYear."_".$monthName][$owner]["Over5K"]."<br>";
		$dataToDisplay = ($lockedData[$FiscalYear."_".$monthName][$owner]["Over5K"] ? $lockedData[$FiscalYear."_".$monthName][$owner]["Over5K"] : $commissionSummary5KByOwner[$monthName][$owner]); //Over 5K threshold amounts
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($monthColumnArray[$monthName] . $startRow, $dataToDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$startRow++;
	foreach ($monthColumnArray as $monthName=>$columnLetter){
		$dataToDisplay = ($lockedData[$FiscalYear."_".$monthName][$owner]["AcquisitionExpense"] ? $lockedData[$FiscalYear."_".$monthName][$owner]["AcquisitionExpense"] : $monthlyAcquisitionsExpenses[$monthName][$owner]);//acquisition Amount
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($monthColumnArray[$monthName] . $startRow, $dataToDisplay);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
	//Now Enter Data 
	$indexNumber++;
    try {
        $objPHPExcel->setActiveSheetIndex($indexNumber);
    } catch (PHPExcel_Exception $e) {
    }
    $startRow = 1;
	if (count(($over5KByOwner[$month][$owner]))){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, "Over 3K Threshold Items");
        } catch (PHPExcel_Exception $e) {
        }
        $startRow++;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, "Owner");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $startRow, "Opportunity Name");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $startRow, "Closed Date");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $startRow, "Salesforce Link");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $startRow, "Description");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $startRow, "Sold Price(s)");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $startRow, "Sold Date(s)");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $startRow, "SalesForcePrice");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . ($startRow - 1) . ":F" . ($startRow))->applyFromArray($BoldStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $startRow++;
		ksort($over5KByOwner[$month][$owner]);
		foreach ($over5KByOwner[$month][$owner] as $sfId=>$rowDetails){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, $rowDetails["ItemInfo"]["owner"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $startRow, $rowDetails["ItemInfo"]["opportunityName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $startRow, $rowDetails["ItemInfo"]["closeDate"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $startRow, $rowDetails["ItemInfo"]["sfItemId"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $startRow, $rowDetails["ItemInfo"]["itemDescription"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $startRow, " ");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $startRow, " ");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $startRow, str_replace(",", "", str_replace("$", "", $rowDetails["ItemInfo"]["sfTotalListPrice"])));
            } catch (PHPExcel_Exception $e) {
            }
            $startRow++;
			if (count(	$rowDetails["SoldInfo"])){
				foreach ($rowDetails["SoldInfo"] as $soldDetails){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, $rowDetails["ItemInfo"]["owner"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $startRow, $rowDetails["ItemInfo"]["opportunityName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("C" . $startRow, $rowDetails["ItemInfo"]["closeDate"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $startRow, " ");
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $startRow, $soldDetails["description"] . ((int)$soldDetails["parentItem"] > 0 ? " [piecemeal]" : ""));
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $startRow, $soldDetails["soldPrice"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $startRow, date("m/d/Y G:i a", strtotime($soldDetails["transactionDate"])));
                    } catch (PHPExcel_Exception $e) {
                    }
                    $startRow++;
				}
			}
			
			
		}
	
		$startRow++;
		$startRow++;
	}
	if (count($commissionDetailsByOwner[$month][$owner])){
		$detailsSectionRow = $startRow;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $detailsSectionRow, "Under 5K Threshold Items");
        } catch (PHPExcel_Exception $e) {
        }
        $startRow++;
		//Retrieve Under 5K details
		//set header row
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . ($startRow), "Owner");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . ($startRow), "Opportunity Name");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . ($startRow), "Closed Date");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . ($startRow), "Salesforce Link");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . ($startRow), "Description");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . ($startRow), "Salesforce Price");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("A" . ($detailsSectionRow) . ":F" . ($startRow))->applyFromArray($BoldStyle);
        } catch (PHPExcel_Exception $e) {
        }
        $startRow++;
		$detailStartRow = $startRow;
		foreach ($commissionDetailsByOwner[$month][$owner] as $opportunityName=>$rowInfo){
			foreach ($rowInfo as $count=>$rowDetails){
				$totalPriceInt = str_replace(",","",str_replace("$","",$rowDetails["TOTAL_PRICE"]));
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, $rowDetails["FULL_NAME"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $startRow, $rowDetails["OPPORTUNITY_NAME"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $startRow, $rowDetails["CLOSE_DATE"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $startRow, $rowDetails["ITEMID"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("E" . $startRow, $rowDetails["LINEITEM_DESCRIPTION"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("F" . $startRow, $totalPriceInt);
                } catch (PHPExcel_Exception $e) {
                }
                $detailLastRow = $startRow;
				$startRow++;
			}
		}
		//summary row
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $detailsSectionRow, "Total Sum");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $detailsSectionRow, "=sum(F" . $detailStartRow . ":F" . $detailLastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
    }
}

//Add Monthly Detail Backup
try {
    $objPHPExcel->setActiveSheetIndex(8);
} catch (PHPExcel_Exception $e) {
}
$startRow = 2;
//set header row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A2", "Owner");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B2", "Opportunity Name");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C2", "Closed Date");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D2", "Salesforce Link");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E2", "Description");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F2", "Salesforce Price");
} catch (PHPExcel_Exception $e) {
}
$startRow++;
$detailStartRow = $startRow;
foreach ($commissionDetailsByOwner[$month] as $ownerName=>$opportunityInfo){
	foreach ($opportunityInfo as $opportunityName=>$rowInfo){
		foreach ($rowInfo as $count=>$rowDetails){
			$totalPriceInt = str_replace(",","",str_replace("$","",$rowDetails["TOTAL_PRICE"]));
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $startRow, $rowDetails["FULL_NAME"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $startRow, $rowDetails["OPPORTUNITY_NAME"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $startRow, $rowDetails["CLOSE_DATE"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $startRow, $rowDetails["ITEMLINK"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $startRow, $rowDetails["LINEITEM_DESCRIPTION"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $startRow, $totalPriceInt);
            } catch (PHPExcel_Exception $e) {
            }
            $detailLastRow = $startRow;
			$startRow++;
		}
	}
}
//summary row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Total Sum");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "=sum(F" . $detailStartRow . ":F" . $detailLastRow . ")");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setTitle("Backup Data (" . ucfirst($month) . ")");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>