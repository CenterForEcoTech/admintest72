<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

 
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$trackingFile = "Orders_".date("y_m", strtotime($invoiceDate))."_pDF.xlsx";
$pipeDelimitedFile = "Orders_".date("y_m", strtotime($invoiceDate))."_pDF.txt";

$pathExtension = "salesforce".$pathSeparator."exports".$pathSeparator.$trackingFile;
$webpathExtension = "salesforce".$pathSeparator."exports".$webpathSeparator.$trackingFile;

$pathExtensionPipeDelimited = "salesforce".$pathSeparator."exports".$pathSeparator.$pipeDelimitedFile;
$webpathExtensionPipeDelimited = "salesforce".$pathSeparator."exports".$webpathSeparator.$pipeDelimitedFile;

$saveLocation = $path.$pathExtension;
$saveLocationPipeDelimited = $path.$pathExtensionPipeDelimited;

 
/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("Etrac Orders")
							 ->setSubject("Etrac Orders")
							 ->setDescription("Etrac Orders");
							 
// open/create the csv file
$pipeDelimited = fopen($saveLocationPipeDelimited, 'w');
$pipeHeader = array();
//START ORDERS
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
	foreach ($eTrackOrdersHeader as $column=>$headerData){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($headerData["width"]);
        } catch (PHPExcel_Exception $e) {
        }
        $pipeHeader[] = $headerData["title"];
	}
    fputs($pipeDelimited, implode("|",$pipeHeader)."\n");
	foreach ($eTrackReportOrders as $rowId=>$columnCounter){
		$thisLastRow = ($rowId+2);
		$pipeRows = array();
		foreach ($columnCounter as $cell=>$dataDisplay){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $dataDisplay);
            } catch (PHPExcel_Exception $e) {
            }
            $pipeRows[] = $dataDisplay;
		}
		fputs($pipeDelimited, implode("|",$pipeRows)."\n");
		//fputcsv($pipeDelimited, $pipeRows,"|");
	}
fclose($pipeDelimited);

	
//style sheet
try {
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A2:K" . $thisLastRow)->applyFromArray(
        array('borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
            'font' => array('size' => 11),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FF9ACD32')
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('ORDERS');
} catch (PHPExcel_Exception $e) {
}
//END JOBS
							 
							 

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $webpathExtension;
//echo $webtrackingLink."<Br>";
$eTrackOrdersFileLink = "<a id='EverSourceTrackingFile' href='".$webtrackingLink."'>".$trackingFile."</a><br>";
$eTrackOrdersFileLinkPipeDelimited = "<a id='EverSourceTrackingFile' href='".$webtrackingLinkPipeDelimited."'>".$pipeDelimitedFile."</a><br>";

?>