<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/RW_TA_Pipeline_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "RW".$pathSeparator."RW_TA_Pipeline_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$SheetIndex = 1;
foreach ($ExcelData as $TabName=>$PipelineRecords){
    try {
        $objPHPExcel->setActiveSheetIndex($SheetIndex);
    } catch (PHPExcel_Exception $e) {
    }
    $rowId = 6;
	$TotalInProgressCount = array();
	foreach ($PipelineRecords as $Pipeline=>$rowsToDisplay){
		$pipelineRowStart = $rowId;
		if (count($rowsToDisplay)){
			foreach ($rowsToDisplay as $rowToDisplay){
				if ($rowToDisplay["Account Name"] == $priorAccountName && $rowToDisplay["Last Action Date"] == $priorLastActionDate){
					$rowId = $rowId-1;
					$Number .= "\n".$rowToDisplay["Number"];
					$Unit .= "\n".$rowToDisplay["Unit"];
					$Material .= "\n".$rowToDisplay["Material"]."   ";
				}else{
					$Number = $rowToDisplay["Number"];
					$Unit = $rowToDisplay["Unit"];
					$Material = $rowToDisplay["Material"];
				}

                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, $rowToDisplay["Primary Campaign: Campaign Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $rowToDisplay["Account Name"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $rowToDisplay["Initiation Date"]);
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $rowToDisplay["Last Action Date"]);
                } catch (PHPExcel_Exception $e) {
                }
                if ($TabName == "Mini TA"){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $rowToDisplay["Green Prospect Phase"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $rowToDisplay["Owner: Full Name"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $rowToDisplay["Green Prospect Type"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $Number);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $Unit);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $Material);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $rowToDisplay["Current Status Detail"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $LastColumn = "K";
				}else{
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, $rowToDisplay["RW DEP Summary Report Submitted"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $rowId, $rowToDisplay["Green Prospect Phase"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowId, $rowToDisplay["Owner: Full Name"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $rowId, $rowToDisplay["Green Prospect Type"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowId, $Number);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("J" . $rowId, $Unit);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $rowId, $Material);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowId, $rowToDisplay["Current Status Detail"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $LastColumn = "L";
				}
				$pipelineRowEnd = $rowId;
				$rowId++;
				$priorAccountName = $rowToDisplay["Account Name"];
				$priorLastActionDate = $rowToDisplay["Last Action Date"];
			}
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $pipelineRowStart . ":" . $LastColumn . $pipelineRowEnd)->getAlignment()->setWrapText(true);
            } catch (PHPExcel_Exception $e) {
            }
            $BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $pipelineRowStart . ":" . $LastColumn . $pipelineRowEnd)->applyFromArray($BStyle);
            } catch (PHPExcel_Exception $e) {
            }

            if ($Pipeline == "Does Not Apply"){
				$CompletedCount = ($pipelineRowEnd-$pipelineRowStart+1);
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("D2", "=COUNTA(A" . $pipelineRowStart . ":A" . $pipelineRowEnd . ")");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Total Completed " . $TabName . " TA");
                } catch (PHPExcel_Exception $e) {
                }
                $rowColor = 'FF90EE90';
			}else{
				$InProgressCount = ($pipelineRowEnd-$pipelineRowStart+1);
				$TotalInProgressCount[] = "COUNTA(A".$pipelineRowStart.":A".$pipelineRowEnd.")";
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("C2", "=SUM(" . implode("+", $TotalInProgressCount) . ")");
                } catch (PHPExcel_Exception $e) {
                }
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowId, "Total " . $TabName . " TA " . $Pipeline);
                } catch (PHPExcel_Exception $e) {
                }
                $rowColor = 'FFFFFF00';
			}
            try {
                $objPHPExcel->getActiveSheet()->mergeCells("A" . $rowId . ":" . $LastColumn . $rowId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("A" . $rowId . ":" . $LastColumn . $rowId)->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => $rowColor)
                    )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }

            $rowId++;
		}
	}
	$SheetIndex++;
}
try {
    foreach ($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) {
        $rd->setRowHeight(-1);
    }
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>