<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/TRC_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "TRC".$pathSeparator."TRC_".date("y-m", strtotime($invoiceEndDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Total = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF4B084')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
//TRC 515V Invoice	
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$corridorRowStart = 19;
foreach ($CorridorCount as $corridor=>$count){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $corridorRowStart, "Completion of Corridor " . $corridor . " Site Visits");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $corridorRowStart, $count);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $corridorRowStart, $corridorRate[$corridor]);
    } catch (PHPExcel_Exception $e) {
    }
    $corridorRowStart++;
}
try {
    $revenueCodesByRatioTotalAmount["TRC"] = $objPHPExcel->getActiveSheet()->getCell("H" . $corridorRowStart)->getCalculatedValue();
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["TRC"] = "515V-".date("y-m",strtotime($invoiceEndDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["TRC"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceStartDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceEndDate)))));
} catch (PHPExcel_Exception $e) {
}


//515V Summary	
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
//add month information
$thisRowId = 1;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "Account");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, "Address");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, "Corridor");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, "Date");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, "Bin Left");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, "Status");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($BoldStyle);
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 2;
foreach ($TRCresultsByCorridor as $corridor=>$corridorDetails){
	foreach ($corridorDetails as $details){
		$zipCode = $details["zipCode"];
		$city = $details["city"];
		$thisCorridor = $corridor;
		//only show statusdetail after 'mailing intro letter'
		$statusDetail = stripos($details["statusDetail"],"mailing intro letter");
		$status = ltrim(substr($details["statusDetail"],($statusDetail+20)),".");
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $details["account"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $thisCorridor);
        } catch (PHPExcel_Exception $e) {
        }

        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $details["completedDate"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $details["boxLeft"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("E" . $thisRowId)->applyFromArray($NoStyle);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, trim($status));
        } catch (PHPExcel_Exception $e) {
        }
        $lastRow = $thisRowId;
		$thisRowId++;
	}
}
$BordersStyle = array(
	'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:F" . $lastRow)->applyFromArray($BordersStyle);
} catch (PHPExcel_Exception $e) {
}

//show summary row


try {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $thisRowId, 'Total With Bins');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . ($thisRowId + 1), 'Total Without Bins');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . ($thisRowId + 2), 'Total Business Visits Completed');
} catch (PHPExcel_Exception $e) {
}


try {
    $objPHPExcel->getActiveSheet()->setCellValue('E' . $thisRowId, '=COUNTIF(E2:E' . $lastRow . ',"Yes")');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue('E' . ($thisRowId + 1), '=COUNTIF(E2:E' . $lastRow . ',"No")');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . ($thisRowId + 2), "=SUM(E" . $thisRowId . ":E" . ($thisRowId + 1) . ")");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $thisRowId . ":F" . $thisRowId)->applyFromArray($YesTotal);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 1) . ":F" . ($thisRowId + 1))->applyFromArray($NoTotal);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . ($thisRowId + 2) . ":F" . ($thisRowId + 2))->applyFromArray($Total);
} catch (PHPExcel_Exception $e) {
}


$CETInternalFiscalRevenueSheetIndex = 2;
$CETInternalFiscalRevenueSheetName = "TRC";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";

?>