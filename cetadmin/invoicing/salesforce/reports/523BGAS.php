<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/BGAS_523Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "BGAS".$pathSeparator."523_Hours&RateCodes_".date("Y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;

//Report Tab
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G7", date("m/1/y", strtotime($invoiceDate)) . " - " . date("m/t/y", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "523-" . date("y-n", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A20", "Berkshire Gas Service Territory Outreach - " . date("F", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}

$rowShift = 0;
$WalkthroughRow  = 23;
$WalkthroughCount = count($campaignResultBreakdown["walkthrough"]);
if ($WalkthroughCount > 2){
	$rowShift = $WalkthroughCount-2;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($WalkthroughRow + 1, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}

$walkThroughCounter = 0;
if ($WalkthroughCount){
	foreach ($campaignResultBreakdown["walkthrough"] as $accountName=>$detail){
		$thisRow = ($WalkthroughRow+$walkThroughCounter);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRow, $detail["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("A" . $thisRow . ":D" . $thisRow);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRow, $detail["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("E" . $thisRow . ":H" . $thisRow);
        } catch (PHPExcel_Exception $e) {
        }
        $Adjustments["Review"]["523 Walkthroughs<br>".$detail["reportLink"]][] = $detail["accountNameLink"];
		$walkThroughCounter++;
	}
}else{
	$Adjustments["Review"]["523 Walkthroughs<br>".$detail["reportLink"]][] = "None Detected";
	$walkThroughCounter++;
}

$rowShift = 0;
$InstallationRow  = ($walkThroughCounter-1)+26;
$InstallationCount = count($bgasInstallationActivity);
if ($InstallationCount > 2){
	$rowShift = $InstallationCount-2;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($InstallationRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}

$InstallationCounter = 0;
if ($InstallationCount){
	foreach ($bgasInstallationActivity as $row=>$detail){
		$thisRow = ($InstallationRow+$InstallationCounter);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRow, $detail["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("A" . $thisRow . ":D" . $thisRow);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRow, $detail["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("E" . $thisRow . ":F" . $thisRow);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRow, $detail["installedItems"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("G" . $thisRow . ":H" . $thisRow);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("G" . $thisRow)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }
        if (strlen($detail["installedItems"]) > 20){
            try {
                $objPHPExcel->getActiveSheet()->getRowDimension($thisRow)->setRowHeight((30 * floor(strlen($detail["installedItems"]) / 20)));
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("E" . $thisRow)->getAlignment()->setWrapText(true);
        } catch (PHPExcel_Exception $e) {
        }
        $Adjustments["Review"]["523 Installations<br>".$detail["reportLink"]][] = $detail["accountNameLink"]." ".$detail["installedItems"];
		$LastRow = $thisRow;
		$InstallationCounter++;
	}
	$styleArray = array(
		'font'  => array(
			'bold'  => false
			)
    );
    try {
        $objPHPExcel->getActiveSheet()->getStyle("A" . $InstallationRow . ":H" . $LastRow)->applyFromArray($styleArray);
    } catch (PHPExcel_Exception $e) {
    }
}else{
	$Adjustments["Review"]["523 Installations<br>".$detail["reportLink"]][] = "None Detected";
}


//Invoice Sheet
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["BGAS 523"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}

//insert hours for 523
//$InvoiceItems = $salesForceByCategory[523]; //uses Salesforce Hours
$InvoiceItems = $InvoiceHourDataByCategory["523"]; //uses CET Dashboard
$invoiceHoursCount = count($InvoiceItems);
//print_pre($InvoiceItems);
$rowShift = 0;
$invoiceRow = 20;
$invoiceSummaryRow = 21;
$thisRowId = $invoiceRow;
if ($invoiceHoursCount){
	foreach ($BillingRateByCategory["BGAS_5"] as $thisCategory=>$categoryInfo){
		if ($InvoiceItems[$thisCategory]["hours"]>0 && $InvoiceItems[$thisCategory]["hours"] != "0"){
			$categoryCount++;
			$hours = $InvoiceItems[$thisCategory]["hours"];
			$thisRate = $InvoiceItems[$thisCategory]["rate"];
			if ($categoryCount > 2){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId), 1);
                } catch (PHPExcel_Exception $e) {
                }
                $rowShift++;
			}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $thisCategory);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $thisRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=F" . $thisRowId . "*G" . $thisRowId);
            } catch (PHPExcel_Exception $e) {
            }
            $Adjustments["Review"]["523 Hours"] = $Adjustments["Review"]["523 Hours"]+$hours;
			$Adjustments["Review"]["523 Total"] = "$".money($Adjustments["Review"]["523 Total"]+($hours*$thisRate));
			$thisLastRow = $thisRowId;
			$thisRowId++;
		}
	}	
}else{
	$Adjustments["Review"]["523 Hours"][] = "None Detected";
	$InvoiceCounter++;
}
//Add subtotal Row
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H" . ($invoiceSummaryRow + $rowShift), "=SUM(H" . $invoiceRow . ":H" . $thisLastRow . ")");
} catch (PHPExcel_Exception $e) {
}


//Hours & Rate Codes Tab
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
$rowShift = 0;
$adminRow = 4;
$accountCount = count($HoursRateCodes[523]);
if ($accountCount > 2){
	$rowShift = $accountCount-2;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($adminRow, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}	
$thisRow = 2;
ksort($HoursRateCodes[523]);
//print_pre($HoursRateCodes[523]);
//print_pre($accountUtilityInfo);
foreach ($HoursRateCodes[523] as $accountParts=>$details){
	//print_pre($details);
	$accountPart = explode("QQQ",$accountParts);
	$account = $accountPart[1];
	$accountName = $accountPart[0];
	$gp = $details["gp"];
	$locationId = $details["locationId"];
	$utilityAccountNumber = (trim($gpUtilityInfo[$gp]["utilityAccountNumber"]) != "-" ? trim($gpUtilityInfo[$gp]["utilityAccountNumber"]) : "");
	$utilityAccountNumber = ($utilityAccountNumber && substr($utilityAccountNumber,0,1)=="7" ? $utilityAccountNumber : trim($accountUtilityInfo[$account]["utilityAccountNumber"]));
	
	$utilityLocationID = (trim($locationId) != "-" ? trim($locationId) : "");
	//$utilityLocationID = ($utilityLocationID ? : "-".$account);
	$utilityLocationID = ($utilityLocationID ? : trim($accountUtilityInfo[$account]["utilityLocationID"]));
	$utilityLocationID = (trim($utilityLocationID) != "-" ? $utilityLocationID : "-".$account);

	$thisAccountNumber = (substr($utilityAccountNumber,0,1) == "0" ? ltrim($utilityAccountNumber,"0") : $utilityAccountNumber);
	$thisAccountNumber = (strlen($thisAccountNumber) == 12 ? $thisAccountNumber : substr($thisAccountNumber,0,12));
	
	$BGasResult = $GBSProvider->getBGASCustomer($thisAccountNumber,$utilityLocationID);
	if (count($BGasResult)){
		$contactFirstName = ($BGasResult->LastName != "#" && $BGasResult->FirstName ? $BGasResult->FirstName : $contactFirstName);
		$contactLastName = ($BGasResult->LastName != "#" && $BGasResult->LastName? $BGasResult->LastName: $contactLastName);
		$rateParts = explode(",",$BGasResult->Rate);
		$rate = trim($rateParts[(count($rateParts)-1)]);
		$contactPhone = ($BGasResult->Phone !="#" ? $BGasResult->Phone : $contactPhone);
		$street = $BGasResult->HouseNumber." ".$BGasResult->StreetName;
		$city = $BGasResult->City;
	}
	
	if ($accountName == "General MF Hours Log"){
		$adminTotalHours[523] = bcadd($adminTotalHours[523],$details["hours"],2);
		$adminTotalCost[523] = bcadd($adminTotalCost[523],$details["cost"],2);
        try {
            $objPHPExcel->getActiveSheet()->removeRow($thisRow, 1);
        } catch (PHPExcel_Exception $e) {
        }
        $rowShift = $rowShift-1;
	}else{
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRow, $utilityLocationID);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRow, $thisAccountNumber);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRow, $rate);
        } catch (PHPExcel_Exception $e) {
        }
        //$objPHPExcel->getActiveSheet()->setCellValue("D".$thisRow,$contactFirstName." ".$contactLastName);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRow, $accountNames[$account]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRow, $accountPhone[$account]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRow, $accountStreet[$account]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRow, $accountCity[$account]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRow, $details["hours"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("J" . $thisRow, $details["cost"]);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRow++;
	}

}
//now add admin hours
try {
    $objPHPExcel->getActiveSheet()->setCellValue("I" . ($adminRow + $rowShift), $adminTotalHours[523]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("J" . ($adminRow + $rowShift), $adminTotalCost[523]);
} catch (PHPExcel_Exception $e) {
}

//523 Account Outreach tab
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A2", "All Customers Currently Engaged - " . date("F", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}
$rowShift = 0;
$OutreachRow  = 4;
unset($campaignResultBreakdown["outreach"]["General BGAS C&I Hours"]);
$OutreachCount = count($campaignResultBreakdown["outreach"]);
if ($OutreachCount > 2){
	$rowShift = $OutreachCount-2;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($OutreachRow + 1, $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}

$OutreachCounter = 0;
if ($OutreachCount){
	foreach ($campaignResultBreakdown["outreach"] as $accountName=>$detail){
		$thisRow = ($OutreachRow+$OutreachCounter);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRow, $detail["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRow, $detail["address"]);
        } catch (PHPExcel_Exception $e) {
        }
        $Adjustments["Review"]["523 Outreach<br>".$detail["reportLink"]][] = $detail["accountNameLink"];
		$OutreachCounter++;
	}
}else{
	$Adjustments["Review"]["523 Outreach<br>".$detail["reportLink"]][] = "None Detected";
	$OutreachCounter++;
}

$CETInternalFiscalRevenueSheetIndex = 4;	
//Create CET Internal Fiscal Revenue Sheet
//include_once('CETInternalRevenueCodeSheet.php');
//Created on the reportBG_MF+CI_CostSavings.php

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>