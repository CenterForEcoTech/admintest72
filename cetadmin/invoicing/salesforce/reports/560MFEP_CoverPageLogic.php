<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/MFEP_CoverPageLogicTemplate.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}


$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$MFEPCoverPage = "560_MFEPCoverPageLogic_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator."560_MFEP".$pathSeparator.$MFEPCoverPage;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator."560_MFEP".$pathSeparator.$MFEPCoverPage;
$saveLocation = $path.$pathExtension;

//update the 560i-ia CP Calcs tab
$coverPageTabOrder = array("560i-ia CP Calcs","560D CP Calcs","560C CP Calcs","560A CP Calcs");
foreach ($CoverPageTemplateData as $CoverPageTab=>$tabDetails){
	$coverPageTabCount = array_search($CoverPageTab, $coverPageTabOrder);
    try {
        $objPHPExcel->setActiveSheetIndex($coverPageTabCount);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A2", date("F", strtotime($invoiceDate)));
    } catch (PHPExcel_Exception $e) {
    }

    $StaffDetailRow = 4;
	$CategoryNameRow = 5;
	//$CoverPageTab = "560i-ia CP Calcs";
	$ThisTemplateTab = $CoverPageTemplateData[$CoverPageTab];
	//print_pre($ThisTemplateTab);
	$ThisTemplateTabReverse = array_reverse($ThisTemplateTab);
	$SubTotalCells = array();	
	$CategoryNameCount = 0;
	foreach ($ThisTemplateTab as $CategoryName=>$EmployeeInfo){
		$rowsToAdd=(count($EmployeeInfo)-1);
		if ($rowsToAdd > 0){
		//	echo "will add :".$rowsToAdd." before ".$CategoryNameRow."<br>";
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore($CategoryNameRow, $rowsToAdd);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$thisRow = $StaffDetailRow;
		$startThisCategoryRow = $thisRow;
		foreach ($EmployeeInfo as $Employee_Name=>$hours){
			//echo $Employee_Name;
			
			$cell = "A".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $Employee_Name);
            } catch (PHPExcel_Exception $e) {
            }
            //echo $cell."<br>";
			$cell = "B".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, "=C" . $thisRow . "+D" . $thisRow);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "C".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $hours["C"]);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "D".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $hours["D"]);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "E".$thisRow;
			$BillingRate = ($BillingRateByEmployee[str_replace(", ","_",$Employee_Name)][$CategoryName] ? : $BillingRateByCategory["Administrative Staff"]->GBSBillingRate_Rate);
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $BillingRate);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "F".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, "=E" . $thisRow . "*D" . $thisRow);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "G".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, "=E" . $thisRow . "*B" . $thisRow);
            } catch (PHPExcel_Exception $e) {
            }
            $thisRow++;
			
			//calculation for Actual CoverPage
			$TotalHours = ($hours["D"]+$hours["C"]);
			$CoverPageItems[$CoverPageTab]["REDAEligible"] = $CoverPageItems[$CoverPageTab]["REDAEligible"]+(bcmul($hours["D"],$BillingRate,2));
			$CoverPageItems[$CoverPageTab]["InvoiceSubTotal"] = $CoverPageItems[$CoverPageTab]["InvoiceSubTotal"]+(bcmul($TotalHours,$BillingRate,2));
			$CoverPageItems[$CoverPageTab]["REDAPercentageDollars"] = round(($CoverPageItems[$CoverPageTab]["REDAEligible"]/$CoverPageItems[$CoverPageTab]["InvoiceSubTotal"])*100);
			$CoverPageItems[$CoverPageTab]["TotalHours"] = $CoverPageItems[$CoverPageTab]["TotalHours"]+$TotalHours;
			$CoverPageItems[$CoverPageTab]["REDAHours"] = $CoverPageItems[$CoverPageTab]["REDAHours"]+$hours["D"];
			$CoverPageItems[$CoverPageTab]["REDAPercentageHours"] = round(($CoverPageItems[$CoverPageTab]["REDAHours"]/$CoverPageItems[$CoverPageTab]["TotalHours"])*100);
			
		}
		$StaffDetailRow = ($thisRow+1);
		$CategoryNameRow = ($thisRow+2);
		//add Category Name row
		$cell = "A".$thisRow;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $CategoryName . " Subtotal");
        } catch (PHPExcel_Exception $e) {
        }
        $cell = "G".$thisRow;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($cell, "=SUM(G" . $startThisCategoryRow . ":G" . ($thisRow - 1) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $SubTotalCells[] = $cell;
		$ThisLastRow = $thisRow;
		//echo $CategoryName." ".$cell;
		//echo "<hr>";
		
		$CategoryNameCount++;
	}
	if ($CategoryNameCount < count($BillingRateByCategory)){
        try {
            $objPHPExcel->getActiveSheet()->removeRow($ThisLastRow + 2);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($ThisLastRow + 1);
        } catch (PHPExcel_Exception $e) {
        }
    }
	//add summary rows
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . ($ThisLastRow + 1), "=SUM(B4:B" . $ThisLastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($ThisLastRow + 1), "=SUM(C4:C" . $ThisLastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . ($ThisLastRow + 1), "=SUM(D4:D" . $ThisLastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    //$objPHPExcel->getActiveSheet()->setCellValue("E".($ThisLastRow+1),"=SUM(E4:E".$ThisLastRow.")");
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("F" . ($ThisLastRow + 1), "=SUM(F4:F" . $ThisLastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("G" . ($ThisLastRow + 1), "=" . implode("+", $SubTotalCells));
    } catch (PHPExcel_Exception $e) {
    }

    //style
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(16);
    } catch (PHPExcel_Exception $e) {
    }
    foreach  ($SubTotalCells as $subTotalCell){
        try {
            $objPHPExcel->getActiveSheet()->getStyle($subTotalCell)->getFont()->setBold(true);
        } catch (PHPExcel_Exception $e) {
        }
    }
	
}//end foreach CoverPageTab
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$MFEPCoverPageLogic = $webpathExtension;
//echo $webtrackingLink."<Br>";
$MFEPCoverPageLogicLink = "<a id='MFEPCoverPageLogicFile' href='".$MFEPCoverPageLogic."'>".$MFEPCoverPage."</a><br>";
?>