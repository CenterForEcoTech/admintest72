<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = ($isStaff ? $rootFolder.$adminFolder."invoicing/" : "")."importfiles/NGRIDResidential_Template.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$path = ($isStaff ? str_replace("cetstaff","cetadmin",$path) : $path);
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "NGRID".$pathSeparator."Residential_Invoice_".date("Y-m", strtotime($invoiceDate)).".xlsx";
$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = ($isStaff ? $CurrentServer.$adminFolder."invoicing/" : "")."salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;
$saveLocation = $path.$pathExtension;
$AmountDueStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FD8D8D8')),
	'font'  => array('bold'=>true),
	'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)

);
$JustBoldStyle = array(
	'font'  => array('bold'=>true)
);

$SubTotalStyle = array(
	'fill' 	=> array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF2F2F2')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
);

//Residential Invoice
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["NGRIDResidential"] = $invoiceNumber;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H8", $InvoiceNumber["NGRIDResidential"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D16", "Re: " . date("M Y", strtotime($invoiceDate)) . " BGAS PIGGYBACK");
} catch (PHPExcel_Exception $e) {
}

$thisRowId = 21;
$rowShift = count($InvoiceData);
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($thisRowId + 1), ($rowShift - 1));
} catch (PHPExcel_Exception $e) {
}

foreach ($InvoiceData as $description=>$DescriptionInfo){
	foreach ($DescriptionInfo as $rateType=>$descriptionInfo){
		$Qty = $descriptionInfo["QTY"];
		$UnitCost = $descriptionInfo["UnitCost"];
		$LaborCost = $descriptionInfo["LaborCost"];
		$TotalCost = bcmul($Qty,$UnitCost,2);
		$TotalLaborCost = bcmul($LaborCost,$Qty,2);
		$TotalISMCost = bcsub($TotalCost,$TotalLaborCost,2);
		$employeeName = $descriptionInfo["EFI"]."|".$description."|".$Qty;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $Qty);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $description);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $descriptionInfo["Manufacturer"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $thisRowId, $UnitCost);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=D" . $thisRowId . "*G" . $thisRowId);
        } catch (PHPExcel_Exception $e) {
        }
        $revenueCodes["NGRIDResidential"]["40220"] = $revenueCodes["NGRIDResidential"]["40220"]+$TotalLaborCost; //labor
		$revenueCodes["NGRIDResidential"]["40222"] = $revenueCodes["NGRIDResidential"]["40222"]+$TotalISMCost; //material
		$revenueCodesByEmployee["NGRIDResidential"]["40220"][$employeeName] = $revenueCodesByEmployee["NGRIDResidential"]["40220"][$employeeName]+$TotalLaborCost; //labor
		$revenueCodesByEmployee["NGRIDResidential"]["40222"][$employeeName] = $revenueCodesByEmployee["NGRIDResidential"]["40222"][$employeeName]+$TotalISMCost; //material
		
		$thisRowId++;
	}
}
$CETInternalFiscalRevenueSheetIndex = 1;
$CETInternalFiscalRevenueSheetName = "NGRIDResidential";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>