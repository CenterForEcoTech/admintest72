<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/BG_MF+CI_Cost&Savings_Template.xlsx";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}
//echo "found ".$inputFileName;
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "BGAS".$pathSeparator."BG_MF+CI_Cost&Savings_".date("Y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator.$ReportsFile;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator.$ReportsFile;

$saveLocation = $path.$pathExtension;
	$BoldStyle = array(
		'font'=>array('bold'=>true)
	);
	$NoStyle = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFFE699'))
	);
	$YesTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FFCF8D6')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))

	);
	$NoTotal = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'F8CBAD')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	$Total = array(
		'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('argb'=>'FF4B084')),
		'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
	);
	
//Savings Tab	
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
//add month information
//code=>array(title,savingsAdminRow,CategoryStartRow,SubtotalRow,ItemStartRow,ItemSubtotalRow,CopayRow,Total)
$codeArray = array(811=>array("Multifamily",3,19,21,24,26,27,28),810=>array("C&I Retro",5,32,34,37,39,40,41));
$rowShift = 0;
foreach ($codeArray as $code=>$programInfo){
	$programTitle = $programInfo[0];
	$RowStart = $programInfo[1]+$rowShift;
	$thisRowId = ($RowStart-1);
	$countRowsToDisplay = $campaignResultBreakdownByCodeCount["installation"][$code];	
	if ($countRowsToDisplay > 1){
		$rowShift = ($countRowsToDisplay-1);
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($RowStart, $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	ksort($campaignResultBreakdownByCode["installation"][$code]);
	ksort($campaignResultBreakdownByAccounts[$code]);
	//print_pre($campaignResultBreakdownByCode["installation"][$code]);
	$installedAccounts = array();
	//print_pre($campaignResultBreakdownByAccounts[$code]);
	foreach ($campaignResultBreakdownByAccounts[$code] as $accountId=>$accountCount){
		$installedGPs[$accountId] = array();
		//print_pre($campaignResultBreakdownByCode["installation"][$code][$accountId]);
		foreach ($campaignResultBreakdownByCode["installation"][$code][$accountId] as $details){
			//print_pre($details);
			$gp = $details["GPLink"];
			if (!in_array($accountId,$installedAccounts)){$installedAccounts[] = $accountId;}
			if (!in_array($gp,$installedGPs[$accountId])){$installedGPs[$accountId][] = $gp;}
			$qty = $details["qty"];
			$unitPrice = $details["incentiveAmount"];
			$unitInstallPrice = $details["installAmount"];
			$incentiveAmount = bcmul($unitPrice,$details["qty"],2);
			$installAmount = bcmul($unitInstallPrice,$details["qty"],2);
			$accountName = $accountNames[$details["accountId"]];
			$jobId = $accountJobId[$details["accountId"]];
			$rate = $details["rate"];
			$address = ($details["street"] ? : $gpNames[$details["GPLink"]]);
			
			$measureName = $details["measureName"];
			$city = $details["city"];
			$totalCost = $details["totalCost"];
			$contractor = $details["contractor"];
			$invoiceAccounts[$code][$accountName][$city]["totalCost"] = bcadd($invoiceAccounts[$code][$accountName][$city]["totalCost"],$totalCost,2);
			$invoiceAccounts[$code][$accountName][$city]["rate"] = $rate;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $jobId);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["utilityLocationID"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->getStyle("C" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, " " . $details["utilityAccountNumber"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $programTitle);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $details["incentiveDate"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $accountName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $details["accountPhoneNumber"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $details["contactFirstName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $details["contactLastName"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $details["contactPhone"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $address);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $details["city"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $details["dwellingUnits"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $measureName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, $qty);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("V" . $thisRowId, $contractor);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("W" . $thisRowId, $details["contractorPhone"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("X" . $thisRowId, $details["contractorAddress"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Y" . $thisRowId, $details["contractorCity"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("Z" . $thisRowId, $details["contractorState"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AA" . $thisRowId, $incentiveAmount);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AB" . $thisRowId, $installAmount);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AC" . $thisRowId, $details["annualSavings"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $details["totalHours"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $totalCost);
            } catch (PHPExcel_Exception $e) {
            }
            if ($contractor == "CET"){
				$intalledProducts[$code] = $intalledProducts[$code]+$incentiveAmount;
				$actualInstalledItems[$code][$measureName]["qty"] = $actualInstalledItems[$code][$measureName]["qty"]+$qty;
				$actualInstalledItems[$code][$measureName]["unitPrice"] = $unitInstallPrice;
			}
			$thisRowId++;
		}//end foreach campaignResultBreakdownByCode
		//now add additional just hours records
		if ($campaignResultBreakdownByCodeAdditional["justhours"][$code][$accountId]){
			$justHours = $campaignResultBreakdownByCodeAdditional["justhours"][$code][$accountId];
			foreach ($justHours as $gp=>$details){
				//print_pre($details);
				if (!in_array($gp,$installedGPs[$accountId])){
                    try {
                        $objPHPExcel->getActiveSheet()->insertNewRowBefore($thisRowId, 1);
                    } catch (PHPExcel_Exception $e) {
                    }
                    $rowShift++;
						$qty = $details["qty"];
						$incentiveAmount = bcmul($details["incentiveAmount"],$details["qty"],2);
						$installAmount = bcmul($details["installAmount"],$details["qty"],2);
						$accountName = $accountNames[$details["accountId"]];
						$jobId = $accountJobId[$details["accountId"]];
						$rate = $details["rate"];
						$address = ($details["street"] ? : $gpNames[$details["GPLink"]]);
						$city = $details["city"];
						$totalCost = $details["totalCost"];
						$contractor = $details["contractor"];
						$invoiceAccounts[$code][$accountName][$city]["totalCost"] = bcadd($invoiceAccounts[$code][$accountName][$city]["totalCost"],$totalCost,2);
						$invoiceAccounts[$code][$accountName][$city]["rate"] = $rate;
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $jobId);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("B" . $thisRowId, $details["utilityLocationID"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle("C" . $thisRowId)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, " " . $details["utilityAccountNumber"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $programTitle);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $rate);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $details["incentiveDate"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, $accountName);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("I" . $thisRowId, $details["accountPhoneNumber"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("K" . $thisRowId, $details["contactFirstName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("L" . $thisRowId, $details["contactLastName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("M" . $thisRowId, $details["contactPhone"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("N" . $thisRowId, $address);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("O" . $thisRowId, $city);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("P" . $thisRowId, $details["dwellingUnits"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $thisRowId, $details["measureName"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("T" . $thisRowId, $qty);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("V" . $thisRowId, $contractor);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("W" . $thisRowId, $details["contractorPhone"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("X" . $thisRowId, $details["contractorAddress"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Y" . $thisRowId, $details["contractorCity"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("Z" . $thisRowId, $details["contractorState"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AA" . $thisRowId, $incentiveAmount);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AB" . $thisRowId, $installAmount);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AC" . $thisRowId, $details["annualSavings"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $details["totalHours"]);
                    } catch (PHPExcel_Exception $e) {
                    }
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $totalCost);
                    } catch (PHPExcel_Exception $e) {
                    }
                    if ($contractor == "CET"){
							$intalledProducts[$code] = $intalledProducts[$code]+$incentiveAmount;
						}
					$thisRowId++;
				}
				
			}
		}
	}	
		
	//add admin hours row
	if ($countRowsToDisplay < 1){
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($thisRowId), 1);
        } catch (PHPExcel_Exception $e) {
        }
    }
	//print_pre($adminTotalHours);
	//echo "<hr>".$code. " AD".$thisRowId." = ".$adminTotalHours[$code]."<br>";
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $thisRowId, $adminTotalHours[$code]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $thisRowId, $adminTotalCost[$code]);
    } catch (PHPExcel_Exception $e) {
    }
    $summaryRows[$code]["adminHours"] = $thisRowId;
}

//Invoice Backup Tab	
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowShift = 0;
//811 info
$adminChargesRow = 10;
$installProductsRows = 15;
$accountRows = 12;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $adminChargesRow, "=Savings!AE" . $summaryRows[811]["adminHours"]);
} catch (PHPExcel_Exception $e) {
}
if (count($invoiceAccounts[811]) > 3){
	$rowShift = count($invoiceAccounts[811])-3;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(($installProductsRows - 1), $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}
$thisRowId = $accountRows;
foreach ($invoiceAccounts[811] as $accountName=>$cityInfo){
	foreach ($cityInfo as $city=>$info){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $accountName);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $city);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $info["totalCost"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $info["rate"]);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
	}
}	
$thisRowId = $installProductsRows+$rowShift;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $intalledProducts[811]);
} catch (PHPExcel_Exception $e) {
}

//810 info
$adminChargesRow = 19+$rowShift;
$installProductsRows = 23+$rowShift;
$accountRows = 20+$rowShift;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $adminChargesRow, "=Savings!AE" . $summaryRows[810]["adminHours"]);
} catch (PHPExcel_Exception $e) {
}
if (count($invoiceAccounts[810]) > 3){
	$rowShift = count($invoiceAccounts[810])-3;
    try {
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(($installProductsRows - 1), $rowShift);
    } catch (PHPExcel_Exception $e) {
    }
}
$thisRowId = $accountRows;
foreach ($invoiceAccounts[810] as $accountName=>$cityInfo){
	foreach ($cityInfo as $city=>$info){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $thisRowId, $accountName);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $thisRowId, $city);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $info["totalCost"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $info["rate"]);
        } catch (PHPExcel_Exception $e) {
        }
        $thisRowId++;
	}
}	
$thisRowId = $installProductsRows+$rowShift;
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $intalledProducts[810]);
} catch (PHPExcel_Exception $e) {
}


//update invoice number
/*this code was moved to _invoiceTools_BGAS.php to accommodate the revenue code generation line 36
$startInvoiceNumberFromJuly2015 = 65;	
$monthsFromJuly2015 = datediff("2015-07-01", $invoiceDate, "month");
$invoiceNumber = $startInvoiceNumberFromJuly2015+$monthsFromJuly2015;
*/
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A6", "Invoice Number " . $InvoiceNumber["BGAS 810/811"]);
} catch (PHPExcel_Exception $e) {
}

//Invoice Tab
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $invoiceNumber);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
//code=>array(title,savingsAdminRow,CategoryStartRow,SubtotalRow,ItemStartRow,ItemSubtotalRow,CopayRow)
$rowShift = 0;
foreach ($codeArray as $code=>$programInfo){
	//add hours
	$CategoryStartRow = $programInfo[2]+$rowShift;
	$CategorySubtotalRow = $programInfo[3]+$rowShift;
	$categoryCount = 0;
	$thisRowId = $CategoryStartRow;
	foreach ($BillingRateByCategory["BGAS_8"] as $thisCategory=>$categoryInfo){
		if ($invoicingResults[$code][$thisCategory]){$categoryCount++;}
		foreach ($invoicingResults[$code][$thisCategory] as $thisRate=>$hours){
			if ($categoryCount > 2){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($CategorySubtotalRow, 1);
                } catch (PHPExcel_Exception $e) {
                }
                $CategorySubtotalRow++;
				$rowShift++;
			}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $thisCategory);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $hours);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $thisRate);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=E" . $thisRowId . "*F" . $thisRowId);
            } catch (PHPExcel_Exception $e) {
            }
            $lastRow = $thisRowId;
			$thisRowId++;
		}
	}
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $CategorySubtotalRow, "=SUM(H" . $CategoryStartRow . ":H" . $lastRow . ")");
    } catch (PHPExcel_Exception $e) {
    }

    //add Item
	$ItemStartRow = $programInfo[4]+$rowShift;
	$ItemSubtotalRow = $programInfo[5]+$rowShift;
	$ItemCount = 0;
	$thisRowId = $ItemStartRow;
	if (count($actualInstalledItems[$code])){
		foreach ($actualInstalledItems[$code] as $itemName=>$itemInfo){
			$ItemCount++;
			if ($ItemCount > 2){
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($ItemSubtotalRow, 1);
                } catch (PHPExcel_Exception $e) {
                }
                $ItemSubtotalRow++;
				$rowShift++;
			}
			
			if (strpos(" ".$itemName,"WiFi Thermostat")){$itemName = "WiFi Thermostat";$WifiCopayCount = $itemInfo["qty"];}
			if (strpos(" ".$itemName,"WiFi Hub")){$itemName = "WiFi Hub";}
			if (strpos(" ".$itemName,"Aerator")){$itemName = "Aerator";}
			if (strpos(" ".$itemName,"1.7gpm Earth Shower")){$itemName = "Showerhead";}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, $itemName);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . $thisRowId, $itemInfo["qty"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("F" . $thisRowId, $itemInfo["unitPrice"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . $thisRowId, "=E" . $thisRowId . "*F" . $thisRowId);
            } catch (PHPExcel_Exception $e) {
            }
            $lastRow = $thisRowId;
			$thisRowId++;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $ItemSubtotalRow, "=SUM(H" . $ItemStartRow . ":H" . $lastRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
        //update Wifi Copay
		$WifiCopayRow = $programInfo[6]+$rowShift;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $WifiCopayRow, $WifiCopayCount);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $WifiCopayRow, "=-(E" . $WifiCopayRow . "*F" . $WifiCopayRow . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $WifiCopayTotal = "+H".$WifiCopayRow;
	}else{
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($ItemStartRow + 1), 1);
        } catch (PHPExcel_Exception $e) {
        }
        $ItemSubtotalRow = $ItemSubtotalRow-1;
		$rowShift = ($rowShift-1);
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $thisRowId, "None");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $ItemSubtotalRow, "=SUM(H" . $ItemStartRow . ":H" . $thisRowId . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $WifiCopayRow = $programInfo[6]+$rowShift;
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($WifiCopayRow), 1);
        } catch (PHPExcel_Exception $e) {
        }
        $rowShift = ($rowShift-1);
		$WifiCopayTotal = "";
	}
	//sectionTotal Row
	$totalRow = $programInfo[7]+$rowShift;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $totalRow, "=H" . $CategorySubtotalRow . "+H" . $ItemSubtotalRow . $WifiCopayTotal);
    } catch (PHPExcel_Exception $e) {
    }
}
//print_pre($actualInstalledItems);
foreach ($actualInstalledItems as $code=>$itemInfo){
	foreach ($itemInfo as $measureName=>$measureInfo){
		$unitPrice = $measureInfo["unitPrice"];
		$source = "BGAS 810/811";
		$cost = $standardizedNames[$measureName]["Cost"];
		$EFI = $standardizedNames[$measureName]["EFI"];
		$description = $standardizedNames[$measureName]["Description"];
		$qty = $measureInfo["qty"];
		// 40222 = ISM Materials
		$dollarAmount = bcmul($cost,$qty,2);
		if ($measureName == "Wifi Hub"){
			$dollarAmount = bcsub($dollarAmount,50,2);
		}
		$revenueCodes[$source][40222] = bcadd($revenueCodes[$source][40222],$dollarAmount,2);
		$revenueCodesByEmployee[$source][40222][$EFI."|".$description."|".$qty] = $dollarAmount;
		
		$invoiceAmount = bcmul($qty,$unitPrice,2);
		// 40220 = ISM Labor
		$laborAmount = bcsub($invoiceAmount,$dollarAmount,2);
		$revenueCodes[$source][40220] = bcadd($revenueCodes[$source][40220],$laborAmount,2);
		$revenueCodesByEmployee[$source][40220][$EFI."|".$description."|".$qty] = $laborAmount;
		
		
	}
}

$CETInternalFiscalRevenueSheetIndex = 3;	
$CETInternalFiscalRevenueSheetName = "BGAS";
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');


try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>