<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once dirname(__FILE__) . '/../../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = "importfiles/MFEP_CoverPageTemplate.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$MFEPCoverPage = "560_MFEPCoverPage_Invoice_".date("y-m", strtotime($invoiceDate)).".xlsx";

$pathExtension = "salesforce".$pathSeparator."invoicingExports".$pathSeparator."560_MFEP".$pathSeparator.$MFEPCoverPage;
$webpathExtension = "salesforce".$pathSeparator."invoicingExports".$webpathSeparator."560_MFEP".$pathSeparator.$MFEPCoverPage;

$saveLocation = $path.$pathExtension;

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A7", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", "560-" . date("y-n", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("C17", $CoverPageItems["560A CP Calcs"]["InvoiceSubTotal"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B18", "EA-REDA-Eligible Portion " . $CoverPageItems["560A CP Calcs"]["REDAPercentageDollars"] . "%:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C18", $CoverPageItems["560A CP Calcs"]["REDAEligible"]);
} catch (PHPExcel_Exception $e) {
}
$A19Text = "CET staff spent ".$CoverPageItems["560A CP Calcs"]["TotalHours"]." hours on Program Development, Marketing and Outreach. ".$CoverPageItems["560A CP Calcs"]["REDAHours"]." (".$CoverPageItems["560A CP Calcs"]["REDAPercentageHours"]."%) of which were spent on tasks eligible for REDA reimbursment.  See attached report for a breakdown of tasks.";
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A19", $A19Text);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("C23", $CoverPageItems["560C CP Calcs"]["InvoiceSubTotal"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B24", "EA-REDA-Eligible Portion " . $CoverPageItems["560C CP Calcs"]["REDAPercentageDollars"] . "%:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C24", $CoverPageItems["560C CP Calcs"]["REDAEligible"]);
} catch (PHPExcel_Exception $e) {
}
$C26Text = "CET staff spent ".$CoverPageItems["560C CP Calcs"]["REDAHours"]." Hours (".$CoverPageItems["560C CP Calcs"]["REDAPercentageHours"]."%) starting TA with ".$newFarmCount." new farm".($newFarmCount === 1 ? "" : "s")." and ".$scopingAuditCount." scoping audit".($scopingAuditCount === 1 ? "" : "s");
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A25", $C26Text);
} catch (PHPExcel_Exception $e) {
}

//add rows for farms
	$rowShift = 0;
	if ($newFarmCount+$scopingAuditCount > 1){
		$rowShift = ($newFarmCount+$scopingAuditCount)-1;
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(30, $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	$farmCounter = 0;
	if (count($newFarms)){
		foreach ($newFarms as $row=>$farmDetail){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . (29 + $farmCounter), $farmDetail["ACCOUNT.NAME"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . (29 + $farmCounter), $farmDetail["ADDRESS1_LINE1"] . " " . $farmDetail["ADDRESS1_CITY"] . ", " . $farmDetail["ADDRESS1_STATE"] . " " . $farmDetail["ADDRESS1_ZIP"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . (29 + $farmCounter), "Intake");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . (29 + $farmCounter), "TBD");
            } catch (PHPExcel_Exception $e) {
            }
            $Adjustments["Review"]["New Farms<br>".$newFarmsReportLink][] = "<a href='".$instance_url."/".$farmDetail["AccountNameLinkValue"]."' target='".$farmDetail["AccountNameLinkValue"]."'>".$farmDetail["ACCOUNT.NAME"]."</a>";
			$farmCounter++;
		}
	}else{
		$Adjustments["Review"]["New Farms<br>".$newFarmsReportLink][] = "None Detected";
	}

	if (count($scopingAudits)){
		foreach ($scopingAudits as $row=>$auditDetail){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . (29 + $farmCounter), $auditDetail["name"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . (29 + $farmCounter), $auditDetail["address"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . (29 + $farmCounter), "Scoping Audit");
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . (29 + $farmCounter), "Completed");
            } catch (PHPExcel_Exception $e) {
            }
            $Adjustments["Review"]["Scoped Audits<br>".$scopedAuditsLink][] = "<a href='".$instance_url."/".$auditDetail["AccountNameLink"]."' target='".$auditDetail["AccountNameLink"]."'>".$auditDetail["name"]."</a>";
			$farmCounter++;
		}
	}else{
		$Adjustments["Review"]["Scoped Audits<br>".$scopedAuditsLink][] = "None Detected";
	}	
	$sectionDRows = 31+($newFarmCount+$scopingAuditCount);
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . (31 + $rowShift), $CoverPageItems["560D CP Calcs"]["InvoiceSubTotal"]);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . (32 + $rowShift), "EA-REDA-Eligible Portion " . $CoverPageItems["560D CP Calcs"]["REDAPercentageDollars"] . "%:");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . (32 + $rowShift), $CoverPageItems["560D CP Calcs"]["REDAEligible"]);
} catch (PHPExcel_Exception $e) {
}
$A33Text = "CET staff spent a total of ".$CoverPageItems["560D CP Calcs"]["TotalHours"]." hours working on the database of which ".$CoverPageItems["560D CP Calcs"]["REDAHours"]."(".$CoverPageItems["560D CP Calcs"]["REDAPercentageHours"]."%) hours were for REDA-Eligible tasks.";
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . (33 + $rowShift), $A33Text);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("C" . (36 + $rowShift), "=" . $CoverPageItems["560i-ia CP Calcs"]["InvoiceSubTotal"] . "+SUM(H" . (41 + $rowShift) . ":H" . (41 + $rowShift + (count($auditIncentives) - 1)) . ")");
} catch (PHPExcel_Exception $e) {
}
//	$objPHPExcel->getActiveSheet()->setCellValue("B".(37+$rowShift),"EA-REDA-Eligible Portion 100%:");
//	$objPHPExcel->getActiveSheet()->setCellValue("C".(37+$rowShift),$CoverPageItems["560D CP Calcs"]["REDAEligible"]);
	$A38Text = "CET staff spent a total of ".$CoverPageItems["560i-ia CP Calcs"]["TotalHours"]." hours processing audit incentives.";
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . (38 + $rowShift), $A38Text);
} catch (PHPExcel_Exception $e) {
}
if (count($auditIncentives)){
		if (count($auditIncentives) > 1){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore((42 + $rowShift), (count($auditIncentives) - 1));
            } catch (PHPExcel_Exception $e) {
            }
        }

		foreach ($auditIncentives as $auditIncentive){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . (41 + $rowShift), $auditIncentive["name"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . (41 + $rowShift), $auditIncentive["address"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("G" . (41 + $rowShift), $auditIncentive["source"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . (41 + $rowShift), $auditIncentive["incentive"]);
            } catch (PHPExcel_Exception $e) {
            }
            $rowShift++;
			$Adjustments["Review"]["Audit Incentives<br>".$MFEPAuditsLink][] = "<a href='".$instance_url."/".$auditIncentive["AccountNameLink"]."' target='".$auditIncentive["AccountNameLink"]."'>".$auditIncentive["name"]."</a> $".money($auditIncentive["incentive"]);
		}
	}else{
		$A41Text = "No Audits were paid in the month of ".date("F",strtotime($invoiceDate));
        try {
            $objPHPExcel->getActiveSheet()->mergeCells("A" . (41 + $rowShift) . ":F" . (41 + $rowShift));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . (41 + $rowShift), $A41Text);
        } catch (PHPExcel_Exception $e) {
        }
        $Adjustments["Review"]["Audit Incentives<br>".$MFEPAuditsLink][] = "None Detected";
	}
try {
    $objPHPExcel->getActiveSheet()->insertNewRowBefore((42 + ($rowShift - 1)), 3);
} catch (PHPExcel_Exception $e) {
}

//$objPHPExcel->getActiveSheet()->removeRow($ThisLastRow+2);
	//$objPHPExcel->getActiveSheet()->getStyle($subTotalCell)->getFont()->setBold(true);

try {
    $objPHPExcel->getActiveSheet()->setTitle('560 ' . date("F", strtotime($invoiceDate)) . ' CP');
} catch (PHPExcel_Exception $e) {
}


//Now create 560 Invoice tab
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$InvoiceNumber["MFEP"] = "560-".date("y-n",strtotime($invoiceDate));
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A8", "Service Provided from " . date("F jS", strtotime($invoiceDate)) . " to " . date("F jS, Y", strtotime(date("m/t/Y", strtotime($invoiceDate)))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $InvoiceNumber["MFEP"]);
} catch (PHPExcel_Exception $e) {
}

$CodeOrderArray = array("560A"=>19,"560C"=>29,"560IA"=>39,"560I"=>54,"560D"=>67,"560B"=>77);
$TotalCostRows = 85;
$CreditsRow = 119;

$BillingRateType = "MFEP"; //used for RevenueCodes
foreach ($CodeOrderArray as $InvoiceCode=>$StaffDetailRow){
	$StaffDetailRow = $StaffDetailRow+$staffRowsAdded;
	$CategoryNameRow = $StaffDetailRow+1;
	$ThisInvoiceData = $InvoiceData[$InvoiceCode];
	$ThisInvoiceDataReverse = array_reverse($ThisInvoiceData);
	$SubTotalCells = array();	
	$CategoriesCountWithNoEmployeeHours = array();
//	echo $InvoiceCode." starting staffRow = ".$StaffDetailRow."<br>";
	if (!count($ThisInvoiceData)){
		$CodesWithNoEmployees[$InvoiceCode] = $StaffDetailRow;
		//remove blank rows	between total and bottom info
        try {
            $objPHPExcel->getActiveSheet()->removeRow($StaffDetailRow + 5);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($StaffDetailRow + 4);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($StaffDetailRow + 3);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($StaffDetailRow + 2);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($StaffDetailRow + 1);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $StaffDetailRow, "No Staff Hours");
        } catch (PHPExcel_Exception $e) {
        }
        $staffRowsAdded = $staffRowsAdded-5;
		
		
	}
	foreach ($ThisInvoiceData as $CategoryName=>$EmployeeInfo){
		$BillingRate = $BillingRateByCategory[$CategoryName]->GBSBillingRate_Rate;
		$categoryRate = $BillingRate;
//		echo $InvoiceCode." ".$CategoryName."  has ".count($EmployeeInfo)." employees<br>";
		if (count($EmployeeInfo)){
			$staffRowsAdded = $staffRowsAdded+(count($EmployeeInfo)-1);
			$rowsToAdd=(count($EmployeeInfo)-1);
	//		echo $CategoryName." ";
			if ($rowsToAdd > 0){
	//			echo "will add ".$rowsToAdd." before ".$CategoryNameRow."<br>";
                try {
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($CategoryNameRow, $rowsToAdd);
                } catch (PHPExcel_Exception $e) {
                }
            }
			$thisRow = $StaffDetailRow;
			$startThisCategoryRow = $thisRow;
	//		echo $CategoryName." staff will start at ".$startThisCategoryRow."<br>";
			foreach ($EmployeeInfo as $Employee_Name=>$hours){
				$employee_name = str_replace(", ","_",$Employee_Name);
				//echo $Employee_Name;
				$TotalHours = ($hours["D"]+$hours["C"]);
				
				$cell = "A".$thisRow;
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($cell, $Employee_Name);
                } catch (PHPExcel_Exception $e) {
                }
                //echo $cell."<br>";
				$cell = "F".$thisRow;
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($cell, $TotalHours);
                } catch (PHPExcel_Exception $e) {
                }

                $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]] = $revenueCodes[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]]+bcmul($TotalHours,$categoryRate,2);
				$revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name] = $revenueCodesByEmployee[$BillingRateType][$EmployeesRevenueCodeByName[$employee_name]][$employee_name]+bcmul($TotalHours,$categoryRate,2);
				
				$thisRow++;
				//calculation for Actual CoverPage
			}
	//		echo $CategoryName." subtotal will start now at row ".$CategoryNameRow."<br>";
			//add Category Name row
			$cell = "A".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $CategoryName . " Subtotal");
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "F".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, "=SUM(F" . $startThisCategoryRow . ":F" . ($thisRow - 1) . ")");
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "G".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, $BillingRate);
            } catch (PHPExcel_Exception $e) {
            }
            $cell = "H".$thisRow;
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($cell, "=G" . $thisRow . "*F" . $thisRow);
            } catch (PHPExcel_Exception $e) {
            }
            $SubTotalCells[] = $cell;
			//echo $CategoryName." ".$cell;
			//echo "<hr>";
			
			$StaffDetailRow = ($thisRow+1);
			$CategoryNameRow = ($thisRow+2);
			$ThisLastRow = $thisRow;
			$CategoryNameCount++;
		}else{
			$CategoriesCountWithNoEmployeeHours[]=1;
			//category is empty
		}
	}// end foreach $ThisInvoiceData as $CategoryName
	if (count($CategoriesCountWithNoEmployeeHours)){
		foreach ($CategoriesCountWithNoEmployeeHours as $id=>$count){
//			echo "will remove row: ".($ThisLastRow+2)."<br>";
//			echo "will remove row: ".($ThisLastRow+1)."<br>";
            try {
                $objPHPExcel->getActiveSheet()->removeRow($ThisLastRow + 2);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->removeRow($ThisLastRow + 1);
            } catch (PHPExcel_Exception $e) {
            }
            $staffRowsAdded = $staffRowsAdded-2;
		}
	}
	//add summary rows
	if (count($SubTotalCells)){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . ($ThisLastRow + 1), "=" . implode("+", $SubTotalCells));
        } catch (PHPExcel_Exception $e) {
        }
        $SubTotalCellsCollective[] = "H".($ThisLastRow+1);
		//style
		foreach  ($SubTotalCells as $subTotalCell){
	//		$objPHPExcel->getActiveSheet()->getStyle($subTotalCell)->getFont()->setBold(true);
		}
	}
	if ($InvoiceCode == "560IA"){
		$AuditIncentiveRow = 47+$staffRowsAdded;
	}
	if ($InvoiceCode == "560I"){
		$ProjectIncentiveRow = 62+$staffRowsAdded;
	}
	
}//end foreach $CodeOrderArray
//add Audit Incentives and Project Incentives
	if (count($auditIncentives)){
		if (count($auditIncentives) > 1){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($AuditIncentiveRow + 1), (count($auditIncentives) - 1));
            } catch (PHPExcel_Exception $e) {
            }
        }
		
		$incentiveRowsAdded = 0;
		foreach ($auditIncentives as $auditIncentive){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . ($AuditIncentiveRow + $incentiveRowsAdded), "Audit Incentive for GDS on behalf of " . $auditIncentive["name"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . ($AuditIncentiveRow + $incentiveRowsAdded), $auditIncentive["date"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . ($AuditIncentiveRow + $incentiveRowsAdded), $auditIncentive["incentive"]);
            } catch (PHPExcel_Exception $e) {
            }

            $revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$auditIncentive["incentive"],2);
				$revenueCodesByEmployee[$BillingRateType]["40952"]["Audit Incentive - ".$auditIncentive["name"]] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"]["Audit Incentive - ".$auditIncentive["name"]],$auditIncentive["incentive"],2);
			
			$incentiveRowsAdded++;
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . ($AuditIncentiveRow + $incentiveRowsAdded), "=SUM(H" . $AuditIncentiveRow . ":H" . ($AuditIncentiveRow + ($incentiveRowsAdded - 1)) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $SubTotalCellsCollective[] = "H".($AuditIncentiveRow+$incentiveRowsAdded);
		$incentiveRowsAdded++;
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . ($AuditIncentiveRow + $incentiveRowsAdded), "560IA Subtotal:");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . ($AuditIncentiveRow + $incentiveRowsAdded), "=SUM(H" . ($AuditIncentiveRow - 2) . ":H" . ($AuditIncentiveRow + ($incentiveRowsAdded - 2)) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("G" . ($AuditIncentiveRow + $incentiveRowsAdded) . ":H" . ($AuditIncentiveRow + $incentiveRowsAdded))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FBFBFBF')
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . ($AuditIncentiveRow + $incentiveRowsAdded))->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""-"??_);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }

        $staffRowsAdded = $staffRowsAdded+$incentiveRowsAdded;
	}else{
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($AuditIncentiveRow + 1));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($AuditIncentiveRow);
        } catch (PHPExcel_Exception $e) {
        }
        $staffRowsAdded = $staffRowsAdded-2;
		$ProjectIncentiveRow = $ProjectIncentiveRow-2;
	}
	if (count($projectIncentives)){
		if ($incentiveRowsAdded){
			$ProjectIncentiveRow = $ProjectIncentiveRow+($incentiveRowsAdded-2);
		}
		if (count($projectIncentives) > 1){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(($ProjectIncentiveRow + 1), (count($projectIncentives) - 1));
            } catch (PHPExcel_Exception $e) {
            }
        }

		$incentiveRowsAdded = 0;
		foreach ($projectIncentives as $projectIncentive){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . ($ProjectIncentiveRow + $incentiveRowsAdded), "MDAR Project Incentive for " . $projectIncentive["name"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("E" . ($ProjectIncentiveRow + $incentiveRowsAdded), $projectIncentive["date"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("H" . ($ProjectIncentiveRow + $incentiveRowsAdded), $projectIncentive["incentive"]);
            } catch (PHPExcel_Exception $e) {
            }

            $revenueCodes[$BillingRateType]["40952"] = bcadd($revenueCodes[$BillingRateType]["40952"],$projectIncentive["incentive"],2);
			$revenueCodesByEmployee[$BillingRateType]["40952"]["Project Incentive - ".$projectIncentive["name"]] = bcadd($revenueCodesByEmployee[$BillingRateType]["40952"]["Project Incentive - ".$projectIncentive["name"]],$projectIncentive["incentive"],2);
			
			$incentiveRowsAdded++;
			$Adjustments["Review"]["Project Incentives<br>".$MFEPProjectsLink][] = "<a href='".$instance_url."/".$projectIncentive["AccountNameLink"]."' target='".$projectIncentive["AccountNameLink"]."'>".$projectIncentive["name"]."</a> $".money($projectIncentive["incentive"]);
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . ($ProjectIncentiveRow + $incentiveRowsAdded), "=SUM(H" . $ProjectIncentiveRow . ":H" . ($ProjectIncentiveRow + ($incentiveRowsAdded - 1)) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        $SubTotalCellsCollective[] = "H".($ProjectIncentiveRow+$incentiveRowsAdded);
		$incentiveRowsAdded++;
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($ProjectIncentiveRow + $incentiveRowsAdded + 1), 2);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("G" . ($ProjectIncentiveRow + $incentiveRowsAdded), "560I Subtotal:");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("H" . ($ProjectIncentiveRow + $incentiveRowsAdded), "=SUM(H" . ($ProjectIncentiveRow - 2) . ":H" . ($ProjectIncentiveRow + ($incentiveRowsAdded - 2)) . ")");
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->getStyle("G" . ($ProjectIncentiveRow + $incentiveRowsAdded) . ":H" . ($ProjectIncentiveRow + $incentiveRowsAdded))->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FBFBFBF')
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
        $incentiveRowsAdded = $incentiveRowsAdded+2;
        try {
            $objPHPExcel->getActiveSheet()->getStyle("H" . ($ProjectIncentiveRow + $incentiveRowsAdded))->getNumberFormat()->setFormatCode('_("$"#,##0.00_);_("$"\(#,##0.00\);_("$""-"??_);_(@_)');
        } catch (PHPExcel_Exception $e) {
        }
        $staffRowsAdded = $staffRowsAdded+$incentiveRowsAdded;
	}else{
        try {
            $objPHPExcel->getActiveSheet()->removeRow($ProjectIncentiveRow + 3);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow(($ProjectIncentiveRow + 2));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->removeRow($ProjectIncentiveRow + 1);
        } catch (PHPExcel_Exception $e) {
        }
        $staffRowsAdded = $staffRowsAdded-3;
		$Adjustments["Review"]["Project Incentives<br>".$MFEPProjectsLink][] = "None Detected";
	}


	
//add Total Costs rows
	$TotalCostRows = $TotalCostRows+$staffRowsAdded;
	$SubTotalFinalRow = $TotalCostRows+3;
	$AmountDueRow = $SubTotalFinalRow+1;
/*
	if (count($SubTotalCellsCollective)){
		//$objPHPExcel->getActiveSheet()->setCellValue("H".($TotalCostRows+$staffRowsAdded),"=".implode("+",$SubTotalCellsCollective));
	}
	$objPHPExcel->getActiveSheet()->setCellValue("H".($SubTotalFinalRow),"=SUM(H".$TotalCostRows.":H".(($SubTotalFinalRow-1)+$staffRowsAdded).")");
	$objPHPExcel->getActiveSheet()->setCellValue("H".($AmountDueRow),"=H".($SubTotalFinalRow+$staffRowsAdded));
	$objPHPExcel->getActiveSheet()->setCellValue("H9","=H".($AmountDueRow));
*/	
//remove blank rows	between total and bottom info
$rowRemoved = 0;
//echo "Remove ".$staffRowsAdded." starting at ".($AmountDueRow+1);
while ($rowRemoved < ($staffRowsAdded-2)){
	$thisRow = ($AmountDueRow+1);
    try {
        $objPHPExcel->getActiveSheet()->removeRow($thisRow);
    } catch (PHPExcel_Exception $e) {
    }
    $rowRemoved++;
}

/* Megan asked to have the raw data separated 9/2016
//Update Report Pieces
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->setCellValue("G7",date("m/1/y - m/t/y",strtotime($invoiceDate)));
$objPHPExcel->getActiveSheet()->setCellValue("G8","560-".date("y-n",strtotime($invoiceDate)));

$CodeOrderArray = array("560A"=>18,"560C"=>25,"560I"=>32,"560IA"=>39,"560D"=>46,"560B"=>53);
foreach ($CodeOrderArray as $InvoiceCode=>$BulletDetailRow){
	$BulletDetailRow = $BulletDetailRow+$bulletRowsAdded;
	$CategoryNameRow = $BulletDetailRow+1;
	$ThisBulletData = $BulletDataByCode[$InvoiceCode];
	$ThisBulletDataReverse = array_reverse($ThisBulletData);
	$SubTotalCells = array();	
//	echo $InvoiceCode." starting staffRow = ".$BulletDetailRow."<br>";
	//echo $InvoiceCode." has ".count($ThisBulletData)." bullets<br>";
	//print_pre($ThisBulletData);
	if (count($ThisBulletData)){
		$Adjustments["Review"]["Bullets - ".$InvoiceCode."<br>".$MFEPBulletsLink] = count($ThisBulletData)." counted";

		$bulletRowsAdded = $bulletRowsAdded+(count($ThisBulletData)-1);
		$rowsToAdd=(count($ThisBulletData)-1);
		if ($rowsToAdd > 0){
//			echo "will add ".$rowsToAdd." before ".$CategoryNameRow."<br>";
			$objPHPExcel->getActiveSheet()->insertNewRowBefore($CategoryNameRow,$rowsToAdd);
		}
		$thisRow = $BulletDetailRow;
		$ThisBulletData = array_orderby($ThisBulletData,"status",SORT_ASC,"deliverables",SORT_ASC);

		foreach ($ThisBulletData as $ThisBullet){
			//echo $Employee_Name;
			$cell = "A".$thisRow;
			$objPHPExcel->getActiveSheet()->setCellValue($cell,$ThisBullet["status"]);
			//echo $cell."<br>";
			$cell = "B".$thisRow;
			$objPHPExcel->getActiveSheet()->setCellValue($cell,$ThisBullet["name"]." ".str_replace("<br>","",$ThisBullet["deliverables"]));
			$objPHPExcel->getActiveSheet()->mergeCells($cell.":H".$thisRow);
			if (strlen($ThisBullet["deliverables"]) > 90){
				$objPHPExcel->getActiveSheet()->getRowDimension($thisRow)->setRowHeight((20*ceil(strlen($ThisBullet["deliverables"])/90)));
			}else{
				$objPHPExcel->getActiveSheet()->getRowDimension($thisRow)->setRowHeight(35);
			}

			$thisRow++;
			//calculation for Actual CoverPage
		
			$BulletDetailRow = ($thisRow+1);
			$CategoryNameRow = ($thisRow+2);
			$ThisLastRow = $thisRow;
		}// end foreach $ThisBulletData as $CategoryName
	}else{
		$Adjustments["Review"]["Bullets - ".$InvoiceCode."<br>".$MFEPBulletsLink] = "None Detected";
		$CategoriesCountWithBullets[]=1;
		//category is empty
	}

}//end foreach $CodeOrderArray
*/

//Now create 560 Farms tab
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A1", "All Farms Currently Engaged - " . date("F", strtotime($invoiceDate)));
} catch (PHPExcel_Exception $e) {
}

if (count($farmsSorted)){
		if (count($farmsSorted) > 2){
            try {
                $objPHPExcel->getActiveSheet()->insertNewRowBefore(4, (count($farmsSorted) - 2));
            } catch (PHPExcel_Exception $e) {
            }
        }

		$farmRow = 3;
		foreach ($farmsSorted as $farmInfo){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("A" . ($farmRow), $farmInfo["name"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("B" . ($farmRow), $farmInfo["city"]);
            } catch (PHPExcel_Exception $e) {
            }
            try {
                $objPHPExcel->getActiveSheet()->setCellValue("C" . ($farmRow), $farmInfo["status"]);
            } catch (PHPExcel_Exception $e) {
            }
            $farmRow++;
			$Adjustments["Review"]["Farms<br>".$farmInfo["reportLink"]][] = "<a href='".$instance_url."/".$farmInfo["AccountNameLink"]."' target='".$farmInfo["AccountNameLink"]."'>".$farmInfo["name"]."</a> ".$farmInfo["status"];
		}
	}
//end Farms Tab

try {
    $objPHPExcel->setActiveSheetIndex(4);
} catch (PHPExcel_Exception $e) {
}
$preInvoiceRow=1;
if (count($preInvoicedNew)){

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $preInvoiceRow, "Account Name");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $preInvoiceRow, "Type");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $preInvoiceRow, "Incentive Amount");
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $preInvoiceRow, "Invoice Date");
    } catch (PHPExcel_Exception $e) {
    }

    $preInvoiceRow++;
	foreach ($preInvoicedNew as $preInvoiced){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $preInvoiceRow, $preInvoiced->GBSPreInvoiced_AccountName);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $preInvoiceRow, $preInvoiced->GBSPreInvoiced_Type);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $preInvoiceRow, $preInvoiced->GBSPreInvoiced_IncentiveAmount);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $preInvoiceRow, $preInvoiced->GBSPreInvoiced_InvoiceDate);
        } catch (PHPExcel_Exception $e) {
        }
        $preInvoiceRow++;
	}
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    } catch (PHPExcel_Exception $e) {
    }
    //$objPHPExcel->getActiveSheet()->setTitle('56 '.date("F",strtotime($invoiceDate)).' CP');
    try {
        $objPHPExcel->getActiveSheet()->setTitle('PreInvoiced Completed in ' . date("M", strtotime($invoiceDate)));
    } catch (PHPExcel_Exception $e) {
    }
    $rawDataBulletSheet = 5;

}else{
    try {
        $objPHPExcel->removeSheetByIndex(4);
    } catch (PHPExcel_Exception $e) {
    }
    $rawDataBulletSheet = 4;
}	
ksort($BulletDataByFarm);
try {
    $objPHPExcel->setActiveSheetIndex($rawDataBulletSheet);
} catch (PHPExcel_Exception $e) {
}
$bulletRow = 2;
foreach ($BulletDataByFarm as $Farm=>$bulletInfo){
	foreach ($bulletInfo as $bulletData){
		$thisMetrics = $bulletData["metrics"];
		$metricsDisplayArray = array();
		$metricsImplode = "";
		if (count($thisMetrics)){
			foreach ($thisMetrics as $thisMetric){
				$metricsDisplay = "";
				foreach ($thisMetric as $key=>$val){
					if ($val){
						if (strpos($key,"\$")){
							$key = str_replace("\$","",$key);
							$val = "$".money($val);
						}
						$metricsDisplay .=str_replace("_"," ",$key).": ".$val.", ";
					}
				}
				$metricsDisplayArray[] = rtrim($metricsDisplay,", ");
			}
			$metricsImplode = implode("\r\n",$metricsDisplayArray);
		}
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $bulletRow, $bulletData["name"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $bulletRow, $farmsById[$bulletData["accountId"]]["city"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $bulletRow, $bulletData["code"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $bulletRow, $bulletData["status"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $bulletRow, $bulletData["deliverables"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $bulletRow, $metricsImplode);
        } catch (PHPExcel_Exception $e) {
        }
        $bulletRow++;
	}
}

$CETInternalFiscalRevenueSheetIndex = ($rawDataBulletSheet+1);
$CETInternalFiscalRevenueSheetName = "MFEP";	
//Create CET Internal Fiscal Revenue Sheet
include_once('CETInternalRevenueCodeSheet.php');

try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation."<Br>";
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$MFEPCoverPageLink = "<a id='MFEPCoverPageLogicFile' href='".$webpathExtension."'>".$MFEPCoverPage."</a><br>";
?>