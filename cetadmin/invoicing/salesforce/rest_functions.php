<?php

function checkSession($instance_url, $reportId, $access_token){
	if ($access_token && $instance_url && $reportId){
		$url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."/describe";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token"));
		if ($GLOBALS['testingServer']){
		//	curl_setopt($curl, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		}
		$json_response = curl_exec($curl);
		curl_close($curl);
		$description = json_decode($json_response, true);
		if ($description[0]["errorCode"] == "INVALID_SESSION_ID"){
			return false;
		}else{
			return true;
		}
	}else{
		return false;
	}
}

function retrieve_json($url,$access_token){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token"));
	if ($GLOBALS['testingServer']){
	//	curl_setopt($curl, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	}
    $json_response = curl_exec($curl);
    curl_close($curl);
	return $json_response;
}

function retrieve_zipcode($address,$googleApiKey){
	$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&key=".$googleApiKey;
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	if ($GLOBALS['testingServer']){
	//	curl_setopt($curl, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	}
    $json_response = curl_exec($curl);
    curl_close($curl);
	$results = json_decode($json_response);
	$addressComponents = $results->results[0]->address_components;
	foreach ($addressComponents as $addressArray=>$addressComponent){
		if ($addressComponent->types[0] == "postal_code"){
			$zipCode = $addressComponent->long_name;
		}
	}
	return $zipCode;
}
function retrieve_attachment($instance_url, $attachmentInfo, $access_token){
	$accountName = str_replace(" ","",$attachmentInfo["AccountName"]);
	$attachmentId = $attachmentInfo["Id"];
	$attachmentName = str_replace(".xls","_".$attachmentId.".xls",$attachmentInfo["Name"]);
	$attachmentName = (strtolower(substr($attachmentName,0,strlen($accountName))) != strtolower($accountName) ? $accountName."_".$attachmentName : $attachmentName);
    $url = $instance_url."/services/data/v35.0/sobjects/Attachment/".$attachmentId."/Body";
	$attachment = retrieve_json($url,$access_token);
	$folder = ($attachmentInfo["Folder"] ? $attachmentInfo["Folder"]."/" : "attachments/");
	$report = $attachmentInfo["Report"];
//	print_r($attachment);
	file_put_contents("salesforce/".$folder.$attachmentName, $attachment);
	if (!$report){ //default for eversource process
		file_put_contents("salesforce/exports/".$folder.$attachmentName, $attachment);
		if (strpos($attachmentName,"pdf")){
			file_put_contents("salesforce/exports/".$attachmentName, $attachment);
		}
	}
	return $attachmentName;

}

require_once('rest_functions_account.php');
require_once('rest_functions_report.php');

?>