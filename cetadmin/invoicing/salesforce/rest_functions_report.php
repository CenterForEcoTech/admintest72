<?php
function retrieve_report_883($instance_url, $reportId, $access_token){
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."/describe";
	$description = json_decode(retrieve_json($url,$access_token),true);
	$fieldNamesArray = $description["reportExtendedMetadata"]["detailColumnInfo"];
	//print_r($description);
	
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."?includeDetails=true";
    $details = json_decode(retrieve_json($url,$access_token),true);
	//print_r($details);
	
	$detailColumns = $details["reportMetadata"]["detailColumns"];
	$TLevels = count($details["factMap"]);
	$ThisLevel = 0;
	while ($ThisLevel <= ($TLevels-2)){
		$TLevelRows[$ThisLevel."!T"] = $details["factMap"][$ThisLevel."!T"]["rows"];
		$ThisLevel++;
	}
	$TLevelRows["T!T"] = $details["factMap"]["rows"];
	foreach ($TLevelRows as $TLevel=>$TRows){
		if (count($TRows)){
			foreach ($TRows as $rowId=>$dataCells){
				$rowData = array();
				foreach ($detailColumns as $columnId=>$fieldName){
					//echo $fieldNamesArray[$fieldName]["label"].": ".$dataCells["dataCells"][$columnId]["label"]."<br>";
					$rowData[$fieldName] = $dataCells["dataCells"][$columnId]["label"];
					if ($fieldName=="Upgrade__c.Name"){
						$accountId = $dataCells["dataCells"][$columnId]["value"];
					}
				}
				$rowData["accountId"] = $accountId;
				$rowCollection[$TLevel][] = $rowData;
				$rowCollection["All"][] = $rowData;
			}
		}
	}
	$results["fieldNamesArray"] = $fieldNamesArray;
	$results["rows"] = $rowCollection["All"];
	return $results;
}
function retrieve_report_TLevel($instance_url, $reportId, $access_token){
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."/describe";
	$description = json_decode(retrieve_json($url,$access_token),true);
	$fieldNamesArray = $description["reportExtendedMetadata"]["detailColumnInfo"];
	//print_pre($description);
	
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."?includeDetails=true";
    $details = json_decode(retrieve_json($url,$access_token),true);
	//print_pre($details);

	
	$detailColumns = $details["reportMetadata"]["detailColumns"];
	$TLevelRows["T!T"] = $details["factMap"]["T!T"]["rows"];
	foreach ($TLevelRows as $TLevel=>$TRows){
		//echo $TLevel;
		//print_pre($TRows);
		if (count($TRows)){
			foreach ($TRows as $rowId=>$dataCells){
				$rowData = array();
				foreach ($detailColumns as $columnId=>$fieldName){
					//echo $fieldNamesArray[$fieldName]["label"].": ".$dataCells["dataCells"][$columnId]["label"]."<br>";
					$rowData[$fieldName] = $dataCells["dataCells"][$columnId]["label"];
					if ($fieldName=="Upgrade__c.Name"){
						$rowData["GPNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
					if (strtolower($fieldName)=="account.name"){
						$rowData["AccountNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
				}
				$rowCollection["All"][] = $rowData;
			}
		}
	}
	$results["fieldNamesArray"] = $fieldNamesArray;
	$results["rows"] = $rowCollection["All"];
	return $results;

}
function retrieve_report_asCSV($instance_url, $reportId, $access_token){
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."/describe";
	$description = json_decode(retrieve_json($url,$access_token),true);
	$fieldNamesArray = $description["reportExtendedMetadata"]["detailColumnInfo"];
	//print_pre($description);
	
    $url = $instance_url."/services/data/v35.0/analytics/reports/".$reportId."?export=1&enc=UTF-8&xf=CSV";
    $details = json_decode(retrieve_json($url,$access_token),true);
//	print_pre($details);

	
	$detailColumns = $details["reportMetadata"]["detailColumns"];
	$TLevelRows["T!T"] = $details["factMap"]["T!T"]["rows"];
	foreach ($TLevelRows as $TLevel=>$TRows){
		//echo $TLevel;
		//print_pre($TRows);
		if (count($TRows)){
			foreach ($TRows as $rowId=>$dataCells){
				$rowData = array();
				foreach ($detailColumns as $columnId=>$fieldName){
					//echo $fieldNamesArray[$fieldName]["label"].": ".$dataCells["dataCells"][$columnId]["label"]."<br>";
					$rowData[$fieldName] = $dataCells["dataCells"][$columnId]["label"];
					if ($fieldName=="Upgrade__c.Name"){
						$rowData["GPNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
					if (strtolower($fieldName)=="account.name"){
						$rowData["AccountNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
					if ($fieldName=="OPPORTUNITY_NAME"){
						$rowData["OpportunityNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
					if ($fieldName=="PRODUCT_NAME"){
						$rowData["ItemNameLinkValue"] = $dataCells["dataCells"][$columnId]["value"];
					}
				}
				$rowCollection["All"][] = $rowData;
			}
		}
	}
	$results["fieldNamesArray"] = $fieldNamesArray;
	$results["rows"] = $rowCollection["All"];
	return $results;

}
?>