<?php
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';

$inputFileName = $trackingFile;
//echo "reading ".$inputFileName."<br>";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get MDAR worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(3);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$headerRow = $sheet->rangeToArray('A2:'.$highestColumn.'2', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	$val = $val? :"Employee_Name";
	$headerNumbers[($col+1)]=$val;
}
//find the summary row
for ($row = 3; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $SummaryRowData = $sheet->rangeToArray('E' . $row . ':E'. $row, NULL, TRUE, FALSE);
	if (trim($SummaryRowData[0][0]) == "Total Combined Areas for Liz Double Check"){
		$SummaryRow = $row;
		break;
	}
}

//  Loop through each row of the worksheet in turn
for ($row = 3; $row < ($SummaryRow-1); $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	foreach($rowData[0] as $k=>$v){
		//echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$rowsRaw[($row-3)][$headerNumbers[($k+1)]] = $v;
	}
}
//cleanup rows
foreach ($rowsRaw as $rowId=>$rowDetails){
	$CoverPageTemplateRows[] = $rowDetails;
}
?>