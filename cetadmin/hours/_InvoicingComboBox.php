<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxInvoicing" ).combobox({
				select: function(event, ui){
					<?php if (!$invoicingComboBoxFunction){?>
					window.location = "?nav=manage-invoicing&InvoicingID="+$(ui.item).val();
					<?php }else{
						echo $invoicingComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($invoicingComboBoxDivColumns ? $invoicingComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$groupsComboBoxHideLabel){?>
				<label>Start typing invoicing name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxInvoicing parameters" name="invoicingId" id="SelectedInvoicingID">
				<option value="">Select one...</option>
				<?php 
					foreach ($invoicingListByName as $name=>$invoicingInfo){
						echo "<option value=\"".$invoicingInfo->id."\"".($SelectedInvoicingID==$invoicingInfo->id ? " selected" : "").">".$invoicingInfo->displayName."</option>";
					}
				?>
			  </select>
			</div>
		</div>