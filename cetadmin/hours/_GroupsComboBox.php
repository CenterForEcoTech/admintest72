<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxGroups" ).combobox({
				select: function(event, ui){
					<?php if (!$groupsComboBoxFunction){?>
					window.location = "?nav=manage-groups&GroupID="+$(ui.item).val();
					<?php }else{
						echo $groupsComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($groupsComboBoxDivColumns ? $groupsComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$groupsComboBoxHideLabel){?>
				<label>Start typing group name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxGroups parameters" name="groupId" id="SelectedGroup">
				<option value="">Select one...</option>
				<?php 
					foreach ($GroupsByName as $name=>$groupInfo){
						echo "<option value=\"".$groupInfo->id."\"".($SelectedGroupsID==$groupInfo->id ? " selected" : "").">".$groupInfo->name." ".($groupInfo->department == "MO" ? "(M&O)" : "")."</option>";
					}
					echo "<option value=\"nonbillable\"".($SelectedGroupsID=="nonbillable" ? " selected" : "").">NonBillable</option>";
				?>
			  </select>
			</div>
		</div>