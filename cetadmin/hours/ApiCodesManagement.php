<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HoursProvider.php");
$hoursProvider = new HoursProvider($dataConn);
include_once($dbProviderFolder."InvoicingProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == "insert_hours"){
			
			$response = $hoursProvider->validateHours($newRecord);
			if ($response->Errors){
				$results = $response;
			}else{
				$insertResponse = $hoursProvider->insertHours($response, getAdminId());
				$results = $insertResponse;
			}
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "invoicing_note"){
			$InvoicingProvider = new InvoicingProvider($dataConn);
			$insertResponse = $InvoicingProvider->updateInvoicingNotes($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "insert_note"){
			
			$insertResponse = $hoursProvider->insertNotes($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_hours"){
			
			$insertResponse = $hoursProvider->submitHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_approve"){
			
			$insertResponse = $hoursProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hours"){
			
			$insertResponse = $hoursProvider->deleteHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hourid"){
			
			$insertResponse = $hoursProvider->deleteHourId($newRecord, getAdminId());
			if ($insertResponse->AddedByAdmin){
				$insertResponse->error = "Only ".$insertResponse->AddedByAdmin.", their supervisor, or Liz Budd can remove this entry.";
			}
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
        } else {
			header("HTTP/1.0 201 Created");
		
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'code_update'){
			$response = $hoursProvider->updateCodes($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'code_add'){
			
			$response = $hoursProvider->addCodes($newRecord, getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif (trim($newRecord->action) == 'group_update'){
			
			$response = $hoursProvider->updateGroups($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'group_add'){
			
			$response = $hoursProvider->addGroups($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif (trim($newRecord->action) == 'farm_update'){
			
			$response = $hoursProvider->updateFarms($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'farm_add'){
			
			$response = $hoursProvider->addFarms($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == "delete_hours"){
			
			$insertResponse = $hoursProvider->deleteHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "delete_hourid"){
			
			$insertResponse = $hoursProvider->deleteHourId($newRecord, getAdminId());
			if ($insertResponse->AddedByAdmin){
				$insertResponse->error = "Only ".$insertResponse->AddedByAdmin." can remove this entry.";
			}
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		/* MOVED TO POST
		}else if (trim($newRecord->action) == "submit_hours"){
			
			$insertResponse = $hoursProvider->submitHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		*/
		}else if (trim($newRecord->action) == "submit_remove"){
			
			$insertResponse = $hoursProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "submit_approve"){
			
			$insertResponse = $hoursProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "approve_remove"){
			
			$insertResponse = $hoursProvider->submitRemove($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_approve"){
			
			$insertResponse = $hoursProvider->approveHours($newRecord, getAdminId());
			$results = $insertResponse;
			header("HTTP/1.0 201 Created");
		}else if (trim($newRecord->action) == "bulk_reject"){
			//if wanted to send notification from dreamhost server
			/*
			$path = getcwd();
			$pathSeparator = "/";
			if (!strpos($path,"xampp")){
				$thisPath = ".:/home/cetdash/pear/share/pear";
				set_include_path($thisPath);
			}else{
				$pathSeparator = "\\";
			}
			*/
			require_once "Mail.php";  
			$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
			$Emails = $newRecord->employeeEmails;
			$rejectionMessage = $newRecord->rejectionMessage;
			$timePeriod = $newRecord->timeperiod;
			foreach ($Emails as $Email){
				$insertResponse = $timePeriod;
					$to = $Email; 
					$recipients = $to.",".$from;
					$subject = "Submitted Hours Rejected"; 
					$body = "Your submitted Hours for ".$timePeriod." were rejected.\n\r  ".$rejectionMessage;  
					$host = "smtp.office365.com"; 
					$port = "587"; 
					$username = $_SESSION['AdminEmail']; 
					$password = $_SESSION['AdminAuthenticationCode'];  
					$headers = array (
						'From' => $from,   
						'To' => $to, 
						'Subject' => $subject
					); 
					$smtp = Mail::factory(
						'smtp',   
						array (
							'host' => $host,     
							'port' => $port,     
							'auth' => true,     
							'username' => $username,     
							'password' => $password
						)
					);  
					$mail = $smtp->send($recipients, $headers, $body);  
			}
			
			$insertResponse = $hoursProvider->submitRemove($newRecord, getAdminId());
	
			$results = print_r($mail,true);
			header("HTTP/1.0 201 Created");
		}else{
            
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hoursProvider->updateCodesDisplayOrder($id,($displayID+1));
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hoursProvider->updateCodesDisplayOrder($id,0);
					}
				}
				if ($action == 'Insert_CodesMatrix'){
					$record->codesId = $item->codesId;
					$record->department = $item->department;
					foreach ($items as $displayID=>$id){
						$record->employeeId = (int)$id;
						$responseCollection[] = $hoursProvider->addCodesMatrix($record,getAdminId());
						$results = $responseCollection;
						if (count($responseCollection)){
							$responseCollection[0] = "Added";
						}
					}
				}
				if ($action == 'Remove_CodesMatrix'){
					foreach ($items as $displayID=>$id){
						$matrixId = (int)$id;
						$responseCollection[] = $hoursProvider->removeCodesMatrix($matrixId);
						if (count($responseCollection)){
							$responseCollection[0] = "Removed";
						}
					}
				}
				
				
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = $action."unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>