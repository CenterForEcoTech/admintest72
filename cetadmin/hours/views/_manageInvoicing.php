<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InvoicingProvider.php");

$invoicingProvider = new InvoicingProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$results = $invoicingProvider->getInvoicingCodes($criteria = null);
$resultArray = $results->collection;
foreach ($resultArray as $result=>$record){
	$invoicingListById[$record->id] = $record;
	$invoicingListByName[$record->displayName] = $record;
}
ksort($invoiceListByName);

//print_pre($invoicingListByName);
//resort the products without display order by InvoicingID
$SelectedInvoicingID = $_GET['InvoicingID'];
$SelectedInvoiceName = $_GET['InvoiceName'];
if ($SelectedInvoiceName && !$SelectedInvoicingID){
	$SelectedInvoicingID = $invoicingListByName[$SelectedInvoiceName]->id;
}
if ($SelectedInvoicingID && $invoicingListById[$SelectedInvoicingID]->invoicingNumberName){
	//Get Invoicing History
	$criteria = new stdClass();
	if ($invoicingListById[$SelectedInvoicingID]->invoicingNumberName == "Rathmann"){
		$criteria->invoiceCodeStartsWith = $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;
	}else{
		$criteria->invoiceCode = $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;
	}
	$criteria->invoiceDateStart = $invoicingListById[$SelectedInvoicingID]->fiscalYearStart;
	if (MySQLDate($invoicingListById[$SelectedInvoicingID]->fiscalYearStart)){
		$criteria->invoiceDateEnd = $invoicingListById[$SelectedInvoicingID]->fiscalYearEnd;
	}else{
		$criteria->invoiceDateEnd = date("Y-m-d",strtotime($invoicingListById[$SelectedInvoicingID]->fiscalYearStart." + 364 days"));
	}
	$results = $invoicingProvider->getInvoicingHistory($criteria);
	foreach ($results as $record){
		if ($invoicingHistoryByDate[strtotime($record["invoiceDate"])]){
			$timestamp = strtotime($record["timestamp"]);
			if ($timestamp > strtotime($invoicingHistoryByDate[strtotime($record["invoiceDate"])]["timestamp"])){
				$invoicingHistoryByDate[strtotime($record["invoiceDate"])]=$record; 
			}
		}else{
			$invoicingHistoryByDate[strtotime($record["invoiceDate"])]=$record; 
		}
			
	}
	
	$criteria = new stdClass();
	$criteria->invoiceCode = $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;
	$criteria->startDate = $invoicingListById[$SelectedInvoicingID]->fiscalYearStart;
	if (MySQLDate($invoicingListById[$SelectedInvoicingID]->fiscalYearStart)){
		$criteria->endDate = $invoicingListById[$SelectedInvoicingID]->fiscalYearEnd;
	}else{
		$criteria->endDate = date("Y-m-d",strtotime($invoicingListById[$SelectedInvoicingID]->fiscalYearStart." + 364 days"));
	}
	$invoicingHistoryCodeAdjustments = $invoicingProvider->getInvoiceHistoryCodeAdjust($criteria);
	//print_pre($invoicingHistoryCodeAdjustments);
	
}else{
	$results = $invoicingProvider->getInvoicingHistory($criteria = null);
	$invoiceCodeNames = array();
	foreach ($results as $record){
		if (!in_array($record["invoiceCode"],$invoiceCodeNames)){
			$invoiceCodeNames[] = $record["invoiceCode"];
		}
	}
	sort($invoiceCodeNames);
	$invoiceHistoryNotFound = true;
}
//print_pre($invoicingHistoryByDate);
$addInvoice = $_GET['addInvoice'];

if ($SelectedInvoicingID){
	//Get Codes	
	include_once($dbProviderFolder."HoursProvider.php");
	$hoursProvider = new HoursProvider($dataConn);
	//Get groups
	$criteria->showAll = true;
	$criteria->noLimit = true;
	$criteria->invoiceId = $SelectedInvoicingID;
	$paginationResult = $hoursProvider->getGroups($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$GroupByID[$record->id] = $record;
		$GroupsByName[$record->name]=$record;
	}
	ksort($GroupsByName);
	//print_pre($GroupByID);

	foreach ($GroupByID as $groupId=>$groupInfo){
		$criteria->showAll = true;
		$criteria->noLimit = true;
		$criteria->groupId = $groupId;
		$paginationResult = $hoursProvider->getCodes($criteria);
		$resultArray = $paginationResult->collection;
		foreach ($resultArray as $result=>$record){
			if ($record->displayOrderId){
				$CodeByID[$record->id] = $record;
				$CodesInThisInvoice[] = $record->name;
				$CodesBudget[$record->name] = $record->budget;
			}
		}
	}
	sort($CodesInThisInvoice);
}
?>
<?php if (!$addInvoice){?>
	<br>
	<div class="fifteen columns">
		<?php include('_InvoicingComboBox.php');?>
	</div>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addInvoice?>
<?php if ($SelectedInvoicingID || $addInvoice){?>
	<style>
		.NonBillable1 {font-size:10pt;}
	</style>
	<form id="InvoiceUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Invoice Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<?php if (!$addInvoice){?><input type="text" name="id" value="<?php echo $invoicingListById[$SelectedInvoicingID]->id;?>"><?php }?>
				</div>
				<div class="four columns">Display Name:</div><input class="four columns" type="text" id="displayName" name="displayName" value="<?php echo $invoicingListById[$SelectedInvoicingID]->displayName;?>">
				<br>
				<div class="four columns">Codes:</div><div class="eight columns">
					<?php 
						if (count($CodesInThisInvoice)){
							echo implode(",",$CodesInThisInvoice);
							//update CodesInThisInvoice
							$response = $invoicingProvider->updateInvoicingCodes_justCodes($CodesInThisInvoice,$SelectedInvoicingID);
						}else{
							
							echo "<span class='alert' style='font-size:8pt;font-weight:bold;'>Once you link a Group to this Invoice Process return and save this invoice details</span>";
						}
					?>
				</div><input type="hidden" id="codes" name="codes" value="<?php echo implode(",",$CodesInThisInvoice);?>">
				<br>
				<div class="four columns">Invoice Code Name:</div>
					<!--
					<select multiple="multiple" id='invoiceNumberName' size="8" class="three columns" style="height:400px;">
						<?php foreach ($invoiceCodeNames as $invoiceCodeName=>$count){
							echo "<option value='".$invoiceCodeName."'>".$invoiceCodeName."</option>";
						}?>
					</select>
					-->
					<input class="two columns" title="Use one of these: <?php echo implode(",",$invoiceCodeNames);?>" maxlength="10" type="text" id="invoicingNumberName" name="invoicingNumberName" value="<?php echo $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;?>"><span style="font-size:8pt;"> (only 10 characters)</span>
				<br clear="all">
				<div class="four columns">Fiscal Year Start:</div><input class="one column date" type="text" id="fiscalYearStart" name="fiscalYearStart" value="<?php echo MySQLDate($invoicingListById[$SelectedInvoicingID]->fiscalYearStart);?>">
				<div class="four columns">Fiscal Year End:</div><input class="one column date" type="text" id="fiscalYearEnd" name="fiscalYearEnd" value="<?php echo MySQLDate($invoicingListById[$SelectedInvoicingID]->fiscalYearEnd);?>">
				<br clear="all">
				<div class="four columns">Entire Budget:</div><input class="three columns" type="text" id="budget" name="budget" value="$<?php echo money($invoicingListById[$SelectedInvoicingID]->budget);?>">
				<br clear="all">
				<div class="four columns">NonCode Related Amount:<br><span style='font-size:8pt;'>(passthroughs and billing regardless of hours)</span></div><input class="three columns" type="text" id="budgetNonCodeRelated" name="budgetNonCodeRelated" value="$<?php echo money($invoicingListById[$SelectedInvoicingID]->budgetNonCodeRelated);?>">
				<br clear="all">
				<div class="four columns">Remaining Budgets For Staff</div>
				<div class="two columns">$<?php echo money((bcsub($invoicingListById[$SelectedInvoicingID]->budget,$invoicingListById[$SelectedInvoicingID]->budgetNonCodeRelated,2)));?><Br><span style="font-size:8pt;">(as calculated)</span></div>
				<div class="four columns">$<?php echo money(array_sum($CodesBudget));?><Br><span style="font-size:8pt;">(as captured on each Code budget line)</span></div>
				<br clear="all">
				<div class="fourteen columns">
					<?php 
						if (array_sum($CodesBudget) > 1.0){
							echo "Breakdown of Budgets By Code:<br clear='all'>";
							foreach($CodesBudget as $codeName=>$budget){
								echo "<div class='four columns'><a href='".$CurrentServer.$adminFolder."hours/?nav=manage-codes&CodeName=".$codeName."'>".$codeName."</a>: $".money($budget)."</div>";
							}
						}else{
							"No Budgets set for individual codes";
						}
					?>
				</div>
				<br>
				<br clear="all">
				<div class="three columns">Notes:</div>
				<div class="ten columns">
					<?php echo $invoicingListById[$SelectedInvoicingID]->notes;?>
				</div>
			</div>
		</fieldset>
			<div id="invoicingResults" class="fifteen columns"></div>
			<br clear="all">
		
		
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-invoicing&InvoicingID=<?php echo $SelectedInvoicingID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addInvoice){?>
					<a href="save-invoice-add" id="save-invoice-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="invoice_add">
				<?php }else{?>
					<a href="save-invoice-update" id="save-invoice-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="invoice_update">
				<?php }?>

			</div>
	</form>
			<br clear="all">
			<br clear="all">
			<div class="nineteen columns" style="width:100%;">
				<?php if (!$invoiceHistoryNotFound){?>
				<fieldset class="tweleve columns"><legend>Invoicing History Manual Entry</legend>
						<div class='row'>
							<div class="two columns">
								Date:<br>
								<input type="text" id="invoiceDate_add" name="invoiceDate" class="date">
							</div>
							<div class="three columns">
								Invoice Number:<Br>
								<input type="text" id="invoiceNumber_add" class="two columns">
							</div>
							<div class="two columns">
								Amount:<Br>
								<input type="text" id="invoiceAmount_add" class="two columns">
							</div>
							<div class="three columns">
								NonStaff Amount:<Br>
								<input type="text" id="nonStaffAmount_add" class="two columns">
							</div>
							<div class="one columns" style="padding-top:10px;">
								<a href="#" class="button-link" id="addInvoiceHistory">Add</a>
							</div>
						</div>
				</fieldset>
				<fieldset class="tweleve columns"><legend>Invoicing History Code Specific Adjustments</legend>
						<div class='row'>
							<div class="two columns">
								Date:<br>
								<input type="text" id="invoiceDate_adjust" name="invoiceDate" class="date">
							</div>
							<div class="three columns">
								Invoice Number:<Br>
								<input type="text" id="invoiceNumber_adjust" class="two columns">
							</div>
							<div class="two columns">
								Code:<Br>
								<select id="invoiceCodeName_adjust" class="two columns">
									<option value=""></option>
									<?php
										foreach ($CodesInThisInvoice as $codeName){
											echo "<option value='".$codeName."'>".$codeName."</option>";
										}
									?>
								</select>
							</div>
							<div class="two columns">
								Amount:<Br>
								<input type="text" id="invoiceAmount_adjust" class="two columns">
							</div>
							<div class="three columns">
								Note:<Br>
								<input type="text" id="invoiceNote_adjust" class="two columns">
							</div>
							<div class="one columns" style="padding-top:10px;">
								<a href="#" class="button-link" id="adjustInvoiceHistory">Add</a>
							</div>
						</div>
				</fieldset>
				<fieldset class="eight columns"><legend>Invoicing History</legend>
				
					<table class="simpleTable">
						<thead>
						<tr>
							<th>Date</th>
							<th>Invoice Number</th>
							<th>Amount</th>
							<th>nonStaffAmount</th>
						</tr>
						</thead>
						<style>
							.nonStaffAmountInput {display:none;width:100px;height:15;font-size:8pt;}
							.nonStaffAmountSpan {display:block;cursor:pointer;}
						
						</style>
						<tbody>
					<?php
						krsort($invoicingHistoryByDate);
						foreach ($invoicingHistoryByDate as $invoiceDate=>$invoiceData){
							echo "<tr>
								<td><span style='display:none;'>".$invoiceDate."</span>".date("m/d/y",$invoiceDate)."</td>
								<td>".$invoiceData["invoiceNumber"]."</td>
								<td>$".money($invoiceData["invoiceAmount"]).($invoiceData["sourcePage"] == "ManualEntry" ? "<span style='color:red' title='Manually Entered'>*</span>"  : "")."</td>
								<td><span id='nonStaffAmountSpan".$invoiceData["id"]."' class='nonStaffAmountSpan' data-id='".$invoiceData["id"]."'>$".money($invoiceData["nonStaffAmount"])."</span><input id='nonStaffAmountInput".$invoiceData["id"]."' class='nonStaffAmountInput' data-id='".$invoiceData["id"]."' value='".$invoiceData["nonStaffAmount"]."'></td>
								</tr>";
						}
					?>
						</tbody>
					</table>
				</fieldset>
				<fieldset class="seven columns"><legend>Invoicing Code Adjustments</legend>
				
					<table class="simpleTable">
						<thead>
						<tr>
							<th>Invoice</th>
							<th>Code</th>
							<th>Amount</th>
							<th>Note</th>
						</tr>
						</thead>
						<style>
							.nonStaffAmountInput {display:none;width:100px;height:15;font-size:8pt;}
							.nonStaffAmountSpan {display:block;cursor:pointer;}
						
						</style>
						<tbody>
					<?php
						foreach ($invoicingHistoryCodeAdjustments as $record){
							echo "<tr>
								<td><span style='display:none;'>".strtotime($record["invoiceDate"])."</span>".$record["invoiceNumber"]."</td>
								<td>".$record["invoiceCodeName"]."</td>
								<td>$".money($record["invoiceAmount"])."</td>
								<td>".$record["invoiceNote"]."<img src='".$CurrentServer."images/redx-small.png' style='float:right;cursor:pointer;' class='deleteCodeAdjustment' data-id='".$record["id"]."'></td>
								</tr>";
						}
					?>
						</tbody>
					</table>
				</fieldset>				
				<?php }else{ echo "You must set the Invoice Code Name To Enter In Invoicing History Amounts";}?>
			</div>
			
		<?php }//if not read only ?>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-invoicing&addInvoice=true" class="button-link">+ Add New Invoice</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 5,
			"paging":   false,
			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
	
	
		var dateClass = $(".date");
		dateClass.width(80);
		
		$("#fiscalYearStart").datepicker({
			maxDate: '0',
			onSelect: function (date) {
				var date2 = $('#fiscalYearStart').datepicker('getDate');
				date2.setDate(date2.getDate() + 364);
				$('#fiscalYearEnd').datepicker('setDate', date2);
			}
		});
		
		
		$("#fiscalYearEnd").datepicker();
		$("#invoiceDate_add").datepicker({ maxDate: '0' });
		$("#invoiceDate_adjust").datepicker({maxDate: '0' });
	

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
	 var formElement = $("#InvoiceUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-invoice-add").click(function(e){

			$('#invoicingResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						console.log(data);
						$('#invoicingResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						var displayOrderValue = parseInt($('#displayOrderId').val());
						$("#name").val('');	
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#invoicingResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedInvoicingID){?>
		$("#save-invoice-update").click(function(e){

			$('#invoicingResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						console.log(data);
						$('#invoicingResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#invoicingResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
		$("#addInvoiceHistory").click(function(e){
			e.preventDefault();
			var data = {};
				data["action"] = "invoiceHistoryManualEntry";
				data["invoiceDate"] = $("#invoiceDate_add").val();
				data["invoiceNumber"] = $("#invoiceNumber_add").val();
				data["invoiceAmount"] = $("#invoiceAmount_add").val();
				data["nonStaffAmount"] = $("#nonStaffAmount_add").val();
				data["invoiceCode"] = "<?php echo $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;?>";
				data["sourcePage"] = "ManualEntry";
				
			console.log(JSON.stringify(data));
			$.ajax({
				url: "ApiInvoicingManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data);
					location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#invoicingResults').show().html(message).addClass("alert");
				}
			});
		});
		$("#adjustInvoiceHistory").click(function(e){
			e.preventDefault();
			var data = {};
				data["action"] = "invoiceHistoryCodeAdjust";
				data["invoiceDate"] = $("#invoiceDate_adjust").val();
				data["invoiceNumber"] = $("#invoiceNumber_adjust").val();
				data["invoiceAmount"] = $("#invoiceAmount_adjust").val();
				data["invoiceCodeName"] = $("#invoiceCodeName_adjust").val();
				data["invoiceNote"] = $("#invoiceNote_adjust").val();
				data["invoiceCode"] = "<?php echo $invoicingListById[$SelectedInvoicingID]->invoicingNumberName;?>";
				
			console.log(JSON.stringify(data));
			$.ajax({
				url: "ApiInvoicingManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data);
					//location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#invoicingResults').show().html(message).addClass("alert");
				}
			});
		});
		
		$(document).on('click','.deleteCodeAdjustment',function(){
			var deleteThis = confirm('Delete This Entry?');
			if (deleteThis){
				var $this = $(this),
					thisId = $this.attr('data-id'),
					data = {};
				data["action"] = "deleteCodeAdjustment";
				data["id"] = thisId;
					
				console.log(JSON.stringify(data));
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						console.log(data);
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#invoicingResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
		$(document).on('click','.nonStaffAmountSpan',function(){
			var $this = $(this),
				thisId = $this.attr('data-id'),
				thisInputId = "#nonStaffAmountInput"+thisId;
				$this.hide();
				$(thisInputId).show().focus();
		});
		$(document).on('blur','.nonStaffAmountInput',function(e){
			var $this = $(this),
			thisId = $this.attr('data-id'),
			thisSpanId = "#nonStaffAmountSpan"+thisId,
			thisValue = Number($this.val().replace(/[^0-9\.-]+/g,""));
			
			$this.hide();
			$(thisSpanId).html("$"+thisValue.toLocaleString());
			$(thisSpanId).show();
			e.preventDefault();
			var data = {};
				data["action"] = "invoiceHistoryNonStaffAmountUpdate";
				data["invoiceId"] = thisId;
				data["nonStaffAmount"] = thisValue;
				
			console.log(JSON.stringify(data));
			$.ajax({
				url: "ApiInvoicingManagement.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data);
//					location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#invoicingResults').show().html(message).addClass("alert");
				}
			});
		});
		
	<?php }//end if SelectedInvoicingID ?>
});
</script>