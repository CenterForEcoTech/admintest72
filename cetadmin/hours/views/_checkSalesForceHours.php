<?php
$EmployeeID = ($_GET["EmployeeID"] ? : 39);
$StartDate = ($_GET["StartDate"] ? : '07/01/2016');
$EndDate = ($_GET["EndDate"] ? : '07/31/2016');
include_once($siteRoot."_setupDataConnection.php");
//include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->noLimit = true;
$criteria->isActive = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
//get codes matrix for this employee
$gbsProvider = new HoursProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->employeeID = $EmployeeID;
$paginationResult = $gbsProvider->getEmployeeCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	if ($record->displayOrderId){
		$CodeNames[$record->name] = $record->id;
	}
}

$validCodes = array("523","810B","811B","525J","524B","524T","560");
$matchedCodes = array_intersect($validCodes, array_keys($CodeNames));
if (count($matchedCodes) && $EmployeeID){
	$searchPayWeekStartDay = date("Y-m-d",strtotime($StartDate));
	$searchPayWeekEndDay = date("Y-m-d",strtotime($EndDate));
	//check Salesforce hours
	require_once $rootFolder.$adminFolder.'gbs/salesforce/config.php';
	include_once($rootFolder.$adminFolder.'gbs/salesforce/soap_connect.php');
	$instance_url = 'https://cet.my.salesforce.com';
//	jake 005U0000002SZ1kIAG
//	yosh 005U0000003dnrlIAA
/*
	$query = "UPDATE Time_Card__c SET CreatedById='005U0000002SZ1kIAG' WHERE CreatedById='005U0000003dnrlIAA'";
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	$queries[] = $query;
	$responses[] = $response;
*/
	
	$EmployeeEmail = ($EmployeeByID[$EmployeeID]->email == "yosh.schulman@cetonline.org" ? $Config_SalesforceUserName : $EmployeeByID[$EmployeeID]->email);
	
	
	//get Contact UserId
	$query = "SELECT Id, Username, LastName, FirstName, Name FROM User WHERE Username='".$EmployeeEmail."'";
	//echo $query;
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	$queries[] = $query;
	$responses[] = $response;
	foreach ($response->records as $record){
		$salesForceUserID = $record->Id;
	}
	
	//Now Get Timecards
	$query ="SELECT Id, Green_Prospect__c, Hours_Worked__c, Date_Worked__c, Description__c, Work_Category__c FROM Time_Card__c WHERE IsDeleted = false AND Date_Worked__c >=".$searchPayWeekStartDay." AND Date_Worked__c<=".$searchPayWeekEndDay." AND CreatedById='".$salesForceUserID."' ORDER BY Date_Worked__c DESC";
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	$queries[] = $query;
	$responses[] = $response;
	$GreenProspects = array();
	foreach ($response->records as $record){
		if (!in_array($record->Green_Prospect__c,$GreenProspects)){$GreenProspects[] = $record->Green_Prospect__c;}
		$workCategory = $record->Work_Category__c;
		$SalesForceHoursByWorkCategory[$record->Green_Prospect__c]["Dates"][strtotime($record->Date_Worked__c)][$workCategory] = bcadd($SalesForceHoursByWorkCategory[$record->Green_Prospect__c]["Dates"][strtotime($record->Date_Worked__c)][$workCategory],$record->Hours_Worked__c,2);
		$SalesForceHours[$record->Green_Prospect__c]["Dates"][strtotime($record->Date_Worked__c)] = bcadd($SalesForceHours[$record->Green_Prospect__c]["Dates"][strtotime($record->Date_Worked__c)],$record->Hours_Worked__c,2);
	}
	
	$SalesForceCampaignCodesToCheck['701U00000002luiIAA']='810B Berkshire Gas C&I';
	$SalesForceCampaignCodesToCheck['701U0000000rNs0IAE']='811B Berkshire Gas MF';
	$SalesForceCampaignCodesToCheck['701U000000027V9IAI']='523 Berkshire Gas';
	$SalesForceCampaignCodesToCheck['701U000000027VBIAY']='525 Columbia Gas';
	$SalesForceCampaignCodesToCheck['701U000000027VAIAY']='524 NGRID Inspection';
	$SalesForceCampaignCodesToCheck['701U00000002cUOIAY']='560 MFEP';
	
	//Now Get Green Prospect Info
	$query = "SELECT Id, Name, Primary_Campaign__c, Account__c, Account__r.Name FROM Upgrade__c WHERE Id IN ('".implode("','",$GreenProspects)."')";
	$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
	$queries[] = $query;
	$responses[] = $response;
	foreach ($response->records as $record){
//						print_pre($record);
		if ($SalesForceCampaignCodesToCheck[$record->Primary_Campaign__c]){
			$AccountNameLink = $record->Account__c;
			$accountName = $record->Account__r->Name;
			$AccountNameHref = "<a href='".$instance_url."/".$AccountNameLink."' target='".$AccountNameLink."'>".$accountName."</a>";

			$GPNameLink = $record->Id;
			$greenProspectName = $record->Name;
			$GPNameHref = "<a href='".$instance_url."/".$GPNameLink."' target='".$GPNameLink."'>".$greenProspectName."</a>";
			
			
			$SalesForceHoursFound[$record->Id] = $SalesForceHours[$record->Id];
			$SalesForceHoursFound[$record->Id]["WorkCategory"] = $SalesForceHoursByWorkCategory[$record->Id];
			$SalesForceHoursFound[$record->Id]["AccountId"] = $AccountNameLink;
			$SalesForceHoursFound[$record->Id]["AccountName"] = $accountName;
			$SalesForceHoursFound[$record->Id]["AccountNameHref"] = $AccountNameHref;
			$SalesForceHoursFound[$record->Id]["GPName"] = $greenProspectName;
			$SalesForceHoursFound[$record->Id]["GPNameHref"] = $GPNameHref;
			$SalesForceHoursFound[$record->Id]["PrimaryCampaign"] = $SalesForceCampaignCodesToCheck[$record->Primary_Campaign__c];
		}
	}
	foreach ($SalesForceHoursFound as $gpLink=>$gpHoursInfo){
		if ($gpHoursInfo["PrimaryCampaign"] == '524 NGRID Inspection'){
			foreach ($gpHoursInfo["WorkCategory"]["Dates"] as $date=>$sfHourInfo){
				foreach ($sfHourInfo as $workCategory=>$sfHour){
					$SalesForceHoursResultsByDate[$date]["details"][$gpHoursInfo["PrimaryCampaign"]][] = $sfHour." ".$workCategory." hours account: ".$gpHoursInfo["AccountName"]." [".$gpHoursInfo["GPNameHref"]."]";
					$SalesForceHoursResultsByDate[$date]["hours"] = bcadd($SalesForceHoursResultsByDate[$date]["hours"],$sfHour,2);
				}
			}
			
		}else{
			foreach ($gpHoursInfo["Dates"] as $date=>$sfHour){
				$SalesForceHoursResultsByDate[$date]["details"][$gpHoursInfo["PrimaryCampaign"]][] = $sfHour." hours account: ".$gpHoursInfo["AccountName"]." [".$gpHoursInfo["GPNameHref"]."]";
				$SalesForceHoursResultsByDate[$date]["hours"] = bcadd($SalesForceHoursResultsByDate[$date]["hours"],$sfHour,2);
			}
		}
	}
	ksort($SalesForceHoursResultsByDate);
	if (count($SalesForceHoursResultsByDate)){
		foreach ($SalesForceHoursResultsByDate as $thisDate=>$thisInfo){
			$campaignDetails = "";
			foreach ($thisInfo["details"] as $primaryCampaign=>$info){
				$campaignDetails .= "<div style='padding-left:20px;'><b>".$primaryCampaign.":</b><br>";
				$campaignDetails .= "<div style='padding-left:20px;'>".implode("<br>",$info)."</div>";
				$campaignDetails .= "</div>";
			}
			$displayResults .= "<b>".date("m/d/Y",$thisDate)."</b> ".$thisInfo["hours"]." hours:<br>".$campaignDetails."<hr>";
		}
	}else{
		$displayResults = "No timecard data found";
	}
	//print_pre($SalesForceHoursResultsByDate);
}//end checking Salesforce for hours

?>