<div class="four columns" style="position;absolute;margin-right:-10px;">
			<table class="reportFixedColumnsComparison display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th colspan="2" style="background-color:white;">Staff</th>
					</tr>
					<tr>
						<th class="StaffName CodeDataHeader">Last Name</th>
						<th class="StaffName CodeDataHeader">First Name</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="StaffName">&nbsp;</th>
						<td class="StaffName">Totals</th>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeDataProjections as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\">".$LastName."</td>";
							echo "<td class=\"StaffName\">".$FirstName."</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		</div>
		<style>
			#ToolTables_DataTables_Table_3_0 {visibility:hidden;}
			.comparisonProjected {background-color:#BFCDE0;}
			.comparisonActual {background-color:#DBD9DA;}
			.isSummaryExcluded { background-color:silver;}
		</style>
		<div class="nineteen columns">
			<table class="reportResultsComparison display" cellspacing="0" width="100%">
				<thead>
						<?php 
							$headerRowData = array();
							$headerRowDataTop = array();
							$footerRowData1 = array();
							$footerRowData2 = array();
							$headerRowDataTop[] = '<tr>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="2">Billable Hours</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="4">Other Hours</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="4">Summary</th></tr>';
							$headerRowData[] = '<tr>
								<th class="CodeDataHeader" style="display:none;">&nbsp;</th>
								<th class="CodeDataHeader" style="display:none;">Totals</th>
								<th class="CodeDataHeader subHours comparisonActual">Actual<br><br>Billable Per Week</th>
								<th class="CodeDataHeader subHours comparisonProjected">Projected<br><br>Billable Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonActual">Actual<br><br>Unbillable Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">Projected<br><br>Unbillable Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonActual">Actual<br><br>501 Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">Projected<br><br>501 Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonActual">Actual<br><br>All Code Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">Projected<br><br>All Code Per Week</th>';
							$headerRowData[] = '<th class="CodeDataHeader">Difference Between Actual & Projection<Br><span style="font-size:8pt;color:red">-negative is amount beyond current time period average</span></th>';
							$headerRowData[] = '<th class="CodeDataHeader">Projection Over<br>'.$numberOfHoursPerWeekLimit.'hrs/week<br><br><br><span style="font-size:8pt;color:red">-negative is amount past '.$numberOfHoursPerWeekLimit.'hrs</span></th>';
							$headerRowData[] = '</tr>';
							//$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">Billable Per Day</th><th class="CodeDataHeader totalHoursComplete comparisonProjected">Total Billable Hours</th>';
							//$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">Unbillable Per Day</th><th class="CodeDataHeader totalHoursComplete comparisonProjected" title="Includes 501">Total Unbillable</th>';
							//$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">501 Per Day</th><th class="CodeDataHeader totalCodeHours comparisonProjected">501 Hrs<Br><span style="font-size:8pt;">(non-work hours)</span></th><th class="CodeDataHeader totalHoursComplete comparisonProjected" title="Total Billable + Total Unbillable - 501">Total Work Hours</th>';
							//$headerRowData[] = '<th class="CodeDataHeader subHours comparisonProjected">All Code Per Day</th><th class="CodeDataHeader totalCodeHours comparisonProjected">All Code Hours</th><th class="CodeDataHeader comparisonProjected" title="Hours remaining from 7 per day">Hours Left To Bill/Day</th><th class="CodeDataHeader comparisonProjected">%Billable Hrs</th><th class="CodeDataHeader comparisonProjected">%Unbillable Hrs</th>';
							foreach ($headerRowDataTop as $headerRowColumn){
								echo $headerRowColumn;
							}
							foreach ($headerRowData as $headerRowColumn){
								echo $headerRowColumn;
							}
				?>
				</thead>
				<tbody>
					<?php 
						$bodyRowData = array();
						foreach ($EmployeeDataProjections as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							$bodyRowData[] = "<tr>".
								"<td style=\"display:none;\"><span style='display:none;'>".$LastName."</span>&nbsp;</td>".
								"<td style=\"display:none;\"><span style='display:none;'>".$FirstName."</span>Totals</td>".
								"<td class='hourSummary subHours comparisonActual PerWeekBillableActual".$EmployeeLastFirstName."'".($BillablePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$BillablePerWeekActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours comparisonProjected PerWeekBillable".$EmployeeLastFirstName."'".($BillablePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$BillablePerWeekProjection[$EmployeeLastFirstName]."</td>";

							$bodyRowData[] = "<td class='hourSummary subHours comparisonActual PerWeekUnbillableActual".$EmployeeLastFirstName."'".($UnbillablePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$UnbillablePerWeekActual[$EmployeeLastFirstName]."</td>";
							$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerWeekUnbillable".$EmployeeLastFirstName."'".($UnbillablePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$UnbillablePerWeekProjection[$EmployeeLastFirstName]."</td>";

							$bodyRowData[] = "<td class='hourSummary subHours comparisonActual PerWeek501Actual".$EmployeeLastFirstName."'".($Hour501PerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$Hour501PerWeekActual[$EmployeeLastFirstName]."</td>";
							$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerWeek501".$EmployeeLastFirstName."'".($Hour501PerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$Hour501PerWeekProjection[$EmployeeLastFirstName]."</td>";

							$bodyRowData[] = "<td class='hourSummary subHours comparisonActual PerWeekCodeHoursActual".$EmployeeLastFirstName."'".($TotalCodePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$TotalCodePerWeekActual[$EmployeeLastFirstName]."</td>";
							$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerWeekCodeHours".$EmployeeLastFirstName."'".($TotalCodePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$TotalCodePerWeekProjection[$EmployeeLastFirstName]."</td>";
							
							$isSummaryExcluded = $EmployeeByName[$EmployeeLastFirstName]->excludeFromProjectionSummary;
							$isSummaryExcludedStyle = "";
							if (!$isSummaryExcluded){
								$comparisonDifference = bcsub($TotalCodePerWeekActual[$EmployeeLastFirstName],$TotalCodePerWeekProjection[$EmployeeLastFirstName],2);
								$comparisonDifferenceTotal = bcadd($comparisonDifferenceTotal,$comparisonDifference,2);
								$comparisonDifference40Hrs = bcsub($numberOfHoursPerWeekLimit,$TotalCodePerWeekProjection[$EmployeeLastFirstName],2);
								$comparisonDifference40HrsTotal = bcadd($comparisonDifference40HrsTotal,$comparisonDifference40Hrs,2);
							}else{
								$comparisonDifference = "NA";
								$comparisonDifference40Hrs = "NA";
								$isSummaryExcludedStyle = " isSummaryExcluded";
							}
							$bodyRowData[] = "<td class='hourSummary subHours ComparisonDifference".$EmployeeLastFirstName.$isSummaryExcludedStyle."'".($comparisonDifference < 0 ? " style='color:red;'" : "").">".$comparisonDifference."</td>";
							$bodyRowData[] = "<td class='hourSummary subHours ComparisonDifference40Hrs".$EmployeeLastFirstName.$isSummaryExcludedStyle."'".($comparisonDifference40Hrs < 0 ? " style='color:red;'" : "").">".$comparisonDifference40Hrs."</td>";

							$bodyRowData[] = "</tr>";
							
							//$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerDayBillable".$EmployeeLastFirstName."'>".$BillablePerDayProjection[$EmployeeLastFirstName]."</td><td class='hourSummary totalHoursComplete comparisonProjected TotalBillable".$EmployeeLastFirstName."'>".$BillableProjection[$EmployeeLastFirstName]."</td>";
							//$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerDayUnbillable".$EmployeeLastFirstName."'>".$UnbillablePerDayProjection[$EmployeeLastFirstName]."</td><td class='hourSummary totalHoursComplete comparisonProjected TotalUnbillable".$EmployeeLastFirstName."'>".$UnbillableProjection[$EmployeeLastFirstName]."</td>";
							//$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerDay501".$EmployeeLastFirstName."'>".$Hour501PerDayProjection[$EmployeeLastFirstName]."</td><td class='hourSummary totalCodeHours comparisonProjected Total501".$EmployeeLastFirstName."'>".$Hour501Projection[$EmployeeLastFirstName]."</td><td class='hourSummary totalHoursComplete comparisonProjected TotalWorkHours".$EmployeeLastFirstName."'>".$TotalHoursWorkedProjection[$EmployeeLastFirstName]."</td>";
							//$bodyRowData[] = "<td class='hourSummary subHours comparisonProjected PerDayCodeHours".$EmployeeLastFirstName."'>".$TotalCodePerDayProjection[$EmployeeLastFirstName]."</td><td class='hourSummary totalCodeHours comparisonProjected TotalCodeHours".$EmployeeLastFirstName."'>".$TotalCodeHoursProjection[$EmployeeLastFirstName]."</td><td class='hourSummary comparisonProjected HoursLeftToBill".$EmployeeLastFirstName."'".($HoursRemainingProjection[$EmployeeLastFirstName] <= 0 ? " style='color:red;'" : "").">".$HoursRemainingProjection[$EmployeeLastFirstName]."</td><td class='hourSummary comparisonProjected PercentBilled".$EmployeeLastFirstName."'>".bcmul($PercentBilledProjection[$EmployeeLastFirstName],100,2)."%</td><td class='hourSummary comparisonProjected PercentUnbilled".$EmployeeLastFirstName."'>".bcmul($PercentUnBillableProjection[$EmployeeLastFirstName],100,2)."%</td>".
						}
						foreach ($bodyRowData as $bodyRow){
							echo $bodyRow;
						}
					?>
				</tbody>
				<tfoot>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekBillableActual comparisonActual\">".$BillablePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekBillable comparisonProjected\">".$BillablePerWeekFooterProjection."</td>";
							
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekUnbillableActual comparisonActual\">".$UnbillablePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekUnbillable comparisonProjected\">".$UnbillablePerWeekFooterProjection."</td>";

							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeek501Actual comparisonActual\">".$Hour501PerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeek501 comparisonProjected\">".$Hour501PerWeekFooterProjection."</td>";
							
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekCodeHoursActual comparisonActual\">".$TotalCodePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekCodeHours comparisonProjected\">".$TotalCodePerWeekFooterProjection."</td>";

							echo "<td class=\"CodeDataFooter footerSeparator FooterComparisonDifference\">".$comparisonDifferenceTotal."</td>";
							echo "<td class=\"CodeDataFooter footerSeparator FooterComparisonDifference40Hrs\">".$comparisonDifference40HrsTotal."</td>";

							//echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayBillable comparisonProjected\">".$BillablePerDayFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalBillable comparisonProjected\">".$BillableFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayUnbillable comparisonProjected\">".$UnbillablePerDayFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalUnbillable comparisonProjected\">".$UnbillableFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDay501 comparisonProjected\">".$Hour501PerDayFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator FooterTotal501 comparisonProjected\">".$Hour501FooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalWorkHours comparisonProjected\">".$TotalHoursWorkedFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayCodeHours comparisonProjected\">".$TotalCodePerDayFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator FooterTotalCodeHours comparisonProjected\">".$TotalCodeHoursFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter footerSeparator FooterHoursLeftToBill comparisonProjected\">".$HoursRemainingFooterProjection."</td>";
							//echo "<td class=\"CodeDataFooter footerSeparator FooterPercentBilled comparisonProjected\">".bcmul($PercentBilledFooterProjection,100,2)."%</td>";
							//echo "<td class=\"CodeDataFooter footerSeparator FooterPercentUnbilled comparisonProjected\">".bcmul($PercentUnBillableFooterProjection,100,2)."%</td>";
						?>
					</tr>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							foreach ($footerRowData2 as $footerRowColumn){
								echo $footerRowColumn;
							}
						?>
							<td class="subHours comparisonActual" style='height:120px;'>Actual Billable Per Week</td>
							<td class="subHours comparisonProjected" style='height:120px;'>Projected Billable Per Week</td>

							<td class="subHours comparisonActual" style='height:120px;'>Actual Unbillable Per Week</td>
							<td class="subHours comparisonProjected" style='height:120px;'>Projected Unbillable Per Week</td>

							<td class="subHours comparisonActual" style='height:120px;'>Actual 501 Per Week</td>
							<td class="subHours comparisonProjected" style='height:120px;'>Projected 501 Per Week</td>

							<td class="subHours comparisonActual" style='height:120px;'>Actual All Code Per Week</td>
							<td class="subHours comparisonProjected" style='height:120px;'>Projected All Code Per Week</td>

							<td class="subHours" style='height:120px;'>Difference or Surplus<br><span style='font-size:8pt;'>(negative is hours/week needed beyond current capacity)</span></td>
							<td class="subHours" style='height:120px;'>Beyond <?php echo $numberOfHoursPerWeekLimit;?>hrs<br><span style='font-size:8pt;'>(negative is staffing shortage)</span></td>
						<!--	
							<td class="subHours comparisonProjected" style='height:120px;'>Billable Per Day</td>
							<td class="totalHoursComplete comparisonProjected" style='height:120px;'>Total</td>
							<td class="subHours comparisonProjected" style='height:120px;'>Unbillable Per Day</td>
							<td class="totalHoursComplete comparisonProjected" title="Includes 501">Total Unbillable</td>
							<td class="subHours comparisonProjected" style='height:120px;'>501 Per Day</td>
							<td class="totalCodeHours comparisonProjected" >501 Hrs</td>
							<td class="totalHoursComplete comparisonProjected" title="Total Billable + Total Unbillable - 501">Total Wrk_Hrs</td>
							<td class="subHours comparisonProjected" style='height:120px;'>All Code Per Day</td>
							<td class="totalCodeHours comparisonProjected" style='height:120px;'>All Code Hours</td>
							<td class="comparisonProjected" title="Hours remaining from 7 per day">Hours Left To Bill/Day</td>
							<td class="comparisonProjected">%Billable Hrs</td>
							<td class="comparisonProjected">%Unbillable Hrs</td>
						-->
					</tr>
				</tfoot>
			</table>
		</div>