<div class="four columns" style="position;absolute;margin-right:-10px;">
			<table class="reportFixedColumnsProjections display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th colspan="2" style="background-color:white;">Staff</th>
					</tr>
					<tr>
						<th class="StaffName CodeDataHeader">Last Name</th>
						<th class="StaffName CodeDataHeader">First Name</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="StaffName">&nbsp;</th>
						<td class="StaffName">Totals</th>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeDataProjections as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\">".$LastName."</td>";
							echo "<td class=\"StaffName\">".$FirstName."</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		</div>
		<style>
			th {min-width:70px;width:100px;border:1pt solid black;}
			#ToolTables_DataTables_Table_2_0 {visibility:hidden;}

		</style>
		<div class="nineteen columns">
			<table class="reportResultsProjections display" cellspacing="0" width="100%">
				<thead>
						<?php 
							$headerRowData = array();
							$headerRowDataTop = array();
							$footerRowData1 = array();
							$footerRowData2 = array();
							$columnsToHideProjections = array();
							$headerRowData[] = '<tr>
								<th class="StaffName CodeDataHeader" style="display:none;">Last Name</th>
								<th class="StaffName CodeDataHeader" style="display:none;">First Name</th>';
							$headerRowDataTop[] = '<tr>';
							$codeColumns = 0;

							
								$lastGroupId = 0;
								$dataColumnNumber = 2;
								$firstDataColumnNumberForThisGroup = 2;
								foreach ($CodesDataProjectionsByGroupId as $groupIdOrder=>$codeInfo){
									ksort($codeInfo);
									foreach ($codeInfo as $codeName=>$codeId){
										$groupId = $CodesByName[$codeName]->groupId;
										$isUnbillableCode = $CodesByID[$codeId]->nonbillable;

										$colorBackground = $GroupsByID[$groupId]->colorBackground;
										$colorBorder = $GroupsByID[$groupId]->colorBorder;
										if ($groupIdOrder == "ZZZNotInvoicedGroupId"){
											$colorBackground = $GroupsByID[$groupIdOrder]->colorBackground;
											$colorBorder = $GroupsByID[$groupIdOrder]->colorBorder;
										}
										$colorBackgroundStyle = ($colorBackground ? "background-color:".$colorBackground.";" : "");
										$colorBorderStyle = ($colorBorder ? "border:1pt solid ".$colorBorder.";" : "");
										$style = "";
										$textStyle = "";
										$isExcludedFromProjections = $CodesByID[$codeId]->excludeFromProjections;
										if ($isExcludedFromProjections || $isUnbillableCode){
											$textStyle = "color:".$colorBorder.";font-style:italic;";
										}
										if ($colorBackgroundStyle || $colorBorderStyle){$style = " style='".$colorBorderStyle.$colorBackgroundStyle.$textStyle."'";}
										if ($lastColorBorder){
											if ($lastGroupId > 0 && $lastGroupId != $groupId && $lastColorBorder != $colorBorder){
												$invoiceId = $GroupsByID[$lastGroupId]->invoiceId;
												$budgetAmountOrg = $invoiceAmountHistoryByInvoiceId[$invoiceId]["budget"];
												$budgetAmount = $remainingBudgetByGroupId[$lastGroupId]["remainingBudget"];
												$remainingNumberOfMonths = $remainingBudgetByGroupId[$lastGroupId]["remainingMonths"];
												if ($budgetAmount > 0.00){
													$used = $invoiceAmountHistoryByInvoiceId[$invoiceId]["used"];
													$remainingPerMonth = $remainingBudgetByGroupId[$lastGroupId]["budgetPerMonth"];
													$allocatedUsed = bcmul($remainingPerMonth,$remainingNumberOfMonths,2);
													$remaining = bcsub($budgetAmount,$allocatedUsed,2);
													$projectedUse = $CodesDataProjectionsBillingTotalsByGroupId[$lastGroupId];
													$projectedRemaining = bcsub($remainingPerMonth,$projectedUse,2);
													$overAllProjectedRemaining = bcmul($projectedRemaining,$remainingNumberOfMonths,2);
													$overAllProjectedRemainingDisplay = " [<span title='Overall Budget Remaining $".money($projectedRemaining)."x".$remainingNumberOfMonths."'>$".money($overAllProjectedRemaining)."</span>";
													$negative ="<span title='Overall Budget Remaining:  Monthly balance remaining in budget unused by staff $".money($projectedRemaining)." x ".$remainingNumberOfMonths." remaining months.' id='Remaining".$lastGroupId."'>";
													if ($projectedRemaining < 0){$negative = "<span style='color:red;' title='Monthly balance overspent from budget by staff' id='Remaining".$lastGroupId."'>";$projectedRemaining = ($projectedRemaining*(-1));}
													$budgetDisplay = "<div style='width:80%;text-align:right;font-size:8pt;padding-right:0px;'><span  title='Original Entire Budget $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetComplete"])." less NonCode Related Budget $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetNonCodeRelated"])."'>$".money($budgetAmountOrg)."</span><hr>
														<span title='Remaining Staff Use Budget'>$".money($budgetAmount)."</span>
														<br><span title='Allocated Per Remaining ".$remainingNumberOfMonths." Months will end up with $".money($remaining)."'>[$".money($remainingPerMonth)."]</span>&nbsp;
														<hr><span title='Projected Monthly Use based on ".$numberOfWeeks." weeks usage ratios'>- <span id='Used".$lastGroupId."'>$".money($CodesDataProjectionsBillingTotalsByGroupId[$lastGroupId])."</span></span>&nbsp;
														<hr style='border:0pt solid black;background-color:black;height:1px;'>
														$negative $".money($overAllProjectedRemaining)."&nbsp;</span>
														</div>";
												}else{
													$budgetDisplay = "<br><br><span style='font-size:8pt;'>Budget Used Up</span>";
												}
												$fiscalYearDisplay = "";
												$fiscalYearStart = MySQLDate($invoicingListById[$GroupsByID[$lastGroupId]->invoiceId]->fiscalYearStart);
												$fiscalYearEnd = MySQLDate($invoicingListById[$GroupsByID[$lastGroupId]->invoiceId]->fiscalYearEnd);
												$monthsLeft = 0;
												if ($fiscalYearStart){
													if (!$fiscalYearEnd){
														$fiscalYearEnd = date("m/d/Y",strtotime($fiscalYearStart." + 364 days"));
													}
													$lastInvoicedDate = date("m/d/Y",$invoiceAmountHistoryByInvoiceId[$invoiceId]["lastInvoiceDate"]);
													$monthsLeft = datediff($lastInvoicedDate,$fiscalYearEnd,"month");
													$fiscalYearDisplay = "<br><span style='font-size:8pt;'>".$fiscalYearStart." to ".$fiscalYearEnd."<br>".$monthsLeft." months left</span>";
												}
												$headerRowData[] = "<th class=\"CodeDataHeader CodeDataHeaderGroup toggleGroupColumns\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";' data-columns='".$firstDataColumnNumberForThisGroup."_".$lastDataColumnNumber."' title='".($CondenseGroup ? "Show" : "Collapse")." Group Codes' data-groupname='".$GroupsByID[$lastGroupId]->name."' data-table='Projections'>".$GroupsByID[$lastGroupId]->name.$toggleButton.$fiscalYearDisplay."</th>";
												$columnsToHideByGroupIdProjections[$lastGroupId][]=$dataColumnNumber;
												$columnsSummaryToHideByGroupIdProjections[$lastGroupId][]=$dataColumnNumber;
												$dataColumnNumber++;
												$headerRowData[] = "<th class=\"CodeDataHeader\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";' data-columns='".$firstDataColumnNumberForThisGroup."_".$lastDataColumnNumber."'>Billing".$budgetDisplay."</th>";
												$columnsToHideByGroupIdProjections[$lastGroupId][]=$dataColumnNumber;
												$columnsSummaryToHideByGroupIdProjections[$lastGroupId][]=$dataColumnNumber;
												$dataColumnNumber++;
												
												$footerRowData1[] = "<td class=\"CodeDataFooter\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";border-top:2pt solid black;'><span id='GroupFooterHours".$lastGroupId."'>".$CodesDataProjectionsTotalsByGroupId[$lastGroupId]."</span></td>";
												$footerRowData1[] = "<td class=\"CodeDataFooter\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";border-top:2pt solid black;'><span id='GroupFooterBilling".$lastGroupId."'>$".money($CodesDataProjectionsBillingTotalsByGroupId[$lastGroupId])."</span></td>";
												
												$footerRowData2[] = "<td class=\"CodeDataFooter\" style='height:120px;border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'>".$GroupsByID[$lastGroupId]->name.$fiscalYearDisplay."</th>";
												$footerRowData2[] = "<td class=\"CodeDataFooter\" style='height:120px;border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'>Billing</th>";

												//echo "<hr>".$GroupsByID[$lastGroupId]->name." ".$GroupsByID[$lastGroupId]->colorBorder." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
												$firstDataColumnNumberForThisGroup = $dataColumnNumber;
												$codeColumns++;
												$codeColumns++;
											}
										}else{
											if ($lastGroupId != $groupId){
												//echo $GroupsByID[$lastGroupId]->name." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
											}
										}
										//$thisGroupIdTotal[$groupId] = $thisGroupIdTotal[$groupId]+$CodesDataProjectionsTotals[$codeId];
										$description = str_replace($codeName."-","",$CodesByName[$codeName]->description);
										$description = str_replace($codeName." -","",$description);
										$description = str_replace($codeName,"",$description);
										$description = trim($description);
										
										$descriptionWrap = wordwrap($description, 15, "<Br>", true);
										$descriptionWrapParts = explode("<Br>",$descriptionWrap);
										if (count($descriptionWrapParts)>3){
											$descriptionWrap = "<span title='".$description."'>".$descriptionWrapParts[0]."<Br>".$descriptionWrapParts[1]."<br>".$descriptionWrapParts[2]."...</span>";
										}
										//$descriptionWrap = $descriptionWrap." ".count($descriptionWrapParts);
										
										
										$CondenseGroupOverRide = true;
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){
											$CondenseGroupOverRide = false;
											$columnsToHideByGroupIdProjections["ZZZNotInvoicedGroupId"][]=$dataColumnNumber;
										}else{
											$columnsToHideByGroupIdProjections[$groupId][]=$dataColumnNumber;
										}
										
										if ($CondenseGroup && $CondenseGroupOverRide){
											$columnsToHideProjections[]=$dataColumnNumber;
										}
										$remainingCodeBalance = bcsub($CodesByID[$codeId]->budget,$CodesDataTotalsBillingAllTime[$codeId],2);
										$projectedUseAmount = bcmul($CodesDataTotalsBillingProjections[$codeId],$remainingBudgetByGroupId[$groupId]["remainingMonths"],2);
										$projectedEndingCodeBalance = bcsub($remainingCodeBalance,$projectedUseAmount,2);
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){
											$headerRowData[] = "<th class=\"CodeDataHeader\"".$style.">".$codeName."<br><span style='font-size:8pt;'>".$descriptionWrap."</span></th>";
										}else{
											$headerRowData[] = "<th class=\"CodeDataHeader\"".$style.">".$codeName."<br><span style='font-size:8pt;'>".$descriptionWrap."</span>
												".(!$isExcludedFromProjections && !$isUnbillableCode? "<div style='font-size:8pt;position:absolute;margin-bottom:1px;bottom:0px;'>
													".($CodesByID[$codeId]->budget > 1 ? "<span title='FY budgeted amount'>$".money($CodesByID[$codeId]->budget)."</span>" : "<span style='color:red;'>No Budget</span>")."<br>
													<span title='staff hours already invoiced'>-$".money($CodesDataTotalsBillingAllTime[$codeId])."</span><br>
													<span title='".$codeName." budget remaining' style='border-top:1pt solid black;' id='remainingCodeBudget".$codeId."'>$".money($remainingCodeBalance)."</span><br>
													(<span title='Monthly projections' style='font-size:8pt;' id='codeProjectionsUsedPerMonth".$codeId."'>$".money($CodesDataTotalsBillingProjections[$codeId])."</span> 
													x ".$remainingBudgetByGroupId[$groupId]["remainingMonths"].")</span><br>
													<div title='Projected Ending Balance: $".money($remainingCodeBalance)." - $".money($projectedUseAmount)."' id='projectedEndingCodeBalance".$codeId."' style='font-size:8pt;color:".($projectedEndingCodeBalance >=0 ? "black" : "red").";border-top:1pt dashed black;'>$".money($projectedEndingCodeBalance)."</div>
												</div>" : "")."
											</th>";
										}
										$footerRowData1[] = "<td class=\"CodeDataFooter\"".str_replace(";'","border-top:2pt solid black;'",$style)." id='CodeFooter".$codeId."'>".$CodesDataProjectionsTotals[$codeId]."</td>";
										$footerRowData2[] = "<td class=\"CodeDataFooter\"".$style.">".$codeName."</td>";
										//echo $codeName.":".$colorBackground." ".$colorBorder." ".$CodesDataTotals[$codeId]." ".$CodesDataTotalsByGroupId[$groupId]."<br>";
										$lastGroupId = $groupId;
										$lastColorBorder = $colorBorder;
										$lastColorBackground = $colorBackground;
										$lastDataColumnNumber = $dataColumnNumber;
										$dataColumnNumber++;
										$codeColumns++;
									}
								}
							//$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="2" style="display:none;">Staff</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="'.$codeColumns.'">Codes</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="3">Billable</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="6">Other Hours</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="7">Summary</th></tr>';
								
							$headerRowData[] = '
								<th class="CodeDataHeader subHours">Billable Per Week</th>
								<th class="CodeDataHeader subHours">Billable Per Day</th>
								<th class="CodeDataHeader totalHoursComplete">Total Billable Hours</th>
								<th class="CodeDataHeader subHours">Unbillable Per Week</th>
								<th class="CodeDataHeader subHours">Unbillable Per Day</th>
								<th class="CodeDataHeader totalHoursComplete" title="Includes 501">Total Unbillable</th>
								<th class="CodeDataHeader subHours">501 Per Week</th>
								<th class="CodeDataHeader subHours">501 Per Day</th>
								<th class="CodeDataHeader totalCodeHours">501 Hrs<Br><span style="font-size:8pt;">(non-work hours)</span></th>
								<th class="CodeDataHeader totalHoursComplete" title="Total Billable + Total Unbillable - 501">Total Work Hours</th>
								<th class="CodeDataHeader subHours">All Code Per Week</th>
								<th class="CodeDataHeader subHours">All Code Per Day</th>
								<th class="CodeDataHeader totalCodeHours">All Code Hours</th>
								<th class="CodeDataHeader" title="Hours remaining from '.$numberOfHoursPerDayLimit.' per day">Hours Left To Bill/Day</th>
								<th class="CodeDataHeader">%Billable Hrs</th>
								<th class="CodeDataHeader">%Unbillable Hrs</th>
							</tr>';
							foreach ($headerRowDataTop as $headerRowColumn){
								echo $headerRowColumn;
							}
							foreach ($headerRowData as $headerRowColumn){
								echo $headerRowColumn;
							}
				?>
				</thead>
				<tfoot>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							foreach ($footerRowData1 as $footerRowColumn){
								echo $footerRowColumn;
							}

							$BillableFooterProjection = floorToFraction($TotalAggregateDataProjections["totalBillable"]);
							$BillablePerWeekFooterProjection = round(($BillableFooterProjection/$numberOfWorkWeeksPerMonth),2);
							$BillablePerDayFooterProjection = round(($BillableFooterProjection/$numberOfWorkDaysPerMonth),2);
							$HoursRemainingFooterProjection = bcsub(($numberOfHoursPerDayLimit*$numberOfWorkDaysPerMonth),$BillablePerDayFooterProjection,2);
							$UnbillableFooterProjection = floorToFraction(($TotalAggregateDataProjections["totalUnBillable"]-$TotalAggregateDataProjections["501Hours"]));
							$UnbillablePerWeekFooterProjection = round(($UnbillableFooterProjection/$numberOfWorkWeeksPerMonth),2);
							$UnbillablePerDayFooterProjection = round(($UnbillableFooterProjection/$numberOfWorkDaysPerMonth),2);
							$Hour501FooterProjection = floorToFraction($TotalAggregateDataProjections["501Hours"]);
							$Hour501PerWeekFooterProjection = round(($Hour501FooterProjection/$numberOfWorkWeeksPerMonth),2);
							$Hour501PerDayFooterProjection = round(($Hour501FooterProjection/$numberOfWorkDaysPerMonth),2);
							$TotalHoursWorkedFooterProjection = bcadd($BillableFooterProjection,$UnbillableFooterProjection,2);
							$TotalCodeHoursFooterProjection = bcadd($TotalHoursWorkedFooterProjection,$Hour501FooterProjection,2);
							$TotalCodePerWeekFooterProjection = round(($TotalCodeHoursFooterProjection/$numberOfWorkWeeksPerMonth),2);
							$TotalCodePerDayFooterProjection = round(($TotalCodeHoursFooterProjection/$numberOfWorkDaysPerMonth),2);
							
							//For projections incorporate the 501 hours because they will consider this as time used
							$TotalWorkPerDayFooterProjection = bcadd($BillablePerDayFooterProjection,$UnbillablePerDayFooterProjection,2);
							$TotalHoursPerDayFooterProjection = bcadd($TotalWorkPerDayFooterProjection,$Hour501PerDayFooterProjection,2);
							$HoursRemainingFooterProjection = bcsub(($numberOfHoursPerDayLimit*(count($EmployeeDataProjections))),$TotalHoursPerDayFooterProjection,2);
							
							$PercentBilledFooterProjection = bcdiv($BillableFooterProjection,$TotalCodeHoursFooterProjection,4);
							$PercentUnBillableFooterProjection = bcdiv($UnbillableFooterProjection,$TotalCodeHoursFooterProjection,4);
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekBillable\">".$BillablePerWeekFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayBillable\">".$BillablePerDayFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalBillable\">".$BillableFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekUnbillable\">".$UnbillablePerWeekFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayUnbillable\">".$UnbillablePerDayFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalUnbillable\">".$UnbillableFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeek501\">".$Hour501PerWeekFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDay501\">".$Hour501PerDayFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator FooterTotal501\">".$Hour501FooterProjection."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator FooterTotalWorkHours\">".$TotalHoursWorkedFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerWeekCodeHours\">".$TotalCodePerWeekFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator FooterPerDayCodeHours\">".$TotalCodePerDayFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator FooterTotalCodeHours\">".$TotalCodeHoursFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter footerSeparator FooterHoursLeftToBill\">".$HoursRemainingFooterProjection."</td>";
							echo "<td class=\"CodeDataFooter footerSeparator FooterPercentBilled\">".bcmul($PercentBilledFooterProjection,100,2)."%</td>";
							echo "<td class=\"CodeDataFooter footerSeparator FooterPercentUnbilled\">".bcmul($PercentUnBillableFooterProjection,100,2)."%</td>";
						?>
					</tr>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							foreach ($footerRowData2 as $footerRowColumn){
								echo $footerRowColumn;
							}
						?>
							<td class="subHours" style='height:120px;'>Billable Per Week</td>
							<td class="subHours" style='height:120px;'>Billable Per Day</td>
							<td class="totalHoursComplete" style='height:120px;'>Total</td>
							<td class="subHours" style='height:120px;'>Unbillable Per Week</td>
							<td class="subHours" style='height:120px;'>Unbillable Per Day</td>
							<td class="totalHoursComplete" title="Includes 501">Total Unbillable</td>
							<td class="subHours" style='height:120px;'>501 Per Week</td>
							<td class="subHours" style='height:120px;'>501 Per Day</td>
							<td class="totalCodeHours" >501 Hrs</td>
							<td class="totalHoursComplete" title="Total Billable + Total Unbillable - 501">Total Wrk_Hrs</td>
							<td class="subHours" style='height:120px;'>All Code Per Week</td>
							<td class="subHours" style='height:120px;'>All Code Per Day</td>
							<td class="totalCodeHours" style='height:120px;'>All Code Hours</td>
							<td title="Hours remaining from <?php echo $numberOfHoursPerDayLimit;?> per day">Hours Left To Bill/Day</td>
							<td>%Billable Hrs</td>
							<td>%Unbillable Hrs</td>
					</tr>
				</tfoot>
				<style>
					.groupHourInput {display:none;width:40px;}
					.groupHourSpan {display:block;}
				</style>
				<tbody>
					<?php 
						$bodyRowData = array();
						foreach ($EmployeeDataProjections as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							$isSummaryExcluded = $EmployeeByName[$EmployeeLastFirstName]->excludeFromProjectionSummary;
							$bodyRowData[] = "<tr>".
								"<td class=\"StaffName\" style=\"display:none;\">".$LastName."</td>".
								"<td class=\"StaffName\" style=\"display:none;\">".$FirstName."</td>";
								$lastGroupId = 0;
								$lastColorBorder = "";
								$lastColorBackground = "";
								$lastBillingRateId = 0;
								
								foreach ($CodesDataProjectionsByGroupId as $groupIdOrder=>$codeInfo){
									ksort($codeInfo);
									foreach ($codeInfo as $codeName=>$codeId){
										$groupId = $CodesByName[$codeName]->groupId;
										$billingRateId = $CodesByName[$codeName]->billingRateId;
										$colorBackground = $GroupsByID[$groupId]->colorBackground;
										$colorBorder = $GroupsByID[$groupId]->colorBorder;
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){
											$colorBackground = $GroupsByID[$groupIdOrder]->colorBackground;
											$colorBorder = $GroupsByID[$groupIdOrder]->colorBorder;
										}
										$colorBackgroundStyle = ($colorBackground ? "background-color:".$colorBackground.";" : "");
										$colorBorderStyle = ($colorBorder ? "border:1pt solid ".$colorBorder.";" : "");
										$style = "";
										$textStyle = "";
										$isExcludedFromProjections = $CodesByID[$codeId]->excludeFromProjections;
										$isUnbillableCode = $CodesByID[$codeId]->nonbillable;
										if ($isExcludedFromProjections || $isUnbillableCode){
											$textStyle = "color:".$colorBorder.";font-style:italic;";
										}
										if ($colorBackgroundStyle || $colorBorderStyle){$style = " style='".$colorBorderStyle.$colorBackgroundStyle.$textStyle."'";}
										if ($lastColorBorder){
											if ($lastGroupId > 0 && $lastGroupId != $groupId && $lastColorBorder != $colorBorder){
												$billingRate = $BillingRateByEmployee[$EmployeeLastFirstName][$lastBillingRateId];

												$bodyRowData[] = "<td align=\"center\" class=\"hourData\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'><span id='".$EmployeeLastFirstName.$lastGroupId."'>".$EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalHours"]."</span>".($EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalHours"] ? " <div style='font-size:8pt;position:relative;float:right;padding-top:2px;'>x$".(!strpos($EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["billingRate"],"_") ? money($EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["billingRate"]) : "<span style='color:red'>missing</span>")." <span id='unbillableHours".$EmployeeLastFirstName.$lastGroupId."' title='Unbillable Hours' class='unbillableHours'>".$EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["unbillableHours"]."</span></div>" : "")."</td>";
												$bodyRowData[] = "<td align=\"center\" class=\"hourData\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'><span id='Billing".$EmployeeLastFirstName.$lastGroupId."'>".($EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalBilling"] ? "$".money($EmployeeDataProjectionsByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalBilling"]) : "")."</span></td>";
											}
										}else{
											if ($lastGroupId != $groupId){
												//echo $GroupsByID[$lastGroupId]->name." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
											}
										}
										$billingRate = (!strpos($EmployeeDataProjectionsByGroupId[$groupId][$EmployeeLastFirstName]["billingRate"],"_") ? $EmployeeDataProjectionsByGroupId[$groupId][$EmployeeLastFirstName]["billingRate"] : 0);
										$CondenseGroupOverRide = true;
										$AllowEdits = "";
										$AllowNoEditsReason = "";
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){$CondenseGroupOverRide = false;$AllowEdits="No";}
										if (!$EmployeeDataProjectionsByGroupId[$groupId][$EmployeeLastFirstName]["billingRate"] || strpos($EmployeeDataProjectionsByGroupId[$groupId][$EmployeeLastFirstName]["billingRate"],"_")){$AllowEdits="No";$AllowNoEditsReason = " title='Cannot adjust because: ".(!$EmployeeDataProjectionsByGroupId[$groupId][$EmployeeLastFirstName]["billingRate"] ? "No Billing Rate set" : "Billing Rate Missing")."'";}
										$theseHours = ($HoursData[$codeId]["totalHours"] ? $HoursData[$codeId]["totalHours"] : "0");
										//exception for 501 hours
										$is501 = 0;
										if ($codeName == "501"){$AllowEdits = "";$AllowNoEditsReason="";$is501 = 1;}
										$dataIsUnbillable = ($isUnbillableCode || $isExcludedFromProjections ? "1" : "0");
										$bodyRowData[] = "<td align=\"center\" class=\"hourData\"".$style.$AllowNoEditsReason."><span id='Span".$EmployeeLastFirstName.$codeId."' class='groupHourSpan".$AllowEdits."' data-groupid='".$EmployeeLastFirstName.$groupId."' data-hourid='".$EmployeeLastFirstName.$codeId."' style='".($theseHours > 0 ? "color:black;" : "color:".$colorBackground.";").($isExcludedFromProjections || $isUnbillableCode ? "color:".($theseHours > 0 ? $colorBorder : $colorBackground).";font-style:italic" :"").";' >".$theseHours."</span><input id='Input".$EmployeeLastFirstName.$codeId."' class='groupHourInput' data-originalhour='".($HoursData[$codeId]["totalHours"] ? : 0)."' data-employeename='".$EmployeeLastFirstName."' data-groupid='".$EmployeeLastFirstName.$groupId."' data-hourid='".$EmployeeLastFirstName.$codeId."' data-billingrate='".$billingRate."' value='".$HoursData[$codeId]["totalHours"]."' data-numberofmonths='".$remainingBudgetByGroupId[$groupId]["remainingMonths"]."' data-isunbillable='".($dataIsUnbillable)."' data-is501='".$is501."' data-issummaryexcluded='".$isSummaryExcluded."' data-fontcolor='".($isExcludedFromProjections || $isUnbillableCode ? "color:".($theseHours > 0 ? $colorBorder : $colorBackground).";font-style:italic" :"")."'></td>";
										$lastGroupId = $groupId;
										$lastColorBorder = $colorBorder;
										$lastColorBackground = $colorBackground;
										$lastBillingRateId = $billingRateId;
									}
								}
							if (!$isSummaryExcluded){
								$staffNotExcludedFromSummary++;
							}

							$BillableProjection[$EmployeeLastFirstName] = floorToFraction($EmployeeAggregateDataProjections[$EmployeeLastFirstName]["totalBillable"]);
							$BillablePerWeekProjection[$EmployeeLastFirstName] = round(($BillableProjection[$EmployeeLastFirstName]/$numberOfWorkWeeksPerMonth),2);
							$BillablePerDayProjection[$EmployeeLastFirstName] = round(($BillableProjection[$EmployeeLastFirstName]/$numberOfWorkDaysPerMonth),2);
							$HoursRemainingProjection[$EmployeeLastFirstName] = bcsub($numberOfHoursPerDayLimit,$BillablePerDayProjection[$EmployeeLastFirstName],2);
							$UnbillableProjection[$EmployeeLastFirstName] = floorToFraction(($EmployeeAggregateDataProjections[$EmployeeLastFirstName]["totalUnBillable"]-$EmployeeAggregateDataProjections[$EmployeeLastFirstName]["501Hours"]));
							$UnbillablePerWeekProjection[$EmployeeLastFirstName] = round(($UnbillableProjection[$EmployeeLastFirstName]/$numberOfWorkWeeksPerMonth),2);
							$UnbillablePerDayProjection[$EmployeeLastFirstName] = round(($UnbillableProjection[$EmployeeLastFirstName]/$numberOfWorkDaysPerMonth),2);
							$Hour501Projection[$EmployeeLastFirstName] = floorToFraction($EmployeeAggregateDataProjections[$EmployeeLastFirstName]["501Hours"]);
							$Hour501PerWeekProjection[$EmployeeLastFirstName] = round(($Hour501Projection[$EmployeeLastFirstName]/$numberOfWorkWeeksPerMonth),2);
							$Hour501PerDayProjection[$EmployeeLastFirstName] = round(($Hour501Projection[$EmployeeLastFirstName]/$numberOfWorkDaysPerMonth),2);
							$TotalHoursWorkedProjection[$EmployeeLastFirstName] = bcadd($BillableProjection[$EmployeeLastFirstName],$UnbillableProjection[$EmployeeLastFirstName],2);
							$TotalCodeHoursProjection[$EmployeeLastFirstName] = bcadd($TotalHoursWorkedProjection[$EmployeeLastFirstName],$Hour501Projection[$EmployeeLastFirstName],2);
							$TotalCodePerWeekProjection[$EmployeeLastFirstName] = round(($TotalCodeHoursProjection[$EmployeeLastFirstName]/$numberOfWorkWeeksPerMonth),2);
							$TotalCodePerDayProjection[$EmployeeLastFirstName] = round(($TotalCodeHoursProjection[$EmployeeLastFirstName]/$numberOfWorkDaysPerMonth),2);
							
							//For projections incorporate the 501 hours because they will consider this as time used
							$TotalWorkPerDayProjection[$EmployeeLastFirstName] = bcadd($BillablePerDayProjection[$EmployeeLastFirstName],$UnbillablePerDayProjection[$EmployeeLastFirstName],2);
							$TotalHoursPerDayProjection[$EmployeeLastFirstName] = bcadd($TotalWorkPerDayProjection[$EmployeeLastFirstName],$Hour501PerDayProjection[$EmployeeLastFirstName],2);
							$HoursRemainingProjection[$EmployeeLastFirstName] = bcsub($numberOfHoursPerDayLimit,$TotalHoursPerDayProjection[$EmployeeLastFirstName],2);
							$TotalDaysWorkedProjection[$EmployeeLastFirstName] = ceil(bcdiv($TotalHoursWorkedProjection[$EmployeeLastFirstName],5,2));
							$PercentBilledProjection[$EmployeeLastFirstName] = bcdiv($BillableProjection[$EmployeeLastFirstName],$TotalCodeHoursProjection[$EmployeeLastFirstName],4);
							$PercentUnBillableProjection[$EmployeeLastFirstName] = bcdiv($UnbillableProjection[$EmployeeLastFirstName],$TotalCodeHoursProjection[$EmployeeLastFirstName],4);
							$bodyRowData[] = 
								"<td class='hourSummary subHours PerWeekBillable".$EmployeeLastFirstName."'".($BillablePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$BillablePerWeekProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerDayBillable".$EmployeeLastFirstName."'>".$BillablePerDayProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete TotalBillable".$EmployeeLastFirstName."'>".$BillableProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerWeekUnbillable".$EmployeeLastFirstName."'".($UnbillablePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$UnbillablePerWeekProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerDayUnbillable".$EmployeeLastFirstName."'>".$UnbillablePerDayProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete TotalUnbillable".$EmployeeLastFirstName."'>".$UnbillableProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerWeek501".$EmployeeLastFirstName."'".($Hour501PerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$Hour501PerWeekProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerDay501".$EmployeeLastFirstName."'>".$Hour501PerDayProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalCodeHours Total501".$EmployeeLastFirstName."'>".$Hour501Projection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete TotalWorkHours".$EmployeeLastFirstName."'>".$TotalHoursWorkedProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerWeekCodeHours".$EmployeeLastFirstName."'".($TotalCodePerWeekProjection[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$TotalCodePerWeekProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours PerDayCodeHours".$EmployeeLastFirstName."'>".$TotalCodePerDayProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalCodeHours TotalCodeHours".$EmployeeLastFirstName."'>".$TotalCodeHoursProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary HoursLeftToBill".$EmployeeLastFirstName."'".($HoursRemainingProjection[$EmployeeLastFirstName] <= 0 ? " style='color:red;'" : "").">".$HoursRemainingProjection[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary PercentBilled".$EmployeeLastFirstName."'>".bcmul($PercentBilledProjection[$EmployeeLastFirstName],100,2)."%</td>".
								"<td class='hourSummary PercentUnbilled".$EmployeeLastFirstName."'>".bcmul($PercentUnBillableProjection[$EmployeeLastFirstName],100,2)."%</td>".
								"</tr>";
						}
						foreach ($bodyRowData as $bodyRow){
							echo $bodyRow;
						}
					?>
				</tbody>
			</table>
		</div>