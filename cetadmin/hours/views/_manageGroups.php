<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

$ThisDepartmentShort = "ALL";

include_once($dbProviderFolder."InvoicingProvider.php");
$invoicingProvider = new InvoicingProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$results = $invoicingProvider->getInvoicingCodes($criteria = null);
$resultArray = $results->collection;
foreach ($resultArray as $result=>$record){
	$invoicingListById[$record->id] = $record;
}

include_once($dbProviderFolder."HoursProvider.php");
$gbsProvider = new HoursProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
//$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
}
ksort($GroupsByName);
//resort the products without display order by GroupID
$SelectedGroupsID = $_GET['GroupID'];
$SelectedGroupName = $_GET['GroupName'];
if ($SelectedGroupName && !$SelectedGroupsID){
	$SelectedGroupsID = $GroupsByName[$SelectedGroupName]->id;
}
$addGroup = $_GET['addGroup'];
//get Codes in this Group
if ($SelectedGroupsID){
	$criteria = new stdClass();
	$criteria->groupId = $SelectedGroupsID;
	//$criteria->department = $ThisDepartmentShort;
	$paginationResult = $gbsProvider->getCodes($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		$CodesWithGroup[$record->name] = $record;
	}

	ksort($CodesWithGroup);
}//end if SelectedGroupName

?>
<?php if (!$addGroup){?>
	<br>
	<div class="fifteen columns">
		<?php include('_GroupsComboBox.php');?>
	</div>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addGroup?>
<?php if ($SelectedGroupsID || $addGroup){?>
	<link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>css/colorpicker.css">
	<style>
		.NonBillable1 {font-size:10pt;}
	</style>
	<form id="GroupUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Group Details</legend>
			<div class="fifteen columns">
				<div class="row">
					<div style="display:none;">
						<?php if (!$addGroup){?><input type="text" name="id" value="<?php echo $GroupByID[$SelectedGroupsID]->id;?>"><?php }?>
					</div>
					<div class="two columns">Name:</div>
					<div class="eight columns">
						<input type="text" id="name" name="name" style="width:100%;" value="<?php echo $GroupByID[$SelectedGroupsID]->name;?>">
					</div>
				</div>
				<div class="row">
					<div class="two columns">Color:</div>
					<div class="three columns">Background<br>
						<input type='text' id="colorPickerBackground" value=""/>
						<input type='hidden' id="colorBackground" name="colorBackground" value="<?php echo $GroupByID[$SelectedGroupsID]->colorBackground;?>"/>
					</div>
					<div class="three columns">Border<br>
						<input type='text' id="colorPickerBorder" value=""/>
						<input type='hidden' id="colorBorder" name="colorBorder" value="<?php echo $GroupByID[$SelectedGroupsID]->colorBorder;?>"/>
					</div>
					<div class="three columns">
						Display In Projections<br>
						<select name="displayInProjections"><option value="No"<?php echo ($GroupByID[$SelectedGroupsID]->displayInProjections != "Yes" ? " selected" : "");?>>No</option><option value="Yes"<?php echo ($GroupByID[$SelectedGroupsID]->displayInProjections == "Yes" ? " selected" : "");?>>Yes</option></select>
					</div>
				</div>
				<hr>
				<div class="fourteen columns">
					<div class="six columns">Related to Which <?php echo ($GroupByID[$SelectedGroupsID]->invoiceId ? "<a href='".$CurrentServer.$adminFolder."hours/?nav=manage-invoicing&InvoicingID=".$GroupByID[$SelectedGroupsID]->invoiceId."'>" : "");?>Invoice Process<?php echo ($GroupByID[$SelectedGroupsID]->invoiceId ? "</a>" : "");?>:</div>
					<div class="five columns">
						<select name="invoiceId">
							<option value="">Not Connected to Invoice Process</option>
							<?php
								foreach ($invoicingListById as $id=>$record){
									echo "<option value='".$id."'".($GroupByID[$SelectedGroupsID]->invoiceId == $id ? " selected" : "").">".$record->displayName." (".$record->codes.")</option>";
								}
							?>
						</select>
					</div>
				</div>
			</div>
		</fieldset>
		<div id="groupResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-groups&GroupID=<?php echo $SelectedGroupsID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addGroup){?>
					<a href="save-group-add" id="save-group-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="group_add">
				<?php }else{?>
					<a href="save-group-update" id="save-group-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="group_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
	<?php if ($SelectedGroupsID){
//			$GroupByID[$SelectedGroupsID]->colorBackground = "#f9cb9c";
//			$GroupByID[$SelectedGroupsID]->colorBorder = "#c9a580";
			
			?>
			<style>
				.codeInGroup {
						padding:2px;
						border:1pt solid <?php echo $GroupByID[$SelectedGroupsID]->colorBorder;?>;
						background-color: <?php echo $GroupByID[$SelectedGroupsID]->colorBackground;?>; 
						text-decoration:none;
						color:black;
					}
				.codeGroup {
						padding:2px;
						border:1pt solid <?php echo $GroupByID[$SelectedGroupsID]->colorBorder;?>;
						background-color: <?php echo $GroupByID[$SelectedGroupsID]->colorBorder;?>; 
					}
			</style>
			<fieldset>
				<legend>Active Codes in this Group</legend>
				<div class="fifteen columns">
						<?php 
							ksort($CodesWithGroup);
							echo "<span class=\"NonBillable codeGroup\">".$GroupByID[$SelectedGroupsID]->name."</span><br>";
							foreach ($CodesWithGroup as $name=>$info){
								if (!$info->dormant){
									echo "<a href='".$CurrentServer.$adminFolder."hours/?nav=manage-codes&CodeID=".$info->id."' class=\"NonBillable".$info->nonbillable." codeInGroup\">".$name." ".trim(str_replace("-","",str_replace($name,"",$info->description))).($info->nonbillable && !strpos(strtolower($info->description),"unbillable") ? " (non-billable)" : "" )."</a><br>";
								}
							}
						?>
				</div>
			</fieldset>
			<fieldset>
				<legend>Dormant Codes in this Group</legend>
				<div class="fifteen columns">
						<?php 
							ksort($CodesWithGroup);
							foreach ($CodesWithGroup as $name=>$info){
								if ($info->dormant){
									echo "<span class=\"NonBillable".$info->nonbillable."\">".$name." ".trim(str_replace("-","",str_replace($name,"",$info->description))).($info->nonbillable && !strpos(strtolower($info->description),"unbillable") ? " (non-billable)" : "" )."</span><br>";
								}
							}
						?>
				</div>
			</fieldset>
<?php }//end if SelectedGroupsID ?>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-groups&addGroup=true" class="button-link">+ Add New Group</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
	

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
	 var formElement = $("#GroupUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-group-add").click(function(e){

			$('#groupResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				//console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#groupResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						var displayOrderValue = parseInt($('#displayOrderId').val());
						$("#name").val('');	
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#groupResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedGroupsID){?>
		$("#save-group-update").click(function(e){

			$('#groupResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						console.log(data);
						$('#groupResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#groupResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
	<?php }//end if SelectedGroupsID ?>
});
</script>
<script type="text/javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/colorpicker.js"></script>
<script>
$(function(){
	$("#colorPickerBackground").spectrum({
		color: "<?php echo ($GroupByID[$SelectedGroupsID]->colorBackground ? $GroupByID[$SelectedGroupsID]->colorBackground : "#fff");?>",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		preferredFormat: "hex",
		localStorageKey: "spectrum.demo",
		hideAfterPaletteSelect:true,
		change: function(color) {
			$("#colorBackground").val(color.toHexString());
			$(".codeInGroup").css("background-color",color.toHexString());
		},
		palette: [
			["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
			["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
			["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
			["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
			["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
			["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
			["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
			["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]		
		]
	});
	$("#colorPickerBorder").spectrum({
		color: "<?php echo ($GroupByID[$SelectedGroupsID]->colorBorder ? $GroupByID[$SelectedGroupsID]->colorBorder : "#fff");?>",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		preferredFormat: "hex",
		localStorageKey: "spectrum.demo",
		hideAfterPaletteSelect:true,
		change: function(color) {
			$("#colorBorder").val(color.toHexString());
			$(".codeInGroup").css("border-color",color.toHexString());
			$(".codeGroup").css("border-color",color.toHexString());
			$(".codeGroup").css("background-color",color.toHexString());
		},
		palette: [
			["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
			["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
			["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
			["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
			["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
			["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
			["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
			["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]		
		]
	});
});
</script>