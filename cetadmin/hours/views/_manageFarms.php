<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");

$gbsProvider = new HoursProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getFarms($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$FarmsByID[$record->id] = $record;
	$FarmsByName[$record->name]=$record;
}
ksort($FarmsByName);
//resort the products without display order by FarmID
$SelectedFarmsID = $_GET['FarmID'];
$SelectedFarmName = $_GET['FarmName'];
if ($SelectedFarmName && !$SelectedFarmsID){
	$SelectedFarmsID = $FarmsByName[$SelectedFarmName]->id;
}
$addFarm = $_GET['addFarm'];
?>
<?php if (!$addFarm){?>
	<br>
	<div class="fifteen columns">
		<?php include('_FarmsComboBox.php');?>
	</div>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addFarm?>
<?php if ($SelectedFarmsID || $addFarm){?>
	<style>
		.NonBillable1 {font-size:10pt;}
	</style>
	<form id="FarmUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Farm Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<?php if (!$addFarm){?><input type="text" name="id" value="<?php echo $FarmsByID[$SelectedFarmsID]->id;?>"><?php }?>
				</div>
				<div class="fourteen columns">
					<div class="two columns">Short Name:</div><div class="ten columns">
						<input type="text" id="name" name="name" style="width:100%;" value="<?php echo $FarmsByID[$SelectedFarmsID]->name;?>">
					</div>
					<br>
					<div class="two columns">Description:</div><div class="ten columns">
						<input type="text" id="description" name="description" style="width:100%;" value="<?php echo $FarmsByID[$SelectedFarmsID]->description;?>">
					</div>
					<br>
					<div class="two columns">Town:</div><div class="ten columns">
						<input type="text" id="town" name="town" style="width:100%;" value="<?php echo $FarmsByID[$SelectedFarmsID]->town;?>">
					</div>
					<br>
					<div class="two columns">Status:</div><div class="ten columns">
						<select name="status">
							<option value="New Farm"<?php echo ($FarmsByID[$SelectedFarmsID]->status == "New Farm" ? " selected" : "");?>>New Farm</option>
							<option value="Active Farm"<?php echo ($FarmsByID[$SelectedFarmsID]->status == "Active Farm" ? " selected" : "");?>>Active Farm</option>
							<option value="Not Active"<?php echo ($FarmsByID[$SelectedFarmsID]->status == "Not Active" ? " selected" : "");?>>Not Active</option>
						</select>
					</div>
					
				</div>
			</div>
		</fieldset>
		<div id="farmResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-farms&FarmID=<?php echo $SelectedFarmsID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addFarm){?>
					<a href="save-farm-add" id="save-farm-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="farm_add">
				<?php }else{?>
					<a href="save-farm-update" id="save-farm-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="farm_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>gbs/?nav=manage-farms&addFarm=true" class="button-link">+ Add New Farm</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
	

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
	 var formElement = $("#FarmUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-farm-add").click(function(e){

			$('#farmResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#farmResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						var displayOrderValue = parseInt($('#displayOrderId').val());
						$("#name").val('');	
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#farmResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedFarmsID){?>
		$("#save-farm-update").click(function(e){

			$('#farmResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#farmResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#farmResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
	<?php }//end if SelectedFarmsID ?>
});
</script>