<div class="four columns" style="position;absolute;margin-right:-10px;">
			<table class="reportFixedColumns display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th colspan="2" style="background-color:white;">Staff</th>
					</tr>
					<tr>
						<th class="StaffName CodeDataHeader">Last Name</th>
						<th class="StaffName CodeDataHeader">First Name</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="StaffName">&nbsp;</th>
						<td class="StaffName">Totals</th>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\">".$LastName."</td>";
							echo "<td class=\"StaffName\">".$FirstName."</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		</div>
		<style>
			th {min-width:70px;width:100px;border:1pt solid black;}
			#ToolTables_DataTables_Table_1_0 {visibility:hidden;}
			th .CodeDataHeader {width:111px;}
		</style>
		<div class="nineteen columns">
			<table class="reportResults display" cellspacing="0" width="100%">
				<thead>
						<?php 
							$headerRowData[] = '<tr>
								<th class="CodeDataHeader" style="display:none;">&nbsp;</th>
								<th class="CodeDataHeader" style="display:none;">Totals</th>';
							$headerRowDataTop[] = '<tr>';
							$codeColumns = 0;

							
								$lastGroupId = 0;
								$dataColumnNumber = 2;
								$firstDataColumnNumberForThisGroup = 2;
								foreach ($CodesDataByGroupId as $groupIdOrder=>$codeInfo){
									ksort($codeInfo);
									foreach ($codeInfo as $codeName=>$codeId){
										$groupId = $CodesByName[$codeName]->groupId;
										$isUnbillableCode = $CodesByID[$codeId]->nonbillable;
										$colorBackground = $GroupsByID[$groupId]->colorBackground;
										$colorBorder = $GroupsByID[$groupId]->colorBorder;
										if ($groupIdOrder == "ZZZNotInvoicedGroupId"){
											$colorBackground = $GroupsByID[$groupIdOrder]->colorBackground;
											$colorBorder = $GroupsByID[$groupIdOrder]->colorBorder;
										}
										$colorBackgroundStyle = ($colorBackground ? "background-color:".$colorBackground.";" : "");
										$colorBorderStyle = ($colorBorder ? "border:1pt solid ".$colorBorder.";" : "");
										$style = "";
										$textStyle = "";
										$isExcludedFromProjections = $CodesByID[$codeId]->excludeFromProjections;
										if ($isExcludedFromProjections || $isUnbillableCode){
											$textStyle = "color:".$colorBorder.";font-style:italic;";
										}
										if ($colorBackgroundStyle || $colorBorderStyle){$style = " style='".$colorBorderStyle.$colorBackgroundStyle.$textStyle."'";}
										if ($lastColorBorder){
											if ($lastGroupId > 0 && $lastGroupId != $groupId && $lastColorBorder != $colorBorder){
												$invoiceId = $GroupsByID[$lastGroupId]->invoiceId;
												$budgetAmount = $invoiceAmountHistoryByInvoiceId[$invoiceId]["budget"];
												$remaining = 0;
												if ($budgetAmount > 0){
													$used = $invoiceAmountHistoryByInvoiceId[$invoiceId]["used"];
													$remaining = bcsub($budgetAmount,$used,2);
													$negative ="<span>";
													if ($remaining < 0){$negative = "<span style='color:red';>-";$remaining = ($remaining*(-1));}
													$budgetDisplay = "<div style='width:80%;text-align:right;font-size:8pt;padding-right:0px;'>
														<span title='Entire Budget $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetComplete"])." less NonCode Related Budget $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetNonCodeRelated"])."'>$".money($budgetAmount)."</span>
														<br><span title='Invoice Total $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["usedComplete"])." less NonCode Related $".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["nonStaffAmount"])."'>- $".money($used)."&nbsp;</span>
														<hr style='border:0pt solid black;background-color:black;height:1px;'>
														<span title='Remaining Amount for Staff Use'>$negative $".money($remaining)."&nbsp;</span></span>
														</div>
														<div style='position:absolute;bottom:0px;font-size:8pt;'>
															<span title='FY Budget from Code Budgets'>$".money($CodesBudgetByGroupID[$lastGroupId])."</span><br>
															<span title='staff hours invoiced amount for this time period. Included reallocation adjusted amount of $".money($CodesDataAdjustmentsByGroupThisTimePeriod[$lastGroupId])."'>-$".money($CodesDataTotalsBillingByGroupThisTimePeriod[$lastGroupId])."</span>
														</div>
														
														";
												}else{
													$budgetDisplay = "<br><br><span style='font-size:8pt;'>Budget Not Indicated</span>";
												}
												$fiscalYearDisplay = "";
												$fiscalYearStart = MySQLDate($invoicingListById[$invoiceId]->fiscalYearStart);
												$fiscalYearEnd = MySQLDate($invoicingListById[$invoiceId]->fiscalYearEnd);
												$monthsLeft = 0;
												if ($fiscalYearStart){
													if (!$fiscalYearEnd){
														$fiscalYearEnd = date("m/d/Y",strtotime($fiscalYearStart." + 364 days"));
													}
													$lastInvoicedDate = date("m/d/Y",$invoiceAmountHistoryByInvoiceId[$invoiceId]["lastInvoiceDate"]);
													$monthsLeft = datediff($lastInvoicedDate,$fiscalYearEnd,"month");
													$fiscalYearDisplay = "<br><span style='font-size:8pt;'>".$fiscalYearStart." to ".$fiscalYearEnd."<br>".$monthsLeft." months left</span>";
												}
												$remainingBudgetByGroupId[$lastGroupId]["originalBudget"] = $budgetAmount;
												$remainingBudgetByGroupId[$lastGroupId]["remainingBudget"] = $remaining;
												$remainingBudgetByGroupId[$lastGroupId]["remainingMonths"] = $monthsLeft;
												$remainingBudgetByGroupId[$lastGroupId]["budgetPerMonth"] = bcdiv($remaining,$monthsLeft,2);
												$remainingBudgetByGroupId[$lastGroupId]["fiscalYearDisplay"] = $fiscalYearDisplay;
												
												$headerRowData[] = "<th class=\"CodeDataHeader CodeDataHeaderGroup toggleGroupColumns\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";' data-columns='".$firstDataColumnNumberForThisGroup."_".$lastDataColumnNumber."' title='".($CondenseGroup ? "Show" : "Collapse")." Group Codes' data-groupname='".$GroupsByID[$lastGroupId]->name."'>".$GroupsByID[$lastGroupId]->name.$toggleButton.$fiscalYearDisplay."</th>";
												$columnsToHideByGroupId[$lastGroupId][]=$dataColumnNumber;
												$columnsSummaryToHideByGroupId[$lastGroupId][]=$dataColumnNumber;
												$dataColumnNumber++;
												$headerRowData[] = "<th class=\"CodeDataHeader\" style='position:relative;border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";' data-columns='".$firstDataColumnNumberForThisGroup."_".$lastDataColumnNumber."'>Billing".$budgetDisplay."</th>";
												$columnsToHideByGroupId[$lastGroupId][]=$dataColumnNumber;
												$columnsSummaryToHideByGroupId[$lastGroupId][]=$dataColumnNumber;
												$dataColumnNumber++;
												
												$footerRowData1[] = "<td class=\"CodeDataFooter\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";border-top:2pt solid black;'>".$CodesDataTotalsByGroupId[$lastGroupId]."</td>";
												$footerRowData1[] = "<td class=\"CodeDataFooter\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";border-top:2pt solid black;'>$".money($invoiceAmountHistoryByInvoiceId[$invoiceId]["used"])."</td>";
												
												$footerRowData2[] = "<td class=\"CodeDataFooter\" style='height:120px;border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'>".$GroupsByID[$lastGroupId]->name.$fiscalYearDisplay."</th>";
												$footerRowData2[] = "<td class=\"CodeDataFooter\" style='height:120px;border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'><span title='Actual Hours billing prior to reallocation of hours'>Billing $".money(bcsub($CodesDataTotalsBillingByGroupThisTimePeriod[$lastGroupId],$CodesDataAdjustmentsByGroupThisTimePeriod[$lastGroupId],2))."</span>".($CodesDataAdjustmentsByGroupThisTimePeriod[$lastGroupId] ? "<span title='Adjusted Amount based on reallocation of hours' style='font-size:8pt;".($CodesDataAdjustmentsByGroupThisTimePeriod[$lastGroupId] < 0 ? "color:red;" : "")."'><br>Reallocation Adjustment Amount: $".money($CodesDataAdjustmentsByGroupThisTimePeriod[$lastGroupId])."</span>" : "")."</th>";

												//echo "<hr>".$GroupsByID[$lastGroupId]->name." ".$GroupsByID[$lastGroupId]->colorBorder." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
												$firstDataColumnNumberForThisGroup = $dataColumnNumber;
												$codeColumns++;
												$codeColumns++;
											}
										}else{
											if ($lastGroupId != $groupId){
												//echo $GroupsByID[$lastGroupId]->name." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
											}
										}
										$thisGroupIdTotal[$groupId] = $thisGroupIdTotal[$groupId]+$CodesDataTotals[$codeId];
										$description = str_replace($codeName."-","",$CodesByName[$codeName]->description);
										$description = str_replace($codeName." -","",$description);
										$description = str_replace($codeName,"",$description);
										$description = trim($description);
										
										$descriptionWrap = wordwrap($description, 15, "<Br>", true);
										$descriptionWrapParts = explode("<Br>",$descriptionWrap);
										if (count($descriptionWrapParts)>3){
											$descriptionWrap = "<span title='".$description."'>".$descriptionWrapParts[0]."<Br>".$descriptionWrapParts[1]."<br>".$descriptionWrapParts[2]."...</span>";
										}
										
										
										$CondenseGroupOverRide = true;
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){
											$CondenseGroupOverRide = false;
											$columnsToHideByGroupId["ZZZNotInvoicedGroupId"][]=$dataColumnNumber;
										}else{
											$columnsToHideByGroupId[$groupId][]=$dataColumnNumber;
										}
										if ($CondenseGroup && $CondenseGroupOverRide){
											$columnsToHide[]=$dataColumnNumber;
										}

										$headerRowData[] = "<th class=\"CodeDataHeader\"".$style.($isExcludedFromProjections ? " title='Excluded From Projections'" : "").">".$codeName."<br><span style='font-size:8pt;'>".$descriptionWrap."</span>
											".(!$isExcludedFromProjections && !$isUnbillableCode ? "<div style='font-size:8pt;position:absolute;margin-bottom:1px;bottom:0px;'>
												".($CodesByID[$codeId]->budget > 1 ? "<span title='FY budgeted amount'>$".money($CodesByID[$codeId]->budget)."</span>" : "<span style='color:red;'>No Budget</span>")."<br><span title='staff hours invoiced amount for this time period".($CodesDataAdjustmentsThisTimePeriod[$codeId] ? " included reallocation adjusted amount of $".money($CodesDataAdjustmentsThisTimePeriod[$codeId])."' style='font-style:italic;" : "")."'>-$".money($CodesDataTotalsBillingThisTimePeriod[$codeId])."</span>
											</div>" : "")."
										</th>";
										$footerRowData1[] = "<td class=\"CodeDataFooter\"".str_replace(";'","border-top:2pt solid black;'",$style).">".$CodesDataTotals[$codeId]."</td>";
										$footerRowData2[] = "<td class=\"CodeDataFooter\"".$style.">".$codeName."</td>";
										//echo $codeName.":".$colorBackground." ".$colorBorder." ".$CodesDataTotals[$codeId]." ".$CodesDataTotalsByGroupId[$groupId]."<br>";
										$lastGroupId = $groupId;
										$lastColorBorder = $colorBorder;
										$lastColorBackground = $colorBackground;
										$lastDataColumnNumber = $dataColumnNumber;
										$dataColumnNumber++;
										$codeColumns++;
									}
								}
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="'.$codeColumns.'">Codes</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="3">Billable</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="6">Other Hours</th>';
							$headerRowDataTop[] = '<th class="CodeDataHeaderTop" colspan="7">Summary</th></tr>';
								
							$headerRowData[] = '
								<th class="CodeDataHeader subHours">Billable Per Week</th>
								<th class="CodeDataHeader subHours">Billable Per Day</th>
								<th class="CodeDataHeader totalHoursComplete">Total Billable Hours</th>
								<th class="CodeDataHeader subHours">Unbillable Per Week</th>
								<th class="CodeDataHeader subHours">Unbillable Per Day</th>
								<th class="CodeDataHeader totalHoursComplete" title="Includes 501">Total Unbillable</th>
								<th class="CodeDataHeader subHours">501 Per Week</th>
								<th class="CodeDataHeader subHours">501 Per Day</th>
								<th class="CodeDataHeader totalCodeHours">501 Hrs<Br><span style="font-size:8pt;">(non-work hours)</span></th>
								<th class="CodeDataHeader totalHoursComplete" title="Total Billable + Total Unbillable - 501">Total Work Hours</th>
								<th class="CodeDataHeader subHours">All Code Per Week</th>
								<th class="CodeDataHeader subHours">All Code Per Day</th>
								<th class="CodeDataHeader totalCodeHours">All Code Hours</th>
								<th class="CodeDataHeader" title="Hours remaining from '.$numberOfHoursPerDayLimit.' per day">Hours Left To Bill/Day</th>
								<th class="CodeDataHeader">%Billable Hrs</th>
								<th class="CodeDataHeader">%Unbillable Hrs</th>
							</tr>';
							foreach ($headerRowDataTop as $headerRowColumn){
								echo $headerRowColumn;
							}
							foreach ($headerRowData as $headerRowColumn){
								echo $headerRowColumn;
							}
				?>
				</thead>
				<tfoot>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							foreach ($footerRowData1 as $footerRowColumn){
								echo $footerRowColumn;
							}

							$BillableFooterActual = $TotalAggregateData["totalBillable"];
							$BillablePerWeekFooterActual = round(($BillableFooterActual/$numberOfWeeks),2);
							$BillablePerDayFooterActual = round(($BillableFooterActual/$numberOfDays),2);
							$HoursRemainingFooterActual = bcsub(($numberOfHoursPerDayLimit*$numberOfDays),$BillablePerDayFooterActual,2);
							$UnbillableFooterActual = ($TotalAggregateData["totalUnBillable"]-$TotalAggregateData["501Hours"]);
							$UnbillablePerWeekFooterActual = round(($UnbillableFooterActual/$numberOfWeeks),2);
							$UnbillablePerDayFooterActual = round(($UnbillableFooterActual/$numberOfDays),2);
							$Hour501FooterActual = $TotalAggregateData["501Hours"];
							$Hour501PerWeekFooterActual = round(($Hour501FooterActual/$numberOfWeeks),2);
							$Hour501PerDayFooterActual = round(($Hour501FooterActual/$numberOfDays),2);
							$TotalHoursWorkedFooterActual = bcadd($BillableFooterActual,$UnbillableFooterActual,2);
							$TotalCodeHoursFooterActual = bcadd($TotalHoursWorkedFooterActual,$Hour501FooterActual,2);
							$TotalCodePerWeekFooterActual = round(($TotalCodeHoursFooterActual/$numberOfWeeks),2);
							$TotalCodePerDayFooterActual = round(($TotalCodeHoursFooterActual/$numberOfDays),2);

							//For projections incorporate the 501 hours because they will consider this as time used
							$TotalWorkPerDayFooterActual = bcadd($BillablePerDayFooterActual,$UnbillablePerDayFooterActual,2);
							$TotalHoursPerDayFooterActual = bcadd($TotalWorkPerDayFooterActual,$Hour501PerDayFooterActual,2);
							$HoursRemainingFooterActual = bcsub(($numberOfHoursPerDayLimit*(count($EmployeeData))),$TotalHoursPerDayFooterActual,2);
							
							$PercentBilledFooterActual = bcdiv($BillableFooterActual,$TotalCodeHoursFooterActual,4);
							$PercentUnBillableFooterActual = bcdiv($UnbillableFooterActual,$TotalCodeHoursFooterActual,4);
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$BillablePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$BillablePerDayFooterActual."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator\">".$BillableFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$UnbillablePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$UnbillablePerDayFooterActual."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator\">".$UnbillableFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$Hour501PerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$Hour501PerDayFooterActual."</td>";
							echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator\">".$Hour501FooterActual."</td>";
							echo "<td class=\"CodeDataFooter totalHoursComplete footerSeparator\">".$TotalHoursWorkedFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$TotalCodePerWeekFooterActual."</td>";
							echo "<td class=\"CodeDataFooter subHours footerSeparator\">".$TotalCodePerDayFooterActual."</td>";
							echo "<td class=\"CodeDataFooter totalCodeHours footerSeparator\">".$TotalCodeHoursFooterActual."</td>";
							echo "<td class=\"CodeDataFooter footerSeparator\">".$HoursRemainingFooterActual."</td>";
							echo "<td class=\"CodeDataFooter footerSeparator\">".bcmul($PercentBilledFooterActual,100,2)."%</td>";
							echo "<td class=\"CodeDataFooter footerSeparator\">".bcmul($PercentUnBillableFooterActual,100,2)."%</td>";
							
						?>
					</tr>
					<tr>
						<td style="display:none;">&nbsp;</td>
						<td style="display:none;">Totals</td>
						<?php 
							foreach ($footerRowData2 as $footerRowColumn){
								echo $footerRowColumn;
							}
						?>
							<td class="subHours" style='height:120px;'>Billable Per Week</td>
							<td class="subHours" style='height:120px;'>Billable Per Day</td>
							<td class="totalHoursComplete" style='height:120px;'>Total</td>
							<td class="subHours" style='height:120px;'>Unbillable Per Week</td>
							<td class="subHours" style='height:120px;'>Unbillable Per Day</td>
							<td class="totalHoursComplete" title="Includes 501">Total Unbillable</td>
							<td class="subHours" style='height:120px;'>501 Per Week</td>
							<td class="subHours" style='height:120px;'>501 Per Day</td>
							<td class="totalCodeHours" >501 Hrs</td>
							<td class="totalHoursComplete" title="Total Billable + Total Unbillable - 501">Total Wrk_Hrs</td>
							<td class="subHours" style='height:120px;'>All Code Per Week</td>
							<td class="subHours" style='height:120px;'>All Code Per Day</td>
							<td class="totalCodeHours" style='height:120px;'>All Code Hours</td>
							<td title="Hours remaining from <?php echo $numberOfHoursPerDayLimit;?> per day">Hours Left To Bill/Day</td>
							<td>%Billable Hrs</td>
							<td>%Unbillable Hrs</td>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							$bodyRowData[] = "<tr>".
								"<td style=\"display:none;\"><span style='display:none;'>".$LastName."</span>&nbsp;</td>".
								"<td style=\"display:none;\"><span style='display:none;'>".$FirstName."</span>Totals</td>";
								$lastGroupId = 0;
								$lastColorBorder = "";
								$lastColorBackground = "";
								$lastBillingRateId = 0;
								
								foreach ($CodesDataByGroupId as $groupIdOrder=>$codeInfo){
									ksort($codeInfo);
									foreach ($codeInfo as $codeName=>$codeId){
										$groupId = $CodesByName[$codeName]->groupId;
										$billingRateId = $CodesByName[$codeName]->billingRateId;
										$colorBackground = $GroupsByID[$groupId]->colorBackground;
										$colorBorder = $GroupsByID[$groupId]->colorBorder;
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){
											$colorBackground = $GroupsByID[$groupIdOrder]->colorBackground;
											$colorBorder = $GroupsByID[$groupIdOrder]->colorBorder;
										}
										$colorBackgroundStyle = ($colorBackground ? "background-color:".$colorBackground.";" : "");
										$colorBorderStyle = ($colorBorder ? "border:1pt solid ".$colorBorder.";" : "");
										$style = "";
										$textStyle = "";
										$isExcludedFromProjections = $CodesByID[$codeId]->excludeFromProjections;
										$isUnbillableCode = $CodesByID[$codeId]->nonbillable;
										if ($isExcludedFromProjections || $isUnbillableCode){
											$textStyle = "color:".$colorBorder.";font-style:italic;";
										}
										if ($colorBackgroundStyle || $colorBorderStyle){$style = " style='".$colorBorderStyle.$colorBackgroundStyle.$textStyle."'";}
										if ($lastColorBorder){
											if ($lastGroupId > 0 && $lastGroupId != $groupId && $lastColorBorder != $colorBorder){
												$billingRate = $BillingRateByEmployee[$EmployeeLastFirstName][$lastBillingRateId];

												$bodyRowData[] = "<td align=\"center\" class=\"hourData\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'>".$EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalHours"].($EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalHours"] ? " <div style='font-size:8pt;position:relative;float:right;padding-top:2px;'>x$".(!strpos($EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["billingRate"],"_") ? money($EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["billingRate"]) : "<span style='color:red'>missing</span>")."</div>" : "")."</td>";
												$bodyRowData[] = "<td align=\"center\" class=\"hourData\" style='border:1pt solid ".$lastColorBackground.";background-color:".$lastColorBorder.";'>".($EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalBilling"] ? "$".$EmployeeDataByGroupId[$lastGroupId][$EmployeeLastFirstName]["totalBilling"] : "")."</td>";
											}
										}else{
											if ($lastGroupId != $groupId){
												//echo $GroupsByID[$lastGroupId]->name." ".$CodesDataTotalsByGroupId[$lastGroupId]."<hr>";
											}
										}
										
										$CondenseGroupOverRide = true;
										if ($groupIdOrder == "AAAZZZNotInvoicedGroupId"){$CondenseGroupOverRide = false;}
										$bodyRowData[] = "<td align=\"center\" class=\"hourData\"".$style.">".$HoursData[$codeId]["totalHours"]."</td>";
										$lastGroupId = $groupId;
										$lastColorBorder = $colorBorder;
										$lastColorBackground = $colorBackground;
										$lastBillingRateId = $billingRateId;
									}
								}

							$BillableActual[$EmployeeLastFirstName] = $EmployeeAggregateData[$EmployeeLastFirstName]["totalBillable"];
							$BillablePerWeekActual[$EmployeeLastFirstName] = round(($BillableActual[$EmployeeLastFirstName]/$numberOfWeeks),2);
							$BillablePerDayActual[$EmployeeLastFirstName] = round(($BillableActual[$EmployeeLastFirstName]/$numberOfDays),2);
							$HoursRemainingActual[$EmployeeLastFirstName] = bcsub($numberOfHoursPerDayLimit,$BillablePerDayActual[$EmployeeLastFirstName],2);
							$UnbillableActual[$EmployeeLastFirstName] = ($EmployeeAggregateData[$EmployeeLastFirstName]["totalUnBillable"]-$EmployeeAggregateData[$EmployeeLastFirstName]["501Hours"]);
							$UnbillablePerWeekActual[$EmployeeLastFirstName] = round(($UnbillableActual[$EmployeeLastFirstName]/$numberOfWeeks),2);
							$UnbillablePerDayActual[$EmployeeLastFirstName] = round(($UnbillableActual[$EmployeeLastFirstName]/$numberOfDays),2);
							$Hour501Actual[$EmployeeLastFirstName] = $EmployeeAggregateData[$EmployeeLastFirstName]["501Hours"];
							$Hour501PerWeekActual[$EmployeeLastFirstName] = round(($Hour501Actual[$EmployeeLastFirstName]/$numberOfWeeks),2);
							$Hour501PerDayActual[$EmployeeLastFirstName] = round(($Hour501Actual[$EmployeeLastFirstName]/$numberOfDays),2);
							$TotalHoursWorkedActual[$EmployeeLastFirstName] = bcadd($BillableActual[$EmployeeLastFirstName],$UnbillableActual[$EmployeeLastFirstName],2);
							$TotalCodeHoursActual[$EmployeeLastFirstName] = bcadd($TotalHoursWorkedActual[$EmployeeLastFirstName],$Hour501Actual[$EmployeeLastFirstName],2);
							$TotalCodePerWeekActual[$EmployeeLastFirstName] = round(($TotalCodeHoursActual[$EmployeeLastFirstName]/$numberOfWeeks),2);
							$TotalCodePerDayActual[$EmployeeLastFirstName] = round(($TotalCodeHoursActual[$EmployeeLastFirstName]/$numberOfDays),2);

							//For projections incorporate the 501 hours because they will consider this as time used
							$TotalWorkPerDayActual[$EmployeeLastFirstName] = bcadd($BillablePerDayActual[$EmployeeLastFirstName],$UnbillablePerDayActual[$EmployeeLastFirstName],2);
							$TotalHoursPerDayActual[$EmployeeLastFirstName] = bcadd($TotalWorkPerDayActual[$EmployeeLastFirstName],$Hour501PerDayActual[$EmployeeLastFirstName],2);
							$HoursRemainingActual[$EmployeeLastFirstName] = bcsub($numberOfHoursPerDayLimit,$TotalHoursPerDayActual[$EmployeeLastFirstName],2);
							$TotalDaysWorkedActual[$EmployeeLastFirstName] = ceil(bcdiv($TotalHoursWorkedActual[$EmployeeLastFirstName],5,2));
							$PercentBilledActual[$EmployeeLastFirstName] = bcdiv($BillableActual[$EmployeeLastFirstName],$TotalCodeHoursActual[$EmployeeLastFirstName],4);
							$PercentUnBillableActual[$EmployeeLastFirstName] = bcdiv($UnbillableActual[$EmployeeLastFirstName],$TotalCodeHoursActual[$EmployeeLastFirstName],4);
							$bodyRowData[] = 
								"<td class='hourSummary subHours'".($BillablePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$BillablePerWeekActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'>".$BillablePerDayActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete'>".$BillableActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'".($UnbillablePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$UnbillablePerWeekActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'>".$UnbillablePerDayActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete'>".$UnbillableActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'".($Hour501PerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$Hour501PerWeekActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'>".$Hour501PerDayActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalCodeHours'>".$Hour501Actual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalHoursComplete'>".$TotalHoursWorkedActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'".($TotalCodePerWeekActual[$EmployeeLastFirstName] > $numberOfHoursPerWeekLimit ? " style='color:red;'" : "").">".$TotalCodePerWeekActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary subHours'>".$TotalCodePerDayActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary totalCodeHours'>".$TotalCodeHoursActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary'>".$HoursRemainingActual[$EmployeeLastFirstName]."</td>".
								"<td class='hourSummary'>".bcmul($PercentBilledActual[$EmployeeLastFirstName],100,2)."%</td>".
								"<td class='hourSummary'>".bcmul($PercentUnBillableActual[$EmployeeLastFirstName],100,2)."%</td>".
								"</tr>";
						}
						foreach ($bodyRowData as $bodyRow){
							echo $bodyRow;
						}
					?>
				</tbody>
			</table>
		</div>