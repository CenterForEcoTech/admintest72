<link rel="stylesheet" href="<?php echo $CurrentServer.$adminFolder;?>css/jquery.fileupload-ui.css">
<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");

$gbsProvider = new GBSProvider($dataConn);
$criteria = new stdClass();
$paginationResult = $gbsProvider->invoiceTool_getBillingRates($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$BillingRateByID[$record->GBSBillingRate_ID] = $record;
	$BillingRateByName[$record->GBSBillingRate_Name] = $record;
	$BillingRateByInvoiceName[$record->GBSBillingRate_InvoiceName][$record->GBSBillingRate_Name] = $record;
	$BillingRateByInvoiceDisplayName[$record->GBSBillingRate_InvoiceDisplayName] = $record->GBSBillingRate_InvoiceName;
}
$employeeListCount = 0;
foreach($BillingRateByInvoiceName as $InvoiceName=>$RateByName){
	$BillingRateDisplay .= "<div class='InvoiceName' id='".$InvoiceName."' style='display:none;'><span class='InvoiceNameSpan'>".$InvoiceName."</span><br>";
	foreach ($RateByName as $Name=>$record){
	//	echo $record->GBSBillingRate_Name." ".money($record->GBSBillingRate_Rate)." ".$record->GBSBillingRate_EmployeeMembers."<Br>";
		$thisID = $record->GBSBillingRate_ID;
		$thisInvoiceName = $record->GBSBillingRate_InvoiceName;
		$thisName = $record->GBSBillingRate_Name;
		$thisRate = $record->GBSBillingRate_Rate;
		$thisEmployeeMembers = $record->GBSBillingRate_EmployeeMembers;
		$thisEmployeeMembersDisplay = explode(",",$thisEmployeeMembers);
		$thisEmployeeMemberDisplay = "";
		foreach ($thisEmployeeMembersDisplay as $Employee){
			$thisEmployeeMemberDisplay = $thisEmployeeMemberDisplay."<span id='employeeList".$employeeListCount."'><img src='".$CurrentServer."images/redx-small.png' style='height:10px;cursor:pointer;' class='removeEmployee' data-billingrateid='".$thisID."' data-employeename='".trim($Employee)."' data-name='".$thisName."' data-employeelistid='employeeList".$employeeListCount."'> ".str_replace("_",", ",trim($Employee))."<br></span>\r\n";
			$employeeListCount++;
		}
		$addEmployeeButton = "<button class='btn btn-success addEmployee' data-billingrateid='".$thisID."' data-name='".$thisName."' data-invoicegroup='".$thisInvoiceName."'>Add Employee</button>";
		$BillingRateDisplay .= "<div class='row template-upload'>
				<div class='four columns'>Rate Category:<textarea id='Name".$thisID."' style='width:200px;height:50px;'>".$thisName."</textarea></div>
				<div class='one columns'>Rate:<input type='text' id='Rate".$thisID."' value='".$thisRate."' style='width:50px;'></div>
				<div class='three columns'>Employees:<div id='employeeNameList".$thisID."'>".$thisEmployeeMemberDisplay.$addEmployeeButton."</div></div>
				<div class='three columns'><br><a class='changeBillingRate button-link' data-billingrateid='".$thisID."'>Change</a><span style='display:none' id='Alert".$thisID."'></span></div>
			</div><hr>";
		
	}
	$BillingRateDisplay .= "</div>";	
}

include_once($dbProviderFolder."HREmployeeProvider.php");

$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
$employeeComboBoxHideLabel = true;
$employeeComboBoxFunction = 'var thisNewName = $(ui.item).text(),
	thisNewNameParts = thisNewName.split(" "),
	thisNewName = thisNewNameParts[1]+"_"+thisNewNameParts[0],
	thisNewNameDisplay = thisNewNameParts[1]+", "+thisNewNameParts[0],
	thisAlert = $("#NewEmployeeAlert");
	$("#NewEmployeeName").val(thisNewName);
	var NewEmployeeNameBillingRateID = $("#NewEmployeeNameBillingRateID").val(),
		NewInvoiceGroupName = $("#NewInvoiceGroupName").val(),
		thisEmployeeNameListID = "#employeeNameList"+NewEmployeeNameBillingRateID,
		thisEmployeeNameListHTML = $(thisEmployeeNameListID).html();
		data = {};
	
	data["action"] = "BillingRate_AddEmployee";
	data["employeeName"] = thisNewName;
	data["id"] = NewEmployeeNameBillingRateID;
	data["invoiceName"] = NewInvoiceGroupName;
	$.ajax({
		url: "ApiInvoicingManagement.php",
		type: "PUT",
		data: JSON.stringify(data),
		success: function(data){
			$(thisAlert).show().html("Success").addClass("alert").addClass("info").fadeOut(5000);
			$(thisEmployeeNameListID).html("<span class=\"alert info\">Added: "+thisNewNameParts[1]+", "+thisNewNameParts[0]+"</span><br>"+thisEmployeeNameListHTML);
		},
		error: function(jqXHR, textStatus, errorThrown){
			var message = $.parseJSON(jqXHR.responseText);
			$(thisAlert).show().html(message).addClass("alert");
		}
	});

';
?>
<div class="twelve columns" id='selectEmployeeForBillingRate' style='display:none;'>Select the Employee to Add to <span id='billingRateName'></span><br><?php include('../hr/_EmployeeComboBox.php');?>
<input id='NewInvoiceGroupName' style='display:none;'>
<input id='NewEmployeeNameBillingRateID' style='display:none;'>
<input id='NewEmployeeName' style='display:none;'>
<span id='NewEmployeeAlert'></span>
</div>
<br clear="all">
<style>
	.InvoiceName {border:1pt dashed red;}
	.InvoiceNameSpan {font-weight:bold;}
</style>

<?php
ksort($BillingRateByInvoiceDisplayName);
echo "<select id='invoiceNameSelector'><option value=''>Select Invoice Type</option>";
foreach ($BillingRateByInvoiceDisplayName as $invoiceDisplayName=>$invoiceName){
	echo "<option value='".$invoiceName."'>".$invoiceDisplayName."</option>";
}
echo "</select>";

echo $BillingRateDisplay;
?>
<br clear='all'>
Instructions:<br>
<ol>
	<li>1. Use the pulldown to select the Invoice Type you want to adjust billing rate information for
	<li>2. To add an employee to the rate category, click the green 'Add Employee' button for that rate category and then use the staff selector that will appear at the top of the page to choose the staff
	<li>3. To adjust the Rate Category name or Rate amount, adjust the field then click the 'Change' button
<ol>
<script type="text/javascript">
	$(function () {
		$("#invoiceNameSelector").on('change',function(){
			$(".InvoiceName").hide();
			var $this = $(this),
				invoiceName = "#"+$this.val();
			$(invoiceName).show();
		});
		$(".removeEmployee").on('click',function(){
			var $this = $(this),
				thisBillingRateID = $this.attr('data-billingrateid'),
				thisEmployeeName = $this.attr('data-employeename'),
				thisName = $this.attr('data-name'),
				thisEmployeeListID = "#"+$this.attr('data-employeelistid'),
				thisAlert = "#Alert"+thisBillingRateID;
			var thisconfirm = confirm("Are you Sure you want to remove "+thisEmployeeName.replace("_",", ")+" from "+thisName);
			if (thisconfirm){
				data = {};
				
				data["action"] = "BillingRate_RemoveEmployee";
				data["employeeName"] = thisEmployeeName;
				data["id"] = thisBillingRateID;
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisAlert).show().html("Success").addClass("alert").addClass("info").fadeOut(5000);
						$(thisEmployeeListID).hide();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
				
			}
			
		});
		
		$(".changeBillingRate").on('click',function(e){
			e.preventDefault();
			var $this = $(this),
				thisId = $this.attr('data-billingrateid'),
				thisNameId = "#Name"+thisId,
				thisRateId = "#Rate"+thisId,
				thisAlert = "#Alert"+thisId,
				thisNameValue = $(thisNameId).val(),
				thisRateValue = $(thisRateId).val(),
				data = {};
				
				data["action"] = "BillingRate_Update";
				data["name"] = thisNameValue;
				data["rate"] = thisRateValue;
				data["id"] = thisId;
				$.ajax({
					url: "ApiInvoicingManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisAlert).show().html("Success").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
		});
		$(".addEmployee").on('click',function(e){
			var $this = $(this),
				thisId = $this.attr('data-billingrateid'),
				thisName = $this.attr('data-name'),
				thisInvoiceGroup = $this.attr('data-invoicegroup');
				$("#selectEmployeeForBillingRate").show();
				$("#billingRateName").html(thisName);
				$('#NewEmployeeName').val('');
				$('#NewEmployeeNameBillingRateID').val(thisId);
				$("#NewInvoiceGroupName").val(thisInvoiceGroup);
				$('.custom-combobox-input').val('');
				$("#NewEmployeeAlert").hide();
		});
		
	});
</script>