<?php
$setTab = "Actual";

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

include_once($dbProviderFolder."InvoicingProvider.php");
$invoicingProvider = new InvoicingProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$results = $invoicingProvider->getInvoicingCodes();
$resultArray = $results->collection;
foreach ($resultArray as $result=>$record){
	$invoicingListById[$record->id] = $record;
	if ($record->invoicingNumberName){
		$invoicingListByInvoiceCode[$record->invoicingNumberName] = $record;
	}
}
//print_pre($invoicingListById);
foreach ($invoicingListByInvoiceCode as $invoiceCode=>$record){
	//print_pre($record);
	$fiscalYearStart = MySQLDate($record->fiscalYearStart);
	$fiscalYearEnd = $record->fiscalYearEnd;
	$invoiceId = $record->id;
	$budget = $record->budget;
	$budgetNonCodeRelated = $record->budgetNonCodeRelated;
	if ($fiscalYearStart){
		if (!MySQLDate($fiscalYearEnd)){ 	
			$fiscalYearEnd = date("Y-m-d",strtotime($fiscalYearStart." + 364 days"));
		}
		$InvoicingFiscalYearsByID[$invoiceId]["fiscalYearStart"] = $record->fiscalYearStart;
		$InvoicingFiscalYearsByID[$invoiceId]["fiscalYearEnd"] = $fiscalYearEnd;
	}else{
		$Adjustments["Alert"]["Fiscal Year Missing"][]= "Invoice Code: ".$invoiceCode." missing Fiscal Year Start information";
	}

	$criteria = new stdClass();
	$criteria->invoiceCode = $invoiceCode;
	$criteria->invoiceDateStart = $record->fiscalYearStart;
	$criteria->invoiceDateEnd = $fiscalYearEnd;
	//print_pre($criteria);
	
	//get historic invoice amounts to know what was billed
	$results = $invoicingProvider->getInvoicingHistory($criteria);
	foreach ($results as $record){
		$thisCode = $record["invoiceCode"];
		$record["budget"] = $budget;
		$record["budgetNonCodeRelated"] = $budgetNonCodeRelated;
		$record["invoiceId"] = $invoiceId;
		if ($invoicingHistoryByDate[$thisCode][strtotime($record["invoiceDate"])]){
			$timestamp = strtotime($record["timestamp"]);
			if ($timestamp > strtotime($invoicingHistoryByDate[$thisCode][strtotime($record["invoiceDate"])]["timestamp"])){
				$invoicingHistoryByDate[$thisCode][strtotime($record["invoiceDate"])]=$record; 
			}
		}else{
			$invoicingHistoryByDate[$thisCode][strtotime($record["invoiceDate"])]=$record; 
		}
	}
}
//print_pre($invoicingHistoryByDate);
foreach ($invoicingHistoryByDate as $invoiceCode=>$invoiceDateInfo){
	krsort($invoiceDateInfo);
	foreach ($invoiceDateInfo as $invoiceDateStr=>$record){	
		//print_pre($record);
		$invoiceId = $record["invoiceId"];
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetComplete"] = $record["budget"];
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetNonCodeRelated"] = $record["budgetNonCodeRelated"];
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["budget"] = bcsub($invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetComplete"],$invoiceAmountHistoryByInvoiceId[$invoiceId]["budgetNonCodeRelated"],2);
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["usedComplete"] = bcadd($invoiceAmountHistoryByInvoiceId[$invoiceId]["usedComplete"],$record["invoiceAmount"],2);
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["nonStaffAmount"] = bcadd($invoiceAmountHistoryByInvoiceId[$invoiceId]["nonStaffAmount"],$record["nonStaffAmount"],2);
		$invoiceAmountHistoryByInvoiceId[$invoiceId]["used"] = bcsub($invoiceAmountHistoryByInvoiceId[$invoiceId]["usedComplete"],$invoiceAmountHistoryByInvoiceId[$invoiceId]["nonStaffAmount"],2);
		if ($invoiceDateStr > $invoiceAmountHistoryByInvoiceId[$invoiceId]["lastInvoiceDate"]){
			$invoiceAmountHistoryByInvoiceId[$invoiceId]["lastInvoiceDate"] = $invoiceDateStr;
		}
		
	}
}
//print_pre($invoiceAmountHistoryByInvoiceId);


$gbsProvider = new HoursProvider($dataConn);
$hrEmployeeProvider = new HREmployeeProvider($dataConn);

$EmployeeID = ($_GET["EmployeeID"] ? $_GET["EmployeeID"] : false);
$SelectedGroupsID = $SelectedGroupsID?:($_GET["GroupID"] ? $_GET["GroupID"] : false);
$SelectedCodesID = ($_GET["CodeID"] ? $_GET["CodeID"] : false);
$Status = ($_GET["Status"] ? $_GET["Status"] : "Approved");
$HideCodes = ($_GET["HideCodes"] == "1" ? true : false);
$DisplayNonProjectedCodes = ($_GET["DisplayNonProjectedCodes"] == "1" ? true : false);
$CondenseGroup = ($_GET["CondenseGroup"] == "1" ? true : false);
$OnlySupervised = ($_GET["OnlySupervised"] == "1" ? true : false);
if ($Status == "null"){$Status = "Approved";}
$StartOfThisMonth = ($InvoiceTool ? date("m/01/Y",strtotime(date("m/01/Y")." -1 month")) : date("m/01/Y"));
$EndOfThisMonth = ($InvoiceTool ? date("m/t/Y",strtotime(date("m/01/Y")." -1 month")) : date("m/t/Y"));
$threeMonthsAgo = date("m/01/Y",strtotime(date("m/01/Y")." -3 month"));
$endOfLastMonth = date("m/t/Y",strtotime(date("m/01/Y")." -1 month"));
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $threeMonthsAgo);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $endOfLastMonth);
$numberOfWeeks = datediff($StartDate, $EndDate, "week","down");
$numberOfDays = datediff($StartDate, $EndDate, "day");
$numberOfMonths =round($numberOfDays/30);

/*
echo "# of months between ".$StartDate."  and ".$EndDate." = ".$numberOfMonths."<br>";
echo "# of weeks = ".$numberOfWeeks."<br>";
echo "# of days = ".$numberOfDays."<br>";
*/

$numberOfWorkDays = getWorkdays($StartDate, $EndDate);
$numberOfWorkWeeks = floor($numberOfWorkDays/5);
$numberOfWeeks = $numberOfWorkWeeks;
$numberOfDays = $numberOfWorkDays;
$numberOfHoursPerDayLimit = 7;
$numberOfHoursPerWeekLimit = 35;
$numberOfWorkDaysPerMonth = bcdiv(getWorkdays($StartDate, date("Y-m-d",strtotime($StartDate." +364 days"))),12,2);
$numberOfWorkWeeksPerMonth = bcdiv($numberOfWorkDaysPerMonth,5,2);

/*
echo "# of work weeks = ".$numberOfWorkWeeks."<br>";
echo "# of work days = ".$numberOfWorkDays."<br>";
echo "# of work days per year = ".getWorkdays($StartDate, date("Y-m-d",strtotime($StartDate." +364 days")))."<br>";
echo "average # of work days per month = ".$numberOfWorkDaysPerMonth."<br>";
echo "average # of work weeks per month = ".$numberOfWorkWeeksPerMonth."<br>";
*/

//Get GBSSupervised
$criteria = new stdClass();
$paginationsResults = $gbsProvider->getSupervisedBy($criteria);
$resultArray = $paginationsResults->collection;
foreach($resultArray as $result=>$record){
	$SupervisedBy[$record->employeeEmail] = $record->supervisorName;
}

$TransitionaryPeriod = true;
	
$nonDepartmentSupervisorDefault = ($TransitionaryPeriod ? "Liz Budd" : "Dave Orsman");

$criteria = new stdClass();
$criteria->noLimit = true;
$criteria->isActive = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeByName[$record->lastName."_".$record->firstName] = $record;
	$nonDepartmentSupervisor = ($SupervisedBy[$record->email] ? $SupervisedBy[$record->email] : $nonDepartmentSupervisorDefault);
	$manager = ($record->manager ? : $nonDepartmentSupervisor);
	$includeThisRecord = true;
	$EmployeesByManager[$manager][]=$record->lastName."_".$record->firstName;
	if ($OnlySupervised){
		if ($manager == $CurrentSupervisorName){
			$includeThisRecord = true;
			$EmployeesByThisManager[] = $record->lastName."_".$record->firstName;

		}else{
			$includeThisRecord = false;
		}
	}
	if ($includeThisRecord){
		if ($record->displayOrderId){
			$EmployeesActive[$record->displayOrderId]=$record;
		}else{
			$EmployeesInActive[$record->firstName]=$record;
		}
	}
}
//print_pre($EmployeeByName["Cook_Josh"]);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AllEmployeesByID[$record->id] = $record;
	$Employee = $record->lastName."_".$record->firstName;
	$BillingRateByEmployee[$Employee]["notInvoiced"] = 1;
	$BillingNameByEmployee[$Employee]["notInvoiced"] = "notInvoiced";

}

//Get Billing Rates
include_once($dbProviderFolder."GBSProvider.php");
$BillingRatesProvider = new GBSProvider($dataConn);

$paginationResult = $BillingRatesProvider->invoiceTool_getBillingRates();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$rateType = $record->GBSBillingRate_InvoiceName;
	$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
	$BillingRateByCategory[$rateType][$record->GBSBillingRate_Name]=$record;
	$BillingRateByID[$record->GBSBillingRate_ID]=$record;
	foreach ($EmployeeMembers as $Employee){
		//$BillingRateByEmployee[$Employee][$record->GBSBillingRate_ID] = $record->GBSBillingRate_Rate;
		$BillingRateByEmployee[$Employee][$rateType] = $record->GBSBillingRate_Rate;
		$BillingNameByEmployee[$Employee][$rateType] = $record->GBSBillingRate_Name;
	}
}
//print_pre($BillingRateByID);
//print_pre($BillingRateByEmployee);

//Get Groups
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupsByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
	if ($record->displayInProjections == "Yes"){
		$GroupsInProjections[] = $record->id;
	}
}
//print_pre($GroupsByID);
ksort($GroupsByName);

//setup NonInvoiced Group Colors
$GroupsByID["AAAZZZNotInvoicedGroupId"]->name = "NotInvoiced";
$GroupsByID["AAAZZZNotInvoicedGroupId"]->colorBackground = "#ffffff";
$GroupsByID["AAAZZZNotInvoicedGroupId"]->colorBorder = "#000000";
$GroupsByID["AAAZZZNotInvoicedGroupId"]->displayInProjections = "Yes";
$GroupsByID["BBBZZZNotInvoicedGroupId"]->name = "NotInvoiced";
$GroupsByID["BBBZZZNotInvoicedGroupId"]->colorBackground = "#ffffff";
$GroupsByID["BBBZZZNotInvoicedGroupId"]->colorBorder = "#000000";
$GroupsByID["BBBZZZNotInvoicedGroupId"]->displayInProjections = "Yes";
$GroupsByID["ZZZNotInvoicedGroupId"]->name = "NotInvoiced";
$GroupsByID["ZZZNotInvoicedGroupId"]->colorBackground = "#ffffff";
$GroupsByID["ZZZNotInvoicedGroupId"]->colorBorder = "#000000";
$GroupsByID["ZZZNotInvoicedGroupId"]->displayInProjections = "Yes";
//print_pre($GroupsByID);
//Get CodeData
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodesByID[$record->id] = $record;
	$CodesBudgetByGroupID[$record->groupId] = bcadd($CodesBudgetByGroupID[$record->groupId],$record->budget,2);
	$CodesByName[$record->name]=$record;
	$CodesInGroup[$record->groupId][] = $record->id;
	$CodesInGroupByName[$record->groupId][] = $record->name;
	if ($record->groupId == 4 || $record->groupId == 5){
		$CodesInGroup["BGAS"][] = $record->id;
		$CodesInGroupByName["BGAS"][] = $record->name;
	}
	if ($record->groupId == 28 || $record->groupId == 26){
		$CodesInGroup["Covanta"][] = $record->id;
		$CodesInGroupByName["Covanta"][] = $record->name;
	}
	if ($record->groupId == 22 || $record->groupId == 48 || $record->groupId == 20 || $record->groupId == 51 || $record->groupId == 21 || $record->groupId == 15 || $record->groupId == 14 || $record->groupId == 57 || $record->groupId == 58 || $record->groupId == 62 || $record->groupId == 69){
		if ($record->groupId == 58){ //split up CT Waste into two groups
			$CodesByGroupName[$record->name][]=$record->name;
		}else{
			$CodesByGroupName[$GroupsByID[$record->groupId]->name][]=$record->name;
		}
		$CodesInGroup["SmallContractHours"][] = $record->id;
		$CodesInGroupByName["SmallContractHours"][] = $record->name;
	}
	if ($record->groupId == 67){
		$CodesByGroupName["RUSHours"][]=$record->name;
		$CodesInGroup["RUSHours"][] = $record->id;
		$CodesInGroupByName["RUSHours"][] = $record->name;
	}
	if ($record->groupId == 36 || $record->groupId == 37 || $record->groupId == 38 || $record->groupId == 39 || $record->groupId == 40 || $record->groupId == 41 || $record->groupId == 52 || $record->groupId == 53 || $record->groupId == 54 || $record->groupId == 55 || $record->groupId == 56){
		$CodesInGroup["BGASResidentialInvoice"][] = $record->id;
		$CodesInGroupByName["BGASResidentialInvoice"][] = $record->name;
	}
	if ($record->id == 217){
		$CodesInGroup["HealthyHomes"][] = $record->id;
		$CodesInGroupByName["HealthyHomes"][] = $record->name;
		$CodesByGroupName["Total Hours"][]=$record->name;
	}
	if ($record->id == 220){
		$CodesInGroup["WGE"][] = $record->id;
		$CodesInGroupByName["WGE"][] = $record->name;
	}
	if ($record->nonbillable){
		$CodesInGroup["nonbillable"][] = $record->id;
		$CodesInGroupByName["nonbillable"][] = $record->name;
	}
	if ($record->displayOrderId){
		$CodesActive[$record->name]=$record;
	}
}
ksort($CodesActive);
ksort($CodesByName);
//print_pre($CodesByName["533"]);
//print_pre($CodesByID[52]);
//print_pre($BillingRateByID[23]);
//get hours

//get savedPRojections
$savedProjections = $gbsProvider->getProjections();
$projectionsCollection = $savedProjections->collection;
$loadedProjection = $_GET['projectionTimeStamp'];
foreach ($projectionsCollection as $record){
	$savedProjectionsByTimeStamp[$record->adjustmentTimeStamp] = $record;
	$savedProjectionsByName[$record->adjustmentName] = $record;
}


if ($Status != "Approved"){
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeId = $SelectedCodesID;
	$criteria->employeeId = $EmployeeID;
	$criteria->status = ($Status != 'All' ? $Status : false);
	//print_pre($criteria);
	$paginationResult = $gbsProvider->getHours($criteria);
	$resultArray = $paginationResult->collection;
	$RecordCount = 0;
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
				if (!in_array($Hours->codeId,$CodesInGroup[$SelectedGroupsID])){
					$includeData = false;
				}
		}
		if ($includeData){
			$EmployeeName= $AllEmployeesByID[$Hours->employeeId]->lastName."_".$AllEmployeesByID[$Hours->employeeId]->firstName;
			$isSummaryExcluded = $EmployeeByName[$EmployeeName]->excludeFromProjectionSummary;

			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
				$thisCodeId = $Hours->codeId;
				$isUnbillable = $CodesByID[$thisCodeId]->nonbillable;
				$thisGroupId = $CodesByID[$thisCodeId]->groupId;
				$thisCodeName = $CodesByID[$thisCodeId]->name;
				$thisBillingRateId = $CodesByID[$thisCodeId]->billingRateId;
				$thisBillingRateName = $BillingRateByID[$thisBillingRateId]->GBSBillingRate_InvoiceName;
				$thisBillingRate = $BillingRateByEmployee[$EmployeeName][$thisBillingRateName];
				$isExcludedFromProjections = $CodesByID[$thisCodeId]->excludeFromProjections;
				if (!$thisBillingRate){
					$thisBillingRate = $EmployeeName;
					if ($GroupsByID[$thisGroupId]->displayInProjections == "Yes" && $Hours->hours && !$isUnbillable){
						$Adjustments["Alert"]["Missing Billing Rate"][$EmployeeName][$thisCodeName]=$EmployeeName." missing Billing Rate for ".$thisBillingRateName." for code:".$thisCodeName;
					}
				}
				
				
				if ($GroupsByID[$thisGroupId]->displayInProjections != "Yes"){
					if ($thisCodeName == "501"){
						$thisGroupId = "ZZZNotInvoicedGroupId";
						$thisBillingRate = 1;
						$thisBillingRateName = "notInvoiced";
					}else{
						$thisCodeId = "NotInvoicedCodeId";
						$thisGroupId = "ZZZNotInvoicedGroupId";
						$thisCodeName = "Aggregate of Other Codes";
						$thisBillingRate = 1;
						$thisBillingRateName = "notInvoiced";
						if ($DisplayNonProjectedCodes){
							$thisCodeId = $Hours->codeId;
							$thisGroupId = $CodesByID[$thisCodeId]->groupId;
							$thisCodeName = $CodesByID[$thisCodeId]->name;
							if (!$GroupsByID[$thisGroupId]->colorBorder && $CondenseGroup){
								$thisCodeId = "NotInvoicedCodeId";
								$thisGroupId = "ZZZNotInvoicedGroupId";
								$thisCodeName = "Aggregate of Other Codes";
							}
						}
					}
				}
				
				
				$EmployeeData[$EmployeeName][$thisCodeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$thisCodeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$thisCodeId]["totalHours"],$Hours->hours,2);
				$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["billingRate"]=$thisBillingRate;
				$thisBilling = bcmul($Hours->hours,$thisBillingRate,2);
				if (!$isExcludedFromProjections && !$isUnbillable){
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalHours"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalHours"],$Hours->hours,2);
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalBilling"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalBilling"],$thisBilling,2);
				}
				if ($isUnbillable){
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"],$Hours->hours,2);
				}
				
				$CodesData[$thisCodeName]=$thisCodeId;
				$GroupIdSeed = ($GroupsByID[$thisGroupId]->colorBorder ? "AAA" : "BBB".$thisCodeName);
				$CodesDataByGroupId[$GroupIdSeed.$thisGroupId][$thisCodeName]=$thisCodeId;
				$CodesDataTotals[$thisCodeId] = bcadd($CodesDataTotals[$thisCodeId],$Hours->hours,2);
				$CodesDataTotalsByGroupId[$thisGroupId] = bcadd($CodesDataTotalsByGroupId[$thisGroupId],$Hours->hours,2);
//				$CodesDataBillingTotalsByGroupId[$thisGroupId] = bcadd($CodesDataBillingTotalsByGroupId[$thisGroupId],$thisBilling,2);
				if ($isUnbillable){
					$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
					$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
				}else{
					$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
					$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
				}
				if ($CodesByID[$Hours->codeId]->name == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}			
				if ($thisGroupId == "ZZZNotInvoicedGroupId"){
					$CodesDataProjectionsByGroupId[$GroupIdSeed.$thisGroupId][$thisCodeName]=$thisCodeId;
					$thisHoursAllocatedMontly = bcdiv($Hours->hours,$numberOfMonths,2);
					$EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"]=bcadd($EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"],$thisHoursAllocatedMontly,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["billingRate"]=$thisBillingRate;
					$thisBilling = bcmul($thisHoursAllocatedMontly,$thisBillingRate,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"],$thisHoursAllocatedMontly,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"],$thisBilling,2);
					if ($CodesByID[$Hours->codeId]->name == "501"){
						$EmployeeAggregateDataProjections[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["501Hours"],$thisHoursAllocatedMontly,2);
						if (!$isSummaryExcluded){
							$TotalAggregateDataProjections["501Hours"]=bcadd($TotalAggregateDataProjections["501Hours"],$thisHoursAllocatedMontly,2);
						}
					}else{
						if ($isUnbillable){
							$EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"],$thisHoursAllocatedMontly,2);
							if (!$isSummaryExcluded){
								$TotalAggregateDataProjections["totalUnBillable"]=bcadd($TotalAggregateDataProjections["totalUnBillable"],$thisHoursAllocatedMontly,2);
							}	
						}else{
							$EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"],$thisHoursAllocatedMontly,2);
							if (!$isSummaryExcluded){
								$TotalAggregateDataProjections["totalBillable"]=bcadd($TotalAggregateDataProjections["totalBillable"],$thisHoursAllocatedMontly,2);
							}
						}
					}

				}
				
				$projectionByGroup[$thisGroupId]["params"]["groupId"]=$thisGroupId;
				$projectionByGroup[$thisGroupId]["params"]["groupName"]=$GroupsByID[$thisGroupId]->name;
				$projectionByGroup[$thisGroupId]["params"]["billingRateEmployees"][$thisBillingRate]=$BillingNameByEmployee[$EmployeeName][$thisBillingRateName];
				$projectionByGroup[$thisGroupId]["params"]["codeIds"][$thisCodeId] = $CodesByID[$thisCodeId]->name;
				$projectionByGroup[$thisGroupId]["params"]["employees"][$EmployeeName] = 1;
				if (!$isExcludedFromProjections && !$isUnbillable){
					$projectionByGroup[$thisGroupId]["totalHours"] = bcadd($projectionByGroup[$thisGroupId]["totalHours"],$Hours->hours,2);
					$projectionByGroup[$thisGroupId]["totalHoursPerBillingRate"][$thisBillingRate] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerBillingRate"][$thisBillingRate],$Hours->hours,2);
					$projectionByGroup[$thisGroupId]["totalHoursPerBillingRatePerEmployee"][$thisBillingRate][$EmployeeName] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerBillingRatePerEmployee"][$thisBillingRate][$EmployeeName],$Hours->hours,2);
				}
				$projectionByGroup[$thisGroupId]["totalHoursPerEmployeePerCode"][$EmployeeName][$thisCodeId] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerEmployeePerCode"][$EmployeeName][$thisCodeId],$Hours->hours,2);
			}//end includeThisData
		}
	}
	ksort($CodesData);
	ksort($CodesDataByGroupId);
}else{
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeName = ($SelectedCodesID ? $CodesByID[$SelectedCodesID]->name : false);
	$criteria->lastName = ($EmployeeID ? $AllEmployeesByID[$EmployeeID]->lastName : false);
	$paginationResult = $gbsProvider->getALLApprovedHours($criteria);
	$resultArray = $paginationResult->collection;
	//print_pre($criteria);
	$RecordCount = 0;
	//print_pre($resultArray);
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
			if (!in_array($Hours->codeName,$CodesInGroupByName[$SelectedGroupsID])){
				$includeData = false;
			}
		}
		if ($includeData){
			$EmployeeName= $Hours->employeeName;
			$isSummaryExcluded = $EmployeeByName[$EmployeeName]->excludeFromProjectionSummary;
			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
	//			$CodeData[$Hours->codeId][]=$Hours;
				$codeId = $CodesByName[$Hours->codeName]->id;
				$thisCodeId = $codeId;
				$isUnbillable = $CodesByID[$thisCodeId]->nonbillable;
				$thisGroupId = $CodesByID[$thisCodeId]->groupId;
				$thisCodeName = $Hours->codeName;
				$thisBillingRateId = $CodesByID[$thisCodeId]->billingRateId;
				$thisBillingRateName = $BillingRateByID[$thisBillingRateId]->GBSBillingRate_InvoiceName;
				$thisBillingRate = $BillingRateByEmployee[$EmployeeName][$thisBillingRateName];
				$isExcludedFromProjections = $CodesByID[$thisCodeId]->excludeFromProjections;
				if (!$thisBillingRate){
					$thisBillingRate = $EmployeeName;
					if ($GroupsByID[$thisGroupId]->displayInProjections == "Yes" && $Hours->hours && !$isUnbillable){
						$Adjustments["Alert"]["Missing Billing Rate"][]=$EmployeeName." missing Billing Rate for ".$thisBillingRateName." for code:".$thisCodeName;
					}
				}

				if ($GroupsByID[$thisGroupId]->displayInProjections != "Yes"){
					
					if ($thisCodeName == "501"){
						$thisGroupId = "ZZZNotInvoicedGroupId";
						$thisBillingRate = 1;
						$thisBillingRateName = "notInvoiced";
					}else{
						$thisCodeId = "NotInvoicedCodeId";
						$thisGroupId = "ZZZNotInvoicedGroupId";
						$thisCodeName = "Aggregate of Other Codes";
						$thisBillingRate = 1;
						$thisBillingRateName = "notInvoiced";
						if ($DisplayNonProjectedCodes){
							$thisCodeId = $codeId;
							$thisGroupId = $CodesByID[$thisCodeId]->groupId;
							$thisCodeName = $Hours->codeName;
							if (!$GroupsByID[$thisGroupId]->colorBorder && $CondenseGroup){
								$thisCodeId = "NotInvoicedCodeId";
								$thisGroupId = "ZZZNotInvoicedGroupId";
								$thisCodeName = "Aggregate of Other Codes";
							}
						}
					}
				}
				
				$EmployeeData[$EmployeeName][$thisCodeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$thisCodeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$thisCodeId]["totalHours"],$Hours->hours,2);
				$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["billingRate"]=$thisBillingRate;
				$thisBilling = bcmul($Hours->hours,$thisBillingRate,2);
				if (!$isExcludedFromProjections && !$isUnbillable){
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalHours"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalHours"],$Hours->hours,2);
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalBilling"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["totalBilling"],$thisBilling,2);
				}
				if ($isUnbillable){
					$EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"]=bcadd($EmployeeDataByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"],$Hours->hours,2);
				}
				
				$CodesData[$thisCodeName]=$thisCodeId;
				$GroupIdSeed = ($GroupsByID[$thisGroupId]->colorBorder ? "AAA" : "BBB".$thisCodeName);
				$CodesDataByGroupId[$GroupIdSeed.$thisGroupId][$thisCodeName]=$thisCodeId;
				$CodesDataTotals[$thisCodeId] = bcadd($CodesDataTotals[$thisCodeId],$Hours->hours,2);
				$CodesDataTotalsByGroupId[$thisGroupId] = bcadd($CodesDataTotalsByGroupId[$thisGroupId],$Hours->hours,2);
//				$CodesDataBillingTotalsByGroupId[$thisGroupId] = bcadd($CodesDataBillingTotalsByGroupId[$thisGroupId],$thisBilling,2);
				
				
				if ($Hours->codeName == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}else{
					if ($isUnbillable){
						$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
						$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
					}else{
						$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
						$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
					}
				}
				if ($thisGroupId == "ZZZNotInvoicedGroupId"){
					$CodesDataProjectionsByGroupId[$GroupIdSeed.$thisGroupId][$thisCodeName]=$thisCodeId;
					$thisHoursAllocatedMontly = bcdiv($Hours->hours,$numberOfMonths,2);
					$EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"]=bcadd($EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"],$thisHoursAllocatedMontly,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["billingRate"]=$thisBillingRate;
					$thisBilling = bcmul($thisHoursAllocatedMontly,$thisBillingRate,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"],$thisHoursAllocatedMontly,2);
					$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"],$thisBilling,2);
					if ($CodesByID[$codeId]->name == "501"){
						$EmployeeAggregateDataProjections[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["501Hours"],$thisHoursAllocatedMontly,2);
						if (!$isSummaryExcluded){
							$TotalAggregateDataProjections["501Hours"]=bcadd($TotalAggregateDataProjections["501Hours"],$thisHoursAllocatedMontly,2);
						}
					}else{
						if ($isUnbillable){
							$EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"],$thisHoursAllocatedMontly,2);
							if (!$isSummaryExcluded){
								$TotalAggregateDataProjections["totalUnBillable"]=bcadd($TotalAggregateDataProjections["totalUnBillable"],$thisHoursAllocatedMontly,2);
							}
						}else{
							$EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"],$thisHoursAllocatedMontly,2);
							if (!$isSummaryExcluded){
								$TotalAggregateDataProjections["totalBillable"]=bcadd($TotalAggregateDataProjections["totalBillable"],$thisHoursAllocatedMontly,2);
							}
						}
					}

				}

				$projectionByGroup[$thisGroupId]["params"]["groupId"]=$thisGroupId;
				$projectionByGroup[$thisGroupId]["params"]["groupName"]=$GroupsByID[$thisGroupId]->name;
				$projectionByGroup[$thisGroupId]["params"]["billingRateEmployees"][$thisBillingRate]=$BillingNameByEmployee[$EmployeeName][$thisBillingRateName];
				$projectionByGroup[$thisGroupId]["params"]["codeIds"][$thisCodeId] = $CodesByID[$thisCodeId]->name;
				$projectionByGroup[$thisGroupId]["params"]["employees"][$EmployeeName] = 1;
				if (!$isExcludedFromProjections && !$isUnbillable){
					$projectionByGroup[$thisGroupId]["totalHours"] = bcadd($projectionByGroup[$thisGroupId]["totalHours"],$Hours->hours,2);
					$projectionByGroup[$thisGroupId]["totalHoursPerBillingRate"][$thisBillingRate] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerBillingRate"][$thisBillingRate],$Hours->hours,2);
					$projectionByGroup[$thisGroupId]["totalHoursPerBillingRatePerEmployee"][$thisBillingRate][$EmployeeName] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerBillingRatePerEmployee"][$thisBillingRate][$EmployeeName],$Hours->hours,2);
				}
				$projectionByGroup[$thisGroupId]["totalHoursPerEmployeePerCode"][$EmployeeName][$thisCodeId] = bcadd($projectionByGroup[$thisGroupId]["totalHoursPerEmployeePerCode"][$EmployeeName][$thisCodeId],$Hours->hours,2);
				
				
				
			}//end includeThisData
		}
	}
	ksort($CodesData);
	ksort($CodesDataByGroupId);
	
}
//print_pre($CodesDataByGroupId);
ksort($EmployeeData);

//Get All Billed Data per code regardless of time frame
foreach ($GroupsInProjections as $groupId){
	//print_pre($GroupsByID[$groupId]);
	$invoiceId = $GroupsByID[$groupId]->invoiceId;
	//print_pre($InvoicingFiscalYearsByID[$invoiceId]);
	$codes = $invoicingListById[$invoiceId]->codes;
	//echo $codes;
	//print_pre($InvoicingFiscalYearsByID);
	if (MySQLDate($InvoicingFiscalYearsByID[$invoiceId]["fiscalYearStart"])){
		//get it for this time period and then also for all time periods
		$criteria = new stdClass();
		$criteria->startDate = date("Y-m-d",strtotime($StartDate));
		$criteria->endDate = date("Y-m-d",strtotime($EndDate));
		$criteria->codes = $codes;
		$paginationResult = $gbsProvider->getALLApprovedHours($criteria);
		$resultArray = $paginationResult->collection;
		foreach ($resultArray as $Hours){
			//print_pre($Hours);
			$EmployeeName= $Hours->employeeName;
			$codeId = $CodesByName[$Hours->codeName]->id;
			$thisCodeId = $codeId;
			$isUnbillable = $CodesByID[$thisCodeId]->nonbillable;
			$thisBillingRateId = $CodesByID[$thisCodeId]->billingRateId;
			$thisBillingRateName = $BillingRateByID[$thisBillingRateId]->GBSBillingRate_InvoiceName;
			$thisBillingRate = $BillingRateByEmployee[$EmployeeName][$thisBillingRateName];
			$isExcludedFromProjections = $CodesByID[$thisCodeId]->excludeFromProjections;
			$hoursRecordId = $Hours->id;
			
			if (!$isExcludedFromProjections && !$isUnbillable && !$hoursAlreadyIncluded[$hoursRecordId]){
				$hoursAlreadyIncluded[$hoursRecordId] = 1;
				$thisBilling = bcmul($Hours->hours,$thisBillingRate,2);
				$CodesDataTotalsBillingThisTimePeriod[$thisCodeId] = bcadd($CodesDataTotalsBillingThisTimePeriod[$thisCodeId],$thisBilling,2);
				$CodesDataTotalsBillingByCodeNameThisTimePeriod[$Hours->codeName][$EmployeeName] = bcadd($CodesDataTotalsBillingByCodeNameThisTimePeriod[$Hours->codeName][$EmployeeName],$thisBilling,2);
				$CodesDataTotalsBillingByGroupThisTimePeriod[$groupId] = bcadd($CodesDataTotalsBillingByGroupThisTimePeriod[$groupId],$thisBilling,2);
			}
		}
		//Now make manually made code adjustments
		$criteria->invoiceCode = $invoicingListById[$invoiceId]->invoicingNumberName;
		if ($invoicingListById[$invoiceId]->invoicingNumberName == "Rathmann"){
			$criteria->invoiceCodeStartsWith = $invoicingListById[$invoiceId]->invoicingNumberName;
		}else{
			$criteria->invoiceCode = $invoicingListById[$invoiceId]->invoicingNumberName;
		}
		$invoicingHistoryCodeAdjustments = $invoicingProvider->getInvoiceHistoryCodeAdjust($criteria);
		foreach ($invoicingHistoryCodeAdjustments as $adjustment){
			$thisCodeId = $CodesByName[$adjustment["invoiceCodeName"]]->id;
			$CodesDataTotalsBillingThisTimePeriod[$thisCodeId] = bcadd($CodesDataTotalsBillingThisTimePeriod[$thisCodeId],$adjustment["invoiceAmount"],2);
			$CodesDataTotalsBillingByGroupThisTimePeriod[$groupId] = bcadd($CodesDataTotalsBillingByGroupThisTimePeriod[$groupId],$adjustment["invoiceAmount"],2);
			$CodesDataAdjustmentsThisTimePeriod[$thisCodeId] = bcadd($CodesDataAdjustmentsThisTimePeriod[$thisCodeId],$adjustment["invoiceAmount"],2);
			$CodesDataAdjustmentsByGroupThisTimePeriod[$groupId] = bcadd($CodesDataAdjustmentsByGroupThisTimePeriod[$groupId],$adjustment["invoiceAmount"],2);
		}
		
		//all time periods
		$criteria = new stdClass();
		$criteria->startDate = $InvoicingFiscalYearsByID[$invoiceId]["fiscalYearStart"];
		$criteria->endDate = $InvoicingFiscalYearsByID[$invoiceId]["fiscalYearEnd"];
		$criteria->codes = $codes;
		//print_pre($criteria);
		$paginationResult = $gbsProvider->getALLApprovedHours($criteria);
		$resultArray = $paginationResult->collection;
		foreach ($resultArray as $Hours){
			//print_pre($Hours);
			$EmployeeName= $Hours->employeeName;
			$codeId = $CodesByName[$Hours->codeName]->id;
			$thisCodeId = $codeId;
			$isUnbillable = $CodesByID[$thisCodeId]->nonbillable;
			$thisBillingRateId = $CodesByID[$thisCodeId]->billingRateId;
			$thisBillingRateName = $BillingRateByID[$thisBillingRateId]->GBSBillingRate_InvoiceName;
			$thisBillingRate = $BillingRateByEmployee[$EmployeeName][$thisBillingRateName];
			$thisBilling = bcmul($Hours->hours,$thisBillingRate,2);
			$isExcludedFromProjections = $CodesByID[$thisCodeId]->excludeFromProjections;
			$hoursRecordId = $Hours->id;
			
			if (!$isExcludedFromProjections && !$isUnbillable && !$hoursAlreadyIncludedAllTime[$hoursRecordId]){
				$hoursAlreadyIncludedAllTime[$hoursRecordId] = 1;
				$CodesDataTotalsBillingAllTime[$thisCodeId] = bcadd($CodesDataTotalsBillingAllTime[$thisCodeId],$thisBilling,2);
				$CodesDataTotalsBillingByGroupAllTime[$groupId] = bcadd($CodesDataTotalsBillingByGroupAllTime[$groupId],$thisBilling,2);
			}
		}
		//Now make manually made code adjustments
		$criteria = new stdClass();
		if ($invoicingListById[$invoiceId]->invoicingNumberName == "Rathmann"){
			$criteria->invoiceCodeStartsWith = $invoicingListById[$invoiceId]->invoicingNumberName;
		}else{
			$criteria->invoiceCode = $invoicingListById[$invoiceId]->invoicingNumberName;
		}
		$invoicingHistoryCodeAdjustments = $invoicingProvider->getInvoiceHistoryCodeAdjust($criteria);
		foreach ($invoicingHistoryCodeAdjustments as $adjustment){
			$thisCodeId = $CodesByName[$adjustment["invoiceCodeName"]]->id;
			$CodesDataTotalsBillingAllTime[$thisCodeId] = bcadd($CodesDataTotalsBillingAllTime[$thisCodeId],$adjustment["invoiceAmount"],2);
			$CodesDataTotalsBillingByGroupAllTime[$groupId] = bcadd($CodesDataTotalsBillingByGroupAllTime[$groupId],$adjustment["invoiceAmount"],2);
			$CodesDataAdjustmentsAllTime[$thisCodeId] = bcadd($CodesDataAdjustmentsAllTime[$thisCodeId],$adjustment["invoiceAmount"],2);
			$CodesDataAdjustmentsByGroupAllTime[$groupId] = bcadd($CodesDataAdjustmentsByGroupAllTime[$groupId],$adjustment["invoiceAmount"],2);
		}
		
		
	}else{
		$Adjustments["Alert"]["Invoicing missing Fiscal Year Start"][] = $invoicingListById[$invoiceId]->displayName;
	}
	
	
	
}
//print_pre($CodesDataTotalsBillingByCodeName["534"]);
//print_pre($CodesDataTotalsBillingByGroupThisTimePeriod[69]);
//print_pre($CodesDataTotalsBillingByGroupAllTime);
//print_pre($EmployeeHistory["Bhattacharya_Shomita"]);
//print_pre($CodesDataTotals);


$DivColumns = "four";
$DivColumns1 = "four";
$employeeComboBoxDivColumns = $DivColumns;
$employeeComboBoxHideLabel = true;
$employeeComboBoxFunction = '$("#SelectedEmployee").val($(ui.item).val()).change();';
$codesComboBoxDivColumns = $DivColumns;
$codesComboBoxHideLabel = true;
$codesComboBoxFunction = '$("#SelectedCode").val($(ui.item).val()).change();';
$groupsComboBoxDivColumns = $DivColumns;
$groupsComboBoxHideLabel = true;
$groupsComboBoxFunction = '$("#SelectedGroup").val($(ui.item).val()).change();';
$customComboBoxInput = "width:150px;font-weight:normal;font-size:8pt;";
?>
<style>
.deleteRow {position:relative;width:10px;height:10px;float:right;top:-20px;right:10px;
	background-image:url("<?php echo $CurrentServer;?>images/redx-small.png");
	background-size: 10px 10px;
    background-repeat: no-repeat;
	background-position: left top;
	cursor:pointer;
}
fieldset {vertical-align:top;}

</style>
	<h3>Projections Hours Report</h3>
	<div class="twenty columns">
		<fieldset>
			<legend>Filters</legend>
			<div class="row">
				<div class="two columns">
					Start Date<bR>
					<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
				</div>
				<div class="two columns">
					End Date<br>
					<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
				</div>
				<div class="two columns" style="display:none;">
					Status<br>
					<select class="parameters" id="SelectedStatus" style="width:100px;">
						<option value="Approved"<?php echo ($Status=="Approved" ? " selected" : "");?>>Approved</option>
						<option value="Submitted"<?php echo ($Status=="Submitted" ? " selected" : "");?>>Submitted</option>
						<option value="Saved"<?php echo ($Status=="Saved" ? " selected" : "");?>>Saved</option>
						<option value="All"<?php echo ($Status=="All" ? " selected" : "");?>>All</option>
					</select>
				</div>
				<div class="two columns">
					<br>
					<button id="resetbutton">Reset Filters</button>
				</div>
			</div>
			<div class="row">
				<div class="three columns" style="display:none;">
					<input class="parameters" type="checkbox" id="HideCodes"<?php echo ($HideCodes ? " checked" : "");?>>Hide All Codes
				</div>
				<div class="five columns">
					<input class="parameters" type="checkbox" id="DisplayNonProjectedCodes"<?php echo ($DisplayNonProjectedCodes ? " checked" : "");?>>Display Non Projected Codes<br>
				</div>
				<div class="three columns">
					<input class="parameters" type="checkbox" id="CondenseGroup"<?php echo ($CondenseGroup ? " checked" : "");?>>Hide Group Codes<br>
				</div>
			</div>
			<div class="row" style="display:none;">
				<div class="<?php echo $DivColumns1;?> columns">
					Employee<br>
					<?php include('../hr/_EmployeeComboBox.php');?>
					<?php if ($EmployeeID){?>
						<div class="deleteRow" data-box="SelectedEmployee">&nbsp;</div>
					<?php }?>
					<Br>
					<input class="parameters" type="checkbox" id="OnlySupervised"<?php echo ($OnlySupervised ? " checked" : "");?>>Only Those You Supervise
				</div>
				<div class="<?php echo $DivColumns1;?> columns">
					Code<br>
					<?php include('_CodesComboBox.php');?>
					<?php if ($SelectedCodesID){?>
						<div class="deleteRow" data-box="SelectedCode">&nbsp;</div>
					<?php }?>
				</div>
				<div class="<?php echo $DivColumns1;?> columns">
					Group<br>
					<?php include('_GroupsComboBox.php');?>
					<?php if ($SelectedGroupsID){?>
						<div class="deleteRow" data-box="SelectedGroup">&nbsp;</div>
					<?php }?>
				</div>
			</div>
		</fieldset>
		<fieldset id="adjustmentsFieldSet"><legend>Saved Adjustments</legend>
			<input type="hidden" id="SavedAdjustmentsTimeStamp" value="<?php echo time();?>">
			<?php 
				if (count($savedProjectionsByTimeStamp)){
					echo "<div class='row'><div class='four columns'>Load a Saved Projection:</div><select id='loadSavedProjections' class='six columns'><option value=''></option>";
						foreach ($savedProjectionsByTimeStamp as $timeStamp=>$record){
							$createdBy = $record->createdBy;
							$createdByParts = explode(".",$createdBy);
							echo "<option value='".$timeStamp."'>".$record->adjustmentName." ".date("m/d/Y h:ia",$timeStamp)." (".ucwords($createdByParts[0]).")</option>";
						}
					echo "</select></div>";
				}
				if($savedProjectionsByTimeStamp[$loadedProjection]){
					//print_pre($savedProjectionsByTimeStamp);
					echo "<div class='row'><div class='four columns'>Currently Loaded Projection:</div><div class='ten columns'>".$savedProjectionsByTimeStamp[$loadedProjection]->adjustmentName." ".date("m/d/Y h:ia",$savedProjectionsByTimeStamp[$loadedProjection]->adjustmentTimeStamp)."<br>(created by ".$savedProjectionsByTimeStamp[$loadedProjection]->createdBy.")</div>";
					$adjustmentByEmployee = $savedProjectionsByTimeStamp[$loadedProjection]->adjustmentByEmployee;
					$adjustmentByGroup = $savedProjectionsByTimeStamp[$loadedProjection]->adjustmentByGroup;
					$loadedProjectionData["ByEmployee"] = json_decode($adjustmentByEmployee);
					$loadedProjectionData["ByGroup"] = json_decode($adjustmentByGroup);
					//print_pre($loadedProjectionData);
					$setTab = "Projection";
					echo "<Br clear='all'><div class='four columns'>Notes:</div><div class='eight columns'>".$savedProjectionsByTimeStamp[$loadedProjection]->adjustmentNotes."</div>";
					echo "</div>";
					$invoiceGroupsToHide = $savedProjectionsByTimeStamp[$loadedProjection]->adjustmentInvoiceGroupsToHide;
				}
			?>
			<div class='row' id='AdjustmentsSaveH3' style='display:none;text-align:top;margin-top:-20px;margin-bottom:-8px;'><hr>
				<div style='font-weight:bold;font-size:+1em;'>Save Current Adjustments</div><Br>
				<div class='six columns' style='margin-top:-25px;'>Adjustments Name:<br><input type="text" id="SavedAdjustmentsName" style="width:200px;"></div>
				<div class='eight columns' style='margin-top:-25px;'>Adjustment Notes:<Br><textarea id="adjustmentNotes" rows="1" style='height:30px;'><?php echo $savedProjectionsByTimeStamp[$loadedProjection]->adjustmentNotes;?></textarea></div>
				<div class='one column' style='margin-top:-25px;'><br><a href="#" class='button-link' id='saveProjectionAdjustments'>Save</a></div>
			</div>
		</fieldset>
		<fieldset class="sixteen columns" title="Click the Group Name to Toggle Visibility"><legend>Invoice Groups</legend>
			<style>
				.invoiceGroupsWrapper {font-weight:bold;margin-right:10px;position:relative;float:left;color:black;cursor:pointer;}
			</style>
			<?php 
				foreach ($CodesDataByGroupId as $groupOrderId=>$groupInfo){
					if (strpos(" ".$groupOrderId,"AAA")){
						$groupId = str_replace("AAA","",$groupOrderId);
						$groupName = ($GroupsByID[$groupId]->name != "NotInvoiced" ? $GroupsByID[$groupId]->name : "Other Codes");
						
						echo "<div class='invoiceGroupsWrapper' style='background-color:".$GroupsByID[$groupId]->colorBackground.";border:1pt solid ".$GroupsByID[$groupId]->colorBorder.";'>
								<div style='position:relative;float:left;'><input type='checkbox' id='invoiceGroupCheckBox".$groupId."' class= 'GroupCheckBox' checked value ='invoiceGroupDisplay".$groupId."' ></div>
								<div id='invoiceGroupDisplay".$groupId."' class='invoiceGroups' style='position:relative;float:right;' data-groupid='".$groupId."'>
									".$groupName."
								</div>
							</div>";
					}
				}
			?>
			<input type="hidden" id="invoiceGroupsToHide" value="">

		</fieldset>
		<br clear="all">
		<?php echo $RecordCount." records for ".count($EmployeeData)." employees";?>
	</div>
	<br clear="all">
	<div>
	<?php
	$TabCodes = array("Actual Hours"=>"Actual","Projected Hours"=>"Projection","Comparison"=>"Comparison","Staff Adjustments"=>"StaffAdjustments","Alerts"=>"Adjustments");
	foreach ($TabCodes as $TabName=>$TabCode){
		$TabHashes[] = $TabCode;
		$TabListItems[] .= '<li><a id="tab-'.$TabCode.'" href="#'.$TabCode.'" class="tab-'.$TabCode.'">'.$TabName.'</a></li>';
	}
	echo '<ul id="admin-tabs" class="tabs">';
			foreach ($TabListItems as $TabItem){
				echo $TabItem;
			}
	echo '</ul>';		
	echo '<div class="tabs-content">';
	?>
			<style>
				table.dataTable tbody {}
				.StaffName {min-width:85px;border:1pt solid #aed0ea;}
				.hourData {border:1pt solid #aed0ea;min-width:40px;}
				.hourSummary {text-align:center;}
				.CodeDataFooter {font-weight:normal;font-size:8pt;text-align:center;vertical-align: text-top;border-top:2pt solid black;}
				.CodeDataHeader {font-size:10pt;vertical-align:text-top;height:130px;cursor:arrow;min-width:100px;position:relative;background-color:white;}
				.CodeDataHeaderTop {background-color:white;}
				.CodeDataHeaderGroup {
					background-image:url(<?php echo $CurrentServer;?>images/collapse.png);
					background-repeat: no-repeat;
					background-size: 15px 15px;}			
				table.dataTable,
				table.dataTable th,
				table.dataTable td {
				  -webkit-box-sizing: content-box;
				  -moz-box-sizing: content-box;
				  box-sizing: content-box;
				}			
				.dataTables_scroll
				{
					overflow:auto;
				}
				.toggleGroupColumns {cursor:pointer;margin-right:2px;height:15px;}
				.subHours {background-color:#f0dbf0;border:1pt solid #000000;}
				.totalHours {background-color:#f6d5f6;border:1pt solid #000000;}
				.totalHoursComplete {background-color:#ffccff;border:1pt solid #000000;}
				.totalCodeHours {background-color:	#ff99ff;border:1pt solid #000000;}
				.footerSeparator {border-top:2pt solid black;}
				.unbillableHours {font-size:8pt;font-style:italic;display:none;}
			</style>
			<style>
			/* width */
			::-webkit-scrollbar {
				width: 10px;
			}

			/* Track */
			::-webkit-scrollbar-track {
				background: #f1f1f1; 
			}
			 
			/* Handle */
			::-webkit-scrollbar-thumb {
				background: #888; 
			}

			/* Handle on hover */
			::-webkit-scrollbar-thumb:hover {
				background: #555; 
			}
			</style>		
			<div id='Actual' class='tab-content'>
				<fieldset class="twenty columns"><legend>Actual Hours For Time Period (<?php echo $numberOfWeeks;?> equivalent work weeks,<?php echo $numberOfDays;?> work days)</legend>
					<?php include_once('views/_projections_Actual.php');?>
				</fieldset>
			</div>
			<?php 
			//print_pre($CodesDataProjectionsByGroupId);
				$remainingBudgetByGroupId["ZZZNotInvoicedGroupId"]["originalBudget"] = 1;
				$remainingBudgetByGroupId["ZZZNotInvoicedGroupId"]["remainingBudget"] = 1;
				$remainingBudgetByGroupId["ZZZNotInvoicedGroupId"]["remainingMonths"] = 1;
				$remainingBudgetByGroupId["ZZZNotInvoicedGroupId"]["budgetPerMonth"] = bcdiv($remaining,$monthsLeft,2);

			//print_pre($projectionByGroup);
				//print_pre($projectionByGroup[12]);
				foreach ($projectionByGroup as $groupId=>$data){
					$groupName = $data["params"]["groupName"];
					//if ($groupName != "NotInvoiced"){
						$totalMonthlyBudget = $remainingBudgetByGroupId[$groupId]["budgetPerMonth"];
						//echo $groupName." = ".$totalMonthlyBudget."<Br>";
						/*
						echo $groupId."<Br>";
						echo $groupName."<Br>";
						echo "Remaining Budget = ".$remainingBudgetByGroupId[$groupId]["remainingBudget"]."<br>";
						echo "Remaining Months = ".$remainingBudgetByGroupId[$groupId]["remainingMonths"]."<br>";
						echo "Remaining BudgetPerMonth = ".$totalMonthlyBudget."<br>";
						*/
						//Get the BillingRate Ratios
						$totalHoursPerGroup = $data["totalHours"];
						//print_pre($data["totalHoursPerBillingRate"] );
						foreach ($data["totalHoursPerBillingRate"] as $billingRate=>$billingRateHours){
							$thisRatio = bcdiv($billingRateHours,$totalHoursPerGroup,9);
							$thisBillingRateAllocatedMonthlyBudget = round(bcmul($totalMonthlyBudget,$thisRatio,9),2);
							$billingRateRatioByGroup[$groupId][$billingRate]["ratio"] = $thisRatio;
							$billingRateRatioByGroup[$groupId][$billingRate]["allocatedMonthlyBudget"] = $thisBillingRateAllocatedMonthlyBudget;
							//echo $billingRate." has a total hours of ".$billingRateHours."<Br>";
							foreach ($data["totalHoursPerBillingRatePerEmployee"][$billingRate] as $employeeName=>$hours){
								$thisRatio = bcdiv($hours,$billingRateHours,9);
								$thisEmployeeAllocatedMonthlyBudget = round(bcmul($thisBillingRateAllocatedMonthlyBudget,$thisRatio,9),2);
								$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["ratio"] = $thisRatio;
								$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["allocatedMonthlyBudget"] = $thisEmployeeAllocatedMonthlyBudget;
								//echo $employeeName." has a total hours of ".$hours."<br>";
								foreach ($data["totalHoursPerEmployeePerCode"][$employeeName] as $codeId => $codeHours){
									$thisRatio = bcdiv($codeHours,$hours,9);
									$thisCodeAllocatedMonthlyBudget = round(bcmul($thisEmployeeAllocatedMonthlyBudget,$thisRatio,9),2);
									$thisHoursAllocatedMontly = floorToFraction(round(bcdiv($thisCodeAllocatedMonthlyBudget,$billingRate,9),2));
									$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["codeData"][$codeId]["ratio"] = $thisRatio;
									$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["codeData"][$codeId]["allocatedMonthlyBudget"] = $thisCodeAllocatedMonthlyBudget;
									$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["codeData"][$codeId]["allocatedMonthlyHoursActual"] = round(bcdiv($thisCodeAllocatedMonthlyBudget,$billingRate,9),2);											
									$billingRateRatioByGroup[$groupId][$billingRate]["EmployeeData"][$employeeName]["codeData"][$codeId]["allocatedMonthlyHoursNearstQuarter"] = $thisHoursAllocatedMontly;											

									//Create projections dataset
									$EmployeeName = $employeeName;
									$isSummaryExcluded = $EmployeeByName[$EmployeeName]->excludeFromProjectionSummary;
									$thisCodeId = $codeId;
									$thisGroupId = $groupId;
									$isUnbillable = $CodesByID[$codeId]->nonbillable;
									$thisCodeName = $CodesByID[$codeId]->name;
									$thisBillingRateId = $CodesByID[$thisCodeId]->billingRateId;
									$thisBillingRateName = $BillingRateByID[$thisBillingRateId]->GBSBillingRate_InvoiceName;
									$thisBillingRate = $BillingRateByEmployee[$EmployeeName][$thisBillingRateName];
									$isExcludedFromProjections = $CodesByID[$thisCodeId]->excludeFromProjections;
									//if ($isExcludedFromProjections){echo $EmployeeName." ".$thisCodeName." is excluded<br>";}
									if (!$thisBillingRate){$thisBillingRate = $EmployeeName;}
									
									
									if ($GroupsByID[$thisGroupId]->displayInProjections != "Yes"){
										if ($thisCodeName == "501"){
											$thisGroupId = "ZZZNotInvoicedGroupId";
											$thisBillingRate = 1;
											$thisBillingRateName = "notInvoiced";
										}else{
											$thisCodeId = "NotInvoicedCodeId";
											$thisGroupId = "ZZZNotInvoicedGroupId";
											$thisCodeName = "Aggregate of Other Codes";
											$thisBillingRate = 1;
											if ($DisplayNonProjectedCodes){
												$thisCodeId = $codeId;
												$thisGroupId = $CodesByID[$thisCodeId]->groupId;
												$thisCodeName = $CodesByID[$codeId]->name;
												if (!$GroupsByID[$thisGroupId]->colorBorder && $CondenseGroup){
													$thisCodeId = "NotInvoicedCodeId";
													$thisGroupId = "ZZZNotInvoicedGroupId";
													$thisCodeName = "Aggregate of Other Codes";
												}
											}
										}
									}
									$CodesDataProjectionsTotals[$thisCodeId] = bcadd($CodesDataProjectionsTotals[$thisCodeId],$thisHoursAllocatedMontly,2);
									if ($thisGroupId != "ZZZNotInvoicedGroupId"){
										$EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"]=bcadd($EmployeeDataProjections[$EmployeeName][$thisCodeId]["totalHours"],$thisHoursAllocatedMontly,2);
										$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["billingRate"]=$thisBillingRate;
										$thisBilling = bcmul($thisHoursAllocatedMontly,$thisBillingRate,2);
										$CodesDataProjectionsByGroupId[$GroupIdSeed.$thisGroupId][$thisCodeName]=$thisCodeId;
										if(!$isExcludedFromProjections && !$isUnbillable){
											$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalHours"],$thisHoursAllocatedMontly,2);
											$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["totalBilling"],$thisBilling,2);
											$CodesDataProjections[$thisCodeName]=$thisCodeId;
											$GroupIdSeed = ($GroupsByID[$thisGroupId]->colorBorder ? "AAA" : "BBB".$thisCodeName);
											$CodesDataTotalsBillingProjections[$thisCodeId] = bcadd($CodesDataTotalsBillingProjections[$thisCodeId],$thisBilling,2);
											$CodesDataProjectionsTotalsByGroupId[$thisGroupId] = bcadd($CodesDataProjectionsTotalsByGroupId[$thisGroupId],$thisHoursAllocatedMontly,2);
											$CodesDataProjectionsBillingTotalsByGroupId[$thisGroupId] = bcadd($CodesDataProjectionsBillingTotalsByGroupId[$thisGroupId],$thisBilling,2);
										}
										if ($isUnbillable){
											$EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"]=bcadd($EmployeeDataProjectionsByGroupId[$thisGroupId][$EmployeeName]["unbillableHours"],$thisHoursAllocatedMontly,2);
										}
										
										if ($CodesByID[$codeId]->name == "501"){
											$EmployeeAggregateDataProjections[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["501Hours"],$thisHoursAllocatedMontly,2);
											if (!$isSummaryExcluded){
												$TotalAggregateDataProjections["501Hours"]=bcadd($TotalAggregateDataProjections["501Hours"],$thisHoursAllocatedMontly,2);
											}
										}else{
											if ($isUnbillable){
												$EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalUnBillable"],$thisHoursAllocatedMontly,2);
												if (!$isSummaryExcluded){
													$TotalAggregateDataProjections["totalUnBillable"]=bcadd($TotalAggregateDataProjections["totalUnBillable"],$thisHoursAllocatedMontly,2);
												}
											}else{
												$EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateDataProjections[$EmployeeName]["totalBillable"],$thisHoursAllocatedMontly,2);
												if (!$isSummaryExcluded){
													$TotalAggregateDataProjections["totalBillable"]=bcadd($TotalAggregateDataProjections["totalBillable"],$thisHoursAllocatedMontly,2);
												}
											}
										}
									}

									
								}
							}
						}
						//print_pre($billingRateRatioByGroup);
						//print_pre($data["totalHoursPerBillingRatePerEmployee"]);
					//} end if NotInvoiced
				}
				ksort($CodesDataProjections);
				ksort($CodesDataProjectionsByGroupId);
				ksort($EmployeeDataProjections);
				/*
				foreach ($CodesDataTotalsBillingProjections as $codeId=>$amount){
					echo $CodesByID[$codeId]->name."=".$amount."<Br>";
				}
				*/
				//print_pre($CodesDataProjectionsByGroupId);
				//print_pre($EmployeeDataProjectionsByGroupId[12]["Kohler_Lisa"]);
			?>
			<div id='Projection' class='tab-content'>
				<fieldset class="twenty columns"><legend>Projected Monthly Hours For Remaining Months</legend>
					<?php include_once('views/_projections_Projections.php');?>
				</fieldset>
			</div>
			<div id='Comparison' class='tab-content' style='position:relative;'>
				<fieldset class="twenty columns"><legend>Comparison of Actual to Projected Summary</legend>
					<?php include_once('views/_projections_Comparison.php');?>
				</fieldset>
			</div>
			<div id='StaffAdjustments' class='tab-content'>
				<style>
					#StaffAdjustmentResults fieldset {vertical-align:top;}
					#StaffAdjustmentByStaffResults fieldset {vertical-align:top;}
				</style>
				<fieldset><legend>By Emplyee</legend>
					<div id="StaffAdjustmentByStaffResults" style='vertical-align: top;'>Nothing changed yet</div>
				</fieldset>
				<hr>
				<fieldset><legend>By Group</legend>
					<div id="StaffAdjustmentResults" style='vertical-align: top;'>Nothing changed yet</div>
				</fieldset>
			</div>
			<?php include_once('../gbs/views/_adjustmentsTab.php');?>

		</div><!--end tab-content-->
		<?php
		echo '<ul id="admin-tabs-bottom" class="tabs">';
				foreach ($TabListItems as $TabItem){
					echo $TabItem;
				}
		echo '</ul>';		
		?>
	</div>
	
<?php if (!$processTrigger){ //don't use javascript if being autoprocessed to get report files?>
<script>
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				employeeId = $("#SelectedEmployee").val(),
				groupId = $("#SelectedGroup").val(),
				codeId = $("#SelectedCode").val(),
				status = $("#SelectedStatus").val(),
				hideCodes = ($("#HideCodes").prop("checked") ? 1 : 0),
				DisplayNonProjectedCodes = ($("#DisplayNonProjectedCodes").prop("checked") ? 1 : 0),
				CondenseGroup = ($("#CondenseGroup").prop("checked") ? 1 : 0),
				onlySupervised = ($("#OnlySupervised").prop("checked") ? 1 : 0),

				filterValues = new Array(startDate,endDate,employeeId,groupId,codeId,status,hideCodes,onlySupervised,DisplayNonProjectedCodes,CondenseGroup);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder).($sourceFolder ? : "hours");?>/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&EmployeeID="+Filters[2]+"&GroupID="+Filters[3]+"&CodeID="+Filters[4]+"&Status="+Filters[5]+"&HideCodes="+Filters[6]+"&OnlySupervised="+Filters[7]+"&DisplayNonProjectedCodes="+Filters[8]+"&CondenseGroup="+Filters[9]+"&nav=<?php echo $navDropDown.$extraVariable;?>#<?php echo $currentTab;?>";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			$(".parameters").val('');
			updateFilters();
			
		});
		
		var table = $('.reportResults').DataTable({
//			dom: 'T<"clear">lfrtip',
//			tableTools: {
//				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
//			},				
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 10,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false,
//			"sScrollY" : "300",
			"scrollCollapse": true

		});
		var table2 = $('.reportFixedColumns').DataTable({
//			dom: 'T<"clear">',
//			"tableTools": {
//				"aButtons": [
//					"print"
//				]
//			},
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
 		});
		
	
	
		var tableProjections = $('.reportResultsProjections').DataTable({
//			dom: 'T<"clear">lfrtip',
//			tableTools: {
//				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
//			},				
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		var table2Projections = $('.reportFixedColumnsProjections').DataTable({
//			dom: 'T<"clear">',
//			"tableTools": {
//				"aButtons": [
//					"print"
//				]
//			},
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		var tableComparison = $('.reportResultsComparison').DataTable({
//			dom: 'T<"clear">lfrtip',
//			tableTools: {
//				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
//			},				
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		var table2Comparison = $('.reportFixedColumnsComparison').DataTable({
//			dom: 'T<"clear">',
//			"tableTools": {
//				"aButtons": [
//					"print"
//				]
//			},
		//	"sScrollX":	"100%",
			"bJQueryUI": false,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		$('.reportResults').wrap('<div class="dataTables_scroll" />');
		$('.reportResultsProjections').wrap('<div class="dataTables_scroll" />');
		$('.reportResultsComparison').wrap('<div class="dataTables_scroll" />');
		
		$('.reportFixedColumns').floatThead();
		$('.reportResults').floatThead({
			position: 'absolute'
		});

		$('.reportFixedColumnsProjections').floatThead();
		$('.reportResultsProjections').floatThead({
			position: 'absolute'
		});
		$('.reportFixedColumnsComparison').floatThead();
		$('.reportResultsComparison').floatThead({
			position: 'absolute'
		});

		$(".GroupCheckBox").on("click",function(e){
			// console.log(e);
			// e.preventDefault();
			var $this = $(this);
			var thisValue = $this.attr('value');
			// console.log(thisValue);
			$("#"+thisValue).click();

		});
		$(".deleteRow").on("click",function(){
			var $this = $(this),
				thisBox = $this.attr('data-box');
				thisBoxID = "#"+thisBox;
				$(thisBoxID).val('').change();
			
		});
		$('.toggleGroupColumns').on('click',function(){
			var $this = $(this),
				dataColumnsToToggle = $this.attr('data-columns'),
				dataTableToToggle = $this.attr('data-table'),
				dataColumns = dataColumnsToToggle.split("_"),
				startColumn = parseInt(dataColumns[0]),
				endColumn = parseInt(dataColumns[1]),
				currentText = $this.html();
				var i = startColumn;
				do {
					// Get the column API object
					if (dataTableToToggle == "Projections"){
						var column = tableProjections.column(i);
					}else{
						var column = table.column(i);
					}
					// Toggle the visibility
					column.visible( ! column.visible() );					
					var thisVisibility =column.visible();
					i++;
				}
				while (i <= endColumn);
				if (thisVisibility){
					$this.attr('title','Collapse Group Codes');
				}else{
					$this.attr('title','Show Group Codes');
				}
		});
		<?php if (count($columnsToHide)){?>
			var columnsToHide = new Array(<?php echo implode(",",$columnsToHide);?>);
			$.each(columnsToHide,function(key,val){
				var column = table.column(val);
				// Toggle the visibility
				column.visible( ! column.visible() );					
			});
		<?php }?>
		<?php if (count($columnsToHideProjections)){?>
			var columnsToHide = new Array(<?php echo implode(",",$columnsToHideProjections);?>);
			$.each(columnsToHide,function(key,val){
				var column = tableProjections.column(val);
				// Toggle the visibility
				column.visible( ! column.visible() );					
			});
		<?php }?>
		var columnsToHideByGroupId = <?php echo json_encode($columnsToHideByGroupId);?>;
		var columnsToHideByGroupIdProjections = <?php echo json_encode($columnsToHideByGroupIdProjections);?>;

		
		var tabs = $('ul.tabs');
		
		tabs.on('click',function(){
		
				$('.reportFixedColumns').floatThead('destroy');
				$('.reportResults').floatThead('destroy');

				$('.reportFixedColumnsProjections').floatThead('destroy');
				$('.reportResultsProjections').floatThead('destroy');

				$('.reportFixedColumnsComparison').floatThead('destroy');
				$('.reportResultsComparison').floatThead('destroy');
				
				$('.reportFixedColumns').floatThead();
				$('.reportResults').floatThead({
					position: 'absolute'
				});

				$('.reportFixedColumnsProjections').floatThead();
				$('.reportResultsProjections').floatThead({
					position: 'absolute'
				});
				
				$('.reportFixedColumnsComparison').floatThead();
				$('.reportResultsComparison').floatThead({
					position: 'absolute'
				});
				
				
		});
		
		tabs.each(function(i) {

			//Get all tabs
			var tab = $(this).find('> li > a');
			tab.click(function(e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {
					var thisClass= $(this).attr('class');
					tabsWithThisClass = "."+thisClass;
					//console.log(tabsWithThisClass);
					e.preventDefault();

					//Make Tab Active
					$(".active").removeClass('active');
					$(tabsWithThisClass).addClass('active');

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}

				}
			});
		});		
		$(".tab-content").hide();
        var hash = window.location.hash,
            tabHashes = new Array("#<?php echo implode("\",\"#",$TabHashes);?>"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
            setTab = function(tabid){
                var tab = $(".tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        }else{
			setTab("<?php echo $setTab;?>");
		}
		
		$(document).on('click','.groupHourSpan',function(){
			var $this = $(this),
				thisGroupId = $this.attr('data-groupid'),
				thisHourId = $this.attr('data-hourid'),
				thisInputId = "#Input"+thisHourId;
				$this.hide();
				$(thisInputId).show().focus();
		});
		var StaffAdjustments = {};

		var StaffAdjustmentsByEmployee = {};
		var CodesByID = new Array();
			<?php 
				foreach ($CodesByID as $codeId=>$codeObject){
					echo "CodesByID[".$codeId."]=".json_encode($codeObject)."; ";
				}
			?>
		var CodesByName = new Array();
			<?php 
				foreach ($CodesByName as $codeId=>$codeObject){
					echo "CodesByName['".$codeId."']=".json_encode($codeObject)."; ";
				}
			?>
		var GroupsByID = new Array();
			<?php 
				foreach ($GroupsByID as $groupId=>$groupObject){
					if (is_numeric($groupId)){
						echo "GroupsByID[".$groupId."]=".json_encode($groupObject)."; ";
					}
				}
			?>
		var GroupsByName = new Array();
			<?php 
				foreach ($GroupsByName as $groupId=>$groupObject){
					echo "GroupsByName['".$groupId."']=".json_encode($groupObject)."; ";
				}
			?>
		//console.log(GroupsByID);
		var staffAdjustmentByGroup = function(adjustmentObj){
				var StaffAdjustments = adjustmentObj;
				var innerDisplay = "";
				$.each(StaffAdjustments,function(groupName,emplObj){
					thisGroupBackgroundColor = GroupsByName[groupName].colorBackground;
					thisGroupBorderColor = GroupsByName[groupName].colorBorder;
					if (groupName == "GBS Dept unbillables"){groupName = "501 Hours";}
					innerDisplay = innerDisplay+"<fieldset style='background-color:"+thisGroupBackgroundColor+";'><legend style='background-color:"+thisGroupBackgroundColor+";border:1pt solid "+thisGroupBorderColor+";border-bottom:0pt;'>"+groupName+"</legend>";
						$.each(emplObj,function(emplName,changeObj){
							var itemsCount = Object.keys(changeObj).length;
							//console.log(Object.keys(changeObj).length);
							var actualNumber = 0,
								totalChanged = 0;
							if (itemsCount > 0){
								innerDisplay = innerDisplay+"<fieldset style='background-color:white;'><legend style='background-color:white;border:1pt solid "+thisGroupBorderColor+";border-bottom:0pt;'>"+emplName.replace("_",", ")+"</legend>";
									$.each(changeObj,function(codeName,hourChange){
										thisCodeDescription = CodesByName[codeName].description;
										hourChange = hourChange.replace("font-style:italic;","");
										//actualNumber = Number(hourChange.replace(/[^0-9\.-]+/g,""));
										var regExp = /\(([^)]+)\)/;
										var matches = regExp.exec(hourChange);
										//matches[1] contains the value between the parentheses
										actualNumber = Number(matches[1]);
										
										totalChanged = totalChanged+actualNumber;
										innerDisplay = innerDisplay+"<span title='"+thisCodeDescription+"'>"+codeName+"</span>:"+hourChange+"<br>";
										
									});
									var totalSpanColor = "black",
										balanceText = totalChanged;
									if (totalChanged < 0){totalSpanColor="red";}
									if (totalChanged > 0){totalSpanColor="green";balanceText = "+"+balanceText}
									innerDisplay = innerDisplay+"<span style='border-top:1pt solid black;'>Balance: <span style='color:"+totalSpanColor+"'>"+balanceText+"</span></span>";
									
								innerDisplay = innerDisplay+"</fieldset>";
							}
						});
					innerDisplay = innerDisplay+"</fieldset>";
				});
				$("#StaffAdjustmentResults").html(innerDisplay);
			
		};
		
		var staffAdjustmentByEmployee = function(adjustmentObj){
			var StaffAdjustmentsByEmployee = adjustmentObj;
			var innerDisplay = "";
			$.each(StaffAdjustmentsByEmployee,function(emplName,groupObj){
				var groupCount = Object.keys(groupObj).length,
					itemsCount = 0;
				//get number of objects to decide if to hide or note
				$.each(groupObj,function(groupName,changeObj){
					itemsCount = Object.keys(changeObj).length;
				});
				if (itemsCount > 0){
					innerDisplay = innerDisplay+"<fieldset><legend>"+emplName.replace("_",", ")+"</legend>";
						var actualNumber = 0,
							totalChanged = 0;
						$.each(groupObj,function(groupName,changeObj){
							thisGroupBackgroundColor = GroupsByName[groupName].colorBackground;
							thisGroupBorderColor = GroupsByName[groupName].colorBorder;
							var itemsCount = Object.keys(changeObj).length;
							//console.log(Object.keys(changeObj).length);
							if (itemsCount > 0){
								//innerDisplay = innerDisplay+"<fieldset style='background-color:white;'><legend style='background-color:white;border:1pt solid "+thisGroupBorderColor+";border-bottom:0pt;'>"+emplName+"</legend>";
									$.each(changeObj,function(codeName,hourChange){
										if (codeName.indexOf("_original")<1 && codeName.indexOf("_codeId")<1){ //exclude original data from display
											thisCodeDescription = CodesByName[codeName].description,
											hourChange = hourChange.replace("font-style:italic;","");
											var regExp = /\(([^)]+)\)/;
											var matches = regExp.exec(hourChange);
											//matches[1] contains the value between the parentheses
											actualNumber = Number(matches[1]);
											totalChanged = totalChanged+actualNumber;
											innerDisplay = innerDisplay+"<span title='"+groupName+": "+thisCodeDescription+"' style='border:1pt solid "+thisGroupBorderColor+";background-color:"+thisGroupBackgroundColor+";'>"+codeName+":"+hourChange+"</span><br>";
										}
									});
									
								//innerDisplay = innerDisplay+"</fieldset>";
							}
						});
						var totalSpanColor = "black",
							balanceText = totalChanged;
						if (totalChanged < 0){totalSpanColor="red";}
						if (totalChanged > 0){totalSpanColor="green";balanceText = "+"+balanceText}
						innerDisplay = innerDisplay+"<span style='border-top:1pt solid black;'>Balance: <span style='color:"+totalSpanColor+"'>"+balanceText+"</span></span>";
						
					innerDisplay = innerDisplay+"</fieldset>";
				}
			});
			$("#StaffAdjustmentByStaffResults").html(innerDisplay);			
		};
		$("")
		$(document).on('blur','.groupHourInput',function(){
			$("#AdjustmentsSaveH3").show();
			var $this = $(this);
			//console.log($this.val());
			if ($this.val() == ""){
				newValue = 0;
			}else{
				newValue = parseFloat($this.val()).toFixed(2);
			}
				var	thisGroupId = $this.attr('data-groupid'), //(EmployeeLastNameFirst + groupid)
					thisHourId = $this.attr('data-hourid'),
					thisBillingRate = $this.attr('data-billingrate'),
					thisEmployeeName = $this.attr('data-employeename'),
					thisOriginalHour = $this.attr('data-originalhour'),
					thisNumberOfMonthsRemaining = $this.attr('data-numberofmonths'),
					isUnbillable = ($this.attr('data-isunbillable') > 0 ? 1 : 0),
					is501 = ($this.attr('data-is501') > 0 ? 1 : 0),
					isSummaryExcluded = ($this.attr('data-issummaryexcluded') > 0 ? 1 : 0),
					thisFontColor = $this.attr('data-fontcolor'),
					justGroupId = thisGroupId.replace(thisEmployeeName,""),
					justCodeId = thisHourId.replace(thisEmployeeName,""),
					thisCodeName = CodesByID[justCodeId].name,
					thisGroupName = GroupsByID[justGroupId].name,
					
					thisSpan = "#Span"+thisHourId,
					thisGroupSpan = "#"+thisGroupId,
					thisBillingSpan = "#Billing"+thisGroupId,
					thisUsedSpan = "#Used"+justGroupId,
					thisRemainingSpan = "#Remaining"+justGroupId,
					thisGroupFooterHours = "#GroupFooterHours"+justGroupId,
					thisGroupFooterBilling = "#GroupFooterBilling"+justGroupId,
					thisCodeFooterTD = "#CodeFooter"+justCodeId;
					
					thisUnbillableSpan = "#unbillableHours"+thisGroupId,
					
					thisPerWeekBillableTD = ".PerWeekBillable"+thisEmployeeName,
					thisPerDayBillableTD = ".PerDayBillable"+thisEmployeeName,
					thisFooterPerDayBillableTD = ".FooterPerDayBillable",
					thisFooterPerWeekBillableTD = ".FooterPerWeekBillable",
					thisTotalBillableTD = ".TotalBillable"+thisEmployeeName,
					thisFooterTotalBillableTD = ".FooterTotalBillable",

					thisPerWeekUnbillableTD = ".PerWeekUnbillable"+thisEmployeeName,
					thisPerDayUnbillableTD = ".PerDayUnbillable"+thisEmployeeName,
					thisFooterPerDayUnbillableTD = ".FooterPerDayUnbillable",
					thisFooterPerWeekUnbillableTD = ".FooterPerWeekUnbillable",
					thisTotalUnbillableTD = ".TotalUnbillable"+thisEmployeeName,
					thisFooterTotalUnbillableTD = ".FooterTotalUnbillable",
					
					thisPerWeek501TD = ".PerWeek501"+thisEmployeeName,
					thisPerDay501TD = ".PerDay501"+thisEmployeeName,
					thisFooterPerDay501TD = ".FooterPerDay501",
					thisFooterPerWeek501TD = ".FooterPerWeek501",
					thisTotal501TD = ".Total501"+thisEmployeeName,
					thisFooterTotal501TD = ".FooterTotal501",
					
					thisTotalWorkHoursTD = ".TotalWorkHours"+thisEmployeeName,
					thisFooterTotalWorkHoursTD = ".FooterTotalWorkHours",
					
					thisPerWeekCodeHoursTD = ".PerWeekCodeHours"+thisEmployeeName,
					thisPerDayCodeHoursTD = ".PerDayCodeHours"+thisEmployeeName,
					thisFooterPerDayCodeHoursTD = ".FooterPerDayCodeHours",
					thisFooterPerWeekCodeHoursTD = ".FooterPerWeekCodeHours",
					thisTotalCodeHoursTD = ".TotalCodeHours"+thisEmployeeName,
					thisFooterTotalCodeHoursTD = ".FooterTotalCodeHours",
					
					thisHoursLeftToBillTD = ".HoursLeftToBill"+thisEmployeeName,
					thisFooterHoursLeftToBillTD = ".FooterHoursLeftToBill",

					thisPercentBilledTD = ".PercentBilled"+thisEmployeeName,
					thisFooterPercentBilledTD = ".FooterPercentBilled",

					thisPercentUnbilledTD = ".PercentUnbilled"+thisEmployeeName,
					thisFooterPercentUnbilledTD = ".FooterPercentUnbilled",
					
					thisCodeProjectionsUsedPerMonth = "#codeProjectionsUsedPerMonth"+justCodeId,
					thisRemainingCodeBudget = "#remainingCodeBudget"+justCodeId,
					thisProjectedEndingCodeBalance = "#projectedEndingCodeBalance"+justCodeId,
					
					thisPerWeekCodeHoursActual = ".PerWeekCodeHoursActual"+thisEmployeeName,
					thisComparisonDifference = ".ComparisonDifference"+thisEmployeeName,
					thisComparisonDifference40Hrs = ".ComparisonDifference40Hrs"+thisEmployeeName,
					thisFooterComparisonDifference = ".FooterComparisonDifference",
					thisFooterComparisonDifference40Hrs = ".FooterComparisonDifference40Hrs",


					oldValue = parseFloat($(thisSpan).html());
				if (!isSummaryExcluded){
					
					oldPerWeekCodeHoursActual = parseFloat($(thisPerWeekCodeHoursActual).html());
					oldComparisonDifference = parseFloat($(thisComparisonDifference).html());
					oldComparisonDifference40Hrs = parseFloat($(thisComparisonDifference40Hrs).html());
					oldFooterComparisonDifference = parseFloat($(thisFooterComparisonDifference).html());
					oldFooterComparisonDifference40Hrs = parseFloat($(thisFooterComparisonDifference40Hrs).html());
				}
				if (!is501){	
					oldGroupValue = parseFloat($(thisGroupSpan).html());
					oldBillingValue = Number($(thisBillingSpan).html().replace(/[^0-9\.-]+/g,""));
					oldUsedValue = Number($(thisUsedSpan).html().replace(/[^0-9\.-]+/g,""));
					oldRemainingValue = Number($(thisRemainingSpan).html().replace(/[^0-9\.-]+/g,""));
					oldGroupFooterValue =parseFloat($(thisGroupFooterHours).html()); 
					oldGroupFooterBillingValue = Number($(thisGroupFooterBilling).html().replace(/[^0-9\.-]+/g,""));
				}
				oldCodeFooterValue =parseFloat($(thisCodeFooterTD).html()); 
				
				oldUnbillableValue = parseFloat($(thisUnbillableSpan).html());
				if (isNaN(oldUnbillableValue)){oldUnbillableValue = 0;}
				
				oldPerWeekBillableValue = parseFloat($(thisPerWeekBillableTD).html()); 
				oldFooterPerWeekBillableValue = parseFloat($(thisFooterPerWeekBillableTD).html()); 
				oldPerDayBillableValue = parseFloat($(thisPerDayBillableTD).html()); 
				oldFooterPerDayBillableValue = parseFloat($(thisFooterPerDayBillableTD).html()); 
				oldTotalBillableValue = parseFloat($(thisTotalBillableTD).html()); 
				oldFooterTotalBillableValue = parseFloat($(thisFooterTotalBillableTD).html()); 

				oldPerWeekUnbillableValue = parseFloat($(thisPerWeekUnbillableTD).html()); 
				oldFooterPerWeekUnbillableValue = parseFloat($(thisFooterPerWeekUnbillableTD).html()); 
				oldPerDayUnbillableValue = parseFloat($(thisPerDayUnbillableTD).html()); 
				oldFooterPerDayUnbillableValue = parseFloat($(thisFooterPerDayUnbillableTD).html()); 
				oldTotalUnbillableValue = parseFloat($(thisTotalUnbillableTD).html()); 
				oldFooterTotalUnbillableValue = parseFloat($(thisFooterTotalUnbillableTD).html()); 
				
				oldPerWeek501Value = parseFloat($(thisPerWeek501TD).html()); 
				oldFooterPerWeek501Value = parseFloat($(thisFooterPerWeek501TD).html()); 
				oldPerDay501Value = parseFloat($(thisPerDay501TD).html()); 
				oldFooterPerDay501Value = parseFloat($(thisFooterPerDay501TD).html()); 
				oldTotal501Value = parseFloat($(thisTotal501TD).html()); 
				oldFooterTotal501Value = parseFloat($(thisFooterTotal501TD).html()); 
				
				oldTotalWorkHoursValue = parseFloat($(thisTotalWorkHoursTD).html()); 
				oldFooterTotalWorkHoursValue = parseFloat($(thisFooterTotalWorkHoursTD).html()); 
				
				oldPerWeekCodeHoursValue = parseFloat($(thisPerWeekCodeHoursTD).html()); 
				oldFooterPerWeekCodeHoursValue = parseFloat($(thisFooterPerWeekCodeHoursTD).html()); 
				oldPerDayCodeHoursValue = parseFloat($(thisPerDayCodeHoursTD).html()); 
				oldFooterPerDayCodeHoursValue = parseFloat($(thisFooterPerDayCodeHoursTD).html()); 
				oldTotalCodeHoursValue = parseFloat($(thisTotalCodeHoursTD).html()); 
				oldFooterTotalCodeHoursValue = parseFloat($(thisFooterTotalCodeHoursTD).html()); 
				
				oldHoursLeftToBillValue = parseFloat($(thisHoursLeftToBillTD).html()); 
				oldFooterHoursLeftToBillValue = parseFloat($(thisFooterHoursLeftToBillTD).html()); 
				
				oldPercentBilledValue = parseFloat($(thisPercentBilledTD).html()); 
				oldFooterPercentBilledValue = parseFloat($(thisFooterPercentBilledTD).html());
				
				oldPercentUnbilledValue = parseFloat($(thisPercentUnbilledTD).html()); 
				oldFooterPercentUnbilledValue = parseFloat($(thisFooterPercentUnbilledTD).html());

				newFooterTotalUnbillableValue = oldFooterTotalUnbillableValue;
				
				if (newValue != oldValue){
					var changeAmount = newValue-oldValue;
					if (is501){
						newPerWeek501Value = oldPerWeek501Value+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newFooterPerWeek501Value = oldFooterPerWeek501Value+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newPerDay501Value = oldPerDay501Value+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get daily average per month
						newFooterPerDay501Value = oldFooterPerDay501Value+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get weekly average per month
						newTotal501Value = oldTotal501Value+changeAmount;
						newFooterTotal501Value = oldFooterTotal501Value+changeAmount;
						
						if (newPerWeek501Value > <?php echo $numberOfHoursPerWeekLimit;?>){colorPerWeek = "red";}else{colorPerWeek = "black";}
						if (newPerDay501Value > <?php echo $numberOfHoursPerDayLimit;?>){colorPerDay = "red";}else{colorPerDay = "black";}
						$(thisPerWeek501TD).html(newPerWeek501Value.toFixed(2));
						$(thisPerWeek501TD).css("color",colorPerWeek);
						$(thisFooterPerWeek501TD).html(newFooterPerWeek501Value.toFixed(2));
						$(thisPerDay501TD).html(newPerDay501Value.toFixed(2));
						$(thisPerDay501TD).css("color",colorPerDay);
						$(thisFooterPerDay501TD).html(newFooterPerDay501Value.toFixed(2));
						$(thisTotal501TD).html(newTotal501Value.toFixed(2));
						$(thisFooterTotal501TD).html(newFooterTotal501Value.toFixed(2));						
						
					}else if (isUnbillable){
						newUnbillableValue = oldUnbillableValue+changeAmount;
						$(thisUnbillableSpan).html(newUnbillableValue);
						
						newPerWeekUnbillableValue = oldPerWeekUnbillableValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newFooterPerWeekUnbillableValue = oldFooterPerWeekUnbillableValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newPerDayUnbillableValue = oldPerDayUnbillableValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get daily average per month
						newFooterPerDayUnbillableValue = oldFooterPerDayUnbillableValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get weekly average per month
						newTotalUnbillableValue = oldTotalUnbillableValue+changeAmount;
						newFooterTotalUnbillableValue = oldFooterTotalUnbillableValue+changeAmount;
						
						if (newPerWeekUnbillableValue > <?php echo $numberOfHoursPerWeekLimit;?>){colorPerWeek = "red";}else{colorPerWeek = "black";}
						if (newPerDayUnbillableValue > <?php echo $numberOfHoursPerDayLimit;?>){colorPerDay = "red";}else{colorPerDay = "black";}
						$(thisPerWeekUnbillableTD).html(newPerWeekUnbillableValue.toFixed(2));
						$(thisPerWeekUnbillableTD).css("color",colorPerWeek);
						$(thisFooterPerWeekUnbillableTD).html(newFooterPerWeekUnbillableValue.toFixed(2));
						$(thisPerDayUnbillableTD).html(newPerDayUnbillableValue.toFixed(2));
						$(thisPerDayUnbillableTD).css("color",colorPerDay);
						$(thisFooterPerDayUnbillableTD).html(newFooterPerDayUnbillableValue.toFixed(2));
						$(thisTotalUnbillableTD).html(newTotalUnbillableValue.toFixed(2));
						$(thisFooterTotalUnbillableTD).html(newFooterTotalUnbillableValue.toFixed(2));						
						
					}else{
						oldCodeProjectionsUsedPerMonthValue = Number($(thisCodeProjectionsUsedPerMonth).html().replace(/[^0-9\.-]+/g,""));
						oldRemainingCodeBudgetValue = Number($(thisRemainingCodeBudget).html().replace(/[^0-9\.-]+/g,""));
						oldProjectedEndingCodeBalance = Number($(thisProjectedEndingCodeBalance).html().replace(/[^0-9\.-]+/g,""));
						
						
						newGroupValue = oldGroupValue+changeAmount;
						newBillingValue = oldBillingValue+(changeAmount*thisBillingRate);
						newUsedValue = oldUsedValue+(changeAmount*thisBillingRate);
						newRemainingValue = oldRemainingValue-(changeAmount*thisBillingRate);
						newRemainingValueTotal = oldRemainingValue-(changeAmount*thisBillingRate*thisNumberOfMonthsRemaining);
						newGroupFooterValue = oldGroupFooterValue+changeAmount;
						newGroupFooterBillingValue = oldGroupFooterBillingValue+(changeAmount*thisBillingRate);
						
						newPerWeekBillableValue = oldPerWeekBillableValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newFooterPerWeekBillableValue = oldFooterPerWeekBillableValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
						newPerDayBillableValue = oldPerDayBillableValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get daily average per month
						newFooterPerDayBillableValue = oldFooterPerDayBillableValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get weekly average per month
						newTotalBillableValue = oldTotalBillableValue+changeAmount;
						newFooterTotalBillableValue = oldFooterTotalBillableValue+changeAmount;
						newCodeProjectionsUsedPerMonthValue = oldCodeProjectionsUsedPerMonthValue+(changeAmount*thisBillingRate);			
						newProjectedEndingCodeBalance = oldRemainingCodeBudgetValue-(newCodeProjectionsUsedPerMonthValue*thisNumberOfMonthsRemaining);

						
						if (newRemainingValueTotal < 0){colorRemainingSpan = "red";}else{colorRemainingSpan = "black";}
						if (newPerWeekBillableValue > <?php echo $numberOfHoursPerWeekLimit;?>){colorPerWeek = "red";}else{colorPerWeek = "black";}
						if (newPerDayBillableValue > <?php echo $numberOfHoursPerDayLimit;?>){colorPerDay = "red";}else{colorPerDay = "black";}
						if (newProjectedEndingCodeBalance < 0){colorCodeBalance = "red";}else{colorCodeBalance = "black";}
						$(thisGroupSpan).html(newGroupValue);
						$(thisBillingSpan).html("$"+newBillingValue.toLocaleString());
						$(thisUsedSpan).html("$"+newUsedValue.toLocaleString());
						$(thisRemainingSpan).html("$"+newRemainingValueTotal.toLocaleString());
						$(thisRemainingSpan).css("color",colorRemainingSpan);
						$(thisRemainingSpan).attr("title","Overall Budget Remaining:  Monthly balance remaining in budget unused by staff $"+newRemainingValue.toLocaleString()+" x "+thisNumberOfMonthsRemaining+" remaining months.");
						$(thisGroupFooterHours).html(newGroupFooterValue);
						$(thisGroupFooterBilling).html("$"+newGroupFooterBillingValue.toLocaleString());
						$(thisPerWeekBillableTD).html(newPerWeekBillableValue.toFixed(2));
						$(thisPerWeekBillableTD).css("color",colorPerWeek);
						$(thisFooterPerWeekBillableTD).html(newFooterPerWeekBillableValue.toFixed(2));
						$(thisPerDayBillableTD).html(newPerDayBillableValue.toFixed(2));
						$(thisPerDayBillableTD).css("color",colorPerDay);
						$(thisFooterPerDayBillableTD).html(newFooterPerDayBillableValue.toFixed(2));
						$(thisTotalBillableTD).html(newTotalBillableValue.toFixed(2));
						$(thisFooterTotalBillableTD).html(newFooterTotalBillableValue.toFixed(2));

						$(thisCodeProjectionsUsedPerMonth).html("$"+newCodeProjectionsUsedPerMonthValue.toLocaleString());
						$(thisProjectedEndingCodeBalance).html("$"+newProjectedEndingCodeBalance.toLocaleString());
						$(thisProjectedEndingCodeBalance).css("color",colorCodeBalance);
						$(thisProjectedEndingCodeBalance).attr("title","Projected Ending Balance: $"+oldRemainingCodeBudgetValue.toLocaleString()+" - $"+(newCodeProjectionsUsedPerMonthValue*thisNumberOfMonthsRemaining).toLocaleString());
					}//end is unbillable
					
					//applies regardless of billable or nonBillable
					newCodeFooterValue = oldCodeFooterValue+changeAmount;
					$(thisCodeFooterTD).html(newCodeFooterValue);

					if (!is501){
						newTotalWorkHoursValue = oldTotalWorkHoursValue+changeAmount;
						newFooterTotalWorkHoursValue = oldFooterTotalWorkHoursValue+changeAmount;
						$(thisTotalWorkHoursTD).html(newTotalWorkHoursValue.toFixed(2));
						$(thisFooterTotalWorkHoursTD).html(newFooterTotalWorkHoursValue.toFixed(2));
					}
					
					newPerWeekCodeHoursValue = oldPerWeekCodeHoursValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
					newFooterPerWeekCodeHoursValue = oldFooterPerWeekCodeHoursValue+(changeAmount/<?php echo $numberOfWorkWeeksPerMonth;?>); //get weekly average per month
					newPerDayCodeHoursValue = oldPerDayCodeHoursValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get daily average per month
					newFooterPerDayCodeHoursValue = oldFooterPerDayCodeHoursValue+(changeAmount/<?php echo $numberOfWorkDaysPerMonth;?>); //get weekly average per month
					newTotalCodeHoursValue = oldTotalCodeHoursValue+changeAmount;
					newFooterTotalCodeHoursValue = oldFooterTotalCodeHoursValue+changeAmount;
					
					if (newPerWeekCodeHoursValue > <?php echo $numberOfHoursPerWeekLimit;?>){colorPerWeek = "red";}else{colorPerWeek = "black";}
					if (newPerDayCodeHoursValue > <?php echo $numberOfHoursPerDayLimit;?>){colorPerDay = "red";}else{colorPerDay = "black";}
					$(thisPerWeekCodeHoursTD).html(newPerWeekCodeHoursValue.toFixed(2));
					$(thisPerWeekCodeHoursTD).css("color",colorPerWeek);
					$(thisFooterPerWeekCodeHoursTD).html(newFooterPerWeekCodeHoursValue.toFixed(2));
					$(thisPerDayCodeHoursTD).html(newPerDayCodeHoursValue.toFixed(2));
					$(thisPerDayCodeHoursTD).css("color",colorPerDay);
					$(thisFooterPerDayCodeHoursTD).html(newFooterPerDayCodeHoursValue.toFixed(2));
					$(thisTotalCodeHoursTD).html(newTotalCodeHoursValue.toFixed(2));
					$(thisFooterTotalCodeHoursTD).html(newFooterTotalCodeHoursValue.toFixed(2));						

					if (!isSummaryExcluded){
						changeLeftToBill = parseFloat(oldPerDayCodeHoursValue-newPerDayCodeHoursValue).toFixed(2);
						$(thisHoursLeftToBillTD).html((<?php echo $numberOfHoursPerDayLimit;?>-newPerDayCodeHoursValue).toFixed(2));
						newFooterHoursLeftToBillValue = parseFloat(oldFooterHoursLeftToBillValue)+parseFloat(changeLeftToBill);

						$(thisFooterHoursLeftToBillTD).html(newFooterHoursLeftToBillValue);
						//set the percentages
						if (is501){
							newPercentBilled = (100*(oldTotalBillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";
							newPercentUnbilled = (100*(oldTotalUnbillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";
							
							newFooterPercentBilled = (100*(oldFooterTotalBillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
							newFooterPercentUnbilled = (100*(oldFooterTotalUnbillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
						}else if (isUnbillable){
							newPercentBilled = (100*(oldTotalBillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";
							newPercentUnbilled = (100*(newTotalUnbillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";
							
							newFooterPercentBilled = (100*(oldFooterTotalBillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
							newFooterPercentUnbilled = (100*(newFooterTotalUnbillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
						}else{
							newPercentBilled = (100*(newTotalBillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";
							newPercentUnbilled = (100*(oldTotalUnbillableValue/newTotalCodeHoursValue)).toFixed(2)+"%";

							newFooterPercentBilled = (100*(oldFooterTotalBillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
							newFooterPercentUnbilled = (100*(newFooterTotalUnbillableValue/newFooterTotalCodeHoursValue)).toFixed(2)+"%";
						}
						$(thisPercentBilledTD).html(newPercentBilled);
						$(thisPercentUnbilledTD).html(newPercentUnbilled);
						$(thisFooterPercentBilledTD).html(newFooterPercentBilled);
						$(thisFooterPercentUnbilledTD).html(newFooterPercentUnbilled);
						
						newComparisonDifference = (oldPerWeekCodeHoursActual-newPerWeekCodeHoursValue);
						$(thisComparisonDifference).html(newComparisonDifference.toFixed(2));
						newComparisonDifference40Hrs = parseFloat(<?php echo $numberOfHoursPerWeekLimit;?>-newPerWeekCodeHoursValue);
						$(thisComparisonDifference40Hrs).html(newComparisonDifference40Hrs.toFixed(2));
						newFooterComparisonDifference = oldFooterComparisonDifference-(oldComparisonDifference-newComparisonDifference);
						$(thisFooterComparisonDifference).html(newFooterComparisonDifference.toFixed(2));
						newFooterComparisonDifference40Hrs = oldFooterComparisonDifference40Hrs-(oldComparisonDifference-newComparisonDifference);
						$(thisFooterComparisonDifference40Hrs).html(newFooterComparisonDifference40Hrs.toFixed(2));
					}else{
						//alert("isExcluded");
					}
				}
				if (isUnbillable){
					var colorSpan = thisFontColor.replace("color:","");
					var changedFromOriginal = newValue-thisOriginalHour;
					var specialText = "";
					if (changedFromOriginal>0){specialText = "<span style='color:green'> (+"+changedFromOriginal.toFixed(2)+")</span>";}
					if (changedFromOriginal<0){specialText = "<span style='color:red'> ("+changedFromOriginal.toFixed(2)+")</span>";}
					if (changedFromOriginal==0){specialText = "";}
				}else{
					var colorSpan = "black";
					var changedFromOriginal = newValue-thisOriginalHour;
					var specialText = "";
					if (changedFromOriginal>0){colorSpan = "green";specialText = " (+"+changedFromOriginal.toFixed(2)+")";}
					if (changedFromOriginal<0){colorSpan = "red";specialText = " ("+changedFromOriginal.toFixed(2)+")";}
					if (changedFromOriginal==0){colorSpan = "black";specialText = "";}
				}
				$(thisSpan).html(newValue+specialText).show();
				$(thisSpan).css("color",colorSpan);
				
				//now set for capturing all changes on StaffAdjustments tab
				var deleteProperty = false;
				if (changedFromOriginal==0){colorSpan = "black";specialText = " unchanged";deleteProperty = true;}
				colorSpan = colorSpan.replace("#000000","black");
				specialText = "<span style='color:"+colorSpan+";'>"+specialText+"</span>";
				$this.hide();
				//byGroupName
				if (StaffAdjustments[thisGroupName] !== null && typeof StaffAdjustments[thisGroupName] === 'object'){
					//Object.defineProperty(StaffAdjustments[thisGroupName], "employeeName", {value : thisEmployeeName, writable : true, enumerable : true, configurable : true});				
					if (StaffAdjustments[thisGroupName][thisEmployeeName] !== null && typeof StaffAdjustments[thisGroupName][thisEmployeeName] === 'object'){
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustments[thisGroupName][thisEmployeeName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
						}else{
							delete StaffAdjustments[thisGroupName][thisEmployeeName][thisCodeName];	
						}
					}else{
						StaffAdjustments[thisGroupName][thisEmployeeName] = {};
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustments[thisGroupName][thisEmployeeName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
						}
					}
				}else{
					StaffAdjustments[thisGroupName] = {};
					//Object.defineProperty(StaffAdjustments[thisGroupName], "employeeName", {value : thisEmployeeName, writable : true, enumerable : true, configurable : true});				
					if (StaffAdjustments[thisGroupName][thisEmployeeName] !== null && typeof StaffAdjustments[thisGroupName][thisEmployeeName] === 'object'){
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustments[thisGroupName][thisEmployeeName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
						}else{
							delete StaffAdjustments[thisGroupName][thisEmployeeName][thisCodeName];	
						}
					}else{
						StaffAdjustments[thisGroupName][thisEmployeeName] = {};
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustments[thisGroupName][thisEmployeeName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
						}
					}
					
				}
				//byEmployee
				
				var thisCodeNameOriginalHour = thisCodeName+"_originalHour"; //used to reconstruct original information for loading saved projections
				var thisCodeNameId = justCodeId+"_codeId"; //used to reconstruct original information for loading saved projections
				if (StaffAdjustmentsByEmployee[thisEmployeeName] !== null && typeof StaffAdjustmentsByEmployee[thisEmployeeName] === 'object'){
					//Object.defineProperty(StaffAdjustments[thisGroupName], "employeeName", {value : thisEmployeeName, writable : true, enumerable : true, configurable : true});				
					if (StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] !== null && typeof StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] === 'object'){
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameOriginalHour, {value : thisOriginalHour, writable : true, enumerable : true, configurable : true});											
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameId, {value : newValue, writable : true, enumerable : true, configurable : true});											
						}else{
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeName];	
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeNameOriginalHour];	
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeNameId];	
						}
					}else{
						StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] = {};
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameOriginalHour, {value : thisOriginalHour, writable : true, enumerable : true, configurable : true});											
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameId, {value : newValue, writable : true, enumerable : true, configurable : true});											
						}
					}
				}else{
					StaffAdjustmentsByEmployee[thisEmployeeName] = {};
					//Object.defineProperty(StaffAdjustmentsByEmployee[thisGroupName], "employeeName", {value : thisEmployeeName, writable : true, enumerable : true, configurable : true});				
					if (StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] !== null && typeof StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] === 'object'){
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameOriginalHour, {value : thisOriginalHour, writable : true, enumerable : true, configurable : true});											
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameId, {value : newValue, writable : true, enumerable : true, configurable : true});											
						}else{
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeName];	
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeNameOriginalHour];	
							delete StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName][thisCodeNameId];	
						}
					}else{
						StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName] = {};
						if (!deleteProperty){
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeName, {value : specialText, writable : true, enumerable : true, configurable : true});				
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameOriginalHour, {value : thisOriginalHour, writable : true, enumerable : true, configurable : true});											
							Object.defineProperty(StaffAdjustmentsByEmployee[thisEmployeeName][thisGroupName], thisCodeNameId, {value : newValue, writable : true, enumerable : true, configurable : true});											
						}
					}
					
				}

				//By Group
				staffAdjustmentByGroup(StaffAdjustments);
				
				//byEmployee
				staffAdjustmentByEmployee(StaffAdjustmentsByEmployee);
				
				//now save it all incase they should be saved
				var	adjustmentData = {};
				adjustmentData["action"] = "saveProjectionAdjustments";
				adjustmentData["adjustmentTimeStamp"] = $("#SavedAdjustmentsTimeStamp").val();
				adjustmentData["adjustmentByEmployee"] = StaffAdjustmentsByEmployee;
				adjustmentData["adjustmentByGroup"] = StaffAdjustments;
				adjustmentData["adjustmentQueryString"] = "<?php foreach($_GET as $key=>$val){$QueryString .= "&".$key."=".$val;} echo $QueryString;?>";
				adjustmentData["adjustmentNotes"] = $("#adjustmentNotes").val();
				adjustmentData["adjustmentInvoiceGroupsToHide"] = $("#invoiceGroupsToHide").val();
				//console.log(JSON.stringify(adjustmentData));
				$.ajax({
					url: "ApiHoursManagement.php",
					type: "PUT",
					data: JSON.stringify(adjustmentData),
					success: function(thisData){
							//console.log(thisData);
							//location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						console.log(message);
					}
				});
				
		});
		var loadStaffAdjustmentByEmployee = function(adjustmentObj){
			var StaffAdjustmentsByEmployee = adjustmentObj;
			
			$.each(StaffAdjustmentsByEmployee,function(emplName,groupObj){
				var groupCount = Object.keys(groupObj).length,
					itemsCount = 0;
				//get number of objects to decide if to hide or note
				$.each(groupObj,function(groupName,changeObj){
					itemsCount = Object.keys(changeObj).length;
				});
				if (itemsCount > 0){
						var actualNumber = 0,
							totalChanged = 0;
						$.each(groupObj,function(groupName,changeObj){
							var itemsCount = Object.keys(changeObj).length;
							//console.log(Object.keys(changeObj).length);
							if (itemsCount > 0){
									$.each(changeObj,function(codeName,newVal){
										if (codeName.indexOf("_codeId")>1){//getCodeId
											var codeNameParts = codeName.split("_"),
												codeId = codeNameParts[0],
												spanId = "#Span"+emplName+codeId,
												inputId = "#Input"+emplName+codeId;
												$(spanId).click();
												$(inputId).val(newVal);
												$(inputId).blur();
										}
									});
							}
						});
				}
			});
		};
		<?php if ($adjustmentByGroup){?>
			var StaffAdjustments = <?php echo $adjustmentByGroup;?>;
			staffAdjustmentByGroup(StaffAdjustments);
		<?php }?>
		<?php if ($adjustmentByEmployee){?>
			var StaffAdjustmentsByEmployee = <?php echo $adjustmentByEmployee;?>;
			staffAdjustmentByEmployee(StaffAdjustmentsByEmployee);
			loadStaffAdjustmentByEmployee(StaffAdjustmentsByEmployee);
		<?php }?>
		
		
		$("#saveProjectionAdjustments").on('click',function(e){
			e.preventDefault();
			if ($("#SavedAdjustmentsName").val()){
				var	data = {};
				data["action"] = "nameProjectionAdjustments";
				data["adjustmentTimeStamp"] = $("#SavedAdjustmentsTimeStamp").val();
				data["adjustmentName"] = $("#SavedAdjustmentsName").val();
				data["adjustmentNotes"] = $("#adjustmentNotes").val();
				data["adjustmentInvoiceGroupsToHide"] = $("#invoiceGroupsToHide").val();
				console.log(data);
				$.ajax({
					url: "ApiHoursManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(resultData){
							window.location.href='<?php echo $CurrentServer.$adminFolder."hours/?".$QueryString;?>&projectionTimeStamp='+$("#SavedAdjustmentsTimeStamp").val();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$(thisAlert).show().html(message).addClass("alert");
					}
				});
			}else{
				alert('You must these adjustments a unique name');
			}
			
		});
		$("#loadSavedProjections").on('change',function(){
			window.location.href='<?php echo $CurrentServer.$adminFolder."hours/?".$QueryString;?>&CondenseGroup=0&projectionTimeStamp='+$(this).val();
		});
		var columnsSummaryToHideByGroupId = <?php echo json_encode($columnsSummaryToHideByGroupId);?>;
		var columnsSummaryToHideByGroupIdProjections = <?php echo json_encode($columnsSummaryToHideByGroupIdProjections);?>;
		
		$(".invoiceGroups").on('click',function(e){
			// console.log(this);
			var $this = $(this);
				thisGroupId = $this.attr('data-groupid'),
				thisInvoiceGroupCheckBoxId = "#invoiceGroupCheckBox"+thisGroupId;
				thisInvoiceGroupCheckBoxValue = $(thisInvoiceGroupCheckBoxId).prop('checked');

				invoiceGroupsToHideVal = $("#invoiceGroupsToHide").val();
				var columnsToHide = columnsToHideByGroupId[thisGroupId];
				var columnsSummaryToHide = columnsSummaryToHideByGroupId[thisGroupId];
				if(!e.originalEvent){
					thisInvoiceGroupCheckBoxValue = !thisInvoiceGroupCheckBoxValue;
				}
				if (thisInvoiceGroupCheckBoxValue){
					 if(e.originalEvent){
					 	$(thisInvoiceGroupCheckBoxId).prop('checked',false);
					 }
					 $("#invoiceGroupsToHide").val(invoiceGroupsToHideVal+"|"+thisGroupId+"|");
					$.each(columnsToHide,function(key,val){
						var column = table.column(val);
						var columnProjections= tableProjections.column(val);
						
						<?php if ($CondenseGroup){?>
							column.visible(true);
							columnProjections.visible(true);
						<?php }?>
						column.visible(false);					
						columnProjections.visible(false);					
					});
					 
				}else{
					 if(e.originalEvent){
					 	$(thisInvoiceGroupCheckBoxId).prop('checked',true);
					 }
					 $("#invoiceGroupsToHide").val(invoiceGroupsToHideVal.replace("|"+thisGroupId+"|",""));
					$.each(columnsToHide,function(key,val){
						var column = table.column(val);
						var columnProjections= tableProjections.column(val);
						column.visible(false);					
						columnProjections.visible(false);					
					});
					<?php if ($CondenseGroup){?>
						$.each(columnsSummaryToHide,function(key,val){
							var column = table.column(val);
							var columnProjections= tableProjections.column(val);
							column.visible(true);					
							columnProjections.visible(true);					
						});
						
					<?php }else{?>
						$.each(columnsToHide,function(key,val){
							var column = table.column(val);
							var columnProjections= tableProjections.column(val);
							column.visible(true);					
							columnProjections.visible(true);					
						});
					<?php }?>

				}
		});
		<?php
			if ($invoiceGroupsToHide){
				$invoiceGroupsToHide = ltrim($invoiceGroupsToHide,"|");
				$invoiceGroupsToHide = rtrim($invoiceGroupsToHide,"|");
				$invoiceGroupsToHide = explode("||",$invoiceGroupsToHide);
				foreach ($invoiceGroupsToHide as $groupId){
					echo "$('#invoiceGroupDisplay".$groupId."').click();\r\n";
				}
			}
		?>
		
	});
</script>
<?php } //don't use javascript if being autoprocessed to get report files?>
