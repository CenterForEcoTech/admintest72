<?php
ini_set('max_execution_time',600);
include_once("../_config.php");
include_once("../_datatableModelBase.php");
//error_reporting(E_ALL);
$pageTitle = "Admin - Hours";
//print_pre($_SESSION);

$nav = (isset($_GET["nav"]) ? $_GET["nav"] : $sessionNav);
if ($nav){
    switch ($nav){
        case "manage-codes":
            $displayPage = "views/_manageCodes.php";
            break;
        case "manage-groups":
            $displayPage = "views/_manageGroups.php";
            break;
        case "manage-invoicing":
            $displayPage = "views/_manageInvoicing.php";
            break;
        case "manage-rates":
            $displayPage = "views/_manageBillingRates.php";
            break;
        case "manage-bullets":
            $displayPage = "views/_manageBullets.php";
            break;
        case "manage-farms":
            $displayPage = "views/_manageFarms.php";
            break;
        case "manage-hoursentry":
            $displayPage = "views/_manageHoursEntry.php";
            break;
        case "manage-submittedhours":
            $displayPage = "views/_manageSubmittedHours.php";
            break;
        case "check-submittedhours":
            $displayPage = "views/_checkSubmittedHours.php";
            break;
		case "reports":
			$displayPage = "_reportsTabs.php";
			break;
        case "report-bycode":
            $displayPage = "views/_reportByCode.php";
            break;
        case "report-bycodeProjection":
            $displayPage = "views/_reportByCodeProjections.php";
			$containerClass = " wideContainer";
            break;
        case "report-bygroup":
            $displayPage = "views/_reportByGroup.php";
            break;
        case "report-bystaff":
            $displayPage = "views/_reportByStaff.php";
            break;
        case "report-rawdata":
            $displayPage = "views/_manageRawData.php";
            break;
        case "salesforceConnect":
            $displayPage = "views/_connectToSalesForce.php";
            break;

        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container'.$containerClass.'">
<h1>Hours Administration</h1>';
include_once("_hoursMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Process Time: {$time}";
?>