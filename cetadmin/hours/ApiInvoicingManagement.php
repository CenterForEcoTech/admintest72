<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."InvoicingProvider.php");
$GBSProvider = new GBSProvider($dataConn);
$invoicingProvider = new InvoicingProvider($dataConn);


$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		$results = "Empty GET";
        header("HTTP/1.0 200 Success");
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		$action = trim($newRecord->action);
		if ($action == "MFEP_AllocationChange"){
			$response = $GBSProvider->invoiceTool_updateMFEPAllocations($newRecord,getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif ($action == "BillingRate_RemoveEmployee"){
			$response = $GBSProvider->invoiceTool_removeEmployeeFromBillingRate($newRecord,getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}elseif ($action == "BillingRate_Update"){
			$response = $GBSProvider->invoiceTool_updateBillingRates($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "BillingRate_AddEmployee"){
			$response = $GBSProvider->invoiceTool_addEmployeeFromBillingRate($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
			
		}elseif ($action == "invoice_update"){
			$response = $invoicingProvider->updateInvoicingCodes($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "invoice_add"){
			$response = $invoicingProvider->addInvoicingCodes($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "invoiceHistoryManualEntry"){
			$record = $newRecord;
			$record->invoiceDate = date("Y-m-1",strtotime($newRecord->invoiceDate));
			$response = $invoicingProvider->insertInvoicingAmount($newRecord,getAdminId());
			$results = $record;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "invoiceHistoryNonStaffAmountUpdate"){
			$response = $invoicingProvider->updateInvoiceHistoryNonStaffAmount($newRecord);
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "invoiceHistoryCodeAdjust"){
			$response = $invoicingProvider->insertInvoiceHistoryCodeAdjust($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");
		}elseif ($action == "deleteCodeAdjustment"){
			$response = $invoicingProvider->deleteCodeAdjustment($newRecord,getAdminId());
			$results = $response;
			header("HTTP/1.0 201 Created");			
		}
		
        break;
}
output_json($results);
die();
?>