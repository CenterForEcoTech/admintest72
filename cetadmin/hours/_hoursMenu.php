<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Hours)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "130";
	$invoicingToolsDropDownItemsTop = "130";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Hours))){?>
			<li><a id="manage-hoursentry" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Entry</a></li>
			<?php if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Hours))){?>
				<li><a id="manage-submittedhours" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-submittedhours&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Manage Submitted Hours</a></li>
				<li><a id="manage-codes" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-codes" class="button-link">Manage Codes</a></li>
				<li><a id="manage-groups" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-groups" class="button-link">Manage Groups</a></li>
				<li><a id="manage-invoicing" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-invoicing" class="button-link">Manage Invoicing</a></li>
				<li><a id="manage-rates" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=manage-rates" class="button-link">Manage Billing Rates</a></li>
			<?php }?>
			<li id="ReportMenuDropDown"><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>hours/?nav=reports" class="button-link">Reports</a>
				<?php ($nav != "reports" ? include('_reportsTabs.php') : "");?>
			</li>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "report-bycode":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-bycodeProjection":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-bygroup":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-bystaff":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-rawdata":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
		
		
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>