<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxBillingRate" ).combobox({
				select: function(event, ui){
					<?php if (!$billingRateComboBoxFuction){?>
					window.location = "?nav=manage-billingRate&BillingRateID="+$(ui.item).val();
					<?php }else{
						echo $billingRateComboBoxFuction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($billingRateComboBoxDivColumns ? $billingRateComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$billingRateComboBoxHideLabel){?>
				<label>Start typing billing rate name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxBillingRate parameters" name="billingRateId" id="billingRateId">
				<option value="">Select one...</option>
				<?php 
					foreach ($BillingRateByName as $name=>$billingRateInfo){
						echo "<option value=\"".$billingRateInfo->GBSBillingRate_ID."\"".($billingRateID==$billingRateInfo->GBSBillingRate_ID ? " selected" : "").">".$billingRateInfo->GBSBillingRate_InvoiceDisplayName."</option>";
					}
				?>
			  </select>
			</div>
		</div>