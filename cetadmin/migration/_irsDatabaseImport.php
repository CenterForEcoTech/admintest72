<?php
set_time_limit(600);
$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$starttime = $mtime;

	$file_folder = $_SERVER['DOCUMENT_ROOT']."/../../../irs_eo_files";
	$userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		if (!is_dir($file_folder)){
			if(mkdir($file_folder, 0700)){
				echo $file_folder." folder created<br>";
			}else{
				echo "There was an error creating the folder";
			}
		}
 
	$i = 1;
	while ($i < 5){
		$target_url = "http://www.irs.gov/file_source/pub/irs-soi/eo".$i.".zip";  
		$IRS_EOFile = "eo".$i;
		$file_zip = $file_folder."/".$IRS_EOFile.".zip";  
		$file_txt = $file_folder."/unzipped";  

		echo "<br>Starting<br>Target_url: $target_url";  
		//echo "<br>Headers stripped out";  

		// make the cURL request to $target_url  
		$ch = curl_init();  
		$fp = fopen("$file_zip", "w");  
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);  
		curl_setopt($ch, CURLOPT_URL,$target_url);  
		curl_setopt($ch, CURLOPT_FAILONERROR, true);  
		curl_setopt($ch, CURLOPT_HEADER,0);  
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);  
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);  
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);  
		curl_setopt($ch, CURLOPT_TIMEOUT, 600);  
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);   
		curl_setopt($ch, CURLOPT_FILE, $fp);  
		$page = curl_exec($ch);  
		if (!$page) {  
			echo "<br />cURL error number:" .curl_errno($ch);  
			echo "<br />cURL error:" . curl_error($ch);  
			exit;  
		}  
		curl_close($ch);  
		echo "<br>Downloaded file: $target_url";  
		//echo "<br>Saved as file: $file_zip";  
		echo "<br>About to unzip ...";  
		// Un zip the file  
		$zip = new ZipArchive;  
		if (! $zip) {  
			echo "<br>Could not make ZipArchive object.";  
			exit;  
		}  
		if($zip->open("$file_zip") != "true") {  
			echo "<br>Could not open $file_zip";  
		}  
		$zip->extractTo("$file_txt");  
		$zip->close();  
		echo "<br>Unzipped file to: $file_txt<br><br>";
	
		$i++;
	}

$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 
echo "<br>All files have been downloaded and saved locally.<br><br>";
echo "This download and extract took ".$totaltime." seconds.<br><Br>"; 

//Build the temporary table
$SQL_Build = "
	CREATE TABLE `irs_new` (
		`irs_ein` VARCHAR(9) NOT NULL, 
		`irs_name` VARCHAR(70) NOT NULL, 
		`irs_sortname` VARCHAR(50) NOT NULL, 
		`irs_address` VARCHAR(35) NOT NULL, 
		`irs_city` VARCHAR(25) NOT NULL, 
		`irs_state` VARCHAR(2) NOT NULL, 
		`irs_zipcode` VARCHAR(10) NOT NULL, 
		`irs_groupexemptionnumber` VARCHAR(5) NOT NULL, 
		`irs_subsectioncode` VARCHAR(2) NOT NULL, 
		`irs_affiliationcode` VARCHAR(1) NOT NULL, 
		`irs_classificationcode` VARCHAR(5) NOT NULL, 
		`irs_exemptstatuscode` VARCHAR(2) NOT NULL, 
		`irs_nteecode` VARCHAR(5) NOT NULL
	) ENGINE = MyISAM;";
echo "Build a temp table:<br>".$SQL_Build;
   
	if ($i == 5){
		echo "Now importing into database:<Br>";
		$i = 1;
		while ($i < 5){
			$IRS_EOFile = "eo".$i;
			$file_location = $file_folder."/unzipped/".$IRS_EOFile.".LST";

			$SQL_IRSMigrate = "
				LOAD DATA LOCAL INFILE '".$file_location."' INTO TABLE irs
				(@var1)
				SET 
				irs_ein=SUBSTR(@var1,1,9), 
				irs_name=SUBSTR(@var1,10,70), 
				irs_address=SUBSTR(@var1,115,35),
				irs_city=SUBSTR(@var1,150,22),
				irs_state=SUBSTR(@var1,172,2),
				irs_zipcode=SUBSTR(@var1,174,10),
				irs_groupexemptionnumber=SUBSTR(@var1,184,4),
				irs_subsectioncode=SUBSTR(@var1,188,2),
				irs_affiliationcode=SUBSTR(@var1,190,1),
				irs_classificationcode=SUBSTR(@var1,191,4),
				irs_exemptstatuscode=SUBSTR(@var1,214,2),
				irs_nteecode=SUBSTR(@var1,279,4),
				irs_sortname=SUBSTR(@var1,283,35);
			";
			echo "<hr>".$SQL_IRSMigrate."<hr>";
			$i++;
		}
		
		
	}
$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 

//rename databases:
$SQL_Rename="DROP TABLE IF EXISTS `irs_old`;RENAME TABLE `irs` TO `irs_old`;RENAME TABLE `irs_new` TO `irs`;";
echo "Renaming Databases: ".$SQL_Rename."<Br>";

echo "<br>Total execution of this migration took ".$totaltime." seconds.<br>"; 

?>