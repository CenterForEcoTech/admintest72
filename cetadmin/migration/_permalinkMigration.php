<?php
/**
 * Does the initial assigning of permalink pid's for merchants, organizations, and individuals. These are unique non-information-rich keys assigned to the
 * merchant for the purpose of linking to their public profile.
 *
 * At some future date, the members may be able to "trump" a pre-existing permalink with a more user-friendly one of their own.
 * However, existing permalinks will remain in-effect (because they are permanent).
 */

include_once("../../Global_Variables.php");
include_once("../../repository/mySqlProvider/PermaLinkProvider.php");
$permalinkProvider = new PermaLinkProviderImpl($mysqli);

$sqlString = "
    SELECT
        MerchantInfo_ID as entity_id,
        'merchant' as entity_name
    FROM merchant_info
    WHERE NOT EXISTS(
        SELECT 1
        FROM permalinks
        WHERE entity_name = 'merchant'
        AND entity_id = MerchantInfo_ID
    )
    UNION
    SELECT
        OrganizationInfo_ID as entity_id,
        'organization' as entity_name
    FROM organization_info
    WHERE NOT EXISTS(
        SELECT 1
        FROM permalinks
        WHERE entity_name = 'organization'
        AND entity_id = OrganizationInfo_ID
    )
    UNION
    SELECT
        IndividualInfo_ID as entity_id,
        'individual' as entity_name
    FROM individual_info
    WHERE NOT EXISTS(
        SELECT 1
        FROM permalinks
        WHERE entity_name = 'individual'
        AND entity_id = IndividualInfo_ID
    )";
$entities = MySqliHelper::get_result($mysqli, $sqlString);
foreach ($entities as $entityArray){
    $entity = (object)$entityArray;
    var_dump($entity);
    $request = new AddPermaLinkRequest();
    $request->setEntityId($entity->entity_id);
    $request->setEntityName($entity->entity_name);
    $permalinkProvider->tryAddPermalink($request);
}
?>