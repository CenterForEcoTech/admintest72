<?php include_once("../_config.php");?>
<html>
<head>
    <title>Migration Administration</title>
    <style type="text/css">
        dt {margin-top:10px;}
    </style>
</head>
<body>

<h1>Migration Options</h1>

	<dl>
		<dt><a href="?a=permalink-migration">Assign Default Permalinks to Merchants, Organizations, and Individuals that don't have one yet.</a>: </dt>
		<dd>
			This action assigns the initial permalink to merchants, organizations, and individuals that exist in the system using a randomized alphanumeric string.
			The idea is that this permalink will be available to be used, but at some future point, the user will be able to
			set one additional one that is more "user-friendly."
		</dd>
		<dt><a href="?a=nonprofit-migration">Core Nonprofit Refactoring Migration</a>: </dt>
		<dd>
			This action creates the Fiscal Sponsor nonprofits that are referenced by their sponsored charities
			if they do not already exist in our system.
		</dd>
		<dt><a href="?a=irs-EOMBF-migration">IRS Exempt Organization Master Business File Migration</a>: </dt>
		<dd>
			This action creates downloads regional files directly from the IRS and imports that data into our own database to be used for Nonprofit lookup.
		</dd>
	</dl>

	<h1>Action log</h1>
	<p>If an action was performed, the output will display below this line:</p>
	<hr solid>

	<?php
	$action = (isset($_GET["a"])) ? $_GET["a"] : "default";

	switch ($action){
		case "nonprofit-migration":
			include_once("_nonprofitMigration.php");
			break;
		case "permalink-migration":
			include_once("_permalinkMigration.php");
			break;
		case "irs-EOMBF-migration":
			include_once("_irsDatabaseImport.php");
			break;
		default:
			break;
	} ?>

</body>
</html>