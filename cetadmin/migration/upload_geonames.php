<?php
include_once("../_config.php");
ini_set("display_errors", "on");
ini_set("max_execution_time", 300); // 5 minutes
error_reporting(1);
include_once($rootFolder."_setupGeolookupConnection.php");
include_once($dbProviderFolder."Helper.php");

abstract class MigrationBase {
    protected $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    public function countRows($filename){
        $num = 0;
        $fp = fopen($filename, 'r');
        if ($fp) {
            while ( !feof($fp))
            {
                $line = fgets($fp);
                $num++;
            }

            fclose($fp);
        }
        return $num;
    }

    public function upload($filename, $maxRows = 25, $skipRows = 0){
        $fp = null;
        if ($skipRows){
            echo "skipping ".$skipRows." rows<br>";
        }
        try{
            $fp = fopen($filename, 'r');
            if ($fp) {
                $numLoadedRows = 0;
                $numReadRows = 0;
                while ( !feof($fp) && ($maxRows == 0 || $numLoadedRows < $maxRows))
                {
                    $line = fgets($fp, 2048);
                    $numReadRows++;
                    if ($numReadRows > $skipRows){
                        $delimiter = "\t";
                        $data = str_getcsv($line, $delimiter);
                        $loaded = $this->load($data);
                        if ($loaded){
                            $numLoadedRows++;
                        }
                    }
                }

                fclose($fp);
            }
        } catch (Exception $e){
            echo "Closing resources<br>";
            if ($fp != null){
                fclose($fp);
            }
        }
    }

    protected abstract function load($dataRow);
}

/*
CREATE TABLE `geonames` (
  `geonameid` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `asciiname` varchar(200) NOT NULL,
  `alternatenames` text NOT NULL,
  `latitude` decimal(16,8) NOT NULL,
  `longitude` decimal(16,8) NOT NULL,
  `feature_class` char(1) NOT NULL,
  `feature_code` varchar(10) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `cc2` varchar(60) NOT NULL,
  `admin1_code` varchar(20) NOT NULL,
  `admin2_code` varchar(80) NOT NULL,
  `admin3_code` varchar(20) NOT NULL,
  `admin4_code` varchar(20) NOT NULL,
  `population` bigint(20) NOT NULL,
  `elevation` int(11) NOT NULL,
  `dem` varchar(100) NOT NULL,
  `timezone` varchar(40) NOT NULL,
  `modification_date` date NOT NULL
  PRIMARY KEY (`geonameid`),
  KEY `name` (`name`,`admin1_code`,`admin2_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

class GeonamesTableRow extends MigrationBase {
    private static $nameToIndexMap = array(
        "geonameid" => 0,
        "name" => 1,
        "asciiname" => 2,
        "alternatenames" => 3,
        "latitude" => 4,
        "longitude" => 5,
        "feature_class" => 6,
        "feature_code" => 7,
        "country_code" => 8,
        "cc2" => 9, // alternate country code
        "admin1_code" => 10, // fipscode
        "admin2_code" => 11, // 2nd level code (e.g., county)
        "admin3_code" => 12, // 3rd level code
        "admin4_code" => 13, // 4th level code
        "population" => 14,
        "elevation" => 15,
        "dem" => 16, // digital elevation model
        "timezone" => 17, // timezoneId
        "modification_date" => 18
    );

    private function getValue($dataRow, $fieldName){
        $index = isset(self::$nameToIndexMap[$fieldName]) ? self::$nameToIndexMap[$fieldName] : -1;
        if ($index >= 0){
            return $dataRow[$index];
        }
        return NULL;
    }

    private static $INSERT_ROW_SQL = "
        INSERT INTO geonames
        (
            geonameid,
            name,
            asciiname,
            alternatenames,
            latitude,
            longitude,
            feature_class,
            feature_code,
            country_code,
            cc2,
            admin1_code,
            admin2_code,
            admin3_code,
            admin4_code,
            population,
            elevation,
            dem,
            timezone,
            modification_date
        )
        SELECT
          ?,?,?,?,?,?,
          ?,?,?,?,?,?,
          ?,?,?,?,?,?,
          ?
        FROM dual
        WHERE NOT EXISTS (
            SELECT 1
            FROM geonames
            WHERE geonameid = ?
            OR name = ?
            AND admin1_code = ?
            AND admin2_code = ?
        )";

    private static $INSERT_ROW_BINDINGS = "isssddssssssssiisssisss";

    protected function load($dataRow, $tryUpdate = false){
        if ($this->isRelevant($dataRow)){
            $geonameid = $this->getValue($dataRow, "geonameid");
            $name = $this->getValue($dataRow, "name");
            $stateAbr = $this->getValue($dataRow, "admin1_code");
            $fips = $this->getValue($dataRow, "admin2_code");
            $dataRow[] = $geonameid;
            $dataRow[] = $name;
            $dataRow[] = $stateAbr;
            $dataRow[] = $fips;
            $executeResult = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, self::$INSERT_ROW_SQL, self::$INSERT_ROW_BINDINGS, $dataRow, $executeResult);
            if (!$executeResult->success && $tryUpdate){
                // try update
                return $this->update($dataRow);
            }
            return $executeResult->success;
        }
        return false;
    }

    private static $UPDATE_ROW_SQL = "
        UPDATE geonames
        SET
            name = ?,
            asciiname = ?,
            alternatenames = ?,
            latitude = ?,
            longitude = ?,
            feature_class = ?,
            feature_code = ?,
            country_code = ?,
            cc2 = ?,
            admin1_code = ?,
            admin2_code = ?,
            admin3_code = ?,
            admin4_code = ?,
            population = ?,
            elevation = ?,
            dem = ?,
            timezone = ?,
            modification_date = ?
        WHERE
            geonameid = ?";

    private static $UPDATE_ROW_BINDINGS = "sssddssssssssiisssi";

    private function update($dataRow){
        $geonameid = $this->getValue($dataRow, "geonameid");
        $sqlParams = array();
        for ($i = 1; $i <= 18; $i++) {
            $sqlParams[] = $dataRow[$i];
        }
        $sqlParams[] = $geonameid;
        $executeResult = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, self::$UPDATE_ROW_SQL, self::$UPDATE_ROW_BINDINGS, $sqlParams, $executeResult);
    }

    private static $CHECK_IS_RELEVANT_SQL = "
        SELECT 1
        FROM geonames_us_postalcodes
        WHERE admin_code1 = ?
        AND admin_code2 = ?
        AND NOT EXISTS (
            SELECT 1
            FROM geonames
            WHERE geonameid = ?
        )";

    private function isRelevant($dataRow){
        $featureClass = $this->getValue($dataRow, "feature_class");
        if ($featureClass != "P"){
            echo ". ";
            return false; // must be populated place
        }
        $stateAbr = $this->getValue($dataRow, "admin1_code");
        $fipsCode = $this->getValue($dataRow, "admin2_code");
        $geonameid = $this->getValue($dataRow, "geonameid");
        $sqlString = self::$CHECK_IS_RELEVANT_SQL;
        $sqlBindings = "ssi";
        $sqlParams = array(
            $stateAbr,
            $fipsCode,
            $geonameid
        );
        $result = MySqliHelper::get_result($this->mysqli, $sqlString, $sqlBindings, $sqlParams);
        if (count($result)){
            echo "1 ";
            return true;
        }
        echo "_ ";
        return false;
    }
}

/*
CREATE TABLE `geonames_us_postalcodes` (
  `country_code` varchar(3) NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `place_name` varchar(180) NOT NULL,
  `admin_name1` varchar(100) NOT NULL,
  `admin_code1` varchar(20) NOT NULL,
  `admin_name2` varchar(100) NOT NULL,
  `admin_code2` varchar(20) NOT NULL,
  `admin_name3` varchar(100) NOT NULL,
  `admin_code3` varchar(20) NOT NULL,
  `latitude` decimal(16,8) NOT NULL,
  `longitude` decimal(16,8) NOT NULL,
  `accuracy` int(11) NOT NULL,
  KEY `postal_code` (`postal_code`,`place_name`),
  KEY `place_name` (`place_name`,`admin_code1`,`admin_code2`),
  KEY `postal_code_2` (`postal_code`,`latitude`,`longitude`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
 */

class PostalCodesTableRow extends MigrationBase {
    private static $indexToNameMap = array(
        0 => "country_code",
        1 => "postal_code",
        2 => "place_name",
        3 => "admin_name1", // state
        4 => "admin_code1", // state
        5 => "admin_name2", // county
        6 => "admin_code2", // county (fips) -- this is a string not a number
        7 => "admin_name3", // community
        8 => "admin_code3", // community
        9 => "latitude",
        10 => "longitude",
        11 => "accuracy" // 1 = estimated, 6 = centroid
    );

    private static $INSERT_ROW_SQL = "
        INSERT INTO geonames_us_postalcodes
        (
            country_code,
            postal_code,
            place_name,
            admin_name1,
            admin_code1,
            admin_name2,
            admin_code2,
            admin_name3,
            admin_code3,
            latitude,
            longitude,
            accuracy
        )
        SELECT
          ?,?,?,?,?,?,
          ?,?,?,?,?,?
        FROM dual
        WHERE NOT EXISTS (
            SELECT 1
            FROM geonames_us_postalcodes
            WHERE postal_code = ?
            AND place_name = ?
        )";

    private static $INSERT_ROW_BINDINGS = "sssssssssddiss";

    protected function load($dataRow){
        $postalCode = $dataRow[1];
        $placeName = $dataRow[2];
        $dataRow[] = $postalCode;
        $dataRow[] = $placeName;
        $executeResult = new ExecuteResponseRecord();
        MySqliHelper::execute($this->mysqli, self::$INSERT_ROW_SQL, self::$INSERT_ROW_BINDINGS, $dataRow, $executeResult);
        return $executeResult->success;
    }
}

/*
CREATE TABLE `geonames_timezones` (
  `CountryCode` varchar(3) NOT NULL,
  `TimeZoneId` varchar(40) NOT NULL,
  `gmt_offset` decimal(3,1) NOT NULL,
  `dst_offset` decimal(3,1) NOT NULL,
  `rawOffset` decimal(3,1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
 */

class TimeZoneTableRow {
    private static $indexToNameMap = array(
        0 => "CountryCode",
        1 => "TimeZoneId",
        2 => "gmt_offset", // deprecated
        3 => "dst_offset", // deprecated
        4 => "rawOffset" // independent of DST
    );

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    public function upload($filename){
        $fp = null;
        try{
            $fp = fopen($filename, 'r');
            if ($fp) {
                $keepGoing = 1;
                while ( !feof($fp) && $keepGoing)
                {
                    $line = fgets($fp, 2048);
                    $delimiter = "\t";
                    $data = str_getcsv($line, $delimiter);
                    if ($data[0] !== self::$indexToNameMap[0]){
                        $keepGoing = $this->load($data);
                        if (!$keepGoing){
                            echo "Process halted due to bad insert";
                        }
                    } else {
                        var_dump($data);
                    }
                }

                fclose($fp);
            }
        } catch (Exception $e){
            echo "Closing resources<br>";
            if ($fp != null){
                fclose($fp);
            }
        }
    }

    private static $INSERT_ROW_SQL = "
        INSERT INTO geonames_timezones
        (
            CountryCode,
            TimeZoneId,
            gmt_offset,
            dst_offset,
            rawOffset
        )
        SELECT
          ?,?,?,?,?
        FROM dual
        WHERE NOT EXISTS (
            SELECT 1
            FROM geonames_timezones
            WHERE CountryCode = ?
            AND TimeZoneId = ?
        )";

    private static $INSERT_ROW_BINDINGS = "ssdddss";

    protected function load($dataRow){
        if ($dataRow[0] !== self::$indexToNameMap[0]){
            $countryCode = $dataRow[0];
            $timeZoneId = $dataRow[1];
            $dataRow[] = $countryCode;
            $dataRow[] = $timeZoneId;
            $executeResult = new ExecuteResponseRecord();
            MySqliHelper::execute($this->mysqli, self::$INSERT_ROW_SQL, self::$INSERT_ROW_BINDINGS, $dataRow, $executeResult);
            return $executeResult->success;
        }
    }
}

//
// PROCESSING CODE
//
echo "Loading timezones<br>";
$timeZoneClass = new TimeZoneTableRow($geoConn);
//$timeZoneClass->upload("timeZones.txt");

echo "Loading postal codes<br>";
$postalCodeClass = new PostalCodesTableRow($geoConn);
//$postalCodeClass->upload("US_postalcodes.txt");

$geonamesClass = new GeonamesTableRow($geoConn);
echo "Counting Geonames<br>";
//$numRows = $geonamesClass->countRows("US_geonames.txt");
echo "There are ".$numRows." rows in US_geonames.txt<br>";

echo "Updating Geonames<br>";
// 502000(skippped) + 252790(notrelevant) + (101317 (existing) - 688(existing skipped) - 34549(created) )
// 820000(skipped) + 109679(notrelevant) + (111478 (existing) - 0(existing skipped) - 10000(created) )
//$geonamesClass->upload("US_geonames.txt", 0, 100000); // this takes a really long time
?>
<font color="blue">Done</font>

<?php
/*
 * Had to manually de-dupe using the "ignore" column on the geonames table. Updated the SQL above to eliminate dupes on insert, but did not test.
 *
INSERT INTO us
(
	Country,
	Zip,
	City,
	StateFull,
	StateAbr,
	County,
	DistrictCode,
	Lat,
	`Long`,
	timezoneID,
	eventBriteTimeZone
)
SELECT
	p.country_code as Country,
	p.postal_code as Zip,
	p.place_name as City,
	p.admin_name1 as StateFull,
	p.admin_code1 as StateAbr,
	p.admin_name2 as County,
	p.admin_code2 as DistrictCode,
	p.latitude as Lat,
	p.longitude as `Long`,
	n.timezone as timezoneID,
	(SELECT eventbriteTimezone FROM geonames_timezones WHERE n.timezone = TimeZoneId) as eventbriteTimeZone
FROM geonames_us_postalcodes p
LEFT JOIN geonames n ON p.place_name = n.name AND p.admin_code1 = n.admin1_code AND p.admin_code2 = n.admin2_code AND n.ignore = 0
WHERE NOT EXISTS (
	SELECT 1
	FROM us
	WHERE Zip = p.postal_code
	AND City = p.place_name
);

-- next, match items by lat long that didn't have a name match in the gazeteer dataset, but match another postal code row with the same lat/long
update  us
join geonames_us_postalcodes p on Zip = p.postal_code and Lat = p.latitude and `Long` = p.longitude
join geonames n on p.place_name = n.name and p.admin_code1 = n.admin1_code and p.admin_code2 = n.admin2_code and n.ignore = 0
set us.timezoneid = n.timezone
where us.timezoneid = '';

-- manual conversions based on outliers


update us set timezoneid = 'America/Adak' where zip in ('99591','99660');
update us set timezoneid = 'America/Anchorage' where stateabr = 'AK' and timezoneid = '';
update us set timezoneid = 'America/Phoenix' where stateabr = 'AZ';
update us set timezoneid = 'Pacific/Honolulu' where stateabr = 'HI';
update us set timezoneid = 'America/Chicago' where stateabr = 'KS' and DistrictCode not in ('071','075','181','199');
update us set timezoneid = 'America/Denver' where stateabr ='KS' and DistrictCode in ('071','075','181','199');
update us set timezoneid = 'America/Detroit' where stateabr = 'MI' and DistrictCode not in ('043','053','071','109');
update us set timezoneid = 'America/Menominee' where stateabr = 'MI' and DistrictCode in ('043','053','071','109');
update us set timezoneid = 'America/New_York' where stateabr = 'FL' and DistrictCode not in ('005','013','033','045','059','063', '091', '113', '131', '133');
update us set timezoneid = 'America/Chicago' where stateabr = 'FL' and DistrictCode in ('005','013','033','059','063', '091', '113', '131', '133');
-- this still leaves Gulf County, FL (045) which is split--need to use lat/long to figure this out

update us set timezoneid = 'America/Boise' where stateabr = 'ID' and DistrictCode not in ('009','017','021','035','049','055', '057', '061', '069', '079');
update us set timezoneid = 'America/Los_Angeles' where stateabr = 'ID' and DistrictCode in ('009','017','021','035','049','055', '057', '061', '069', '079');

update us set timezoneid = 'America/New_York' where stateabr = 'IN' and DistrictCode not in ('051', '073','089','091','111','123','127','129','147','149','163','173') and timezoneid = '';
update us set timezoneid = 'America/Chicago' where stateabr = 'IN' and DistrictCode in ('051', '073','089','091','111','123','127','129','147','149','163','173') and timezoneid = '';

update us set timezoneid = 'America/New_York' where stateabr = 'KY' and DistrictCode not in ('001','003','007','009','027','031','033','035','039','047','053','055','057','059','061','075','083','085','087','091','099','101','105','107','139','141','145','149','157','169','171','177','183','207','213','219','221','225','227','233') and timezoneid = '';
update us set timezoneid = 'America/Chicago' where stateabr = 'KY' and DistrictCode in ('001','003','007','009','027','031','033','035','039','047','053','055','057','059','061','075','083','085','087','091','099','101','105','107','139','141','145','149','157','169','171','177','183','207','213','219','221','225','227','233') and timezoneid = '';

update us set timezoneid = 'America/Chicago' where stateabr = 'ND' and DistrictCode not in ('001','007','011','025','033','037','041','053','085','087','089') and timezoneid = '';
update us set timezoneid = 'America/Denver' where stateabr = 'ND' and DistrictCode in ('001','007','011', '033','037','041','087','089') and timezoneid = '';
-- this still leaves Dunn Count (025), McKenzie County (053), and Sioux Count(085)

update us set timezoneid = 'America/Chicago' where stateabr = 'NE' and DistrictCode not in ('005','007','013','029','031','033','045','049','057','069','075','091','101','105','123','135','157','161','165') and timezoneid = '';
update us set timezoneid = 'America/Denver' where stateabr = 'NE' and DistrictCode in ('005','007','013','029','033','045','049','057','069','075','091','101','105','123','135','157','161','165') and timezoneid = '';
-- this still leaves Cherry County (031)

update us set timezoneid = 'America/Los_Angeles' where stateabr = 'OR' and DistrictCode not in ('045') and timezoneid = '';

update us set timezoneid = 'America/Chicago' where stateabr = 'SD' and DistrictCode not in ('007','019','031','033','041','047','055','063','071','081','093','103','105','113','117','137') and timezoneid = '';
update us set timezoneid = 'America/Denver' where stateabr = 'SD' and DistrictCode in ('007','019','031','033','041','047','055','063','071','081','093','103','105','113','137') and timezoneid = '';
-- this still leaves Stanley County, SD (117) which is split -- need to use lat/long to figure this out.

update us set timezoneid = 'America/Chicago' where stateabr = 'TN' and DistrictCode not in ('001','009','011','013','019','025','029','057','059','063','065','067','073','089','091','093','105','107','121','123','129','139','143','145','151','155','163','171','173','179');
update us set timezoneid = 'America/New_York' where stateabr = 'TN' and DistrictCode in ('001','009','011','013','019','025','029','057','059','063','065','067','073','089','091','093','105','107','121','123','129','139','143','145','151','155','163','171','173','179');
update us set timezoneid = 'America/Chicago' where stateabr = 'TX' and DistrictCode not in ('141','229');
update us set timezoneid = 'America/Denver' where stateabr = 'TX' and DistrictCode in ('141','229');


update us set timezoneid = 'America/New_York' where stateabr in ('CT','DC','DE','GA','MA','MD', 'ME', 'NC', 'NH', 'NJ', 'NY', 'OH', 'PA', 'RI', 'VA', 'WV') and timezoneid = '';
update us set timezoneid = 'America/Chicago' where stateabr in ('AL', 'AR', 'IA', 'IL', 'LA', 'MN', 'MO', 'MS', 'OK','SC','VT', 'WI') and timezoneid = '';
update us set timezoneid = 'America/Denver' where stateabr in ('CO', 'MT', 'NM','UT', 'WY') and timezoneid = '';
update us set timezoneid = 'America/Los_Angeles' where stateabr in ('CA', 'NV', 'WA') and timezoneid = '';

-- adjustments based on lat long matches
update us
join geonames_us_postalcodes p on Lat = p.latitude and `Long` = p.longitude and us.stateabr = p.admin_code1
join geonames n on p.place_name = n.name and p.admin_code1 = n.admin1_code and p.admin_code2 = n.admin2_code and n.ignore = 0
set timezoneid =  n.timezone
where us.timezoneid = '';

-- manual adjustment to errors reported:
-- temecula, ca
UPDATE us
SET Lat = '33.5033',
	`Long` = '-117.1236'
WHERE Zip = '92589';

 */
?>