<?php
/**
 * Migrates data for the nonprofit refactoring
 */
// marking this dead -- it is only documentation at this point
die();

include_once("../../Global_Variables.php");
include_once("../../repository/mySqlProvider/Helper.php");
include_once("../../repository/mySqlProvider/MemberProvider.php");
include_once("../../repository/mySqlProvider/OrgProvider.php");

/* Find all Fiscal Sponsors referenced by their sponsored charities, that don't have their own record */
$sqlString = "
    SELECT
        EventTransaction_ID AS EventTransaction_ID,
        EventTransaction_OrgEIN AS FiscalSponsorEin
    FROM event_transaction
    WHERE EventTransaction_OrgId = 0
    AND Length(EventTransaction_OrgEIN) > 0

    UNION
    SELECT
        0 AS EventTransaction_ID,
        OrganizationInfo_FiscalSponsorEIN AS FiscalSponsorEin
    FROM organization_info child
    WHERE OrganizationInfo_FiscalSponsorEIN IS NOT NULL
    AND NOT EXISTS (
        SELECT 1
        FROM organization_info parent
        WHERE parent.OrganizationInfo_EIN = child.OrganizationInfo_FiscalSponsorEIN
    )";

$sqlUpdateString = "
    UPDATE event_transaction
    SET EventTransaction_OrgID = ?
    WHERE EventTransaction_ID = ?";
$sqlUpdateBindings = "ii";

$recordsWithoutParents = MySqliHelper::get_result($mysqli, $sqlString);
printf("%s sponsored nonprofits exist without an existing Fiscal Sponsor Record.<br>", count($recordsWithoutParents));
$asObject = true; // silly thing we need for first giving lookup
if (count($recordsWithoutParents)){
    include_once("../../Library/FirstGiving_AP.php");
    $orgProvider = new OrgProvider($mysqli);
    $memberProvider = new OrganizationMemberProvider($mysqli);
    foreach ($recordsWithoutParents as $childOrg){
        $einValue = $childOrg["FiscalSponsorEin"];
        $eventTransactionId = $childOrg["EventTransaction_ID"];
        $existingOrgRecord = $orgProvider->getByEIN($einValue);
        if ($existingOrgRecord->id > 0){
            var_dump($existingOrgRecord);
            printf("Fiscal Sponsor already exists for %s (%s).<br>", $einValue, $existingOrgRecord->name);
            if ($eventTransactionId > 0){
                $sqlUpdateParams = array($existingOrgRecord->id, $eventTransactionId);
                $updateResult = MySqliHelper::execute($mysqli, $sqlUpdateString, $sqlUpdateBindings, $sqlUpdateParams);
                if ($updateResult){
                    printf("EventTransaction update for %s.<br>", $existingOrgRecord->id);
                } else {
                    printf("Unable to update EventTransaction %s<br>", $existingOrgRecord->id);
                }
            }
        } else {
            $charityObject = FirstGivingOrganizationSearch::LookupEIN($einValue);
            if (isset($charityObject->OrganizationName) && strlen($charityObject->OrganizationName) > 0){

                $companyRecord = new OrganizationRecord();
                $charityObject->mapToOrgRecord($companyRecord);
                $companyRecord->primaryContact = new ContactRecord();

                $orgId = $memberProvider->addOrganization($companyRecord);
                if ($orgId){
                    printf("Organization record created for %s (%s) with ID=%s.<br>", $einValue, $companyRecord->name, $orgId);
                } else {
                    printf("Unable to create a record for %s (%s). Reason unknown.", $einValue, $charityObject->OrganizationName);
                }
            } else {
                printf("FirstGiving Lookup for %s did not return a name. No org record created.<br>", $einValue);
            }
        }
    }
}
?>