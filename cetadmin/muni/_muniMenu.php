<?php
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
$rebateDropDownItemsTop = 35;
$reportDropDownItemsTop = 35;
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>" style="position:relative;">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Muni))){?>
			<!--
			<li><a id="customer-intake" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-intake" class="button-link">Customer Intake</a></li>
			-->
			<li><a id="phonelog" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=phonelog" class="button-link">Phone Log</a></li>
			<li><a id="customer-data" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data" class="button-link">Customer Details</a></li>
			<li id="RebateMenuDropDown"><a id="rebates" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebates" class="button-link">Rebates</a>
				<?php ($nav != "rebates" ? include('_rebateTabs.php') : "");?>
			<li><a id="auditForm" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=auditForm" class="button-link">Audit Form</a></li>
			<li id="ReportMenuDropDown"><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=reports" class="button-link">Reports</a>
				<?php ($nav != "reports" ? include('_reportTabs.php') : "");?>
			<!--
			<li><a id="rebate-tester" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebateTester" class="button-link">Rebate Data Tester</a></li>
			-->
			<li><a id="attachments" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=attachments" class="button-link">Downloaded Forms/Attachments</a></li>
			<li><a id="pdfs" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=pdfs" class="button-link">Blank Rebate PDFs</a></li>
			<!--hidden because only used from link if file does not download
				<li><a id="form-receiver" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=formReceiver" class="button-link">Form Receiver Tool</a></li>
				<li><a id="attachmentTool" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=attachmentTool" class="button-link">Attachment Tool</a></li>
			-->
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#rebateDropDownItems").hide();
			$("#reportDropDownItems").hide();
		});
		$("#rebates.button-link").on("mouseover",function(){
			$("#rebateDropDownItems").show();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "report-approved":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-paid":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "rebates":
					$nav = "rebates";
					$showRebatesDropDown = true;
					break;
				case "rebate-heathotwater":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-efficiency":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-coolhome":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-poolpump":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-appliance":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-commercialheat":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				case "rebate-wifi":
					$navDropDown = $nav;
					$nav = "rebates";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
			if (currentPage == "rebates"){
				var currentDropDown = '<?php echo $navDropDown;?>',
				activeDropDown;
				if (currentDropDown){
					activeDropDown = $("#rebateDropDownItems").find("#"+currentDropDown);
					if (activeDropDown.length){
						activeDropDown.addClass("ui-state-highlight");
					}
				}
			
			}
        }
    });
</script>
<?php }?>