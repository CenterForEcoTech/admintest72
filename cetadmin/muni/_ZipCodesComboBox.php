<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxZipCodes" ).combobox({
				select: function(event, ui){
					<?php if (!$zipCodesComboBoxFunction){?>
					alert('zip picked');
					//window.location = "?nav=<?php echo $_GET['nav'];?>&CustomerID="+$(ui.item).val();
					<?php }else{
						echo $zipCodesComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($zipCodesComboBoxDivColumns ? $zipCodesComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$zipCodesComboBoxHideLabel){?>
				<label>Start typing zipCode name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxZipCodes" id="SelectedZipCode<?php echo rand();?>" data-hiddenid="">
				<option value="">Select one...</option>
				<?php 
					foreach ($ZipCodeByID as $zipCode=>$zipCodeInfo){
						echo "<option value=\"".$zipCodeInfo->ZipCode."\"".($SelectedCustomerIDZipCode == $zipCodeInfo->ZipCode ? " selected" : "").">".$zipCodeInfo->ZipCode." ".$zipCodeInfo->City." [".$zipCodeInfo->County."]</option>";
					}
				?>
			  </select>
			</div>
		</div>