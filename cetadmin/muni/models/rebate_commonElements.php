<?php
$adminFullName = $_SESSION["AdminAuthObject"]["adminFullName"];
$adminFullNameParts = explode(" ",$adminFullName);
	$dataSet["customerID"] = 2;
	$dataSet["accountHolderFirstName"] = $adminFullNameParts[1].$randNumb;
	$dataSet["accountHolderLastName"] = $adminFullNameParts[0];
	$dataSet["utilityName"] = "Westfield Gas & Electric".$randNumb;
	$dataSet["utilityAccountNumber"] = "PersonAc".$randNumb."Location".$randNumb;
	$dataSet["utilityAccountNumberPerson"] = "PersonAc".$randNumb;
	$dataSet["utilityAccountNumberLocation"] = "Location".$randNumb;
	$dataSet["formName"] = $formType;
	$dataSet["rebateMadePayableTo"] = "PayTo".$randNumb;

	$dataSet["installedAddressOwnedOrLeased"] = "Owned";
	$dataSet["installedAddress"] = "iAddress".$randNumb." St";
	$dataSet["installedCity"] = "iCity".$randNumb;
	$dataSet["installedState"] = "iState".$randNumb;
	$dataSet["installedZip"] = $randNumb;
	$dataSet["installedPhone"] = "iPhone".$randNumb;
	$dataSet["installedWorkPhone"] = "iWorkPhone".$randNumb;
	$dataSet["installedFax"] = "iFax".$randNumb;
	$dataSet["installedEmail"] = "iEmail".$randNumb."@email.com";

	$dataSet["mailingAddress"] = "mAddress".$randNumb." St";
	$dataSet["mailingCity"] = "mCity".$randNumb;
	$dataSet["mailingState"] = "mState".$randNumb;
	$dataSet["mailingZip"] = $randNumb;

	$dataSet["ownerFirstName"] = "oFirst".$randNumb;
	$dataSet["ownerLastName"] = "oLast".$randNumb;
	$dataSet["ownerTaxId"] = "oTaxID".$randNumb;
	$dataSet["ownerAddress"] = "oAddress".$randNumb." St";
	$dataSet["ownerCity"] = "oCity".$randNumb;
	$dataSet["ownerState"] = "oState".$randNumb;
	$dataSet["ownerZip"] = $randNumb;
	$dataSet["ownerPhone"] = "oPhone".$randNumb;
	$dataSet["ownerFax"] = "oFax".$randNumb;
	$dataSet["ownerEmail"] = "oEmail".$randNumb."@email.com";

	
	$dataSet["contractorName"] = "CName".$randNumb;
	$dataSet["contractorContractorName"] = "CContractorName".$randNumb;
	$dataSet["contractorLicenseNumber"] = "CLicence".$randNumb;
	$dataSet["contractorTaxID"] = "CTax".$randNumb;
	$dataSet["contractorAddress"] = "CAddress".$randNumb;
	$dataSet["contractorCity"] = "CCity".$randNumb;
	$dataSet["contractorState"] = "CState".$randNumb;
	$dataSet["contractorZip"] = "CZip".$randNumb;
	$dataSet["contractorPhone"] = "CPhoe".$randNumb;
	$dataSet["contractorEmail"] = "CEmail".$randNumb;

	$dataSet["customerInitials"] = "CustInitials".$randNumb;
	$dataSet["staffInitials"] = "StaffInitials".$randNumb;
	$dataSet["customerAcknowledgementDate"] = date("Y-m-d");
	//$dataSet["receivedTimeStamp"] defaults to current timestamp
	$dataSet["addedByAdmin"] = "Admin".$randNumb;
	//$dataSet["updatedByAdmin"] = "".$randNumb;
	$dataSet["rawFormData"] = json_encode($dataSet);
	$dataSet["ipAddress"] = "CET_IPAddress";
	$dataSet["oid"] = "CET_OID";
?>