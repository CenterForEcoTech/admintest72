<?php
		$dataSet["contractorNATECertified"] = 1;
		$dataSet["equipmentNewConstruction"] = 1;
		$dataSet["equipmentReplacement"] = 1;
		$dataSet["equipmentAdding"] = 1;
		$dataSet["equipmentNewOrAdd"] = 1;
		$dataSet["equipmentInstalledType"] = "New Construction,Replacement System,Adding to existing ductwork,New or additional ductwork and air conditioning";
		$dataSet["equipmentInstalledDate"] = date("Y-m-d");
		$dataSet["equipmentInstalledCost"] = 18.00;
		$dataSet["equipmentAHRIRef"] = "EAHRI".$randNumb;
		$dataSet["equipmentManufacturer"] = "EManu".$randNumb;
		$dataSet["equipmentTXVorEXV"] = "ETV";
		$dataSet["equipmentCondensorModelNumber"] = "EConModel".$randNumb;
		$dataSet["equipmentCoilModelNumber"] = "ECoilModel".$randNumb;
		$dataSet["equipmentAHRIRatedSEER"] = "ESeer".$randNumb;
		$dataSet["equipmentAHRIRatedEER"] = "EEer".$randNumb;
		$dataSet["equipmentHSPF"] = "EHSPF".$randNumb;
		$dataSet["equipmentNewUnitSize"] = "ESize".$randNumb;
		$dataSet["equipmentMiniSplit"] = 1;
		$dataSet["equipmentRebateAmount"] = 18.00;
		$dataSet["supportingFile"] = "FileLocation".$randNumb;

?>