<?php
//array(SprypointFieldName,CETFieldName)
//valid field matching as of 11/19/2015
$FieldArray_Appliance = array(
	"Your_Name"=>"accountHolderFirstName","email_address_RE_Home_Appliance_Rebate_Form"=>"installedEmail",
	"Customer_Name"=>"accountHolderFirstName","last_name"=>"accountHolderLastName","Residential_Electric_Utility_Account_Number"=>"utilityAccountNumber",
	"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip",
	"Home_Phone"=>"installedPhone","Work_Phone"=>"installedWorkPhone","email_address_RE_Appliance_Rebate_Application"=>"installedEmail",

	"Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	"Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",

	"Energy_Star_Appliance"=>"applianceType","Brand"=>"applianceBrand","Model_Number"=>"applianceModelNumber","Serial_Number"=>"applianceSerialNumber",
	"Purchase_Date"=>"storePurchaseDate","Purchase_Price"=>"storePurchasePrice",

	"Store_Name"=>"storeName",
	"Store_Address"=>"storeAddress","Store_City"=>"storeCity","Store_State"=>"storeState","Store_Zip"=>"storeZipCode",

	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials"
	);
	
//valid field matching as of 11/19/2015 TODO DatabaseUPDATE
$FieldArray_CommercialHeat = array(
	"Account_Holder_Last_Name"=>"accountHolderLastName","Account_Holder_Name"=>"accountHolderFirstName","Gas_Account_Number"=>"utilityAccountNumber",
	"Installed_Address"=>"installedAddress","Installed_City"=>"installedCity","Installed_State"=>"installedState","Installed_Zip"=>"installedZip",
	"Phone_Number"=>"installedPhone","email_address_RE_Commercial_and_Industrial_High_Efficiency_Heating_and_Water_Prescriptive_Rebate"=>"installedEmail",

	"Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	"rebate_mailing_address"=>"mailingAddress","rebate_city"=>"mailingCity","rebate_state"=>"mailingState","rebate_zip"=>"mailingZip",

	"Contractor_Name"=>"contractorName","License_Number"=>"contractorLicenseNumber",
	"Contractor_Address"=>"contractorAddress","Contractor_City"=>"contractorCity","Contractor_State"=>"contractorState","Contractor_Zip"=>"contractorZip",
	"Contractor_Phone_Number"=>"contractorPhone","Contractor_Email"=>"contractorEmail",

	"Facility_Name"=>"facilityName","Facility_Contact"=>"facilityContact","Facility_Type"=>"facilityType",
	"Installed_Address_Facility"=>"facilityInstalledAddress","Facility_City"=>"facilityInstalledCity","Facility_State"=>"facilityInstalledState","Facility_Zip"=>"facilityInstalledZip",
	"Phone_Number_Facility"=>"facilityPhone","Facility_Email"=>"facilityEmail",

	"Boiler_500_Sizing"=>"measure_Boiler85InputSize",
	"Boiler_500_Total_Rebate"=>"measure_Boiler85RebateAmount",
	"Boiler_500_Manufacturer"=>"measure_Boiler85Manufacturer",
	"Boiler_500_Model"=>"measure_Boiler85ModelNumber",
	"Boiler_500_Quantity"=>"measure_Boiler85QuantityInstalled",
	"Boiler_500_Rating"=>"measure_Boiler85EfficiencyRating",
	"Boiler_500_Cost"=>"measure_Boiler85InstalledCost",

	"Boiler_1000_Sizing"=>"measure_Boiler90InputSize",
	"Boiler_1000_Total_Rebate"=>"measure_Boiler90RebateAmount",
	"Boiler_1000_Manufacturer"=>"measure_Boiler90Manufacturer",
	"Boiler_1000_Model"=>"measure_Boiler90ModelNumber",
	"Boiler_1000_Quantity"=>"measure_Boiler90QuantityInstalled",
	"Boiler_1000_Rating"=>"measure_Boiler90EfficiencyRating",
	"Boiler_1000_Cost"=>"measure_Boiler90InstalledCost",


	"Boiler_1500_Sizing"=>"measure_Boiler95InputSize",
	"Boiler_1500_Total_Rebate"=>"measure_Boiler95RebateAmount",
	"Boiler_1500_Manufacturer"=>"measure_Boiler95Manufacturer",
	"Boiler_1500_Model"=>"measure_Boiler95ModelNumber",
	"Boiler_1500_Quantity"=>"measure_Boiler95QuantityInstalled",
	"Boiler_1500_Rating"=>"measure_Boiler95EfficiencyRating",
	"Boiler_1500_Installed"=>"measure_Boiler95InstalledCost",

	"Integrated_Water_Heater_1200_Sizing"=>"measure_Integrated90InputSize",
	"Integrated_Water_Heater_1200_Total_Rebate"=>"measure_Integrated90RebateAmount",
	"Integrated_Water_Heater_1200_Manufacturer"=>"measure_Integrated90Manufacturer",
	"Integrated_Water_Heater_1200_Model"=>"measure_Integrated90ModelNumber",
	"Integrated_Water_Heater_1200_Quantity"=>"measure_Integrated90QuantityInstalled",
	"Integrated_Water_Heater_1200_Rating"=>"measure_Integrated90EfficiencyRating",
	"Integrated_Water_Heater_1200_Installed"=>"measure_Integrated90InstalledCost",

	"Integrated_Water_Heater_1600_Sizing"=>"measure_Integrated95InputSize",
	"Integrated_Water_Heater_1600_Total_Rebate"=>"measure_Integrated95RebateAmount",
	"Integrated_Water_Heater_1600_Manufacturer"=>"measure_Integrated95Manufacturer",
	"Integrated_Water_Heater_1600_Model"=>"measure_Integrated95ModelNumber",
	"Integrated_Water_Heater_1600_Quantity"=>"measure_Integrated95QuantityInstalled",
	"Integrated_Water_Heater_1600_Rating"=>"measure_Integrated95EfficiencyRating",
	"Integrated_Water_Heater_1600_Installed"=>"measure_Integrated95InstalledCost",
	
	"HEF_300_Sizing"=>"measure_Furnace95InputSize",
	"HEF_300_Total_Rebate"=>"measure_Furnace95RebateAmount",
	"HEF_300_Manufacturer"=>"measure_Furnace95Manufacturer",
	"HEF_300_Model"=>"measure_Furnace95ModelNumber",
	"HEF_300_Quantity"=>"measure_Furnace95QuantityInstalled",
	"HEF_300_Rating"=>"measure_Furnace95EfficiencyRating",
	"HEF_300_Cost"=>"measure_Furnace95InstalledCost",

	"HEF_600_Sizing"=>"measure_Furnace97InputSize",
	"HEF_600_Total_Rebate"=>"measure_Furnace97RebateAmount",
	"HEF_600_Manufacturer"=>"measure_Furnace97Manufacturer",
	"HEF_600_Model"=>"measure_Furnace97ModelNumber",
	"HEF_600_Quantity"=>"measure_Furnace97QuantityInstalled",
	"HEF_600_Rating"=>"measure_Furnace97EfficiencyRating",
	"HEF_600_Cost"=>"measure_Furnace97InstalledCost",

	"Indirect_Hot_Water_400_Sizing"=>"measure_IndirectWaterInputSize",
	"Indirect_Hot_Water_400_Total_Rebate"=>"measure_IndirectWaterRebateAmount",
	"Indirect_Hot_Water_400_Manufacturer"=>"measure_IndirectWaterManufacturer",
	"Indirect_Hot_Water_400_Model"=>"measure_IndirectWaterModelNumber",
	"Indirect_Hot_Water_400_Quantity"=>"measure_IndirectWaterQuantityInstalled",
	"Indirect_Hot_Water_400_Rating"=>"measure_IndirectWaterEfficiencyRating",
	"Indirect_Hot_Water_400_Installed"=>"measure_IndirectWaterInstalledCost",

	"Tankless_Water_500_Sizing"=>"measure_Tankless82InputSize",
	"Tankless_Water_500_Rebate_Amount"=>"measure_Tankless82RebateAmount",
	"Tankless_Water_500_Manufacturer"=>"measure_Tankless82Manufacturer",
	"Tankless_Water_500_Model"=>"measure_Tankless82ModelNumber",
	"Tankless_Water_500_Quantity"=>"measure_Tankless82QuantityInstalled",
	"Tankless_Water_500_Rating"=>"measure_Tankless82EfficiencyRating",
	"Tankless_Water_500_Installed"=>"measure_Tankless82InstalledCost",

	"Tankless_Water_800_Sizing"=>"measure_Tankless94InputSize",
	"Tankless_Water_800_Total_Rebate"=>"measure_Tankless94RebateAmount",
	"Tankless_Water_800_Manufacturer"=>"measure_Tankless94Manufacturer",
	"Tankless_Water_800_Model"=>"measure_Tankless94ModelNumber",
	"Tankless_Water_800_Quantity"=>"measure_Tankless94QuantityInstalled",
	"Tankless_Water_800_Rating"=>"measure_Tankless94EfficiencyRating",
	"Tankless_Water_800_Installed"=>"measure_Tankless94InstalledCost",

	"Water_Storage_50_Sizing"=>"measure_H2OStorage62InputSize",
	"Water_Storage_50_Total_Rebate"=>"measure_H2OStorage62RebateAmount",
	"Water_Storage_50_Manufacturer"=>"measure_H2OStorage62Manufacturer",
	"Water_Storage_50_Model"=>"measure_H2OStorage62ModelNumber",
	"Water_Storage_50_Quantity"=>"measure_H2OStorage62QuantityInstalled",
	"Water_Storage_50_Rating"=>"measure_H2OStorage62EfficiencyRating",
	"Water_Storage_50_Installed"=>"measure_H2OStorage62InstalledCost",

	"Water_Storage_100_Sizing"=>"measure_H2OStorage67InputSize",
	"Water_Storage_100_Total_Rebate"=>"measure_H2OStorage67RebateAmount",
	"Water_Storage_100_Manufacturer"=>"measure_H2OStorage67Manufacturer",
	"Water_Storage_100_Model"=>"measure_H2OStorage67ModelNumber",
	"Water_Storage_100_Quantity"=>"measure_H2OStorage67QuantityInstalled",
	"Water_Storage_100_Rating"=>"measure_H2OStorage67EfficiencyRating",
	"Water_Storage_100_Installed"=>"measure_H2OStorage67InstalledCost",

	"Total_Anticipated_Rebate"=>"totalAnticipatedRebate",
	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials"
	);
	
//valid field matching as of 11/19/2015
$FieldArray_PoolPump=array(
	"Customer_Name"=>"accountHolderFirstName","last_name"=>"accountHolderLastName","Utility_Account_Number"=>"utilityAccountNumber",
	"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip",
	"Home_Phone"=>"installedPhone","email_address_RE_Pool_Pump_Rebate_Application"=>"installedEmail",

	"rebate_payable_to"=>"rebateMadePayableTo",
	"Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",
	
	"Contractor_Company_Name"=>"contractorName","contractor_license_number"=>"contractorLicenseNumber",
	"Contractor_Address"=>"contractorAddress","Contractor_City"=>"contractorCity","Contractor_State"=>"contractorState","Contractor_Zip"=>"contractorZip",
	"Contractor_Phone"=>"contractorPhone","contractor_email"=>"contractorEmail",

	"Replaced_Pump_Manufacturer"=>"replacedManufacturer","Replaced_Pump_Model_Number"=>"replacedModelNumber","Replaced_Pump_Horsepower"=>"replacedHorsepower","Type_of_Replaced_Pool_Pump"=>"replacedType",
	"New_Pump_Manufacturer"=>"newManufacturer","New_Pump_Model_Number"=>"newModelNumber","New_Pump_Horsepower"=>"newHorsepower","Type_of_New_Pool_Pump"=>"newType",
	"Controller_Manufacturer"=>"newControllerManufacturer","Model_Number"=>"newControllerModelNumber",
	"Purchase_Date"=>"newPurchaseDate","Purchase_Price"=>"newPrice","Installation_Date"=>"newInstalledDate",

	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	);

//valid field matching as of 11/19/2015
$FieldArray_CoolHome = array(
	"customer_last_name"=>"accountHolderLastName",
	"Customer_Name"=>"accountHolderFirstName","Customer_Last_Name"=>"accountHolderLastName","Electric_Account_Number"=>"utilityAccountNumber",
	"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip",
	"Home_Phone"=>"installedPhone","email_address_RE_AC_and_Heat_Pump"=>"installedEmail",

	"Rebate_Name"=>"rebateMadePayableTo",
	"Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",

	"Landlord_Name"=>"ownerFirstName","Landlord_last_name"=>"ownerLastName","Tax_ID"=>"ownerTaxId",
	"landlord_address"=>"ownerAddress","landlord_city"=>"ownerCity","landlord_state"=>"ownerState","landlord_zip"=>"ownerZip",
	"landlord_phone"=>"ownerPhone","landlord_email"=>"ownerEmail",

	"Company_Name"=>"contractorName","Contractor_license"=>"contractorLicenseNumber",
	"Contractor_Address"=>"contractorAddress","Contractor_City"=>"contractorCity","Contractor_State"=>"contractorState","Contractor_Zip"=>"contractorZip",
	"Telephone_Number"=>"contractorPhone","Contractor_Email_Address"=>"contractorEmail",

	"Coil_Model_Number"=>"equipmentCoilModelNumber","Condenser_Model_Number"=>"equipmentCondensorModelNumber",
	"AHRI_Rated_EER"=>"equipmentAHRIRatedEER","AHRI_Rated_SEER"=>"equipmentAHRIRatedSEER","AHRI_Ref_Number"=>"equipmentAHRIRef",
	"HSPF"=>"equipmentHSPF","TXV_or_EXV_Installed"=>"equipmentTXVorEXV",
	"Manufacturer"=>"equipmentManufacturer","Mini_Split"=>"equipmentMiniSplit",
	"New_Equipment_Installed"=>"equipmentInstalledType","New_Unit_Size"=>"equipmentNewUnitSize",

	"CentralAC16Manufacturer"=>"measure_CentralAC16Manufacturer",
	"CentralAC16CondenserModelNumber"=>"measure_CentralAC16CondenserModelNumber",
	"CentralAC16CoilModelNumber"=>"measure_CentralAC16CoilModelNumber",
	"CentralAC16SEER"=>"measure_CentralAC16SEER",
	"CentralAC16EER"=>"measure_CentralAC16EER",
	"CentralAC16HSPF"=>"measure_CentralAC16HSPF",
	"CentralAC16SizeTons"=>"measure_CentralAC16SizeTons",
	"CentralAC16InstallCost"=>"measure_CentralAC16InstallCost",
	"CentralAC16Rebate"=>"measure_CentralAC16Rebate",
	"CentralAC16QtyInstalled"=>"measure_CentralAC16QtyInstalled",
	"CentralAC16TotalRebate"=>"measure_CentralAC16TotalRebate",
	"CentralAC16Approved"=>"measure_CentralAC16Approved",

	"CentralAC18Manufacturer"=>"measure_CentralAC18Manufacturer",
	"CentralAC18CondenserModelNumber"=>"measure_CentralAC18CondenserModelNumber",
	"CentralAC18CoilModelNumber"=>"measure_CentralAC18CoilModelNumber",
	"CentralAC18SEER"=>"measure_CentralAC18SEER",
	"CentralAC18EER"=>"measure_CentralAC18EER",
	"CentralAC18HSPF"=>"measure_CentralAC18HSPF",
	"CentralAC18SizeTons"=>"measure_CentralAC18SizeTons",
	"CentralAC18InstallCost"=>"measure_CentralAC18InstallCost",
	"CentralAC18Rebate"=>"measure_CentralAC18Rebate",
	"CentralAC18QtyInstalled"=>"measure_CentralAC18QtyInstalled",
	"CentralAC18TotalRebate"=>"measure_CentralAC18TotalRebate",
	"CentralAC18Approved"=>"measure_CentralAC18Approved",
	
	"Ductless18Manufacturer"=>"measure_Ductless18Manufacturer",
	"Ductless18CondenserModelNumber"=>"measure_Ductless18CondenserModelNumber",
	"Ductless18CoilModelNumber"=>"measure_Ductless18CoilModelNumber",
	"Ductless18SEER"=>"measure_Ductless18SEER",
	"Ductless18EER"=>"measure_Ductless18EER",
	"Ductless18HSPF"=>"measure_Ductless18HSPF",
	"Ductless18SizeTons"=>"measure_Ductless18SizeTons",
	"Ductless18InstallCost"=>"measure_Ductless18InstallCost",
	"Ductless18Rebate"=>"measure_Ductless18Rebate",
	"Ductless18QtyInstalled"=>"measure_Ductless18QtyInstalled",
	"Ductless18TotalRebate"=>"measure_Ductless18TotalRebate",
	"Ductless18Approved"=>"measure_Ductless18Approved",
	
	"Ductless20Manufacturer"=>"measure_Ductless20Manufacturer",
	"Ductless20CondenserModelNumber"=>"measure_Ductless20CondenserModelNumber",
	"Ductless20CoilModelNumber"=>"measure_Ductless20CoilModelNumber",
	"Ductless20SEER"=>"measure_Ductless20SEER",
	"Ductless20EER"=>"measure_Ductless20EER",
	"Ductless20HSPF"=>"measure_Ductless20HSPF",
	"Ductless20SizeTons"=>"measure_Ductless20SizeTons",
	"Ductless20InstallCost"=>"measure_Ductless20InstallCost",
	"Ductless20Rebate"=>"measure_Ductless20Rebate",
	"Ductless20QtyInstalled"=>"measure_Ductless20QtyInstalled",
	"Ductless20TotalRebate"=>"measure_Ductless20TotalRebate",
	"Ductless20Approved"=>"measure_Ductless20Approved",

	"Install_Cost"=>"equipmentInstalledCost",
	"Install_Date"=>"equipmentInstalledDate",
	"Rebate_Amount"=>"equipmentRebateAmount",

	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials"
	);
//valid field matching as of 11/19/2015
$FieldArray_HeatHotwater = array(
	"Account_Holder_Last_Name"=>"accountHolderLastName","Account_Holder_Name"=>"accountHolderFirstName","Gas_Account_Number"=>"utilityAccountNumber",
	"Installed_Address"=>"installedAddress","Installed_City"=>"installedCity","Installed_Cost"=>"installedCost","Installed_State"=>"installedState","Installed_Zip"=>"installedZip",
	"email_address_RE_Residential_High_Efficiency_Heating_and_Water_Heating_Rebate"=>"installedEmail","Phone_Number"=>"installedPhone",

	"Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	"Rebate_Address"=>"mailingAddress","rebate_city"=>"mailingCity","rebate_state"=>"mailingState","rebate_zip"=>"mailingZip",
	
	"Landlord_Owner_Name"=>"ownerFirstName","Landlord_Owner_Last_Name"=>"ownerLastName",
	"Landlord_Owner_Address"=>"ownerAddress","Landlord_Owner_City"=>"ownerCity","Landlord_Owner_State"=>"ownerState","Landlord_Owner_Zip"=>"ownerZip",
	"Landlord_Owner_Phone_Number"=>"ownerPhone","Landlord_Owner_Fax_Number"=>"ownerFax","Landlord_Owner_Email"=>"ownerEmail",
	"Tax_ID_Number"=>"ownerTaxId",
	
	"Contractor_Name"=>"contractorName",
	"Contractor_Address"=>"contractorAddress","Contractor_City"=>"contractorCity","Contractor_State"=>"contractorState","Contractor_Zip"=>"contractorZip",
	"Contractor_Phone_Number"=>"contractorPhone","Contractor_Email"=>"contractorEmail",
	"License_Number"=>"contractorLicenseNumber","Federal_Tax_ID_Number"=>"contractorTaxID",

	"HEF_300_Manufacturer"=>"measure_Furnace95Manufacturer",
	"HEF_300_Model"=>"measure_Furnace95ModelNumber",
	"HEF_300_Rating"=>"measure_Furnace95EfficiencyRating",
	"HEF_300_Input"=>"measure_Furnace95InputSize",
	"HEF_300_Total_Rebate"=>"measure_Furnace95RebateAmount",
	"HEF_300_Quantity"=>"measure_Furnace95QuantityInstalled",
	"HEF_300_Installed_Cost"=>"measure_Furnace95InstalledCost",

	"HEF_600_Manufacturer"=>"measure_Furnace97Manufacturer",
	"HEF_600_Model"=>"measure_Furnace97ModelNumber",
	"HEF_600_Rating"=>"measure_Furnace97EfficiencyRating",
	"HEF_600_Input"=>"measure_Furnace97InputSize",
	"HEF_600_Total_Rebate"=>"measure_Furnace97RebateAmount",
	"HEF_600_Quantity"=>"measure_Furnace97QuantityInstalled",
	"HEF_600_Installed_Cost"=>"measure_Furnace97InstalledCost",
	
	"HydronicBoiler_500_Manufacturer"=>"measure_Boiler85Manufacturer",
	"HydronicBoiler_500_Model"=>"measure_Boiler85ModelNumber",
	"HydronicBoiler_500_Rating"=>"measure_Boiler85EfficiencyRating",
	"HydronicBoiler_500_Input"=>"measure_Boiler85InputSize",
	"HydronicBoiler_500_Rebate_Amount"=>"measure_Boiler85RebateAmount",
	"HydronicBoiler_500_Quantity"=>"measure_Boiler85QuantityInstalled",
	"HydronicBoiler_500_Installed_Cost"=>"measure_Boiler85InstalledCost",
	
	"HydronicBoiler_1000_Manufacturer"=>"measure_Boiler90Manufacturer",
	"HydronicBoiler_1000_Model"=>"measure_Boiler90ModelNumber",
	"HydronicBoiler_1000_Rating"=>"measure_Boiler90EfficiencyRating",
	"HydronicBoiler_1000_Input"=>"measure_Boiler90InputSize",
	"HydronicBoiler_1000_Total_Rebate"=>"measure_Boiler90RebateAmount",
	"HydronicBoiler_1000_Quantity"=>"measure_Boiler90QuantityInstalled",
	"HydronicBoiler_1000_Installed_Cost"=>"measure_Boiler90InstalledCost",

	"HydronicBoiler_1500_Manufacturer"=>"measure_Boiler95Manufacturer",
	"HydronicBoiler_1500_Model"=>"measure_Boiler95ModelNumber",
	"HydronicBoiler_1500_Rating"=>"measure_Boiler95EfficiencyRating",
	"HydronicBoiler_1500_Input"=>"measure_Boiler95InputSize",
	"HydronicBoiler_1500_Total_Rebate"=>"measure_Boiler95RebateAmount",
	"HydronicBoiler_1500_Quantity"=>"measure_Boiler95QuantityInstalled",
	"HydronicBoiler_1500_Installed_Cost"=>"measure_Boiler95InstalledCost",
	
	"Integrated_Water_Heater_1200_Input"=>"measure_Integrated90InputSize",
	"Integrated_Water_Heater_1200_Rebate"=>"measure_Integrated90RebateAmount",
	"Integrated_Water_Heater_1200_Manufacturer"=>"measure_Integrated90Manufacturer",
	"Integrated_Water_Heater_1200_Model"=>"measure_Integrated90ModelNumber",
	"Integrated_Water_Heater_Manufacturer_1200_Quantity"=>"measure_Integrated90QuantityInstalled",
	"Integrated_Water_Heater_1200_Rating"=>"measure_Integrated90EfficiencyRating",
	"Integrated_Water_Heater_1200_Installed_Cost"=>"measure_Integrated90InstalledCost",

	"Integrated_Water_Heater_1600_Input"=>"measure_Integrated95InputSize",
	"Integrated_Water_Heater_1600_Rebate"=>"measure_Integrated95RebateAmount",
	"Integrated_Water_Heater_1600_Manufacturer"=>"measure_Integrated95Manufacturer",
	"Integrated_Water_Heater_1600_Model"=>"measure_Integrated95ModelNumber",
	"Integrated_Water_Heater_1600_Quantity"=>"measure_Integrated95QuantityInstalled",
	"Integrated_Water_Heater_1600_Rating"=>"measure_Integrated95EfficiencyRating",
	"Integrated_Water_Heater_1600_Installed_Cost"=>"measure_Integrated95InstalledCost",
	
	"IndirectWater_400_Manufacturer"=>"measure_IndirectWaterManufacturer",
	"IndirectWater_400_Model"=>"measure_IndirectWaterModelNumber",
	"IndirectWater_400_Rating"=>"measure_IndirectWaterEfficiencyRating",
	"IndirectWater_400_Input"=>"measure_IndirectWaterInputSize",
	"IndirectWater_400_Total_Rebate"=>"measure_IndirectWaterRebateAmount",
	"IndirectWater_400_Quantity"=>"measure_IndirectWaterQuantityInstalled",
	"IndirectWater_400_Installed_Cost"=>"measure_IndirectWaterInstalledCost",
	
	"WaterStorage_50_Manufacturer"=>"measure_H2OStorage62Manufacturer",
	"WaterStorage_50_Model"=>"measure_H2OStorage62ModelNumber",
	"WaterStorage_50_Rating"=>"measure_H2OStorage62EfficiencyRating",
	"WaterStorage_50_Input"=>"measure_H2OStorage62InputSize",
	"WaterStorage_50_Rebate_Amount"=>"measure_H2OStorage62RebateAmount",
	"WaterStorage_50_Quantity"=>"measure_H2OStorage62QuantityInstalled",
	"WaterStorage_50_Installed_Cost"=>"measure_H2OStorage62InstalledCost",
	
	"WaterStorage_100_Manufacturer"=>"measure_H2OStorage67Manufacturer",
	"WaterStorage_100_Model"=>"measure_H2OStorage67ModelNumber",
	"WaterStorage_100_Rating"=>"measure_H2OStorage67EfficiencyRating",
	"WaterStorage_100_Input"=>"measure_H2OStorage67InputSize",
	"WaterStorage_100_Total_Rebate"=>"measure_H2OStorage67RebateAmount",
	"WaterStorage_100_Quantity"=>"measure_H2OStorage67QuantityInstalled",
	"WaterStorage_100_Installed_Cost"=>"measure_H2OStorage67InstalledCost",

	"TanklessWater_500_Manufacturer"=>"measure_Tankless82Manufacturer",
	"TanklessWater_500_Model"=>"measure_Tankless82ModelNumber",
	"TanklessWater_500_Rating"=>"measure_Tankless82EfficiencyRating",
	"TanklessWater_500_Input"=>"measure_Tankless82InputSize",
	"TanklessWater_500_Total_Rebate"=>"measure_Tankless82RebateAmount",
	"TanklessWater_500_Quantity"=>"measure_Tankless82QuantityInstalled",
	"TanklessWater_500_Installed_Cost"=>"measure_Tankless82InstalledCost",

	"TanklessWater_800_Manufacturer"=>"measure_Tankless94Manufacturer",
	"TanklessWater_800_Model"=>"measure_Tankless94ModelNumber",
	"TanklessWater_800_Rating"=>"measure_Tankless94EfficiencyRating",
	"TanklessWater_800_Input"=>"measure_Tankless94InputSize",
	"TanklessWater_800_Total_Rebate"=>"measure_Tankless94RebateAmount",
	"TanklessWater_800_Quantity"=>"measure_Tankless94QuantityInstalled",
	"TanklessWater_800_Installed_Cost"=>"measure_Tankless94InstalledCost",

	"Installation_Date"=>"installedDate","Total_Anticipated_Rebate"=>"totalAnticipatedRebate",
	"Customer_initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	);

//valid field matching as of 11/19/2015
$FieldArray_Efficiency = array(
	"Account_Holder_First_Name"=>"accountHolderFirstName","Account_Holder_Last_Name"=>"accountHolderLastName","Utility_Account_Number"=>"utilityAccountNumber",

	"Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	"rebate_mailing_address"=>"mailingAddress","rebate_city"=>"mailingCity","rebate_state"=>"mailingState","rebate_zip"=>"mailingZip",

	"Installed_Address"=>"installedAddress","Installed_City"=>"installedCity","Installed_State"=>"installedState","Installed_Zip"=>"installedZip",
	"Phone_Number"=>"installedPhone","email_address_RE_Energy_Efficiency_Rebate_Application"=>"installedEmail",

	"Owner_Landlord_Name"=>"ownerFirstName","Landlord_last_name"=>"ownerLastName","Tax_ID_Number"=>"ownerTaxId",
	"Owner_Landlord_Address"=>"ownerAddress","Owner_Landlord_City"=>"ownerCity","Owner_Landlord_State"=>"ownerState","Owner_Landlord_Zip"=>"ownerZip",
	"Owner_Landlord_Eml"=>"ownerEmail","Owner_Landlord_Phone_Number"=>"ownerPhone",

	"Contractor_Name"=>"contractorName","License_Number"=>"contractorLicenseNumber",
	"Contractor_Address"=>"contractorAddress","Contractor_City"=>"contractorCity","Contractor_State"=>"contractorState","Contractor_Zip"=>"contractorZip",
	"Contractor_Email"=>"contractorEmail","Contractor_Phone_Number"=>"contractorPhone",

	"Eligible_measures"=>"eligibleMeasures","Provide_Description_of_Measure"=>"measuresInstalled",

	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials"
);


	
/* Deprecated 2/15/2017, active as of 11/19/2015
	//bottom row valid field matching prior to 11/19/2015
$sprypointFormTypes = array(
	"re_commercial_and_industrial_high_efficiency_heating_and_water_prescriptive_rebate"=>"CommercialHeat",
	"re_commercial_and_industrial_high_efficiency_heating_and_water"=>"CommercialHeat",
	
	"re_appliance_rebate_application"=>"Appliance",
	"re_home_appliance_rebate_form"=>"Appliance",

	"re_pool_pump_rebate_application"=>"PoolPump",
	"re_high_efficiency_pool_pump_rebate_application"=>"PoolPump",
	
	"re_ac_and_heat_pump"=>"CoolHome",
	"re_cool_homes_rebate_application"=>"CoolHome",
	
	"re_residential_high_efficiency_heating_and_water_heating_rebate"=>"HeatHotwater",
	"re_residential_high_efficiency_heating_and_water_heating_rebate_program"=>"HeatHotwater",
	
	"re_energy_efficiency_rebate_application"=>"Efficiency",
	"re_home_efficiency_incentive_program"=>"Efficiency"
	);	
*/
//valid as of 2/15/2017	
$sprypointFormTypes = array(
	"589a11cf110e2100735d77ee"=>"CommercialHeat",
	"5899ebddda01a9006cfad05e"=>"Appliance",
	"5899eb8e110e2100735d771b"=>"PoolPump",
	"5899dad6110e2100735d76ad"=>"CoolHome",
	"5899e658110e2100735d76e9"=>"HeatHotwater",
	"589a1108da01a9006cfad161"=>"Efficiency"
	);	
	
	
$sprypointFields["589a11cf110e2100735d77ee"] = $FieldArray_CommercialHeat;	
$sprypointFields["5899ebddda01a9006cfad05e"] = $FieldArray_Appliance;
$sprypointFields["5899eb8e110e2100735d771b"] = $FieldArray_PoolPump;	
$sprypointFields["5899dad6110e2100735d76ad"] = $FieldArray_CoolHome;
$sprypointFields["5899e658110e2100735d76e9"] = $FieldArray_HeatHotwater;
$sprypointFields["589a1108da01a9006cfad161"] = $FieldArray_Efficiency;

/*LEGACY FIELDS
//Deprecated 2/15/2017
$sprypointFields["re_commercial_and_industrial_high_efficiency_heating_and_water_prescriptive_rebate"] = $FieldArray_CommercialHeat;
$sprypointFields["re_commercial_and_industrial_high_efficiency_heating_and_water"] = $FieldArray_CommercialHeat;	
	
$sprypointFields["re_home_appliance_rebate_form"] = $FieldArray_Appliance;
$sprypointFields["re_appliance_rebate_application"]= $FieldArray_Appliance;

$sprypointFields["re_pool_pump_rebate_application"] = $FieldArray_PoolPump;	
$sprypointFields["re_high_efficiency_pool_pump_rebate_application"] = $FieldArray_PoolPump;	

$sprypointFields["re_ac_and_heat_pump"] = $FieldArray_CoolHome;
$sprypointFields["re_cool_homes_rebate_application"] = $FieldArray_CoolHome;

$sprypointFields["re_residential_high_efficiency_heating_and_water_heating_rebate"] = $FieldArray_HeatHotwater;
$sprypointFields["re_residential_high_efficiency_heating_and_water_heating_rebate_program"] = $FieldArray_HeatHotwater;

$sprypointFields["re_energy_efficiency_rebate_application"] = $FieldArray_Efficiency;
$sprypointFields["re_home_efficiency_incentive_program"] = $FieldArray_Efficiency;

//Deprecated 11/19/2015
$sprypointFields["re_home_appliance_rebate_form"] = array(
	"Your_Name"=>"accountHolderFirstName","Residential_Electric_Utility_Account_Number"=>"utilityAccountNumber",
	"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip","Home_Phone"=>"installedPhone","Work_Phone"=>"installedWorkPhone","email_address_RE_Home_Appliance_Rebate_Form"=>"installedEmail",
	"Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",
	"Energy_Star_Appliance"=>"applianceType","Brand"=>"applianceBrand","Model_Number"=>"applianceModelNumber","Serial_Number"=>"applianceSerialNumber",
	"Store_Name"=>"storeName","Store_Address"=>"storeAddress","Store_City"=>"storeCity","Store_State"=>"storeState","Store_Zip"=>"storeZipCode","Purchase_Date"=>"storePurchaseDate","Purchase_Price"=>"storePurchasePrice",
	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials");
	
$sprypointFields["re_commercial_and_industrial_high_efficiency_heating_and_water"]=array(
	 "Account_Holder_Name"=>"accountHolderFirstName","Gas_Account_Number"=>"utilityAccountNumber",

	 "Mailing_Address"=>"mailingAddress",
	 "City"=>"mailingCity",
	 "State"=>"mailingState",
	 "Zip"=>"mailingZip",
	 
	 "email_address"=>"installedEmail",
	 "Fax_Number"=>"installedFax",
	 "Installation_Date"=>"installedDate",
	 "Installed_Address"=>"installedAddress",
	 "Installed_City"=>"installedCity",
	 "Installed_Cost"=>"installedCost",
	 "Installed_State"=>"installedState",
	 "Installed_Zip"=>"installedZip",
	 "Phone_Number"=>"installedPhone",
	 "Property_Owned_or_Leased"=>"installedAddressOwnedOrLeased",
	 
	 "Facility_Name"=>"facilityName",
	 "Facility_Contact"=>"facilityContact",
	 "Facility_City"=>"facilityInstalledCity",
	 "Phone_Number_Facility"=>"facilityPhone",
	 "Facility_Email"=>"facilityEmail",
	 "Installed_Address_Facility"=>"facilityInstalledAddress",
	 "Facility_State"=>"facilityInstalledState",
	 "Facility_Type"=>"facilityType",
	 "Facility_Zip"=>"facilityInstalledZip",
	 "Approximate_Age_of_Facility"=>"facilityApproximateAgeInYears",
	 "Main_Building_Square_Footage"=>"facilitySqrFtMainBuilding",
	 "Other_Buildings_Square_Footage"=>"facilitySqrFtOtherBuildings",
	 "Brief_Overview_of_Space_Use_and_Processes"=>"facilityOverviewOfSpace",

	 "Contractor_Address"=>"contractorAddress",
	 "Contractor_City"=>"contractorCity",
	 "Contractor_Email"=>"contractorEmail",
	 "Contractor_Name"=>"contractorName",
	 "License_Number"=>"contractorLicenseNumber",
	 "Federal_Tax_ID_Number"=>"contractorTaxID",
	 "Contractor_Phone_Number"=>"contractorPhone",
	 "Contractor_State"=>"contractorState",
	 "Contractor_Zip"=>"contractorZip",	 
	 
	 "Manufacturer_furnace"=>"measure_FurnaceManufacturer",
	 "Model_Number_furnace"=>"measure_FurnaceModelNumber",
	 "Efficiency_Rating_furnace"=>"measure_FurnaceEfficiencyRating",
	 "Input_Size_furnace"=>"measure_FurnaceInputSize",
	 "Rebate_Amount_furnace"=>"measure_FurnaceRebateAmount",
	 "Quantity_Installed_furnace"=>"measure_FurnaceQuantityInstalled",
	 "Anticipated_Rebate_furnace"=>"measure_FurnaceInstalledCost",

	 "Manufacturer_boiler"=>"measure_BoilerManufacturer",
	 "Model_Number_boiler"=>"measure_BoilerModelNumber",
	 "Efficiency_Rating_boiler"=>"measure_BoilerEfficiencyRating",
	 "Input_Size_boiler"=>"measure_BoilerInputSize",
	 "Rebate_Amount_boiler"=>"measure_BoilerRebateAmount",
	 "Quantity_Installed_boiler"=>"measure_BoilerQuantityInstalled",
	 "Anticipated_Rebate_boiler"=>"measure_BoilerInstalledCost",

	 "Manufacturer_Water_Heater"=>"measure_IndirectWaterManufacturer",
	 "Model_Number_Water_Heater"=>"measure_IndirectWaterModelNumber",
	 "Efficiency_Rating_Water_Heater"=>"measure_IndirectWaterEfficiencyRating",
	 "Input_Size_Water_Heater"=>"measure_IndirectWaterInputSize",
	 "Rebate_Amount_Water_Heater"=>"measure_IndirectWaterRebateAmount",
	 "Quantity_Installed_Water_Heater"=>"measure_IndirectWaterQuantityInstalled",
	 "Anticipated_Rebate_Water_Heater"=>"measure_IndirectWaterInstalledCost",
	 
	 "Manufacturer_Storage_Water_Heater"=>"measure_StorageWaterManufacturer",
	 "Model_Number_Storage_Water_Heater"=>"measure_StorageWaterModelNumber",
	 "Efficiency_Rating_Storage_Water_Heater"=>"measure_StorageWaterEfficiencyRating",
	 "Input_Size_Storage_Water_Heater"=>"measure_StorageWaterInputSize",
	 "Rebate_Amount_Storage_Water_Heater"=>"measure_StorageWaterRebateAmount",
	 "Quantity_Installed_Storage_Water_Heater"=>"measure_StorageWaterQuantityInstalled",
	 "Anticipated_Rebate_Storage_Water_Heater"=>"measure_StorageWaterInstalledCost",

	 "Manufacturer_Tankless_Water_Heater"=>"measure_TanklessWaterManufacturer",
	 "Model_Number_Tankless_Water_Heater"=>"measure_TanklessWaterModelNumber",
	 "Efficiency_Rating_Tankless_Water_Heater"=>"measure_TanklessWaterEfficiencyRating",
	 "Input_Size_Tankless_Water_Heater"=>"measure_TanklessWaterInputSize",
	 "Rebate_Amount_Tankless_Water_Heater"=>"measure_TanklessWaterRebateAmount",
	 "Quantity_Installed_Tankless_Water_Heater"=>"measure_TanklessWaterQuantityInstalled",
	 "Anticipated_Rebate_Tankless_Water_Heater"=>"measure_TanklessWaterInstalledCost",

	 
	 "Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	 "Total_Anticipated_Rebate"=>"totalAnticipatedRebate",

	 "Customer_Initials"=>"customerInitials",
	 "Date"=>"customerAcknowledgementDate",
	 "employee_initials"=>"staffInitials"

	);
$sprypointFields["re_high_efficiency_pool_pump_rebate_application"]=array(
		"Your_Name"=>"accountHolderFirstName","Residential_Municipal_Electric_Utility_Account_Number"=>"utilityAccountNumber",
		"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip","Home_Phone"=>"installedPhone","Work_Phone"=>"installedWorkPhone","email_address"=>"installedEmail",
		"Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",
		
		"Contractor_Company_Name"=>"contractorName",
		"License_Number"=>"contractorLicenseNumber",
		"Federal_Tax_ID_Number"=>"contractorTaxID",
		"Contact_Person"=>"contractorContactName",
		"Contractor_Address"=>"contractorAddress",
		"Contractor_City"=>"contractorCity",
		"Contractor_Email"=>"contractorEmail",
		"Contractor_Phone"=>"contractorPhone",
		"Contractor_State"=>"contractorState",
		"Contractor_Zip"=>"contractorZip",	 
		
		"Replaced_Pump_Manufacturer"=>"replacedManufacturer",
		"Replaced_Pump_Model_Number"=>"replacedModelNumber",
		"Replaced_Pump_Horsepower"=>"replacedHorsepower",
		"Type_of_Replaced_Pool_Pump"=>"replacedType",
				
		"New_Pump_Manufacturer"=>"newManufacturer",
		"New_Pump_Model_Number"=>"newModelNumber",
		"New_Pump_Horsepower"=>"newHorsepower",
		"Type_of_New_Pool_Pump"=>"newType",

		"Controller_Manufacturer"=>"newControllerManufacturer","Model_Number"=>"newControllerModelNumber",
		
		"Installation_Date"=>"newInstalledDate","Purchase_Date"=>"newPurchaseDate","Purchase_Price"=>"newPrice",
		"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	);
$sprypointFields["re_cool_homes_rebate_application"] = array(
	"Customer_Name"=>"accountHolderFirstName","Electric_Account_Number"=>"utilityAccountNumber",
	"Address"=>"installedAddress","City"=>"installedCity","State"=>"installedState","Zip"=>"installedZip","Home_Phone"=>"installedPhone","Business_Phone"=>"installedWorkPhone","email_address"=>"installedEmail",
	"Rebate_Name"=>"rebateMadePayableTo","Rebate_Address"=>"mailingAddress","Rebate_City"=>"mailingCity","Rebate_State"=>"mailingState","Rebate_Zip"=>"mailingZip",

	"Company_Name"=>"contractorName",
//	"License_Number"=>"contractorLicenseNumber",
//	"Federal_Tax_ID_Number"=>"contractorTaxID",
	"Contact_Person"=>"contractorContactName",
	"Contractor_Address"=>"contractorAddress",
	"Contractor_City"=>"contractorCity",
	"Contractor_Email_Address"=>"contractorEmail",
	"Telephone_Number"=>"contractorPhone",
	"Contractor_State"=>"contractorState",
	"Contractor_Zip"=>"contractorZip",	 
	"Is_technician_NATE_Certified"=>"contractorNATECertified",
	
	"New_Equipment_Installed"=>"equipmentInstalledType",
	"AHRI_Rated_EER"=>"equipmentAHRIRatedSEER","AHRI_Rated_SEER"=>"equipmentAHRIRatedEER","AHRI_Ref_Number"=>"equipmentAHRIRef",
	"Coil_Model_Number"=>"equipmentCoilModelNumber","Condenser_Model_Number"=>"equipmentCondensorModelNumber",
	"HSPF"=>"equipmentHSPF","TXV_or_EXV_Installed"=>"equipmentTXVorEXV","Manufacturer"=>"equipmentManufacturer","Mini_Split"=>"equipmentMiniSplit",
	"New_Unit_Size"=>"equipmentNewUnitSize","Rebate_Amount"=>"equipmentRebateAmount",
	"Install_Cost"=>"equipmentInstalledCost","Install_Date"=>"equipmentInstalledDate",

	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	
	);
	
$sprypointFields["re_residential_high_efficiency_heating_and_water_heating_rebate_program"] = array(
	"Account_Holder_Name"=>"accountHolderFirstName","Gas_Account_Number"=>"utilityAccountNumber",
	"Mailing_Address"=>"mailingAddress","City"=>"mailingCity","State"=>"mailingState","Zip"=>"mailingZip",

	"email_address"=>"installedEmail",
	"Fax_Number"=>"installedFax",
	"Installation_Date"=>"installedDate",
	"Installed_Address"=>"installedAddress",
	"Installed_City"=>"installedCity",
	"Installed_Cost"=>"installedCost",
	"Installed_State"=>"installedState",
	"Installed_Zip"=>"installedZip",
	"Phone_Number"=>"installedPhone",

	"Property_Owned_or_Leased"=>"ownedOrLeased",
	"Landlord_Owner_Address"=>"ownerAddress",
	"Landlord_Owner_City"=>"ownerCity",
	"Landlord_Owner_Eml"=>"ownerEmail",
	"Landlord_Owner_Fax_Number"=>"ownerFax",
	"Landlord_Owner_Name"=>"ownerFirstName",
	"Tax_ID_Number"=>"ownerTaxId",
	"Landlord_Owner_Phone_Number"=>"ownerPhone",
	"Landlord_Owner_State"=>"ownerState",
	"Landlord_Owner_Zip"=>"ownerZip",

	"Contractor_Address"=>"contractorAddress",
	"Contractor_City"=>"contractorCity",
	"Contractor_Eml"=>"contractorEmail",
	"Contractor_Name"=>"contractorName",
	"License_Number"=>"contractorLicenseNumber",
	"Federal_Tax_ID_Number"=>"contractorTaxID",
	"Contractor_Phone_Number"=>"contractorPhone",
	"Contractor_State"=>"contractorState",
	"Contractor_Zip"=>"contractorZip",	 

	"Manufacturer_furnace"=>"measure_FurnaceManufacturer",
	"Model_Number_furnace"=>"measure_FurnaceModelNumber",
	"Efficiency_Rating_furnace"=>"measure_FurnaceEfficiencyRating",
	"Input_Size_furnace"=>"measure_FurnaceInputSize",
	"Rebate_Amount_furnace"=>"measure_FurnaceRebateAmount",
	"Quantity_Installed_furnace"=>"measure_FurnaceQuantityInstalled",
	"Anticipated_Rebate_furnace"=>"measure_FurnaceInstalledCost",

	"Manufacturer_boiler"=>"measure_BoilerManufacturer",
	"Model_Number_boiler"=>"measure_BoilerModelNumber",
	"Efficiency_Rating_boiler"=>"measure_BoilerEfficiencyRating",
	"Input_Size_boiler"=>"measure_BoilerInputSize",
	"Rebate_Amount_boiler"=>"measure_BoilerRebateAmount",
	"Quantity_Installed_boiler"=>"measure_BoilerQuantityInstalled",
	"Anticipated_Rebate_boiler"=>"measure_BoilerInstalledCost",

	"Manufacturer_water_heater"=>"measure_IndirectWaterManufacturer",
	"Model_Number_water_heater"=>"measure_IndirectWaterModelNumber",
	"Efficiency_Rating_water_heater"=>"measure_IndirectWaterEfficiencyRating",
	"Input_Size_water_heater"=>"measure_IndirectWaterInputSize",
	"Rebate_Amount_water_heater"=>"measure_IndirectWaterRebateAmount",
	"Quantity_Installed_water_heater"=>"measure_IndirectWaterQuantityInstalled",
	"Anticipated_Rebate_water_heater"=>"measure_IndirectWaterInstalledCost",

	"Manufacturer_storage_water_heater"=>"measure_StorageWaterManufacturer",
	"Model_Number_storage_water_heater"=>"measure_StorageWaterModelNumber",
	"Efficiency_Rating_storage_water_heater"=>"measure_StorageWaterEfficiencyRating",
	"Input_Size_storage_water_heater"=>"measure_StorageWaterInputSize",
	"Rebate_Amount_storage_water_heater"=>"measure_StorageWaterRebateAmount",
	"Quantity_Installed_storage_water_heater"=>"measure_StorageWaterQuantityInstalled",
	"Anticipated_Rebate_storage_water_heater"=>"measure_StorageWaterInstalledCost",

	"Manufacturer_tankless_water_heater"=>"measure_TanklessWaterManufacturer",
	"Model_Number_tankless_water_heater"=>"measure_TanklessWaterModelNumber",
	"Efficiency_Rating_tankless_water_heater"=>"measure_TanklessWaterEfficiencyRating",
	"Input_Size_tankless_water_heater"=>"measure_TanklessWaterInputSize",
	"Rebate_Amount_tankless_water_heater"=>"measure_TanklessWaterRebateAmount",
	"Quantity_Installed_tankless_water_heater"=>"measure_TanklessWaterQuantityInstalled",
	"Anticipated_Rebate_tankless_water_heater"=>"measure_TanklessWaterInstalledCost",


	"Rebate_Made_Payable_To"=>"rebateMadePayableTo",
	"Total_Anticipated_Rebate"=>"totalAnticipatedRebate",

	"Customer_initials"=>"customerInitials",
	"Date"=>"customerAcknowledgementDate",
	"employee_initials"=>"staffInitials"
	);

$sprypointFields["re_home_efficiency_incentive_program"] = array(
	"Account_Holder_First_Name"=>"accountHolderFirstName",
	"Account_Holder_Last_Name"=>"accountHolderLastName",
	
	"Name_of_Municipal_Utility"=>"utilityName","Utility_Account_Number"=>"utilityAccountNumber",
	"Rebate_Made_Payable_To"=>"rebateMadePayableTo","Mailing_Address"=>"mailingAddress","Mailing_City"=>"mailingCity","Mailing_State"=>"mailingState","Mailing_Zip"=>"mailingZip",
	
	"Installed_Address"=>"installedAddress","Installed_City"=>"installedCity","Installed_State"=>"installedState","Installed_Zip"=>"installedZip","Phone_Number"=>"installedPhone","Fax_Number"=>"installedFax","email_address"=>"installedEmail",
	
	"Property_Owned_or_Leased"=>"ownedOrLeased",
	"Owner_Landlord_Name"=>"ownerFirstName","Tax_ID_Number"=>"ownerTaxId",
	"Owner_Landlord_Address"=>"ownerAddress","Owner_Landlord_City"=>"ownerCity","Owner_Landlord_State"=>"ownerState","Owner_Landlord_Zip"=>"ownerZip",
	"Owner_Landlord_Eml"=>"ownerEmail","Owner_Landlord_Fax_Number"=>"ownerFax","Owner_Landlord_Phone_Number"=>"ownerPhone",

	"Contractor_Address"=>"contractorAddress",
	"Contractor_City"=>"contractorCity",
	"Contractor_Eml"=>"contractorEmail",
	"Contractor_Name"=>"contractorName",
	"License_Number"=>"contractorLicenseNumber",
	"Federal_Tax_ID_Number"=>"contractorTaxID",
	"Contractor_Phone_Number"=>"contractorPhone",
	"Contractor_State"=>"contractorState",
	"Contractor_Zip"=>"contractorZip",	 
	
	"Provide_Description_of_Measure"=>"measuresInstalled",
	
	"Customer_Initials"=>"customerInitials","Date"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials"
	);


*/
?>