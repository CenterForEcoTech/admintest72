<?php
		$dataSet["replacedManufacturer"] = "RManu".$randNumb;
		$dataSet["replacedModelNumber"] = "RModel".$randNumb;
		$dataSet["replacedHorsepower"] = "RHorse".$randNumb;
		$dataSet["replacedType"] = "SingleSpeed";

		$dataSet["newManufacturer"] = "NewManu".$randNumb;
		$dataSet["newModelNumber"] = "NewModel".$randNumb;
		$dataSet["newHorsepower"] = "NewHorse".$randNumb;
		$dataSet["newType"] = "TwoSpeed";
		$dataSet["newControllerManufacturer"] = "NewContManu".$randNumb;
		$dataSet["newControllerModelNumber"] = "NewContModel".$randNumb;
		$dataSet["newPrice"] = $randNumb.".00";
		$dataSet["newPurchaseDate"] = date("Y-m-d");
		$dataSet["newInstalledDate"] = date("Y-m-d");
		$dataSet["supportingFile"] = "FileLocation".$randNumb;
?>