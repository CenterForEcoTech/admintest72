<?php
		$dataSet["measure_FurnaceManufacturer"] = "MFurnaceManu".$randNumb;
		$dataSet["measure_FurnaceModelNumber"] = "MFurnaceModel".$randNumb;
		$dataSet["measure_FurnaceEfficiencyRating"] = "MFurnaceRating".$randNumb;
		$dataSet["measure_FurnaceInputSize"] = "MFurnaceInput".$randNumb;
		$dataSet["measure_FurnaceRebateAmount"] = 18.00;
		$dataSet["measure_FurnaceQuantityInstalled"] = 1;
		$dataSet["measure_FurnaceInstalledCost"] = 18.00;
		
		$dataSet["measure_BoilerManufacturer"] = "MBoilerManu".$randNumb;
		$dataSet["measure_BoilerModelNumber"] = "MBoilerModel".$randNumb;
		$dataSet["measure_BoilerEfficiencyRating"] = "MBoilerRating".$randNumb;
		$dataSet["measure_BoilerInputSize"] = "MBoilerInput".$randNumb;
		$dataSet["measure_BoilerRebateAmount"] = 19.00;
		$dataSet["measure_BoilerQuantityInstalled"] = 1;
		$dataSet["measure_BoilerInstalledCost"] = 19.00;
		
		$dataSet["measure_IndirectWaterManufacturer"] = "MIndirectManu".$randNumb;
		$dataSet["measure_IndirectWaterModelNumber"] = "MIndirectModel".$randNumb;
		$dataSet["measure_IndirectWaterEfficiencyRating"] = "MIndirectRating".$randNumb;
		$dataSet["measure_IndirectWaterInputSize"] = "MIndirectInput".$randNumb;
		$dataSet["measure_IndirectWaterRebateAmount"] = 20.00;
		$dataSet["measure_IndirectWaterQuantityInstalled"] = 1;
		$dataSet["measure_IndirectWaterInstalledCost"] = 20.00;
		
		$dataSet["measure_StorageWaterManufacturer"] = "MStorageManu".$randNumb;
		$dataSet["measure_StorageWaterModelNumber"] = "MStorageModel".$randNumb;
		$dataSet["measure_StorageWaterEfficiencyRating"] = "MStorageRating".$randNumb;
		$dataSet["measure_StorageWaterInputSize"] = "MStorageInput".$randNumb;
		$dataSet["measure_StorageWaterRebateAmount"] = 21.00;
		$dataSet["measure_StorageWaterQuantityInstalled"] = 1;
		$dataSet["measure_StorageWaterInstalledCost"] = 21.00;
		
		$dataSet["measure_TanklessWaterManufacturer"] = "MTanklessManu".$randNumb;
		$dataSet["measure_TanklessWaterModelNumber"] = "MTanklessModel".$randNumb;
		$dataSet["measure_TanklessWaterEfficiencyRating"] = "MTanklessRating".$randNumb;
		$dataSet["measure_TanklessWaterInputSize"] = "MTanklessInput".$randNumb;
		$dataSet["measure_TanklessWaterRebateAmount"] = 22.00;
		$dataSet["measure_TanklessWaterQuantityInstalled"] = 1;
		$dataSet["measure_TanklessWaterInstalledCost"] = 22.00;
		
		$dataSet["totalInstalledCost"] = 125.00;
		$dataSet["installedCost"] = 65.00;
		$dataSet["installedDate"] = date("Y-m-d");
		$dataSet["supportingFile"] = "FileLocation".$randNumb;
?>