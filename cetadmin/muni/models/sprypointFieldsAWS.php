<?php
//valid field matching as of 10/26/2017
$FieldArray_Appliance = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","email"=>"installedEmail",
	"city"=>"installedCity","phoneNumber"=>"installedPhone",
	"state"=>"installedState","zip"=>"installedZip",
	
	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",

	"radioField1"=>"applianceType","brand"=>"applianceBrand","purchasePrice"=>"storePurchasePrice",
	"model"=>"applianceModelNumber","datePurchased"=>"storePurchaseDate","serialNumberRebatewillnotbeprocessedwithouttheserialnumber"=>"applianceSerialNumber",
	"storeName"=>"storeName","storeStreetAddress"=>"storeAddress","storeCity"=>"storeCity","storeState"=>"storeState","storeZip"=>"storeZipCode",

	"signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing"
	);
	
	
//valid field matching as of 10/27/2017
$FieldArray_PoolPump=array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",
	
	"companyName"=>"contractorName","contractorLicenseNumber"=>"contractorLicenseNumber",
	"streetAddress"=>"contractorAddress","city1"=>"contractorCity","state2"=>"contractorState","zip2"=>"contractorZip",
	"contractorPhoneNumber"=>"contractorPhone","contractorEmail"=>"contractorEmail",

	"replacedPumpManufacturer"=>"replacedManufacturer","replacedModelNo"=>"replacedModelNumber","replacedPumpHorsepower"=>"replacedHorsepower","replacedPumpSpeedsingleorother"=>"replacedType",
	"pumpManufacturer"=>"newManufacturer","modelNo"=>"newModelNumber","horsepower"=>"newHorsepower","speedtwospeedorvariable"=>"newType",
	"controllerManufacturer"=>"newControllerManufacturer","controllerModelNumber"=>"newControllerModelNumber",
	"purchaseDate"=>"newPurchaseDate","purchasePrice"=>"newPrice","installDate"=>"newInstalledDate",

	"signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing","status"=>"nothing"
	);
//valid field matching as of 10/26/2017
$FieldArray_CoolHome = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",
	
	"companyName"=>"contractorName","contractorLicenseNumber"=>"contractorLicenseNumber",
	"streetAddress"=>"contractorAddress","city1"=>"contractorCity","state2"=>"contractorState","zip2"=>"contractorZip",
	"contractorPhoneNumber"=>"contractorPhone","contractorEmail"=>"contractorEmail",
	
	"landlordOwnerName"=>"ownerFirstName","taxIdNumberifincorporated"=>"ownerTaxId",
	"landlordOwnerStreetAddress"=>"ownerAddress","landlordOwnerCity"=>"ownerCity","landlordOwnerState"=>"ownerState","landlordOwnerZip"=>"ownerZip",
	"landlordOwnerEmailAddress"=>"ownerEmail","landlordOwnerPhoneNumber"=>"ownerPhone",

	"centralACorHeatPumpInformationtobecompletedbycontractor"=>"equipmentInstalledType",
	"coilModelNo"=>"equipmentCoilModelNumber","condenserModelNo"=>"equipmentCondensorModelNumber",
	"eer"=>"equipmentAHRIRatedEER","seer"=>"equipmentAHRIRatedSEER",
	"hspf"=>"equipmentHSPF","equipmentTXVorEXV"=>"equipmentTXVorEXV",
	"manufacturer"=>"equipmentManufacturer","sizetons"=>"equipmentNewUnitSize",

	"equipmentAHRIRef"=>"equipmentAHRIRef",
	
	"CentralAC16Manufacturer"=>"measure_CentralAC16Manufacturer",
	"CentralAC16CondenserModelNumber"=>"measure_CentralAC16CondenserModelNumber",
	"CentralAC16CoilModelNumber"=>"measure_CentralAC16CoilModelNumber",
	"CentralAC16SEER"=>"measure_CentralAC16SEER",
	"CentralAC16EER"=>"measure_CentralAC16EER",
	"CentralAC16HSPF"=>"measure_CentralAC16HSPF",
	"CentralAC16SizeTons"=>"measure_CentralAC16SizeTons",
	"CentralAC16InstallCost"=>"measure_CentralAC16InstallCost",
	"CentralAC16Rebate"=>"measure_CentralAC16Rebate",
	"CentralAC16QtyInstalled"=>"measure_CentralAC16QtyInstalled",
	"CentralAC16TotalRebate"=>"measure_CentralAC16TotalRebate",
	"CentralAC16Approved"=>"measure_CentralAC16Approved",

	"CentralAC18Manufacturer"=>"measure_CentralAC18Manufacturer",
	"CentralAC18CondenserModelNumber"=>"measure_CentralAC18CondenserModelNumber",
	"CentralAC18CoilModelNumber"=>"measure_CentralAC18CoilModelNumber",
	"CentralAC18SEER"=>"measure_CentralAC18SEER",
	"CentralAC18EER"=>"measure_CentralAC18EER",
	"CentralAC18HSPF"=>"measure_CentralAC18HSPF",
	"CentralAC18SizeTons"=>"measure_CentralAC18SizeTons",
	"CentralAC18InstallCost"=>"measure_CentralAC18InstallCost",
	"CentralAC18Rebate"=>"measure_CentralAC18Rebate",
	"CentralAC18QtyInstalled"=>"measure_CentralAC18QtyInstalled",
	"CentralAC18TotalRebate"=>"measure_CentralAC18TotalRebate",
	"CentralAC18Approved"=>"measure_CentralAC18Approved",
	
	"Ductless18Manufacturer"=>"measure_Ductless18Manufacturer",
	"Ductless18CondenserModelNumber"=>"measure_Ductless18CondenserModelNumber",
	"Ductless18CoilModelNumber"=>"measure_Ductless18CoilModelNumber",
	"Ductless18SEER"=>"measure_Ductless18SEER",
	"Ductless18EER"=>"measure_Ductless18EER",
	"Ductless18HSPF"=>"measure_Ductless18HSPF",
	"Ductless18SizeTons"=>"measure_Ductless18SizeTons",
	"Ductless18InstallCost"=>"measure_Ductless18InstallCost",
	"Ductless18Rebate"=>"measure_Ductless18Rebate",
	"Ductless18QtyInstalled"=>"measure_Ductless18QtyInstalled",
	"Ductless18TotalRebate"=>"measure_Ductless18TotalRebate",
	"Ductless18Approved"=>"measure_Ductless18Approved",
	
	"Ductless20Manufacturer"=>"measure_Ductless20Manufacturer",
	"Ductless20CondenserModelNumber"=>"measure_Ductless20CondenserModelNumber",
	"Ductless20CoilModelNumber"=>"measure_Ductless20CoilModelNumber",
	"Ductless20SEER"=>"measure_Ductless20SEER",
	"Ductless20EER"=>"measure_Ductless20EER",
	"Ductless20HSPF"=>"measure_Ductless20HSPF",
	"Ductless20SizeTons"=>"measure_Ductless20SizeTons",
	"Ductless20InstallCost"=>"measure_Ductless20InstallCost",
	"Ductless20Rebate"=>"measure_Ductless20Rebate",
	"Ductless20QtyInstalled"=>"measure_Ductless20QtyInstalled",
	"Ductless20TotalRebate"=>"measure_Ductless20TotalRebate",
	"Ductless20Approved"=>"measure_Ductless20Approved",

	"installCost"=>"equipmentInstalledCost",
	"installDate"=>"equipmentInstalledDate",
	"totalRebate"=>"equipmentRebateAmount",
	"qtyInstalled"=>"qtyInstalled",

	"signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"formType"=>"nothing","formId"=>"nothing","rebateAmount"=>"nothing","rebatemadepayabletothiscustomerandaddress"=>"nothing"
	);
//valid field matching as of 10/26/2017
$FieldArray_HeatHotwater = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",
	
	"landlordOwnerName"=>"ownerFirstName","taxIdNumberifincorporated"=>"ownerTaxId",
	"landlordOwnerStreetAddress"=>"ownerAddress","landlordOwnerCity"=>"ownerCity","landlordOwnerState"=>"ownerState","landlordOwnerZip"=>"ownerZip",
	"landlordOwnerEmailAddress"=>"ownerEmail","landlordOwnerPhoneNumber"=>"ownerPhone",

	"companyName"=>"contractorName","contractorLicenseNumber"=>"contractorLicenseNumber",
	"streetAddress"=>"contractorAddress","city1"=>"contractorCity","state2"=>"contractorState","zip2"=>"contractorZip",
	"contractorPhoneNumber"=>"contractorPhone","contractorEmail"=>"contractorEmail",
	
	"HEF_300_Manufacturer"=>"measure_Furnace95Manufacturer",
	"HEF_300_Model"=>"measure_Furnace95ModelNumber",
	"HEF_300_Rating"=>"measure_Furnace95EfficiencyRating",
	"HEF_300_Input"=>"measure_Furnace95InputSize",
	"HEF_300_Total_Rebate"=>"measure_Furnace95RebateAmount",
	"HEF_300_Quantity"=>"measure_Furnace95QuantityInstalled",
	"HEF_300_Installed_Cost"=>"measure_Furnace95InstalledCost",

	"HEF_600_Manufacturer"=>"measure_Furnace97Manufacturer",
	"HEF_600_Model"=>"measure_Furnace97ModelNumber",
	"HEF_600_Rating"=>"measure_Furnace97EfficiencyRating",
	"HEF_600_Input"=>"measure_Furnace97InputSize",
	"HEF_600_Total_Rebate"=>"measure_Furnace97RebateAmount",
	"HEF_600_Quantity"=>"measure_Furnace97QuantityInstalled",
	"HEF_600_Installed_Cost"=>"measure_Furnace97InstalledCost",
	
	"HydronicBoiler_500_Manufacturer"=>"measure_Boiler85Manufacturer",
	"HydronicBoiler_500_Model"=>"measure_Boiler85ModelNumber",
	"HydronicBoiler_500_Rating"=>"measure_Boiler85EfficiencyRating",
	"HydronicBoiler_500_Input"=>"measure_Boiler85InputSize",
	"HydronicBoiler_500_Rebate_Amount"=>"measure_Boiler85RebateAmount",
	"HydronicBoiler_500_Quantity"=>"measure_Boiler85QuantityInstalled",
	"HydronicBoiler_500_Installed_Cost"=>"measure_Boiler85InstalledCost",
	
	"HydronicBoiler_1000_Manufacturer"=>"measure_Boiler90Manufacturer",
	"HydronicBoiler_1000_Model"=>"measure_Boiler90ModelNumber",
	"HydronicBoiler_1000_Rating"=>"measure_Boiler90EfficiencyRating",
	"HydronicBoiler_1000_Input"=>"measure_Boiler90InputSize",
	"HydronicBoiler_1000_Total_Rebate"=>"measure_Boiler90RebateAmount",
	"HydronicBoiler_1000_Quantity"=>"measure_Boiler90QuantityInstalled",
	"HydronicBoiler_1000_Installed_Cost"=>"measure_Boiler90InstalledCost",

	"HydronicBoiler_1500_Manufacturer"=>"measure_Boiler95Manufacturer",
	"HydronicBoiler_1500_Model"=>"measure_Boiler95ModelNumber",
	"HydronicBoiler_1500_Rating"=>"measure_Boiler95EfficiencyRating",
	"HydronicBoiler_1500_Input"=>"measure_Boiler95InputSize",
	"HydronicBoiler_1500_Total_Rebate"=>"measure_Boiler95RebateAmount",
	"HydronicBoiler_1500_Quantity"=>"measure_Boiler95QuantityInstalled",
	"HydronicBoiler_1500_Installed_Cost"=>"measure_Boiler95InstalledCost",
	
	"Integrated_Water_Heater_1200_Input"=>"measure_Integrated90InputSize",
	"Integrated_Water_Heater_1200_Total_Rebate"=>"measure_Integrated90RebateAmount",
	"Integrated_Water_Heater_1200_Manufacturer"=>"measure_Integrated90Manufacturer",
	"Integrated_Water_Heater_1200_Model"=>"measure_Integrated90ModelNumber",
	"Integrated_Water_Heater_1200_Quantity"=>"measure_Integrated90QuantityInstalled",
	"Integrated_Water_Heater_1200_Rating"=>"measure_Integrated90EfficiencyRating",
	"Integrated_Water_Heater_1200_Installed_Cost"=>"measure_Integrated90InstalledCost",

	"Integrated_Water_Heater_1600_Input"=>"measure_Integrated95InputSize",
	"Integrated_Water_Heater_1600_Total_Rebate"=>"measure_Integrated95RebateAmount",
	"Integrated_Water_Heater_1600_Manufacturer"=>"measure_Integrated95Manufacturer",
	"Integrated_Water_Heater_1600_Model"=>"measure_Integrated95ModelNumber",
	"Integrated_Water_Heater_1600_Quantity"=>"measure_Integrated95QuantityInstalled",
	"Integrated_Water_Heater_1600_Rating"=>"measure_Integrated95EfficiencyRating",
	"Integrated_Water_Heater_1600_Installed_Cost"=>"measure_Integrated95InstalledCost",
	
	"IndirectWater_400_Manufacturer"=>"measure_IndirectWaterManufacturer",
	"IndirectWater_400_Model"=>"measure_IndirectWaterModelNumber",
	"IndirectWater_400_Rating"=>"measure_IndirectWaterEfficiencyRating",
	"IndirectWater_400_Input"=>"measure_IndirectWaterInputSize",
	"IndirectWater_400_Total_Rebate"=>"measure_IndirectWaterRebateAmount",
	"IndirectWater_400_Quantity"=>"measure_IndirectWaterQuantityInstalled",
	"IndirectWater_400_Installed_Cost"=>"measure_IndirectWaterInstalledCost",
	
	"WaterStorage_50_Manufacturer"=>"measure_H2OStorage62Manufacturer",
	"WaterStorage_50_Model"=>"measure_H2OStorage62ModelNumber",
	"WaterStorage_50_Rating"=>"measure_H2OStorage62EfficiencyRating",
	"WaterStorage_50_Input"=>"measure_H2OStorage62InputSize",
	"WaterStorage_50_Total_Rebate"=>"measure_H2OStorage62RebateAmount",
	"WaterStorage_50_Quantity"=>"measure_H2OStorage62QuantityInstalled",
	"WaterStorage_50_Installed_Cost"=>"measure_H2OStorage62InstalledCost",
	
	"WaterStorage_100_Manufacturer"=>"measure_H2OStorage67Manufacturer",
	"WaterStorage_100_Model"=>"measure_H2OStorage67ModelNumber",
	"WaterStorage_100_Rating"=>"measure_H2OStorage67EfficiencyRating",
	"WaterStorage_100_Input"=>"measure_H2OStorage67InputSize",
	"WaterStorage_100_Total_Rebate"=>"measure_H2OStorage67RebateAmount",
	"WaterStorage_100_Quantity"=>"measure_H2OStorage67QuantityInstalled",
	"WaterStorage_100_Installed_Cost"=>"measure_H2OStorage67InstalledCost",

	"TanklessWater_500_Manufacturer"=>"measure_Tankless82Manufacturer",
	"TanklessWater_500_Model"=>"measure_Tankless82ModelNumber",
	"TanklessWater_500_Rating"=>"measure_Tankless82EfficiencyRating",
	"TanklessWater_500_Input"=>"measure_Tankless82InputSize",
	"TanklessWater_500_Total_Rebate"=>"measure_Tankless82RebateAmount",
	"TanklessWater_500_Quantity"=>"measure_Tankless82QuantityInstalled",
	"TanklessWater_500_Installed_Cost"=>"measure_Tankless82InstalledCost",

	"TanklessWater_800_Manufacturer"=>"measure_Tankless94Manufacturer",
	"TanklessWater_800_Model"=>"measure_Tankless94ModelNumber",
	"TanklessWater_800_Rating"=>"measure_Tankless94EfficiencyRating",
	"TanklessWater_800_Input"=>"measure_Tankless94InputSize",
	"TanklessWater_800_Total_Rebate"=>"measure_Tankless94RebateAmount",
	"TanklessWater_800_Quantity"=>"measure_Tankless94QuantityInstalled",
	"TanklessWater_800_Installed_Cost"=>"measure_Tankless94InstalledCost",

	"Installation_Date"=>"installedDate","Total_Anticipated_Rebate"=>"totalAnticipatedRebate",
	"signature1"=>"customerInitials","created"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	"qtyInstalled"=>"nothing","signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing","qtyInstalled"=>"nothing"
	);
	
	
//valid field matching as of 10/26/2017
$FieldArray_CommercialHeat = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",

	"companyName"=>"contractorName","contractorLicenseNumber"=>"contractorLicenseNumber",
	"streetAddress"=>"contractorAddress","city1"=>"contractorCity","state2"=>"contractorState","zip2"=>"contractorZip",
	"contractorPhoneNumber"=>"contractorPhone","contractorEmail"=>"contractorEmail",

	"facilityTypeselectone"=>"facilityType",
	"landlordOwnerName"=>"facilityName","taxIdNumberifincorporated"=>"facilityContact",
	"landlordOwnerStreetAddress"=>"facilityInstalledAddress","landlordOwnerCity"=>"facilityInstalledCity","landlordOwnerState"=>"facilityInstalledState","landlordOwnerZip"=>"facilityInstalledZip",
	"landlordOwnerPhoneNumber"=>"facilityPhone","landlordOwnerEmailAddress"=>"facilityEmail",
	
	"HEF_300_Input"=>"measure_Furnace95InputSize",
	"HEF_300_Total_Rebate"=>"measure_Furnace95RebateAmount",
	"HEF_300_Manufacturer"=>"measure_Furnace95Manufacturer",
	"HEF_300_Model"=>"measure_Furnace95ModelNumber",
	"HEF_300_Quantity"=>"measure_Furnace95QuantityInstalled",
	"HEF_300_Rating"=>"measure_Furnace95EfficiencyRating",
	"HEF_300_Installed_Cost"=>"measure_Furnace95InstalledCost",

	"HEF_600_Input"=>"measure_Furnace97InputSize",
	"HEF_600_Total_Rebate"=>"measure_Furnace97RebateAmount",
	"HEF_600_Manufacturer"=>"measure_Furnace97Manufacturer",
	"HEF_600_Model"=>"measure_Furnace97ModelNumber",
	"HEF_600_Quantity"=>"measure_Furnace97QuantityInstalled",
	"HEF_600_Rating"=>"measure_Furnace97EfficiencyRating",
	"HEF_600_Installed_Cost"=>"measure_Furnace97InstalledCost",
	
	"Boiler_500_Input"=>"measure_Boiler85InputSize",
	"Boiler_500_Total_Rebate"=>"measure_Boiler85RebateAmount",
	"Boiler_500_Manufacturer"=>"measure_Boiler85Manufacturer",
	"Boiler_500_Model"=>"measure_Boiler85ModelNumber",
	"Boiler_500_Quantity"=>"measure_Boiler85QuantityInstalled",
	"Boiler_500_Rating"=>"measure_Boiler85EfficiencyRating",
	"Boiler_500_Installed_Cost"=>"measure_Boiler85InstalledCost",

	"Boiler_1000_Input"=>"measure_Boiler90InputSize",
	"Boiler_1000_Total_Rebate"=>"measure_Boiler90RebateAmount",
	"Boiler_1000_Manufacturer"=>"measure_Boiler90Manufacturer",
	"Boiler_1000_Model"=>"measure_Boiler90ModelNumber",
	"Boiler_1000_Quantity"=>"measure_Boiler90QuantityInstalled",
	"Boiler_1000_Rating"=>"measure_Boiler90EfficiencyRating",
	"Boiler_1000_Installed_Cost"=>"measure_Boiler90InstalledCost",

	"Boiler_1500_Input"=>"measure_Boiler95InputSize",
	"Boiler_1500_Total_Rebate"=>"measure_Boiler95RebateAmount",
	"Boiler_1500_Manufacturer"=>"measure_Boiler95Manufacturer",
	"Boiler_1500_Model"=>"measure_Boiler95ModelNumber",
	"Boiler_1500_Quantity"=>"measure_Boiler95QuantityInstalled",
	"Boiler_1500_Rating"=>"measure_Boiler95EfficiencyRating",
	"Boiler_1500_Installed_Cost"=>"measure_Boiler95InstalledCost",

	"Integrated_Water_Heater_1200_Input"=>"measure_Integrated90InputSize",
	"Integrated_Water_Heater_1200_Total_Rebate"=>"measure_Integrated90RebateAmount",
	"Integrated_Water_Heater_1200_Manufacturer"=>"measure_Integrated90Manufacturer",
	"Integrated_Water_Heater_1200_Model"=>"measure_Integrated90ModelNumber",
	"Integrated_Water_Heater_1200_Quantity"=>"measure_Integrated90QuantityInstalled",
	"Integrated_Water_Heater_1200_Rating"=>"measure_Integrated90EfficiencyRating",
	"Integrated_Water_Heater_1200_Installed_Cost"=>"measure_Integrated90InstalledCost",

	"Integrated_Water_Heater_1600_Input"=>"measure_Integrated95InputSize",
	"Integrated_Water_Heater_1600_Total_Rebate"=>"measure_Integrated95RebateAmount",
	"Integrated_Water_Heater_1600_Manufacturer"=>"measure_Integrated95Manufacturer",
	"Integrated_Water_Heater_1600_Model"=>"measure_Integrated95ModelNumber",
	"Integrated_Water_Heater_1600_Quantity"=>"measure_Integrated95QuantityInstalled",
	"Integrated_Water_Heater_1600_Rating"=>"measure_Integrated95EfficiencyRating",
	"Integrated_Water_Heater_1600_Installed_Cost"=>"measure_Integrated95InstalledCost",
	
	"Indirect_Hot_Water_400_Input"=>"measure_IndirectWaterInputSize",
	"Indirect_Hot_Water_400_Total_Rebate"=>"measure_IndirectWaterRebateAmount",
	"Indirect_Hot_Water_400_Manufacturer"=>"measure_IndirectWaterManufacturer",
	"Indirect_Hot_Water_400_Model"=>"measure_IndirectWaterModelNumber",
	"Indirect_Hot_Water_400_Quantity"=>"measure_IndirectWaterQuantityInstalled",
	"Indirect_Hot_Water_400_Rating"=>"measure_IndirectWaterEfficiencyRating",
	"Indirect_Hot_Water_400_Installed_Cost"=>"measure_IndirectWaterInstalledCost",

	"Water_Storage_50_Input"=>"measure_H2OStorage62InputSize",
	"Water_Storage_50_Total_Rebate"=>"measure_H2OStorage62RebateAmount",
	"Water_Storage_50_Manufacturer"=>"measure_H2OStorage62Manufacturer",
	"Water_Storage_50_Model"=>"measure_H2OStorage62ModelNumber",
	"Water_Storage_50_Quantity"=>"measure_H2OStorage62QuantityInstalled",
	"Water_Storage_50_Rating"=>"measure_H2OStorage62EfficiencyRating",
	"Water_Storage_50_Installed_Cost"=>"measure_H2OStorage62InstalledCost",

	"Water_Storage_100_Input"=>"measure_H2OStorage67InputSize",
	"Water_Storage_100_Total_Rebate"=>"measure_H2OStorage67RebateAmount",
	"Water_Storage_100_Manufacturer"=>"measure_H2OStorage67Manufacturer",
	"Water_Storage_100_Model"=>"measure_H2OStorage67ModelNumber",
	"Water_Storage_100_Quantity"=>"measure_H2OStorage67QuantityInstalled",
	"Water_Storage_100_Rating"=>"measure_H2OStorage67EfficiencyRating",
	"Water_Storage_100_Installed_Cost"=>"measure_H2OStorage67InstalledCost",

	"Tankless_Water_500_Input"=>"measure_Tankless82InputSize",
	"Tankless_Water_500_Rebate_Amount"=>"measure_Tankless82RebateAmount",
	"Tankless_Water_500_Manufacturer"=>"measure_Tankless82Manufacturer",
	"Tankless_Water_500_Model"=>"measure_Tankless82ModelNumber",
	"Tankless_Water_500_Quantity"=>"measure_Tankless82QuantityInstalled",
	"Tankless_Water_500_Rating"=>"measure_Tankless82EfficiencyRating",
	"Tankless_Water_500_Installed_Cost"=>"measure_Tankless82InstalledCost",

	"Tankless_Water_800_Input"=>"measure_Tankless94InputSize",
	"Tankless_Water_800_Total_Rebate"=>"measure_Tankless94RebateAmount",
	"Tankless_Water_800_Manufacturer"=>"measure_Tankless94Manufacturer",
	"Tankless_Water_800_Model"=>"measure_Tankless94ModelNumber",
	"Tankless_Water_800_Quantity"=>"measure_Tankless94QuantityInstalled",
	"Tankless_Water_800_Rating"=>"measure_Tankless94EfficiencyRating",
	"Tankless_Water_800_Installed_Cost"=>"measure_Tankless94InstalledCost",


	"Installation_Date"=>"installedDate","Total_Anticipated_Rebate"=>"totalAnticipatedRebate",
	"signature1"=>"customerInitials","created"=>"customerAcknowledgementDate","employee_initials"=>"staffInitials",
	"qtyInstalled"=>"nothing","signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing","qtyInstalled"=>"nothing","status"=>"nothing"
	
	);
	

//valid field matching as of 10/26/2017
$FieldArray_Efficiency = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","state1"=>"mailingState","zip1"=>"mailingZip",
	
	"companyName"=>"contractorName","contractorLicenseNumber"=>"contractorLicenseNumber",
	"streetAddress"=>"contractorAddress","city1"=>"contractorCity","state2"=>"contractorState","zip2"=>"contractorZip",
	"contractorPhoneNumber"=>"contractorPhone","contractorEmail"=>"contractorEmail",
	
	"landlordOwnerName"=>"ownerFirstName","taxIdNumberifincorporated"=>"ownerTaxId",
	"landlordOwnerStreetAddress"=>"ownerAddress","landlordOwnerCity"=>"ownerCity","landlordOwnerState"=>"ownerState","landlordOwnerZip"=>"ownerZip",
	"landlordOwnerEmailAddress"=>"ownerEmail","landlordOwnerPhoneNumber"=>"ownerPhone",


	"centralACorHeatPumpInformationtobecompletedbycontractor"=>"eligibleMeasures","measureDescriptionieairsealinghoursinsulationlocationinsulationdescriptionRvalueaddedheatingsystemmakeandmodel"=>"measuresInstalled",

	"signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing","status"=>"nothing"
);
//valid field matching as of 10/26/2017
$FieldArray_WiFi = array(
	"customerName"=>"accountHolderFirstName","westfieldGasElectricAccountNumber"=>"utilityAccountNumber",
	"streetAddresswhereequipmentwasinstalled"=>"installedAddress","city"=>"installedCity","state"=>"installedState","zip"=>"installedZip",
	"phoneNumber"=>"installedPhone","email"=>"installedEmail",

	"rebateMailingAddress"=>"mailingAddress","rebateCity"=>"mailingCity","rebatestate"=>"mailingState","rebatezip"=>"mailingZip",

	"manufacturer1"=>"measure_WiFiManufacturer",
	"modelNumber1"=>"measure_WiFiModelNumber",
	"dateInstalled1"=>"measure_WiFiDateInstalled",
	"totalRebate"=>"measure_WiFiTotalAmount",
	"quantity1"=>"measure_WiFiQuantity",
	"purchasePrice1"=>"measure_WiFiPurchasePrice",
	
	"signature1"=>"customerInitials","pleaseuploadyourcurrentelectricbillanddatedreceipt"=>"customerAcknowledgementDate","created"=>"customerAcknowledgementDate",
	"rebatemadepayabletothiscustomerandaddress"=>"nothing","testDate"=>"nothing","formType"=>"nothing","formId"=>"nothing","status"=>"nothing"
);

//forms automated:appliance, poolpump,efficiency,wifi,coolHome, HeatHotwater
	
//valid as of 10/26/2017	
$sprypointFormTypes = array(
	"589a11cf110e2100735d77ee"=>"CommercialHeat",
	"5899ebddda01a9006cfad05e"=>"Appliance",
	"5899eb8e110e2100735d771b"=>"PoolPump",
	"5899dad6110e2100735d76ad"=>"CoolHome",
	"5899e658110e2100735d76e9"=>"HeatHotwater",
	"589a1108da01a9006cfad161"=>"Efficiency",
	"58e7ec54412603008b728222"=>"WiFi"
	);	
	
	
$sprypointFields["589a11cf110e2100735d77ee"] = $FieldArray_CommercialHeat;	
$sprypointFields["5899ebddda01a9006cfad05e"] = $FieldArray_Appliance;
$sprypointFields["5899eb8e110e2100735d771b"] = $FieldArray_PoolPump;	
$sprypointFields["5899dad6110e2100735d76ad"] = $FieldArray_CoolHome;
$sprypointFields["5899e658110e2100735d76e9"] = $FieldArray_HeatHotwater;
$sprypointFields["589a1108da01a9006cfad161"] = $FieldArray_Efficiency;
$sprypointFields["58e7ec54412603008b728222"] = $FieldArray_WiFi;

?>