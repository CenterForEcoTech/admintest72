<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxCustomers" ).combobox({
				select: function(event, ui){
					<?php if (!$customerComboBoxFunction){?>
					window.location = "?nav=<?php echo $_GET['nav'];?>&CustomerID="+$(ui.item).val();
					<?php }else{
						echo $customerComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
			var timer = null,
				uiautocomplete = $(".ui-autocomplete"),
				addNewCustomerButton = $("#addNewCustomerButton");

			$(".custom-combobox-input").on('keyup',function(){
				$("#customerNotFound").val($(this).val());
				clearTimeout(timer);
				timer = setTimeout(updateComments, 1000);
			});
			var updateComments = function(){
				var autocompleteDisplay = uiautocomplete.css('display');
				if (autocompleteDisplay == "none"){
					addNewCustomerButton.css('display','block');
				}else{
					addNewCustomerButton.css('display','none');
				}
			}
		  });
		</script>			
		<div class="<?php echo ($customerComboBoxDivColumns ? $customerComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$customerComboBoxHideLabel){?>
				<label>Start typing customer name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxCustomers parameters" id="SelectedCustomer" data-hiddenid="customerNotFound">
				<option value="">Select one...</option>
				<?php 
					foreach ($CustomerByID as $customer=>$customerInfo){
						echo "<option value=\"".$customerInfo->id."\"".($SelectedCustomerID == $customerInfo->id ? " selected" : "").">".$customerInfo->accountHolderFirstName." ".$customerInfo->accountHolderLastName." [".$customerInfo->utilityAccountNumber."]</option>";
					}
				?>
			  </select>
			</div>
			<div>
			  <a href="#" class="three columns button-link" id="addNewCustomerButton" style="display:none;">Add As New Customer</a>
			  <input type="hidden" id="customerNotFound">
			</div>
		</div>