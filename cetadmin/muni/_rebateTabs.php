<div id="<?php echo (!$showRebatesDropDown ? "rebateDropDownItems" : "rebateItems");?>">
	<a id="rebate-heathotwater" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-heathotwater" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Residential Heat & Hot Water</a><br>
	<a id="rebate-efficiency" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-efficiency" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Efficiency</a><br>
	<a id="rebate-coolhome" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-coolhome" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Central AC HeatPump</a><br>
	<a id="rebate-poolpump" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-poolpump" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Pool Pump</a><br>
	<a id="rebate-appliance" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-appliance" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Appliance</a><br>
	<a id="rebate-commercialheat" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-commercialheat" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Commercial Heat</a><br>
	<a id="rebate-wifi" href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=rebate-wifi" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">WiFi</a><br>	
	
</div>