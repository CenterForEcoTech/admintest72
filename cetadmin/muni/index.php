<?php
ini_set('memory_limit','14096M');
set_time_limit(0);

include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - Municipalities";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "auditForm":
            $displayPage = "views/_auditTemplate.php";
            break;
        case "auditCustomerReport":
            $displayPage = "views/_auditCustomerReport.php";
            break;
        case "rebateForm":
            $displayPage = "views/_rebateForm.php";
            break;
        case "rebateTester":
            $displayPage = "views/tester.php";
            break;
        case "phonelog":
            $displayPage = "views/_phoneLog.php";
            break;
        case "customer-data":
            $displayPage = "views/_customerData.php";
            break;
        case "customer-intake":
            $displayPage = "views/_customerIntake.php";
            break;
		case "formReceiver":
            $displayPage = "views/receiver.php";
            break;
		case "attachments":
            $displayPage = "views/attachments.php";
            break;
		case "pdfs":
            $displayPage = "views/pdfs.php";
            break;
		case "attachmentTool":
            $displayPage = "views/attachmentTool.php";
            break;
		case "reports":
			$displayPage = "_reportTabs.php";
			break;
		case "report-approved":
			$displayPage = "views/_reportApproved.php";
			break;
		case "report-paid":
			$displayPage = "views/_reportPaid.php";
			break;
		case "rebates":
			$displayPage = "_rebateTabs.php";
			break;
		case "rebate-heathotwater":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-efficiency":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-coolhome":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-poolpump":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-appliance":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-commercialheat":
			$displayPage = "views/_rebateTemplate.php";
			break;
		case "rebate-wifi":
			$displayPage = "views/_rebateTemplate.php";
			break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '<div class="container">';
if ($_GET['actualAudit']){
	echo '<h3>Muni Audit</h1>';	
	$actualAudit = true;
}else{
	echo '<h1>Municipalities Administration</h1>';
	include_once("_muniMenu.php");
}
include_once('views/_phoneLogSessionInfo.php');
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>