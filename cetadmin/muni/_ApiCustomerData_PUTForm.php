<?php
				$results->formName = $formName;
				//check to see if customer already in system before adding new customer
				$criteria = new stdClass();
				$criteria->utilityAccountNumber = $newRecord->utilityAccountNumber;
				$customerLookup = $muniProvider->getResCustomer($criteria);
				
				$customerRecord = new stdClass();
				$customerRecord = $newRecord;
				$customerRecord->rebateType = $newRecord->formName;
				$customerRecord->updatedTimeStamp = date("Y-m-d H:i:s");
				$results->customerLookup =$customerLookup; 
				
				if (!$customerRecord->accountHolderLastName){
					$accountName = explode(" ",$customerRecord->accountHolderFirstName);
					$customerRecord->accountHolderFirstName = "";
					if (count($accountName) > 1){
						$accountLastName = array_pop($accountName); //remove last element
						$accountFirstName = implode(" ",$accountName);
					}else{
						$accountLastName = $accountFirstName = $customerRecord->accountHolderFirstName;
					}
					$customerRecord->accountHolderFirstName = trim(ucwords(strtolower($accountFirstName)));
					$customerRecord->accountHolderLastName = ucwords(strtolower($accountLastName));
				}else{
					$customerRecord->accountHolderFirstName = trim(ucwords(strtolower($newRecord->accountHolderFirstName)));
					$customerRecord->accountHolderLastName = ucwords(strtolower($newRecord->accountHolderLastName));
				}
				
				if (!$customerLookup->success){
				//since new customer, add them and process the form with their customer id
					$customerAddResponse = $muniProvider->addResCustomer($customerRecord,getAdminId());
					$customerRecord->customerID=$customerAddResponse->insertedId;
					$results->customerAddRespose = $customerAddResponse;
					$results->customerID = $customerRecord->customerID;
				}else{
//				print_pre($customerRecord);
					$customerCollection = $customerLookup->collection;
					$currentCustomer = $customerCollection[0];
					$updateCustomer = (object) array_merge((array)$currentCustomer, (array)$customerRecord);
					$updateCustomer->id = ($customerRecord->customerID ? $customerRecord->customerID : $currentCustomer->id);
					$customerRecord->customerID = ($customerRecord->customerID ? $customerRecord->customerID : $currentCustomer->id);
					$results->combinedUpdateCustomerRecord = $updateCustomer;
					$customerUpdateResponse = $muniProvider->updateResCustomer($updateCustomer,getAdminId());
					$results->utilityAccountNumber = $newRecord->utilityAccountNumber;
					$results->customerUpdateResponse = $customerUpdateResponse;
				}
				$action = $newRecord->action;
				$actionParts = explode("_",$action);
				if (count($actionParts)){
					$actionRebate = ucfirst($actionParts[0]);
					$actionType = $actionParts[1];
				}
				$classActionName = $actionType.$actionRebate.$formName;
				//addRebatePoolPump,addRebateAppliance,addRebateCoolHome,addRebateHeatHotwater,addRebateEfficiency,addRebateCommercialHeat
				//updateRebatePoolPump,updateRebateAppliance,updateRebateCoolHome,updateRebateHeatHotwater,updateRebateEfficiency,updateRebateCommercialHeat
				$rebateInsertReponse = $muniProvider->{$classActionName}($customerRecord,getAdminId());
				
				$resultFieldName = "form".$actionType."Response";
				$results->{$resultFieldName} = $rebateInsertReponse;
?>