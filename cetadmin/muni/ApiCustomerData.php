<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."MuniProvider.php");
include_once($dbProviderFolder."APIAuthProvider.php");
$results = new stdClass();

$newRecord = json_decode($currentRequest->rawData);
$key =  $newRecord->key;
//$key = "skeletonCET35";
//make sure ip is authorized
$ipAddressSource = $_SERVER['REMOTE_ADDR'];
$criteria = new stdClass();
$criteria->ip = $ipAddressSource;
$criteria->secret = $secret;
$apiAuthProvider = new APIAuthProvider($dataConn);
$apiResults = $apiAuthProvider->get($criteria);
$apiAuthorized = true;
/*if (count($apiResults)){
	$apiStatus = $apiResults[0]->status;
	if ($apiStatus != "active"){
		$results = "IP Address Status Not Active";
		header("HTTP/1.0 401 Unauthorized");
	}else{
		$apiAuthorized = true;
		//log this authorization
		$id = $apiResults[0]->id;
		$apiAccessLogResults = $apiAuthProvider->addAccessLog($id,$ipAddressSource);
	}
}else{
	$results = "IP Address ".$ipAddressSource." Not Authorized";
	header("HTTP/1.0 401 Unauthorized");
}*/
//override for Audit input
$newRecord = json_decode($currentRequest->rawData);
if ($newRecord->action == 'audit_add'){$apiAuthorized = true;}

if ($apiAuthorized){
	switch(strtoupper($currentRequest->verb)){
		case "DELETE":
			$results = "request verb not permitted";
			header("HTTP/1.0 403 Forbidden");
			break;
		case "GET":
			$formType = $_GET['formType'];
			$randNumb = rand(15,200);
			$dataSet["success"] = true;
			switch ($formType){
				case "WiFi":
					include_once('models/rebate_wifi.php');
					break;
				case "PoolPump":
					include_once('models/rebate_poolpump.php');
					break;
				case "Appliance":
					include_once('models/rebate_appliance.php');
					break;
				case "CoolHome":
					include_once('models/rebate_coolhome.php');
					break;
				case "HeatHotwater":
					include_once('models/rebate_heathotwater.php');
					break;
				case "Efficiency":
					include_once('models/rebate_efficiency.php');
					break;
				case "CommercialHeat":
					$dataSet["customerType"] = "commercial";
					include_once('models/rebate_commercialheat.php');
					break;
				default:
					$dataSet["success"] = false;
					$dataSet["formName"] = "undesignated form";
					$dataSet["currentRequest"] = $currentRequest;
					break;
				
			}
			include_once('models/rebate_commonElements.php');
			$dataSet["formData"] = json_encode($dataSet);
			$results = $dataSet;
			header("HTTP/1.0 200 OK");
			break;
		case "POST":
			$action = $_GET['action'];
			$newRecord = json_decode($currentRequest->rawData);
			if ($action == "fileupload"){
				foreach($_FILES as $fileInfo){
					$fileName = str_replace(")","",str_replace("(","",str_replace(" ","_",$fileInfo['name'])));
					$data = $fileInfo;
					$formName = str_replace("rebate","",$_GET['formName']);
					$oid = $_GET['oid'];
					$path = getcwd();
					$destinationFolder = $path.(strpos($path,"xampp") ? "\\rebateAttachments\\" : "/rebateAttachments/");
					$destinationName = $formName."_".$oid."_".$fileName;
					$destination = $destinationFolder.$destinationName;
					move_uploaded_file($fileInfo["tmp_name"], $destination);
				}				
				
				$results = $destinationName;
				header("HTTP/1.0 200 OK");
				
			}
			if ($action == "fileuploadAdditional"){
				foreach($_FILES as $fileInfo){
					$fileName = str_replace(")","",str_replace("(","",str_replace(" ","_",$fileInfo['name'])));
					$data = $fileInfo;
					$formName = str_replace("rebate","",$_GET['formName']);
					$oid = $_GET['oid'];
					$rebateId = $_GET['rebateId'];
					$path = getcwd();
					$destinationFolder = $path.(strpos($path,"xampp") ? "\\rebateAttachments\\" : "/rebateAttachments/");
					$destinationName = $formName."_".$oid."_".$fileName;
					$destination = $destinationFolder.$destinationName;
					if (file_exists($destination)){
						$destinationName = $formName."_".$oid."_".rand(10,100).$fileName;
						$destination = $destinationFolder.$destinationName;
					}
					move_uploaded_file($fileInfo["tmp_name"], $destination);
					//now update the rebateId
					$updateRecord  = new stdClass();
					$updateRecord->addDocument  = true;
					$updateRecord->rebateId = $rebateId;
					$updateRecord->documentName = $destinationName;
					$updateRecord->rebateName = $formName;
					
					$muniProvider = new MuniProvider($dataConn);
					$response = $muniProvider->updateDocument($updateRecord, getAdminId());
				}		
				$results = $response;
				header("HTTP/1.0 200 OK");
			}
			if ($action == "auditPhotoUpload"){
				foreach($_FILES as $fileInfo){
					$fileName = str_replace(")","",str_replace("(","",str_replace(" ","_",$fileInfo['name'])));
					$fileName = rawurlencode($fileName);
					$data = $fileInfo;
					$customerId = $_GET['customerId'];
					$path = getcwd();
					$destinationFolder = $path.(strpos($path,"xampp") ? "\\auditAttachments\\" : "/auditAttachments/");
					$destinationName = $customerId."_".$fileName;
					$destination = $destinationFolder.$destinationName;
					move_uploaded_file($fileInfo["tmp_name"], $destination);
				}				
				
				$results = $destinationName;
				header("HTTP/1.0 200 OK");
				
			}
			if ($action == "auditPhotoUploadAdditional"){
				foreach($_FILES as $fileInfo){
					$fileName = str_replace(")","",str_replace("(","",str_replace(" ","_",$fileInfo['name'])));
					$fileName = rawurlencode($fileName);
					$data = $fileInfo;
					$customerId = $_GET['customerId'];
					$path = getcwd();
					$destinationFolder = $path.(strpos($path,"xampp") ? "\\auditAttachments\\" : "/auditAttachments/");
					$destinationName = $customerId."_".$fileName;
					$destination = $destinationFolder.$destinationName;
					move_uploaded_file($fileInfo["tmp_name"], $destination);
					//now update the rebateId
					$updateRecord  = new stdClass();
					$updateRecord->addDocument  = true;
					$updateRecord->auditId = $_GET['auditId'];
					$updateRecord->documentName = $destinationName;
					
					$muniProvider = new MuniProvider($dataConn);
					$response = $muniProvider->updateAuditDocument($updateRecord, getAdminId());
				}		
				$results = $response;
				header("HTTP/1.0 200 OK");
			}
			if (!$action){
				$action = $newRecord->action;
				$muniProvider = new MuniProvider($dataConn);
				if ($action == "checkApproved"){
					$fields = $newRecord->fields;
					$rebateName = $newRecord->rebateName;
					$criteria = new stdClass();
					$criteria->rebateName = $rebateName;
					$criteria->fields = $fields;
					$fieldSearch = $muniProvider->checkApprovedRes($criteria);
					$results = $fieldSearch;
					header("HTTP/1.0 200 OK");
				}
				if ($action == "checkContractor"){
					$license = $newRecord->license;
					$criteria = new stdClass();
					$criteria->license = $license;
					$fieldSearch = $muniProvider->checkContractorLicense($criteria);
					$results = $fieldSearch;
					header("HTTP/1.0 200 OK");
				}
			}
			
			break;
		case "PUT":
			$muniProvider = new MuniProvider($dataConn);
			$newRecord = json_decode($currentRequest->rawData);
			$formName = trim($newRecord->formName);
			$testingResult[] = "newRecord: ".print_r($newRecord,true);
			if ($formName != ""){
				include('_ApiCustomerData_PUTForm.php');				
			}else{
				$results = "form not identified";
				//$testingResult[] = "form not identified";
				if (trim($newRecord->action) == 'intake_add'){
					$results = new stdClass();
					$criteria = new stdClass();
					$criteria->utilityAccountNumber = ($newRecord->utilityAccountNumber ? : $newRecord->utilityAccountNumberSource);
					$customerLookup = $muniProvider->getResCustomer($criteria);
					$testingResult[] = $newRecord;
					$testingResult[] = "getRestCustomer Criteria ".print_r($criteria,true);
					$testingResult[] = "getRestCustomer results ".print_r($customerLookup,true);
					
					$customerRecord = new stdClass();
					$customerRecord = $newRecord;
					$customerRecord->auditStatus = $newRecord->auditStatus;
					$customerRecord->updatedTimeStamp = date("Y-m-d H:i:s");
					$results->customerLookup =$customerLookup; 
					if (!count($customerLookup->collection)){
						$testingResult[] = "addResCustomer: ".print_r($customerRecord,true);
						//since new customer, add them and process the form with their customer id
						$customerAddResponse = $muniProvider->addResCustomer($customerRecord,getAdminId());
						$testingResult[] = "customerAddResponse: ".print_r($customerAddResponse,true);
						$customerRecord->customerID=$customerAddResponse->insertedId;
						$results->customerAddRespose = $customerAddResponse;
						$results->customerID = $customerRecord->customerID;
						
					}else{
						$testingResult[] = '$customerLookup->success';
						$customerCollection = $customerLookup->collection;
						$currentCustomer = $customerCollection[0];
						$updateCustomer = (object) array_merge((array)$currentCustomer, (array)$customerRecord);
						$updateCustomer->id = ($customerRecord->customerID ? $customerRecord->customerID : $currentCustomer->id);
						$customerRecord->customerID = ($customerRecord->customerID ? $customerRecord->customerID : $currentCustomer->id);
						$results->combinedUpdateCustomerRecord;
						$customerUpdateResponse = $muniProvider->updateResCustomer($updateCustomer,getAdminId());
						$results->utilityAccountNumber = $newRecord->utilityAccountNumber;
						$results->customerUpdateResponse = $customerUpdateResponse;
					}
					
					//$response = $muniProvider->addResCustomer($newRecord, getAdminId());
					if ($customerRecord->customerID){
						//$results->finalCustomerID = $customerRecord->customerID;
						//$results->testingResults = $testingResult;
						$results = $customerRecord->customerID;
						header("HTTP/1.0 201 Created");
					} else {
						$results = $response->error;
						header("HTTP/1.0 409 Conflict");
					}
				}elseif (trim($newRecord->action) == 'updateCustomerData'){
					$results = new stdClass();
					$customerId = $newRecord->customerId;
					$fieldName = $newRecord->fieldName;
					$value = $newRecord->value;
					${$fieldName} = $value;
					
					//first check to make sure account number is not currently being used by anyone else
					if ($UtilityAccountNumber){
						$results->text = "checking to see if accountNumber: ".$UtilityAccountNumber." exists";
						$criteria = new stdClass();
						$criteria->utilityAccountNumber = $UtilityAccountNumber;
						$customerLookup = $muniProvider->getResCustomer($criteria);
						if (count($customerLookup->collection)){
							if ($customerLookup->collection[0]->customerID==$customerId){
								$results->text = "it is the same client";
							}else{
								$results->text = "belongs to someone else";
							}
						}else{
							$results->text = "new account number";
							$record = new stdClass();
							$record->id=$customerId;
							$record->singleField = true;
							$record->fieldName = $fieldName;
							$record->value = $value;
							$response = $muniProvider->updateResCustomer($record, getAdminId());
						}
					}else{
						$results->text = "updating field not account number";
						$record = new stdClass();
						$record->id=$customerId;
						$record->singleField = true;
						$record->fieldName = $fieldName;
						$record->value = $value;
						$response = $muniProvider->updateResCustomer($record, getAdminId());
					}
					$results->customerLookup = $customerLookup->collection[0]->customerID;
					$results->update = $response;

				}elseif (trim($newRecord->action) == 'intake_update'){
					$response = $muniProvider->updateResCustomer($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_customerstatus'){
					$recordId = $newRecord->recordId;
					$status = $newRecord->status;
					$response = $muniProvider->updateResCustomerStatus($recordId, $status, getAdminId());
					if ($response->success){
						$results = $response;
						header("HTTP/1.0 201 Created");
					} else {
						$results = $response->error;
						header("HTTP/1.0 409 Conflict");
					}
				}elseif (trim($newRecord->action) == 'update_currentstatus'){
					$response = $muniProvider->updateRebateStatus($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_auditstatus'){
					$response = $muniProvider->updateAuditStatus($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_rebate_verifieddate'){
					$response = $muniProvider->updateRebateVerifiedDate($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_audit_verifieddate'){
					$response = $muniProvider->updateAuditVerifiedDate($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'audit_sendemail'){
					$CustomerName = $newRecord->customerName;
					$CustomerID = $newRecord->customerId;
					include_once('views/_auditEmailAlert.php');
					$results = $mail;
					header("HTTP/1.0 201 Created");
				}elseif (trim($newRecord->action) == 'update_auditor'){
					$response = $muniProvider->updateAuditor($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_note'){
					$response = $muniProvider->updateRebateNotes($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_auditnote'){
					$response = $muniProvider->updateAuditNotes($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_approvedamount'){
					$response = $muniProvider->updateRebateApprovedAmount($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'update_document'){
					$response = $muniProvider->updateDocument($newRecord, getAdminId());
					$results = $response;
					/*
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
					*/
				}elseif (trim($newRecord->action) == 'update_auditdocument'){
					$response = $muniProvider->updateAuditDocument($newRecord, getAdminId());
					$results = $response;
					header("HTTP/1.0 201 Created");
				}elseif (trim($newRecord->action) == 'phoneCallInfo_save'){
					$response = $muniProvider->addPhoneLog($newRecord, getAdminId());
						if ($response->success){
							$results = $response->insertedId;
							header("HTTP/1.0 201 Created");
						} else {
							$results = $response->error;
							header("HTTP/1.0 409 Conflict");
						}
				}elseif (trim($newRecord->action) == 'phoneCallSession_remove'){
					unset($_SESSION['PhoneLog']);
					$results = true;
					header("HTTP/1.0 201 Created");
				}elseif (trim($newRecord->action) == 'audit_add'){
					//$results = $newRecord;
					$response = $muniProvider->addAudit($newRecord, getAdminId());
					$results = $response;
					header("HTTP/1.0 201 Created");
				}elseif (trim($newRecord->action) == 'exporttofiscal'){
					$response = $muniProvider->exporttofiscal($newRecord, getAdminId());
					$results = $response;
					$CompleteFileNameCsv = "exports/exportToFiscal".date("Y-m-d_H_i_s").".csv";
					$fpCompleted = fopen($CompleteFileNameCsv, 'w');
					$header = array_keys((array)$newRecord->exportRecords[0]);
					fputcsv($fpCompleted, $header);
					foreach ($newRecord->exportRecords as $exportRecord) {
						fputcsv($fpCompleted, (array)$exportRecord);
					}
					fclose($fpCompleted);
					$fileLink = $CurrentServer.$adminFolder.'muni/'.$CompleteFileNameCsv;
					$results["link"] = $fileLink;
					$results["filename"] = str_replace("exports/","",$CompleteFileNameCsv);
					
					header("HTTP/1.0 201 Created");
				}elseif (trim($newRecord->action) == "addSIR"){
						$updateRecord  = new stdClass();
						$updateRecord->sirItem = $newRecord->sirItem;
						$updateRecord->auditId = $newRecord->auditId;
						$updateRecord->addSIR = true;
					
						$muniProvider = new MuniProvider($dataConn);
						$response = $muniProvider->addSIR($updateRecord, getAdminId());
					$results = $response;
					header("HTTP/1.0 200 OK");
				}elseif (trim($newRecord->action) == "deleteSIR"){
						$updateRecord  = new stdClass();
						$updateRecord->sirItem = $newRecord->sirItem;
						$updateRecord->auditId = $newRecord->auditId;
						$updateRecord->deleteSIR = true;
					
						$muniProvider = new MuniProvider($dataConn);
						$response = $muniProvider->addSIR($updateRecord, getAdminId());
					$results = $response;
					header("HTTP/1.0 200 OK");
				}elseif (trim($newRecord->action) == "revertApproval"){
					$database = $newRecord->database;
					$customerId = $newRecord->customerId;
					$recordId = $newRecord->recordId;
					$muniProvider = new MuniProvider($dataConn);
					$response = $muniProvider->revertApproval($database,$recordId,$customerId);
					$results = $response;
					header("HTTP/1.0 200 OK");
				}else{
					$results = $newRecord;
					header("HTTP/1.0 201 Created");
				}
			}
			break;
	}
}
output_json($results);
die();
?>