<?php
$thisIsAuditPage = true;
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$SelectedCustomerID = $_GET['CustomerID'];

$newCustomerName = $_GET['NewCustomerName'];
if ($newCustomerName){
	$newCustomerNameParts = explode(" ",$newCustomerName);
	$newCustomerFirstName = ucfirst($newCustomerNameParts[0]);
	$newCustomerLastName = ucfirst($newCustomerNameParts[1]);
}
$newPhoneCustomerName = $_GET['NewPhoneCustomer'];
if ($newPhoneCustomerName){
	$newCustomerName = true;
	$newCustomerFirstName = $_SESSION['PhoneLog']->firstName;
	$newCustomerLastName = $_SESSION['PhoneLog']->lastName;
	$newCustomerAccountNumber = $_SESSION['PhoneLog']->accountNumber;
	$newCustomerPhoneNumber = $_SESSION['PhoneLog']->phoneNumber;
}
	
$muniProvider = new MuniProvider($dataConn);
$customerDataResults = $muniProvider->getResCustomer();
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
$auditFields = array("auditFloorArea","auditHouseWidth","auditHouseLength","auditConditionedStories","auditHeatingEquipmentType","auditHeatingFuel");
$CustomerIDWithAuditInfo = array();
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
	foreach ($auditFields as $auditField){
		if ($record->{$auditField}){
			$CustomerIDAuditInfo[$record->id][$auditField] = $record->{$auditField};
		}
	}
}
$auditStatus = $CustomerByID[$SelectedCustomerID]->auditStatus;
//print_pre($CustomerByID[$SelectedCustomerID]);
foreach ($CustomerIDAuditInfo as $CustomerID=>$auditField){
	if ($auditField){
		$CustomerIDWithAuditInfo[] = $CustomerID;
	}
}
//get rebate status name
$rebateStatusNameResults = $muniProvider->getRebateStatusNames();
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
}

//get rebate info for current client
$criteria = new stdClass();
$criteria->customerId = $SelectedCustomerID;
$auditInforResults = $muniProvider->getAuditInfo($criteria);
$rawFormData = json_decode($auditInforResults->MuniAudit_RawFormData);
$auditId = $auditInforResults->MuniAudit_ID;
//print_pre($rawFormData);
//echo $rebateName;
echo ($SelectedCustomerID && !$actualAudit && $auditStatus? "Direct link to this page: ".$CurrentServer."cetstaff/?linkPage=Muni&CustomerID=".$SelectedCustomerID : "");
$CustomerDetails = $CustomerByID[$SelectedCustomerID];

$defaultFoundationFloorRvalue = 4;
$defaultSillJoistSize = 12;
$defaultFoundationWallRvalue = 2;

$AuditDetailsForNEAT["ClientInfo_ClientID"] = $SelectedCustomerID."_".substr($CustomerDetails->accountHolderFirstName,0,1).$CustomerDetails->accountHolderLastName.str_replace(" ","",$CustomerDetails->installedAddress);
$AuditDetailsForNEAT["ClientInfo_AltClientID"] = $CustomerDetails->utilityAccountNumber;
$AuditDetailsForNEAT["ClientInfo_Address"] = $CustomerDetails->installedAddress;
$AuditDetailsForNEAT["ClientInfo_City"] = $CustomerDetails->installedCity;
$AuditDetailsForNEAT["ClientInfo_State"] = $CustomerDetails->installedState;
$AuditDetailsForNEAT["ClientInfo_Zip"] = $CustomerDetails->installedZip."-3108";
$AuditDetailsForNEAT["ClientInfo_DwellingType"] = "Site Built";
$AuditDetailsForNEAT["ClientInfo_DwellingOwnership"] = "Owned";
$AuditDetailsForNEAT["ClientInfo_DwellingPrimaryHeatingFuel"] = $rawFormData->heatingFuel;

$AuditDetailsForNEAT["Contacts_FullName"] = $CustomerDetails->accountHolderFullName;
$AuditDetailsForNEAT["Contacts_NameDetailsFirst"] = $CustomerDetails->accountHolderFirstName;
$AuditDetailsForNEAT["Contacts_NameDetailsLast"] = $CustomerDetails->accountHolderLastName;
$AuditDetailsForNEAT["Contacts_Address"] = ($CustomerDetails->mailingAddress ? $CustomerDetails->mailingAddress : $CustomerDetails->installedAddress);
$AuditDetailsForNEAT["Contacts_City"] = $CustomerDetails->mailingCity;
$AuditDetailsForNEAT["Contacts_State"] = $CustomerDetails->mailingState;
$AuditDetailsForNEAT["Contacts_Zip"] = $CustomerDetails->mailingZip;
$AuditDetailsForNEAT["Contacts_Workphone"] = $CustomerDetails->installedPhone;
$AuditDetailsForNEAT["Contacts_Email"] = (extract_emails_from($CustomerDetails->installedEmail) ? $CustomerDetails->installedEmail : NULL);

$AuditDetailsForNEAT["AuditInfo_AuditName"] = $rawFormData->auditAuditCode;
$AuditDetailsForNEAT["AuditInfo_Auditor"] = $rawFormData->auditorAuditor;
$AuditDetailsForNEAT["AuditInfo_WeatherFile"] = $rawFormData->auditWeatherFile;
$AuditDetailsForNEAT["AuditInfo_ConditionedStories"] = $CustomerDetails->auditConditionedStories;
$AuditDetailsForNEAT["AuditInfo_FloorArea"] = $CustomerDetails->auditFloorArea;

$AuditDetailsForNEAT["Shell_WallCode"] = $rawFormData->wallCode;
$AuditDetailsForNEAT["Shell_WallType"] = $rawFormData->wallType;
$AuditDetailsForNEAT["Shell_StudeSize"] = $rawFormData->wallStudSize;
$AuditDetailsForNEAT["Shell_ExteriorType"] = $rawFormData->wallExteriorType;
$AuditDetailsForNEAT["Shell_ExposedTo"] = $rawFormData->wallExposedTo;
$AuditDetailsForNEAT["Shell_Orientation"] = $rawFormData->wallOrientation;

$intakeConditionedStories = $CustomerIDAuditInfo[$SelectedCustomerID]["auditConditionedStories"];
$intakeHouseWidth = $CustomerIDAuditInfo[$SelectedCustomerID]["auditHouseWidth"];
$intakeHouseLength = $CustomerIDAuditInfo[$SelectedCustomerID]["auditHouseLength"];

$AuditDetailsForNEAT["Shell_GrossArea"] = ($rawFormData->wallGrossAreaToBeInsulated ? $rawFormData->wallGrossAreaToBeInsulated : (($intakeConditionedStories*16*$intakeHouseWidth)+($intakeConditionedStories*16*$intakeHouseLenght)));
$AuditDetailsForNEAT["Shell_MeasureNumber"] = 1; //always need Wall info
$AuditDetailsForNEAT["Shell_ExistingInsulationType"] = $rawFormData->existingInsulation;
$AuditDetailsForNEAT["Shell_ExistingInsulationRValue"] = ($rawFormData->existingInsulation == "None" ? 0 : ($rawFormData->addedInsulation == "None" && !$rawFormData->existingInsulationRValue ? 12 : $rawFormData->existingInsulationRValue));
$AuditDetailsForNEAT["Shell_AddedInsulationType"] = $rawFormData->addedInsulation;
//$AuditDetailsForNEAT["Shell_AddedInsulationRValue"] = $rawFormData->addedInsulationRvalue;

$AuditDetailsForNEAT["UnfinishedAttics_AtticCode"] = $rawFormData->atticUnfinishedCode;
$AuditDetailsForNEAT["UnfinishedAttics_AtticType"] = $rawFormData->atticUnfinishedType;
$AuditDetailsForNEAT["UnfinishedAttics_JoistSpacing"] = $rawFormData->atticUnfinishedJoistSpacing;
$AuditDetailsForNEAT["UnfinishedAttics_Area"] = $rawFormData->atticUnfinishedAreaSqft;
$AuditDetailsForNEAT["UnfinishedAttics_RoofColor"] = $rawFormData->atticUnfinishedRoofColor;
$AuditDetailsForNEAT["UnfinishedAttics_ExistingInsulationType"] = $rawFormData->atticUnfinishedExistingInsulation;
$AuditDetailsForNEAT["UnfinishedAttics_ExistingInsulationDepth"] = ($rawFormData->atticUnfinishedExistingInsulationDepth ? $rawFormData->atticUnfinishedExistingInsulationDepth : 0);
$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationMeasureNumber"] = $rawFormData->atticUnfinishedAddedInsulationMeasureNumber;
$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationType"] = $rawFormData->atticUnfinishedAddedInsulation;
//$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationRValue"] = $rawFormData->atticUnfinishedAddedInsulationRValue;
$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationDepth"] = ($rawFormData->atticUnfinishedAddedInsulationDepth + $rawFormData->atticUnfinishedExistingInsulationDepth);

$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationMeasureNumber"] = $rawFormData->atticFinishedKneewallAddedInsulationMeasureNumber;
$AuditDetailsForNEAT["FinishedAttics_KneeWallAtticCode"] = $rawFormData->atticFinishedKneewallCode;
$AuditDetailsForNEAT["FinishedAttics_KneeWallAtticAreaType"] = $rawFormData->atticFinishedKneewallType;
$AuditDetailsForNEAT["FinishedAttics_KneeWallAtticArea"] = $rawFormData->atticFinishedKneewallAreaSqft;
$AuditDetailsForNEAT["FinishedAttics_KneeWallExistingInsulationType"] = $rawFormData->atticFinishedKneewallExistingInsulation;
$AuditDetailsForNEAT["FinishedAttics_KneeWallExistingInsulationDepth"] = ($rawFormData->atticFinishedKneewallExistingInsulationDepth ? $rawFormData->atticFinishedKneewallExistingInsulationDepth : 0);
$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationType"] = $rawFormData->atticFinishedKneewallAddedInsulation;
//$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationRValue"] = $rawFormData->atticFinishedKneewallAddedInsulationRValue;
$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationDepth"] = ($rawFormData->atticFinishedKneewallAddedInsulationDepth + $rawFormData->atticFinishedKneewallExistingInsulationDepth);

$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationMeasureNumber"] = $rawFormData->atticFinishedSlopeAddedInsulationMeasureNumber;
$AuditDetailsForNEAT["FinishedAttics_SlopeAtticCode"] = $rawFormData->atticFinishedSlopeCode;
$AuditDetailsForNEAT["FinishedAttics_SlopeAtticAreaType"] = $rawFormData->atticFinishedSlopeType;
$AuditDetailsForNEAT["FinishedAttics_SlopeAtticArea"] = $rawFormData->atticFinishedSlopeAreaSqft;
$AuditDetailsForNEAT["FinishedAttics_SlopeExistingInsulationType"] = $rawFormData->atticFinishedSlopeExistingInsulation;
$AuditDetailsForNEAT["FinishedAttics_SlopeExistingInsulationDepth"] = ($rawFormData->atticFinishedSlopeExistingInsulationDepth ? $rawFormData->atticFinishedSlopeExistingInsulationDepth : 0);
$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationType"] = $rawFormData->atticFinishedSlopeAddedInsulation;
//$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationRValue"] = $rawFormData->atticFinishedSlopeAddedInsulationRValue;
$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationDepth"] = ($rawFormData->atticFinishedSlopeAddedInsulationDepth + $rawFormData->atticFinishedSlopeExistingInsulationDepth);

$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationMeasureNumber"] = ($rawFormData->atticFinishedCapAddedInsulationMeasureNumber ? ($rawFormData->atticFinishedSlopeAddedInsulationMeasureNumber ? 2 : 1) : "");
$AuditDetailsForNEAT["FinishedAttics_CapAtticCode"] = $rawFormData->atticFinishedCapCode;
$AuditDetailsForNEAT["FinishedAttics_CapAtticAreaType"] = $rawFormData->atticFinishedCapType;
$AuditDetailsForNEAT["FinishedAttics_CapAtticArea"] = $rawFormData->atticFinishedCapAreaSqft;
$AuditDetailsForNEAT["FinishedAttics_CapExistingInsulationType"] = $rawFormData->atticFinishedCapExistingInsulation;
$AuditDetailsForNEAT["FinishedAttics_CapExistingInsulationDepth"] = ($rawFormData->atticFinishedCapExistingInsulationDepth ? $rawFormData->atticFinishedCapExistingInsulationDepth : 0);
$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationType"] = $rawFormData->atticFinishedCapAddedInsulation;
//$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationRValue"] = $rawFormData->atticFinishedCapAddedInsulationRValue;
$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationDepth"] = ($rawFormData->atticFinishedCapAddedInsulationDepth + $rawFormData->atticFinishedCapExistingInsulationDepth);

$AuditDetailsForNEAT["Foundations_FoundationCode"] = $rawFormData->foundationCode;
$AuditDetailsForNEAT["Foundations_FoundationType"] = $rawFormData->foundationType;
$AuditDetailsForNEAT["Foundations_MeasureNumber"] = $rawFormData->foundationMeasureNumber;
$AuditDetailsForNEAT["Foundations_FloorArea"] = ($rawFormData->foundationFloorArea ? $rawFormData->foundationFloorArea : $CustomerDetails->auditFloorArea);
$AuditDetailsForNEAT["Foundations_FloorExistingRValue"] = ($rawFormData->foundationFloorExistingInsulationRValue ? $rawFormData->foundationFloorExistingInsulationRValue : 4);
$AuditDetailsForNEAT["Foundations_FloorAddedInsulationType"] = $rawFormData->foundationFloorAddedInsulationAddedType;
$AuditDetailsForNEAT["Foundations_SillJoistSize"] = (trim($rawFormData->foundationSillJoistSize) ? $rawFormData->foundationSillJoistSize : $defaultSillJoistSize);
$AuditDetailsForNEAT["Foundations_SillPerimeterToInsulate"] = $rawFormData->foundationSillPerimeterToInsulate;
$AuditDetailsForNEAT["Foundations_SillAddedInsulationType"] = $rawFormData->foundationSillAddedInsulationAddedType;
$AuditDetailsForNEAT["Foundations_FoundationWallHeight"] = $rawFormData->foundationWallHeight;
$AuditDetailsForNEAT["Foundations_FoundationWallHeightExposed"] = $rawFormData->foundationWallHeightExposed;
$AuditDetailsForNEAT["Foundations_FoundationWallPerimeter"] = $rawFormData->foundationWallPerimeter;
$AuditDetailsForNEAT["Foundations_FoundationWallExistingInsulationRValue"] = (trim($rawFormData->foundationWallExistingInsulationRValue) ? $rawFormData->foundationWallExistingInsulationRValue : $defaultFoundationWallRvalue);
$AuditDetailsForNEAT["Foundations_FoundationWallAddedInsulation"] = $rawFormData->foundationWallAddedInsulationAddedType;

$AuditDetailsForNEAT["Heating_SystemCode"] = $rawFormData->heatingCode;
$AuditDetailsForNEAT["Heating_EquipmentType"] = $rawFormData->heatingEquipmentType;
$AuditDetailsForNEAT["Heating_Fuel"] = $rawFormData->heatingFuel;
$AuditDetailsForNEAT["Heating_Location"] = $rawFormData->heatingLocation;
$AuditDetailsForNEAT["Heating_HeatSupplied"] = $rawFormData->heatingSuppliedPercentage;
$AuditDetailsForNEAT["Heating_PrimarySystem"] = $rawFormData->heatingPrimarySystem;
$AuditDetailsForNEAT["Heating_InputUnits"] = $rawFormData->heatingInputUnits;
$AuditDetailsForNEAT["Heating_OutputCapacity"] = $rawFormData->heatingOutputCapacity;
$AuditDetailsForNEAT["Heating_SteadyStateEfficiency"] = $rawFormData->heatingSteadyStateEfficiency;
$AuditDetailsForNEAT["Heating_Condition"] = $rawFormData->heatingCondition;
$AuditDetailsForNEAT["Heating_ReplacementOptions"] = $rawFormData->heatingReplacementOptions;
$AuditDetailsForNEAT["Heating_ReplacementFuel"] = $rawFormData->heatingReplacementFuel;
$AuditDetailsForNEAT["Heating_ReplacementIncludeInSIR"] = $rawFormData->heatingReplacementIncludeInSIR;
$AuditDetailsForNEAT["Heating_ReplacementSystemAFUE"] = $rawFormData->heatingReplacementSystemAFUE;
$AuditDetailsForNEAT["Heating_ReplacementLaborCost"] = $rawFormData->heatingReplacementLaborCost;
$AuditDetailsForNEAT["Heating_ReplacementMaterialCost"] = $rawFormData->heatingReplacementMaterialCost;

$AuditDetailsForNEAT["Ducts_AirLeakageRateBefore"] = $rawFormData->ductAirLeakageRateBefore;
$AuditDetailsForNEAT["Ducts_AirPressureBefore"] = $rawFormData->ductAirPressureBefore;
$AuditDetailsForNEAT["Ducts_AirLeakageRateAfter"] = $rawFormData->ductAirLeakageRateAfter;
$AuditDetailsForNEAT["Ducts_AirPressureAfter"] = $rawFormData->ductAirPressureAfter;
$AuditDetailsForNEAT["Ducts_InfiltrationCosts"] = ($rawFormData->ductAirSealingCosts ? $rawFormData->ductAirSealingCosts : 0);

$AuditDetailsForNEAT["BoilerPlateItems"] = $rawFormData->boilerPlateItems;

$AuditDetailsForNEAT["Photos_Path"] = $auditInforResults->SupportingFile;
$AuditDetailsForNEAT["Notes"] = $rawFormData->notes;

?>
<!--
<?php print_pre($AuditDetailsForNEAT);?>
<table border=1><tr><td valign="top"><?php print_pre($CustomerByID[$SelectedCustomerID]);?></td><td valign="top"><?php print_pre($rawFormData);?></td></tr></table>
-->
<hr>
<?php if (!$SelectedCustomerID && !$newCustomerName){?>
	<div class="fifteen columns">
		<?php 
			$comboBoxRemoveIfInvalid = " ";
			include('_CustomerComboBox.php');
		?>
	</div>
<?php }else{?>
	<!--
	Current Status: <select>
	<?php
	if (!$currentRebateStatus){echo "<option value=\"1\">New</option>";}
	foreach ($RebateStatusNameByID as $val=>$text){
			echo "<option value=\"".$val."\"".($currentRebateStatus == $val ? " selected" : "").">".$text."</option>";
	}
	?>
		</select>
	-->
	<?php
	include_once('_auditForm.php');
	?>
<?php }?>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		dateClass.datepicker({
			maxDate:0
		});
		$(".details-title").css('cursor','pointer');
		
		var closedDetailsClass = $(".closedDetails"),
			missingDetails = $(".missingDetails");
		closedDetailsClass.hide();
		missingDetails.show();
		$('.asDisplay').on('click', 'span.details-title', function () {
			var detailsSpan = $(this).next('span');
			detailsSpan.click();
		} );
		$('.asDisplay').on('click', 'span.details-open', function () {
			var div = $(this).parent('div');
			var details = $(this).next('div');

			if ( details.is(":visible") ) {
				details.hide();
				div.removeClass('opened');
			}
			else {
				details.show();
				div.addClass('opened');
			}
		} );
		$("#addNewCustomerButton").on('click',function(e){
			e.preventDefault();
			window.location = "?nav=<?php echo $_GET['nav'];?>&NewCustomerName="+$("#customerNotFound").val();

		});
		$("#addNewPhoneCustomerButton").on('click',function(e){
			e.preventDefault();
			window.location = "?nav=<?php echo $_GET['nav'];?>&NewPhoneCustomer=true";

		});
		$("#utilityAccountNumberSource").on('keyup',function(){
			$("#utilityAccountNumberHidden").val($(this).val());
		});

		var formElementRebate = $("#onlineRebateForm");
		 
		formElementRebate.validate({
			rules: {
				utilityAccountNumber: {
					required: true
				}
			}
		});
		
		$("#saveHidden").click(function(e){
			$('#onlineRebateResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElementRebate.valid()){
				//console.log(JSON.stringify(formElementRebate.serializeObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(formElementRebate.serializeObject()),
					success: function(data){
						//console.log(data)
						$('#onlineRebateResults').html("Saved").addClass("alert").addClass("info");
					},
					error: function(jqXHR, textStatus, errorThrown){
						//console.log(jqXHR);
						var message = $.parseJSON(jqXHR.responseText);
						$('#onlineRebateResults').html(message).addClass("alert");
					}
				});
			}
		});
	});
</script>