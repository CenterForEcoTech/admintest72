<?php if (count($_SESSION['PhoneLog'])){?>
			<br>
			<fieldset id="closeRecentPhoneFieldSet">
				<legend>Most Recent Phone Log Customer Information<span id="closeRecentPhone" title="Remove Recent Phone Information" class="ui-icon ui-icon-circle-close" style="cursor:pointer;top:2px;position:relative;float:right;"></span></legend>
				<b><?php echo $_SESSION['PhoneLog']->firstName." ".$_SESSION['PhoneLog']->lastName." ".($_SESSION['PhoneLog']->accountNumber ? "Account#" .$_SESSION['PhoneLog']->accountNumber : "").($_SESSION['PhoneLog']->phoneNumber ? " Phone:".$_SESSION['PhoneLog']->phoneNumber : "");?></b>
				<a href="#" class="button-link" id="addNewPhoneCustomerButton" style="display:block;">Add As New Customer</a>
			</fieldset>
				<script type="text/javascript" language="javascript" class="init">
					$(document).ready(function() {
						$("#closeRecentPhone").on('click',function(){
							var data = JSON.stringify({'action':'phoneCallSession_remove'});
							$.ajax({
								url: "ApiCustomerData.php",
								type: "PUT",
								data: data,
								success: function(data){
									$("#closeRecentPhoneFieldSet").hide();
								}
							});
							
						});
					});
				</script>
		<?php }?>
