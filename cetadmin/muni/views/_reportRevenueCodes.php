<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("WGE RevenueCodes".$InvoiceDate)
							 ->setSubject("WGE RevenueCodes".$InvoiceDate)
							 ->setDescription("WGE RevenueCodes");
							 

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/muni/";
	$revenueCodepath = "/home/cetdash/cetdashboard.info/admintest/cetadmin/gbs/salesforce/reports/";
}else{
	$pathSeparator = "\\";
	$path = $path.$pathSeparator;
	$revenueCodepath = "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\gbs\\salesforce\\reports\\";
}

$invoiceFile = "revenueCodesWGEInvoice_".$invoiceMonth."_".date("s").".xlsx";

$pathExtension = "exports".$pathSeparator.$invoiceFile;
$webpathExtension = "exports".$webpathSeparator.$invoiceFile;
$saveLocation = $path.$pathExtension;

$CETInternalFiscalRevenueSheetIndex = 0;	
$CETInternalFiscalRevenueSheetName = "WG&E";
//Create CET Internal Fiscal Revenue Sheet
$GLCode = " – 1";
include($revenueCodepath.'CETInternalRevenueCodeSheet.php');
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}

//echo $saveLocation;
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webinvoiceLink = $CurrentServer.$adminFolder."muni/".$webpathExtension;
echo "<a id='WGEInvoiceFile' href='".$webinvoiceLink."'>".$invoiceFile."</a><br>";
?>