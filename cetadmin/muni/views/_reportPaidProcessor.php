<?php
set_time_limit(0);
ini_set('memory_limit','14096M');
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$rebateTypes = array("Heat Hot water"=>"rebateHeatHotwater","Efficiency"=>"rebateEfficiency","Cool Home"=>"rebateCoolHome","Pool Pump"=>"rebatePoolPump","Appliance"=>"rebateAppliance","Commercial Heat"=>"rebateCommercialHeat","WiFi"=>"rebateWiFi");
$descriptionFields = array(
	"HeatHotwater"=>array(
		"MuniResRebateHeatHotwater_Measure_Furnace95",
		"MuniResRebateHeatHotwater_Measure_Furnace97",
		"MuniResRebateHeatHotwater_Measure_Boiler85",
		"MuniResRebateHeatHotwater_Measure_Boiler90",
		"MuniResRebateHeatHotwater_Measure_Boiler95",
		"MuniResRebateHeatHotwater_Measure_Integrated90",
		"MuniResRebateHeatHotwater_Measure_Integrated95",
		"MuniResRebateHeatHotwater_Measure_IndirectWater",
		"MuniResRebateHeatHotwater_Measure_H2OStorage62",
		"MuniResRebateHeatHotwater_Measure_H2OStorage67",
		"MuniResRebateHeatHotwater_Measure_Tankless82",
		"MuniResRebateHeatHotwater_Measure_Tankless94"
	),
	"Efficiency"=>array(
		"MuniResRebateEfficiency_AirSealing",
		"MuniResRebateEfficiency_Insulation",
		"MuniResRebateEfficiency_Heating"
	),
	"CoolHome"=>array(
		"MuniResRebateCoolHome_Measure_CentralAC16",
		"MuniResRebateCoolHome_Measure_CentralAC18",
		"MuniResRebateCoolHome_Measure_Ductless18",
		"MuniResRebateCoolHome_Measure_Ductless20"
	),
	"PoolPump"=>array("MuniResRebatePoolPump_New"),
	"Appliance"=>array("MuniResRebateAppliance_Appliance"),
	"CommercialHeat"=>array(
		"MuniComRebateHeatHotwater_Measure_Furnace95",
		"MuniComRebateHeatHotwater_Measure_Furnace97",
		"MuniComRebateHeatHotwater_Measure_Boiler85",
		"MuniComRebateHeatHotwater_Measure_Boiler90",
		"MuniComRebateHeatHotwater_Measure_Boiler95",
		"MuniResRebateHeatHotwater_Measure_Integrated90",
		"MuniResRebateHeatHotwater_Measure_Integrated95",
		"MuniComRebateHeatHotwater_Measure_IndirectWater",
		"MuniComRebateHeatHotwater_Measure_H2OStorage62",
		"MuniComRebateHeatHotwater_Measure_H2OStorage67",
		"MuniComRebateHeatHotwater_Measure_Tankless82",
		"MuniComRebateHeatHotwater_Measure_Tankless94"
	),
	"WiFi"=>array("MuniResRebateWiFi_Measure_WiFi")
);
$descriptionHotWaterKeys = array("Furnace"=>"Furnace","Boiler"=>"Boiler","Integrated"=>"Integrated Water Heater and Boiler","IndirectWater"=>"Indirect Water Heater","H2OStorage"=>"Water Heating Storage","Tankless"=>"Tankless Water Heater");
$descriptionCoolHomeKeys = array("CentralAC"=>"Central AC","Ductless"=>"Ductless Mini-Split");
$descriptionEfficiencyKeys = array("Blower Door Test & Air Sealing / 50% up to $300"=>"AirSealingApprovedAmount","Insulation / 50% up to $300"=>"InsulationApprovedAmount","Heating System / 50% up to $300"=>"HeatingApprovedAmount");
$descriptionWiFiKeys = array("WiFi"=>"WiFi");

$reportRebateStatus = 7; //7 is dispersed

$muniProvider = new MuniProvider($dataConn);

$criteria = new stdClass();
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
}

//get audit history
$auditStatus = 10;//10 = report sent
$criteria = new stdClass();
$criteria->currentStatus = $auditStatus; 
$audits = $muniProvider->getAuditInfo($criteria);
foreach ($audits as $auditRecord){
	//print_pre($CustomerByID[$auditRecord->MuniAudit_CustomerID]);
	$auditInfo = new stdClass();
	$auditInfo->auditId = $auditRecord->MuniAudit_ID;
	$auditInfo->customerId = $auditRecord->MuniAudit_CustomerID;
	$auditInfo->firstName = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->accountHolderFirstName;
	$auditInfo->lastName = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->accountHolderLastName;
	$auditInfo->auditDate = MySQLDate($CustomerByID[$auditRecord->MuniAudit_CustomerID]->auditScheduledDate);
	$auditInfo->address = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->installedAddress;
	$auditInfo->city = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->installedCity;
	$auditInfo->state = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->installedState;
	$auditInfo->zip = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->installedZip;
	$auditInfo->customerFullName = $CustomerByID[$auditRecord->MuniAudit_CustomerID]->accountHolderFullName;
	$invoicedByParts = ($auditRecord->InvoicedBy ? explode("|",$auditRecord->InvoicedBy) : array());
	$auditInfo->invoicedDate = (MySQLDate($invoicedByParts[0]) ? MySQLDate($invoicedByParts[0]) : null);
	$auditDateByCustomerID[$auditRecord->MuniAudit_CustomerID] = date("m/d/Y", strtotime($auditRecord->MuniAudit_AddedTimeStamp));
	$auditRecordsUnsorted[strtotime($CustomerByID[$auditRecord->MuniAudit_CustomerID]->auditScheduledDate)][] = $auditInfo;
//	$auditRecords[] = $auditInfo;
}
//print_pre($auditRecordsUnsorted);
krsort($auditRecordsUnsorted);
foreach ($auditRecordsUnsorted as $timestamp=>$timeStampData){
	foreach ($timeStampData as $auditStuff){
		$auditRecords[] = $auditStuff;
	}
}
//print_pre($auditRecords);


//getRebateCodes
$rebateCodeResults = $muniProvider->getRebateCodes();
$rebateCodeCollection = $rebateCodeResults->collection;
foreach ($rebateCodeCollection as $result=>$record){
	$rebateCodeByID[$record->MuniRebateCode_ID] = $record;
	$rebateCodeByFieldName[trim($record->MuniRebateCode_FieldName)] = $record;
}
//print_pre($rebateCodeByFieldName);

$criteria = new stdClass();
$criteria->rebateDispersedToFiscal = true;
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
	foreach ($rebateTypes as $arrayName=>$fieldName){
		$fieldName = str_replace("rebate","",$fieldName);
		$invoicedRebates[$fieldName] = array();
		//echo $fieldName;

		//get list of all rebates for this type for this client
		$criteria = new stdClass();
		$criteria->rebateName=str_replace("rebate","",$fieldName);
		$criteria->customerId = $record->id;
		$criteria->currentStatus = $reportRebateStatus; //7 is dispersed to fiscal
		$rebateInfoResults = $muniProvider->getRebateInfo($criteria);
		$rebateInfoCollection = $rebateInfoResults->collection;
		//print_pre($rebateInfoCollection);
		//echo $rebateName;
		$ThisRebateFieldHeader = ($fieldName == "CommercialHeat" ? "MuniComRebateHeatHotwater" : "MuniResRebate".str_replace(" ","",str_replace("Water","water",$fieldName)));
		echo $ThisRebateFieldHeader;
		$description = $descriptionFields[$fieldName];
		foreach ($rebateInfoCollection as $rebateResult=>$rebateRecord){
			//if ($record->id == 15){print_pre($rebateInfoCollection);}
			if ($rebateRecord->CurrentStatus == $reportRebateStatus){
				//print_pre($rebateRecord);
				//get description fields
				$descriptionText = array();
				foreach ($description as $tableFieldName){
					$dispersedObj = new stdClass();
					$customerId=$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"};
					$dispersedObj->customerId = $customerId;
					$dispersedObj->account = $CustomerByID[$customerId]->utilityAccountNumber;
					$dispersedObj->firstName = $CustomerByID[$customerId]->accountHolderFirstName;
					$dispersedObj->lastName = $CustomerByID[$customerId]->accountHolderLastName;
					$dispersedObj->customerFullName = $CustomerByID[$customerId]->accountHolderFullName;
					$dispersedObj->address = ($CustomerByID[$customerId]->mailingAddress ? $CustomerByID[$customerId]->mailingAddress : $CustomerByID[$customerId]->installedAddress);
					$dispersedObj->city = ($CustomerByID[$customerId]->mailingCity ? $CustomerByID[$customerId]->mailingCity : $CustomerByID[$customerId]->installedCity);
					$dispersedObj->state = ($CustomerByID[$customerId]->mailingState ? $CustomerByID[$customerId]->mailingState : $CustomerByID[$customerId]->installedState);
					$dispersedObj->zip = ($CustomerByID[$customerId]->mailingZip ? $CustomerByID[$customerId]->mailingZip : $CustomerByID[$customerId]->installedZip);
					$dispersedObj->auditDate = $auditDateByCustomerID[$customerId];					
					$invoicedByParts = ($rebateRecord->InvoicedBy ? explode("|",$rebateRecord->InvoicedBy) : array());
					$verifiedByParts = ($rebateRecord->VerifiedBy ? explode("|",$rebateRecord->VerifiedBy) : array());
					$dispersedObj->invoicedDate = (MySQLDate($invoicedByParts[0]) ? MySQLDate($invoicedByParts[0]) : null);
					$dispersedObj->verifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
					if ($fieldName == "HeatHotwater" || $fieldName == "CommercialHeat" || $fieldName == "WiFi"){
						$approvedFieldName = $tableFieldName."Approved";
						$rebateCode = $rebateCodeByFieldName[$approvedFieldName];
						$isMeasureApproved =  $rebateRecord->{$approvedFieldName};
						//echo $tableFieldName. "Approved=".$isMeasureApproved."<br>";
						if ($isMeasureApproved == "Yes"){
							//print_pre($rebateCodeByFieldName[$approvedFieldName]);
							$dispersedObj->rebateType = $fieldName;
							$dispersedObj->rebateId = $rebateRecord->id;
							$dispersedObj->clientId = $customerId;
							$dispersedObj->qty = ($rebateRecord->{$tableFieldName."QuantityInstalled"} > 0 ? $rebateRecord->{$tableFieldName."QuantityInstalled"} : 1);
							if ($fieldName == "WiFi"){
								$dispersedObj->rebateAmount = $rebateRecord->{$tableFieldName."TotalAmount"};
								$dispersedObj->totalAmount = $rebateRecord->{$tableFieldName."TotalAmount"};
							}else{
								$dispersedObj->rebateAmount = $rebateRecord->{$tableFieldName."RebateAmount"};
								$dispersedObj->totalAmount = $rebateRecord->{$tableFieldName."RebateAmount"};
							}
							$dispersedObj->dispersedDate = MySQLDate($rebateRecord->DispersedToFiscalDate);
							$dispersedObj->receivedDate = date("m/d/Y", strtotime($rebateRecord->receivedTimeStamp));
							$dispersedObj->rebatePayableTo = $rebateRecord->{$ThisRebateFieldHeader."_RebateMadePayableTo"};
							$dispersedObj->manufacturer = $rebateRecord->{$tableFieldName."Manufacturer"};
							$dispersedObj->model = $rebateRecord->{$tableFieldName."ModelNumber"};
							$dispersedObj->efficiency = $rebateRecord->{$tableFieldName."EfficiencyRating"};
							$dispersedObj->inputsize = $rebateRecord->{$tableFieldName."InputSize"};
							$dispersedObj->rebateCodeFieldName = $approvedFieldName;

							$savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$lifetime = ((int)$rebateCode->MuniRebateCode_Lifetime > 0 ? (int)$rebateCode->MuniRebateCode_Lifetime : "");
							$lifetimeSavings = ((float)$rebateCode->MuniRebateCode_LifetimeSavings > 0 ? (float)$rebateCode->MuniRebateCode_LifetimeSavings : "");
							$peakDemandSavingsPerYear = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear : "");
							$peakDemandLifetimeSavings = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime : "");
							if($rebateCode->MuniRebateCode_SavingsType == "kWh"){
								$kWhSavings = $savingsPerYear;
								$kWhLifetimeSavings = $lifetimeSavings;
								$mmbtuSavingsPerYear = "";
								$MCFSavings = "";
								$MCFLifetimeSavings = "";
							}else{
								$kWhSavings = "";
								$kWhLifetimeSavings = "";
								$mmbtuSavingsPerYear = $savingsPerYear;
								$MCFSavings = ((float)$rebateCode->MuniRebateCode_MCFSavings > 0 ? (float)$rebateCode->MuniRebateCode_MCFSavings : "");
								$MCFLifetimeSavings = $lifetimeSavings;
							}
							$dispersedObj->partCode = ($rebateCode->MuniRebateCode_PartCode ? $rebateCode->MuniRebateCode_PartCode : $approvedFieldName);
							$dispersedObj->description = $rebateCode->MuniRebateCode_Description;
							$dispersedObj->savingsType = $rebateCode->MuniRebateCode_SavingsType;
							$dispersedObj->savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$dispersedObj->lifetime = $lifetime;
							$dispersedObj->lifetimeSavings = $lifetimeSavings;
							$dispersedObj->mmbtuSavings = $mmbtuSavingsPerYear;
							$dispersedObj->mcfSavings = $MCFSavings;
							$dispersedObj->mcfLifetimeSavings = $MCFLifetimeSavings;
							$dispersedObj->kWhSavings = $kWhSavings;
							$dispersedObj->kWhLifetimeSavings = $kWhLifetimeSavings;
							$dispersedObj->peakDemandSavingsPerYear = $peakDemandSavingsPerYear;
							$dispersedObj->peakDemandLifetimeSavings = $peakDemandLifetimeSavings;
							
							$dispersedObj->rebateCode = $rebateCodeByFieldName[$approvedFieldName];
							$dispersedObj->rebateDetails = $rebateRecord;
							$dispersedObj->equipmentRebatePartCode = $dispersedObj->partCode;
							$dispersedObj->equipmentRebateQty = $dispersedObj->qty;
							$dispersedObj->equipmentRebateAmount = $dispersedObj->totalAmount;
							$dispersedObj->equipmentVerifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
							$descriptionText[] = $dispersedObj;
						}
						$implodekey = ", ";
					}elseif ($fieldName == "CoolHome"){
						$approvedFieldName = $tableFieldName."Approved";
						$rebateCode = $rebateCodeByFieldName[$approvedFieldName];
						$isMeasureApproved =  $rebateRecord->{$approvedFieldName};
						//echo $tableFieldName. "Approved=".$isMeasureApproved."<br>";
						if ($isMeasureApproved == "Yes"){
							//print_pre($rebateCodeByFieldName[$approvedFieldName]);
							$dispersedObj->rebateType = $fieldName;
							$dispersedObj->rebateId = $rebateRecord->id;
							$dispersedObj->clientId = $customerId;
							$dispersedObj->qty = $rebateRecord->{$tableFieldName."QtyInstalled"};
							$dispersedObj->rebateAmount = $rebateRecord->{$tableFieldName."Rebate"};
							$dispersedObj->totalAmount = $rebateRecord->{$tableFieldName."TotalRebate"};
							$dispersedObj->dispersedDate = MySQLDate($rebateRecord->DispersedToFiscalDate);
							$dispersedObj->receivedDate = date("m/d/Y", strtotime($rebateRecord->receivedTimeStamp));
							$dispersedObj->rebatePayableTo = $rebateRecord->{$ThisRebateFieldHeader."_RebateMadePayableTo"};
							$dispersedObj->manufacturer = $rebateRecord->{$tableFieldName."Manufacturer"};
							$dispersedObj->condenserModel = $rebateRecord->{$tableFieldName."CondenserModelNumber"};
							$dispersedObj->coilModel = $rebateRecord->{$tableFieldName."CoilModelNumber"};
							$dispersedObj->seer = $rebateRecord->{$tableFieldName."SEER"};
							$dispersedObj->eer = $rebateRecord->{$tableFieldName."EER"};
							$dispersedObj->hspf = $rebateRecord->{$tableFieldName."HSPF"};
							$dispersedObj->unitsize = $rebateRecord->{$tableFieldName."SizeTons"};
							
							$savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$lifetime = ((int)$rebateCode->MuniRebateCode_Lifetime > 0 ? (int)$rebateCode->MuniRebateCode_Lifetime : "");
							$lifetimeSavings = ((float)$rebateCode->MuniRebateCode_LifetimeSavings > 0 ? (float)$rebateCode->MuniRebateCode_LifetimeSavings : "");
							$peakDemandSavingsPerYear = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear : "");
							$peakDemandLifetimeSavings = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime : "");
							if($rebateCode->MuniRebateCode_SavingsType == "kWh"){
								$kWhSavings = $savingsPerYear;
								$kWhLifetimeSavings = $lifetimeSavings;
								$mmbtuSavingsPerYear = "";
								$MCFSavings = "";
								$MCFLifetimeSavings = "";
							}else{
								$kWhSavings = "";
								$kWhLifetimeSavings = "";
								$mmbtuSavingsPerYear = $savingsPerYear;
								$MCFSavings = ((float)$rebateCode->MuniRebateCode_MCFSavings > 0 ? (float)$rebateCode->MuniRebateCode_MCFSavings : "");
								$MCFLifetimeSavings = $lifetimeSavings;
							}
							$dispersedObj->partCode = ($rebateCode->MuniRebateCode_PartCode ? $rebateCode->MuniRebateCode_PartCode : $approvedFieldName);
							$dispersedObj->description = $rebateCode->MuniRebateCode_Description;
							$dispersedObj->savingsType = $rebateCode->MuniRebateCode_SavingsType;
							$dispersedObj->savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$dispersedObj->lifetime = $lifetime;
							$dispersedObj->lifetimeSavings = $lifetimeSavings;
							$dispersedObj->mmbtuSavings = $mmbtuSavingsPerYear;
							$dispersedObj->mcfSavings = $MCFSavings;
							$dispersedObj->mcfLifetimeSavings = $MCFLifetimeSavings;
							$dispersedObj->kWhSavings = $kWhSavings;
							$dispersedObj->kWhLifetimeSavings = $kWhLifetimeSavings;
							$dispersedObj->peakDemandSavingsPerYear = $peakDemandSavingsPerYear;
							$dispersedObj->peakDemandLifetimeSavings = $peakDemandLifetimeSavings;
							
							$dispersedObj->rebateCode = $rebateCodeByFieldName[$approvedFieldName];
							$dispersedObj->rebateDetails = $rebateRecord;
							$dispersedObj->equipmentRebatePartCode = $dispersedObj->partCode;
							$dispersedObj->equipmentRebateQty = $dispersedObj->qty;
							$dispersedObj->equipmentRebateAmount = $dispersedObj->totalAmount;
							$dispersedObj->equipmentVerifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
							$descriptionText[] = $dispersedObj;
						}
					}elseif ($fieldName == "Efficiency"){
						$approvedFieldName = $tableFieldName."ApprovedAmount";
						$rebateCode = $rebateCodeByFieldName[$approvedFieldName];
						$isMeasureApproved =  $rebateRecord->{$approvedFieldName};
						if ($isMeasureApproved > 0){
							//print_pre($rebateCodeByFieldName[$approvedFieldName]);
							$dispersedObj->rebateType = $fieldName;
							$dispersedObj->rebateId = $rebateRecord->id;
							$dispersedObj->clientId = $customerId;
							$dispersedObj->qty = 1;
							$dispersedObj->rebateAmount = $rebateRecord->{$tableFieldName."ApprovedAmount"};
							$dispersedObj->totalAmount = $rebateRecord->{$tableFieldName."ApprovedAmount"};
							$dispersedObj->dispersedDate = MySQLDate($rebateRecord->DispersedToFiscalDate);
							$dispersedObj->receivedDate = date("m/d/Y", strtotime($rebateRecord->receivedTimeStamp));
							$dispersedObj->rebatePayableTo = $rebateRecord->{$ThisRebateFieldHeader."_RebateMadePayableTo"};
					
							$savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$lifetime = ((int)$rebateCode->MuniRebateCode_Lifetime > 0 ? (int)$rebateCode->MuniRebateCode_Lifetime : "");
							$lifetimeSavings = ((float)$rebateCode->MuniRebateCode_LifetimeSavings > 0 ? (float)$rebateCode->MuniRebateCode_LifetimeSavings : "");
							$peakDemandSavingsPerYear = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear : "");
							$peakDemandLifetimeSavings = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime : "");
							if($rebateCode->MuniRebateCode_SavingsType == "kWh"){
								$kWhSavings = $savingsPerYear;
								$kWhLifetimeSavings = $lifetimeSavings;
								$mmbtuSavingsPerYear = "";
								$MCFSavings = "";
								$MCFLifetimeSavings = "";
							}else{
								$kWhSavings = "";
								$kWhLifetimeSavings = "";
								$mmbtuSavingsPerYear = $savingsPerYear;
								$MCFSavings = ((float)$rebateCode->MuniRebateCode_MCFSavings > 0 ? (float)$rebateCode->MuniRebateCode_MCFSavings : "");
								$MCFLifetimeSavings = $lifetimeSavings;
							}
							$dispersedObj->partCode = ($rebateCode->MuniRebateCode_PartCode ? $rebateCode->MuniRebateCode_PartCode : $approvedFieldName);
							$dispersedObj->description = $rebateCode->MuniRebateCode_Description;
							$dispersedObj->savingsType = $rebateCode->MuniRebateCode_SavingsType;
							$dispersedObj->savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$dispersedObj->lifetime = $lifetime;
							$dispersedObj->lifetimeSavings = $lifetimeSavings;
							$dispersedObj->mmbtuSavings = $mmbtuSavingsPerYear;
							$dispersedObj->mcfSavings = $MCFSavings;
							$dispersedObj->mcfLifetimeSavings = $MCFLifetimeSavings;
							$dispersedObj->kWhSavings = $kWhSavings;
							$dispersedObj->kWhLifetimeSavings = $kWhLifetimeSavings;
							$dispersedObj->peakDemandSavingsPerYear = $peakDemandSavingsPerYear;
							$dispersedObj->peakDemandLifetimeSavings = $peakDemandLifetimeSavings;

							$dispersedObj->rebateCode = $rebateCodeByFieldName[$approvedFieldName];
							$dispersedObj->rebateDetails = $rebateRecord;
							$dispersedObj->weatherizationRebatePartCode = $dispersedObj->partCode;
							$dispersedObj->weatherizationRebateQty = 1;
							$dispersedObj->weatherizationRebateAmount = $rebateRecord->{$tableFieldName."ApprovedAmount"};
							$dispersedObj->weatherizationVerifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
							$dispersedObj->equipmentRebatePartCode = null;
							$dispersedObj->equipmentRebateQty = null;
							$dispersedObj->equipmentRebateAmount = null;
							$dispersedObj->equipmentVerifiedDate = null;
							$descriptionText[] = $dispersedObj;
						}
					}elseif ($fieldName == "Appliance"){
						$approvedFieldName = $tableFieldName."Type";
						$isMeasureApproved =  $rebateRecord->{$approvedFieldName};
						$approvedFieldName = $tableFieldName.strtolower(str_replace(" ","",str_replace("/","",$isMeasureApproved)));
						$rebateCode = $rebateCodeByFieldName[$approvedFieldName];
						if ($isMeasureApproved){
							//print_pre($rebateCodeByFieldName[$approvedFieldName]);
							$dispersedObj->rebateType = $fieldName;
							$dispersedObj->rebateId = $rebateRecord->id;
							$dispersedObj->clientId = $customerId;
							$dispersedObj->qty = 1;
							$dispersedObj->rebateAmount = $rebateRecord->RebateAmountApproved;
							$dispersedObj->totalAmount = $rebateRecord->RebateAmountApproved;
							$dispersedObj->dispersedDate = MySQLDate($rebateRecord->DispersedToFiscalDate);
							$dispersedObj->receivedDate = date("m/d/Y", strtotime($rebateRecord->receivedTimeStamp));
							$dispersedObj->rebatePayableTo = $rebateRecord->{$ThisRebateFieldHeader."_RebateMadePayableTo"};
							$dispersedObj->manufacturer = $rebateRecord->{$tableFieldName."Brand"};
							$dispersedObj->model = $rebateRecord->{$tableFieldName."ModelNumber"};
							
							$savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$lifetime = ((int)$rebateCode->MuniRebateCode_Lifetime > 0 ? (int)$rebateCode->MuniRebateCode_Lifetime : "");
							$lifetimeSavings = ((float)$rebateCode->MuniRebateCode_LifetimeSavings > 0 ? (float)$rebateCode->MuniRebateCode_LifetimeSavings : "");
							$peakDemandSavingsPerYear = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear : "");
							$peakDemandLifetimeSavings = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime : "");
							if($rebateCode->MuniRebateCode_SavingsType == "kWh"){
								$kWhSavings = $savingsPerYear;
								$kWhLifetimeSavings = $lifetimeSavings;
								$mmbtuSavingsPerYear = "";
								$MCFSavings = "";
								$MCFLifetimeSavings = "";
							}else{
								$kWhSavings = "";
								$kWhLifetimeSavings = "";
								$mmbtuSavingsPerYear = $savingsPerYear;
								$MCFSavings = ((float)$rebateCode->MuniRebateCode_MCFSavings > 0 ? (float)$rebateCode->MuniRebateCode_MCFSavings : "");
								$MCFLifetimeSavings = $lifetimeSavings;
							}
							$dispersedObj->partCode = ($rebateCode->MuniRebateCode_PartCode ? $rebateCode->MuniRebateCode_PartCode : $approvedFieldName);
							$dispersedObj->description = $rebateCode->MuniRebateCode_Description;
							$dispersedObj->savingsType = $rebateCode->MuniRebateCode_SavingsType;
							$dispersedObj->savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$dispersedObj->lifetime = $lifetime;
							$dispersedObj->lifetimeSavings = $lifetimeSavings;
							$dispersedObj->mmbtuSavings = $mmbtuSavingsPerYear;
							$dispersedObj->mcfSavings = $MCFSavings;
							$dispersedObj->mcfLifetimeSavings = $MCFLifetimeSavings;
							$dispersedObj->kWhSavings = $kWhSavings;
							$dispersedObj->kWhLifetimeSavings = $kWhLifetimeSavings;
							$dispersedObj->peakDemandSavingsPerYear = $peakDemandSavingsPerYear;
							$dispersedObj->peakDemandLifetimeSavings = $peakDemandLifetimeSavings;

							$dispersedObj->rebateCode = $rebateCodeByFieldName[$approvedFieldName];
							$dispersedObj->rebateDetails = $rebateRecord;
							$dispersedObj->equipmentRebatePartCode = $dispersedObj->partCode;
							$dispersedObj->equipmentRebateQty = $dispersedObj->qty;
							$dispersedObj->equipmentRebateAmount = $dispersedObj->totalAmount;
							$dispersedObj->equipmentVerifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
							$descriptionText[] = $dispersedObj;
						}
					}elseif ($fieldName == "PoolPump"){
						$approvedFieldName = $tableFieldName."Manufacturer";
						$rebateCode = $rebateCodeByFieldName[$approvedFieldName];
						$isMeasureApproved =  $rebateRecord->{$approvedFieldName};
						if ($isMeasureApproved){
							//print_pre($rebateCodeByFieldName[$approvedFieldName]);
							$dispersedObj->rebateType = $fieldName;
							$dispersedObj->rebateId = $rebateRecord->id;
							$dispersedObj->clientId = $customerId;
							$dispersedObj->qty = 1;
							$dispersedObj->rebateAmount = $rebateRecord->RebateAmountApproved;
							$dispersedObj->totalAmount = $rebateRecord->RebateAmountApproved;
							$dispersedObj->dispersedDate = MySQLDate($rebateRecord->DispersedToFiscalDate);
							$dispersedObj->receivedDate = date("m/d/Y", strtotime($rebateRecord->receivedTimeStamp));
							$dispersedObj->rebatePayableTo = $rebateRecord->{$ThisRebateFieldHeader."_RebateMadePayableTo"};
							$dispersedObj->manufacturer = $rebateRecord->{$tableFieldName."Manufacturer"};
							$dispersedObj->model = $rebateRecord->{$tableFieldName."ModelNumber"};
							
							$savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$lifetime = ((int)$rebateCode->MuniRebateCode_Lifetime > 0 ? (int)$rebateCode->MuniRebateCode_Lifetime : "");
							$lifetimeSavings = ((float)$rebateCode->MuniRebateCode_LifetimeSavings > 0 ? (float)$rebateCode->MuniRebateCode_LifetimeSavings : "");
							$peakDemandSavingsPerYear = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsPerYear : "");
							$peakDemandLifetimeSavings = ((float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime > 0 ? (float)$rebateCode->MuniRebateCode_PeakDemandSavingsLifetime : "");
							if($rebateCode->MuniRebateCode_SavingsType == "kWh"){
								$kWhSavings = $savingsPerYear;
								$kWhLifetimeSavings = $lifetimeSavings;
								$mmbtuSavingsPerYear = "";
								$MCFSavings = "";
								$MCFLifetimeSavings = "";
							}else{
								$kWhSavings = "";
								$kWhLifetimeSavings = "";
								$mmbtuSavingsPerYear = $savingsPerYear;
								$MCFSavings = ((float)$rebateCode->MuniRebateCode_MCFSavings > 0 ? (float)$rebateCode->MuniRebateCode_MCFSavings : "");
								$MCFLifetimeSavings = $lifetimeSavings;
							}
							$dispersedObj->partCode = ($rebateCode->MuniRebateCode_PartCode ? $rebateCode->MuniRebateCode_PartCode : $approvedFieldName);
							$dispersedObj->description = $rebateCode->MuniRebateCode_Description;
							$dispersedObj->savingsType = $rebateCode->MuniRebateCode_SavingsType;
							$dispersedObj->savingsPerYear = ((float)$rebateCode->MuniRebateCode_SavingsPerYear > 0 ? (float)$rebateCode->MuniRebateCode_SavingsPerYear : "");
							$dispersedObj->lifetime = $lifetime;
							$dispersedObj->lifetimeSavings = $lifetimeSavings;
							$dispersedObj->mmbtuSavings = $mmbtuSavingsPerYear;
							$dispersedObj->mcfSavings = $MCFSavings;
							$dispersedObj->mcfLifetimeSavings = $MCFLifetimeSavings;
							$dispersedObj->kWhSavings = $kWhSavings;
							$dispersedObj->kWhLifetimeSavings = $kWhLifetimeSavings;
							$dispersedObj->peakDemandSavingsPerYear = $peakDemandSavingsPerYear;
							$dispersedObj->peakDemandLifetimeSavings = $peakDemandLifetimeSavings;
							
							$dispersedObj->rebateCode = $rebateCodeByFieldName[$approvedFieldName];
							$dispersedObj->rebateDetails = $rebateRecord;
							$dispersedObj->equipmentRebatePartCode = $dispersedObj->partCode;
							$dispersedObj->equipmentRebateQty = $dispersedObj->qty;
							$dispersedObj->equipmentRebateAmount = $dispersedObj->totalAmount;
							$dispersedObj->equipmentVerifiedDate = (MySQLDate($verifiedByParts[0]) ? MySQLDate($verifiedByParts[0]) : null);
							$descriptionText[] = $dispersedObj;
						}
					}
				}
				if (count($descriptionText)){
					foreach ($descriptionText as $dispersedObject){
						$rebates[strtotime($rebateRecord->DispersedToFiscalDate)][] = $dispersedObject;
					}
				}
			}
		}
	}
}
$rebateStatusNameResults = $muniProvider->getRebateStatusNames($criteria=null);
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
}
$auditStatusNameResults = $muniProvider->getAuditStatusNames();
$auditStatusNameCollection = $auditStatusNameResults->collection;
foreach ($auditStatusNameCollection as $statusResult=>$statusRecord){
	$AuditStatusNameByID[$statusRecord->id] = $statusRecord->name;
}

//phoneLog
$criteria = new stdClass();
$phoneLogResults = $muniProvider->getPhoneLog($criteria);
$phoneLogCollection = $phoneLogResults->collection;

krsort($rebates);
//print_pre($rebates);
foreach ($rebates as $dispersedDate=>$dispersedDateData){
	foreach ($dispersedDateData as $rebateObj){
		$yearMonth = strtotime(date("Y-m-01",strtotime($rebateObj->dispersedDate)));
		$year = strtotime(date("Y-01-01",strtotime($rebateObj->dispersedDate)));
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["qty"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["qty"] + $rebateObj->qty;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["amount"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["amount"] + $rebateObj->totalAmount;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["kWhSavingsTotal"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["kWhSavingsTotal"] + $rebateObj->kWhSavings;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["lifetimekWhSavingsTotal"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["lifetimekWhSavingsTotal"] + $rebateObj->kWhLifetimeSavings;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["mmBtuSavingsTotal"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["mmBtuSavingsTotal"] + $rebateObj->mmbtuSavings;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["mcfSavingsTotal"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["mcfSavingsTotal"] + $rebateObj->mcfSavings;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["lifetimeMCFSavingsTotal"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["lifetimeMCFSavingsTotal"] + $rebateObj->mcfLifetimeSavings;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandSavingsPerYear"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandSavingsPerYear"] + $rebateObj->peakDemandSavingsPerYear;
		$monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandLifetimeSavings"] = $monthlyRecords[$yearMonth][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandLifetimeSavings"] + $rebateObj->peakDemandLifetimeSavings;
		
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["qty"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["qty"] + $rebateObj->qty;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["amount"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["amount"] + $rebateObj->totalAmount;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["kWhSavingsTotal"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["kWhSavingsTotal"] + $rebateObj->kWhSavings;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["lifetimekWhSavingsTotal"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["lifetimekWhSavingsTotal"] + $rebateObj->kWhLifetimeSavings;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["mmBtuSavingsTotal"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["mmBtuSavingsTotal"] + $rebateObj->mmbtuSavings;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["mcfSavingsTotal"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["mcfSavingsTotal"] + $rebateObj->mcfSavings;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["lifetimeMCFSavingsTotal"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["lifetimeMCFSavingsTotal"] + $rebateObj->mcfLifetimeSavings;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandSavingsPerYear"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandSavingsPerYear"] + $rebateObj->peakDemandSavingsPerYear;
		$yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["peakDemandLifetimeSavings"] = $yearlyRecords[$year][$rebateObj->rebateType][$rebateObj->partCode]["lifetimeMCFSavingsTotal"] + $rebateObj->peakDemandLifetimeSavings;
	}
}
krsort($monthlyRecords);
krsort($yearlyRecords);
?>