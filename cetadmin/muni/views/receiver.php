Online Forms are automatically downloaded every 10 minutes.  Use this page to force a download of online forms.
<hr>
<?php
include_once($dbProviderFolder."MuniProvider.php");
include_once($siteRoot."_setupDataConnection.php");
$muniProvider = new MuniProvider($dataConn);
include_once('models/sprypointFields.php');
$path = getcwd();
if (strpos($path,"xampp")){
	$testingServer = true;
}
		$url = "https://wgeserv.sprypoint.com/cet/getdata" ;
		$curlHeaders = array('X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
		if(!curl_exec($ch)){
			echo curl_error($ch) . '" - Code: ' . curl_errno($ch);
			echo "<br>Using hard coded sample set<br>";
			$contents = '[{"_id":{"$oid":"561fec3893783ecd8780bc1b"},"origin":"10.113.200.123","fields":{"Store_State":["TEST - DNP"],"Address":["TEST - DNP"],"Store_City":["TEST - DNP"],"Rebate_Zip":[""],"Residential_Electric_Utility_Account_Number":["TEST - DNP"],"Store_Address":["TEST - DNP"],"Serial_Number":["TEST - DNP"],"Rebate_City":[""],"Purchase_Date":["10/15/2015"],"Purchase_Price":["TEST - DNP"],"employee_initials":[""],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"Work_Phone":[""],"City":["TEST - DNP"],"Your_Name":["TEST - DNP"],"Home_Phone":["TEST - DNP"],"Store_Name":["TEST - DNP"],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Energy_Star_Appliance":[""],"Model_Number":["TEST - DNP"],"Rebate_Address":["TEST - DNP"],"State":["TEST - DNP"],"Brand":["TEST - DNP"],"Store_Zip":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Home_Appliance_Rebate_Form","isRead":false,"created":"2015-10-15T18:11.04.312Z"},{"_id":{"$oid":"561fec1493783ecd8780bc19"},"origin":"10.113.200.123","fields":{"New_Pump_Manufacturer":["TEST - DNP"],"Replaced_Pump_Horsepower":[""],"Replaced_Pump_Manufacturer":[""],"Address":["TEST - DNP"],"Residential_Municipal_Electric_Utility_Account_Number":["TEST - DNP"],"New_Pump_Model_Number":["TEST - DNP"],"New_Pump_Horsepower":["TEST - DNP"],"Rebate_Zip":[""],"Contractor_Zip":["TEST - DNP"],"Rebate_City":[""],"Controller_Manufacturer":["TEST - DNP"],"Purchase_Date":["10/15/2015"],"Type_of_New_Pool_Pump":["Two-speed"],"Purchase_Price":["99"],"Contractor_Company_Name":["TEST - DNP"],"Replaced_Pump_Model_Number":[""],"employee_initials":[""],"Installation_Date":["10/15/2015"],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"City":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Your_Name":["TEST - DNP"],"Home_Phone":["TEST - DNP"],"Type_of_Replaced_Pool_Pump":[""],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Model_Number":["TEST - DNP"],"Rebate_Address":[""],"Contact_Person":["TEST - DNP"],"State":["TEST - DNP"],"Contractor_State":["TEST - DNP"],"Contractor_Phone":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_High_Efficiency_Pool_Pump_Rebate_Application","isRead":false,"created":"2015-10-15T18:10.28.463Z"},{"_id":{"$oid":"561febe393783ecd8780bc17"},"origin":"10.113.200.123","fields":{"Install_Cost":["99"],"Business_Phone":["TEST - DNP"],"Contractor_Email_Address":["kstrang@sprypoint.com"],"Company_Name":["TEST - DNP"],"Address":["TEST - DNP"],"TXV_or_EXV_Installed":["TXV"],"AHRI_Rated_EER":[""],"Customer_Name":["TEST - DNP"],"Rebate_Zip":[""],"Electric_Account_Number":["TEST - DNP"],"Contractor_Zip":["TEST - DNP"],"Mini_Split":["Yes"],"Rebate_City":[""],"Install_Date":["10/15/2015"],"Rebate_Name":[""],"Is_technician_NATE_Certified":["Yes"],"Manufacturer":["99"],"employee_initials":[""],"New_Unit_Size":[""],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"AHRI_Rated_SEER":[""],"City":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Rebate_Amount":["99"],"New_Equipment_Installed":[""],"Home_Phone":["TEST - DNP"],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Coil_Model_Number":[""],"Rebate_Address":[""],"Contact_Person":["TEST - DNP"],"State":["TEST - DNP"],"HSPF":[""],"Telephone_Number":["TEST - DNP"],"Contractor_State":["TEST - DNP"],"AHRI_Ref_Number":["99"],"Condenser_Model_Number":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Cool_Homes_Rebate_Application","isRead":false,"created":"2015-10-15T18:09.39.069Z"},{"_id":{"$oid":"561feba593783ecd8780bc15"},"origin":"10.71.199.148","fields":{"Anticipated_Rebate_tankless_water_heater":[""],"Input_Size_furnace":[""],"Anticipated_Rebate_boiler":[""],"Anticipated_Rebate_water_heater":[""],"Rebate_Amount_furnace":[""],"Landlord_Owner_Name":[""],"Fax_Number":[""],"Efficiency_Rating_boiler":[""],"License_Number":["TEST - DNP"],"Landlord_Owner_Phone_Number":[""],"Installed_Cost":["TEST - DNP"],"Efficiency_Rating_furnace":[""],"Quantity_Installed_water_heater":[""],"Quantity_Installed_boiler":[""],"Property_Owned_or_Leased":["Owned"],"Anticipated_Rebate_storage_water_heater":[""],"Total_Anticipated_Rebate":["TEST - DNP"],"Rebate_Amount_boiler":[""],"Quantity_Installed_furnace":[""],"Model_Number_water_heater":[""],"Contractor_Zip":["TEST - DNP"],"Landlord_Owner_Address":[""],"Anticipated_Rebate_furnace":[""],"Model_Number_furnace":[""],"Model_Number_storage_water_heater":[""],"Installed_Zip":["TEST - DNP"],"Landlord_Owner_Zip":[""],"Manufacturer_boiler":[""],"Landlord_Owner_City":[""],"Quantity_Installed_storage_water_heater":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Installation_Date":["10/15/2015"],"Quantity_Installed_tankless_water_heater":[""],"Installed_City":["TEST - DNP"],"Gas_Account_Number":["TEST - DNP"],"Account_Holder_Name":["TEST - DNP"],"Contractor_Eml":["kstrang@sprypoint.com"],"Contractor_Address":["TEST - DNP"],"Model_Number_tankless_water_heater":[""],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Tax_ID_Number":[""],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Customer_initials":["TEST - DNP"],"Manufacturer_storage_water_heater":[""],"Input_Size_storage_water_heater":[""],"Rebate_Amount_water_heater":[""],"Installed_Address":["TEST - DNP"],"Input_Size_water_heater":[""],"Manufacturer_water_heater":[""],"Phone_Number":["TEST - DNP"],"Rebate_Amount_tankless_water_heater":[""],"Manufacturer_furnace":[""],"Rebate_Amount_storage_water_heater":[""],"Manufacturer_tankless_water_heater":[""],"Efficiency_Rating_water_heater":[""],"Input_Size_tankless_water_heater":[""],"Model_Number_boiler":[""],"Contractor_State":["TEST - DNP"],"Landlord_Owner_State":[""],"Landlord_Owner_Eml":[""],"Efficiency_Rating_storage_water_heater":[""],"Landlord_Owner_Fax_Number":[""],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Input_Size_boiler":[""],"Efficiency_Rating_tankless_water_heater":[""],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Residential_High_Efficiency_Heating_and_Water_Heating_Rebate_Program","isRead":false,"created":"2015-10-15T18:08.37.938Z"},{"_id":{"$oid":"561feb6e93783ecd8780bc13"},"origin":"10.71.199.148","fields":{"Owner_Landlord_Fax_Number":[""],"Fax_Number":["TEST - DNP"],"License_Number":["TEST - DNP"],"Mailing_Zip":[""],"Contractor_Zip":["TEST - DNP"],"Provide_Description_of_Measure":["TEST - DNP"],"Mailing_State":[""],"Mailing_Address":[""],"Name_of_Municipal_Utility":["TEST - DNP"],"Owner_Landlord_Eml":[""],"Installed_Zip":["TEST - DNP"],"Owner_Landlord_State":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Customer_Initials":["TEST - DNP"],"Installed_City":["TEST - DNP"],"Mailing_City":[""],"Account_Holder_First_Name":["TEST - DNP"],"Contractor_Eml":["kstrang@sprypoint.com"],"Contractor_Address":["TEST - DNP"],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Tax_ID_Number":[""],"Date":["10/15/2015"],"Owner_Landlord_Name":[""],"Contractor_City":["TEST - DNP"],"Owner_Landlord_City":[""],"Installed_Address":["TEST - DNP"],"Phone_Number":["TEST - DNP"],"Owner_Landlord_Address":[""],"Owner_Landlord_Zip":[""],"Contractor_State":["TEST - DNP"],"Account_Holder_Last_Name":["TEST - DNP"],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Owner_Landlord_Phone_Number":[""],"Utility_Account_Number":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Home_Efficiency_Incentive_Program","isRead":false,"created":"2015-10-15T18:07.42.824Z"},{"_id":{"$oid":"561feb3a93783ecd8780bc11"},"origin":"10.71.199.148","fields":{"Facility_Name":["TEST - DNP"],"Approximate_Age_of_Facility":["99"],"Brief_Overview_of_Space_Use_and_Processes":["TEST - DNP"],"Input_Size_furnace":[""],"Facility_State":["TEST - DNP"],"Anticipated_Rebate_boiler":[""],"Anticipated_Rebate_Storage_Water_Heater":[""],"Model_Number_Water_Heater":[""],"Facility_Zip":["TEST - DNP"],"Installed_Address_Facility":["TEST - DNP"],"Quantity_Installed_Storage_Water_Heater":[""],"Rebate_Amount_furnace":[""],"Fax_Number":["TEST - DNP"],"Efficiency_Rating_boiler":[""],"License_Number":["TEST - DNP"],"Model_Number_Tankless_Water_Heater":[""],"Installed_Cost":["99"],"Efficiency_Rating_furnace":[""],"Quantity_Installed_boiler":[""],"Facility_City":["TEST - DNP"],"Property_Owned_or_Leased":["TEST - DNP"],"Anticipated_Rebate_Water_Heater":[""],"Total_Anticipated_Rebate":["99"],"Rebate_Amount_boiler":[""],"Quantity_Installed_furnace":[""],"Main_Building_Square_Footage":["99"],"Contractor_Zip":["TEST - DNP"],"Efficiency_Rating_Tankless_Water_Heater":[""],"Facility_Email":[""],"Anticipated_Rebate_furnace":[""],"Model_Number_furnace":[""],"Mailing_Address":[""],"Rebate_Amount_Storage_Water_Heater":[""],"Installed_Zip":["TEST - DNP"],"Manufacturer_Storage_Water_Heater":[""],"Manufacturer_boiler":[""],"Rebate_Amount_Tankless_Water_Heater":[""],"Anticipated_Rebate_Tankless_Water_Heater":[""],"Manufacturer_Water_Heater":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Installation_Date":["10/15/2015"],"Customer_Initials":["TEST - DNP"],"Efficiency_Rating_Storage_Water_Heater":[""],"Installed_City":["TEST - DNP"],"Gas_Account_Number":["TEST - DNP"],"Model_Number_Storage_Water_Heater":[""],"Contractor_Email":["kstrang@sprypoint.com"],"City":[""],"Account_Holder_Name":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Rebate_Amount_Water_Heater":[""],"Input_Size_Tankless_Water_Heater":[""],"Quantity_Installed_Water_Heater":[""],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Zip":[""],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Facility_Type":["Commercial (Wholesale/Retail)"],"Facility_Contact":["TEST - DNP"],"Efficiency_Rating_Water_Heater":[""],"Installed_Address":["TEST - DNP"],"Manufacturer_Tankless_Water_Heater":[""],"Input_Size_Storage_Water_Heater":[""],"State":[""],"Phone_Number":["TEST - DNP"],"Quantity_Installed_Tankless_Water_Heater":[""],"Phone_Number_Facility":["TEST - DNP"],"Manufacturer_furnace":[""],"Model_Number_boiler":[""],"Other_Buildings_Square_Footage":["999"],"Contractor_State":["TEST - DNP"],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Input_Size_boiler":[""],"Input_Size_Water_Heater":[""],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Commercial_and_Industrial_High_Efficiency_Heating_and_Water","isRead":false,"created":"2015-10-15T18:06.50.401Z"}]';
		}else{		
			$contents = curl_exec($ch);
		}
		curl_close($ch);

		$jsonContents = json_decode($contents);

		
		$oid = '$oid'; //item in object uses $ 
		if (count($jsonContents)){
			foreach ($jsonContents as $id=>$formInfo){
				$thisOid = $formInfo->_id->{$oid};
				$thisFormType = strtolower($formInfo->formType);
				$thisIpAddress = $formInfo->origin;
				$thisFilesArray = $formInfo->files;
				$received["oid"][] = $thisOid;
				$theseFields = array();
				$theseNewFields = array();
				$fieldName = "";
				foreach ($formInfo->fields as $fieldName=>$fieldValues){
					$formFields[$thisFormType][]=$fieldName;
					foreach ($fieldValues as $fieldValue){
						$fieldValue = ($fieldValue ? $fieldValue : NULL);
						$theseFields[$fieldName] = trim($fieldValue);
						if (!$sprypointFields[$thisFormType][$fieldName]){echo "<hr>".$thisFormType," ".$fieldName."=".$fieldValue."<hr>";}
						$theseNewFields[$sprypointFields[$thisFormType][$fieldName]] = trim($fieldValue);
					}
				}
				ksort($theseFields);
				ksort($theseNewFields);
				$theseNewFields["formType"] = $sprypointFormTypes[$thisFormType];
				$theseNewFields["ipAddress"] = $thisIpAddress;
				$theseNewFields["oid"] = $thisOid;
				$theseNewFields["rawFormData"] = json_encode($theseFields)." files:".json_encode($thisFilesArray);
				$readyToBeProcessed[$thisOid]["formType"]=$sprypointFormTypes[$thisFormType];
				$readyToBeProcessed[$thisOid]["fields"]=$theseFields;
				$readyToBeProcessed[$thisOid]["CETfields"]=$theseNewFields;
				$readyToBeProcessed[$thisOid]["files"] = $thisFilesArray;
				$readyToBeProcessed[$thisOid]["formInfo"] = $formInfo;
				//print_pre($formInfo);
			}
	//		print_pre($formFields);
	//		print_pre($readyToBeProcessed);
	//		print_pre($jsonContents);

			foreach ($readyToBeProcessed as $oid=>$CETFields){

				$results = new stdClass();
				$newRecord = new stdClass();
				$formName = $CETFields["formType"];
				//echo "This OID was just received ".$oid."<Br>";
				$newRecord->formName = $formName;
				//print_pre($CETFields["CETfields"]);
				if (count($CETFields["CETfields"])){
					foreach ($CETFields["CETfields"] as $fieldName=>$fieldValue){
					//echo "hi";
						$newRecord->$fieldName = $fieldValue;
					}
				}else{
					echo $formName." for ".$oid." has no CETfields";
				}
				if ($formName == "CommercialHeat"){$newRecord->customerType = "commercial";}
				$newRecord->action = "rebate_add";
				include('_ApiCustomerData_PUTForm.php');
				if ($rebateInsertReponse->insertedId){
					$successfullInsert[] = array($formName=>$oid) ;
					if (count($CETFields["files"])){
						foreach ($CETFields["files"] as $fileId=>$fileInfo){
							$fileName = rawurlencode($fileInfo->name);
							$url = "https://wgeserv.sprypoint.com/cet/download-file/for-object/".$oid."/with-name/".$fileName ;
							$curlHeaders = array('X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f');
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
							curl_setopt($ch, CURLOPT_POST, 0);
							curl_setopt($ch, CURLOPT_HTTPGET, 1);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
							$data = curl_exec($ch);
							$error = curl_error($ch);
							$error_no = curl_errno($ch);
							curl_close($ch);
							if(!$data){
								echo $error . '" - Code: ' . $error_no;
								echo "<br>Attached files were not retrieved for oid=".$oid."<br>";
							}else{		
								$fileName = str_replace("%20","_",$fileName);
								$fileName = str_replace("%28","",str_replace("%29","",$fileName));

								$destinationFolder = $path."/rebateAttachments/";
								$destinationName = $formName."_".$oid."_".$fileName;
								$destination = $destinationFolder.$destinationName;
								$file = fopen($destination, "w+");
								fputs($file, $data);
								fclose($file);
								//now update the record to have the file information
								$updateFileResults = $muniProvider->updateFileStatus($formName,$destinationName,$oid);
							}
						}
					}
				}else{
					print_pre($rebateInsertReponse);
				}
			}
			
			foreach ($successfullInsert as $oidList){
				foreach ($oidList as $formName=>$oid){
					$fields = array('$oid' => $oid);
					$data_string = json_encode($fields);
				
				
					$user_agent = 'Mozilla/5.0 (Windows; U;Windows NT 5.1; ru; rv:1.8.0.9) Gecko/20061206 Firefox/1.5.0.9';
					$header = array(
						"X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f",
						"Content-Type: application/json",
						"Content-Length: ".strlen($data_string)
						);
				
					$url="https://wgeserv.sprypoint.com/cet/confirmread/".$oid;
					$curlHeaders = array('X-Auth-Token:214efdac-733b-11e5-8bcf-feff819cdc9f');
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_HEADER, 1);	

					if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
					if (!$testingServer){$response = curl_exec($ch);}
					$curlInfo = curl_getinfo($ch);
					$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
					//print_pre($curlInfo);
					//echo "<hr>";
					
					$curlResult['header'] = substr($response, 0, $header_size);
					$curlResult['body'] = substr( $response, $header_size );
					$curlResult['http_code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
					$curlResult['last_url'] = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);					
					//print_pre($curlResult);
					curl_close($ch);
					if (!$testingServer){					
						if ($curlResult['http_code'] == "200"){
							$updateOidResults = $muniProvider->updateOIDStatus($formName,$oid);
							echo "Successfully received and confirmed receipt of ".$formName." ".$oid."<br>";
						}else{
							echo "There was an error confirming receipt of ".$formName." ".$oid."<br>";
							echo $curlResult['header']."<hr>";
						}
					}else{
						echo "Testing Server will not set items to 'read' so production server can still retrieve item ".$oid."<br>";
					}
				}
				
			}
		}else{
			echo "No new forms received today<br>";
		}
?>
<!--
<button class="process" data-formtype="Appliance">Get Appliance Forms</button>
<button class="process" data-formtype="PoolPump">Get Pool Pump Forms</button>
<button class="process" data-formtype="CoolHome">Get Cool Home Forms</button>
<button class="process" data-formtype="HeatHotwater">Get Heat & Hot Water Forms</button>
<button class="process" data-formtype="Efficiency">Get Energy Efficiency Forms</button>
<button class="process" data-formtype="CommercialHeat">Get Commercial Heat Forms</button>
<br><hr>
<button id="sprypoint" data-formtype="SpypointFeed">Get All Forms From Spypoint</button>
<Br><Br>
Status of transfer of data
<div id="onlineRebateResults" class="alert info"></div>
<script>
$(function(){
	$(".process").click(function(){
		var $this = $(this),
			formType = $this.attr('data-formtype');
		var dataSrc = {};
		$('#onlineRebateResults').html("Requesting "+formType+" forms");
		dataSrc["formType"] = formType;
		$.ajax({
			url: "<?php echo $CurrentServer.$adminFolder;?>muni/ApiCustomerData.php",
			type: "GET",
			data: dataSrc,
			success: function(data){
				//console.log(data)
				if (data.success && data.formData){
					$('#onlineRebateResults').html(data.formName+" received...now about to process").removeClass("info").addClass("info");
					//console.log(data.formData);
					var thisFormData = data.formData;
					$.ajax({
						url: "<?php echo $CurrentServer.$adminFolder;?>muni/ApiCustomerData.php",
						type: "PUT",
						data: thisFormData,
						success: function(data){
							console.log(data)
							$('#onlineRebateResults').html(formType+" processed").removeClass("info").addClass("info");
						},
						error: function(jqXHR, textStatus, errorThrown){
							console.log(jqXHR);
							var message = $.parseJSON(jqXHR.responseText);
							$('#onlineRebateResults').html(message).removeClass("info");
						}
					});
					
				}else{
					$('#onlineRebateResults').html(data.formName+" unsuccessful...process halted").removeClass("info").addClass("info");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR);
				var message = $.parseJSON(jqXHR.responseText);
				$('#onlineRebateResults').html(message).removeClass("info");
			}
		});
	});
	
	$("#sprypoint").click(function(){
		var $this = $(this),
			formType = $this.attr('data-formtype');
		$('#onlineRebateResults').html("Requesting Spypoint Data");
		$.ajax({
			url: "https://wgeserv.sprypoint.com/cet/getdata",
			type: "GET",
			beforeSend: function(xhr){xhr.setRequestHeader('X-Auth-Token','214efdac-733b-11e5-8bcf-feff819cdc9f');},			
			success: function(data){
				console.log(data)
				/*
				if (data.success && data.formData){
					$('#onlineRebateResults').html(data.formName+" received...now about to process").removeClass("info").addClass("info");
					//console.log(data.formData);
					var thisFormData = data.formData;
					$.ajax({
						url: "<?php echo $CurrentServer.$adminFolder;?>muni/ApiCustomerData.php",
						type: "PUT",
						data: thisFormData,
						success: function(data){
							console.log(data)
							$('#onlineRebateResults').html(formType+" processed").removeClass("info").addClass("info");
						},
						error: function(jqXHR, textStatus, errorThrown){
							console.log(jqXHR);
							var message = $.parseJSON(jqXHR.responseText);
							$('#onlineRebateResults').html(message).removeClass("info");
						}
					});
					
				}else{
					$('#onlineRebateResults').html(data.formName+" unsuccessful...process halted").removeClass("info").addClass("info");
				}
				*/
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR);
				var message = $.parseJSON(jqXHR.responseText);
				$('#onlineRebateResults').html(message).removeClass("info");
			}
		});
	});
	
});
</script>
-->