<?php if ($SelectedCustomerID){
	include_once($siteRoot."_setupDataConnection.php");
	include_once($dbProviderFolder."MuniProvider.php");
	$currentStatusBorderColor = "white";
	$rebateAmountBorderColor = "white";
	$currentStatusProcessEndValues = array("Approved","Denied","Dispersed to Fiscal");
?>
	<style>
		.smallNote {font-size:8pt}
		.mediumNote {font-size:10pt}
		.currentStatus {padding-right:10px;margin:2px;border:1pt dashed <?php echo $currentStatusBorderColor;?>;}
		.rebateApprovedAmount {width:60px !important;height:17px !important;}
		.rebateApprovedAmountButton {height:17px !important;border:1pt solid blue;}
		.rebateAmount {padding-right:10px;margin:2px;border:1pt dashed <?php echo $rebateAmountBorderColor;?>;}
		.rebateAmountHidden {display:none;}
		.rebateDenied {padding-right:10px;margin:2px;border:1pt dashed <?php echo $rebateAmountBorderColor;?>;}
		.rebateDeniedHidden {display:none;}
		.currentStatusSelector {display:none;}
		.currentStatusSelectPulldown {font-size:10px;width:120px;height:15px;}
		.saveApprovedAmount {right:30%;display:none;}
		.editCurrentStatusHidden {display:none;}
	</style>
	<br clear="all">
	<h3><?php echo (trim($CustomerByID[$SelectedCustomerID]->accountHolderFullName) ? $CustomerByID[$SelectedCustomerID]->accountHolderFullName : "Customer");?> Rebate Progress<?php echo ($rebateTypesKey ? " for ".str_replace("Cool Home","Central AC HeatPump",$rebateTypesKey) ." Rebate".($rebateId ? "" : "s"): "");?></h3>
	<div class="section mediumNote" style="position:relative;float:left;">
		<?php
			$rebatesFound = false;
			foreach ($rebateTypes as $DisplayName=>$DatabaseName){
				$SelectedRebateType = ($_GET['RebateType'] ? $_GET['RebateType'] : $DisplayName);
				$criteria = new stdClass();
				$criteria->rebateName = $SelectedRebateType;
				$criteria->orderByDateReceived = true;
				$criteria->customerId = $SelectedCustomerID;
				$criteria->rebateId = ($rebateId ? $rebateId : null);
				$muniProvider = new MuniProvider($dataConn);
				$rebateDataResults = $muniProvider->getRebateInfo($criteria);
				$rebateCollection = $rebateDataResults->collection;
				if (count($rebateCollection)){
					foreach ($rebateCollection as $result=>$record){
						$allPartsApproved = true;
						$ApprovedParts = array();
						$ApprovedPartsTotal = 0;
						//check to see if all parts are approved for Hotwater rebates
						if ($SelectedRebateType == "Heat Hot water" || $SelectedRebateType == "Commercial Heat"){
							$tablePrefix = ($SelectedRebateType == "Heat Hot water" ? "MuniResRebateHeatHotwater_" : "MuniComRebateHeatHotwater_");
							$measureTypes = array("High Efficiency Furnace >95 AFUE"=>"Furnace95","High Efficiency Furnace >97 AFUE"=>"Furnace97","High Efficiency HydronicBoiler >85 AFUE"=>"Boiler85","High Efficiency HydronicBoiler >90 AFUE"=>"Boiler90","High Efficiency HydronicBoiler >95 AFUE"=>"Boiler95","Integrated Water Heater and Condensing Boiler >90 AFUE"=>"Integrated90","Integrated Water Heater and Condensing Boiler >95 AFUE"=>"Integrated95","High Efficiency Indirect Water Heater"=>"IndirectWater","Water Heating Storage >62 EF"=>"H2OStorage62","Water Heating Storage >67 EF"=>"H2OStorage67","Tankless Water Heater >82 EF"=>"Tankless82","Tankless Water Heater >94 EF"=>"Tankless94");
							$measureFields = array("Manufacturer","ModelNumber","RebateAmount","Approved");
							foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
								$rebateAmount = $record->{$tablePrefix."Measure_".$measureTypeInputName."RebateAmount"};
								$approved = $record->{$tablePrefix."Measure_".$measureTypeInputName."Approved"};
								$manufacturer = $record->{$tablePrefix."Measure_".$measureTypeInputName."Manufacturer"};
								$modelNumber = $record->{$tablePrefix."Measure_".$measureTypeInputName."ModelNumber"};
								if ($manufacturer){
									if ($rebateAmount > 0){
										if ($approved == "No"){
											$allPartsApproved = false;
										}else{
											if ($approved !="Denied"){
												$ApprovedParts[$measureTypeDisplay] = "$".money($rebateAmount)." ".$manufacturer.": ".$modelNumber;
												$ApprovedPartsTotal = $ApprovedPartsTotal+$rebateAmount;
											}
										}
										foreach ($measureFields as $measureFieldDisplay){
											$thisMeasureFieldName = $tablePrefix."Measure_".$measureTypeInputName.$measureFieldDisplay;
											$thisValue = $record->{$thisMeasureFieldName};
											$PartsApproved[$measureTypeDisplay][$measureFieldDisplay] = $thisValue;
										}
									}else{
										$allPartsApproved = false;
									}
								}
							}
						}
						if ($SelectedRebateType == "WiFi"){
							$tablePrefix = "MuniResRebateWiFi_";
							$measureTypes = array("WiFi"=>"WiFi");
							$measureFields = array("Manufacturer","ModelNumber","TotalAmount","Approved");
							foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
								$rebateAmount = $record->{$tablePrefix."Measure_".$measureTypeInputName."TotalAmount"};
								$approved = $record->{$tablePrefix."Measure_".$measureTypeInputName."Approved"};
								$manufacturer = $record->{$tablePrefix."Measure_".$measureTypeInputName."Manufacturer"};
								$modelNumber = $record->{$tablePrefix."Measure_".$measureTypeInputName."ModelNumber"};
								if ($manufacturer){
									if ($rebateAmount > 0){
										if ($approved == "No"){
											$allPartsApproved = false;
										}else{
											if ($approved !="Denied"){
												$ApprovedParts[$measureTypeDisplay] = "$".money($rebateAmount)." ".$manufacturer.": ".$modelNumber;
												$ApprovedPartsTotal = $ApprovedPartsTotal+$rebateAmount;
											}
										}
										foreach ($measureFields as $measureFieldDisplay){
											$thisMeasureFieldName = $tablePrefix."Measure_".$measureTypeInputName.$measureFieldDisplay;
											$thisValue = $record->{$thisMeasureFieldName};
											$PartsApproved[$measureTypeDisplay][$measureFieldDisplay] = $thisValue;
										}
									}else{
										$allPartsApproved = false;
									}
								}
							}
						}
		
						if ($SelectedRebateType == "Cool Home"){
							$tablePrefix = "MuniResRebateCoolHome_";
							$measureTypes = array("Central AC >16 SEER"=>"CentralAC16","Central AC >18 SEER"=>"CentralAC18","Ductless Mini-Split >18 SEER"=>"Ductless18","Ductless Mini-Split >20 SEER"=>"Ductless20");
							$measureFields = array("Manufacturer","CondenserModelNumber","TotalRebate","Approved");
							foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
								$rebateAmount = $record->{$tablePrefix."Measure_".$measureTypeInputName."TotalRebate"};
								$approved = $record->{$tablePrefix."Measure_".$measureTypeInputName."Approved"};
								$manufacturer = $record->{$tablePrefix."Measure_".$measureTypeInputName."Manufacturer"};
								$modelNumber = $record->{$tablePrefix."Measure_".$measureTypeInputName."CondenserModelNumber"};
								if ($manufacturer){
									if ($rebateAmount > 0){
										if ($approved == "No"){
											$allPartsApproved = false;
										}else{
											if ($approved !="Denied"){
												$ApprovedParts[$measureTypeDisplay] = "$".money($rebateAmount)." ".$manufacturer.": ".$modelNumber;
												$ApprovedPartsTotal = $ApprovedPartsTotal+$rebateAmount;
											}
										}
										foreach ($measureFields as $measureFieldDisplay){
											$thisMeasureFieldName = $tablePrefix."Measure_".$measureTypeInputName.$measureFieldDisplay;
											$thisValue = $record->{$thisMeasureFieldName};
											if ($measureFieldDisplay == "TotalRebate"){$measureFieldDisplay = "RebateAmount";}
											$PartsApproved[$measureTypeDisplay][$measureFieldDisplay] = $thisValue;
										}
									}else{
										$allPartsApproved = false;
									}
								}
							}
						}
						if ($SelectedRebateType == "Efficiency"){
							$tablePrefix = "MuniResRebateEfficiency_";
							$measureTypes = array("EV Charger Bill Credit"=>"EVInstallationApprovedAmount","Blower Door Test & Air Sealing / 50% up to $300"=>"AirSealingApprovedAmount","Insulation / 50% up to $300"=>"InsulationApprovedAmount","Heating System / 50% up to $300"=>"HeatingApprovedAmount");
							$EligibleMeasures = explode(",",$record->{$tablePrefix."EligibleMeasures"});							
							foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
								$rebateAmount = $record->{$tablePrefix.$measureTypeInputName};
								$approved = $record->{$tablePrefix.$measureTypeInputName};
								$measureName = explode(" / ",$measureTypeDisplay);
								if (in_array($measureTypeDisplay,$EligibleMeasures)){
									if ($rebateAmount > 0 || $measureTypeInputName == "EVInstallationApprovedAmount"){
										$ApprovedParts[$measureName[0]] = "$".money($rebateAmount)." ".$measureName[0];
										$ApprovedPartsTotal = $ApprovedPartsTotal+$rebateAmount;
										$PartsApproved[$measureName[0]][$measureFieldDisplay] = $thisValue;
										//echo "Measure type input name: ".$measureTypeInputName."<br>";
									}else{
										$allPartsApproved = false;
										$PartsApproved[$measureName[0]]["Approved"] = "No";
									}
								}
							}
						}
					//	$RebateByID[$record->id] = $record; 
						//check if file is actually downloaded:
						if ($record->supportingFile){
							$path = getcwd();
							$documentListCheck = explode(",",$record->supportingFile);
							$documentReviewLinks = array();
							foreach ($documentListCheck as $documentId=>$documentName){
								if (trim($documentName)){
									$documentPath = "rebateAttachments/".$documentName;
									$fileParts = explode("_",$documentName);
									$fileExists = file_exists($documentPath);
									if (!$fileExists){
										$documentPath = "rebateAttachments/AWS/".$documentName;
									}
									//echo $documentPath."=".$fileExists."<Br>";
									//$documentDownloadLink = (!$fileExists ? $CurrentServer.$adminFolder."muni/?nav=attachmentTool&formName=".$SelectedRebateType."&oid=".$record->oid."&fileName=".$fileParts[2] : "");
									$documentReviewLinks[] = "<a id=\"documentReview".$record->id."_".$documentId."\" href=\"".$CurrentServer.$adminFolder."muni/".$documentPath."\" target=\"".$documentName."\" title=\"Click for file ".$documentName."\">".(strpos($documentName,".pdf") ? "<img src=\"".$CurrentServer."images/file_pdf.png\" style=\"height:20;border:0pt solid black;position:relative;top:3px;\">" : "<img src=\"".$CurrentServer."images/file_attached.png\" style=\"height:20;border:0pt solid black;position:relative;top:3px;\">")."</a> ".$documentDownloadLink;
								}
							}

						}
						
						$statusLine = "<a target='rebate".$SelectedCustomerID.$record->id."' href='".$CurrentServer.$adminFolder."muni/?nav=rebate-".strtolower(str_replace(" ","",$DisplayName))."&name=".$DisplayName."&CustomerID=".$SelectedCustomerID."&RebateID=".$record->id."'>".str_replace("Cool Home","Central AC HeatPump",$DisplayName)."</a>";
						$documentReviewLink = (count($documentReviewLinks) ? "Supporting File".(count($documentReviewLinks)>1 ? 's' :'').":" : "").implode(" ",$documentReviewLinks);
						$addedBy = $record->addedBy;
						$addedBy = ($record->oid == "CET_OID" ? emailInitials($addedBy) : "Customer");
						$currentStatus = $RebateStatusNameByID[$record->CurrentStatus];
						$currentStatusSetByParts = explode("|",$record->CurrentStatusSetBy);
						$currentStatusSetDate = (date("m/d/Y",strtotime($currentStatusSetByParts[0])) != '12/31/1969' ? date("m/d/Y g:i a",strtotime($currentStatusSetByParts[0])) : "");
						$currentStatusSetBy = " ".$currentStatusSetDate." by ".($record->CurrentStatus < 2 ? $addedBy : emailInitials($currentStatusSetByParts[1]));
						$rebateAmountApproved = $record->RebateAmountApproved;
						$rebateAmountApprovedByParts = explode("|",$record->RebateAmountApprovedBy);
						//print_pre($rebateAmountApprovedByParts);
						$rebateAmountApprovedDate = (date("m/d/Y",strtotime($rebateAmountApprovedByParts[0])) != '12/31/1969' ? date("m/d/Y g:i a",strtotime($rebateAmountApprovedByParts[0])) : "");
						$rebateAmountApprovedBy = ($rebateAmountApproved ? $rebateAmountApprovedDate." by ".emailInitials($rebateAmountApprovedByParts[1]) : "");
						$rebateAmountApprovedBySpan = ($rebateAmountApprovedBy ? $rebateAmountApprovedBy : "<span id='approvedDate".$DatabaseName.$record->id."'></span><br>");
						$rebateAmountApprovedBySpan = (trim($currentStatusSetBy) == trim($rebateAmountApprovedBy) ? "" : $rebateAmountApprovedBySpan);
						$approvedDate = MySQLDate($record->ApprovedDate);
						$approvedDateBy = ($approvedDate ? "<div>Approved Date: ".$approvedDate." approved by ".emailInitials($record->ApprovedDateBy)."</div>" : "");
						$allPartsApproved = ($approvedDateBy ? true : $allPartsApproved);
						$deniedDate = MySQLDate($record->DeniedDate);
						$deniedDateBy = ($deniedDate ? $deniedDate." denied by ".emailInitials($record->DeniedDateBy) : "");
						$verifiedBy = $record->VerifiedBy;
						$verifiedByParts = explode("|",$verifiedBy);
						if (count($verifiedByParts)){$verifiedByDate = $verifiedByParts[0]; $verifiedByName = emailInitials($verifiedByParts[1]);}
						$notes = $record->Notes;
						$notesLastUpdatedBy = ($notes ? "notes last updated ".date("m/d/Y g:i a",strtotime($record->NotesUpdatedDate))." by ".emailInitials($record->NotesUpdatedBy) : "&nbsp;");
						$currentStatusSelector = "<select class='currentStatusSelectPulldown' data-rebatename='".$DatabaseName."' data-rebateid='".$record->id."' id='selectStatus".$DatabaseName.$record->id."'>";
						$currentStatusText = "";
						foreach ($RebateStatusNameByID as $statusId=>$statusName){
								if($statusId==$record->CurrentStatus){$currentStatusText = $statusName;}
								$currentStatusSelector .= "<option value='".$statusId."'".($statusId==$record->CurrentStatus ? " selected" : "").">".$statusName."</option>";
						}
						$currentStatusSelector .= "</select>";
						$editCurrentStatusDisplay = (in_array($currentStatusText,$currentStatusProcessEndValues) ? "Hidden" : "");
					?>
						<div class="section" style="position:relative;float:left;">
							<div><?php echo $statusLine;?> [<?php echo date("m/d/Y g:i a",strtotime($record->receivedTimeStamp));?> by <?php echo $addedBy;?>]<br><?php echo $documentReviewLink;?></div>
							<div class="currentStatus">Status: <span class="currentStatusSelector" id="currentStatus<?php echo $DatabaseName.$record->id;?>Selector"><?php echo $currentStatusSelector;?></span><span id="currentStatus<?php echo $DatabaseName.$record->id;?>"><?php echo $currentStatus.$currentStatusSetBy;?></span><span class="edit editCurrentStatus<?php echo $editCurrentStatusDisplay;?>" title="click to edit list" data-editid="currentStatus<?php echo $DatabaseName.$record->id;?>" style="text-decoration:underline;cursor:pointer">Change Status</span><?php if ($currentStatus == "Approved"){echo " <button title='set back to received' data-database='".$DatabaseName."' data-id='".$record->id."' data-customerid='".$SelectedCustomerID."' id='revertButton'>revert</button>";}?></div>
							<div class="rebateAmount<?php echo (!$approvedDate ? "Hidden" : "");?>" id="rebateAmountDisplay<?php echo $DatabaseName.$record->id;?>">
								
								<?php //echo ($rebateAmountApproved ? $approvedDateBy : "not yet approved");?>
								Amount Approved:
								<?php if ($allPartsApproved){?>
									<?php if (!$rebateAmountApprovedBy){?>
										$<input type='text' class='rebateApprovedAmount' data-rebatename="<?php echo $DatabaseName;?>" data-rebateid="<?php echo $record->id;?>" id='rebateApprovedAmount<?php echo $DatabaseName.$record->id;?>' name='rebateApprovedAmount' value='<?php echo $ApprovedPartsTotal;?>'><span class="edit saveApprovedAmount" title="click to save" data-approvedamountid="rebateApprovedAmount<?php echo $DatabaseName.$record->id;?>" style="text-decoration:underline;cursor:pointer;right:0px;<?php echo ($ApprovedPartsTotal > 0 ? " display:block;" : "");?>">Save Amount</span>
										<?php 
											if (count($ApprovedParts)){
												echo "<br clear='all'>".implode("<br>",$ApprovedParts);
												echo "<br>Total: $".money($ApprovedPartsTotal);
											}
										?>
									<?php }else{echo "$".money($rebateAmountApproved);}?>
									<?php echo $rebateAmountApprovedBySpan;?>
								<?php }else{
										echo "<div style='border:1pt dashed grey;padding:3px;background-color:#fff0b3'><b>The individual rebate amounts need ".str_replace(">".$DisplayName."<",">approval<",$statusLine)."</b><br>";
										foreach ($PartsApproved as $measure=>$measureDetails){
											if ($measureDetails["Approved"] == "No"){
												echo $measure." $".money($measureDetails["RebateAmount"])."<br>";
											}
										}
										echo "</div>";
									}?>
							</div>
							<div>
							<?php
								$deleteVerifiedDateLink = " <img src='".$CurrentServer."images/redx-small.png' style='text-align;right;' class='deleteVerifiedDate' data-rebatename='".$DatabaseName."' data-rebateid='".$record->id."'>";
								if (!MySQLDate($verifiedByDate)){
									$canBeDeletedVerifiedDate = false;
									$deleteVerifiedDateLink = "";
								}else{
									$canBeDeletedVerifiedDate = true;
								}
								if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni)) && $canBeDeletedVerifiedDate){
									$deleteVerifiedDateLink = $deleteVerifiedDateLink;
								}else{
									$deleteVerifiedDateLink = "";
								}
							
							?>
								Verified Date: <span id="verifiedStatus<?php echo $DatabaseName.$record->id;?>"><?php if (!MySQLDate($verifiedByDate)){?><input type="text" class="date dateVerified" value="<?php echo MySQLDate($verifiedByDate);?>" name="verified" data-rebatename="<?php echo $DatabaseName;?>" data-rebateid="<?php echo $record->id;?>"><?php }else{ echo MySQLDate($verifiedByDate)." by ".$verifiedByName.$deleteVerifiedDateLink;}?></span>
							</div>
							<div>
								Notes:<br>
								<textarea style="width:300px;" class="notes" data-rebatename="<?php echo $DatabaseName;?>" data-rebateid="<?php echo $record->id;?>"><?php echo $notes;?></textarea>
								<br><span class="smallNote" id="notefooter<?php echo $DatabaseName.$record->id;?>"><?php echo $notesLastUpdatedBy;?></span>
							</div>
						</div>
<?php
					}
					//print_pre($RebateByID);
					$rebatesFound = true;
				}
				//add an add button if on a Rebate form
				if ($isRebateTemplate && count($RebateInfoByCustomerID[$SelectedCustomerID])>0){
				?>
					<div class="section" style="position:relative;float:left;padding-left:30px;">
					<a href="<?php echo $CurrentServer.$adminFolder."muni/?nav=".$_GET['nav']."&CustomerID=".$_GET['CustomerID'];?>&NewRebate=true" class="button-link">+ New Rebate</a>
					</div>
				<?php
				}//end if SelectedCustomerID
			}
			if (!$rebatesFound){
				echo "No Rebates Found";
			}
		?>		
	</div>
	<br clear="all">
	<script>
		$(document).ready(function() {
			$("#revertButton").on('click',function(){
				var $this = $(this),
					customerId = $this.attr('data-customerid'),
					recordId = $this.attr('data-id'),
					database = $this.attr('data-database');
					data = {};
					data["action"] = "revertApproval";
					data["database"] = database;
					data["customerId"] = customerId;
					data["recordId"] = recordId;
					
					console.log(data);
					$.ajax({
						url: "ApiCustomerData.php",
						type: "PUT",
						data: JSON.stringify(data),
						success: function(data){
							location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
						}
					});
				
				
			});
			$(".dateVerified").datepicker({
				onSelect: function (date) {
					var $this = $(this),
						thisRebateId = $this.attr('data-rebateid'),
						thisRebateName = $this.attr('data-rebatename'),
						thisVal = date,
						thisCurrentStatus = "#verifiedStatus"+thisRebateName+thisRebateId,
						data = {};
					
					data["action"] = "update_rebate_verifieddate";
					data["rebateId"] = thisRebateId;
					data["verifiedDate"] = thisVal;
					data["rebateName"] = thisRebateName;
					data["customerId"] = <?php echo $SelectedCustomerID;?>;
					$.ajax({
						url: "ApiCustomerData.php",
						type: "PUT",
						data: JSON.stringify(data),
						success: function(data){
							console.log(data);
							$(thisCurrentStatus).html(thisVal+" just set by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
						}
					});
					
				}
			});
			$(".date").css('width','80px');
			
			$(".deleteVerifiedDate").on('click',function(){
				var $this = $(this),
					thisRebateId = $this.attr('data-rebateid'),
					thisRebateName = $this.attr('data-rebatename'),
					thisVal = "0000-00-00",
					thisCurrentStatus = "#verifiedStatus"+thisRebateName+thisRebateId,
					data = {};
					
					data["action"] = "update_rebate_verifieddate";
					data["rebateId"] = thisRebateId;
					data["verifiedDate"] = thisVal;
					data["rebateName"] = thisRebateName;
					data["customerId"] = <?php echo $SelectedCustomerID;?>;
					$.ajax({
						url: "ApiCustomerData.php",
						type: "PUT",
						data: JSON.stringify(data),
						success: function(data){
							console.log(data);
							$(thisCurrentStatus).html("Verified Date just removed by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
						}
					});
			});
			
			
			var timer = null;
			var rebateStatus = <?php echo json_encode($RebateStatusNameByID);?>;
			var notesClass = $(".notes"),
				currentStatusClass = $(".currentStatus"),
				editCurrentStatusClass = $(".editCurrentStatus"),
				currentStatusSelectPulldownClass = $(".currentStatusSelectPulldown"),
				saveApprovedAmountClass = $(".saveApprovedAmount"),
				rebateApprovedAmountClass = $(".rebateApprovedAmount");
				
			var updateStatus = function(selectorId){
				var selectorId = "#"+selectorId,
					$this = $(selectorId),
					thisVal = $this.val(),
					selectorIdText = selectorId+" option[value='"+thisVal+"']",
					thisText = $(selectorIdText).text(),
					thisRebateName = $this.attr('data-rebatename'),
					thisRebateId = $this.attr('data-rebateid'),
					thisCurrentStatus = "#currentStatus"+thisRebateName+thisRebateId,
					thisCurrentStatusSelector = thisCurrentStatus+"Selector",
					StatusChange = {};
					
					StatusChange["action"] = "update_currentstatus";
					StatusChange["rebateId"] = thisRebateId;
					StatusChange["currentStatus"] = thisVal;
					StatusChange["currentText"] = thisText;
					StatusChange["rebateName"] = thisRebateName;
					StatusChange["customerId"] = <?php echo $SelectedCustomerID;?>;
				var	data = StatusChange;
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisCurrentStatus).html(rebateStatus[thisVal]+" just set by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						$(thisCurrentStatus).toggle();
						$(thisCurrentStatusSelector).toggle();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						console.log(message);
					}
				});

			};
				
			currentStatusSelectPulldownClass.on('change',function(){
				var $this = $(this),
					thisId = $this.attr('id');
					thisText = $("option:selected",this).text(),
					thisRebateName = $this.attr('data-rebatename'),
					thisRebateId = $this.attr('data-rebateid');
				if (thisText == "Approved"){
					var rebateAmountDisplayID = "#rebateAmountDisplay"+thisRebateName+thisRebateId;
					$(rebateAmountDisplayID).show();
				}else{
					updateStatus(thisId);
				}
			});
			editCurrentStatusClass.on('click',function(){
				var $this = $(this),
					thisEditId = $this.attr('data-editid'),
					currentStatus = "#"+thisEditId;
					currentStatusSelector = "#"+thisEditId+"Selector";
					$(currentStatus).toggle();
					$(currentStatusSelector).toggle();
			});
			/*
			currentStatusClass.mouseover(function(){
				var $this = $(this);
					$this.css('border','1pt dashed silver');
			});
			currentStatusClass.mouseout(function(){
				var $this = $(this);
					$this.css('border','1pt dashed <?php echo $currentStatusBorderColor;?>');
			});
			*/
			notesClass.on('keyup',function(){
				var thisNote = $(this);
				clearTimeout(timer);
				timer = setTimeout(function(){updateComments(thisNote);}, 1000);
			});
			var updateComments = function(thisNote){
				var thisNote = thisNote,
					thisVal = thisNote.val(),
					thisRebateName = thisNote.attr('data-rebatename'),
					thisRebateId = thisNote.attr('data-rebateid'),
					thisNoteFooter = "#notefooter"+thisRebateName+thisRebateId;
				
				var Comments = {};
					Comments["action"] = "update_note";
					Comments["rebateId"] = thisRebateId;
					Comments["notes"] = thisVal;
					Comments["rebateName"] = thisRebateName;
					data = Comments;
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisNoteFooter).html("notes just updated by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						thisNote.val(message);
					}
				});
			};
			rebateApprovedAmountClass.on('keyup',function(){
				var $this = $(this);
				if ($this.val()){
					if ($.isNumeric($this.val())){
						$this.next('span').show();
					}else{
						$this.val('');
						alert('Approved Amount must only be a numbers (no $ or ,)');
					}
				}
			});
			saveApprovedAmountClass.on('click',function(){
				var $this = $(this),
					thisApprovedAmountId = "#"+$this.attr('data-approvedamountid');
					updateAmountApproved($(thisApprovedAmountId),$this);

			});
			var updateAmountApproved = function(thisApprovedAmount,thisSaveSpan){
				var thisApprovedAmount = thisApprovedAmount,
					thisSaveSpan = thisSaveSpan,
					thisVal = thisApprovedAmount.val(),
					thisRebateName = thisApprovedAmount.attr('data-rebatename'),
					thisRebateId = thisApprovedAmount.attr('data-rebateid'),
					thisApprovedDate = "#approvedDate"+thisRebateName+thisRebateId;

				var Comments = {};
					Comments["action"] = "update_approvedamount";
					Comments["rebateId"] = thisRebateId;
					Comments["approvedAmount"] = thisVal;
					Comments["rebateName"] = thisRebateName;
					data = Comments;
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisApprovedDate).html("just approved by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						thisSaveSpan.hide();
						//now up date the status for approved
						var selectStatusId = "selectStatus"+thisRebateName+thisRebateId;
						updateStatus(selectStatusId);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						thisApprovedDate.val(message);
					}
				});
			};
			
		});
	</script>
<?php }//end if $SelectedCustomerID ?>