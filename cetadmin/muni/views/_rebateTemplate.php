<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$newCustomerName = $_GET['NewCustomerName'];
if ($newCustomerName){
	$newCustomerNameParts = explode(" ",$newCustomerName);
	$newCustomerFirstName = ucfirst($newCustomerNameParts[0]);
	$newCustomerLastName = ucfirst($newCustomerNameParts[1]);
}
$newPhoneCustomerName = $_GET['NewPhoneCustomer'];
if ($newPhoneCustomerName){
	$newCustomerName = true;
	$newCustomerFirstName = $_SESSION['PhoneLog']->firstName;
	$newCustomerLastName = $_SESSION['PhoneLog']->lastName;
	$newCustomerAccountNumber = $_SESSION['PhoneLog']->accountNumber;
	$newCustomerPhoneNumber = $_SESSION['PhoneLog']->phoneNumber;
}
	
$muniProvider = new MuniProvider($dataConn);
$customerDataResults = $muniProvider->getResCustomer();
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
}

//get rebate status name
$rebateStatusNameResults = $muniProvider->getRebateStatusNames();
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
}
$rebateNavTypes = array("rebate-heathotwater"=>"Heat Hot Water","rebate-efficiency"=>"Efficiency","rebate-coolhome"=>"Cool Home","rebate-poolpump"=>"Pool Pump","rebate-appliance"=>"Appliance","rebate-commercialheat"=>"Commercial Heat","gasCredit"=>"Gas Credit","rebate-wifi"=>"WiFi");
$SelectedCustomerID = $_GET['CustomerID'];
$rebateName = ($_GET['name'] ? $_GET['name'] : $rebateNavTypes[$navDropDown]);
//print_pre($CustomerByID[$SelectedCustomerID]);

//get rebate info for current client
$rebateId = $_GET['RebateID'];
$criteria = new stdClass();
$criteria->rebateName=$rebateName;
$criteria->customerId = $SelectedCustomerID;
$criteria->rebateId = $rebateId;
//print_pre($criteria);
$rebateInfoResults = $muniProvider->getRebateInfo($criteria);
$rebateInfoCollection = $rebateInfoResults->collection;
//print_pre($rebateInfoCollection);
//echo $rebateName;
$ThisRebateFieldHeader = ($rebateName == "Commercial Heat" ? "MuniComRebateHeatHotwater" : "MuniResRebate".str_replace(" ","",str_replace("Water","water",$rebateName)));
foreach ($rebateInfoCollection as $rebateResult=>$rebateRecord){
$RebateInfoByCustomerID[$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"}] = $rebateRecord;
}
//print_pre($RebateInfoByCustomerID);
//print_pre($RebateInfoByCustomerID[$SelectedCustomerID]);
$rebateName = "rebate".str_replace(" ","",str_replace("Water","water",$rebateName));
$currentRebateStatus = $CustomerByID[$SelectedCustomerID]->$rebateName;
$titleDisplay = " Rebate Info".(trim($CustomerByID[$SelectedCustomerID]->accountHolderFullName) ? " [Account Holder: ".$CustomerByID[$SelectedCustomerID]->accountHolderFullName."]" : "");
//echo $rebateName;

//check for missing parts
$ContractorFields = array("ContractorName","ContractorLicenseNumber","ContractorAddress","ContractorCity","ContractorState","ContractorZip","ContractorPhone","ContractorEmail");
if ($rebateName == "rebateCoolHome"){$ContractorFields[] = "ContractorNATECertified";}
foreach ($ContractorFields as $contractorField){
	$thisField = $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_".$contractorField};
	if ($thisField){$ContractorFieldValue["TotalFieldsWithData"] = $ContractorFieldValue["TotalFieldsWithData"]+1;}
	$ContractorFieldValue[$contractorField] = $thisField;
}
//print_pre($ContractorFieldValue);
$contractorInfoMissing = (count($ContractorFields) == $ContractorFieldValue["TotalFieldsWithData"] ? false : true);
$RebateFields["rebateCommercialHeat"] = array("Measure_Furnace95Manufacturer","Measure_Furnace95ModelNumber","Measure_Furnace95EfficiencyRating","Measure_Furnace95InputSize","Measure_Furnace95RebateAmount","Measure_Furnace95QuantityInstalled","Measure_Furnace95InstalledCost","Measure_Furnace95Approved","Measure_Furnace97Manufacturer","Measure_Furnace97ModelNumber","Measure_Furnace97EfficiencyRating","Measure_Furnace97InputSize","Measure_Furnace97RebateAmount","Measure_Furnace97QuantityInstalled","Measure_Furnace97InstalledCost","Measure_Furnace97Approved","Measure_Boiler85Manufacturer","Measure_Boiler85ModelNumber","Measure_Boiler85EfficiencyRating","Measure_Boiler85InputSize","Measure_Boiler85RebateAmount","Measure_Boiler85QuantityInstalled","Measure_Boiler85InstalledCost","Measure_Boiler85Approved","Measure_Boiler90Manufacturer","Measure_Boiler90ModelNumber","Measure_Boiler90EfficiencyRating","Measure_Boiler90InputSize","Measure_Boiler90RebateAmount","Measure_Boiler90QuantityInstalled","Measure_Boiler90InstalledCost","Measure_Boiler90Approved","Measure_Boiler95Manufacturer","Measure_Boiler95ModelNumber","Measure_Boiler95EfficiencyRating","Measure_Boiler95InputSize","Measure_Boiler95RebateAmount","Measure_Boiler95QuantityInstalled","Measure_Boiler95InstalledCost","Measure_Boiler95Approved","Measure_Integrated90Manufacturer","Measure_Integrated90ModelNumber","Measure_Integrated90EfficiencyRating","Measure_Integrated90InputSize","Measure_Integrated90RebateAmount","Measure_Integrated90QuantityInstalled","Measure_Integrated90InstalledCost","Measure_Integrated90Approved","Measure_Integrated95Manufacturer","Measure_Integrated95ModelNumber","Measure_Integrated95EfficiencyRating","Measure_Integrated95InputSize","Measure_Integrated95RebateAmount","Measure_Integrated95QuantityInstalled","Measure_Integrated95InstalledCost","Measure_Integrated95Approved","Measure_IndirectWaterManufacturer","Measure_IndirectWaterModelNumber","Measure_IndirectWaterEfficiencyRating","Measure_IndirectWaterInputSize","Measure_IndirectWaterRebateAmount","Measure_IndirectWaterQuantityInstalled","Measure_IndirectWaterInstalledCost","Measure_IndirectWaterApproved","Measure_H2OStorage62Manufacturer","Measure_H2OStorage62ModelNumber","Measure_H2OStorage62EfficiencyRating","Measure_H2OStorage62InputSize","Measure_H2OStorage62RebateAmount","Measure_H2OStorage62QuantityInstalled","Measure_H2OStorage62InstalledCost","Measure_H2OStorage62Approved","Measure_H2OStorage67Manufacturer","Measure_H2OStorage67ModelNumber","Measure_H2OStorage67EfficiencyRating","Measure_H2OStorage67InputSize","Measure_H2OStorage67RebateAmount","Measure_H2OStorage67QuantityInstalled","Measure_H2OStorage67InstalledCost","Measure_H2OStorage67Approved","Measure_Tankless82Manufacturer","Measure_Tankless82ModelNumber","Measure_Tankless82EfficiencyRating","Measure_Tankless82InputSize","Measure_Tankless82RebateAmount","Measure_Tankless82QuantityInstalled","Measure_Tankless82InstalledCost","Measure_Tankless82Approved","Measure_Tankless94Manufacturer","Measure_Tankless94ModelNumber","Measure_Tankless94EfficiencyRating","Measure_Tankless94InputSize","Measure_Tankless94RebateAmount","Measure_Tankless94QuantityInstalled","Measure_Tankless94InstalledCost","Measure_Tankless94Approved","TotalAnticipatedRebate","InstalledCost","InstalledDate","SupportingFile");
$RebateFields["rebateHeatHotwater"] =   array("Measure_Furnace95Manufacturer","Measure_Furnace95ModelNumber","Measure_Furnace95EfficiencyRating","Measure_Furnace95InputSize","Measure_Furnace95RebateAmount","Measure_Furnace95QuantityInstalled","Measure_Furnace95InstalledCost","Measure_Furnace95Approved","Measure_Furnace97Manufacturer","Measure_Furnace97ModelNumber","Measure_Furnace97EfficiencyRating","Measure_Furnace97InputSize","Measure_Furnace97RebateAmount","Measure_Furnace97QuantityInstalled","Measure_Furnace97InstalledCost","Measure_Furnace97Approved","Measure_Boiler85Manufacturer","Measure_Boiler85ModelNumber","Measure_Boiler85EfficiencyRating","Measure_Boiler85InputSize","Measure_Boiler85RebateAmount","Measure_Boiler85QuantityInstalled","Measure_Boiler85InstalledCost","Measure_Boiler85Approved","Measure_Boiler90Manufacturer","Measure_Boiler90ModelNumber","Measure_Boiler90EfficiencyRating","Measure_Boiler90InputSize","Measure_Boiler90RebateAmount","Measure_Boiler90QuantityInstalled","Measure_Boiler90InstalledCost","Measure_Boiler90Approved","Measure_Boiler95Manufacturer","Measure_Boiler95ModelNumber","Measure_Boiler95EfficiencyRating","Measure_Boiler95InputSize","Measure_Boiler95RebateAmount","Measure_Boiler95QuantityInstalled","Measure_Boiler95InstalledCost","Measure_Boiler95Approved","Measure_Integrated90Manufacturer","Measure_Integrated90ModelNumber","Measure_Integrated90EfficiencyRating","Measure_Integrated90InputSize","Measure_Integrated90RebateAmount","Measure_Integrated90QuantityInstalled","Measure_Integrated90InstalledCost","Measure_Integrated90Approved","Measure_Integrated95Manufacturer","Measure_Integrated95ModelNumber","Measure_Integrated95EfficiencyRating","Measure_Integrated95InputSize","Measure_Integrated95RebateAmount","Measure_Integrated95QuantityInstalled","Measure_Integrated95InstalledCost","Measure_Integrated95Approved","Measure_IndirectWaterManufacturer","Measure_IndirectWaterModelNumber","Measure_IndirectWaterEfficiencyRating","Measure_IndirectWaterInputSize","Measure_IndirectWaterRebateAmount","Measure_IndirectWaterQuantityInstalled","Measure_IndirectWaterInstalledCost","Measure_IndirectWaterApproved","Measure_H2OStorage62Manufacturer","Measure_H2OStorage62ModelNumber","Measure_H2OStorage62EfficiencyRating","Measure_H2OStorage62InputSize","Measure_H2OStorage62RebateAmount","Measure_H2OStorage62QuantityInstalled","Measure_H2OStorage62InstalledCost","Measure_H2OStorage62Approved","Measure_H2OStorage67Manufacturer","Measure_H2OStorage67ModelNumber","Measure_H2OStorage67EfficiencyRating","Measure_H2OStorage67InputSize","Measure_H2OStorage67RebateAmount","Measure_H2OStorage67QuantityInstalled","Measure_H2OStorage67InstalledCost","Measure_H2OStorage67Approved","Measure_Tankless82Manufacturer","Measure_Tankless82ModelNumber","Measure_Tankless82EfficiencyRating","Measure_Tankless82InputSize","Measure_Tankless82RebateAmount","Measure_Tankless82QuantityInstalled","Measure_Tankless82InstalledCost","Measure_Tankless82Approved","Measure_Tankless94Manufacturer","Measure_Tankless94ModelNumber","Measure_Tankless94EfficiencyRating","Measure_Tankless94InputSize","Measure_Tankless94RebateAmount","Measure_Tankless94QuantityInstalled","Measure_Tankless94InstalledCost","Measure_Tankless94Approved","TotalAnticipatedRebate","InstalledCost","InstalledDate","SupportingFile");
$RebateFields["rebateEfficiency"] = array("EligibleMeasures","MeasuresInstalled","AirSealingApprovedAmount","InsulationApprovedAmount","HeatingApprovedAmount");
$RebateFields["rebateCoolHome"] = array("EquipmentNewConstruction","EquipmentReplacement","EquipmentAdding","EquipmentNewOrAdd","EquipmentInstalledType","EquipmentInstalledDate","EquipmentInstalledCost","EquipmentAHRIRef","EquipmentManufacturer","EquipmentTXVorEXV","EquipmentCondensorModelNumber","EquipmentCoilModelNumber","EquipmentAHRIRatedSEER","EquipmentAHRIRatedEER","EquipmentHSPF","EquipmentNewUnitSize","EquipmentMiniSplit","EquipmentRebateAmount","Measure_CentralAC16Manufacturer","Measure_CentralAC16CondenserModelNumber","Measure_CentralAC16CoilModelNumber","Measure_CentralAC16SEER","Measure_CentralAC16EER","Measure_CentralAC16HSPF","Measure_CentralAC16SizeTons","Measure_CentralAC16InstallCost","Measure_CentralAC16Rebate","Measure_CentralAC16QtyInstalled","Measure_CentralAC16TotalRebate","Measure_CentralAC16Approved","Measure_CentralAC18Manufacturer","Measure_CentralAC18CondenserModelNumber","Measure_CentralAC18CoilModelNumber","Measure_CentralAC18SEER","Measure_CentralAC18EER","Measure_CentralAC18HSPF","Measure_CentralAC18SizeTons","Measure_CentralAC18InstallCost","Measure_CentralAC18Rebate","Measure_CentralAC18QtyInstalled","Measure_CentralAC18TotalRebate","Measure_CentralAC18Approved","Measure_Ductless18Manufacturer","Measure_Ductless18CondenserModelNumber","Measure_Ductless18CoilModelNumber","Measure_Ductless18SEER","Measure_Ductless18EER","Measure_Ductless18HSPF","Measure_Ductless18SizeTons","Measure_Ductless18InstallCost","Measure_Ductless18Rebate","Measure_Ductless18QtyInstalled","Measure_Ductless18TotalRebate","Measure_Ductless18Approved","Measure_Ductless20Manufacturer","Measure_Ductless20CondenserModelNumber","Measure_Ductless20CoilModelNumber","Measure_Ductless20SEER","Measure_Ductless20EER","Measure_Ductless20HSPF","Measure_Ductless20SizeTons","Measure_Ductless20InstallCost","Measure_Ductless20Rebate","Measure_Ductless20QtyInstalled","Measure_Ductless20TotalRebate","Measure_Ductless20Approved");
$RebateFields["rebatePoolPump"] = array("ReplacedManufacturer","ReplacedModelNumber","ReplacedHorsepower","ReplacedType","NewManufacturer","NewModelNumber","NewHorsepower","NewType","NewControllerManufacturer","NewControllerModelNumber","NewPrice","NewPurchaseDate","NewInstalledDate");
$RebateFields["rebateAppliance"] = array("ApplianceType","ApplianceBrand","ApplianceModelNumber","ApplianceSerialNumber","StoreName","StoreAddress","StoreCity","StoreState","StoreZipCode","StorePurchasePrice","StorePurchaseDate","ReceivedTimeStamp");
$RebateFields["rebateWiFi"] = array("Measure_WiFiDateInstalled","Measure_WiFiManufacturer","Measure_WiFiModelNumber","Measure_WiFiPurchasePrice","Measure_WiFiQuantity","Measure_WiFiTotalAmount","Measure_WiFiApproved");
foreach ($RebateFields[$rebateName] as $rebateFieldName){
	$thisField = $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_".$rebateFieldName};
	if ($thisField){$RebateFieldValue[$rebateName]["TotalFieldsWithData"] = $RebateFieldValue[$rebateName]["TotalFieldsWithData"]+1;}
	$RebateFieldValue[$rebateName][$rebateFieldName] = $thisField;
	$RebateFieldValue[$rebateName]["oid"] = $RebateInfoByCustomerID[$SelectedCustomerID]->oid;
}
$rebateName = str_replace("Water","water",$rebateName);
$measureInfoMissing = (count($RebateFields[$rebateName]) == $RebateFieldValue[$rebateName]["TotalFieldsWithData"] ? false : true);
$DocumentationFields = array("SupportingFile");
foreach ($DocumentationFields as $documetationField){
	$thisField = $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_".$documetationField};
	if ($thisField){$DocumentationFieldValue["TotalFieldsWithData"] = $DocumentationFieldValue["TotalFieldsWithData"]+1;}
	$DocumentationFieldValue[$documetationField] = $thisField;
}
$documentationInfoMissing = (count($DocumentationFields) == $DocumentationFieldValue["TotalFieldsWithData"] ? false : true);

$FacilityFields = array("FacilityName","FacilityContact","FacilityEmail","FacilityPhone","FacilityInstalledAddress","FacilityInstalledCity","FacilityInstalledState","FacilityInstalledZip","FacilityType");
foreach ($FacilityFields as $facilityField){
	$thisField = $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_".$facilityField};
	if ($thisField){$FacilityFieldValue["TotalFieldsWithData"] = $FacilityFieldValue["TotalFieldsWithData"]+1;}
	$FacilityFieldValue[$facilityField] = $thisField;
}
//print_pre($FacilityFieldValue);
$facilityInfoMissing = (count($FacilityFields) == $FacilityFieldValue["TotalFieldsWithData"] ? false : true);

//array used for _customerRebateStatus to limit displaying only status for specific rebate Type
$rebateTypes = array("Heat Hot water"=>"rebateHeatHotwater","Efficiency"=>"rebateEfficiency","Cool Home"=>"rebateCoolHome","Pool Pump"=>"rebatePoolPump","Appliance"=>"rebateAppliance","Commercial Heat"=>"rebateCommercialHeat","WiFi"=>"rebateWiFi");
$rebateTypesKey = array_search($rebateName,$rebateTypes);
$rebateTypesName = $rebateName;
$rebateTypes = array($rebateTypesKey=>$rebateTypesName);

if ($_GET['NewRebate']){
	$RebateInfoByCustomerID = array();
	$RebateFieldValue = array();
	$measureInfoMissing	= true;
	$DocumentationFieldValue = array();
	$documentationInfoMissing = true;
	$FacilityFieldValue = array();
	$facilityInfoMissing = true;
}

?>
<hr>
<?php if (!$SelectedCustomerID && !$newCustomerName){?>
	<div class="fifteen columns">
		<?php 
			$comboBoxRemoveIfInvalid = " ";
			include('_CustomerComboBox.php');
		?>
	</div>
<?php }else{?>
	<?php
		$isRebateTemplate = true;
		if (!$_GET['NewRebate']){
			include_once('_customerRebateStatus.php');
		}
	?>
	<br>
	<a href="#" id="rebateHeaderButton" class="button-link">Show Rebate Header Info</a>
	<?php include_once('rebateTemplates/_'.$navDropDown.'.php');?>
<?php }?>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		<?php if ($contractorCheck){?>
			var contractorCheckEl = $(".contractorCheck");
			var checkingContractorLicense = function(){
				var valCount = 0,
					data = {};
					fieldsToCheck = {};
				if (contractorCheckEl.val() !=""){
					data["license"] = contractorCheckEl.val();
					data["action"] = "checkContractor";
					//console.log(JSON.stringify(data));
					
					$.ajax({
						url: "ApiCustomerData.php",
						type: "POST",
						data: JSON.stringify(data),
						success: function(data){
							console.log(data);
							var countdata = data["count"];
							if (countdata > 0){
								$("#contractorCheckResults").show().html('<br>*Note: license confirmed on Prior Rebates');
							}
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);

						}
					});
					
					
				}
				
			};
			
			checkingContractorLicense();
			
			contractorCheckEl.on('blur',function(){
				checkingContractorLicense(this);
			});
			
		<?php }?>
		<?php if($checkApproved){?>
			var checkApprovedEl = $(".checkApproved");
			
			var checkingApproved = function(el){
				var $this = $(el),
					thisFieldName = $this.attr('data-fieldname'),
					thisFieldClass = "."+thisFieldName,
					thisFieldCount = $(thisFieldClass).length,
					thisApprovalDiv = "#ApprovalDiv"+thisFieldName,
					thisPreviouslyApproved = "#PreviouslyApproved"+thisFieldName,
					valCount = 0,
					data = {};
					fieldsToCheck = {};
				
					
				$.each($(thisFieldClass),function(key,val){
					var thisEl = $(val),
						thisElVal = thisEl.val();
					if (thisElVal != ''){
						valCount++;
						fieldsToCheck[thisEl.attr('name')] = thisEl.val();
					}
					//console.log(thisElVal);
				});
				if (valCount == thisFieldCount){
					data["fields"] = fieldsToCheck;
					data["action"] = "checkApproved";
					data["rebateName"] = "<?php echo str_replace("-","_",$navDropDown);?>";
					//console.log(JSON.stringify(data));
					
					$.ajax({
						url: "ApiCustomerData.php",
						type: "POST",
						data: JSON.stringify(data),
						success: function(data){
							console.log(data);
							var countdata = data["count"];
							if (countdata > 0){
								$(thisApprovalDiv).show();
								$(thisPreviouslyApproved).show().html('<br>*Note: Item Approved on Prior Rebates');
							}
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);

						}
					});
					
					
				}
				
			};
			
			$.each(checkApprovedEl,function(key,val){
				checkingApproved(val);
			});
			
			checkApprovedEl.on('blur',function(){
				checkingApproved(this);
			});
			
		<?php }?>
		
		
		var dateClass = $(".date");
		dateClass.width(80);
		dateClass.datepicker({
			maxDate:0
		});
		
		var rebateHeaderClass = $(".rebateHeader"),
			rebateHeaderButton = $("#rebateHeaderButton"),
			termsAndConditionsClass = $(".termsAndConditions"),
			closedDetailsClass = $(".closedDetails"),
			missingDetails = $(".missingDetails");
		rebateHeaderClass.hide();
		termsAndConditionsClass.hide();
		closedDetailsClass.hide();
		missingDetails.show();
		rebateHeaderButton.on('click',function(){
			var $this = $(this);
				if ($this.html() == '<span class="ui-button-text">Show Rebate Header Info</span>'){
					$this.html('<span class="ui-button-text">Hide Rebate Header Info</span>');
					rebateHeaderClass.slideDown();
				}else{
					$this.html('<span class="ui-button-text">Show Rebate Header Info</span>');
					rebateHeaderClass.slideUp();
				}
		});
		$("#closeRebateHeader").on('click',function(){
			rebateHeaderButton.click();
		});
		$("#termsButton").on('click',function(){
			termsAndConditionsClass.toggle();
		});
		$('.asDisplay').on('click', 'span.details-open', function () {
			var div = $(this).parent('div');
			var details = $(this).next('div');

			if ( details.is(":visible") ) {
				details.hide();
				div.removeClass('opened');
			}
			else {
				details.show();
				div.addClass('opened');
			}
		} );
		$("#addNewCustomerButton").on('click',function(e){
			e.preventDefault();
			window.location = "?nav=<?php echo $_GET['nav'];?>&NewCustomerName="+$("#customerNotFound").val();

		});
		$("#addNewPhoneCustomerButton").on('click',function(e){
			e.preventDefault();
			window.location = "?nav=<?php echo $_GET['nav'];?>&NewPhoneCustomer=true";

		});
		$("#utilityAccountNumberSource").on('keyup',function(){
			$("#utilityAccountNumberHidden").val($(this).val());
		});

		var formCustomerIntakeForm = $("#CustomerIntakeForm");
		var formElementRebate = $("#onlineRebateForm");
		formCustomerIntakeForm.validate({
			rules: {
				utilityAccountNumber<?php echo ($_GET['NewCustomerName'] ? "Source" : "");?>: "required",
				accountHolderFirstName: "required",
				accountHolderLastName: "required",
				ownerFirstName: "required",
				ownerLastName: "required"
			},
			messages: {
				utilityAccountNumber<?php echo ($_GET['NewCustomerName'] ? "Source" : "");?>: "If unknown use the button",
				accountHolderFirstName: "First Name",
				accountHolderLastName: "Last Name",
				ownerFirstName: "Owner First Name",
				ownerLastName: "Owner Last Name"
			}
		});
		formElementRebate.validate({
			rules: {
				customerAcknowledgementDate: {required: true}
			}
		});
		
		$("#save").click(function(e){
			$('#onlineRebateResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			<?php if (!$rebateId && !$SelectedCustomerID){?>
			if (formCustomerIntakeForm.valid()){
			<?php }?>
				if (formElementRebate.valid()){
					<?php if ($newCustomerName){?>
						$("#save-intake-add").click();
					<?php }else{?>
						$("#saveHidden").click();
					<?php }?>
					$(this).hide();
				}
			<?php if (!$rebateId && !$SelectedCustomerID){?>
			}else{
				alert('Missing Fields in Contact Info');
			}
			<?php }?>
		});
		$("#saveHidden").click(function(e){
			var backToCustomerDetails = $("#backToCustomerDetails");
			$('#onlineRebateResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElementRebate.valid()){
				console.log(JSON.stringify(formElementRebate.serializeObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(formElementRebate.serializeObject()),
					success: function(data){
						//console.log("rebate form data");
						console.log(data);
						if (data.customerLookup){
							var thisCustomerId = data.customerLookup.collection[0].customerId;
						}else{
							var thisCustomerId = data.customerID;
						}
						$('#onlineRebateResults').html("Saved").addClass("alert").addClass("info");
						backToCustomerDetails.show().attr('href','<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data&CustomerID='+thisCustomerId);

					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log(jqXHR);
						var message = $.parseJSON(jqXHR.responseText);
						$('#onlineRebateResults').html(message).addClass("alert");
					}
				});
			}
		});
		$(".deleteDocument").click(function(e){
			e.preventDefault();
			var $this = $(this),
				rebateName = $this.attr('data-rebatename'),
				rebateId = $this.attr('data-rebateid'),
				documentId = $this.attr('data-documentid'),
				documentTarget = "#documentTarget"+rebateId+"_"+documentId;
				documentImage = "#documentImage"+rebateId+"_"+documentId;
				documentReview = "#documentReview"+rebateId+"_"+documentId;
				customerId = $this.attr('data-customerid'),
				documentName = $this.attr('data-documentname'),
				data = {};
			data['action'] = 'update_document';
			data['deleteDocument'] = true;
			data['rebateName'] = rebateName;
			data['rebateId'] = rebateId;
			data['customerId'] = customerId;
			data['documentName'] = documentName;
			console.log(data);
			$.ajax({
				url: "ApiCustomerData.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data)
					$this.hide();
					console.log(documentTarget);
					$(documentTarget).hide();
					$(documentImage).hide();
					$(documentReview).hide();
					$("#supportingFileValueAdditional").val(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					console.log(jqXHR);
					var message = $.parseJSON(jqXHR.responseText);
					$('#DeleteDocumentResults').html(message).addClass("alert");
				}
			});
			
		});
	});
</script>