<?php
$path = getcwd();
$pathSeparator = "/";
if (strpos($path,"xampp")){
	$pathSeparator = "\\";
	$testingServer = true;
}
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";}else{$path = "..";}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($path.$pathSeparator."_config.php");

$dir = $path.$pathSeparator."muni".$pathSeparator."rebateAttachments".$pathSeparator."AWS".$pathSeparator;
$sinceFile = $dir."since.txt";
$sinceDateStamp = file_get_contents($sinceFile); //TODO use a query to find out when last checked
//echo "since: ".date("m/d/Y H:i:s", strtotime($sinceDateStamp));

// Open a directory, and read its contents
if (is_dir($dir)){
  if ($dh = opendir($dir)){
    while (($file = readdir($dh)) !== false){
		if (strlen($file)>10 && !strpos($file,".txt")){
			$lastModified = date ("Y-m-d", filemtime($dir.$file));
			$lastModifiedDate = date ("Y-m-d_H:i:s", filemtime($dir.$file));
			$filesByLastModified[$lastModified][] = array(
				"fileName"=>$file,
				"lastModified"=>$lastModifiedDate,
				"href"=>"<a href='".$CurrentServer.$adminFolder."muni/rebateAttachments/AWS/".$file."' title='Last Modified:".$lastModifiedDate."' target='".$file."'>".$file."</a>"
			);
			//echo "filename:" . $file . "was last modified: " . date ("F d Y H:i:s.", filemtime($dir.$file))."<br>";
		}
    }
    closedir($dh);
  }
}
echo "<h2>View and Retrieve Attachments</h2><button id='checkForAttachments'>Check For New Rebates Attachments</button> <span id='results'>Last Checked: ".date("m/d/Y H:i a", strtotime($sinceDateStamp))."</span><hr>";
foreach ($filesByLastModified as $lastDate=>$files){
	echo date("m/d/Y",strtotime($lastDate))."<br>";
	echo "<div style='padding:20px;'>";
	foreach ($files as $file){
		echo $file["href"]."<br>";
	}
	echo "</div><hr>";
}
?>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		$("#checkForAttachments").on('click',function(){
			var $this = $(this);
			$this.html('Checking...');
			$("#results").html('<img src="<?php echo $CurrentServer.$adminFolder."images/loading.gif";?>" style="height:20px;">');
			$.ajax({
				url: "<?php echo $CurrentServer.$adminFolder;?>cronjobs/muni_WestfieldReceiverAWSv2.php",
				type: "GET",
				data: {checkType: "attachments"},
				success: function(data){
					console.log(data);
					if (data > 0){
						$("#results").html(data+' attachments downloaed');
						location.reload();
					}else{
						$this.html('Check For New Rebates Attachments');
						$("#results").html('No new attachments');
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					console.log(message);

				}
			});
		});
	});
</script>