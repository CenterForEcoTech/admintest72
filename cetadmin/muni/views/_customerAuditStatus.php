<?php if ($SelectedCustomerID){
	include_once($siteRoot."_setupDataConnection.php");
	include_once($dbProviderFolder."MuniProvider.php");
	$currentStatusBorderColor = "white";
	$rebateAmountBorderColor = "white";
	$currentStatusProcessEndValues = array("Report Sent");
	
	$auditStatusNameResults = $muniProvider->getAuditStatusNames();
	$auditStatusNameCollection = $auditStatusNameResults->collection;
	foreach ($auditStatusNameCollection as $statusResult=>$statusRecord){
		$AuditStatusNameByID[$statusRecord->id] = $statusRecord->name;
		$AuditStatusByID[$statusRecord->id] = $statusRecord;
	}
	$AuditStatusByID[0] = $AuditStatusByID[1]; //set a blank status
	$AuditStatusNameByID[0] = "";
	
?>
	<style>
		.smallNote {font-size:8pt}
		.mediumNote {font-size:10pt}
		.auditcurrentStatus {padding-right:10px;margin:2px;border:1pt dashed <?php echo $currentStatusBorderColor;?>;}
		.auditcurrentStatusSelector {display:none;}
		.auditcurrentStatusSelectPulldown {font-size:10px;width:120px;height:15px;}
		.auditeditCurrentStatusHidden {display:none;}
		.auditeditCurrentStatus {text-decoration:underline;cursor:pointer;}
		.auditScheduledBy {font-size:10px;right:0px;margin-left:50px;}
	</style>
	<br clear="all">
	<h3><?php echo (trim($CustomerByID[$SelectedCustomerID]->accountHolderFullName) ? $CustomerByID[$SelectedCustomerID]->accountHolderFullName : "Customer");?> Audit Progress</h3>
	<div class="section mediumNote" style="position:relative;float:left;">
		<?php
		set_time_limit(300000);
		ini_set('memory_limit','14096M');
			$auditStatus = $CustomerByID[$SelectedCustomerID]->auditStatus;
			//print_pre($CustomerByID[$SelectedCustomerID]);
			//get auditInfo
			$criteria = new stdClass();
			$criteria->customerId = $SelectedCustomerID;
			$auditInforResults = $muniProvider->getAuditInfo($criteria);
			$rawFormData = json_decode($auditInforResults->MuniAudit_RawFormData);
			$auditTimeStamp = $auditInforResults->MuniAudit_AddedTimeStamp;
			$auditAddedBy = $auditInforResults->MuniAudit_AddedByAdmin;
			

			if ($auditStatus){
				//check if file is actually downloaded:
				$addedBy = emailInitials($auditAddedBy);
				$currentStatus = $AuditStatusNameByID[$auditStatus];
					$currentStatus = "
						<div id='auditStatusDiv".$Record->id."'style='display:inline-block;height:20px;text-align:right;padding:2px;background-color:".$AuditStatusByID[$auditStatus]->backgroundColor.";border:1pt solid ".$AuditStatusByID[$auditStatus]->borderColor.";'>
							<a style='float:left;height:20px;text-decoration:none;color:".$AuditStatusByID[$auditStatus]->textColor.";' href='".$CurrentServer.$adminFolder."muni/?nav=auditForm&CustomerID=".$SelectedCustomerID."'>".$AuditStatusByID[$auditStatus]->name."</a>
							<span style='display:none;'>".$AuditStatusByID[$auditStatus]->name."</span>".$deleteAuditLink."
						</div>
							";
						if ($Record->auditStatus > 0){
							$auditStatusLine .= $currentStatus;
						}

				$currentStatusSetByParts = explode("|",$auditInforResults->CurrentStatusSetBy);
				$currentStatusSetDate = (date("m/d/Y",strtotime($currentStatusSetByParts[0])) != '12/31/1969' ? date("m/d/Y g:i a",strtotime($currentStatusSetByParts[0])) : "");
				$currentStatusSetByName = $addedBy;
				$currentStatusSetByName = ($auditInforResults->CurrentStatus > 1 ? $addedBy : emailInitials($currentStatusSetByParts[1]));
				$currentStatusSetBy = " ".$currentStatusSetDate .(strlen($currentStatusSetByName) > 22 ? " by ".$currentStatusSetByName : "");  //22 is the strlen of the spans for a blank emailInitials
				$notes = $auditInforResults->Notes;
				$notesLastUpdatedBy = ($notes ? "notes last updated ".date("m/d/Y g:i a",strtotime($auditInforResults->NotesUpdatedDate))." by ".emailInitials($auditInforResults->NotesUpdatedBy) : "&nbsp;");
				$currentStatusSelector = "<select class='auditcurrentStatusSelectPulldown' data-auditid='".$auditInforResults->MuniAudit_ID."'>";
				$currentStatusText = "";
				foreach ($AuditStatusNameByID as $statusId=>$statusName){
						if($statusId==$auditInforResults->CurrentStatus){$currentStatusText = $statusName;}
						if ($statusName == "Cancelled"){
							$currentStatusSelector .= "<option disabled>-----------</option>";
						}
						$currentStatusSelector .= "<option value='".$statusId."'".($statusId==$auditInforResults->CurrentStatus ? " selected" : "").">".$statusName."</option>";
				}
				$currentStatusSelector .= "</select>";
				$editCurrentStatusDisplay = (in_array($currentStatusText,$currentStatusProcessEndValues) ? "Hidden" : "");
				$scheduledDate = (MySQLDate($auditInforResults->ScheduledDate) ? $auditInforResults->ScheduledDate : $CustomerByID[$SelectedCustomerID]->auditScheduledDate);
				$scheduledTime = ($auditInforResults->ScheduledTime ? $auditInforResults->ScheduledTime : $CustomerByID[$SelectedCustomerID]->auditScheduledTime);
				$auditor = ($auditInforResults->Auditor ? $auditInforResults->Auditor : $CustomerByID[$SelectedCustomerID]->auditor);
				$scheduledBy = ($auditInforResults->ScheduledBy ? $auditInforResults->ScheduledBy : $CustomerByID[$SelectedCustomerID]->auditScheduledBy);
				if ($scheduledBy){
					$scheduledByParts = explode("|",$scheduledBy);
					$scheduledBySetDate = (date("m/d/Y",strtotime($scheduledByParts[0])) != '12/31/1969' ? date("m/d/Y g:i a",strtotime($scheduledByParts[0])) : "");
					$scheduledByName = emailInitials($scheduledByParts[1]);
					if ($auditor == "Cancelled"){
						$scheduledBySetBy = " Audit Cancelled on ".$scheduledBySetDate .(strlen($scheduledByName) > 22 ? " by ".$scheduledByName : "");  //22 is the strlen of the spans for a blank emailInitials
					}else{
						$scheduledBySetBy = " Audit Scheduled and Auditor entered on ".$scheduledBySetDate .(strlen($scheduledByName) > 22 ? " by ".$scheduledByName : "");  //22 is the strlen of the spans for a blank emailInitials
					}
				}
				$verifiedBy = $auditInforResults->VerifiedBy;
				$verifiedByParts = explode("|",$verifiedBy);
				if (count($verifiedByParts)){$verifiedByDate = $verifiedByParts[0]; $verifiedByName = emailInitials($verifiedByParts[1]);}
			?>
				<div class="section" style="position:relative;float:left;">
					<div><?php echo $statusLine." ".($auditTimeStamp ? "[Added: ".date("m/d/Y g:i a",strtotime($auditTimeStamp))." by ".$addedBy."]" : "")."<br>".$documentReviewLink;?></div>
					<div class="auditcurrentStatus"><span class="auditeditCurrentStatus<?php echo $editCurrentStatusDisplay;?>" title="click to edit status" data-editid="auditcurrentStatus<?php echo $auditInforResults->MuniAudit_ID;?>">Change Status:</span> <span class="auditcurrentStatusSelector" id="auditcurrentStatus<?php echo $auditInforResults->MuniAudit_ID;?>Selector"><?php echo $currentStatusSelector;?></span><span id="auditcurrentStatus<?php echo $auditInforResults->MuniAudit_ID;?>" style="padding-left:45px;"><?php echo $currentStatus.$currentStatusSetBy;?></span></div>
					<div>Audit <span id="auditScheduledCanceledDate<?php echo $auditInforResults->MuniAudit_ID;?>">Scheduled</span>: 
						<input title="To update a date, wait for page to fully load, pick the new date, then change the Auditor to a blank space and then change the Auditor back to set new date" type="text" class="auditDate" value="<?php echo MySQLDate($scheduledDate);?>" id="scheduledDate<?php echo $auditInforResults->MuniAudit_ID;?>"><br>
						<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Time:<input title="To update a time, wait for page to fully load, enter the new time, then change the Auditor to a blank space and then change the Auditor back to set new time" type="text" style="width:100px;" value="<?php echo $scheduledTime;?>" id="scheduledTime<?php echo $auditInforResults->MuniAudit_ID;?>"><br>
					</div>
					
					<div>Auditor: 
						<select id="auditor<?php echo $auditInforResults->MuniAudit_ID;?>" style="width:50%;" class="auditorSelectPulldown" data-auditid="<?php echo $auditInforResults->MuniAudit_ID;?>" data-customerid="<?php echo $SelectedCustomerID;?>" data-scheduleddate="scheduledDate<?php echo $auditInforResults->MuniAudit_ID;?>" data-scheduledtime="scheduledTime<?php echo $auditInforResults->MuniAudit_ID;?>">
							<option></option>
							<option value="Angel Ortiz"<?php echo ($auditor == "Angel Ortiz" || $auditor == "Angel O" ? " selected":"");?>>Angel Ortiz</option>
							<option value="Sam Stroman"<?php echo ($auditor == "Sam Stroman" || $auditor == "Sam S" ? " selected":"");?>>Sam Stroman </option>
							<option value="Ben Coe"<?php echo ($auditor == "Ben Coe" || $auditor == "Ben C" ? " selected":"");?>>Ben Coe</option>
							<option value="Matt Zarotny"<?php echo ($auditor == "Matt Zarotny" || $auditor == "Matt Z" ? " selected":"");?>>Matt Zarotny</option>
							<option value="CJ Hanley"<?php echo ($auditor == "CJ Hanley" | $auditor == "CJ H" ? " selected":"");?>>CJ Hanley</option>
							<option></option>
							<option value="Cancelled"<?php echo ($auditor == "Cancelled" ? " selected":"");?>>Cancelled</option>></option>
							<option value="Jen Putnam"<?php echo ($auditor == "Jen Putnam" ? " selected":"");?> disabled>Jen Putnam</option>
							<option value="Cedar Blazek"<?php echo ($auditor == "Cedar Blazek" ? " selected":"");?> disabled>Cedar Blazek</option>
						</select>
						<?php if ($scheduledBySetBy){?><br><span class="auditScheduledBy" id="auditScheduledBySpan<?php echo $auditInforResults->MuniAudit_ID;?>"><?php echo $scheduledBySetBy;?></span><?php }?>
					</div>
					<!--
						Process moved to tied to Weatherization Verification (Efficiency Rebates)
					<div>
						Verified Date: <span id="verifiedStatus<?php echo $auditInforResults->MuniAudit_ID;?>"><?php if (!MySQLDate($verifiedByDate)){?><input type="text" class="date" value="<?php echo MySQLDate($verifiedByDate);?>" name="verified" data-auditid="<?php echo $auditInforResults->MuniAudit_ID;?>"><?php }else{ echo MySQLDate($verifiedByDate)." by ".$verifiedByName;}?></span>
					</div>
					-->
					<div>
						Notes:<br>
						<?php if ($auditInforResults->MuniAudit_ID){?>
							<textarea style="width:300px;" class="auditnotes" data-auditid="<?php echo $auditInforResults->MuniAudit_ID;?>"><?php echo $notes;?></textarea>
							<br><span class="smallNote" id="auditnotefooter<?php echo $auditInforResults->MuniAudit_ID;?>"><?php echo $notesLastUpdatedBy;?></span>
						<?php }else{ ?>
							(Notes available once Audit data has been entered)
						<?php }?>
					</div>
					<?php
					 if ($auditStatus <2){
						echo "Direct link for Auditor:<br><a href='".$CurrentServer."cetstaff/?linkPage=Muni&CustomerID=".$SelectedCustomerID."'>".$CurrentServer."cetstaff/?linkPage=Muni&CustomerID=".$SelectedCustomerID."</a>";
					 }
					?>
				</div>
			<?php
			}
		?>		
	</div>
	<br clear="all">
	<script>
		$(document).ready(function() {
			$(".date").datepicker({
				onSelect: function (date) {
					var $this = $(this),
						thisAuditId = $this.attr('data-auditid'),
						thisVal = date,
						thisCurrentStatus = "#verifiedStatus"+thisAuditId,
						data = {};
					
					data["action"] = "update_audit_verifieddate";
					data["auditId"] = thisAuditId;
					data["verifiedDate"] = thisVal;
					data["customerId"] = <?php echo $SelectedCustomerID;?>;
					$.ajax({
						url: "ApiCustomerData.php",
						type: "PUT",
						data: JSON.stringify(data),
						success: function(data){
							console.log(data);
							$(thisCurrentStatus).html(thisVal+" just set by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
						}
					});
					
				}
			});
			$(".date").css('width','80px');
			
			var auditDateClass = $(".auditDate");
			auditDateClass.width(80);
			auditDateClass.datepicker();
			var timer = null;
			var auditStatus = <?php echo json_encode($AuditStatusNameByID);?>;
			var auditnotesClass = $(".auditnotes"),
				auditcurrentStatusClass = $(".auditcurrentStatus"),
				auditeditCurrentStatusClass = $(".auditeditCurrentStatus"),
				auditcurrentStatusSelectPulldownClass = $(".auditcurrentStatusSelectPulldown"),
				auditorSelectPulldownClass = $(".auditorSelectPulldown");
				
			auditcurrentStatusSelectPulldownClass.on('change',function(){
				var $this = $(this),
					thisVal = $this.val(),
					thisText = $("option:selected",this).text(),
					thisAuditId = $this.attr('data-auditid'),
					thisCurrentStatus = "#auditcurrentStatus"+thisAuditId,
					thisCurrentStatusSelector = thisCurrentStatus+"Selector",
					thisAuditScheduledCanceledDate = "#auditScheduledCanceledDate"+thisAuditId,
					thisScheduledDate = "#scheduledDate"+thisAuditId,
					thisScheduledTime = "#scheduledTime"+thisAuditId,
					thisAuditor = "#auditor"+thisAuditId;
				
				if (thisText == "Cancelled"){
					$(thisAuditScheduledCanceledDate).html('Cancelled');
					$(thisScheduledDate).val('<?php echo $TodaysDate;?>');
					$(thisScheduledTime).val('');
					$(thisAuditor).val('Cancelled').change();
				}
					
				var StatusChange = {};
					StatusChange["action"] = "update_auditstatus";
					StatusChange["auditId"] = thisAuditId;
					StatusChange["currentStatus"] = thisVal;
					StatusChange["currentText"] = thisText;
					StatusChange["customerId"] = <?php echo $SelectedCustomerID;?>;
					data = StatusChange;
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						console.log(data);
						$(thisCurrentStatus).html(auditStatus[thisVal]+" just set by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
						$(thisCurrentStatus).toggle();
						$(thisCurrentStatusSelector).toggle();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						thisNote.val(message);
					}
				});
			});
			auditeditCurrentStatusClass.on('click',function(){
				var $this = $(this),
					thisEditId = $this.attr('data-editid'),
					currentStatus = "#"+thisEditId;
					currentStatusSelector = "#"+thisEditId+"Selector";
					$(currentStatus).toggle();
					$(currentStatusSelector).toggle();
			});
			auditnotesClass.on('keyup',function(){
				var thisNote = $(this);
				clearTimeout(timer);
				timer = setTimeout(function(){auditupdateComments(thisNote);}, 1000);
			});
			var auditupdateComments = function(thisNote){
				var thisNote = thisNote,
					thisVal = thisNote.val(),
					thisAuditId = thisNote.attr('data-auditid'),
					thisNoteFooter = "#auditnotefooter"+thisAuditId;
				
				var Comments = {};
					Comments["action"] = "update_auditnote";
					Comments["auditId"] = thisAuditId;
					Comments["notes"] = thisVal;
					data = Comments;
					console.log(data);
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						console.log(data);
						$(thisNoteFooter).html("notes just updated by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						thisNote.val(message);
					}
				});
			};
			auditorSelectPulldownClass.on('change',function(){
				var $this = $(this),
					thisVal = $this.val(),
					thisText = $("option:selected",this).text(),
					thisAuditId = $this.attr('data-auditid'),
					thisCustomerId = $this.attr('data-customerid'),
					thisScheduledDateID = "#"+$this.attr('data-scheduleddate'),
					thisScheduledDate = $(thisScheduledDateID).val(),
					thisScheduledTimeID = "#"+$this.attr('data-scheduledtime'),
					thisScheduledTime = $(thisScheduledTimeID).val(),
					thisAuditScheduledBySpan = "#auditScheduledBySpan"+thisAuditId;
					console.log(thisAuditId);
				if (thisScheduledDate){
					
					var StatusChange = {};
						StatusChange["action"] = "update_auditor";
						StatusChange["auditId"] = thisAuditId;
						StatusChange["customerId"] = thisCustomerId;
						StatusChange["auditor"] = thisVal;
						StatusChange["scheduledDate"] = thisScheduledDate;
						StatusChange["scheduledTime"] = thisScheduledTime;
						data = StatusChange;
						console.log(data);
					$.ajax({
						url: "ApiCustomerData.php",
						type: "PUT",
						data: JSON.stringify(data),
						success: function(data){
							$(thisAuditScheduledBySpan).html("Audit Scheduled and Auditor just entered by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
							console.log(data);
							/*
							$(thisCurrentStatus).html(auditStatus[thisVal]+" just set by <?php echo emailInitials($_SESSION['AdminEmail']);?>");
							$(thisCurrentStatus).toggle();
							$(thisCurrentStatusSelector).toggle();
							*/
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							console.log(message);
						}
					});
				}else{
					alert('You must have a date schedule before selecting the auditor');
					$this.val('');
				}
			});
			
		});
	</script>
<?php }//end if $SelectedCustomerID ?>