<?php 
	if ($nav=="auditForm"){
		if ($auditStatus){
			include_once('_customerAuditStatus.php');
		}
	}else{
		if ($auditStatus){
			include_once('_customerAuditStatus.php');
		}
		foreach ($rebateTypes as $rebateName=>$rebateObj){
			if ($CustomerByID[$SelectedCustomerID]->{$rebateObj}){$rebatesFound = true;}
		}
		if ($rebatesFound){
			include_once('_customerRebateStatus.php');
		}
	}
?>
<?php $SelectedRebateType = $_GET['RebateType'];?>
<?php if ($SelectedCustomerID || ($SelectedCustomerID && $nav=="auditForm" && $auditStatus)){?>
	<hr>
	<a href="#intakeForm" id="intakeButton" class="button-link noCustomer">Show Customer Details</a>
<?php }?>
<style>
	.state {width:40px !important;}
	.zip {width:80px !important;}
	.phone {width:150px !important;}
	.fax {width:150px !important;}
	.section {border:1pt solid black;padding:5px;margin:2px;}
	.subsection {padding-left:20px;}
	.commercial {display:none;}
	.intakeForm {display:<?php echo ($nav =="customer-intake" || $newCustomerName || ($nav == "auditForm" && !$auditStatus) ? "" : "none");?>;}
	.intakeOnly {display:<?php echo ($SelectedCustomerID ? "none" : "");?>;}
</style>
<div class="intakeForm">
	<form id="CustomerIntakeForm">
		<h3><?php echo ($newCustomerName ? "New " :"");?>Customer Intake Info<?php echo ($nav == "auditForm" ? " for Audit" : "");?></h3>
		Customer Type:<select name="customerType" id="customerType"><option value="residential"<?php echo ($CustomerByID[$SelectedCustomerID]->customerType == "residential" ? " selected" : "");?>>Residential</option><option value="commercial"<?php echo ($CustomerByID[$SelectedCustomerID]->customerType == "commercial" ? " selected" : "");?>>Commercial</option></select><br>
		<div class="section">
			Account Holder <bR>
				<div class="subsection">
					First Name: <input type="text" name="accountHolderFirstName" value="<?php echo ($newCustomerName ? $newCustomerFirstName : $CustomerByID[$SelectedCustomerID]->accountHolderFirstName);?>" required> 
					Last Name: <input type="text" name="accountHolderLastName" value="<?php echo ($newCustomerName ? $newCustomerLastName : $CustomerByID[$SelectedCustomerID]->accountHolderLastName);?>" required><br>
					Utility Name: <input type="text" name="utilityName" value="<?php echo ($CustomerByID[$SelectedCustomerID]->utilityName ? $CustomerByID[$SelectedCustomerID]->utilityName : "Westfield Gas & Electric");?>"><br>
					Utility Account Number: <input type="text" id="utilityAccountNumberSource" name="utilityAccountNumber<?php echo ($_GET['NewCustomerName'] ? "Source" : "");?>" value="<?php echo ($newCustomerAccountNumber ? $newCustomerAccountNumber : $CustomerByID[$SelectedCustomerID]->utilityAccountNumber);?>" required>
						<?php if (!$newCustomerAccountNumber && !$CustomerByID[$SelectedCustomerID]->utilityAccountNumber){?>
							<a href="#" id="unknownAccountNumber" class="button-link">Unknown At This Time</a>
						<?php }?>
				</div>
		</div>
		<div class="section">
			Address to be serviced / Equipment Installed<br>
				<div class="subsection">
	<div class="fifteen columns">
		<?php 
		/*
			$zipCodeDataResults = $muniProvider->getZipCodes();
			$zipCodeCollection = $zipCodeDataResults->collection;
			foreach ($zipCodeCollection as $result=>$record){
				$ZipCodeByID[$record->MuniZipCode_ID] = $record;
			}
		
			$SelectedCustomerIDZipCode = $CustomerByID[$SelectedCustomerID]->installedZip;
			include('_ZipCodesComboBox.php');
		*/
		?>
	</div>

					Owned Or Leased: <input type="radio" name="installedAddressOwnedOrLeased" value="Owned"<?php echo ($CustomerByID[$SelectedCustomerID]->installedAddressOwnedOrLeased == "Owned" ? " checked" : "");?>>Owned <input type="radio" name="installedAddressOwnedOrLeased" value="Leased"<?php echo ($CustomerByID[$SelectedCustomerID]->installedAddressOwnedOrLeased == "Leased" ? " checked" : "");?>>Leased<br>
					Street Address: <input type="text" id="installedAddress" name="installedAddress" class="source streetAddress" data-sourcetype="streetAddress" value="<?php echo $CustomerByID[$SelectedCustomerID]->installedAddress;?>"><br>
					City: <input type="text" id="installedCity" name="installedCity" class="source city" data-sourcetype="city" value="<?php echo ($CustomerByID[$SelectedCustomerID]->installedCity ? $CustomerByID[$SelectedCustomerID]->installedCity : "Westfield");?>"> 
					State: <input type="text" id="installedState" name="installedState" class="source state" data-sourcetype="state" value="<?php echo ($CustomerByID[$SelectedCustomerID]->installedState ? $CustomerByID[$SelectedCustomerID]->installedState : "MA");?>"> 
					Zip: <input type="text" id="installedZip" name="installedZip" class="source zip" data-sourcetype="zip" value="<?php echo ($CustomerByID[$SelectedCustomerID]->installedZip ? $CustomerByID[$SelectedCustomerID]->installedZip : "01085");?>"><br>
					Phone: <input type="text" id="installedPhone" name="installedPhone" class="source phone" data-sourcetype="phone" value="<?php echo ($newCustomerPhoneNumber ? $newCustomerPhoneNumber : $CustomerByID[$SelectedCustomerID]->installedPhone);?>">
<!--					
					Work Phone: <input type="text" id="installedWorkPhone" name="installedWorkPhone" class="source workphone" data-sourcetype="workphone" value="<?php echo $CustomerByID[$SelectedCustomerID]->installedWorkPhone;?>">
					Fax: <input type="text" id="installedFax" name="installedFax" class="source fax" data-sourcetype="fax" value="<?php echo $CustomerByID[$SelectedCustomerID]->installedFax;?>"><br>
-->
					Email: <input type="text" id="installedEmail" name="installedEmail" class="source email" data-sourcetype="email" value="<?php echo $CustomerByID[$SelectedCustomerID]->installedEmail;?>"><br>
				</div>
		</div>
		<div class="section">
			Rebate Made Payable To: <input type="text" name="rebateMadePayableTo" value="<?php echo $CustomerByID[$SelectedCustomerID]->rebateMadePayableTo;?>">
		</div>
		<div class="section">
			Mailing Address:<br>
				<div class="subsection">
					<span class="intakeOnly"><input type="checkbox" class="propertyOwnerCheck" data-addresstype="mailing">Different than Address to be serviced / Equipment Installed<Br></span>
					Street Address: <input type="text" id="mailingAddress" name="mailingAddress" class="streetAddress" value="<?php echo $CustomerByID[$SelectedCustomerID]->mailingAddress;?>"><br>
					City: <input type="text" id="mailingCity" name="mailingCity" class="city" value="<?php echo ($CustomerByID[$SelectedCustomerID]->mailingCity ? $CustomerByID[$SelectedCustomerID]->mailingCity : "Westfield");?>">
					State: <input type="text" id="mailingState" name="mailingState" class="state" value="<?php echo ($CustomerByID[$SelectedCustomerID]->mailingState ? $CustomerByID[$SelectedCustomerID]->mailingState : "MA");?>">
					Zip: <input type="text" id="mailingZip" name="mailingZip" class="zip" value="<?php echo ($CustomerByID[$SelectedCustomerID]->mailingZip ? $CustomerByID[$SelectedCustomerID]->mailingZip : "01085");?>">
				</div>
		</div>
		<div class="section">
			Property Owner: <br>
				<div class="subsection">
					First Name: <input type="text" name="ownerFirstName" value="<?php echo ($newCustomerName ? $newCustomerFirstName : $CustomerByID[$SelectedCustomerID]->ownerFirstName);?>"><br>
					Last Name: <input type="text" name="ownerLastName" value="<?php echo ($newCustomerName ? $newCustomerLastName : $CustomerByID[$SelectedCustomerID]->ownerLastName);?>"><br>
					TaxID: <input type="text" name="ownerTaxId" value="<?php echo $CustomerByID[$SelectedCustomerID]->ownerTaxId;?>"><br>
					<span class="intakeOnly"><input type="checkbox" class="propertyOwnerCheck" data-addresstype="owner">Different than Address to be serviced / Equipment Installed<br></span>
					Address: <input type="text" id="ownerAddress" name="ownerAddress" class="streetAddress" value="<?php echo $CustomerByID[$SelectedCustomerID]->ownerAddress;?>"><br>
					City: <input type="text" id="ownerCity" name="ownerCity" class="city" value="<?php echo ($CustomerByID[$SelectedCustomerID]->ownerCity ? $CustomerByID[$SelectedCustomerID]->ownerCity : "Westfield");?>">
					State: <input type="text" id="ownerState" name="ownerState" class="state" value="<?php echo ($CustomerByID[$SelectedCustomerID]->ownerState ? $CustomerByID[$SelectedCustomerID]->ownerState : "MA");?>">
					Zip: <input type="text" id="ownerZip" name="ownerZip" class="zip" value="<?php echo ($CustomerByID[$SelectedCustomerID]->ownerZip ? $CustomerByID[$SelectedCustomerID]->ownerZip : "01085");?>"><br>
					Phone: <input type="text" id="ownerPhone" name="ownerPhone" class="phone" value="<?php echo ($newCustomerPhoneNumber ? $newCustomerPhoneNumber : $CustomerByID[$SelectedCustomerID]->ownerPhone);?>">
<!--					Fax: <input type="text" id="ownerFax" name="ownerFax" class="fax" value="<?php echo $CustomerByID[$SelectedCustomerID]->ownerFax;?>"><br>-->
					Email: <input type="text" id="ownerEmail" name="ownerEmail" class="email" value="<?php echo $CustomerByID[$SelectedCustomerID]->ownerEmail;?>">
				</div>
		</div>
		<?php if ($thisIsAuditPage || in_array($SelectedCustomerID,$CustomerIDWithAuditInfo)){?>
		<div class="section" id="auditIntakeSection">
			<?php include_once('views/_auditForm_HeatingVariables.php');?>
			Audit Only: <a href="http://gis.vgsi.com/westfieldma/Search.aspx" target="WestfieldAssessor">Westfield Assessor Property Search</a><br>
				<div class="subsection">
				Floor Area (sq ft): <input type="text" name="auditFloorArea" value="<?php echo $CustomerByID[$SelectedCustomerID]->auditFloorArea;?>"><br>
				House Width: <input type="text" name="auditHouseWidth" value="<?php echo $CustomerByID[$SelectedCustomerID]->auditHouseWidth;?>"><br>
				House Length: <input type="text" name="auditHouseLength" value="<?php echo $CustomerByID[$SelectedCustomerID]->auditHouseLength;?>"><br>
				Conditioned Stories:
				<select name="auditConditionedStories">
					<option value="1"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "1" ? " selected" : "");?>>1</option> 
					<option value="1.25"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "1.25" ? " selected" : "");?>>1.25</option> 
					<option value="1.5"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "1.5" ? " selected" : "");?>>1.5</option> 
					<option value="1.75"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "1.75" ? " selected" : "");?>>1.75</option> 
					<option value="2"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "2" ? " selected" : "");?>>2</option> 
					<option value="2.25"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "2.25" ? " selected" : "");?>>2.25</option> 
					<option value="2.5"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "2.5" ? " selected" : "");?>>2.5</option> 
					<option value="2.75"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "2.75" ? " selected" : "");?>>2.75</option> 
					<option value="3"<?php echo ($CustomerByID[$SelectedCustomerID]->auditConditionedStories == "3" ? " selected" : "");?>>3</option> 
				</select>
				<br>
				Heat Equipment Type: 
				<select name="auditHeatingEquipmentType">
					<option value=""></option>
					<?php
						foreach ($heatingEquipmentType as $val=>$text){
							echo "<option value=\"".$val."\"".($val==$CustomerByID[$SelectedCustomerID]->auditHeatingEquipmentType ? " selected" : "").">".$val."</option>";	
						}
					?>
				</select> 
				<br clear="all">
				Heating Fuel Type: 
				<select name="auditHeatingFuel">
					<option value=""></option>
					<?php 
						foreach ($heatingFuel as $val=>$text){
							echo "<option value=\"".$val."\"".($val==$CustomerByID[$SelectedCustomerID]->auditHeatingFuel ? " selected" : "").">".$val."</option>";	
						}
					?>
				</select>
				</div>
		</div>
		<?php }//end thisIsAuditPage ?>
		<div class="section">
			Notes (not rebate specific): <br>
				<div class="subsection">
					<textarea name="customerNotes"><?php echo $CustomerByID[$SelectedCustomerID]->customerNotes;?></textarea>
				</div>
		</div>
		<div id="customerResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data&CustomerID=<?php echo $SelectedCustomerID;?>" id="cancel" class="button-link"<?php echo ($newCustomerName ? " style=\"display:none;\"" : "");?>>Cancel</a>
				<?php if ($nav=="auditForm"){?>
					<input type="hidden" name="auditStatus" value="<?php echo ($CustomerByID[$SelectedCustomerID]->auditStatus ? $CustomerByID[$SelectedCustomerID]->auditStatus : "1");?>">
				<?php }?>
				<?php if (!$SelectedCustomerID){?>
					<a href="save-intake-add" id="save-intake-add" class="button-link do-not-navigate"<?php echo ($newCustomerName && !$thisIsAuditPage ? " style=\"display:none;\"" : "");?>>+Add</a>
					<input type="hidden" name="action" value="intake_add">
				<?php }else{?>
					<a href="save-intake-update" id="save-intake-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="intake_update">
					<input type="hidden" name="id" value="<?php echo $SelectedCustomerID;?>">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
</div><!-- class intakeForm-->
<br clear="all">
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
	
		var classIntakeForm = $(".intakeForm"),
			customerResults = $('#customerResults');
			
		$("#intakeButton").on('click',function(){
			var $this = $(this);
				if ($this.html() == '<span class="ui-button-text">Show Customer Details</span>'){
					$this.html('<span class="ui-button-text">Hide Customer Details</span>');
					classIntakeForm.slideDown();
				}else{
					$this.html('<span class="ui-button-text">Show Customer Details</span>');
					classIntakeForm.slideUp();
				}
		});
			
		$(".propertyOwnerCheck").on('click',function(){
			var $this = $(this),
				thisAddressType = $this.attr('data-addresstype'),
				streetAddressId = "#"+thisAddressType+"Address",
				cityId = "#"+thisAddressType+"City",
				stateId = "#"+thisAddressType+"State",
				zipId = "#"+thisAddressType+"Zip",
				phoneId = "#"+thisAddressType+"Phone",
				faxId = "#"+thisAddressType+"Fax",
				emailId = "#"+thisAddressType+"Email";
				
			if ($this.prop('checked')){
				$(streetAddressId).val('');
				$(cityId).val('');
				$(stateId).val('');
				$(zipId).val('');
				if (thisAddressType == "owner"){
					$(phoneId).val('');
					$(faxId).val('');
					$(emailId).val('');
				}
			}
		});
		
		$(".source").on('keyup',function(){
			var $this = $(this),
				thisSourceType = $this.attr('data-sourcetype'),
				thisSourceTypeClass = "."+thisSourceType,
				thisVal = $this.val();
				console.log(thisSourceTypeClass);
				$(thisSourceTypeClass).val(thisVal);
		});
		
		
		 var formElement = $("#CustomerIntakeForm");
		 
		 formElement.validate({
			rules: {
				utilityAccountNumber<?php echo ($_GET['NewCustomerName'] ? "Source" : "");?>: "required",
				accountHolderFirstName: "required",
				accountHolderLastName: "required",
				ownerFirstName: "required",
				ownerLastName: "required"
			},
			messages: {
				utilityAccountNumber<?php echo ($_GET['NewCustomerName'] ? "Source" : "");?>: "If unknown use the button",
				accountHolderFirstName: "First Name",
				accountHolderLastName: "Last Name",
				ownerFirstName: "Owner First Name",
				ownerLastName: "Owner Last Name"
			}
		});
		$("#cancel").click(function(e){
			e.preventDefault();
			parent.history.back(); 
			return false;							   
		});
		
		$("#save-intake-update").click(function(e){

			customerResults.html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				//console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						//console.log(data);
						var resultText = '<?php echo ($nav == "auditForm" ? "Direct link for Energy Auditor: ".$CurrentServer."cetstaff/?linkPage=Muni&CustomerID=".$SelectedCustomerID : "Saved");?>';
						customerResults.html(resultText).addClass("alert").addClass("info");
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						customerResults.html(message).addClass("alert");
					}
				});
			}
		});
		$("#save-intake-add").click(function(e){

			customerResults.html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				//console.log(JSON.stringify(formElement.serializeObject()));
				console.log(JSON.stringify(formElement.serializeNonNullObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeNonNullObject()),
					success: function(data){
						//console.log(data);
						<?php if ($thisIsAuditPage){?>
							location.href='<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data&CustomerID='+data;
						<?php }else{?>
							<?php if ($newCustomerName){?>
								$("#customerId").val(data);
								$("#saveHidden").click();
							<?php }else{?>
								location.href='<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data&CustomerID='+data;
								customerResults.html("Saved").addClass("alert").addClass("info");
							<?php }?>
						<?php }?>
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						customerResults.html(message).addClass("alert");
					}
				});
			}else{
				alert('There are required Customer Intake Fields');
				$("#save-intake-add").show();
			}
		});
		//put utility in correct format of 123456-123456
		$("#utilityAccountNumberSource").on('keyup',function(){
			var $this = $(this),
				thisVal = $this.val();
			if (thisVal.length == 6){
				thisVal = thisVal+"-";
				$this.val(thisVal);
			}
			if (thisVal.indexOf('--')){
				thisVal = thisVal.replace('--','-');
				$this.val(thisVal);
			}
		});
	});
	$("#unknownAccountNumber").click(function(e){
		e.preventDefault();
		var UnknownAccountNumber = 'UnknownAtThisTime'+Date.now();
		$("#utilityAccountNumberSource").val(UnknownAccountNumber);
		$("#utilityAccountNumberHidden").val(UnknownAccountNumber);
	});
	
</script>