<?php
$wallCode = "Walls";
$wallType = array("Platform Frame","Platform Frame");//value in NEAT and text to be displayed
$wallStudSize = array("2 x 4","2 x 4");//value in NEAT and text to be displayed
$wallExteriorType = array("Wood","Wood");//value in NEAT and text to be displayed
$wallExposedTo = array("Outside","Outside (Ambient)");//value in NEAT and text to be displayed
$wallOrientation = array("North","North");//value in NEAT and text to be displayed
$wallMeasureNumber = 1;
$existingInsulationYes = array("Fiberglass Batts","Fiberglass Batts"); //value in NEAT and text to be displayed
$existingInsulationRValue = 12;
$existingInsulationNoValue = "None"; //value in NEAT will be a number
$existingInsulationValues = array("None"=>1,"Fiberglass Batts"=>5);
$addedInsulationYes = array("Blown Cellulose","Blown Cellulose"); //value in NEAT and text to be displayed
$addedInsulationNoValue = "None"; //value in NEAT will be a number
$addedInsulationValues = array("None"=>1,"Blown Cellulose"=>2);

$atticAddedInsulationDepthArray = array("Blown Cellulose"=>array(3,6,9,12),"Dense Pack Cellulose"=>array(4,6,9,12));

$atticUnfinishedCode = "UnfinishedAttic";
$atticUnfinishedTypes = array("Unfloored"=>1,"Floored"=>2,"Cathedral"=>3);//value in NEAT and text to be displayed
$atticUnfinishedJoistSpacing = 16;
$atticUnfinishedAreaSqft = $intakeHouseWidth*$intakeHouseLength;
$atticUnfinishedRoofColor = array("Normal","Normal or Weathered");//value in NEAT and text to be displayed
$atticUnfinishedExistingInsulation = array("None"=>1,"Blown Cellulose"=>2,"Blown Fiberglass"=>3,"Rockwool"=>4,"Fiberglass Batts"=>5,"Other"=>6);//value in NEAT and text to be displayed
$atticUnfinishedAddedInsulationMeasureNumber = 1;
$atticUnfinishedAddedInsulation = array("None"=>1,"Blown Cellulose"=>2,"Dense Pack Cellulose"=>3);//value in NEAT and text to be displayed

$atticFinishedKneewallCode = "AtticKneewall";
$atticFinishedKneewallFloorTypes = array("Unfloored"=>"Unfloored","Floored"=>"Floored");//value in NEAT and text to be displayed
$atticFinishedKneewallAreaSqft = 0;
$atticFinishedKneewallRoofColor = array("Normal","Normal or Weathered");//value in NEAT and text to be displayed
$atticFinishedKneewallExistingInsulation = array("None"=>1,"Blown Cellulose"=>2,"Blown Fiberglass"=>3,"Rockwool"=>4,"Fiberglass Batts"=>5,"Other"=>6);//value in NEAT and text to be displayed
$atticFinishedKneewallAddedInsulationMeasureNumber = 1;
$atticFinishedKneewallAddedInsulation = array("None"=>1,"Fiberglass Batts"=>2,"Blown Cellulose"=>3,"Rigid Foam Board"=>4,"Dense Pack Cellulose"=>5);//value in NEAT and text to be displayed

$atticFinishedSlopeCode = "AtticSlope";
$atticFinishedSlopeFloorTypes = array("Unfloored"=>"Unfloored","Floored"=>"Floored");//value in NEAT and text to be displayed
$atticFinishedSlopeAreaSqft = 0;
$atticFinishedSlopeRoofColor = array("Normal","Normal or Weathered");//value in NEAT and text to be displayed
$atticFinishedSlopeExistingInsulation = array("None"=>1,"Blown Cellulose"=>2,"Blown Fiberglass"=>3,"Rockwool"=>4,"Fiberglass Batts"=>5,"Other"=>6);//value in NEAT and text to be displayed
$atticFinishedSlopeAddedInsulationMeasureNumber = 1;
$atticFinishedSlopeAddedInsulation = array("None"=>1,"Blown Cellulose"=>2,"Dense Pack Cellulose"=>5);//value in NEAT and text to be displayed

$atticFinishedCapCode = "AtticCap";
$atticFinishedCapFloorTypes = array("Unfloored"=>"Unfloored","Floored"=>"Floored");//value in NEAT and text to be displayed
$atticFinishedCapAreaSqft = 0;
$atticFinishedCapRoofColor = array("Normal","Normal or Weathered");//value in NEAT and text to be displayed
$atticFinishedCapExistingInsulation = array("None"=>1,"Blown Cellulose"=>2,"Blown Fiberglass"=>3,"Rockwool"=>4,"Fiberglass Batts"=>5,"Other"=>6);//value in NEAT and text to be displayed
$atticFinishedCapAddedInsulationMeasureNumber = 2;
$atticFinishedCapAddedInsulation = array("None"=>1,"Blown Cellulose"=>2,"Dense Pack Cellulose"=>5);//value in NEAT and text to be displayed

$foundationCode = "Foundation";
$foundationTypes = array("Conditioned"=>1,"NonConditioned"=>2);//value in NEAT and text to be displayed
$foundationMeasureNumber = 1;
$foundationFloorAddedInsulationAddedType = array("None"=>1,"RigidFoamBoard"=>2,"FiberglassBatts"=>3);//value in NEAT and text to be displayed
$foundationSillAddedInsulationAddedType = array("None"=>1,"FiberglassBatts"=>2,"RigidFoamBoard"=>3);//value in NEAT and text to be displayed
$foundationWallAddedInsulationAddedType = array("None"=>1,"RigidFoamBoard"=>3);//value in NEAT and text to be displayed
$foundationWallHeightDefault = 8;
$foundationWallHeightExposedDefault = 25;

$heatingCode = "Heat";
$heatingSuppliedPercentage = 100;
$heatingPrimarySystem = "checked";//value in NEAT

include_once('views/_auditForm_HeatingVariables.php');

$heatingLocation = array("Heated Space"=>1,"Unconditioned Space"=>2,"Unintentionally Heated"=>3);//value in NEAT and text to be displayed

$heatingDetails["Forced Air Furnace"]["Oil"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Forced Air Furnace"]["Oil"]["OutputCapacity"] = 72;
$heatingDetails["Forced Air Furnace"]["Oil"]["SteadyStateEfficiency"] = 75;
$heatingDetails["Forced Air Furnace"]["Oil"]["Condition"] = "Fair";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Oil";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 3000;
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Forced Air Furnace"]["Oil"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Forced Air Furnace"]["Propane"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Forced Air Furnace"]["Propane"]["OutputCapacity"] = 72;
$heatingDetails["Forced Air Furnace"]["Propane"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Forced Air Furnace"]["Propane"]["Condition"] = "Fair";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Propane";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 2000;
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Forced Air Furnace"]["Propane"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Steam Boiler"]["Oil"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Steam Boiler"]["Oil"]["OutputCapacity"] = 88;
$heatingDetails["Steam Boiler"]["Oil"]["SteadyStateEfficiency"] = 75;
$heatingDetails["Steam Boiler"]["Oil"]["Condition"] = "Fair";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Oil";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 3000;
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Steam Boiler"]["Oil"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Steam Boiler"]["Propane"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Steam Boiler"]["Propane"]["OutputCapacity"] = 88;
$heatingDetails["Steam Boiler"]["Propane"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Steam Boiler"]["Propane"]["Condition"] = "Fair";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Propane";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 3000;
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Steam Boiler"]["Propane"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Hot Water Boiler"]["Oil"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Hot Water Boiler"]["Oil"]["OutputCapacity"] = 88;
$heatingDetails["Hot Water Boiler"]["Oil"]["SteadyStateEfficiency"] = 75;
$heatingDetails["Hot Water Boiler"]["Oil"]["Condition"] = "Fair";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Oil";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 3000;
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Hot Water Boiler"]["Oil"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Hot Water Boiler"]["Propane"]["InputUnits"] = "Gallons per Hour";
$heatingDetails["Hot Water Boiler"]["Propane"]["OutputCapacity"] = 88;
$heatingDetails["Hot Water Boiler"]["Propane"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Hot Water Boiler"]["Propane"]["Condition"] = "Fair";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Propane";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 3000;
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Hot Water Boiler"]["Propane"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;


$heatingDetails["Other"]["Oil"]["InputUnits"] = "";
$heatingDetails["Other"]["Oil"]["OutputCapacity"] = 5;
$heatingDetails["Other"]["Oil"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Other"]["Oil"]["Condition"] = "Fair";
$heatingDetails["Other"]["Oil"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Other"]["Oil"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Other"]["Oil"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Other"]["Oil"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Other"]["Oil"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Other"]["Propane"]["InputUnits"] = "";
$heatingDetails["Other"]["Propane"]["OutputCapacity"] = 5;
$heatingDetails["Other"]["Propane"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Other"]["Propane"]["Condition"] = "Fair";
$heatingDetails["Other"]["Propane"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Other"]["Propane"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Other"]["Propane"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Other"]["Propane"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Other"]["Propane"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Forced Air Furnace"]["Natural Gas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["OutputCapacity"] = 72;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Condition"] = "Fair";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["OutputCapacity"] = 72;
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Condition"] = "Fair";
/*
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 4300;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["SystemAFUE"] = 97;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["MaterialCost"] = 6000;
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
*/
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Forced Air Furnace"]["Natural Gas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Forced Air Furnace"]["NaturalGas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Steam Boiler"]["Natural Gas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Steam Boiler"]["Natural Gas"]["OutputCapacity"] = 88;
$heatingDetails["Steam Boiler"]["Natural Gas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Condition"] = "Fair";
$heatingDetails["Steam Boiler"]["NaturalGas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Steam Boiler"]["NaturalGas"]["OutputCapacity"] = 88;
$heatingDetails["Steam Boiler"]["NaturalGas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Steam Boiler"]["NaturalGas"]["Condition"] = "Fair";
/*
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 5800;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["SystemAFUE"] = 95;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["LaborCost"] = 2000;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["MaterialCost"] = 7000;
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
*/
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Steam Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;
$heatingDetails["Steam Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Steam Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Steam Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Steam Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Steam Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Hot Water Boiler"]["Natural Gas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["OutputCapacity"] = 88;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Condition"] = "Fair";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["InputUnits"] = "KBtu per Hour";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["OutputCapacity"] = 88;
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["SteadyStateEfficiency"] = 80;
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Condition"] = "Fair";
/*
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["SystemAFUE"] = 85;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["LaborCost"] = 1200;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["MaterialCost"] = 5800;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Standard Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["Fuel"] = "Natural Gas";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["SystemAFUE"] = 95;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["LaborCost"] = 2000;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["MaterialCost"] = 7000;
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["High Efficiency Replacement Mandatory"]["IncludeInSIR"] = 1;
*/
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Hot Water Boiler"]["Natural Gas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Hot Water Boiler"]["NaturalGas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Other"]["Natural Gas"]["InputUnits"] = "";
$heatingDetails["Other"]["Natural Gas"]["OutputCapacity"] = 5;
$heatingDetails["Other"]["Natural Gas"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Other"]["Natural Gas"]["Condition"] = "Fair";
$heatingDetails["Other"]["Natural Gas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Other"]["Natural Gas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Other"]["Natural Gas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Other"]["Natural Gas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Other"]["Natural Gas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;
$heatingDetails["Other"]["NaturalGas"]["InputUnits"] = "";
$heatingDetails["Other"]["NaturalGas"]["OutputCapacity"] = 5;
$heatingDetails["Other"]["NaturalGas"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Other"]["NaturalGas"]["Condition"] = "Fair";
$heatingDetails["Other"]["NaturalGas"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Other"]["NaturalGas"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Other"]["NaturalGas"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Other"]["NaturalGas"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Other"]["NaturalGas"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Forced Air Furnace"]["Other"]["InputUnits"] = "";
$heatingDetails["Forced Air Furnace"]["Other"]["OutputCapacity"] = 5;
$heatingDetails["Forced Air Furnace"]["Other"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Forced Air Furnace"]["Other"]["Condition"] = "Fair";
$heatingDetails["Forced Air Furnace"]["Other"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Forced Air Furnace"]["Other"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Forced Air Furnace"]["Other"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Forced Air Furnace"]["Other"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Forced Air Furnace"]["Other"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Steam Boiler"]["Other"]["InputUnits"] = "";
$heatingDetails["Steam Boiler"]["Other"]["OutputCapacity"] = 5;
$heatingDetails["Steam Boiler"]["Other"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Steam Boiler"]["Other"]["Condition"] = "Fair";
$heatingDetails["Steam Boiler"]["Other"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Steam Boiler"]["Other"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Steam Boiler"]["Other"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Steam Boiler"]["Other"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Steam Boiler"]["Other"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Hot Water Boiler"]["Other"]["InputUnits"] = "";
$heatingDetails["Hot Water Boiler"]["Other"]["OutputCapacity"] = 5;
$heatingDetails["Hot Water Boiler"]["Other"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Hot Water Boiler"]["Other"]["Condition"] = "Fair";
$heatingDetails["Hot Water Boiler"]["Other"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Hot Water Boiler"]["Other"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Hot Water Boiler"]["Other"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Hot Water Boiler"]["Other"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Hot Water Boiler"]["Other"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$heatingDetails["Other"]["Other"]["InputUnits"] = "";
$heatingDetails["Other"]["Other"]["OutputCapacity"] = 5;
$heatingDetails["Other"]["Other"]["SteadyStateEfficiency"] = 50;
$heatingDetails["Other"]["Other"]["Condition"] = "Fair";
$heatingDetails["Other"]["Other"]["Replacement"]["Evaluate None"]["Fuel"] = "";
$heatingDetails["Other"]["Other"]["Replacement"]["Evaluate None"]["SystemAFUE"] = "";
$heatingDetails["Other"]["Other"]["Replacement"]["Evaluate None"]["LaborCost"] = "";
$heatingDetails["Other"]["Other"]["Replacement"]["Evaluate None"]["MaterialCost"] = "";
$heatingDetails["Other"]["Other"]["Replacement"]["Evaluate None"]["IncludeInSIR"] = 0;

$ductAirSealingHours = 8;
$ductAirSealingCostPerHour = 84.32;
$ductAirSealingCosts = bcmul($ductAirSealingHours,$ductAirSealingCostPerHour,2);

$boilerPlateEnergyItems = array("Air Sealing","Insulation","Heating System Oil or Propane",);
$boilerPlateRebateItems = array("High Efficient Gas Fired Heating systems","High Efficient Gas Fired Water Heating systems","Central AC system","Ductless Mini Split","Heat Pump water heater","Clothes Washer","Refrigerator","Freezer","Dishwasher","Room Air Conditioner","Dehumidifier","Air Purifier","Pool Pump");

?>