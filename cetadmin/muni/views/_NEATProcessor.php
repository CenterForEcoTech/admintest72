<?php
$TransmitNEAT = $newRecord->TransmitNEAT;
$SelectedCustomerID = $newRecord->SelectedCustomerID;
$AuditDetailsForNEAT = (array)$newRecord->AuditDetailsForNEAT;

error_reporting(1);
	$dbName = $_SERVER["DOCUMENT_ROOT"]."admintest/cetadmin/muni/data/wa8-9.mdb";
	$dbName = "L:\\GHS\\WeatherAssistantProgram\\wa8-9.mdb";
	$db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$dateCreated = date("m/d/Y g:i:s A");
$fuelType = array("Natural Gas"=>1,"Oil"=>2,"Electricity"=>3,"Propane"=>4,"Wood"=>5,"Coal"=>6,"Kerosene"=>7,"Other"=>8);
$auditorContactID = array("Matt Zarotny"=>77966630,"Cedar Blazek"=>1453304561,"Lisa Kohler"=>1441819678,"CJ Hanley"=>1473882663,"Ben Coe"=>1510600801,"Angel Ortiz"=>1512576882);
$existingInsulationValues = array("None"=>1,"Fiberglass Batts"=>5);
$heatingUnitsID = array("kBtu per Hour"=>2,"KBtu per Hour"=>2,"Gallons per Hour"=>3);
$heatingLocation = array("Heated Space"=>1,"HeatedSpace"=>1,"Unconditioned Space"=>2,"Unintentionally Heated"=>3);//value in NEAT and text to be displayed
$equipmentConditionID = array("Fair"=>2);
$heatingRetroStatusID = array("Evaluate None"=>10,"Standard Efficiency Replacement Mandatory"=>4,"High Efficiency Replacement Mandatory"=>5);
$heatingEquipmentType = array("Forced Air Furnace"=>3,"Steam Boiler"=>4,"Steam"=>4,"Hot Water Boiler"=>5,"Hot Water"=>5,"Other"=>1);//value in NEAT and text to be displayed
	//"Gravity Air Furnace"=>2,"Fixed Electric Resistance"=>6,"Portable Electric Resistance"=>7,"Heat Pump"=>8,"Vented Space Heater"=>10,"Unvented Space Heater"=>9
$heatingFuel = array("NaturalGas"=>1,"Natural Gas"=>1,"Oil"=>2,"Propane"=>4,"Other"=>8);	//value in NEAT and text to be displayed
	//"Electric"=>3, "Wood"=>5, "Coal"=>6, "Kerosene"=>7


//First Check to see if client is already in the system
$SIRData["status"] = "checking for clientRecord";
$SIRData["statuss"][] = "checking for clientRecord";
$sql  = "SELECT ClientID FROM tblClient WHERE ClientRecordName='".$AuditDetailsForNEAT["ClientInfo_ClientID"]."'";
$result = $db->query($sql);
while ($row = $result->fetch()) {
	$oldClientID = $row["ClientID"];
}
$SIRData["status"] = "clientID = ".$oldClientID;
$SIRData["statuss"][] = "clientID = ".$oldClientID;
if ($oldClientID){
	$SIRData["status"] = "Client ID Found and Retrieving Audit";
	$SIRData["statuss"][] = "Client ID Found and Retrieving Audit";
	//Get JobID
	$sql  = "SELECT JobID FROM tblNJob WHERE ClientID=".$oldClientID;
	$result = $db->query($sql);
	while ($row = $result->fetch()) {
		$oldJobID = $row["JobID"];
	}
	if ($oldJobID){
		$SIRData["status"] = "Client Found and Retrieving audit data";
		$SIRData["statuss"][] = "Client Found and Retrieving audit data";
		//GET SIR values
		$sql = "SELECT `RunID` FROM tblNResult WHERE `JobID` = ".$oldJobID;
		$result = $db->query($sql);
		while ($row = $result->fetch()) {
			$RunID = $row["RunID"];
		}
		//echo "Using data from NEAT for ClientID:".$oldClientID.", JobID:".$oldJobID.", AuditRunID: ".$RunID;
		if ($RunID){
			$SIRData["status"] = "Retrieving Audit Scores";
			$SIRData["statuss"][] = "Retrieving Audit Scores";
			//Get Material Quantity
			$sql = "SELECT * FROM tblNResultMaterial WHERE `RunID` = ".$RunID;
			$result = $db->query($sql);
			while ($row = $result->fetch()) {
				$SIRMaterials[$row["RunID"]][$row["Material"]." - ".$row["Type"]]["Quantity"] = $row["Quantity"];
				$SIRMaterials[$row["RunID"]][$row["Material"]." - ".$row["Type"]]["Units"] = $row["Units"];
			}
			//Get MeasureMaterial
			$sql = "SELECT * FROM tblNResultMeasureMaterial WHERE `RunID` = ".$RunID;
			$result = $db->query($sql);
			while ($row = $result->fetch()) {
				$SIRMeasureMaterials[$row["RunID"]][$row["Index"]]["Measure"] = $row["Description"];
				$SIRMeasureMaterials[$row["RunID"]][$row["Index"]]["Quantity"] = $SIRMaterials[$row["RunID"]][$row["Description"]]["Quantity"];
				$SIRMeasureMaterials[$row["RunID"]][$row["Index"]]["Units"] = $SIRMaterials[$row["RunID"]][$row["Description"]]["Units"];
			}

			
			$sql = "SELECT * FROM tblNResultSIR WHERE `RunID` = ".$RunID;
			$result = $db->query($sql);
			while ($row = $result->fetch()) {
				$SIRResults[] = $row;
			}
			$SIRData["status"] = "Audit Scores Received";
			$SIRData["statuss"][] = "Audit Scores Received";
			$SIRData["SIRMeasureMaterials"] = $SIRMeasureMaterials;
			$SIRData["SIRResults"] = $SIRResults;
			$SIRData["AuditCount"] = 1;
			$SIRData["Link"] = '<a href="?nav=auditForm&CustomerID='.$SelectedCustomerID.'&TransmitNEAT=true&setTab=SIRResults" class="button-link">Refresh SIR Score from NEAT</a>';
		}else{
			$SIRData["AuditCount"] = 0;
			$SIRData["Link"] = 'No audits found. Open Up NEAT, click Site Built (NEAT) button, and select by Client Name: '.$AuditDetailsForNEAT["Contacts_NameDetailsLast"].', '.$AuditDetailsForNEAT["Contacts_NameDetailsFirst"].' and then click the button for Run Audit.<br>
				After the Audit Runs, use the following button to pull the SIR results into the report:<Br>'.'<a href="?nav=auditForm&CustomerID='.$SelectedCustomerID.'&TransmitNEAT=true&setTab=SIRResults" class="button-link">Retrieve SIR Score from NEAT</a>';
		}
	}else{
		$SIRData["AuditCount"] = 0;
		$SIRData["Link"] = 'No audits found. Open Up NEAT, click Site Built (NEAT) button, and select by Client Name: '.$AuditDetailsForNEAT["Contacts_NameDetailsLast"].', '.$AuditDetailsForNEAT["Contacts_NameDetailsFirst"].' and then click the button for Run Audit.<br>
			After the Audit Runs, use the following button to pull the SIR results into the report:<Br>'.'<a href="?nav=auditForm&CustomerID='.$SelectedCustomerID.'&TransmitNEAT=true&setTab=SIRResults" class="button-link">Retrieve SIR Score from NEAT</a>';
	}
	
}else{
	$SIRData["status"] = "Client Data Not Yet Imported to NEAT";
	$SIRData["Link"] = '<a href="?nav=auditForm&CustomerID='.$SelectedCustomerID.'&TransmitNEAT=true&setTab=SIRResults" class="button-link">Import into NEAT</a>';
	$SIRData["statuss"][] = "No Client Found";
	if ($TransmitNEAT){
		$SIRData["status"] = "Adding New Client";
		$SIRData["statuss"][] = "Adding New Client";
		$sql = "INSERT INTO tblClient (
			AgencyName,AgencyState,ClientRecordName,OtherIDNum,AuditTypeID,
			DwellingTypeID,DwellingOwnershipID,NumUnits,Address,City,
			State,ZipCode,NumElderly,NumDisabled,NumChildren,
			NumNative,PrimaryHeatFuelTypeID,LibraryID,LanguageID,Created,
			LastEdited,WhoCreated,WhoEdited
			) 
			VALUES (
			 ?,?,?,?,?,
			 ?,?,?,?,?,
			 ?,?,?,?,?,
			 ?,?,?,?,?,
			 ?,?,?
			)";
		 $statement = $db->prepare($sql);
		 $params = array(
				"The Center for Eco Technology","MA",$AuditDetailsForNEAT["ClientInfo_ClientID"],$AuditDetailsForNEAT["ClientInfo_AltClientID"],1,
				1,1,1,$AuditDetailsForNEAT["ClientInfo_Address"],$AuditDetailsForNEAT["ClientInfo_City"],
				$AuditDetailsForNEAT["ClientInfo_State"],$AuditDetailsForNEAT["ClientInfo_Zip"],0,0,0,
				0,$fuelType[$AuditDetailsForNEAT["ClientInfo_DwellingPrimaryHeatingFuel"]],1313093956,1,$dateCreated,
				$dateCreated,"CET","CET"
			);
		$statement->execute($params);

		$SIRData["statuss"][] = "Getting ClientID";
		$sql  = "SELECT ClientID FROM tblClient WHERE ClientRecordName='".$AuditDetailsForNEAT["ClientInfo_ClientID"]."'";
		$result = $db->query($sql);
		while ($row = $result->fetch()) {
			$newClientID = $row["ClientID"];
		}

		$SIRData["statuss"][] = "Adding New Client Contact";
		$sql = "INSERT INTO `tblClientContact` (
				`ClientID`,`ContactName`,`TypeID`,`FirstName`,`LastName`,
				`Address`,`City`,`State`,`ZipCode`,`WorkPhone`,
				`Email`
			) VALUES (
				?,?,?,?,?,
				?,?,?,?,?,
				?
			)";
		$params = array(
			(int)$newClientID,$AuditDetailsForNEAT["Contacts_NameDetailsLast"].", ".$AuditDetailsForNEAT["Contacts_NameDetailsFirst"],1,$AuditDetailsForNEAT["Contacts_NameDetailsFirst"],$AuditDetailsForNEAT["Contacts_NameDetailsLast"],
			$AuditDetailsForNEAT["Contacts_Address"],$AuditDetailsForNEAT["Contacts_City"],$AuditDetailsForNEAT["Contacts_State"],$AuditDetailsForNEAT["Contacts_Zip"],$AuditDetailsForNEAT["Contacts_Workphone"],
			$AuditDetailsForNEAT["Contacts_Email"]
		);
		$SIRData["sql"] = $sql;
		$SIRData["params"] = $params;
		$statement = $db->prepare($sql);
		$statement->execute($params);

		$SIRData["statuss"][] = "Adding New Audit";
		$sql = "INSERT INTO `tblNJob` (
				`ClientID`,`JobName`,`LibraryID`,`LibFuelID`,`SupplyID`,
				`AssignedToID`,`ConditionedStories`,`FloorArea`,`WeatherFile`,`Created`,
				`LastEdited`,`WhoCreated`,`WhoEdited`
			) VALUES (
				?,?,?,?,?,
				?,?,?,?,?,
				?,?,?
			)";
		$params = array(
			(int)$newClientID,$AuditDetailsForNEAT["AuditInfo_AuditName"],1313093956,175188213,798450043,
			$auditorContactID[$AuditDetailsForNEAT["AuditInfo_Auditor"]],(int)$AuditDetailsForNEAT["AuditInfo_ConditionedStories"],(int)filter_var($AuditDetailsForNEAT["AuditInfo_FloorArea"], FILTER_SANITIZE_NUMBER_INT),"BOSTONMA.WX",$dateCreated,
			$dateCreated,"CET","CET"
		);
		$statement = $db->prepare($sql);
		$statement->execute($params);

		$SIRData["statuss"][] = "Getting Audit JobID";
		$sql  = "SELECT JobID FROM tblNJob WHERE ClientID=".$newClientID;
		$result = $db->query($sql);
		while ($row = $result->fetch()) {
			$newJobID = $row["JobID"];
		}
		$SIRData["statuss"][] ="Client and Audit Added";
		$SIRData["statusDetails"] ="ClientID = ".$newClientID." added and JobID ".$newJobID." added";
		//Shell Wall information
		$sql = "INSERT INTO `tblNWall` (
				`JobID`,`WallCode`,`StudSizeID`,`OrientationID`,`WallExposureID`,
				`WallExteriorID`,`WallLoadTypeID`,`Area`,`WallExistInsulationID`,`RValue`,
				`WallAddInsulationID`,`MeasureNumber`
			) VALUES (
				?,?,?,?,?,
				?,?,?,?,?,
				?,?
			)";
		$params = array(
			$newJobID,$AuditDetailsForNEAT["Shell_WallCode"],3,1,1,
			1,2,$AuditDetailsForNEAT["Shell_GrossArea"],$existingInsulationValues[$AuditDetailsForNEAT["Shell_ExistingInsulationType"]],$AuditDetailsForNEAT["Shell_ExistingInsulationRValue"],
			$addedInsulationValues[$AuditDetailsForNEAT["Shell_AddedInsulationType"]],$AuditDetailsForNEAT["Shell_MeasureNumber"]
		);
		$statement = $db->prepare($sql);
		$statement->execute($params);
		$SIRData["Results"][] = "Wall added";

		//Shell UnfinishedAttic
		if ($AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationMeasureNumber"]){
			$sql = "INSERT INTO `tblNAttic` (
					`JobID`,`AtticCode`,`AtticTypeID`,`JoistSpacing`,`Area`,
					`AtticExistInslID`,`ExistDepth`,`AtticAddInslID`,`MaxDepth`,`MeasureNumber`,
					`RoofColorID`
				) VALUES (
					?,?,?,?,?,
					?,?,?,?,?,
					?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["UnfinishedAttics_AtticCode"],$atticUnfinishedTypes[$AuditDetailsForNEAT["UnfinishedAttics_AtticType"]],$AuditDetailsForNEAT["UnfinishedAttics_JoistSpacing"],$AuditDetailsForNEAT["UnfinishedAttics_Area"],
				$atticUnfinishedExistingInsulation[$AuditDetailsForNEAT["UnfinishedAttics_ExistingInsulationType"]],$AuditDetailsForNEAT["UnfinishedAttics_ExistingInsulationDepth"],$atticUnfinishedAddedInsulation[$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationType"]],$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationDepth"],$AuditDetailsForNEAT["UnfinishedAttics_AddedInsulationMeasureNumber"],
				2
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["Results"][] = "Unfinished Attic added";
		}

		//Shell FinishedAttic Kneewall
		if ($AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationMeasureNumber"]){
			$sql = "INSERT INTO `tblNFinAttic` (
					`JobID`,`AtticCode`,`AtticTypeID`,`Area`,`AtticExistInslID`,
					`ExistDepth`,`KneewallAddInslID`,`MaxDepth`,`MeasureNumber`,`RoofColorID`
				) VALUES (
					?,?,?,?,?,
					?,?,?,?,?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["FinishedAttics_KneeWallAtticCode"],3,$AuditDetailsForNEAT["FinishedAttics_KneeWallAtticArea"],$atticFinishedKneewallExistingInsulation[$AuditDetailsForNEAT["FinishedAttics_KneeWallExistingInsulationType"]],
				$AuditDetailsForNEAT["FinishedAttics_KneeWallExistingInsulationDepth"],$atticFinishedKneewallAddedInsulation[$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationType"]],$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationDepth"],$AuditDetailsForNEAT["FinishedAttics_KneeWallAddedInsulationMeasureNumber"],2
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["Results"][] = "Kneewall Attic added";
		}
		//Shell FinishedAttic Slope
		if ($AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationMeasureNumber"]){
			$sql = "INSERT INTO `tblNFinAttic` (
					`JobID`,`AtticCode`,`AtticTypeID`,`Area`,`AtticExistInslID`,
					`ExistDepth`,`AtticAddInslID`,`MaxDepth`,`MeasureNumber`,`RoofColorID`
				) VALUES (
					?,?,?,?,?,
					?,?,?,?,?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["FinishedAttics_SlopeAtticCode"],4,$AuditDetailsForNEAT["FinishedAttics_SlopeAtticArea"],$atticFinishedSlopeExistingInsulation[$AuditDetailsForNEAT["FinishedAttics_SlopeExistingInsulationType"]],
				$AuditDetailsForNEAT["FinishedAttics_SlopeExistingInsulationDepth"],$atticFinishedSlopeAddedInsulation[$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationType"]],$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationDepth"],$AuditDetailsForNEAT["FinishedAttics_SlopeAddedInsulationMeasureNumber"],2
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["Results"][] = "Slope Attic added";
		}

		//Shell FinishedAttic Cap
		if ($AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationMeasureNumber"]){
			$sql = "INSERT INTO `tblNFinAttic` (
					`JobID`,`AtticCode`,`AtticTypeID`,`Area`,`AtticExistInslID`,
					`ExistDepth`,`AtticAddInslID`,`MaxDepth`,`MeasureNumber`,`RoofColorID`
				) VALUES (
					?,?,?,?,?,
					?,?,?,?,?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["FinishedAttics_CapAtticCode"],4,$AuditDetailsForNEAT["FinishedAttics_CapAtticArea"],$atticFinishedCapExistingInsulation[$AuditDetailsForNEAT["FinishedAttics_CapExistingInsulationType"]],
				$AuditDetailsForNEAT["FinishedAttics_CapExistingInsulationDepth"],$atticFinishedCapAddedInsulation[$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationType"]],$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationDepth"],$AuditDetailsForNEAT["FinishedAttics_CapAddedInsulationMeasureNumber"],2
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["Results"][] = "Cap Attic added";
		}


		//Shell Foundation
		if ($AuditDetailsForNEAT["Foundations_MeasureNumber"]){
			$sql = "INSERT INTO `tblNFoundation` (
					`JobID`,`FoundationCode`,`FoundationTypeID`,`Area`,`Perimeter`,
					`CeilingRValue`,`SillPerimeter`,`FloorJoistHeight`,`WallHeight`,`WallExposed`,
					`WallRValue`,`AddSillInsulID`,`AddFloorInsulID`,`AddFoundationlInsulID`,`MeasureNumber`
				) VALUES (
					?,?,?,?,?,
					?,?,?,?,?,
					?,?,?,?,?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["Foundations_FoundationCode"],$foundationTypes[$AuditDetailsForNEAT["Foundations_FoundationType"]],$AuditDetailsForNEAT["Foundations_FloorArea"],$AuditDetailsForNEAT["Foundations_FoundationWallPerimeter"],
				$AuditDetailsForNEAT["Foundations_FloorExistingRValue"],$AuditDetailsForNEAT["Foundations_SillPerimeterToInsulate"],$AuditDetailsForNEAT["Foundations_SillJoistSize"],$AuditDetailsForNEAT["Foundations_FoundationWallHeight"],$AuditDetailsForNEAT["Foundations_FoundationWallHeightExposed"],
				$AuditDetailsForNEAT["Foundations_FoundationWallExistingInsulationRValue"],$foundationSillAddedInsulationAddedType[$AuditDetailsForNEAT["Foundations_SillAddedInsulationType"]],$foundationFloorAddedInsulationAddedType[$AuditDetailsForNEAT["Foundations_FloorAddedInsulationType"]],1,$AuditDetailsForNEAT["Foundations_MeasureNumber"]
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["Results"][] = "Foundation added";
		}

		//Heat
			$sql = "INSERT INTO `tblNHeating` (
				`JobID`,`SystemCode`,`TypeID`,`HeatingEquipTypeID`,`FuelTypeID`,
				`HeatingEquipLocationID`,`PercentSupplied`,`EfficiencyUnitsID`,`HeatingUnitsID`,
				`OutputCapacity`,`HeatingEquipConditionID`,`Primary`,`SSEfficiency`,
				`IncludeInSIR`,`HeatingRetroStatusID`,
				`RetroAFUEStandardEff`,`RetroAFUEHighEff`,`RetroFuelTypeID`,`LaborCostStandardEff`,`LaborCostHighEff`,
				`MaterialCostStandardEff`,`MaterialCostHighEff`
				) VALUES (
				?,?,?,?,?,
				?,?,?,?,
				?,?,?,?,
				?,?,
				?,?,?,?,?,
				?,?
			)";
		$RetroAFUEStandard = NULL;
		$RetroAFUEHigh = NULL;
		$LaborCostStandard = NULL;
		$LaborCostHigh = NULL;
		$MaterialCostStandard = NULL;
		$MaterialCostHigh = NULL;
		if ($heatingRetroStatusID[$AuditDetailsForNEAT["Heating_ReplacementOptions"]] == 4){
			$RetroAFUEStandard = $AuditDetailsForNEAT["Heating_ReplacementSystemAFUE"];
			$LaborCostStandard = $AuditDetailsForNEAT["Heating_ReplacementLaborCost"];
			$MaterialCostStandard = $AuditDetailsForNEAT["Heating_ReplacementMaterialCost"];
		}
		if ($heatingRetroStatusID[$AuditDetailsForNEAT["Heating_ReplacementOptions"]] == 5){
			$RetroAFUEHigh = $AuditDetailsForNEAT["Heating_ReplacementSystemAFUE"];
			$LaborCostHigh = $AuditDetailsForNEAT["Heating_ReplacementLaborCost"];
			$MaterialCostHigh = $AuditDetailsForNEAT["Heating_ReplacementMaterialCost"];
		}
			$params = array(
				$newJobID,$AuditDetailsForNEAT["Heating_SystemCode"],0,$heatingEquipmentType[$AuditDetailsForNEAT["Heating_EquipmentType"]],$heatingFuel[$AuditDetailsForNEAT["Heating_Fuel"]],
				$heatingLocation[$AuditDetailsForNEAT["Heating_Location"]],$AuditDetailsForNEAT["Heating_HeatSupplied"],0,$heatingUnitsID[$AuditDetailsForNEAT["Heating_InputUnits"]],
				$AuditDetailsForNEAT["Heating_OutputCapacity"],$equipmentConditionID[$AuditDetailsForNEAT["Heating_Condition"]],TRUE,$AuditDetailsForNEAT["Heating_SteadyStateEfficiency"],
				$AuditDetailsForNEAT["Heating_ReplacementIncludeInSIR"],$heatingRetroStatusID[$AuditDetailsForNEAT["Heating_ReplacementOptions"]],
				$RetroAFUEStandard,$RetroAFUEHigh,$heatingFuel[$AuditDetailsForNEAT["Heating_ReplacementFuel"]],$LaborCostStandard,$LaborCostHigh,
				$MaterialCostStandard,$MaterialCostHigh
			);
			$paramsActuals = array(
				$newJobID,$AuditDetailsForNEAT["Heating_SystemCode"],0,$AuditDetailsForNEAT["Heating_EquipmentType"],$AuditDetailsForNEAT["Heating_Fuel"],
				$AuditDetailsForNEAT["Heating_Location"],$AuditDetailsForNEAT["Heating_HeatSupplied"],0,$AuditDetailsForNEAT["Heating_InputUnits"],
				$AuditDetailsForNEAT["Heating_OutputCapacity"],$AuditDetailsForNEAT["Heating_Condition"],TRUE,$AuditDetailsForNEAT["Heating_SteadyStateEfficiency"],
				$AuditDetailsForNEAT["Heating_ReplacementIncludeInSIR"],$AuditDetailsForNEAT["Heating_ReplacementOptions"],
				$RetroAFUEStandard,$RetroAFUEHigh,$AuditDetailsForNEAT["Heating_ReplacementFuel"],$LaborCostStandard,$LaborCostHigh,
				$MaterialCostStandard,$MaterialCostHigh
			);

			$statement = $db->prepare($sql);
			$SIRData["sql"] = $sql;
			$SIRData["params"] = $params;
			$SIRData["paramsActuals"] = $paramsActuals;
			$statement->execute($params);
			$SIRData["execute"] = $statement;
			$SIRData["Results"][] = "Heating added";

		//Ducts and Infiltration	
			$sql = "INSERT INTO tblNDuctandInfil (
					`JobID`,`AirLeakRedCost`,`PreInfCfm`,`PreInfPa`,`PostInfCfm`,`PostInfPa`
				) VALUES (
					?,?,?,?,?,?
				)";
			$params = array(
				$newJobID,$AuditDetailsForNEAT["Ducts_InfiltrationCosts"],$AuditDetailsForNEAT["Ducts_AirLeakageRateBefore"],$AuditDetailsForNEAT["Ducts_AirPressureBefore"],$AuditDetailsForNEAT["Ducts_AirLeakageRateAfter"],$AuditDetailsForNEAT["Ducts_AirPressureAfter"]
			);
			$statement = $db->prepare($sql);
			$statement->execute($params);
			$SIRData["status"] = "Client and Audit Added";
			$SIRData["statuss"][] = "Client and Audit Added";
			$SIRData["Results"][] = "Air Sealing Added";
			$SIRData["Results"][] = "Now Open Up NEAT, click Site Built (NEAT) button, and select by Client Name: ".$AuditDetailsForNEAT["Contacts_NameDetailsLast"].", ".$AuditDetailsForNEAT["Contacts_NameDetailsFirst"]." and then click the button for Run Audit.<br>
				After the Audit Runs, use the following button to pull the SIR results into the report:<Br>";
			$SIRData["Link"] = '<a href="?nav=auditForm&CustomerID='.$SelectedCustomerID.'&TransmitNEAT=true&setTab=SIRResults" class="button-link">Retrieve SIR Score from NEAT</a>';
	}//end if TransmitNEAT
}//end if not OldClientID	
?>