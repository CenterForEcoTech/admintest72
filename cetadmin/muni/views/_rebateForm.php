<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<center>Westfield Gas & Electric</center>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="seven columns">
			<u>Eligible Measure(s)</u><br>
			<b>Blower Door Test & Air Sealing</b><br>
			<b>Energy Star Rated Heating System</b><br>
			(85% AFUE or greater)<br>
			<b>Insulation</b><br>
			<br>
			<b>Note:</b> Customers that implement more than one eligible measure listed above based on the highest SIR in their Audit Report are eligible for a maximum of $900/customer/year
		</div>
		<div class="five columns">
			<u>Rebate</u><br>
			50%, up to $300<br>
			50%, up to $300<br>
			<br>
			50%, up to $300<br>
		</div>
	</div>
	<br clear="all">
	<div class="row">
		<div class="twelve columns">
			<b>Eligible Projects</b><Br>
			In general, the most cost-effective audit recommendations should be installed first, excluding lighting. Renters should seek approval from their landlord before installing any measures.
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			<b>How to Apply</b><br>
			<ul>
				<ul>
					<li>Please contact the HELPS toll-free line at: 1-888-333-7525 to schedule a Free Home Energy Audit, then after measure(s) installation contact HELPS to schedule post installation inspection. The inspector will conduct a site visit and complete the required inspection form. The inspection must be performed before submitting a rebate application.
					<li>Complete and submit this application here with the signed inspection form and invoice attached. When submitting the application, <b>please attach originals of all dated receipts/work orders that document the installation. Submission must be postmarked by 1/31/16</b>. 
					<br>
					<Br>
					These receipts and/or work orders should include the name, license number, address, and phone number of the contractor that completed the installation. You may wish to retain a copy of all documents for your records. 
					<br>
					<Br>
					If you have any questions, or if you would like assistance in completing this form, call our toll free number at 1-888-333-7525.
					<br>
				</ul>
				
			</ul>
		</div>
	</div>
	<hr>
</div>
<form id="onlineRebateForm">
	<div class="row">
		<div class="twelve columns">
			1.) Customer Information
		</div>
	</div>
	<div class="row">
		<div class="six columns">
			Account Holder First Name<span style="color: #FF0000;">*</span><br>
			<input name="accountHolderFirstName" type="text" value="">
		</div>
		<div class="six columns">
			Account Holder Last Name<span style="color: #FF0000;">*</span><br>
			<input name="accountHolderLastName" type="text" value="">
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			Installed Address<span style="color: #FF0000;">*</span><br>
			<input name="installedAddress" type="text" value="" style="width:680px;">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			City<span style="color: #FF0000;">*</span><br>
			<input name="installedCity" type="text" value="">
		</div>
		<div class="four columns">
			State<span style="color: #FF0000;">*</span><br>
			<input name="installedState" type="text" value="">
		</div>
		<div class="four columns">
			Zip<span style="color: #FF0000;">*</span><br>
			<input name="installedZip" type="text" value="">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Phone Number<span style="color: #FF0000;">*</span><br>
			<input name="installedPhone" type="text" value="">
		</div>
		<div class="four columns">
			Fax Number<span style="color: #FF0000;">*</span><br>
			<input name="installedFax" type="text" value="">
		</div>
		<div class="four columns">
			Email<span style="color: #FF0000;">*</span><br>
			<input name="installedEmail" type="text" value="">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Name of Municipal Utility<span style="color: #FF0000;">*</span><br>
			<input name="utilityName" type="text" value="">
		</div>
		<div class="four columns">
			Utility Account Number<span style="color: #FF0000;">*</span><br>
			<input name="utilityAccountNumber" type="text" value="">
		</div>
		<div class="four columns">
			Rebate Made Payable To<span style="color: #FF0000;">*</span><br>
			<input name="rebateMadePayableTo" type="text" value="">
		</div>
	</div>
	<hr style="border:1pt dashed black;">
	<div class="row">
		<div class="twelve columns">
			Mailing Address<span style="color: #FF0000;">*</span><br>
			<input name="mailingAddress" type="text" value="" style="width:680px;"><br>
			(For rebate check if different than installed address)
			<br><Br>
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			City<span style="color: #FF0000;">*</span><br>
			<input name="mailingCity" type="text" value="">
		</div>
		<div class="four columns">
			State<span style="color: #FF0000;">*</span><br>
			<input name="mailingState" type="text" value="">
		</div>
		<div class="four columns">
			Zip<span style="color: #FF0000;">*</span><br>
			<input name="mailingZip" type="text" value="">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			2. Owner/Landlord Information (if different from above)
		</div>
	</div>
	<div class="row">
		<div class="six columns">
			Owner/Landlord <span style="color: #FF0000;">*</span><br>
			FirstName <input name="ownerFirstName" type="text" value="">
			LastName <input name="ownerLastName" type="text" value="">
		</div>
		<div class="six columns">
			Tax ID Number<span style="color: #FF0000;">*</span><br>
			<input name="ownerTaxId" type="text" value=""><br>
			(Required if owner is incorporated)<Br>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="twelve columns">
			Owner/Landlord Address<span style="color: #FF0000;">*</span><br>
			<input name="ownerAddress" type="text" value="" style="width:680px;">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			City<span style="color: #FF0000;">*</span><br>
			<input name="ownerCity" type="text" value="">
		</div>
		<div class="four columns">
			State<span style="color: #FF0000;">*</span><br>
			<input name="ownerState" type="text" value="">
		</div>
		<div class="four columns">
			Zip<span style="color: #FF0000;">*</span><br>
			<input name="ownerZip" type="text" value="">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Phone Number<span style="color: #FF0000;">*</span><br>
			<input name="ownerPhone" type="text" value="">
		</div>
		<div class="four columns">
			Fax Number<span style="color: #FF0000;">*</span><br>
			<input name="ownerFax" type="text" value="">
		</div>
		<div class="four columns">
			Email<span style="color: #FF0000;">*</span><br>
			<input name="ownerEmail" type="text" value="">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			3. Contractor Information
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Contractor Name<span style="color: #FF0000;">*</span><br>
			<input name="contractorName" type="text" value="">
		</div>
		<div class="four columns">
			License Number<span style="color: #FF0000;">*</span><br>
			<input name="contractorLicenseNumber" type="text" value="">
		</div>
		<div class="four columns">
			Federal Tax ID Number<span style="color: #FF0000;">*</span><br>
			<input name="contractorTaxId" type="text" value=""><br>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			Contractor Address<span style="color: #FF0000;">*</span><br>
			<input name="contractorAddress" type="text" value="" style="width:680px;">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			City<span style="color: #FF0000;">*</span><br>
			<input name="contractorCity" type="text" value="">
		</div>
		<div class="four columns">
			State<span style="color: #FF0000;">*</span><br>
			<input name="contractorState" type="text" value="">
		</div>
		<div class="four columns">
			Zip<span style="color: #FF0000;">*</span><br>
			<input name="contractorZip" type="text" value="">
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Phone Number<span style="color: #FF0000;">*</span><br>
			<input name="contractorPhone" type="text" value="">
		</div>
		<div class="four columns">
			Fax Number<span style="color: #FF0000;">*</span><br>
			<input name="contractorFax" type="text" value="">
		</div>
		<div class="four columns">
			Email<span style="color: #FF0000;">*</span><br>
			<input name="contractorEmail" type="text" value="">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			3. Measures Installed
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			Provide Description of Measure(s)<span style="color: #FF0000;">*</span><br>
			<textarea name="measuresInstalled" cols="400"></textarea><br>
			(i.e. Air Sealing, Attic Insulation, Wall Insulation, etc.)<br>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="six columns">
			Signed Inspection File Upload<span style="color: #FF0000;">*</span><br>
			<input type="file" name="measuresInspectionFile">
		</div>
		<div class="six columns">
			Invoice File Upload<span style="color: #FF0000;">*</span><br>
			<input type="file" name="measuresInvoiceFile">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			5. Customer Acknowledgement<br>
			I hereby request a rebate for the listed work. I certify that a licensed contractor has installed the measures listed in accordance with state and local codes.
		</div>
	</div>
	<div class="row">
		<div class="four columns">
			Customer Initials<span style="color: #FF0000;">*</span><br>
			<input type="text" name="customerInitials">
		</div>
		<div class="three columns">
			Date<span style="color: #FF0000;">*</span><br>
			<input type="text" name="customerAcknowledgementDate" class="date">
		</div>
		<div class="four columns">
			If application is completed by WG&E employee, please enter initials<br>
			<input type="text" name="staffInitials"><br>
			Only enter if application is completed by WG&E employee.
		</div>
	</div>
	<a href="#" id="save">Save</a>
	<input type="hidden" name="action" value="customerData_add">
	<input type="hidden" name="secret" value="skeletonCET35">
</form>
	<div id="onlineRebateResults">results</div>
</div> <!--end Div container -->
<script>
$(function(){
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	var formElement = $("#onlineRebateForm");
	 
	formElement.validate({
        rules: {
			customerAcknowledgmentDate: {
				required: true
			}
        }
    });
	
    $("#save").click(function(e){
        $('#onlineRebateResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiCustomerData.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					console.log(data)
                    $('#onlineRebateResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
					console.log(jqXHR);
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#onlineRebateResults').html(message).addClass("alert");
                }
            });
        }
    });
});
</script>