<?php
include_once($dbProviderFolder."MuniProvider.php");
include_once($siteRoot."_setupDataConnection.php");
$muniProvider = new MuniProvider($dataConn);
$path = getcwd();
if (strpos($path,"xampp")){
	$testingServer = true;
}
	$formName = str_replace(" ","",$_GET['formName']);
	$fileName = $_GET['fileName'];
	$oid = $_GET['oid'];
	
	if ($formName && $fileName && $oid){
		$url = "https://wgeserv.sprypoint.com/cet/download-file/for-object/".$oid."/with-name/".rawurlencode($fileName) ;
		$curlHeaders = array('X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
		if(!curl_exec($ch)){
			echo curl_error($ch) . '" - Code: ' . curl_errno($ch);
			echo "<br>Attached files were not retrieved for oid=".$oid."<br>";
		}else{		
			$data = curl_exec($ch);
			$error = curl_error($ch); 
		}
		$destinationFolder = $path."/rebateAttachments/";
		$destinationName = $formName."_".$oid."_".$fileName;
		$destination = $destinationFolder.$destinationName;
		$file = fopen($destination, "w+");
		fputs($file, $data);
		fclose($file);
		curl_close($ch);
		//now update the record to have the file information
		$updateFileResults = $muniProvider->updateFileStatus($formName,$destinationName,$oid);
		echo "<a href=\"".$CurrentServer.$adminFolder."muni/rebateAttachments/".$destinationName."\">".$destinationName."</a>";
	}else{
		echo "set querystring formName, fileName, and oid";
	}
?>