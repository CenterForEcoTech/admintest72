<h3>Appliance<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-applianceHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Appliance Information <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<div class="row">
					<div class="twelve columns">
						<?php 
							if (strtolower($RebateFieldValue["rebateAppliance"]["ApplianceType"]) == "array"){
								$rawDataParts = explode(" files:",$rebateInfoCollection[0]->MuniResRebateAppliance_RawFormData);
								//print_pre($rawDataParts);
								$ApplianceTypeRawData = json_decode($rawDataParts[0]);
								$Energy_Star_Appliance = $ApplianceTypeRawData->Energy_Star_Appliance;
								$Energy_Star_Appliances = explode(",",$Energy_Star_Appliance);
								$RebateFieldValue["rebateAppliance"]["ApplianceType"] = $Energy_Star_Appliances[0];
								//print_pre($Energy_Star_Appliances[0]);
							}
							$ApplianceArray = explode(",",$RebateFieldValue["rebateAppliance"]["ApplianceType"]);
							$ApplianceInputType = (count($ApplianceArray) > 1 ? "checkbox" : "radio");
							$EnergyStarArray = array("Clothes Washer"=>"certified-clothes-washers","Freezer"=>"certified-residential-freezers","Room A/C"=>"certified-room-air-conditioners","Heat Pump Water Heater"=>"certified-geothermal-heat-pumps","Refrigerator"=>"certified-residential-refrigerators","Dishwasher"=>"certified-residential-dishwashers","Dehumidifier"=>"certified-dehumidifiers","Air Purifier"=>"certified-room-air-cleaners","EV Charger Level 1"=>"certified-evchargers","EV Charger Level 2"=>"certified-evchargers");				
							//$EnergyStarArray = array("Clothes Washer"=>"certified-clothes-washers","Freezer"=>"certified-residential-freezers","Room A/C"=>"certified-room-air-conditioners","Heat Pump Water Heater"=>"certified-geothermal-heat-pumps","Refrigerator"=>"certified-residential-refrigerators","Dishwasher"=>"certified-residential-dishwashers","Dehumidifier"=>"certified-dehumidifiers","Air Purifier"=>"certified-room-air-cleaners");
							$EnergyStarApplianceSearch = $EnergyStarArray[$ApplianceArray[0]];
						?>
						<b>Energy Star Appliance</b><br>
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Clothes Washer"<?php echo (in_array("Clothes Washer",$ApplianceArray) ? " checked" : "");?>>Clothes Washer&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Freezer"<?php echo (in_array("Freezer",$ApplianceArray) ? " checked" : "");?>>Freezer&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Room A/C"<?php echo (in_array("Room A/C",$ApplianceArray) ? " checked" : "");?>>Room A/C&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Heat Pump Water Heater"<?php echo (in_array("Heat Pump Water Heater",$ApplianceArray) ? " checked" : "");?>>Heat Pump Water Heater&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Refrigerator"<?php echo (in_array("Refrigerator",$ApplianceArray) ? " checked" : "");?>>Refrigerator&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Dishwasher"<?php echo (in_array("Dishwasher",$ApplianceArray) ? " checked" : "");?>>Dishwasher&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Dehumidifier"<?php echo (in_array("Dehumidifier",$ApplianceArray) ? " checked" : "");?>>Dehumidifier&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="Air Purifier"<?php echo (in_array("Air Purifier",$ApplianceArray) ? " checked" : "");?>>Air Purifier&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="<?php echo $ApplianceInputType;?>" name="applianceType" value="EV Charger"<?php echo (in_array("EV Charger",$ApplianceArray) ? " checked" : "");?>>EV Charger&nbsp;&nbsp;&nbsp;&nbsp;
				<br>
						Note: User a Separate Form for each Purchased Appliance Listed Above.<Br>
						<img src="<?php echo $CurrentServer;?>images/energystar.png" style="height:20px;width:20px;">Verify Energy Star Rating for '<a href="http://www.energystar.gov/productfinder/product/<?php echo $EnergyStarApplianceSearch;?>/results?search_text=<?php echo substr($RebateFieldValue["rebateAppliance"]["ApplianceModelNumber"],0,3);?>" target="EStar"><?php echo $EnergyStarApplianceSearch;?></a>'
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Brand<span style="color: #FF0000;">*</span><br>
						<input type="text" name="applianceBrand" value="<?php echo $RebateFieldValue["rebateAppliance"]["ApplianceBrand"];?>">
					</div>
					<div class="five columns">
						Model Number<span style="color: #FF0000;">*</span><br>
						<input type="text" name="applianceModelNumber" value="<?php echo $RebateFieldValue["rebateAppliance"]["ApplianceModelNumber"];?>"<?php $checkApproved =true;?> class="checkApproved applianceModelNumber" data-fieldname="applianceModelNumber">
						<br><span id="PreviouslyApprovedapplianceModelNumber" style="font-size:10pt;"></span>
					</div>
					<div class="four columns">
						Serial Number of Appliance or Level of EV Charger<span style="color: #FF0000;">*</span><br>
						<input type="text" name="applianceSerialNumber" value="<?php echo $RebateFieldValue["rebateAppliance"]["ApplianceSerialNumber"];?>"><br>
					</div>
				</div>
				Note: Rebate will not be processed without the serial number.
				<div class="row">
					<br clear="all">
					<b>Store Information for Purchased Appliance or Contractor Information for EV Charger</b>
					<br clear="all">
					<div class="four columns">
						Store Name<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storeName" value="<?php echo $RebateFieldValue["rebateAppliance"]["StoreName"];?>">
					</div>
				</div>
				<div class="row">
					<div class="eight columns">
						Store Address<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storeAddress" value="<?php echo $RebateFieldValue["rebateAppliance"]["StoreAddress"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						City<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storeCity" value="<?php echo $RebateFieldValue["rebateAppliance"]["StoreCity"];?>">
					</div>
					<div class="four columns">
						State<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storeState" value="<?php echo $RebateFieldValue["rebateAppliance"]["StoreState"];?>">
					</div>
					<div class="four columns">
						Zip<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storeZipCode" value="<?php echo $RebateFieldValue["rebateAppliance"]["StoreZipCode"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Purchase Price<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storePurchasePrice" value="<?php echo ($RebateFieldValue["rebateAppliance"]["StorePurchasePrice"] ? "$".money($RebateFieldValue["rebateAppliance"]["StorePurchasePrice"]) : "");?>">
					</div>
					<div class="four columns">
						Purchase Date<span style="color: #FF0000;">*</span><br>
						<input type="text" name="storePurchaseDate" class="date" value="<?php echo MySQLDate($RebateFieldValue["rebateAppliance"]["StorePurchaseDate"]);?>">
					</div>
				</div>
			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="Appliance">
	</form>
	<div id="onlineRebateResults"></div>
	<?php include_once('termsConditions.php');?>
</div> <!--end Div container -->