<div class="<?php echo ($contractorInfoMissing ? "opened" : "");?>">
	<hr>
	Contractor Information <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<div class="closedDetails<?php echo ($contractorInfoMissing ? " missingDetails" : "");?>">
		<div class="row">
			<div class="four columns">
				Contractor Name<br>
				<input name="contractorName" type="text" value="<?php echo $ContractorFieldValue["ContractorName"];?>">
			</div>
			<div class="five columns">
				License Number (<a style="font-size:10pt;" href="https://elicensing.state.ma.us/CitizenAccess/" target="clookup">License Lookup</a>)<br>
				<input name="contractorLicenseNumber" type="text" value="<?php echo $ContractorFieldValue["ContractorLicenseNumber"];?>"<?php $contractorCheck=true;?> class="contractorCheck">
				<span id="contractorCheckResults" style="font-size:10pt;"></span>
			</div>
<!--		
			<div class="four columns">
				Federal Tax ID Number<br>
				<input name="contractorTaxID" type="text" value="<?php echo $ContractorFieldValue["ContractorTaxID"];?>"><br>
			</div>
-->
		</div>
<!--		
		<div class="row">
			<div class="four columns">
				Contractor Contact Name<br>
				<input name="contractorContactName" type="text" value="<?php echo $ContractorFieldValue["ContractorContactName"];?>">
			</div>
		</div>
-->
		<div class="row">
			<div class="twelve columns">
				Contractor Address<br>
				<input name="contractorAddress" type="text" value="<?php echo $ContractorFieldValue["ContractorAddress"];?>" style="width:680px;">
			</div>
		</div>
		<div class="row">
			<div class="four columns">
				City<br>
				<input name="contractorCity" type="text" value="<?php echo $ContractorFieldValue["ContractorCity"];?>">
			</div>
			<div class="four columns">
				State<br>
				<input name="contractorState" type="text" value="<?php echo $ContractorFieldValue["ContractorState"];?>">
			</div>
			<div class="four columns">
				Zip<br>
				<input name="contractorZip" type="text" value="<?php echo $ContractorFieldValue["ContractorZip"];?>">
			</div>
		</div>
		<div class="row">
			<div class="four columns">
				Phone Number<br>
				<input name="contractorPhone" type="text" value="<?php echo $ContractorFieldValue["ContractorPhone"];?>">
			</div>
			<div class="four columns">
				Email<br>
				<input name="contractorEmail" type="text" value="<?php echo $ContractorFieldValue["ContractorEmail"];?>">
			</div>
		</div>
<!--		
		<?php if ($rebateName == "rebateCoolHome"){?>
			<div class="row">
				<div class="nine columns">
					Is technician NATE Certified? <input name="contractorNATECertified" type="radio" value="Yes"<?php echo ($ContractorFieldValue["ContractorNATECertified"] == "Yes" ? " checked" : "");?>>Yes
					<input name="contractorNATECertified" type="radio" value="No"<?php echo ($ContractorFieldValue["ContractorNATECertified"] == "No" ? " checked" : "");?>>No
					<br>Note: Not required for rebate eligibility
				</div>
			</div>
		<?php }?>
-->
	</div> <!--closedDetails-->
</div>