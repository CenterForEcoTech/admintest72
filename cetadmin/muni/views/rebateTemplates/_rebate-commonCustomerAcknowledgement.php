<hr>
	Customer Acknowledgement<br>
	I hereby request a rebate for the listed work. I certify that a licensed contractor has installed the measures listed in accordance with state and local codes.
	<div class="row">
		<div class="four columns">
			Customer Initials<span style="color: #FF0000;">*</span><br>
			<input type="text" name="customerInitials" value="<?php echo $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_CustomerInitials"};?>">
		</div>
		<div class="three columns">
			Date<span style="color: #FF0000;">*</span><br>
			<input type="text" name="customerAcknowledgementDate" class="date" value="<?php echo MySQLDate($RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_CustomerAcknowledgementDate"});?>">
		</div>
		<div class="nine columns">
			If application is completed by WG&E/CET employee, please enter initials<br>
			<input type="text" name="staffInitials" value="<?php echo $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_StaffInitials"};?>"><br>
			Only enter if application is completed by WG&E/CET employee.
		</div>
	</div>
