<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			WG&E's Commercial and Industrial High Efficiency Heating and Water Heating Prescriptive Rebate program is designed to expedite the processing and installation of typical gas heating and water heating energy efficiency opportunities. Rebates are offered to promote the installation of premium efficiency heating and water heating equipment and offset the incremental cost of such systems over standard replacements.<br>
			<br>
			<b>Eligible Projects</b>
			The table below summarizes eligible equipment, technical requirements, and rebates available through the program.<br>
			<i>All installations must be performed by licensed heating contractors or plumbers</i>.<br>
			Additional energy efficiency measures not listed here may be eligible for incentives through other WG&E energy efficiency programs.<br> 
		</div>
	</div>
	<div class="row">
		<div class="seven columns">
			<u>Technical Requirements</u><br>
			<b>Space Heating –Furnaces</b></br>
				95% AFUE or greater and ECM Motor, up to 150 MBH<Br>
				97% AFUE or greater and ECM Motor, up to 150 MBH<br>
			<b>Space Heating – Hydronic Boilers</b><Br>
				85% AFUE or greater, up to 300 MBH<br>
				90% AFUE or greater, up to 300 MBH<Br>
				95% AFUE or greater, up to 300 MBH<br>
			<b>Water Heating - Indirect</b><br>
				Attached to Natural Gas Boiler<br>
			<B>Water Heating - Storage</b><br>
				High Efficiency (0.62 EF)<Br>
				ENERGY STAR® Labeled (0.67 EF)<br>
			<b>Water Heating - Tankless</b><br>
				Energy Factor of .82 or greater<Br>
				Energy Factor of .94 or greater<br>	
		</div>
		<div class="five columns">
			<u>Rebate</u><br>
			<br>
			$300<Br>
			$600<br>
			<br>
			$500<br>
			$1,000<br>
			$1,500<br>
			<br>
			$400<br>
			<br>
			$50<br>
			$100<br>
			<br>
			$500<br>
			$800
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<b>How to Apply</b><br>
			<ul style="list-style-type:circle;">
				<li>Review this application and contact your licensed heating contractor or plumber to help you determine which models may be eligible. For a database of performance-certified technologies, please visit <a href="http://www.ahridirectory.org" target="ahrdirectory">www.ahridirectory.org</a>.
				<li>Within twelve (12) months of the installation date<sup>1</sup> , complete and submit here. 
				<br>
				<Br>
				When submitting the application, <b>please make sure to attach originals of all dated receipts/work orders that document the installation</b>. These receipts and/or work orders should include the name, license number, address, and phone number of the contractor that completed the installation. You may wish to retain a copy of all documents for your records. 
				<br>
				<Br>
				If you have any questions, or if you would like assistance in completing this form, call our toll free number at 1-877-296-4312.
			</ul>
			<br clear="all">
			<hr style="border:1pt black dashed;">
			<sup>1</sup>Projects consisting of five (5) or more units, or applications with rebates of $25,000 or greater must receive pre-approval before equipment installation. Please submit the application along with any quotes or relevant cost data prior to installation of any equipment. Projects under $25,000 do not require pre-approval
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>
