<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			All Information Below is Required to Receive Rebate for the Purchase of Qualifying Energy Star Pool Pump
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<b>Eligibility for Rebate</b>:<br>
			<ol style="list-style-type:circle;">
				<li>Eligibility is Based Solely on the ENERGY STAR Qualified Products List at www.energystar.gov</li>
				<li>Limit One Rebate per Account Every Five Years</li>
				<li>Installation Must Include a Qualifying Controller as Applicable, Only Twospeed and Variable Speed Models are Eligible</li>
				<li>Eligible Pump must be installed within the Electric Municipal Service Territory, be UL listed and be Installed in Accordance with the National Electric Code and Manufacturer’s Recommendations</li>
				<li>Qualified Pool Pump Must Be Purchased between 01/01/15 and 12/31/15 and Application Form Postmarked by 01/31/16</li>
				<li>The Utility Reserves the Right to End these Rebates at Any Time without Prior Notice</li>
			</ol>
			<br clear="all">
			<b>Additional Conditions</b><br>
			<ol style="list-style-type:circle;">
				<li>This rebate may not be combined with any other utility or energy efficiency service provider offer.</li>
				<li>The participating utility or energy efficiency service provider reserves the right to conduct field inspections to verify installations.</li>
				<li>Participating utility or energy efficiency service does not guarantee the performance of installed equipment, expressly or implicitly.</li>
			</ol>
			<br clear="all">
			<b>For More Information</b><br>
			Call Toll Free: 1 (877) 296-4312<Br>
			<br clear="all">
			<b>How to get your rebate</b><br>
			<ol style="list-style-type:decimal;">
				<li>Attach a copy of your dated sales receipt: Must include the qualifying model, manufacturer, purchase date, purchase price and the contractor's information.</li>
				<li>Attach a copy of your recent electric bill.</li>
			</ol>
			Note: Allow 6-8 weeks for processing
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>