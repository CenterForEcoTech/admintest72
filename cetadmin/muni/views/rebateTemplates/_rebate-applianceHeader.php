<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			All Information Below is Required to Receive Rebate for the Purchase of NEW Energy Star ® Appliances
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<div class="six columns">
				<b>Energy Star Appliance Rebates</b><br>
				Clothes Washer<br>
				Refrigerator (≥ 12 cu. ft.)<br>
				Freezer (≥ 12 cu. ft.)<br>
				Dishwasher<br>
				Room A/C (EER Greater than 11.2)<br>
				Dehumidifier<br>
				Air Purifier<br>
				Heat Pump Water Heater<br>
			</div>
			<div class="four columns">
				<br>
				$25<br>
				$25<br>
				$25<br>
				$25<br>
				$25<br>
				$25<br>
				$25<br>
				$300			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<b>Additional Conditions</b><br>
			<ol style="list-style-type:circle;">
				<li>Qualified Appliances Must Be Purchased between 01/01/15 and 12/31/15 and Postmarked by 01/31/16</li>
				<li>Appliance Eligibility is Based Solely on the Appliance List at www.energystar.gov</li>
				<li>One Rebate of Each of the Above Appliances every Three Years</li>
				<li>The Utility Reserves the Right to End these Rebates at Any Time</li>
			</ol>
			<br clear="all">
			<b>Additional Restrictions</b><br>
			<ol style="list-style-type:circle;">
				<li>Only NEW Energy Star Appliances are Eligible for Rebate</li>
				<li>Only Customers Residing in 1-4 Unit Residences are Eligible to Participate</li>
				<li>Leased Appliances are Not Eligible to Receive Rebates</li>
				<li>Appliances Included in the Purchase of a Residence are Not Eligible</li>
			</ol>
			<br clear="all">
			<b>For More Information</b><br>
			Call Toll Free: 1 (888) 655-6767<Br>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<b>How to get your rebate</b><br>
			<ol style="list-style-type:decimal;">
				<li>Attach a copy of your dated sales receipt.</li>
				<li>Attach a copy of your recent electric bill.</li>
			</ol>
			Note: Allow 6-8 weeks for processing
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>