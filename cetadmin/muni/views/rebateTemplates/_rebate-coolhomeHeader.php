<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			Offer valid on equipment purchased and installed between January 1, 2015 - December 31, 2015 (subject to funding availability)<Br>All submissions must be postmarked by January 31, 2016<br>
			<img src="<?php echo $CurrentServer.$adminFolder;?>muni/images/seerChart.png">
			<br clear="all">
			<sup>1</sup>SEER-Seasonal Energy Efficiency Ratio<br>
			<sup>2</sup>EER Energy Efficiency Ratio is a measure of instantaneous cooling efficiency<br>
			<sup>3</sup>HSPF-Heating Seasonal Performance Factor<br>
			*Ductless Mini-Split cooling-only units are not eligible<Br>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<b>To receive your rebate check by mail, follow these steps</b>:<br>
			<ol style="list-style-type:decimal;">
				<li>Purchase and install an ENERGY STAR® qualifying product at a property with an active electric account with one of the participating energy efficiency providers.</li>
				<li>Obtain your contractor's invoice with equipment make, coil and condenser model numbers, size in tons, date and location of installation and total installation cost.</li>
				<li>Submit the following on this form.
					<ul style="list-style-type:circle;">
						<li>This application, completed accurately and legibly.</li>
						<li>Contractor's invoice showing required information and proof of purchase.</li>
						<li>Copy of "ACCA approved version 8 Manual J" Load (Sizing) Calculation (Note: Not needed for ductless mini-split systems.)</li>
						<li>Copy of your most recent electric bill.</li>
						<li>A copy of the AHRI Certificate. Visit www.ahridirectory.org or contact your Heating and Air Conditioning Contractor to obtain a copy.</li>
					</ul>
					<br>
					Note: Photocopy your entire submission for your records
			</ol>
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>