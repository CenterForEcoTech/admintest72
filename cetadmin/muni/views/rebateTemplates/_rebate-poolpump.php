<h3>Pool Pump<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-poolpumpHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Pump Information <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<b>Replaced Pool Pump Information, if Applicable</b><br>
				<div class="row">
					<div class="four columns">
						Replaced Pump Manufacturer<span style="color: #FF0000;">*</span><br>
						<input name="replacedManufacturer" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["ReplacedManufacturer"];?>">
					</div>
					<div class="four columns">
						Replaced Pump Model Model<span style="color: #FF0000;">*</span><br>
						<input name="replacedModelNumber" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["ReplacedModelNumber"];?>">
					</div>
					<div class="four columns">
						Replaced Pump Horsepower<span style="color: #FF0000;">*</span><br>
						<input name="replacedHorsepower" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["ReplacedHorsepower"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Type of Replaced Pool Pump<span style="color: #FF0000;">*</span><br>
						<input name="replacedType" type="radio" value="Single-speed"<?php echo ($RebateFieldValue["rebatePoolPump"]["ReplacedType"] == "Single-speed" ? " checked" : "");?>>Single-speed&nbsp;&nbsp;&nbsp;&nbsp;
						<input name="replacedType" type="radio" value="Other"<?php echo ($RebateFieldValue["rebatePoolPump"]["ReplacedType"] == "Other" ? " checked" : "");?>>Other
					</div>
				</div>
				<br clear="all">
				<b>New Pool Pump and Controller Information</b><br>
				<div class="row">
					<div class="four columns">
						New Pump Manufacturer<span style="color: #FF0000;">*</span><br>
						<input name="newManufacturer" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["NewManufacturer"];?>">
					</div>
					<div class="four columns">
						New Pump Model Model<span style="color: #FF0000;">*</span><br>
						<input name="newModelNumber" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["NewModelNumber"];?>">
					</div>
					<div class="four columns">
						New Pump Horsepower<span style="color: #FF0000;">*</span><br>
						<input name="newHorsepower" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["NewHorsepower"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Type of New Pool Pump<span style="color: #FF0000;">*</span><br>
						<input name="newType" type="radio" value="Two-speed"<?php echo ($RebateFieldValue["rebatePoolPump"]["NewType"] == "Two-speed" ? " checked" : "");?>>Two-speed&nbsp;&nbsp;&nbsp;&nbsp;
						<input name="newType" type="radio" value="Variable"<?php echo ($RebateFieldValue["rebatePoolPump"]["NewType"] == "Variable" ? " checked" : "");?>>Variable
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Controller Manufacture<span style="color: #FF0000;">*</span><br>
						<input name="newControllerManufacturer" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["NewControllerManufacturer"];?>">
					</div>
					<div class="four columns">
						Controller Model Number<span style="color: #FF0000;">*</span><br>
						<input name="newControllerModelNumber" type="text" value="<?php echo $RebateFieldValue["rebatePoolPump"]["NewControllerModelNumber"];?>">
					</div>
					<div class="four columns">
						Purchase Price<span style="color: #FF0000;">*</span><br>
						<input name="newPrice" type="text" value="<?php echo ($RebateFieldValue["rebatePoolPump"]["NewPrice"] ? "$".money($RebateFieldValue["rebatePoolPump"]["NewPrice"]) : "");?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Purchased Date<span style="color: #FF0000;">*</span><br>
						<input name="newPurchaseDate" type="text" value="<?php echo MySQLDate($RebateFieldValue["rebatePoolPump"]["NewPurchaseDate"]);?>" class="date">
					</div>
					<div class="four columns">
						Installation Date<span style="color: #FF0000;">*</span><br>
						<input name="newInstalledDate" type="text" value="<?php echo MySQLDate($RebateFieldValue["rebatePoolPump"]["NewInstalledDate"]);?>" class="date">
					</div>
				</div>

			
			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="PoolPump">
	</form>
	<div id="onlineRebateResults"></div>
	<?php include_once('termsConditions.php');?>
</div> <!--end Div container -->