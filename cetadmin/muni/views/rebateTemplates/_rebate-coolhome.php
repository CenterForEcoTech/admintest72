<h3>Central AC HeatPump<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-coolhomeHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Measures / Equipment Identification <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<ul>
				<?php
					$measureTypes = array("Central AC >16 SEER"=>"CentralAC16","Central AC >18 SEER"=>"CentralAC18","Ductless Mini-Split >18 SEER"=>"Ductless18","Ductless Mini-Split >20 SEER"=>"Ductless20");
					$measureFields = array("Manufacturer"=>"three","Condenser Model Number"=>"three","Coil Model Number"=>"three","SEER"=>"one","EER"=>"one","HSPF"=>"one","Size Tons"=>"two","Install Cost"=>"three","Rebate"=>"three","Qty Installed"=>"two","Total Rebate"=>"five");
					foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
					?><li>
						<div class="row">
							<b><?php echo $measureTypeDisplay;?>:</b>
							<br clear="all">
							<?php 
								$thisCounter = 0;
								foreach ($measureFields as $measureFieldDisplay=>$measureColumnSize){
									$thisCounter++;
									$thisMeasureFieldName = "measure_".$measureTypeInputName.str_replace("/","",str_replace(" ","",$measureFieldDisplay));
									$thisValue = $RebateFieldValue["rebateCoolHome"][ucfirst($thisMeasureFieldName)];
									$thisMeasureManufacturer = $RebateFieldValue["rebateCoolHome"][ucfirst("measure_".$measureTypeInputName."Manufacturer")];
									$thisDisplayValue = "";
									$inputType="text";
									$thisValueChecked = "";
									$thisValueNotChecked = "";
									$thisStyle = "";
									$belowDisplay = "";
									if ($measureFieldDisplay == "Rebate" || $measureFieldDisplay == "Install Cost" || $measureFieldDisplay == "Total Rebate"){
										$thisValue = ($thisValue == "0.00" ? "" : $thisValue);
										if ($thisValue && $thisValue !="0.00"){
											$thisValue = "$".money($thisValue);
										}
									}
									$thisDisplayValue = $thisValue;
									$measureFieldDisplayText = $measureFieldDisplay."<br>";
									$thisMeasureRebateApprovedField = "measure_".$measureTypeInputName."Approved";
									if ($measureFieldDisplayText == "Condenser Model Number<br>"){$measureFieldDisplayText = "Condenser Model<br>";}
									if ($measureFieldDisplay == "Total Rebate"){
										$thisMeasureRebateAmount = $RebateFieldValue["rebateCoolHome"][ucfirst("measure_".$measureTypeInputName."TotalRebate")];
										$thisMeasureRebateApproved = $RebateFieldValue["rebateCoolHome"][ucfirst($thisMeasureRebateApprovedField)];
										if ($thisMeasureManufacturer || ($thisMeasureRebateAmount && $thisMeasureRebateAmount != "0.00")){
											$spanStyleYes = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#00CC00" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid green;cursor:pointer;";
											$spanStyleNo = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#ffffff" : "orange").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$divStyle = "display:block";
										}else{
											$spanStyleYes = "padding-right:5px;border:1pt solid green;cursor:pointer;padding-top:2px;";
											$spanStyleNo = "padding-right:5px;border:1pt solid orange;cursor:pointer;padding-top:2px;";
											$divStyle = "display:none";
										}
										$belowFieldDisplayText = "<div style='font-size:10pt;".$divStyle."' id='ApprovalDiv".$thisMeasureRebateApprovedField."'>Approved: <span style='".$spanStyleYes."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Yes').checked=true;\">";
										$thisValueChecked = ($thisMeasureRebateApproved == "Yes" ? " checked='checked'" : "");
										$thisValueNotChecked = "Yes</span><br><span style='margin-left:64px;".$spanStyleNo."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."No').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."No' name='".$thisMeasureRebateApprovedField."' value='No'".($thisMeasureRebateApproved == "Yes" ? "" : " checked='checked'")."> No</span><span id='PreviouslyApproved".$thisMeasureRebateApprovedField."' style='display:none'></span></div>"; 
										$belowDisplay = "<br clear='all'>".$belowFieldDisplayText."<input id=\"".$thisMeasureRebateApprovedField."Yes\" name=\"".$thisMeasureRebateApprovedField."\" type=\"radio\" value=\"Yes\"".$thisValueChecked.">".$thisValueNotChecked;

									}
									$thisInfo = "";
									if (strpos(" ".$measureFieldDisplayText,"Manufacturer") || strpos(" ".$measureFieldDisplayText,"Model")){
										$thisInfo = " class='checkApproved ".$thisMeasureRebateApprovedField."' data-fieldname='".$thisMeasureRebateApprovedField."'";
										$checkApproved = true;
									}

								?>
									<div class="<?php echo $measureColumnSize;?> columns">
										<?php echo $measureFieldDisplayText;?>
										<input name="<?php echo $thisMeasureFieldName;?>" type="<?php echo $inputType;?>" value="<?php echo $thisDisplayValue;?>" style="<?php echo $thisStyle;?>"<?php echo $thisInfo;?>>
										<?php echo $belowDisplay;?>
									</div>
								<?php
									if ($thisCounter == 3 || $measureFieldDisplay == "Size Tons"){
										echo "<br clear='all'>";
									}
								}
							?>
						</div>
					</li>
					<?php
					}
				?>
				</ul>
			</div><!--closedDetails-->
		</div>
		
		<div class="<?php echo ($facilityInfoMissing ? "opened" : "");?>">
			<hr>
			Equipment Installed <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<div class="row">
					<div class="twelve columns">
						<?php $EquipmentInstalledType = explode(",",$RebateFieldValue["rebateCoolHome"]["EquipmentInstalledType"]);?>
						New Equipment Installed (to be completed by contractor)<br>
						<input type="checkbox" name="equipmentInstalledType" value="New Construction"<?php echo (in_array("New Construction",$EquipmentInstalledType) ? " checked" : "");?>>New Construction&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="equipmentInstalledType" value="Replacement System"<?php echo (in_array("Replacement System",$EquipmentInstalledType) ? " checked" : "");?>>Replacement System<br>
						<input type="checkbox" name="equipmentInstalledType" value="Adding to existing ductwork"<?php echo (in_array("Adding to existing ductwork",$EquipmentInstalledType) ? " checked" : "");?>>Adding to existing ductwork&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="equipmentInstalledType" value="New or additional ductwork and air conditioning"<?php echo (in_array("New or additional ductwork and air conditioning",$EquipmentInstalledType) ? " checked" : "");?>>New or additional ductwork and air conditioning
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Installed Date<span style="color: #FF0000;">*</span><br>
						<input name="equipmentInstalledDate" type="text" value="<?php echo MySQLDate($RebateFieldValue["rebateCoolHome"]["EquipmentInstalledDate"]);?>" class="date">
					</div>
					<div class="four columns">
						Installed Cost<span style="color: #FF0000;">*</span><br>
						<input name="equipmentInstalledCost" type="text" value="<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentInstalledCost"] ? "$".money($RebateFieldValue["rebateCoolHome"]["EquipmentInstalledCost"]) : "");?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						AHRI Ref. Num<span style="color: #FF0000;">*</span><br>
						<input name="equipmentAHRIRef" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentAHRIRef"];?>">
					</div>
					<div class="four columns">
						Manufacturer<span style="color: #FF0000;">*</span><br>
						<input name="equipmentManufacturer" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentManufacturer"];?>">
					</div>
				</div>
				<div class="row">
					<div class="five columns">
						TXV or EXV Installed<span style="color: #FF0000;">*</span><br>
						<input name="equipmentTXVorEXV" type="radio" value="TXV"<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentTXVorEXV"]=="TXV" ? " checked" : "");?>>TXV&nbsp;&nbsp;&nbsp;
						<input name="equipmentTXVorEXV" type="radio" value="EXV"<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentTXVorEXV"]=="EXV" ? " checked" : "");?>>EXV
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Condenser Model Num<span style="color: #FF0000;">*</span><br>
						<input name="equipmentCondensorModelNumber" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentCondensorModelNumber"];?>">
					</div>
					<div class="four columns">
						Coil Model Num<span style="color: #FF0000;">*</span><br>
						<input name="equipmentCoilModelNumber" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentCoilModelNumber"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						AHRI-Rated SEER**<span style="color: #FF0000;">*</span><br>
						<input name="equipmentAHRIRatedSEER" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentAHRIRatedSEER"];?>">
					</div>
					<div class="four columns">
						AHRI-Rated EER**<span style="color: #FF0000;">*</span><br>
						<input name="equipmentAHRIRatedEER" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentAHRIRatedEER"];?>">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						HSPF<span style="color: #FF0000;">*</span><br>
						<input name="equipmentHSPF" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentHSPF"];?>">
					</div>
					<div class="four columns">
						New Unit Size<span style="color: #FF0000;">*</span><br>
						<input name="equipmentNewUnitSize" type="text" value="<?php echo $RebateFieldValue["rebateCoolHome"]["EquipmentNewUnitSize"];?>"><br>
						(in Tons***)
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						Mini Split<span style="color: #FF0000;">*</span><br>
						<input name="equipmentMiniSplit" type="radio" value="Yes"<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentMiniSplit"] == "Yes" ? " checked" : "");?>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
						<input name="equipmentMiniSplit" type="radio" value="No"<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentMiniSplit"] == "No" ? " checked" : "");?>>No

					</div>
					<div class="four columns">
						Rebate Amount<span style="color: #FF0000;">*</span><br>
						<input name="equipmentRebateAmount" type="text" value="<?php echo ($RebateFieldValue["rebateCoolHome"]["EquipmentRebateAmount"] ? "$".money($RebateFieldValue["rebateCoolHome"]["EquipmentRebateAmount"]) : "");?>">
					</div>
				</div>
			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="CoolHome">
	</form>
	<div id="onlineRebateResults"></div>
	<?php include_once('termsConditions.php');?>
</div> <!--end Div container -->