<h3>Efficiency<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-efficiencyHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Measures Installed <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				Eligible Measures:<br clear="all">
				<?php $EligibleMeasures = explode(",",$RebateFieldValue["rebateEfficiency"]["EligibleMeasures"]);?>
				<div class="row">
					<div class="seven columns">
						<input onClick="if (this.checked){document.getElementById('airsealingDiv').style.display='block';}else{document.getElementById('airsealingDiv').style.display='none';}" type="checkbox" name="eligibleMeasures" value="Blower Door Test & Air Sealing / 50% up to $300"<?php echo (in_array("Blower Door Test & Air Sealing / 50% up to $300",$EligibleMeasures) ? " checked" : "");?>>Blower Door Test & Air Sealing / 50% up to $300
					</div>
					<div class="four columns" id="airsealingDiv" style="padding:2px;border:1pt solid green;background-color:<?php echo ($RebateFieldValue["rebateEfficiency"]["AirSealingApprovedAmount"] ? "#00CC00" : "#ffffff");?>;display:<?php echo (in_array("Blower Door Test & Air Sealing / 50% up to $300",$EligibleMeasures) ? "block" : "none");?>">
						Approved Amount:<input style="width:75px;" type="input" name="airSealingApprovedAmount" value="<?php echo $RebateFieldValue["rebateEfficiency"]["AirSealingApprovedAmount"]; ?>">
					</div>
				</div>
				<div class="row">
					<div class="seven columns">
						<input onClick="if (this.checked){document.getElementById('evchargerDiv').style.display='block';}else{document.getElementById('evchargerDiv').style.display='none';}" type="checkbox" name="eligibleMeasures" value="EV Charger Bill Credit"<?php echo (in_array("EV Charger Bill Credit",$EligibleMeasures) ? " checked" : "");?>>EV Charger Installation Bill Credit
					</div>
				</div>
				<div class="row">
					<div class="seven columns">
						<input onClick="if (this.checked){document.getElementById('insulationDiv').style.display='block';}else{document.getElementById('insulationDiv').style.display='none';document.getElementById('insulationValue').value='';}" type="checkbox" name="eligibleMeasures" value="Insulation / 50% up to $300"<?php echo (in_array("Insulation / 50% up to $300",$EligibleMeasures) ? " checked" : "");?>>Insulation / 50% up to $300
					</div>
					<div class="four columns" id="insulationDiv" style="padding:2px;border:1pt solid green;background-color:<?php echo ($RebateFieldValue["rebateEfficiency"]["InsulationApprovedAmount"] ? "#00CC00" : "#ffffff");?>;display:<?php echo (in_array("Insulation / 50% up to $300",$EligibleMeasures) ? "block" : "none");?>">
						Approved Amount:<input style="width:75px;" id="insulationValue" type="input" name="insulationApprovedAmount" value="<?php echo $RebateFieldValue["rebateEfficiency"]["InsulationApprovedAmount"]; ?>">
					</div>
				</div>
				<div class="row">
					<div class="seven columns">
						<input onClick="if (this.checked){document.getElementById('heatingDiv').style.display='block';}else{document.getElementById('heatingDiv').style.display='none';}" type="checkbox" name="eligibleMeasures" value="Heating System / 50% up to $300"<?php echo (in_array("Heating System / 50% up to $300",$EligibleMeasures) ? " checked" : "");?>>Heating System / 50% up to $300
					</div>
					<div class="four columns" id="heatingDiv" style="padding:2px;border:1pt solid green;background-color:<?php echo ($RebateFieldValue["rebateEfficiency"]["HeatingApprovedAmount"] ? "#00CC00" : "#ffffff");?>;display:<?php echo (in_array("Heating System / 50% up to $300",$EligibleMeasures) ? "block" : "none");?>">
						Approved Amount:<input style="width:75px;" type="input" name="heatingApprovedAmount" value="<?php echo $RebateFieldValue["rebateEfficiency"]["HeatingApprovedAmount"]; ?>">
					</div>
				</div>
				<div class="row">
					<div class="twelve columns">
						Provide Description of Measure(s)<span style="color: #FF0000;">*</span><br>
						<textarea name="measuresInstalled" cols="400"><?php echo $RebateFieldValue["rebateEfficiency"]["MeasuresInstalled"];?></textarea><br>
						(i.e. Air Sealing, Attic Insulation, Wall Insulation, etc.)<br>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="Efficiency">
	</form>
</div><!--asDisplay-->
<div id="onlineRebateResults"></div>
<?php include_once('termsConditions.php');?>