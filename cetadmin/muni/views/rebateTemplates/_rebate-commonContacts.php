<?php if ($newCustomerName || ($thisIsAuditPage && !in_array($SelectedCustomerID,$CustomerIDWithAuditInfo))){?>
	<?php 
		if (!$actualAudit){
			include_once('views/_customerIntakeForm.php');
		}
	?>
<?php }else{?>
	<div>
		<span class="details-title">Customer Information</span> <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<div class="closedDetails">
			<div class="row">
				<div class="three columns">Account Holder:</div>
				<div class="nine columns"><b><a href="<?php echo $CurrentServer.$adminFolder;?>muni/?nav=customer-data&CustomerID=<?php echo $SelectedCustomerID;?>"><?php echo $CustomerByID[$SelectedCustomerID]->accountHolderFirstName;?> <?php echo $CustomerByID[$SelectedCustomerID]->accountHolderLastName;?></a></b></div>
			</div>
			<div class="row">
				<div class="three columns">Installed Address:</div>
				<div class="six columns">
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedAddress ? "<b>".$CustomerByID[$SelectedCustomerID]->installedAddress."</b>" : "no address on file");?></b>,
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedCity ? "<b>".$CustomerByID[$SelectedCustomerID]->installedCity."</b>" : "no city on file");?> <?php echo ($CustomerByID[$SelectedCustomerID]->installedState ? ", <b>".$CustomerByID[$SelectedCustomerID]->installedState."</b>" : "");?> <?php echo ($CustomerByID[$SelectedCustomerID]->installedZip ? "<b>".$CustomerByID[$SelectedCustomerID]->installedZip."</b>" : "");?>
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedAddressOwnedOrLeased ? "(".$CustomerByID[$SelectedCustomerID]->installedAddressOwnedOrLeased.")" : "");?>			
				</div>
			</div>
			<div class="row">
				<div class="three columns">Contact Info:</div>
				<div class="nine columns">
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedPhone ? "<b>".$CustomerByID[$SelectedCustomerID]->installedPhone."</b> (Phone) | " : "no phone on file <b>|</b> ");?>
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedWorkPhone ? "<b>".$CustomerByID[$SelectedCustomerID]->installedWorkPhone."</b> (Work) | " : "no work phone on file <b>|</b> ");?>
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedFax ? "<b>".$CustomerByID[$SelectedCustomerID]->installedFax."</b> (Fax) | " : "no fax on file <b>|</b>");?>
					<?php echo ($CustomerByID[$SelectedCustomerID]->installedEmail ? "<b>".$CustomerByID[$SelectedCustomerID]->installedEmail."</b>" : "no email on file");?>
				</div>
			</div>
			<div class="row">
				<div class="three columns">Utility Info:</div>
				<div class="nine columns">
					<?php echo ($CustomerByID[$SelectedCustomerID]->utilityName ? "<b>".$CustomerByID[$SelectedCustomerID]->utilityName."</b>" : "[no utility name on file] ");?> 
					Account#: <?php echo ($CustomerByID[$SelectedCustomerID]->utilityAccountNumber ? "<b>".$CustomerByID[$SelectedCustomerID]->utilityAccountNumber."</b>" : "no account number on file");?>
				</div>
			</div>
			<?php 
				if ($CustomerByID[$SelectedCustomerID]->mailingAddress){$rebateMailingInfo[] = $CustomerByID[$SelectedCustomerID]->mailingAddress;}
				if ($CustomerByID[$SelectedCustomerID]->mailingCity){$rebateMailingInfo[] = $CustomerByID[$SelectedCustomerID]->mailingCity;}
				if ($CustomerByID[$SelectedCustomerID]->mailingState){$rebateMailingInfo[] = $CustomerByID[$SelectedCustomerID]->mailingState;}
				if ($CustomerByID[$SelectedCustomerID]->mailingZip){$rebateMailingInfo[] = $CustomerByID[$SelectedCustomerID]->mailingZip;}
			?>
			<?php if ($CustomerByID[$SelectedCustomerID]->rebateMadePayableTo || count($rebateMailingInfo)){?>
				<div class="row">
					<div class="three columns">Rebate Payable Info:</div>
					<div class="nine columns">
						<b>
							<?php echo $CustomerByID[$SelectedCustomerID]->rebateMadePayableTo;?><br>
							<?php if (count($rebateMailingInfo)){echo implode(", ",$rebateMailingInfo);}?>
						</b>
					</div>
				</div>
			<?php }?>
			<div class="row">
				<div class="three columns">Notes:</div>
				<div class="nine columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->customerNotes;?></b></div>
			</div>
		</div> <!--end closedDetails-->
	</div>
	<?php 
		if ($CustomerByID[$SelectedCustomerID]->ownerAddress){$ownerMailingInfo[] = $CustomerByID[$SelectedCustomerID]->ownerAddress;}
		if ($CustomerByID[$SelectedCustomerID]->ownerCity){$ownerMailingInfo[] = $CustomerByID[$SelectedCustomerID]->ownerCity;}
		if ($CustomerByID[$SelectedCustomerID]->ownerState){$ownerMailingInfo[] = $CustomerByID[$SelectedCustomerID]->ownerState;}
		if ($CustomerByID[$SelectedCustomerID]->ownerZip){$ownerMailingInfo[] = $CustomerByID[$SelectedCustomerID]->ownerZip;}
	?>
	<?php if ($CustomerByID[$SelectedCustomerID]->ownerFirstName || count($ownerMailingInfo)){?>
		<div>
			<hr>
			<span class="details-title">Owner/Landlord Information</span> <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails">
				<div class="row">
					<div class="four columns">Owner/Landlord:</div>
					<div class="eight columns">
						<b><?php echo $CustomerByID[$SelectedCustomerID]->ownerFirstName;?> <?php echo $CustomerByID[$SelectedCustomerID]->ownerLastName;?></b>
						<?php echo ($CustomerByID[$SelectedCustomerID]->ownerTaxId ? "&nbsp;&nbsp;&nbsp;Tax ID: <b>".$CustomerByID[$SelectedCustomerID]->ownerTaxId."</b>" : "");?>
					</div>
				</div>
				<div class="row">
					<div class="four columns">Owner/Landlord Address:</div>
					<div class="eight colums">
						<b>
							<?php if (count($ownerMailingInfo)){echo implode(", ",$ownerMailingInfo);}?>
						</b>
					</div>
				</div>
				<div class="row">
					<div class="four columns">Owner Contact Info:</div>
					<div class="eight columns">
						<?php echo ($CustomerByID[$SelectedCustomerID]->ownerPhone ? "<b>".$CustomerByID[$SelectedCustomerID]->ownerPhone."</b> (Phone) | " : "");?>
						<?php echo ($CustomerByID[$SelectedCustomerID]->ownerFax ? "<b>".$CustomerByID[$SelectedCustomerID]->ownerFax."</b> (Fax) | " : "");?>
						<b><?php echo $CustomerByID[$SelectedCustomerID]->ownerEmail;?></b>
					</div>
				</div>
			</div> <!--closedDetails-->
		</div>
	<?php }?>
	<?php if ($thisIsAuditPage){?>
		<div>
			<hr>
			<span class="details-title">Audit Intake Information</span> <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails">
				<div class="row">
					<div class="three columns">Floor Area (sq ft):</div><div class="two columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditFloorArea;?></b></div><br>
					<div class="three columns">House Width:</div><div class="two columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditHouseWidth;?></b></div><br>
					<div class="three columns">House Length:</div><div class="two columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditHouseLength;?></b></div><br>
					<div class="three columns">Conditioned Stories:</div><div class="two columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditConditionedStories;?></b></div><br>
					<div class="three columns">Heat Equipment Type:</div><div class="three columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditHeatingEquipmentType;?></b></div><br>
					<div class="three columns">Heating Fuel Type:</div><div class="two columns"><b><?php echo $CustomerByID[$SelectedCustomerID]->auditHeatingFuel;?></b></div>
				</div>
				<a href="<?php echo $CurrentServer.$adminFolder."muni/?nav=customer-data&CustomerID=".$SelectedCustomerID."#auditIntakeSection";?>" class="button-link">Edit Audit Intake</a>
			</div>
		</div>
	
	<?php }?>
<?php }//end if newCustomerName ?>