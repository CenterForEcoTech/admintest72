<h3>WiFi<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-wifiHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Rebate Items <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<ul>
				<?php 
					$measureTypes = array("Wireless Thermostat"=>"WiFi");
					$measureFields = array("Date Installed","Manufacturer","Model Number","Purchase Price","Quantity","Rebate Amount","Total Amount");
					foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
					?><li>
						<div class="row">
							<b><?php echo $measureTypeDisplay;?>:</b>
							<br clear="all">
				
							<?php 
								$measureTypes = array("Wireless Thermostat"=>"WiFi");
								$measureFields = array("Date Installed","Manufacturer","Model Number","Purchase Price","Quantity","Rebate Amount","Total Amount");
								$thisCounter = 0;
								foreach ($measureFields as $measureFieldDisplay){
									$thisCounter++;
									$thisMeasureFieldName = "measure_".$measureTypeInputName.str_replace("/","",str_replace(" ","",$measureFieldDisplay));
									$thisValue = $RebateFieldValue["rebateWiFi"][ucfirst($thisMeasureFieldName)];
									$thisMeasureManufacturer = $RebateFieldValue["rebateWiFi"][ucfirst("measure_".$measureTypeInputName."Manufacturer")];
									$thisDisplayValue = "";
									$inputType="text";
									$thisValueChecked = "";
									$thisValueNotChecked = "";
									$belowDisplay = "";
									if ($measureFieldDisplay == "Date Installed"){$thisValue = date("m/d/Y",strtotime($thisValue));}

									if ($measureFieldDisplay == "Rebate Amount" || $measureFieldDisplay == "Purchase Price" || $measureFieldDisplay == "Total Amount"){
										$thisValue = ($thisValue == "0.00" ? "" : $thisValue);
										if ($measureFieldDisplay == "Rebate Amount"){$thisValue = 50;}
										if ($thisValue && $thisValue !="0.00"){
											$thisValue = "$".money($thisValue);
										}
									}
									$thisDisplayValue = $thisValue;
									$measureFieldDisplayText = $measureFieldDisplay."<br>";
									$thisMeasureRebateApprovedField = "measure_".$measureTypeInputName."Approved";
									if ($measureFieldDisplay == "Total Amount"){
										$thisMeasureRebateAmount = $RebateFieldValue["rebateWiFi"][ucfirst("measure_".$measureTypeInputName."TotalAmount")];
										$thisMeasureRebateApproved = $RebateFieldValue["rebateWiFi"][ucfirst($thisMeasureRebateApprovedField)];
										if ($thisMeasureManufacturer || ($thisMeasureRebateAmount && $thisMeasureRebateAmount != "0.00")){
											$spanStyleYes = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#00CC00" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid green;cursor:pointer;";
											$spanStyleNo = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#ffffff" : "orange").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$spanStyleDenied = "background-color:".($thisMeasureRebateApproved == "Denied" ? "red" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$divStyle = "display:block";
										}else{
											$spanStyleYes = "padding-right:5px;border:1pt solid green;cursor:pointer;padding-top:2px;";
											$spanStyleNo = "padding-right:5px;border:1pt solid orange;cursor:pointer;padding-top:2px;";
											$spanStyleDenied = "padding-right:5px;border:1pt solid red;cursor:pointer;padding-top:2px;";
											$divStyle = "display:none";
										}
										$belowFieldDisplayText = "<div style='font-size:10pt;".$divStyle."'>Approved: <span style='".$spanStyleYes."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Yes').checked=true;\">";
										$thisValueChecked = ($thisMeasureRebateApproved == "Yes" ? " checked='checked'" : "");
										$thisValueNotChecked = "Yes</span>
											<br><span style='margin-left:64px;".$spanStyleNo."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."No').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."No' name='".$thisMeasureRebateApprovedField."' value='No'".($thisMeasureRebateApproved == "Yes" ? "" : " checked='checked'")."> No</span>
											<br><span style='margin-left:64px;".$spanStyleDenied."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Denied').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."Denied' name='".$thisMeasureRebateApprovedField."' value='Denied'".($thisMeasureRebateApproved == "Denied" ? " checked='checked'" : "")."> Denied</span>
											<span id='PreviouslyApproved".$thisMeasureRebateApprovedField."' style='display:none'></span>
											</div>"; 
										$belowDisplay = "<br clear='all'>".$belowFieldDisplayText."<input id=\"".$thisMeasureRebateApprovedField."Yes\" name=\"".$thisMeasureRebateApprovedField."\" type=\"radio\" value=\"Yes\"".$thisValueChecked.">".$thisValueNotChecked;

									}
									$thisInfo = "";
									if (strpos(" ".$measureFieldDisplayText,"Manufacturer") || strpos(" ".$measureFieldDisplayText,"Model")){
										$thisInfo = " class='checkApproved ".$thisMeasureRebateApprovedField."' data-fieldname='".$thisMeasureRebateApprovedField."'";
										$checkApproved = true;
									}
									
								?>
									<div class="three columns">
										<?php echo $measureFieldDisplayText;?>
										<input name="<?php echo $thisMeasureFieldName;?>" type="<?php echo $inputType;?>" value="<?php echo $thisDisplayValue;?>"<?php echo $thisInfo;?>>
										<?php echo $belowDisplay;?>
									</div>
								<?php
									if ($thisCounter == 4){
										echo "<br clear='all'>";
										$thisCounter = 0;
									}
								}
							?>
						</div>
					</li>
					<?php
					}
				?>
				</ul>
			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="WiFi">
	</form>
</div><!--asDisplay-->
<div id="onlineRebateResults"></div>
<?php include_once('termsConditions.php');?>