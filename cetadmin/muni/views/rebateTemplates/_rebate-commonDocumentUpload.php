<?php
	$documentList = explode(",",$DocumentationFieldValue["SupportingFile"]);
	foreach ($documentList as $documentId=>$documentName){
		if (trim($documentName)){
			$documentFileLocation = $siteRoot.$adminFolder."muni/rebateAttachments/".$documentName;
			$documentURLLocation = $CurrentServer.$adminFolder."muni/rebateAttachments/".$documentName;
			if (!file_exists($documentFileLocation)){
				$documentFileLocation = $siteRoot.$adminFolder."muni/rebateAttachments/AWS/".$documentName;
				$documentURLLocation = $CurrentServer.$adminFolder."muni/rebateAttachments/AWS/".$documentName;
			}
			$documentImageReviewLink .= " <a id=\"documentImage".$rebateId."_".$documentId."\" href=\"".$documentURLLocation."\" target=\"".$documentName."\" title=\"Click for file ".$documentName."\">".(strpos($documentName,".pdf") ? "<img src=\"".$CurrentServer."images/file_pdf.png\" style=\"height:20;border:0pt solid black;position:relative;top:3px;\">" : "<img src=\"".$CurrentServer."images/file_attached.png\" style=\"height:20;border:0pt solid black;position:relative;top:3px;\">")."</a>";
		}
	}
?>
		<div class="<?php echo ($documentationInfoMissing ? "opened" : "");?>">
			<hr>
			Documentation Files <?php echo $documentImageReviewLink;?> <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="row closedDetails<?php echo ($documentationInfoMissing ? " missingDetails" : "");?>">
				<div class="twelve columns">
					<?php if (!$DocumentationFieldValue["SupportingFile"]){?>
						Upload Supporting Document<Br>
						<input type="file" id="supportingFile" name="fileButton"><span id="fileuploadStatus"></span><br>
						<input type="hidden" name="supportingFile" value="" id="supportingFileValue">
						If you need to upload multiple documents, please zip and upload as a single file. If you require instruction on how to zip files, please refer to the link below.
						<br><br>
						Zip Instructions
						<a href="http://windows.microsoft.com/en-ca/windows/compress-uncompress-files-zip-files#1TC=windows-7" target="zipInstructions">Zip Instructions</a><br>
						Note: This link opens in a new window
					<?php }else{
							foreach ($documentList as $documentId=>$documentName){
								if (trim($documentName)){
									echo "<a href=\"".$CurrentServer.$adminFolder."muni/rebateAttachments/".$documentName."\" target=\"".$documentName."\" id=\"documentTarget".$rebateId."_".$documentId."\">".$documentName."</a>";
									if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni))){
										$deleteRebateLink = "<img src='".$CurrentServer."images/redx-small.png' style='text-align;right;' class='deleteDocument' data-rebateid='".$rebateId."' data-documentid='".$documentId."' data-rebatename='".str_replace(" ","",$rebateName)."' data-customerid='".$SelectedCustomerID."' data-documentname='".$documentName."'><span id='DeleteDocumentResults'></span>";
										echo $deleteRebateLink;
									}
									echo "&nbsp;&nbsp;";
								}
							}
							echo '<input type="hidden" name="supportingFile" value="'.implode(",",$documentList).'" id="supportingFileValueAdditional">';

							//if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni))){
								echo '
									<div>
										Upload Additional Supporting Document<Br>
										<input type="file" id="supportingFileAdditional"><span id="fileuploadStatusAdditional"></span><br>
									</div>
								';
							//}
					}?>
				</div>
			</div>
		</div>
		
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		// Variable to store your files
		var files;

		// Add events
		$('#supportingFile').on('change', prepareUpload);
		$('#supportingFileAdditional').on('change', prepareUploadAdditional);

		// Grab the files and set them to our variable
		function prepareUpload(event)
		{
		  files = event.target.files;
		// Create a formdata object and add the files
			var data = new FormData();
			$.each(files, function(key, value)
			{
				data.append(key, value);
			});

			$.ajax({
				url: "ApiCustomerData.php?action=fileupload&formName=<?php echo $rebateName;?>&oid=<?php echo ($RebateInfoByCustomerID[$SelectedCustomerID]->oid ? $RebateInfoByCustomerID[$SelectedCustomerID]->oid : "CET_OID");?>",
				type: 'POST',
				data: data,
				cache: false,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR)
				{
					$("#supportingFileValue").val(data);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					// Handle errors here
					console.log('ERRORS: ' + textStatus);
					// STOP LOADING SPINNER
				},
				xhrFields: {
				  // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
				  onprogress: function (progress) {
					// calculate upload progress
					var percentage = Math.floor((progress.total / progress.total) * 100);
					// log upload progress to console
					if (percentage === 100) {
						$("#fileuploadStatus").html('File Successfully Uploaded. Besure to click the Save Button below.').addClass("alert").addClass("info");
					}
				  }
				}
			});		  
		  
		}		
		function prepareUploadAdditional(event)
		{
		  files = event.target.files;
		// Create a formdata object and add the files
			var data = new FormData();
			$.each(files, function(key, value)
			{
				data.append(key, value);
			});
			$.ajax({
				url: "ApiCustomerData.php?action=fileuploadAdditional&formName=<?php echo str_replace(" ","",$rebateName);?>&oid=<?php echo ($RebateInfoByCustomerID[$SelectedCustomerID]->oid ? $RebateInfoByCustomerID[$SelectedCustomerID]->oid : "CET_OID");?>&rebateId=<?php echo $rebateId;?>",
				type: 'POST',
				data: data,
				cache: false,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR)
				{
					$("#fileuploadStatusAdditional").html('File Successfully Uploaded. Besure to click the Save Button below.').addClass("alert").addClass("info");
					$("#supportingFileValueAdditional").val(data);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					// Handle errors here
					console.log('ERRORS: ' + textStatus);
					// STOP LOADING SPINNER
				},
				xhrFields: {
				  // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
				  onprogress: function (progress) {
					// calculate upload progress
					var percentage = Math.floor((progress.total / progress.total) * 100);
					// log upload progress to console
					if (percentage === 100) {
						$("#fileuploadStatusAdditional").html('File Successfully Uploaded. Besure to click the Save Button below.').addClass("alert").addClass("info");
					}
				  }
				}
			});		  
		  
		}		

	/*	
		// grab your file object from a file input
		$('#supportingFile').on('change',function(){
			sendFile(this.files[0]);
		});

		function sendFile(file) {
			var data = new FormData(file[0]);
			console.log(file);
			console.log(data);
			$("#fileuploadStatus").html('').removeClass('alert').removeClass('info');
			$.ajax({
				type: 'POST',
				url: "ApiCustomerData.php?action=fileupload&formName=<?php echo $rebateName;?>&oid=<?php echo ($RebateInfoByCustomerID[$SelectedCustomerID]->oid ? $RebateInfoByCustomerID[$SelectedCustomerID]->oid : "CET_OID");?>",
				cache: false,
				dataType: 'json',
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				data: file,
				success: function (data) {
					console.log(data);
				  // do something
			},
			xhrFields: {
			  // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
			  onprogress: function (progress) {
				// calculate upload progress
				var percentage = Math.floor((progress.total / progress.total) * 100);
				// log upload progress to console
				console.log('progress', percentage);
				if (percentage === 100) {
					$("#fileuploadStatus").html('File Successfully Uploaded').addClass("alert").addClass("info");
				}
			  }
			},
			processData: false,
			contentType: file.type
			});
		}	
		
	*/
	});
</script>