<h3>Commercial Heat<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-commercialheatHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($facilityInfoMissing ? "opened" : "");?>">
			<hr>
			Facility Information <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($facilityInfoMissing ? " missingDetails" : "");?>">

				<div class="row">
					<div class="four columns">
						Facility Name: <br>
						<input name="facilityName" type="text" value="<?php echo $FacilityFieldValue["FacilityName"];?>">
					</div>
					<div class="four columns">
						Facility Contact: <br>
						<input name="facilityContact" type="text" value="<?php echo $FacilityFieldValue["FacilityContact"];?>">
					</div>
				</div>
				<div class="row">
					<div class="five columns">
						Facility Contact Phone Number: <br>
						<input name="facilityPhone" type="text" value="<?php echo $FacilityFieldValue["FacilityPhone"];?>">
					</div>
					<div class="four columns">
						Facility Contact Email:<br> 
						<input name="facilityEmail" type="text" value="<?php echo $FacilityFieldValue["FacilityEmail"];?>">
					</div>
				</div>
				<div class="row">
					<div class="twelve columns">
						Installed Address: <br>
						<input name="facilityInstalledAddress" type="text" value="<?php echo $FacilityFieldValue["FacilityInstalledAddress"];?>" style="width:680px;">
					</div>
				</div>
				<div class="row">
					<div class="four columns">
						City: <br>
						<input name="facilityInstalledCity" type="text" value="<?php echo $FacilityFieldValue["FacilityInstalledCity"];?>">
					</div>
					<div class="four columns">
						State: <br>
						<input name="facilityInstalledState" type="text" value="<?php echo $FacilityFieldValue["FacilityInstalledState"];?>">
					</div>
					<div class="four columns">
						Zip: <br>
						<input name="facilityInstalledZip" type="text" value="<?php echo $FacilityFieldValue["FacilityInstalledZip"];?>">
					</div>
				</div>
<!--
				<div class="row">
					<div class="four columns">
						Main Building Sqr Footage: <br>
						<input name="facilitySqrFtMainBuilding" type="text" value="<?php echo $FacilityFieldValue["FacilitySqrFtMainBuilding"];?>">
					</div>
					<div class="four columns">
						Other Buildings Sqr Footage: <br>
						<input name="facilitySqrFtOtherBuildings" type="text" value="<?php echo $FacilityFieldValue["FacilitySqrFtOtherBuildings"];?>">
					</div>
					<div class="four columns">
						Age of Facility (in years):<br> 
						<input name="facilityApproximateAgeInYears" type="text" value="<?php echo $FacilityFieldValue["FacilityApproximateAgeInYears"];?>">
					</div>
				</div>
-->
				<div class="row">
					Facility Type:<br clear="all">
					<div class=" columns">
						<?php $FacilityType = explode(",",$FacilityFieldValue["FacilityType"]);?>
						<input type="checkbox" name="facilityType" value="Commercial (Wholesale/Retail)"<?php echo (in_array("Commercial (Wholesale/Retail)",$FacilityType) ? " checked" : "");?>>Commercial (Wholesale/Retail)<br>
						<input type="checkbox" name="facilityType" value="Multi-family (5 Units or more)"<?php echo (in_array("Multi-family (5 Units or more)",$FacilityType) ? " checked" : "");?>>Multi-family (5 Units or more)<Br>
						<input type="checkbox" name="facilityType" value="Government"<?php echo (in_array("Government",$FacilityType) ? " checked" : "");?>>Government<Br>
						<input type="checkbox" name="facilityType" value="Industrial"<?php echo (in_array("Industrial",$FacilityType) ? " checked" : "");?>>Industrial
					</div>
					<div class="three columns">
						<input type="checkbox" name="facilityType" value="Not-for-profit"<?php echo (in_array("Not-for-profit",$FacilityType) ? " checked" : "");?>>Not-for-profit<br>
						<input type="checkbox" name="facilityType" value="Restaurant"<?php echo (in_array("Restaurant",$FacilityType) ? " checked" : "");?>>Restaurant<Br>
						<input type="checkbox" name="facilityType" value="Healthcare"<?php echo (in_array("Healthcare",$FacilityType) ? " checked" : "");?>>Healthcare<Br>
						<input type="checkbox" name="facilityType" value="K-12 School"<?php echo (in_array("K-12 School",$FacilityType) ? " checked" : "");?>>K-12 School
					</div>
					<div class="three columns">
						<input type="checkbox" name="facilityType" value="Warehouse"<?php echo (in_array("Warehouse",$FacilityType) ? " checked" : "");?>>Warehouse<br>
						<input type="checkbox" name="facilityType" value="Agriculture"<?php echo (in_array("Agriculture",$FacilityType) ? " checked" : "");?>>Agriculture<Br>
						<input type="checkbox" name="facilityType" value="College/University"<?php echo (in_array("College/University",$FacilityType) ? " checked" : "");?>>College/University<Br>
					</div>
					<div class="five columns">
						<input type="checkbox" name="facilityType" value="Hospitality"<?php echo (in_array("Hospitality",$FacilityType) ? " checked" : "");?>>Hospitality<br>
						<input type="checkbox" name="facilityType" value="Office"<?php echo (in_array("Office",$FacilityType) ? " checked" : "");?>>Office<br>
						<input type="checkbox" name="facilityType" value="Other - add to comments below"<?php echo (in_array("Other - add to comments below",$FacilityType) ? " checked" : "");?>>Other - add to comments below<Br>
					</div>
				</div>
				<div class="row">
					<div class="twelve columns">
						Brief Overview of Space Use & Processes:<br>
						What is the space used for generally? Is the space utilized differently during each season?<br>
						<textarea name="facilityOverviewOfSpace"><?php echo $FacilityFieldValue["FacilityOverviewOfSpace"];?></textarea>

					</div>
				</div>

			</div>
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Energy Efficiency Opportunities - Measures / Equipment Identification <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<ul>
				<?php
					$measureTypes = array("High Efficiency Furnace >95 AFUE"=>"Furnace95","High Efficiency Furnace >97 AFUE"=>"Furnace97","High Efficiency HydronicBoiler >85 AFUE"=>"Boiler85","High Efficiency HydronicBoiler >90 AFUE"=>"Boiler90","High Efficiency HydronicBoiler >95 AFUE"=>"Boiler95","Integrated Water Heater and Condensing Boiler >90 AFUE"=>"Integrated90","Integrated Water Heater and Condensing Boiler >95 AFUE"=>"Integrated95","High Efficiency Indirect Water Heater"=>"IndirectWater","Water Heating Storage >62 EF"=>"H2OStorage62","Water Heating Storage >67 EF"=>"H2OStorage67","Tankless Water Heater >82 EF"=>"Tankless82","Tankless Water Heater >94 EF"=>"Tankless94");
					$measureFields = array("Manufacturer","Model Number","Efficiency Rating","Input/Size","Installed Cost","Quantity Installed","Rebate Amount");
					foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
					?><li>
						<div class="row">
							<b><?php echo $measureTypeDisplay;?>:</b>
							<br clear="all">
							<?php 
								$thisCounter = 0;
								foreach ($measureFields as $measureFieldDisplay){
									$thisCounter++;
									$thisMeasureFieldName = "measure_".$measureTypeInputName.str_replace("/","",str_replace(" ","",$measureFieldDisplay));
									$thisValue = $RebateFieldValue["rebateCommercialHeat"][ucfirst($thisMeasureFieldName)];
									$thisMeasureManufacturer = $RebateFieldValue["rebateHeatHotwater"][ucfirst("measure_".$measureTypeInputName."Manufacturer")];
									$thisDisplayValue = "";
									$inputType="text";
									$thisValueChecked = "";
									$thisValueNotChecked = "";
									$belowDisplay = "";
									if ($measureFieldDisplay == "Rebate Amount" || $measureFieldDisplay == "Installed Cost"){
										$thisValue = ($thisValue == "0.00" ? "" : $thisValue);
										if ($thisValue && $thisValue !="0.00"){
											$thisValue = "$".money($thisValue);
										}
									}
									$thisDisplayValue = $thisValue;
									$measureFieldDisplayText = $measureFieldDisplay."<br>";
									$thisMeasureRebateApprovedField = "measure_".$measureTypeInputName."Approved";
									if ($measureFieldDisplay == "Rebate Amount"){
										$thisMeasureRebateAmount = $RebateFieldValue["rebateCommercialHeat"][ucfirst("measure_".$measureTypeInputName."RebateAmount")];
										$thisMeasureRebateApproved = $RebateFieldValue["rebateCommercialHeat"][ucfirst($thisMeasureRebateApprovedField)];
										if ($thisMeasureManufacturer || ($thisMeasureRebateAmount && $thisMeasureRebateAmount != "0.00")){
											$spanStyleYes = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#00CC00" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid green;cursor:pointer;";
											$spanStyleNo = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#ffffff" : "orange").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$spanStyleDenied = "background-color:".($thisMeasureRebateApproved == "Denied" ? "red" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$divStyle = "display:block";
										}else{
											$spanStyleYes = "padding-right:5px;border:1pt solid green;cursor:pointer;padding-top:2px;";
											$spanStyleNo = "padding-right:5px;border:1pt solid orange;cursor:pointer;padding-top:2px;";
											$spanStyleDenied = "padding-right:5px;border:1pt solid red;cursor:pointer;padding-top:2px;";
											$divStyle = "display:none";
										}
										$belowFieldDisplayText = "<div style='font-size:10pt;".$divStyle."'>Approved: <span style='".$spanStyleYes."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Yes').checked=true;\">";
										$thisValueChecked = ($thisMeasureRebateApproved == "Yes" ? " checked='checked'" : "");
										$thisValueNotChecked = "Yes</span>
											<br><span style='margin-left:64px;".$spanStyleNo."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."No').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."No' name='".$thisMeasureRebateApprovedField."' value='No'".($thisMeasureRebateApproved == "Yes" ? "" : " checked='checked'")."> No</span>
											<br><span style='margin-left:64px;".$spanStyleDenied."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Denied').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."Denied' name='".$thisMeasureRebateApprovedField."' value='Denied'".($thisMeasureRebateApproved == "Denied" ? " checked='checked'" : "")."> Denied</span>
											<span id='PreviouslyApproved".$thisMeasureRebateApprovedField."' style='display:none'></span>
											</div>"; 
										$belowDisplay = "<br clear='all'>".$belowFieldDisplayText."<input id=\"".$thisMeasureRebateApprovedField."Yes\" name=\"".$thisMeasureRebateApprovedField."\" type=\"radio\" value=\"Yes\"".$thisValueChecked.">".$thisValueNotChecked;

									}
									$thisInfo = "";
									if (strpos(" ".$measureFieldDisplayText,"Manufacturer") || strpos(" ".$measureFieldDisplayText,"Model")){
										$thisInfo = " class='checkApproved ".$thisMeasureRebateApprovedField."' data-fieldname='".$thisMeasureRebateApprovedField."'";
										$checkApproved = true;
									}
									
								?>
									<div class="three columns">
										<?php echo $measureFieldDisplayText;?>
										<input name="<?php echo $thisMeasureFieldName;?>" type="<?php echo $inputType;?>" value="<?php echo $thisDisplayValue;?>"<?php echo $thisInfo;?>>
										<?php echo $belowDisplay;?>
									</div>
								<?php
									if ($thisCounter == 4){
										echo "<br clear='all'>";
										$thisCounter = 0;
									}
								}
							?>
						</div>
					</li>
					<?php
					}
				?>
				</ul>
				<br clear="all">
				<div class="row">
					<div class="four columns">
						Total Anticipated Rebate<br>
						<input type="text" name="totalAnticipatedRebate" value="<?php echo ($RebateFieldValue["rebateCommercialHeat"]["TotalAnticipatedRebate"] ? "$".money($RebateFieldValue["rebateCommercialHeat"]["TotalAnticipatedRebate"]) : "");?>">
					</div>
					<div class="four columns">
						Installed Cost<br>
						<input type="text" name="installedCost" value="<?php echo ($RebateFieldValue["rebateCommercialHeat"]["InstalledCost"] ? "$".money($RebateFieldValue["rebateCommercialHeat"]["InstalledCost"]) : "");?>">
					</div>
					<div class="four columns">
						Installation Date<br>
						<input type="text" name="installedDate" class="date" value="<?php echo MySQLDate($RebateFieldValue["rebateCommercialHeat"]["InstalledDate"]);?>">
					</div>
				</div>
			</div><!--closedDetails-->
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="CommercialHeat">
	</form>
	<div id="onlineRebateResults"></div>
	<?php include_once('termsConditions.php');?>
</div> <!--end Div container -->