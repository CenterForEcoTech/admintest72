<a href="#" id="save" class="button-link">Save</a><a href="#" style="display:none;" id="backToCustomerDetails" class="button-link">Back To Customer Details</a>
		<a href="#" id="saveHidden" class="button-link" style="display:none;">Save Hidden</a>
		<input type="hidden" id="customerId" name="customerID" value="<?php echo $SelectedCustomerID;?>">
		<input type="hidden" id="utilityAccountNumberHidden" name="utilityAccountNumber" value="<?php echo ($newCustomerAccountNumber ? $newCustomerAccountNumber : $CustomerByID[$SelectedCustomerID]->utilityAccountNumber);?>">
		<input type="hidden" name="id" value="<?php echo $RebateInfoByCustomerID[$SelectedCustomerID]->{$ThisRebateFieldHeader."_ID"};?>">
		<input type="hidden" name="action" value="rebate_<?php echo (count($RebateInfoByCustomerID[$SelectedCustomerID]) ? "update" : "add");?>">
		<input type="hidden" name="secret" value="skeletonCET35">
		<input type="hidden" name="ipAddress" value="<?php echo ($RebateInfoByCustomerID[$SelectedCustomerID]->ipAddress ? $RebateInfoByCustomerID[$SelectedCustomerID]->ipAddress : "CET_IPAddress");?>">
		<input type="hidden" name="oid" value="<?php echo ($RebateInfoByCustomerID[$SelectedCustomerID]->oid ? $RebateInfoByCustomerID[$SelectedCustomerID]->oid : "CET_OID");?>">