<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			Rebate for Wireless Thermostat Installation
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<center>Westfield Gas & Electric</center>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="seven columns">
			<u>Eligible Thermostats</u><br>
			<b>This Brand</b><br>
			<b>That Brand</b><br>
			<br>
			<b>Note:</b> Some Note Here
		</div>
		<div class="five columns">
			<u>Rebate</u><br>
			50%, up to $300<br>
			50%, up to $300<br>
			<br>
		</div>
	</div>
	<br clear="all">
	<div class="row">
		<div class="twelve columns">
			<b>Eligible Projects</b><Br>
			Is this necessary
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			<b>How to Apply</b><br>
			<ul>
				<li style="list-style-type:circle;">Please contact the HELPS toll-free line at: 1-888-333-7525 to schedule a Free Home Energy Audit, then after measure(s) installation contact HELPS to schedule post installation inspection. The inspector will conduct a site visit and complete the required inspection form. The inspection must be performed before submitting a rebate application.</li>
				<br>
				<li style="list-style-type:circle;">Complete and submit this application here with the signed inspection form and invoice attached. When submitting the application, <b>please attach originals of all dated receipts/work orders that document the installation. Submission must be postmarked by 1/31/16</b>.</li>
				<Br>
				These receipts and/or work orders should include the name, license number, address, and phone number of the contractor that completed the installation. You may wish to retain a copy of all documents for your records.<br>
				<Br>
				If you have any questions, or if you would like assistance in completing this form, call our toll free number at 1-888-333-7525.
				<br>
			</ul>
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>