<h3>Heat & Hot Water<?php echo $titleDisplay;?></h3>
<?php include_once('views/rebateTemplates/_rebate-heathotwaterHeader.php');?>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
	<form id="onlineRebateForm">
		<?php include_once('views/rebateTemplates/_rebate-commonContractorInfo.php');?>
		<div class="<?php echo ($measureInfoMissing ? "opened" : "");?>">
			<hr>
			Energy Efficiency Opportunities - Measures / Equipment Identification <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<div class="closedDetails<?php echo ($measureInfoMissing ? " missingDetails" : "");?>">
				<ul>
				<?php
					$measureTypes = array("High Efficiency Furnace >95 AFUE"=>"Furnace95","High Efficiency Furnace >97 AFUE"=>"Furnace97","High Efficiency HydronicBoiler >85 AFUE"=>"Boiler85","High Efficiency HydronicBoiler >90 AFUE"=>"Boiler90","High Efficiency HydronicBoiler >95 AFUE"=>"Boiler95","Integrated Water Heater and Condensing Boiler >90 AFUE"=>"Integrated90","Integrated Water Heater and Condensing Boiler >95 AFUE"=>"Integrated95","High Efficiency Indirect Water Heater"=>"IndirectWater","Water Heating Storage >62 EF"=>"H2OStorage62","Water Heating Storage >67 EF"=>"H2OStorage67","Tankless Water Heater >82 EF"=>"Tankless82","Tankless Water Heater >94 EF"=>"Tankless94");
					$measureFields = array("Manufacturer","Model Number","Efficiency Rating","Input/Size","InstalledCost","Quantity Installed","Rebate Amount");
					foreach ($measureTypes as $measureTypeDisplay=>$measureTypeInputName){
					?><li>
						<div class="row">
							<b><?php echo $measureTypeDisplay;?>:</b>
							<br clear="all">
							<?php 
								$thisCounter = 0;
								foreach ($measureFields as $measureFieldDisplay){
									$thisCounter++;
									$thisMeasureFieldName = "measure_".$measureTypeInputName.str_replace("/","",str_replace(" ","",$measureFieldDisplay));
									$thisValue = $RebateFieldValue["rebateHeatHotwater"][ucfirst($thisMeasureFieldName)];
									$thisMeasureManufacturer = $RebateFieldValue["rebateHeatHotwater"][ucfirst("measure_".$measureTypeInputName."Manufacturer")];
									
									$thisDisplayValue = "";
									$inputType="text";
									$thisValueChecked = "";
									$thisValueNotChecked = "";
									$thisStyle = "";
									$belowDisplay = "";
									if ($measureFieldDisplay == "Rebate Amount" || $measureFieldDisplay == "Installed Cost"){
										$thisValue = ($thisValue == "0.00" ? "" : $thisValue);
										if ($thisValue && $thisValue !="0.00"){
											$thisValue = "$".money($thisValue);
										}
									}
									$thisDisplayValue = $thisValue;
									$measureFieldDisplayText = $measureFieldDisplay."<br>";
									$thisMeasureRebateApprovedField = "measure_".$measureTypeInputName."Approved";
									if ($measureFieldDisplay == "Rebate Amount"){
										$thisMeasureRebateAmount = $RebateFieldValue["rebateHeatHotwater"][ucfirst("measure_".$measureTypeInputName."RebateAmount")];
										$thisMeasureRebateApprovedField = "measure_".$measureTypeInputName."Approved";
										$thisMeasureRebateApproved = $RebateFieldValue["rebateHeatHotwater"][ucfirst($thisMeasureRebateApprovedField)];
										if ($thisMeasureManufacturer || ($thisMeasureRebateAmount && $thisMeasureRebateAmount != "0.00")){
											$spanStyleYes = "background-color:".($thisMeasureRebateApproved == "Yes" ? "#00CC00" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid green;cursor:pointer;";
											$spanStyleNo = "background-color:".($thisMeasureRebateApproved == "No" ? "orange" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$spanStyleDenied = "background-color:".($thisMeasureRebateApproved == "Denied" ? "red" : "#ffffff").";padding-top:2px;padding-right:5px;border:1pt solid red;cursor:pointer;";
											$divStyle = "display:block";
										}else{
											$spanStyleYes = "padding-right:5px;border:1pt solid green;cursor:pointer;padding-top:2px;";
											$spanStyleNo = "padding-right:5px;border:1pt solid orange;cursor:pointer;padding-top:2px;";
											$spanStyleDenied = "padding-right:5px;border:1pt solid red;cursor:pointer;padding-top:2px;";
											$divStyle = "display:none";
										}
										$belowFieldDisplayText = "<div style='font-size:10pt;".$divStyle."' id='ApprovalDiv".$thisMeasureRebateApprovedField."'>Approved: <span style='".$spanStyleYes."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Yes').checked=true;\">";
										$thisValueChecked = ($thisMeasureRebateApproved == "Yes" ? " checked='checked'" : "");
										$thisValueNotChecked = "Yes</span>
											<br><span style='margin-left:64px;".$spanStyleNo."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."No').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."No' name='".$thisMeasureRebateApprovedField."' value='No'".($thisMeasureRebateApproved == "Yes" ? "" : " checked='checked'")."> No</span>
											<br><span style='margin-left:64px;".$spanStyleDenied."' onClick=\"document.getElementById('".$thisMeasureRebateApprovedField."Denied').checked=true;\"><input type='radio' id='".$thisMeasureRebateApprovedField."Denied' name='".$thisMeasureRebateApprovedField."' value='Denied'".($thisMeasureRebateApproved == "Denied" ? " checked='checked'" : "")."> Denied</span>
											<span id='PreviouslyApproved".$thisMeasureRebateApprovedField."' style='display:none'></span>											
											</div>"; 
										$belowDisplay = "<br clear='all'>".$belowFieldDisplayText."<input id=\"".$thisMeasureRebateApprovedField."Yes\" name=\"".$thisMeasureRebateApprovedField."\" type=\"radio\" value=\"Yes\"".$thisValueChecked.">".$thisValueNotChecked;
									}
									$thisInfo = "";
									if (strpos(" ".$measureFieldDisplayText,"Manufacturer") || strpos(" ".$measureFieldDisplayText,"Model")){
										$thisInfo = " class='checkApproved ".$thisMeasureRebateApprovedField."' data-fieldname='".$thisMeasureRebateApprovedField."'";
										$checkApproved = true;
									}
									
								?>
									<div class="three columns">
										<?php echo $measureFieldDisplayText;?>
										<input name="<?php echo $thisMeasureFieldName;?>" type="<?php echo $inputType;?>" value="<?php echo $thisDisplayValue;?>" style="<?php echo $thisStyle;?>"<?php echo $thisInfo;?>>
										<?php echo $belowDisplay;?>
									</div>
								<?php
									if ($thisCounter == 3){
										echo "<br clear='all'>";
									}
								}
							?>
						</div>
					</li>
					<?php
					}
				?>
				</ul>
				<br clear="all">
				<div class="row">
					<div class="four columns">
						Total Anticipated Rebate<br>
						<input type="text" name="totalAnticipatedRebate" value="<?php echo ($RebateFieldValue["rebateHeatHotwater"]["TotalAnticipatedRebate"] ? "$".money($RebateFieldValue["rebateHeatHotwater"]["TotalAnticipatedRebate"]) : "");?>">
					</div>
					<div class="four columns">
						Installed Cost<br>
						<input type="text" name="installedCost" value="<?php echo ($RebateFieldValue["rebateHeatHotwater"]["InstalledCost"] ? "$".money($RebateFieldValue["rebateHeatHotwater"]["InstalledCost"]) : "");?>">
					</div>
					<div class="four columns">
						Installation Date<br>
						<input type="text" name="installedDate" class="date" value="<?php echo MySQLDate($RebateFieldValue["rebateHeatHotwater"]["InstalledDate"]);?>">
					</div>
				</div>
			</div><!--closedDetails-->
		</div>
		<?php include_once('views/rebateTemplates/_rebate-commonDocumentUpload.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonCustomerAcknowledgement.php');?>
		<?php include_once('views/rebateTemplates/_rebate-commonFooter.php');?>
		<input type="hidden" name="formName" value="HeatHotwater">
	</form>
	<div id="onlineRebateResults"></div>
	<?php include_once('termsConditions.php');?>
</div> <!--end Div container -->