<div class="rebateHeader">
	<div class="row">
		<div class="twelve columns">
			<b>Program Overview</b><br>
			HELPS Home Incentive Program is designed to assist the customer with the cost of installation for the most cost effective measures recommended in a home energy audit. Those measures are shown on the energy audit report and have a higher Savings to Investment Ratio (SIR) of greater than one (1). The more cost effective a measure is, the higher the SIR.
			<br>
			<i>Example: Of the measures listed on the audit report, one has an SIR of 10.9 and another has a SIR of 0.85. If the customer installs the measure with the lower SIR and not the measure with the higher SIR, they will not be eligible to receive an incentive. Typically, insulation, air-sealing, heating systems and lighting have the higher SIRs.</i><br> 
			<br>
			All new heating systems receiving an incentive must have an Energy Star certification.<br>
			<br>
			Self-installation of insulation is not eligible for rebate.<Br>
			<Br>
			<b>General rule for cost-effective home energy efficiency: Insulate and air-seal first. (Ask the auditor for a list of firms providing air-sealing services).</b>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<center>Westfield Gas & Electric</center>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="seven columns">
			<u>Eligible Measure(s)</u><br>
			<b>Blower Door Test & Air Sealing</b><br>
			<b>Energy Star Rated Heating System</b><br>
			(85% AFUE or greater)<br>
			<b>Insulation</b><br>
			<br>
			<b>Note:</b> Customers that implement more than one eligible measure listed above based on the highest SIR in their Audit Report are eligible for a maximum of $900/customer/year
		</div>
		<div class="five columns">
			<u>Rebate</u><br>
			50%, up to $300<br>
			50%, up to $300<br>
			<br>
			50%, up to $300<br>
		</div>
	</div>
	<br clear="all">
	<div class="row">
		<div class="twelve columns">
			<b>Eligible Projects</b><Br>
			In general, the most cost-effective audit recommendations should be installed first, excluding lighting. Renters should seek approval from their landlord before installing any measures.
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="twelve columns">
			<b>How to Apply</b><br>
			<ul>
				<li style="list-style-type:circle;">Please contact the HELPS toll-free line at: 1-888-333-7525 to schedule a Free Home Energy Audit, then after measure(s) installation contact HELPS to schedule post installation inspection. The inspector will conduct a site visit and complete the required inspection form. The inspection must be performed before submitting a rebate application.</li>
				<br>
				<li style="list-style-type:circle;">Complete and submit this application here with the signed inspection form and invoice attached. When submitting the application, <b>please attach originals of all dated receipts/work orders that document the installation. Submission must be postmarked by 1/31/16</b>.</li>
				<Br>
				These receipts and/or work orders should include the name, license number, address, and phone number of the contractor that completed the installation. You may wish to retain a copy of all documents for your records.<br>
				<Br>
				If you have any questions, or if you would like assistance in completing this form, call our toll free number at 1-888-333-7525.
				<br>
			</ul>
		</div>
	</div>
	<a href="#" class="button-link" id="closeRebateHeader">Hide Rebate Header Info</a>
	<hr>
</div>
