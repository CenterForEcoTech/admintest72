<?php
	$path = getcwd();
	$url = "https://173.13.82.161/admintest/cetadmin/cronjobs/muni_NEATReceiver.php" ;
	if (strpos($path,"xampp")){
		$testingServer = true;
		$url = "https://192.168.2.15/admintest/cetadmin/cronjobs/muni_NEATReceiver.php" ;
	}

	$data = array('TransmitNEAT'=>$_GET['TransmitNEAT'],'SelectedCustomerID'=>$SelectedCustomerID,'AuditDetailsForNEAT'=>$AuditDetailsForNEAT);
	$data_json = json_encode($data);
	$curlHeaders = array('Content-Type: application/json','X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f');
	//echo $url;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$returnedData = curl_exec($ch); //returned in JsonFormat
	$error = curl_error($ch);
	$error_no = curl_errno($ch);
	curl_close($ch);
	//echo "Returned data: ";
	//print_pre($returnedData);
	if(!$returnedData){
		echo $error . '" - Code: ' . $error_no;
		echo "<span class='alert'>Connection to the NEAT software could not be established.  Please contact Lauren Holway at extension 246</span>";
	}
	$SIRReturnedData = json_decode($returnedData);
	//print_pre($SIRReturnedData);
?>