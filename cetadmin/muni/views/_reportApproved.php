<?php
set_time_limit(3000000);
ini_set('memory_limit','14096M');
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$rebateTypes = array("Heat Hot water"=>"rebateHeatHotwater","Efficiency"=>"rebateEfficiency","Cool Home"=>"rebateCoolHome","Pool Pump"=>"rebatePoolPump","Appliance"=>"rebateAppliance","Commercial Heat"=>"rebateCommercialHeat","WiFi"=>"rebateWiFi");
$descriptionFields = array(
	"HeatHotwater"=>array(
		"MuniResRebateHeatHotwater_Measure_Furnace95Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Furnace97Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Boiler85Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Boiler90Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Boiler95Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Integrated90Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Integrated95Manufacturer",
		"MuniResRebateHeatHotwater_Measure_IndirectWaterManufacturer",
		"MuniResRebateHeatHotwater_Measure_H2OStorage62Manufacturer",
		"MuniResRebateHeatHotwater_Measure_H2OStorage67Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Tankless82Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Tankless94Manufacturer"
	),
	"Efficiency"=>array("MuniResRebateEfficiency_EligibleMeasures"),
	"CoolHome"=>array(
		"MuniResRebateCoolHome_Measure_CentralAC16Manufacturer",
		"MuniResRebateCoolHome_Measure_CentralAC18Manufacturer",
		"MuniResRebateCoolHome_Measure_Ductless18Manufacturer",
		"MuniResRebateCoolHome_Measure_Ductless20Manufacturer"
	),
	"PoolPump"=>array("MuniResRebatePoolPump_NewManufacturer"),
	"Appliance"=>array(
		"MuniResRebateAppliance_ApplianceType",
		"MuniResRebateAppliance_ApplianceBrand"
	),
	"CommercialHeat"=>array(
		"MuniComRebateHeatHotwater_Measure_Furnace95Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Furnace97Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Boiler85Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Boiler90Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Boiler95Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Integrated90Manufacturer",
		"MuniResRebateHeatHotwater_Measure_Integrated95Manufacturer",
		"MuniComRebateHeatHotwater_Measure_IndirectWaterManufacturer",
		"MuniComRebateHeatHotwater_Measure_H2OStorage62Manufacturer",
		"MuniComRebateHeatHotwater_Measure_H2OStorage67Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Tankless82Manufacturer",
		"MuniComRebateHeatHotwater_Measure_Tankless94Manufacturer"
	),
	"WiFi"=>array("MuniResRebateWiFi_Measure_WiFiManufacturer")
);
$descriptionHotWaterKeys = array("Furnace"=>"Furnace","Boiler"=>"Boiler","Integrated"=>"Integrated Water Heater and Boiler","IndirectWater"=>"Indirect Water Heater","H2OStorage"=>"Water Heating Storage","Tankless"=>"Tankless Water Heater");
$descriptionCoolHomeKeys = array("CentralAC"=>"Central AC","Ductless"=>"Ductless Mini-Split");
$descriptionEfficiencyKeys = array("EV Charger Bill Credit"=>"EVInstallationApprovedAmount","Blower Door Test & Air Sealing / 50% up to $300"=>"AirSealingApprovedAmount","Insulation / 50% up to $300"=>"InsulationApprovedAmount","Heating System / 50% up to $300"=>"HeatingApprovedAmount");
$descriptionWiFiKeys = array("WiFi"=>"WiFi");

$reportRebateStatus = 5; //5 is approved

$muniProvider = new MuniProvider($dataConn);
$criteria = new stdClass();
$criteria->rebateApproved = true;
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
foreach ($customerCollection as $result=>$record){
	echo "!";
	$CustomerByID[$record->id] = $record;
	//echo $record;
	foreach ($rebateTypes as $arrayName=>$fieldName){
		$fieldName = str_replace("rebate","",$fieldName);
		//echo $fieldName;

		//get list of all rebates for this type for this client
		$criteria = new stdClass();
		$criteria->rebateName=str_replace("rebate","",$fieldName);
		$criteria->customerId = $record->id;
		$rebateInfoResults = $muniProvider->getRebateInfo($criteria);
		$rebateInfoCollection = $rebateInfoResults->collection;
		//echo $rebateName;
		
		$ThisRebateFieldHeader = ($fieldName == "CommercialHeat" ? "MuniComRebateHeatHotwater" : "MuniResRebate".str_replace(" ","",str_replace("Water","water",$fieldName)));
		//echo $ThisRebateFieldHeader."<br>";
		$description = $descriptionFields[$fieldName];
		foreach ($rebateInfoCollection as $rebateResult=>$rebateRecord){
			
			//if ($record->id == 15){print_pre($rebateInfoCollection);}
			if ($rebateRecord->CurrentStatus == $reportRebateStatus){
				//get description fields
				$descriptionText = array();
				$modelText = array();
				foreach ($description as $tableFieldName){
					if ($fieldName == "HeatHotwater" || $fieldName == "CommercialHeat"){
						if ($fieldName == "CommercialHeat"){
							$tableFieldName = str_replace("MuniRes","MuniCom",$tableFieldName);
						}
					//echo "CustomerID=".$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"}." ".$tableFieldName."<br>";
						if ($rebateRecord->{$tableFieldName}){
							$rebateFieldName = str_replace("Manufacturer","RebateAmount",$tableFieldName);
							$approvedFieldName = str_replace("Manufacturer","Approved",$tableFieldName);
							foreach ($descriptionHotWaterKeys as $key=>$text){
								if (strpos($tableFieldName,$key)){
									if ($rebateRecord->{$approvedFieldName} == "Yes"){
										$descriptionText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $text.": ".$rebateRecord->{$tableFieldName};
										$modelFieldName = str_replace("Manufacturer","ModelNumber",$tableFieldName);
										$modelText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $rebateRecord->{$modelFieldName};
										$rebateToCheck[$rebateRecord->id]["record"]=$rebateRecord;
										$rebateToCheck[$rebateRecord->id]["descriptionText"][]=$descriptionText;
										$rebateToCheck[$rebateRecord->id]["modelFieldName"][]=$modelFieldName;
										$rebateToCheck[$rebateRecord->id]["modelText"][]=$modelText;
									}
								}
							}
						}
						$implodekey = ", ";
					}elseif ($fieldName == "CoolHome"){
						//echo "CustomerID=".$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"}." ".$tableFieldName."<br>";
						if ($rebateRecord->{$tableFieldName}){
							$rebateFieldName = str_replace("Manufacturer","TotalRebate",$tableFieldName);
							foreach ($descriptionCoolHomeKeys as $key=>$text){
								if (strpos($tableFieldName,$key)){
									$descriptionText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $text.": ".$rebateRecord->{$tableFieldName};
									$modelFieldName = str_replace("Manufacturer","CondenserModelNumber",$tableFieldName);
									$modelText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $rebateRecord->{$modelFieldName};
								}
							}
						}
						$implodekey = ", ";
					}elseif ($fieldName == "Efficiency"){
						//echo "CustomerID=".$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"}." ".$tableFieldName."<br>";
						if ($rebateRecord->{$tableFieldName}){
							$eligibleMeasures = explode(",",$rebateRecord->{$tableFieldName});
							foreach ($descriptionEfficiencyKeys as $key=>$text){
								if (in_array($key,$eligibleMeasures)){
									$keyParts = explode(" / ",$key);
									$descriptionText[$text][$rebateRecord->{$ThisRebateFieldHeader."_".$text}] = $keyParts[0];
									$modelFieldName = str_replace("EligibleMeasures","MeasuresInstalled",$tableFieldName);
									$modelText[$text][$rebateRecord->{$ThisRebateFieldHeader."_".$text}] = $rebateRecord->{$modelFieldName};
									//.": ".$rebateRecord->{$ThisRebateFieldHeader."_".$text};
								}
							}
						}
						$implodekey = ", ";
					}elseif ($fieldName == "Appliance"){
						$descriptionText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = $rebateRecord->{$tableFieldName};
						$modelFieldName = str_replace("ApplianceBrand","ApplianceModelNumber",$tableFieldName);
						$modelText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = $rebateRecord->{$modelFieldName};
						$implodekey = ": ";
					}elseif ($fieldName == "PoolPump"){
						$descriptionText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = "New Pool Pump: ".$rebateRecord->{$tableFieldName};
						$modelFieldName = str_replace("Manufacturer","ModelNumber",$tableFieldName);
						$modelText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = $rebateRecord->{$modelFieldName};
						$implodekey = ": ";
					}elseif ($fieldName == "WiFi"){
						if ($rebateRecord->{$tableFieldName}){
							$rebateFieldName = str_replace("Manufacturer","TotalAmount",$tableFieldName);
							$approvedFieldName = str_replace("Manufacturer","Approved",$tableFieldName);
							foreach ($descriptionWiFiKeys as $key=>$text){
								if (strpos($tableFieldName,$key)){
									if ($rebateRecord->{$approvedFieldName} == "Yes"){
										$descriptionText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $text.": ".$rebateRecord->{$tableFieldName};
										$modelFieldName = str_replace("Manufacturer","ModelNumber",$tableFieldName);
										$modelText[$rebateFieldName][$rebateRecord->{$rebateFieldName}] = $rebateRecord->{$modelFieldName};
									}
								}
							}
						}
						$implodekey = ", ";
					}else{
						$descriptionText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = str_replace(",",", ",$rebateRecord->{$tableFieldName});
						$modelFieldName = str_replace("Manufacturer","ModelNumber",$tableFieldName);
						$modelText["RebateAmountApproved"][$rebateRecord->RebateAmountApproved] = $rebateRecord->{$modelFieldName};
						$implodekey = ", ";
					}
				}
				foreach ($descriptionText as $rebateAmountNameKey=>$rebateAmountName){
					
					$thisRebateAmountName = key($rebateAmountName);
					//echo "<hr>".$rebateAmountNameKey.$thisRebateAmountName."<hr>";
					foreach ($rebateAmountName as $rebateAmount=>$descriptionDisplay){
	//					$descriptionDisplay = implode($implodekey,$descriptionText);
						$model = $modelText[$rebateAmountNameKey][$rebateAmount];
						$rebates[][$record->id][$arrayName][] = array(
							"rebateId"=>$rebateRecord->{$ThisRebateFieldHeader."_ID"},
							"customerId"=>$rebateRecord->{$ThisRebateFieldHeader."_CustomerID"},
							"approvedAmount"=>$rebateAmount,
							"approvedDate"=>$rebateRecord->ApprovedDate,
							"approvedBy"=>$rebateRecord->ApprovedDateBy,
							"status"=>$rebateRecord->CurrentStatus,
							"descriptionText"=>$descriptionDisplay,
							"model"=>$model
							);
					}
				}
			}
		}
	}
}
//print_pre($rebateToCheck);

$rebateStatusNameResults = $muniProvider->getRebateStatusNames($criteria=null);
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
}
$auditStatusNameResults = $muniProvider->getAuditStatusNames($criteria=null);
$auditStatusNameCollection = $auditStatusNameResults->collection;
foreach ($auditStatusNameCollection as $statusResult=>$statusRecord){
	$AuditStatusNameByID[$statusRecord->id] = $statusRecord->name;
}
//parse through to get the rebatesInfo
//print_pre($rebates);
foreach ($rebates as $rebatecount){
	foreach($rebatecount as $CustomerID=>$approvedRecords){
		//echo "<br>CustomerID=".$CustomerID.":<br>";
		foreach ($approvedRecords as $rebateName=>$rebateInfoArray){
			foreach ($rebateInfoArray as $arrayId=>$arrayInfo){
			//echo $rebateName.$arrayId."|";
				$criteria = new stdClass();
				$criteria->rebateName=str_replace("rebate","",$rebateName);
				$criteria->rebateId = $arrayInfo["rebateId"];
				$rebateInfoResults = $muniProvider->getRebateInfo($criteria);
				$rebateInfoCollection = $rebateInfoResults->collection;
				foreach ($rebateInfoCollection as $id=>$collectionRecord){
					$rebateInfo = array(
						"customerId"=> $arrayInfo["customerId"],
						"type"=> $rebateName,
						"approvedAmount"=> $arrayInfo["approvedAmount"],
						"approvedDate"=> $arrayInfo["approvedDate"],
						"approvedBy"=> $arrayInfo["approvedBy"],
						"descriptionText"=> $arrayInfo["descriptionText"],
						"rebateId"=> $arrayInfo["rebateId"],
						"model"=>$arrayInfo["model"]
					);
					$rebatesCollection[] = $rebateInfo;
				}
				
			}
		}	
	}
}
?>

<h3>Customers with <?php echo $RebateStatusNameByID[$reportRebateStatus];?> Status</h3>
<div class="row">
	<div class="twelve columns">
		<fieldset>
			<legend>Filters</legend>
			Rebate Type: <select id="RebateType" class="parameters">
				<option value="">All Rebate Types</option>
			<?php 
				foreach ($rebateTypes as $arrayName=>$fieldName){
					echo "<option value=\"".$fieldName."\"".($SelectedRebateType==$fieldName ? " selected" : "").">".$arrayName."</option>";
				}
			?></select>
		</fieldset>
	</div>
</div>
<?php //print_pre($rebatesCollection);?>
<div class="row">
	<div class="fifteen columns">
		<div class="savebuttons" class="ten columns" style="text-align:right;">
			<div class="eight columns" id="exportResults">&nbsp;</div>
			<a href="approve-code-update" id="exportButton" class="button-link do-not-navigate info">Export Selected To Fiscal</a>
		</div>
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="max-width:61px;text-align:center;" class="checkColumn page-break"><button id="checkAll" style="cursor:pointer;font-size:8pt;">check all</button></th>
					<th align='left'>VendorID</th>
					<th align='left'>FullName</th>
					<th align='left'>Payable To</th>
					<th align='left'>MailingAddress</th>
					<th align='left'>City</th>
					<th align='left'>State</th>
					<th align='left'>Zip</th>
					<th align='left'>ExpenseAccount</th>
					<th align='left'>RebateType</th>
					<th align='left'>RebateAmount</th>
					<th align='left'>Description</th>
					<th align='left'>ApprovedDate</th>
					<th align='left'>ApprovedBy</th>
					<th align='left'>Model#</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($rebatesCollection as $recordId=>$approvedInfo){
						$customerId = $approvedInfo["customerId"];
						$rebateId  = $approvedInfo["rebateId"];
						$vendorId = str_replace(" ","",substr($CustomerByID[$customerId]->accountHolderFirstName,0,1).substr($CustomerByID[$customerId]->accountHolderLastName,0,4).$CustomerByID[$customerId]->utilityAccountNumber);
						$fullName = $CustomerByID[$customerId]->accountHolderFullName;
						$payableTo = ($CustomerByID[$customerId]->rebateMadePayableTo ? $CustomerByID[$customerId]->rebateMadePayableTo : $CustomerByID[$customerId]->accountHolderFullName);
						$address = ($CustomerByID[$customerId]->mailingAddress ? $CustomerByID[$customerId]->mailingAddress : $CustomerByID[$customerId]->installedAddress);
						$city = ($CustomerByID[$customerId]->mailingCity ? $CustomerByID[$customerId]->mailingCity : $CustomerByID[$customerId]->installedCity);
						$state = ($CustomerByID[$customerId]->mailingState ? $CustomerByID[$customerId]->mailingState : $CustomerByID[$customerId]->installedState);
						$zip = ($CustomerByID[$customerId]->mailingZip ? $CustomerByID[$customerId]->mailingZip : $CustomerByID[$customerId]->installedZip);
						$expenseAccount = "70801";
						$rebateType = $approvedInfo["type"];
						$rebateTypeDisplay = str_replace("Cool Home","Central AC HeatPump",$rebateType);
						$rebateAmount = $approvedInfo["approvedAmount"];
						$descriptionText = $approvedInfo["descriptionText"];
						$approvedDate = $approvedInfo["approvedDate"];
						$approvedBy = $approvedInfo["approvedBy"];
						$modelNumber = $approvedInfo["model"];
						echo "<tr id='row".$recordId."'>
								<td align=\"center\" class=\"checkColumn page-break\">
									<input class=\"toExportCheck"."\" 
										data-vendorid=\"".$vendorId."\" 
										data-fullname=\"".$fullName."\" 
										data-payableto=\"".$payableTo."\" 
										data-address=\"".$address."\" 
										data-city=\"".$city."\" 
										data-state=\"".$state."\" 
										data-zip=\"".$zip."\" 
										data-expenseaccount=\"".$expenseAccount."\" 
										data-rebatetype=\"".$rebateType."\" 
										data-rebateamount=\"".$rebateAmount."\" 
										data-descriptiontext=\"".$descriptionText."\" 
										data-customerid=\"".$customerId."\" 
										data-rowid=\"row".$recordId."\"
									type=\"checkbox\" value=\"".$rebateId."\">
								</td>
								<td valign='top' nowrap='nowrap'>".$vendorId."</td>
								<td valign='top' nowrap='nowrap'>".$fullName."</td>
								<td valign='top' nowrap='nowrap'>".$payableTo."</td>
								<td valign='top' nowrap='nowrap'>".$address."</td>
								<td valign='top' nowrap='nowrap'>".$city."</td>
								<td valign='top' nowrap='nowrap'>".$state."</td>
								<td valign='top' nowrap='nowrap'>".$zip."</td>
								<td valign='top' nowrap='nowrap'>".$expenseAccount."</td>
								<td valign='top' nowrap='nowrap'>".$rebateTypeDisplay."</td>
								<td valign='top' nowrap='nowrap'>".$rebateAmount."</td>
								<td valign='top' nowrap='nowrap'>".$descriptionText."</td>
								<td valign='top' nowrap='nowrap'>".MySQLDate($approvedDate)."</td>
								<td valign='top' nowrap='nowrap'>".$approvedBy."</td>
								<td valign='top' nowrap='nowrap'>".$modelNumber."</td>
							</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var getFilters = function(){
			var rebateType = $("#RebateType").val();
				filterValues = new Array(rebateType);
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>muni?RebateType="+Filters[0]+"&nav=customer-data&SelectedCustomerID=<?php echo $SelectedCustomerID;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$(".parameters").val('');
			updateFilters();
		});
		
		var tableRawData = $('.rawDataReport').dataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
							{
								"sExtends":    "copy"
							}
				]				
				
				
			},			
			select: true,
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 100
		});
		
		$("button").on("click",function(){
			var checkBoxes = $(".toExportCheck");
			$.each(checkBoxes,function(){
				var $this = $(this);
				$this.prop("checked", "checked");
			});
		});
		
		var toExportCheck = $(".toExportCheck");
		$("#exportButton").on('click',function(){
			var ExportToFiscal = {};
			var ExportRecords = new Array();
			var ExportValuesFound = false;
			var checkedRows = new Array();
			$('#exportResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
			$.each($(".toExportCheck"),function(){
				var $this = $(this);
				if($this.prop("checked")){
					var ExportRecord = {};
					checkedRows.push($this.attr('data-rowid'));
					ExportRecord["vendorId"] = $this.attr('data-vendorid');
					ExportRecord["fullName"] = $this.attr('data-fullname');
					ExportRecord["payableTo"] = $this.attr('data-payableto');
					ExportRecord["address"] = $this.attr('data-address');
					ExportRecord["city"] = $this.attr('data-city');
					ExportRecord["state"] = $this.attr('data-state');
					ExportRecord["zip"] = $this.attr('data-zip');
					ExportRecord["expenseAccount"] = $this.attr('data-expenseaccount');
					ExportRecord["rebateType"] = $this.attr('data-rebatetype');
					ExportRecord["rebateAmount"] = $this.attr('data-rebateamount');
					ExportRecord["descriptionText"] = $this.attr('data-descriptiontext');
					ExportRecord["customerId"] = $this.attr('data-customerid');
					ExportRecord["rebateId"] = $this.val();
					ExportRecord["dispersedToFiscal"] = "<?php echo date("m/d/Y");?>";
					ExportRecords.push(ExportRecord);
					ExportValuesFound = true;
				}
			});
			if (ExportValuesFound){
				ExportToFiscal["action"] = "exporttofiscal";
				ExportToFiscal["exportRecords"] = ExportRecords;
				data = JSON.stringify(ExportToFiscal);
				console.log(data);
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(ExportToFiscal),
					success: function(data){
						$("#exportFile").attr('href',data.link).text(data.filename);
						window.location.href = data.link;
						var removeThese = new Array();
						$.each(checkedRows,function(key,val){
							var rowId = "#"+val;
							tableRawData.fnDeleteRow($(rowId));						
						});
						   // Delete the row
						   //tableRawData.fnDeleteRow(aPos);						
						   $('#exportResults').show().html('The file has been sent to your downloads<br><a href="'+data.link+'">'+data.filename+'</a>').removeClass("alert").removeClass("info").addClass("info");
						
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#exportResults').show().html(message).addClass("alert");
					}
				});
			}else{
				$('#exportResults').show().html("You must check at least rebate").removeClass("alert").removeClass("info").addClass("alert");
			}
		});
		
		
	});
</script>