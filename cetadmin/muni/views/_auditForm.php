<?php
ini_set('memory_limit','1024M');
set_time_limit(25000);
//START populated from intake data
			

$intakeConditionedStories = $CustomerIDAuditInfo[$SelectedCustomerID]["auditConditionedStories"];
$intakeHouseWidth = $CustomerIDAuditInfo[$SelectedCustomerID]["auditHouseWidth"];
$intakeHouseLength = $CustomerIDAuditInfo[$SelectedCustomerID]["auditHouseLength"];
$intakeFloorArea = ($CustomerIDAuditInfo[$SelectedCustomerID]["auditFloorArea"] ? $CustomerIDAuditInfo[$SelectedCustomerID]["auditFloorArea"] : ($intakeHouseWidth*$intakeHouseLength*$intakeConditionedStories));
$intakeHousePerimeter = ($intakeHouseWidth*2)+($intakeHouseLength*2);
$intakeHeatingEquipmentType = ($CustomerIDAuditInfo[$SelectedCustomerID]["auditHeatingEquipmentType"] ? $CustomerIDAuditInfo[$SelectedCustomerID]["auditHeatingEquipmentType"] : "Steam Boiler");
$intakeHeatingFuel = ($CustomerIDAuditInfo[$SelectedCustomerID]["auditHeatingFuel"] ? $CustomerIDAuditInfo[$SelectedCustomerID]["auditHeatingFuel"] : "Oil");
//END populated from intake data

$auditAuditCode = "Audit_".date("Y_m_d").str_replace(" ","",$CustomerByID[$SelectedCustomerID]->accountHolderLastName.$CustomerByID[$SelectedCustomerID]->accountHolderFirstName);
$auditAgencyName = "The Center for Eco Technology";
$auditAgencyState = "MA";
$auditorAuditor = $_SESSION["AdminAuthObject"]["adminFullName"];


$auditSetupLibrary = array("CETSetupLibraryCode","CET Setup Library");//value in NEAT and text to be displayed
$auditFuelCostLibrary = array("DefaultCosts","Default Costs");//value in NEAT and text to be displayed
$auditSupplyLibrary = array("CETSupplyLibrary","CET Supply Library");//value in NEAT and text to be displayed
$auditWeatherFile = array("BostonWeather","BOSTONMA,WX");//value in NEAT and text to be displayed

$houseVolume = $intakeFloorArea*$intakeConditionedStories*8; //sqfoot x floors x 8'
$houseNFactors = array("1"=>"19","1.25"=>"17.9","1.5"=>"16.8","1.75"=>"16.1","2"=>"15.4","2.25"=>"14.9","2.5"=>"14.4","2.75"=>"14.05","3"=>"13.7"); //conditionedFloors=>factor

//shared storage for NEAT parameters
include_once('_auditFormNEATParameters.php');

$tabSections = array("auditInfo"=>"Audit Info","shellInfo"=>"Shell Info","unfinishedAtticInfo"=>"Unfinished Attic","finishedAtticKneewallInfo"=>"Finished Attic Kneewall","finishedAtticSlopeInfo"=>"Finished Attic Slope","finishedAtticCapInfo"=>"Finished Attic Cap","foundationInfo"=>"Foundation","heatingInfo"=>"Heating","ductInfo"=>"Building Leakage","boilerPlate"=>"Checkbox Items","notes"=>"Notes");


?>
<style>
	.auditTab {border:1pt solid black;margin:5px;padding:5px;width:90%;display:none;}
	.active {display:block;}
	.auditTabTitle {font-weight:bold;}
	.auditSubSection {border: 1pt dashed black;padding:5px;}
	.toBeHidden {border:1pt dashed red;padding:2px;margin:5px;display:none;}
	.toBeHiddenExplain {border:1pt dashed red;padding:2px;margin:5px;}
	input.houseDimension {width: 50px; height:20px;}
	.instructions {font-weight:bold;}
	.explanation {font-size:8pt;}
	.forPrint {display:none;}
	.pageBreak {page-break-before: always !important; page-break-after: always !important;}
	<?php //if($actualAudit){echo ".onScreen {display:none;}";}?>
	@media print {
		.onScreen {display:none;}
		.forPrint {display:block;float:left;}
		.toBeHidden {display:none;}
		.forPrintAdditional {float:right;}
	}
</style>
<div class="asDisplay">
	<?php include_once('views/rebateTemplates/_rebate-commonContacts.php');?>
</div>
<?php if (!$newCustomerName && $auditStatus){?>
	<ul id="audit-tabs" class="tabs onScreen" data-tabs="tabs">
		<?php
			$setTab = ($_GET['setTab'] ? $_GET['setTab'] : "auditInfo"); //default tab to start on
			foreach ($tabSections as $tabId=>$tabDisplay){
				echo "<li><a id=\"tab-".$tabId."\" href=\"#".$tabId."\">".$tabDisplay."</a></li>\r\n";
				$tabHashes[]='"#'.$tabId.'"';
			}
			echo "<li><a id=\"tab-Attachments\" href=\"#Attachments\">Attachments</a></li>\r\n";
			//hide save button if already recorded
			if (!count($rawFormData)){
				echo "<li><a id=\"tab-AuditSave\" href=\"#AuditSave\">Save</a></li>\r\n";
			}else{
				echo "<li><a id=\"tab-NEAT\" href=\"#NEAT\">NEAT Data</a></li>\r\n";
				echo "<li><a id=\"tab-SIRResults\" href=\"#SIRResults\">SIR Results</a></li>\r\n";
			}
		?>
	</ul>
	<div class="pageBreak">
		<form id="auditFormData">
			<div id="auditInfo" class="auditTab active">
				<div class="auditTabTitle">Audit Info</div>
				<div id="clientId">
					<span class="forPrint">Auditor: <input type="text"></span><br clear="all">
				</div>
				<br clear="both">
				<div id="houseInfo">
					Floor Area (sq ft) <input type="text" value="<?php echo $intakeFloorArea;?>" id="floorArea" name="floorArea" class="houseDimension" disabled> House Width <input type="text" value="<?php echo $intakeHouseWidth;?>" data-area="floorArea" data-otherdimension="houseLength" data-thirddimension="conditionedStories" id="houseWidth" class="houseDimension areaDimension houseVolume"> x Length <input type="text" value="<?php echo $intakeHouseLength;?>" data-area="floorArea" data-otherdimension="houseWidth" data-thirddimension="conditionedStories" id="houseLength" class="houseDimension areaDimension houseVolume"> x Conditioned Stories <input type="text" value="<?php echo $intakeConditionedStories;?>" id="conditionedStories" class="houseDimension houseVolume numbersOnly"> (Populated from intake process)<br>
					AccountNumber: <input type="text" name="auditUtilityAccountNumber" class="customerData" data-fieldname="UtilityAccountNumber" data-clientid="<?php echo $SelectedCustomerID;?>" value="<?php echo $CustomerByID[$SelectedCustomerID]->utilityAccountNumber;?>"><br>
					Email: <input type="text" name="installedEmail" class="customerData" data-fieldname="InstalledEmail" data-clientid="<?php echo $SelectedCustomerID;?>" value="<?php echo $CustomerByID[$SelectedCustomerID]->installedEmail;?>"><br>
				</div>
				<div class="toBeHidden">
					Client: <input type="text" name="auditClientID" value="<?php echo $SelectedCustomerID;?>"><br>
					Audit Name: <input type="text" name="auditAuditCode" value="<?php echo $auditAuditCode;?>"><br>
					Auditor: <input type="text" name="auditorAuditor" value="<?php echo $auditorAuditor;?>"><br>
					Agency Name: <input type="text" name="auditAgencyName" value="<?php echo $auditAgencyName;?>"><br>
					Agency State: <input type="text" name="auditAgencyState" value="<?php echo $auditAgencyState;?>"><br>
					
					<div class="auditSubSection">
						Libraries and Other Options:<br>
						SetupLibrary: <input type="radio" name="auditSetupLibrary" value="<?php echo $auditSetupLibrary[0];?>" checked><?php echo $auditSetupLibrary[1];?><br>
						Fuel Costs Library: <input type="radio" name="auditFuelCostLibrary" value="<?php echo $auditFuelCostLibrary[0];?>" checked><?php echo $auditFuelCostLibrary[1];?><br>
						Supply Library: <input type="radio" name="auditSupplyLibrary" value="<?php echo $auditSupplyLibrary[0];?>" checked><?php echo $auditSupplyLibrary[1];?><br>
						Weather File: <input type="radio" name="auditWeatherFile" value="<?php echo $auditWeatherFile[0];?>" checked><?php echo $auditWeatherFile[1];?><br>
					</div>
					
				</div>
			</div>
			<div id="shellInfo" class="auditTab">
				<div class="auditTabTitle">Shell Info</div>
				<div id="wallInfo">
					<div class="instructions">Only indicate gross area of wall to Insulate.  If no insulation is recommended leave blank</div>
					Wall Gross Area to Insulate (sq ft) <input type="text" id="wallGrossAreaToBeInsulated" name="wallGrossAreaToBeInsulated" value="<?php echo (($intakeConditionedStories*16*$intakeHouseWidth)+($intakeConditionedStories*16*$intakeHouseLenght));?>" class="houseDimension">(conditioned stories:<span id="shellConditionedStories"><?php echo $intakeConditionedStories;?></span> x Floor Area Dimensions W:<span id="shellFloorWidth"><?php echo $intakeHouseWidth;?></span>' x L:<span id="shellFloorLength"><?php echo $intakeHouseLength;?></span>')<br>
					<input type="checkbox" id="entireExterior"> entire exterior walls &nbsp;&nbsp;&nbsp;&nbsp; OR &nbsp;&nbsp;&nbsp;&nbsp; Total Gross Wall Length: <input type="text" id="grossWallCalculator_Width" class="houseDimension grossWallCalculator"> x Total Gross Wall Height: <input type="text" id="grossWallCalculator_Height" class="houseDimension grossWallCalculator" value="8"><Br>
				</div>
				<div class="auditTabTitle">Window Info</div>
				Single Pane Windows <input type="radio" name="singlePaneWidows" value="None" checked>None <input type="radio" name="singlePaneWidows" value="Yes">Some
				<div id="insulationInfo" class="toBeHidden">
					<div class="row">
						<div class="five columns">
							<div class="auditSubSection">
								Wall Code: <input type="radio" name="wallCode" value="<?php echo $wallCode;?>" checked><?php echo $wallCode;?><br>
								Wall Gross Area Hidden <input type="text" id="wallGrossAreaToBeInsulatedHidden" value="" class="houseDimension"><br>
								Wall Type: <input type="radio" name="wallType" value="<?php echo $wallType[0];?>" checked><?php echo $wallType[1];?><br>
								Wall Stud Size: <input type="radio" name="wallStudSize" value="<?php echo $wallStudSize[0];?>" checked><?php echo $wallStudSize[1];?><br>
								Wall Exterior Type: <input type="radio" name="wallExteriorType" value="<?php echo $wallExteriorType[0];?>" checked><?php echo $wallExteriorType[1];?><br>
								Wall Exposed To: <input type="radio" name="wallExposedTo" value="<?php echo $wallExposedTo[0];?>" checked><?php echo $wallExposedTo[1];?><br>
								Wall Orientation: <input type="radio" name="wallOrientation" value="<?php echo $wallOrientation[0];?>" checked><?php echo $wallOrientation[1];?><br>
								Wall Measure #: <input type="radio" name="wallMeasureNumber" id="wallMeasureNumber" value="<?php echo $wallMeasureNumber;?>" checked><?php echo $wallMeasureNumber;?><br>
							</div>
						</div>
						<div class="ten columns">
							<div class="auditSubSection">
								Existing Insulation: <input type="radio" id="existingInsulationYes" name="existingInsulation" value="<?php echo $existingInsulationYes[0];?>" checked>Yes <input type="radio" id="existingInsulationNo" name="existingInsulation" value="<?php echo $existingInsulationNoValue;?>">No <span class="explanation">(yes = <?php echo $existingInsulationYes[1];?> with R-value <input type="text" name="existingInsulationRValue" value="<?php echo $existingInsulationRValue;?>" id="existingInsulationRValue" class="houseDimension">)</span><br>
								Added Insulation: <input type="radio" id="addedInsulationYes" name="addedInsulation" value="<?php echo $addedInsulationYes[0];?>">Yes <input type="radio" id="addedInsulationNo" name="addedInsulation" value="<?php echo $addedInsulationNoValue;?>" checked>No <span class="explanation">(yes = <?php echo $addedInsulationYes[1];?>)</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="unfinishedAtticInfo" class="auditTab">
				<div class="auditTabTitle">Attic Unfinished</div>
				<div>
					<span style="float:left;">Attic Unfinished Type: </span> 
						<span class="forPrint">
						<?php 
							foreach ($atticUnfinishedTypes as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
						</span>
					<select class="onScreen" name="atticUnfinishedType">
						<?php 
							foreach ($atticUnfinishedTypes as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select>
					<br>
				</div>
				<div>
					<span style="float:left;">Attic Unfinished Existing Insulation: </span> 
						<span class="forPrint">
						<?php 
							foreach ($atticUnfinishedExistingInsulation as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
						</span>
					<select class="onScreen" name="atticUnfinishedExistingInsulation">
						<?php 
							foreach ($atticUnfinishedExistingInsulation as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select>
					<span class="forPrintAdditional">
						Existing Depth (in) 
						<select id="atticUnfinishedExistingInsulationDepth" name="atticUnfinishedExistingInsulationDepth" class="onScreen" style="width:50px;">
							<option value="">0</option>
							<option value="3">3</option>
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select> 
						<br>
					</span>

				</div>
				<div>
					Attic Unfinished Area (sqft) to Insulate <input type="text" name="atticUnfinishedAreaSqft" id="atticUnfinishedAreaSqft" value="<?php echo $atticUnfinishedAreaSqft;?>" class="houseDimension"> Width <input type="text" value="<?php echo $intakeHouseWidth;?>" data-area="atticUnfinishedAreaSqft" data-otherdimension="atticUnfinishedLength" id="atticUnfinishedWidth" class="houseDimension areaDimension"> x Length <input type="text" value="<?php echo $intakeHouseLength;?>" data-area="atticUnfinishedAreaSqft" data-otherdimension="atticUnfinishedWidth" id="atticUnfinishedLength" class="houseDimension areaDimension"><br>
					<br>
				</div>
				<div>
					<span style="float:left;">Attic Unfinished Added Insulation: </span>
					<select id="atticUnfinishedAddedInsulation" name="atticUnfinishedAddedInsulation" class="onScreen">
						<?php 
							foreach ($atticUnfinishedAddedInsulation as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select> 
					<span class="forPrint">
						<?php 
							foreach ($atticUnfinishedAddedInsulation as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
					</span>
					
					<span class="forPrintAdditional">
						Added Depth (in) 
						<select id="atticUnfinishedAddedInsulationDepth" name="atticUnfinishedAddedInsulationDepth" class="onScreen" style="width:50px;">
						</select> 
						<br>
					</span>
					<br clear="all">
				</div>
				<div class="toBeHidden">
					Attic Unfinished Code <input type="radio" name="atticUnfinishedCode" value="<?php echo $atticUnfinishedCode;?>" checked><?php echo $atticUnfinishedCode;?><br>
					Attic Unfinished Joist Spacing (in) <input type="radio" name="atticUnfinishedJoistSpacing" value="<?php echo $atticUnfinishedJoistSpacing;?>" checked><?php echo $atticUnfinishedJoistSpacing;?><Br>
					Attic Unfinished Roof Color <input type="radio" name="atticUnfinishedRoofColor" value="<?php echo $atticUnfinishedRoofColor[0];?>" checked><?php echo $atticUnfinishedRoofColor[1];?><Br>
					Attic Unfinished Added Insulation Measure #: <input type="radio" id="atticUnfinishedAddedInsulationMeasureNumber" name="atticUnfinishedAddedInsulationMeasureNumber" value="<?php echo $atticUnfinishedAddedInsulationMeasureNumber;?>"><?php echo $atticUnfinishedAddedInsulationMeasureNumber;?><br>
				</div>
			</div>
			<div id="finishedAtticKneewallInfo" class="auditTab">
				<div class="auditTabTitle">Attic Kneewall</div>
				Kneewall Area (sqft) to Insulate <input type="text" name="atticFinishedKneewallAreaSqft" id="atticFinishedKneewallAreaSqft" value="<?php echo $atticFinishedKneewallAreaSqft;?>" class="houseDimension"> Width <input type="text" value="" data-area="atticFinishedKneewallAreaSqft" data-otherdimension="atticFinishedKneewallLength" id="atticFinishedKneewallWidth" class="houseDimension areaDimension"> x Length <input type="text" value="" data-area="atticFinishedKneewallAreaSqft" data-otherdimension="atticFinishedKneewallWidth" id="atticFinishedKneewallLength" class="houseDimension areaDimension"><br>
				<div>
					<span style="float:left;">Attic Kneewall Existing Insulation: </span> 
						<span class="forPrint">
						<?php 
							foreach ($atticFinishedKneewallExistingInsulation as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
						</span>
					<select class="onScreen" name="atticFinishedKneewallExistingInsulation">
						<?php 
							foreach ($atticFinishedKneewallExistingInsulation as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select>
					<span class="forPrintAdditional">
						Existing Depth (in) 
						<select id="atticFinishedKneewallExistingInsulationDepth" name="atticFinishedKneewallExistingInsulationDepth" class="onScreen" style="width:50px;">
							<option value="">0</option>
							<option value="3">3</option>
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select> 
						<br>
					</span>

				</div>
				<br clear="all">
				<span style="float:left;">Kneewall Added Insulation: </span>
				<select id="atticFinishedKneewallAddedInsulation" name="atticFinishedKneewallAddedInsulation" class="onScreen">
					<?php 
						foreach ($atticFinishedKneewallAddedInsulation as $val=>$text){
							echo "<option value=\"".$val."\">".$val."</option>";	
						}
					?>
				</select> 
				<span class="forPrint">
					<?php 
						foreach ($atticFinishedKneewallAddedInsulation as $val=>$text){
							echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>
				</span>
				<br clear="all">
				<div class="toBeHidden">
					Attic Kneewall Type: <input type="radio" name="atticFinishedKneewallType" value="Kneewall" checked>Kneewall<br>
					Attic Kneewall Code <input type="radio" name="atticFinishedKneewallCode" value="<?php echo $atticFinishedKneewallCode;?>" checked><?php echo $atticFinishedKneewallCode;?><br>
					Attic Kneewall Roof Color <input type="radio" name="atticFinishedKneewallRoofColor" value="<?php echo $atticFinishedKneewallRoofColor[0];?>" checked><?php echo $atticFinishedKneewallRoofColor[1];?><Br>
					Attic Kneewall Added Insulation Measure #: <input type="radio" id="atticFinishedKneewallAddedInsulationMeasureNumber" name="atticFinishedKneewallAddedInsulationMeasureNumber" value="<?php echo $atticFinishedKneewallAddedInsulationMeasureNumber;?>"><?php echo $atticFinishedKneewallAddedInsulationMeasureNumber;?><br>
				</div>
			</div>
			<br class="pageBreak">
			<div id="finishedAtticSlopeInfo" class="auditTab">
				<div class="auditTabTitle">Finished Attic Slope</div>
				Slope Area (sqft) to Insulate <input type="text" name="atticFinishedSlopeAreaSqft" id="atticFinishedAreaSqftSlope" value="<?php echo $atticFinishedSlopeAreaSqft;?>" class="houseDimension"> Width <input type="text" value="" data-area="atticFinishedAreaSqftSlope" data-otherdimension="atticFinishedSlopeLength" id="atticFinishedSlopeWidth" class="houseDimension areaDimension"> x Length <input type="text" value="" data-area="atticFinishedAreaSqftSlope" data-otherdimension="atticFinishedSlopeWidth" id="atticFinishedSlopeLength" class="houseDimension areaDimension"><br>
				<div>
					<span style="float:left;">Attic Slope Existing Insulation: </span> 
						<span class="forPrint">
						<?php 
							foreach ($atticFinishedSlopeExistingInsulation as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
						</span>
					<select class="onScreen" name="atticFinishedSlopeExistingInsulation">
						<?php 
							foreach ($atticFinishedSlopeExistingInsulation as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select>
					<span class="forPrintAdditional">
						Existing Depth (in) 
						<select id="atticFinishedSlopeExistingInsulationDepth" name="atticFinishedSlopeExistingInsulationDepth" class="onScreen" style="width:50px;">
							<option value="">0</option>
							<option value="3">3</option>
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select> 
						<br>
					</span>

				</div>
				<br clear="all">
				<span style="float:left;">Slope Added Insulation: </span>
				<select id="atticFinishedSlopeAddedInsulation" name="atticFinishedSlopeAddedInsulation" class="onScreen">
					<?php 
						foreach ($atticFinishedSlopeAddedInsulation as $val=>$text){
							echo "<option value=\"".$val."\">".$val."</option>";	
						}
					?>
				</select> 
				<span class="forPrint">
					<?php 
						foreach ($atticFinishedSlopeAddedInsulation as $val=>$text){
							echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>
				</span>
				<span class="forPrintAdditional">
					Added Depth (in) <select id="atticFinishedSlopeAddedInsulationDepth" name="atticFinishedSlopeAddedInsulationDepth" class="onScreen" style="width:50px;">
					</select> 
					
				</span>
				
				<br clear="all">
				<div class="toBeHidden">
					Attic Slope Type: <input type="radio" name="atticFinishedSlopeType" value="RoofRafter" checked>RoofRafter<br>
					Attic Slope Code <input type="radio" name="atticFinishedSlopeCode" value="<?php echo $atticFinishedSlopeCode;?>" checked><?php echo $atticFinishedSlopeCode;?><br>
					Attic Slope Roof Color <input type="radio" name="atticFinishedSlopeRoofColor" value="<?php echo $atticFinishedSlopeRoofColor[0];?>" checked><?php echo $atticFinishedSlopeRoofColor[1];?><Br>
					Attic Slope Added Insulation Measure #: <input type="radio" id="atticFinishedSlopeAddedInsulationMeasureNumber" name="atticFinishedSlopeAddedInsulationMeasureNumber" value="<?php echo $atticFinishedSlopeAddedInsulationMeasureNumber;?>"><?php echo $atticFinishedSlopeAddedInsulationMeasureNumber;?><br>
				</div>
			</div>
			<div id="finishedAtticCapInfo" class="auditTab">
				<div class="auditTabTitle">Finished Attic Cap</div>
				Cap Area (sqft) to Insulate <input type="text" name="atticFinishedCapAreaSqft" id="atticFinishedAreaSqftSlope" value="<?php echo $atticFinishedAreaSqft1;?>" class="houseDimension"> Width <input type="text" value="" data-area="atticFinishedAreaSqftSlope" data-otherdimension="atticFinishedSlopeLength" id="atticFinishedSlopeWidth" class="houseDimension areaDimension"> x Length <input type="text" value="" data-area="atticFinishedAreaSqftSlope" data-otherdimension="atticFinishedSlopeWidth" id="atticFinishedSlopeLength" class="houseDimension areaDimension"><br>
				<div>
					<span style="float:left;">Attic Cap Existing Insulation: </span> 
						<span class="forPrint">
						<?php 
							foreach ($atticFinishedCapExistingInsulation as $val=>$text){
								echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
							}
						?>
						</span>
					<select class="onScreen" name="atticFinishedCapExistingInsulation">
						<?php 
							foreach ($atticFinishedCapExistingInsulation as $val=>$text){
								echo "<option value=\"".$val."\">".$val."</option>";	
							}
						?>
					</select>
					<span class="forPrintAdditional">
						Existing Depth (in) 
						<select id="atticFinishedCapExistingInsulationDepth" name="atticFinishedCapExistingInsulationDepth" class="onScreen" style="width:50px;">
							<option value="">0</option>
							<option value="3">3</option>
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select> 
						<br>
					</span>

				</div>
				<br clear="all">
				<span style="float:left;">Cap Added Insulation: </span>
				<select id="atticFinishedCapAddedInsulation" name="atticFinishedCapAddedInsulation" class="onScreen">
					<?php 
						foreach ($atticFinishedCapAddedInsulation as $val=>$text){
							echo "<option value=\"".$val."\">".$val."</option>";	
						}
					?>
				</select> 
				<span class="forPrint">
					<?php 
						foreach ($atticFinishedCapAddedInsulation as $val=>$text){
							echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>
				</span>
				<span class="forPrintAdditional">
					Added Depth (in) <select id="atticFinishedCapAddedInsulationDepth" name="atticFinishedCapAddedInsulationDepth" class="onScreen" style="width:50px;">
					</select> 
				</span>
				
				<br clear="all">
				<div class="toBeHidden">
					Attic Cap Type: <input type="radio" name="atticFinishedCapType" value="RoofRafter" checked>RoofRafter<br>
					Attic Cap Code <input type="radio" name="atticFinishedCapCode" value="<?php echo $atticFinishedCapCode;?>" checked><?php echo $atticFinishedCapCode;?><br>
					Attic Cap Roof Color <input type="radio" name="atticFinishedCapRoofColor" value="<?php echo $atticFinishedCapRoofColor[0];?>" checked><?php echo $atticFinishedCapRoofColor[1];?><Br>
					Attic Cap Added Insulation Measure #: <input type="radio" id="atticFinishedCapAddedInsulationMeasureNumber" name="atticFinishedCapAddedInsulationMeasureNumber" value="<?php echo $atticFinishedCapAddedInsulationMeasureNumber;?>"><?php echo $atticFinishedCapAddedInsulationMeasureNumber;?><br>
				</div>
			</div>
			<div id="foundationInfo" class="auditTab">
				<div class="auditTabTitle">Foundation</div>
				<span style="float:left;">Foundation Type: </span>
				<select id="foundationType" name="foundationType" class="onScreen">
					<?php 
						foreach ($foundationTypes as $val=>$text){
							echo "<option value=\"".$val."\">".$val."</option>";	
						}
					?>
				</select> (Must be Non Conditioned if adding Foundation Insulation)
				<span class="forPrint">
					<?php 
						foreach ($foundationTypes as $val=>$text){
							echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>
				</span>
				<br>
				<div class="auditSubSection">
					Ceiling:<br>
					Area to Insulate (sq ft) <input type="text" name="foundationFloorArea" id="foundationFloorArea" value="" class="houseDimension"> Width <input type="text" data-area="foundationFloorArea" data-otherdimension="foundationFloorLength" id="foundationFloorWidth" value="" class="houseDimension areaDimension"> x  Length <input type="text" data-area="foundationFloorArea" data-otherdimension="foundationFloorWidth" id="foundationFloorLength" value="" class="houseDimension areaDimension">
					<br clear="all">	
					<div>
						Existing Insulation R Value <input type="text" name="foundationFloorExistingInsulationRValue" value="" class="houseDimension">
					</div>
					<span style="float:left;">Added Insulation Type </span>
						<select name="foundationFloorAddedInsulationAddedType" class="foundationAddedInsulation onScreen">
							<?php 
								foreach ($foundationFloorAddedInsulationAddedType as $val=>$text){
									echo "<option value=\"".$val."\">".$val."</option>";	
								}
							?>
						</select>
						<span class="forPrint">
							<?php 
								foreach ($foundationFloorAddedInsulationAddedType as $val=>$text){
									echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
								}
							?>
						</span>
						<br>
				</div>
				<br>
				<div class="auditSubSection">
					Rim Joist:<br>
					Floor Joist Size (in) <input type="text" name="foundationSillJoistSize" id="foundationSillJoistSize" value="<?php echo $defaultSillJoistSize;?>" class="houseDimension"><br>
					Perimeter to Insulate (ft) <input type="text" name="foundationSillPerimeterToInsulate" id="foundationSillPerimeterToInsulate" value="<?php echo $intakeHousePerimeter;?>" class="houseDimension"> (Populated from intake process)<br>
					<span style="float:left;">Added Insulation Type </span>
						<select name="foundationSillAddedInsulationAddedType" class="foundationAddedInsulation onScreen">
							<?php 
								foreach ($foundationSillAddedInsulationAddedType as $val=>$text){
									echo "<option value=\"".$val."\">".$val."</option>";	
								}
							?>
						</select>
						<span class="forPrint">
							<?php 
								foreach ($foundationSillAddedInsulationAddedType as $val=>$text){
									echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
								}
							?>
						</span>
						<br>
				</div>
				<br>
				<div class="auditSubSection toBeHidden">
					Foundation Wall:<br>
					Height (ft) <input type="text" name="foundationWallHeight" id="foundationWallHeight" value="<?php echo $foundationWallHeightDefault;?>" class="houseDimension"><br>
					Height Exposed (%) <input type="text" name="foundationWallHeightExposed" id="foundationWallHeightExposed" value="<?php echo $foundationWallHeightExposedDefault;?>" class="houseDimension"><br>
					Perimeter Insulate (ft) <input type="text" name="foundationWallPerimeter" id="foundationWallPerimeter" value="<?php echo $intakeHousePerimeter;?>" class="houseDimension"> (Populated from intake process)<br>
					Existing Insulation R-value <input type="text" name="foundationWallExistingInsulationRValue" value="<?php echo $defaultFoundationWallRvalue;?>" class="houseDimension"><br>
					<span style="float:left;">Added Insulation Type </span> 
						<select name="foundationWallAddedInsulationAddedType" class="foundationAddedInsulation onScreen">
							<?php 
								foreach ($foundationWallAddedInsulationAddedType as $val=>$text){
									echo "<option value=\"".$val."\">".$val."</option>";	
								}
							?>
						</select>
						<span class="forPrint">
							<?php 
								foreach ($foundationWallAddedInsulationAddedType as $val=>$text){
									echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
								}
							?>					
						</span>
						<br>
				</div>
				<div class="toBeHidden">
					Foundation Code: <input type="radio" name="foundationCode" value="<?php echo $foundationCode;?>" checked><?php echo $foundationCode;?><br>
					Foundation Measure #: <input type="radio" id="foundationMeasureNumber" name="foundationMeasureNumber" value="<?php echo $foundationMeasureNumber;?>"><?php echo $foundationMeasureNumber;?><br>
				</div>
			</div>

			<div id="heatingInfo" class="auditTab">
				<div class="auditTabTitle">Heating</div>
				<span style="float:left;">Equipment Type: </span>
				<select name="heatingEquipmentType" id="heatingEquipmentType" class="heatingFuelFactor onScreen" data-factortype='equipmentType'>
					<?php
						foreach ($heatingEquipmentType as $val=>$text){
							echo "<option value=\"".$val."\"".($val==$intakeHeatingEquipmentType ? " selected" : "").">".$val."</option>";	
						}
					?>
				</select> 
				<span class="forPrint">
					<?php 
						foreach ($heatingEquipmentType as $val=>$text){
							echo "<input type=\"radio\"".($val==$intakeHeatingEquipmentType ? " checked" : "").">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>					
				</span>
				<span class="forPrintAdditional">
					(Populated from intake process)
				</span>
				<br clear="all">
				<span style="float:left;">Fuel Type: </span>
				<select name="heatingFuel" id="heatingFuel" class="heatingFuelFactor onScreen" data-factortype='fuelType'>
					<?php 
						foreach ($heatingFuel as $val=>$text){
							echo "<option value=\"".$val."\"".($val==$intakeHeatingFuel ? " selected" : "").">".$val."</option>";	
						}
					?>
				</select>
				<span class="forPrint">
					<?php 
						foreach ($heatingFuel as $val=>$text){
							echo "<input type=\"radio\"".($val==$intakeHeatingFuel ? " checked" : "").">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>					
				</span>
				<span class="forPrintAdditional">
					(Populated from intake process)
				</span>
				<br clear="all">

				<span style="float:left;">Location: </span>
				<select name="heatingLocation" class="onScreen">
					<?php 
						foreach ($heatingLocation as $val=>$text){
							echo "<option value=\"".$val."\">".$val."</option>";	
						}
					?>
				</select>
				<span class="forPrint">
					<?php 
						foreach ($heatingLocation as $val=>$text){
							echo "<input type=\"radio\">".$val."&nbsp;&nbsp;&nbsp;";	
						}
					?>					
				</span>
				<br clear="all">
				<span style="float:left;">Replacement Options: </span>
					<select name="heatingReplacementOptions" id="heatingReplacementOptions" data-factortype='replacementOption' class="onScreen">
					</select>
					<span class="forPrint">
						<input type="radio">Standard Efficiency Replacement Manadatory&nbsp;&nbsp;&nbsp;<input type="radio">High Efficiency Replacement Manadatory&nbsp;&nbsp;&nbsp;
						<input type="radio">Evaluate None
					</span>
				<br clear="all">
				<div class="toBeHidden">
					<div id="heatingDetailsGas" class="heatingDetails">
					<span id="heatingEquipmentTypeText"></span> Details:<br>
					Input Units: <input type="text" name="heatingInputUnits" id="heatingInputUnits" value=""><br>
					Output Capacity: <input type="text" name="heatingOutputCapacity" id="heatingOutputCapacity" value=""><br>
					Steady State Efficiency: <input type="text" name="heatingSteadyStateEfficiency" id="heatingSteadyStateEfficiency" value=""><br>
					Condition: <input type="text" name="heatingCondition" id="heatingCondition" value=""><br>
					Replacement Fuel: <input type="text" name="heatingReplacementFuel" id="heatingReplacementFuel" value=""><br>
					System AFUE: <input type="text" name="heatingReplacementSystemAFUE" id="heatingReplacementSystemAFUE" value=""><br>
					Labor Cost: <input type="text" name="heatingReplacementLaborCost" id="heatingReplacementLaborCost" value=""><br>
					Material Cost: <input type="text" name="heatingReplacementMaterialCost" id="heatingReplacementMaterialCost" value=""><br>
					Include In SIR: <input type="checkbox" name="heatingReplacementIncludeInSIR" id="heatingReplacementIncludeInSIR" value="1"><br>
					</div>
				</div>
				<div class="toBeHidden">
					Heat Code: <input type="radio" name="heatingCode" value="<?php echo $heatingCode;?>" checked><?php echo $heatingCode;?><br>
					Heat Supplied (%): <input type="radio" name="heatingSuppliedPercentage" value="<?php echo $heatingSuppliedPercentage;?>" checked><?php echo $heatingSuppliedPercentage;?><br>
					Primary System: <input type="checkbox" name="heatingPrimarySystem" value="<?php echo $heatingPrimarySystem;?>" checked><br>
				</div>
			</div>
			<div id="ductInfo" class="auditTab">
				<div class="auditTabTitle">Building Leakage</div>
					Evaluate the house as:<br clear="all">
						<div style="padding-left:20px;width:50%;float:left;clear:both;">
							<input type="radio" id="ductHouseTypeTight" name="ductHouseType" value="tight" class="ductHouseType"<?php echo ($rawFormData->ductHouseType=="tight" ? " checked" : "");?>>Tight (no airsealing)<br>
							<input type="radio" id="ductHouseTypeKindaLeaky" name="ductHouseType" value="kindaleaky" class="ductHouseType"<?php echo ($rawFormData->ductHouseType=="kindaleaky" ? " checked" : "");?>>KindaLeaky (upto 4 hours airsealing)<br>
							<input type="radio" id="ductHouseTypeLeaky" name="ductHouseType" value="leaky" class="ductHouseType"<?php echo ($rawFormData->ductHouseType=="leaky" || !$rawFormData->ductHouseType ? " checked" : "");?>>Leaky (more than 4 hours airsealing)<br>
						</div>
						<div style="width:30%;float:left;">
							Air Sealing Hours:
							<?php
								if ($rawFormData->ductHouseType){
									if ($rawFormData->ductHouseType == "tight"){
										$ductAirSealingHours = 0;
									}else{
										$ductAirSealingHours = bcdiv($rawFormData->ductAirSealingCosts,$ductAirSealingCostPerHour,0);
									}
									echo $ductAirSealingHours;
								}else{
							?>
								<br>
								<select id="ductAirSealingHours" style="width:50px;" class="onScreen">
									<?php
										for($x=5; $x <= 20; $x++){
											echo "<option value=\"".$x."\"".($x==$ductAirSealingHours ? " selected" : "").">".$x."</option>";
										}
									?>
								</select>
							<?php }?>
							<input type="text" class="houseDimension forPrint" style="float:right;">
						</div>
					<br clear="all">
					<div class="toBeHidden">
						Whole House Blower Door Measurements:<br>
						Volume of House: <span id="ductVolumeOfHouse"><?php echo $houseVolume;?></span> cubic ft = [sqft of house:<span id="ductSqFtOfHouse"><?php echo $intakeFloorArea;?></span> x Conditioned Stores:<span id="ductConditionedStories"><?php echo $intakeConditionedStories;?></span> x 8']<br>
						N-Factor: <span id="ductNFactor"></span><br>
						Before Weatherization (Existing) Air Leakage Rate (cfm): <input type="text" name="ductAirLeakageRateBefore" id="ductAirLeakageRateBefore" value=""><br>
						After Weatherization (Target or Actual) Air Leakage Rate (cfm): <input type="text" name="ductAirLeakageRateAfter" id="ductAirLeakageRateAfter" value=""><br>
						Before Air Pressure: <input type="text" class="houseDimension" name="ductAirPressureBefore" value="50"><br>
						After Air Pressure: <input type="text" class="houseDimension" name="ductAirPressureAfter" value="50"><br>
						Air Sealing Costs: $<input type="text" class="houseDimension" name="ductAirSealingCosts" value="<?php echo $ductAirSealingCosts;?>" id="ductAirSealingCostsHidden"> (<span id="ductAirSealingHoursSpan"><?php echo $ductAirSealingHours;?></span> Hours x $<?php echo $ductAirSealingCostPerHour;?>/hr)<br>
					</div>
			</div>
			<div id="boilerPlate" class="auditTab">
				Which items below should be recommended to upgrade:<br>
				<div class="row">
				Energy Efficiency Rebates:<br>
				<?php
					//print_pre($rawFormData);
					$selectedBoilerPlateItemsRaw = $rawFormData->boilerPlateItems;
					if (!is_array($selectedBoilerPlateItems)){
						$selectedBoilerPlateItems[] = $selectedBoilerPlateItemsRaw;
					}else{
						$selectedBoilerPlateItems = $selectedBoilerPlateItemsRaw;
					}
					$bcount = 0;
					//sort($boilerPlateEnergyItems);
					foreach ($boilerPlateEnergyItems as $BoilerPlateItem){
						echo "<div class='three columns'><input type='checkbox' name='boilerPlateItems' value='".$BoilerPlateItem."'".(in_array($BoilerPlateItem,$selectedBoilerPlateItems) ? " checked" : "").">".$BoilerPlateItem."</div>";
						$bcount++;
						if ($bcount == 4){
							echo "</div><div class='row'>";
							$bcount = 0;
						}
					}
				?>
				<br clear="all">
				Additional Rebates:<br>
				<?php
					$bcount = 0;
					//sort($boilerPlateRebateItems);
					foreach ($boilerPlateRebateItems as $BoilerPlateItem){
						echo "<div class='three columns'><input type='checkbox' name='boilerPlateItems' value='".$BoilerPlateItem."'".(in_array($BoilerPlateItem,$selectedBoilerPlateItems) ? " checked" : "").">".$BoilerPlateItem."</div>";
						$bcount++;
						if ($bcount == 4){
							echo "</div><div class='row'>";
							$bcount = 0;
						}
					}
				?>
				</div>
			</div>
			<div id="notes" class="auditTab">
				Notes:<br>
				<textarea name="notes"><?php echo $rawFormData->notes;?></textarea>
			</div>
			<div id="Attachments" class="auditTab">
				<div style="position:relative;float;left">
					<?php if (!$auditInforResults->MuniAudit_ID){?>
						<div id="CurrentSupportingFiles"></div>
							Upload Attachments (Photo and Reports)<Br>
							<input type="file" id="supportingFile"><span id="fileuploadStatus"></span><br>
							<input type="text" name="supportingFile" value="" id="supportingFileValue">
					<?php }else{
							echo "<div id=\"CurrentSupportingFiles\">";
							$documentList = explode(",",trim($auditInforResults->SupportingFile));
							foreach ($documentList as $documentId=>$documentName){
								if (trim($documentName)){
									echo "<a href=\"".$CurrentServer.$adminFolder."muni/auditAttachments/".$documentName."\" target=\"".$documentName."\" id=\"documentTarget".$auditId."_".$documentId."\">".$documentName."</a>";
									if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni))){
										$deleteRebateLink = "<img src='".$CurrentServer."images/redx-small.png' style='text-align;right;' class='deleteDocument' data-auditid='".$auditId."' data-documentid='".$documentId."' data-customerid='".$SelectedCustomerID."' data-documentname='".$documentName."'><span id='DeleteDocumentResults'></span>";
										echo $deleteRebateLink;
									}
									echo "&nbsp;&nbsp;";
								}
							}
							echo "</div>";
							echo '<input type="hidden" name="supportingFile" value="'.implode(",",$documentList).'" id="supportingFileValueAdditional">';

							if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni))){
								echo '
									<div>
										Upload Additional Supporting Document<Br>
										<input type="file" id="supportingFileAdditional"><span id="fileuploadStatusAdditional"></span><br>
									</div>
								';
							}
					}?>
		
					<script type="text/javascript" language="javascript" class="init">
						$(document).ready(function() {
							// Variable to store your files
							var files;

							// Add events
							$('#supportingFile').on('change', prepareUpload);
							$('#supportingFileAdditional').on('change', prepareUploadAdditional);

							// Grab the files and set them to our variable
							function prepareUpload(event)
							{
							  files = event.target.files;
							// Create a formdata object and add the files
								var data = new FormData();
								$.each(files, function(key, value)
								{
									data.append(key, value);
								});

								$.ajax({
									url: "ApiCustomerData.php?action=auditPhotoUpload&customerId=<?php echo $SelectedCustomerID;?>",
									type: 'POST',
									data: data,
									cache: false,
									processData: false, // Don't process the files
									contentType: false, // Set content type to false as jQuery will tell the server its a query string request
									success: function(data, textStatus, jqXHR)
									{
										//console.log(data);
										$("#supportingFileValue").val(data);
									},
									error: function(jqXHR, textStatus, errorThrown)
									{
										// Handle errors here
										//console.log('ERRORS: ' + textStatus);
										// STOP LOADING SPINNER
									},
									xhrFields: {
									  // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
									  onprogress: function (progress) {
										// calculate upload progress
										var percentage = Math.floor((progress.total / progress.total) * 100);
										// log upload progress to console
										if (percentage === 100) {
											$("#fileuploadStatus").html('File Successfully Uploaded.').addClass("alert").addClass("info");
										}
									  }
									}
								});		  
							  
							}		
							function prepareUploadAdditional(event)
							{
							  files = event.target.files;
							// Create a formdata object and add the files
								var data = new FormData();
								$.each(files, function(key, value)
								{
									data.append(key, value);
								});
								$.ajax({
									url: "ApiCustomerData.php?action=auditPhotoUploadAdditional&customerId=<?php echo $SelectedCustomerID;?>&auditId=<?php echo $auditId;?>",
									type: 'POST',
									data: data,
									cache: false,
									processData: false, // Don't process the files
									contentType: false, // Set content type to false as jQuery will tell the server its a query string request
									success: function(data, textStatus, jqXHR)
									{
										$("#supportingFileValueAdditional").val(data);
									},
									error: function(jqXHR, textStatus, errorThrown)
									{
										// Handle errors here
										//console.log('ERRORS: ' + textStatus);
										// STOP LOADING SPINNER
									},
									xhrFields: {
									  // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
									  onprogress: function (progress) {
										// calculate upload progress
										var percentage = Math.floor((progress.total / progress.total) * 100);
										// log upload progress to console
										if (percentage === 100) {
											$("#fileuploadStatusAdditional").html('File Successfully Uploaded.').addClass("alert").addClass("info");
										}
									  }
									}
								});		  
							  
							}		
							$(".deleteDocument").click(function(e){
								e.preventDefault();
								var $this = $(this),
									auditId = $this.attr('data-auditid'),
									documentId = $this.attr('data-documentid'),
									documentTarget = "#documentTarget"+auditId+"_"+documentId;
									documentImage = "#documentImage"+auditId+"_"+documentId;
									documentReview = "#documentReview"+auditId+"_"+documentId;
									customerId = $this.attr('data-customerid'),
									documentName = $this.attr('data-documentname'),
									data = {};
								data['action'] = 'update_auditdocument';
								data['deleteDocument'] = true;
								data['auditId'] = auditId;
								data['customerId'] = customerId;
								data['documentName'] = documentName;
								//console.log(data);
								$.ajax({
									url: "ApiCustomerData.php",
									type: "PUT",
									data: JSON.stringify(data),
									success: function(data){
										//console.log(data)
										$this.hide();
										//console.log(documentTarget);
										$(documentTarget).hide();
										$(documentImage).hide();
										$("#supportingFileValueAdditional").val(data);
									},
									error: function(jqXHR, textStatus, errorThrown){
										//console.log(jqXHR);
										var message = $.parseJSON(jqXHR.responseText);
										$('#DeleteDocumentResults').html(message).addClass("alert");
									}
								});
								
							});

						});
					</script>			
				</div>
			</div>
			<?php if (!count($rawFormData)){?>
			<div id="AuditSave" class="auditTab">
				<div style="position:relative;float:right;top:-50px;">
					<a href="#" id="audit_save" class="button-link">Save</a>
					<input type="hidden" name="action" value="audit_add">
					<input type="hidden" name="secret" value="skeletonCET35">
					<div id="onlineRebateResults"></div>
				</div>
			</div>
			<?php }else{?>
				<style>
					.odd {background-color:#E2E4FF;}
					.NeatSection {border:0pt solid red;background-color:#ffe5e5;}
					.NeatSection1 {border:0pt solid red;}
				</style>
				<div id="NEAT" class="auditTab">
					<?php
						$color = true;
						$subSectionSpacerText = array("KneeWallAddedInsulationMeasureNumber"=>"Kneewall","SlopeAddedInsulationMeasureNumber"=>"Slope","CapAddedInsulationMeasureNumber"=>"Cap");
						$measureNumberValue = array("UnfinishedAttics"=>"AddedInsulationMeasureNumber","FinishedAttics"=>array("KneeWallAddedInsulationMeasureNumber","SlopeAddedInsulationMeasureNumber","CapAddedInsulationMeasureNumber"),"Foundations"=>"MeasureNumber");
						echo "<div class=\"NeatSection\">";
						foreach ($AuditDetailsForNEAT as $SectionInfo=>$SectionValue){
							//echo $SectionInfo;
							$SectionParts = explode("_",$SectionInfo);
							$SectionTitle = ($SectionParts[0] == "Photos" ? "Attachments" : $SectionParts[0]);
							if ($priorSectionTitle != $SectionTitle){
								if (array_key_exists($SectionTitle,$measureNumberValue)){
									if (is_array($measureNumberValue[$SectionTitle])){
										foreach ($subSectionSpacerText as $subSectionMeasure=>$subSectionTitle){
											$subSectionIncludeInNeat[$subSectionTitle] = (int)($AuditDetailsForNEAT[$SectionParts[0]."_".$subSectionMeasure] ? true : false);
											$subSectionIncludeInNeat["allSubSection"] = $subSectionIncludeInNeat["allSubSection"]+$subSectionIncludeInNeat[$subSectionTitle];
										}
										foreach ($subSectionIncludeInNeat as $thisTitle=>$thisValue){
											if ($thisTitle != "allSubSection"){
												if ($thisValue > 0){$subSectionIncludeText[] = $thisTitle;}
											}
										}
										if ($subSectionIncludeInNeat["allSubSection"] < 1){
											$includeMeasureInNeat = false;
										}else{
											$includeMeasureInNeat = true;
										}
									}else{
										$includeMeasureInNeat = ($AuditDetailsForNEAT[$SectionParts[0]."_".$measureNumberValue[$SectionTitle]] ? true : false);
									}
									if (!$includeMeasureInNeat){
										$NoMeasureText =  "No Measure To Be Included in NEAT, leave this section blank in NEAT<br>";
									}else{
										$NoMeasureText = "Fill out this section in NEAT<br>";
									}
								}else{
									$includeMeasureInNeat = true;
									$NoMeasureText = "";
								}
								echo "</div><hr><div class=\"NeatSection".$includeMeasureInNeat."\"><h4>".$SectionTitle."</h4>";
								echo $NoMeasureText;
								$color = true;
							}
							
							//display options for AttachedFiles
							if ($SectionTitle == "Attachments"){
								$documentList = explode(",",trim($SectionValue));
								$SectionValueDisplay = "";
								foreach ($documentList as $documentId=>$documentName){
									if (trim($documentName)){
										$SectionValueDisplay .= "<a href=\"".$CurrentServer.$adminFolder."muni/auditAttachments/".$documentName."\" target=\"".$documentName."\">".$documentName."</a><br>";
									}
								}
							}elseif ($SectionTitle == "BoilerPlateItems"){
								$SectionValueDisplay = "";
								if (count($SectionValue)>1){
									foreach ($SectionValue as $item){
										$SectionValueDisplay .= $item."<br>";
									}
								}else{
									if (count($SectionValue) ==1){
										$SectionValueDisplay = $SectionValue."<br>";
									}else{
										$SectionValueDisplay = "No Items Checked";
									}
								}
							}else{
								$colorDisplay = (($color = !$color)?' odd':'');
								$SectionValueDisplay = ($subSectionSpacerText[$SectionParts[1]] ? "<br>".$subSectionSpacerText[$SectionParts[1]]."<br>" : "");
								if ($SectionValueDisplay){
									if ($subSectionIncludeInNeat[$subSectionSpacerText[$SectionParts[1]]]){echo "</div><div class=\"NeatSection1\">";}else{echo "</div><div class=\"NeatSection\">";}
								}
								$SectionValueDisplay .=  "<div class=\"five columns".$colorDisplay."\" style=\"text-align:right;\">".$SectionParts[1].":</div><div class=\"five columns".$colorDisplay."\"><b>".$SectionValue."</b></div><Br clear=\"all\">";
							}
							echo $SectionValueDisplay;
							$priorSectionTitle = $SectionParts[0];
						}
					?>
					</div>
				</div>
				<div id="SIRResults" class="auditTab">
					<?php
						$SIRReturnedData = new stdClass();
						include('views/_NEAT.php');
						echo "<span id='currentStatusChangeText'>".$SIRReturnedData->status."</span>&nbsp;&nbsp;&nbsp;<span id='currentStatusChangeTextAjaxLoader' style='display:none;'><img src='".$CurrentServer."images/ajaxLoaderSmall.gif'> Attempting to Send Email</span><br>";
						if ($SIRReturnedData->status == "Client and Audit Added"){
							foreach ($SIRReturnedData->Results as $SIRDataResults){
								echo "<br>".$SIRDataResults;
							}
							if ($auditInforResults->CurrentStatus < 3){
								$currentStatusId = 3;
								$currentStatusText = 'Data Transferred to NEAT';
								$updateStatusFunction = "updateStatus();";
								
								//send email alert
								$sendEmailAlert = true;
							}
						}
						if ($SIRReturnedData->status == "Client Found and Retrieving Audit Data"){
							if ($auditInforResults->CurrentStatus < 3){
								$currentStatusId = 3;
								$currentStatusText = 'Data Transferred to NEAT';
								$updateStatusFunction = "updateStatus();";
							}
						}
						if ($SIRReturnedData->status == "Audit Scores Received"){
							$SIRResults = $SIRReturnedData->SIRResults;
							$SIRMeasureMaterials = (count($SIRReturnedData->SIRMeasureMaterials) ? objectToArray($SIRReturnedData->SIRMeasureMaterials) : array());
							if ($auditInforResults->CurrentStatus < 4){
								$currentStatusId = 4;
								$currentStatusText = 'SIR Score Available';
								$updateStatusFunction = "updateStatus();";
							}
						}
						echo $SIRReturnedData->Link;
					?>
					<hr>
					<a target="CustomerReport<?php echo $SelectedCustomerID;?>" href="<?php echo $CurrentServer.$adminFolder;?>muni/CustomerReport.php?CustomerID=<?php echo $SelectedCustomerID;?>" class="button-link">Create Customer Report</a>
					<style>
						.sirData {border:1pt solid black;padding:5px;text-align:center;}
					</style>
					<table style="border:1pt solid black;" cellpadding=0 cellspacing=0 id="sirTable">
						<tr class="sirData" style="background-color:#c1d72d;">
							<th class="sirData">Recommended Measure</th>
							<th class="sirData">Quantity</th>
							<th class="sirData">Estimated* Measure Savings ($)/Year</th>
							<th class="sirData">Estimated* Measure Cost ($)</th>
							<th class="sirData">Estimated* Savings to Investment Ration (SIR)</th>
						</tr>
						<tbody>
							<tr class="sirData">
								<td class="sirData"><input type="text" id="sirMeasure0"></td>
								<td class="sirData"><input type="text" class="one column" id="sirQty0"></td>
								<td class="sirData"><input type="text" class="two columns" id="sirSavings0"></td>
								<td class="sirData"><input type="text" class="two columns" id="sirCost0"></td>
								<td class="sirData"><input type="text" class="one column" id="sirScore0"><a href="#" data-rowid="0" class="button-link sirAdd" style="float:right;">Add</a></td>
							</tr>
							<?php 
								if ($auditInforResults->SIRData){
									$SIRDataList = explode("|",trim($auditInforResults->SIRData));
									foreach ($SIRDataList as $sirId=>$sirItem){
										$sirItemObject = json_decode($sirItem);
										$deleteLink = "<span style='float:right;cursor:pointer;'><img onClick='deleteSIR(".$sirItem.",".$auditId.");' src='".$CurrentServer."images/redx-small.png' style='text-align;right;'></span>";

										echo "<tr class=\"sirData\" id=\"".$sirItemObject->rowId."\">";
										echo "<td class=\"sirData\">".$sirItemObject->measure."</td>";
										echo "<td class=\"sirData\">".$sirItemObject->qty."</td>";
										echo "<td class=\"sirData\">".$sirItemObject->savings."</td>";
										echo "<td class=\"sirData\">".$sirItemObject->cost."</td>";
										echo "<td class=\"sirData\">".$sirItemObject->score.$deleteLink."</td>";
										echo "</tr>";
									}
								}
								if (count($SIRResults)){
									echo "<tr><td colspan='5' align='center'><br><b>Currently Retrieved Values From NEAT</b></td></tr>";
									$sirCount = 1;
									foreach ($SIRResults as $measure){
										$component = ($measure->Components ? "[".$measure->Components."]" : "");
										$material = $SIRMeasureMaterials[$measure->RunID][$measure->Index]["Measure"];
										$qty =$SIRMeasureMaterials[$measure->RunID][$measure->Index]["Quantity"];
										$units = $SIRMeasureMaterials[$measure->RunID][$measure->Index]["Units"];
										if ($measure->Measure == "Infiltration Redctn"){
											$material = "Air Sealing";
											$units = ($measure->Cost/$ductAirSealingCostPerHour)." hours";
										}else{
											$qty =round($qty);
										}
										if (trim($material) == ''){$material = $measure->Measure;}
										echo '
										<tr class="sirData">
											<td class="sirData"><input type="text" id="sirMeasure'.$sirCount.'" value="'.$component.' '.$material.'"></td>
											<td class="sirData"><input type="text" class="two columns" id="sirQty'.$sirCount.'" value="'.$qty.' '.$units.'"></td>
											<td class="sirData"><input type="text" class="two columns" id="sirSavings'.$sirCount.'" value="$'.money($measure->Savings).'"></td>
											<td class="sirData"><input type="text" class="two columns" id="sirCost'.$sirCount.'" value="$'.money($measure->Cost).'"></td>
											<td class="sirData"><input type="text" class="one column" id="sirScore'.$sirCount.'" value="'.round($measure->SIR,2).'"><a href="#" data-rowid="'.$sirCount.'" class="button-link sirAdd" style="float:right;">Add</a></td>
										</tr>';
										$sirCount++;
									}	
								}
							?>
							
							
						</tbody>
					</table>					
				</div>
				<script type="text/javascript" language="javascript" class="init">
					function deleteSIR(sirItem,auditId){
						var sirItem = sirItem,
							auditId = auditId,
							rowId = "#"+sirItem.rowId,
							sirData = {};
							sirData["action"] = "deleteSIR";
							sirData["auditId"] = auditId,
							sirData["sirItem"] = sirItem;
							
							$.ajax({
								url: "ApiCustomerData.php",
								type: "PUT",
								data: JSON.stringify(sirData),
								success: function(data){
									$(rowId).hide();
								},
								error: function(jqXHR, textStatus, errorThrown){
									//console.log(jqXHR);
									var message = $.parseJSON(jqXHR.responseText);
									$('#onlineRebateResults').html(message).addClass("alert");
								}
							});
					};

					$(function(){
						<?php if ($updateStatusFunction){?>
							var updateStatus = function(){
								var StatusChange = {};
									StatusChange["action"] = "update_auditstatus";
									StatusChange["auditId"] = <?php echo $auditId;?>;
									StatusChange["currentStatus"] = <?php echo $currentStatusId;?>;
									StatusChange["currentText"] = '<?php echo $currentStatusText;?>';
									StatusChange["customerId"] = <?php echo $SelectedCustomerID;?>;
									data = StatusChange;
								//console.log(data);
								$.ajax({
									url: "ApiCustomerData.php",
									type: "PUT",
									data: JSON.stringify(data),
									success: function(data){
										$("#currentStatusChangeText").html('Audit Status Updated to: <?php echo $currentStatusText;?>');
										<?php if ($sendEmailAlert){?>
											$("#currentStatusChangeTextAjaxLoader").show();
											var emailData = {};
												emailData["action"] = "audit_sendemail";
												emailData["customerId"] = <?php echo $SelectedCustomerID;?>;
												emailData["customerName"] = "<?php echo $AuditDetailsForNEAT["Contacts_NameDetailsLast"].", ".$AuditDetailsForNEAT["Contacts_NameDetailsFirst"];?>";
												//console.log(emailData);
											$.ajax({
												url: "ApiCustomerData.php",
												type: "PUT",
												data: JSON.stringify(emailData),
												success: function(data){
													$("#currentStatusChangeTextAjaxLoader").html('<span class="alert info">Email Notification Sent</span>');
												},
												error: function(jqXHR, textStatus, errorThrown){
													var message = $.parseJSON(jqXHR.responseText);
													//console.log(message);
												}
											});
										<?php }?>
									},
									error: function(jqXHR, textStatus, errorThrown){
										var message = $.parseJSON(jqXHR.responseText);
										//console.log(message);
									}
								});
							};
							<?php echo $updateStatusFunction;?>
						<?php }?>

						$(".sirAdd").on('click',function(e){
							e.preventDefault();
							var $this = $(this),
								thisRowID = $this.attr('data-rowid');
							var randID = "sirRow"+Math.floor((Math.random() * 100) + 1);
							var measureId = "#sirMeasure"+thisRowID,
								qtyId = "#sirQty"+thisRowID,
								savingsId = "#sirSavings"+thisRowID,
								costId = "#sirCost"+thisRowID,
								scoreId = "#sirScore"+thisRowID;
								
							var measure = $(measureId),
								qty = $(qtyId),
								savings = $(savingsId),
								cost = $(costId),
								score = $(scoreId),
								sirItem = {},
								sirData = {};
								
								sirItem["measure"] = measure.val();
								sirItem["qty"] = qty.val();
								sirItem["savings"] = savings.val();
								sirItem["cost"] = cost.val();
								sirItem["score"] = score.val();
								sirItem["rowId"] = randID;
								
								sirData["action"] = "addSIR";
								sirData["auditId"] = <?php echo $auditId;?>,
								sirData["sirItem"] = sirItem;
								//console.log(sirData);
								var sirItemObject = JSON.stringify(sirItem);
								var deleteLink = "<span style='float:right;cursor:pointer;'><img onClick='deleteSIR("+JSON.stringify(sirItem)+",<?php echo $auditId;?>);' src='<?php echo $CurrentServer;?>images/redx-small.png' style='text-align;right;'></span>";
								$('#sirTable > tbody:last-child').append('<tr class="sirData" id="'+randID+'"><td class="sirData">'+measure.val()+'</td><td class="sirData">'+qty.val()+'</td><td class="sirData">'+savings.val()+'</td><td class="sirData">'+cost.val()+'</td><td class="sirData">'+score.val()+deleteLink+'</td></tr>');
								measure.val('');
								qty.val('');
								savings.val('');
								cost.val('');
								score.val('');
								$.ajax({
									url: "ApiCustomerData.php",
									type: "PUT",
									data: JSON.stringify(sirData),
									success: function(data){
										//console.log(data)
									},
									error: function(jqXHR, textStatus, errorThrown){
										//console.log(jqXHR);
										var message = $.parseJSON(jqXHR.responseText);
										$('#onlineRebateResults').html(message).addClass("alert");
									}
								});
								
						});
					});
				</script>
			<?php }?>
			
			
		</form>
	</div> <!-- end Tab Content-->
		<button id="HideShow">Toggle Hidden Item</button>
		<button id="HideShowTabs">Show All Tabs</button>
	</div> <!--end Div container -->
	<script>
	$(function(){

	
		var dateClass = $(".date");
		dateClass.width(80);
		dateClass.datepicker({
			maxDate:0
		});
		var heatingDetails = <?php echo json_encode($heatingDetails);?>;
		var houseNFactors = <?php echo json_encode($houseNFactors);?>;

		var conditionedStories = $("#conditionedStories"),
			wallGrossAreaToBeInsulated = $("#wallGrossAreaToBeInsulated"),
			wallGrossAreaToBeInsulatedHidden = $("#wallGrossAreaToBeInsulatedHidden"),
			disabledWallGrossAreaToBeInsulated = $("#disabledWallGrossAreaToBeInsulated"),
			wallMeasureNumber = $("#wallMeasureNumber"),
			houseVolume = $(".houseVolume"),
			houseWidth = $("#houseWidth"),
			houseLength = $("#houseLength"),
			floorArea = $("#floorArea"),
			floorAreaHidden = $("#floorAreaHidden"),
			entireExterior = $("#entireExterior"),
			grossWallCalculator_Width = $("#grossWallCalculator_Width"),
			grossWallCalculator_Height = $("#grossWallCalculator_Height"),
			existingInsulationNo = $("#existingInsulationNo"),
			existingInsulationYes = $("#existingInsulationYes"),
			existingInsulationRValue = $("#existingInsulationRValue"),
			addedInsulationNo = $("#addedInsulationNo"),
			addedInsulationYes = $("#addedInsulationYes"),
			shellFloorWidth = $("#shellFloorWidth"),
			shellFloorLength = $("#shellFloorLength"),
			atticUnfinishedWidth = $("#atticUnfinishedWidth"),
			atticUnfinishedLength = $("#atticUnfinishedLength"),
			atticUnfinishedAddedInsulation = $("#atticUnfinishedAddedInsulation"),
			atticUnfinishedAddedInsulationMeasureNumber = $("#atticUnfinishedAddedInsulationMeasureNumber"),
			atticUnfinishedAddedInsulationDepth = $("#atticUnfinishedAddedInsulationDepth"),
			atticFinishedKneewallAddedInsulation = $("#atticFinishedKneewallAddedInsulation"),
			atticFinishedKneewallAddedInsulationMeasureNumber = $("#atticFinishedKneewallAddedInsulationMeasureNumber"),
			atticFinishedSlopeAddedInsulation = $("#atticFinishedSlopeAddedInsulation"),
			atticFinishedSlopeAddedInsulationMeasureNumber = $("#atticFinishedSlopeAddedInsulationMeasureNumber"),
			atticFinishedSlopeAddedInsulationDepth = $("#atticFinishedSlopeAddedInsulationDepth"),
			atticFinishedCapAddedInsulation = $("#atticFinishedCapAddedInsulation"),
			atticFinishedCapAddedInsulationMeasureNumber = $("#atticFinishedCapAddedInsulationMeasureNumber"),
			atticFinishedCapAddedInsulationDepth = $("#atticFinishedCapAddedInsulationDepth"),
			foundationAddedInsulationClass = $(".foundationAddedInsulation"),
			foundationMeasureNumber = $("#foundationMeasureNumber"),
			foundationFloorArea = $("#foundationFloorArea"),
			heatingFuelFactor = $(".heatingFuelFactor"),
			heatingEquipmentTypeTextSpan = $("#heatingEquipmentTypeText"),
			heatingEquipmentType = $("#heatingEquipmentType"),
			heatingInputUnits = $("#heatingInputUnits"),
			heatingOutputCapacity = $("#heatingOutputCapacity"),
			heatingSteadyStateEfficiency = $("#heatingSteadyStateEfficiency"),
			heatingCondition = $("#heatingCondition"),
			heatingReplacementOptions = $("#heatingReplacementOptions"),
			heatingReplacementFuel = $("#heatingReplacementFuel"),
			heatingReplacementSystemAFUE = $("#heatingReplacementSystemAFUE"),
			heatingReplacementLaborCost = $("#heatingReplacementLaborCost"),
			heatingReplacementMaterialCost = $("#heatingReplacementMaterialCost"),
			heatingReplacementIncludeInSIR = $("#heatingReplacementIncludeInSIR"),
			ductAirSealingHours = $("#ductAirSealingHours"),
			ductAirSealingCostsHidden = $("#ductAirSealingCostsHidden");
			ductVolumeOfHouse = $("#ductVolumeOfHouse"),
			ductConditionedStories = $("#ductConditionedStories"),
			ductSqFtOfHouse = $("#ductSqFtOfHouse"),
			ductNFactor = $("#ductNFactor"),
			ductNFactor.text(houseNFactors[parseFloat(conditionedStories.val())]),
			ductAirLeakageRateBefore = $("#ductAirLeakageRateBefore"),
			ductAirLeakageRateAfter = $("#ductAirLeakageRateAfter"),
			airFactorLeaky = .8,
			airFactorKindaLeaky = .52,
			airFactorTight = .4375,
			ductHouseType = $(".ductHouseType"),
			heatingDetailsObj = {};
			
		$(".numbersOnly").keyup(function(){
			this.value = this.value.replace(/[^0-9\.]/g,'');
		});

		foundationFloorArea.on('keyup, change',function(){
			$("#foundationFloorAreaHidden").val($(this).val());
		});
		
		foundationAddedInsulationClass.on('change',function(){
			var isNotNone = false;
			$.each(foundationAddedInsulationClass,function(key,item){
				if ($("option:selected",item).text() != "None"){
					isNotNone = true;
				}
				
			});
			if (isNotNone){
				foundationMeasureNumber.prop('checked',true);
				$("#foundationType").val('NonConditioned');
			}else{
				foundationMeasureNumber.prop('checked',false);
				$("#foundationType").val('Conditioned');
			}
		});
			

			
		atticUnfinishedAddedInsulation.on('change',function(){
			var $this = $(this),
				addedInsulationDepths = <?php echo json_encode($atticAddedInsulationDepthArray);?>;
				thisValue = $this.val();
			if ($("option:selected",this).text() != 'None'){
				atticUnfinishedAddedInsulationMeasureNumber.prop('checked',true);
				atticUnfinishedAddedInsulationDepth.empty();
				$.each(addedInsulationDepths[thisValue],function(key,val){
					atticUnfinishedAddedInsulationDepth.append($("<option></option>").attr("value", val).text(val));
				});
			}else{
				atticUnfinishedAddedInsulationDepth.empty();
				atticUnfinishedAddedInsulationMeasureNumber.prop('checked',false);
			}
		});
		atticFinishedKneewallAddedInsulation.on('change',function(){
			if ($("option:selected",this).text() != 'None'){
				atticFinishedKneewallAddedInsulationMeasureNumber.prop('checked',true);
			}else{
				atticFinishedKneewallAddedInsulationMeasureNumber.prop('checked',false);
			}
		});		
		atticFinishedSlopeAddedInsulation.on('change',function(){
			var $this = $(this),
				addedInsulationDepths = <?php echo json_encode($atticAddedInsulationDepthArray);?>;
				thisValue = $this.val();
			if ($("option:selected",this).text() != 'None'){
				atticFinishedSlopeAddedInsulationMeasureNumber.prop('checked',true);
				atticFinishedSlopeAddedInsulationDepth.empty();
				$.each(addedInsulationDepths[thisValue],function(key,val){
					atticFinishedSlopeAddedInsulationDepth.append($("<option></option>").attr("value", val).text(val));
				});
			}else{
				atticFinishedSlopeAddedInsulationDepth.empty();
				atticFinishedSlopeAddedInsulationMeasureNumber.prop('checked',false);
			}		
		});		
		atticFinishedCapAddedInsulation.on('change',function(){
			var $this = $(this),
				addedInsulationDepths = <?php echo json_encode($atticAddedInsulationDepthArray);?>;
				thisValue = $this.val();
			if ($("option:selected",this).text() != 'None'){
				atticFinishedCapAddedInsulationMeasureNumber.prop('checked',true);
				atticFinishedCapAddedInsulationDepth.empty();
				$.each(addedInsulationDepths[thisValue],function(key,val){
					atticFinishedCapAddedInsulationDepth.append($("<option></option>").attr("value", val).text(val));
				});
			}else{
				atticFinishedCapAddedInsulationDepth.empty();
				atticFinishedCapAddedInsulationMeasureNumber.prop('checked',false);
			}
		});		
		var ductLeakage = function(vol,airFactor,nFactor){
			console.log("vol="+vol);
			console.log("airFactor="+airFactor);
			console.log("nFactor="+nFactor);
			var leakage = ((vol*airFactor)/60)*nFactor;
			console.log("leakage="+leakage);
			var roundedAnswer = Math.round(leakage);
			console.log("return Math.round="+roundedAnswer);
			return Math.round(leakage);
		};
		
		var ductLeakageLeaky = ductLeakage(ductVolumeOfHouse.text(),airFactorLeaky,houseNFactors[parseFloat(conditionedStories.val())]);
		var ductLeakageKindaLeaky = ductLeakage(ductVolumeOfHouse.text(),airFactorKindaLeaky,houseNFactors[parseFloat(conditionedStories.val())]);
		var ductLeakageTight = ductLeakage(ductVolumeOfHouse.text(),airFactorTight,houseNFactors[parseFloat(conditionedStories.val())]);
		ductAirLeakageRateBefore.val(ductLeakageLeaky);
		ductAirLeakageRateAfter.val(ductLeakageTight);
		
		ductHouseType.on('change',function(){
			var $this = $(this),
				thisAirFactorBefore = airFactorTight;
			if ($this.val() == "leaky"){
				thisAirFactorBefore = airFactorLeaky;
				var thisDuctCost = 8*<?php echo $ductAirSealingCostPerHour;?>,
					ductAirSealingHoursList = new Array(<?php for($x=5; $x <= 19; $x++){echo $x.",";}?>20);
				ductAirSealingCostsHidden.val(thisDuctCost);
				ductAirSealingHours.empty();
				$.each(ductAirSealingHoursList,function(key,val){
					ductAirSealingHours.append($("<option></option>").attr("value", val).text(val));
				});
				ductAirSealingHours.val(8);
				$("#ductAirSealingHoursSpan").text('8');
				
			}else if ($this.val() == "kindaleaky"){
				thisAirFactorBefore = airFactorKindaLeaky;
				var thisDuctCost = 4*<?php echo $ductAirSealingCostPerHour;?>,
					ductAirSealingHoursList = new Array(<?php for($x=1; $x <= 3; $x++){echo $x.",";}?>4);
				ductAirSealingHours.empty();
				$.each(ductAirSealingHoursList,function(key,val){
					ductAirSealingHours.append($("<option></option>").attr("value", val).text(val));
				});
				ductAirSealingCostsHidden.val(thisDuctCost);
				ductAirSealingHours.val(4);
				$("#ductAirSealingHoursSpan").text('4');
				
			}else{
				ductAirSealingHours.val(0);
				ductAirSealingCostsHidden.val('');
				$("#ductAirSealingHoursSpan").text('0');

			}
			var ductLeakageBefore = ductLeakage(ductVolumeOfHouse.text(),thisAirFactorBefore,houseNFactors[parseFloat(conditionedStories.val())]);
			var ductLeakageAfter = ductLeakage(ductVolumeOfHouse.text(),airFactorTight,houseNFactors[parseFloat(conditionedStories.val())]);
			ductAirLeakageRateBefore.val(ductLeakageBefore);
			ductAirLeakageRateAfter.val(ductLeakageAfter);
		});
		
		houseVolume.on('blur',function(){
			var thisHouseSqFt = houseWidth.val()*houseLength.val();
				thisConditionedStories = parseFloat(conditionedStories.val());
				thisVolume = thisConditionedStories*thisHouseSqFt*8;
			ductVolumeOfHouse.text(thisVolume);
			ductConditionedStories.text(thisConditionedStories);
			ductSqFtOfHouse.text(thisHouseSqFt);
			//get the airleakage and set leakage rates
			if ($("#ductHouseTypeTight").prop('checked')){
				var thisAirFactorBefore = airFactorTight;
			}else{
				var thisAirFactorBefore = airFactorLeaky;
			}
			var ductLeakageBefore = ductLeakage(ductVolumeOfHouse.text(),thisAirFactorBefore,houseNFactors[thisConditionedStories]);
			var ductLeakageAfter = ductLeakage(ductVolumeOfHouse.text(),airFactorTight,houseNFactors[thisConditionedStories]);
			ductAirLeakageRateBefore.val(ductLeakageBefore);
			ductAirLeakageRateAfter.val(ductLeakageAfter);
			ductNFactor.text(houseNFactors[thisConditionedStories]);

		});
		ductAirSealingHours.on('change',function(){
			var $this = $(this),
				ductAirSealingHours = $this.val(),
				ductAirSealingCostPerHour = <?php echo $ductAirSealingCostPerHour;?>,
				ductAirSealingCost = Math.round(parseFloat(ductAirSealingHours*ductAirSealingCostPerHour)*100)/100;
			$("#ductAirSealingHoursSpan").text(ductAirSealingHours);
			ductAirSealingCostsHidden.val(ductAirSealingCost);
		});
		var heatingReplacementFunction = function(replacementObj){
			var replacementObj = replacementObj;
			heatingReplacementFuel.val(replacementObj['Fuel']);
			heatingReplacementSystemAFUE.val(replacementObj['SystemAFUE']);
			heatingReplacementLaborCost.val(replacementObj['LaborCost']);
			heatingReplacementMaterialCost.val(replacementObj['MaterialCost']);
			if (replacementObj['IncludeInSIR']){
				heatingReplacementIncludeInSIR.prop('checked',true);
			}else{
				heatingReplacementIncludeInSIR.prop('checked',false);
			}
		};
			
		heatingDetailsObj = heatingDetails['<?php echo $intakeHeatingEquipmentType;?>']['<?php echo $intakeHeatingFuel;?>'];
				heatingEquipmentTypeTextSpan.text('<?php echo $intakeHeatingEquipmentType;?>');
				heatingInputUnits.val(heatingDetailsObj['InputUnits']);
				heatingOutputCapacity.val(heatingDetailsObj['OutputCapacity']);
				heatingSteadyStateEfficiency.val(heatingDetailsObj['SteadyStateEfficiency']);
				heatingCondition.val(heatingDetailsObj['Condition']);
				$.each(heatingDetailsObj['Replacement'], function(key,value){
					heatingReplacementOptions.append($("<option></option>")
					.attr("value",key)
					.text(key));
				});
				var heatingReplacemetOptionDefaultText = $("#heatingReplacementOptions option:first-child").text();
				heatingReplacementFunction(heatingDetailsObj['Replacement'][heatingReplacemetOptionDefaultText]);
				
		
		heatingFuelFactor.on('change',function(){
			var heatingEquipmentTypeText = $("#heatingEquipmentType option:selected").text(),
				heatingFuelText = $("#heatingFuel option:selected").text();
				
				heatingEquipmentTypeTextSpan.text(heatingEquipmentTypeText);
				heatingDetailsObj = heatingDetails[heatingEquipmentTypeText][heatingFuelText];
				heatingInputUnits.val(heatingDetailsObj['InputUnits']);
				heatingOutputCapacity.val(heatingDetailsObj['OutputCapacity']);
				heatingSteadyStateEfficiency.val(heatingDetailsObj['SteadyStateEfficiency']);
				heatingCondition.val(heatingDetailsObj['Condition']);
				heatingReplacementOptions.children().remove();
				$.each(heatingDetailsObj['Replacement'], function(key,value){
					heatingReplacementOptions.append($("<option></option>")
					.attr("value",key)
					.text(key));
				});
				var heatingReplacemetOptionSelectedText = $("#heatingReplacementOptions option:first-child").text();
				heatingReplacementFunction(heatingDetailsObj['Replacement'][heatingReplacemetOptionSelectedText]);
		});
		heatingReplacementOptions.on('change',function(){
			var thisText = $('option:selected',this).text();
			heatingReplacementFunction(heatingDetailsObj['Replacement'][thisText]);
		});
			
		$(".areaDimension").on('keyup',function(){
			var $this = $(this);
				thisAreaID = "#"+$this.attr('data-area'),
				thisOtherDimensionID = "#"+$this.attr('data-otherdimension'),
				thisThirdDimensionID = "#"+$this.attr('data-thirddimension'),
				thisDimension = $this.val(),
				thisOtherDimension = $(thisOtherDimensionID).val(),
				thisThirdDimension = $(thisThirdDimensionID).val(),
				thisArea = parseInt(thisDimension)*parseInt(thisOtherDimension);
				if (thisThirdDimension > 0){
					thisArea = thisArea*parseInt(thisThirdDimension);
				}
				if (thisArea > 0){
					$(thisAreaID).val(thisArea);
				}else{
					$(thisAreaID).val('');
				}
				
		});
		conditionedStories.on('keyup',function(){
			var adjustedFloorArea = parseInt(houseWidth.val())*parseInt(houseLength.val())*parseFloat(conditionedStories.val());
			floorArea.val(adjustedFloorArea);
			floorAreaHidden.val(adjustedFloorArea);
		});
		entireExterior.on('click',function(){
			var $this = $(this),
				conditionedStoriesVal = parseFloat(conditionedStories.val()),
				thisHouseWidth = parseInt(houseWidth.val());
				thisHouseLength = parseInt(houseLength.val());
			//Formula for entire house wall gross area = ((houseWidth(ft) x 8(ft) x conditionedStories) x 2) + ((houseLength(ft) x 8(ft) x conditionedStories) x 2) =
			// (houseWidth x conditionedStories x 16) + (houseLength x conditionedStories x 16)
			var entireHouseWall = (thisHouseWidth*conditionedStoriesVal*16) + (thisHouseLength*conditionedStoriesVal*16);

			if ($this.prop("checked")){
				wallGrossAreaToBeInsulated.val(entireHouseWall);
				wallGrossAreaToBeInsulatedHidden.val(entireHouseWall);
				disabledWallGrossAreaToBeInsulated.val(entireHouseWall);
				existingInsulationNo.prop('checked',true);
				existingInsulationYes.prop('checked',false);
				existingInsulationRValue.val('');
				addedInsulationNo.prop('checked',false);
				addedInsulationYes.prop('checked',true);
				wallMeasureNumber.prop('checked',true);
			}else{
				wallGrossAreaToBeInsulated.val('');
				wallGrossAreaToBeInsulatedHidden.val('');
				grossWallCalculator_Width.val('');
				disabledWallGrossAreaToBeInsulated.val('');
				existingInsulationNo.prop('checked',false);
				existingInsulationYes.prop('checked',true);
				existingInsulationRValue.val('<?php echo $existingInsulationRValue;?>');
				addedInsulationNo.prop('checked',true);
				addedInsulationYes.prop('checked',false);
				wallMeasureNumber.prop('checked',false);
			}
		});
		houseWidth.on('keyup',function(){
			var thisVal = $(this).val();
			shellFloorWidth.text(thisVal);
			atticUnfinishedWidth.val(thisVal);
		});
		houseLength.on('keyup',function(){
			var thisVal = $(this).val();
			shellFloorLength.text(thisVal);
			atticUnfinishedLength.val(thisVal);
		});
		$("#wallGrossAreaToBeInsulated").on('keyup',function(){
			var thisVal = $(this).val();
			if (!isNaN(thisVal)){
				wallMeasureNumber.prop('checked',true);
			}else{
				wallMeasureNumber.prop('checked',false);
			}
		});
		$(".grossWallCalculator").on('keyup',function(){
			entireExterior.attr('checked', false);
			wallWidth = parseInt(grossWallCalculator_Width.val());
			wallHeight = parseInt(grossWallCalculator_Height.val());
			grossWall = (wallWidth*wallHeight);
			if (!isNaN(grossWall)){
				wallGrossAreaToBeInsulated.val(grossWall);
				wallGrossAreaToBeInsulatedHidden.val(grossWall);
				wallMeasureNumber.prop('checked',true);
				addedInsulationYes.prop('checked',true);
			}else{
				wallGrossAreaToBeInsulated.val('');
				wallGrossAreaToBeInsulatedHidden.val('');
				wallMeasureNumber.prop('checked',false);
				addedInsulationYes.prop('checked',false);
			}
		});
		
		$("#HideShow").on('click',function(){
			$(".toBeHidden").toggle();
		});
		$("#HideShowTabs").on('click',function(){
			$(".auditTab").show();
		});
		
		
		var auditformElement = $("#auditFormData");
		 
		auditformElement.validate({
			rules: {
				auditUtilityAccountNumber: {
					required: true
				}
			}
		});
		
		$("#audit_save").click(function(e){
		    $('#onlineRebateResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (auditformElement.valid()){
				//console.log(JSON.stringify(auditformElement.serializeObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(auditformElement.serializeObject()),
					success: function(data){
						//console.log(data)
						var successmessage = '<?php echo ($actualAudit ? "Saved.  You can close this browser window now." : "Saved");?>';
						$('#onlineRebateResults').html(successmessage).addClass("alert").addClass("info");
						<?php if ($actualAudit){?>
							$("#audit_save").hide();
						<?php }?>
						
					},
					error: function(jqXHR, textStatus, errorThrown){
						//console.log(jqXHR);
						var message = $.parseJSON(jqXHR.responseText);
						$('#onlineRebateResults').html(message).addClass("alert");
					}
				});
			}
		});
		
		$(".customerData").on('blur',function(){
			var $this = $(this),
				thisClientId = $this.attr('data-clientid'),
				thisFieldName = $this.attr('data-fieldname'),
				thisValue = $this.val();
			var data = {};
			data["action"] = "updateCustomerData";
			data["customerId"] = thisClientId;
			data["fieldName"] = thisFieldName;
			data["value"] = thisValue;
			$.ajax({
				url: "ApiCustomerData.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
					console.log(data)
				},
				error: function(jqXHR, textStatus, errorThrown){
					//console.log(jqXHR);
					var message = $.parseJSON(jqXHR.responseText);
					$('#onlineRebateResults').html(message).addClass("alert");
				}
			});

		});
	});
	</script>
	<script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript">
		$(function(){
			var hash = window.location.hash,
				tabHashes = new Array(<?php echo implode(",",$tabHashes);?>),
				isTabHash = $.inArray(hash, tabHashes) !== -1,
				isOnSiteConfigTab = hash.substring(0, 6) === "#form-",
				setTab = function(tabid){
					var tab = $("#tab-" + tabid),
						content = $("#" + tabid);
					tab.click();
					tab.addClass("active");
					content.addClass("active").show();
				};

			if (isTabHash){
				// let tabs.js take care of this
				setTab(hash.substring(1));
				// if it's the first tab, we need to reposition user at the top
				$("html,body").animate({
					scrollTop: 0
				});
			} else if (isOnSiteConfigTab || !(hash.length)){
				setTab("<?php echo $setTab;?>");
			}
		});
	</script>
<?php }//if not $newCustomerName ?>