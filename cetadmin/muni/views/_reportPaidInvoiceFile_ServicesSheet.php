<?php
//titles
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A" . $rowCounter, "Energy Audits");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowCounter, "Rebates");
} catch (PHPExcel_Exception $e) {
}

//formatting
try {
    $objPHPExcel->getActiveSheet()->getStyle("A" . $rowCounter)->applyFromArray($styleBoldArray);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D" . $rowCounter)->applyFromArray($styleBoldArray);
} catch (PHPExcel_Exception $e) {
}

//formulas
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowCounter, "=COUNTA(A3:A100)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $rowCounter, "=SUM(G3:G100)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("I" . $rowCounter, "=COUNTA(I3:I100)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("L" . $rowCounter, "=SUM(L3:L100)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("N" . $rowCounter, "=COUNTA(N3:N100)");
} catch (PHPExcel_Exception $e) {
}

$ServicesAuditHeader = array(
	"A"=>array("title"=>"Customer","object"=>"customerFullName"),
	"B"=>array("title"=>"Energy Audit Date","object"=>"auditDate")
);
if ($previouslyInvoiceAuditsDisplay){
	$ServicesAuditHeader["C"] = array("title"=>"Previously Invoiced Date","object"=>"invoicedDate");
}
$ServicesRebateHeader = array(
	"D"=>array("title"=>"Customer","object"=>"customerFullName"),
	"E"=>array("title"=>"Rebate Dispersed Date","object"=>"dispersedDate"),
	"F"=>array("title"=>"Equipment Rebate Type","object"=>"equipmentRebatePartCode"),
	"G"=>array("title"=>"Equipment Rebate Quantity","object"=>"equipmentRebateQty"),
	"H"=>array("title"=>"Equipment Rebate amount","object"=>"equipmentRebateAmount"),
	"I"=>array("title"=>"Equipment Verification","object"=>"equipmentVerifiedDate"),
	
	"J"=>array("title"=>"Energy Audit Date","object"=>"auditDate"),
	"K"=>array("title"=>"Weatherization Rebate Type","object"=>"weatherizationRebatePartCode"),
	"L"=>array("title"=>"Weatherization Rebate Quantity","object"=>"weatherizationRebateQty"),
	"M"=>array("title"=>"Weatherization Rebate Amount","object"=>"weatherizationRebateAmount"),
	"N"=>array("title"=>"Weatherization Verification","object"=>"weatherizationVerifiedDate")
);
if ($previouslyInvoiceAuditsDisplay){
	$ServicesRebateHeader["O"] = array("title"=>"Previously Invoiced Date","object"=>"invoicedDate");
}
$rowCounter=2;

foreach ($ServicesAuditHeader as $column=>$headerData){
	$title = $headerData["title"];
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $title);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
            array('borders' => array(
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
            )
        );
    } catch (PHPExcel_Exception $e) {
    }
}
foreach ($ServicesRebateHeader as $column=>$headerData){
	$title = $headerData["title"];
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $title);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
            array('borders' => array(
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
            )
        );
    } catch (PHPExcel_Exception $e) {
    }
}
							 
$rowCounter=3;
//add audit records
foreach ($auditRecords as $auditObject){
	$serviceDate = strtotime($auditObject->auditDate);
	$invoicedDate = $auditObject->invoicedDate;
	if ($serviceDate <= strtotime($endMonth) && !$invoicedDate){ //catch any prior uninvoiced audits.  This changes was made by Heather and Lisa 3/8/2018
//	if ($serviceDate >= strtotime($startMonth) && $serviceDate <= strtotime($endMonth)){
		if ($auditObject->invoicedDate && !$previouslyInvoiceAuditsDisplay){
			if (!in_array($auditObject->auditId,$previouslyInvoicedAudits)){
				$previouslyInvoicedAudits[$auditObject->auditId] = $auditObject;
			}
		}else{
			foreach ($ServicesAuditHeader as $column=>$headerData){
                try {
                    $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $auditObject->{$headerData["object"]});
                } catch (PHPExcel_Exception $e) {
                }
            }
			$rowCounter++;	
			if (!in_array($auditObject->auditId,$invoicedAudits) && !$previouslyInvoiceAuditsDisplay){
				$invoicedAudits[] = $auditObject->auditId;
			}
		}
	}
}
$rowCounter=3;
//add rebate records
foreach ($rebates as $dispersedDate=>$dispersedDateData){
	foreach ($dispersedDateData as $dispersedRebate){
		$serviceDate = strtotime($dispersedRebate->dispersedDate);
		if ($serviceDate >= strtotime($startMonth) && $serviceDate <= strtotime($endMonth)){
			if ($dispersedRebate->invoicedDate && !$previouslyInvoiceAuditsDisplay){
				if (!in_array($dispersedRebate->rebateId,$previouslyInvoicedRebate)){
					$previouslyInvoicedRebates[strtotime($dispersedRebate->DispersedToFiscalDate)][] = $dispersedRebate;
					$previouslyInvoicedRebatesCount++;
				}
			}else{
			
				foreach ($ServicesRebateHeader as $column=>$headerData){
                    try {
                        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $dispersedRebate->{$headerData["object"]});
                    } catch (PHPExcel_Exception $e) {
                    }
                }
				$rowCounter++;
				if (!in_array($dispersedRebate->rebateId,$invoicedRebates[$dispersedRebate->rebateType]) && !$previouslyInvoiceAuditsDisplay){
					$invoicedRebates[$dispersedRebate->rebateType][] = $dispersedRebate->rebateId;
				}
			}
		}
	}
}
//Set Styles
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(24);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(24);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A2:O2")->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP, 'wrap' => true)));
} catch (PHPExcel_Exception $e) {
}
?>