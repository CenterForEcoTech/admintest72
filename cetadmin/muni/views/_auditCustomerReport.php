<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");
$muniProvider = new MuniProvider($dataConn);

$SelectedCustomerID = $_GET['CustomerID'];

//get Customer info
$criteria = new stdClass();
$criteria->id = $SelectedCustomerID;
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
}
//print_pre($CustomerByID);

//get rebate info for current client
$criteria = new stdClass();
$criteria->customerId = $SelectedCustomerID;
$auditInforResults = $muniProvider->getAuditInfo($criteria);
$rawFormData = json_decode($auditInforResults->MuniAudit_RawFormData);
$auditId = $auditInforResults->MuniAudit_ID;
$boilerPlateItems = $rawFormData->boilerPlateItems;
$notes = $rawFormData->notes;
$supportingFiles = $auditInforResults->SupportingFile;
$SupportingFiles = explode(",",$supportingFiles);
if (count($SupportingFiles)){
	foreach ($SupportingFiles as $SupportingFile){
		if (strpos(strtolower($SupportingFile),".jpg")){
			$supportingFile = $SupportingFile;
		}
	}
	
}else{
	$supportingFile = "";
}
//print_pre($rawFormData);
//get boilerPlateItems
$boilerPlateResults = $muniProvider->getBoilerPlate();
$boilerPlateCollection = $boilerPlateResults->collection;
foreach ($boilerPlateCollection as $result=>$record){
	$category = strtolower($record->category);
	$boilerPlateByCategory[$category] = $record;
}
include_once('customerReportTemplate/reportTemplate.htm');
if ($auditInforResults->CurrentStatus <= 6){
	$currentStatusId = 6;
	$currentStatusText = 'Report Created';
	$updateStatusFunction = "updateStatus();";
}
?>
<script type="text/javascript" language="javascript" class="init">
	$(function(){
		<?php if ($updateStatusFunction){?>
			var updateStatus = function(){
				var StatusChange = {};
					StatusChange["action"] = "update_auditstatus";
					StatusChange["auditId"] = <?php echo $auditId;?>;
					StatusChange["currentStatus"] = <?php echo $currentStatusId;?>;
					StatusChange["currentText"] = '<?php echo $currentStatusText;?>';
					StatusChange["customerId"] = <?php echo $SelectedCustomerID;?>;
					data = StatusChange;
				console.log(data);
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						//$("#currentStatusChangeText").html('Status Updated to <?php echo $currentStatusText;?>');
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						//console.log(message);
					}
				});
			};
			<?php echo $updateStatusFunction;?>
		<?php }?>
	});
</script>