<?php
ini_set('memory_limit','14096M');
set_time_limit(2500000);
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("WGE Rebate Tracking")
							 ->setSubject("WGE Rebate Tracking")
							 ->setDescription("WGE Rebate Tracking, Audits, and Phone Log");
//START REBATE RECORDS
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($RebateHeader as $column=>$headerData){
	$title = ($headerData["title"] == '&nbsp;' ? " " : $headerData["title"]);
			if ($headerData["object"] == 'space' || $headerData["object"] == 'invoice_data'){
                try {
                    $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
                        array('borders' => array(
                            'left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)
                        )
                        )
                    );
                } catch (PHPExcel_Exception $e) {
                }
            }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $title);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowCounter++;
//add some rows
foreach ($rebates as $dispersedDate=>$dispersedDateData){
	foreach ($dispersedDateData as $dispersedRebate){
		foreach ($RebateHeader as $column=>$headerData){
			if ($headerData["object"] == 'invoice_data' || $headerData["object"] == 'space'){
				if ($headerData["object"] == 'invoice_data'){
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
                            array('fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('argb' => 'A8A8A8')
                            ),
                                'borders' => array(
                                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)
                                )
                            )
                        );
                    } catch (PHPExcel_Exception $e) {
                    }
                }else{
                    try {
                        $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
                            array('borders' => array(
                                'left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)
                            )
                            )
                        );
                    } catch (PHPExcel_Exception $e) {
                    }
                }
			}
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $dispersedRebate->{$headerData["object"]});
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowCounter++;	
	}
}
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Rebates');
} catch (PHPExcel_Exception $e) {
}
//END REBATE RECORDS
							 
							 
//START ENERGY Audits
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($AuditHeader as $column=>$headerData){
	if ($headerData["object"] == 'space' || $headerData["object"] == 'invoice_data'){
        try {
            $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
                array('borders' => array(
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)
                )
                )
            );
        } catch (PHPExcel_Exception $e) {
        }
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowCounter++;
//add some rows
foreach ($auditRecords as $auditObject){
	foreach ($AuditHeader as $column=>$headerData){
		if ($headerData["object"] == 'invoice_data'){
            try {
                $objPHPExcel->getActiveSheet()->getStyle($column . $rowCounter)->applyFromArray(
                    array('fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => 'A8A8A8')
                    ),
                        'borders' => array(
                            'left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)
                        )
                    )
                );
            } catch (PHPExcel_Exception $e) {
            }
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $auditObject->{$headerData["object"]});
        } catch (PHPExcel_Exception $e) {
        }
    }
	$rowCounter++;	
}

// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Energy Audits');
} catch (PHPExcel_Exception $e) {
}
//END ENERGY Audits

//START PHONE LOG
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}

// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($PhoneLogHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowCounter++;

//add some rows
foreach ($phoneLogCollection as $ID=>$Record){
	$thisTimeStamp = $Record->timeStamp;
	$newTimeStamp = date('m/d/y',strtotime($thisTimeStamp));
	foreach ($PhoneLogHeader as $column=>$headerData){
		if ($headerData["object"] == "timeStamp"){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $newTimeStamp);
            } catch (PHPExcel_Exception $e) {
            }
        }else{
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $Record->{$headerData["object"]});
            } catch (PHPExcel_Exception $e) {
            }
        }
	}
	$rowCounter++;
}//end foreach
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Phone Log');
} catch (PHPExcel_Exception $e) {
}
//END PHONE LOG

//START MONTHLY
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}

// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(3);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($MonthlyHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowCounter++;

//add some rows
foreach ($monthlyData as $yearMonth=>$yearMonthArray){
	foreach ($yearMonthArray as $row){
		foreach ($MonthlyHeader as $column=>$headerData){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $row[$headerData["object"]]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowCounter++;
	}
}//end foreach
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('MONTHLY');
} catch (PHPExcel_Exception $e) {
}
//END MONTHLY

//START YTD
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}

// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(4);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
foreach ($YearlyHeader as $column=>$headerData){
    try {
        $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $headerData["title"]);
    } catch (PHPExcel_Exception $e) {
    }
}
$rowCounter++;

//add some rows
foreach ($yearlyData as $year=>$yearArray){
	foreach ($yearArray as $row){
		foreach ($YearlyHeader as $column=>$headerData){
            try {
                $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCounter, $row[$headerData["object"]]);
            } catch (PHPExcel_Exception $e) {
            }
        }
		$rowCounter++;
	}
}//end foreach
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('YEARLY');
} catch (PHPExcel_Exception $e) {
}
//END MONTHLY

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/muni/";}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$trackingFile = "exportWGERebateTracking_".date("Y_m_d").".xlsx";
$templateFile = "WGEinvoice_template.xltx";

$pathExtension = "exports".$pathSeparator.$trackingFile;
$webpathExtension = "exports".$webpathSeparator.$trackingFile;
$webtemplatepathExtension = "exports".$webpathSeparator.$templateFile;
$saveLocation = $path.$pathExtension;


//echo $saveLocation;
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webtrackingLink = $CurrentServer.$adminFolder."muni/".$webpathExtension;
$templateLink = $CurrentServer.$adminFolder."muni/".$webtemplatepathExtension;
echo "<a id='WGERebateTrackingFile' href='".$webtrackingLink."'>".$trackingFile."</a><br>";
//echo "<a id='WGETemplateFile' href='".$templateLink."'>WGE Template File</a>";
?>