<?php
set_time_limit(0);
ini_set('memory_limit','14096M');
$time_start = microtime(true);
include_once('views/_reportPaidProcessor.php');
$tabSections = array("rebatesDispersed"=>"Rebates Dispersed to Fiscal","auditsSent"=>"Audits Sent","phoneLog"=>"Phone Log","monthlySummary"=>"Monthly Summary","yearlySummary"=>"Yearly Summary","trackingFile"=>"Tracking File","invoiceFile"=>"Invoice File");
?>
<style>
	.active {display:block;}
	.auditTabTitle {font-weight:bold;}
	.auditSubSection {border: 1pt dashed black;padding:5px;}
	th {white-space:nowrap;}
	td {white-space:nowrap;}
</style>
<ul id="audit-tabs" class="tabs onScreen" data-tabs="tabs">
	<?php
		$setTab = ($_GET['setTab'] ? $_GET['setTab'] : "trackingFile"); //default tab to start on
		foreach ($tabSections as $tabId=>$tabDisplay){
			echo "<li><a id=\"tab-".$tabId."\" href=\"#".$tabId."\">".$tabDisplay."</a></li>\r\n";
			$tabHashes[]='"#'.$tabId.'"';
		}
	?>
</ul>
<form>
	<div id="yearlySummary" class="auditTab">
		<div class="auditTabTitle">Year Summary: <?php echo $AuditStatusNameByID[$auditStatus];?></div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
						<?php $YearlyHeader = 
								array(
									"A"=>array("title"=>"Year","object"=>"year"),
									"B"=>array("title"=>"Rebates","object"=>"rebateType"),
									"C"=>array("title"=>"Code","object"=>"rebateCode"),
									"D"=>array("title"=>"Qty","object"=>"qty"),
									"E"=>array("title"=>"Amount","object"=>"amount"),
									"F"=>array("title"=>"kWh Savings","object"=>"kWhSavingsTotal"),
									"G"=>array("title"=>"Lifetime kWh Savings","object"=>"lifetimekWhSavingsTotal"),
									"H"=>array("title"=>"mmBtu Savings","object"=>"mmBtuSavingsTotal"),
									"I"=>array("title"=>"MCF Savings","object"=>"mcfSavingsTotal"),
									"J"=>array("title"=>"Lifetime MCF Savings","object"=>"lifetimeMCFSavingsTotal"),
									"K"=>array("title"=>"Peak Demand kW Savings","object"=>"peakDemandSavingsPerYear"),
									"L"=>array("title"=>"Peak Demand kW Lifetime Savings","object"=>"peakDemandLifetimeSavings")
								);?>
						<?php 
							foreach ($YearlyHeader as $column=>$headerData){
								echo "<th>".$headerData["title"]."</th>";
							}
						?>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($yearlyRecords as $year=>$yearObj){
								$Year = date("Y",$year);
								foreach ($yearObj as $rebateType=>$rebateTypeObj){
									ksort($rebateTypeObj);
									foreach ($rebateTypeObj as $rebateCode=>$rebateCodeObj){
										ksort($rebateCodeObj);
											$rowTD = array();
										echo "<tr>";
												foreach ($YearlyHeader as $column=>$headerData){
													$sortingdata = "";
													if ($headerData["object"] == "year"){
														if ($displayedYear == $Year){
															$displayObj = "&nbsp;";
														}else{
															$displayObj = $Year;
														}
														$displayObj = $Year; //Heather wanted to display in every row as of 9/29/2017
														$sortingdata = "<span style='display:none'>".$year."</span>";
														$displayedYear = $Year;
													}elseif ($headerData["object"] == "rebateType"){
														if ($displayedType == $rebateType){
															$displayObj = "&nbsp;";												
														}else{
															$displayObj = $rebateType;	
														}
														$displayObj = $rebateType;	 //Heather wanted to display in every row as of 9/29/2017
														$displayedType = $rebateType;
													}elseif ($headerData["object"] == "rebateCode"){
														$displayObj = $rebateCode;												
													}else{
														$displayObj = $rebateCodeObj[$headerData["object"]];
													}
													echo "<td>".$sortingdata.$displayObj."</td>";
													$rowTD[$headerData["object"]]=str_replace("&nbsp;"," ",$displayObj);
												}
										$yearlyData[$year][]=$rowTD;
												
										echo "</tr>";
										
									}
								}
							}
							$time_end = microtime(true);
							$execution_time = ($time_end - $time_start);
							echo '<b>Total Execution Time:</b> '.$execution_time;
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div> <!-- end Yearly-->
	<div id="monthlySummary" class="auditTab">
		<div class="auditTabTitle">Monthly Summary: <?php echo $AuditStatusNameByID[$auditStatus];?></div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
						<?php $MonthlyHeader = 
								array(
									"A"=>array("title"=>"Year","object"=>"year"),
									"B"=>array("title"=>"Month","object"=>"month"),
									"C"=>array("title"=>"Rebates","object"=>"rebateType"),
									"D"=>array("title"=>"Code","object"=>"rebateCode"),
									"E"=>array("title"=>"Qty","object"=>"qty"),
									"F"=>array("title"=>"Amount","object"=>"amount"),
									"G"=>array("title"=>"kWh Savings","object"=>"kWhSavingsTotal"),
									"H"=>array("title"=>"Lifetime kWh Savings","object"=>"lifetimekWhSavingsTotal"),
									"I"=>array("title"=>"mmBtu Savings","object"=>"mmBtuSavingsTotal"),
									"J"=>array("title"=>"MCF Savings","object"=>"mcfSavingsTotal"),
									"K"=>array("title"=>"Lifetime MCF Savings","object"=>"lifetimeMCFSavingsTotal"),
									"L"=>array("title"=>"Peak Demand kW Savings","object"=>"peakDemandSavingsPerYear"),
									"M"=>array("title"=>"Peak Demand kW Lifetime Savings","object"=>"peakDemandLifetimeSavings")
									
								);?>
						<?php 						
							foreach ($MonthlyHeader as $column=>$headerData){
								echo "<th>".$headerData["title"]."</th>";
							}
						?>
						</tr>
					</thead>
					<tbody>
						<?php
							//reset sections
							$displayedYear = "";
							$displayedMonth = "";
							$displayedType = "";
							foreach ($monthlyRecords as $yearMonth=>$yearMonthObj){
								$Year = date("Y",$yearMonth);
								$Month = date("M",$yearMonth);
								foreach ($yearMonthObj as $rebateType=>$rebateTypeObj){
									ksort($rebateTypeObj);
									foreach ($rebateTypeObj as $rebateCode=>$rebateCodeObj){
										ksort($rebateCodeObj);
											$rowTD = array();
										echo "<tr>";
												foreach ($MonthlyHeader as $column=>$headerData){
													$sortingdata = "";
													if ($headerData["object"] == "year"){
														if ($displayedYear == $Year){
															$displayObj = "&nbsp;";
														}else{
															$displayObj = $Year;
														}
														$displayObj = $Year; //Heather wanted to display in every row as of 9/29/2017
														$sortingdata = "<span style='display:none'>".$yearMonth."</span>";
														$displayedYear = $Year;
													}elseif ($headerData["object"] == "month"){
														if ($displayedMonth == $Month){
															$displayObj = "&nbsp;";												
														}else{
															$displayObj = $Month;												
														}
														$displayObj = $Month;//Heather wanted to display in every row as of 9/29/2017												
														$displayedMonth = $Month;
													}elseif ($headerData["object"] == "rebateType"){
														if ($displayedType == $rebateType){
															$displayObj = "&nbsp;";												
														}else{
															$displayObj = $rebateType;	
														}
														$displayObj = $rebateType;	//Heather wanted to display in every row as of 9/29/2017
														$displayedType = $rebateType;
													}elseif ($headerData["object"] == "rebateCode"){
														$displayObj = $rebateCode;												
													}else{
														$displayObj = $rebateCodeObj[$headerData["object"]];
													}
													echo "<td>".$sortingdata.$displayObj."</td>";
													$rowTD[$headerData["object"]]=str_replace("&nbsp;"," ",$displayObj);
												}
										$monthlyData[$yearMonth][]=$rowTD;
												
										echo "</tr>";
										
									}
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="rebatesDispersed" class="auditTab">
		<div class="auditTabTitle">Customers with Rebates Status: <?php echo $RebateStatusNameByID[$reportRebateStatus];?></div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
						<?php $RebateHeader = 
								array(
									"A"=>array("title"=>"Approved Date (Date sent to fiscal)","object"=>"dispersedDate"),
									"B"=>array("title"=>"Rebate Type","object"=>"rebateType"),
									"C"=>array("title"=>"Part code","object"=>"partCode"),
									"D"=>array("title"=>"Qty","object"=>"qty"),
									"E"=>array("title"=>"Rebate Amount","object"=>"rebateAmount"),
									"F"=>array("title"=>"Total Amount Paid","object"=>"totalAmount"),
									"G"=>array("title"=>"Lifetime","object"=>"lifetime"),
									
									"H"=>array("title"=>"kWh Savings","object"=>"kWhSavings"),
									"I"=>array("title"=>"kWh Lifetime Savings","object"=>"kWhLifetimeSavings"),
									"J"=>array("title"=>"MMBTU savings","object"=>"mmbtuSavings"),
									"K"=>array("title"=>"MCF Savings","object"=>"mcfSavings"),
									"L"=>array("title"=>"MCF Lifetime Savings","object"=>"mcfLifetimeSavings"),
									"M"=>array("title"=>"&nbsp;","object"=>"space"),
									"N"=>array("title"=>"Rebate ID#","object"=>"rebateId"),

									"O"=>array("title"=>"Received Date","object"=>"receivedDate"),
									"P"=>array("title"=>"First","object"=>"firstName"),
									"Q"=>array("title"=>"Last","object"=>"lastName"),
									"R"=>array("title"=>"Address","object"=>"address"),
									"S"=>array("title"=>"City","object"=>"city"),
									"T"=>array("title"=>"State","object"=>"state"),
									"U"=>array("title"=>"Zip","object"=>"zip"),
									"V"=>array("title"=>"AccountNumber;","object"=>"account"),
									
									"W"=>array("title"=>"&nbsp;","object"=>"space"),
									"X"=>array("title"=>"Description","object"=>"description"),
									"Y"=>array("title"=>"Manufacturer","object"=>"manufacturer"),
									"Z"=>array("title"=>"Model","object"=>"model"),
									"AA"=>array("title"=>"Efficiency","object"=>"efficiency"),
									"AB"=>array("title"=>"Input/Size","object"=>"inputsize"),
									"AC"=>array("title"=>"Condenser  Model #","object"=>"condenserModel"),
									
									"AD"=>array("title"=>"Coil Model #","object"=>"coilModel"),
									"AE"=>array("title"=>"SEER","object"=>"seer"),
									"AF"=>array("title"=>"EER","object"=>"eer"),
									"AG"=>array("title"=>"HSPF","object"=>"hspf"),
									"AH"=>array("title"=>"Unit Size","object"=>"unitsize"),
									
									"AI"=>array("title"=>"&nbsp;","object"=>"invoice_data"),
									"AJ"=>array("title"=>"Customer","object"=>"customerFullName"),
									"AK"=>array("title"=>"Rebate Dispersed Date","object"=>"dispersedDate"),
									"AL"=>array("title"=>"Equipment Rebate Type","object"=>"partCode"),
									"AM"=>array("title"=>"Equipment Rebate Quantity","object"=>"qty"),
									"AN"=>array("title"=>"Equipment Rebate amount","object"=>"totalAmount"),
									"AO"=>array("title"=>"Equipment Verification","object"=>"equipmentVerification"),
									
									"AP"=>array("title"=>"Energy Audit Date","object"=>"auditDate"),
									"AQ"=>array("title"=>"Weatherization Rebate Quantity","object"=>"weatherizationRebateQty"),
									"AR"=>array("title"=>"Weatherization Rebate Amount","object"=>"weatherizationRebateAmount"),
									"AS"=>array("title"=>"Weatherization Verification","object"=>"weatherizationVerification")
									
								);?>
						<?php 
							foreach ($RebateHeader as $column=>$headerData){
								echo "<th>".$headerData["title"]."</th>";
							}
						?>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($rebates as $dispersedDate=>$dispersedDateData){
								foreach ($dispersedDateData as $dispersedRebate){
									echo "<tr>";
											foreach ($RebateHeader as $column=>$headerData){
												if ($headerData["object"] == "dispersedDate"){
													echo "<td><span style='display:none;'>".$dispersedDate."</span>".$dispersedRebate->{$headerData["object"]}."</td>";
												}else{
													echo "<td>".$dispersedRebate->{$headerData["object"]}."</td>";
												}
											}
									echo "</tr>";
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="auditsSent" class="auditTab">
		<div class="auditTabTitle">Customers with Audit Status: <?php echo $AuditStatusNameByID[$auditStatus];?></div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
						<?php $AuditHeader = 
								array(
									"A"=>array("title"=>"Audit Date","object"=>"auditDate"),
									"B"=>array("title"=>"First","object"=>"firstName"),
									"C"=>array("title"=>"Last","object"=>"lastName"),
									"D"=>array("title"=>"Address","object"=>"address"),
									"E"=>array("title"=>"City","object"=>"city"),
									"F"=>array("title"=>"State","object"=>"state"),
									"G"=>array("title"=>" ","object"=>"invoice_data"),
									"H"=>array("title"=>"Customer","object"=>"customerFullName"),
									"I"=>array("title"=>"Energy Audit Date","object"=>"auditDate")
								);?>
						<?php 
							foreach ($AuditHeader as $column=>$headerData){
								echo "<th>".$headerData["title"]."</th>";
							}
						?>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($auditRecords as $auditObject){
								echo "<tr>";
										foreach ($AuditHeader as $column=>$headerData){
											if ($headerData["object"] == "auditDate"){
												echo "<td><span style='display:none;'>".$auditObject->auditDate."</span>".$auditObject->{$headerData["object"]}."</td>";
											}else{
												echo "<td>".$auditObject->{$headerData["object"]}."</td>";
											}
										}
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="phoneLog" class="auditTab">
		<div class="auditTabTitle">Phone Log</div>
		<div class="row">
			<div class="fifteen columns">
				<table class="rawDataReport display" cellspacing="0" width="100%">
					<thead>
						<tr>
						<?php $PhoneLogHeader = 
								array(
									"A"=>array("title"=>"Date of Call","object"=>"timeStamp"),
									"B"=>array("title"=>"First","object"=>"firstName"),
									"C"=>array("title"=>"Last","object"=>"lastName"),
									"D"=>array("title"=>"Phone","object"=>"phoneNumber"),
									"E"=>array("title"=>"Reason For Call","object"=>"reasonForCall"),
									"F"=>array("title"=>"How They Heard","object"=>"howTheyHeard"),
									"G"=>array("title"=>"Action Taken","object"=>"actionTaken"),
									"H"=>array("title"=>"Notes","object"=>"notes"),
									"I"=>array("title"=>"CSR","object"=>"addedByAdmin")
								);?>
						<?php 
							foreach ($PhoneLogHeader as $column=>$headerData){
								echo "<th>".$headerData["title"]."</th>";
							}
						?>
						</tr>
					</thead>
					<tbody>
						<?php	
							foreach ($phoneLogCollection as $ID=>$Record){
								$thisTimeStamp = $Record->timeStamp;
								$newTimeStamp = date('m/d/y',strtotime($thisTimeStamp));
						?>
								<tr>
									<?php
										foreach ($PhoneLogHeader as $column=>$headerData){
											if ($headerData["object"] == "timeStamp"){
												echo "<td><span style='display:none;'>".strtotime($thisTimeStamp)."</span>".$newTimeStamp."</td>";
											}elseif ($headerData["object"] == "addedByAdmin"){
												echo "<td>".emailInitials($Record->{$headerData["object"]})."</td>";
											}else{
												echo "<td>".$Record->{$headerData["object"]}."</td>";
											}
										}
									?>
								</tr>
						<?php 
							}//end foreach MonthYearInfo 
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="trackingFile" class="auditTab">
		<div class="auditTabTitle">Rebate Tracking File</div>
		<?php include_once ('_reportPaidExportFile.php');?>
	</div>
	<div id="invoiceFile" class="auditTab">
		<?php
			$invoiceMonth = $_GET['invoiceDate'];
			$startFiscalYearMonth = "11";
			$thisMonth = date("m");
			if ($thisMonth < $startFiscalYearMonth){
				$startFiscalYearYear = date("Y",strtotime(date("m/d/Y")." -1 year"));
			}else{
				$startFiscalYearYear = date("Y");
			}
			$startFiscalYearDate = $startFiscalYearDateStart = $startFiscalYearYear."-11-01";
			while ($startFiscalYearDate < date("Y-m-01")){
				$startFiscalYearDateEnd = $startFiscalYearDate;
				$activeMonths[] = $startFiscalYearDate;
				$startFiscalYearDate = date("Y-m-01",strtotime($startFiscalYearDate." +1 month"));
			}
			//exception for first month of fiscal year trying to access last fiscal year
			if (!count($activeMonths)){
				$startFiscalYearDate = $startFiscalYearDateStart = date("Y-11-01",strtotime(date("m/d/Y")." -1 year"));
				while ($startFiscalYearDate < date("Y-m-01")){
					$startFiscalYearDateEnd = $startFiscalYearDate;
					$activeMonths[] = $startFiscalYearDate;
					$startFiscalYearDate = date("Y-m-01",strtotime($startFiscalYearDate." +1 month"));
				}
				
			}
			//$activeMonths = array("2016-05-01","2016-04-01","2016-03-01","2016-02-01","2016-01-01","2015-12-01","2015-11-01");
		?>
		<div class="auditTabTitle">Invoice Files for <?php echo date("m/01/Y",strtotime($startFiscalYearDateStart))." - ".date("m/t/Y",strtotime($startFiscalYearDateEnd));?></div>
		<select name="invoiceMonth" style="width:150px;" id="invoiceMonth">
			<option value="">Select Month</option>
			<?php 
			foreach ($activeMonths as $invoiceDate){
					echo "<option value=\"".$invoiceDate."\" ".($invoiceMonth == $invoiceDate ? " selected" : "").">".date("M Y",strtotime($invoiceDate))."</option>";
			}
			?>
		</select>
		<?php 
			if ($invoiceMonth){
				
			//Get Billing Rates
			include_once($siteRoot."_setupDataConnection.php");
			include_once($dbProviderFolder."GBSProvider.php");
			$GBSProvider = new GBSProvider($dataConn);
			$billingRatesInvoiceNames = array("WGE");
			foreach ($billingRatesInvoiceNames as $invoiceName){
				$criteria = new stdClass();
				$criteria->invoiceName = $invoiceName;
				$paginationResult = $GBSProvider->invoiceTool_getBillingRates($criteria);
				$resultArray = $paginationResult->collection;
				foreach ($resultArray as $result=>$record){
					$EmployeeMembers = explode(",",$record->GBSBillingRate_EmployeeMembers);
					$BillingRateByCategory[$criteria->invoiceName][$record->GBSBillingRate_Name]=$record;
					foreach ($EmployeeMembers as $Employee){
						$BillingRateEmployees[$criteria->invoiceName][$record->GBSBillingRate_Name][] = $Employee;
						$BillingRateByEmployees[$criteria->invoiceName][$Employee][$record->GBSBillingRate_Name] = $record->GBSBillingRate_Rate;
					}
				}
			}	
			
			//print_pre($BillingRateEmployees);
			
			
			$InvoiceTool = true;
			$InvoicingToolCodeGroup = "WGE";
			$SelectedGroupsID = "WGE";
			$sourceFolder = "muni";
			$extraVariable = "&invoiceDate=".$invoiceMonth;
			$currentTab = "invoiceFile";
			include('../hours/views/_reportByCode.php');
			//echo "<br clear='all'>";
			$IncludedEmployees = array();
			$ExcludedEmployees = array();
			//print_pre($InvoiceDataByDate);
				foreach ($BillingRateByCategory as $InvoiceName=>$CategoryNameInfo){
					foreach ($CategoryNameInfo as $CategoryName=>$RateInfo){
						foreach ($InvoiceData as $Employee_Name=>$hours){
							if (!$EmployeesRevenueCodeByName[$Employee_Name]){
								$Adjustments["Alert"]["Staff missing Revenue Code"][str_replace("_",", ",$Employee_Name)]=" ";
							}
//							echo $InvoiceName." ".$CategoryName." ".$Employee_Name."<br>";
//							print_pre($hours);
							if (in_array($Employee_Name,$BillingRateEmployees[$InvoiceName][$CategoryName])){
								foreach ($hours as $code=>$hour){
									if ($hour && $hour != "0.00" && strtolower(substr($code,-1)) != "u" && strtolower($code) != "totalhours"){
										if (!$IncludedEmployees[$code][$Employee_Name]){
											//$InvoiceName = $code;
											$InvoiceHourDataByCategory[$code][$CategoryName]["hours"] = $InvoiceHourDataByCategory[$code][$CategoryName]["hours"]+$hour;
											$InvoiceHourDataByCategory[$code][$CategoryName]["rate"] = $RateInfo->GBSBillingRate_Rate;
											$IncludedEmployees[$code][$Employee_Name] = 1;
										}
									}
								}
							}else{
								$ExcludedEmployees[$Employee_Name] = $hours;
							}
						}
					}
				}
				//find all excluded Employees
				foreach ($IncludedEmployees as $code=>$Employee_Name){
					foreach ($Employee_Name as $EmployeeName=>$val){
						unset($ExcludedEmployees[$EmployeeName]);
					}
				}
				foreach ($ExcludedEmployees as $Employee_Name=>$val){
					$hoursDisplay = array();
					foreach ($val as $code=>$hours){
						if ($hours && $hours != "0.00" && $code !="TotalHours"){
							$hoursDisplay[] = $code.": ".$hours;
						}
					}
					if (count($hoursDisplay)){
						$Adjustments["Alert"]["Employees missing Billing Rate Code"][] = str_replace("_",", ",$Employee_Name).implode(", ",$hoursDisplay);
					}
				}
			
			
			
			
			
			
			
			
				
				
				echo "<br>";
				
				include_once ('_reportPaidInvoiceFile.php');
				echo ($_GET['markAsInvoiced'] ? "<span class='alert info'>Items Marked as Invoiced</span>" : "<input type='checkbox' id='markAsInvoiced' name='markAsInvoiced' value='yes'>Mark items in file as invoiced<br>");
				echo "<hr><br>Below is the revenue codes to send to Fiscal.  If you make any changes to the invoice please update this document accordingly<Br>";
				include_once('_reportRevenueCodes.php');
				
				
			}
			
		?>
	</div>
</form>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		/*
		var getFiltersPaid = function(){
			var rebateType = $("#RebateType").val();
				filterValues = new Array(rebateType);
				return filterValues;
		};
		
		var updateFiltersPaid = function(){
			var Filters=getFiltersPaid();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>muni?RebateType="+Filters[0]+"&nav=customer-data&SelectedCustomerID=<?php echo $SelectedCustomerID;?>#";
		}
		$(".parametersPaid").on('change',function(){
			updateFiltersPaid();
		});
		$("#resetbutton").on('click',function(){
			$(".parameters").val('');
			updateFiltersPaid();
		});
		*/
		$("#invoiceMonth").on('change',function(){
			var $this = $(this);
			//console.log("<?php echo $CurrentServer.$adminFolder;?>muni?nav=report-paid&invoiceDate="+$this.val()+"#invoiceFile");
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>muni?nav=report-paid&invoiceDate="+$this.val()+"#invoiceFile";
		});
		$("#markAsInvoiced").on('click',function(){
			var invoiceDate = $("#invoiceMonth").val();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>muni?nav=report-paid&invoiceDate="+invoiceDate+"&markAsInvoiced=true#invoiceFile";
		});
		var tableRawData = $('.rawDataReport').dataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
							{
								"sExtends":    "copy"
							}
				]				
				
				
			},			
			select: true,
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 20
		});
		
		//force download of file
		//window.location.href = "<?php echo $webtrackingLink;?>";
		
	});
</script>
<script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
	$(function(){
		var hash = window.location.hash,
			tabHashes = new Array(<?php echo implode(",",$tabHashes);?>),
			isTabHash = $.inArray(hash, tabHashes) !== -1,
			setTab = function(tabid){
				var tab = $("#tab-" + tabid),
					content = $("#" + tabid);
				tab.click();
				tab.addClass("active");
				content.addClass("active").show();
			};

		if (isTabHash){
			// let tabs.js take care of this
			setTab(hash.substring(1));
			// if it's the first tab, we need to reposition user at the top
			$("html,body").animate({
				scrollTop: 0
			});
		} else if (!(hash.length)){
			setTab("<?php echo $setTab;?>");
		}
	});
</script>
