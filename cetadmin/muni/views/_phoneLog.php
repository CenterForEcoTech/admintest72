<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$muniProvider = new MuniProvider($dataConn);
$phoneLogResults = $muniProvider->getPhoneLog($criteria);
$phoneLogCollection = $phoneLogResults->collection;

?>
<blockquote><i>"Hello, Westfield Gas and Electric Efficiency program, this is <b><?php echo $_SESSION['AdminAuthObject']['adminFullName'];?></b> speaking, how may I help you?"</i></blockquote>
<h2>Phone Log</h2>
<form id="PhoneLogForm">
	<div class="row">
		<div class="four columns">
			Customer First Name:<br>
			<input type="text" name="firstName" class="inputValue">
		</div>
		<div class="four columns">
			Customer Last Name:<br>
			<input type="text" name="lastName" class="inputValue">
		</div>
	</div>
	<div class="row">
		<div class="three columns">
			<select name="reasonForCall" style="width:150px;" class="inputValue">
				<option value="">Reason For Call</option>
				<option value="Audit Request">Audit Request</option>
				<option value="Misdirected">Misdirected</option>
				<option value="Program Info">Program Info</option>
				<option value="Rebate Help">Rebate Help</option>
				<option value="Rebate Info">Rebate Info</option>
				<option value="Tech Help">Tech Help</option>
			</select>
		</div>
		<div class="five columns">
			<select name="howTheyHeard" style="width:250px;" class="inputValue">
				<option value="">How they heard about program</option>
				<option value="WGELD Website">WGELD Website</option>
				<option value="WGELD Mailing">WGELD Mailing</option>
				<option value="Friend">Friend</option>
				<option value="Newpaper">Newspaper</option>
				<option value="Radio">Radio</option>
				<option value="Other">Other</option>
			</select>
		</div>
		<div class="three colums">
			<select name="actionTaken" style="width:150px;" class="inputValue">
				<option value="">Action Taken</option>
				<option value="Scheduled Audit">Scheduled Audit</option>
				<option value="Sent Paper Work">Sent Paper Work</option>
				<option value="Referred To Another Agency">Referred To Another Agency</option>
				<option value="Provided Info over the phone">Provided Info over the phone</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="five columns">
			Notes:<br>
			<textarea name="notes" class="inputValue"></textarea>
		</div>
	</div>
	<fieldset>
		<legend>Optional Info</legend>
		<div class="row">
			<div class="four columns">
				Municipal Name:<br>
				<select name="municipalName" style="width:170px;" class="inputValue" id="municipalName">
					<option value="Westfield">Westfield</option>
				</select>
			</div>
			<div class="four columns">
				Account Number:<br>
				<input type="text" id="accoutNumber" name="accountNumber" class="three columns inputValue"><br><br>
				<a href="#" id="unknownAccountNumber" class="button-link" style="width:157px;margin-left:10px;">Unknown At This Time</a>
			</div>
			<div class="four columns">
				Call Back Phone Number:<br>
				<input type="text" name="phoneNumber" class="two columns inputValue">
			</div>
		</div>
	</fieldset>
	<div class="row" id="PhoneResults"></div>
	<div class="row">
		<div class="five columns">
			<input type="hidden" name="action" value="phoneCallInfo_save">
			<a href="#" class="button-link" id="savePhoneCallInfo">Save Phone Call Info</a>
		</div>
	</div>
</form>

<div class="row">
	<div class="fifteen columns">
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>TimeStamp</th>
					<th>Reason For Call</th>
					<th>How They Heard</th>
					<th>Action Taken</th>
					<th>Notes</th>
					<th>Optional Info</th>
					<th>Added By</th>
				</tr>
			</thead>
			<tbody>
				<?php	
					foreach ($phoneLogCollection as $ID=>$Record){
						$thisTimeStamp = $Record->timeStamp;
						$newTimeStamp = date('m/d/y g:ia',strtotime("$thisTimeStamp + 3 hours"));
						$rebates = array();
				?>
						<tr>
							<td><?php echo $newTimeStamp;?></td>
							<td><?php echo $Record->reasonForCall;?></td>
							<td><?php echo $Record->howTheyHeard;?></td>
							<td><?php echo $Record->actionTaken;?></td>
							<td><?php echo $Record->notes;?></td>
							<td>
								<?php
								echo $Record->firstName." ".$Record->lastName;
								echo (trim($Record->firstName) || trim($Record->lastName) ? "<br>" : "");
								echo ($Record->accountNumber || $Record->municipalName !="Westfield" ? $Record->municipalName." Account# ".$Record->accountNumber."<br>" : "");
								echo $Record->phoneNumber;
								?>
							</td>
							<td><?php echo emailInitials($Record->addedByAdmin);?></td>
						</tr>
				<?php 
					}//end foreach MonthYearInfo 
				?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var tableRawData = $('.rawDataReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 100
		});
	});
</script>

<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
	
		 var formElement = $("#PhoneLogForm"),
			phoneResults = $("#PhoneResults"),
			inputValueClass = $(".inputValue"),
			municipalName = $("#municipalName");
		 
		 formElement.validate({
			rules: {
				reasonForCall: {
					required: true
				}
			}
		});
		$("#cancel").click(function(e){
			e.preventDefault();
			parent.history.back(); 
			return false;							   
		});
		
		inputValueClass.change(function(){
			var $this = $(this);
			$this.css('border','2pt solid green');
		});
		
		$("#savePhoneCallInfo").click(function(e){

			phoneResults.html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				//console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						//console.log(data);
						phoneResults.html("Saved").addClass("alert").addClass("info");
						inputValueClass.css('border','1pt solid black');
						inputValueClass.val('');
						municipalName.val($("#municipalName option:first").val());
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						phoneResults.html(message).addClass("alert");
					}
				});
			}
		});
		$("#unknownAccountNumber").click(function(e){
			e.preventDefault();
			var UnknownAccountNumber = 'UnknownAtThisTime'+Date.now();
			$("#accoutNumber").val(UnknownAccountNumber);
		});
		
	});
</script>
