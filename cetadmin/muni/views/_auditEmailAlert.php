<?php
	$path = getcwd();
	$pathSeparator = "/";
	if (!strpos($path,"xampp")){
		$thisPath = ".:/home/cetdash/pear/share/pear";
		set_include_path($thisPath);
	}

	require_once "Mail.php";  
	$from = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
//	$to = "Lisa Kohler <Lisa.Kohler@cetonline.org>"; //Lisa asked to have CJ be the contact for this email on 5/25/2017
// CJ asked not to get alerts for other staff on 2/9/2018
	$to = $_SESSION['AdminAuthObject']['adminFullName']." <".$_SESSION['AdminEmail'].">"; 
	$recipients = $to.",IT Admin <it@cetonline.org>";
	$subject = "Audit Data added to NEAT for ".$CustomerName; 
	$host = "smtp.office365.com"; 
	$port = "587"; 
	$username = $_SESSION['AdminEmail']; 
	$password = $_SESSION['AdminAuthenticationCode'];  
	$headers = array (
		'From' => $from,   
		'To' => $to, 
		'Reply-to' => $from,
		'Subject' => $subject
	); 
	$smtp = Mail::factory(
		'smtp',   
		array (
			'host' => $host,     
			'port' => $port,     
			'auth' => true,     
			'username' => $username,     
			'password' => $password
		)
	);  
	$body = "Open Up NEAT, click Site Built (NEAT) button, and select by Client Name: ".$CustomerName." and then click the button for Run Audit";
	//$emailRecordId = $productProvider->movementRecordEmail($body,$to,$headers,$from,$username."QQQ".$password);
	$body = $body."\n\r\n\rTo create the customer report visit: ".$CurrentServer.$adminFolder."muni/?nav=auditForm&CustomerID=".$CustomerID."&TransmitNEAT=true&setTab=SIRResults";
	$mail = $smtp->send($recipients, $headers, $body); 
	//print_pre($mail);
	//$updateEmailRecordId = $productProvider->movementRecordEmailUpdate($emailRecordId,$mail);
?>