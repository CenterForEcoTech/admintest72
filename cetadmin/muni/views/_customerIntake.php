<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$muniProvider = new MuniProvider($dataConn);
$customerDataResults = $muniProvider->getResCustomer();
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
}

//get names for rebate status
$rebateStatusNameResults = $muniProvider->getRebateStatusNames();
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
}

$SelectedCustomerID = $_GET['CustomerID'];
//print_pre($CustomerByID[$SelectedCustomerID]);
include_once("views/_customerIntakeForm.php");
?>
