<?php
ini_set('memory_limit','14096M');
set_time_limit(0);
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel.php';

$InvoiceNum = "# ".date("Ym",strtotime($invoiceMonth));
$InvoiceNumber["WG&E"] = $InvoiceNum;

$startMonth = date("Y-m-01",strtotime($invoiceMonth));
$endMonth = date("Y-m-t",strtotime($invoiceMonth));
$InvoicePoNumber = "# 15-78";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CET")
							 ->setLastModifiedBy("CET")
							 ->setTitle("WGE Invoice".$InvoiceDate)
							 ->setSubject("WGE Invoice".$InvoiceDate)
							 ->setDescription("WGE Invoice");
							 
//START SERVICES RECORDS
// Add header data
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->setActiveSheetIndex(1);
} catch (PHPExcel_Exception $e) {
}
$rowCounter = 1;
$styleBoldArray = array(
		'font'  => array(
			'bold' => true,
			'size'  => 14,
			'name' => 'Arial'
    ));
	
$invoicedAudits = array();
include('views/_reportPaidInvoiceFile_ServicesSheet.php');

// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Services');
} catch (PHPExcel_Exception $e) {
}
//END SERVICES RECORDS
							 
							 
							 
//START INVOICE COVER SHEET
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$gdImage = imagecreatefromjpeg('../images/cet_invoicelogoBW.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('CET Logo');$objDrawing->setDescription('CET Logo');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(75);
$objDrawing->setCoordinates('A2');
try {
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("E2", "INVOICE");
} catch (PHPExcel_Exception $e) {
}


try {
    $objPHPExcel->getActiveSheet()->setCellValue("A7", "Invoice for Westfield Gas + Electric Efficiency Program");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A9", "112 Elm Street");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A10", "Pittsfield, MA  01201");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A11", "[413] 446-2291");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("D9", "INVOICE NO.");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D10", "DATE");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D11", "REF PO NUMBER");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D12", "PERIOD");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E9", " " . $InvoiceNum);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E10", " " . date("m/d/Y"));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E11", " " . $InvoicePoNumber);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E12", " " . date("n/j/y", strtotime($invoiceMonth)) . " to " . date("n/t/y", strtotime($invoiceMonth)));
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("A13", "TO");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B13", "Westfield Gas and Electric");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B14", "100 Elm St.");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B15", "Westfield, MA 01085");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("A17", " ");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B17", "DESCRIPTION");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C17", "QTY");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D17", "UNIT PRICE");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E17", "TOTAL");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("A18", "Efficiency Program Services");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A25", "Hourly Services");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B18", "Monthly Service and Administration Fee - " . date("M Y", strtotime($invoiceMonth)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C18", "1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D18", "2750.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E18", "=(D18*C18)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40205"] = bcadd($revenueCodes["WG&E"]["40205"], $objPHPExcel->getActiveSheet()->getCell("E18")->getCalculatedValue(), 2);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B19", "Energy Audit");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C19", "=Services!B1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D19", "230.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E19", "=(D19*C19)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40202"] = $objPHPExcel->getActiveSheet()->getCell("E19")->getCalculatedValue();
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B20", "Equipment Verification");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C20", "=Services!I1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D20", "120.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E20", "=(D20*C20)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40205"] = bcadd($revenueCodes["WG&E"]["40205"], $objPHPExcel->getActiveSheet()->getCell("E20")->getCalculatedValue(), 2);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B21", "Weatherization Verification");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C21", "=Services!N1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D21", "120.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E21", "=(D21*C21)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40205"] = bcadd($revenueCodes["WG&E"]["40205"], $objPHPExcel->getActiveSheet()->getCell("E21")->getCalculatedValue(), 2);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B22", "Rebate Processing - Weatherization");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C22", "=Services!L1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D22", "35.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E22", "=(D22*C22)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40205"] = bcadd($revenueCodes["WG&E"]["40205"], $objPHPExcel->getActiveSheet()->getCell("E22")->getCalculatedValue(), 2);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B23", "Rebate Processing - Equipment");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C23", "=Services!G1");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D23", "38.00");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E23", "=(D23*C23)");
} catch (PHPExcel_Exception $e) {
}
try {
    $revenueCodes["WG&E"]["40205"] = bcadd($revenueCodes["WG&E"]["40205"], $objPHPExcel->getActiveSheet()->getCell("E23")->getCalculatedValue(), 2);
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B24", "Efficiency Program Services Total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E24", "=SUM(E18:E23)");
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->setCellValue("B25", "Hourly Services total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E25", "='Hourly Services'!D15");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B26", "Hourly Services total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E26", "='Hourly Services'!D15");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C27", "TOTAL AMOUNT DUE");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("E27", "=SUM(E26,E24)");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->mergeCells("A18:A24");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->mergeCells("A25:A26");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->mergeCells("C27:D27");
} catch (PHPExcel_Exception $e) {
}
//Set Styles
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(48);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(19);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(24);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("E2")->applyFromArray(array('font' => array('bold' => true, 'size' => 14)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A7")->applyFromArray(array('font' => array('bold' => true)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1:E28")->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FFFFFFFF'))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("D9:D12")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("E9:E12")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('A18:A26')->getAlignment()->setWrapText(true);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A18:A26")->applyFromArray(array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP), 'borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('A24')->applyFromArray(array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN), 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('A25')->applyFromArray(array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('A26')->applyFromArray(array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN), 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('B25:E25')->applyFromArray(array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('E24:E26')->applyFromArray(array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('B24:E24')->applyFromArray(
        array(
            'borders' => array(
                'left' => array('left' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('B25:E26')->applyFromArray(
        array(
            'borders' => array(
                'left' => array('left' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('D18:E24')->getNumberFormat()->setFormatCode('_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('E25:E27')->getNumberFormat()->setFormatCode('_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A17:E17")->applyFromArray(
        array(
            'fill' =>
                array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'E4EAF4')
                ),
            'borders' => array(
                'inside' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font' => array('bold' => true)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("B18:E23")->applyFromArray(
        array(
            'borders' => array(
                'inside' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("B24")->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array('bold' => true, 'italic' => true)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("E24")->applyFromArray(
        array(
            'font' => array('bold' => true)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("B26")->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array('bold' => true, 'italic' => true)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("E26")->applyFromArray(
        array(
            'font' => array('bold' => true)
        )
    );
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("C27:E27")->applyFromArray(array('font' => array('bold' => true, 'size' => 14)));
} catch (PHPExcel_Exception $e) {
}
// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Invoice');
} catch (PHPExcel_Exception $e) {
}

try {
    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
} catch (PHPExcel_Exception $e) {
}

//END INVOICE SHEET
							 
							 
//START Hourly
//create a new worksheet
try {
    $objPHPExcel->createSheet();
} catch (PHPExcel_Exception $e) {
}
// Add header data
try {
    $objPHPExcel->setActiveSheetIndex(2);
} catch (PHPExcel_Exception $e) {
}
$hourlyServicesArray = array(
	"4"=>array("title"=>"Director","rate"=>140),
	"5"=>array("title"=>"Management","rate"=>120),
	"6"=>array("title"=>"Administrative","rate"=>85),
	"7"=>array("title"=>"Marketing Management","rate"=>120),
	"8"=>array("title"=>"Marketing Specialist","rate"=>95),
	"9"=>array("title"=>"IT Management","rate"=>130),
	"10"=>array("title"=>"IT Specialist","rate"=>130),
	"11"=>array("title"=>"Training Specialist","rate"=>95),
	"12"=>array("title"=>"Trainees","rate"=>95),
	"13"=>array("title"=>"Energy Specialist","rate"=>100),
	"14"=>array("title"=>"IT Consultant","rate"=>180)
);
foreach ($hourlyServicesArray as $row=>$data){
	$title = $data["title"];
	$employeesInTitle = $BillingRateEmployees["WGE"][$title];
	$thisTotalHours = "";
	if (count($employeesInTitle)){
		foreach ($employeesInTitle as $employee_name){
			$thisTotalHours = bcadd($thisTotalHours,$InvoiceData[$employee_name]["TotalHours"],2);
		}
	}
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $row, $title);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $row, $thisTotalHours);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $row, $data["rate"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $row, "=(B" . $row . "*C" . $row . ")");
    } catch (PHPExcel_Exception $e) {
    }
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Hourly Services");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A3", "Positions");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("B3", "Hours");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("C3", "Rate");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D3", "Total");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("A15", "Hourly Subtotal");
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->setCellValue("D15", "=SUM(D4:D12)");
} catch (PHPExcel_Exception $e) {
}

//Set Styles
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray(array('font' => array('bold' => true, 'size' => 14)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->applyFromArray(array('font' => array('bold' => true)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle("A15:D15")->applyFromArray(array('font' => array('bold' => true)));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('B4:D14')->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
} catch (PHPExcel_Exception $e) {
}
try {
    $objPHPExcel->getActiveSheet()->getStyle('C4:D15')->getNumberFormat()->setFormatCode('_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)');
} catch (PHPExcel_Exception $e) {
}


// Rename worksheet
try {
    $objPHPExcel->getActiveSheet()->setTitle('Hourly Services');
} catch (PHPExcel_Exception $e) {
}
//END ENERGY Audits
if (count($previouslyInvoicedAudits)){
	$auditRecords = $previouslyInvoicedAudits;
	$rebates = $previouslyInvoicedRebates;
	//START PREVIOUSLY INVOICED RECORDS
	// Add header data
	//create a new worksheet
    try {
        $objPHPExcel->createSheet();
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->setActiveSheetIndex(3);
    } catch (PHPExcel_Exception $e) {
    }
    $rowCounter = 1;
	$styleBoldArray = array(
			'font'  => array(
				'bold' => true,
				'size'  => 14,
				'name' => 'Arial'
		));
	$previouslyInvoiceAuditsDisplay = true;	
	include('views/_reportPaidInvoiceFile_ServicesSheet.php');

	// Rename worksheet
    try {
        $objPHPExcel->getActiveSheet()->setTitle('Previously Invoiced Services');
    } catch (PHPExcel_Exception $e) {
    }
    //END PREVIOULSY SERVICES RECORDS
	
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
try {
    $objPHPExcel->setActiveSheetIndex(0);
} catch (PHPExcel_Exception $e) {
}
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/muni/";}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$invoiceFile = "exportWGEInvoice_".$invoiceMonth."_".date("s").".xlsx";

$pathExtension = "exports".$pathSeparator.$invoiceFile;
$webpathExtension = "exports".$webpathSeparator.$invoiceFile;
$saveLocation = $path.$pathExtension;


//echo $saveLocation;
try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
$webinvoiceLink = $CurrentServer.$adminFolder."muni/".$webpathExtension;
echo "<a id='WGEInvoiceFile' href='".$webinvoiceLink."'>".$invoiceFile."</a><br>";

//now process invoiced Items
if($_GET['markAsInvoiced']){
	if (count($invoicedAudits)){
		$auditIdList = implode(",",$invoicedAudits);
		$auditInvoicedResults = $muniProvider->updateAuditInvoiced($auditIdList,getAdminId());
	}
	echo count($invoicedAudits)." Audit".(count($invoicedAudits) == 1 ? " " : "s ")."Included for Invoicing<br>";
	if (count($previouslyInvoicedAudits)){
		echo count($previouslyInvoicedAudits)." Audit".(count($previouslyInvoicedAudits) == 1 ? " " : "s "). "Previously Invoiced<br>";
	}
	foreach ($invoicedRebates as $formName=>$rebateIds){
		$invoicedRebatesCount = count($rebateIds);
		if ($invoicedRebatesCount){
			$rebateIdList = implode(",",$rebateIds);
			$rebateInvoicedResults = $muniProvider->updateRebateInvoicedDate($formName,$rebateIdList,getAdminId());
		}
	}
	echo $invoicedRebatesCount." Rebate".($invoicedRebatesCount == 1 ? " " : "s ")."Included for Invoicing<br>";
	if ($previouslyInvoicedRebatesCount){
		echo $previouslyInvoicedRebatesCount." Rebate".($previouslyInvoicedRebatesCount == 1 ? " " : "s "). "Previously Invoiced<br>";
	}
}
?>