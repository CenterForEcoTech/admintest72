<?php
set_time_limit(300000);
ini_set('memory_limit','14096M');
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."MuniProvider.php");

$SelectedRebateType = $_GET['RebateType'];
$criteria = new stdClass();
if ($SelectedRebateType){$criteria->rebateType = $SelectedRebateType;}
$muniProvider = new MuniProvider($dataConn);
$customerDataResults = $muniProvider->getResCustomer($criteria);
$customerCollection = $customerDataResults->collection;
//print_pre($customerCollection);
$auditFields = array("auditFloorArea","auditHouseWidth","auditHouseLength","auditConditionedStories","auditHeatingEquipmentType","auditHeatingFuel");
$CustomerIDWithAuditInfo = array();
foreach ($customerCollection as $result=>$record){
	$CustomerByID[$record->id] = $record;
		
	foreach ($auditFields as $auditField){
		if ($record->{$auditField}){
			$CustomerIDAuditInfo[$record->id][$auditField] = $record->{$auditField};
		}
	}
}
foreach ($CustomerIDAuditInfo as $CustomerID=>$auditField){
	if ($auditField){
		$CustomerIDWithAuditInfo[] = $CustomerID;
	}
}

//get names for rebate status
$rebateStatusNameResults = $muniProvider->getRebateStatusNames();
$rebateStatusNameCollection = $rebateStatusNameResults->collection;
foreach ($rebateStatusNameCollection as $statusResult=>$statusRecord){
	$RebateStatusNameByID[$statusRecord->id] = $statusRecord->name;
	$RebateStatusByID[$statusRecord->id] = $statusRecord;
}
$auditStatusNameResults = $muniProvider->getAuditStatusNames();
$auditStatusNameCollection = $auditStatusNameResults->collection;
foreach ($auditStatusNameCollection as $statusResult=>$statusRecord){
	$AuditStatusNameByID[$statusRecord->id] = $statusRecord->name;
	$AuditStatusByID[$statusRecord->id] = $statusRecord;
}
$rebateTypes = array("Heat Hot water"=>"rebateHeatHotwater","Efficiency"=>"rebateEfficiency","Cool Home"=>"rebateCoolHome","Pool Pump"=>"rebatePoolPump","Appliance"=>"rebateAppliance","Commercial Heat"=>"rebateCommercialHeat","WiFi"=>"rebateWiFi");
$SelectedRebateType = $_GET['RebateType'];
$SelectedCustomerID = $_GET['CustomerID'];
//print_pre($CustomerByID[$SelectedCustomerID]);
$auditStatus = $CustomerByID[$SelectedCustomerID]->auditStatus;
?>
<?php include_once('views/_customerIntakeForm.php');?>
<br clear="all">
<hr>
<h3>Existing Customers</h3>
<div class="row">
	<div class="twelve columns">
		<fieldset class="eight columns">
			<legend>Filters</legend>
			Rebate Type: <select id="RebateType" class="parameters">
				<option value="">All Rebate Types</option>
			<?php 
				foreach ($rebateTypes as $arrayName=>$fieldName){
					echo "<option value=\"".$fieldName."\"".($SelectedRebateType==$fieldName ? " selected" : "").">".$arrayName."</option>";
				}
			?></select>
		</fieldset>
		<fieldset class="twelve columns">
			<legend>Rebate Status Key</legend>
			<?php 
				foreach ($RebateStatusByID as $id=>$record){
					echo " <div class='rebateStatusKey' style='cursor:pointer;position:relative;float:left;padding:2px;border:1pt solid ".$record->borderColor.";background-color:".$record->backgroundColor.";color:".$record->textColor."'>".$record->name."</div>";
				}
			?>
		</fieldset>
		<fieldset class="fourteen columns">
			<legend>Audit Status Key</legend>
			<?php 
				foreach ($AuditStatusByID as $id=>$record){
					echo " <div class='auditStatusKey' style='cursor:pointer;position:relative;float:left;padding:2px;border:1pt solid ".$record->borderColor.";background-color:".$record->backgroundColor.";color:".$record->textColor."'>".$record->name."</div>";
				}
			?>
		</fieldset>
	</div>
</div>
<div class="row">
	<div class="fifteen columns">
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Last Updated</th>
					<th>Received Date</th>
					<th>Account Number</th>
					<th>LastName</th>
					<th>FirstName</th>
					<th>Address</th>
					<th>Email</th>
					<th>Rebate Status</th>
					<th>Audit Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$rowId = 0;
					foreach ($customerCollection as $ID=>$Record){
						//Filters names with commas or ampersands to standardize data for NEAT
					/*	if (strpos($Record->accountHolderFirstName,"&")!== FALSE)
							$Record->accountHolderFirstName = str_replace("&","and",$Record->accountHolderFirstName);
						if (strpos($Record->accountHolderLastName,"&")!== FALSE)
							$Record->accountHolderLastName = str_replace("&","and",$Record->accountHolderLastName);
						if (strpos($Record->accountHolderLastName,"'")!== FALSE)
							$Record->accountHolderLastName = str_replace("'","",$Record->accountHolderLastName); */
						//echo "First Name: ".$Record->accountHolderFirstName." Last Name: ".$Record->accountHolderLastName."<br>"; 
						$thisTimeStamp = ($Record->updatedTimeStamp ? $Record->updatedTimeStamp : $Record->addedTimeStamp);
						$newTimeStamp = date('Y-m-d g:ia',strtotime($thisTimeStamp." + 3 hours"));
						$thisReceivedStamp = $Record->addedTimeStamp;
						$newReceivedStamp = date('Y-m-d g:ia',strtotime("$thisReceivedStamp + 3 hours"));
						$rebates = array();
						foreach ($rebateTypes as $arrayName=>$fieldName){
							if ($Record->{$fieldName} > 0){
								$fieldName = str_replace("rebate","",$fieldName);
								//echo $fieldName;

								//get list of all rebates for this type for this client
								$criteria = new stdClass();
								$criteria->rebateName=str_replace("rebate","",$fieldName);
								$criteria->customerId = $Record->id;
								$rebateInfoResults = $muniProvider->getRebateInfo($criteria);
								$rebateInfoCollection = $rebateInfoResults->collection;
								//print_pre($rebateInfoCollection);
								//echo $rebateName;
								//echo $fieldName."=";
								$ThisRebateFieldHeader = ($fieldName == "CommercialHeat" ? "MuniComRebateHeatHotwater" : "MuniResRebate".str_replace(" ","",str_replace("Water","water",$fieldName)));
								//echo $ThisRebateFieldHeader."<br>";
								foreach ($rebateInfoCollection as $rebateResult=>$rebateRecord){
									$rebates[$arrayName][] = array("rebateId"=>$rebateRecord->{$ThisRebateFieldHeader."_ID"},"status"=>$rebateRecord->CurrentStatus);
								}
							}
						}
						//get RebateStatus
						$rebateStatusLine = "";
						$auditStatusLine = "";
						$canBeDeleted = true;
						$countOfRebateTypes = count($rebates);

						foreach ($rebates as $rebate=>$statusInfoArray){
							$countOfRebateStatus = count($statusInfoArray);
							foreach ($statusInfoArray as $statusInfo){
								$deleteRebateLink = "<img src='".$CurrentServer."images/redx-small.png' style='text-align;right;' class='deleteRebate' data-rebateid='".$statusInfo["rebateId"]."' data-rebatename='".str_replace(" ","",$rebate)."' data-customerid='".$Record->id."' data-statusdiv='rebateStatusDiv".$Record->id."_".str_replace(" ","",$rebate).$statusInfo["rebateId"]."'>";
								if ($statusInfo["status"] > 1) {
									$canBeDeleted = false;
									$deleteRebateLink = "";
								}
								if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni)) && $canBeDeleted){
									$deleteRebateLink = $deleteRebateLink;
								}else{
									$deleteRebateLink = "";
								}
								$statusLine = "
									<div id='rebateStatusDiv".$Record->id."_".str_replace(" ","",$rebate).$statusInfo["rebateId"]."'style='height:20px;text-align:right;padding:2px;background-color:".$RebateStatusByID[$statusInfo["status"]]->backgroundColor.";border:1pt solid ".$RebateStatusByID[$statusInfo["status"]]->borderColor.";'>
										<a style='float:left;height:20px;text-decoration:none;color:".$RebateStatusByID[$statusInfo["status"]]->textColor.";' href='".$CurrentServer.$adminFolder."muni/?nav=rebate-".strtolower(str_replace(" ","",$rebate))."&name=".$rebate."&CustomerID=".$Record->id."&RebateID=".$statusInfo["rebateId"]."'>".$rebate."</a>
										<span style='display:none;'>".$RebateStatusByID[$statusInfo["status"]]->name."</span>".$deleteRebateLink."
									</div>
									";
								if ($statusInfo["status"] > 0){
									if ($SelectedRebateType){
											if ($SelectedRebateType == $rebateTypes[$rebate]){
												$rebateStatusLine .= $statusLine;
											}
									}else{
										$rebateStatusLine .= $statusLine;
									}
								}
							}
						}
						if ($Record->auditStatus > 1){$canBeDeleted = false;} //LH change back to 1 and true
							$statusLine = "
									<div id='auditStatusDiv".$Record->id."'style='height:20px;text-align:right;padding:2px;background-color:".$AuditStatusByID[$Record->auditStatus]->backgroundColor.";border:1pt solid ".$AuditStatusByID[$Record->auditStatus]->borderColor.";'>
										<a style='float:left;height:20px;text-decoration:none;color:".$AuditStatusByID[$Record->auditStatus]->textColor.";' href='".$CurrentServer.$adminFolder."muni/?nav=auditForm&CustomerID=".$Record->id."'>".$AuditStatusByID[$Record->auditStatus]->name."</a>
										<span style='display:none;'>".$AuditStatusByID[$Record->auditStatus]->name."</span>".$deleteAuditLink."
									</div>
									";
								if ($Record->auditStatus > 0){
									$auditStatusLine .= $statusLine;
								}

						
				?>
						<tr id='row<?php echo $ID;?>'<?php echo ($Record->id == $SelectedCustomerID ? " style='background-color:lightgreen;border:1pt solid green;'":"");?>>
							<td><?php echo $newTimeStamp;?></td>
							<td>
								<?php echo $newReceivedStamp;?>
								<?php if (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Muni)) && $canBeDeleted){?>
									<img data-rowid='row<?php echo $ID;?>' data-recordid='<?php echo $Record->id;?>' src="<?php echo $CurrentServer;?>images/redx-small.png" class="deleteUser" style="cursor:pointer;" title="This Will Delete This Customer">
								<?php }?>

							</td>
							<td><?php echo ($Record->id == $SelectedCustomerID ? "<span style='display:none;'>".(count($customerCollection)+1000)."</span>":"");?><a href="<?php echo $CurrentServer.$adminFolder."muni/?nav=customer-data&CustomerID=".$Record->id;?>"><?php echo $Record->utilityAccountNumber;?></a></td>
							<td><?php echo $Record->accountHolderLastName;?></td>
							<td><?php echo $Record->accountHolderFirstName;?></td>
							<td><?php echo $Record->installedAddress;?></td>
							<td><?php echo $Record->installedEmail;?></td>
							<td nowrap="nowrap" class='rebateStatusTD'><?php echo $rebateStatusLine;?></td>
							<td nowrap="nowrap" class='auditStatusTD'><?php echo $auditStatusLine;?></td>
						</tr>
				<?php 
						$rowId++;
					}//end foreach MonthYearInfo 
				?>
			</tbody>
		</table>
	</div>
</div>
<input type="hidden" id="alertVariableStatus" value="show">
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var getFilters = function(){
			var rebateType = $("#RebateType").val();
				filterValues = new Array(rebateType);
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>muni?RebateType="+Filters[0]+"&nav=customer-data&SelectedCustomerID=<?php echo $SelectedCustomerID;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$(".parameters").val('');
			updateFilters();
		});
		
		var tableRawData = $('.rawDataReport').dataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 100
		});
		$(".rebateStatusKey").on('click',function(){
			var $this = $(this);
			$("#DataTables_Table_0_filter").find('input[type=search]').val($this.text()).keyup();
		});
		$(".auditStatusKey").on('click',function(){
			var $this = $(this);
			$("#DataTables_Table_0_filter").find('input[type=search]').val($this.text()).keyup();
		});

		var alertVariableStatus = $("#alertVariableStatus");
		$(".deleteUser").on('click',function(){
			var confirmed = confirm('This will delete this customer and all their rebates.  Are you sure you want to delete?');
			if (confirmed){
				var $this = $(this),
					thisRecordId = $this.attr('data-recordid'),
					thisRowId = $this.attr('data-rowid'),
					data = {};
					data['recordId'] = thisRecordId;
					data['status'] = 'Deleted';
					data['action'] = 'update_customerstatus';
					
				//look for deletable rebates and mark those as deleted first
				deletableDivs = $this.parent().parent().children('td.rebateStatusTD').children();//'img.deleteRebate');
				$.each(deletableDivs,function(key,deletableDiv){
					alertVariableStatus.val('hide');
					deleteLinks = $(deletableDiv).children('img.deleteRebate').click();
				});
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						console.log(data)
						var rowId = "#"+thisRowId;
						console.log('will delete row'+rowId);
							tableRawData.fnDeleteRow($(rowId));	
						//$('#onlineRebateResults').html("Saved").addClass("alert").addClass("info");
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log("error"+jqXHR);
						var message = $.parseJSON(jqXHR.responseText);
						//$('#onlineRebateResults').html(message).addClass("alert");
					}
				});
				alertVariableStatus.val('show');
			}
		});
		
		$(".deleteRebate").on('click',function(){
			//check the alertvariablestatus
			var alertVariableStatusVal = alertVariableStatus.val();
			if (alertVariableStatusVal == "show"){
				var confirmed = confirm('This will delete this rebate.  Are you sure you want to delete?');
			}else{
				var confirmed = true;
				
			}
			if (confirmed){
				var $this = $(this),
					thisRebateId = $this.attr('data-rebateid'),
					thisCustomerId = $this.attr('data-customerid'),
					thisRebateName = $this.attr('data-rebatename'),
					thisStatusDiv = "#"+$this.attr('data-statusdiv');
				var StatusChange = {};
					StatusChange["action"] = "update_currentstatus";
					StatusChange["rebateId"] = thisRebateId;
					StatusChange["currentStatus"] = '0';
					StatusChange["currentText"] = 'Deleted';
					StatusChange["rebateName"] = thisRebateName;
					StatusChange["customerId"] = thisCustomerId;
				var	data = StatusChange;
				$.ajax({
					url: "ApiCustomerData.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$(thisStatusDiv).hide();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						console.log(message);
					}
				});				
			}
		});
		
	});
</script>