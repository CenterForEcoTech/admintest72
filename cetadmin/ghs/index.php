<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - GHS";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "inspection-schedule":
            $displayPage = "views/_inspectionSchedule.php";
            break;
        case "manage-codes":
            $displayPage = "views/_manageCodes.php";
            break;
        case "manage-groups":
            $displayPage = "views/_manageGroups.php";
            break;
        case "manage-hoursentry":
            $displayPage = "views/_manageHoursEntry.php";
            break;
        case "manage-submittedhours":
            $displayPage = "views/_manageSubmittedHours.php";
            break;
		case "reports":
			$displayPage = "_reportsTabs.php";
			break;
        case "report-bycode":
            $displayPage = "views/_reportByCode.php";
            break;
        case "report-bygroup":
            $displayPage = "views/_reportByGroup.php";
            break;
        case "report-bystaff":
            $displayPage = "views/_reportByStaff.php";
            break;
        case "report-rawdata":
            $displayPage = "views/_manageRawData.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>GHS Administration</h1>';
include_once("_ghsMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>