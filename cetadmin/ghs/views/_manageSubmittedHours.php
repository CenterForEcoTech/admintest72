<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
//include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$ThisDepartment = 'Green Home Services';
$ThisDepartmentShort = "GHS";

//Get GBSSupervised
$gbsProvider = new HoursProvider($dataConn);
$criteria = new stdClass();
$criteria->department = $ThisDepartmentShort;
$paginationsResults = $gbsProvider->getSupervisedBy($criteria);
$resultArray = $paginationsResults->collection;
foreach($resultArray as $result=>$record){
	$SupervisedBy[$record->employeeEmail] = $record->supervisorName;
}

$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->department = $ThisDepartment;
$criteria->hoursTrackingGHS = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
//print_pre($resultArray);

$CurrentSupervisorName = $_SESSION["AdminAuthObject"]["adminFullName"];
//echo "Currently Logged in As :".$CurrentSupervisorName ;
//$CurrentSupervisorName = "Denice Hallstein";
//$CurrentSupervisorName = "Heather McCreary";
//$CurrentSupervisorName = "Julie Craumer";
//$CurrentSupervisorName = "Lisa Kohler";
//$CurrentSupervisorName = "Jae McAuley";
//echo " Testing As :".$CurrentSupervisorName;

$TransitionaryManagerExceptionsPeckingOrder = array("Jae McAuley","Lisa Kohler","Denice Hallstein");
$TransitionaryManagerExceptions["Jae McAuley"] = array();
$TransitionaryManagerExceptions["Lisa Kohler"] = array("Jae McAuley");
$TransitionaryManagerExceptionList = $TransitionaryManagerExceptions[$CurrentSupervisorName];
$TransitionaryPeriod = false;
	
$nonDepartmentSupervisorDefault = ($TransitionaryPeriod ? "Denice Hallstein" : "Denice Hallstein");
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeEmailByID[$record->id] = $record->fullName." <".$record->email.">";
	
	$nonDepartmentSupervisor = ($SupervisedBy[$record->email] ? $SupervisedBy[$record->email] : $nonDepartmentSupervisorDefault);
	$manager = (trim($record->department) != $ThisDepartment ? $nonDepartmentSupervisor : $record->manager);
	if ($TransitionaryPeriod){
		$manager = (in_array($record->fullName,$TransitionaryManagerExceptionList) ? "Lorenzo Macaluso" : $CurrentSupervisorName);
		if (!in_array($CurrentSupervisorName,$TransitionaryManagerExceptionsPeckingOrder)){ $manager = "Lorenzo Macaluso";} 
	}
	//exception for Denice
	if ($manager == "Michelle Dailey"){
		$manager = "Denice Hallstein";		
	};
	//exception for Jae
	//$manager = (trim($record->title) == "Director of ".$ThisDepartment ? "Jae McAuley" : $manager);
	if ($CurrentSupervisorName == "Jae McAuley"){
		$manager = "Jae McAuley";		
	};
	//exception for Liz during transition period
	//if ($CurrentSupervisorName == "Liz Budd"){$manager = "Liz Budd";}
	if (strtolower($record->fullName) == strtolower($CurrentSupervisorName)){
		$manager = $CurrentSupervisorName;
	}

	$EmployeesByManager[$manager][]=$record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
if (count($EmployeesByManager[$CurrentSupervisorName])){
	
	//print_pre($EmployeesByManager[$CurrentSupervisorName]);
	//	foreach ($EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName] as $id=>$ManageesName){

	$StartOfThisMonth = date("m/01/Y");
	$EndOfThisMonth = date("m/t/Y");
	$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $StartOfThisMonth);
	$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $EndOfThisMonth);
	//figure out if last day of month is not a sunday how to include the next days of the following month
	$lastDayOfWeekOfEndDate = date("w",strtotime($EndDate));
	$DaysUntilSunday = (7-$lastDayOfWeekOfEndDate);
	$NewEndDate = date("m/d/Y",strtotime($EndDate." + ".$DaysUntilSunday." days"));

	//get the number of days per month
	$nextMonthName = date("M",strtotime($StartDate));
	$lastMonthName = date("M",strtotime($EndDate));
		$ThisMonth = date("n",strtotime($StartDate));
		$ThisYear = date("Y",strtotime($StartDate));
		$MonthYear = $nextMonthName." ".$ThisYear;
		//get all the days at once:
		$endDay = date("t",strtotime($StartDate));
		$thisDay = 1;
		$FirstSunday = date("Y-n-j",strtotime("First Sunday of ".$ThisYear."-".$ThisMonth."-1"));
		$FirstSundayDay = date("j",strtotime($FirstSunday));
		$LastSaturday = date("Y-n-j",strtotime("Last Saturday of ".$ThisYear."-".$ThisMonth."-1"));
		$LastSaturdayDay = date("j",strtotime($LastSaturday));
		$FirstMonday = date("Y-n-j",strtotime("First Monday of ".$ThisYear."-".$ThisMonth."-1"));
		$FirstMondayDay = date("j",strtotime($FirstMonday));
		$LastSunday = date("Y-n-j",strtotime("Last Sunday of ".$ThisYear."-".$ThisMonth."-1"));
		$LastSundayDay = date("j",strtotime($LastSunday));
		$FirstDayOfMonth = $ThisYear."-".$ThisMonth."-1";
		//USING PayPeriods month start and end dates
		$usingPayPeriods = true;
		if ($usingPayPeriods){
			$FirstMonday = ($FirstMondayDay > 2 ? date("Y-n-j",strtotime($FirstMonday." -1 week")) : $FirstMonday);
			$LastSunday = ($LastSundayDay < $endDay ? date("Y-n-j",strtotime($LastSunday." +1 week")) : $LastSunday);
			$thisDay = $FirstMonday;
			$endDay = $LastSunday;
			while (strtotime($thisDay) <= strtotime($endDay)){
				$HorizontalDisplay[$MonthYear]["Days"][] = date("d",strtotime($thisDay));
				$ThisWeek = date("W", strtotime($thisDay)); //+1 day to get week number for weeks that start or Sunday instead of Monday
				$Weeks[$ThisWeek][] = date("Y-m-d",strtotime($thisDay));
				$thisDay = date("Y-n-j",strtotime($thisDay." +1 day"));
			}
		}else{
			/* USING Calendar Month start and end dates */
			while ($thisDay <= $endDay){
				$HorizontalDisplay[$MonthYear]["Days"][] = $thisDay;
				$ThisWeek = date("W", strtotime($ThisYear."-".$ThisMonth."-".$thisDay));
				$Weeks[$ThisWeek][] = $ThisYear."-".$ThisMonth."-".$thisDay;
				$thisDay++;
			}
		}
	$weekCount = 1;
	$FirstMonday = date("Y-m-d",strtotime($FirstMonday));

	foreach ($Weeks as $Number=>$Dates){
		if (in_array($FirstMonday,$Dates)){
			$FirstWeekToStart = $Number;
		}
		$LastWeek = $Number;
		$weekCount++;

	}
	$ShowThisWeek =(int)$FirstWeekToStart;
	$weekCount = 1;
	$TodaysWeekPicker = 1;
	//compensate for last days of Dec might be part of first week for January
	if ((int)$LastWeek < (int)$FirstWeekToStart){$LastWeek = 53;}
	while ($ShowThisWeek <=$LastWeek){
		$LeadingZero = "";
		if ($ShowThisWeek < 10){$LeadingZero = "0";}
		$FirstDateOfWeek = current($Weeks[$LeadingZero.$ShowThisWeek]);
		$LastDateOfWeek = end($Weeks[$LeadingZero.$ShowThisWeek]);
		if (date("Y",strtotime($FirstDateOfWeek)) != "1969"){ //ignore dates that do not calculate because of compensating for last days of Dec that might be part of first week in January
			$WeekPicker[$weekCount]["display"] = date("m/d/Y",strtotime($FirstDateOfWeek))." to ".date("m/d/Y",strtotime($LastDateOfWeek));
			$WeekPicker[$weekCount]["start"] = $FirstDateOfWeek;
			$WeekPicker[$weekCount]["end"] = $LastDateOfWeek;
		}
		//check to see which week TodaysDate is in
		if (strtotime($TodaysDate) >= strtotime($FirstDateOfWeek) && strtotime($TodaysDate) <= strtotime($LastDateOfWeek)){
			$TodaysWeekPicker = $weekCount;
		}

		$ShowThisWeek++;
		$weekCount++;
	}
	$WeekPickerNumberGet = $_GET["WeekPickerNumber"];
	$WeekPickerNumber = ($WeekPickerNumberGet != "null" && $WeekPickerNumberGet !="undefined" ? $WeekPickerNumberGet : $TodaysWeekPicker);
	?>
	<div class="twelve columns">
		<h3>GHS Hours Submitted for Staff Supervised By <?php echo $CurrentSupervisorName;?></h3>
	</div>
	<?php
		//get codes
		$criteria = new stdClass();
		$criteria->showAll = true;
		$criteria->noLimit = true;
		$criteria->department = $ThisDepartmentShort;
		$paginationResult = $gbsProvider->getCodes($criteria);
		$resultArray = $paginationResult->collection;
		foreach ($resultArray as $result=>$record){
			$CodeByID[$record->id] = $record;
			if ($record->name == "501"){
				$SickVacationCode[] = $record->id;
			}
			if ($record->displayOrderId){
				$CodesActive[$record->displayOrderId]=$record;
				$CodesByName[$record->name]=$record;
				$CodesById[$record->id]=$record;
	//			$CodesById[]=$record;
				$CodeNames[$record->name] = $record->id;
			}else{
				$CodesInActive[$record->name]=$record;
			}

		}
		
		$criteria = new stdClass();
		$criteria->noteDate = $WeekPicker[$WeekPickerNumber]["start"];
		$criteria->department = $ThisDepartmentShort;
		$paginationResult = $gbsProvider->getNotes($criteria);
		$resultArray = $paginationResult->collection;
		foreach ($resultArray as $result=>$record){
			$Notes[$record->employeeId] = $record->note;
		}
	?>


	<br clear="all">
	<div style="display:none;">
		<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
		<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
		<button>reset filters</button>
	</div>
	<style>
	.ComboBoxInput {min-width:200px;}
	.dayInput {border-right:1pt solid #aed0ea;border-bottom:0pt solid #aed0ea;min-width:15px;text-align:center;}
	table.dataTable td {
		padding:0px;
		margins:0px;
	}
	.totals {min-width:25px;font-size:8pt;max-with:35px;text-align:left;}
	td.totals {border:1pt solid #aed0ea;}
	.SubmittedValue {border:1pt solid #aed0ea;background-color:#C2C2D6;}
	.SubmittedValueData {border:1pt solid #aed0ea;background-color:#FFCC80;}
	.ApprovedValueData {border:1pt solid #aed0ea;background-color:#99E699;}
	.deleteRow {position:relative;margin-top:5px;width:10px;height:10px;float:right;
		background-image:url("<?php echo $CurrentServer;?>images/redx-small.png");
		background-size: 10px 10px;
		background-repeat: no-repeat;
		background-position: left top;
		cursor:pointer;
	}
	.helpRow {position:relative;width:5px;height:5px;float:right;border:1pt solid red;cursor:pointer;}
	.headerRow {text-align:center;font-size:10pt;}
	.codeNameDisplay {width:90%;float:left;}
	.ui-datepicker-calendar {display: none;}
	.ui-datepicker-trigger {cursor:pointer;}
	</style>
	<?php
					//now get data for this week
					$WeekStart = strtotime($WeekPicker[$WeekPickerNumber]["start"]);
					$WeekStartOrg = $WeekStart;
					$WeekEnd = strtotime($WeekPicker[$WeekPickerNumber]["end"]);
					
					/* Ignoring PayWeek Structure and only doing monthly structure
					//Get PayWeek which are Monday-Sunday (regardless of month limitations)
						//Get Week number and Year for Start of month
						$StartMonthWeekNumber = date("W",$WeekStart);
						$StartMonthYear = date("Y",$WeekStart);
						
						//First Monday of the Week
						$PayWeekStartDay = date("m/d/Y", strtotime($StartMonthYear."W".$StartMonthWeekNumber));
						$PayDayIncrement = 0;
						//create array of all days in the week Mon-Sun
						while ($PayDayIncrement < 7){
							$PayWeekDay = date("Y-m-d", strtotime($PayWeekStartDay." + ".$PayDayIncrement."Days"));
							$PayWeek[] = $PayWeekDay;
							$PayWeekEndDay = $PayWeekDay;
						$PayDayIncrement++;
						}
						$SaveHoursStartDate = $PayWeekStartDay;
						$SaveHoursEndDate = $PayWeekEndDay;
						//End create PayWeek data
					*/
					$SaveHoursStartDate = $WeekPicker[$WeekPickerNumber]["start"];
					$SaveHoursEndDate = $WeekPicker[$WeekPickerNumber]["end"];
					//get currently saved hours
					$criteria = new stdClass();
					$criteria->startDate = date("Y-m-d",strtotime($SaveHoursStartDate));
					$criteria->endDate = date("Y-m-d",strtotime($SaveHoursEndDate));
					$criteria->statusOr = "SubmittedORApproved";
					$criteria->department = $ThisDepartmentShort;
					$paginationResult = $gbsProvider->getHours($criteria);
					$resultArray = $paginationResult->collection;
					foreach ($resultArray as $Hours){
						$HoursData[$Hours->hoursDate][$Hours->employeeId][]=$Hours;
						$HoursDataTotals[$Hours->hoursDate][$Hours->employeeId][$Hours->status]=bcadd($HoursDataTotals[$Hours->hoursDate][$Hours->employeeId][$Hours->status],$Hours->hours,2);
						if ($CodesById[$Hours->codeId]->nonbillable){
							$HoursDataTotalsNonBillable[$Hours->hoursDate][$Hours->employeeId][$Hours->status]=bcadd($HoursDataTotalsNonBillable[$Hours->hoursDate][$Hours->employeeId][$Hours->status],$Hours->hours,2);
						}
						$CodeName = $CodesById[$Hours->codeId]->name;
						$CodeName = $CodeName." ".trim(str_replace($CodeName,"",trim(str_replace("-","",$CodesById[$Hours->codeId]->description))));
						$EmployeeHoursData[$Hours->employeeId][$CodeName][$Hours->hoursDate]=bcadd($EmployeeHoursData[$Hours->employeeId][$CodeName][$Hours->hoursDate],$Hours->hours,2);
						$EmployeeHoursDataIds[$Hours->employeeId][] = $Hours->id;
						$CodeData[$Hours->codeId][]=$Hours;
						if (in_array($Hours->codeId,$SickVacationCode)){
							$PayWeekSicVacationData[$Hours->hoursDate][$Hours->employeeId] = bcadd($PayWeekSicVacationData[$Hours->hoursDate][$Hours->employeeId],$Hours->hours,2);
						}else{
							$PayWeekRegularData[$Hours->hoursDate][$Hours->employeeId] = bcadd($PayWeekRegularData[$Hours->hoursDate][$Hours->employeeId],$Hours->hours,2);
						}
					}
					$CodeRows = (count($CodeData) ? count($CodeData) : 0);
					foreach ($CodeData as $CodeID=>$CodeInfo){
						$CodesWithData[] = $CodeID;
					}
					//now resort CodesWithData to match alphabetical ordering
					foreach ($CodeNames as $CodeName=>$CodeId){
						if (count($CodeData[$CodeId])){
							$SortedCodes[$CodeId] = $CodeData[$CodeId];
						}
					}
					$CodeData = $SortedCodes;
			$DisplayMonthYear = str_replace(" ","",$MonthYear);
		?>
		<div class="row">
			<div class="three columns">
				<h3>
					<?php echo $MonthYear;?>
					<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$MonthYear)));?>">
				</h3>
			</div>
			<div class="six columns">
				<br>
				<select id="weekPicker" class="parameters" style="width:250px;">
				<?php
					foreach ($WeekPicker as $WeekNumber=>$WeekData){
						echo "<option value=\"".$WeekNumber."\"".($WeekPickerNumber == $WeekNumber ? " selected" : "").">Week ".$WeekNumber.": ".$WeekData["display"]."</option>";
						if ($WeekPickerNumber == $WeekNumber){$SelectedWeekDataText =$WeekData["display"];}
					}
				?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="savebuttons" class="ten columns" style="text-align:right;">
				<div class="eight columns" id="hoursResults">&nbsp;</div>
				<a href="approve-code-update" id="approvebutton" class="button-link do-not-navigate info">Approve</a>
			</div>
			<div class="nothingToApprove" class="ten columns" style="text-align:center;display:none;">
				Nothing to Approve for this week
			</div>
			<style>
				.staffName {max-width:120px;min-width:120px;}
				.hiddenColumn {display:none;}
				.dayInput {min-width:25px;max-width:35px;}
			</style>
			<table class="reportResults display" cellspacing="0" width="100%" cellpadding="0" id="table<?php echo $DisplayMonthYear;?>">
				<thead>
					<tr>
						<th style="max-width:61px;text-align:center;" class="checkColumn page-break"><button id="checkAll" style="cursor:pointer;font-size:8pt;">check all</button></th>
						<th class="page-break" style="min-width:20px;"></th>
						<th class="hiddenColumn"></th>
						<th class="staffName">Staff Name</th>
						<?php
							while ($WeekStart <=  $WeekEnd){
								echo "<th class=\"dayInput\">".date("D m/d",$WeekStart)."</th>";
								$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
							}
						
						?>
						<?php //if (count($CodeData)){?>
							<th class="totals dayInput" align="left">Totals</th>
						<?php //}?>
					</tr>
				</thead>
				<tbody>
					<form id="gbsHoursForm" name="gbsHoursForm">
						<?php
							foreach ($EmployeesByManager[$CurrentSupervisorName] as $recordId=>$EmployeeInfo){
								$EmployeeID = $EmployeeInfo->id;
								$WeekStart = $WeekStartOrg;
								$thisStaffTotal = 0;
								$thisStaffTotalNonBillable = 0;
								$ThisRowData = "";
								$SubmittedCount = 0;
								$ApprovedCount = 0;
								while ($WeekStart <=  $WeekEnd){
									$hoursAmount = 0;
									$hourStatus = "";
									$hoursDate = date("Y-m-d",$WeekStart);
									if ($HoursDataTotals[$hoursDate][$EmployeeID]["Submitted"]){
										$SubmittedCount++;
										$hourStatus = "Submitted";

									}
									if ($HoursDataTotals[$hoursDate][$EmployeeID]["Approved"]){
										$ApprovedCount++;
										$hourStatus = "Approved";
									}
									$hoursAmountSubmitted = $HoursDataTotals[$hoursDate][$EmployeeID]["Submitted"];
									$hoursAmountApproved = $HoursDataTotals[$hoursDate][$EmployeeID]["Approved"];
									$hoursAmountCombined = bcadd($hoursAmountSubmitted,$hoursAmountApproved,2);
									$hoursAmountSubmittedNonBillable = $HoursDataTotalsNonBillable[$hoursDate][$EmployeeID]["Submitted"];
									$hoursAmountApprovedNonBillable = $HoursDataTotalsNonBillable[$hoursDate][$EmployeeID]["Approved"];
									$hoursAmountCombinedNonBillable = bcadd($hoursAmountSubmittedNonBillable,$hoursAmountApprovedNonBillable,2);
									$thisStaffTotal = bcadd($thisStaffTotal,$hoursAmountCombined,2);
									$thisStaffTotalNonBillable = bcadd($thisStaffTotalNonBillable,$hoursAmountCombinedNonBillable,2);
									if ($hoursAmountSubmitted){$hourStatus = "Submitted";$hoursAmount = $hoursAmountSubmitted;}
									if ($hoursAmountApproved){$hourStatus = "Approved";$hoursAmount = $hoursAmountApproved;}
									$hoursAmount = ($hoursAmount ? $hoursAmount : "&nbsp;");
									$ThisRowData .="<td class=\"dayInput ".$hourStatus."ValueData\">".$hoursAmount."</td>";
									$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
								}
	//								<td align=\"center\"><input class=\"toApproveCheck".($hourStatus == "Submitted" ? " hasData" : "")."\" data-hasdata=\"".($hourStatus == "Submitted" ? "true" : "false")."\" type=\"checkbox\" value=\"".($hourStatus == "Submitted" ? implode(",",$EmployeeHoursDataIds[$EmployeeID]) : "")."\"".($hourStatus == "Submitted" ? "" : " disabled")."></td>
								echo "<tr>
									<td align=\"center\" class=\"checkColumn page-break\"><input class=\"toApproveCheck".($SubmittedCount ? " hasData" : "")."\" data-employeeid=\"".$EmployeeID."\" data-hasdata=\"".($SubmittedCount ? "true" : "false")."\" type=\"checkbox\" value=\"".($SubmittedCount ? implode(",",$EmployeeHoursDataIds[$EmployeeID]) : "")."\"".($SubmittedCount ? "" : " disabled")."></td>
									<td class=\"page-break".((int)$thisStaffTotal > 0 ? " details-control" : "")."\"></td>
									<td class=\"hiddenColumn\">".$EmployeeID."</td>
									<td  class=\"staffName\"><a href=\"?nav=manage-hoursentry&EmployeeID=".$EmployeeID."&WeekPickerNumber=".$WeekPickerNumber."&StartDate=".$StartDate."&EndDate=".$EndDate."&ShowAllCodes=true\">".$EmployeeInfo->fullName."</a></td>";
								echo $ThisRowData;
								echo "<td>".($thisStaffTotal ? $thisStaffTotal : "&nbsp;")."</td>";
								echo "</tr>";
								$CheckWritersTotals[$EmployeeInfo->lastName.", ".$EmployeeInfo->firstName] = $thisStaffTotal;
							}
							//create javascript weeks for formating child rows
							$WeekStart = $WeekStartOrg;
							while ($WeekStart <=  $WeekEnd){
								$hoursDate = date("Y-m-d",$WeekStart);
								$javascriptDates[] = $hoursDate;
								$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
							}
							ksort($CheckWritersTotals);

						?>
						<?php //echo $BlankRows;?>
					</form>
				</tbody>
			
			</table>
			<div class="savebuttons" class="ten columns" style="text-align:left;">
				<div class="eight columns" id="hoursResultsReject">&nbsp;</div>
				<div class="twelve columns">Add Message for Reason for Rejection:
					<textarea class="ten columns" id="rejectionMessage"></textarea><br>
					<a href="reject-code-update" id="rejectbutton" class="button-link do-not-navigate alert">Reject</a>
				</div>
			</div>
			<br clear="all">
			<?php 
				if (count($SavedDayTotals)){
					echo "Displayed Total: ".array_sum($SavedDayTotals)." hours";
					/*
					if (array_sum($SavedDayTotals) != bcadd($SickVacationWeekTotal,$RegularWeekTotal,2)){
						echo " (different than CheckWriters Total Hours because of month range)";
					}
					*/
				}
			?>
		</div>
		<!--
		<?php if (count($PayWeekRegularData) || count($PayWeekSicVacationDataData)){?>
			<div class="row" style="padding-left:10px;display:block;">
				<div class="twelve columns">
					<style>
						.CWTop {
							background-image:url('<?php echo $CurrentServer;?>images/CWHeaderTotals.png');
							background-size:180px;
							background-repeat:no-repeat;
							width:180px;
							height:38px;
							text-align:center;
							font-size:11px;
							font-weight:bold;
						}
						.CWTable{
							font-size: 10px;
							font-family: Arial;
						}
						.CWHeader {
							background-color:LightGray;
							text-align:center;
						}	
						.CWFirstColumn {
							text-align:left;
						}
						.CWData{
							background-color:LightGray;
							border-top: solid 1px White;
							border-right: solid 1px Gray;
							border-bottom: solid 1px Gray;
							border-left: solid 1px White;
							padding:2px;
							padding-top: 5px;
							padding-bottom: 5px;
						}
						.CWValue {
							text-align:right;
						}
					</style>
					CheckWriters Timesheet Mgr Approval View:
					<div class="CWTop"></div>
					<table border="1" cellspacing="0" class="CWTable">
						<tr class="CWHeader">
							<td class="CWFirstColumn CWData">Employee Name</td>
							<td class="CWData">Total Hours</td>
						</tr>
						<?php
							foreach($CheckWritersTotals as $EmployeeName=>$Total){
								echo '<tr><td class="CWFirstColumn CWData">'.$EmployeeName.'</td><td class="CWData CWValue">'.$Total.'</td></tr>';
							}
						?>
					</table>
				</div>
			</div>
		<?php }//end if RegularPay?>
		-->
			<br clear="all">
			<br clear="all">
			<div class="row">
				<div class="twelve columns">
					<fieldset>
						<legend>Instructions</legend>
						<ol style="list-style-type:decimal;">
							<li><span style="font-weight:bold;">Review Hours</span><br>
								Staff with hours submitted or approved for this time frame will have <span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> 
								that can be clicked to show the details for that day.  The details will include the breakdown by code.<br>
								The default week display is the current week of the current month.  Use the Week selector to choose the week in the month.<Br>
								If you need to go back to a different month, you can click the calendar icon <img src="<?php echo $CurrentServer;?>css/images/icon-calendar.png" width="15" height="15"> to change month.<Br>
							<!--
							<li><span style="font-weight:bold;">Check Mark Staff to Approve </span><br> 
								Staff with hours to be approved will have a checkbox.<br>
								You can use the <button style="font-size:8pt;">check all</button> to check all the staff at once.<br>
							<li><span style="font-weight:bold;">Click 'Approve' Button</span><br> 
								Page will refresh showing all the staff with Approved hours. 
								Hours submitted but not yet approved will be highlighted in <span style="background-color:#FFCC80">orange</span>.<br>
								Hours already approved will be highlighted in <span style="background-color:#99E699">green</span>.
							-->
						</ol>
					</fieldset>
				</div>
			</div>
	<script>
		$(document).ready(function() {
			var	jsondataEmployeeEmailById = <?php echo json_encode($EmployeeEmailByID);?>;
			var	jsondataStaffData = <?php echo json_encode($EmployeeHoursData);?>;
			var jsonWeekData = <?php echo json_encode($javascriptDates);?>;
			var weekday=new Array(7);
				weekday[0]="Mon";
				weekday[1]="Tue";
				weekday[2]="Wed";
				weekday[3]="Thu";
				weekday[4]="Fri";
				weekday[5]="Sat";
				weekday[6]="Sun";
			function format ( d ) {
				// `d` is the original data object for the row
				var thisItem = d,
					jsondata = jsondataStaffData,
					detailDisplayRow = "";
					var TableHeader = '<tr style="font-size:8pt;"><td style=\"width:180px;\">Code</td>';
					$.each(jsonWeekData,function(key,hourDate){
						var thisDate = new Date(hourDate);
						TableHeader = TableHeader+'<td style="width:50px;font-size:8pt;">'+weekday[thisDate.getDay()]+'</td>';
					});
					TableHeader = TableHeader+'<td style="width:50px;font-size:8pt;">Totals</td></tr>';
					$.each(jsondata[d],function(code,hourData){
						detailDisplayRow = detailDisplayRow+'<tr style="font-size:8pt;"><td style="width:180px;">'+code+'</td>';
						var totalHours = 0;
						$.each(jsonWeekData,function(key,hourDate){
							if (hourData[hourDate]){
								detailDisplayRow = detailDisplayRow + '<td style="width:52px;font-size:8pt;">'+hourData[hourDate]+'</td>';
								totalHours = totalHours+parseFloat(hourData[hourDate]);
							}else{
								detailDisplayRow = detailDisplayRow + '<td style="width:52px;font-size:8pt;">&nbsp;</td>';
							}
						});
						detailDisplayRow = detailDisplayRow + '<td style="width:50px;font-size:8pt;">'+totalHours+'</td></tr>';
					});
			
				return '<table class="detailCountDisplay" border="1" cellpadding="0" cellspacing="0" width="100%" style="border-color:#aed0ea;">'+
						'<colgroup><col><col><col><col><col><col></colgroup>'+TableHeader+detailDisplayRow+
						'</table>';
			}
			
			
			
			$("#CWTotalHours").html('<?php echo bcadd($SickVacationWeekTotal,$RegularWeekTotal,2);?>');
			$("button").on("click",function(){
				var checkBoxes = $(".toApproveCheck");
				$.each(checkBoxes,function(){
					var $this = $(this);
					if ($this.attr('data-hasdata') == "true"){
						$this.prop("checked", "checked");
					}
				});
			});
			

			var dateClass = $(".date");
			dateClass.width(80);
			$("#StartDate").datepicker({
				onSelect: function (date) {
					var date2 = $('#StartDate').datepicker('getDate');
					date2.setDate(date2.getDate() + 30);
					$('#EndDate').datepicker('setDate', date2);
					//sets minDate to dt1 date + 1
					$('#EndDate').datepicker('option', 'minDate', date2);
					updateFilters();
				}
			});
			$('#EndDate').datepicker({
				onClose: function () {
					var dt1 = $('#StartDate').datepicker('getDate');
					var dt2 = $('#EndDate').datepicker('getDate');
					//check to prevent a user from entering a date below date of dt1
					if (dt2 <= dt1) {
						var minDate = $('#EndDate').datepicker('option', 'minDate');
						$('#EndDate').datepicker('setDate', minDate);
					}
					updateFilters();
				}
			});		
			var getFilters = function(){
				var startDate = $("#StartDate").val(),
					endDate = $("#EndDate").val(),
					weekPicker = $("#weekPicker").val(),
					filterValues = new Array(startDate,endDate,weekPicker);
					
					return filterValues;
			};
		
			var updateFilters = function(){
				var Filters=getFilters();
				var refreshLocation = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>ghs/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&WeekPickerNumber="+Filters[2]+"&nav=<?php echo $nav;?>#";
				window.location.href = refreshLocation;
				return(refreshLocation);
			}
			$("#showAllCodes").on('click',function(){
				$("#ShowAllCodes").val('<?php echo ($_GET['ShowAllCodes'] ? "" : "true");?>');
				updateFilters();
			});
			$(".parameters").on('change',function(){
				updateFilters();
			});
			$("#resetbutton").on('click',function(){
				$("#StartDate").val('');
				$("#EndDate").val('');
				updateFilters();
				
			});
			
			$('.date-picker').datepicker({
				showOn: "button",
				buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
				buttonImageOnly: true,
				buttonText: 'Click to change month',
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy',
				minDate: '01/01/14',
				maxDate: '<?php echo date("m/t/Y");?>',
				onClose:
					function(dateText, inst){
						var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						$(this).datepicker('setDate', new Date(year, month, 1));
						$("#StartDate").datepicker('setDate', new Date(year, month, 1));
						var date2 = $(this).datepicker('getDate');
						date2.setMonth(date2.getMonth() +1);
						date2.setDate(date2.getDate() -1);
						$("#EndDate").datepicker('setDate', date2);
						$("#weekPicker").val('');
						updateFilters();
					}
			
			});
			
			
			var table = $('.reportResults').DataTable({
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
								{
									"sExtends":    "copy"
								},
								{
									"sExtends":    "print",
									"fnComplete": function ( nButton, oConfig, oFlash, sFlash ) {
										$(".page-break").hide();
									}
								}
					]				
				},				
				"scrollX": true,
				"bJQueryUI": true,
				"bSearchable":false,
				"bFilter":false,
				"bAutoWidth": false,
				"bSort": false,
				"iDisplayLength": 100,
				"paging":   false,
				"ordering": false,
				"info":     false
			});
			
			// Add event listener for opening and closing details
			$('.reportResults tbody').on('click', 'td.details-control', function () {
				var tr = $(this).parents('tr');
				var row = table.row( tr );

				if ( row.child.isShown() ) {
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}
				else {
					// Open this row
					var dataObj = row.data(),
						employeeId = dataObj[2];
						row.child( format(employeeId) ).show();
						tr.addClass('shown');
				}
			} );
		
			var  SubmittedValueDataSpan = $(".SubmittedValueDataSpan");
			var  ApprovedValueDataSpan = $(".ApprovedValueDataSpan");
			$("#adjustStatus").on('change',function(){
				var DayValues = {};
				var HourIDs = new Array();
				var DayValuesFound = false;
				$('#hoursResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
				var ValueDataSpan = (SubmittedValueDataSpan.length ? SubmittedValueDataSpan : ApprovedValueDataSpan);
				$.each(ValueDataSpan, function(key,obj){
					var thisObj = $(obj);
					if (thisObj.attr('data-hourid')){
						DayValuesFound = true;
						HourIDs.push(thisObj.attr('data-hourid'));
					}
				});			
				if (DayValuesFound){
					var $this = $(this);
					var EmployeeID = $("#SelectedEmployee").val();
					DayValues["action"] = $this.val();
					DayValues["hourIds"] = HourIDs;
					DayValues["status"] = $this.attr('data-status');
					data = JSON.stringify(DayValues);
					$.ajax({
						url: "ApiCodesManagement.php",
						type: "PUT",
						data: JSON.stringify(DayValues),
						success: function(data){
							location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#hoursResults').show().html(message).addClass("alert");
						}
					});
					
				}else{
					$('#hoursResults').show().html("Please indicate the Billing Code and hours").addClass("alert");
				}
			});
			var hasData = $(".hasData");
			$("#approvebutton").on('click',function(){
				var DayValues = {};
				var HourIDs = new Array();
				var DayValuesFound = false;
				$('#hoursResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
				$.each(hasData,function(){
					var $this = $(this);
					if($this.prop("checked")){
						var theseValues = $this.val(),
							theseVal = theseValues.split(",");
						$.each(theseVal,function(key,val){
							HourIDs.push(val);
						})
						DayValuesFound = true;
					}
				});
				if (DayValuesFound){
					DayValues["action"] = "bulk_approve";
					DayValues["hourIds"] = HourIDs;
					DayValues["status"] = "Approved";
					data = JSON.stringify(DayValues);
					//console.log(data);
					$.ajax({
						url: "ApiCodesManagement.php",
						type: "PUT",
						data: JSON.stringify(DayValues),
						success: function(data){
							location.reload();
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#hoursResults').show().html(message).addClass("alert");
						}
					});
				}else{
					$('#hoursResults').show().html("You must check at least one staff member with data").removeClass("alert").removeClass("info").addClass("alert");
				}
			});
			
			$("#rejectbutton").on('click',function(){
				$('#hoursResultsReject').html('&nbsp;');
				var DayValues = {};
				var HourIDs = new Array();
				var EmployeeEmails = new Array();
				var DayValuesFound = false;
				var ReadyToSubmit = false;
				var rejectMessage = $("#rejectionMessage").val();
				$.each(hasData,function(){
					var $this = $(this);
					if($this.prop("checked")){
						var theseValues = $this.val(),
							theseVal = theseValues.split(",");
						$.each(theseVal,function(key,val){
							HourIDs.push(val);
						})
						var theseEmployeeEmails = $this.attr('data-employeeid');
						EmployeeEmails.push(jsondataEmployeeEmailById[theseEmployeeEmails]);
						DayValuesFound = true;
						ReadyToSubmit = true;
					}
				});
				if (!DayValuesFound){
					$('#hoursResultsReject').show().append("You must check at least one staff member with hours to reject.\n").removeClass("alert").removeClass("info").addClass("alert");
				}
				if (!rejectMessage){
					$('#hoursResultsReject').show().append("You must include a message for the rejection reason.\n").removeClass("alert").removeClass("info").addClass("alert");
					ReadyToSubmit = false;
				}

				if (ReadyToSubmit){
					$('#hoursResultsReject').show().html("Processing...\n").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
					DayValues["action"] = "bulk_reject";
					DayValues["hourIds"] = HourIDs;
					DayValues["employeeEmails"] = EmployeeEmails;
					DayValues["status"] = "Saved";
					DayValues["rejectionMessage"] = rejectMessage;
					DayValues["timeperiod"]= "<?php echo $SelectedWeekDataText;?>";
					data = JSON.stringify(DayValues);
					//console.log(data);
					$.ajax({
						url: "ApiCodesManagement.php",
						type: "PUT",
						data: JSON.stringify(DayValues),
						success: function(data){
							if (data == "1"){
								location.reload();
							}else{
								$('#hoursResultsReject').show().html(data).removeClass("alert").removeClass("info").addClass("alert");
								console.log(data);
							}
	
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#hoursResultsReject').show().html(message).addClass("alert");
						}
					});
				}
			});
			
			//hide 'Approve Button' if nothing to approve
			if (hasData.length < 1){
				$(".checkColumn").hide();
				$(".savebuttons").hide();
				//$(".nothingToApprove").show();
				$(".staffName").css("width","177px");
			}

			
		});
	</script>
<?php } else{
	echo '<div class="row">
			<div class="twelve columns" style="text-align:center;">
				There are no employees you are authorized to Approve submitted hours
			</div>
		</div>';
}//end if no count(EmployeeByManager) ?>