<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");

$ThisDepartment = 'Green Home Services';
$ThisDepartmentShort = "GHS";

$gbsProvider = new HoursProvider($dataConn);
$lastDayOfThisMonth = date("m/t/Y");

$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : "02/01/2015");
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $lastDayOfThisMonth);

$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$CodesActive[$record->displayOrderId]=$record;
		$CodesByName[$record->name]=$record;
	}else{
		$CodesInActive[$record->name]=$record;
	}

}

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
ksort($EmployeeByID);

$criteria = new stdClass();
$criteria->startDate = date("Y-m-d",strtotime($StartDate));
$criteria->endDate = date("Y-m-d",strtotime($EndDate));
$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getHours($criteria);
$resultArray = $paginationResult->collection;


foreach ($resultArray as $Hours=>$HoursInfo){
	foreach ($HoursInfo as $key=>$val){
		$val = 0;
		if ($key == "codeId"){$val = 150;}
		if ($key == "employeeId"){$val = 150;}
		if ($key == "addedByAdmin"){$val = 100;}
		$Columns[$key] = $val;
	}
	$HoursData[$Hours->hoursDate][]=$Hours;
	$CodeData[$Hours->codeId][]=$Hours;
}
?>
<style>
.minWidth150 {
	min-width:150px;
}
.minWidth100 {
	min-width:100px;
}
</style>
	<h3>Raw Data</h3>
	<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
	<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date"><br>
	<table id="tableInfo" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<?php
					foreach ($Columns as $hdr=>$val){
						$columnDisplay = str_replace("Admin","",$hdr);
						$columnDisplay = str_replace("Id","",$columnDisplay);
						echo "<th class=\"minWidth".$val."\">".$columnDisplay."</th>";
					}
				?>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<?php
					foreach ($Columns as $hdr=>$val){
						$columnDisplay = str_replace("Admin","",$hdr);
						$columnDisplay = str_replace("Id","",$columnDisplay);
						echo "<th class=\"minWidth".$val."\">".$columnDisplay."</th>";
					}
				?>
			</tr>
		</tfoot>
		<tbody>
			<?php
				foreach ($resultArray as $column=>$data){
					echo "<tr>";
					foreach ($data as $rowid=>$rowinfo){
						$displayData = $rowinfo;
						if ($rowid == "codeId"){
							$displayData = $CodeByID[$rowinfo]->description;
						}
						if ($rowid == "employeeId"){
							$displayData = $EmployeeByID[$rowinfo]->fullName;
						}
						if ($rowid == "addedByAdmin" || $rowid == "submittedByAdmin" || $rowid == "approvedByAdmin"){
							$displayData = explode("@",$rowinfo);
							$displayData = $displayData[0];
						}
						if ($rowid == "timeStamp" || $rowid == "submittedDate" || $rowid == "approvedDate"){
							if ($rowinfo){
								$displayData = date("m/d/Y g:h a",strtotime($rowinfo));
							}
						}
						if ($rowid == "hoursDate"){
							if ($rowinfo){
								$displayData = date("m/d/Y",strtotime($rowinfo));
							}
						}
						echo "<td class=\"minWidth".$Columns[$rowid]."\">".$displayData."</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
	
	<script type="text/javascript">
	$(function () {
		
		var table = $('#tableInfo').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 50,
		});

		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setMonth(date2.getMonth() +1);
				date2.setDate(date2.getDate() -1);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		
		$(".FilterParam").on("click",function(){
			updateFilters();
		});
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters(),
				Params = "StartDate="+Filters[0]+"&EndDate="+Filters[1];
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>gbs/?"+Params+"&nav=report-rawdata#";
		}
	});
	</script>