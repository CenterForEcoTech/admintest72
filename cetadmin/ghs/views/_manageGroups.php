<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");

$ThisDepartment = 'Green Home Services';
$ThisDepartmentShort = "GHS";

$gbsProvider = new HoursProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
}
ksort($GroupsByName);
//resort the products without display order by GroupID
$SelectedGroupsID = $_GET['GroupID'];
$SelectedGroupName = $_GET['GroupName'];
if ($SelectedGroupName && !$SelectedGroupsID){
	$SelectedGroupsID = $GroupsByName[$SelectedGroupName]->id;
}
$addGroup = $_GET['addGroup'];
//get Codes in this Group
if ($SelectedGroupsID){
	$criteria = new stdClass();
	$criteria->groupId = $SelectedGroupsID;
	$criteria->department = $ThisDepartmentShort;
	$paginationResult = $gbsProvider->getCodes($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		$CodesWithGroup[$record->name] = $record;
	}

	ksort($CodesWithGroup);
}//end if SelectedGroupName



?>
<?php if (!$addGroup){?>
	<br>
	<div class="fifteen columns">
		<?php include('_GroupsComboBox.php');?>
	</div>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addGroup?>
<?php if ($SelectedGroupsID || $addGroup){?>
	<style>
		.NonBillable1 {font-size:10pt;}
	</style>
	<form id="GroupUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Group Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<?php if (!$addGroup){?><input type="text" name="id" value="<?php echo $GroupByID[$SelectedGroupsID]->id;?>"><?php }?>
				</div>
				<div class="fourteen columns">
					<div class="two columns">Name:</div><div class="ten columns">
						<input type="text" id="name" name="name" style="width:100%;" value="<?php echo $GroupByID[$SelectedGroupsID]->name;?>">
				</div>
			</div>
		</fieldset>
		<div id="groupResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-groups&GroupID=<?php echo $SelectedGroupsID;?>" id="cancel" class="button-link">Cancel</a>
				<input type="hidden" name="department" value="<?php echo $ThisDepartmentShort;?>">
				<?php if ($addGroup){?>
					<a href="save-group-add" id="save-group-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="group_add">
				<?php }else{?>
					<a href="save-group-update" id="save-group-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="group_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
	<?php if ($SelectedGroupsID){?>
			<fieldset>
				<legend>Codes in this Group</legend>
				<div class="fifteen columns">
						<?php 
							ksort($CodesWithGroup);
							foreach ($CodesWithGroup as $name=>$info){
								echo "<span class=\"NonBillable".$info->nonbillable."\">".$name." ".trim(str_replace("-","",str_replace($name,"",$info->description))).($info->nonbillable && !strpos(strtolower($info->description),"unbillable") ? " (non-billable)" : "" )."</span><br>";
							}
						?>
				</div>
			</fieldset>
	<?php }//end if SelectedGroupsID ?>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-groups&addGroup=true" class="button-link">+ Add New Group</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
	

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
	 var formElement = $("#GroupUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-group-add").click(function(e){

			$('#groupResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#groupResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						var displayOrderValue = parseInt($('#displayOrderId').val());
						$("#name").val('');	
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#groupResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedGroupsID){?>
		$("#save-group-update").click(function(e){

			$('#groupResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#groupResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#groupResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
	<?php }//end if SelectedGroupsID ?>
});
</script>