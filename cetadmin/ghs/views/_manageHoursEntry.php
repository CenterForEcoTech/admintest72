<?php
ini_set("display_errors", "on"); error_reporting(1);

$EmployeeID = $_GET["EmployeeID"];

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$ThisDepartment = 'Green Home Services';
$ThisDepartmentShort = "GHS";

//IF STAFF IS ALLOWED TO APPROVE THEIR OWN HOURS THEN SET TO TRUE
$StaffCanApproveOwnData = true;

$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->department = $ThisDepartment;
$criteria->hoursTrackingGHS = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}

$StartOfThisMonth = date("m/01/Y");
$EndOfThisMonth = date("m/t/Y");
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $StartOfThisMonth);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $EndOfThisMonth);
//figure out if last day of month is not a sunday how to include the next days of the following month
$lastDayOfWeekOfEndDate = date("w",strtotime($EndDate));
$DaysUntilSunday = (7-$lastDayOfWeekOfEndDate);
$NewEndDate = date("m/d/Y",strtotime($EndDate." + ".$DaysUntilSunday." days"));
//get the number of days per month
$nextMonthName = date("M",strtotime($StartDate));
$lastMonthName = date("M",strtotime($EndDate));
	$ThisMonth = date("n",strtotime($StartDate));
	$ThisYear = date("Y",strtotime($StartDate));
	$MonthYear = $nextMonthName." ".$ThisYear;
	//get all the days at once:
	$endDay = date("t",strtotime($StartDate));
	$thisDay = 1;
	$FirstSunday = date("Y-n-j",strtotime("First Sunday of ".$ThisYear."-".$ThisMonth."-1"));
	$FirstSundayDay = date("j",strtotime($FirstSunday));
	$LastSaturday = date("Y-n-j",strtotime("Last Saturday of ".$ThisYear."-".$ThisMonth."-1"));
	$LastSaturdayDay = date("j",strtotime($LastSaturday));
	$FirstMonday = date("Y-n-j",strtotime("First Monday of ".$ThisYear."-".$ThisMonth."-1"));
	$FirstMondayDay = date("j",strtotime($FirstMonday));
	$LastSunday = date("Y-n-j",strtotime("Last Sunday of ".$ThisYear."-".$ThisMonth."-1"));
	$LastSundayDay = date("j",strtotime($LastSunday));
	$FirstDayOfMonth = $ThisYear."-".$ThisMonth."-1";
	//USING PayPeriods month start and end dates
	$usingPayPeriods = true;
	$weekCounter = array();
	if ($usingPayPeriods){
		$FirstMonday = ($FirstMondayDay > 2 ? date("Y-n-j",strtotime($FirstMonday." -2 week")) : $FirstMonday); //subtrack 2 to include any straggler prior pay periods
		$LastSunday = ($LastSundayDay < $endDay ? date("Y-n-j",strtotime($LastSunday." +1 week")) : $LastSunday); 
		$thisDay = $FirstMonday;
		$endDay = $LastSunday;
		while (strtotime($thisDay) <= strtotime($endDay)){
			$HorizontalDisplay[$MonthYear]["Days"][] = date("d",strtotime($thisDay));
			$ThisWeek = date("W", strtotime($thisDay)); //+1 day to get week number for weeks that start or Sunday instead of Monday
			$Weeks[$ThisWeek][] = date("Y-m-d",strtotime($thisDay));
			$thisDay = date("Y-n-j",strtotime($thisDay." +1 day"));
			if (!in_array($ThisWeek,$weekCounter)){
				$weekCounter[] = $ThisWeek;
			}
		}
	}else{
		/* USING Calendar Month start and end dates */
		while ($thisDay <= $endDay){
			$HorizontalDisplay[$MonthYear]["Days"][] = $thisDay;
			$ThisWeek = date("W", strtotime($ThisYear."-".$ThisMonth."-".$thisDay));
			$Weeks[$ThisWeek][] = $ThisYear."-".$ThisMonth."-".$thisDay;
			$thisDay++;
			if (!in_array($ThisWeek,$weekCounter)){
				$weekCounter[] = $ThisWeek;
			}
		}
	}
$weekCount = 1;
$FirstMonday = date("Y-m-d",strtotime($FirstMonday));
foreach ($Weeks as $Number=>$Dates){
	if (in_array($FirstMonday,$Dates)){
		$FirstWeekToStart = $Number;
	}
	$LastWeek = $Number;
	$weekCount++;

}
$ShowThisWeek =(int)$FirstWeekToStart;
$weekCount = 1;
$TodaysWeekPicker = 1;
//proceed as normal for rest of the year
foreach ($weekCounter as $weekIncluded){
	$FirstDateOfWeek = current($Weeks[$weekIncluded]);
	$LastDateOfWeek = end($Weeks[$weekIncluded]);
	if (date("Y",strtotime($FirstDateOfWeek)) != "1969"){ //ignore dates that do not calculate because of compensating for last days of Dec that might be part of first week in January
		$WeekPicker[$weekCount]["display"] = date("m/d/Y",strtotime($FirstDateOfWeek))." to ".date("m/d/Y",strtotime($LastDateOfWeek));
		$WeekPicker[$weekCount]["start"] = $FirstDateOfWeek;
		$WeekPicker[$weekCount]["end"] = $LastDateOfWeek;
	}
	//check to see which week TodaysDate is in
	if (strtotime($TodaysDate) >= strtotime($FirstDateOfWeek) && strtotime($TodaysDate) <= strtotime($LastDateOfWeek)){
		$TodaysWeekPicker = $weekCount;
	}

	$ShowThisWeek++;
	$weekCount++;
}
$employeeComboBoxHideLabel = true;
$employeeComboBoxFunction = '$("#SelectedEmployee").val($(ui.item).val()).change();';
$WeekPickerNumberGet = $_GET["WeekPickerNumber"];
$WeekPickerNumber = ($WeekPickerNumberGet != "null" && $WeekPickerNumberGet !="undefined" ? $WeekPickerNumberGet : $TodaysWeekPicker);
?>
<div class="ten columns" style="margin-left:-23px;">
	<div class="<?php echo ($staffFolder ? "eight" : "four");?> columns" style="text-align:right;"><h3>GHS Hours Entry for <?php if ($staffFolder){echo $_SESSION["AdminAuthObject"]["adminFullName"];}?></h3></div>
	<div class="four columns"<?php echo ($staffFolder ? " style=\"display:none;\"" : "");?>><br><?php include('../hr/_EmployeeComboBox.php');?></div>
</div>
<input type="hidden" id="SelectedEmployee" value="<?php echo $EmployeeID;?>" class="parameters">
<?php
	//get codes matrix for this employee
	$gbsProvider = new HoursProvider($dataConn);
	$criteria = new stdClass();
	$criteria->showAll = true;
	$criteria->noLimit = true;
	$criteria->department = $ThisDepartmentShort;
	if ($EmployeeID){
		$criteria->employeeID = $EmployeeID;
	}
	$paginationResult = $gbsProvider->getEmployeeCodes($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$CodeByID[$record->id] = $record;
		if ($record->name == "501"){
			$SickVacationCode[] = $record->id;
		}
		if ($record->displayOrderId){
			$CodesActive[$record->displayOrderId]=$record;
			$CodesByName[$record->name]=$record;
			$CodesByIdUnsorted[$record->id]=$record;
//			$CodesById[]=$record;
			$CodeNames[$record->name] = $record->id;
		}else{
			$CodesInActive[$record->name]=$record;
		}

	}
	ksort($CodeNames);
	foreach ($CodeNames as $Name=>$record){
		$CodesById[$record] = $CodesByIdUnsorted[$record];
	}
	//Get Notes for EmployeeByID
	
	$criteria = new stdClass();
	$criteria->employeeId = $EmployeeID;
	$criteria->noteDate = $WeekPicker[$WeekPickerNumber]["start"];
	$criteria->department = $ThisDepartmentShort;
	//print_pre($criteria);
	$paginationResult = $gbsProvider->getNotes($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$Note = $record->note;
	}
?>


<br clear="all">
<div style="display:none;">
	<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
	<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
	<button>reset filters</button>
</div>
<script>
  $(function() {
	$( ".comboboxCodes" ).combobox({
		select: function(event, ui){
			var $this = $(this),
				thisText = $(ui.item).text().replace("-","<br>"),
				thisMonthYear = $this.attr('data-monthyear'),
				thisCount = parseInt($this.attr('data-count')),
				thisDisplayID = "#display"+thisMonthYear+thisCount,
				thisSelectID = "#select"+thisMonthYear+thisCount,
				NextRowID = "#row"+thisMonthYear+(thisCount+1);
			$(thisDisplayID).html(thisText);
			$(thisSelectID).hide();
			$(thisDisplayID).show();
			$(NextRowID).show();
			
		}
	});
	$(".editDisplay").on("click",function(){
		var $this = $(this),
			thisID = $this.attr('id').replace("display",""),
			thisSelectID = "#select"+thisID;
			$this.hide();
			$(thisSelectID).show();
	
	});
  });
</script>	
<style>
.ComboBoxInput {min-width:200px;}
.dayInput {border-right:1pt solid #aed0ea;border-bottom:0pt solid #aed0ea;min-width:15px;text-align:center;}
table.dataTable td {
	padding:0px;
	margins:0px;
}
.totals {min-width:34px;font-size:8pt;}
td.totals {border:1pt solid #aed0ea;}
.SubmittedValue {border:1pt solid #aed0ea;background-color:#C2C2D6;}
.SubmittedValueData {border:1pt solid #aed0ea;background-color:#FFCC80;}
.ApprovedValueData {border:1pt solid #aed0ea;background-color:#99E699;}
.deleteRow {position:relative;margin-top:5px;width:10px;height:10px;float:right;
	background-image:url("<?php echo $CurrentServer;?>images/redx-small.png");
	background-size: 10px 10px;
    background-repeat: no-repeat;
	background-position: left top;
	cursor:pointer;
}
.helpRow {position:relative;width:5px;height:5px;float:right;border:1pt solid red;cursor:pointer;}
.headerRow {text-align:center;font-size:10pt;}
.codeNameDisplay {width:90%;float:left;}
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
</style>
<?php
				//now get data for this week
				$WeekStart = strtotime($WeekPicker[$WeekPickerNumber]["start"]);
				$WeekStartOrg = $WeekStart;
				$WeekEnd = strtotime($WeekPicker[$WeekPickerNumber]["end"]);
				//Get PayWeek which are Monday-Sunday (regardless of month limitations)
					//Get Week number and Year for Start of month
					$StartMonthWeekNumber = date("W",$WeekStart);
					$StartMonthYear = date("Y",$WeekStart);
					
					//First Monday of the Week
					$PayWeekStartDay = date("m/d/Y", strtotime($StartMonthYear."W".$StartMonthWeekNumber));
					$PayDayIncrement = 0;
					//special case for End/Start of new year
					//if (strtotime($PayWeekStartDay)==strtotime("01/02/2017")){$PayWeekStartDay = "01/01/2016";}
					//create array of all days in the week Mon-Sun
					while ($PayDayIncrement < 7){
						$PayWeekDay = date("Y-m-d", strtotime($PayWeekStartDay." + ".$PayDayIncrement."Days"));
						$PayWeekEndYear = date("Y",strtotime($PayWeekDay));
						$PayWeek[] = $PayWeekDay;
						$PayWeekEndDay = $PayWeekDay;
					$PayDayIncrement++;
					}
				//End create PayWeek data
				
				if ($EmployeeID){
					//get currently saved hours
					$criteria = new stdClass();
					$criteria->employeeId=$EmployeeID;
					$criteria->startDate = date("Y-m-d",strtotime($PayWeekStartDay));
					$criteria->endDate = date("Y-m-d",strtotime($PayWeekEndDay));
					//print_pre($criteria);
					$paginationResult = $gbsProvider->getHours($criteria);
					$resultArray = $paginationResult->collection;
					foreach ($resultArray as $Hours){
						$HoursData[$Hours->hoursDate][]=$Hours;
						$CodeData[$Hours->codeId][]=$Hours;
						if (in_array($Hours->codeId,$SickVacationCode)){
							$PayWeekSicVacationData[$Hours->hoursDate] = bcadd($PayWeekSicVacationData[$Hours->hoursDate],$Hours->hours,2);
						}else{
							$PayWeekRegularData[$Hours->hoursDate] = bcadd($PayWeekRegularData[$Hours->hoursDate],$Hours->hours,2);
						}
					}
				}
				$CodeRows = (count($CodeData) ? count($CodeData) : 0);
				foreach ($CodeData as $CodeID=>$CodeInfo){
					$CodesWithData[] = $CodeID;
				}
				//print_pre($HoursData);
				foreach ($HoursData as $HourDate=>$DateInfo){
					if (strtotime($HourDate) >= $WeekStart && strtotime($HourDate) <= $WeekEnd){
						foreach ($DateInfo as $id=>$info){
							if ($info->status == "Submitted" || $info->status == "Approved"){
								$DatesWithSubmitted[] = $HourDate;
							}
							if ($info->status == "Approved"){
								$DatesApproved[] = $HourDate;
							}
						}
					}
				}
				//now resort CodesWithData to match alphabetical ordering
				foreach ($CodeNames as $CodeName=>$CodeId){
					if (count($CodeData[$CodeId])){
						$SortedCodes[$CodeId] = $CodeData[$CodeId];
					}
				}
				$CodeData = $SortedCodes;
?>
<?php 
if ($EmployeeID){
		$DisplayMonthYear = str_replace(" ","",$MonthYear);
	?>
	<div class="row">
		<div class="six columns" style="border:0pt solid red;">
			<div class="five columns">
				<h3>
					<?php echo $MonthYear;?>
					<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$MonthYear)));?>">
				</h3>
			</div>
			<div class="five columns">
				<select id="weekPicker" class="parameters" style="width:250px;">
				<?php
					foreach ($WeekPicker as $WeekNumber=>$WeekData){
						$thisWeekCounter++;
						echo "<option value=\"".$WeekNumber."\"".($WeekPickerNumber == $WeekNumber ? " selected" : "").">Week ".$thisWeekCounter.": ".$WeekData["display"]."</option>";
					}
				?>
				</select>
			</div>
		</div>
		<div class="ten columns" style="display:none;border:0pt solid red;height:100px;">
			Notes for this week:<br>
			<textarea style="width:100%;" id="notes" data-week="<?php echo $WeekPicker[$WeekPickerNumber]["start"];?>"><?php echo $Note;?></textarea>
		</div>
	</div>
	<!--
	<?php if (count($PayWeekRegularData) || count($PayWeekSicVacationDataData)){?>
		<div class="row" style="padding-left:10px;">
			<div class="twelve columns">
				<style>
					.CWTop {
						background-image:url('<?php echo $CurrentServer;?>images/CWHeader<?php echo (count($DatesWithSubmitted) ? "Submitted" : "");?>.png');
						background-size:509px;
						background-repeat:no-repeat;
						width:509px;
						height:32px;
						text-align:center;
						font-size:11px;
						font-weight:bold;
					}
					.CWTable{
						font-size: 10px;
						font-family: Arial;
					}
					.CWHeader {
						background-color:LightGray;
						text-align:center;
					}	
					.CWFirstColumn {
						text-align:left;
					}
					.CWData{
						border-top: solid 1px White;
						border-right: solid 1px Gray;
						border-bottom: solid 1px Gray;
						border-left: solid 1px White;
						padding:2px;
						padding-top: 5px;
						padding-bottom: 5px;
					}
					.CWValue {
						text-align:right;
					}
				</style>
				<div class="CWTop"><br><div class="two columns">&nbsp;</div>Total Hours <span id="CWTotalHours">hi</span></div>
				<table border="1" cellspacing="0" class="CWTable">
					<tr class="CWHeader">
						<td class="CWFirstColumn CWData">Code</td>
						<td class="CWData">ProgrmCode</td>
						<td class="CWData">Sub-Code</td>
						<?php 
							foreach ($PayWeek as $PayDay){
								echo '<td class="CWData">'.date("D",strtotime($PayDay)).'<br>'.date("m/d/y",strtotime($PayDay)).'</td>';
							}
						?>
						<td class="CWData">Total</td>
					</tr>
					<tr>
						<td class="CWFirstColumn CWData">Regular</td>
						<td class="CWData">&nbsp;</td>
						<td class="CWData">&nbsp;</td>
						<?php 
							foreach ($PayWeek as $PayDay){
								$RegularWeekTotal = bcadd($RegularWeekTotal,$PayWeekRegularData[$PayDay],2);
								echo '<td class="CWData CWValue">'.($PayWeekRegularData[$PayDay] ? $PayWeekRegularData[$PayDay] : "&nbsp;").'</td>';
							}
						?>
						<td class="CWData CWValue"><?php echo $RegularWeekTotal;?></td>
					</tr>
					<tr>
						<td class="CWFirstColumn CWData">Sick/Vaction</td>
						<td class="CWData">&nbsp;</td>
						<td class="CWData">&nbsp;</td>
						<?php 
							foreach ($PayWeek as $PayDay){
								$SickVacationWeekTotal = bcadd($SickVacationWeekTotal,$PayWeekSicVacationData[$PayDay],2);
								echo '<td class="CWData CWValue">'.($PayWeekSicVacationData[$PayDay] ? $PayWeekSicVacationData[$PayDay] : "&nbsp;").'</td>';
							}
						?>
						<td class="CWData CWValue"><?php echo $SickVacationWeekTotal;?></td>
					</tr>
				</table>
			</div>
		</div>
	<?php }//end if RegularPay?>
	-->
	<div class="row">
		<div class="thirteen columns">
			If you are missing codes, please contact Yosh Schulman to grant you access to the codes you are missing
		</div>
		<br clear="all">
		<div class="four columns" style="display:none;">
			<?php if (!count($DatesWithSubmitted)){?>
				<a href="show-all-codes" id="showAllCodes" class="button-link do-not-navigate parameters" title="<?php echo ($_GET['ShowAllCodes'] ? "Collapse Unfilled" : "All");?> Codes Assigned to <?php echo $DisplayEmployeeName;?>"><?php echo ($_GET['ShowAllCodes'] ? "Use Code Picker" : "Show All Codes");?></a>
			<?php }?>
			<input type="hidden" id="ShowAllCodes" value="<?php echo $_GET['ShowAllCodes'];?>">
		</div>
		<div class="savebuttons" class="ten columns" style="text-align:right;">
		<?php if (!$ReadOnlyTrue){?>
			<?php if (count($DatesWithSubmitted) && !$isStaff){?>
					<select id="adjustStatus" style="width:120px;">
						<option>Adjust Status</option>
						<?php if (count($DatesApproved)){?>
							<option value="approve_remove" data-status="Approved">Revert to Just Saved Status<?php if (!$StaffCanApproveOwnData){?> (remove Approved and Submitted status)<?php }?></option>
						<?php }else{?>
							<?php if (!$StaffCanApproveOwnData){?>
								<option value="submit_remove" data-status="Submitted">Reject</option>
								<option value="submit_approve" data-status="Approved">Approve</option>
							<?php }?>
						<?php }?>
					</select>
			<?php }?>
			<div class="eight columns" id="hoursResults">&nbsp;</div>
				<?php if (!count($DatesWithSubmitted)){?>
					<a href="save-code-update" id="savebutton" class="button-link do-not-navigate">Save</a>
					<?php if (count($CodesWithData)){?>
						<a href="submit-code-update" id="submitbutton" class="button-link do-not-navigate">Submit</a>
					<?php }?>
				<?php }else{?>
					<?php if ($isStaff){?>
						Hours have been submitted.<br>
						If you need them adjusted contact your department manager.
					<?php }?>
				<?php }?>
		<?php }//end if ReadOnly ?>
		</div>
		<table class="reportResults display" cellspacing="0" width="100%" cellpadding="0" id="table<?php echo $DisplayMonthYear;?>">
				<?php 
					$CodesActive = array_keys($CodesActive);
					$rowObj = new stdClass();
					$rowObj->DisplayMonthYear = $DisplayMonthYear;
					$rowObj->trCount = 1;
					$rowObj->trCountEnd = $CodeRows;
					$rowObj->WeekStartOrg = $WeekStartOrg;
					$rowObj->WeekEnd = $WeekEnd;
					$rowObj->RowDisplay = "block";
					$rowObj->Submitted = count($DatesWithSubmitted);
					if (count($DatesWithSubmitted)){
						$rowInfo = $gbsProvider->CodePickerRow($rowObj,$CodesById,$CodeData);
					}else{
						if ($_GET['ShowAllCodes']){
							$rowInfo = $gbsProvider->CodeDisplayRow($rowObj,$CodesById,$CodeData);
						}else{
							$rowInfo = $gbsProvider->CodePickerRow($rowObj,$CodesById,$CodeData);
						}
					}
					$SavedRows = $rowInfo["displayRow"];
					$SavedDayTotals = $rowInfo["dayTotals"];
				?>
			<thead>
				<tr>
					<td class="ComboBoxInput" align="left">Billing Code</td>
					<?php
						while ($WeekStart <=  $WeekEnd){
							echo "<td class=\"dayInput\">".date("D m/d",$WeekStart)."</td>";
							$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
						}
					
					?>
					<?php //if (count($CodeData)){?>
						<td class="totals"><span style="font-size:8pt;">Totals</span></td>
					<?php //}?>
				</tr>
				<tr>
					<td class="ComboBoxInput" align="right">Totals</td>
				<?php
					$WeekStart = $WeekStartOrg;
					while ($WeekStart <=  $WeekEnd){
						echo "<td class=\"dayInput columndate".date("Y-m-d",$WeekStart)."\">".$SavedDayTotals[$WeekStart]."</td>";
						$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
					}
					
				?>
					<?php //if (count($CodeData)){?>
						<td class="totals">&nbsp;</td>
					<?php //}?>
				</tr>
			</thead>
			<?php if (count($CodeData)){?>
			<tfoot>
				<tr>
					<td class="ComboBoxInput">Billing Code</td>
					<?php
						$WeekStart = $WeekStartOrg;
						while ($WeekStart <=  $WeekEnd){
							echo "<td class=\"dayInput\">".date("D m/d",$WeekStart)."</td>";
							$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
						}
					
					?>
					<?php if (count($CodeData)){?>
						<td class="totals">&nbsp;</td>
					<?php }?>
				</tr>
				<tr>
					<td class="ComboBoxInput" align="right">Totals</td>
				<?php
					$WeekStart = $WeekStartOrg;
					while ($WeekStart <=  $WeekEnd){
						echo "<td class=\"dayInput\"><span  id=\"columndateFooter".date("Y-m-d",$WeekStart)."\">".$SavedDayTotals[$WeekStart]."</span></td>";
						$WeekStart = strtotime(date("m/d/Y",$WeekStart)." +1 day");
					}
					
				?>
					<td class="totals">&nbsp;</td>
				</tr>
			</tfoot>
			<?php }?>
			
			
			<tbody>
				<form id="gbsHoursForm" name="gbsHoursForm">
					<?php 
						$textToRemove = array("Admin Assistant","Dept Manager","Prog Manager");
						echo str_replace($textToRemove," ",$SavedRows);?>
					<?php //echo $BlankRows;?>
				</form>
			</tbody>
		</table>
		<br clear="all">
		<!--
		<?php 
			if (count($SavedDayTotals)){
				echo "Displayed Total: ".array_sum($SavedDayTotals)." hours";
				if (count($DatesWithSubmitted)){
					if (count($DatesApproved)){
						echo " approved";
					}else{
						echo " submitted";
					}
				}else{
					echo " saved";
				}
				if (array_sum($SavedDayTotals) != bcadd($SickVacationWeekTotal,$RegularWeekTotal,2)){
					echo " (different than CheckWriters Total Hours because of month range)";
				}
			}
		?>
		-->
	</div>
<?php
}else{
?>
	<div class="ten columns">
		<div class="eight columns" style="padding-left:10px;"><?php if ($isStaff){?>Your profile has not been indicated by HR to utilize GHS Hours Tracking<?php }?></div>
	</div>
<?php }//end if EmployeeID?>
		<br clear="all">
		<br clear="all">
		<div class="row">
			<div class="twelve columns">
				<fieldset>
					<legend>Instructions</legend>
					<ol style="list-style-type:decimal;">
						<?php
							$StartLanguage = "Start by selecting";
							if (!$isStaff){
								$StartLanguage = "Next select";
						?>
							<li><span style="font-weight:bold;">Start by selecting the Staff</span><br> 
								The Staff Picker is a combo box that can either be used as a pulldown or autocomplete by starting to type the name of the staff member you want to select.
						<?php }//end if not staff?>
						<li><span style="font-weight:bold;"><?php echo $StartLanguage;?> the appropriate week</span><br> 
							The page will default to the current week of the current month.<br>
							If you need to go back to a different month, you can click the calendar icon <img src="<?php echo $CurrentServer;?>css/images/icon-calendar.png" width="15" height="15"> to change month
						<li><span style="font-weight:bold;">Indicate Hours for the appropriate Billing Code on the appropriate date</span><br> 
							Billing Codes displayed are those that the selected staff member has been granted access.<br>
							<!--To show only codes that have hours, click the 'Use Code Picker' button.<br>
							The Code Picker option is a combo box that can either be used as a pulldown or autocomplete by starting to type the code you want to select.<br>
							-->
							Totals for the week will be tallied by day at the top and bottom of the chart and by code at the right of the chart. 
						<li><span style="font-weight:bold;">Save hours</span><br> 
							Click the 'Save' button to capture the information just entered and have it available the next time this week is viewed.<Br>
						<li><span style="font-weight:bold;">Delete hours</span><br> 
							Click the <img src="<?php echo $CurrentServer;?>images/redx-small.png" width="10" height="10"> button to remove hours for that code.<Br>
						<!--
						<li><span style="font-weight:bold;">Enter Notes</span><br>
							Notes for the week can be entered in the text area at the top of the page.<br>
							Notes are continually saved as you type.<br>
							The text area can be expanded by grabbing and dragging the lower right corner icon <img src="<?php echo $CurrentServer;?>/images/textareaCorner.png">.
						-->
						<li><span style="font-weight:bold;">Submit hours once the week's information is complete</span><br> 
							Click the 'Submit' button to mark displayed items as submitted and will be highlighted in <span style="background-color:#99E699">green</span>.
						<!--
							Click the 'Submit' button to mark displayed items as submitted and ready for approval.<Br>
							Submitted hours can not be edited unless an administrator adjusts the status from 'Submitted' back to 'Saved'.<br>
							Submitted hours will be highlighted in <span style="background-color:#FFCC80">orange</span>.<br>
							Approved hours will be highlighted in <span style="background-color:#99E699">green</span>.
						-->
					</ol>
				</fieldset>
			</div>
		</div>

<script>
	$(document).ready(function() {
		$("#CWTotalHours").html('<?php echo bcadd($SickVacationWeekTotal,$RegularWeekTotal,2);?>');

		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				weekPicker = $("#weekPicker").val(),
				employeeId = $("#SelectedEmployee").val(),
				showAllCodes = $("#ShowAllCodes").val(),
				filterValues = new Array(startDate,endDate,weekPicker,employeeId,showAllCodes);
				
				return filterValues;
		};
	
		var updateFilters = function(){
			var Filters=getFilters();
			var refreshLocation = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>ghs/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&WeekPickerNumber="+Filters[2]+"&EmployeeID="+Filters[3]+"&ShowAllCodes="+Filters[4]+"&nav=<?php echo $nav;?>#";
			window.location.href = refreshLocation;
			return(refreshLocation);
		}
		$("#showAllCodes").on('click',function(){
			$("#ShowAllCodes").val('<?php echo ($_GET['ShowAllCodes'] ? "" : "true");?>');
			updateFilters();
		});
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		
		$('.date-picker').datepicker({
			showOn: "button",
			buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
			buttonImageOnly: true,
			buttonText: 'Click to change month',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			minDate: '01/01/14',
			maxDate: '<?php echo date("m/t/Y");?>',
			onClose:
				function(dateText, inst){
					var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, month, 1));
					$("#StartDate").datepicker('setDate', new Date(year, month, 1));
					var date2 = $(this).datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$("#EndDate").datepicker('setDate', date2);
					$("#weekPicker").val('');
					updateFilters();
				}
		
		});
		
		
		var table = $('.reportResults').DataTable({
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"info":     false
		});

		var dayInputValues = $(".dayInputValue");
		
		dayInputValues.on('blur',function(){
			var $this = $(this),
				thisValue = $this.val(),
				thisHourId = $this.attr('data-hourid'),
				DayValues = {};
			
			if (thisValue == '' && thisHourId !=''){
				DayValues["action"] = "delete_hourid";
				DayValues["hourid"] = thisHourId;
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "POST",
					data: JSON.stringify(DayValues),
					success: function(data){
						if (data.error == '' || !data.error){
							$('#hoursResults').show().html("Deleted").addClass("alert");
							location.reload();
						}else{
							$('#hoursResults').show().html(data.error).addClass("alert");
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#hoursResults').show().html(message).addClass("alert");
					}
				});				
			}
			
		});
		dayInputValues.on('keyup',function(){
			//get all the values for this column
			var $this = $(this),
				thisDate = $this.attr('data-columndate'),
				thisRow = $this.attr('data-rowCount'),
				thisColumn = ".columndate"+thisDate,
				thisColumnInputs = ".columndateInput"+thisDate,
				thisRowInputs = ".rowCountInput"+thisRow,
				thisColumnHeader = thisColumn+" .DataTables_sort_wrapper",
				thisColumnHeaderEl = $(thisColumnHeader),
				thisColumnFooter = "#columndateFooter"+thisDate,
				thisColumnFooterEl = $(thisColumnFooter),
				thisRowTotal = "#rowTotal"+thisRow,
				thisRowTotalEl = $(thisRowTotal),
				htmlStart = thisColumnHeaderEl.html();
				htmlClean = htmlStart.replace('<span class="DataTables_sort_icon"></span>','');
				htmlVal = (parseInt(htmlClean)+parseInt($this.val()));
				
				var ColVal = 0;
				$(thisColumnInputs).each(function(){
					var $thisClass = $(this),
					thisVal = parseFloat($thisClass.val());
					if (thisVal > 0){
						ColVal = parseFloat(ColVal)+thisVal;
					}
				});
				
				var RowVal = 0
				$(thisRowInputs).each(function(){
					var $thisClass = $(this),
					thisVal = parseFloat($thisClass.val());
					if (thisVal >0){
						RowVal = parseFloat(RowVal)+thisVal;
					}
				});
				
				thisColumnHeaderEl.html(ColVal);
				thisColumnFooterEl.html(ColVal);
				thisRowTotalEl.html(RowVal);
				$("#submitbutton").hide();

				
		});
		
		
		$("#savebutton").on('click',function(){
			var DayValues = {};
			var DayValuesFound = false;
			$('#hoursResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
			$.each(dayInputValues, function(key,obj){
				var thisObj = $(obj);
				if (thisObj.val().trim() != ''){
					DayValuesFound = true;
					DayValues[thisObj.attr('id')] = thisObj.val();
				}
			});			
			if (DayValuesFound){
				var EmployeeID = $("#SelectedEmployee").val();
				DayValues["action"] = "insert_hours";
				DayValues["employeeID"] = parseInt(EmployeeID);
				DayValues["department"] = '<?php echo $ThisDepartmentShort;?>';				
				console.log(DayValues);

				$.ajax({
					url: "ApiCodesManagement.php",
					type: "POST",
					data: JSON.stringify(DayValues),
					success: function(data){
//						console.log(data);
						if (data.Errors == '' || !data.Errors){
							location.reload();
						}else{
							$('#hoursResults').show().html(data.Errors).addClass("alert");
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log('error');
						console.log(jqXHR);
						var message = $.parseJSON(jqXHR.responseText);
						$('#hoursResults').show().html(message).addClass("alert");
					}
				});
			}else{
				$('#hoursResults').show().html("Please indicate the Billing Code and hours").addClass("alert");
			}
		});
		$(".deleteRow").on('click',function(){
			var DayValues = {},
				EmployeeID = $("#SelectedEmployee").val();
			var $this = $(this),
				thisId = $this.attr('id'),
				thisRow = "#"+$this.attr('data-row');
	
				DayValues["action"] = "delete_hours";
				DayValues["hourIDs"] = thisId;
				var data = JSON.stringify(DayValues);
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "POST",
					data: JSON.stringify(DayValues),
					success: function(data){
						if (data.Errors == '' || !data.Errors){
							$('#hoursResults').show().html("Deleted").addClass("alert");
							$(thisRow).hide();
							location.reload();
						}else{
							$('#hoursResults').show().html(data.Errors).addClass("alert");
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#hoursResults').show().html(message).addClass("alert");
					}
				});
		});
		
		$("#submitbutton").on('click',function(){
			var DayValues = {};
			var HourIDs = new Array();
			var DayValuesFound = false;
			$('#hoursResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");

			$.each(dayInputValues, function(key,obj){
				var thisObj = $(obj);
				if (thisObj.val().trim() != '' && thisObj.attr('data-hourid')){
					DayValuesFound = true;
					HourIDs.push(thisObj.attr('data-hourid'));
				}
			});			
			if (DayValuesFound){
				var EmployeeID = $("#SelectedEmployee").val();
				DayValues["action"] = "submit_<?php echo ($StaffCanApproveOwnData ? "approve" : "hours");?>";
				DayValues["hourIds"] = HourIDs;
				data = JSON.stringify(DayValues);
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "POST",
					data: JSON.stringify(DayValues),
					success: function(data){
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#hoursResults').show().html(message).addClass("alert");
					}
				});
				
			}else{
				$('#hoursResults').show().html("Please indicate the Billing Code and hours").addClass("alert");
			}
		});
		var  SubmittedValueDataSpan = $(".SubmittedValueDataSpan");
		var  ApprovedValueDataSpan = $(".ApprovedValueDataSpan");
		$("#adjustStatus").on('change',function(){
			var DayValues = {};
			var HourIDs = new Array();
			var DayValuesFound = false;
			$('#hoursResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert").addClass("info");
			var ValueDataSpan = (SubmittedValueDataSpan.length ? SubmittedValueDataSpan : ApprovedValueDataSpan);
			$.each(ValueDataSpan, function(key,obj){
				var thisObj = $(obj);
				if (thisObj.attr('data-hourid')){
					DayValuesFound = true;
					HourIDs.push(thisObj.attr('data-hourid'));
				}
			});			
			if (DayValuesFound){
				var $this = $(this);
				var EmployeeID = $("#SelectedEmployee").val();
				DayValues["action"] = $this.val();
				DayValues["hourIds"] = HourIDs;
				DayValues["status"] = $this.attr('data-status');
				data = JSON.stringify(DayValues);
				$.ajax({
					url: "ApiCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(DayValues),
					success: function(data){
						location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#hoursResults').show().html(message).addClass("alert");
					}
				});
				
			}else{
				$('#hoursResults').show().html("Please indicate the Billing Code and hours").addClass("alert");
			}
		});

		var timer = null;
		var notes = $("#notes");
		notes.on('keyup',function(){
			clearTimeout(timer);
			timer = setTimeout(updateComments, 1000);
		});
		var updateComments = function(){
			var thisVal = notes.val(),
				thisDate = notes.attr('data-week');
			
			var Comments = {};
				Comments["action"] = "insert_note";
				Comments["noteDate"] = thisDate;
				Comments["employeeId"] = "<?php echo $EmployeeID;?>";
				Comments["note"] = thisVal;
				Comments["department"] = '<?php echo $ThisDepartmentShort;?>';
				data = Comments;
				//console.log(data);
			$.ajax({
				url: "ApiCodesManagement.php",
				type: "POST",
				data: JSON.stringify(data),
				success: function(data){
					//console.log(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					notes.val(message);
				}
			});
		};
		
	});
</script>