<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HoursProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$ThisDepartment = 'Green Home Services';
$ThisDepartmentShort = "GHS";

$gbsProvider = new HoursProvider($dataConn);
$hrEmployeeProvider = new HREmployeeProvider($dataConn);

$EmployeeID = ($_GET["EmployeeID"] ? $_GET["EmployeeID"] : false);
$SelectedGroupsID = ($_GET["GroupID"] ? $_GET["GroupID"] : false);
$SelectedCodesID = ($_GET["CodeID"] ? $_GET["CodeID"] : false);
$Status = ($_GET["Status"] ? $_GET["Status"] : "Approved");
$HideCodes = ($_GET["HideCodes"] == "1" ? true : false);
$OnlySupervised = ($_GET["OnlySupervised"] == "1" ? true : false);
if ($Status == "null"){$Status = "Approved";}
$StartOfThisMonth = date("m/01/Y");

$ByMonthPicker = $_GET['ByMonthPicker'];
if ($ByMonthPicker){
	$StartOfThisMonth = $_GET['StartDate'];
	$EndOfThisMonth = $_GET['EndDate'];
	$StartYear = (date("m",strtotime($StartOfThisMonth)) > 1 ? date("Y",strtotime($StartOfThisMonth)) : (date("Y",strtotime($StartOfThisMonth))-1));
	$FirstPayPeriodOfYear = date("Y-m-d",strtotime("First Monday of January ".$StartYear));
	//echo $FirstPayPeriodOfYear."<br>";
	$PriorMonthPayPeriod = $FirstPayPeriodOfYear;

	$nextMonthName = date("M",strtotime($StartOfThisMonth));
	$ThisYear = date("Y",strtotime($StartOfThisMonth));
	$MonthYear = $nextMonthName." ".$ThisYear;	
	
	echo " Start of This Month:".$StartOfThisMonth;
	//echo $PriorMonthPayPeriod;
	while (strtotime($PriorMonthPayPeriod) < strtotime($StartOfThisMonth)){
		$PriorMonthPayPeriod = date("Y-m-d",strtotime($PriorMonthPayPeriod." +14 days"));
		if (strtotime($PriorMonthPayPeriod) < strtotime($StartOfThisMonth)){
			$LastPayPeriodofPriorMonth =$PriorMonthPayPeriod;
			//echo $PriorMonthPayPeriod."<br>";
			//echo strtotime($PriorMonthPayPeriod) ."<". strtotime($StartOfThisMonth)."<br>";
		}
	};
	$StartDate = date("m/d/Y",strtotime($LastPayPeriodofPriorMonth));
	echo " Last Pay Period from Prior Month Start Date: ".$StartDate;
	$LastPayPeriod = $PriorMonthPayPeriod;
	
	echo "<br>End of This Month:".$EndOfThisMonth;
	while (strtotime($LastPayPeriod) < strtotime($EndOfThisMonth)){
		$LastPayPeriod = date("Y-m-d",strtotime($LastPayPeriod." +14 days"));
		if (strtotime($LastPayPeriod) <= strtotime($EndOfThisMonth)){
			$LastPayPeriodofMonth =$LastPayPeriod;
			//echo $PriorMonthPayPeriod."<br>";
			//echo $LastPayPeriodofMonth ."<". $EndOfThisMonth."<br>";
			//echo strtotime($LastPayPeriodofMonth) ."<". strtotime($EndOfThisMonth)."<br>";
		}
	};
	$EndDate = date("m/d/Y",strtotime($LastPayPeriodofMonth." -1 day"));
	echo " Last Pay Period of this Month End Date: ".$EndDate;

}else{
	$EndOfThisMonth = date("m/t/Y");
	$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $StartOfThisMonth);
	$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $EndOfThisMonth);
	$nextMonthName = date("M",strtotime($StartDate));
	$ThisYear = date("Y",strtotime($StartDate));
	$MonthYear = $nextMonthName." ".$ThisYear;	
}



//Get GBSSupervised
$criteria = new stdClass();
$criteria->department = $ThisDepartmentShort;
$paginationsResults = $gbsProvider->getSupervisedBy($criteria);
$resultArray = $paginationsResults->collection;
foreach($resultArray as $result=>$record){
	$SupervisedBy[$record->employeeEmail] = $record->supervisorName;
}

$CurrentSupervisorName = $_SESSION["AdminAuthObject"]["adminFullName"];
//$CurrentSupervisorName = "Denice Hallstein";
//$CurrentSupervisorName = "Heather McCreary";
//$CurrentSupervisorName = "Julie Craumer";
//$CurrentSupervisorName = "Lisa Kohler";
//$CurrentSupervisorName = "Jae McAuley";
//echo "Currently Logged in As :".$CurrentSupervisorName ;
if ($CurrentSupervisorName == "Julie Craumer" || $CurrentSupervisorName == "Jae McAuley"){
	$OnlySupervised = false;
	$AlwaysShowAllStaff = true;
}else{
	$OnlySupervised = $_GET["OnlySupervised"];
	$OnlySupervised = (trim($OnlySupervised) == '' || $OnlySupervised > 0 ? true : false);
}



$TransitionaryManagerExceptionsPeckingOrder = array("Jae McAuley","Lisa Kohler","Denice Hallstein");
$TransitionaryManagerExceptions["Jae McAuley"] = array();
$TransitionaryManagerExceptions["Lisa Kohler"] = array("Jae McAuley");
$TransitionaryManagerExceptionList = $TransitionaryManagerExceptions[$CurrentSupervisorName];
$TransitionaryPeriod = false;
	
$nonDepartmentSupervisorDefault = ($TransitionaryPeriod ? "Denice Hallstein" : "Denice Hallstein");
$criteria = new stdClass();
$criteria->department = $ThisDepartment;
$criteria->hoursTrackingGHS = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
/*
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeEmailByID[$record->id] = $record->fullName." <".$record->email.">";
	
	$nonDepartmentSupervisor = ($SupervisedBy[$record->email] ? $SupervisedBy[$record->email] : $nonDepartmentSupervisorDefault);
	$manager = (trim($record->department) != $ThisDepartment ? $nonDepartmentSupervisor : $record->manager);
	if ($TransitionaryPeriod){
		$manager = (in_array($record->fullName,$TransitionaryManagerExceptionList) ? "Jae McAuley" : $CurrentSupervisorName);
		if (!in_array($CurrentSupervisorName,$TransitionaryManagerExceptionsPeckingOrder)){ $manager = "Jae McAuley";} 
	}
	//exception for Denice
	if ($manager == "Michelle Dailey"){
		$manager = "Denice Hallstein";		
	};
	//exception for Jae
	//$manager = (trim($record->title) == "Director of ".$ThisDepartment ? "Jae McAuley" : $manager);
	if ($CurrentSupervisorName == "Jae McAuley"){
		$manager = "Jae McAuley";		
	};
	//exception for Liz during transition period
	//if ($CurrentSupervisorName == "Liz Budd"){$manager = "Liz Budd";}
	$EmployeesByManager[$manager][]=$record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
}
*/
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$includeThisRecord = true;
	$EmployeeEmailByID[$record->id] = $record->fullName." <".$record->email.">";
	
	$nonDepartmentSupervisor = ($SupervisedBy[$record->email] ? $SupervisedBy[$record->email] : $nonDepartmentSupervisorDefault);
	$manager = (trim($record->department) != $ThisDepartment ? $nonDepartmentSupervisor : $record->manager);
	if ($TransitionaryPeriod){
		$manager = (in_array($record->fullName,$TransitionaryManagerExceptionList) ? "Jae McAuley" : $CurrentSupervisorName);
		if (!in_array($CurrentSupervisorName,$TransitionaryManagerExceptionsPeckingOrder)){ $manager = "Jae McAuley";} 
	}
	//exception for Denice
	if ($manager == "Michelle Dailey"){
		$manager = "Denice Hallstein";		
	};
	//exception for Jae
	//$manager = (trim($record->title) == "Director of ".$ThisDepartment ? "Jae McAuley" : $manager);
	if ($CurrentSupervisorName == "Jae McAuley"){
		$manager = "Jae McAuley";		
	};
	$EmployeesByManager[$manager][]=$record->lastName."_".$record->firstName;
	if ($OnlySupervised){
		if ($manager == $CurrentSupervisorName || strtolower($record->fullName) == strtolower($CurrentSupervisorName)){
			$includeThisRecord = true;
			$EmployeesByThisManager[] = $record->lastName."_".$record->firstName;

		}else{
			$includeThisRecord = false;
		}
	}
	if ($includeThisRecord){
		if ($record->displayOrderId){
			$EmployeesActive[$record->displayOrderId]=$record;
		}else{
			$EmployeesInActive[$record->firstName]=$record;
		}
	}
}
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AllEmployeesByID[$record->id] = $record;
}

//Get Groups
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupsByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
}
ksort($GroupsByName);

//Get CodeData
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->department = $ThisDepartmentShort;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodesByID[$record->id] = $record;
	$CodesByName[trim($record->name)]=$record;
	$CodesInGroup[$record->groupId][] = $record->id;
	$CodesInGroupByName[$record->groupId][] = $record->name;
	if ($record->nonbillable){
		$CodesInGroup["nonbillable"][] = $record->id;
		$CodesInGroupByName["nonbillable"][] = $record->name;
	}
	if ($record->displayOrderId){
		$CodesActive[$record->name]=$record;
	}
}
ksort($CodesActive);
ksort($CodesByName);
//print_pre($CodesByName);
//get hours
if ($Status != "Approved"){
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeId = $SelectedCodesID;
	$criteria->employeeId = $EmployeeID;
	$criteria->status = ($Status != 'All' ? $Status : false);
	$criteria->department = $ThisDepartmentShort;
	//print_pre($criteria);
	$paginationResult = $gbsProvider->getHours($criteria);
	$resultArray = $paginationResult->collection;
	$RecordCount = 0;
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
				if (!in_array($Hours->codeId,$CodesInGroup[$SelectedGroupsID])){
					$includeData = false;
				}
		}
		if ($includeData){
			$EmployeeName= $AllEmployeesByID[$Hours->employeeId]->lastName."_".$AllEmployeesByID[$Hours->employeeId]->firstName;
			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
				$EmployeeData[$EmployeeName][$Hours->codeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$Hours->codeId]["detail"][]=array(date("M-d",strtotime($Hours->dateWorked)),$Hours->hours);
				$EmployeeData[$EmployeeName][$Hours->codeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$Hours->codeId]["totalHours"],$Hours->hours,2);
				$CodesData[$CodesByID[$Hours->codeId]->name]=$Hours->codeId;
				$CodesDataTotals[$Hours->codeId] = bcadd($CodesDataTotals[$Hours->codeId],$Hours->hours,2);
				
				if ($CodesByID[$Hours->codeId]->nonbillable){
					$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
					$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
				}else{
					$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
					$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
				}
				if ($CodesByID[$Hours->codeId]->name == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}			
			}//end includeThisData
		}
	}
	ksort($CodesData);
}else{
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeName = ($SelectedCodesID ? $CodesByID[$SelectedCodesID]->name : false);
	$criteria->lastName = ($EmployeeID ? $AllEmployeesByID[$EmployeeID]->lastName : false);
	$paginationResult = $gbsProvider->getGHSApprovedHours($criteria);
	$resultArray = $paginationResult->collection;
	$RecordCount = 0;
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
			if (!in_array($Hours->codeName,$CodesInGroupByName[$SelectedGroupsID])){
				$includeData = false;
			}
		}
		if ($includeData){
			$EmployeeName= $Hours->employeeName;
			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
	//			$CodeData[$Hours->codeId][]=$Hours;
				$codeName = trim($Hours->codeName);
				$groupName = $GroupsByID[$CodesByName[$codeName]->groupId]->name;
				$codeId = $CodesByName[$codeName]->id;
				$EmployeeData[$EmployeeName][$codeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$codeId]["detail"][]=array(date("M-d",strtotime($Hours->dateWorked)),$Hours->hours);
				$EmployeeData[$EmployeeName][$codeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$codeId]["totalHours"],$Hours->hours,2);
				$CodesData[$codeName]=$codeId;
				$CodesDataTotals[$codeId] = bcadd($CodesDataTotals[$codeId],$Hours->hours,2);
				$HoursByCode[$groupName][$codeName] = bcadd($HoursByCode[$groupName][$codeName],$Hours->hours,2);
				
				if ($Hours->codeName == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}else{
					if ($CodesByName[$Hours->codeName]->nonbillable){
						$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
						$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
					}else{
						$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
						$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
					}
				}
			}//end includeThisData
		}
	}
	ksort($CodesData);
	
}
ksort($HoursByCode);
ksort($EmployeeData);
$DivColumns = "four";
$DivColumns1 = "four";
$employeeComboBoxDivColumns = $DivColumns;
$employeeComboBoxHideLabel = true;
$employeeComboBoxFunction = '$("#SelectedEmployee").val($(ui.item).val()).change();';
$codesComboBoxDivColumns = $DivColumns;
$codesComboBoxHideLabel = true;
$codesComboBoxFunction = '$("#SelectedCode").val($(ui.item).val()).change();';
$groupsComboBoxDivColumns = $DivColumns;
$groupsComboBoxHideLabel = true;
$groupsComboBoxFunction = '$("#SelectedGroup").val($(ui.item).val()).change();';
$customComboBoxInput = "width:150px;font-weight:normal;font-size:8pt;";
?>
<style>
.deleteRow {position:relative;width:10px;height:10px;float:right;top:-20px;right:10px;
	background-image:url("<?php echo $CurrentServer;?>images/redx-small.png");
	background-size: 10px 10px;
    background-repeat: no-repeat;
	background-position: left top;
	cursor:pointer;
}
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
</style>
	<h3>GHS Hours Report</h3>
	<div class="twenty columns">
		<fieldset>
			<legend>Filters</legend>
			<div class="four columns">
				By Payperiod Month:<br><?php echo $MonthYear;?>
				<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$MonthYear)));?>">
				<input type="hidden" size="8" id="ByMonthPicker" name="ByMonthPicker" value="">
			</div>
			
			<div class="three columns">
				By Date Range:<br>
				Start Date<bR>
				<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo $StartDate;?>" title="Start Date">
			</div>
			<div class="two columns">
				<br>
				End Date<br>
				<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo $EndDate;?>" title="End Date">
			</div>
			<div class="two columns" style="display:none;">
				Status<br>
				<select class="parameters" id="SelectedStatus" style="width:100px;">
					<option value="Approved"<?php echo ($Status=="Approved" ? " selected" : "");?>>Approved</option>
					<option value="Submitted"<?php echo ($Status=="Submitted" ? " selected" : "");?>>Submitted</option>
					<option value="Saved"<?php echo ($Status=="Saved" ? " selected" : "");?>>Saved</option>
					<option value="All"<?php echo ($Status=="All" ? " selected" : "");?>>All</option>
				</select>
			</div>
			<div class="three columns" style="display:none;">
				<Br>
				<input class="parameters" type="checkbox" id="HideCodes"<?php echo ($HideCodes ? " checked" : "");?>>Hide Codes<br>
			</div>
			<br clear="all">
			<div class="<?php echo $DivColumns1;?> columns">
				Employee<br>
				<?php include('../hr/_EmployeeComboBox.php');?>
				<?php if ($EmployeeID){?>
					<div class="deleteRow" data-box="SelectedEmployee">&nbsp;</div>
				<?php }?>
				<div style="display:<?php echo ($AlwaysShowAllStaff ? "none" : "block");?>"> 
					<input class="parameters" type="checkbox" id="OnlySupervised"<?php echo ($OnlySupervised ? " checked" : "");?>>Only Those You Supervise
				</div>
			</div>
			<div class="<?php echo $DivColumns1;?> columns" style="display:none;">
				Code<br>
				<?php include('_CodesComboBox.php');?>
				<?php if ($SelectedCodesID){?>
					<div class="deleteRow" data-box="SelectedCode">&nbsp;</div>
				<?php }?>
			</div>
			<div class="<?php echo $DivColumns1;?> columns">
				Group<br>
				<?php include('_GroupsComboBox.php');?>
				<?php if ($SelectedGroupsID){?>
					<div class="deleteRow" data-box="SelectedGroup">&nbsp;</div>
				<?php }?>
			</div>
			<div class="two columns">
				<br>
				<button id="resetbutton">Reset Filters</button>
			</div>
		</fieldset>
		<br clear="all">
		<?php echo $RecordCount." records for ".count($EmployeeData)." employees";?>
	</div>
	<br clear="all">
	<div>
		<h4>Details Breakdown</h4>
	
		<style>
			table.dataTable tbody {}
			.StaffName {min-width:85px;border:1pt solid #aed0ea;}
			.hourData {border:1pt solid #aed0ea;min-width:40px;}
			.CodeDataFooter {font-weight:normal;font-size:8pt;text-align:center;}
			.CodeDetails {display:none;}
		</style>
		<Br clear="all">
		<div class="four columns" style="margin-top:<?php echo ($SelectedGroupsID ? "-9" : "23");?>px;margin-right:-10px;">
			<br><br>
			<table class="reportFixedColumns display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="StaffName">Last Name</th>
						<th class="StaffName">First Name</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\">".$LastName."</td>";
							echo "<td class=\"StaffName\">".$FirstName."</td>";
						}
					?>
				</tbody>
			</table>
		</div>
		<style>
			th {min-width:70px;}
		</style>
		<div class="ten columns">
			<table class="reportResults display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="StaffName" style="display:none;">Last Name</th>
						<th class="StaffName" style="display:none;">First Name</th>
						<?php 
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									echo "<th class=\"CodeDataHeader\">".$codeName."</th>";
								}
							}
						?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="StaffName" style="display:none;">&nbsp;</td>
						<td class="StaffName" style="display:none;">Totals</td>
						<?php 
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									echo "<td class=\"CodeDataFooter\">".$CodesDataTotals[$codeId]."</td>";
								}
							}
						?>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\" style=\"display:none;\">".$LastName."</td>";
							echo "<td class=\"StaffName\" style=\"display:none;\">".$FirstName."</td>";
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									$dataDetails = "";
									foreach ($HoursData[$codeId]["detail"] as $dateWorked){$dataDetails .=$dateWorked[0]." ".$dateWorked[1]."\n";}

									echo "
										<td align=\"center\" class=\"hourData\">
											<span class='hourDataDetail' title='".$dataDetails."' style='cursor:pointer'>".$HoursData[$codeId]["totalHours"]."</span>
											<div class=\"CodeDetails\">".$FirstName."<br>".$dataDetails."</div>
										</td>";
								}
							}
							echo "</tr>";
							
						}
					?>
				</tbody>
			</table>
		</div>
		<br clear="all">
		<div style="display:<?php echo (!$OnlySupervised ? "block" : "none");?>;">
			<hr>
			<h4>Pivot Table Summary</h4>

			<div class="twenty columns">
				<table class="reportResults display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Category</th>
							<th>Code</th>
							<th>Hours</th>
							<th>Rate</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach ($HoursByCode as $GroupName=>$GroupBreakdown){
								$thisAmount = 0;
								$ThisTotalHours = 0;
								$ThisTotalAmount = 0;
								echo "<tr>";
								echo "<td>".$GroupName."</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
								echo "</tr>";
								foreach ($GroupBreakdown as $thisCodeName=>$thisCodeHours){
									$thisAmount = bcmul($CodesByName[$thisCodeName]->rate,$thisCodeHours,2);
									$ThisTotalHours = bcadd($ThisTotalHours,$thisCodeHours,2);
									$ThisTotalAmount = bcadd($ThisTotalAmount,$thisAmount,2);
									echo "<tr><td>&nbsp;</td>";
									echo "<td>".$thisCodeName."</td>";
									echo "<td>".$thisCodeHours."</td>";
									echo "<td>".$CodesByName[$thisCodeName]->rate."</td>";
									echo "<td>".($CodesByName[$thisCodeName]->rate*$thisCodeHours)."</td>";
									echo "</tr>";
								}
								echo "<tr>";
								echo "<td>".$GroupName."</td><td>Total</td><td>".$ThisTotalHours."</td><td>&nbsp;</td><td>".$ThisTotalAmount."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
<script>
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		
		$('.date-picker').datepicker({
			showOn: "button",
			buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
			buttonImageOnly: true,
			buttonText: 'Click to change month',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			minDate: '01/01/14',
			maxDate: '<?php echo date("m/t/Y");?>',
			onClose:
				function(dateText, inst){
					var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, month, 1));
					$("#StartDate").datepicker('setDate', new Date(year, month, 1));
					var date2 = $(this).datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$("#EndDate").datepicker('setDate', date2);
					$("#weekPicker").val('');
					$("#ByMonthPicker").val('true');
					updateFilters();
				}
		
		});
		$("#StartDate").focus(function () {
			$(".ui-datepicker-calendar").show();
		});		
		$("#EndDate").focus(function () {
			$(".ui-datepicker-calendar").show();
		});		
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				employeeId = $("#SelectedEmployee").val(),
				groupId = $("#SelectedGroup").val(),
				codeId = $("#SelectedCode").val(),
				status = $("#SelectedStatus").val(),
				hideCodes = ($("#HideCodes").prop("checked") ? 1 : 0),
				onlySupervised = ($("#OnlySupervised").prop("checked") ? 1 : 0),
				byMonthPicker = $("#ByMonthPicker").val(),

				filterValues = new Array(startDate,endDate,employeeId,groupId,codeId,status,hideCodes,onlySupervised,byMonthPicker);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>ghs/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&EmployeeID="+Filters[2]+"&GroupID="+Filters[3]+"&CodeID="+Filters[4]+"&Status="+Filters[5]+"&HideCodes="+Filters[6]+"&OnlySupervised="+Filters[7]+"&ByMonthPicker="+Filters[8]+"&nav=<?php echo $navDropDown;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			$(".parameters").val('');
			updateFilters();
			
		});

		
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"sScrollX":	"100%",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		var table2 = $('.reportFixedColumns').DataTable({
			"sScrollX":	"100%",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		$(".deleteRow").on("click",function(){
			var $this = $(this),
				thisBox = $this.attr('data-box');
				thisBoxID = "#"+thisBox;
				$(thisBoxID).val('').change();
			
		});
		$(".hourDataDetail").on("click",function(){
			var $this = $(this);
			$(this).next('.CodeDetails').toggle();
		});
	});
</script>
<br clear="all">
<a href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=report-rawdata" class="button-link">Raw Data</a>