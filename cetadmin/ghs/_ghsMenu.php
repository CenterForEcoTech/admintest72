<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_GHS)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "90";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>hours?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Admin (moved to Hours tab)</a></li>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GHS_Inspection)) || $ThisMenuSuperAdmin){?>
			<li><a href="https://repos/admintest/cetadmin/residential/?nav=inspection-schedule" class="button-link">Inspection Schedule Report (moved to Residential tab)</a></li>			
		<?php }?>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GHS))){?>
			<!--
			<li><a id="manage-codes" href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-codes" class="button-link">Manage Billing Codes</a></li>
			<li><a id="manage-groups" href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-groups" class="button-link">Manage Billing Groups</a></li>
			<li><a id="manage-hoursentry" href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Billable Hours Entry</a></li>
			<li><a id="manage-submittedhours" href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=manage-submittedhours&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Billable Hours Review</a></li>
			<li><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=report-bycode&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&ByMonthPicker=true" class="button-link">Billable Hours Reports</a>
				<?php //($nav != "reports" ? include('_reportsTabs.php') : "");?>
			</li>
			-->
<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "report-bycode":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				case "report-rawdata":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
		
		
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>