<?php

$scrollId = "";
$alertArray = array();

//check to see if file is submitted
if (isset($_POST['CodesCampaign_TimeStamp'])){
    include_once($dbProviderFolder."Helper.php");
	//TODO validate code does not already exist
	
	//Loop through all the posted form info to build field names
	$sqlParams = array();
	foreach ($_POST as $key => $val) {
		//echo $key."=".$val."<br>";
		$sqlStringParamsKey .= $key.",";
		$sqlStringParamsVal .= "?,";
		$sqlBindingsType = "s";
		if (substr($key,-2)=="ID"){$sqlBindingsType = "i";}
		if (substr($key,-4)=="Date"){if(MySQLDate($val)){$val = MySQLDateUpdate($val);}else{$val='0000-00-00';}}
		if (substr($key,-6)=="Amount"){$sqlBindingsType = "d";}
		$sqlBindings .= $sqlBindingsType;
		array_push($sqlParams,$val);
	}
	$sqlStringParamsKey = rtrim($sqlStringParamsKey,",");
	$sqlStringParamsVal = rtrim($sqlStringParamsVal,",");
	$sqlString = "INSERT INTO codes_campaign (".$sqlStringParamsKey.") VALUES (".$sqlStringParamsVal.")";
	//echo $sqlString."<br>";
	//echo $sqlBindings."<br>";
	//print_r($sqlParams);
	$updateResult = MySqliHelper::execute($mysqli, $sqlString, $sqlBindings, $sqlParams);
}

if (isset($_POST['CodesCampaign_ID'])){
    include_once($dbProviderFolder."Helper.php");
	//Loop through all the posted form info to build field names
	$sqlParams = array();
	foreach ($_POST as $key => $val) {
		//echo $key."=".$val."<br>";
		$sqlStringParams .= $key." = ?,";
		$sqlBindingsType = "s";
		if (substr($key,-2)=="ID"){$sqlBindingsType = "i";}
		if (substr($key,-4)=="Date"){if(MySQLDate($val)){$val = MySQLDateUpdate($val);}else{$val='0000-00-00';}}
		if (substr($key,-6)=="Amount"){$sqlBindingsType = "d";}
		$sqlBindings .= $sqlBindingsType;
		array_push($sqlParams,$val);
	}
	$sqlStringParams = rtrim($sqlStringParams,",");
	$sqlString = "UPDATE codes_campaign SET ".$sqlStringParams." WHERE CodesCampaign_ID = ".$_POST['CodesCampaign_ID'];
	//echo $sqlString."<br>";
	//echo $sqlBindings."<br>";
	//print_r($sqlParams);
	$updateResult = MySqliHelper::execute($mysqli, $sqlString, $sqlBindings, $sqlParams);
	if ($updateResult){
		$alertArray[$_POST['CodesCampaign_ID']] = "Change has been made.";
		if (count($alertArray) == 1){
			$scrollId = "form-".$_POST['CodesCampaign_ID'];
		}
	}
}


$SQL_Codes = "SELECT * FROM codes_campaign ORDER BY CodesCampaign_TimeStamp DESC";
$result_Codes = mysqli_query($result_Codes, $SQL_Codes);
$CodesArrayInsert = array();
$c = 0;
while ($c < mysqli_num_rows($result_Codes)){
//CodesCampaign_ID,CodesCampaign_Code,CodesCampaign_Comments,CodesCampaign_ValidUntilDate,
//CodesCampaign_ValidUseDate,CodesCampaign_Destination,CodesCampaign_ServiceRateType,CodesCampaign_ServiceRateAmount,CodesCampaign_TimeStamp 
	$i = 0;
	while ($i < mysqli_field_count($result_Codes)){
		if (substr(mysql_field_name($result_Codes,$i),-2) == "ID"){
			if (!mysql_result($result_Codes,$c,mysql_field_name($result_Codes,$i))){
				${mysql_field_name($result_Codes,$i)} = 0;
			}else{
				mysqli_data_seek($result_Codes,$c);
				$fetch = mysqli_fetch_array($result_Codes);
				${mysql_field_name($result_Codes,$i)} = $fetch[mysql_field_name($result_Codes,$i)];
			}
		}else{
			if (!mysql_result($result_Codes,$c,mysql_field_name($result_Codes,$i))){
				${mysql_field_name($result_Codes,$i)} = "";
			}else{
				mysqli_data_seek($result_Codes,$c);
				$fetch = mysqli_fetch_array($result_Codes);
				${mysql_field_name($result_Codes,$i)} = $fetch[mysql_field_name($result_Codes,$i)];
			}
		}
		$CodesArray[mysql_result($result_Codes,$c,"CodesCampaign_ID")][mysql_field_name($result_Codes,$i)]=${mysql_field_name($result_Codes,$i)};
		$i++;
	//echo $c."=".mysql_field_name($result_Codes,$i)."=".${mysql_field_name($result_Codes,$i)}."<br>";
	}
	$c++;
}
$i = 0;
while ($i < mysqli_field_count($result_Codes)){
	array_push($CodesArrayInsert,mysql_field_name($result_Codes,$i));
	$i++;
}

?>

<h2>Referral Codes</h2>
ValidUseDate is the first day that a code is valid.<br>
ValidUntilDate is the last day that a code is valid (and it is valid through the end of that day).<br>
A blank in either field is treated as infinity in that direction. In other words, a code without a ValidUntilDate never expires, and a code without a ValidUseDate is active as soon as it is created.
<div id="referralCodes-forms">
		<form method="POST" action="#ReferralCodes" id="form-NewCode">
			<fieldset>
			<?php
			foreach ($CodesArrayInsert as $key){

				$InputStyle = " style=\"width:200px;\"";
				$InputClass = "";
				$val = "";
				if(substr($key,-4) == "Date"){
					$InputClass = " class=\"auto-datepicker1\"";
				}
				if ($InputClass == " class=\"auto-datepicker1\""){$InputStyle = " style=\"width:100px;\"";}
				$isHidden = (substr($key,-9) == "TimeStamp" ? true : false);
				$is_ID = (substr($key,-3) == "_ID" ? true : false);
				$keyDisplay = explode("_",$key);
				if ($keyDisplay[1] == "Code" || $keyDisplay[1] == "ServiceRateAmount"){$InputStyle = " style=\"width:100px;\"";}
				if ($keyDisplay[1] == "ServiceRateAmount"){$val = "3.00";}
				if ($keyDisplay[1] == "Destination"){$val = "events.causetown.org";}
				if (!$isHidden && !$is_ID){
			?>
						<label style="float:left; padding:10px;">
						<span><?php echo $keyDisplay[1];?></span>
						<?php if ($keyDisplay[1] == "ServiceRateType"){?>
							<select name="<?php echo $key;?>" style="width:120px;">
                                <option value="percentage">percentage</option>
                                <option value="flatfee">flatfee</option>
                                <option value="annual">annual</option>
                            </select>
						<?php }else{?>
							<input type="text" name="<?php echo $key;?>" value="<?php echo $val;?>"<?php echo $InputClass.$InputStyle;?>>
						<?php }?>
						</label>
			<?php
				}//end isHidden
			}//end foreach ReferralCode ?>
			<input type="hidden" value="<?php echo date("Y-m-d g:i:s");?>" name="<?php echo "CodesCampaign_TimeStamp";?>">
			<input type="submit" class="button-link" value="Add New Code">
			</fieldset>
		</form>

	<?php
	foreach ($CodesArray as $ReferralCode){
	?>
		<form method="POST" action="#ReferralCodes" id="form-<?php echo $ReferralCode["CodesCampaign_ID"];?>">
			<fieldset>
			<?php
			$alertValue = isset($alertArray[$ReferralCode["CodesCampaign_ID"]]) ? $alertArray[$ReferralCode["CodesCampaign_ID"]] : "";
			foreach ($ReferralCode as $key=>$value){

				$InputStyle = " style=\"width:200px;\"";
				$InputClass = "";
				if(substr($key,-4) == "Date"){
					$InputClass = " class=\"auto-datepicker1\"";
					$value=MySQLDate($value);
				}
				if ($InputClass == " class=\"auto-datepicker1\""){$InputStyle = " style=\"width:100px;\"";}
				$isHidden = (substr($key,-9) == "TimeStamp" ? true : false);
				$is_ID = (substr($key,-3) == "_ID" ? true : false);
				$keyDisplay = explode("_",$key);
				if ($keyDisplay[1] == "Code" || $keyDisplay[1] == "ServiceRateAmount"){$InputStyle = " style=\"width:100px;\"";}
				if (!$isHidden && !$is_ID){
			?>
						<label style="float:left; padding:10px;">
						<span><?php echo $keyDisplay[1];?></span>
						<?php if ($keyDisplay[1] == "ServiceRateType"){?>
							<select name="<?php echo $key;?>" style="width:120px;">
                                <option value="percentage"<?php if ($value=="percentage"){echo " selected";}?>>percentage</option>
                                <option value="flatfee"<?php if ($value=="flatfee"){echo " selected";}?>>flatfee</option>
                                <option value="annual"<?php if ($value=="annual"){echo " selected";}?>>annual</option>
                            </select>
						<?php }else{?>
							<input type="text" name="<?php echo $key;?>" value="<?php echo $value;?>"<?php echo $InputClass.$InputStyle;?><?php if ($keyDisplay[1] == "Code"){echo " disabled";}?>>
						<?php }?>
						</label>
			<?php
				}//end isHidden
			}//end foreach ReferralCode ?>
			<input type="hidden" value="<?php echo $ReferralCode["CodesCampaign_ID"];?>" name="<?php echo "CodesCampaign_ID";?>">
			<input class="button-link" type="submit" value="Change">
			</fieldset>
			<div class="alert"><?php echo $alertValue;?></div>
				
		</form>
		<hr>
<?php
	} // foreach CodesArray
	?>
</div>


<script type="text/javascript">
	$(function(){
		$(".auto-datepicker1").datepicker();

		var formsDiv = $("#referralCodes-forms");

		formsDiv.on("focus", "input,textarea,select", function(){
			$(this).closest("form").find(".alert").html("");
		});
		formsDiv.on("change", "select", function(){
			var form = $(this).closest("form");
			form.find(".alert").html("Processing...");
			form.submit();
		});
	});
</script>