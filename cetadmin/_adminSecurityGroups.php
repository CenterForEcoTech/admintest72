<?php
//Security Groups
/*
	1 = SuperAdmin
	20 = Super Admin Read Only
	30 = Inventory - SuperAdmin
	40 = HR - SuperAdmin
	42 = Inventory - ReadOnly
	43 = HR - ReadOnly
	44 = Energy Specialist Commissions - ReadOnly
	45 = Super Admin Read Only with access to Commissions
	46 = GBS - Super Admin
	47 = GHS - Super Admin
	48 = Forecast - Super Admin
	49 = MO - Super Admin
	50 = GHS - Inspection Report
	51 = Muni - Super Admin
	52 = Muni - Customer Service
	53 = Invoicing - Super Admin
	54 = Residential - Super Admin
	55 = Hours - Super Admin
	56 = Residential - Auditor
	57 = Residential - HES Stats
	58 = Hours - Report Access
*/
	

$Security_Tabs_Inventory = array(1,20,30,42,44,45); // can access the Inventory Tab
$Security_Tabs_Tools = array(1); // can access the Tools Tab
$Security_Tabs_SiteConfigs = array(1); // can access the Tools Tab
$Security_Tabs_HR = array(1,20,40,43,45); // can access the Tools Tab
$Security_Tabs_GBS = array(1,20,45,46); // can access the Tools Tab
$Security_Tabs_MO = array(1,20,45,49); // can access the Tools Tab
$Security_Tabs_GHS = array(1,20,45,47,50); // can access the Tools Tab
$Security_Tabs_Forecast = array(1,20,45,48); // can access the Forecast Tab
$Security_Tabs_Resources = array(1,20,30,40,42,43,44,45,46,47,48,49,50); // can access the Resource Tab
$Security_Tabs_Muni = array(1,20,51,52); // can access the Tools Tab
$Security_Tabs_Admin = array(1,20,45); // can access the Tools Tab
$Security_Tabs_Invoicing = array(1,20,45,53); // can access the Retail Invoicing Tab
$Security_Tabs_Residential = array(1,20,45,54,56,57); // can access the Residential Tab
$Security_Tabs_Hours = array(1,20,45,55,58); // can access the Hours Tab

$Security_Menu_Inventory = array(1,20,30,42,44,45); // can access the Inventory Menus
$Security_CommissionsReport_Inventory = array(1,44,45); //can access Commission Report
$Security_Menu_Warehouse = array(1,20,30,45); // can access the Warehouse Menus
$Security_Menu_Tools = array(1); //can access Tools Menus
$Security_Menu_Tools_Admin = array(1); //can access Tools Menus for Admins
$Security_Menu_HR = array(1,20,40,43,45); 
$Security_Menu_GBS = array(1,20,45,46); 
$Security_Menu_MO = array(1,20,45,49); 
$Security_Menu_GHS = array(1,20,45,47); 
$Security_Menu_GHS_Inspection = array(1,20,45,47,50);
$Security_Menu_Forecast = array(1,20,45,48); //can access Forecast Menus
$Security_Menu_Resources = array(1,20,30,40,42,43,44,45,46,47,48,49,50); //can access Resource Menus
$Security_Menu_Muni = array(1,20,51,52); 
$Security_Menu_Admin = array(1);
$Security_Menu_Invoicing = array(1,20,45,53); //can access Retail Menus
$Security_Menu_Residential = array(1,20,45,54,56,57); //can access Residential Menus
$Security_Menu_ResidentialHESStats = array(1,20,45,54,57); //can access Residential Menus
$Security_Menu_Hours = array(1,20,45,55,58); //can access Hours Menus

$Security_Group_ReadOnly = array(20,42,43,44,45); //cannot submit forms
$ReadOnlyTrue = (count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly)) ? true : false);
//Will not be affected by other ReadyOnly groupings
$Security_SuperAdmin_Inventory = array(1,30);
$Security_SuperAdmin_Tools = array(1); 
$Security_SuperAdmin_SiteConfigs = array(1);
$Security_SuperAdmin_HR = array(1,40); 
$Security_SuperAdmin_GBS = array(1,46);
$Security_SuperAdmin_MO = array(1,49);
$Security_SuperAdmin_GHS = array(1,47);
$Security_SuperAdmin_Forecast = array(1,45,48);
$Security_SuperAdmin_Resources = array(1); 
$Security_SuperAdmin_Muni = array(1,51); 
$Security_SuperAdmin_Admin = array(1); 
$Security_SuperAdmin_Invoicing = array(1,45,53);
$Security_SuperAdmin_Residential = array(1,45,54);
$Security_SuperAdmin_Hours = array(1,45,55);
?>