<?php

//buffer cleared at bottom of script, look there for output order
ob_start();
$scrollId = "";
$oldEnvValue = $Config_environmentName;
$newEnvValue = $oldEnvValue;
$alertArray = array();

//check to see if file is submitted
if (isset($_POST['submitForm'])){
    include_once($dbProviderFolder."Helper.php");
	//echo "hi";
	//Loop through all the posted form info to build field names
	foreach ($_POST as $key => $val) {
        $sqlString = "UPDATE configs SET Config_DefaultValue = ? WHERE Config_Name = ?";
        $sqlBindings = "ss";
        $sqlParams = array($val, $key);
        $updateResult = MySqliHelper::execute($mysqli, $sqlString, $sqlBindings, $sqlParams);
		//echo $SQL_Update;
		$SQLUpdate = mysqli_query($SQLUpdate, $SQL_Update);
		if ($updateResult){
            $$key = $val; // set the global config value
            $newEnvValue = $Config_environmentName; // set environment name if it has changed
			$alertArray[$key] = "Configuration has been updated.";
			if (count($alertArray) == 1){
				$scrollId = "form-".$key;
			}
		}
	}
}
$SQL_Configs = "SELECT * FROM configs ORDER BY Config_OrderID";
$result_Configs = mysqli_query($result_Configs, $SQL_Configs);
$c = 0;
while ($c < mysqli_num_rows($result_Configs)){
	mysqli_data_seek($result_Configs,$c);
	$fetch = mysqli_fetch_array($result_Configs);
	$ConfigsArray[mysql_result($result_Configs,$c,"Config_Name")]["DefaultValue"] = $fetch["Config_DefaultValue"];
	mysqli_data_seek($result_Configs,$c);
	$fetch = mysqli_fetch_array($result_Configs);
	$ConfigsArray[mysql_result($result_Configs,$c,"Config_Name")]["Description"] = $fetch["Config_Description"];
	mysqli_data_seek($result_Configs,$c);
	$fetch = mysqli_fetch_array($result_Configs);
	$ConfigsArray[mysql_result($result_Configs,$c,"Config_Name")]["Type"] = $fetch["Config_Type"];
	mysqli_data_seek($result_Configs,$c);
	$fetch = mysqli_fetch_array($result_Configs);
	$ConfigsArray[mysql_result($result_Configs,$c,"Config_Name")]["Options"] = $fetch["Config_Options"];
	$c++;
}
?>

<h2>Site Config Values</h2>

<p class="info">These configuration keys (and values) are defined in MySQL, not in the codebase. They can be found in the table <code>configs</code> of the database <code>cet_admin</code>.</p>
<div id="config-forms"><?php

	foreach ($ConfigsArray as $key => $object) {
		$configType = $object["Type"];
		$defaultValue = $object["DefaultValue"];
		$configDesc = $object["Description"];
		$rawOptions = $object["Options"];
		$submitType = $configType == "select" ? "hidden" : "submit";
		$alertValue = isset($alertArray[$key]) ? $alertArray[$key] : ""; ?>

	<form method="POST" id="form-<?php echo $key;?>">
		<fieldset>
			<label>
				<?= $key; ?><br />
				<span class="info"><?php echo $configDesc;?></span><?php

				if ($configType == "select") {
					$allOptions = explode(",", $rawOptions); ?>

				<select name="<?php echo $key;?>"><?php

					foreach ($allOptions as $option){
						$optionArray = explode("|",$option);
						$optionValue = $optionArray[0];
						$optionText = count($optionArray) > 1 ? $optionArray[1] : $optionValue;
						$selectedText = ($optionValue == $defaultValue) ? " selected" : ""; ?>

					<option value="<?php echo $optionValue;?>" <?php echo $selectedText;?>><?php echo $optionText;?></option><?php

					} ?>

				</select><?php

				} else if ($configType == "text" || $configType == "password") { ?>

				<input type="<?php echo $configType;?>" name="<?php echo $key;?>" value="<?php echo $defaultValue;?>"><?php

				} else if ($configType == "textarea") { ?>

				<textarea cols="80" rows="6" name="<?php echo $key;?>"><?php echo $defaultValue;?></textarea><?php

				} else {
					echo $key." doesn't have a type?"; var_dump($object);
				} ?>

			</label>
			<input type="<?php echo $submitType; ?>" name="submitForm" value="Change">
			<div class="alert"><?php echo $alertValue;?></div>
		</fieldset>
	</form>
<?php } // end foreach ?>
</div>
<script type="text/javascript">
	$(function(){
		var formsDiv = $("#config-forms"),
            scrollElement = $("#<?php echo $scrollId;?>"),
            oldEnvName = '<?php echo $oldEnvValue;?>',
            newEnvName = '<?php echo $newEnvValue;?>';

		formsDiv.on("focus", "input,textarea,select", function(){
			$(this).closest("form").find(".alert").html("");
		});
		formsDiv.on("change", "select", function(){
			var form = $(this).closest("form");
			form.find(".alert").html("Processing...");
			form.submit();
		});

        if (scrollElement.length){
            setTimeout(function(){
                var bodyElement = $("body");
                if (oldEnvName !== newEnvName){
                    bodyElement.removeClass(oldEnvName).addClass(newEnvName);
                }
                $("#tab-SiteConfig").click();
                $('html, body').animate({
                    scrollTop: scrollElement.offset().top - 20
                }, 500);
            }, 500)
        }
	});
</script>

<?php
$siteConfigDisplay = ob_get_contents();
ob_clean();
include_once('_deploy.php');
echo $siteConfigDisplay;
?>