<?php
/*THIS FILE IS NOT USED BUT WAS TESTED TO DETERMINE METHODS FOR EXTRACTING DATA FROM CSG EXTRACT FILES TO GET P&L DATA*/

include_once($dbProviderFolder."CSGDataProvider.php");
$csgDataProvider = new CSGDataProvider($dataConn);
$criteria = new stdClass();
//$extractResults = $csgDataProvider->getExtractData();
foreach ($extractResults as $extract){
	$YearMonth = date("Y-m-01",strtotime($extract->Date));
	$Description = strtoupper(trim($extract->Description));
	$qty = $extract->Qty;
	$price = $extract->Programprice;
	$progCode = strtoupper($extract->Progcode);
	switch($progCode){
		case "416":
			if ($Description == "HEA VISIT"){
				$HEAVisits[$YearMonth][$extract->Invcode] = $HEAVisits[$YearMonth][$extract->Invcode]+1;
			}else{
				$ISMS[$YearMonth][$extract->Invcode] = $ISMS[$YearMonth][$extract->Invcode]+($price);
				$ISMSProgramPrice = $ISMSProgramPrice+($extract->Programprice);
			}
			break;
		case "416S":
			if ($Description == "SPECIAL HOME VISIT"){
				$SHVisits[$YearMonth][$extract->Invcode] = $SHVisits[$YearMonth][$extract->Invcode]+1;
			}else{
				$ISMS[$YearMonth][$extract->Invcode] = $ISMS[$YearMonth][$extract->Invcode]+($price);
				$ISMSProgramPrice = $ISMSProgramPrice+($extract->Programprice);
			}
			break;
		case "418":
			$ISMS[$YearMonth][$extract->Invcode] = $ISMS[$YearMonth][$extract->Invcode]+($price);
			$ISMSProgramPrice = $ISMSProgramPrice+($extract->Programprice);
			break;
		case "418B":
			$ISMS[$YearMonth][$extract->Invcode] = $ISMS[$YearMonth][$extract->Invcode]+($price);
			$ISMSProgramPrice = $ISMSProgramPrice+($extract->Programprice);
			break;
		case "413":
			if ($Description == "AIR SEALING ONLY COMPLETION FEE"){
				$IIC["IICFeesAirSealingOnly_NATGRID"][$YearMonth][$extract->Invcode] = $IIC["IICFeesAirSealingOnly_NATGRID"][$YearMonth][$extract->Invcode]+1;
			}
			if ($Description == "HPC WEATHERIZATION COMPLETION FEE" || $Description == "IIC WEATHERIZATION COMPLETION FEE"){
				$IIC["IICCompletionFees_NATGRID"][$YearMonth][$extract->Invcode] = $IIC["IICCompletionFees_NATGRID"][$YearMonth][$extract->Invcode]+1;
			}
			if ($Description == "COMPLETIONFEE"){
				$IIC["IICContractManagementFee_WMECO"][$YearMonth][$extract->Invcode] = $IIC["IICContractManagementFee_WMECO"][$YearMonth][$extract->Invcode]+1;
			}
			if ($Description == "INSPECTION FEE"){
				$Inspections["InspectionFee_NATGRID"][$YearMonth][$extract->Invcode] = $Inspections["InspectionFee_NATGRID"][$YearMonth][$extract->Invcode]+1;
			}
			break;
		default:
//			echo $progCode."<br>";
			break;
	
	}
}

$extractHubActionResults = $csgDataProvider->getHUBBGExtractData();
//print_pre($extractHubActionResults);
foreach ($extractHubActionResults as $extract){
	$YearMonth = date("Y-m-01",strtotime($extract->INSTALL_DT));
	$Description = strtoupper(trim($extract->DESCRIPTION));
	$qty = $extract->INS_QTY;
	$utilPrice = $extract->UTIL_PRICE;
	$custPrice = $extract->CUST_PRICE;
	$measStatus = strtoupper($extract->MEAS_STATUS);

	$Descriptions[$Description]=1;
	switch($Description){
		case "HEA VISIT":
			$HEAVisits[$YearMonth]["BGAS"] = $HEAVisits[$YearMonth]["BGAS"]+1;
			break;
		case "SPECIAL HOME VISIT":
			$SHVisits[$YearMonth]["BGAS"] = $SHVisits[$YearMonth]["BGAS"]+1;
			break;
		case "HEA ADDITIONAL UNIT VISIT":
			$HEAAUVisits[$YearMonth]["BGAS"] = $HEAAUVisits[$YearMonth]["BGAS"]+1;
			break;
		default:
			if ($Description == "EARTH SHOWERHEAD (1.7GPM)" || $Description == "FLIP AERATOR (2.2GPM)" || $Description == "STANDARD AERATOR (1.5GPM)" || $Description == "THERMOSTAT: HONEYWELL"){
				$ISMS[$YearMonth]["BGAS"] = $ISMS[$YearMonth]["BGAS"]+($utilPrice*$qty);
			}
			break;
	}
	if ($measStatus == "BILLING"){
		$IICBGAS[$YearMonth]["BGAS"] = ($IICBGAS[$YearMonth]["BGAS"]+(($custPrice+$utilPrice)*(($forecastingVariables->IICWeatherizationFeeBGAS)/100)));
//		$IICBGAS[$YearMonth]["BGAS"] = ($IICBGAS[$YearMonth]["BGAS"]+(($custPrice+$utilPrice)));
	}
}

$extractHubScheduleReportResults = $csgDataProvider->getHUBScheduleReportData();
//print_pre($extractHubScheduleReportResults);
foreach ($extractHubScheduleReportResults as $extract){
	$YearMonth = date("Y-m-01",strtotime($extract["Appt Date"]));
	$ApptType = strtoupper(trim($extract["Appt Type"]));
	$ProgramID = strtoupper(trim($extract["Program ID"]));
	switch($ProgramID){
		case "NGRID":
			$Program = "NATGRID";
			if ($ApptType == "MECHANICAL REBATE VERIFICATION"){
				$Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program] = $Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program]+1;
			}
			break;
		case "BERKSHIRE GAS":
			$Program = "BGAS";
			if ($ApptType == "GAS INSPECTION"){
				$Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program]+1;
			}
			if ($ApptType == "ADDITIONAL UNIT GAS INSPECTION"){
				$Inspections["InspectionFeeAddUnit_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFeeAddUnit_BGAS"][$YearMonth][$Program]+1;
			}
			if (strpos(" ".$ApptType,'INSPECTION') && !strpos(" ".$ApptType,'LOAN INSPECTION') && !strpos(" ".$ApptType,'GAS INSPECTION')){
				$Inspections["InspectionFee_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFee_BGAS"][$YearMonth][$Program]+1;
			}
			if ($ApptType == "MECHANICAL REBATE VERIFICATION"){
				$Inspections["MechanicalRebateInspection_NATGRID"][$YearMonth]["NATGRID"] = $Inspections["MechanicalRebateInspection_NATGRID"][$YearMonth]["NATGRID"]+1;
			}		
			break;
		case "WESTERN MASSACHUSETTS ELECTRIC":
			$Program = "WMECO";
			if (strpos(" ".$ApptType,'INSPECTION') && !strpos(" ".$ApptType,'LOAN INSPECTION') && !strpos(" ".$ApptType,'GAS INSPECTION')){
				$Inspections["InspectionFee_WMECO"][$YearMonth][$Program] = $Inspections["InspectionFee_WMECO"][$YearMonth][$Program]+1;
			}
			if ($ApptType == "MECHANICAL REBATE VERIFICATION"){
				$Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program] = $Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program]+1;
			}
			break;
	}
}

//print_pre($HEAVisits);
foreach ($HEAVisits as $YearMonth=>$Utility){
	$Revenues[$YearMonth]["HEA"] = (($forecastingVariables->HEAAssessmentFeeNATGRID*$Utility["NATGRID"])+($forecastingVariables->HEAAssessmentFeeWMECO*$Utility["WMECO"])+($forecastingVariables->HEAAssessmentFeeBGAS*$Utility["BGAS"]));
	$RevenueTotals[$YearMonth]["HEA Fees"] = $RevenueTotals[$YearMonth]["HEA Fees"]+$Revenues[$YearMonth]["HEA"];
}
foreach ($SHVisits as $YearMonth=>$Utility){
	$Revenues[$YearMonth]["SHV"] = (($forecastingVariables->SHVAssessmentFeeNATGRID*$Utility["NATGRID"])+($forecastingVariables->SHVAssessmentFeeWMECO*$Utility["WMECO"])+($forecastingVariables->SHVAssessmentFeeBGAS*$Utility["BGAS"]));
	$RevenueTotals[$YearMonth]["HEA Fees"] = $RevenueTotals[$YearMonth]["HEA Fees"]+$Revenues[$YearMonth]["SHV"]+$Revenues[$YearMonth]["HEAAU"];
}
foreach ($HEAAUVisits as $YearMonth=>$Utility){
	$Revenues[$YearMonth]["HEAAU"] = (($forecastingVariables->HEAAUAssessmentFeeBGAS*$Utility["BGAS"]));
	$RevenueTotals[$YearMonth]["HEA Fees"] = $RevenueTotals[$YearMonth]["HEA Fees"]+$Revenues[$YearMonth]["HEAAU"];
}
foreach ($ISMS as $YearMonth=>$Utility){
	$Revenues[$YearMonth]["ISMS"] = (($Utility["NATGRID"])+($Utility["WMECO"])+($Utility["BGAS"]));
	$RevenueTotals[$YearMonth]["ISMS"] = $Revenues[$YearMonth]["ISMS"];
}
//print_pre($IIC);
foreach ($IIC as $ForecstingVar=>$YearMonthData){
	foreach ($YearMonthData as $YearMonth=>$Utility){
		$ForecstingVarArray = explode("_",$ForecstingVar);
		$ForecstingVarName=str_replace("_","",$ForecstingVar);
		$Revenues[$YearMonth][$ForecstingVarArray[0]] = (($forecastingVariables->$ForecstingVarName*$Utility[$ForecstingVarArray[1]]));
		$RevenueTotals[$YearMonth]["IIC Contract Fees"]	= ($RevenueTotals[$YearMonth]["IIC Contract Fees"]+$Revenues[$YearMonth][$ForecstingVarArray[0]]);
	}
}
//print_pre($IICBGAS);
foreach($IICBGAS as $YearMonth=>$Utility){
	$Revenues[$YearMonth]["IICWeatherizationFeeBGAS"] = $Utility["BGAS"]+4000;
	$RevenueTotals[$YearMonth]["IIC Contract Fees"]	= (floor(($RevenueTotals[$YearMonth]["IIC Contract Fees"]+$Utility["BGAS"])*100)/100)+4000;
}
//print_pre($Inspections);
foreach ($Inspections as $ForecstingVar=>$YearMonthData){
	foreach ($YearMonthData as $YearMonth=>$Utility){
		$ForecstingVarArray = explode("_",$ForecstingVar);
		$ForecstingVarName=str_replace("_","",$ForecstingVar);
		$Revenues[$YearMonth][$ForecstingVarArray[0]] = ($Revenues1[$YearMonth][$ForecstingVarArray[0]]+($forecastingVariables->$ForecstingVarName*$Utility[$ForecstingVarArray[1]]));
		$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$Revenues[$YearMonth][$ForecstingVarArray[0]]);
	}
}
//print_pre($Revenues);
//print_pre($RevenueTotals);
echo "Other Revenue Fudge Factor=$".money($forecastingVariables->OtherRevenueFudgeFactor)."<br>";
foreach ($RevenueTotals as $YearMonth=>$RevenueType){
	//echo date("M Y", strtotime($YearMonth)).": ";
	$TotalAmount = $forecastingVariables->OtherRevenueFudgeFactor;
	foreach ($RevenueType as $Total){
		$TotalAmount = $TotalAmount+$Total;
	}
	$asOfDate = strtotime(date("Y-m-12", strtotime($YearMonth)))*1000;
	//echo "$".money($TotalAmount)."<br>";
	$RawRevenue[]=array($asOfDate,(int)$TotalAmount);
}
?>