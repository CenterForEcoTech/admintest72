<?php
	if (!$IncludeRevenueForecast){
?>
	<?php
	$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/1/Y",strtotime($TodaysDate)));
	$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime($TodaysDate." +1 month")));
	$ExtrapolatedDataCurrentMonth = ($_GET['includeExtrapolatedDataCurrentMonth'] ? " checked" : "");
	$ExtrapolatedDataFutureMonth=($_GET['includeExtrapolatedDataFutureMonth'] ? " checked" : "");
	$OtherRevenueCurrentMonth = ($_GET['otherRevenueCurrentMonth'] ? $_GET['otherRevenueCurrentMonth'] : "10000");
	$OtherRevenueFutureMonth=($_GET['otherRevenueFutureMonth'] ? $_GET['otherRevenueFutureMonth'] : "10000");
	?>
	<style>
	legend {font-weight:bold;}
	</style>
	<div class="fourteen columns">
		<div class="five columns"><h3>Forecast Revenue Data</h3></div>
		<br clear="all">
		<div class="eight columns">
			<div class="three columns">Date Range:</div>
			<div class="four columns">
				<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
				<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date"><br>
			</div>
		</div>
		<div class="nine columns">
			<div class="three columns">Extrapolated Data:</div>
			<div class="five columns">
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataCurrentMonth"<?php echo $ExtrapolatedDataCurrentMonth;?>><?php echo date("F",strtotime($StartDate));?>&nbsp;&nbsp;
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataFutureMonth"<?php echo $ExtrapolatedDataFutureMonth;?>>Future months<br>
			</div>
		</div>
	</div>
	
	<script>
		$(document).ready(function() {
			
			var dateClass = $(".date");
			dateClass.width(80);
			$("#StartDate").datepicker({
				maxDate:0,
				onSelect: function (date) {
					var date2 = $('#StartDate').datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$('#EndDate').datepicker('setDate', date2);
					//sets minDate to dt1 date + 1
					$('#EndDate').datepicker('option', 'minDate', date2);
					updateFilters();
				}
			});
			$('#EndDate').datepicker({
				onClose: function () {
					var dt1 = $('#StartDate').datepicker('getDate');
					var dt2 = $('#EndDate').datepicker('getDate');
					//check to prevent a user from entering a date below date of dt1
					if (dt2 <= dt1) {
						var minDate = $('#EndDate').datepicker('option', 'minDate');
						$('#EndDate').datepicker('setDate', minDate);
					}
					updateFilters();
				}
			});		
			
			$(".FilterParam").on("click blur",function(){
				updateFilters();
			});
			
			var getFilters = function(){
				var startDate = $("#StartDate").val(),
					endDate = $("#EndDate").val(),
					includeExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
					includeExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0),
					filterValues = new Array(startDate,endDate,includeExtrapolatedDataCurrentMonth,includeExtrapolatedDataFutureMonth);
					
					return filterValues;
			};
			
			var updateFilters = function(){
				var Filters=getFilters(),
					Params = "StartDate="+Filters[0]+
							"&EndDate="+Filters[1]+
							"&includeExtrapolatedDataCurrentMonth="+Filters[2]+
							"&includeExtrapolatedDataFutureMonth="+Filters[3];
				window.location.href = "<?php echo $CurrentServer.$adminFolder;?>forecast/?"+Params+"&nav=<?php echo $navDropDown;?>#";
			}
		});
	</script>
<?php }?>
	<fieldset>
		<legend>Contract Revenue Data for Current and Next Month</legend>
		<div class="fifteen columns"> 
			<div id="ContractSummary"><img src="<?php echo $CurrentServer."images/ajaxLoader.gif";?>"> Loading Results</div>
			<div id="ContractDetails"></div>
		</div>
		<div class="fifteen columns">
			<fieldset>
				<legend>Data Source</legend>
				Uses Sharepoint Data for Installed/Billed dates and count/amounts.<br>
				Amount shown is revenue generated, not contract amounts.<Br>
				Contracts that are Billed in this month are included.<br>
				Contracts that have not yet billed but should bill based on days to bill from contractor are also included.<br>
			</fieldset>
			<fieldset>
				<legend>Extrapolated Data</legend>
				Current month will combine Billed and Not Yet Billed amount but install scheduled for current month.<br>
				Expected Total contract amounts from ES Expectations will be used if Total is less than expected amount.<br>
				NOTE: Future months always combines Billed and Not Yet Billed amounts.<br>
			</fieldset>
		</div>
		<div class="fifteen columns" id="ContractFormulas"></div>
		<div class="fifteen columns" id="ContractNotes"></div>
	</fieldset>
<script>
	$(document).ready(function() {
		function formatMoney(x) {
			return "$"+x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		var ExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
			ExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0);
		$.ajax({
			url: "<?php echo $CurrentServer.$adminFolder."sharepoint/GetSharePointDataAPI.php";?>",
			type: "GET",
			data: {
					action:"ContractsToComplete",
					startDate:"<?php echo $StartDate;?>",
					endDate:"<?php echo $EndDate;?>",
					includeExtrapolatedDataCurrentMonth: ExtrapolatedDataCurrentMonth,
					includeExtrapolatedDataFutureMonth: ExtrapolatedDataFutureMonth
			},
			success: function(data){
				//console.log(data);
				var firstMonth = new Date("<?php echo date("Y,m,d", strtotime($StartDate));?>");
				var details = "";
				$.each(data.Contracts,function(YearMonth,YearMonthData){
					var YearMonthParts = YearMonth.split("-");
					var splitYear = YearMonthParts[0];
					var splitMonth = YearMonthParts[1];
					var thisYearMonth = splitYear+","+splitMonth+",01";
					var thisMonth = new Date(thisYearMonth);
					var amount = 0;
					var Amount = 0;
					details = details + "<fieldset><legend>"+YearMonth+"-01</legend>";
					console.log(YearMonthData);
					$.each(YearMonthData,function(Utility,UtilityData){
						console.log(Utility);
						console.log(UtilityData);
						//Only check first month...all other months must use combined information
						var RevType = "#"+YearMonth+"-01"+"Contracts";
						if (firstMonth.getTime()==thisMonth.getTime()){
							if (Utility == "Total"){
								var totalAmount = formatMoney(UtilityData["Totals"].toFixed(0));
								if (UtilityData["Billed"]){
									var billedAmount = formatMoney(UtilityData["Billed"].toFixed(0));
									var BilledAmount = UtilityData["Billed"].toFixed(0);
								}else{
									var billedAmount = formatMoney(0);
									var BilledAmount = 0;
								}
								var TotalAmount = UtilityData["Totals"].toFixed(0);
							}else{
								//build details
								DisplayUtility = (Utility == "WMECO" ? "ESOURCE" : Utility);
								details = details + "<fieldset><legend>"+DisplayUtility+"</legend>";
								$.each(UtilityData,function(ContractType,Amounts){
									var ContractTypes = ContractType.split("_");
									var Billed = (Amounts["Billed"] ? formatMoney(Amounts["Billed"].toFixed(0)) : 0);
									var Total = (Amounts["Total"] ? formatMoney(Amounts["Total"].toFixed(0)) : 0);
									var NotYetBilled = (Amounts["NotYetBilled"] ? formatMoney(Amounts["NotYetBilled"].toFixed(0)) : 0);
									details = details+"<fieldset><legend>"+ContractTypes[0]+"</legend>Total = "+Total+"<Br>Billed = "+Billed+"<br>NotYetBilled = "+NotYetBilled+"</fieldset>";
								});
								details = details+"</fieldset>";
							}
							if (ExtrapolatedDataCurrentMonth){
								amount=totalAmount;
								Amount=TotalAmount
							}else{
								amount=billedAmount;
								Amount=BilledAmount
							}
							$(RevType).html(amount);
							$(RevType).attr('data-amount',Amount);
						}else{
							if (Utility == "Total"){
								amount = formatMoney(UtilityData["Totals"].toFixed(0));
								Amount = UtilityData["Totals"].toFixed(0);
							}else{
								//build details
								DisplayUtility = (Utility == "WMECO" ? "ESOURCE" : Utility);
								details = details + "<fieldset><legend>"+DisplayUtility+"</legend>";
								$.each(UtilityData,function(ContractType,Amounts){
									var ContractTypes = ContractType.split("_");
									var Billed = (Amounts["Billed"] ? formatMoney(Amounts["Billed"].toFixed(0)) : 0);
									var NotYetBilled = (Amounts["NotYetBilled"] ? formatMoney(Amounts["NotYetBilled"].toFixed(0)) : 0);
									details = details+"<fieldset><legend>"+ContractTypes[0]+"</legend>Total = "+formatMoney(Amounts["Total"].toFixed(0))+"<Br>Billed = "+Billed+"<br>NotYetBilled = "+NotYetBilled+"</fieldset>";
								});
								details = details+"</fieldset>";
							}
							$(RevType).html(amount);
							$(RevType).attr('data-amount',Amount);
						}
					});
					details = details+"</fieldset>";
				});
				//$("#ContractSummary").html(data.responseText);
				$("#ContractSummary").html(details);
				var results = $(".results.contracts");
				results.each(function() {
					var $this = $(this),
						thisId = "#"+$this.attr('id'),
						YearMonth = $this.attr('data-yearmonth'),
						Amount = parseInt($this.attr('data-amount'));
					if ($(thisId).html()=="--"){
						$(thisId).html('&nbsp;');
						$(thisId).attr('data-amount',0);
					}
					var	Totals = "#"+YearMonth+"Totals",
						TotalsEl = $(Totals);
						totalsElAmount = parseInt(TotalsEl.attr('data-amount')),
						totalAmount = (totalsElAmount+Amount);
						TotalsEl.attr('data-amount',totalAmount); 
						if (TotalsEl.attr('data-amount') > 0){
							TotalsEl.html(formatMoney(TotalsEl.attr('data-amount'))); 
						}	
				});
				//display Formulas
				var FormulaDisplay = "";
				$.each(data.Formulas,function(FormulaCategory,FormulaInfo){
					FormulaDisplay += "<fieldset><legend>Formulas Affecting "+FormulaCategory+"</legend>";
					$.each(FormulaInfo,function(Type,Value){
						FormulaDisplay += "<div class=\"five columns formulaType\">"+Type+"</div>";
						FormulaDisplay += "<div class=\"nine columns formulaValue\">"+Value+"</div>";
					});
					FormulaDisplay += "</fieldset>";
				});
				$("#ContractFormulas").append(FormulaDisplay);
				
				//display Notes
				var NoteDisplay = "<fieldset><legend>Notes Affecting Contract Revenue</legend>";
				$.each(data.Notes,function(NoteItem,NoteText){
					NoteDisplay = NoteDisplay+"<div class=\"fifteen columns\">"+NoteText+".</div>";
				});
				NoteDisplay = NoteDisplay+"</fieldset>";
				$("#ContractNotes").append(NoteDisplay);

			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				console.log(message);
			}
		});
	});
</script>