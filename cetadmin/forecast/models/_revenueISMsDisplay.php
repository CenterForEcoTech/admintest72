<?php
	if (!$IncludeRevenueForecast){
?>
	<?php
	$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/1/Y",strtotime($TodaysDate)));
	$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime($TodaysDate." +1 month")));
	$ExtrapolatedDataCurrentMonth = ($_GET['includeExtrapolatedDataCurrentMonth'] ? " checked" : "");
	$ExtrapolatedDataFutureMonth=($_GET['includeExtrapolatedDataFutureMonth'] ? " checked" : "");
	$OtherRevenueCurrentMonth = ($_GET['otherRevenueCurrentMonth'] ? $_GET['otherRevenueCurrentMonth'] : "10000");
	$OtherRevenueFutureMonth=($_GET['otherRevenueFutureMonth'] ? $_GET['otherRevenueFutureMonth'] : "10000");
	?>
	<style>
	legend {font-weight:bold;}
	</style>
	<div class="fourteen columns">
		<div class="five columns"><h3>Forecast Revenue Data</h3></div>
		<br clear="all">
		<div class="eight columns">
			<div class="three columns">Date Range:</div>
			<div class="four columns">
				<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
				<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date"><br>
			</div>
		</div>
		<div class="nine columns">
			<div class="three columns">Extrapolated Data:</div>
			<div class="five columns">
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataCurrentMonth"<?php echo $ExtrapolatedDataCurrentMonth;?>><?php echo date("F",strtotime($StartDate));?>&nbsp;&nbsp;
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataFutureMonth"<?php echo $ExtrapolatedDataFutureMonth;?>>Future months<br>
			</div>
		</div>
	</div>
	
	<script>
		$(document).ready(function() {
			
			var dateClass = $(".date");
			dateClass.width(80);
			$("#StartDate").datepicker({
				maxDate:0,
				onSelect: function (date) {
					var date2 = $('#StartDate').datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$('#EndDate').datepicker('setDate', date2);
					//sets minDate to dt1 date + 1
					$('#EndDate').datepicker('option', 'minDate', date2);
					updateFilters();
				}
			});
			$('#EndDate').datepicker({
				onClose: function () {
					var dt1 = $('#StartDate').datepicker('getDate');
					var dt2 = $('#EndDate').datepicker('getDate');
					//check to prevent a user from entering a date below date of dt1
					if (dt2 <= dt1) {
						var minDate = $('#EndDate').datepicker('option', 'minDate');
						$('#EndDate').datepicker('setDate', minDate);
					}
					updateFilters();
				}
			});		
			
			$(".FilterParam").on("click blur",function(){
				updateFilters();
			});
			
			var getFilters = function(){
				var startDate = $("#StartDate").val(),
					endDate = $("#EndDate").val(),
					includeExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
					includeExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0),
					filterValues = new Array(startDate,endDate,includeExtrapolatedDataCurrentMonth,includeExtrapolatedDataFutureMonth);
					
					return filterValues;
			};
			
			var updateFilters = function(){
				var Filters=getFilters(),
					Params = "StartDate="+Filters[0]+
							"&EndDate="+Filters[1]+
							"&includeExtrapolatedDataCurrentMonth="+Filters[2]+
							"&includeExtrapolatedDataFutureMonth="+Filters[3];
				window.location.href = "<?php echo $CurrentServer.$adminFolder;?>forecast/?"+Params+"&nav=<?php echo $navDropDown;?>#";
			}
		});
	</script>
<?php }?>
	<fieldset>
		<legend>ISM Revenue Data for Current and Next Month</legend>
		<div class="fifteen columns"> 
			<div id="ISMsSummary"><img src="<?php echo $CurrentServer."images/ajaxLoader.gif";?>"> Loading Results</div>
			<div id="ISMsDetails"></div>
		</div>
		<div class="fifteen columns">
			<fieldset>
				<legend>Data Source</legend>
				Uses scheduled data from HUB and ISM install history from BulbReport.
			</fieldset>		
			<fieldset>
				<legend>Extrapolation Data</legend>
				Uses higher of actual scheduled visits or expected visits per month per Energy Specialist.
			</fieldset>		
		</div>
		<div class="fifteen columns" id="ISMFormulas"></div>
		<div class="fifteen columns" id="ISMNotes"></div>
	</fieldset>
<script>
	$(document).ready(function() {
		function formatMoney(x) {
			return "$"+x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		var ExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
			ExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0);
		$.ajax({
			url: "<?php echo $CurrentServer.$adminFolder."forecast/ApiRevenue.php";?>",
			type: "GET",
			data: {
					action:"ISMs",
					startDate:"<?php echo $StartDate;?>",
					endDate:"<?php echo $EndDate;?>",
					includeExtrapolatedDataCurrentMonth: ExtrapolatedDataCurrentMonth,
					includeExtrapolatedDataFutureMonth: ExtrapolatedDataFutureMonth
				},
			success: function(data){
				//$("#ISMsSummary").html(data.responseText);
				var details ="";
				$.each(data.ISMRevenue, function(YearMonth,YearMonthData){
					details = details +"<fieldset><legend>"+YearMonth+"</legend>ISM Count:<br>";
					$.each(YearMonthData, function(ISM,Amount){
						details = details + ISM+'='+Amount+'<br>'
					});
					details = details + '<br>COGS='+(formatMoney(data.COGS[YearMonth]))+'<br>'
					details = details+"</fieldset>";
				});
				$.each(data.ISMRevenueTotal, function(YearMonth,Amount){
					RevType = "#"+YearMonth+"ISMs";
					$(RevType).html(formatMoney(Amount.toFixed(0)));
					$(RevType).attr('data-amount',Amount.toFixed(0));
				});
				$.each(data.COGS, function(YearMonth,Amount){
					RevType = "#"+YearMonth+"COGS";
					$(RevType).html(formatMoney(Amount.toFixed(0)));
					$(RevType).attr('data-amount',Amount.toFixed(0));
				});
				$("#ISMsSummary").html(details);
				var results = $(".results.isms");
				results.each(function() {
					var $this = $(this),
						thisId = "#"+$this.attr('id'),
						YearMonth = $this.attr('data-yearmonth'),
						Amount = parseInt($this.attr('data-amount'));
					if ($(thisId).html()=="--"){
						$(thisId).html('&nbsp;');
						$(thisId).attr('data-amount',0);
					}
					var	Totals = "#"+YearMonth+"Totals",
						TotalsEl = $(Totals);
						totalsElAmount = parseInt(TotalsEl.attr('data-amount')),
						totalAmount = (totalsElAmount+Amount);
						TotalsEl.attr('data-amount',totalAmount); 
						if (TotalsEl.attr('data-amount') > 0){
							TotalsEl.html(formatMoney(TotalsEl.attr('data-amount'))); 
						}	
				});
				//display Formulas
				var FormulaDisplay = "";
				$.each(data.Formulas,function(FormulaCategory,FormulaInfo){
					FormulaDisplay += "<fieldset><legend>Formulas Affecting "+FormulaCategory+"</legend>";
					$.each(FormulaInfo,function(Type,Value){
						FormulaDisplay += "<div class=\"five columns formulaType\">"+Type+"</div>";
						FormulaDisplay += "<div class=\"nine columns formulaValue\">"+Value+"</div>";
					});
					FormulaDisplay += "</fieldset>";
				});
				$("#ISMFormulas").append(FormulaDisplay);
				
				//display Notes
				var NoteDisplay = "<fieldset><legend>Notes Affecting HEA/Inspection Fee Revenue</legend>";
				$.each(data.Notes,function(NoteItem,NoteText){
					NoteDisplay = NoteDisplay+"<div class=\"fifteen columns\">"+NoteText+".</div>";
				});
				NoteDisplay = NoteDisplay+"</fieldset>";
				$("#ISMNotes").append(NoteDisplay);
				
			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				console.log(message);
			}
		});
	});
</script>