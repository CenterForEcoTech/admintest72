<?php
error_reporting(1);
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 600);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CETDataProvider.php");
include_once($dbProviderFolder."CSGDailyDataProvider.php");
include_once($dbProviderFolder."WarehouseProvider.php");
include_once($dbProviderFolder."ProductProvider.php");
include_once($dbProviderFolder."ForecastingProvider.php");

$TotalDays = DateDiff($StartDate,$EndDate);

$productProvider = new ProductProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;

foreach ($resultArray as $result=>$record){
	//echo $result;
	$ProductsByEFI[$record->efi] = $record;
	$ProductsByDescription[$record->description] = $record->efi;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}
}

$warehouseProvider = new WarehouseProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$NameParts = explode(" ",$record->description);
	$ShortName = $NameParts[0]." ".substr($NameParts[1],0,1);
	$WarehouseByName[$record->name] = $ShortName;
	$WarehouseByShortName[$ShortName] = $record->name;
	if ($record->displayOrderId){
		$ActiveWarehouseByName[]=$ShortName;
		$ActiveWarehouseByID[]=strtoupper($record->name);
	}
}

$forecastingProvider = new ForecastingProvider($dataConn);
$criteria = new stdClass();
$criteria->date = date("Y-m-d");
$lastAverages = $forecastingProvider->lastAveragesES($criteria);
if (count($lastAverages)){
	//Used stored averages so don't have to rerun calculations
	$donotAddLastAverages = true;
	foreach  ($lastAverages as $crewAvg){
		$crew = $crewAvg["crew"];
		//echo $crew."<br>";
		if (in_array($crew,$ActiveWarehouseByID)){
			$ESPrefix = ($crew{strlen($crew)-3}=="1" ? "XX" : "").$crew;
			$crew = $ESPrefix."QQQ".$WarehouseByName[$crew];
			$type = $crewAvg["type"];
			if (strpos(" ".$type,"Revenue") || strpos(" ".$type,"COGS")){
				$type = $type;
			}else{
				$type = ($type != 'Bulbs' ? $ProductsByEFI[$crewAvg["type"]]->description : $crewAvg["type"]);
			}
			$qty = $crewAvg["qty"];
			$CrewISMTotalsAverage[$crew][$type] = $qty;		
			$ISMS[$type] = 1;
		}
	}
}else{

	$csgDailyDataProvider = new CSGDailyDataProvider($dataConn);
	$ThreeMonthsAgo = date("m/01/Y",strtotime($TodaysDate." -4 months"));
	$LastMonth = date("m/01/Y",strtotime($TodaysDate." -1 months"));
	$efiQtyByCrew = $csgDailyDataProvider->getEFIQtyByCrew($ThreeMonthsAgo ,$LastMonth);
	$resultsCollection = $efiQtyByCrew->collection;
	//echo "here";
	//print_pre($resultsCollection);
	$nonbulbs = array('3000.161','3010.020','3010.100','5000.229','7005.609');
	foreach ($resultsCollection as $data){
		$siteId = $data->siteId;
		$crew = strtoupper($data->crew);
		$auditorName = $data->auditor_name;
		$installDate = $data->installed_dt;
		$YearMonth = date("Y-m-01",strtotime($installDate));
		$efi = $data->efiPart;
		$partId = $data->partId;
		$qty = $data->qty;
		if ($efi == "3010.10"){$efi = "3010.100";}
		if ($efi == "3010.02"){$efi = "3010.020";}
		$description = ($ProductsByEFI[$efi]->description ? $ProductsByEFI[$efi]->description : $efi);
		$type = (in_array($efi,$nonbulbs) ? $description : "Bulbs");
		//echo $crew."<br>";
		if (in_array($crew,$ActiveWarehouseByID)){
			$ESPrefix = ($crew{strlen($crew)-3}=="1" ? "XX" : "").$crew;
			$crew = $ESPrefix."QQQ".$WarehouseByName[$crew];
			//get the number of unique sites visited
				$CrewSites[$crew]["site"][] = $siteId;
				$SiteCount[$crew] = array_unique($CrewSites[$crew]["site"],SORT_STRING);
				//re-key array for aesthetics
				$SiteCount[$crew] = array_values($SiteCount[$crew]);
				
			$crewSiteCount[$crew] = count($SiteCount[$crew]);
	//		$CrewISMsByPartId[$crew][$type][$partId] = $CrewBulbsByPartId[$crew][$type][$partId]+$qty;
	//		$CrewISMsByEfi[$crew][$type][$efi] = $CrewBulbsByPartId[$crew][$type][$efi]+$qty;
			$CrewISMTotals[$crew][$type] = $CrewISMTotals[$crew][$type]+$qty;
			$CrewISMTotalsAverage[$crew][$type] = round(($CrewISMTotals[$crew][$type]/$crewSiteCount[$crew]),0,PHP_ROUND_HALF_UP);
			$ISMS[$type] = 1;
			
			$cogs = bcmul($qty,$ProductsByEFI[$efi]->cost,2);
			$retail = bcmul($qty,$ProductsByEFI[$efi]->retail,2);

			$CrewISMTotals[$crew]["CogsTotal"] = $CrewISMTotals[$crew]["CogsTotal"]+$cogs;
			$CrewISMTotals[$crew]["CogsDetails"][$YearMonth] = $CrewISMTotals[$crew]["CogsDetails"][$YearMonth]+$cogs;
			$CrewISMTotals[$crew]["CogsAverage"] = ceil(array_sum($CrewISMTotals[$crew]["CogsDetails"])/count($CrewISMTotals[$crew]["CogsDetails"]));
			$CrewISMTotalsAverage[$crew]["COGSPerMonth"] = ceil(array_sum($CrewISMTotals[$crew]["CogsDetails"])/count($CrewISMTotals[$crew]["CogsDetails"]));
			$CrewISMTotalsAverage[$crew]["COGSPerVisit"] = round(($CrewISMTotals[$crew]["CogsTotal"]/$crewSiteCount[$crew]),0,PHP_ROUND_HALF_UP);;

			$CrewISMTotals[$crew]["RevenueTotal"] = $CrewISMTotals[$crew]["RevenueTotal"]+$retail;
			$CrewISMTotals[$crew]["RevenueDetails"][$YearMonth] = $CrewISMTotals[$crew]["RevenueDetails"][$YearMonth]+$retail;
			$CrewISMTotals[$crew]["RevenueAverage"] = ceil(array_sum($CrewISMTotals[$crew]["RevenueDetails"])/count($CrewISMTotals[$crew]["RevenueDetails"]));
			$CrewISMTotalsAverage[$crew]["RevenuePerMonth"] = ceil(array_sum($CrewISMTotals[$crew]["RevenueDetails"])/count($CrewISMTotals[$crew]["RevenueDetails"]));
			$CrewISMTotalsAverage[$crew]["RevenuePerVisit"] = round(($CrewISMTotals[$crew]["RevenueTotal"]/$crewSiteCount[$crew]),0,PHP_ROUND_HALF_UP);;
			
		}
	}
	$ISMS["RevenuePerVisit"] = 1;
	$ISMS["RevenuePerMonth"] = 1;
	$ISMS["COGSPerVisit"] = 1;
	$ISMS["COGSPerMonth"] = 1;
}
ksort($CrewISMTotalsAverage);
$path = ($path ? $path : $siteRoot.$adminFolder);
include_once($path.'sharepoint/SharePointAPIFiles.php');

$dateEvents = array("AirSealing","Insulation");

$ignoreContractor = array("Ron Desellier Electric");
$ignoreES = array("HP","Rtl_");
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$SPRecordsCount = $SPRecordsCount+count(${$ArrayName});
	foreach (${$ArrayName} as $SPData){
	//	$ES = trim($SPData["Energy Specialist"]);
		$ES = trim($SPData["energy_x0020_specialist"]);
		if (substr($ES,0,2) != "HP" && substr($ES,0,4) != "Rtl_" && $ES != "A Mathews" && $ES != "Jeanne C" && $ES != ""){
			if (strtoupper(substr($ES,0,3)) == "CET"){
				$ES = $WarehouseByName[strtoupper($ES)];
			}
	//		$Status = $SPData["Status"];
			$Status = $SPData["status"];
			//only use active ES and exclude SHV
			if ($Status!="SHV"){
				if(in_array($ES,$ActiveWarehouseByName)){
					$ESPrefix = ($WarehouseByShortName[$ES]{strlen($WarehouseByShortName[$ES])-3}=="1" ? "XX" : "").$WarehouseByShortName[$ES];
					$ES = $ESPrefix."QQQ".$ES;
				}else{
					$ES = "OLDQQQStaff";
				}
				$Notes[] = "Only displaying Active Energy Specialists";
				$Notes[] = "Excluding 'A Mathews' and 'Jeanne C'";
				$Notes[] = "Dataset is from Sharepoint Data";
				$Notes[] = "Excluding 'SHV' records";
				$Notes[] = "Summary row shows average of each month results";
				$Notes[] = "Detail rows shows data sum for the month";
	/*
				$HEADate = $SPData["HEADate"];
				$Contractor = $SPData["Contractor"];
				$WorkOrderSentDate = $SPData["WorkOrderSentDate"];
				$AcceptanceDate = $SPData["AcceptanceDate"];
				$InstallDate = $SPData["InstallDate"];
				$RoadblockKnobAndTube = $SPData["Roadblock-KnobAndTube"];
				$RoadblockMoisture = $SPData["Roadblock-Moisture"];
				$RoadblockCombustionSafety = $SPData["Roadblock-CombustionSafety"];
				$RoadblockStructural = $SPData["Roadblock-Structural"];
				$RoadblockAsbestos = $SPData["Roadblock-Asbestos"];
				$SiteID = $SPData["SiteID"];
				$InsulationContractIssuedDate = $SPData["Insulation_ContractIssuedDate"];
				$InsulationContractSignedDate = $SPData["Insulation_SignedDate"];
				$InsulationContractBilledDate = $SPData["Insulation_BillingDate"];
				$InsulationOriginalContractAmount = $SPData["Insulation_OriginalContractAmount"];
				$InsulationFinalContractAmount = $SPData["Insulation_FinalContractAmount"];
				$AirSealingContractIssuedDate = $SPData["AirSealing_ContractIssuedDate"];
				$AirSealingContractSignedDate = $SPData["AirSealing_SignedDate"];
				$AirSealingContractBilledDate = $SPData["AirSealing_BillingDate"];
				$AirSealingOriginalContractAmount = $SPData["AirSealing_OriginalContractAmount"];
				$AirSealingFinalContractAmount = $SPData["AirSealing_FinalContractAmount"];
	*/			
				$Utility = $SPData["utility"];
				switch($Utility){
					case "Berkshire Gas":
						$Utility = "BGAS";
						break;
					case "National Grid":
						$Utility = "NATGRID";
						break;
				}
				$HEADate = (date("Y",strtotime($SPData["hea_x0020_date"])) == 1969 ? "0000-00-00" : date("Y-m-d",strtotime($SPData["hea_x0020_date"])));
				$Contractor = $SPData["contractor"];
				$WorkOrderSentDate = ($SPData["work_x0020_order_x0020_sent"] ? date("Y-m-d",strtotime($SPData["work_x0020_order_x0020_sent"])) : "0000-00-00");
				$AcceptanceDate = ($SPData["acceptance_x0020_date"] ? date("Y-m-d",strtotime($SPData["acceptance_x0020_date"])) : "0000-00-00");
				$InstallDate = ($SPData["install_x0020_date"] ? date("Y-m-d",strtotime($SPData["install_x0020_date"])) : "0000-00-00");
				$RoadblockKnobAndTube = $SPData["roadblock_x002d_k_x0026_t"];
				$RoadblockMoisture = $SPData["roadblock_x002d_moisture"];
				$RoadblockCombustionSafety = $SPData["roadblock_x002d_combustion_x0020"];
				$RoadblockStructural = $SPData["roadblocks"];
				$RoadblockAsbestos = $SPData["roadblocks_x002d_asbestos"];
				$SiteID = $SPData["site_x0020_id"];
				$InsulationContractIssuedDate = ($SPData["contracts_x0020_issued_x0020_dat"] ? date("Y-m-d",strtotime($SPData["contracts_x0020_issued_x0020_dat"])) : "0000-00-00");
				$InsulationContractSignedDate = ($SPData["insulation_x0020_date_x0020_sign"] ? date("Y-m-d",strtotime($SPData["insulation_x0020_date_x0020_sign"])) : "0000-00-00");
				$InsulationContractBilledDate = ($SPData["insulation_x0020_billing_x0020_d"] ? date("Y-m-d",strtotime($SPData["insulation_x0020_billing_x0020_d"])) : "0000-00-00");
				$InsulationFinalContractAmount = $SPData["insulation_x0020_final_x0020_con"];
				$InsulationOriginalContractAmount = $SPData["insulation_x0020_original_x0020_"];
				$AirSealingContractIssuedDate = ($SPData["a_x002f_s_x0020_contracts_x0020_"] ? date("Y-m-d",strtotime($SPData["a_x002f_s_x0020_contracts_x0020_"])) : "0000-00-00");
				$AirSealingContractSignedDate = ($SPData["a_x002f_s_x0020_date_x0020_signe"] ? date("Y-m-d",strtotime($SPData["a_x002f_s_x0020_date_x0020_signe"])) : "0000-00-00");
				$AirSealingContractBilledDate = ($SPData["utility_x0020_billing_x0020_date"] ? date("Y-m-d",strtotime($SPData["utility_x0020_billing_x0020_date"])) : "0000-00-00");
				$AirSealingFinalContractAmount = $SPData["final_x0020__x0024_"];
				$AirSealingOriginalContractAmount = $SPData["a_x002f_s_x0020_original_x0020_c"];
				$TstatContractIssuedDate = ($SPData["tstats_x0020_contracts_x0020_iss"] ? date("Y-m-d",strtotime($SPData["tstats_x0020_contracts_x0020_iss"])) : "0000-00-00");
				$TstatContractSignedDate = ($SPData["tstats_x0020_date_x0020_signed"] ? date("Y-m-d",strtotime($SPData["tstats_x0020_date_x0020_signed"])) : "0000-00-00");
				$TstatContractBilledDate = ($SPData["tstat_x0020_billing_x0020_date"] ? date("Y-m-d",strtotime($SPData["tstat_x0020_billing_x0020_date"])) : "0000-00-00");
				$TstatFinalContractAmount = $SPData["tstat_x0020_final_x0020_contract"];
				$TstatOriginalContractAmount = $SPData["tstat_x0020_original_x0020_contr"];
				$BilledCount = 0;
				$BillingYear1 = 0;
				$BillingYear2 = 0;
				$BillingMonthYear = "";
				$ContractorBilledDate = "0000-00-00";
				if (MySQLDate($AirSealingContractBilledDate)){$ContractorBilledDate = $AirSealingContractBilledDate;}
				if (MySQLDate($AirSealingContractBilledDate) && strtotime($AirSealingContractBilledDate) >= strtotime($StartDate) && strtotime($AirSealingContractBilledDate) <= strtotime($EndDate)){
					$BillingMonthYear = date("Y-m",strtotime($AirSealingContractBilledDate));
					$BillingYear1 = date("Y",strtotime($AirSealingContractBilledDate));
					$BilledDates[$ES][$BillingMonthYear][$Utility]["airsealing"]["total"]=$BilledDates[$ES][$BillingMonthYear][$Utility]["airsealing"]["total"]+$AirSealingFinalContractAmount;
					$BilledDates[$ES][$BillingMonthYear][$Utility]["Totals"]=$BilledDates[$ES][$BillingMonthYear][$Utility]["Totals"]+$AirSealingFinalContractAmount;
					$BilledDates[$ES][$BillingMonthYear]["Totals"]=$BilledDates[$ES][$BillingMonthYear]["Totals"]+$AirSealingFinalContractAmount;
					$ESData[$ES][$BillingMonthYear]["BilledAmount"] = $ESData[$ES][$BillingMonthYear]["BilledAmount"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$ESData[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"]+($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					
					$BilledCount = 1;
					$ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"] = $ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"]+$BilledCount;
					$ESData[$ES][$BillingMonthYear]["BilledCount"] = $ESData[$ES][$BillingMonthYear]["BilledCount"]+$BilledCount;
					$BilledDates[$ES][$BillingMonthYear]["count"] = $BilledDates[$ES][date("Y-m",strtotime($InsulationContractBilledDate))]["count"]+$BilledCount;
					$BilledDates[$ES][$BillingMonthYear][$Utility]["count"] = $BilledDates[$ES][date("Y-m",strtotime($InsulationContractBilledDate))][$Utility]["count"]+$BilledCount;
					$Totals["BilledAmount"] = $Totals["BilledAmount"] + ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$Totals[$Utility]["BilledAmount"] = $Totals[$Utility]["BilledAmount"] + ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
					$Totals[$Utility]["BilledCount"] = $Totals[$Utility]["BilledCount"] + $BilledCount;
				}
				$Notes[] = "If contract has Billed Date, but Final Amount is blank, Original Amount is used";
				$BillingMonthYear = "";
				if (MySQLDate($InsulationContractBilledDate)){$ContractorBilledDate = $InsulationContractBilledDate;}
				if (MySQLDate($InsulationContractBilledDate) && strtotime($InsulationContractBilledDate) >= strtotime($StartDate) && strtotime($InsulationContractBilledDate) <= strtotime($EndDate)){
					$BillingMonthYear = date("Y-m",strtotime($InsulationContractBilledDate));
					$BillingYear2 = date("Y",strtotime($InsulationContractBilledDate));
					$BilledDates[$ES][$BillingMonthYear][$Utility]["insulation"]["total"]=$BilledDates[$ES][$BillingMonthYear][$Utility]["insulation"]["total"]+$InsulationFinalContractAmount;
					$BilledDates[$ES][$BillingMonthYear][$Utility]["Totals"]=$BilledDates[$ES][$BillingMonthYear][$Utility]["Totals"]+$InsulationFinalContractAmount;
					$BilledDates[$ES][$BillingMonthYear]["Totals"]=$BilledDates[$ES][$BillingMonthYear]["Totals"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);;
					$ESData[$ES][$BillingMonthYear]["BilledAmount"] = $ESData[$ES][$BillingMonthYear]["BilledAmount"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
					$ESData[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"]+($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);;
					
					$Notes[] = "If both Airsealing and Insulation contracts both billed then the count is only 1 unless they were billed in different years.";
					if (!$BilledCount || $BillingYear1 != $BillingYear2){
						$BilledCount = 1;
						$ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"] = $ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"]+$BilledCount;
						$ESData[$ES][$BillingMonthYear]["BilledCount"] = $ESData[$ES][$BillingMonthYear]["BilledCount"]+$BilledCount;
						$BilledDates[$ES][$BillingMonthYear]["count"] = $BilledDates[$ES][date("Y-m",strtotime($InsulationContractBilledDate))]["count"]+$BilledCount;
						$BilledDates[$ES][$BillingMonthYear][$Utility]["count"] = $BilledDates[$ES][date("Y-m",strtotime($InsulationContractBilledDate))][$Utility]["count"]+$BilledCount;
						$Totals["BilledAmount"] = $Totals["BilledAmount"] + ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
						$Totals[$Utility]["BilledAmount"] = $Totals[$Utility]["BilledAmount"] + ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
						$Totals[$Utility]["BilledCount"] = $Totals[$Utility]["BilledCount"] + $BilledCount;
					}
				}
				$BillingMonthYear = "";
				if (MySQLDate($TstatContractBilledDate) && strtotime($TstatContractBilledDate) >= strtotime($StartDate) && strtotime($TstatContractBilledDate) <= strtotime($EndDate)){
					//$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))][$Utility]["tstat"]["items"][]=$TstatFinalContractAmount;
					$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))][$Utility]["tstat"]["total"]=$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))][$Utility]["tstat"]["total"]+$TstatFinalContractAmount;
					$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))][$Utility]["Totals"]=$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))][$Utility]["Totals"]+$TstatFinalContractAmount;
					$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))]["Totals"]=$BilledDates[$ES][date("Y-m",strtotime($TstatContractBilledDate))]["Totals"]+$TstatFinalContractAmount;
					$BillingMonthYear = date("Y-m",strtotime($TstatContractBilledDate));
					$ESData[$ES][$BillingMonthYear]["BilledAmount"] = $ESData[$ES][$BillingMonthYear]["BilledAmount"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					//$ESData[$ES][$BillingMonthYear][$Utility]["BilledAmount"] = $ESData[$ES][$BillingMonthYear][$Utility]["BilledCount"]+($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$Totals["BilledAmount"] = $Totals["BilledAmount"] + ($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					//$Totals[$Utility]["BilledAmount"] = $Totals[$Utility]["BilledAmount"] + ($TstatFinalContractAmount > 0 ? $TstatFinalContractAmount : $TstatOriginalContractAmount);
					$Notes[] = "Tstat Install Billed amounts are excluded from Utility Amounts but included for ES Commissions";
				}
				
				if (MySQLDate($HEADate) && (strtotime($HEADate) >= strtotime($StartDate) && strtotime($HEADate) <= strtotime($EndDate))){
					$RoadBlocked = 0;
					if (($RoadblockKnobAndTube == 'TRUE' || $RoadblockKnobAndTube > 0) || ($RoadblockMoisture == 'TRUE' || $RoadblockMoisture > 0) || ($RoadblockCombustionSafety == 'TRUE' || $RoadblockCombustionSafety>0) || ($RoadblockStructural == 'TRUE' || $RoadblockStructural>0)|| ($RoadblockAsbestos == 'TRUE' || $RoadblockAsbestos>0)){
						$RoadBlocked = 1;
						$Notes[] = "Considered Roadblocked if any one of the Roadblocked Items are indicated AND there are no contracts issued/signed";
					}
					
					$IssuedDates = array();
					$SignedDates = array();
					$BillingDates = array();
					$TotalOriginalContractAmount = 0;
					$TotalFinalContractAmount = 0;
					//look at the different contract types
						if (MySQLDate($AirSealingContractIssuedDate)){$IssuedDates[] = strtotime($AirSealingContractIssuedDate);}
						if (MySQLDate($AirSealingContractSignedDate)){$SignedDates[] = strtotime($AirSealingContractSignedDate);}
						if (MySQLDate($AirSealingContractBilledDate)){$BillingDates[] = strtotime($AirSealingContractBilledDate);}
						$TotalOriginalContractAmount = ($TotalOriginalContractAmount+$AirSealingOriginalContractAmount);
						$TotalFinalContractAmount = ($TotalFinalContractAmount+$AirSealingFinalContractAmount);
						if (MySQLDate($InsulationContractIssuedDate)){$IssuedDates[] = strtotime($InsulationContractIssuedDate);}
						if (MySQLDate($InsulationContractSignedDate)){$SignedDates[] = strtotime($InsulationContractSignedDate);}
						if (MySQLDate($InsulationContractBilledDate)){$BillingDates[] = strtotime($InsulationContractBilledDate);}
						$TotalOriginalContractAmount = ($TotalOriginalContractAmount+$InsulationOriginalContractAmount);
						$TotalFinalContractAmount = ($TotalFinalContractAmount+$InsulationFinalContractAmount);
					//get the earliest date 
					$EarliestIssueDate = (count($IssuedDates) ? date("Y-m-d",min($IssuedDates)) : "0000-00-00");
					$EarliestSignedDate = (count($SignedDates) ? date("Y-m-d",min($SignedDates)) : "0000-00-00");
					$EarliestBillingDate = (count($BillingDates) ? date("Y-m-d",min($BillingDates)) : "0000-00-00");
					
					if (MySQLDate($HEADate)){
						$MonthYear = date("Y-m",strtotime($HEADate));
						$ESData[$ES][$MonthYear]["SiteCount"] = $ESData[$ES][$MonthYear]["SiteCount"]+1;
						$Totals["SiteVisits"] = $Totals["SiteVisits"]+1;
						if ($Status == "No Work"){
							$ESData[$ES][$MonthYear]["NoWork"] = $ESData[$ES][$MonthYear]["NoWork"]+1;
							$Totals["NoWork"] = $Totals["NoWork"]+1;
							$ESData[$ES][$MonthYear]["SiteID"][] = $SiteID;
						}else{
							if (count($IssuedDates) > 0){
								if (count($SignedDates) > 0){
									$ESData[$ES][$MonthYear]["ContractsSigned"] = $ESData[$ES][$MonthYear]["ContractsSigned"]+1;
									$Totals["ContractsSigned"] = $Totals["ContractsSigned"]+1;
									if ($RoadBlocked > 0){
										$ESData[$ES][$MonthYear]["RoadBlockedMitigated"] = $ESData[$ES][$MonthYear]["RoadBlockedMitigated"]+$RoadBlocked;
									}
								}
							}else{
								if ($RoadBlocked > 0){
									$ESData[$ES][$MonthYear]["RoadBlocked"] = $ESData[$ES][$MonthYear]["RoadBlocked"]+$RoadBlocked;
									$Totals["RoadBlocked"] = $Totals["RoadBlocked"]+1;
								}else{
									$ESData[$ES][$MonthYear]["Pending"] = $ESData[$ES][$MonthYear]["Pending"]+1;
									$Totals["Pending"] = $Totals["Pending"]+1;
									$Notes[] = "Site considered 'Pending' if Status is not 'No Work', there are no Roadblocks, and no contracts signed";
								}
							}
							if (count($BillingDates) > 0){
								$ESData[$ES][$MonthYear]["ContractsBilled"] = $ESData[$ES][$MonthYear]["ContractsBilled"]+1;
								$Totals["ContractsBilled"] = $Totals["ContractsBilled"]+1;
							}
							
						}
						if (MySQLDate($EarliestIssueDate)){
								$DaystoIssue = datediff($HEADate,$EarliestIssueDate);
								$ESData[$ES][$MonthYear]["DaysToIssue"][] = $DaystoIssue;
								$Formulas["ES"]["Days To Issue"] = "Number of Days between HEA Visit and Contract Issue Date";
						}
						if (MySQLDate($EarliestSignedDate)){
							$DaystoSign = datediff($EarliestIssueDate,$EarliestSignedDate);
							$ESData[$ES][$MonthYear]["DaysToSign"][] = $DaystoSign;
							$Formulas["ES"]["Days To Sign"] = "Number of Days between earliest Issue Date and Customer Signed Date";
						}
						if (MySQLDate($EarliestBillingDate)){
							$DaystoBill = datediff($HEADate,$EarliestBillingDate);
							$ESData[$ES][$MonthYear]["DaysToBill"][] = $DaystoBill;
							$Formulas["ES"]["Days To Bill"] = "Number of Days between HEA Visit and Earliest Billed Date";
						}
						$ESData[$ES][$MonthYear]["TotalOriginalContractAmount"] = ($ESData[$ES][$MonthYear]["TotalOriginalContractAmount"]+$TotalOriginalContractAmount);
						$ESData[$ES][$MonthYear]["TotalFinalContractAmount"] = ($ESData[$ES][$MonthYear]["TotalFinalContractAmount"]+$TotalFinalContractAmount);
						$Totals["FinalContractAmount"] = $Totals["FinalContractAmount"]+$TotalFinalContractAmount;
						if ($TotalFinalContractAmount > 0){
							$ESData[$ES][$MonthYear]["TotalContractChangedAmount"] = ($ESData[$ES][$MonthYear]["TotalFinalContractAmount"]-$ESData[$ES][$MonthYear]["TotalOriginalContractAmount"]);
							$ESData[$ES][$MonthYear]["TotalContractedAmount"][] = $TotalFinalContractAmount;
							$Totals["TotalContractAmount"] = $Totals["TotalContractAmount"]+$TotalFinalContractAmount;
						}else{
							$ESData[$ES][$MonthYear]["TotalContractedAmount"][] = $TotalOriginalContractAmount;
							$Totals["TotalContractAmount"] = $Totals["TotalContractAmount"]+$TotalOriginalContractAmount;
						}
						
						$ESData[$ES][$MonthYear]["ContractsSignedCount"] = count($ESData[$ES][$MonthYear]["DaysToSign"]);
						$ESData[$ES][$MonthYear]["ContractsIssuedCount"] = count($ESData[$ES][$MonthYear]["DaysToIssue"]);
						$ESData[$ES][$MonthYear]["ContractsBilledCount"] = count($ESData[$ES][$MonthYear]["DaysToBill"]);
						$ESData[$ES][$MonthYear]["SignedRate"] = (floor(($ESData[$ES][$MonthYear]["ContractsSigned"]/$ESData[$ES][$MonthYear]["SiteCount"])*100));
						$Formulas["ES"]["Signed Rate"] = "(Number of HEA Visits this Month that get Contracts Signed)/(Number of HEA Visits this Month)";

						$ESData[$ES][$MonthYear]["ConversionRate"] = (floor(($ESData[$ES][$MonthYear]["BilledCount"]/$ESData[$ES][$MonthYear]["SiteCount"])*100));
						$Formulas["ES"]["Conversion Rate"] = "(Number of Contracts with Billed Date in this Month)/(Number of HEA Visits this Month)";
						
						$ESData[$ES][$MonthYear]["AverageNumberofDaystoIssueContracts"] = ceil(array_sum($ESData[$ES][$MonthYear]["DaysToIssue"])/$ESData[$ES][$MonthYear]["ContractsIssuedCount"]);
						$ESData[$ES][$MonthYear]["AverageNumberofDaystoSignContracts"] = ceil(array_sum($ESData[$ES][$MonthYear]["DaysToSign"])/$ESData[$ES][$MonthYear]["ContractsSignedCount"]);
						$ESData[$ES][$MonthYear]["AverageNumberofDaystoBillContracts"] = ceil(array_sum($ESData[$ES][$MonthYear]["DaysToBill"])/$ESData[$ES][$MonthYear]["ContractsBilledCount"]);
						$ESData[$ES][$MonthYear]["AverageContractedAmount"] = ceil(array_sum($ESData[$ES][$MonthYear]["TotalContractedAmount"])/count($ESData[$ES][$MonthYear]["TotalContractedAmount"]));
					}
					if ($Contractor != "To Be Assigned" && trim($Contractor) != '' && !in_array($Contractor,$ignoreContractor)){
			//SELECT `SharePointID`,`SiteID`,`HEADate`, `WorkOrderSentDate`, `AcceptanceDate`,`InstallDate`  FROM `cet_sharepoint` WHERE `Contractor` LIKE '%Cozy Home Performance%' AND HEADate BETWEEN '2014-08-01' AND '2014-08-30' ORDER BY `HEADate` DESC  			
						if (MySQLDate($WorkOrderSentDate)){
							if (MySQLDate($InstallDate) && !MySQLDate($AcceptanceDate)){
								$AcceptanceDate = date("Y-m-d", strtotime($WorkOrderSentDate." + 2 days"));
								$Notes[] = "If Contractor has an Install Date, but no Acceptance Date, Acceptance Date will default to 2 days after Work Sent Date";
							}
							if (MySQLDate($AcceptanceDate)){
								$DaysToAccept = datediff($WorkOrderSentDate,$AcceptanceDate);
								$ContractorData[$Contractor][$MonthYear]["DaysToAccept"][] = $DaysToAccept;
								$ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToAccept"][] = $DaysToAccept;
							}
							$Formulas["Contractor"]["Days To Accept"] = "Days between Work Order Sent and Work Accepted Date";
							
							if (MySQLDate($InstallDate)){
								$DaysToInstall = datediff($AcceptanceDate,$InstallDate);
								$ContractorData[$Contractor][$MonthYear]["DaysToInstall"][] = $DaysToInstall;
								$ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToInstall"][] = $DaysToInstall;
								if (MySQLDate($ContractorBilledDate)){
									//BilledDate is always end of month so using 15th of the month
									$Notes[] = "*Since Billed Date is always end of the month, the 15th of the month is used to determine days between Install Date and Bill Date for Contractor Days To Bill";
									$DaysToBill = abs(datediff($InstallDate,date("Y-m-15",strtotime($ContractorBilledDate))));
									if ($DaysToBill > 0){
										$ContractorData[$Contractor][$MonthYear]["DaysToBill"][] = $DaysToBill;
										$ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToBill"][] = $DaysToBill;
									}
								}
							}
							$Formulas["Contractor"]["Days To Install"] = "Days between Accepted Date and Installed Date";
							$Formulas["Contractor"]["Days To Bill"] = "Days between Installed Date and Billed Date (*See Note for Contractor Billed Date)";
						}
						if ($TotalOriginalContractAmount > 1){
							$ContractorData[$Contractor][$MonthYear]["ContractAmount"][] = ($TotalFinalContractAmount > $TotalOriginalContractAmount ? $TotalFinalContractAmount : $TotalOriginalContractAmount);
							$ContractorData["ZZZContractor Averages"][$MonthYear]["ContractAmount"][] = ($TotalFinalContractAmount > $TotalOriginalContractAmount ? $TotalFinalContractAmount : $TotalOriginalContractAmount);
						}
						$ContractorData[$Contractor][$MonthYear]["AverageDaystoAccept"] = ceil(array_sum($ContractorData[$Contractor][$MonthYear]["DaysToAccept"])/count($ContractorData[$Contractor][$MonthYear]["DaysToAccept"]));
						$ContractorData[$Contractor][$MonthYear]["AverageDaystoInstall"] = ceil(array_sum($ContractorData[$Contractor][$MonthYear]["DaysToInstall"])/count($ContractorData[$Contractor][$MonthYear]["DaysToInstall"]));
						$ContractorData[$Contractor][$MonthYear]["AverageDaystoBill"] = ceil(array_sum($ContractorData[$Contractor][$MonthYear]["DaysToBill"])/count($ContractorData[$Contractor][$MonthYear]["DaysToBill"]));
						$ContractorData[$Contractor][$MonthYear]["AverageContractAmount"] = ceil(array_sum($ContractorData[$Contractor][$MonthYear]["ContractAmount"])/count($ContractorData[$Contractor][$MonthYear]["ContractAmount"]));
						$ContractorData["ZZZContractor Averages"][$MonthYear]["AverageDaystoAccept"] = ceil(array_sum($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToAccept"])/count($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToAccept"]));
						$ContractorData["ZZZContractor Averages"][$MonthYear]["AverageDaystoInstall"] = ceil(array_sum($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToInstall"])/count($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToInstall"]));
						$ContractorData["ZZZContractor Averages"][$MonthYear]["AverageDaystoBill"] = ceil(array_sum($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToBill"])/count($ContractorData["ZZZContractor Averages"][$MonthYear]["DaysToBill"]));
						$ContractorData["ZZZContractor Averages"][$MonthYear]["AverageContractAmount"] = ceil(array_sum($ContractorData["ZZZContractor Averages"][$MonthYear]["ContractAmount"])/count($ContractorData["ZZZContractor Averages"][$MonthYear]["ContractAmount"]));
						$ContractorTotals["ContractAmount"] = $ContractorTotals["ContractAmount"]+($TotalFinalContractAmount > $TotalOriginalContractAmount ? $TotalFinalContractAmount : $TotalOriginalContractAmount);
					}
				}//end only HEADates in timeframe
			}//end using only active specialists
		}//end ignoring other EnergySpecialist
	}//end foreach sprecord
	${$ArrayName} = null;
}//end foreach view
//empty $SPRecords to free up memory
$SPRecords = array();
$Notes[] = "Accessed ".$SPRecordsCount." records";
	
ksort($ESData);
ksort($ContractorData);
//print_pre($ContractorData["Beyond Green"]);
foreach($ContractorData as $Contractor=>$Data){
	$DisplayRow = $Contractor."<Br>";
	$TableRow = "";
	ksort($Data);
	foreach ($Data as $MonthYear=>$Details){
		//print_pre($Details);
			$TableRow .="<tr><td>".$MonthYear."</td>";
			$TableRow .="<td>".($Details["AverageDaystoAccept"] < 1 ? "Same Day" : $Details["AverageDaystoAccept"])."</td>";
			$TableRow .="<td>".($Details["AverageDaystoInstall"] > 0 ? $Details["AverageDaystoInstall"] : "Not Installed Dates")."</td>";
			$TableRow .="<td>".($Details["AverageDaystoBill"] > 0 ? $Details["AverageDaystoBill"] : "No Billed Dates")."</td>";
			$TableRow .="<td>".($Details["AverageDaystoInstall"] > 0 ? "$".money($Details["AverageContractAmount"],true,true) : "" )."</td>";
			$TableRow .= "</tr>";
			$DisplayRow .=$MonthYear.":<Br>".
				"Average Days to Accept=".($Details["AverageDaystoAccept"] < 1 ? "Same Day" : $Details["AverageDaystoAccept"])."<br>";
			if ($Details["AverageDaystoInstall"] > 0){
				$DisplayRow .= "Average Days to Install=".$Details["AverageDaystoInstall"]."<br>";
				$DisplayRow .= "Average Contract Amount=$".money($Details["AverageContractAmount"])."<br>";
			}
			if ($Details["AverageDaystoBill"] > 0){
				$DisplayRow .= "Average Days to Bill=".$Details["AverageDaystoBill"]."<br>";
			}
			$DisplayRow .= "<br><br>";
			
			$ContractorAggregate[$Contractor]["DaysToAccept"][] = $Details["AverageDaystoAccept"];
			$ContractorAggregate[$Contractor]["DaysToInstall"][] = $Details["AverageDaystoInstall"];
			if ($Details["AverageDaystoBill"] > 0){
				$ContractorAggregate[$Contractor]["DaysToBill"][] = $Details["AverageDaystoBill"];
			}
			$ContractorAggregate[$Contractor]["ContractAmount"][] = $Details["AverageContractAmount"];
	}
	$DisplayRow .= "<hr>";
	$DetailContractorTableRows[$Contractor][] = $TableRow;
	$DetailDisplayRows[] = $DisplayRow;	
}
//print_pre($DetailDisplayRows);
//print_pre($DetailContractorTableRows);
foreach ($ESData as $EnergySpecialist=>$Data){
		$DisplayRow = $EnergySpecialist."<br>";
		$TableRow = "";
		ksort($Data);
		foreach ($Data as $MonthYear=>$Details){			
				$Details["ConversionRate"] = ($EnergySpecialist == "OLDQQQStaff" ? 0 : (floor(($Details["BilledCount"]/$Details["SiteCount"])*100)));

			$TableRow .= "<tr><td>".$MonthYear."</td>";
			$DisplayRow .= $MonthYear.":<br>".
				"Sites Visited=".$Details["SiteCount"]."<br>".
				"Sites With No Work=".($Details["NoWork"] ? $Details["NoWork"] : "0")."<br>".
				"Sites RoadBlocked=".($Details["RoadBlocked"] ? $Details["RoadBlocked"] : "0")."<br>".
				"Sites Pending Contracts to be Signed=".($Details["Pending"] ? $Details["Pending"] : "0")."<br>".
				"Contracts Signed=".($Details["ContractsSigned"] ? $Details["ContractsSigned"] : "0")."<br>".
				"Signed Rate=".$Details["SignedRate"]."%<br>";
				"Conversion Rate=".$Details["ConversionRate"]."%<br>";
			$TableRow .= "<td>".$Details["SiteCount"]."</td>";
			$TableRow .= "<td>".($Details["NoWork"] ? $Details["NoWork"] : "0")."</td>";
			$TableRow .= "<td>".($Details["RoadBlocked"] ? $Details["RoadBlocked"] : "0")."</td>";
			$TableRow .= "<td>".($Details["Pending"] ? $Details["Pending"] : "0")."</td>";
			$TableRow .= "<td>".($Details["ContractsSigned"] ? $Details["ContractsSigned"] : "0")."</td>";
			$TableRow .= "<td>".($Details["BilledCount"] ? $Details["BilledCount"] : "0")."</td>";
//			$TableRow .= "<td>".$Details["SignedRate"]."%</td>";
			$TableRow .= "<td>".$Details["ConversionRate"]."%</td>";
				$ESAggregate[$EnergySpecialist]["SitesVisted"][] = $Details["SiteCount"];
				$ESAggregate[$EnergySpecialist]["NoWork"][] = $Details["NoWork"];
				$ESAggregate[$EnergySpecialist]["RoadBlocked"][] = $Details["RoadBlocked"];
				$ESAggregate[$EnergySpecialist]["Pending"][] = $Details["Pending"];
				$ESAggregate[$EnergySpecialist]["ContractsSigned"][] = $Details["ContractsSigned"];
				$ESAggregate[$EnergySpecialist]["ContractsBilled"][] = $Details["ContractsBilled"];
				$ESAggregate[$EnergySpecialist]["SignedRate"][] = $Details["SignedRate"];
				$ESAggregate[$EnergySpecialist]["ConversionRate"][] = $Details["ConversionRate"];
//			if ($Details["ContractsSigned"] > 0){
				$Commission = bcmul($Details["BilledAmount"],0.02);
				$Formulas["ES"]["Commission"] = "2% of Total Contract Amount with Billed Date in this Month";
				$DisplayRow .=
				"Average Number of Days to Issue Contracts=".$Details["AverageNumberofDaystoIssueContracts"]."<br>".
				"Average Number of Days to Sign Contracts=".$Details["AverageNumberofDaystoSignContracts"]."<br>".
				"Average Number of Days to Bill Contracts=".$Details["AverageNumberofDaystoBillContracts"]."<br>".
				"Total Original Contracted Amount=$".money($Details["TotalOriginalContractAmount"])."<br>".
				"Total Final Contracted Amount=$".money($Details["TotalFinalContractAmount"])."<br>".
				"Total Change in Charged Amount=$".money($Details["TotalContractChangedAmount"])."<br>".				
				"Average Contracted Amount=$".money($Details["AverageContractedAmount"])."<br>".
				"Total Billed Amount=$".money($Details["BilledAmount"])."<br>".
				"Total Commission=$".money($Commission).".<br>";
						
				$TableRow .= "<td>".$Details["AverageNumberofDaystoIssueContracts"]."</td>";
				$TableRow .= "<td>".$Details["AverageNumberofDaystoSignContracts"]."</td>";
				$TableRow .= "<td>".$Details["AverageNumberofDaystoBillContracts"]."</td>";
				$TableRow .= "<td>$".money($Details["TotalOriginalContractAmount"])."</td>";
				$TableRow .= "<td>$".money($Details["TotalFinalContractAmount"])."</td>";
				$TableRow .= "<td>$".money($Details["TotalContractChangedAmount"])."</td>";
				$TableRow .= "<td>$".money($Details["AverageContractedAmount"],true,true)."</td>";
				if ($includeCommissions){
//					$TableRow .= "<td>$".money($Details["BilledAmount"])."</td>";
//					$TableRow .= "<td>$".money($Commission)."</td>";
				}
				$ESAggregate[$EnergySpecialist]["BilledAmount"][] = $Details["BilledAmount"];
				$ESAggregate[$EnergySpecialist]["DaysToIssueContracts"][] = $Details["AverageNumberofDaystoIssueContracts"];
				$ESAggregate[$EnergySpecialist]["DaysToSignContracts"][] = $Details["AverageNumberofDaystoSignContracts"];
				$ESAggregate[$EnergySpecialist]["DaysToBillContracts"][] = $Details["AverageNumberofDaystoBillContracts"];
				$ESAggregate[$EnergySpecialist]["FinalContractAmounts"][] = $Details["TotalFinalContractAmount"];
				
				$ESAggregate[$EnergySpecialist]["NATGRID"]["BilledCount"][] = $Details["NATGRID"]["BilledCount"];
				$ESAggregate[$EnergySpecialist]["WMECO"]["BilledCount"][] = $Details["WMECO"]["BilledCount"];
				$ESAggregate[$EnergySpecialist]["BGAS"]["BilledCount"][] = $Details["BGAS"]["BilledCount"];
				$ESAggregate[$EnergySpecialist]["BGAS"]["BilledAmount"][] = $Details["BGAS"]["BilledAmount"];
				$TableRow .= "<td>".$Details["NATGRID"]["BilledCount"]."</td>";
				$TableRow .= "<td>".$Details["WMECO"]["BilledCount"]."</td>";
				$TableRow .= "<td>".$Details["BGAS"]["BilledCount"]."</td>";
				if ($includeCommissions){
//					$TableRow .= "<td>$".money($Details["BGAS"]["BilledAmount"])."</td>";
				}
//
/*
			}else{
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
				$TableRow .= "<td></td>";
			}
*/
			$DisplayRow .= "<br><br>";
			$TableRow .= "</tr>";
			//print_pre($Details);
		}
		$DisplayRow .= "<hr>";
		$DetailDisplayRows[] = $DisplayRow;
		$DetailTableRows[str_replace("XX","",$EnergySpecialist)][] = $TableRow;
}
//print_pre($ContractorAggregate);
foreach($ContractorAggregate as $Contractor=>$Details){
	$ContDaysToAccept = ceil(array_sum($Details["DaysToAccept"])/count($Details["DaysToAccept"]));
	$ContDaysToInstall = ceil(array_sum($Details["DaysToInstall"])/count($Details["DaysToInstall"]));
	$TableRow = "<tr>";
	$TableRow .= "<td class=\"details-control\"></td>";
	$TableRow .= "<td>".$Contractor."</td>";
	$TableRow .= "<td>".($ContDaysToAccept > 0 ? $ContDaysToAccept : "Same Day")."</td>";	
	$TableRow .= "<td>".($ContDaysToInstall > 0 ? $ContDaysToInstall : "No Install Dates Yet")."</td>";	
	$TableRow .= "<td>".ceil(array_sum($Details["DaysToBill"])/count($Details["DaysToBill"]))."</td>";	
	$TableRow .= "<td>$".money(ceil(array_sum($Details["ContractAmount"])/count($Details["ContractAmount"])),true,true)."</td>";	
	$TableRow .= "<td style='display:none;'>Contractor</td>";
	$ContractorTableRow[$Contractor]["daysToAccept"]=ceil(array_sum($Details["DaysToAccept"])/count($Details["DaysToAccept"]));
	$ContractorTableRow[$Contractor]["daysToInstall"]=ceil(array_sum($Details["DaysToInstall"])/count($Details["DaysToInstall"]));
	$ContractorTableRow[$Contractor]["daysToBill"]=ceil(array_sum($Details["DaysToBill"])/count($Details["DaysToBill"]));
	$ContractorTableRow[$Contractor]["contractAmount"]=ceil(array_sum($Details["ContractAmount"])/count($Details["ContractAmount"]));
	$SummaryContractorTableRows[] = $TableRow;	
}
foreach($ESAggregate as $ES=>$Details){
	$TableRow = "<tr>";
	$TableRow .= "<td class=\"details-control\"></td>";
	$ESName = explode("QQQ",str_replace("XX","",$ES));
	$TableRow .= "<td class=\"esName energySpecialist\" data-esname=\"".$ESName[1]."\">".str_replace("<br>","",str_replace("XX","",$ES))."</td>";
	$DisplayRow = $ES."<Br>";
	$AverageNumberOfSitesVisitedPerMonth = ceil(array_sum($Details["SitesVisted"])/count($Details["SitesVisted"]));
	$DisplayRow .= "Average # of Sites Visited Per Month=".$AverageNumberOfSitesVisitedPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfSitesVisitedPerMonth."</td>";	
	$TableRowData[$ES]["siteVisits"] = $AverageNumberOfSitesVisitedPerMonth;
	$Aggregate["SiteVisits"][] = $AverageNumberOfSitesVisitedPerMonth;
	
	$AverageNumberOfNoWorkPerMonth = ceil(array_sum($Details["NoWork"])/count($Details["NoWork"]));
	$DisplayRow .= "Average # of Sites with No Work Per Month=".$AverageNumberOfNoWorkPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfNoWorkPerMonth."</td>";
	$TableRowData[$ES]["noWork"] = $AverageNumberOfNoWorkPerMonth;
	$Aggregate["NoWork"][] = $AverageNumberOfNoWorkPerMonth;
	
	$AverageNumberOfRoadBlockedPerMonth = ceil(array_sum($Details["RoadBlocked"])/count($Details["RoadBlocked"]));
	$DisplayRow .= "Average # of Sites with RoadBlocks Per Month=".$AverageNumberOfRoadBlockedPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfRoadBlockedPerMonth."</td>";
	$TableRowData[$ES]["roadBlocked"] = $AverageNumberOfRoadBlockedPerMonth;
	$TableRow .="<td align=\"center\" style='display:none;'>EnergySpecialist</td>";
	$Aggregate["RoadBlock"][] = $AverageNumberOfRoadBlockedPerMonth;
	
	$AverageNumberOfPendingPerMonth = ceil(array_sum($Details["Pending"])/count($Details["Pending"]));
	$DisplayRow .= "Average # of Sites Pending Contracts to be Signed Per Month=".$AverageNumberOfPendingPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfPendingPerMonth."</td>";
	$TableRowData[$ES]["pending"] = $AverageNumberOfPendingPerMonth;
	$Aggregate["Pending"][] = $AverageNumberOfPendingPerMonth;
	
	$AverageNumberOfContractsPerMonth = ceil(array_sum($Details["ContractsSigned"])/count($Details["ContractsSigned"]));
	$DisplayRow .= "Average # of Contracts Per Month=".$AverageNumberOfContractsPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfContractsPerMonth."</td>";
	$TableRowData[$ES]["signed"] = $AverageNumberOfContractsPerMonth;
	$Aggregate["Contracts"][] = $AverageNumberOfContractsPerMonth;

	$AverageNumberOfContractsBilledPerMonth = ceil(array_sum($Details["ContractsBilled"])/count($Details["ContractsBilled"]));
	$DisplayRow .= "Average # of Contracts Billed Per Month=".$AverageNumberOfContractsBilledPerMonth."<br>";
	$TableRow .= "<td align=\"center\">".$AverageNumberOfContractsBilledPerMonth."</td>";
	$Aggregate["ContractsBilled"][] = $AverageNumberOfContractsBilledPerMonth;
	$Totals["ContractsBilled"] = $Totals["ContractsBilled"]+array_sum($Details["ContractsBilled"]);

	
	$AverageSignedRate = ceil(array_sum($Details["SignedRate"])/count($Details["SignedRate"]));
	$DisplayRow .= "Average SignedRate=".$AverageSignedRate."%<br>";
//	$TableRow .= "<td align=\"center\">".$AverageSignedRate."%</td>";
	$TableRowData[$ES]["signedRate"] = $AverageSignedRate;
//	$Aggregate["SignedRate"][] = $AverageSignedRate;
	
	$AverageConversionRate = ceil(array_sum($Details["ConversionRate"])/count($Details["ConversionRate"]));
	$DisplayRow .= "Average ConversionRate=".$AverageConversionRate."%<br>";
	$TableRow .= "<td align=\"center\">".$AverageConversionRate."%</td>";
	$TableRowData[$ES]["conversionRate"] = $AverageConversionRate;
	$Aggregate["ConversionRate"][] = $AverageConversionRate;
	
	$AverageDaysToIssueContracts = ceil(array_sum($Details["DaysToIssueContracts"])/count($Details["DaysToIssueContracts"]));
	$DisplayRow .= "Average # of Days to Issue Contracts=".($AverageDaysToIssueContracts < 1 ? "Same Day" : $AverageDaysToIssueContracts)."<br>";
	$TableRow .= "<td align=\"center\">".($AverageSignedRate > 0 ? ($AverageDaysToIssueContracts < 1 ? "Same Day" : $AverageDaysToIssueContracts) : '' )."</td>";
	$TableRowData[$ES]["daysToIssue"] = $AverageDaysToIssueContracts;
	$Aggregate["DaysToIssue"][] = $AverageDaysToIssueContracts;
	
	$AverageDaysToSignContracts = ceil(array_sum($Details["DaysToSignContracts"])/count($Details["DaysToSignContracts"]));
	$DisplayRow .= "Average # of Days to Sign Contracts=".($AverageDaysToSignContracts < 1 ? "Same Day" : $AverageDaysToSignContracts)."<br>";
	$TableRow .= "<td align=\"center\">".($AverageSignedRate > 0 ? ($AverageDaysToSignContracts < 1 ? "Same Day" : $AverageDaysToSignContracts) : '' )."</td>";
	$TableRowData[$ES]["daysToSign"] = $AverageDaysToSignContracts;
	$Aggregate["DaysToSign"][] = $AverageDaysToSignContracts;

	//remove contracts  amounts with 0 dollars
	foreach ($Details["FinalContractAmounts"] as $key=>$value) {
		if ($value < 1){
			unset($Details["FinalContractAmounts"][$key]);
		}
	}	
	
	//remove billed contracts with 0 date
	foreach ($Details["DaysToBillContracts"] as $key=>$value) {
		if ($value < 1){
			unset($Details["DaysToBillContracts"][$key]);
		}
	}	
	$AverageDaysToBillContracts = ceil(array_sum($Details["DaysToBillContracts"])/count($Details["DaysToBillContracts"]));
	if ($AverageDaysToBillContracts){
		$DisplayRow .= "Average # of Days to Bill Contracts=".$AverageDaysToBillContracts."<br>";
		$TableRow .= "<td align=\"center\">".$AverageDaysToBillContracts."</td>";
		$TableRowData[$ES]["daysToBill"] = $AverageDaysToBillContracts;
		$Aggregate["DaysToBill"][] = $AverageDaysToBillContracts;
	}else{
		$DisplayRow .= "No Billed Contract Dates<br>";
		$TableRow .= "<td></td>";
		$TableRowData[$ES]["daysToBill"] = 0;
		$Aggregate["DaysToBill"][] = 0;
	}
	
	$AverageContractAmount = ceil(array_sum($Details["FinalContractAmounts"])/count($Details["FinalContractAmounts"]));
	if ($AverageContractAmount){
		$Commission = bcmul(array_sum($Details["BilledAmount"]),0.02);
		$DisplayRow .= "Average Contract Amount $=".money($AverageContractAmount)."<br>";
		$TableRow .= "<td align=\"center\">$".money($AverageContractAmount,true,true)."</td>";
		if ($includeCommissions){
//			$TableRow .= "<td align=\"center\">$".money(array_sum($Details["BilledAmount"]),true,true)."</td>";
//			$TableRow .= "<td align=\"center\">$".money($Commission,true,true)."</td>";
		}
		$TableRowData[$ES]["contractAmount"] = $AverageContractAmount;
		$TableRowData[$ES]["billedAmount"] = array_sum($Details["BilledAmount"]);
		$TableRowData[$ES]["commissions"] = $Commission;
			$TableRowData[$ES]["NATGRID_BilledCount"] = ceil(array_sum($Details["NATGRID"]["BilledCount"])/count($Details["NATGRID"]["BilledCount"]));
			$TableRowData[$ES]["WMECO_BilledCount"] = ceil(array_sum($Details["WMECO"]["BilledCount"])/count($Details["WMECO"]["BilledCount"]));
			$TableRowData[$ES]["BGAS_BilledCount"] = ceil(array_sum($Details["BGAS"]["BilledCount"])/count($Details["BGAS"]["BilledCount"]));
			$TableRowData[$ES]["BGAS_BilledAmount"] = ceil(array_sum($Details["BGAS"]["BilledAmount"])/count($Details["BGAS"]["BilledAmount"]));
			$TableRow .= "<td align=\"center\">".ceil(array_sum($Details["NATGRID"]["BilledCount"])/count($Details["NATGRID"]["BilledCount"]))."</td>";
			$TableRow .= "<td align=\"center\">".ceil(array_sum($Details["WMECO"]["BilledCount"])/count($Details["WMECO"]["BilledCount"]))."</td>";
			$TableRow .= "<td align=\"center\">".ceil(array_sum($Details["BGAS"]["BilledCount"])/count($Details["BGAS"]["BilledCount"]))."</td>";
		$Aggregate["ContractAmount"][] = $AverageContractAmount;
		if ($includeCommissions){
	//		$TableRow .= "<td align=\"center\">$".money((ceil(array_sum($Details["BGAS"]["BilledAmount"])/count($Details["BGAS"]["BilledAmount"]))),true,true)."</td>";
	//		$Aggregate["BilledAmount"][] = array_sum($Details["BilledAmount"]);
	//		$Aggregate["Commissions"][] = $Commission;
		}
		$Aggregate["NATGRID_BilledCount"][] = ceil(array_sum($Details["NATGRID"]["BilledCount"])/count($Details["NATGRID"]["BilledCount"]));
		$Aggregate["WMECO_BilledCount"][] = ceil(array_sum($Details["WMECO"]["BilledCount"])/count($Details["WMECO"]["BilledCount"]));
		$Aggregate["BGAS_BilledCount"][] = ceil(array_sum($Details["BGAS"]["BilledCount"])/count($Details["BGAS"]["BilledCount"]));
		if ($includeCommissions){
//			$Aggregate["BGASBilledAmount"][] = (ceil(array_sum($Details["BGAS"]["BilledAmount"])/count($Details["BGAS"]["BilledAmount"])));
		}
	}else{
		$DisplayRow .= "No Contracts with Amounts<br>";
		$TableRow .= "<td></td>";
		if ($includeCommissions){
//			$TableRow .= "<td></td>";
//			$TableRow .= "<td></td>";
		}
		$TableRow .= "<td></td>";
		$TableRow .= "<td></td>";
		$TableRow .= "<td></td>";
		if ($includeCommissions){
//			$TableRow .= "<td></td>";
		}
	}
	
	$DisplayRow .= "<hr>";
	$TableRow .= "</tr>";
	$SummaryDisplayRows[] = $DisplayRow;	
	$SummaryTableRows[] = $TableRow;	
}
$ESAverageData = $TableRowData;
$criteria = new stdClass();
$critiera->date = date("Y-m-d");
$lastContractAverages = $forecastingProvider->lastAveragesContractsES($critiera);
if (count($lastContractAverages)){
	$donotAddLastContractAverages = true;
}
$SaveAsAverages = ($SaveAsAverages ? $SaveAsAverages : $_GET['SaveAsAverages']);
if ($SaveAsAverages){
	$donotAddLastContractAverages = false;
}
if (!$donotAddLastContractAverages){
	//first drop any averages for today already
	$dropContractAverages = $forecastingProvider->dropAveragesES();
	
	//Create ES Contract Averages
	foreach ($ESAverageData as $crew=>$averages){
		$ContractsObj = new stdClass();
		$Crew = explode("QQQ",str_replace("XX","",$crew));
		$ContractsObj->crew = $Crew[0];
		foreach ($averages as $key=>$val){
			$ContractsObj->$key = $val;
		}
		$insertContractAverages = $forecastingProvider->insertAveragesContractsES($ContractsObj);
		$ContractAverages[] = $insertContractAverages;
	}
	//Create ES ISM Averages
	foreach ($CrewISMTotalsAverage as $crew=>$isms){
		$Crew = explode("QQQ",str_replace("XX","",$crew));
		foreach ($ISMS as $ism=>$count){
			$displayVal = ($CrewISMTotalsAverage[$crew][$ism] > 0 ? $CrewISMTotalsAverage[$crew][$ism] : 0);
			$type = ($ism != "Bulbs" ? $ProductsByDescription[$ism] : "Bulbs");
			if (strpos(" ".$ism,"Revenue") || strpos(" ".$ism,"COGS")){
				$type = $ism;
			}
			$insertAverages = $forecastingProvider->insertAveragesES($Crew[0],$type,$displayVal);
		}
	}
	//Create Contractor Averages
	foreach ($ContractorTableRow as $Contrator=>$average){
		$ContractorObj = new stdClass();
		$ContractorObj->contractor = $Contrator;
		foreach ($average as $key=>$val){
			$ContractorObj->$key = $val;
		}
		$insertContractorAverages = $forecastingProvider->insertAveragesContractorsES($ContractorObj);
		$ContractorAverages[] = $insertContractorAverages;
	}
}

?>