<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ForecastingProvider.php");
$forecastingProvider = new ForecastingProvider($dataConn);

$scrollId = "";
$alertArray = array();

//check to see if file is submitted
if ($_POST['submitForm']=="Change"){
	$updateResult = $forecastingProvider->updateVariables($_POST);
	if ($updateResult->success){
		foreach ($_POST as $key => $val) {
            $$key = $val; // set the global config value
			$alertArray[$key] = "Variable has been updated.";
			if (count($alertArray) == 1){
				$scrollId = "form-".$key;
			}
		}
	}
}
if ($_POST['action']=="variable_add"){
	$insertResult = $forecastingProvider->insertVariables($_POST);
	if ($insertResult->success){
		$alertValueAdded = "Added ".$_POST["name"];
	}
}

$forecastingVariables = $forecastingProvider->getVariableDetails();
foreach ($forecastingVariables as $variableInfo){
	$VariablesArray[$variableInfo->name]["DefaultValue"]=$variableInfo->defaultValue;
	$VariablesArray[$variableInfo->name]["Name"]=$variableInfo->name;
	$VariablesArray[$variableInfo->name]["Description"]=$variableInfo->description;
	$VariablesArray[$variableInfo->name]["Type"]=$variableInfo->type;
	$VariablesArray[$variableInfo->name]["Options"]=$variableInfo->options;
	$VariablesArray[$variableInfo->name]["Category"]=$variableInfo->category;
}
?>

<h2>Forecasting Variables</h2>
<div class="alert"><?php echo $alertValueAdded;?></div>
<div class="fifteen columns">
	<div class="seven columns">
		<div id="variable-forms">
		<?php
			foreach ($VariablesArray as $key => $object) {
				$variableName = $object["Name"];
				$variableType = $object["Type"];
				$defaultValue = $object["DefaultValue"];
				$variableDesc = $object["Description"];
				$rawOptions = $object["Options"];
				$submitType = $variableType == "select" ? "hidden" : "submit";
				$alertValue = isset($alertArray[$key]) ? $alertArray[$key] : ""; ?>

			<?php if (!$ReadOnlyTrue){?>
			<form method="POST" id="form-<?php echo $key;?>">
			<?php }?>
				<fieldset>
					<label>
						<span title="<?php echo $variableName;?>" class="variableDesc"><?php echo $variableDesc;?></span>
						<div class="variableName" style="font-weight:normal;"><?php echo $variableName;?></div>
						<?php

						if ($variableType == "select") {
							$allOptions = explode(",", $rawOptions); ?>

						<select name="<?php echo $key;?>"><?php

							foreach ($allOptions as $option){
								$optionArray = explode("|",$option);
								$optionValue = $optionArray[0];
								$optionText = count($optionArray) > 1 ? $optionArray[1] : $optionValue;
								$selectedText = ($optionValue == $defaultValue) ? " selected" : ""; ?>

							<option value="<?php echo $optionValue;?>" <?php echo $selectedText;?>><?php echo $optionText;?></option><?php

							} ?>

						</select><?php

						} else if ($variableType == "number") { ?>

						<input type="text" name="<?php echo $key;?>" value="<?php echo $defaultValue;?>" class="numberValidate"><?php
						
						} else if ($variableType == "text") { ?>

						<input type="text" name="<?php echo $key;?>" value="<?php echo $defaultValue;?>"><?php

						} else if ($variableType == "textarea") { ?>

						<textarea cols="80" rows="6" name="<?php echo $key;?>"><?php echo $defaultValue;?></textarea><?php

						} else {
							echo $key." doesn't have a type?"; var_dump($object);
						} ?>

					</label>
						<?php if (!$ReadOnlyTrue){?>
							<input type="<?php echo $submitType; ?>" name="submitForm" value="Change">
						<?php }?>
					<div class="alert"><?php echo $alertValue;?></div>
				</fieldset>
			<?php if (!$ReadOnlyTrue){?>
			</form>
			<?php }?>
		<?php } // end foreach ?>
		</div>
	</div>
	<div class="seven columns">
		<form id="VariableAddForm" class="basic-form seven columns override_skel" method="post">
			<fieldset>
				<legend>New Variable Entry</legend>
				<div class="seven columns">
					<div class="three columns">Description:</div>
						<textarea class="four columns" name="description" id="VariableDescription"></textarea>

					<br clear="all">
					<div class="three columns">Name:</div>
					<div class="four columns"><input type="text" name="name" id="VariableName"></div>

					<br clear="all">
					<div class="three columns">Type:</div>
					<div class="four columns">
						<select id="Type" name="type">
							<option></option>
							<option value="text">text</option>
							<option value="number">number</option>
							<option value="select">select</option>
						</select>
					</div>
					
					<br clear="all">
					<div class="three columns">Default Value:</div>
					<div class="four columns"><input type="text" name="defaultValue"></div>
					
					<br clear="all">
					<div class="three columns">Category:</div>
					<div class="four columns">
						<select id="Category" name="category">
							<option></option>
							<option value="Variable">Variable</option>
							<option value="Constant">Constant</option>
							<option value="Percentage">Percentage</option>
							<option value="Assumption">Assumption</option>
						</select>
					</div>
					
				</div>
			</fieldset>
			<div id="trainingEntryResults" class="fifteen columns"></div>
				<div class="fifteen columns">
					<?php if (!$ReadOnlyTrue){?>
						<a href="save-variable-entry" id="save-variable-entry" class="button-link do-not-navigate">Add</a>
						<input type="hidden" name="action" value="variable_add">
					<?php }//if not read only ?>
				</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$(".variableName").hide();
		$(".variableDesc").click(function(){
			var $this = $(this);
			$(this).next().toggle();
		});
		var toCamelCase = function(str){
			return str.replace(/-\w/g,function(match){return match[1].toUpperCase()})
		},
		toTitleCase = function(str) {
			return str.replace(/(?:^|\s)\w/g, function(match) {
				return match.toUpperCase();
			});
		};
		
		var variableName = $("#VariableName"),
			variableDescription = $("#VariableDescription");
			
			variableDescription.keyup(function(){
			var DescriptionVal = $(this).val(),
				NameVal = DescriptionVal.replace(/(([^\s]+\s\s*){4})(.*)/,"$1");
				var thisVal = toTitleCase(NameVal).replace(/ /g,'');
				variableName.val(thisVal.replace(/[^\w\s]/gi, ''));
			});
	
		$('select').css('height','22px');
		var dropdownlist = $('select');
		dropdownlist.css('width','auto');
		$('input[type="text"]').css('height','22px');
		$('input[type="checkbox"]').css('width','13px');
		$(".input_text_tiny").css({'width':'25px','height':'20px'});
		$(".input_text_small").css({'width':'45px','height':'20px'});	
		
		var formsDiv = $("#variable-forms"),
            scrollElement = $("#<?php echo $scrollId;?>");

		formsDiv.on("focus", "input,textarea,select", function(){
			$(this).closest("form").find(".alert").html("");
		});
		formsDiv.on("change", "select", function(){
			var form = $(this).closest("form");
			form.find(".alert").html("Processing...");
			form.submit();
		});

        if (scrollElement.length){
            setTimeout(function(){
                $('html, body').animate({
                    scrollTop: scrollElement.offset().top - 20
                }, 500);
            }, 500)
        }
		
		$("#save-variable-entry").click(function(){
			$("#VariableAddForm").submit();
		});
	});
</script>