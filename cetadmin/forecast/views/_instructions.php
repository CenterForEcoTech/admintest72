<style>
.tableBorders {border-bottom:1pt solid black;}
</style>
<div class="eighteen columns">
		<div class="fourteen columns">
			There are four types of revenue types to be calculated: HEA Fees, ISMs, IIC Completion Fees, Inspection Fees.  What follows are instructions on how to bring this data into the forecasting models.  Further documentation provide in this <a href="<?php echo $CurrentServer;?>media/documents/ghsforecastingmodelinfo.pdf">PDF</a>
		</div>
		<div class="one columns">
			<a href="<?php echo $CurrentServer;?>media/documents/ghsforecastingmodelinfo.pdf"><img src="https://repos/admintest/images/file_pdf.png" title="GHS Forecast Documentation"></a><br>
		</div>
		<br>
		<fieldset>
			<legend>Methods for obtaining data</legend>
				<div class="seventeen columns tableBorders">
					<div class="two columns tableData">Source</div>
					<div class="six columns tableData">Revenue Type</div>
					<div class="six columns tableData">Obtained By</div>
				</div>
				<div class="seventeen columns tableBorders">
					<div class="two columns tableData">HUB-report</div>
					<div class="six columns tableData">HEA & Inspection Fees(NGRID, WMECO, and BGAS) Note: this pulls from the schedule</div>
					<div class="six columns tableData">Run a report called reports->Schedule-List of Schedules. Sort by Crew with date range then filter Crew for 'CET'</div>
				</div>
				<div class="seventeen columns tableBorders">
					<div class="two columns tableData">SharePoint</div>
					<div class="six columns tableData">Contract Fee</div>
					<div class="six columns tableData">Updated by staff and pulled into forecasting system every hour.</div>
				</div>
				<div class="seventeen columns tableBorders">
					<div class="two columns tableData">ISM Installations (Bulb Report)</div>
					<div class="six columns tableData">ISMs</div>
					<div class="six columns tableData">CSG Provides a daily file that is imported daily automatically and once a month a Bulb Report that is imported.</div>
				</div>
				<!--
				<tr class="fourteen columns">
					<td valign="top" class="two columns">Extract File</td>
					<td valign="top" class="six columns">HEA Fees (NGRID and WMEC)<br>ISMs (NGRID and WMEC)<br>IIC Completion Fees (NGRID and WMEC)<br>Inspections Fees (NGRID)</td>
					<td valign="top" class="five columns">Email from CSG to Julie Craumer and there is little we can do other than wait to receive that file.</td>
				</tr>
				
				<tr class="fourteen columns">
					<td valign="top" class="two columns">HUB-action</td>
					<td valign="top" class="six columns">HEA Fees (BGAS)<br>ISMs (BGAS)<br>IIC Completion Fees (BGAS)</td>
					<td valign="top" class="five columns">Run an action called actions->generate invoice reports->BGAS extract.  Use an appropriate date range.  This report may take an hour to generate so be patient.</td>
				</tr>
				<tr class="fourteen columns">
					<td valign="top" class="two columns">MRV File</td>
					<td valign="top" class="six columns">Inspection Fees (count of Mechanical Rebate Inspections for NGRID and WMECO)</td>
					<td valign="top" class="five columns">Email from CSG to Julie Craumer.</td>
				</tr>
				-->				
		</fieldset>
		<br>
<!--		
		<fieldset>
			<legend>Extract File</legend>
			<style>ol {list-style-type:decimal;}</style>
			<ol>
				<li>Save extract data as CSV
				<li>Using PHPMyAdmin import csv file using 1st line as column headers_list
				<li>Add column to this new table ID as Int NULL
				<li>Move data from this new table to csg_extract_data
				<li>Drop this new table
			</ol>
		</fieldset>
		<fieldset>
			<legend>HUB-action</legend>
			<ol>
				<li>Save HUB data as CSV
				<li>Format APPTDATE, ISSUED_DT, SIGNED_DT, INSTALL_DT to YYY-MM-DD
				<li>Format DEPOSIT, CUST_PRICE, UTIL_PRICE as decimal
				<li>Using PHPMyAdmin import csv file using 1st line as column headers_list
				<li>Add column to this new table ID as Int NULL
				<li>Move data only from this new table to csg_hub_bgextract_data
				<li>Drop this new table
			</ol>
		</fieldset>
		<fieldset>
			<legend>MRV</legend>
			<form id="mrvForm">
				Manually Enter in the count here:<br>(this count will apply for both NGRID and WMECO)<br>
				<div class="three columns">Utility:<select name="utility"><option></option><option value="NATGRID">NATGRID</option><option value="WMECO">WMECO</option></select></div>
				<div class="three columns">Date:<input type="txt" class="date" name="countDate"></div>
				<div class="two columns">Count:<input type="text" class="input_text_tiny" name="countQty"></div>
				
				<div class="three columns">
					<?php if (!$ReadOnlyTrue){?>
						<a href="save-mrv-count" id="save-mrv-count" class="button-link do-not-navigate">Add</a>
						<input type="hidden" name="action" value="mrvcount_add">
					<?php }//if not read only ?>
				</div>
				<div id="mrvEntryResults" class="six columns"></div>
			</form>
			
			
		</fieldset>
-->
		<fieldset>
			<legend>HUB-report</legend>
			<ol>
				<li>Export HUB data and save as .csv file type to your computer.
				<li>Open <a href="?nav=forecast-apptanalysis">Appt Type Analysis</a> and click the button for <a href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=edit-file&id=15" class="button-link">Add Schedule Data</a>
				<li>Click the 'Select File' (green button) and select the HUB data file you just saved to your computer.
				<li>Click the 'Save' (blue button).
				<li>The page will reload with the new data and automatically import new records and clean out any duplicate records.
			</ol>
		</fieldset>
		<fieldset>
			<legend>SharePoint</legend>
			<ol>
				<li>No Outside Effort Required to Obtain Data
			</ol>
		</fieldset>
		<fieldset>
			<legend>ISM Installs/Bulb Report</legend>
			<ol>
				<li>No Outside Effort Required to Obtain Data
			</ol>
		</fieldset>

<script>
$(function(){
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
		
	var formElement = $("#mrvForm");
	 
	formElement.validate({
        rules: {
            countDate: {
                required: true
            },
            countQty: {
                required: true
            }
        }
    });
	
	
    $("#save-mrv-count").click(function(e){

        $('#mrvEntryResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiForecasting.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#mrvEntryResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#mrvEntryResults').html(message).addClass("alert");
                }
            });
        }
    });

});
</script>