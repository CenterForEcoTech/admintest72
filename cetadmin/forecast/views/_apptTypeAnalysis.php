<?php
error_reporting(1);
ini_set('max_execution_time', 1200); //20 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);
$ThisMonth = date("m/01/Y");
$NextMonth = date("m/t/Y",strtotime($TodaysDate."+1 month"));
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $ThisMonth);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $NextMonth);

$criteria = new stdClass();
$criteria->startDate = $StartDate;
$criteria->endDate = $EndDate;
$results = $CSGDataProvider->getApptTypeAnlysis($criteria);
//print_pre($results);
if (count($results)){
	//remove the file so there should only be one schedulereport file in the directory
	foreach ($results as $ID=>$Data){
		$ApptTypeArray[$Data["yearMonth"]][$Data["apptType"]][$Data["asOfDate"]] = $Data["count"];
		$ApptTypeAsOfDatesRaw[$Data["yearMonth"]][]=$Data["asOfDate"];
		$ApptTypeTotal[$Data["yearMonth"]][$Data["apptType"]]= $Data["count"];
		$AsofDateTypeTotal[$Data["apptType"]][$Data["asOfDate"]]= $AsofDateTypeTotal[$Data["apptType"]][$Data["asOfDate"]]+$Data["count"];
		$AsOfDatesRaw[] = $Data["asOfDate"];
	}

	//Remove duplicates
	foreach ($ApptTypeAsOfDatesRaw as $YearMonth=>$AsOfDates){
		$cleanAsOfDates = array_unique($AsOfDates);
		$orderedCleanAsOfDates = asort($cleanAsOfDates);

		$ApptTypeAsOfDates[$YearMonth] = $cleanAsOfDates;
	}
	$AsOfDates = array_unique($AsOfDatesRaw);
	
}
$HighlightedTypes = array("HEA","HEA (open slots)","Unplanned Loss Appt","Inspection","Inspection (open slots)","Return Visit","Special Home Visit");

?>

	<script src="<?php echo $CurrentServer.$adminFolder;?>js/highstock/highstock.js"></script>
	<script src="<?php echo $CurrentServer.$adminFolder;?>js/highstock/modules/exporting.js"></script>
	<br clear="all">
	<div id="container" class="highchart"></div>
	<div id="containerChart" class="highchart"></div>
	<script type="text/javascript">
	$(function () {
	  var months = ['start','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var timeConverter = function(UNIX_timestamp,showWhat){
		  var a = new Date(UNIX_timestamp);
			var monthlist = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		  var year = a.getFullYear();
		  var month = monthlist[a.getMonth()];
		  var date = a.getDate();
		  var hour = a.getHours();
		  var min = a.getMinutes();
		  var sec = a.getSeconds();
		  var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		  var monthYear = month + ' ' + year;
			if (showWhat == "mmYY"){
				return monthYear;
			}else{
				return time;
			}
		},
		formatNumber = function(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
		};	
		var seriesOptions = [],
			seriesCounter = 0,
			names = ['<?php 
						$months[] = date("Y-m-01", strtotime( date( 'Y-m-01' )." +1 month"));
						for ($i = 0; $i <= 11; $i++) {
							$months[] = date("Y-m-01", strtotime( date( 'Y-m-01' )." -".$i." months"));
						}
					sort($months);
					echo implode("','",$months);
					?>'],
			now = new Date().getTime(),

			// create the chart when all data is loaded
			createChart = function () {

				$('#containerChart').highcharts('StockChart', {
					chart: {
						height: 530
					},
					plotOptions: {
						series: {
							marker: {
								enabled: true
							}
						}
					},
					title:{
						text:'HEA Scheduling Trend'
					},
					credits:{enabled:false},
					rangeSelector: {
						inputEnabled: $('#containerChart').width() > 480,
						selected: 10
					},
					legend:{
						enabled: true,
						layout:'vertical',
						align:'left',
						verticalAlign:'top',
						backgroundColor:'#fff',
						borderColor:'#ccc',
						borderWidth:.5,
						y:30,
						x:0,
						itemWidth:135,
						itemStyle:{
							fontWeight:'bold'
						},
						itemHiddenStyle:{
							fontWeight:'bold'
						},
						title: {
							text: 'Month<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
							style: {
								fontStyle: 'italic'
							}
						}
					},
					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: 'silver'
						}]
					},


					tooltip: {
						pointFormat: '<span style="color:{series.color}">{series.name}{series.yosh}</span>: <b>{point.y}</b><br/>',
						valueDecimals: 2
					},
					series: seriesOptions
				});
			};
			
		
		$.each(names, function (i, name) {
			var YearMonth = name,
				YearMonthParts = name.split('-'),
				MonthNumber = parseInt(YearMonthParts[1]),
				seriesName = months[MonthNumber]+" "+YearMonthParts[0],
				visibility = true,
				linewidth = 2;
			var url = '<?php echo $CurrentServer.$adminFolder;?>stockchart/data.php?type=forecast&report=apptanalysis&StartDate='+YearMonth+'&callback=?';
				$.getJSON(url,    function (data) {
					seriesOptions[i] = {
						name: seriesName,
						visible: visibility,
						data: data,
						lineWidth: linewidth					
					};

					// As we're loading the data asynchronously, we don't know what order it will arrive. So
					// we keep a counter and create the chart when all the data is loaded.
					seriesCounter += 1;

					if (seriesCounter === names.length) {
						createChart();
					}
				});
		});
	});
	</script>




<script>
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				filterValues = new Array(startDate,endDate);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>forecast/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&nav=<?php echo $nav;?>#";
		}
		$("button").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			updateFilters();
			
		});
		
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"bJQueryUI": true,
			"sScrollX":	"100%",
			"bScrollCollapse": true,
			"bPaginate":	false,
			"aoColumnDefs": [
                { "bWidth": '20%', "aTargets": [0] }
            ]
			
		});
<?php if (count($ApptTypeArray)){?>
		new $.fn.dataTable.FixedColumns( table );
<?php }?>
		
		var tableTotals = $('#Totals').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"bJQueryUI": true,
			"sScrollX":	"100%",
			"bScrollCollapse": true,
			"bPaginate":	false,
			"aoColumnDefs": [
                { "bWidth": '20%', "aTargets": [0] }
            ]
			
		});
<?php if (count($ApptTypeArray)){?>
		new $.fn.dataTable.FixedColumns( tableTotals );
<?php }?>
	});
</script>
<style>
.ApptDataHeader {border-right:0pt solid blue;font-size:10pt;}
.ApptTotal {border-left:1pt solid black;}
.ApptType {min-width:175px;}
.HighlightedTypes {background-color:#c2c2ff;border-top:1pt solid #8a8ae6;}
</style>
<h3>Appt Type Analysis 			<?php if (!$ReadOnlyTrue){?><a href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=edit-file&id=15" class="button-link">Add Schedule Data</a><?php }?></h3>
<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
<button>reset filters</button>
<?php if (count($ApptTypeArray)){?>
	<?php foreach ($ApptTypeArray as $YearMonth=>$data){?>
			<br clear="all">
			<h2><?php echo date("F Y",strtotime($YearMonth));?></h2>
			<table class="reportResults display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="ApptType" style="box-sizing:border-box;border:0pt;">Appt Type</th>
						<?php 
							foreach ($ApptTypeAsOfDates[$YearMonth] as $AsOfDate){
								echo "<th class=\"ApptDataHeader\">".date("m/d/y",strtotime($AsOfDate))."</th>";
							}
						?>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($data as $apptType=>$value){
							$classType = "";
							if (in_array($apptType,$HighlightedTypes)){
								$classType = " HighlightedTypes";
							}
							echo "<tr><td class=\"ApptType".$classType."\">".$apptType."</td>";
							foreach($ApptTypeAsOfDates[$YearMonth] as $AsOfDate){
								echo "<td align=\"center\" class=\"ApptData".$classType."\">".$ApptTypeArray[$YearMonth][$apptType][$AsOfDate]."</td>";
							}
							echo "<td class=\"ApptTotal".$classType."\" align=\"center\">".$ApptTypeTotal[$YearMonth][$apptType]."</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
	<?php } // foreach ApptTypeSorted 
	//Also include Totals:
	?>
	<br clear="all">
		<h2>Totals</h2>
			<table id="Totals" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="ApptType" style="box-sizing:border-box;border:0pt;">Appt Type</th>
						<?php 

							foreach ($AsOfDates as $AsOfDate){
								echo "<th class=\"ApptDataHeader\">".date("m/d/y",strtotime($AsOfDate))."</th>";
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($AsofDateTypeTotal as $apptType=>$asOfDateInfo){
							$classType = "";
							if (in_array($apptType,$HighlightedTypes)){
								$classType = " HighlightedTypes";
							}
							echo "<tr><td class=\"ApptType".$classType."\">".$apptType."</td>";
							foreach($AsOfDates as $AsOfDate){
								echo "<td align=\"center\" class=\"ApptData".$classType."\">".$AsofDateTypeTotal[$apptType][$AsOfDate]."</td>";
							}
							echo "</tr>";
						}
					?>
				</tbody>
			</table>

<?php 
}else{// end if count ?>
<br>There is no Schedule Data for this time period.  Please Add Schedule Data or choose an earlier time period.
<?php }?>