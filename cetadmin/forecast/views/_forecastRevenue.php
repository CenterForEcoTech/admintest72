<?php
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : date("m/1/Y",strtotime($TodaysDate)));
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : date("m/t/Y",strtotime($TodaysDate." +1 month")));
$ExtrapolatedDataCurrentMonth = ($_GET['includeExtrapolatedDataCurrentMonth'] ? " checked" : "");
$ExtrapolatedDataFutureMonth=($_GET['includeExtrapolatedDataFutureMonth'] ? " checked" : "");
$OtherRevenueCurrentMonth = ($_GET['otherRevenueCurrentMonth'] ? $_GET['otherRevenueCurrentMonth'] : "5000");
$OtherRevenueFutureMonth=($_GET['otherRevenueFutureMonth'] ? $_GET['otherRevenueFutureMonth'] : "5000");

//get Expenses for lastthree months to create moving average of monthly expenses
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ForecastingProvider.php");
$forecastingProvider = new ForecastingProvider($dataConn);

$criteria = new stdClass();
$criteria->startDate = date("m/1/Y",strtotime($TodaysDate." -4 months"));
$criteria->endDate = date("m/1/Y",strtotime($TodaysDate." -1 month"));
$PLresultArray = $forecastingProvider->getPL($criteria);
foreach ($PLresultArray as $column=>$data){
	if ($data->type == "Total Expenses"){
		$Expenses[] = round($data->actual,0,PHP_ROUND_HALF_UP);
	}
}
$AverageMonthlyExpense = ceil(array_sum($Expenses)/count($Expenses));
$ExpensesCurrentMonth = ($_GET['expensesCurrentMonth'] ? $_GET['expensesCurrentMonth'] : $AverageMonthlyExpense);
$ExpensesFutureMonth=($_GET['expensesFutureMonth'] ? $_GET['expensesFutureMonth'] : $AverageMonthlyExpense);

?>
<link rel="stylesheet" href="<?php echo $CurrentServer.$adminFolder;?>css/jquery.fileupload-ui.css">
<style>
legend {font-weight:bold;}
.factorHeader {
	border-bottom:1pt solid black;font-weight:bold;
}
.smallInput {width:70px;}
.smallFont {font-size:10pt;}
</style>
<div class="five columns"><h3>Forecast Revenue Data</h3></div>
<br clear="all">
<div class="eight columns">
	<fieldset>
		<legend>Factors</legend>
		<div class="seven columns">
			<div class="two columns factorHeader smallFont">Factor</div>
			<div class="two columns factorHeader smallFont">First Month</div>
			<div class="two columns factorHeader smallFont">Other Month</div>
		</div>
		<div class="seven columns">
			<div class="two columns smallFont">Date Range:</div>
			<div class="two columns">
				<input type="text" class="date auto-watermarked FilterParam" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
			</div>
			<div class="two columns">
				<input type="text" class="date auto-watermarked FilterParam" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date"><br>
			</div>
		</div>
		<div class="seven columns">
			<div class="two columns smallFont">Extrapolate:</div>
			<div class="two columns">
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataCurrentMonth"<?php echo (trim($ExtrapolatedDataCurrentMonth) == "checked" ? $ExtrapolatedDataCurrentMonth." value=\"Yes\"" : " value=\"No\"");?>>
			</div>
			<div class="two columns">
				<input type="checkbox" class="FilterParam" id="includeExtrapolatedDataFutureMonth"<?php echo (trim($ExtrapolatedDataFutureMonth) == "checked" ? $ExtrapolatedDataFutureMonth." value=\"Yes\"" : " value=\"No\"");?>>
			</div>
		</div>
		<div class="seven columns">
			<div class="two columns smallFont">Other Revenue:</div>
			<div class="two columns">
				<span style="float:left;">$</span><input type="input" class="auto-watermarked FilterParam smallInput" id="otherRevenueCurrentMonth" value="<?php echo $OtherRevenueCurrentMonth;?>" title="<?php echo date("F",strtotime($StartDate));?>">
			</div>
			<div class="two columns">
				<span style="float:left;">$</span><input type="input" class="auto-watermarked FilterParam smallInput" id="otherRevenueFutureMonth" value="<?php echo $OtherRevenueFutureMonth;?>" title="Future Months">
			</div>
		</div>
		<div class="seven columns">
			<div class="two columns smallFont">Expenses*:</div>
			<div class="two columns">
				<span style="float:left;">$</span><input type="input" class="auto-watermarked FilterParam smallInput" id="expensesCurrentMonth" value="<?php echo $ExpensesCurrentMonth;?>" title="<?php echo date("F",strtotime($StartDate));?>">
			</div>
			<div class="two columns">
				<span style="float:left;">$</span><input type="input" class="auto-watermarked FilterParam smallInput" id="expensesFutureMonth" value="<?php echo $ExpensesFutureMonth;?>" title="Future Months">
			</div>
		</div>
	</fieldset>
	<div class="six columns">
		<?php if (!$ReadOnlyTrue){?>
			<button class="btn btn-primary" id="saveforecast">
				<span>Save Forecast Data as Shown</span>
			</button>	
		<?php }?>
	</div>
	<br clear="all">
	<div class="six columns" id="saveMessage">&nbsp;</div>
</div>
<div class="twenty columns">
	<fieldset>
		<legend>Revenues</legend>
		<div class="fourteen columns">
			<?php //determine the number of months in range
				$nextMonthName = date("F",strtotime($StartDate));
				$nextMonthCounter = 0;
				$lastMonthName = date("F",strtotime($EndDate));
				$otherRevenue = $OtherRevenueCurrentMonth;
				$expenses = $ExpensesCurrentMonth;
				do{
					$ThisMonth = date("n",strtotime($StartDate." +".$nextMonthCounter." month"));
					$nextMonthName = date("F",strtotime($StartDate." +".$nextMonthCounter." month"));
					$ajaxLoader = "<img src=\"".$CurrentServer."images/ajaxLoaderSmall.gif\">";
					$MonthHolder = "<div class=\"three columns\">
										<div class=\"three columns\">".date("F",strtotime($StartDate." +".$nextMonthCounter." month"))."</div><Br clear=\"all\">
										<div class=\"three columns\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."HEAFees\">".$ajaxLoader."</span></div>
										<div class=\"three columns\"><span class=\"results isms\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."ISMs\">".$ajaxLoader."</span></div>
										<div class=\"three columns\"><span class=\"results contracts\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."Contracts\">".$ajaxLoader."</span></div>
										<div class=\"three columns\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."InspectionFees\">".$ajaxLoader."</span></div>
										<div class=\"three columns\"><span class=\"results otherrevenue\" data-amount=\"".$otherRevenue."\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."OtherRevenue\">$".money($otherRevenue,true,true)."</span></div>
										<div class=\"three columns\"><span class=\"results totals\" data-amount=\"".$otherRevenue."\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."Totals\">$".money($otherRevenue,true,true)."</span></div>
										<br clear=\"all\"><br>
										<div class=\"three columns\"><span class=\"results COGS\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."COGS\">".$ajaxLoader."</span></div>
										<br clear=\"all\"><br>
										<div class=\"three columns\"><span class=\"results Expenses\" data-amount=\"".$expenses."\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."Expenses\">$".money($expenses,true,true)."</span></div>
										<br clear=\"all\"><br>
										<div class=\"three columns\" style=\"display:none\"><span class=\"results NetIncome\" data-amount=\"0\" data-yearmonth=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."\" id=\"".date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"))."NetIncome\">".$ajaxLoader."</span></div>
									</div>";
					$otherRevenue = $OtherRevenueFutureMonth;
					$expenses = $ExpensesFutureMonth;
					$RevenueHolders[] = $MonthHolder;
					$nextMonthCounter++;				
				} while ( $nextMonthName != $lastMonthName);
			?>
				
			<div class="four columns"><br>
				<div class="four columns">Home Energy Assess Fees</div>
				<div class="four columns">ISMs</div>
				<div class="four columns">IIC Contract Comp Fees</div>
				<div class="four columns">Inspection Fees</div>
				<div class="four columns">Other Revenue</div>
				<div class="four columns">Totals</div>
				<br clear="all"><br>
				<div class="four columns">COGS</div>
				<br clear="all"><br>
				<div class="four columns">Expenses</div>
				<br clear="all"><br>
				<div class="four columns" style="display:none;">Net Income</div>
			</div>
			<?php
				foreach ($RevenueHolders as $RevenueHolder){
					echo $RevenueHolder;
				}
			?>
		</div>
	</fieldset>
	<br clear="all">
	<div class="ten columsn">
		<fieldset>
			<legend>Notes</legend>
			*Suggested Expenses are calculated from prior three month average
		</fieldset>
	</div>
</div>
<br clear="all">
<br>
<button class="btn btn-success" id="ShowRevenueDetails">Show Revenue Details</button>
<div id="RevenueDetails">
	<?php $IncludeRevenueForecast = true;?>
	<?php include_once('models/_revenueHEAFeesDisplay.php');?>
	<?php include_once('models/_revenueContractsToCompleteDisplay.php');?>
	<?php include_once('models/_revenueISMsDisplay.php');?>
</div>
<script>
	$(document).ready(function() {
		
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setMonth(date2.getMonth() +1);
				date2.setDate(date2.getDate() -1);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var FilterParams = $(".FilterParam");
			FilterParams.on("click blur",function(){
			updateFilters();
		});
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				includeExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
				includeExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0),
				otherRevenueCurrentMonth = $("#otherRevenueCurrentMonth").val(),
				otherRevenueFutureMonth = $("#otherRevenueFutureMonth").val(),
				expensesCurrentMonth = $("#expensesCurrentMonth").val(),
				expensesFutureMonth = $("#expensesFutureMonth").val(),
				filterValues = new Array(startDate,endDate,includeExtrapolatedDataCurrentMonth,includeExtrapolatedDataFutureMonth,otherRevenueCurrentMonth,otherRevenueFutureMonth,expensesCurrentMonth,expensesFutureMonth);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters(),
				Params = "StartDate="+Filters[0]+
						"&EndDate="+Filters[1]+
						"&includeExtrapolatedDataCurrentMonth="+Filters[2]+
						"&includeExtrapolatedDataFutureMonth="+Filters[3]+
						"&otherRevenueCurrentMonth="+Filters[4]+
						"&otherRevenueFutureMonth="+Filters[5]+
						"&expensesCurrentMonth="+Filters[6]+
						"&expensesFutureMonth="+Filters[7];
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>forecast/?"+Params+"&nav=<?php echo $nav;?>#";
		}
		$("#saveforecast").on('click',function(){
				var results = $(".results");
				var ready = true;
				var plData = "";
				var TotalAmount = 0;
				var YearMonthAmount = {};
				results.each(function() {
					var $this = $(this),
						YearMonth = $this.attr('data-yearmonth'),
						thisType = $this.attr('id').replace(YearMonth,""),
						Amount = parseInt($this.attr('data-amount'));
						html = $this.html();
						if (html == '<?php echo $ajaxLoader;?>'){
							ready = false;
						}else{
							if (thisType == "COGS" || thisType == "Expenses"){
								thisAmount = -Amount;
							}else if (thisType == "NetIncome" || thisType== "Totals"){
								thisAmount = 0;
							}else{
								thisAmount = Amount;
							}
							if (parseInt(YearMonthAmount[YearMonth]) == "NaN" || typeof YearMonthAmount[YearMonth] == "undefined"){
								YearMonthAmount[YearMonth] = new Array();
								YearMonthAmount[YearMonth]=0;
							}
							YearMonthAmount[YearMonth] += thisAmount;
							thisNetIncome = "#"+YearMonth+"NetIncome";
							$(thisNetIncome).html(YearMonthAmount[YearMonth]);
							$(thisNetIncome).attr('data-amount',YearMonthAmount[YearMonth]);
							
								plData = plData+YearMonth+"_"+thisType+"_"+Amount+"|";
						}
						
				});
				var ForeCastVariables = {};
				FilterParams.each(function(){
					var $this = $(this),
						thisId = $this.attr('id'),
						thisValue = $this.val();
					ForeCastVariables[thisId] = thisValue;
					
				});
				if (!ready){
					$('#saveMessage').html("please wait until page finishes loading").addClass("alert").fadeOut(3000);
				}else{
					var sendData = JSON.stringify({action:"pl_add",plData:plData,plVariables:ForeCastVariables});
					$.ajax({
						url: "<?php echo $CurrentServer.$adminFolder."forecast/ApiForecasting.php";?>",
						type: "PUT",
						data: sendData,
						success: function(data){
							$('#saveMessage').html(data).removeClass("alert").addClass("alert").addClass("info").fadeIn();
							$("#saveforecast").fadeOut( "slow" );
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#saveMessage').html(message).removeClass("info").addClass("alert");
						}
					});					
				}
			
			
		});
		var RevenueDetails = $("#RevenueDetails"),
			ShowRevenueDetails = $("#ShowRevenueDetails");
			RevenueDetails.hide();
		ShowRevenueDetails.on('click',function(){
			$(this).text(function(i, text){
				return text === "Show Revenue Details" ? "Hide Revenue Details" : "Show Revenue Details";
			})
			RevenueDetails.slideToggle("slow");
		});	
	});
	
</script>