<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ForecastingProvider.php");

$forecastingProvider = new ForecastingProvider($dataConn);
$forecastingVariables = $forecastingProvider->getVariables();
$categoryTypeResults = $forecastingProvider->getPLCategoryTypes();
$resultArray = $paginationResult->collection;

foreach ($categoryTypeResults as $result){
	$CategoryTypes[] = trim($result["category"])."_".trim($result["type"]);
}
$CurrentMonth = date("n",strtotime($TodaysDate));
$CurrentYear = date("Y",strtotime($TodaysDate));
$FiscalYear = ($CurrentMonth < 11 ? ($CurrentYear-1) : $CurrentYear);
$FYStart = "11/1/".$FiscalYear;
$FYEnd = "10/31/".($FiscalYear+1);
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $FYStart);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $FYEnd);

$criteria = new stdClass();
$criteria->startDate = $StartDate;
$criteria->endDate = $EndDate;
$PLresultArray = $forecastingProvider->getPL($criteria);
foreach ($PLresultArray[0] as $varName=>$varValue){
	if (strpos($varName,"_")){
		$columnName = explode("_",$varName);
	}else{
		$columnName[1]=$varName;
	}
	$TableColumns[] = $columnName[1];
}
//print_pre($PLresultArray);
foreach ($PLresultArray as $column=>$data){
	$date = date("Y-m-01",strtotime($data->date));
	$type = $data->type;
	$forecast = round($data->forecast,0,PHP_ROUND_HALF_UP);
	$amount = round($data->actual,0,PHP_ROUND_HALF_UP);
	$budget = round($data->budget,0,PHP_ROUND_HALF_UP);
	$forecastInfo = false;
	if (!$amount && $forecast > 0){
		$amount = $forecast;
		$forecastInfo = true;
	}else{
		$RevenuesYTD[$type]["actual"] = $RevenuesYTD[$type]["actual"]+$amount;
		$RevenuesYTD[$type]["forecast"] = $RevenuesYTD[$type]["forecast"]+$forecast;
	}
	$forecastAsOfDate = MySQLDate($data->forecastAsOfDate);
	$addTotal = $amount;
	$addExpenses = 0;
	$addBudgetTotal = $budget;
	$addBudgetExpenses = 0;
	switch ($type){
		case "HEA Fees":
			$type = "HEAFees";
			break;
		case "ISMS":
			$type = "ISMs";
			break;
		case "IIC Contract Fees":
			$type = "Contracts";
			break;
		case "Inspection Fees":
			$type = "InspectionFees";
			break;
		case "Other Revenue":
			$type = "OtherRevenue";
			break;
		case "Total COGS":
			$type = "COGS";
			$addTotal = 0;
			$addExpenses = $amount;
			$addBudgetTotal = 0;
			$addBudgetExpenses = $budget;
			break;							
		case "Total Expenses":
			$type = "Expenses";
			$addTotal = 0;
			$addExpenses = $amount;
			$addBudgetTotal = 0;
			$addBudgetExpenses = $budget;
			break;							
		case "Net Income":
			$type = "NetIncome";
			$addTotal = 0;
			$addBudgetTotal = 0;
			break;							
		default:
			$type = $type;
			$addTotal = 0;
			$addBudgetTotal = 0;
			break;
	}
	$Revenues[$date]["forecastAsOfDate"]=$forecastAsOfDate;
	$Revenues[$date]["forecastInfo"]=$forecastInfo;
	$Revenues[$date]["forecastAmount"][$type]=$forecast;
	$Revenues[$date][$type]=$amount;
	$Revenues[$date]["Totals"]=$Revenues[$date]["Totals"]+$addTotal;
	$Revenues[$date]["GrossProfit"]=($Revenues[$date]["Totals"]-$Revenues[$date]["COGS"]);
	if ($budget > 0){
		$RevenueBudget[$date][$type]=$budget;
		$RevenueBudget[$date]["Totals"]=$RevenueBudget[$date]["Totals"]+$addBudgetTotal;
		$RevenueBudget[$date]["GrossProfit"]=($RevenueBudget[$date]["Totals"]-$RevenueBudget[$date]["COGS"]);
	}
}
//print_pre($Revenues["2016-01-01"]);
?>
<style>
.scrolldiv {overflow-x:scroll;}
.forecastInfo {background-color:#d7ebf9;border: 1px solid #aed0ea;}
.YTD {background-color:#c2c2ff;border: 1px solid #8a8ae6;}
.totalRevenue {
		border-top:1pt solid black;width:80%!important;
		border-bottom:1pt solid black;width:80%!important;
	}
.grossProfit {
		border-top:1pt solid black;width:80%!important;
		border-bottom:1pt solid black;width:80%!important;
	}
.netIncome {
		font-weight:bold;
		border-top:1pt solid black;width:80%!important;
		border-bottom:4px double black;width:80%!important;
	}
.revenueBudget {border:1pt solid #D8D8DC;background-color:#F0F0F5;display:none;}
.revenueActual {width;450px;}
.revenueBudget input {width:70px;height:20px;}
.monthLabel {border:0pt solid black;float:left;}
.monthComment {border:0pt solid black;float:left;margin-top:5px;margin-left:-5px;cursor:pointer;}
#commentTextArea {width:100%;}
#commentLastEditedBy {font-weight:normal;font-style: italic;font-size:10pt;text-align:right;float:right;width:100%;}
</style>
	<!--<button id="addRawData">add raw data</button>-->
	<div class="six columns">
		<h3>Profit and Loss</h3>
		<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
		<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
		<a href="show-budget" id="showBudget" class="button-link do-not-navigate parameters" title="Toggle Budgets">Toggle Budgets</a><br>
	</div>
	<div class="nine columns" id="commentDisplay" style="display:none;">
		<input type="hidden" id="commentCurrentDisplay" value="">
		<textarea id="commentTextArea"></textarea><br>
		<span id="commentLastEditedBy"></span>
	</div>
	<div class="sixteen columns">
		<fieldset>
			<legend>P&L Historic and Forecasted</legend>
				<div class="three columns"><br>
					<div class="four columns">HEA Fees</div>
					<div class="four columns">ISMs</div>
					<div class="four columns">Contract Comp Fees</div>
					<div class="four columns">Inspection Fees</div>
					<div class="four columns">Other Revenue</div>
					<div class="four columns totalRevenue">Total Revenue</div>
					<br clear="all">
					<br clear="all">
					<div class="four columns">COGS</div>
					<div class="four columns grossProfit">Gross Profit</div>
					<br clear="all">
					<br clear="all">
					<div class="four columns">Expenses</div>
					<div class="four columns netIncome">Net Income</div>
					<br clear="all">
					<br clear="all">
				</div>
			<div class="twelve columns scrolldiv ">
				<div style="width:<?php echo ceil((count($Revenues)/5))+2;?>000px;">
					<?php //determine the number of months in range
						$nextMonthName = date("FY",strtotime($StartDate));
						$nextMonthCounter = 0;
						$lastMonthName = date("FY",strtotime($EndDate));
						do{
							$ThisMonth = date("n",strtotime($StartDate." +".$nextMonthCounter." month"));
							$nextMonthName = date("FY",strtotime($StartDate." +".$nextMonthCounter." month"));
							$ThisDate = date("Y-m-01",strtotime($StartDate." +".$nextMonthCounter." month"));
							$columnClass = "";
							$columnTitle = "";
							if ($Revenues[$ThisDate]["forecastAsOfDate"]){
								$columnTitle = " title=\"Forecast As Of ".$Revenues[$ThisDate]["forecastAsOfDate"]."\"";
							}
							if ($Revenues[$ThisDate]["forecastInfo"]){
								$columnClass = " forecastInfo";
							}
							$MonthHolder = "<div class=\"two columns".$columnClass."\"".$columnTitle."\">
												<div class=\"three columns\"><span class=\"monthLabel\">".date("M Y",strtotime($StartDate." +".$nextMonthCounter." month"))."</span><span id=\"Comment".$ThisDate."\" class=\"ui-icon ui-icon-triangle-1-se monthComment\"></span></div><Br clear=\"all\">
												<div class=\"three columns\" title=\"HEA Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["HEAFees"],true,true)."\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."HEAFees\">$".money($Revenues[$ThisDate]["HEAFees"],true,true)."</span></div>
												<div class=\"three columns\" title=\"ISMs - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["ISMs"],true,true)."\"><span class=\"results isms\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."ISMs\">$".money($Revenues[$ThisDate]["ISMs"],true,true)."</span></div>
												<div class=\"three columns\" title=\"IIC Contract Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["Contracts"],true,true)."\"><span class=\"results contracts\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Contracts\">$".money($Revenues[$ThisDate]["Contracts"],true,true)."</span></div>
												<div class=\"three columns\" title=\"Inspection Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["InspectionFees"],true,true)."\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."InspectionFees\">$".money($Revenues[$ThisDate]["InspectionFees"],true,true)."</span></div>
												<div class=\"three columns\" title=\"Other Revenue - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["OtherRevenue"],true,true)."\"><span class=\"results otherrevenue\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."OtherRevenue\">$".money($Revenues[$ThisDate]["OtherRevenue"],true,true)."</span></div>
												<div class=\"three columns totalRevenue\" title=\"Total Revenue\"><span class=\"results totals\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Totals\">$".money($Revenues[$ThisDate]["Totals"],true,true)."</span></div>
												<br clear=\"all\">
												<br clear=\"all\">
												<div class=\"three columns\" title=\"COGS - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["COGS"],true,true)."\"><span class=\"COGS\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."COGS\">$".money($Revenues[$ThisDate]["COGS"],true,true)."</span></div>
												<div class=\"three columns grossProfit\" title=\"Gross Profit\"><span data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Totals\">$".money($Revenues[$ThisDate]["GrossProfit"],true,true)."</span></div>
												<br clear=\"all\">
												<br clear=\"all\">
												<div class=\"three columns\" title=\"Expenses - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["Expenses"],true,true)."\"><span class=\"Expenses\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Expenses\">$".money($Revenues[$ThisDate]["Expenses"],true,true)."</span></div>
												<div class=\"three columns netIncome\" title=\"Net Income - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["NetIncome"],true,true)."\"><span data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."NetIncome\">$".($Revenues[$ThisDate]["NetIncome"] ? money($Revenues[$ThisDate]["NetIncome"],true,true) : money($Revenues[$ThisDate]["forecastAmount"]["NetIncome"],true,true))."</span></div>
												<br clear=\"all\">
												<br clear=\"all\">
											</div>";
							$RevenueHolders[] = $MonthHolder;
							if (count($RevenueBudget[$ThisDate])){
								$BudgetHolder = "<div class=\"two columns revenueBudget\">
													<div class=\"three columns\">".date("M",strtotime($StartDate." +".$nextMonthCounter." month"))." Budget</div><Br clear=\"all\">
													<div class=\"three columns\" title=\"HEA Fees - Budget\"><span class=\"heafees\">$".money($RevenueBudget[$ThisDate]["HEAFees"],true,true)."</span></div>
													<div class=\"three columns\" title=\"ISMs - Budget\"><span class=\"isms\">$".money($RevenueBudget[$ThisDate]["ISMs"],true,true)."</span></div>
													<div class=\"three columns\" title=\"IIC Contract Fees - Budget\"><span class=\"contracts\">$".money($RevenueBudget[$ThisDate]["Contracts"],true,true)."</span></div>
													<div class=\"three columns\" title=\"Inspection Fees - Budget\"><span class=\"heafees\">$".money($RevenueBudget[$ThisDate]["InspectionFees"],true,true)."</span></div>
													<div class=\"three columns\" title=\"Other Revenue - Budget\"><span class=\"otherrevenue\">$".money($RevenueBudget[$ThisDate]["OtherRevenue"],true,true)."</span></div>
													<div class=\"three columns totalRevenue\" title=\"Total Revenue - Budget\"><span class=\"totals\">$".money($RevenueBudget[$ThisDate]["Totals"],true,true)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
													<div class=\"three columns\" title=\"COGS - Budget\"><span class=\"COGS\">$".money($RevenueBudget[$ThisDate]["COGS"],true,true)."</span></div>
													<div class=\"three columns grossProfit\" title=\"Gross Profit\"><span>$".money($RevenueBudget[$ThisDate]["GrossProfit"],true,true)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
													<div class=\"three columns\" title=\"Expenses - Budget\"><span class=\"Expenses\">$".money($RevenueBudget[$ThisDate]["Expenses"],true,true)."</span></div>
													<div class=\"three columns netIncome\" title=\"Net Income - Budget\"><span>$".money($RevenueBudget[$ThisDate]["NetIncome"],true,true)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
												</div>";
								$RevenueHolders[] = $BudgetHolder;
								$forecastTotalRevenue = ($Revenues[$ThisDate]["forecastAmount"]["HEAFees"]+$Revenues[$ThisDate]["forecastAmount"]["ISMs"]+$Revenues[$ThisDate]["forecastAmount"]["Contracts"]+$Revenues[$ThisDate]["forecastAmount"]["InspectionFees"]+$Revenues[$ThisDate]["forecastAmount"]["OtherRevenue"]);
								$forecastGrossProfit = ($forecastTotalRevenue-$Revenues[$ThisDate]["forecastAmount"]["COGS"]);
								$ForecastHolder = "<div class=\"two columns revenueBudget\"".$columnTitle.">
													<div class=\"three columns\"><span class=\"monthLabel\">".date("M",strtotime($StartDate." +".$nextMonthCounter." month"))." Forecast</span></span></div><Br clear=\"all\">
													<div class=\"three columns\" title=\"HEA Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["HEAFees"],true,true)."\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."HEAFees\">$".money($Revenues[$ThisDate]["forecastAmount"]["HEAFees"],true,true)."</span></div>
													<div class=\"three columns\" title=\"ISMs - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["ISMs"],true,true)."\"><span class=\"results isms\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."ISMs\">$".money($Revenues[$ThisDate]["forecastAmount"]["ISMs"],true,true)."</span></div>
													<div class=\"three columns\" title=\"IIC Contract Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["Contracts"],true,true)."\"><span class=\"results contracts\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Contracts\">$".money($Revenues[$ThisDate]["forecastAmount"]["Contracts"],true,true)."</span></div>
													<div class=\"three columns\" title=\"Inspection Fees - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["InspectionFees"],true,true)."\"><span class=\"results heafees\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."InspectionFees\">$".money($Revenues[$ThisDate]["forecastAmount"]["InspectionFees"],true,true)."</span></div>
													<div class=\"three columns\" title=\"Other Revenue - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["OtherRevenue"],true,true)."\"><span class=\"results otherrevenue\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."OtherRevenue\">$".money($Revenues[$ThisDate]["forecastAmount"]["OtherRevenue"],true,true)."</span></div>
													<div class=\"three columns totalRevenue\" title=\"Total Revenue\"><span class=\"results totals\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Totals\">$".money($forecastTotalRevenue,TRUE,TRUE)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
													<div class=\"three columns\" title=\"COGS - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["COGS"],true,true)."\"><span class=\"COGS\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."COGS\">$".money($Revenues[$ThisDate]["forecastAmount"]["COGS"],true,true)."</span></div>
													<div class=\"three columns grossProfit\" title=\"Gross Profit\"><span data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Totals\">$".money($forecastGrossProfit,true,true)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
													<div class=\"three columns\" title=\"Expenses - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["Expenses"],true,true)."\"><span class=\"Expenses\" data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."Expenses\">$".money($Revenues[$ThisDate]["forecastAmount"]["Expenses"],true,true)."</span></div>
													<div class=\"three columns netIncome\" title=\"Net Income - Forecasted Amount $".money($Revenues[$ThisDate]["forecastAmount"]["NetIncome"],true,true)."\"><span data-amount=\"0\" data-yearmonth=\"".$ThisDate."\" id=\"".$ThisDate."NetIncome\">$".money($Revenues[$ThisDate]["forecastAmount"]["NetIncome"],true,true)."</span></div>
													<br clear=\"all\">
													<br clear=\"all\">
												</div>";
								$RevenueHolders[] = $ForecastHolder;
								
							}else{//end if budget
								$ExecPL_Date = date("Y-m-t",strtotime($StartDate." +".$nextMonthCounter." month"));
								$EnterActualsHolder = "<div class=\"two columns revenueBudget\">
														<div class=\"three columns\">".date("M",strtotime($StartDate." +".$nextMonthCounter." month"))." THISTYPE</div><Br clear=\"all\">
														<div class=\"three columns\" title=\"HEA Fees - THISTYPE\"><span class=\"heafees\">$<input class=\"revenueActualEntry\" id=\"RAE_HEATHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Revenues\" data-type=\"HEA Fees\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns\" title=\"ISMs - THISTYPE\"><span class=\"isms\">$<input class=\"revenueActualEntry\" id=\"RAE_ISMSTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Revenues\" data-type=\"ISMS\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns\" title=\"IIC Contract Fees - THISTYPE\"><span class=\"contracts\">$<input class=\"revenueActualEntry\" id=\"RAE_IICTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Revenues\" data-type=\"IIC Contract Fees\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns\" title=\"Inspection Fees - THISTYPE\"><span class=\"heafees\">$<input class=\"revenueActualEntry\" id=\"RAE_InspectionTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Revenues\" data-type=\"Inspection Fees\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns\" title=\"Other Revenue - THISTYPE\"><span class=\"otherrevenue\">$<input class=\"revenueActualEntry\" id=\"RAE_OtherTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Revenues\" data-type=\"Other Revenue\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns totalRevenue\" title=\"Total Revenue - THISTYPE\">$<span id=\"revenueTHISTYPETotals".$ExecPL_Date."\"></span></div>
														<br clear=\"all\">
														<br clear=\"all\">
														<div class=\"three columns\" title=\"COGS - THISTYPE\"><span class=\"COGS\">$<input class=\"revenueActualEntry\" id=\"RAE_COGSTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"COGS\" data-type=\"Total COGS\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns grossProfit\" title=\"Gross Profit\">$<span id=\"revenueTHISTYPEGrossProfit".$ExecPL_Date."\"></span></div>
														<br clear=\"all\">
														<br clear=\"all\">
														<div class=\"three columns\" title=\"Expenses - THISTYPE\"><span class=\"Expenses\">$<input class=\"revenueActualEntry\" id=\"RAE_ExpensesTHISTYPE".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Expenses\" data-type=\"Total Expenses\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></span></div>
														<div class=\"three columns netIncome\" style=\"font-weight:normal;\" title=\"Net Income - THISTYPE\">$<input class=\"revenueActualEntry\" id=\"revenueTHISTYPENetIncome".$ExecPL_Date."\" data-date=\"".$ExecPL_Date."\" data-actualbudget=\"THISTYPE\" data-department=\"GHS\" data-category=\"Net Income\" data-type=\"Net Income\" data-valuetype=\"THISTYPE\" type=\"text\" value=\"\"></div>
														<br clear=\"all\">
														<br clear=\"all\">
													</div>";
									$RevenueHolders[] = str_replace("THISTYPE","Actual",$EnterActualsHolder);
									$RevenueHolders[] = str_replace("THISTYPE","Budget",$EnterActualsHolder);
							}
							
							$nextMonthCounter++;				
						} while ( $nextMonthName != $lastMonthName);
						
						
						$YTDHolder = "<div class=\"three columns YTD\">
											<div class=\"three columns\">YTD Actuals</div><Br clear=\"all\">
											<div class=\"three columns\" title=\"HEA Fees - Forecasted Amount $".money($RevenuesYTD["HEA Fees"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["HEA Fees"]["actual"],true,true)."</span></div>
											<div class=\"three columns\" title=\"ISMs - Forecasted Amount $".money($RevenuesYTD["ISMS"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["ISMS"]["actual"],true,true)."</span></div>
											<div class=\"three columns\" title=\"IIC Contract Fees - Forecasted Amount $".money($RevenuesYTD["IIC Contract Fees"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["IIC Contract Fees"]["actual"],true,true)."</span></div>
											<div class=\"three columns\" title=\"Inspection Fees - Forecasted Amount $".money($RevenuesYTD["Inspection Fees"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["Inspection Fees"]["actual"],true,true)."</span></div>
											<div class=\"three columns\" title=\"Other Revenue - Forecasted Amount $".money($RevenuesYTD["Other Revenue"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["Other Revenue"]["actual"],true,true)."</span></div>";
											$YTDTotalRevenue = ($RevenuesYTD["HEA Fees"]["actual"]+$RevenuesYTD["ISMS"]["actual"]+$RevenuesYTD["IIC Contract Fees"]["actual"]+$RevenuesYTD["Inspection Fees"]["actual"]+$RevenuesYTD["Other Revenue"]["actual"]);
						$YTDHolder .= "<div class=\"three columns totalRevenue\" title=\"Total Revenue\"><span>$".money($YTDTotalRevenue,true,true)."</span></div>
											<br clear=\"all\">
											<br clear=\"all\">
											<div class=\"three columns\" title=\"COGS - Forecasted Amount $".money($RevenuesYTD["Total COGS"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["Total COGS"]["actual"],true,true)."</span></div>
											<div class=\"three columns grossProfit\" title=\"Gross Profit\"><span>$".money(($YTDTotalRevenue-$RevenuesYTD["Total COGS"]["actual"]),true,true)."</span></div>
											<br clear=\"all\">
											<br clear=\"all\">
											<div class=\"three columns\" title=\"Expenses - Forecasted Amount $".money($RevenuesYTD["Total Expenses"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["Total Expenses"]["actual"],true,true)."</span></div>
											<div class=\"three columns netIncome\" title=\"Net Income - Forecasted Amount $".money($RevenuesYTD["Net Income"]["forecast"],true,true)."\"><span>$".money($RevenuesYTD["Net Income"]["actual"],true,true)."</span></div>
											<br clear=\"all\">
											<br clear=\"all\">
										</div>";
						$RevenueHolders[] = $YTDHolder;

						
					?>
						
					<?php
						foreach ($RevenueHolders as $RevenueHolder){
							echo $RevenueHolder;
						}
					?>
				</div>
			</div>
		</fieldset>
	</div>
	
	<script src="<?php echo $CurrentServer.$adminFolder;?>js/highstock/highstock.js"></script>
	<script src="<?php echo $CurrentServer.$adminFolder;?>js/highstock/modules/exporting.js"></script>
	<br clear="all">
	<div id="container" class="highchart"></div>
	<div id="containerChart" class="highchart"></div>
	<table id="tableInfo" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<?php
					foreach ($TableColumns as $hdr){
						echo "<th>".$hdr."</th>";
					}
				?>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<?php
					foreach ($TableColumns as $hdr){
						echo "<th>".$hdr."</th>";
					}
				?>
			</tr>
		</tfoot>
		<tbody>
			<?php
				foreach ($PLresultArray as $column=>$data){
					echo "<tr>";
					foreach ($data as $rowid=>$rowinfo){
						echo "<td>".$rowinfo."</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
	<script type="text/javascript">
	$(function () {
		var timeConverter = function(UNIX_timestamp,showWhat){
		  var a = new Date(UNIX_timestamp);
		  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		  var year = a.getFullYear();
		  var month = months[a.getMonth()];
		  var date = a.getDate();
		  var hour = a.getHours();
		  var min = a.getMinutes();
		  var sec = a.getSeconds();
		  var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		  var monthYear = month + ' ' + year;
			if (showWhat == "mmYY"){
				return monthYear;
			}else{
				return time;
			}
		},
		formatNumber = function(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
		};	
		var seriesOptions = [],
			seriesCounter = 0,
			seriesName = [],
			netIncome = 0,
			names = ['<?php echo implode("','",$CategoryTypes);?>','Total_Total Net Income','RawData_Raw Data'],
			now = new Date().getTime(),

			// create the chart when all data is loaded
			createChart = function () {

				$('#containerChart').highcharts('StockChart', {
					chart: {
						height: 430
					},
					plotOptions: {
						series: {
							marker: {
								enabled: true
							}
						}
					},
					title:{
						text:'P&L Linear'
					},
					credits:{enabled:false},
					rangeSelector: {
						inputEnabled: $('#containerChart').width() > 480,
						selected: 10
					},
					/*
					legend:{
						enabled: true,
						layout:'vertical',
						align:'left',
						verticalAlign:'top',
						backgroundColor:'#fff',
						borderColor:'#ccc',
						borderWidth:.5,
						y:30,
						x:0,
						itemWidth:135,
						itemStyle:{
							fontWeight:'bold'
						},
						itemHiddenStyle:{
							fontWeight:'bold'
						},
						title: {
							text: 'EFI<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
							style: {
								fontStyle: 'italic'
							}
						}
					},
					*/
					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: 'silver'
						}]
					},


					tooltip: {
						pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
						valueDecimals: 2
					},
					series: seriesOptions
				});
			},
			createBarChart = function () {

				$('#container').highcharts({
					chart: {
						type: 'column'
					},
					plotOptions: {
						column: {
							stacking: 'normal'
						},
						series: {
							events: {
								click: function (event) {
									var thisSeriesIndex = seriesName.indexOf(this.name);
									//alert ('Category: '+ this.category +', value: '+ this.y);
								},							
								legendItemClick: function () {
									var thisSeriesIndex = seriesName.indexOf(this.name),
										thisVisible = this.visible,
										thisStackKey = this.stackKey,
										thisArray = this.yData;
										netIncomeIndex = seriesName.indexOf("Net Income"),
										netIncomeArray = this.chart.series[netIncomeIndex].yData;
										var chart = $('.highchart').highcharts();
										var chart1 = $('#container').highcharts();
										var chart2 = $('#containerChart').highcharts();
										
										//chart.series[0].data[0].update(-150);
									if (thisStackKey.indexOf('Revenues') > 0){
										if (thisVisible){
											for (var p = 0; p < thisArray.length; p++) {
												var newVal = netIncomeArray[p]-thisArray[p];
												netIncomeArray[p] =  newVal;
											}									
											chart2.series[thisSeriesIndex].hide();
												
										}else{
											for (var p = 0; p < thisArray.length; p++) {
												var newVal = netIncomeArray[p]+thisArray[p];
												netIncomeArray[p] = newVal;
											}									
											chart2.series[thisSeriesIndex].show();
										}
									}
									if (thisStackKey.indexOf('Expenses') > 0){
										if (thisVisible){
											for (var p = 0; p < thisArray.length; p++) {
												var newVal = netIncomeArray[p]+thisArray[p];
												netIncomeArray[p] = newVal;
											}									
											chart2.series[thisSeriesIndex].hide();
										}else{
											for (var p = 0; p < thisArray.length; p++) {
												var newVal = netIncomeArray[p]-thisArray[p];
												netIncomeArray[p] = newVal;
											}									
											chart2.series[thisSeriesIndex].show();
										}
									}
									chart1.series[netIncomeIndex].setData(this.chart.series[netIncomeIndex].yData);
									chart2.series[netIncomeIndex].setData(this.chart.series[netIncomeIndex].yData);
									var total = 0;
									$.each(netIncomeArray,function() {
										total += this;
									});
									var netTotalIncomeIndex = seriesName.indexOf("Total Net Income");
									
									chart1.series[netTotalIncomeIndex].data[0].update(total); 
									chart2.series[netTotalIncomeIndex].data[0].update(total); 
									chart2.series[netTotalIncomeIndex].data[1].update(total); 
									if (thisVisible){
									}else{
										chart2.series[netTotalIncomeIndex].show();
									}
									//console.log('Total Income total='+total);
								}
							}
						}
					},
					title: {
						text: 'P&L Stacked'
					},
					
					credits:{enabled:false},
					xAxis: {
						type: 'datetime',
						labels: {
							enabled: true,
//							rotation: -45,
							style: {
								fontSize: '10px',
								fontFamily: 'Verdana, sans-serif'
							}
						}
					},					
					yAxis: {
						labels: {
							formatter: function () {
								return (this.value > 0 ? ' + ' : '') + this.value;
							}
						},
						tickPixelInterval: 20,
						plotLines: [{
							value: 0,
							width: 1,
							color: 'silver'
						}]
					},

				   tooltip: {
						formatter: function () {
							var tooltipDisplay = '<b>' + timeConverter(this.x,"mmYY") + '</b><br/>' +
								this.series.name + ': $' + formatNumber(this.y) + '<br/>' +
								'Total: $' + formatNumber(this.point.stackTotal);
							if (this.series.name == "Total Net Income"){
								tooltipDisplay = '<b>' + this.series.name + ': $' + formatNumber(this.y) + '</b><br/>';
							}
							return tooltipDisplay;
						}
					},
					legend:{
						enabled: true,
						title: {
							text: '<span style="font-size: 10px; color: #666; font-weight: normal">Click legend item to hide it and Net Income will recalculate</span>',
							style: {
								fontStyle: 'italic'
							}
						}
					},
					series: seriesOptions
				});
			};
			

		$.each(names, function (i, name) {
			var categoryType = name.split("_"),
				category = categoryType[0],
				type = categoryType[1],
				stacked = category,
				visibility = true,
				linewidth = 1;
				//startDate = $("#StartDate").val(),
				//endDate = $("#EndDate").val();
			if (category == "COGS"){stacked = "Expenses";}
			if (category == "Total"){visibility = true;linewidth = 5;}
			//console.log(category);
			//console.log(fullName);
			seriesName[i] = categoryType[1];
			var url = '../stockchart/data.php?type=forecast&report=PL&department=GHS&category='+categoryType[0]+'&categoryType='+categoryType[1]+'&startDate=<?php echo $StartDate;?>&endDate=<?php echo $EndDate;?>&callback=?';
				$.getJSON(url,    function (data) {
					//console.log(categoryType[0]);
					//console.log(data);
					if (categoryType[0] != 'RawData'){
						seriesOptions[i] = {
							name: categoryType[1],
							visible: visibility,
							data: data,
							stack: stacked,
							lineWidth: linewidth
						};
					}
					if (categoryType[1]=="Net Income"){
						NetIncomeData = data;
						var dataCount = 0;
						$.each(data,function() {
							netIncome += parseInt(this[1]);
							if (dataCount<1){
								TotalNetIncomeStartDate = this[0];
							}	
							dataCount++;
							TotalNetIncomeEndDate = this[0];
							//console.log(TotalNetIncomeEndDate);
						});
					}

					// As we're loading the data asynchronously, we don't know what order it will arrive. So
					// we keep a counter and create the chart when all the data is loaded.
					seriesCounter += 1;

					if (seriesCounter === names.length) {
						createChart();
						createBarChart();
						var netTotalIncomeIndex = seriesName.indexOf('Total Net Income')
						var chart1 = $('#container').highcharts();
						var chart2 = $('#containerChart').highcharts();
						chart1.series[netTotalIncomeIndex].addPoint([TotalNetIncomeEndDate,netIncome]);
						chart2.series[netTotalIncomeIndex].addPoint([TotalNetIncomeStartDate,netIncome]);
						chart2.series[netTotalIncomeIndex].addPoint([TotalNetIncomeEndDate,netIncome]);
						//chart1.series[netTotalIncomeIndex].hide();
						//chart2.series[netTotalIncomeIndex].hide();
						
					}
				});
		});
		$('#addRawData').click(function(){
			var chart1 = $('#container').highcharts();
			var rawDataIndex = seriesName.indexOf('Raw Data');
			if (chart1.series.length == 9 ){
				chart1.addSeries({
					name: 'Raw Data',
					color: 'purple',
					data: <?php echo json_encode($RawRevenue);?>
				});
			}
			$(this).hide();
		});
		
		var table = $('#tableInfo').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 50,
		});

				var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			maxDate:0,
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setMonth(date2.getMonth() +1);
				date2.setDate(date2.getDate() -1);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		
		$(".FilterParam").on("click",function(){
			updateFilters();
		});
		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				includeExtrapolatedDataCurrentMonth = ($("#includeExtrapolatedDataCurrentMonth").prop("checked") ? 1 : 0),
				includeExtrapolatedDataFutureMonth = ($("#includeExtrapolatedDataFutureMonth").prop("checked") ? 1 : 0),
				filterValues = new Array(startDate,endDate,includeExtrapolatedDataCurrentMonth,includeExtrapolatedDataFutureMonth);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters(),
				Params = "StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&includeExtrapolatedDataCurrentMonth="+Filters[2]+"&includeExtrapolatedDataFutureMonth="+Filters[3];
			window.location.href = "<?php echo $CurrentServer.$adminFolder;?>forecast/?"+Params+"&nav=<?php echo $nav;?>#";
		}
		var revenueBudget = $(".revenueBudget");
		revenueBudget.hide();
		$("#showBudget").on('click',function(){
			revenueBudget.toggle("slide",500);
		});
		
		var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		  };
		})();		
		
		var revenueActualEntry = $(".revenueActualEntry");
		
		var updater = null;
		revenueActualEntry.on('blur',function(){
			var $this = $(this);
			if ($this.val()){
				var	thisActualBudget = $this.attr('data-actualbudget'),
					thisDate = $this.attr('data-date'),
					thisFocus = $this.index(".revenueActualEntry")+1;
					updateActualEntry($this,thisFocus);
					sumActuals(thisDate,thisActualBudget,thisFocus);
			}
		});		
		var updateActualEntry = function(thisElement,thisFocus){
			var $this = thisElement,
				thisActualBudget = $this.attr('data-actualbudget'),
				thisDate = $this.attr('data-date'),
				thisDepartment = $this.attr('data-department'),
				thisCategory = $this.attr('data-category'),
				thisType = $this.attr('data-type'),
				thisValueType = $this.attr('data-valuetype'),
				thisValue = $this.val(),
				thisFocus = thisFocus;
			var PLEntry = {};
			PLEntry["action"] = "enter_actualbudget";
			PLEntry["Date"] = thisDate;
			PLEntry["Department"] = thisDepartment;
			PLEntry["Category"] = thisCategory;
			PLEntry["Type"] = thisType;
			PLEntry["TypeValue"] = thisValueType;
			PLEntry[thisValueType] = thisValue;
			data = PLEntry;
			revenueActualEntry.attr("disabled","disabled");
			$.ajax({
				url: "ApiForecasting.php",
				type: "GET",
				data: data,
				success: function(data){
					revenueActualEntry.removeAttr("disabled");
					var thisFocusText = '.revenueActualEntry:eq('+thisFocus+')';
					$(thisFocusText).focus();
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					console.log(message);
				}
			});
			
		};
		
		var sumActuals = function(entryDate,entryActualBudget,thisFocus){
			var entryDate = entryDate,
				entryActualBudget = entryActualBudget,
				heaID = "#RAE_HEA"+entryActualBudget+entryDate,
				ismsID = "#RAE_ISMS"+entryActualBudget+entryDate,
				iicID = "#RAE_IIC"+entryActualBudget+entryDate,
				inspectionID = "#RAE_Inspection"+entryActualBudget+entryDate,
				otherID = "#RAE_Other"+entryActualBudget+entryDate,
				cogsID = "#RAE_COGS"+entryActualBudget+entryDate,
				expensesID = "#RAE_Expenses"+entryActualBudget+entryDate,
				revenueTotalID = "#revenue"+entryActualBudget+"Totals"+entryDate,
				revenueGrossProfitID = "#revenue"+entryActualBudget+"GrossProfit"+entryDate,
				revenueNetIncomeID = "#revenue"+entryActualBudget+"NetIncome"+entryDate;
				
			var hea = ($(heaID).val() ? $(heaID).val() : 0),
				isms = ($(ismsID).val() ? $(ismsID).val() : 0),
				iic = ($(iicID).val() ? $(iicID).val() : 0),
				inspection = ($(inspectionID).val() ? $(inspectionID).val() : 0),
				other = ($(otherID).val() ? $(otherID).val() : 0),
				cogs = ($(cogsID).val() ? $(cogsID).val() : 0),
				expenses = ($(expensesID).val() ? $(expensesID).val() : 0),
	
				revenueTotalAmount = parseFloat(hea)+parseFloat(isms)+parseFloat(iic)+parseFloat(inspection)+parseFloat(other),
				revenueGrossProfitAmount = revenueTotalAmount-parseFloat(cogs),
				revenueNetIncomeAmount = revenueGrossProfitAmount-parseFloat(expenses);
				$(revenueTotalID).text(revenueTotalAmount.toFixed(2));
				$(revenueGrossProfitID).text(revenueGrossProfitAmount.toFixed(2));
				$(revenueNetIncomeID).val(revenueNetIncomeAmount.toFixed(2));
				updateActualEntry($(revenueNetIncomeID),thisFocus);
		};
		
		
		
		
		
		
		
		var commentDisplay = $("#commentDisplay"),
			commentCurrentDisplay = $("#commentCurrentDisplay"),
			commentTextArea = $("#commentTextArea"),
			commentLastEditedBy = $("#commentLastEditedBy");
		
		$(".monthComment").on('click',function(){
			commentLastEditedBy.html('');

			var $this = $(this),
				thisId = $this.attr('id');
				
			if (commentCurrentDisplay.val() == thisId){
					commentDisplay.slideToggle("down");
			}else{
				commentDisplay.show();
			}
			commentCurrentDisplay.val(thisId)
			var Comments = {};
				Comments["action"] = "get_comments";
				Comments["commentDate"] = thisId.replace("Comment","");
				data = Comments;
			$.ajax({
				url: "ApiForecasting.php",
				type: "GET",
				data: data,
				success: function(data){
					commentTextArea.val(data.comment);
					if (data.lastEditedBy){
						commentLastEditedBy.html("last edited by: "+data.lastEditedBy);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					commentTextArea.val(message);
				}
			});
		});
		
		var timer = null;
		commentTextArea.on('keyup',function(){
			clearTimeout(timer);
			timer = setTimeout(updateComments, 1000);
		});
		var updateComments = function(){
			var thisVal = commentTextArea.val();
			
			var Comments = {};
				Comments["action"] = "pl_comments";
				Comments["commentDate"] = commentCurrentDisplay.val().replace("Comment","");
				Comments["comment"] = thisVal;
				data = Comments;
			$.ajax({
				url: "ApiForecasting.php",
				type: "PUT",
				data: JSON.stringify(data),
				success: function(data){
//					console.log(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					commentTextArea.val(message);
				}
			});
		};
	});
	</script>
