<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - Forecast";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "forecast-display":
            $displayPage = "views/_forecastDisplay.php";
            break;
        case "forecast-profitandloss":
            $displayPage = "views/_forecastProfitAndLoss.php";
            break;
        case "forecast-profitandloss-print":
            $displayPage = "views/_forecastProfitAndLoss_printView.php";
            break;
		case "forecast-forecastrevenue":
            $displayPage = "views/_forecastRevenue.php";
			break;
        case "forecast-energyspecialistexpectations":
            $displayPage = "views/_energySpecialistExpectations.php";
            break;
		case "revenuedata":
			$displayPage = "_forecastMenuRevenueDataTab.php";
			break;
        case "forecast-contractstocomplete":
            $displayPage = "views/_contractsToComplete.php";
            break;
        case "forecast-heafees":
            $displayPage = "views/_revenueHEAFees.php";
            break;
        case "forecast-isms":
            $displayPage = "views/_revenueISMs.php";
            break;
        case "forecast-apptanalysis":
            $displayPage = "views/_apptTypeAnalysis.php";
            break;
        case "forecast-datasets":
            $displayPage = "views/_manageDatasets.php";
            break;
        case "forecast-variables":
            $displayPage = "views/_manageVariables.php";
            break;
        case "forecast-calculations":
            $displayPage = "views/_manageCalculations.php";
            break;
        case "forecast-functions":
            $displayPage = "views/_manageFunctions.php";
            break;
        case "instructions":
            $displayPage = "views/_instructions.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Forecast Administration</h1>';
include_once("_forecastMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>