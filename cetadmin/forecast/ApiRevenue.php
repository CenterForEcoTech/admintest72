<?php
error_reporting(1);
ini_set('max_execution_time', 90);

include_once("../_config.php");
include_once($siteRoot."functions.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
include_once($dbProviderFolder."CSGDailyDataProvider.php");
include_once($dbProviderFolder."ForecastingProvider.php");


$forecastingProvider = new ForecastingProvider($dataConn);
$forecastingVariables = $forecastingProvider->getVariables();

$currentRequest = new Request();
$parameters = $currentRequest->parameters;
$results = new stdClass();
$StartDate = $parameters["startDate"];
$EndDate = $parameters["endDate"];
//get number of months in range
$datetime1 = date_create(date("Y-m-01",strtotime($StartDate)));
$datetime2 = date_create(date("Y-m-01",strtotime($EndDate)));
$CurrentMonth = date("Y-m",strtotime($StartDate));

$interval = date_diff($datetime1, $datetime2);
$numMonths = $interval->m;
$n = 0;
$YearMonthArray = array();
while ($n <=$numMonths){
	$YearMonthRange[] = date("Y-m-01",strtotime($StartDate." +".$n." months"));
	$n++;
}


$includeExtrapolatedDataCurrentMonth = ($parameters['includeExtrapolatedDataCurrentMonth'] > 0 ? true : false);
$includeExtrapolatedDataFutureMonth = ($parameters['includeExtrapolatedDataFutureMonth'] > 0 ? true : false);

$cetDataProvider = new CSGDataProvider($dataConn);
$cetDailyDataProvider = new CSGDailyDataProvider($dataConn);
$criteria = new stdClass();
$criteria->startDate = $StartDate;
$criteria->endDate = $EndDate;
$scheduleData = $cetDataProvider->getHUBScheduleReportData($criteria);

$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
include_once($dbProviderFolder."ProductProvider.php");
$productProvider = new ProductProvider($dataConn);
$paginationResult = $productProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $resultID=>$record){
	$ProductsByEFI[$record->efi] = $record;
	$ProductsByDescription[$record->description] = $record->efi;
	if ($record->displayOrderId){
		$ProductsWithDisplay[$record->displayOrderId]=$record;
	}else{
		$ProductsWithOutDisplay[$record->efi]=$record;
	}
}

include_once($dbProviderFolder."WarehouseProvider.php");
$warehouseProvider = new WarehouseProvider($dataConn);
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $resultID=>$record){
	$NameParts = explode(" ",$record->description);
	$ShortName = $NameParts[0]." ".substr($NameParts[1],0,1);
	$WarehouseByName[$record->name] = $ShortName;
	$WarehouseByShortName[$ShortName] = $record->name;
	if ($record->displayOrderId){
		$ActiveWarehouseByName[]=$ShortName;
		$ActiveWarehouseByID[]=strtoupper($record->name);
	}
}

$criteria = new stdClass();
$criteria->mostRecent = true;
$lastAverages = $forecastingProvider->lastAveragesES($criteria);
$lastContractAverages = $forecastingProvider->lastAveragesContractsES($criteria);
//create expectations for averages of Crew based on ISMS
if (count($lastAverages)){
	foreach  ($lastAverages as $crewAvg){
		$crew = $crewAvg["crew"];
		if (in_array($crew,$ActiveWarehouseByID)){
			$type = $crewAvg["type"];
			if (strpos(" ".$type,"Revenue") || strpos(" ".$type,"COGS")){
				$type = $type;
			}else{
				$type = ($type != 'Bulbs' ? $ProductsByEFI[$crewAvg["type"]]->description : $crewAvg["type"]);
			}
			$qty = $crewAvg["qty"];
			$CrewForecaster[$crew][$type] = $qty;		
			$ISMS[$type] = 1;
		}
	}
}
//create expectations for averages of Crew HEA Visits
if (count($lastContractAverages)){
	foreach  ($lastContractAverages as $crewAvg){
		$crew = $crewAvg->crew;
		if (in_array($crew,$ActiveWarehouseByID)){
			$siteVisits = $crewAvg->siteVisits;
			$daysToBill = $crewAvg->daysToBill;
			$CrewForecaster[$crew]["Expected Site Visits"] = $siteVisits;		
			$ExpectedVisitsPerMonth = $ExpectedVisitsPerMonth+$siteVisits;		
			$ISMS["Expected Site Visits"] = 1;
			$CrewForecaster[$crew]["Days To Bill"] = $daysToBill;		
			$ISMS["Days To Bill"] = 1;
		}
	}
}

//create expectations for Inspection Visits
$criteria = new stdClass();
$criteria->inspections = true;
$criteria->startDate = date("Y-m-01",strtotime($TodaysDate." -3 months"));
$inspectionCounts = $cetDataProvider->getApptTypeAnlysis($criteria);
//print_pre($inspectionCounts);
foreach($inspectionCounts as $inspectionType){
	$apptType = $inspectionType["apptType"];
	$condensedType = $apptType;
	$addType = true;
	switch($apptType){
		case "Loan Inspection":
			$addType = false;
			break;
		case "Inspection (open slots)":
			$addType = false;
			break;
		case "Gas Inspection":
			$apptType = $apptType;
			break;
		case "Mechanical Rebate Verification":
			$apptType = $apptType;
			break;
		default:
//			$apptType = "Inspection";
			$condensedType = "Inspections";
			$inspectionAllTypes[] = $apptType;

			break;
	}
	if ($addType){
		$count = $inspectionType["maxCount"];
		$inspectionTypes[$apptType]["count"][]=$count;
		$inspectionTypes[$apptType]["condensedType"]=$condensedType;
		$inspectionTypes[$apptType]["average"]=ceil(array_sum($inspectionTypes[$apptType]["count"])/count($inspectionTypes[$apptType]["count"]));
		
	}
}
$inspectionAllTypes = array_unique($inspectionAllTypes);
$Notes[] = "Inspections Fee includes the following inspection types: [`".implode("`, `",$inspectionAllTypes)."`].  All other inspection types are considered separate types"; 
foreach($inspectionTypes as $ApptType){
	$expectedMonthlyInspections[$ApptType["condensedType"]] = $expectedMonthlyInspections[$ApptType["condensedType"]]+$ApptType["average"];
}

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		if ($parameters["action"] == "HEAFees"){
		
			foreach($scheduleData as $ID=>$Data){
				$ApptDate = $Data["Appt Date"];
				$ApptType = strtoupper(trim($Data["Appt Type"]));
				$CrewID = $Data["Crew ID"];
				$PrimaryProvider = $Data["Primary Provider"];
				$ProgramID = strtoupper($Data["Program ID"]);
				$YearMonth = date("Y-m-01",strtotime($ApptDate));
				$YearMonthFound[] = date("Y-m-01",strtotime($ApptDate));
				
				$ApptTypesByApptType[$ApptType][$YearMonth][$ProgramID] = $ApptTypesByApptType[$ApptType][$YearMonth][$ProgramID]+1;
				$ApptTypesByProgram[$ProgramID][$YearMonth][$ApptType] = $ApptTypesByProgram[$ProgramID][$YearMonth][$ApptType]+1;
				$ApptTypesByCrew[$CrewID][$YearMonth][$ApptType] = $ApptTypesByCrew[$CrewID][$YearMonth][$ApptType]+1;
				$Programs[$ProgramID] = 1;
				
				
				switch($ProgramID){
					case "NGRID":
						$Program = "NATGRID";
						$Formulas["Inspection Fees"]["NATGRID MRV Fee"] = "$".money($forecastingVariables->MechanicalRebateInspectionNATGRID)." x Number of NATGRID Mechanical Rebate Inspections";
						$Formulas["HEA Fees"]["NATGRID HEA Fee"] = "$".money($forecastingVariables->HEAAssessmentFeeNATGRID)." x Number of NATGRID HEA Visits";
						$Formulas["HEA Fees"]["NATGRID SHV Fee"] = "$".money($forecastingVariables->SHVAssessmentFeeNATGRID)." x Number of NATGRID SHV Visits";
						$Formulas["Inspection Fees"]["NATGRID Inspection Fee"] = "$".money($forecastingVariables->InspectionFeeNATGRID)." x Number of NATGRID Inspections";
						if ($ApptType == "MECHANICAL REBATE VERIFICATION"){
							$InspectionCount["Mechanical Rebate Verification"][$YearMonth] = $InspectionCount["Mechanical Rebate Verification"][$YearMonth]+1;
							$Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program] = $Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["MechanicalRebateInspection"] = ($Revenues[$YearMonth]["MechanicalRebateInspection"]+$forecastingVariables->MechanicalRebateInspectionNATGRID);
							$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->MechanicalRebateInspectionNATGRID);
						}
						if ($ApptType == "HEA"){
							$HEAVisits[$YearMonth][$Program] = $HEAVisits[$YearMonth][$Program]+1;
							$VisitTotals[$YearMonth] = $VisitTotals[$YearMonth]+1;
							$Revenues[$YearMonth]["HEA"] = ($Revenues[$YearMonth]["HEA"] + $forecastingVariables->HEAAssessmentFeeNATGRID);
							$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->HEAAssessmentFeeNATGRID);
						}
						if ($ApptType == "SPECIAL HOME VISIT"){
							$SHVisits[$YearMonth][$Program] = $SHVisits[$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["SHV"] = ($Revenues[$YearMonth]["SHV"]+$forecastingVariables->SHVAssessmentFeeNATGRID);
							$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->SHVAssessmentFeeNATGRID);
						}
						if ($ApptType == "INSPECTION"){
							$InspectionCount["Inspections"][$YearMonth] = $InspectionCount["Inspections"][$YearMonth]+1;
							$Inspections["InspectionFee_NATGRID"][$YearMonth][$Program] = $Inspections["InspectionFee_NATGRID"][$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["InspectionFee"] = ($Revenues[$YearMonth]["InspectionFee"]+$forecastingVariables->InspectionFeeNATGRID);
							$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeNATGRID);
						}
						break;
					case "BERKSHIRE GAS":
						$Program = "BGAS";
						$Notes[] = "It is not clear how to indicate Additional Unit Gas Inspections in HUB so they are being recorded as regular Gas Inspections";
						$Formulas["Inspection Fees"]["BGAS Gas Inspection Fee"] = "$".money($forecastingVariables->InspectionFeeGasBGAS)." x Number of Gas Inspections";
						$Formulas["Inspection Fees"]["BGAS Gas Inspection Fee"] = "$".money($forecastingVariables->InspectionFeeGasBGAS)." x Number of Gas Inspections";
						$Formulas["Inspection Fees"]["BGAS Inspection Fee"] = "$".money($forecastingVariables->InspectionFeeBGAS)." x Number of BGAS Inspections (aka MRV for BGAS)";
						$Formulas["HEA Fees"]["BGAS HEA Fee"] = "$".money($forecastingVariables->HEAAssessmentFeeBGAS)." x Number of BGAS HEA Visits";
						$Formulas["HEA Fees"]["BGAS SHV Fee"] = "$".money($forecastingVariables->SHVAssessmentFeeBGAS)." x Number of BGAS SHV Visits";
						$Formulas["HEA Fees"]["BGAS HEA Additional Unit Fee"] = "$".money($forecastingVariables->HEAAUAssessmentFeeBGAS)." x Number of BGAS HEA Additional Unit Visits";
						switch($ApptType){
							case "GAS INSPECTION":
								$InspectionCount["Gas Inspection"][$YearMonth] = $InspectionCount["Gas Inspection"][$YearMonth]+1;
								$Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program]+1;
								$Revenues[$YearMonth]["InspectionFeeGas"] = ($Revenues[$YearMonth]["InspectionFeeGas"]+$forecastingVariables->InspectionFeeGasBGAS);
								$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeGasBGAS);
								break;
							case "ADDITIONAL UNIT GAS INSPECTION":
								$Inspections["InspectionFeeAddUnit_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFeeAddUnit_BGAS"][$YearMonth][$Program]+1;
								$Revenues[$YearMonth]["InspectionFeeAddUnit"] = ($Revenues[$YearMonth]["InspectionFeeAddUnit"]+$forecastingVariables->InspectionFeeAddUnitBGAS);
								$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeAddUnitBGAS);
								break;
							case "INSPECTION":
								$InspectionCount["Inspections"][$YearMonth] = $InspectionCount["Inspections"][$YearMonth]+1;
								$Inspections["InspectionFee_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFee_BGAS"][$YearMonth][$Program]+1;
								$Revenues[$YearMonth]["InspectionFee"] = ($Revenues[$YearMonth]["InspectionFee"]+$forecastingVariables->InspectionFeeBGAS);
								$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeBGAS);
								break;
							case "MECHANICAL REBATE VERIFICATION":
								$InspectionCount["Mechanical Rebate Verification"][$YearMonth] = $InspectionCount["Mechanical Rebate Verification"][$YearMonth]+1;
								$Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program] = $Inspections["InspectionFeeGas_BGAS"][$YearMonth][$Program]+1;
								$Revenues[$YearMonth]["InspectionFeeGas"] = ($Revenues[$YearMonth]["InspectionFeeGas"]+$forecastingVariables->InspectionFeeGasBGAS);
								$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeGasBGAS);
								break;
							case "HEA VISIT":
								$HEAVisits[$YearMonth]["BGAS"] = $HEAVisits[$YearMonth]["BGAS"]+1;
								$VisitTotals[$YearMonth] = $VisitTotals[$YearMonth]+1;
								$Revenues[$YearMonth]["HEA"] = ($Revenues[$YearMonth]["HEA"]+$forecastingVariables->HEAAssessmentFeeBGAS);
								$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->HEAAssessmentFeeBGAS);
								break;
							case "SPECIAL HOME VISIT":
								$SHVisits[$YearMonth]["BGAS"] = $SHVisits[$YearMonth]["BGAS"]+1;
								$Revenues[$YearMonth]["SHV"] = ($Revenues[$YearMonth]["SHV"]+$forecastingVariables->SHVAssessmentFeeBGAS);
								$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->SHVAssessmentFeeBGAS);
								break;
							case "HEA ADDITIONAL UNIT VISIT":
								$HEAAUVisits[$YearMonth]["BGAS"] = $HEAAUVisits[$YearMonth]["BGAS"]+1;
								$VisitTotals[$YearMonth] = $VisitTotals[$YearMonth]+1;
								$Revenues[$YearMonth]["HEAAU"] = ($Revenues[$YearMonth]["HEAAU"]+$forecastingVariables->HEAAUAssessmentFeeBGAS);
								$RevenueTotals[$YearMonth]["HEA Fees"] = $RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->HEAAUAssessmentFeeBGAS;
								break;
							
						}
					case "WESTERN MASSACHUSETTS ELECTRIC":
						$Program = "WMECO";
						$Formulas["Inspection Fees"]["EverSource Inspection Fee"] = "$".money($forecastingVariables->InspectionFeeWMECO)." x Number of EverSource Inspections";
						$Formulas["Inspection Fees"]["EverSource MRV Fee"] = "$".money($forecastingVariables->MechanicalRebateInspectionWMECO)." x Number of EverSource Mechanical Rebate Inspections";
						$Formulas["HEA Fees"]["EverSource HEA Fee"] = "$".money($forecastingVariables->HEAAssessmentFeeWMECO)." x Number of EverSource HEA Visits";
						$Formulas["HEA Fees"]["EverSource SHV Fee"] = "$".money($forecastingVariables->SHVAssessmentFeeWMECO)." x Number of EverSource SHV Visits";
						if ($ApptType == "INSPECTION"){
							$InspectionCount["Inspections"][$YearMonth] = $InspectionCount["Inspections"][$YearMonth]+1;
							$Inspections["InspectionFee_WMECO"][$YearMonth][$Program] = $Inspections["InspectionFee_WMECO"][$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["InspectionFee"] = ($Revenues[$YearMonth]["InspectionFee"]+$forecastingVariables->InspectionFeeWMECO);
							$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->InspectionFeeWMECO);
						}
						if ($ApptType == "MECHANICAL REBATE VERIFICATION"){
							$InspectionCount["Mechanical Rebate Verification"][$YearMonth] = $InspectionCount["Mechanical Rebate Verification"][$YearMonth]+1;
							$Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program] = $Inspections["MechanicalRebateInspection_".$Program][$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["MechanicalRebateInspection"] = ($Revenues[$YearMonth]["MechanicalRebateInspection"]+$forecastingVariables->MechanicalRebateInspectionWMECO);
							$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"]+$forecastingVariables->MechanicalRebateInspectionWMECO);
						}
						if ($ApptType == "HEA"){
							$HEAVisits[$YearMonth][$Program] = $HEAVisits[$YearMonth][$Program]+1;
							$VisitTotals[$YearMonth] = $VisitTotals[$YearMonth]+1;
							$Revenues[$YearMonth]["HEA"] = ($Revenues[$YearMonth]["HEA"]+$forecastingVariables->HEAAssessmentFeeWMECO);
							$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->HEAAssessmentFeeWMECO);
						}
						if ($ApptType == "SPECIAL HOME VISIT"){
							$SHVisits[$YearMonth][$Program] = $SHVisits[$YearMonth][$Program]+1;
							$Revenues[$YearMonth]["SHV"] = ($Revenues[$YearMonth]["SHV"]+$forecastingVariables->SHVAssessmentFeeWMECO);
							$RevenueTotals[$YearMonth]["HEA Fees"] = ($RevenueTotals[$YearMonth]["HEA Fees"]+$forecastingVariables->SHVAssessmentFeeWMECO);
						}
						break;
				}
			}
			//check to see if including extrapolated data
			foreach ($InspectionCount as $InspType=>$YearMonthData){
				foreach ($YearMonthData as $YearMonth=>$InspCount){
					switch($InspType){
						case "Inspections":
							$variableAmount = $forecastingVariables->InspectionFeeNATGRID;
							break;
						case "Mechanical Rebate Verification":
							$variableAmount = $forecastingVariables->InspectionFeeNATGRID;
							break;
					}
					//check to see if including extrapolated data
					if (strtotime($CurrentMonth."-01") == strtotime($YearMonth)){
						$expectedAmount = bcmul($InspectionCount[$InspType][$YearMonth],$variableAmount);
						$expectedInspectionFees[$YearMonth][$InspType] = $expectedInspectionFees[$YearMonth][$InspType]+$expectedAmount;
						$expectedInspectionFees[$YearMonth]["Inspection Fees"] = $expectedInspectionFees[$YearMonth]["Inspection Fees"]+$expectedAmount;
					}
					if (strtotime($CurrentMonth."-01") < strtotime($YearMonth)){
						$expectedAmount = bcmul($InspectionCount[$InspType][$YearMonth],$variableAmount);
						$expectedInspectionFees[$YearMonth][$InspType] = $expectedInspectionFees[$YearMonth][$InspType]+$expectedAmount;
						$expectedInspectionFees[$YearMonth]["Inspection Fees"] = $expectedInspectionFees[$YearMonth]["Inspection Fees"]+$expectedAmount;
					}
				}
			}
			foreach ($Revenues as $YearMonth=>$YearMonthData){
				//check to see if including extrapolated data
				if ($includeExtrapolatedDataCurrentMonth && strtotime($CurrentMonth."-01") == strtotime($YearMonth)){
					$Revenues[$YearMonth]["InspectionFee"] = ($Revenues[$YearMonth]["InspectionFee"] > $expectedInspectionFees[$YearMonth]["Inspections"] ? $Revenues[$YearMonth]["InspectionFee"] : $expectedInspectionFees[$YearMonth]["Inspections"]);
					$Revenues[$YearMonth]["MechanicalRebateInspection"] = ($Revenues[$YearMonth]["MechanicalRebateInspection"] > $expectedInspectionFees[$YearMonth]["Mechanical Rebate Verification"] ? $Revenues[$YearMonth]["MechanicalRebateInspection"] : $expectedInspectionFees[$YearMonth]["Mechanical Rebate Verification"]);
				}
				if ($includeExtrapolatedDataFutureMonth && strtotime($CurrentMonth."-01") < strtotime($YearMonth)){
					$Revenues[$YearMonth]["InspectionFee"] = ($Revenues[$YearMonth]["InspectionFee"] > $expectedInspectionFees[$YearMonth]["Inspections"] ? $Revenues[$YearMonth]["InspectionFee"] : $expectedInspectionFees[$YearMonth]["Inspections"]);
					$Revenues[$YearMonth]["MechanicalRebateInspection"] = ($Revenues[$YearMonth]["MechanicalRebateInspection"] > $expectedInspectionFees[$YearMonth]["Mechanical Rebate Verification"] ? $Revenues[$YearMonth]["MechanicalRebateInspection"] : $expectedInspectionFees[$YearMonth]["Mechanical Rebate Verification"]);
				}
			}
//			print_pre($expectedInspectionFees);
			foreach ($RevenueTotals as $YearMonth=>$YearMonthData){
				//check to see if including extrapolated data
				if ($includeExtrapolatedDataCurrentMonth && strtotime($CurrentMonth."-01") == strtotime($YearMonth)){
					$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"] > $expectedInspectionFees[$YearMonth]["Inspection Fees"] ? $RevenueTotals[$YearMonth]["Inspection Fees"] : $expectedInspectionFees[$YearMonth]["Inspection Fees"]);
				}
				if ($includeExtrapolatedDataFutureMonth && strtotime($CurrentMonth."-01") < strtotime($YearMonth)){
					$RevenueTotals[$YearMonth]["Inspection Fees"] = ($RevenueTotals[$YearMonth]["Inspection Fees"] > $expectedInspectionFees[$YearMonth]["Inspection Fees"] ? $RevenueTotals[$YearMonth]["Inspection Fees"] : $expectedInspectionFees[$YearMonth]["Inspection Fees"]);
				}
			}

			foreach ($YearMonthRange as $YearMonth){
				if (!in_array($YearMonth,$YearMonthFound)){
					$YearMonthMissing[] = $YearMonth;
				}
			}
			
			foreach ($RevenueTotals as $YearMonth=>$RevenueType){
				//echo date("M Y", strtotime($YearMonth)).": ";
				$TotalAmount = 0;
				foreach ($RevenueType as $RevType=>$Total){
					$TotalAmount = $TotalAmount+$Total;
					$AllRevenueTotal = $AllRevenueTotal+$Total;
					//echo $RevType.$CurrentMonth.$YearMonth."<br>";
					$SummaryTotal[$YearMonth]["Totals"]["Type"][$RevType] = $SummaryTotal[$YearMonth]["Totals"]["Type"][$RevType]+$Total;
					$SummaryTotal[$YearMonth]["Totals"]["Total"] = $SummaryTotal[$YearMonth]["Totals"]["Total"]+$Total;
					$RevenueSummary[$YearMonth][$RevType] = $RevenueSummary[$YearMonth][$RevType]+$Total;
					//add missing months
					foreach ($YearMonthMissing as $MissingYearMonth){
						$Revenues[$MissingYearMonth]["HEA"] = 0;
						$Revenues[$MissingYearMonth]["SHV"] = 0;
						$Revenues[$MissingYearMonth]["MechanicalRebateInspection"] = ($includeExtrapolatedDataFutureMonth ? $expectedInspectionFees[$YearMonth]["Mechanical Rebate Verification"] : 0);
						$Revenues[$MissingYearMonth]["InspectionFee"] = ($includeExtrapolatedDataFutureMonth ? $expectedInspectionFees[$YearMonth]["Inspections"] : 0);
						$Revenues[$MissingYearMonth]["HEAAU"] = 0;
						$Revenues[$MissingYearMonth]["InspectionFeeGas"] = 0;
						$VisitTotals[$MissingYearMonth] = 0;
						$SummaryTotal[$MissingYearMonth]["Totals"]["Type"][$RevType] = 0;
						$SummaryTotal[$MissingYearMonth]["Totals"]["Total"] = 0;
						if ($RevType == "Inspection Fees"){
							$RevenueSummary[$MissingYearMonth][$RevType] = ($includeExtrapolatedDataFutureMonth ? $expectedInspectionFees[$YearMonth][$RevType] : 0);
						}else{
							$RevenueSummary[$MissingYearMonth][$RevType] = 0;
						}
					}
				}
				
				$asOfDate = strtotime(date("Y-m-12", strtotime($YearMonth)))*1000;
				//echo "$".money($TotalAmount)."<br>";
				$RawRevenue[]=array($asOfDate,(int)$TotalAmount);
			}

			$Notes = array_unique($Notes);
			$result["Notes"] = $Notes;
			$result["Formulas"] = $Formulas;
			$result["Visits"]["ExpectedVisits"] = $ExpectedVisitsPerMonth;
			$result["Visits"]["HEA"] = $VisitTotals;
			$result["Visits"]["AssessmentFee"] = (int)$forecastingVariables->HEAAssessmentFeeNATGRID;
			$result["RawRevenue"] = $RawRevenue;
			$result["Revenue"] = $Revenues;
//			print_pre($Revenues);
			$result["RevenueTotals"] = $RevenueTotals;
			$result["RevenueSummary"] = $RevenueSummary;
			$result["responseText"] = "Between ".$StartDate." and ".$EndDate." the combined expected amount from HEA and Inspection Fees work will be: $".money($AllRevenueTotal,true,true)."<br>".
				"Which breaks down to:<br>";
			//print_pre($SummaryTotal);
			foreach ($SummaryTotal as $YearMonth=>$Totals){
				$result["responseText"] .= $YearMonth."=$".money($SummaryTotal[$YearMonth]["Totals"]["Total"],true,true)."
					<br>&nbsp;&nbsp;&nbsp;&nbsp; [HEA Fees: $".money($SummaryTotal[$YearMonth]["Totals"]["Type"]["HEA Fees"],true,true)." Inspection Fees: $".money($SummaryTotal[$YearMonth]["Totals"]["Type"]["Inspection Fees"],true,true)."]<br>";
			}						
			
			
		}
		if ($parameters["action"] == "ISMs"){
			$Notes = array();
			$Formulas = array();
			$results = array();

			//Setup the revenue per ISM
			$ISMTypes = array('Bulbs','1.75gpm Earth Showerhead','1.5gmp Aerator','Tricklestar PowerStrip','Honeywell Thermostat','2.2gpm FlipAerator');
			$ISMTypesVariables["Bulbs"] = $forecastingVariables->ISMPriceBulb;
			$ISMTypesVariables["1.75gpm Earth Showerhead"] = $forecastingVariables->ISMShowerHeadPrice;
			$ISMTypesVariables["1.5gmp Aerator"] = $forecastingVariables->ISMStdAeratorPrice;
			$ISMTypesVariables["Tricklestar PowerStrip"] = $forecastingVariables->ISMPowerStripPrice;
			$ISMTypesVariables["Honeywell Thermostat"] = $forecastingVariables->ISMThermostatPrice;
			$ISMTypesVariables["2.2gpm FlipAerator"] = $forecastingVariables->ISMFlipAeratorPrice;
			$Notes[] = "ISM Revenue is determined by `Retail` price in product management for each ISM";
			$Notes[] = "ISM COGS is determined by `Cost` price in product management for each ISM";
			//get scheduled data		
			foreach($scheduleData as $ID=>$Data){
				$ApptDate = $Data["Appt Date"];
				$ApptType = strtoupper(trim($Data["Appt Type"]));
				$CrewID = strtoupper($Data["Crew ID"]);
				$YearMonth = date("Y-m-01",strtotime($ApptDate));
				$YearMonthFound[] = $YearMonth;
//				if (in_array($CrewID,$ActiveWarehouseByID)){
					$ApptTypesByApptType[$ApptType][$YearMonth][$ProgramID] = $ApptTypesByApptType[$ApptType][$YearMonth][$ProgramID]+1;
					$ApptTypesByProgram[$ProgramID][$YearMonth][$ApptType] = $ApptTypesByProgram[$ProgramID][$YearMonth][$ApptType]+1;
					$ApptTypesByCrew[$CrewID][$YearMonth][$ApptType] = $ApptTypesByCrew[$CrewID][$YearMonth][$ApptType]+1;
					if ($Data["Site ID"]){
						$ApptTypesByCrew[$CrewID][$YearMonth]["SiteIDs"][] = $Data["Site ID"];
					}
					$ApptTypesByCrew[$CrewID][$YearMonth]["SiteIDs"] = array_unique($ApptTypesByCrew[$CrewID][$YearMonth]["SiteIDs"]);
					$Programs[$ProgramID] = 1;
//				}
			}
			ksort($ApptTypesByCrew);
			$YearMonthFound = array_unique($YearMonthFound);
			foreach ($YearMonthRange as $YearMonth){
				if (!in_array($YearMonth,$YearMonthFound)){
					$YearMonthMissing[] = $YearMonth;
				}
			}
			foreach ($ApptTypesByCrew as $Crew=>$YearMonthData){
				//echo $Crew."<br>";
				foreach ($YearMonthData as $YearMonth=>$ApptType){
					$VisitTotal = $ApptType["HEA"]+$ApptType["SPECIAL HOME VISIT"];
					//echo $VisitTotal."<br>";
					//extrapoliate data			
					if ($includeExtrapolatedDataCurrentMonth && strtotime($CurrentMonth) == strtotime($YearMonth)){
						//echo $Crew." expects HEA visit count=".$CrewForecaster[$Crew]["Expected Site Visits"].", there are scheduled visits in ".$YearMonth.": ".$VisitTotal."<br>";
						$VisitTotal = (int)((int)$CrewForecaster[$Crew]["Expected Site Visits"] > (int)$VisitTotal ? $CrewForecaster[$Crew]["Expected Site Visits"] : $VisitTotal);
						//echo $Crew." will use expected visit count: ".$VisitTotal."<Br><br>";
					}
					if ($includeExtrapolatedDataFutureMonth && strtotime($CurrentMonth) < strtotime($YearMonth)){
						//echo $Crew." expects HEA visit count=".$CrewForecaster[$Crew]["Expected Site Visits"].", there are scheduled visits in ".$YearMonth.": ".$VisitTotal."<br>";
						$VisitTotal = (int)((int)$CrewForecaster[$Crew]["Expected Site Visits"] > (int)$VisitTotal ? $CrewForecaster[$Crew]["Expected Site Visits"] : $VisitTotal);
						//echo $Crew." will use expected visit count: ".$VisitTotal."<Br><br>";
					}
					$Revenue = 0;
					$Revenue = bcmul($CrewForecaster[$Crew]["RevenuePerVisit"],$VisitTotal); 
					$Formulas["ISMs"]["ISM Revenue"] = "Expected ISM Revenue per Energy Specialist/HEA Visit x Number of HEA+SHV Visits for that ES";
					$Formulas["COGS"]["Total COGS"] = "Expected ISM COGS per Energy Specialist/HEA Visit x Number of HEA+SHV Visits for that ES";
					$ISMRevenueTotal[$YearMonth] = $ISMRevenueTotal[$YearMonth]+$Revenue;
					$ISMTotal = $ISMTotal +$Revenue;
					foreach ($ISMTypes as $ISM){
						$Count = 0;
						$Count = bcmul($CrewForecaster[$Crew][$ISM],$VisitTotal); 
						$ISMRevenue[$YearMonth][$ISM] = $ISMRevenue[$YearMonth][$ISM]+$Count;
						$ISMRevenueTotals[$ISM] = $ISMRevenueTotals[$ISM]+$Count;
					}
					$COGS = bcmul($CrewForecaster[$Crew]["COGSPerVisit"],$VisitTotal); 
					$COGSTotal[$YearMonth] = $COGSTotal[$YearMonth]+$COGS;
				}
				foreach ($YearMonthMissing as $MissingYearMonth){
					$Revenue = 0;
					$COGS = 0;
					if ($includeExtrapolatedDataFutureMonth){
						$Revenue = bcmul($CrewForecaster[$Crew]["RevenuePerVisit"],$CrewForecaster[$Crew]["Expected Site Visits"]); 
						$COGS = bcmul($CrewForecaster[$Crew]["COGSPerVisit"],$CrewForecaster[$Crew]["Expected Site Visits"]); 
					}
					$ISMRevenueTotal[$MissingYearMonth] = $ISMRevenueTotal[$MissingYearMonth]+$Revenue;
					$ISMTotal = $ISMTotal +$Revenue;
					$COGSTotal[$MissingYearMonth] = $COGSTotal[$MissingYearMonth]+$COGS;
					foreach ($ISMTypes as $ISM){
						$Count = 0;
						if ($includeExtrapolatedDataFutureMonth){
							$Count = bcmul($CrewForecaster[$Crew][$ISM],$CrewForecaster[$Crew]["Expected Site Visits"]); 
						}
						$ISMRevenue[$MissingYearMonth][$ISM] = $ISMRevenue[$MissingYearMonth][$ISM]+$Count;
						$ISMRevenueTotals[$ISM] = $ISMRevenueTotals[$ISM]+$Count;
					}
				}
			}
			$Notes = array_unique($Notes);
			$result["Notes"] = $Notes;
			$result["Formulas"] = $Formulas;
			$result["ISMRevenue"] = $ISMRevenue;			
			$result["ISMRevenueTotals"] = $ISMRevenueTotals;			
			$result["ISMRevenueTotal"] = $ISMRevenueTotal;			
			$result["COGS"] = $COGSTotal;
			$result["responseText"] = "Between ".$StartDate." and ".$EndDate." the combined expected amount from ISMs: $".money($ISMTotal)."<br>
				Which breaks down to:<br>";
				foreach ($ISMRevenueTotal as $MonthYear=>$Amount){
					$result["responseText"] .= $MonthYear."=$".money($Amount)."<br>";
				}						
			$result["responseText"] .= $result["Notes"];

		}
		$results = $result;
		header("HTTP/1.0 201 Created");
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		echo "this is PUT";
        break;
}
output_json($results);
die();
?>