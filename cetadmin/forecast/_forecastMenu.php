<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Forecast)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "145";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
<!--
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Forecast))){?>
			<li><a id="forecast-profitandloss" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-profitandloss" class="button-link" title="Review Actual and Forecast Amounts">Profit and Loss</a></li>
			<li><a id="forecast-forecastrevenue" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-forecastrevenue" class="button-link" title="Set parameters for creating forecasted amounts to be saved to P&L">Create Forecast Revenue Data</a></li>
			<br clear="all">
			<hr>
			<li><a id="forecast-energyspecialistexpectations" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-energyspecialistexpectations" class="button-link" title="Review ES performance based on Contract and ISM work">Energy Specialist Expectations</a></li>
			<?php if ($adminSecurityGroupId == 1){?>
			<li id="ReportMenuDropDown"><a id="revenuedata" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=revenuedata" class="button-link">Revenue Data Details</a>
				<?php ($nav != "revenuedata" ? include('_forecastMenuRevenueDataTab.php') : "");?>
			</li>

			<?php }?>
			<li><a id="forecast-apptanalysis" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-apptanalysis" class="button-link" title="Review Schedule Appts as accumulating for the month">Appt Type Analysis</a></li>
			<li><a id="forecast-variables" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-variables" class="button-link" title="Set Revenue Variables">Variables</a></li>
			<li><a id="instructions" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=instructions" class="button-link" title="Instructions on capturing data">Documentation</a></li>

			
			<li><a id="forecast-display" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-display" class="button-link">Forecast Display</a></li>
			<li><a id="forecast-datasets" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-datasets" class="button-link">Datasets</a></li>
			<li><a id="forecast-calculations" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-calculations" class="button-link">Calculations</a></li>
			<li><a id="forecast-functions" href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-functions" class="button-link">Functions</a></li>
-->
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console" title="Return to Main Menus">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
		});
		$("#revenuedata.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "revenuedata":
					$nav = "revenuedata";
					$showReportsDropDown = true;
					break;
				case "forecast-heafees":
					$navDropDown = $nav;
					$nav = "revenuedata";
					break;
				case "forecast-isms":
					$navDropDown = $nav;
					$nav = "revenuedata";
					break;
				case "forecast-contractstocomplete":
					$navDropDown = $nav;
					$nav = "revenuedata";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>