<style>
.energySpecialist {min-width:100px;}
</style>
<h2>Energy Specialist ISM Averages / Visit (HEA and SHV)</h2>
<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Energy Specialist</th>
			<?php
				foreach ($ISMS as $ism=>$count){
					echo "<th>".str_replace("Per","<br>Per ",$ism)."</th>";
				}
			?>
		</tr>
	</thead>

	<tbody>
		<?php
			//check to see if we should insert new averages
			foreach ($CrewISMTotalsAverage as $crew=>$isms){
				$Crew = explode("QQQ",str_replace("XX","",$crew));
				echo "<tr>";
				echo "<td class=\"energySpecialist\"><a href=\"".$CurrentServer.$adminFolder."inventory/?nav=warehouse-management&WarehouseName=".$Crew[0]."\">".$Crew[0]."</a> ".$Crew[1]."</td>";
				foreach ($ISMS as $ism=>$count){
					$displayVal = ($CrewISMTotalsAverage[$crew][$ism] > 0 ? $CrewISMTotalsAverage[$crew][$ism] : 0);
					$Averages[$ism][] = $displayVal;
					$displayFormat = $displayVal;
					$displayAlign = "center";
					$type = ($ism != "Bulbs" ? $ProductsByDescription[$ism] : "Bulbs");
					if (strpos(" ".$ism,"Revenue") || strpos(" ".$ism,"COGS")){
						$displayFormat = "$".money($displayVal,true,true);
						$displayAlign = "left";
						$type = $ism;
					}
					echo "<td align=\"".$displayAlign."\">".$displayFormat."</td>";
				}
				echo "</tr>";
			}
		?>
	</tbody>
	<tfoot>
		<th>Averages</th>
		<?php
			foreach ($ISMS as $ism=>$count){
				$displayVal = ceil(array_sum($Averages[$ism])/count($Averages[$ism]));
				$displayFormat = $displayVal;
				$displayAlign = "center";
				if (strpos(" ".$ism,"Revenue") || strpos(" ".$ism,"COGS")){
					$displayFormat = "$".money($displayVal,true,true);
					$displayAlign = "left";
				}
				
				echo "<th align=\"".$displayAlign."\">".$displayFormat."</th>";
			}
		?>
	</tfoot>
</table>