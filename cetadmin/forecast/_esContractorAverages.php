<hr>
<h2>Contractor Averages</h2>
<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th></th>
			<th>Contractor</th>
			<th>DaysTo Accept</th>
			<th>DaysTo Install</th>
			<th>DaysTo Bill</th>
			<th>Contract Amount</th>
			<th style="display:none;"></th>
		</tr>
	</thead>

	<tbody>
		<?php
			foreach ($SummaryContractorTableRows as $Row){
				if (strpos(" ".$Row,"ZZZ")){
					$FooterRow = $Row;
				}else{
					echo str_replace("ZZZ","",$Row);
				}
			}
		?>
	</tbody>
	<tfoot>
		<?php
			echo str_replace("<td class=\"details-control\"></td>","<td></td>",str_replace("ZZZContractor","",$FooterRow));
		?>
	</tfoot>

</table>
