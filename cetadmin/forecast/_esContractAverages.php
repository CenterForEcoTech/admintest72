<style>
.energySpecialist {min-width:300px;}
</style>
<h2>Energy Specialist Averages <span style="font-weight:normal;font-size:10pt;">(<a href="#Notes">Notes</a>)</span></h2>
<table class="reportResults display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th></th>
			<th class="energySpecialist">Energy Specialist</th>
			<th>Site Visits</th>
			<th>No Work</th>
			<th>Road Blocked</th>
			<th>Pending Signature</th>
			<th>Contracts Signed</th>
			<th>Contracts Billed</th>
<!--
			<th>Signed Rate</th>
-->
			<th>Conv Rate</th>
			<th>DaysTo Issue</th>
			<th>DaysTo Sign</th>
			<th>DaysTo Bill</th>
			<th>Contract Totals</th>
			<?php if ($includeCommissions){?>
<!--
				<th>Billed Amount</th>
				<th>Commission</th>
-->
			<?php }?>
			<th>NATGRID Contracts</th>
			<th>ESOURCE Contracts</th>
			<th>BGAS Contracts</th>
			<?php if ($includeCommissions){?>
<!--
				<th>BGAS Amount</th>
-->
			<?php }?>	
			<th style="display:none;"></th>
		</tr>
		<tfoot>
			<tr>
				<th></th>
				<th class="energySpecialist">Averages</th>
				<?php 
					foreach ($Aggregate as $AggType=>$details){
							$DisplayValue = ceil(array_sum($Aggregate[$AggType])/count($Aggregate[$AggType]));
							
						echo "<th align='center'>".($AggType == "ContractAmount" || $AggType == "BilledAmount"|| $AggType == "Commissions" || $AggType=="BGASBilledAmount" ? "$".money($DisplayValue,true,true) : $DisplayValue).($AggType == "ConversionRate" || $AggType == "SignedRate" ? "%" : "")."</th>";
					}
				?>
				<th style="display:none;"></th>
			</tr>
			<tr>
				<th></th>
				<th class="energySpecialist">Totals</th>
				<th><?php echo $Totals["SiteVisits"];?></th>
				<th><?php echo $Totals["NoWork"];?></th>
				<th><?php echo $Totals["RoadBlocked"];?></th>
				<th><?php echo $Totals["Pending"];?></th>
				<th><?php echo $Totals["ContractsSigned"];?></th>
				<th><?php echo $Totals["ContractsBilled"];?></th>
<!--
				<th><?php echo floor($Totals["ContractsSigned"]/$Totals["SiteVisits"]*100);?>%</th>
-->
				<th><?php echo floor($Totals["ContractsBilled"]/$Totals["SiteVisits"]*100);?>%</th>
				<th></th>
				<th></th>
				<th></th>
				<th>$<?php echo money($Totals["FinalContractAmount"],true,true);?></th>
				<?php if ($includeCommissions){?>
<!--
					<th>$<?php echo money($Totals["BilledAmount"],true,true);?></th>
					<th>$<?php echo money(bcmul($Totals["BilledAmount"],0.02),true,true);?></th>
-->
				<?php }?>
			<th><?php echo $Totals["NATGRID"]["BilledCount"];?></th>
			<th><?php echo $Totals["WMECO"]["BilledCount"];?></th>
			<th><?php echo $Totals["BGAS"]["BilledCount"];?></th>
			<?php if ($includeCommissions){?>
<!--
				<th>$<?php echo money(bcmul($Totals["BGAS"]["BilledAmount"],0.02),true,true);?></th>
-->
			<?php }?>	
			<th style="display:none;"></th>
			</tr>
		</tfoot>	
	</thead>

	<tbody>
		<?php
		//print_pre($EFICount);
//print_pre($CrewCount);
			foreach ($SummaryTableRows as $Row){
				echo str_replace("QQQ","&nbsp;",$Row);
			}
		?>
	</tbody>
</table>