<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."ForecastingProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$action = $_GET["action"];
		if ($action == "get_comments"){
			$commentDate = $_GET["commentDate"];
			$result = "";
            $forecastingProvider = new ForecastingProvider($dataConn);
			$response = $forecastingProvider->getPLComments($commentDate);
			if ($response[$commentDate]){
				$result = $response[$commentDate];
			}
		}
		if ($action == "enter_actualbudget"){
			$record = $_GET;
            $forecastingProvider = new ForecastingProvider($dataConn);
			$response = $forecastingProvider->enterActualBudget($record);
			$result = $response;
		}
		$results = $result;
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'pl_comments'){
	            $forecastingProvider = new ForecastingProvider($dataConn);
				$record = new stdClass();
				$record->commentDate = $newRecord->commentDate;
				$record->comment = $newRecord->comment;
				$response = $forecastingProvider->addPLComment($record, getAdminId());
				if ($response->success){
					$results = $response;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}
		if (trim($newRecord->action) == 'mrvcount_add'){
	            $forecastingProvider = new ForecastingProvider($dataConn);
				$utility = $newRecord->utility;
				$date = MySQLDateUpdate($newRecord->countDate);
				$count = $newRecord->countQty;
				
				$response = $forecastingProvider->mrvadd($utility,$date,$count, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}
		if (trim($newRecord->action) == 'pl_add'){
	            $forecastingProvider = new ForecastingProvider($dataConn);
				$rawData = rtrim($newRecord->plData,"|");
				$rawData = explode("|",$rawData);
				$forecastVariables = $newRecord->plVariables;
				foreach ($forecastVariables as $key=>$val){
					$plVariables[] = $key.": ".$val;
				}
				foreach ($rawData as $forecastItem){
					$forecast = explode("_",$forecastItem);
					$date = date("Y-m-t",strtotime($forecast[0]));
					$type = $forecast[1];
					$amount = $forecast[2];
					$category = "Revenues";
					switch($type){
						case "HEAFees":
							$type = "HEA Fees";
							break;
						case "InspectionFees":
							$type = "Inspection Fees";
							break;
						case "Contracts":
							$type = "IIC Contract Fees";
							break;
						case "ISMs":
							$type = "ISMS";
							break;						
						case "OtherRevenue":
							$type = "Other Revenue";
							break;
						case "COGS":
							$type = "Total COGS";
							$category = "COGS";
							break;							
						case "Expenses":
							$type = "Total Expenses";
							$category = "Expenses";
							break;							
						case "NetIncome":
							$type = "Net Income";
							$category = "Net Income";
							break;							
					}
					if ($type !="Totals"){
						$plObject = new stdClass();
						$plObject->plDate = $date;
						$plObject->department = "GHS";
						$plObject->category = $category;
						$plObject->type = $type;
						
						$plObject->actual = 0;
						$plObject->budget = 0;
						$plObject->forecast = $amount;
						$plObject->forecastAsOfDate = date("Y-m-d");
						$plObject->forecastVariables = implode(",",$plVariables);
						$response = $forecastingProvider->insertPL($plObject);
						$responses[] = $response;
					}
				}
				if (count($responses)){
					$results = "Forecast Revenues Successfully Saved";
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}
        break;
}
output_json($results);
die();
?>