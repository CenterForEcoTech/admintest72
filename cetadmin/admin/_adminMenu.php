<?php
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Admin)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Admin))){?>
			<li><a id="manage-adminusers" href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-adminusers" class="button-link">Admin Users</a></li> 
			<li><a id="manage-siteonfigs" href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-siteonfigs" class="button-link">Site Configs</a></li>
			<li><a id="manage-securitygroups" href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-securitygroups" class="button-link">Security Groups</a></li>
			<li><a id="get-sharepointdata" href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=get-sharepointdata" class="button-link">Get Sharepoint Data</a></li>
			<li><a id="cronjobs" href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=cronjobs" class="button-link">Cron Jobs</a></li>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    <?php if ($scrollId) {
        // to support the site config tab, we want to scroll to AFTER the tab has received focus
        ?>

        $('html, body').animate({
            scrollTop: $("#<?php echo $scrollId;?>").offset().top - 250
        }, 500);
<?php } ?>
		
    });
</script>
<?php }?>