<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - Admin tools";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "manage-adminusers":
            $displayPage = "views/_manageAdminUsers.php";
            break;
        case "manage-siteonfigs":
            $displayPage = "views/_manageSiteConfigs.php";
            break;
        case "manage-securitygroups":
            $displayPage = "views/_manageSecurityGroups.php";
            break;
        case "get-sharepointdata":
            $displayPage = "views/_getSharePointData.php";
            break;
        case "cronjobs":
            $displayPage = "views/_cronJobs.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
// CHECK if we're on production
$headerEnvSuffix = '';
$prod_http_host  = 'cetdashboard.info';
//$prod_http_host  = 'localhost:8088'; //for testing locally
if($_SERVER['HTTP_HOST'] == $prod_http_host){
	// If so, add warning page header
	$headerEnvSuffix = ' <span style="display: inline-block; background-color:red; color: white;">PRODUCTION</span>';
}
echo '
<div class="container">
<h1>Admin Administration' . $headerEnvSuffix . '</h1>';
include_once("_adminMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>
