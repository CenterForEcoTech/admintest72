<?php
include_once("../_config.php");
$repo = $_POST['repo'];
$repoChoice = $_POST['repoChoice'];

if (!$repo){
	if ($_POST['readystate'] == "yesImready"){
		echo "<form action=\"deploy.php\" method=\"post\"><input type=\"hidden\" name=\"repo\" value=\"".$repoChoice."\">";
		echo "<input type=\"submit\" value=\"Click When You Are Ready To Pull In Updates and Use ".$repoChoice." Repository\" style=\"color:green;font-size:18pt;\">";
		echo "</form>";
	}else{
		echo "Not Authorized<Br>";
	}
}else{
	$options = array(
		'log' => 'deployments.log',
		'date_format' => 'Y-m-d H:i:sP',
		'branch' => $repo,
		'remote' => 'origin',
	);
	date_default_timezone_set('America/New_York');

	class Deploy {

	  /**
	  * A callback function to call after the deploy has finished.
	  * 
	  * @var callback
	  */
	  public $post_deploy;
	  
	  /**
	  * The name of the file that will be used for logging deployments. Set to 
	  * FALSE to disable logging.
	  * 
	  * @var string
	  */
	  private $_log = 'deployments.log';
	  private $_loghead = 'deployment_head.log';

	  /**
	  * The timestamp format used for logging.
	  * 
	  * @link    http://www.php.net/manual/en/function.date.php
	  * @var     string
	  */
	  private $_date_format = 'Y-m-d H:i:sP';

	  /**
	  * The name of the branch to pull from.
	  * 
	  * @var string
	  */
	  private $_branch = 'default';

	  /**
	  * The name of the remote to pull from.
	  * 
	  * @var string
	  */
	  private $_remote = 'origin';

	  /**
	  * The directory where your website and hg repository are located, can be 
	  * a relative or absolute path
	  * 
	  * @var string
	  */
	  private $_directory;

	  /**
	  * Sets up defaults.
	  * 
	  * @param  string  $directory  Directory where your website is located
	  * @param  array   $data       Information about the deployment
	  */
	  public function __construct($directory, $options = array())
	  {
		  // Determine the directory path
		  $this->_directory = realpath($directory).DIRECTORY_SEPARATOR;

		  $available_options = array('log', 'date_format', 'branch', 'remote');

		  foreach ($options as $option => $value)
		  {
			  if (in_array($option, $available_options))
			  {
				  $this->{'_'.$option} = $value;
			  }
		  }

		  $this->log('Attempting deployment...');
	  }

	  /**
	  * Writes a message to the log file.
	  * 
	  * @param  string  $message  The message to write
	  * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
	  */
	  public function log($message, $type = 'INFO')
	  {
		  if ($this->_log)
		  {
			  // Set the name of the log file
			  $filename = $this->_log;

			  if ( ! file_exists($filename))
			  {
				  // Create the log file
				  file_put_contents($filename, '');

				  // Allow anyone to write to log files
				  chmod($filename, 0666);
			  }

			  // Write the message into the log file
			  // Format: time --- type: message
			  if ($type == 'SPACE'){
				file_put_contents($filename,$message.PHP_EOL, FILE_APPEND);
			  }else{
				file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
			  }
		  }
	  }
	  public function loghead($message)
	  {
		  if ($this->_loghead)
		  {
			  // Set the name of the log file
			  $filename = $this->_loghead;

			  if ( ! file_exists($filename))
			  {
				  // Create the log file
				  file_put_contents($filename, '');

				  // Allow anyone to write to log files
				  chmod($filename, 0666);
			  }

			  // Write the message into the log file
			  // Format: time --- type: message
			file_put_contents($filename, date($this->_date_format)."\n".$message);
		  }
	  }

	  /**
	  * Executes the necessary commands to deploy the website.
	  */
	  public function execute()
	  {
		  try
		  {
			  // Make sure we're in the right directory
			  exec('cd '.$this->_directory, $output);
			  //$this->log('Changing working directory... '.implode(' ', $output));
			  $this->log("Updating ".$this->_directory." '".$this->_branch."' branch");
			  // This will remove all files and rebuild them from scratch...not to be used regularly
			  //exec('hg update --clean', $output);
			  //$this->log('Reseting repository... '.implode(' ', $output));

			  // Update the local repository
			  exec('hg pull', $output2);
			  $this->log("Pulling in changes...hg pull:\n\t\t".implode(" ", $output2));

			  // Reveal changes captured
			  exec('hg heads', $output3);
			  $this->log("hg heads:\n\t\t".implode("\n\t\t", $output3));

			  // Discard any changes to tracked files since our last deploy
			  exec('hg update '.$this->_branch, $output4);
			  $this->log("Loading repository for '".$this->_branch."' branch ...hg update ".$this->_branch.":\n\t\t ".implode(' ', $output4));

			  
			  //TODO check to see if current branch is same as updating $this->_branch...if different then do clean
			  // This will remove all files and rebuild them from scratch...not to be used regularly
			  exec('hg update --clean', $output5);
			  $this->log("Reseting repository for'".$this->_branch."' branch...hg update --clean :\n\t\t ".implode(' ', $output5));

			  
			  //create log for reviewing just changes to this branch for rolling back purposes
			  $this->loghead(implode("\n", $output3).implode(' ', $output4));

			  // Secure the .hg directory
			  exec('chmod -R og-rx .hg');
			  $this->log("Securing .hg directory...");

			  if (is_callable($this->post_deploy))
			  {
				  call_user_func($this->post_deploy, $this->_data);
			  }

			  $this->log("Deployment successful.");
			  $this->log("*************************\n","SPACE");
		  }
		  catch (Exception $e)
		  {
			  $this->log($e, 'ERROR');
		  }
	  }

	  public function rollback()
	  {
		  try
		  {
			  // Make sure we're in the right directory
			  exec('cd '.$this->_directory, $output);
			  //$this->log('Changing working directory... '.implode(' ', $output));
			  $this->log("Rollback '".$this->_branch."' branch");

			  // Update the local repository
			  exec('hg rollback', $output2);
			  $this->log("Rolling back changes...hg rollback:\n\t\t".implode(" ", $output2));

			  // Discard any changes to tracked files since our last deploy
			  exec('hg update '.$this->_branch, $output4);
			  $this->log("Loading repository for '".$this->_branch."' branch ...hg update ".$this->_branch.":\n\t\t ".implode(' ', $output4));

			  // Secure the .hg directory
			  exec('chmod -R og-rx .hg');
			  $this->log("Securing .hg directory...");

			  if (is_callable($this->post_deploy))
			  {
				  call_user_func($this->post_deploy, $this->_data);
			  }

			  $this->log("Rollback successful.");
			  $this->log("*************************\n","SPACE");
		  }
		  catch (Exception $e)
		  {
			  $this->log($e, 'ERROR');
		  }
	  }

	}
	
	if ($_POST['rollback'] == "rollback"){
		$deploy = new Deploy('../',$options);
		$deploy->rollback();
	}else{
		// This is just an example
		$deploy = new Deploy('../',$options);
		$deploy->execute();
	}
	if (!$_POST['RollBack']){
	/*
		$url = "https://api.hipchat.com/v1/rooms/message?auth_token=9fadebee1df98d310d65e019f37946&room_id=81864&from=Deployment&message=".$repo."%20branch%20Deployed%20on%20".$_SERVER["SERVER_NAME"]."%20by%20".SessionManager::getAdminUsername();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$contents = curl_exec($ch);
		curl_close($ch);
	*/
		echo "<form action=\"deploy.php\" method=\"post\"><input type=\"hidden\" name=\"repo\" value=\"stag\"><input type=\"hidden\" name=\"rollback\" value=\"rollback\">";
		echo "Yikes!..<input type=\"submit\" name=\"RollBack\" value=\"RollBack! That Update was a Mistake!\" style=\"color:red;font-size:18pt;\">";
		echo "</form>";
		if ($repo == "default" || $repo == "Staging"){
			echo "<br>!!!Be sure to set local repository back to Yosh or Consultant before doing any more changes!!!";
		}
		echo "<br>Prior Changeset shown below for '".$repo."' branch:<br>";
		$headlog = nl2br(file_get_contents('deployment_head.log'));
		//echo $headlog;
		$headlog_array = explode("changeset:",$headlog);
		$c = 0;
		while ($c < count($headlog_array)){
			if ($repo == "default"){
				if (!strpos($headlog_array[$c],"branch:")){
					echo "ChangeSet: ".$headlog_array[$c]."<hr>";
				}
			}else{
				if (strpos(str_replace(" ","",$headlog_array[$c]),"branch:".$repo,0)){
					echo "ChangeSet: ".$headlog_array[$c]."<hr>";
				}
			}
			$c++;
		}
	} else {
		echo "<h4 style=\"color:blue;font-weight:bold;\">Phew...that was close!</h4>Thank goodness we have such a mighty tool to prevent epic failures like the ones you just prevented.<br>
			Be sure to add your name to the Hero's board and thank the one responsible for giving you the opportunity to get there...and provide some pointers on what would have prevented you from becoming the hero in the first place while you are at it.<Br>";
		echo "<br><a href=\"index.php\">Get me outta here.</a><br><Br>Have a calming beverage while you are at it.<br><img src=\"chillout.jpg\" height=\"75%\"><Br>";
	}
}//end valid call to page
?>