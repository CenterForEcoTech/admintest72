<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."AdminUsersProvider.php");
$adminUserProvider = new AdminUsersProvider($mysqli);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $adminUserProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AdminUsersByID[$record->id] = $record;
	if ($record->displayOrderId){
		$AdminUsersActive[$record->displayOrderId]=$record;
		$AdminUsersByFullName[$record->fullName]=$record;
		$SecurityGroupsMembers[$record->securityGroupId][] = $record;
	}else{
		$AdminUsersInActive[$record->firstName]=$record;
	}
}
//resort the products without display order by AdminUserID
ksort($AdminUsersInActive);
$SelectedAdminUserID = $_GET['AdminUserID'];
$SelectedAdminUserName = $_GET['AdminUserName'];
if ($SelectedAdminUserName && !$SelectedAdminUserID){
	$SelectedAdminUserID = $AdminUsersByFullName[$SelectedAdminUserName]->id;
}
$addAdminUser = $_GET['addadminuser'];
$displayOrderId = $AdminUsersByID[$SelectedAdminUserID]->displayOrderId;
if ($addAdminUser){$displayOrderId = (count($AdminUsersActive)+1);}


include_once($dbProviderFolder."AdminSecurityGroupsProvider.php");
$adminSecurityGroupProvider = new AdminSecurityGroupsProvider($mysqli);
$paginationResult = $adminSecurityGroupProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$SecurityGroups[$record->id] = $record->name;
}

?>
<?php if (!$addAdminUser){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:380px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li {
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .employeeDescription {font-weight:normal;}
  .adminUserName {font-weight:bold;}

  </style>
<br>
  
	<div class="fifteen columns">
		<?php include('_AdminUsersComboBox.php');?>
		<div class="two columns">
			<button id="showAdminUserSelectForm" style="font-size:8pt;">Change Display Order</button>
		</div>
	</div>
	<fieldset id="adminUserSelectForm">
		<legend>Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Active Admin Users</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($AdminUsersActive as $admin=>$adminInfo){
						echo "<li id=\"".$adminInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-adminusers&AdminUserID=".$adminInfo->id."\">".$adminInfo->fullName."</a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>InActive Admin Users</b></div>
				<ul id="sortable2" class="connectedSortable">
				<?php 
					foreach ($AdminUsersInActive as $admin=>$adminInfo){
						echo "<li id=\"".$adminInfo->id."\" class=\"ui-state-disabled\"><a href=\"?nav=manage-adminusers&AdminUserID=".$adminInfo->id."\" style=\"text-decoration:none;\"><span class=\"adminUserName\">".$adminInfo->fullName."</span></a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addadminuser?>
<?php if ($SelectedAdminUserID || $addAdminUser){?>
	<form id="AdminUserUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Admin User Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addAdminUser){?><input type="text" name="id" value="<?php echo $AdminUsersByID[$SelectedAdminUserID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Contact Info</legend>
						<div class="one columns">
							<img class="picture_id1" src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $AdminUsersByID[$SelectedAdminUserID]->userEmail;?>&size=HR64x64">
							<img class="picture_id2" src="https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $AdminUsersByID[$SelectedAdminUserID]->userEmail;?>&size=HR64x64">
						</div>
						<div class="seven columns">
							<div class="two columns">First Name:</div><div class="four columns"><input type="text" id="firstName" name="firstName" value="<?php echo $AdminUsersByID[$SelectedAdminUserID]->firstName;?>"></div>
							<br clear="both">
							<div class="two columns">Last Name:</div><div class="four columns"><input type="text" id="lastName" name="lastName" value="<?php echo $AdminUsersByID[$SelectedAdminUserID]->lastName;?>"></div>
							<br clear="both">
							<div class="two columns">Email:</div><div class="four columns"><input type="text" name="userEmail" value="<?php echo $AdminUsersByID[$SelectedAdminUserID]->userEmail;?>"></div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Security Group Access</legend>
						<div class="one columns">
							&nbsp;
						</div>
						<div class="seven columns">
							<div class="twelve columns">
									<?php
										$securityGroupsArray = explode(",",$AdminUsersByID[$SelectedAdminUserID]->securityGroups);
										foreach ($SecurityGroups as $SecurityGroupID=>$SecurityGroup){
											echo "<input class='securityGroups' name='securityGroups' type='checkbox' value='".$SecurityGroupID."'".(in_array($SecurityGroupID,$securityGroupsArray) ? " checked":"").">".$SecurityGroup."<br>";
										}
									?>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Primary Security Group</legend>
						<div class="one columns">
							&nbsp;
						</div>
						<div class="seven columns">
							<div class="two columns"><a href="?nav=manage-securitygroups&SecurityGroupID=<?php echo $AdminUsersByID[$SelectedAdminUserID]->securityGroupId;?>">Security Group</a>:</div>
							<div class="four columns">
								<select name="securityGroupId" id="securityGroupId">
										<option value="">No Access</option>
									<?php
										foreach ($SecurityGroups as $SecurityGroupID=>$SecurityGroup){
											echo "<option value='".$SecurityGroupID."'".($SecurityGroupID==$AdminUsersByID[$SelectedAdminUserID]->securityGroupId ? " selected":"").">".$SecurityGroup."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="adminUserResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-adminusers&AdminUserID=<?php echo $SelectedAdminUserID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addAdminUser){?>
					<a href="save-adminuser-add" id="save-adminuser-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="adminuser_add">
				<?php }else{?>
					<a href="save-adminuser-update" id="save-adminuser-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="adminuser_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
			<fieldset>
				<legend>Other Users in This Security Group</legend>
				<div class="fifteen columns">
						<ul id="orgchart-source" style="display:none;">
							<?php
								echo "<li><em>".$SecurityGroups[$AdminUsersByID[$SelectedAdminUserID]->securityGroupId]."</em>";
								echo "<ul>";
								foreach ($SecurityGroupsMembers[$AdminUsersByID[$SelectedAdminUserID]->securityGroupId] as $id=>$Members){
									echo "<li class=\"big\"><a href=\"?nav=manage-adminusers&AdminUserID=".$Members->id."\">".$Members->fullName."</a>";
									if ($Members->userEmail == "it@cetonline.org"){
										echo "<br clear=\"all\"><img class=\"noprofilepicture\" src=\"".$CurrentServer."images/noprofilepic.png\">";
									}else{
										echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$Members->userEmail."&size=HR64x64\">
										<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$Members->userEmail."&size=HR64x64\">";
									}
									echo "</li>";
								}
								echo "</ul>";
							?>
						</ul>
						<div id="orgchart-container" class="reset-this"></div>					

				</div>
			</fieldset>
	</form>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-adminusers&addadminuser=true" class="button-link">+ Add New Admin User</a>
		</div>
	<?php }?>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>

<script>
$(function(){
	$('#orgchart-source').orgChart({container: $('#orgchart-container')});
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	$("#adminUserSelectForm").hide();	
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedAdminUserID){?>
		$("#adminUserSelectForm").hide();
	<?php }?>
	$("#showAdminUserSelectForm").click(function(){
		$("#adminUserSelectForm").toggle();		
	});

	$("#adminUserSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	var securityGroups = $(".securityGroups");
	$("#securityGroupId").on('change',function(){
		var $this = $(this);
		$.each(securityGroups,function(key,obj){
			var thisObj = $(obj)
			if ($this.val() == thisObj.val()){
				thisObj.attr('checked','checked');
			}
		});
		
	});
	
	 var formElement = $("#AdminUserUpdateForm");
	 
	 formElement.validate({
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
				email: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-adminuser-update").click(function(e){

        $('#adminUserResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiAdminUserManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					console.log(data);
                    $('#adminUserResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#adminUserResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-adminuser-add").click(function(e){

        $('#adminUserResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiAdminUserManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#adminUserResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#adminUserResults').html(message).addClass("alert");
                }
            });
        }
    });
	
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
			$(".noprofilepicture").hide();
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
			$(".noprofilepicture").hide();
		}
	});
	
	
});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
				if ($(ui.item).parents('#sortable1').length > 0) {
					$(ui.item).switchClass('ui-state-disabled', 'ui-state-default');
				} else {
					$(ui.item).switchClass('ui-state-default', 'ui-state-disabled');
				}					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiAdminUserManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
	<?php }//not in readonly?>
});
</script>