<style>
.active {color:green;}
.inactive {color:red;}
</style>
<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."AdminProvider.php");
$adminProvider = new AdminProvider($mysqli);
$cronJobResult = $adminProvider->getCronJobs();
foreach ($cronJobResult as $cronJob){
	$id = $cronJob->id;
	$title = $cronJob->title;
	$recipients = $cronJob->recipients;
	$startDate = $cronJob->startDate;
	$schedule = $cronJob->schedule;
	$message = $cronJob->message;
	$lastRun = $cronJob->lastRun;
	$lastRunStatus = $cronJob->lastRunStatus;
	$active = $cronJob->active;
	?>
		<div class="row">
			<div class="one column<?php echo ($active ? " active" : " inactive");?>" style="text-align:right;"><?php echo ($active ? " active" : " inactive");?></div>
			<div class="eleven columns">
				<span class="<?php echo ($active ? " active" : " inactive");?>"><b>Title:</b> <?php echo $title;?></span><br>
				<b>Recipients:</b> <?php echo $recipients;?><br>
				<b>Start Date:</b> <?php echo MySQLDate($startDate);?><br>
				<b>Schedule:</b> Every <?php echo $schedule;?><br>
				<b>Last Run:</b> <?php echo date("m/d/Y",strtotime($lastRun)).($lastRunStatus == "1" ? " successfully" : " with errors");?><br>
				<b>Next to Run:</b> <?php echo date("m/d/Y",strtotime($lastRun." +".$schedule));?><br>
				<b>Message:</b>
					<div style="border-bottom:1pt solid black;padding-left:20px;"><pre><?php echo $message;?></pre></div>
			</div>
		</div>
	<?php
}
?>