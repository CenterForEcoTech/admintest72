<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."AdminSecurityGroupsProvider.php");
$adminSecurityGroupsProvider = new AdminSecurityGroupsProvider($mysqli);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $adminSecurityGroupsProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$SecurityGroupsByID[$record->id] = $record;
	$SecurityGroupsActive[$record->displayOrderId]=$record;
}

//resort the products without display order by SecurityGroupID
$SelectedSecurityGroupID = $_GET['SecurityGroupID'];
$addSecurityGroup = $_GET['addsecuritygroup'];
$displayOrderId = $SecurityGroupsByID[$SelectedSecurityGroupID]->displayOrderId;
if ($addSecurityGroup){$displayOrderId = (count($SecurityGroupsActive)+1);}

//get employees in the department
if ($SelectedSecurityGroupID){
	include_once($dbProviderFolder."AdminUsersProvider.php");
	$adminUsersProvider = new AdminUsersProvider($mysqli);
	$criteria->showAll = false;
	$criteria->securityGroupId = $SelectedSecurityGroupID;
	$paginationResult = $adminUsersProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$SecurityGroupMembers[$record->fullName]["id"] = $record->id;
		$SecurityGroupMembers[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
		$AdminUsersActive[$record->displayOrderId]=$record;
	}

	ksort($SecurityGroupMembers);
}//end if selected Department
?>
<?php if (!$addSecurityGroup){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:150px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li {
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .employeeDescription {font-weight:normal;}
  .employeeName {font-weight:bold;}

  </style>
	<div class="fifteen columns"><bR>
		<button id="showSecurityGroupsSelectForm">Choose A Different Security Group</button>
	</div>
	<fieldset id="securityGroupsSelectForm">
		<legend>Choose A Security Group In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Security Groups</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($SecurityGroupsActive as $department=>$departmentInfo){
						echo "<li id=\"".$departmentInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-securitygroups&SecurityGroupID=".$departmentInfo->id."\" title=\"".$departmentInfo->description."\">".$departmentInfo->name."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedSecurityGroupID || $addSecurityGroup){?>
	<form id="SecurityGroupsUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Security Group Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addSecurityGroup){?><input type="text" name="id" value="<?php echo $SecurityGroupsByID[$SelectedSecurityGroupID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Details</legend>
						<div class="seven columns">
							<div class="two columns">Name:</div><div class="four columns"><input type="text" name="name" value="<?php echo $SecurityGroupsByID[$SelectedSecurityGroupID]->name;?>"></div>
							<br clear="all">
							<div class="two columns">Description:</div><div class="four columns"><input type="text" name="description" value="<?php echo $SecurityGroupsByID[$SelectedSecurityGroupID]->description;?>"></div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="securityGroupResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-securitygroups&SecurityGroupID=<?php echo $SelectedSecurityGroupID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addSecurityGroup){?>
					<a href="save-securitygroup-add" id="save-securitygroup-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="securitygroup_add">
				<?php }else{?>
					<a href="save-securitygroup-update" id="save-securitygroup-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="securitygroup_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
		<fieldset>
			<legend>Admin Users in this Security Group</legend>
			<div class="fifteen columns">
				<?php 
				if (count($AdminUsersActive)){
					include('_AdminUsersComboBox.php');
				}
				?>
				<br clear="all"><br>
				<ul id="orgchart-source" style="display:none;">
					<?php
						echo "<li><em>".$SecurityGroupsByID[$SelectedSecurityGroupID]->name."</em>";
						echo "<ul>";
						foreach ($AdminUsersActive as $id=>$Members){
							echo "<li class=\"big\"><a href=\"?nav=manage-adminusers&AdminUserID=".$Members->id."\">".$Members->fullName."</a>";
							if ($Members->userEmail == "it@cetonline.org"){
								echo "<br clear=\"all\"><img class=\"noprofilepicture\" src=\"".$CurrentServer."images/noprofilepic.png\">";
							}else{
								echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$Members->userEmail."&size=HR64x64\">
								<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$Members->userEmail."&size=HR64x64\">";
							}
							echo "</li>";
						}
						echo "</ul>";
					?>
				</ul>
				<div id="orgchart-container" class="reset-this"></div>					
			</div>
			<br clear="all"><br>
			<div class="fifteen columns">
				<div id="orgchart-container" class="reset-this"></div>
			</div>
		</fieldset>
	</form>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>admin/?nav=manage-securitygroups&addsecuritygroup=true" class="button-link">+ Add New Security Group</a>
		</div>
	<?php }?>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>
<script>
$(function(){
	$('#orgchart-source').orgChart({
		container: $('#orgchart-container'),
		interactive: true,
		showLevels: 2,
		stack: true,
		depth: 2
	});
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});


	$("#showSecurityGroupsSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedSecurityGroupID){?>
		$("#securityGroupsSelectForm").hide();
		$("#showSecurityGroupsSelectForm").show();
	<?php }?>
	$("#showSecurityGroupsSelectForm").click(function(){
		$("#securityGroupsSelectForm").show();		
		$(this).hide();
	});

	$("#securityGroupsSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#SecurityGroupsUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-securitygroup-update").click(function(e){

        $('#securityGroupResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiSecurityGroupsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#securityGroupResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#securityGroupResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-securitygroup-add").click(function(e){

        $('#securityGroupResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiSecurityGroupsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#securityGroupResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#securityGroupResults').html(message).addClass("alert");
                }
            });
        }
    });
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});


});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiSecurityGroupsManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
	<?php }//not in readonly?>
});
</script>