<?php
$ER = $_GET['ER'];
$Test = $_GET['T'];
$ErrorLogShown = false;
if ($_GET['EL'] == 'remove'){
	unlink('error_log.log');
}
if (isset($_GET['ER']) && $_GET['ER']){
	error_reporting(E_ALL);
	ini_set('display_errors','On');
	ini_set('error_log','error_log.log');
}
?>
<form method="get" id="ERForm" action="?nav=tools-error-testing">
	<input type="hidden" name="nav" value="tools-error-testing">
    <label for="ERCheck">
	    <input id="ERCheck" type="checkbox" name="ER" value="true"<?php if ($_GET['ER']){echo " checked";}?>>Turn on Error Reporting
    </label>
</form>
<script>
    $("#ERCheck").on('click',function(){
        $("#ERForm").submit();
    });
</script>
<?php
	if (file_exists('error_log.log')){
		$ErrorLogShown = true;
		echo "<a href=\"error_log.log\" target=\"ErrorLog\">error_log.log</a>";
		echo "<br><a href=\"?nav=tools-error-testing&EL=remove&ER=".$ER."\">Clear error_log.log file</a>";
	}
?>
<ul>
	<li><a href="?nav=tools-error-testing&T=phpInfo&ER=<?php echo $ER;?>">PHPInfo()</a>
	<li><a href="?nav=tools-error-testing&T=badDB&ER=<?php echo $ER;?>">Incorrect DB Connection error</a>
	<li><a href="?nav=tools-error-testing&T=badSQL&ER=<?php echo $ER;?>">Other sql error</a> (like invalid column name, no value assigned to parameter, etc)
	<li><a href="?nav=tools-error-testing&T=badPHP&ER=<?php echo $ER;?>">PHP Parse error</a>
	<li><a href="?nav=tools-error-testing&T=PHPFatalError&ER=<?php echo $ER;?>">PHP Fatal Errors</a>
    <li><a href="?nav=tools-error-testing&T=fatal&ER=<?php echo $ER;?>">Initialize unknown class</a>
    <li><a href="?nav=tools-error-testing&T=error&ER=<?php echo $ER;?>">Triggered User Error</a>*
    <li><a href="?nav=tools-error-testing&T=warning&ER=<?php echo $ER;?>">Triggered User Warning</a>*
    <li><a href="?nav=tools-error-testing&T=notice&ER=<?php echo $ER;?>">Triggered User Notice</a>*
	<?php if (($Test == 'PHPFatalError' || $Test == 'badPHP') && !$ErrorLogShown){?><br>[Note Fatal/Parse Errors stops the page from continuing. So link for error_log won't display nor will 'End Results'	(<a href="error_log.log" target="errorLog">Click Here Instead</a>)<?php }?>
</ul>
    * Add "&amp;text=1" or something to the URL if you want a different message for error log testing
<div id="results" style="white-space:pre-linewrap;">
<?php
if ($Test){
	echo "<h4>Results</h4>";
	echo "<div style=\"padding-left:20px;\"><div style=\"border:1pt solid black;padding:5px;\">";
    switch ($Test){
        case "phpInfo":
			include("_toolsErrorTesting_PHPInfo.php");
            break;
        case "badDB":
			include("_toolsErrorTesting_BadDB.php");
            break;
        case "badSQL":
			include("_toolsErrorTesting_BadSQL.php");
            break;
        case "badPHP":
			include("_toolsErrorTesting_PHPParseError.php");
            break;
        case "PHPFatalError":
			include("_toolsErrorTesting_PHPFatalError.php");
            break;
        case "fatal":
            $junk = new NonexistentObjectType();
            break;
        case "error":
            trigger_error("this is a triggered error ".$_GET['text'], E_USER_ERROR);
            break;
        case "warning":
            trigger_error("this is a triggered warning ".$_GET['text'], E_USER_WARNING);
            break;
        case "notice":
            trigger_error("this is a triggered notice ".$_GET['text'], E_USER_NOTICE);
            break;
        default:
            break;
    }
	echo "</div></div>";
	echo "<h4>End Results</h4>";
}
?>
</div>
<?php
	if (file_exists('error_log.log') && !$ErrorLogShown){
		echo "<a href=\"error_log.log\" target=\"ErrorLog\">error_log.log</a>";
		echo "<br><a href=\"?nav=tools-error-testing&EL=remove&ER=".$_GET['ER']."\">Clear error_log.log file</a>";
	}
?>
