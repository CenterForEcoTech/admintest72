<h3>Email Log Tester</h3>
<p>Type a bad email address or delete the contents of the message body to generate an error.</p>
<p class="help">NOTICE: If you refresh the page, the email will send again!</p>
<?php
$formData = (object)array(
    "email_to" => CausetownSessionManager::getAdminEmail(),
    "subject" => "Test Email Subject",
    "body" => "Test email body"
);
if ($_POST['email_to']){
    $formData->email_to = htmlspecialchars($_POST['email_to']);
    $formData->subject = htmlspecialchars($_POST['subject']);
    $formData->body = htmlspecialchars($_POST['body']);
    include_once($dbProviderFolder."MySqliEmailLogger.php");
    include_once($repositoryApiFolder."EmailHelper.php");
    $emailMessage = new EmailMessage();
    $emailMessage->addAddress($formData->email_to, "");
    $emailMessage->setFrom($SMTPFromEmail, $SMTPFromEmailName."");
    $emailMessage->Subject = $formData->subject;
    $emailMessage->setHtmlBody($formData->body);

    $throwErrors = true;
    $emailLogger = new MySqliEmailLogger($mysqli);
    $emailProvider = new EmailProvider($emailLogger, $SMTPEmailhost, $SMTPEmailport, $SMTPEmailusername, $SMTPEmailpassword, $throwErrors);
    $emailResponse = $emailProvider->smtpSend($emailMessage, $isDevMode_Email);
}
?>

<form method="POST">
    <label>
        <div>To:</div>
        <input type="text" name="email_to" value="<?php echo $formData->email_to;?>">
    </label>
    <label>
        <div>Subject:</div>
        <input type="text" name="subject" value="<?php echo $formData->subject;?>">
    </label>
    <label>
        <div>Body:</div>
        <textarea name="body"><?php echo $formData->body;?></textarea>
    </label>
    <label>
    <input type="submit" value="Send Email">
    </label>
</form>

<h3>Results</h3>
    <p>Below is the response variable after attempting the email send. You should look for "emailSent", "logId" for relevant data, and check the DB manually to see if the log record appears as expected.</p>
<?php
if (isset($emailResponse)){
    var_dump($emailResponse);
}
?>