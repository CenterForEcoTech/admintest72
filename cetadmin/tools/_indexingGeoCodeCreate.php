<?php
include_once('..\Global_Variables.php');
//error_reporting(1);
include_once($repositoryApiFolder."DataContracts.php");
require_once($repositoryApiFolder."config.php");
include_once($dbProviderFolder."MemberProvider.php");

$memberProvider = new MerchantMemberProvider($mysqli);

$MissingGeoCodes = $memberProvider->getMissingGeoCodes();
echo "There are ".count($MissingGeoCodes)." records without GeoCodes.<br>";
$x = 0;
$merchantNames = array();
if (count($MissingGeoCodes)){
	foreach ($MissingGeoCodes as $mCodes=>$missingCodes){
		$getLat = "";
		$getLong = "";
		//print_r($missingCodes);
		//echo "'a=".$missingCodes["MerchantInfo_Address"]." c=".$missingCodes["MerchantInfo_City"]." s=".$missingCodes["MerchantInfo_State"]." z=".$missingCodes["MerchantInfo_Zip"]."'<br>";
			$GeoCode = GetGeoCode($missingCodes["MerchantInfo_Address"].",".$missingCodes["MerchantInfo_City"].",".$missingCodes["MerchantInfo_State"]." ".$missingCodes["MerchantInfo_Zip"]);
			$GeoCodeLatLong = explode(",",$GeoCode);
			$getLat = $GeoCodeLatLong[0];
			$getLong = $GeoCodeLatLong[1];
			//echo "first run=".trim($getLat).",".trim($getLong)."<br>";
			$x++;
			if (trim($getLat)=="" || trim($getLong)==""){
				//try again without zip
				//echo urlencode($missingCodes["MerchantInfo_Address"].",".$missingCodes["MerchantInfo_City"].",".$missingCodes["MerchantInfo_State"])."<Br>";
				$GeoCode = GetGeoCode(urlencode($missingCodes["MerchantInfo_Address"].",".$missingCodes["MerchantInfo_City"].",".$missingCodes["MerchantInfo_State"]));
				//print_r($GeoCode);
				$GeoCodeLatLong = explode(",",$GeoCode);
				$getLat = $GeoCodeLatLong[0];
				$getLong = $GeoCodeLatLong[1];
				//echo "second run=".trim($getLat).",".trim($getLong)."<br>";
				$x++;
			}
			if (trim($getLat)=="" || trim($getLong)==""){
				//try again without address
				//echo $missingCodes["MerchantInfo_City"].",".$missingCodes["MerchantInfo_State"]."<br>";
				$GeoCode = GetGeoCode($missingCodes["MerchantInfo_City"].",".$missingCodes["MerchantInfo_State"]);
				$GeoCodeLatLong = explode(",",$GeoCode);
				$getLat = $GeoCodeLatLong[0];
				$getLong = $GeoCodeLatLong[1];
				//echo "third run=".trim($getLat).",".trim($getLong)."<br>";
				$x++;
			}
		//echo "lat=".$getLat.", long=".$getLong;
		
		//update the member record
		$updateMerchantGeoCodes = $memberProvider->updateMissingGeoCodes($getLat,$getLong,$missingCodes["MerchantInfo_ID"]);
		//print_r($updateMerchantGeoCodes);
		//echo "\r\n<hr>";
		array_push($merchantNames,"<span style=\"text-decoration:underline;font-weight:bold;\">".$missingCodes["MerchantInfo_Name"].":</span> ".$missingCodes["MerchantInfo_Address"].", ".$missingCodes["MerchantInfo_City"].", ".$missingCodes["MerchantInfo_State"]." ".$missingCodes["MerchantInfo_Zip"]." [".$getLat.",".$getLong."]");
		
		if ($x==10){
			$breakAlert = "Stopped after $x attempts to prevent exceeding Google API calls per second limits.<Br>"
				."<a id=\"indexing-event-latlong\" href=\"".$CurrentServer.$adminFolder."indexing/?nav=indexing-event-latlong\" class=\"button-link\">Continue Processing</a>";
			break;
		}
	}
	echo "The following merchants were updated:<ul>";
	foreach ($merchantNames as $merchantnames){
		echo "<li>".$merchantnames."</ li>";
	}
	echo "</ul>".$breakAlert;
}
//print_r($documents);
?>
