<?php
error_reporting(1);
ini_set('max_execution_time', 1200); //20 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);


include_once('FTPSProvider.php');

$FTPFileFolder = str_replace("//","/",$siteRoot).$adminFolder.$Config_FTP_CSGSaveFileLocation;


// *** Create the FTP object
$ftpObj = new FTPClient();
 
// *** Connect
if ($ftpObj -> connect($Config_FTP_CSGServer, $Config_FTP_CSGUsername, $Config_FTP_CSGPassword,true)){ 
    // *** Then add FTP code here
    $ftpMessage = "Successfully Connected to FTP Server<br>\r";
} else {
    $ftpMessage = "Failed to connect<br>\r";
}
echo ($CronJob ? str_replace("<br>","",$ftpMessage) : $ftpMessage);
// *** Get folder contents
//$ftpObj->changeDir($Config_FTP_CSGSourceLocation); //Currently not needed as file location is in root
$contentsArray = $ftpObj->getDirListing();
//echo count($contentsArray)." files in ".$Config_FTP_CSGSourceLocation." directory<br>";
$fileCountToBeDownloaded = 0;
$fileCountAlreadyDownloaded = 0;

foreach ($contentsArray as $fileRecord){

	$fileInfo = explode(" ",$fileRecord);
	foreach ($fileInfo as $fileinfo){
		if (strpos($fileinfo,".")){
			$fileName = $fileinfo;
		}
	}
	$fileName = str_replace("//","/",$fileName);
	$fileFrom = $fileName;      # The location on the server
	$fileTo = $FTPFileFolder.$fileName;            # Local dir to save to
	//FIRST CHECK TO SEE IF FILE WAS ALREADY ADDED THEN RUN THE SCRIPTS BELOW
	if (strpos(" ".$fileName,"CET_ISM_20")){ //only download the ISM files for the 21st century
		//also only get files that start in Nov 2014 because of when 
		$fileNameParts = explode("CET_ISM_",$fileName);
		if ((int)$fileNameParts[1] > 20141101){
			//echo $fileName."<br>";
			$results = $CSGDailyDataProvider->searchDailyDataFileHistory($fileTo);
			$successCount = 0;
			if (!$results->totalRecords){
				// *** Download file
				$ftpObj->downloadFile($fileFrom, $fileTo);
				$fileCountToBeDownloaded++;


				//print_pre($ftpObj -> getMessages());
			}else{	
				$fileCountAlreadyDownloaded++;
			}
		}
	}
} 
$fileMessage = $fileCountToBeDownloaded." Files No Yet Imported<br>\r".$fileCountAlreadyDownloaded." Files Already Imported<br>\r";
echo ($CronJob ? str_replace("<br>","",$fileMessage) : $fileMessage);

include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($FTPFileFolder, false, false);
foreach ($files as $file){
	$fileLocation = str_replace("\\","",$file);
	$fileLocation = str_replace("//","/",$fileLocation);
	$checkingFile = "Downloading ".$fileLocation."<br>";
	//FIRST CHECK TO SEE IF FILE WAS ALREADY ADDED THEN RUN THE SCRIPTS BELOW
	$results = $CSGDailyDataProvider->searchDailyDataFileHistory($fileLocation);
	$successCount = 0;
	if (!$results->totalRecords){
		//$checkingFile .= "file not yet imported<br>";
		$txt_file    = file_get_contents($fileLocation);
		$rows        = explode("\n", $txt_file);
		//put the first row as column headers
		foreach ($rows as $data=>$val){
			$content = explode(";",$val);
			if (count($content)<2){$content = explode(",",$val);}
			if ($data < 1){
				$CSGData["Columns"] = $content;
			}else{
				$CSGData["Rows"][] = $content;
			}
		}
		//convert column names to standardized format
		foreach ($CSGData["Columns"] as $Id=>$Column){
			$ConvertColumnName = $CSGDailyRecord::convertColumnName($Column);
			$CSGData["Columns"][$Id]=trim($ConvertColumnName);
		}
		//update row information with column name so that they can be matched to CSGDailyDataRecord
		foreach ($CSGData["Rows"] as $ID=>$value){
			foreach ($value as $colID=>$rowVal){
				$cleanRowVal = str_replace('"','',$rowVal);
				//adjust variation in date format
				if (($CSGData["Columns"][$colID] == "installed_dt" || $CSGData["Columns"][$colID] == "apptDate") && strpos($cleanRowVal,"/")){
					$cleanRowValDate = explode(" ",$cleanRowVal);
					$cleanRowVal = MySQLDateUpdate($cleanRowValDate[0]);
				}
				if ($CSGData["Columns"][$colID] == "crew"){
					$cleanRowVal = strtoupper($cleanRowVal);
				}
				$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
			}
		}
		$checkingFile .=  "Columns and Data Parsed.  Beginning to Insert Multiple Rows<br>\r";
//		print_pre($CSGDataArray);
		$results = $CSGDailyDataProvider->insertDailyDataMulitpleRows($CSGDataArray,$fileLocation);
		foreach ($results["insertResults"] as $result){
			$successCount = $successCount+$result->success;
		}
		//Now update the counts
/*		
		$checkingFile .=  "Now Updating Inventory Count<br>\r";
		foreach ($results["updateCountInfo"] as $transferInfo){
			$warehouseProvider = new WarehouseProvider($dataConn);
			$productProvider = new ProductProvider($dataConn);
			$TransferResults = $productProvider->transfer($transferInfo, 9999);
			$results = $TransferResults;					
			$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
			$results[0]["FromWarehouseChange"] = $updateResults["FromWarehouseChange"];
			$results[0]["ToWarehouseChange"] = $updateResults["ToWarehouseChange"];
			$resultCollection["updateCountResults"][] = $results;
		}
	
		$successFiles[] = $results["fileHistory"];
		//insert all the rows at once
		$checkingFile .=  $successCount." Rows Imported and Inventory Count Adjusted";
		//print_pre($results);
*/
		$checkingFile .=  $successCount." Rows Imported but Inventory Count NOT adjusted";
		
		break;  
	}else{
		$checkingFileImported = $checkingFile."File already imported<br>\r";
	}// end if file not yet inserted
}
if ($successCount){?>
	<?php echo ($CronJob ? str_replace("<br>","",$checkingFile) : $checkingFile);?>
	<?php if (!$CronJob){?>
		<div>Proceeding to Next File<span id="dots"></span></div>
		<script type="text/javascript">
		$(function(){
			var dots = 0;
			function type() {
				if(dots < 3) {
					$('#dots').append('.');
					dots++;
				} else {
					$('#dots').html('');
					dots = 0;
				}
			}
			setInterval (type, 600);
			location.reload();
		});
		</script>
	<?php }?>
<?php }else{?>
	All Files have been imported<br>
<?php }?>