<?php
error_reporting(1);
ini_set('max_execution_time', 300); //5 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
if($Config_UseManualDataForMinCount == "nonotuck"){
	$endDate = $TodaysDate;
	$startDate = date("Y-m-d", strtotime($endDate." - ".$Config_DaysForInstallCount." days"));

	//get movement history for nonotuck
	$criteria = new stdClass();
	$criteria->movementType = "Transfer";
	$criteria->startDate = $startDate;
	$criteria->endDate = $endDate;
	$criteria->warehouseFrom = "Nonotuck";
	$criteria->notReturned = true;
	$movementHistoryResults = $productProvider->getProductMovmentHistory($criteria);
	$movementHistory = $movementHistoryResults->collection;
	foreach ($movementHistory as $movement){
		$EFI = $movement->efi;
		$units = $movement->units;
		$MovementTotals[$EFI] = $MovementTotals[$EFI]+$units;
	}
	foreach ($MovementTotals as $efi=>$units){
		$record = new stdClass();
		$record->minimum = $units;
		$record->efi = $efi;
		$results = $productProvider->updateMinimum($record);
	}

	if ($results->success){
		echo " Minimum Counts Updated Based on Nonotuck Movement Out over the past ".$Config_DaysForInstallCount." days\n ";
	}else{
		echo " There was an error in the update of minimum counts\n ";
		print_pre($results);
	}
	echo "<br>\n";
	//get count for days on hand purposes
	$endDate = $TodaysDate;
	$startDate = date("m/d/Y", strtotime($endDate." - ".$Config_DaysForDaysOnHandCount." days"));
	$criteria = new stdClass();
	$criteria->movementType = "Transfer";
	$criteria->startDate = $startDate;
	$criteria->endDate = $endDate;
	$criteria->warehouseFrom = "Nonotuck";
	$movementHistoryResults = $productProvider->getProductMovmentHistory($criteria);
	$movementHistory = $movementHistoryResults->collection;
	$MovementTotals = array();
	foreach ($movementHistory as $movement){
		$EFI = $movement->efi;
		$units = $movement->units;
		$MovementTotals[$EFI] = $MovementTotals[$EFI]+$units;
	}
	foreach ($MovementTotals as $efi=>$units){
		$record = new stdClass();
		$record->qty = floor($units/$Config_DaysForDaysOnHandCount);
		$record->efi = $efi;
		$results = $productProvider->updateDaysOnHand($record);
	}
	if ($results->success){
		echo " Days On Hand Counts Updated Based on Average Nonotuck Movement Out over the past ".$Config_DaysForDaysOnHandCount." days\n ";
	}else{
		echo " There was an error in the update of minimum counts\n ";
		print_pre($results);
	}
}elseif($Config_UseManualDataForMinCount == "manual"){
	$products = $productProvider->get();
	foreach ($products->collection as $id=>$product){
		$record->qty = $product->minimumQty;
		$record->efi = $product->efi;
		$record->days = 21;
		$results = $productProvider->insertMinimumHistory($record);
	}
	if ($results->success){
		echo "Using Manual Override for Minimun On Hand Count\n";
	}else{
		echo " There was an error in the update of minimum counts using manual data\n ";
		print_pre($results);
	}
}else{

	//Get count for min on hand purposes
	$endDate = MySQLDateUpdate($TodaysDate);
	$startDate = date("Y-m-d", strtotime($endDate." -".$Config_DaysForInstallCount." days"));
	$results = $CSGDailyDataProvider->getPriorDailyData($startDate,$endDate);
	foreach ($results as $ID=>$CSGRecord){
		foreach ($CSGRecord as $id=>$csgRecord){
			if (trim($csgRecord->EFIPART)){
				$efi = trim($csgRecord->EFIPART);
				$qty = (int)$csgRecord->QTY;
				$CSGRecordCount[$efi] = $CSGRecordCount[$efi]+$qty;
			}
		}
	}
	foreach ($CSGRecordCount as $EFI=>$qty){
			$record->minimum = $qty;
			$record->efi = $EFI;
			$results = $productProvider->updateMinimum($record);
	}
	if ($results->success){
		echo " Minimum Counts Updated Based on Usage over the past ".$Config_DaysForInstallCount." days\n ";
	}else{
		echo " There was an error in the update of minimum counts\n ";
		print_pre($results);
	}
	echo "<br>\n";
	//get count for days on hand purposes
	$endDate = MySQLDateUpdate($TodaysDate);
	$startDate = date("Y-m-d", strtotime($endDate." -".$Config_DaysForDaysOnHandCount." days"));
	$results = $CSGDailyDataProvider->getPriorDailyDataSum($startDate,$endDate);
	foreach ($results->collection as $ID=>$csgRecord){
		if (trim($csgRecord->efiPart)){
			$efi = trim($csgRecord->efiPart);
			$qty = (int)$csgRecord->qty;
			$CSGRecordSum[$efi] = $CSGRecordSum[$efi]+$qty;
		}
	}
	foreach ($CSGRecordSum as $EFI=>$qty){
			$record->qty = floor($qty/$Config_DaysForDaysOnHandCount);
			$record->efi = $EFI;
			$results = $productProvider->updateDaysOnHand($record);
	}
	if ($results->success){
		echo " Days On Hand Counts Updated Based on Average Usage over the past ".$Config_DaysForDaysOnHandCount." days\n ";
	}else{
		echo " There was an error in the update of minimum counts\n ";
		print_pre($results);
	}
}//end if Config_UseManualDataForMinCount==manual
?>