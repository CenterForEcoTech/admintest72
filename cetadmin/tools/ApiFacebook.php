<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();
$results = new stdClass();

include_once($dbProviderFolder."SocialMediaReachProvider.php");

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
        if ($_GET['action'] == 'follower_count' && $_GET['handle']){
            $provider = new SocialMediaReachProvider($mysqli);
            $provider->setFacebookCredentials($Config_FaceBook_AppID, $Config_FaceBook_AppSecret);
            $criteria = SocialMediaHandle::createFacebookHandle($_GET['handle']);

            $response = $provider->getCounts($criteria);
            if (is_numeric($response->count)){
                $results = $response->count;
            } else {
                $results = $response->message;
            }
        }
        break;
    case "POST":
        echo "this is a POST";
        break;
    case "PUT":
        echo "this is a PUT";
        break;
}
output_json($results);
die();
?>