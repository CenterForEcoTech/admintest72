<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
require_once dirname(__FILE__) . '/../../excelExportClasses/PHPExcel/IOFactory.php';
$inputFileName = "model/eobTemplate.xlsx";

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//print_pre($passThroughsByJobID);
$path = getcwd();
$pathSeparator = "/";
$webpathSeparator = "/";
if (!strpos($path,"xampp")){$path = $path.$pathSeparator;}else{$pathSeparator = "\\";$path = $path.$pathSeparator;}

$ReportsFile = "EOBFiles".$pathSeparator."EOBFiles_".date("y_m", strtotime($invoiceDate)).".xlsx";

$pathExtension = $ReportsFile;
$webpathExtension = $ReportsFile;

$saveLocation = $path.$pathExtension;
$sheetNumber = 0;
try {
    $objPHPExcel->setActiveSheetIndex($sheetNumber);
} catch (PHPExcel_Exception $e) {
}
try {
    $templateSheet = $objPHPExcel->getActiveSheet()->copy();
} catch (PHPExcel_Exception $e) {
}

foreach ($eobData as $SiteID=>$data){
    $sheet_title = ucfirst(strtolower($eobExtractData[$SiteID]["lastName"]));
	$contractor = ucwords(strtolower($eobExtractData[$SiteID]["contractor"]));
	$contractor = ($contractor ? : "CET");
	if ($sheetNumber > 0 ){
		$thisSheet = clone $templateSheet;
        try {
            $objPHPExcel->addSheet($thisSheet);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->setActiveSheetIndex($sheetNumber);
        } catch (PHPExcel_Exception $e) {
        }
    }
    try {
        $objPHPExcel->getActiveSheet()->setTitle($sheet_title);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B5", ucwords(strtolower($eobExtractData[$SiteID]["firstName"])) . " " . ucwords(strtolower($eobExtractData[$SiteID]["lastName"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B6", ucwords(strtolower($eobExtractData[$SiteID]["address"])));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("B7", ucwords(strtolower($eobExtractData[$SiteID]["city"])) . " " . $eobExtractData[$SiteID]["state"] . " " . $eobExtractData[$SiteID]["zip"]);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D5", $SiteID);
    } catch (PHPExcel_Exception $e) {
    }

    $rowShift = 0;
	//Insert rows of data here
	$rowId = 11;
	$measureCount = count($eobExtractDetails[$SiteID]);
	if ($measureCount > 2){
		$rowShift = ($measureCount-2);
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($rowId + 1), $rowShift);
        } catch (PHPExcel_Exception $e) {
        }
    }
	foreach ($eobExtractDetails[$SiteID] as $extractDetail){
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $rowId, $extractDetail["DESCRIPTION"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $extractDetail["PROP_QTY"]);
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, ($extractDetail["CUST_PRICE"] / $extractDetail["PROP_QTY"]));
        } catch (PHPExcel_Exception $e) {
        }
        try {
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, ($extractDetail["CUST_PRICE"]));
        } catch (PHPExcel_Exception $e) {
        }
        $lastMeasureRow = $rowId;
		$rowId++;
		
	}
	//Total Row
	$rowId = (13+$rowShift);
	$installedMeasuresTotalRow = "E".$rowId;
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $rowId, "=SUM(E11:E" . $lastMeasureRow . ")");
    } catch (PHPExcel_Exception $e) {
    }

    $blankRowsToAdd = 0;
	if ($rowShift < 13){
		$blankRowsToAdd = (13-$rowShift);
        try {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(($rowId + 1), $blankRowsToAdd);
        } catch (PHPExcel_Exception $e) {
        }
        $rowShift = ($rowShift+$blankRowsToAdd);
	}
	
	$rowId = (19+$rowShift);
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $rowId, $data->final_x0020__x0024_);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $rowId, $contractor);
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 1), (($data->insulation_x0020_final_x0020_con) * (.75)));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 2), "=C" . $rowId . "+C" . ($rowId + 1));
    } catch (PHPExcel_Exception $e) {
    }

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 5), "=" . $installedMeasuresTotalRow . "-C" . ($rowId + 2));
    } catch (PHPExcel_Exception $e) {
    }
    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 6), $data->deposit_x0020_amount);
    } catch (PHPExcel_Exception $e) {
    }

    try {
        $objPHPExcel->getActiveSheet()->setCellValue("C" . ($rowId + 8), "=C" . ($rowId + 5) . "-C" . ($rowId + 6));
    } catch (PHPExcel_Exception $e) {
    }


    if ($sheetNumber > 0 ){
		unset($thisSheet);
	}
	$sheetNumber++;
}
unset($templateSheet);      

//echo $saveLocation."<Br>";

try {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
} catch (PHPExcel_Reader_Exception $e) {
}
try {
    $objWriter->save($saveLocation);
} catch (PHPExcel_Writer_Exception $e) {
}
//echo $webtrackingLink."<Br>";
$ReportsFileLink = "<a href='".$webpathExtension."'>".$ReportsFile."</a><br>";
?>