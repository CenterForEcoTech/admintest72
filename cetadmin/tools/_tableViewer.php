<?php
error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."UtilitiesProvider.php");
$utilitiesProvider = new UtilitiesProvider($dataConn);

$tables = $utilitiesProvider->getTables();
$tableCollection = $tables->collection;

$selectedTable = $_GET['TableName'];
$excludedList = array();
$includeBlankDefault = true;
$classList="";

$tableSelect = $utilitiesProvider->createSelectPulldown($tableCollection,"TableName",$selectedTable,$excludedList,$includeBlankDefault, $classList);

if ($selectedTable){
	$tableInfo = $utilitiesProvider->getTableData($selectedTable);
	$tableCollection = $tableInfo->collection;
	//print_pre($tableCollection);
	foreach ($tableCollection[0] as $varName=>$varValue){
		if (strpos($varName,"_")){
			$columnName = explode("_",$varName);
		}else{
			$columnName[1]=$varName;
		}
		$TableColumns[] = $columnName[1];
	}
}

?>
<h3>Table Viewer</h3>
<?php echo $tableSelect;?>
<script>
$(document).ready(function() {
	$("#TableName").on('change',function(){
		window.location.href = "<?php echo $CurrentServer.$adminFolder;?>tools/?nav=table_viewer&TableName="+$(this).val();
	});
	
<?php if ($selectedTable){?>
	var table = $('#tableInfo').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
		},				
		"scrollX": true,
		"bJQueryUI": true,
		"bSearchable":true,
		"bAutoWidth": true,
		"bSort": true,
		"iDisplayLength": 50,
	});

<?php }?>	
	
});
</script>
<hr>
<?php if ($selectedTable){?>

	<table id="tableInfo" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<?php
					foreach ($TableColumns as $hdr){
						echo "<th>".$hdr."</th>";
					}
				?>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<?php
					foreach ($TableColumns as $hdr){
						echo "<th>".$hdr."</th>";
					}
				?>
			</tr>
		</tfoot>
		<tbody>
			<?php
				foreach ($tableCollection as $column=>$data){
					echo "<tr>";
					foreach ($data as $rowid=>$rowinfo){
						echo "<td>".$rowinfo."</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
<?php }?>
