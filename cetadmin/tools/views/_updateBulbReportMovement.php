<?php	
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);

$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$BatchSize = 5;
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

//now mark it as installed item if efi ism
$efiPart = $transferCollection->EFI;
$siteId = $transferCollection->SiteID;
$actionMessage = "";

if ($siteId && trim($efiPart)){
	$installDate = $transferCollection->TransferDate;
	$transferInfo = array();
	$TransferResults = null;
	$resultsCollection = null;
	$transferInfo["WarehouseFromLocation"] = $transferCollection->WarehouseFromLocation;
	$transferInfo["WarehouseToLocation"] = "Installed";
	$transferInfo["PurchaseOrder"] = $transferCollection->PurchaseOrder;
	$transferInfo["TransferDate"] = $installDate;
	$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
	$transferInfo["EFI"] = $efiPart;
	$resultsCollection["qty"][] = $transferCollection->Qty;
	$resultsCollection["qty"][] = (int)$transferCollection->Qty;
	$transferInfo["Qty"] = (int)$transferCollection->Qty;
	$resultsCollection["qty"][] = $transferInfo["Qty"];
	$transferInfo["SiteID"] = $siteId;
	$resultsCollection["updateCountInfo"][] = $transferInfo;
	$TransferResults = $productProvider->transferItem($transferInfo, getAdminId());
	$resultsCollection["qty"][] = $TransferResults[0]["units"];
	$actionMessage = "Transfer complete now updating Warehouse Count";
	
	//Now update the counts
	$updateResults = $warehouseProvider->updateSingleWarehouseCount($TransferResults);
	$resultsCollection["MarkedInstalled"][] = $updateResults;
	$actionMessage .= "\nUpdate of Warehouse Count complete";
}else{
	$resultsCollection["AddedButNotInstalled"][] = $result;
	$actionMessage = "Not an ISM to Adjust Count";
}
$resultsCollection["Message"][] = $actionMessage;
?>