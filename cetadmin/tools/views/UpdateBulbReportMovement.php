<?php	
ini_set('max_execution_time', 2400); //40 minutes
header("Content-Type: text/event-stream\n\n");

include_once("../../_config.php");
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);
$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$BatchSize = 5;
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

$criteria = new stdClass();
//startDate and endDate set on APIDataTools
$criteria->startDate = $_GET["startDate"];
$criteria->endDate = $_GET["endDate"];
$dailyResults = $CSGDailyDataProvider->getDailyData($criteria);

$siteCount = 1;
foreach ($dailyResults as $result){
	if ($siteCount < 50){
		echo "data: Starting to Updating Record #".$siteCount."\n\n";
		//now mark it as installed item if efi ism
		$efiPart = $EFIByPartID[trim(strtoupper($result->partId))];
		
		if ($result->siteId && trim($efiPart)){
			$installDate = explode(" ",$result->installed_dt);
			$transferInfo = array();
			$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
			$transferInfo["WarehouseToLocation"] = "Installed";
			$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
			$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
			$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
			$transferInfo["EFI"] = $efiPart;
			$transferInfo["Qty"] = $result->qty;
			$resultCollection["updateCountInfo"][] = $transferInfo;
			echo "data: Starting to Transfer Item\n\n";
			$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
			echo "data: Transfer complete for ".$result->siteId." EFI ".$efiPart." now updating Warehouse Count\n";
			
			//Now update the counts
			$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
			$resultsCollection["MarkedInstalled"][] = $updateResults;
			echo "data: Update of Warehouse Count complete\n";
		}else{
			$resultsCollection["AddedButNotInstalled"][] = $result;
		}
	}else{
		break;
	}
	$siteCount++;
}
echo "event: completed\n";
die();
?>