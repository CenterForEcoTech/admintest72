<script type="text/javascript">
		// THESE TWO VARIABLES STORE THE TIME AND DATE WHEN THE PAGE IS LOADED
		var startDate = new Date();
		var startTime = startDate.getTime();

		// THIS FUNCTION CALCULATES THE SECONDS ELAPSED SINCE THE PAGE WAS LOADED
		function seconds_elapsed () { 
			var date_now = new Date (); 
			var time_now = date_now.getTime (); 
			var time_diff = time_now - startTime; 
			var seconds_elapsed = Math.floor ( time_diff / 1000 ); 
			return ( seconds_elapsed ); 
		} 

		// THIS FUNCTION TAKES THE SECONDS ELAPSED AND CONVERTS THEM FOR OUTPUT
		function time_spent () { 
			// TAKE THE SECONDS ELAPSED
			var secs = seconds_elapsed ();

			// CONVERT SECONDS TO MINUTES AND SECONDS
			var mins = Math.floor ( secs / 60 );
			secs -= mins * 60;

			// CONVERT MINUTES TO HOURS AND MINUTES
			var hour = Math.floor ( mins / 60 );
			mins -= hour * 60;

			// DISPLAY THE FINAL OUTPUT TIME STRING
			var outputTime = pad ( hour ) + ":" + pad ( mins ) + ":" + pad ( secs );
			$(".timeElapsed").text(outputTime);

			var doneWithLongProcess = $('#inventoryCountResults').text();
			
			if (doneWithLongProcess != 'Done'){
				// RECURSIVELY RE-RUN THE FUNCTION EVERY SECOND
				setTimeout( "time_spent ()", 1000 ); 
			}
		}

		// THIS FUNCTION INSERTS A LEADING ZERO (IF NECESSARY) TO PROVIDE UNIFORM OUTPUT
		function pad ( num ){
			return ( ( num > 9 ) ? num : "0" + num );
		}		
		time_spent();
</script>
<div class="timeElapsed">Elapsed Time</div>
<?php
error_reporting(1);
ini_set('max_execution_time', 2400); //40 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);

$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$BatchSize = 5;
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

//Get list of files to review
$folderLocation = $siteRoot.$adminFolder.$Config_FTP_CSGSaveMonthlyFileLocation;

include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($folderLocation, false, false);
foreach ($files as $file){
	//only look at csv files
	if (strpos($file,"csv")){
		$fileLocation = str_replace("\\","/",$file);
		echo $fileLocation.":";
		//FIRST CHECK TO SEE IF FILE WAS ALREADY ADDED THEN RUN THE SCRIPTS BELOW
		$results = $CSGDailyDataProvider->searchMonthlyDataFileHistory($fileLocation);
		$successCount = 0;
		if ($results->totalRecords < 1){	
			echo " Proceeding to process<br>";
			$csv_file    = file_get_contents($fileLocation);
			$rows        = explode("\n", $csv_file);
			//put the first row as column headers
			foreach ($rows as $data=>$val){
				$content = explode(";",$val);
				if (count($content)<2){$content = explode(",",$val);}
				if ($data < 1){
					$CSGData["Columns"] = $content;
				}else{
					$CSGData["Rows"][] = $content;
				}
			}
			//convert column names to standardized format
			foreach ($CSGData["Columns"] as $Id=>$Column){
				$ConvertColumnName = $CSGDailyRecord::convertColumnName($Column);
				$CSGData["Columns"][$Id]=trim($ConvertColumnName);
			}
			//update row information with column name so that they can be matched to CSGDailyDataRecord
			foreach ($CSGData["Rows"] as $ID=>$value){
				foreach ($value as $colID=>$rowVal){
					$cleanRowVal = str_replace('"','',$rowVal);
					if ($CSGData["Columns"][$colID] == "crew"){
						$cleanRowVal = strtoupper($cleanRowVal);
					}
					$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
				}
			}
			echo "Columns and Data Parsed.<br>";

			/*
			echo "Beginning to Searching for matched records<br>\r";
			$results = $CSGDailyDataProvider->prepareMonthlyDataMulitpleRows($CSGDataArray,$fileLocation);
			//print_pre($results);
			echo count($results["misMatch"])." MisMatched records that have wrong CREW to Auditor Name<br>\r";
			echo count($results["notFound"])." Not Found records that have were not included in the Daily Data<br>\r";
			echo "Of those Not Found, ".count($results["notFoundISM"])." records had ISM installs and ".count($results["notFoundVisit"])." were just visits<br>\r";
			$recordOriginalCounts = $CSGDailyDataProvider->monthlyDataCounts($fileLocation,count($results["misMatch"]),count($results["notFoundISM"]),count($results["notFoundVisit"]));
			$resultsCount["notFound"] = $results["notFound"];
			//update the mismatched records
			foreach ($results["misMatch"] as $result){
				$transferInfo = array();
				echo "update: ".$result->auditor_name."=".$WarehouseByDescription[$result->auditor_name]." where ".$result->id."<br>";
				if ($WarehouseByDescription[$result->auditor_name]){
					$resultsCollection["updates"][] = $CSGDailyDataProvider->updateCSGData($WarehouseByDescription[$result->auditor_name],$result->id);
				}
				//Now transfer the installed units to the correct Warehouse
				//if misMatch didn't have an EFI then it wouldn't have been 'installed' so process like a normal install otherwise do a transfer
				if (!$result->efiPart){
					$installDate = explode(" ",$result->installed_dt);
					$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
					$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
					$transferInfo["WarehouseToLocation"] = "Installed";
					$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$efiPart = $EFIByPartID[trim(strtoupper($result->partId))];
					$transferInfo["Total_".$efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;

					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["NoEfis"][] = $updateResults;

				}else{
					//first move the item back from install them take it out of the auditor
					$installDate = explode(" ",$result->installed_dt);
					$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
					$transferInfo["WarehouseFromLocation"] = "Installed";
					$transferInfo["WarehouseToLocation"] = strtoupper($result->crew);
					$transferInfo["MovementType"] = "Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$transferInfo["Total_".$result->efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;
					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["PutBack"][] = $updateResults;

					//now mark it as installed item
					$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
					$transferInfo["WarehouseToLocation"] = "Installed";
					$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$transferInfo["Total_".$result->efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;
					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["MarkedInstalled"][] = $updateResults;
				}
//				echo "update crew from ".$result->crew." to ".$WarehouseByDescription[$result->auditor_name]." WHERE id=".$result->id."<bR>";
			}//end foreach mismatch
			echo "Done adjusting mismatched<Br>";
			if (count($resultsCollection["updates"])){
				echo count($resultsCollection["updates"])." CSG records updated with corrected CREW<br>";
			}
			if (count($resultsCollection["NoEfis"])){
				echo count($resultsCollection["NoEfis"])." records with No EFI's updated<br>";
			}
			if (count($resultsCollection["PutBack"])){
				echo count($resultsCollection["PutBack"])." records adjusted warehouse location on installed from incorrect crew location based on matched Auditor Name<br>";
			}
			*/
			
			echo "Convert fresh records to objects, delete old daily records, and insert monthly records<br>\r";
			$results = $CSGDailyDataProvider->prepareMonthlyDataRows($CSGDataArray,$fileLocation);
			echo "Conversion and clean up done.<br>";
			echo "Attempting to add ".count($results["notFound"])." records.<br>";
			echo "<div style='height:150px;overflow-y:scroll'>";
			//now add all the No found installed in batches of 100
			$startCount = 0;
			$echo_time = time();
			$interval = 10;
			$resultsCount["notFound"] = $results["notFound"];
				foreach ($results["notFound"] as $result){
//					echo "startCount='".$startCount."' and BatchSize='".$BatchSize."'<br>";
//					if ($startCount == $BatchSize) break;
					
						//now mark it as installed item if efi ism
						$efiPart = $EFIByPartID[trim(strtoupper($result->partId))];
//						echo "efiPart=".$efiPart."<br>";
//						echo "siteId=".$result->siteId."<br>";
						
						if ($result->siteId && trim($efiPart)){
//							echo "with SiteId and efiPart we can initiate a transfer<br>";
							$installDate = explode(" ",$result->installed_dt);
							$transferInfo = array();
							$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
							$transferInfo["WarehouseToLocation"] = "Installed";
							$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
							$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
							$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
							$transferInfo["Total_".$efiPart]=(int)$result->qty;
							$transferInfo["SiteID"] = $result->siteId;
							$transferInfo["EFI"] = $efiPart;
							$transferInfo["Qty"] = (int)$result->qty;
							$transferInfo["ID1"] = $result->id1;
							$TransferCollection[] = $transferInfo;
						}else{
							$resultsCollection["AddedButNotInstalled"][] = $result;
						}
						
						//echo "result:";
						//print_pre($result);
						//Now add this record into the csg_dailydata
//						echo "Adding this record into csg_dailydata<Br>";
						$insertResults = $CSGDailyDataProvider->insertMonthlyData($result,$fileLocation);
						//echo "insertResults:";
						//print_pre($insertResults);
						foreach ($insertResults["insertResults"] as $insertResult){
							$successCount = $successCount+$insertResult->success;
						}
						if ($echo_time + $interval == time()){
							echo "Successfully added ".$successCount." records<br>";
							$echo_time = time(); // set up timestamp for next interval
							ob_flush();
							flush();   
						}

					$startCount++;
				}
			echo "</div>";

				//print_pre($resultsCollection);
/*			 BATCH Processing....showed no improvement in performance
			//process all the transfers at once
			//echo "<hr>TransferCollection:";
			//print_pre($TransferCollection);
			$TransferResults = $productProvider->transferMultiple($TransferCollection, getAdminId());
			
			//refactor the TransferResults to have the correct ID;
			if ($TransferResults->success){
				$t=0;
				foreach ($TransferResults->records as $transferRecord){
					$newTransfers = new stdClass();
					$transferRecord->id = $TransferResults->insertedId+$t;
					$newTransfers = $transferRecord;
					$Transfers[]=$newTransfers;
					$t++;
				}
			}
			//print_pre($Transfers);
			$TransfersArray = json_decode(json_encode($Transfers), true);
			//print_pre($TransfersArray);
			$updateResults = $warehouseProvider->updateWarehouseCount($TransfersArray);
			//print_pre($updateResults);
			$resultsCollection["MarkedInstalled"][] = $updateResults;
*/
			$processedMonth = $results["processedMonth"];
		}else{
			echo " Already Processed<br>";
			$processedMonth = $results->monthProcessed;
			$resultsCount["notFound"] = 0;
		}//end if not already added
		
	}//end if csv
}
if ($TransferCollection > 1){
	echo "<br>".$successCount." rows added to CSG Data<br>\n";
	echo count($TransferCollection)." Inventory records to be transferred for the month ".$processedMonth."<br>\n";
	echo count($resultsCollection["AddedButNotInstalled"])." Visits records added<br>\n";
?>
		<div class="row">
			<div class="five columns">
				<button id="inventoryCountUpdate">Update Inventory Count</button>
			</div>
			<div class="seven columns">
				<div class="timeElapsed">Elapsed Time</div>
			</div>
		</div>
		<div id="inventoryQueueResults"></div>
		<div id="inventoryCountResults"></div>

		<table id="inventoryCountTable">
			<thead>
				<tr><th>SiteId</th><th>Install Date</th><th>Crew</th><th>EFI</th><th>Units</th><th>Remaining Units</th></tr>
			</thead>
			<tbody>
			
			</tbody>
		</table>
		<script type="text/javascript">
		$.fn.LongProcess = function () {
			var _this = this;
			this.notifications = [];
			this.actions = [];

			this.add = function (_notification, _action) {
				this.notifications.push(_notification);
				this.actions.push(_action);
			};
			this.run = function () {

				if (!_this.actions && !_this.notifications) {
					return "Empty";
				}
				//******************************************************************
				//This section makes the actions lag one step behind the notifications.
				var notification = null;
				if (_this.notifications.length > 0) notification = _this.notifications.shift();

				var action = null;
				if ((_this.actions.length >= _this.notifications.length + 2) || (_this.actions.length > 0 && _this.notifications.length == 0)) 
					action = _this.actions.shift();
				//****************************************************************
				if (!action && !notification) {
					return "Completed";
				}

				if (action) action();        
				if (notification) notification();

				setTimeout(_this.run, 100); 
				//setTimeout(_this.run,1); //set to 1 after you've entered your actual long running process. The 1000 is there to just show the delay.
			}
			return this;
		};		
		
		
		$(function(){
			var inventoryCountResults = $('#inventoryCountResults');
			var inventoryQueueResults = $('#inventoryQueueResults');
			var inventoryCountTable = $("#inventoryCountTable");
			$("#inventoryCountUpdate").on('click',function(e){
				e.preventDefault();
				inventoryCountResults.show().html("Processing...").removeClass("info").removeClass("alert").addClass("alert");
				var transferCollection = <?php echo json_encode($TransferCollection);?>;
					//console.log(transferCollection);
				inventoryQueueResults.removeClass("info").removeClass("alert").addClass("alert");
				inventoryCountResults.removeClass("info").removeClass("alert").addClass("alert").addClass("info");
				var process = $().LongProcess(),
					totalCount = transferCollection.length;
				$.each(transferCollection,function(key,obj){
					var percentage = Math.round(Math.round((key/totalCount)*10000)/100);
					var displayKey = (key+1);
					process.add(function () {
							inventoryQueueResults.text("Processing item "+displayKey+" into Queue "+percentage+"% complete");
						}, function () {
						var items = {};
							items["action"] = 'inventoryCountUpdate',
							items["TransferCollection"] = obj,
							data = JSON.stringify(items);
							$.ajax({
								url: "ApiDataTools.php",
								type: "POST",
								data: data,
								success: function(data){
									if (data.error == '' || !data.error){
//										console.log(key);
//										console.log(data);
										inventoryCountResults.text("Item "+displayKey+" Processed: "+data.MarkedInstalled[0]["summary"]).removeClass("info").removeClass("alert").addClass("alert").addClass("info");
//										$('#inventoryCountTable tr:last').after("<tr><td>"+data.MarkedInstalled[0]["siteId"]+"</td><td>"+data.MarkedInstalled[0]["installDate"]+"</td><td>"+data.MarkedInstalled[0]["crewId"]+"</td><td>"+data.MarkedInstalled[0]["efi"]+"</td><td>"+data.MarkedInstalled[0]["units"]+"</td><td>"+data.MarkedInstalled[0]["remainingUnits"]+"</td></tr>");
										InventoryUpdateResults.rows.add( [[
											data.MarkedInstalled[0]["siteId"],
											data.MarkedInstalled[0]["installDate"],
											data.MarkedInstalled[0]["crewId"],
											data.MarkedInstalled[0]["efi"],
											data.MarkedInstalled[0]["units"],
											data.MarkedInstalled[0]["remainingUnits"]
										]] ).draw();
										if (displayKey == totalCount){
											inventoryCountResults.text("Done");
										}
								}else{
										inventoryCountResults.html(data.error).removeClass("info").removeClass("alert").addClass("alert");
									}
								},
								error: function(jqXHR, textStatus, errorThrown){
									var message = $.parseJSON(jqXHR.responseText);
									inventoryCountResults.html(message).removeClass("info").removeClass("alert").addClass("alert");
								}
							});
					});					
				});
				process.add(function () {
						inventoryQueueResults.text("All Items Queued");
					}, function () {
						//inventoryCountResults.text("Done");
				});
				process.run();
			});

			var InventoryUpdateResults = inventoryCountTable.DataTable({
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
				},				
				"scrollX": true,
				"bJQueryUI": true,
				"bSearchable":false,
				"bFilter":false,
				"bAutoWidth": true,
				"bSort": false,
				"iDisplayLength": 10
			});

		});
		
		</script>
<?php
}else{	
	//if no results, then add the file		
	if (count($resultsCount["notFound"]) < 1){$resultCollection["fileHistory"] = $CSGDailyDataProvider->insertMonthlyDataFileHistory($fileLocation);}		
	if ($resultCollection["fileHistory"]->success){
		echo "All Rows from ".$fileLocation." have been imported<br>";
	}else{
		echo "No New Bulb Reports found.  Use the File Library to upload a new bulb report";
	}
}
?>
1)Export Transfer history and paste into csv file.<br>
2)Clean Transfer History records and make sure trailing zeros are intack for 3010.020 and 3010.100 and Name/CET match<br>
2)Set Date to YYYY-MM-DD format and order by date.<br>
3)Remove last blank row via notepad++ edit.<br>
4)Copy file into FTPFiles/MovementHistory folder and run Transfer History update report<br>
5)Process April Bulb report to remove John DuPerry-CET to John DuPerry and set date fields to YYYY-MM-DD format and reorder by Install_dt<br>
6)Also check that one AuditorName to One CrewID and fix any inconsistencies before importing<br>