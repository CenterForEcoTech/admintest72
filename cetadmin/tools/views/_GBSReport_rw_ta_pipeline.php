<?php
error_reporting(1);
ini_set('max_execution_time', 1200); //20 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);

$BatchSize = 10;

//Get list of files to review
$folderLocationToGet = $siteRoot.$adminFolder.$Config_FTP_CSGSaveScheduleFileLocation;
$folderLocationToSave = $siteRoot.$adminFolder.$Config_FTP_CSGSaveScheduleFileToLocation;

include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($folderLocationToGet, false, true);
if (count($files)){
	foreach ($files as $file){
		//only look at csv files
		if (strpos($file,"csv")){
			$fileLocation = str_replace("\\","/",$file);
			$csv_file    = file_get_contents($fileLocation);
			$rows        = explode("\n", $csv_file);
			//put the first row as column headers
			foreach ($rows as $data=>$val){
				$val = preg_replace_callback('/\"(.+?)\"/i', function($match) {
					return str_replace(',', ';', $match[0]);
				}, $val);
				//echo $result;
				$content = explode(",",$val);
	//			echo $data.":".$val."<br>";
				if ($data ==1){
					$CSGData["Columns"] = $content;
				}else{
					//ignore blank rows
					if ($content[0]){
						$CSGData["Rows"][] = $content;
					}
				}
			}
			//convert column names to standardized format
			foreach ($CSGData["Columns"] as $Id=>$Column){
				$ConvertColumnName = $Column;
				$CSGData["Columns"][$Id]=trim($ConvertColumnName);
			}

			$ThisYear = date("Y");
			$NextYear = date("Y",strtotime($TodaysDate.'+1 year'));
			//update row information with column name so that they can be matched to CSGDailyDataRecord
			foreach ($CSGData["Rows"] as $ID=>$value){
				foreach ($value as $colID=>$rowVal){
					$cleanRowVal = str_replace(';',',',$rowVal);
					$cleanRowVal = str_replace(array("\n", "\t", "\r","\""),'',$cleanRowVal);
					//check if date
					if (substr($cleanRowVal,-4)==$ThisYear || substr($cleanRowVal,-4)==$NextYear){$cleanRowVal = MySQLDateUpdate($cleanRowVal);}
					if ($CSGData["Columns"][$colID] == "crew"){
						$cleanRowVal = strtoupper($cleanRowVal);
					}
					$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
					$CSV[$ID][]=$cleanRowVal;
				}
			}
			
			$fileToSave = $folderLocationToSave.'/report.csv';
			$f = @fopen($fileToSave, "r+");
			if ($f !== false) {
				ftruncate($f, 0);
				fclose($f);
			}
			//print_pre($CSV);
			convert_to_csv($CSV, $fileToSave, ',');
			$results = $CSGDataProvider->loadExternalReportData($fileToSave);
			//print_pre($results);
			if ($results["success"]){
				//remove the file so there should only be one schedulereport file in the directory
				unlink($fileLocation);
				echo count($CSV)." records successfully added<br>";
				
				//now create analysis of dates
				//print_pre($CSGDataArray);
				foreach ($CSGDataArray as $ID=>$ColumnData){
					$YearMonth = date("Y-m-01",strtotime($ColumnData["Appt Date"]));
					$ApptType  = $ColumnData["Appt Type"];
					$CrewID = strtoupper($ColumnData["Crew ID"]);
					if (trim($ApptType)==''){
						//CET_58=Jeanne Comeau CET_113=Rory Donohoe CET_109 Mckeon, Brendan
						if ($CrewID == 'CET_58' || $CrewID == 'CET_113' || $CrewID == 'CET_109'){
							$ApptType = "Inspection (open slots)";
						}else{
							$ApptType = "HEA (open slots)";
						}
					}
					$ApptTypes[$YearMonth][$ApptType] = $ApptTypes[$YearMonth][$ApptType]+1;
					if (strpos(" ".$ApptType,"Blocked")){
						$ApptTypes[$YearMonth]["BLOCKED"] = $ApptTypes[$YearMonth]["BLOCKED"]+1;
					}
				}
				foreach ($ApptTypes as $YearMonth=>$data){
					ksort($data);
					$ApptTypesSorted[$YearMonth] = $data;
				}
				foreach ($ApptTypesSorted as $YearMonth=>$data){
					foreach ($data as $ApptType=>$value){
						$results = $CSGDataProvider->insertScheduleReportAnalysis($ApptType,$value,$YearMonth,$TodaysDate);
					}
				}
				
			}
			
		}//end if csv
	}
}else{	
	echo "No Schedule Reports found.  Use the File Library to upload a new schedule report";
	echo "<br><br>Redirecting to <a href=\"".$CurrentServer.$adminFolder."forecast/?nav=forecast-apptanalysis\">Full Monthly Report</a> in 3 seconds";
	$DontRunDeDup = true;
}
?>
<script>
	$(document).ready(function() {
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});
		
	});
</script>

<?php if (count($ApptTypesSorted)){?>
	<h3>Appt Type Analysis (<a href="<?php echo $CurrentServer.$adminFolder;?>forecast/?nav=forecast-apptanalysis" style="font-size:10pt;"> see full monthly report</a>)</h3>
	<?php foreach ($ApptTypesSorted as $YearMonth=>$data){?>
			<h2><?php echo date("F Y",strtotime($YearMonth));?></h2>
			<table class="reportResults display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Appt Type</th>
						<th>As Of <?php echo $TodaysDate;?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($data as $apptType=>$value){
							echo "<tr>
									<td>".$apptType."</td>
									<td>".$value."</td>
								 </tr>";
						}
					?>
				</tbody>
			</table>
	<?php } // foreach ApptTypeSorted 
}// end if count ?>