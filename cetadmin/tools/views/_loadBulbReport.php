<?php
error_reporting(1);
ini_set('max_execution_time', 1200); //20 minutes
$time_start = microtime(true); 
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);

$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$BatchSize = 10;
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

//Get list of files to review
$folderLocation = $siteRoot.$adminFolder.$Config_FTP_CSGSaveMonthlyFileLocation;

include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($folderLocation, false, true);
foreach ($files as $file){
	//only look at csv files
	if (strpos($file,"csv")){
		$fileLocation = str_replace("\\","/",$file);
		//FIRST CHECK TO SEE IF FILE WAS ALREADY ADDED THEN RUN THE SCRIPTS BELOW
		$results = $CSGDailyDataProvider->searchMonthlyDataFileHistory($fileLocation);
		$successCount = 0;
		if (!$results->totalRecords){
			echo $fileLocation."<br>";
			$csv_file    = file_get_contents($fileLocation);
			$rows        = explode("\n", $csv_file);
			//put the first row as column headers
			foreach ($rows as $data=>$val){
				$content = explode(";",$val);
				if (count($content)<2){$content = explode(",",$val);}
				if ($data < 1){
					$CSGData["Columns"] = $content;
				}else{
					$CSGData["Rows"][] = $content;
				}
			}
			//convert column names to standardized format
			foreach ($CSGData["Columns"] as $Id=>$Column){
				$ConvertColumnName = $CSGDailyRecord::convertColumnName($Column);
				$CSGData["Columns"][$Id]=trim($ConvertColumnName);
			}
			//update row information with column name so that they can be matched to CSGDailyDataRecord
			foreach ($CSGData["Rows"] as $ID=>$value){
				foreach ($value as $colID=>$rowVal){
					$cleanRowVal = str_replace('"','',$rowVal);
					if ($CSGData["Columns"][$colID] == "crew"){
						$cleanRowVal = strtoupper($cleanRowVal);
					}
					$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
				}
			}
			$checkingFile .=  "Columns and Data Parsed.  Beginning to Searching for matched records<br>\r";
			//print_pre($CSGDataArray);
			$results = $CSGDailyDataProvider->prepareMonthlyDataMulitpleRows($CSGDataArray,$fileLocation);
			
			$checkingFile .= count($results["misMatch"])." MisMatched records that have wrong CREW to Auditor Name<br>\r";
			$checkingFile .= count($results["notFound"])." Not Found records that have were not included in the Daily Data<br>\r";
			$checkingFile .= "Of those Not Found, ".count($results["notFoundISM"])." records had ISM installs and ".count($results["notFoundVisit"])." were just visits<br>\r";
			$recordOriginalCounts = $CSGDailyDataProvider->monthlyDataCounts($fileLocation,count($results["misMatch"]),count($results["notFoundISM"]),count($results["notFoundVisit"]));
			$resultsCount["notFound"] = $results["notFound"];
//			print_pre($results);

			//update the mismatched records
			foreach ($results["misMatch"] as $result){
				$transferInfo = array();
				$resultsCollection["updates"][] = $CSGDailyDataProvider->updateCSGData($WarehouseByDescription[$result->auditor_name],$result->id);
				//Now transfer the installed units to the correct Warehouse
				//if misMatch didn't have an EFI then it wouldn't have been 'installed' so process like a normal install otherwise do a transfer
				if (!$result->efiPart){
					$installDate = explode(" ",$result->installed_dt);
					$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
					$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
					$transferInfo["WarehouseToLocation"] = "Installed";
					$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$efiPart = $EFIByPartID[trim(strtoupper($result->partId))];
					$transferInfo["Total_".$efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;

					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["NoEfis"][] = $updateResults;

				}else{
					//first move the item back from install them take it out of the auditor
					$installDate = explode(" ",$result->installed_dt);
					$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
					$transferInfo["WarehouseFromLocation"] = "Installed";
					$transferInfo["WarehouseToLocation"] = strtoupper($result->crew);
					$transferInfo["MovementType"] = "Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$transferInfo["Total_".$result->efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;
					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["PutBack"][] = $updateResults;

					//now mark it as installed item
					$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
					$transferInfo["WarehouseToLocation"] = "Installed";
					$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
					$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
					$transferInfo["Total_".$result->efiPart]=$result->qty;
					$TransferCollection[] = $transferInfo;
					$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
					
					//Now update the counts
					$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
					$resultsCollection["MarkedInstalled"][] = $updateResults;
				}
//				echo "update crew from ".$result->crew." to ".$WarehouseByDescription[$result->auditor_name]." WHERE id=".$result->id."<bR>";
			}//end foreach mismatch
			if (count($resultsCollection["updates"])){
				echo count($resultsCollection["updates"])." CSG records updated with corrected CREW";
			}
			if (count($resultsCollection["NoEfis"])){
				echo count($resultsCollection["NoEfis"])." records with No EFI's updated<br>";
			}
			if (count($resultsCollection["PutBack"])){
				echo count($resultsCollection["PutBack"])." records adjusted warehouse location on installed from incorrect crew location based on matched Auditor Name<br>";
			}

			//now add all the No found installed in batches of 100
			$startCount = 0;
				foreach ($results["notFound"] as $result){
					//echo "startCount='".$startCount."' and BatchSize='".$BatchSize."'<br>";
					if ($startCount == $BatchSize) break;
					
						//now mark it as installed item if efi ism
						$efiPart = $EFIByPartID[trim(strtoupper($result->partId))];
						//echo "efiPart=".$efiPart."<br>";
						//echo "siteId=".$result->siteId."<br>";
						if ($result->siteId && trim($efiPart)){
							$installDate = explode(" ",$result->installed_dt);
							$transferInfo = array();
							$transferInfo["WarehouseFromLocation"] = strtoupper($WarehouseByDescription[$result->auditor_name]);
							$transferInfo["WarehouseToLocation"] = "Installed";
							$transferInfo["PurchaseOrder"] = "SiteID: ".$result->siteId.",ProjectID: ".$result->projectId.",Measure: ".$result->meastable;
							$transferInfo["TransferDate"] = MySQLDate($installDate[0]);
							$transferInfo["MovementType"] = "Installed - Bulb Report Correction";
							$transferInfo["Total_".$efiPart]=$result->qty;
							$TransferCollection[] = $transferInfo;
							$resultCollection["updateCountInfo"][] = $transferInfo;
							$TransferResults = $productProvider->transfer($transferInfo, getAdminId());
							
							//Now update the counts
							$updateResults = $warehouseProvider->updateWarehouseCount($TransferResults);
							$resultsCollection["MarkedInstalled"][] = $updateResults;
							
						}else{
							$resultsCollection["AddedButNotInstalled"][] = $result;
						}
						//echo "result:";
						//print_pre($result);
						
						//Now add this record into the csg_dailydata
						$insertResults = $CSGDailyDataProvider->insertMonthlyData($result,$fileLocation);
						
						//echo "insertResults:";
						//print_pre($insertResults);
						foreach ($insertResults["insertResults"] as $insertResult){
							$successCount = $successCount+$insertResult->success;
						}
					$startCount++;
				}
				//print_pre($resultsCollection);
		}else{
			$resultsCount["notFound"] = 0;
		}//end if not already added
	
		
		
		
	}//end if csv
}
			echo $checkingFile;


			$time_end = microtime(true);
			$execution_time = floor(($time_end - $time_start)*100)/100;
			//execution time of the script
			echo '<b>Total Execution Time:</b> '.$execution_time.' Sec<br><Br>';
?>
