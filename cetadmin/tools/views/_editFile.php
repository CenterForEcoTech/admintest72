<?php

if ($fileManager){
    ?>
    <h3>Edit File</h3>
    <form id="fileupload" action="ApiFileLibrary.php" method="POST" enctype="multipart/form-data" class="sixteen columns normal template-upload">
        <?php if ($fileRecord->id){?>
        <button class="btn btn-danger delete do-not-navigate" >
            <span>Delete</span>
        </button>
        <input type="hidden" name="id[]" value="<?php echo $fileRecord->id?>">
        <div class="preview two columns">
            <span><a href="<?php echo $fileRecord->folderUrl.$fileRecord->fileName;?>" target="file-preview" title="Click to view original file"><img src="<?php echo $CurrentServer;?>images/file_<?php echo (strpos($fileRecord->fileName,"csv") ? "csv" : "pdf") ;?>.png"></a></span>
        </div>
        <?php } ?>
        <div class="eleven columns">
            <div class="metadata">
                <label class="name"><strong>Name: </strong><span><?php echo $fileRecord->fileName;?></span></label>
                <label class="size"><strong>Size: </strong><span><?php echo $fileRecord->fileSize;?><span></label>
                <label class="type"><strong>Type: </strong><span><?php echo $fileRecord->fileType;?><span></label>
            </div>
            <div class="btn btn-success fileinput-button">
                <span>Select file...</span>
                <input type="file" name="files[]" multiple>
				<?php echo ($fileRecord->id == 50 || $fileRecord->id == 15 ? "[must be saved as file type CSV (MS-DOS) (*.csv)]" : "");?>
            </div>
        </div>
        <br>
        <div class="sixteen columns">
            <label>
                <div>*Title:</div>
                <input type="text" id="title" name="title[]" required value="<?php echo $fileRecord->title;?>">
            </label>

            <label>
                <div>Keywords:</div>
                <input type="text" name="keywords[]" value="<?php echo $fileRecord->keywords?>">
            </label>
			<label>
				<div>File Storage Destination:</div>
				<?php $folderlocations = array($adminFolder.$Config_FTP_CSGSaveMonthlyFileLocation,$adminFolder.$Config_FTP_CSGSaveScheduleFileLocation,$adminFolder.$Config_FTP_CSGSaveInspectionFileLocation,"media/documents","media/images");?>
				<select name="uploadFileLocation">
					<?php
						foreach($folderlocations as $Folder){
							$FolderName = $CurrentServer.$Folder."/";
							echo "<option value=\"".$Folder."\"".($fileRecord->folderUrl==$FolderName ? " selected" : "").">".$Folder."</option>";

						}
					?>
				</select>
			</label>
            <button type="submit" class="btn btn-primary start">
                <span>Save</span>
            </button>
            <button class="btn btn-warning cancel do-not-navigate">
                <span>Cancel</span>
            </button>

            <h4>Notes</h4>
<?php
// a little "formatting"
//@see http://php.net/manual/en/function.str-replace.php#example-5440
$newlines   = array("\r\n", "\n", "\r");
$replace = '<br />';

$htmlDescription = str_replace($newlines, $replace, $fileRecord->description);
 ?>
            <div><?= $htmlDescription ?></div>
            <hr></hr>
            <label>
                <div>Edit Notes:</div>
                <textarea name="description[]"><?php echo $fileRecord->description;?></textarea>
            </label>
            <button type="submit" class="btn btn-primary start">
                <span>Save</span>
            </button>

      </div>

    </form>
    <div id="dialog-confirm-delete" title="Delete this file?" style="display:none;">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this file?</p>
    </div>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    </script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>tmpl.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo $CurrentServer."js/jquery-file-upload/";?>jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script type="text/javascript">
        $(function () {
            'use strict';
            var uploadForm = $('#fileupload'),
                previewNode = uploadForm.find(".preview span"),
                loadFile = function(file, callback){
                    callback("");
                },
                renderPreview = function (file) {
                    var node = previewNode,
                        metadataContainer = uploadForm.find(".metadata"),
                        nameNode = metadataContainer.find("label.name span"),
                        typeNode = metadataContainer.find("label.type span"),
                        sizeNode = metadataContainer.find("label.size span");
                    if (/^application\/pdf$/i.test(file.type)){
                        return (loadFile && loadFile(
                            file,
                            function (img) {
                                node.html(img);
                                nameNode.html(file.name);
                                typeNode.html(file.type);
                                sizeNode.html(file.size);
                            }
                        ));
                    }else if (/^application\/vnd.ms-excel$/i.test(file.type)){
                        return (loadFile && loadFile(
                            file,
                            function (img) {
                                node.html(img);
                                nameNode.html(file.name);
                                typeNode.html(file.type);
                                sizeNode.html(file.size);
                            }
                        ));
					}else if (/^text\/csv$/i.test(file.type)){
                        return (loadFile && loadFile(
                            file,
                            function (img) {
                                node.html(img);
                                nameNode.html(file.name);
                                typeNode.html(file.type);
                                sizeNode.html(file.size);
                            }
                        ));
                    } else {
                        alert("Invalid file type.");
                    }
                },
                dataObject;

            uploadForm.on("submit", function (e) {
                e.preventDefault();
                var button = $(e.currentTarget),
                    data = dataObject,
					navlocation = '<?php
								if ($fileRecord->folderUrl == $CurrentServer.$adminFolder.$Config_FTP_CSGSaveScheduleFileLocation."/"){
									echo "update_schedulereport";
								}elseif ($fileRecord->folderUrl == $CurrentServer.$adminFolder.$Config_FTP_CSGSaveInspectionFileLocation."/"){
									echo "update_inspectionreport";
								}else{
									echo "edit-file";
								}?>';

                if (data && data.submit && !data.jqXHR && data.submit().success(function (result, textStatus, jqXHR) {
                    window.location.href = "<?php echo $CurrentServer.$adminFolder;?>tools/?nav="+navlocation+"&id=<?php if ($id){echo $id."\"";}else{echo "\" + result[0].id";}?>;
                })) {
                    button.prop('disabled', true);
                } else {
                    // do a standard submit
                    $.ajax({
                        url: "ApiFileLibrary.php",
                        type: "POST",
                        data: JSON.stringify(uploadForm.serializeObject()),
                        dataType: "json",
                        contentType: "application/json",
                        statusCode:{
                            200: function(data){
                                window.location.href = "<?php echo $CurrentServer.$adminFolder;?>tools/?nav="+navlocation+"&id=<?php if ($id){echo $id."\"";}else{echo "\" + data[0].id";}?>;
                            },
                            409: function(jqXHR, textStatus, errorThrown){
                                var data = $.parseJSON(jqXHR.responseText),
                                    record = data.record;
                                if (data.message){
                                    alert(data.message);
                                } else {
                                    alert("Unexpected condition encountered. Please refresh the page and try again.")
                                }
                            }
                        }
                    });
                }
            });

            uploadForm.on("click", "button.cancel", function(e){
                e.preventDefault();
                window.location = "?nav=file-library";
            });

            uploadForm.on("click", "button.delete", function(e){
                $( "#dialog-confirm-delete" ).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Delete": function() {
                            var url = "ApiFileLibrary.php?file=<?php echo urlencode($fileRecord->fileName);?>&uploadFileLocation=<?php echo urlencode(str_replace($CurrentServer,"",$fileRecord->folderUrl));?>";

                            // delete
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                dataType: "json",
                                contentType: "application/json",
                                statusCode:{
                                    200: function(data){
                                        alert("File has been deleted. The page will be refreshed");
                                       window.location = "?nav=file-library";
                                    },
                                    409: function(jqXHR, textStatus, errorThrown){
                                        var data = $.parseJSON(jqXHR.responseText);
                                        if (data.message){
                                            alert(data.message);
                                        } else {
                                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                                        }
                                    }
                                }
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            });

            // Initialize the jQuery File Upload widget:
            uploadForm.fileupload({
                url: 'ApiFileLibrary.php',
                dropZone: uploadForm,
                pasteZone: uploadForm,
                uploadTemplateId: null,
                acceptFileTypes: /^text\/csv|application\/pdf|vnd.ms-excel$/i,
                drop: function (e, data) {
                    $.each(data.files, function (index, file) {
                        renderPreview(file);
                    });
                },
                change: function (e, data) {
                    $.each(data.files, function (index, file) {
                        renderPreview(file);
                    });
                },
                add: function (e, data) {
                    dataObject = data;
                }
            });

            uploadForm.bind('fileuploadsubmit', function (e, data) {
                data.formData = uploadForm.serializeArray();
            });

        });
    </script>
<?php
}
?>
