<script type="text/javascript">
		// THESE TWO VARIABLES STORE THE TIME AND DATE WHEN THE PAGE IS LOADED
		var startDate = new Date();
		var startTime = startDate.getTime();

		// THIS FUNCTION CALCULATES THE SECONDS ELAPSED SINCE THE PAGE WAS LOADED
		function seconds_elapsed () { 
			var date_now = new Date (); 
			var time_now = date_now.getTime (); 
			var time_diff = time_now - startTime; 
			var seconds_elapsed = Math.floor ( time_diff / 1000 ); 
			return ( seconds_elapsed ); 
		} 

		// THIS FUNCTION TAKES THE SECONDS ELAPSED AND CONVERTS THEM FOR OUTPUT
		function time_spent () { 
			// TAKE THE SECONDS ELAPSED
			var secs = seconds_elapsed ();

			// CONVERT SECONDS TO MINUTES AND SECONDS
			var mins = Math.floor ( secs / 60 );
			secs -= mins * 60;

			// CONVERT MINUTES TO HOURS AND MINUTES
			var hour = Math.floor ( mins / 60 );
			mins -= hour * 60;

			// DISPLAY THE FINAL OUTPUT TIME STRING
			var outputTime = pad ( hour ) + ":" + pad ( mins ) + ":" + pad ( secs );
			$(".timeElapsed").text(outputTime);
			
			var doneWithLongProcess = $('#inventoryCountResults').text();
			
			if (doneWithLongProcess != 'Done'){
				// RECURSIVELY RE-RUN THE FUNCTION EVERY SECOND
				setTimeout( "time_spent ()", 1000 ); 
			}
		}

		// THIS FUNCTION INSERTS A LEADING ZERO (IF NECESSARY) TO PROVIDE UNIFORM OUTPUT
		function pad ( num ){
			return ( ( num > 9 ) ? num : "0" + num );
		}		
		time_spent();
</script>
<div class="timeElapsed">Elapsed Time</div>
<?php
error_reporting(1);
ini_set('max_execution_time', 2400); //40 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);

$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$BatchSize = 5;
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

//Get list of files to review
$folderLocation = $siteRoot.$adminFolder."FTPFiles/MovementHistory";
include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($folderLocation, false, false);
foreach ($files as $file){
	//only look at csv files
	if (strpos($file,"csv")){
		$fileLocation = str_replace("\\","/",$file);
		echo $fileLocation.":";
		$results = new stdClass();
		$successCount = 0;
		$results->totalRecords = 0;
		if ($results->totalRecords < 1){	
			echo " Proceeding to process<br>";
			$csv_file    = file_get_contents($fileLocation);
			$rows        = explode("\n", $csv_file);
			//put the first row as column headers
			foreach ($rows as $data=>$val){
				$content = explode(",",$val);
				if ($data < 1){
					$CSGData["Columns"] = $content;
				}else{
					$CSGData["Rows"][] = $content;
				}
			}
			//convert column names to standardized format
			foreach ($CSGData["Columns"] as $Id=>$Column){
				$CSGData["Columns"][$Id]=trim($Column);
			}
			//update row information with column name so that they can be matched to CSGDailyDataRecord
			foreach ($CSGData["Rows"] as $ID=>$value){
				foreach ($value as $colID=>$rowVal){
					$cleanRowVal = str_replace('"','',$rowVal);
					if ($CSGData["Columns"][$colID] == "crew"){
						$cleanRowVal = strtoupper($cleanRowVal);
					}
					$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
				}
			}
			echo "Columns and Data Parsed.<br>";
			echo "Preparing ".count($CSGDataArray)." Transfer History records.<br>";
			//now add all the No found installed in batches of 100
				foreach ($CSGDataArray as $id=>$row){
					$efiPart = $row["EFI"];
					
							$transferInfo = array();
							$transferInfo["WarehouseFromLocation"] = $row["From"];
							$transferInfo["WarehouseToLocation"] = $row["To"];
							$transferInfo["PurchaseOrder"] = "";
							$transferInfo["TransferDate"] = MySQLDate($row["Date"]);
							$transferInfo["MovementType"] = $row["Type"];
							$transferInfo["Total_".$efiPart]=(int)$row["Qty"];
							$transferInfo["SiteID"] = "";
							$transferInfo["EFI"] = $efiPart;
							$transferInfo["Qty"] = (int)$row["Qty"];
							$transferInfo["ID1"] = "";
							$transferInfo["ByAdmin"] = $row["By"];
							$TransferCollection[] = $transferInfo;
				}

			$processedMonth = date("Y M",strtotime($row["Date"]));
			//Move file into complete folderLocation
			$movedFileLocation = str_replace("MovementHistory/","MovementHistory/complete/",$fileLocation);
			rename($fileLocation,$movedFileLocation);
		}else{
			echo " Already Processed<br>";
			$processedMonth = $results->monthProcessed;
			$resultsCount["notFound"] = 0;
		}//end if not already added
		
	}//end if csv
}
if ($TransferCollection > 1){
	echo count($TransferCollection)." Movement records ready to be transferred for the month ".$processedMonth."<br>\n";
?>
		<div class="row">
			<div class="five columns">
				<button id="inventoryCountUpdate">Install Transfer History</button>
			</div>
			<div class="seven columns">
				<div class="timeElapsed">Elapsed Time</div>
			</div>
		</div>
		<div id="inventoryQueueResults"></div>
		<div id="inventoryCountResults"></div>

		<table id="inventoryCountTable">
			<thead>
				<tr><th>Transfer Date</th><th>From</th><th>To</th><th>EFI</th><th>Units</th><th>By</th></tr>
			</thead>
			<tbody>
			
			</tbody>
		</table>
		<script type="text/javascript">
		$.fn.LongProcess = function () {
			var _this = this;
			this.notifications = [];
			this.actions = [];

			this.add = function (_notification, _action) {
				this.notifications.push(_notification);
				this.actions.push(_action);
			};
			this.run = function () {

				if (!_this.actions && !_this.notifications) {
					return "Empty";
				}
				//******************************************************************
				//This section makes the actions lag one step behind the notifications.
				var notification = null;
				if (_this.notifications.length > 0) notification = _this.notifications.shift();

				var action = null;
				if ((_this.actions.length >= _this.notifications.length + 2) || (_this.actions.length > 0 && _this.notifications.length == 0)) 
					action = _this.actions.shift();
				//****************************************************************
				if (!action && !notification) {
					return "Completed";
				}

				if (action) action();        
				if (notification) notification();

				setTimeout(_this.run, 100); 
				//setTimeout(_this.run,1); //set to 1 after you've entered your actual long running process. The 1000 is there to just show the delay.
			}
			return this;
		};		
		
		
		$(function(){
			var inventoryCountResults = $('#inventoryCountResults');
			var inventoryQueueResults = $('#inventoryQueueResults');
			var inventoryCountTable = $("#inventoryCountTable");
			$("#inventoryCountUpdate").on('click',function(e){
				e.preventDefault();
				inventoryCountResults.show().html("Processing...").removeClass("info").removeClass("alert").addClass("alert");
				var transferCollection = <?php echo json_encode($TransferCollection);?>;
					//console.log(transferCollection);
				inventoryQueueResults.removeClass("info").removeClass("alert").addClass("alert");
				inventoryCountResults.removeClass("info").removeClass("alert").addClass("alert").addClass("info");
				var process = $().LongProcess(),
					totalCount = transferCollection.length;
				$.each(transferCollection,function(key,obj){
					var percentage = Math.round(Math.round((key/totalCount)*10000)/100);
					var displayKey = (key+1);
					process.add(function () {
							inventoryQueueResults.text("Processing item "+displayKey+" into Queue "+percentage+"% complete");
						}, function () {
						var items = {};
							items["action"] = 'movementHistoryInstall',
							items["TransferCollection"] = obj,
							data = JSON.stringify(items);
							$.ajax({
								url: "ApiDataTools.php",
								type: "POST",
								data: data,
								success: function(data){
									if (data.error == '' || !data.error){
//										console.log(key);
//										console.log(data);
										inventoryCountResults.text("Item "+displayKey+" Processed").removeClass("info").removeClass("alert").addClass("alert").addClass("info");
										InventoryUpdateResults.rows.add( [[
											data.MarkedInstalled[0]["installDate"],
											data.MarkedInstalled[0]["crewId"],
											data.MarkedInstalled[0]["toWarehouse"],
											data.MarkedInstalled[0]["efi"],
											data.MarkedInstalled[0]["units"],
											data.MarkedInstalled[0]["byAdmin"]
										]] ).draw();
										if (displayKey == totalCount){
											inventoryCountResults.text("Done");
										}

								}else{
										inventoryCountResults.html(data.error).removeClass("info").removeClass("alert").addClass("alert");
									}
								},
								error: function(jqXHR, textStatus, errorThrown){
									var message = $.parseJSON(jqXHR.responseText);
									inventoryCountResults.html(message).removeClass("info").removeClass("alert").addClass("alert");
								}
							});
					});					
				});
				process.add(function () {
						inventoryQueueResults.text("All Items Queued");
					}, function () {
						//inventoryCountResults.text("Done");
				});
				process.run();

				
			});

			var InventoryUpdateResults = inventoryCountTable.DataTable({
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
				},				
				"scrollX": true,
				"bJQueryUI": true,
				"bSearchable":false,
				"bFilter":false,
				"bAutoWidth": true,
				"bSort": false,
				"iDisplayLength": 10
			});

		});
		
		</script>
<?php
}else{	
	//if no results, then add the file		
	if (count($resultsCount["notFound"]) < 1){$resultCollection["fileHistory"] = $CSGDailyDataProvider->insertMonthlyDataFileHistory($fileLocation);}		
	if ($resultCollection["fileHistory"]->success){
		echo "All Rows from ".$fileLocation." have been imported<br>";
	}else{
		echo "No New Bulb Reports found.  Use the File Library to upload a new bulb report";
	}
}
?>

1)Export Transfer history and paste into csv file.<br>
2)Clean Transfer History records and make sure trailing zeros are intack for 3010.020 and 3010.100 and Name/CET match<br>
2)Set Date to YYYY-MM-DD format and order by date.<br>
3)Remove last blank row via notepad++ edit.<br>
4)Copy file into FTPFiles/MovementHistory folder and run Transfer History update report<br>
5)Process April Bulb report to remove John DuPerry-CET to John DuPerry and set date fields to YYYY-MM-DD format and reorder by Install_dt<br>
6)Also check that one AuditorName to One CrewID and fix any inconsistencies before importing<br>

<hr>
Procedure for rebuild.<br>
DISABLED WIRELESS.  USE ETHERNET CABLE.<br>
<br>
1)Backup `ghs_inventory_products_movementhistory, productount_current, productcount<br>
2)Clear out ghs_inventory_products_movementhistory, and drop ghs_inventory_warehouse_productcount_current, ghs_inventory_warehouse_productcount<br>
3)Copy ghs_inventory_warehouse_productcount_2014_11_05 to ghs_inventory_warehouse_productcount_current and ghs_inventory_warehouse_productcount<br>
4)Rebuild indexes for productcount_current: CurrentCountByWarehouseEFI UNIQUE on _WarehouseLocationName AND _ProductsEFI<br>
5)Rebuild indexes for productcount: EfiAndWarehouse INDEX on _ProductsEFI AND _WarehouseLocationName<br>
6)Clean Transfer History records make sure trailing zeros are intack for 3010.020 and 3010.100<br>
7)Run Transfer History by date<br>
8)Copy results into report<br>
9)Move BulbReport into folder and run Bulb Report Update script<br>
	a)Install into DailyData<br>
	b)Update Movement history and Count<br>
	<br>
10)Repeat for next month<br>