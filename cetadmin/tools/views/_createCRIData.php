<?php
ini_set('memory_limit', '512M');
error_reporting(1);
$adminFolderToUse = ($isStaff ? $staffFolder : $adminFolder);

$monthYear = ($_GET['monthYear'] ? date("M Y", strtotime($_GET['monthYear'])) : date("M Y", strtotime("-1 month")));
$invoiceDate = date("m/1/Y",strtotime($monthYear));
$yearMonth = date("Y_m",strtotime($monthYear));

$time_start = microtime(true); 
$path = getcwd();
if (!strpos($path,"xampp")){
	$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
	$thispath = "";
}else{
	$path = "";
	include_once("../_config.php");
	$thispath = "../";
}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($thispath.$path.'sharepoint/SharePointAPIFiles.php');
ini_set('max_execution_time', 1200); //300 seconds = 5 minutes

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$warehouseProvider = new WarehouseProvider($dataConn);
$warehouseResults = $warehouseProvider->get();
$warehouseCollection = $warehouseResults->collection;
//Get list of active warehouse locations
foreach ($warehouseCollection as $warehouse){
	if ($warehouse->displayOrderId && strpos(" ".$warehouse->name,"CET")){
		$ActiveWarehouses[] = $warehouse->name;
	}
}
$ActiveWarehouseString = implode("','",$ActiveWarehouses);

include_once($dbProviderFolder."CETDataProvider.php");
$cetDataProvider = new CETDataProvider($dataConn);
foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	//echo $cachedFiledLocationName."-".$ID.".json"."<Br>";
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	foreach (${$ArrayName} as $SPData){
		$spData = (object)$SPData;
		//$results[] = $cetDataProvider->insertSharePointData($spData);
		$siteId = $spData->site_x0020_id;
		$SiteIDData[$siteId]["phone"] = $spData->phone_x0020_number;
		$SiteIDData[$siteId]["utility"] = $spData->utility;
		$SiteIDData[$siteId]["secondaryutility"] = $spData->secondary_x0020_utility;
		$ASfinal = $spData->final_x0020__x0024_;
		$ASoriginal = $spData->a_x002f_s_x0020_original_x0020_c;
		$INSfinal = $spData->insulation_x0020_final_x0020_con;
		$INSoriginal = $spData->insulation_x0020_original_x0020_;
		$eobCreate = false;
		if ($ASfinal != $ASoriginal){$eobCreate = true;}
		if ($INSfinal != $INSoriginal){$eobCreate = true;}
		
		$billDate = $spData->utility_x0020_billing_x0020_date;
		
		$billYearMonth = date("Y_m",strtotime($billDate));

		
		$SiteIDData[$siteId]["final"] = $final;
		$SiteIDData[$siteId]["original"] = $original;
		
		if ($eobCreate && $billYearMonth == $yearMonth){
			$eobData[$siteId] = $spData;
		}
		
		erase_val($SPData);
		$SPData = null;
	}//end foreach sprecord
	${$ArrayName} = null;
}//end foreach view
//echo $yearMonth." ".count($eobData[$yearMonth])." EOBs";
//print_pre($eobData);
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
//echo '<b>Total Process Time:</b> '.$execution_time;


$criteria = new stdClass();
$criteria->startDate = date("Y-m-d",strtotime($invoiceDate));
$criteria->endDate = date("Y-m-t",strtotime($invoiceDate));
$criteria->proposedStartDate = date("Y-m-d",strtotime($invoiceDate." -2 months"));
$criteria->proposedEndDate = date("Y-m-t",strtotime($invoiceDate));
$criteria->primaryProvider = "BERKSHIR";
$criteria->crewIn = $ActiveWarehouseString;
?>
<style>
.ui-datepicker-calendar {display: none;}
.ui-datepicker-trigger {cursor:pointer;}
</style>

<h1>Import BGAS Extract File</h2>
Instructions:<br>
Receive Extracts from Heather McCreary and set date on this page<br>
<div class="row">
	<div class="three columns">
		<h3>
			<?php echo $monthYear;?>
			<input type="hidden" size="8" data-source="StartDate" data-display="StartDateSpan" class="date-picker" id="MonthPicker" name="MonthPicker" value="<?php echo date("m/1/Y", strtotime(str_replace(" "," 1 ",$monthYear)));?>">
		</h3>
	</div>
</div>
<br clear="all">
<?php
$ImportFileName = "../gbs/importfiles/extract/BGASExtract_".$yearMonth.".xls";
//echo "ImportFileName ".$ImportFileName."<br>";
$target_file = $siteRoot.$adminFolder."gbs/".$ImportFileName;
//echo $target_file;
if(count($_FILES['fileToUpload1'])) {
	$tmpFileName = $_FILES["fileToUpload1"]["tmp_name"][0];
	if ($_GET['type'] == "uploadFile"){
		$ImportFileReceived = true;
		move_uploaded_file($tmpFileName, $target_file);
	}
}
if ($ImportFileReceived || $_GET['useUploadedFile']){
	//new import that data and parse it out 
	$trackingFile = (trim($_GET['useUploadedFile']) ? $_GET['useUploadedFile'] : $ImportFileName);
	echo "Using File just uploaded<br>";
	ob_flush();
	include('../gbs/salesforce/read_rawFileReader.php');
	echo "will delete prior YearMonth ".$yearMonth." records and add new records<br>";
	$criteria2 = new stdClass();
	$criteria2->yearMonth = $yearMonth;
	//first delete old records
	$deleteExtractBgas = $cetDataProvider->deleteExtractDataBGAS($criteria2);
	//add new records
	foreach ($rowsRaw as $index=>$rowInfo){
		if ($rowInfo["SITEID"]){
			foreach ($rowInfo as $key=>$val){
				if (trim($val) == ""){
					$val = '';
				}
				if (substr($key,-3) == "_DT" || substr($key,-4) == "DATE"){
					$valorg = $val;
					$val = date("Y-m-d",strtotime($val));
					if ($val == "1969-12-31"){
//						$dateFormat = "mm/dd/YYYY";
						$val = exceldatetotimestamp($valorg,$dateFormat);
//						$val = date("m/d/Y",strtotime($val));
						if ($val == "12/31/1969"){
							$val = '';
						}
					}
				}
				$cleanData[$key] = $val;
			}
			$cleanRows[] = $cleanData;
		}
	}
	
	$insertExtractBgas = $cetDataProvider->insertExtractDataBGAS($cleanRows,$criteria2);
	echo count($insertExtractBgas["successes"])." records imported<br>";
	print_pre($insertExtractBgas["errors"]);
}
if (!$ImportFileReceived){
	if (file_exists($target_file)){
		$lastUploadedDateParts = explode("File",$ImportFileName);
		$lastDEPUploadedDate = str_replace("_","-",str_replace(".xlsx","",$lastUploadedDateParts[1]))."-01";
		
		echo "<br><br><a href='".$ImportFileName."'>View Existing Extract File</a> last uploaded ".date("F d, Y H:ia",filemtime($target_file))."<br>";
		$ImportFileName = ($isStaff ? $rootFolder.$adminFolder."gbs/".$ImportFileName : $ImportFileName);
		$trackingFile = $ImportFileName;
	}
}
?>


	<a id='uploadFile' href="#" class='button-link do-not-navigate'>Upload File</a><br>
	<div id="uploadFileForm" style='display:none;'>
		<form method="post" action="<?php echo $CurrentServer.$adminFolderToUse;?>tools/?type=uploadFile<?php foreach($_GET as $key=>$val){echo "&".$key."=".$val;};?>" enctype="multipart/form-data">
			<div class="row">
				<div class="eight columns">
					<input type="file" name="fileToUpload1[]" id="fileToUpload1" multiple="" onChange="makeFileList1();" /><Br>
				</div>
				<div class="eight columns">
					<strong>File You Selected:</strong>
					<ul id="fileList1"><li>No Files Selected</li></ul>
					<input type="submit" value="Begin Processing" id="submitButton1" style="display:none;">
				</div>
			</div>
			
			<script type="text/javascript">
				function makeFileList1() {
					var input = document.getElementById("fileToUpload1");
					var ul = document.getElementById("fileList1");
					while (ul.hasChildNodes()) {
						ul.removeChild(ul.firstChild);
					}
					for (var i = 0; i < input.files.length; i++) {
						var li = document.createElement("li"),
							fileName = input.files[i].name,
							fileLength = fileName.length,
							fileExt = fileName.substr(fileLength-4);
							console.log(fileExt);
						if (fileExt == ".xls" || fileExt == "xlsx"){
							li.innerHTML = input.files[i].name;
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'block';
						}else{
							li.innerHTML = 'You must save '+fileName+' as a .xls or .xlsx file first';
							ul.appendChild(li);
							document.getElementById('submitButton1').style.display = 'none';
						}
					}
					if(!ul.hasChildNodes()) {
						var li = document.createElement("li");
						li.innerHTML = 'No Files Selected';
						ul.appendChild(li);
						document.getElementById('submitButton1').style.display = 'none';
					}
				}
			</script>
		</form>
	</div>

<script type="text/javascript">
	$(function () {
		$("#uploadFile").on('click',function(){
			$("#uploadFileForm").toggle();
		});
		
		var table = $('.simpleTable').DataTable({
			"scrollX": true,
//			"scrollY": "300px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 25,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		var tableLimited = $('.simpleTableLimited').DataTable({
			"scrollX": true,
			"scrollY": "240px",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 10,
			"paging":   false,
//			"ordering": false,
			"scrollCollapse": true,
			"info":     false
		});
		$(".DataTables_sort_wrapper").first().click().click();
		
		var getFilters = function(){
			var monthYear = $("#MonthPicker").val(),
				filterValues = new Array(monthYear);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>tools/?monthYear="+Filters[0]+"&nav=cri_report#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		
		$('.date-picker').datepicker({
			showOn: "button",
			buttonImage: "<?php echo $CurrentServer;?>css/images/icon-calendar.png",
			buttonImageOnly: true,
			buttonText: 'Click to change month',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			minDate: '01/01/14',
			maxDate: '<?php echo date("m/t/Y");?>',
			onClose:
				function(dateText, inst){
					var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, month, 1));
					$("#StartDate").datepicker('setDate', new Date(year, month, 1));
					var date2 = $(this).datepicker('getDate');
					date2.setMonth(date2.getMonth() +1);
					date2.setDate(date2.getDate() -1);
					$("#EndDate").datepicker('setDate', date2);
					$("#weekPicker").val('');
					updateFilters();
				}
		
		});

		
	});
</script>

<!--
Copy the Sheet 'SQL Results' into new document.<br>
Make sure header row is:<br>
SITEID,CUST_FIRST_NAME,CUST_LAST_NAME,ADDRESS,CITY,STATE,ZIP,PLUS4,NUM_UNITS,PRIMARY_PRVD,PROVIDERID,PROVIDERCODE,ACCTCUST,LOCATIONID,SECONDARY_PRVD,PROVIDERID2,PROVIDERCODE2,ACCTCUST2,HEATFUEL,DHWFUEL,CUST_TYPE,APPTDATE,APPTTYPE,AUDITOR_FIRST_NAME,AUDITOR_LAST_NAME,CONTRACT,MEASTABLE,ID0,ID1,DESCRIPTION,PROP_PARTID,PROP_QTY,INS_PARTID,INS_QTY,MBTU_SAVINGS,DEPOSIT,CUST_PRICE,UTIL_PRICE,PAYEE,ISSUED_DT,SIGNED_DT,INSTALL_DT,INV_STATUS,MEAS_STATUS,INSTALLER_NAME,MEASURELIFE,ICTV_AWARE_SIR,ICTV_AWARE_SIMPLEPAYBACK,Therms<br>
Then Remove the Header Row otherwise it gets imported as a row.<Br>
Convert dates to YYYY-MM-DD format and save as CSV.<br>
Open this file in NotePad++ and check for any dates that did not convert to the formate by searching for / and doing a text replace for that date in the new format.<br>
Remove any blank last rows and re-save.<br>
Open up PhpMyAdmin and make a backup copy of cri_extract_bgas.<br>
Import the csv file you created into the table, cri_extract_bgas, using the header row above.<br>
Then run this report for the date range you just imported.<br>
-->
For the Month of <?php echo $invoiceDate;?><br>
To change month use Calendar to pick a month and year<br>

<?php
$extractBgas = $cetDataProvider->getCRIExtractDataBGAS($criteria);

$proposedContracts = $cetDataProvider->getProposedEZDocData($criteria);
//print_pre($openContracts);

$ocpReport = $cetDataProvider->getCRIOCP($criteria);
	$SiteID = 1;
	$jobCompleted[$SiteID] = array("SITEID","APPTDATE","APPTSTART","SCHEDAPPTTYPE","PROGRAMTYPE","ELECTRICUTILITYNAME","ELECTRICACCTNUM","GASUTILITYNAME","GASACCTNUM","HEATINGSOURCE","VENDOR1",
		"VENDOR2","VENDOR3","AGE","SQFT","BLDTYPE","UNITS","CONTACTNAME","CONTACTPHONE","FACILITYNAME","Custid","Fname","Lname","ADDRESS","CITY","STATE","ZIP","PHONE","CELL","EMAIL","LOWINCOME","MDATE");
	$jobInstalled[] = $jobCompleted;	
	$jobProposed[] = $jobCompleted;
	$measureCompleted[$SiteID] = array("SITEID","MID","MDATE","MNUMERICVALUE","MTEXTVALUE","MBOOLEANVALUE","UTILITYBILLED","INSTALLER","INSTALLERTYPE");
	$measuresProposed[$SiteID] = array("SITEID","MID","MDATE","MNUMERICVALUE","UTILITYBILLED");
	$measureInstalled[] = $measureCompleted;
	$measureProposed[] = $measuresProposed;
foreach ($extractBgas as $extractData){
	$SiteID = $extractData["SITEID"];
	$measureCompleted = array();
	$jobsCompleted[$SiteID]["SITEID"] = $SiteID;
	$measureCompleted[$SiteID]["SITEID"] = $SiteID;
	$measureCompleted[$SiteID]["MID"] = $extractData["INS_PARTID"];
	$jobsCompleted[$SiteID]["SCHEDDATE"] = MySQLDate($extractData["APPTDATE"]);
	$measureCompleted[$SiteID]["MDATE"] = MySQLDate($extractData["INSTALL_DT"]);
	$measureCompleted[$SiteID]["MNUMERICVALUE"] = $extractData["INS_QTY"];
	$measureCompleted[$SiteID]["MTEXTVALUE"] = "";
	$measureCompleted[$SiteID]["MBOOLEANVALUE"] = "";
	$measureCompleted[$SiteID]["UTILITYBILLED"] = $extractData["PRIMARY_PRVD"];
	$measureCompleted[$SiteID]["INSTALLER"] = $extractData["INSTALLER_NAME"];
	$measureCompleted[$SiteID]["INSTALLERTYPE"] = "IIC";
	$jobsCompleted[$SiteID]["APPTSTART"] = "";
	$jobsCompleted[$SiteID]["SCHEDAPPTTYPE"] = $extractData["APPTTYPE"];
	$jobsCompleted[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsCompleted[$SiteID]["ELECTRICUTILITYNAME"] = $extractData["SECONDARY_PRVD"];
	$jobsCompleted[$SiteID]["ELECTRICACCTNUM"] = "";
	$jobsCompleted[$SiteID]["GASUTILITYNAME"] = $extractData["PRIMARY_PRVD"];
	$jobsCompleted[$SiteID]["GASACCTNUM"] = "";
	$jobsCompleted[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsCompleted[$SiteID]["VENDOR1"] = "CSG";
	$jobsCompleted[$SiteID]["VENDOR2"] = "CET";
	$jobsCompleted[$SiteID]["VENDOR3"] = "";
	$jobsCompleted[$SiteID]["AGE"] = "";
	$jobsCompleted[$SiteID]["SQFT"] = "";
	$jobsCompleted[$SiteID]["BLDTYPE"] = "";
	$jobsCompleted[$SiteID]["UNITS"] = "";
	$jobsCompleted[$SiteID]["CONTACTNAME"] = "";
	$jobsCompleted[$SiteID]["CONTACTPHONE"] = "";
	$jobsCompleted[$SiteID]["FACILITYNAME"] = "";
	$jobsCompleted[$SiteID]["Custid"] = $extractData["ACCTCUST"];
	$jobsCompleted[$SiteID]["Fname"] = $extractData["CUST_FIRST_NAME"];
	$jobsCompleted[$SiteID]["Lname"] = $extractData["CUST_LAST_NAME"];
	$jobsCompleted[$SiteID]["ADDRESS"] = $extractData["ADDRESS"];
	$jobsCompleted[$SiteID]["CITY"] = $extractData["CITY"];
	$jobsCompleted[$SiteID]["STATE"] = $extractData["STATE"];
	$jobsCompleted[$SiteID]["ZIP"] = (strlen($extractData["ZIP"]) >= 5 ? $extractData["ZIP"] : "0".$extractData["ZIP"]);
	$jobsCompleted[$SiteID]["PHONE"] = $SiteIDData[$SiteID]["phone"];
	$jobsCompleted[$SiteID]["CELL"] = "";
	$jobsCompleted[$SiteID]["EMAIL"] = "";
	$jobsCompleted[$SiteID]["LOWINCOME"] = "";
	$jobsCompleted[$SiteID]["MDATE"] = MySQLDate($extractData["INSTALL_DT"]);
	$measureInstalled[] = $measureCompleted;
}
$jobInstalled[] = $jobsCompleted;
foreach ($proposedContracts as $ezDocData){
	$SiteID = $ezDocData["ResidentialEZDocData_SiteID"];
	$dataByFieldName = json_decode($ezDocData["ResidentialEZDocData_DataByFieldName"]);
	$dataByMeasure = json_decode($ezDocData["ResidentialEZDocData_DataByMeasure"]);
	$proposedDate = date("m/d/Y",strtotime($dataByFieldName->APPTDATE));
	foreach ($dataByMeasure as $measureType=>$measureData){
		if ($measureType !="Installed Today"){
			foreach ($measureData as $count=>$measureInfo){
				
				foreach ($measureInfo as $measure){
					if (trim($measure->PROP_PARTID) != ""){
						$measuresProposed = array();
						$measuresProposed[$SiteID]["SITEID"] = $SiteID;
						$measuresProposed[$SiteID]["MID"] = $measure->PROP_PARTID;
						$measuresProposed[$SiteID]["MDATE"] = $proposedDate;
						$measuresProposed[$SiteID]["MNUMERICVALUE"] = $measure->PROP_QTY;
						$measuresProposed[$SiteID]["UTILITYBILLED"] = "BGAS";
						
						$measureProposed[] = $measuresProposed;
					}
				}
			}
		}
	}

	//print_pre($dataByFieldName);
	$jobsProposed[$SiteID]["SITEID"] = $SiteID;
	$jobsProposed[$SiteID]["SCHEDDATE"] = $proposedDate;
	$jobsProposed[$SiteID]["APPTSTART"] = "";
	$jobsProposed[$SiteID]["SCHEDAPPTTYPE"] = "";
	$jobsProposed[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsProposed[$SiteID]["ELECTRICUTILITYNAME"] = $dataByFieldName->SECONDARY_PRVD;
	$jobsProposed[$SiteID]["ELECTRICACCTNUM"] = $dataByFieldName->ACCTCUST2;
	$jobsProposed[$SiteID]["GASUTILITYNAME"] = "BGAS";
	$jobsProposed[$SiteID]["GASACCTNUM"] = $dataByFieldName->ACCTCUST;
	$jobsProposed[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsProposed[$SiteID]["VENDOR1"] = "CET";
	$jobsProposed[$SiteID]["VENDOR2"] = $dataByFieldName->AUDITOR_FIRST_NAME;
	$jobsProposed[$SiteID]["VENDOR3"] = "";
	$jobsProposed[$SiteID]["AGE"] = "";
	$jobsProposed[$SiteID]["SQFT"] = "";
	$jobsProposed[$SiteID]["BLDTYPE"] = "";
	$jobsProposed[$SiteID]["UNITS"] = "";
	$jobsProposed[$SiteID]["CONTACTNAME"] = "";
	$jobsProposed[$SiteID]["CONTACTPHONE"] = "";
	$jobsProposed[$SiteID]["FACILITYNAME"] = "";
	$jobsProposed[$SiteID]["Custid"] = "";
	$jobsProposed[$SiteID]["Fname"] = $dataByFieldName->CUST_FIRST_NAME;
	$jobsProposed[$SiteID]["Lname"] = $dataByFieldName->CUST_LAST_NAME;
	$jobsProposed[$SiteID]["ADDRESS"] = $dataByFieldName->ADDRESS;
	$jobsProposed[$SiteID]["CITY"] = $dataByFieldName->CITY;
	$jobsProposed[$SiteID]["STATE"] = "MA";
	$jobsProposed[$SiteID]["ZIP"] = (strlen($dataByFieldName->ZIP) >= 5 ? $dataByFieldName->ZIP : "0".$dataByFieldName->ZIP);
	$jobsProposed[$SiteID]["PHONE"] = $dataByFieldName->PHONE;
	$jobsProposed[$SiteID]["CELL"] = "";
	$jobsProposed[$SiteID]["EMAIL"] = $dataByFieldName->EMAIL;
	$jobsProposed[$SiteID]["LOWINCOME"] = "";
	$jobsProposed[$SiteID]["MDATE"] = $proposedDate;
}
//print_pre($jobsProposed);
/* old way when CSG sent us OCP data file that went into table cri_ocp
foreach ($ocpReport as $ocpReportData){
	$SiteID = $ocpReportData["SITEID"];
	$measuresProposed = array();
	$jobsProposed[$SiteID]["SITEID"] = $SiteID;
	$measuresProposed[$SiteID]["SITEID"] = $SiteID;
	$measuresProposed[$SiteID]["MID"] = $ocpReportData["PARTID"];
	$measuresProposed[$SiteID]["MDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$measuresProposed[$SiteID]["MNUMERICVALUE"] = $ocpReportData["QTY"];
	$measuresProposed[$SiteID]["UTILITYBILLED"] = $ocpReportData["PRIMARY_PROVIDER"];
	$jobsProposed[$SiteID]["SCHEDDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$jobsProposed[$SiteID]["APPTSTART"] = "";
	$jobsProposed[$SiteID]["SCHEDAPPTTYPE"] = "";
	$jobsProposed[$SiteID]["PROGRAMTYPE"] = "Mass Save SF";
	$jobsProposed[$SiteID]["ELECTRICUTILITYNAME"] = $SiteIDData[$SiteID]["secondaryutility"];
	$jobsProposed[$SiteID]["ELECTRICACCTNUM"] = "";
	$jobsProposed[$SiteID]["GASUTILITYNAME"] = $ocpReportData["PRIMARY_PROVIDER"];
	$jobsProposed[$SiteID]["GASACCTNUM"] = "";
	$jobsProposed[$SiteID]["HEATINGSOURCE"] = "Gas";
	$jobsProposed[$SiteID]["VENDOR1"] = "CSG";
	$jobsProposed[$SiteID]["VENDOR2"] = $ocpReportData["CREW"];
	$jobsProposed[$SiteID]["VENDOR3"] = "";
	$jobsProposed[$SiteID]["AGE"] = "";
	$jobsProposed[$SiteID]["SQFT"] = "";
	$jobsProposed[$SiteID]["BLDTYPE"] = "";
	$jobsProposed[$SiteID]["UNITS"] = "";
	$jobsProposed[$SiteID]["CONTACTNAME"] = "";
	$jobsProposed[$SiteID]["CONTACTPHONE"] = "";
	$jobsProposed[$SiteID]["FACILITYNAME"] = "";
	$jobsProposed[$SiteID]["Custid"] = "";
	$jobsProposed[$SiteID]["Fname"] = $ocpReportData["FIRST_NAME"];
	$jobsProposed[$SiteID]["Lname"] = $ocpReportData["LAST_NAME"];
	$jobsProposed[$SiteID]["ADDRESS"] = $ocpReportData["ADDRESS"];
	$jobsProposed[$SiteID]["CITY"] = $ocpReportData["CITY"];
	$jobsProposed[$SiteID]["STATE"] = $ocpReportData["STATE"];
	$jobsProposed[$SiteID]["ZIP"] = (strlen($ocpReportData["ZIP"]) >= 5 ? $ocpReportData["ZIP"] : "0".$ocpReportData["ZIP"]);
	$jobsProposed[$SiteID]["PHONE"] = $ocpReportData["HOME"];
	$jobsProposed[$SiteID]["CELL"] = $ocpReportData["CELL"];
	$jobsProposed[$SiteID]["EMAIL"] = $ocpReportData["CUSTOMER_EMAIL"];
	$jobsProposed[$SiteID]["LOWINCOME"] = "";
	$jobsProposed[$SiteID]["MDATE"] = MySQLDate($ocpReportData["PROPOSED_DT"]);
	$measureProposed[] = $measuresProposed;

}
*/
$jobProposed[] = $jobsProposed;
	
$fileTypes = array("job","measure");
$fileCategories = array("Installed","Proposed"); //ClearResults will send Proposed because they changed the format of the OCP data on us to a degree that did not provide the information we needed
//$fileCategories = array("Installed");
$folderLocation = $path."tools/CRIData/";
foreach ($fileCategories as $fileCategory){
	foreach ($fileTypes as $fileType){
		$CompleteFileNameTxt = $folderLocation.$fileType.$fileCategory.$yearMonth.".txt";
		$arrayName = $fileType.$fileCategory;
		$fpCompleted = fopen($CompleteFileNameTxt, 'wt');
		foreach (${$arrayName} as $id=>$siteId) {
			foreach ($siteId as $field){
				fputcsv($fpCompleted, $field,"\t");
			}
		}
		fclose($fpCompleted);
		//remove all quote encapsulation
		$data = file_get_contents($CompleteFileNameTxt);
		$data = str_replace('"','', $data);
		$data = str_replace('\n','\r\n', $data);
		file_put_contents($CompleteFileNameTxt, $data);
		$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'tools/'.str_replace($path."tools","",$CompleteFileNameTxt).'">'.str_replace($path."tools/CRIData/","",$CompleteFileNameTxt).'</a><br>';
		$files_to_zip[] = $CompleteFileNameTxt;

		$CompleteFileNameCsv = $folderLocation.$fileType.$fileCategory.$yearMonth.".csv";
		$fpCompleted = fopen($CompleteFileNameCsv, 'w');
		foreach (${$arrayName} as $id=>$siteId) {
			foreach ($siteId as $field){
				fputcsv($fpCompleted, $field);
			}
		}
		fclose($fpCompleted);
		$fileLinks .= '<a href="'.$CurrentServer.$adminFolder.'tools/'.str_replace($path."tools","",$CompleteFileNameCsv).'">'.str_replace($path."tools/CRIData/","",$CompleteFileNameCsv).'</a><br><hr>';
		$files_to_zip[] = $CompleteFileNameCsv;
		
	}
}
$zippedFileName = $folderLocation.'CET_CRIData_'.$yearMonth.'.zip';
$zippedFiles = create_zip($files_to_zip,$zippedFileName,true,$folderLocation);

$criteria = new stdClass();
$criteria->yearMonth = $yearMonth;
$criteria->primaryProvider = "BERKSHIR";
$BgasExtractData = $cetDataProvider->getCRIExtractDataBGAS($criteria);
foreach ($BgasExtractData as $extractData){
	$SiteID = $extractData["SITEID"];
	if ($eobData[$SiteID]){
		$eobExtractDetails[$SiteID][] = $extractData;
		$eobExtractData[$SiteID]["firstName"] = $extractData["CUST_FIRST_NAME"];
		$eobExtractData[$SiteID]["lastName"] = $extractData["CUST_LAST_NAME"];
		$eobExtractData[$SiteID]["address"] = $extractData["ADDRESS"];
		$eobExtractData[$SiteID]["city"] = $extractData["CITY"];
		$eobExtractData[$SiteID]["state"] = $extractData["STATE"];
		$eobExtractData[$SiteID]["zip"] = $extractData["ZIP"];
		$eobExtractData[$SiteID]["contractor"] = ($extractData["INSTALLER_NAME"] && strtoupper($extractData["INSTALLER_NAME"]) != "CET" ? $extractData["INSTALLER_NAME"] : $eobExtractData[$SiteID]["contractor"]);
	}
}

echo count($eobData)." SharePoint Data<br>";
ksort($eobData);
ksort($eobExtractData);
echo count($eobExtractData)." Extract Data<br>";
if (count($eobData) != count($eobExtractData)){
	echo "<table border=1 cellpadding=5><tr><td>SharePoint Sites</td><td>Extract File Sites</td></tr>";
	echo "<Tr><td valign='top'>";
	foreach ($eobData as $siteId=>$data){
		echo $siteId."<br>";
	}
	echo "</td><td valign='top'>";
	foreach ($eobExtractData as $siteId=>$data){
		echo $siteId."<br>";
	}
	echo "</td></tr></table>";
	echo "Will Not Create EOB Files!<Br>";
}else{
//	include_once('model/eobReport.php');
}
echo $ReportsFileLink;

/*
$jobsCompleteFileNameTxt = "jobsCompleted.txt";
$fpCompleted = fopen($jobsCompleteFileNameTxt, 'w');
foreach ($jobCompleted as $fields) {
    fputcsv($fpCompleted, $fields,"\t");
}
fclose($fpCompleted);

$jobsCompleteFileNameCsv = "jobsCompleted.csv";
$fpCompleted = fopen($jobsCompleteFileNameCsv, 'w');
foreach ($jobCompleted as $fields) {
    fputcsv($fpCompleted, $fields);
}
fclose($fpCompleted);

$jobsProposedFileNameTxt = "jobsProposed.txt";
$fpProposed = fopen($jobsProposedFileNameTxt, 'w');
foreach ($jobProposed as $fields) {
    fputcsv($fpProposed, $fields,"\t");
}
fclose($fpProposed);

$jobsProposedFileNameCsv = "jobsProposed.csv";
$fpProposed = fopen($jobsProposedFileNameCsv, 'w');
foreach ($jobProposed as $fields) {
    fputcsv($fpProposed, $fields);
}
fclose($fpProposed);
*/
echo $fileLinks;
echo "Zip files at: <a href='".$CurrentServer.$adminFolder."tools/CRIData/".str_replace($path."tools/CRIData/","",$zippedFileName)."'>".str_replace($path."tools/CRIData/","",$zippedFileName)."</a><br>";

$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
echo $execution_time;
?>
<!--
<a href="<?php echo $CurrentServer.$adminFolder."tools/".$jobsCompleteFileNameTxt;?>">JobsCompleted.txt</a><br>
<a href="<?php echo $CurrentServer.$adminFolder."tools/".$jobsCompleteFileNameCsv;?>">JobsCompleted.csv</a>
<hr>
<a href="<?php echo $CurrentServer.$adminFolder."tools/".$jobsProposedFileNameTxt;?>">JobsProposed.txt</a><br>
<a href="<?php echo $CurrentServer.$adminFolder."tools/".$jobsProposedFileNameCsv;?>">JobsProposed.csv</a>
-->

