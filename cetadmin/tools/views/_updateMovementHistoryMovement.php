<?php	
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$CSGDailyRecord = new CSGDailyDataRecord();
$CSGDailyDataProvider = new CSGDailyDataProvider($dataConn);
$productProvider = new ProductProvider($dataConn);
$warehouseProvider = new WarehouseProvider($dataConn);

$EFIByPartID = $CSGDailyDataProvider->getEFIByPartID();
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $warehouseProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$WarehouseByDescription[$record->description] = $record->name;
}

//now mark it as installed item if efi ism
$efiPart = $transferCollection->EFI;
$actionMessage = "";
$adminId = $transferCollection->ByAdmin;

	$installDate = $transferCollection->TransferDate;
	$transferInfo = array();
	$TransferResults = null;
	$resultsCollection = null;
	$transferInfo["WarehouseFromLocation"] = $transferCollection->WarehouseFromLocation;
	$transferInfo["WarehouseToLocation"] = $transferCollection->WarehouseToLocation;
	$transferInfo["PurchaseOrder"] = $transferCollection->PurchaseOrder;
	$transferInfo["TransferDate"] = $installDate;
	$transferInfo["MovementType"] = $transferCollection->MovementType;
	$transferInfo["EFI"] = $efiPart;
	$transferInfo["Qty"] = (int)$transferCollection->Qty;
	$transferInfo["SiteID"] = $siteId;
	$resultsCollection["updateCountInfo"][] = $transferInfo;
	$TransferResults = $productProvider->transferItem($transferInfo,$adminId);
	$resultsCollection["TransferResults"]= $TransferResults;
	$actionMessage = "Transfer complete now updating Warehouse Count";
	
	//Now update the counts
	$updateResults = $warehouseProvider->updateSingleWarehouseCount($TransferResults);
	$resultsCollection["MarkedInstalled"][] = $updateResults;
	$actionMessage .= "\nUpdate of Warehouse Count complete";
$resultsCollection["Message"][] = $actionMessage;
?>