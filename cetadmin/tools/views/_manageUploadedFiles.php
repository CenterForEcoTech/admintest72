<h3>Manage Uploaded Files</h3>
<p>
	<?php if (!$ReadOnlyTrue){?>
		<a href="?nav=edit-file" id="add-file-button" class="button-link">+ Add File</a>
	<?php }?>
</p>
<table id="file-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>File Name</th>
        <th>Mime Type</th>
        <th>Title</th>
        <th>Keywords</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){

        var fileListTable = $("#file-list"),
            apiUrl = "<?php echo $CurrentServer.$adminFolder;?>tools/ApiFileLibrary.php<?php echo $QueryStringApiFileLibrary;?>",
            lastRequestedCriteria = null,
            oTable = fileListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "iDisplayLength": 50,
                "aaSorting": [ [0, "desc"]],
                "aoColumns" :[
                    {
                        "mData": "id",
                        "mRender": function ( data, type, full ) {
                            return '<a href="?nav=edit-file&id='+data+'">'+data+'</a>';
                        },
                        "sWidth": "50px"
                    },
                    {
                        "mData": "name",
                        "sWidth": "150px"
                    },
                    {"mData": "mimeType"},
                    {"mData": "title"},
                    {"mData": "keywords"}
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": lastRequestedCriteria,
                        "success": fnCallback

                    } );
                }
            });
    });
</script>