<?php
error_reporting(1);
ini_set('max_execution_time', 1200); //20 minutes
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);

$BatchSize = 10;

//Get list of files to review
$folderLocationToGet = $siteRoot.$adminFolder.$Config_FTP_CSGSaveInspectionFileLocation;
$folderLocationToSave = $siteRoot.$adminFolder.$Config_FTP_CSGSaveInspectionFileToLocation;

//Get Refused Inspections:
$InspectionsRefused = $CSGDataProvider->getInspectionRefused();
foreach ($InspectionsRefused as $refused){
	$refusedInspections[]=$refused["siteId"];
}

$path = ($path ? $path : $siteRoot.$adminFolder);
include_once($path.'sharepoint/SharePointAPIFiles.php');

foreach ($Views as $ID=>$GUI){
	$ArrayName = "SPRecord".$ID;
	//echo $ArrayName."<hr>";
	$thisFileContents = null;
	$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
	${$ArrayName} = json_decode($thisFileContents,true);
	$thisFileContents = null;
	//echo "<span class='memoryusage'>Processing Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	//ob_flush();
	foreach (${$ArrayName} as $SPData){
		//print_pre($SPData);
		$siteId = $SPData["site_x0020_id"];
		$utility = trim($SPData["utility"]);
		$SPUtilities[$siteId] = $utility;
//		if (!MySQLDate($ColumnData["Insp_Dt"]) && !in_array($SiteID,$refusedInspections) && $SPUtilities[$SiteID]=="Berkshire Gas"){
			
		
	}//end foreach sprecord
	${$ArrayName} = null;
	//echo "<span class='memoryusage'>Completed Sharepoint Data Part ".($ID+1).": Current Peak Memory Usage ".round((memory_get_peak_usage(false)/1024/1024),2)." MB,  Currently Allocating ".round((memory_get_usage()/1024/1024),2)." MB<br></span>";
	//ob_flush();

	//echo "<br>MemoryUsage:".$ArrayName." ".memory_get_usage();
}//end foreach view


include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($folderLocationToGet, false, true);
if (count($files)){
	foreach ($files as $file){
		//only look at csv files
		if (strpos($file,"csv")){
			$fileLocation = str_replace("\\","/",$file);
			$csv_file    = file_get_contents($fileLocation);
			$rows        = explode("\n", $csv_file);
			//put the first row as column headers
			foreach ($rows as $data=>$val){
				$val = preg_replace_callback('/\"(.+?)\"/i', function($match) {
					return str_replace(',', ';', $match[0]);
				}, $val);
				//echo $result;
				$content = explode(",",$val);
	//			echo $data.":".$val."<br>";
				if ($data <1){
					$CSGData["Columns"] = $content;
				}else{
					//ignore blank rows
					if ($content[0]){
						$CSGData["Rows"][] = $content;
					}
				}
			}
			//convert column names to standardized format
			foreach ($CSGData["Columns"] as $Id=>$Column){
				$ConvertColumnName = $Column;
				$CSGData["Columns"][$Id]=trim($ConvertColumnName);
			}

			$ThisYear = date("Y");
			$NextYear = date("Y",strtotime($TodaysDate.'+1 year'));
			//update row information with column name so that they can be matched to CSGDailyDataRecord
			foreach ($CSGData["Rows"] as $ID=>$value){
				foreach ($value as $colID=>$rowVal){
					$cleanRowVal = str_replace(';',',',$rowVal);
					$cleanRowVal = str_replace(array("\n", "\t", "\r","\""),'',$cleanRowVal);
					//check if date
					if ($CSGData["Columns"][$colID] == "PAYDT" || $CSGData["Columns"][$colID] == "Insp_Dt"){$cleanRowVal = date("Y-m-d",strtotime($cleanRowVal));}
					if ($cleanRowVal == "1969-12-31"){$cleanRowVal = "0000-00-00";}
					if ($CSGData["Columns"][$colID] == "crew"){
						$cleanRowVal = strtoupper($cleanRowVal);
					}
					$CSGDataArray[$ID][$CSGData["Columns"][$colID]]=$cleanRowVal;
					$CSV[$ID][]=$cleanRowVal;
				}
			}
			
			$fileToSave = $folderLocationToSave.'/report.csv';
			$f = @fopen($fileToSave, "r+");
			if ($f !== false) {
   				ftruncate($f, 0);
				fclose($f);
			}
			//print_pre($CSV);
			convert_to_csv($CSV, $fileToSave, ',');
			$results = $CSGDataProvider->loadExternalReportData($fileToSave,"csg_hub_inspectionreport_data");
			if ($results["success"]){
				//remove the file so there should only be one schedulereport file in the directory
				unlink($fileLocation);
				echo count($CSV)." records successfully analyzed<br>";
				//now create analysis of dates
				foreach ($CSGDataArray as $ID=>$ColumnData){
					$InspectionData = new stdClass();
					$SiteID  = $ColumnData["SITEID"];
					if (!MySQLDate($ColumnData["Insp_Dt"]) && !in_array($SiteID,$refusedInspections) && $SPUtilities[$SiteID]=="Berkshire Gas"){
						$InspectionData->LastName = $ColumnData["Name_Last"];
						$InspectionData->FirstName = $ColumnData["Name_First"];
						$InspectionData->Address = $ColumnData["Address"];
						$InspectionData->City = $ColumnData["City"];
						$InspectionData->Cell = $ColumnData["Cell"];
						$InspectionData->Home = $ColumnData["Home"];
						$InspectionData->Email = (strpos($ColumnData["Email"],"@") ? $ColumnData["Email"] : "");
						$InspectionData->ContractorName = $ColumnData["NAME"];
						$InspectionData->ContractorRank = $ColumnData["Contractor_Rank"]; //L,M,H (low medium high)
						$InspectionData->PriorCalls = ($ColumnData["Num_of_Calls"] ? $ColumnData["Num_of_Calls"] : 0);
						$InspectionData->Campaign = $ColumnData["Campaign"];
						$InspectionReport[$SiteID] = $InspectionData;
					}//end if Not Inspection Date
				}
				ksort($InspectionReport);
			}
			
		}//end if csv
	}
}else{	
	echo "No Inspection Schedule Reports found.  Use the File Library to upload a new inspection schedule report";
	//echo "<br><br>Redirecting to <a href=\"".$CurrentServer.$adminFolder."forecast/?nav=forecast-apptanalysis\">Full Monthly Report</a> in 3 seconds";
}
?>
<style>
	th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>
<script>
	$(document).ready(function() {
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"scrollCollapse": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": true,
			"iDisplayLength": 20,
		});
		
		$(".refusedButton").on('click',function(){
			var $this = $(this);
				siteId = $this.attr('data-siteid'),
				thisClass = "."+siteId,
				data = {};
				data["action"] = "mark_refused";
				data["siteId"] = siteId;
				$('#refuseResults').hide();
				$.ajax({
					url: "ApiInspectionManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						console.log(data);
						$(thisClass).hide();
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#refuseResults').show().html(message).addClass("alert");
					}
				});
		});
		$("#utilitySelector").on('change',function(){
			var thisVal = $(this).val();
			location.href='<?php echo $CurrentServer.$adminFolder;?>ghs/?nav=inspection-schedule&utility='+thisVal;
		});
		
	});
</script>

<?php if (count($InspectionReport)){?>
	Showing Utility <select id="utilitySelector"><option value="Berkshire Gas"<?php echo ($utilitySelected == "Berkshire Gas" ? " selected" : "");?>>Berkshire Gas</option><option value="WMECO"<?php echo ($utilitySelected == "WMECO" ? " selected" : "");?>>Eversource/WMECO</option></select>
	<h3>Inspection Schedule Analysis</h3>
	<div id="refuseResults"></div>
			<table class="reportResults display" cellspacing="0">
				<thead>
					<tr>
						<th>Status</th>
						<th>SiteID</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Address</th>
						<th>City</th>
						<th>Phone</th>
						<th>Contractor</th>
						<th>Rank</th>
						<th>Prior Calls</th>
						<th>Campaign</th>
						<th>Utility</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($InspectionReport as $SiteID=>$inspectionData){
							echo "<tr class='".$SiteID."'>
									<td><button style='font-size:10pt;' class='refusedButton' data-siteid='".$SiteID."'>Refused</button></td>
									<td>".$SiteID."</td>
									<td>".$inspectionData->LastName."</td>
									<td>".$inspectionData->FirstName."</td>
									<td>".$inspectionData->Address."</td>
									<td>".$inspectionData->City."</td>
									<td>".($inspectionData->Cell ? $inspectionData->Cell." (cell)<br>" : "&nbsp;").($inspectionData->Home ? $inspectionData->Home." (home)" : "&nbsp;")."</td>
									<td>".$inspectionData->ContractorName."</td>
									<td>".($inspectionData->ContractorRank != "N/A" ? $inspectionData->ContractorRank : "&nbsp;")."</td>
									<td>".$inspectionData->PriorCalls."</td>
									<td>".($inspectionData->Campaign != "NONE" ? $inspectionData->Campaign : "&nbsp;")."</td>
									<td>Berkshire Gas</td>
								 </tr>";
						}
					?>
				</tbody>
			</table>
<?php
}// end if count ?>