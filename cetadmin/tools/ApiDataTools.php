<?php
error_reporting(1);
ini_set('max_execution_time', 2400); //40 minutes

include_once("../_config.php");
include_once($siteRoot."functions.php");
include_once($siteRoot."assert_is_ajax.php");

$currentRequest = new Request();
$parameters = $currentRequest->parameters;
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		if ($parameters["action"] == "CleanScheduleReport"){
//			$resultCount = $CSGDataProvider->cleanReportData();
			$scriptLocation = "/usr/local/php55/bin/php ".$siteRoot.$adminFolder."tools/toolsCleanScheduleData.php";
			exec($scriptLocation." >/dev/null &", $output, $return);

			// Return will return non-zero upon an error
			if (!$return) {
				$result = "Duplicate records removal processing in the background.";
			} else {
				$result = "There was a problem removing the duplicates.  Please let Yosh know";
			}
		}
		$results = $result;
		header("HTTP/1.0 201 Created");
        break;
    case "POST":
		$rawData = json_decode($currentRequest->rawData);
		$action = $rawData->action;
		if ($action == "inventoryCountUpdate"){
			$startDate = $rawData->startDate;
			$endDate = $rawData->endDate;
			$transferCollection = $rawData->TransferCollection;
			include_once('views/_updateBulbReportMovement.php');
			$results = $resultsCollection;
		}elseif ($action == "movementHistoryInstall"){
			$transferCollection = $rawData->TransferCollection;
			include_once('views/_updateMovementHistoryMovement.php');
			$results = $resultsCollection;
		}else{
			$results = "no action";
		}
        break;
    case "PUT":
		echo "this is PUT";
        break;
}
output_json($results);
die();
?>