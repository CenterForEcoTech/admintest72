<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Tools)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Tools))){?>
<!--			<li><a id="ftp-getfiles" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=ftp-getfiles" class="button-link">FTP Get CSG Data Files</a></li>
			<li><a id="update_onhandminimums" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=update_onhandminimums" class="button-link">Update On Hand Minimums</a></li>
			<li><a id="update_bulbreport" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=update_bulbreport" class="button-link">Bulb Report</a></li>
			<li><a id="update_MovementHistory" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=update_MovementHistory" class="button-link">Install Movement History</a></li>
			<li><a id="update_schedulereport" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=update_schedulereport" class="button-link">Schedule Report</a></li>
			<li><a id="table_viewer" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=table_viewer" class="button-link">Table Viewer</a></li>
		-->
			<li><a id="cri_report" href="<?php echo $CurrentServer.$adminFolder;?>residential/?nav=report-BGASExtractProcessor" class="button-link">BGAS Extract Processor</a> (moved to residential)</li>
			<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Tools_Admin))){?>
				<li><a id="tools-error-logs" href="<?php echo $CurrentServer.$adminFolder;?>monitoring" class="button-link">Error Logs</a></li>
				<li><a id="tools-error-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=tools-error-testing&ER=<?php echo $_GET['ER'];?>" class="button-link">Error Testing</a></li>
			<?php }?>
			<li class="separator">&nbsp;</li>
			<li style="display:none;"><a id="edit-file" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=edit-file" class="button-link">Upload File</a></li>
<!--	
        <li><a id="file-library" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=file-library" class="button-link">File Library</a></li>
-->
			<div style="display:none;">
				<li><a id="email-log-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=email-log-testing" class="button-link">Email Log Tester</a></li>

				<li class="separator">&nbsp;</li>
				<li><a id="charity-search-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=charity-search-testing" class="button-link">Charity Search Tester</a></li>
				<li><a id="iframe-embed-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=iframe-embed-testing" class="button-link">iFrame Embed Tester</a></li>

				<li class="separator">&nbsp;</li>
				<li><a id="follower-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=follower-testing" class="button-link">Facebook/Twitter follower Tester</a></li>

				<li class="separator">&nbsp;</li>
				<li><a id="billcom-testing" href="<?php echo $CurrentServer.$adminFolder;?>tools/?nav=billcom-testing" class="button-link">Bill.Com Health</a></li>
			</div>
		<?php } ?>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            } else if (currentPage == "billcom-clientapi-login" || currentPage == "billcom-billcomv2_api-login"){
                $("#billcom-testing").addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>