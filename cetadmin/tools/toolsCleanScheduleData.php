<?php
error_reporting(1);
ini_set('max_execution_time', 600);

include_once("../_config.php");
include_once($siteRoot."functions.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."CSGDataProvider.php");
$CSGDataProvider = new CSGDataProvider($dataConn);
$resultCount = $CSGDataProvider->cleanReportData();
$result = $resultCount." duplicate records removed";
$results = $result;
header("HTTP/1.0 201 Created");
output_json($results);
?>