<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");
$pageTitle = "Admin - Tools";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
$fileuploadpage=false;

if ($nav){
    switch ($nav){
        case "ftp-getfiles":
			$displayPage = "_ftpGetFiles.php";
            break;
        case "file-library":
            $displayPage = "views/_manageUploadedFiles.php";
            break;
        case "edit-file":
			$nav = "file-library";
			$fileuploadpage=true;
			$uploadFileLocation = $adminFolder."FTPFiles/CSG/BulbReport";
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <!-- TODO: could simplify here: $customCSS = '<link ... />' -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $modelTemplate = "model/_editFileModel.php";
            $displayPage = "views/_editFile.php";
            break;
        case "update_onhandminimums":
			$displayPage = "_updateOnHandMinimums.php";
            break;
        case "update_inspectionreport":
			$displayPage = "views/_updateInspectionReport.php";
            break;
        case "update_bulbreport":
			$displayPage = "views/_updateBulbReport.php";
            break;
        case "update_MovementHistory":
			$displayPage = "views/_updateMovementHistory.php";
            break;
        case "update_schedulereport":
			$displayPage = "views/_updateScheduleReport.php";
            break;
        case "cri_report":
			$displayPage = "views/_createCRIData.php";
            break;
		case "table_viewer":
			$displayPage = "_tableViewer.php";
			break;
        case "tools-error-testing":
			$displayPage = "_toolsErrorTesting.php";
            break;
        case "email-log-testing":
            $displayPage = "_emailLogTesting.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Tools Administration</h1>';

include_once("_toolsMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>
