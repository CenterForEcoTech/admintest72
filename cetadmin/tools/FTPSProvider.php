<?php
Class FTPClient
{
    // *** Class variables
 
    public function __construct() { }
	
	private $connectionId;
	private $loginOk = false;
	private $messageArray = array();

	private function logMessage($message)
	{
		$this->messageArray[] = $message;
	}
	
	public function getMessages()
	{
		return $this->messageArray;
	}	
	
	public function connect ($server, $ftpUser, $ftpPassword, $isPassive = false)
	{
		// *** Set up basic connection
		$this->connectionId = ftp_connect($server);
	 
		// *** Login with username and password
		$loginResult = ftp_login($this->connectionId, $ftpUser, $ftpPassword);
	 
		// *** Sets passive mode on/off (default off)
		ftp_pasv($this->connectionId, $isPassive);
	 
		// *** Check connection
		if ((!$this->connectionId) || (!$loginResult)) {
			$this->logMessage('FTP connection has failed!');
			$this->logMessage('Attempted to connect to ' . $server . ' for user ' . $ftpUser, true);
			return false;
		} else {
			$this->logMessage('Connected to ' . $server . ', for user ' . $ftpUser);
			$this->loginOk = true;
			return true;
		}
	}	

	public function makeDir($directory)
	{
		// *** If creating a directory is successful...
		if (ftp_mkdir($this->connectionId, $directory)) {
	 
			$this->logMessage('Directory "' . $directory . '" created successfully');
			return true;
	 
		} else {
	 
			// *** ...Else, FAIL.
			$this->logMessage('Failed creating directory "' . $directory . '"');
			return false;
		}
	}	
	
	public function uploadFile ($fileFrom, $fileTo)
	{
		// *** Set the transfer mode
		$asciiArray = array('txt', 'csv');
		$extension = end(explode('.', $fileFrom));
		if (in_array($extension, $asciiArray)) {
			$mode = FTP_ASCII;      
		} else {
			$mode = FTP_BINARY;
		}
	 
		// *** Upload the file
		$upload = ftp_put($this->connectionId, $fileTo, $fileFrom, $mode);
	 
		// *** Check upload status
		if (!$upload) {
	 
				$this->logMessage('FTP upload has failed!');
				return false;
	 
			} else {
				$this->logMessage('Uploaded "' . $fileFrom . '" as "' . $fileTo);
				return true;
			}
	}	
	
	public function changeDir($directory)
	{
		if (ftp_chdir($this->connectionId, $directory)) {
			$this->logMessage('Current directory is now: ' . ftp_pwd($this->connectionId));
			return true;
		} else { 
			$this->logMessage('Couldn\'t change directory');
			return false;
		}
	}	

	public function getDirListing($directory = '.', $parameters = '-la')
	{
		// get contents of the current directory
		$contentsArray = ftp_nlist($this->connectionId, $parameters . '  ' . $directory);
		$this->logMessage('ftp_nlist executed for '.$directory);
	 
		return $contentsArray;
	}	
	
	public function downloadFile ($fileFrom, $fileTo)
	{
	 
		// *** Set the transfer mode
		$asciiArray = array('txt', 'csv');
		$extension = end(explode('.', $fileFrom));
		if (in_array($extension, $asciiArray)) {
			$mode = FTP_ASCII;      
		} else {
			$mode = FTP_BINARY;
		}
		// try to download $remote_file and save it to $handle
		if (ftp_get($this->connectionId, $fileTo, $fileFrom, $mode, 0)) {
	 
			return true;
			$this->logMessage(' file "' . $fileTo . '" successfully downloaded');
		} else {
	 
			return false;
			$this->logMessage('There was an error downloading file "' . $fileFrom . '" to "' . $fileTo . '"');
		}
	}	
	
}


/**
 * FTP with Implicit SSL/TLS Class
 *
 * Simple wrapper for cURL functions to transfer an ASCII file over FTP with implicit SSL/TLS
 *
 * @category    Class
 * @author      Max Rice
 * @since       1.0
 */

class FTP_Implicit_SSL {

	/** @var resource cURL resource handle */
	private $curl_handle;

	/** @var string cURL URL for upload */
	private $url;

	/**
	 * Connect to FTP server over Implicit SSL/TLS
	 *
	 *
	 * @access public
	 * @since 1.0
	 * @param string $username
	 * @param string $password
	 * @param string $server
	 * @param string $server_type
	 * @param int $port
	 * @param string $initial_path
	 * @param bool $passive_mode
	 * @throws Exception - blank username / password / port
	 * @return \FTP_Implicit_SSL
	 */
	public function __construct( $username, $password, $server, $server_type = "ftps", $port = 990,$initial_path = '', $passive_mode = false ) {
		$this->user = $username;
		$this->pass = $password;
		$this->path = $initial_path;
		$this->servertype = $server_type;

		// check for blank username
		if ( ! $username )
			throw new Exception( 'FTP Username is blank.' );

		// don't check for blank password (highly-questionable use case, but still)

		// check for blank server
		if ( ! $server )
			throw new Exception( 'FTP Server is blank.' );

		// check for blank port
		if ( ! $port )
			throw new Exception ( 'FTP Port is blank.', WC_XML_Suite::$text_domain );

		// set host/initial path
		//$this->url = "ftps://{$server}/{$initial_path}";
		$this->url = $server_type."://{$server}/{$initial_path}";

		// setup connection
		$this->curl_handle = curl_init();

		// check for successful connection
		if ( ! $this->curl_handle )
			throw new Exception( 'Could not initialize cURL.' );	

		// connection options
		$options = array(
			CURLOPT_USERPWD => $username . ':' . $password,
			CURLOPT_SSL_VERIFYPEER => false, // don't verify SSL
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_FTP_SSL => CURLFTPSSL_ALL, // require SSL For both control and data connections
			CURLOPT_FTPSSLAUTH => CURLFTPAUTH_DEFAULT, // let cURL choose the FTP authentication method (either SSL or TLS)
			CURLOPT_PORT => $port,
			CURLOPT_TIMEOUT => 60,
		);		
		
		// cURL FTP enables passive mode by default, so disable it by enabling the PORT command and allowing cURL to select the IP address for the data connection
		if ( ! $passive_mode )
		$options[ CURLOPT_FTPPORT ] = '-';

		// set connection options, use foreach so useful errors can be caught instead of a generic "cannot set options" error with curl_setopt_array()
		foreach ( $options as $option_name => $option_value ) {
			if ( ! curl_setopt( $this->curl_handle, $option_name, $option_value ) )
				throw new Exception( sprintf( 'Could not set cURL option: %s', $option_name ) );
		}

	}		
	/**
	 * Write file into temporary memory and upload stream to remote file
	 *
	 * @access public
	 * @since 1.0
	 * @param string $file_name - remote file name to create
	 * @param string $file - file content to upload
	 * @throws Exception - Open remote file failure or write data failure
	 */


	public function upload( $file_name, $file ) {
	
		curl_setopt( $this->curl_handle, CURLOPT_UPLOAD, true);
		// set file name
		if ( ! curl_setopt( $this->curl_handle, CURLOPT_URL, $this->url . $file_name ))
			throw new Exception ( "Could not set cURL file name: $file_name" );

		// open memory stream for writing
		$stream = fopen( 'php://temp', 'w+' );

		// check for valid stream handle
		if ( ! $stream )
			throw new Exception( 'Could not open php://temp for writing.' );

		// write file into the temporary stream
		fwrite( $stream, $file );

		// rewind the stream pointer
		rewind( $stream );

		// set the file to be uploaded
		if ( ! curl_setopt( $this->curl_handle, CURLOPT_INFILE, $stream ) )
			throw new Exception( "Could not load file $file_name" );

		// upload file
		if ( ! curl_exec( $this->curl_handle ) )
			throw new Exception( sprintf( 'Could not upload file. cURL Error: [%s] - %s', curl_errno( $this->curl_handle ), curl_error( $this->curl_handle ) ) );

		// close the stream handle
		fclose( $stream );

		return TRUE;
	}

	/**
	* Download file from FTPS default directory
	*
	* @param $file_name
	* @return string
	*/

	public function download($file_name){
		curl_setopt( $this->curl_handle, CURLOPT_URL, $this->url . $file_name);
		curl_setopt( $this->curl_handle, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($this->curl_handle);
		if( strlen($result) ){
			return $result;
		} else {
			return 'nothing';
		}
	}
	
	/**
	* Returns curl handle in case you want to capture additional information about the connection
	*
	* @return resource
	*/
	public function getHandle(){
		return $this->curl_handle;
	}	

	/**
	* @return array array of file names
	* @throws Exception
	*/
	public function ftplist(){
		if ( ! curl_setopt( $this->curl_handle, CURLOPT_URL, $this->url))
			throw new Exception ( "Could not set cURL directory: $this->url" );

		curl_setopt( $this->curl_handle,CURLOPT_FTPLISTONLY,1);
		curl_setopt( $this->curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl_handle);
		print_pre($result);
		$files = explode("\n",trim($result));
		if( count($files) ){
			return $files;
		} else {
			return array();
		}
	}

	/**
	* Delete specified file
	*
	* @param $file_name
	* @return string
	* @throws Exception
	*/
	public function delete($file_name){

		if ( ! curl_setopt( $this->curl_handle, CURLOPT_URL, $this->url))
			throw new Exception ( "Could not set cURL directory: $this->url" );

		if( !in_array($file_name, $this->ftplist()) ){
			throw new Exception ( $file_name . ' not found' );
		}

		curl_setopt( $this->curl_handle,CURLOPT_QUOTE,array('DELE ' . $this->path . $file_name ));
		curl_setopt( $this->curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl_handle);
		//print_r( curl_getinfo($this->curl_handle) ) ;
		$files = explode("\n",trim($result));

		if( ! in_array( $file_name, $files) ){
			return 'SUCCESS';
		} else {
			return 'FAILED';
		}
	}

	/**
	* Attempt to close cURL handle
	* Note - errors suppressed here as they are not useful
	*
	* @access public
	* @since 1.0
	*/
	public function __destruct() {
		@curl_close( $this->curl_handle );
	}
}

class SFTPConnection
{
    private $connection;
    private $sftp;

    public function __construct($host, $port=22)
    {
        $this->connection = @ssh2_connect($host, $port);
		echo $host;
        if (! $this->connection)
            throw new Exception("Could not connect to $host on port $port.");
    }

    public function login($username, $password)
    {
        if (! @ssh2_auth_password($this->connection, $username, $password))
            throw new Exception("Could not authenticate with username $username " . "and password $password.");
        $this->sftp = @ssh2_sftp($this->connection);
        if (! $this->sftp)
            throw new Exception("Could not initialize SFTP subsystem.");
    }

    public function uploadFile($local_file, $remote_file)
    {
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp$remote_file", 'w');
        if (! $stream)
            throw new Exception("Could not open file: $remote_file");
        $data_to_send = @file_get_contents($local_file);
        if ($data_to_send === false)
            throw new Exception("Could not open local file: $local_file.");
        if (@fwrite($stream, $data_to_send) === false)
            throw new Exception("Could not send data from file: $local_file.");
        @fclose($stream);
    }
    
        function scanFilesystem($remote_file) {
              $sftp = $this->sftp;
            $dir = "ssh2.sftp://$sftp$remote_file";  
              $tempArray = array();
            $handle = opendir($dir);
          // List all the files
            while (false !== ($file = readdir($handle))) {
            if (substr("$file", 0, 1) != "."){
              if(is_dir($file)){
//                $tempArray[$file] = $this->scanFilesystem("$dir/$file");
               } else {
                 $tempArray[]=$file;
               }
             }
            }
           closedir($handle);
          return $tempArray;
        }    

    public function receiveFile($remote_file, $local_file)
    {
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp$remote_file", 'r');
        if (! $stream)
            throw new Exception("Could not open file: $remote_file");
        $contents = fread($stream, filesize("ssh2.sftp://$sftp$remote_file"));            
        file_put_contents ($local_file, $contents);
        @fclose($stream);
    }
        
    public function deleteFile($remote_file){
      $sftp = $this->sftp;
      unlink("ssh2.sftp://$sftp$remote_file");
    }
}

?>