<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxCodes" ).combobox({
				select: function(event, ui){
					<?php if (!$codesComboBoxFunction){?>
					window.location = "?nav=manage-revenueCodes&SelectedRevenueCode="+$(ui.item).val();
					<?php }else{
						echo $codesComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($revenueCodesComboBoxDivColumns ? $revenueCodesComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$revenueCodesComboBoxHideLabel){?>
				<label>Start typing code name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxCodes parameters" id="SelectedCode">
				<option value="">Select one...</option>
				<?php 
					foreach ($RevenueCodes as $code=>$name){
						echo "<option value=\"".$code."\"".($SelectedRevenueCode==$code ? " selected" : "").">".$name." </option>";
					}
				?>
			  </select>
			</div>
		</div>