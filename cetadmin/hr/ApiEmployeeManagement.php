<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HREmployeeProvider.php");
include_once($dbProviderFolder."HRTrainingsProvider.php");
include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'employee_update'){
			$response = $hrEmployeeProvider->update($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'employee_add'){
			$response = $hrEmployeeProvider->add($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'location_update'){
			$location_old = $newRecord->OfficeLocationSrc;
			$location_new = $newRecord->OfficeLocationVal;
			$response = $hrEmployeeProvider->updateOfficeLocations($location_old,$location_new);
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'Insert_TrainingMatrix'){
				$action = $newRecord->action;
				$items = $newRecord->items;
				$employeeId = $newRecord->employeeId;
				/*
				$responseCollection[] = $action.$employeeId.$items;
				foreach ($items as $displayID=>$id){
					//$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,0, getAdminId());
					$responseCollection[] = "Add TrainingID ".$displayID."to EmployeeID.".$employeeId;
				}
*/				
			$results = 	$action;
			header("HTTP/1.0 201 Created");
			/*
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
			*/
		}else{
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,($displayID+1), getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,0, getAdminId());
					}
				}
				if ($action == 'Insert_TrainingMatrix'){
					if ($item->employeeId){
						$record->employeeId = $item->employeeId;
						$record->employeeFullName= $item->employeeFullName;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						foreach ($items as $displayID=>$id){
							$record->trainingId = $id;
							$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
							$responseCollection[] = $addMatrixResponse;
							if (count($responseCollection)){
								$responseCollection[0] = "Added";
							}
							//check for completed status to set expiration date
							if ($record->status == "Completed"){
								$criteria = new stdClass();
								$criteria->id = $id;
								$trainingRecordCollection = $hrTrainingsProvider->get($criteria);
								$trainingRecord = $trainingRecordCollection->collection;
								$isAnnuallyRequired = $trainingRecord[0]->isAnnuallyRequired;
								$trainingName = $trainingRecord[0]->name;
									
								//check to see if TrainingID is AnnuallyRecurring
								if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
									$employeeId = $record->employeeId;
									$employeeFullName = $record->employeeFullName;
									$activityRecord = new stdClass();
									$activityRecord->matrixId = $addMatrixResponse->insertedId;
									$activityRecord->employeeId = $employeeId;
									$activityRecord->employeeName = $employeeFullName;
									$activityRecord->trainingId = $id;
									$activityRecord->trainingName = $trainingName;
									$activityRecord->status = "Completed";
									$activityRecord->expirationDate = date("m/d/Y",strtotime($record->affectiveDate." +1 year"));
									//$completedRecord->addedByAdmin;
									$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
									//$responseCollection[0] = $activityRecord;							
								}
							}
							
						}
					}
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						
						//check for completed status to set expiration date
						if ($record->status == "Completed"){
								$criteria = new stdClass();
								$criteria->id = $item->trainingId;
								$trainingRecordCollection = $hrTrainingsProvider->get($criteria);
								$trainingRecord = $trainingRecordCollection->collection;
								$isAnnuallyRequired = $trainingRecord[0]->isAnnuallyRequired;
						}

						
						foreach ($items as $displayID=>$id){
							$record->employeeInfo = $id;
							$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
							$responseCollection[] = $addMatrixResponse;
							if (count($responseCollection)){
								$responseCollection[0] = "Added";
							}
								
							//check to see if TrainingID is AnnuallyRecurring
							if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
								$employeeInfo = explode("_",$id);
								$employeeId = $employeeInfo[0];
								$employeeFullName = $employeeInfo[1];
								$activityRecord = new stdClass();
								$activityRecord->matrixId = $addMatrixResponse->insertedId;
								$activityRecord->employeeId = $employeeId;
								$activityRecord->employeeName = $employeeFullName;
								$activityRecord->trainingId = $record->trainingId;
								$activityRecord->trainingName = $record->trainingName;
								$activityRecord->status = "Completed";
								$activityRecord->expirationDate = date("m/d/Y",strtotime($record->affectiveDate." +1 year"));
								//$completedRecord->addedByAdmin;
								$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
							}
						}
					}
				}
				if ($action == 'Insert_TrainingMatrixByDepartment'){
					if ($item->departmentName){
						$criteria->showAll = true;
						$criteria->noLimit = true;
						$paginationResult = $hrTrainingsProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						$categoryNames = array();
						//get Training Info
						foreach ($resultArray as $result=>$record){
							$TrainingsByID[$record->id] = $record;
						}
						
						//get department Employees
						$criteria = new stdClass();
						$criteria->showAll = false;
						$criteria->isManager = false;
						$criteria->department = urldecode($item->departmentName);
						$paginationResult = $hrEmployeeProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						
						//loop through all the trainings
						foreach ($items as $displayID=>$Trainingid){
							$record = new stdClass();
							$record->trainingId = $TrainingsByID[$Trainingid]->id;
							$record->trainingName= $TrainingsByID[$Trainingid]->name;
							$record->trainingDisplayOrderId= $TrainingsByID[$Trainingid]->displayOrderId;
							$record->affectiveDate = $item->affectiveDate;
							$record->status = $item->status;
							$isAnnuallyRequired = $TrainingsByID[$Trainingid]->isAnnuallyRequired;
							//loop through all employees
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								$responseCollection[] = $addMatrixResponse;
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
								if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
									$employeeId = $employeeObj->id;
									$employeeFullName = $employeeObj->fullName;
									$activityRecord = new stdClass();
									$activityRecord->matrixId = $addMatrixResponse->insertedId;
									$activityRecord->employeeId = $employeeId;
									$activityRecord->employeeName = $employeeFullName;
									$activityRecord->trainingId = $record->trainingId;
									$activityRecord->trainingName = $record->trainingName;
									$activityRecord->status = "Completed";
									$activityRecord->expirationDate = date("m/d/Y",strtotime($record->affectiveDate." +1 year"));
									//$completedRecord->addedByAdmin;
									$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
								}
							}
						}
					}
					
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						//check for completed status to set expiration date
						if ($item->status == "Completed"){
								$criteria = new stdClass();
								$criteria->id = $item->trainingId;
								$trainingRecordCollection = $hrTrainingsProvider->get($criteria);
								$trainingRecord = $trainingRecordCollection->collection;
								$isAnnuallyRequired = $trainingRecord[0]->isAnnuallyRequired;
						}

						foreach ($items as $displayID=>$department){
							$criteria = new stdClass();
							$criteria->showAll = false;
							$criteria->isManager = false;
							$criteria->department = $department;
							$paginationResult = $hrEmployeeProvider->get($criteria);
							$resultArray = $paginationResult->collection;
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								$responseCollection[] = $addMatrixResponse;
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
								
								//check to see if TrainingID is AnnuallyRecurring
								if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
									$employeeId = $employeeObj->id;
									$employeeFullName = $employeeObj->fullName;
									$activityRecord = new stdClass();
									$activityRecord->matrixId = $addMatrixResponse->insertedId;
									$activityRecord->employeeId = $employeeId;
									$activityRecord->employeeName = $employeeFullName;
									$activityRecord->trainingId = $item->trainingId;
									$activityRecord->trainingName = $item->trainingName;
									$activityRecord->status = "Completed";
									$activityRecord->expirationDate = date("m/d/Y",strtotime($item->affectiveDate." +1 year"));
									//$completedRecord->addedByAdmin;
									$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
								}
								
								
								
							}
						}
					}
				}
				if ($action == 'Insert_TrainingMatrixByTitle'){
					if ($item->titleId){
						$criteria = new stdClass();
						$criteria->showAll = true;
						$criteria->noLimit = true;
						$paginationResult = $hrEmployeeDepartmentsProvider->getTitles($criteria);
						$resultArray = $paginationResult->collection;
						$categoryNames = array();
						//get Training Info
						foreach ($resultArray as $result=>$record){
							$TrainingsByID[$record->id] = $record;
						}
						
						//get department Employees
						$criteria = new stdClass();
						$criteria->showAll = false;
						$criteria->isManager = false;
						$criteria->titleId = urldecode($item->titleId);
						$paginationResult = $hrEmployeeProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						
						//loop through all the trainings
						foreach ($items as $displayID=>$Trainingid){
							$record = new stdClass();
							$record->trainingId = $Trainingid;
							$record->trainingName= $TrainingsByID[$Trainingid]->name;
							$record->trainingDisplayOrderId= $TrainingsByID[$Trainingid]->displayOrderId;
							$record->affectiveDate = $item->affectiveDate;
							$record->status = $item->status;
							$isAnnuallyRequired = $TrainingsByID[$Trainingid]->isAnnuallyRequired;
							//loop through all employees
							if (count($resultArray)){
								foreach ($resultArray as $employeeObj){
									$record->employeeId = $employeeObj->id;
									$record->employeeFullName = $employeeObj->fullName;
									$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
									$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
									$responseCollection[] = $addMatrixResponse;
									if (count($responseCollection)){
										$responseCollection[0] = "Added";
									}
									if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
										$employeeId = $employeeObj->id;
										$employeeFullName = $employeeObj->fullName;
										$activityRecord = new stdClass();
										$activityRecord->matrixId = $addMatrixResponse->insertedId;
										$activityRecord->employeeId = $employeeId;
										$activityRecord->employeeName = $employeeFullName;
										$activityRecord->trainingId = $record->trainingId;
										$activityRecord->trainingName = $record->trainingName;
										$activityRecord->status = "Completed";
										$activityRecord->expirationDate = date("m/d/Y",strtotime($record->affectiveDate." +1 year"));
										//$completedRecord->addedByAdmin;
										$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
									}
								}
							}else{
								$responseCollection[0] = "Added";
							}
							//also insert into hr_training_matrix_bytype
							//ON Duplicate will set status to Active
							$insertRecord = new stdClass();
							$insertRecord->trainingId = $Trainingid;
							$insertRecord->type = "Title";
							$insertRecord->typeId = $item->titleId;
							$trainingRecordResults = $hrTrainingsProvider->insertTrainingMatrixByType($insertRecord);
						}
					}
					
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						//check for completed status to set expiration date
						if ($item->status == "Completed"){
								$criteria = new stdClass();
								$criteria->id = $item->trainingId;
								$trainingRecordCollection = $hrTrainingsProvider->get($criteria);
								$trainingRecord = $trainingRecordCollection->collection;
								$isAnnuallyRequired = $trainingRecord[0]->isAnnuallyRequired;
						}

						foreach ($items as $displayID=>$titleId){
							$criteria = new stdClass();
							$criteria->showAll = false;
							$criteria->isManager = false;
							$criteria->titleId = $titleId;
							$paginationResult = $hrEmployeeProvider->get($criteria);
							$resultArray = $paginationResult->collection;
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								$responseCollection[] = $addMatrixResponse;
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
								
								//check to see if TrainingID is AnnuallyRecurring
								if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
									$employeeId = $employeeObj->id;
									$employeeFullName = $employeeObj->fullName;
									$activityRecord = new stdClass();
									$activityRecord->matrixId = $addMatrixResponse->insertedId;
									$activityRecord->employeeId = $employeeId;
									$activityRecord->employeeName = $employeeFullName;
									$activityRecord->trainingId = $item->trainingId;
									$activityRecord->trainingName = $item->trainingName;
									$activityRecord->status = "Completed";
									$activityRecord->expirationDate = date("m/d/Y",strtotime($item->affectiveDate." +1 year"));
									//$completedRecord->addedByAdmin;
									$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
								}
								
								
								
							}
						}
					}
				}
				if ($action == 'Insert_TrainingMatrixByCategory'){
					if ($item->categoryId){
						$criteria = new stdClass();
						$criteria->showAll = true;
						$criteria->noLimit = true;
						$paginationResult = $hrEmployeeDepartmentsProvider->getCategories($criteria);
						$resultArray = $paginationResult->collection;
						$categoryNames = array();
						//get Training Info
						foreach ($resultArray as $result=>$record){
							$TrainingsByID[$record->id] = $record;
						}
						
						//get department Employees
						$criteria = new stdClass();
						$criteria->showAll = false;
						$criteria->isManager = false;
						$criteria->categoryId = urldecode($item->categoryId);
						$paginationResult = $hrEmployeeProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						
						//loop through all the trainings
						foreach ($items as $displayID=>$Trainingid){
							$record = new stdClass();
							$record->trainingId = $Trainingid;
							$record->trainingName= $TrainingsByID[$Trainingid]->name;
							$record->trainingDisplayOrderId= $TrainingsByID[$Trainingid]->displayOrderId;
							$record->affectiveDate = $item->affectiveDate;
							$record->status = $item->status;
							$isAnnuallyRequired = $TrainingsByID[$Trainingid]->isAnnuallyRequired;
							//loop through all employees
							if (count($resultArray)){
								foreach ($resultArray as $employeeObj){
									$record->employeeId = $employeeObj->id;
									$record->employeeFullName = $employeeObj->fullName;
									$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
									$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
									$responseCollection[] = $addMatrixResponse;
									if (count($responseCollection)){
										$responseCollection[0] = "Added";
									}
									if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
										$employeeId = $employeeObj->id;
										$employeeFullName = $employeeObj->fullName;
										$activityRecord = new stdClass();
										$activityRecord->matrixId = $addMatrixResponse->insertedId;
										$activityRecord->employeeId = $employeeId;
										$activityRecord->employeeName = $employeeFullName;
										$activityRecord->trainingId = $record->trainingId;
										$activityRecord->trainingName = $record->trainingName;
										$activityRecord->status = "Completed";
										$activityRecord->expirationDate = date("m/d/Y",strtotime($record->affectiveDate." +1 year"));
										//$completedRecord->addedByAdmin;
										$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
									}
								}
							}else{
								$responseCollection[0] = "Added";
							}
							//also insert into hr_training_matrix_bytype
							//ON Duplicate will set status to Active
							$insertRecord = new stdClass();
							$insertRecord->trainingId = $Trainingid;
							$insertRecord->type = "Category";
							$insertRecord->typeId = $item->categoryId;
							$trainingRecordResults = $hrTrainingsProvider->insertTrainingMatrixByType($insertRecord);
						}
					}
					
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						//check for completed status to set expiration date
						if ($item->status == "Completed"){
								$criteria = new stdClass();
								$criteria->id = $item->trainingId;
								$trainingRecordCollection = $hrTrainingsProvider->get($criteria);
								$trainingRecord = $trainingRecordCollection->collection;
								$isAnnuallyRequired = $trainingRecord[0]->isAnnuallyRequired;
						}

						foreach ($items as $displayID=>$titleId){
							$criteria = new stdClass();
							$criteria->showAll = false;
							$criteria->isManager = false;
							$criteria->titleId = $titleId;
							$paginationResult = $hrEmployeeProvider->get($criteria);
							$resultArray = $paginationResult->collection;
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								$responseCollection[] = $addMatrixResponse;
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
								
								//check to see if TrainingID is AnnuallyRecurring
								if ($isAnnuallyRequired && $addMatrixResponse->insertedId){
									$employeeId = $employeeObj->id;
									$employeeFullName = $employeeObj->fullName;
									$activityRecord = new stdClass();
									$activityRecord->matrixId = $addMatrixResponse->insertedId;
									$activityRecord->employeeId = $employeeId;
									$activityRecord->employeeName = $employeeFullName;
									$activityRecord->trainingId = $item->trainingId;
									$activityRecord->trainingName = $item->trainingName;
									$activityRecord->status = "Completed";
									$activityRecord->expirationDate = date("m/d/Y",strtotime($item->affectiveDate." +1 year"));
									//$completedRecord->addedByAdmin;
									$updatedActivityRecords[] = $hrTrainingsProvider->addActivity($activityRecord,getAdminId());
								}
								
								
								
							}
						}
					}
				}
				
				if ($action == 'Remove_TrainingMatrix'){
					include_once($dbProviderFolder."HRTrainingsProvider.php");
					$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrTrainingsProvider->removeTrainingMatrix($id,getAdminId());
						if (count($responseCollection)){
							$responseCollection[0] = $id."Removed";
						}
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = $action." unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>