<?php include_once('_comboBoxIncludes.php');?>
		<script>
 
		  $(function() {
			$( ".comboboxTraining" ).combobox({
				select: function(event, ui){
					<?php if (!$trainingComboBoxFunction){?>
					window.location = "?nav=manage-trainings&TrainingID="+$(ui.item).val();
					<?php }else{
						echo $trainingComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="ten columns">
			<div class="ui-widget">
				<?php if(!$trainingComboBoxHideLabel){?>
				<label>Start typing training name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxTraining">
				<option value="">Select one...</option>
				<?php 
					foreach ($TrainingsActive as $training=>$trainingInfo){
						echo "<option value=\"".$trainingInfo->id."\">".$trainingInfo->name."</option>";
					}
				?>
			  </select>
			</div>
		</div>