<?php
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_HR)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}

?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_HR))){?>
			<li><a id="manage-employees" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-employees" class="button-link">Manage Employees</a></li>
			<li><a id="manage-revenueCodes" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-revenueCodes" class="button-link">Manage Revenue Codes</a></li>
			<!--<li><a id="manage-departments" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-departments" class="button-link">Manage Departments</a></li>
			<li><a id="manage-titles" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-titles" class="button-link">Manage Titles</a></li>
			<li><a id="manage-categories" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-categories" class="button-link">Manage Categories</a></li>
			<li><a id="manage-trainingentry" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainingentry" class="button-link">Training Entry</a></li>
			
			
			<li><a id="manage-trainings" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings" class="button-link">Manage Trainings</a></li>
	        <li><a id="file-library" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=file-library" class="button-link">File Library</a></li>
	        <li><a id="create-onetimeuselink" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=create-onetimeuselink" class="button-link">EEO</a></li>
	        <li><a id="staffmanualtracker" href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=staffmanualtracker" class="button-link">Staff Manual</a></li>
	-->		
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>