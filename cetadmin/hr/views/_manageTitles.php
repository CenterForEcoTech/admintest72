<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeDepartmentsProvider->getTitles($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$TitleByID[$record->id] = $record;
	$TitleActive[$record->displayOrderId]=$record;
	$TitleByName[$record->name] = $record;
}
include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	$TrainingsActive[$record->displayOrderId]=$record;
}

$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = false;
$criteria->isManager = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentDirectors[] = $record->firstName." ".$record->lastName;
}
//resort the products without display order by TitleID
$SelectedTitleID = $_GET['TitleID'];
$SelectedTitleName = $_GET['TitleName'];
if ($SelectedTitleName && !$SelectedTitleID){
	$SelectedTitleID = $TitleByName[$SelectedTitleName]->id;
}

$addTitle = $_GET['addTitle'];
$displayOrderId = $TitleByID[$SelectedTitleID]->displayOrderId;
if ($addTitle){$displayOrderId = (count($TitleActive)+1);}

//get employees with this title
if ($SelectedTitleID){
	$criteria->showAll = false;
	$criteria->isManager = false;
	$criteria->titleId = $SelectedTitleID;
	$paginationResult = $hrEmployeeProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		if ($record->displayOrderId > 0){
			$EmployeesActive[$record->fullName]=$record;
			$TitleEmployees[$record->fullName]["id"] = $record->id;
			$TitleEmployees[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
			$TitleEmployeesByManager[$record->manager][] = $record;
			$departmentDirector = $record->manager;
		}
	}
	ksort($TitleEmployees);
	//print_pre($TitleEmployeesByManager);

	$managerCount = count($TitleEmployeesByManager);
}//end if selected Department


?>
<?php if (!$addTitle){?>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:140px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .employeeDescription {font-weight:normal;}
  .employeeName {font-weight:bold;}
  div.orgChart{background-color: <?php echo ($TitleByID[$SelectedTitleID]->color ? $TitleByID[$SelectedTitleID]->color  : "#ffffe8");?>;}

  </style>
	<div class="fifteen columns"><bR>
		<button id="showTitleSelectForm">Choose A Different Title</button>
	</div>
	<fieldset id="titleSelectForm">
		<legend>Choose A Title In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Titles</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($TitleActive as $title=>$titleInfo){
						echo "<li id=\"".$titleInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-titles&TitleID=".$titleInfo->id."\">".$titleInfo->name."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedTitleID || $addTitle){?>
	<form id="TitleUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Title Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addTitle){?><input type="text" name="id" value="<?php echo $TitleByID[$SelectedTitleID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Details</legend>
						<div class="seven columns">
							<div class="two columns">Name:</div><div class="four columns"><input type="text" name="name" value="<?php echo $TitleByID[$SelectedTitleID]->name;?>"></div>
							<br clear="all">
							<div class="two columns">Active Status:</div>
							<div class="two columns">
								<select name="activeId">
										<option value="1"<?php echo ($TitleByID[$SelectedTitleID]->activeId ? " selected" : "");?>>Active</option>
										<option value="0"<?php echo (!$TitleByID[$SelectedTitleID]->activeId ? " selected" : "");?>>InActive</option>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="titleResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-titles&TitleID=<?php echo $SelectedTitleID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addTitle){?>
					<a href="save-title-add" id="save-title-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="title_add">
				<?php }else{?>
					<a href="save-title-update" id="save-title-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="title_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<?php if ($SelectedTitleID){?>
		<fieldset>
			<legend>Employees with this Title</legend>
			<div class="fifteen columns">
				<?php 
				foreach ($TitleEmployees as $EmployeeName=>$employeeInfo){
					echo "<a href='?nav=manage-employees&EmployeeID=".$employeeInfo["id"]."' class='button-link'>".$EmployeeName."</a> ";
				}
				?>
			</div>
			<br clear="all"><br>
		</fieldset>
		<?php }?>
	</form>
		<?php if ($SelectedTitleID){?>
		<fieldset>
		<legend>Trainings/Certifications</legend>
		<div class="fifteen columns">
			<div class="three columns" id="trainingMessage">&nbsp;</div>
			
			<div class="nine columns">&nbsp;
			<!--
				Effective Date: <input type="text" class="date" id="AffectiveDate" name="affectiveDate" value="<?php echo $TodaysDate;?>">&nbsp;&nbsp;&nbsp;
				Status <select id="Status" name="status">
					<?php
						foreach ($TrainingStatusList as $TrainingStatus){
							echo "<option value=\"".$TrainingStatus->name."\">".$TrainingStatus->name."</option>";
						}
					?>
				</select>
			-->
			</div>
			
			<div class="six columns"> 
			<?php
						/*
							$insertRecord = new stdClass();
							$insertRecord->trainingId = 11;
							$insertRecord->type = "Title";
							$insertRecord->typeId = 5;
							$trainingRecordResults = $hrTrainingsProvider->insertTrainingMatrixByType($insertRecord);			
							print_pre($trainingRecordResults);
						*/
							
							$criteria = new stdClass();
							$criteria->type = "Title";
							$criteria->typeId = $SelectedTitleID;
							$trainingRecordCollection = $hrTrainingsProvider->getTrainingMatrixByType($criteria);
							foreach ($trainingRecordCollection->collection as $matrixRecord){
								$TrainingMatrix[$matrixRecord->id] = $matrixRecord;
							}
			
			?>
				<div class="five columns"><b>Drag Training Here to Assign</b><Br><span style="font-size:10pt;">(all staff with this title)</span></div>
				<ul id="sortable3" class="connectedSortable">
				<?php 
					foreach ($TrainingMatrix as $trainingMatrixId=>$matrixInfo){
						$trainingName = $TrainingsByID[$matrixInfo->trainingId]->name;
						$assignedTrainings[] = $matrixInfo->trainingId;
						echo "<li id=\"".$matrixInfo->id."\" data-trainingId=\"".$matrixInfo->trainingId."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$trainingName."</a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="five columns"><b>Trainings/Certifications</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
				<ul id="sortable4" class="connectedSortable">
				<?php 
					foreach ($TrainingsActive as $training=>$trainingInfo){
						if (!in_array($trainingInfo->id,$assignedTrainings)){
							echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
						}
					}
				?>
				</ul>
			</div>
		</div>
	</fieldset>
	<?php }?>
	
	
	
<?php }else{ //end if no department selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-titles&addTitle=true" class="button-link">+ Add New Title</a>
		</div>
	<?php }?>
<?php }?>
<script>
$(function(){
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});


	$("#showTitleSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedTitleID){?>
		$("#titleSelectForm").hide();
		$("#showTitleSelectForm").show();
	<?php }?>
	$("#showTitleSelectForm").click(function(){
		$("#titleSelectForm").show();		
		$(this).hide();
	});

	$("#titleSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#TitleUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
            director: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-title-update").click(function(e){

        $('#titleResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeTitlesManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#titleResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#titleResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-title-add").click(function(e){

        $('#titleResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeTitlesManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#titleResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					

                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#titleResults').html(message).addClass("alert");
                }
            });
        }
    });

});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");
	
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiEmployeeTitlesManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedTitleID){?>
			$( "#sortable3, #sortable4" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedTrainings = new Array();
					var removeTrainings = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisTrainingId = $thisLi.attr('data-trainingId');
						if (thisTrainingId){
//							removeTrainings.push($thisLi.attr('id'));
						}else{
							assignedTrainings.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedTrainings.length){
						var AffectiveDate = '<?php echo date("m/d/Y");?>',
							Status = 'Assigned',
							TitleName = '<?php echo urlencode($TitleByID[$SelectedTitleID]->name);?>',
							TitleID = <?php echo $SelectedTitleID;?>;

						DisplayArray.push({action: 'Insert_TrainingMatrixByTitle',TitleName: TitleName,titleId: TitleID,affectiveDate: AffectiveDate, status: Status,items:assignedTrainings});
					}
					if (removeTrainings.length){
//						DisplayArray.push({action: 'Remove_TrainingMatrix',items:removeTrainings});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						console.log(DisplayArray);
						$.ajax({
							url: "ApiEmployeeManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								console.log(data);
								$('#trainingMessage').fadeIn().html("Trainings "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#trainingMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
		<?php }//end if $SelectedEmployeeID ?>
		
		
		
		
<?php }//not in readonly?>
});
</script>