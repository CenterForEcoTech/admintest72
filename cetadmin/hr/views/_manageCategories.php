<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeDepartmentsProvider->getCategories($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CategoryByID[$record->id] = $record;
	$CategoryActive[$record->displayOrderId]=$record;
	$CategoryByName[$record->name] = $record;
}
include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	$TrainingsActive[$record->displayOrderId]=$record;
}

$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = false;
$criteria->isManager = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentDirectors[] = $record->firstName." ".$record->lastName;
}
//resort the products without display order by CategoryID
$SelectedCategoryID = $_GET['CategoryID'];
$SelectedCategoryName = $_GET['CategoryName'];
if ($SelectedCategoryName && !$SelectedCategoryID){
	$SelectedCategoryID = $CategoryByName[$SelectedCategoryName]->id;
}

$addCategory = $_GET['addCategory'];
$displayOrderId = $CategoryByID[$SelectedCategoryID]->displayOrderId;
if ($addCategory){$displayOrderId = (count($CategoryActive)+1);}

//get employees with this title
if ($SelectedCategoryID){
	$criteria->showAll = false;
	$criteria->isManager = false;
	$criteria->categoryId = $SelectedCategoryID;
	$paginationResult = $hrEmployeeProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		if ($record->displayOrderId > 0){
			$EmployeesActive[$record->fullName]=$record;
			$CategoryEmployees[$record->fullName]["id"] = $record->id;
			$CategoryEmployees[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
			$CategoryEmployeesByManager[$record->manager][] = $record;
			$departmentDirector = $record->manager;
		}
	}
	ksort($CategoryEmployees);
	//print_pre($CategoryEmployeesByManager);

}//end if selected Department


?>
<?php if (!$addCategory){?>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:140px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .employeeDescription {font-weight:normal;}
  .employeeName {font-weight:bold;}
  div.orgChart{background-color: <?php echo ($CategoryByID[$SelectedCategoryID]->color ? $CategoryByID[$SelectedCategoryID]->color  : "#ffffe8");?>;}

  </style>
	<div class="fifteen columns"><bR>
		<button id="showCategorySelectForm">Choose A Different Category</button>
	</div>
	<fieldset id="categorySelectForm">
		<legend>Choose A Category In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Categories</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($CategoryActive as $category=>$categoryInfo){
						echo "<li id=\"".$categoryInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-categories&CategoryID=".$categoryInfo->id."\">".$categoryInfo->name."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedCategoryID || $addCategory){?>
	<form id="CategoryUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Category Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addCategory){?><input type="text" name="id" value="<?php echo $CategoryByID[$SelectedCategoryID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Details</legend>
						<div class="seven columns">
							<div class="two columns">Name:</div><div class="four columns"><input type="text" name="name" value="<?php echo $CategoryByID[$SelectedCategoryID]->name;?>"></div>
							<br clear="all">
							<div class="two columns">Active Status:</div>
							<div class="two columns">
								<select name="activeId">
										<option value="1"<?php echo ($CategoryByID[$SelectedCategoryID]->activeId ? " selected" : "");?>>Active</option>
										<option value="0"<?php echo (!$CategoryByID[$SelectedCategoryID]->activeId ? " selected" : "");?>>InActive</option>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="categoryResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-categories&CategoryID=<?php echo $SelectedCategoryID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addCategory){?>
					<a href="save-category-add" id="save-category-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="category_add">
				<?php }else{?>
					<a href="save-category-update" id="save-category-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="category_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<!--
		<?php if ($SelectedCategoryID){?>
		<fieldset>
			<legend>Employees in this Category</legend>
			<div class="fifteen columns">
				<?php 
				foreach ($CategoryEmployees as $EmployeeName=>$employeeInfo){
					echo "<a href='?nav=manage-employees&EmployeeID=".$employeeInfo["id"]."' class='button-link'>".$EmployeeName."</a> ";
				}
				?>
			</div>
			<br clear="all"><br>
		</fieldset>
		<?php }?>
		-->
	</form>
		<div style="display:none;"><!--hide assigning training-->
		<?php if ($SelectedCategoryID){?>
		<fieldset>
		<legend>Trainings/Certifications</legend>
			<div class="fifteen columns">
				<div class="three columns" id="trainingMessage">&nbsp;</div>
				
				<div class="nine columns">&nbsp;
				<!--
					Effective Date: <input type="text" class="date" id="AffectiveDate" name="affectiveDate" value="<?php echo $TodaysDate;?>">&nbsp;&nbsp;&nbsp;
					Status <select id="Status" name="status">
						<?php
							foreach ($TrainingStatusList as $TrainingStatus){
								echo "<option value=\"".$TrainingStatus->name."\">".$TrainingStatus->name."</option>";
							}
						?>
					</select>
				-->
				</div>
				
				<div class="six columns"> 
				<?php
							/*
								$insertRecord = new stdClass();
								$insertRecord->trainingId = 11;
								$insertRecord->type = "Title";
								$insertRecord->typeId = 5;
								$trainingRecordResults = $hrTrainingsProvider->insertTrainingMatrixByType($insertRecord);			
								print_pre($trainingRecordResults);
							*/
								
								$criteria = new stdClass();
								$criteria->type = "Category";
								$criteria->typeId = $SelectedCategoryID;
								$trainingRecordCollection = $hrTrainingsProvider->getTrainingMatrixByType($criteria);
								foreach ($trainingRecordCollection->collection as $matrixRecord){
									$TrainingMatrix[$matrixRecord->id] = $matrixRecord;
								}
				
				?>
					<div class="five columns"><b>Drag Training Here to Assign</b><Br><span style="font-size:10pt;">(for all staff in this category)</span></div>
					<ul id="sortable3" class="connectedSortable">
					<?php 
						foreach ($TrainingMatrix as $trainingMatrixId=>$matrixInfo){
							$trainingName = $TrainingsByID[$matrixInfo->trainingId]->name;
							$assignedTrainings[] = $matrixInfo->trainingId;
							echo "<li id=\"".$matrixInfo->id."\" data-trainingId=\"".$matrixInfo->trainingId."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$trainingName."</a></li>";
						}
					?>
					</ul>
				</div>
				<div class="one columns">&nbsp;</div>
				<div class="six columns"> 
					<div class="five columns"><b>Trainings/Certifications</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
					<ul id="sortable4" class="connectedSortable">
					<?php 
						foreach ($TrainingsActive as $training=>$trainingInfo){
							if (!in_array($trainingInfo->id,$assignedTrainings)){
								echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
							}
						}
					?>
					</ul>
				</div>
			</div>
		</fieldset>
		<?php }?>
		</div><!--end hide assign training-->
	
	
<?php }else{ //end if no department selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-categories&addCategory=true" class="button-link">+ Add New Category</a>
		</div>
	<?php }?>
<?php }?>
<script>
$(function(){
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});


	$("#showCategorySelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedCategoryID){?>
		$("#categorySelectForm").hide();
		$("#showCategorySelectForm").show();
	<?php }?>
	$("#showCategorySelectForm").click(function(){
		$("#categorySelectForm").show();		
		$(this).hide();
	});

	$("#categorySelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#CategoryUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-category-update").click(function(e){

        $('#categoryResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeCategoriesManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#categoryResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#categoryResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-category-add").click(function(e){

        $('#categoryResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeCategoriesManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#categoryResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					

                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#categoryResults').html(message).addClass("alert");
                }
            });
        }
    });

});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");
	
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiEmployeeCategoriesManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedCategoryID){?>
			$( "#sortable3, #sortable4" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedTrainings = new Array();
					var removeTrainings = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisTrainingId = $thisLi.attr('data-trainingId');
						if (thisTrainingId){
//							removeTrainings.push($thisLi.attr('id'));
						}else{
							assignedTrainings.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedTrainings.length){
						var AffectiveDate = $("#AffectiveDate").val(),
							Status = $("#Status").val(),
							CategoryName = '<?php echo urlencode($CategoryByID[$SelectedCategoryID]->name);?>',
							CategoryID = <?php echo $SelectedCategoryID;?>;

						DisplayArray.push({action: 'Insert_TrainingMatrixByCategory',CategoryName: CategoryName,categoryId: CategoryID,affectiveDate: AffectiveDate, status: Status,items:assignedTrainings});
					}
					if (removeTrainings.length){
//						DisplayArray.push({action: 'Remove_TrainingMatrix',items:removeTrainings});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						console.log(DisplayArray);
						$.ajax({
							url: "ApiEmployeeManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								//console.log(data);
								$('#trainingMessage').fadeIn().html("Trainings "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#trainingMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
		<?php }//end if $SelectedEmployeeID ?>
<?php }//not in readonly?>
});
</script>