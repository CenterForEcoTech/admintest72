<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeDepartmentsProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentsByName[$record->name] = $record;
	$Departments[$record->id] = $record->name;
}	

$paginationResultTitles = $hrEmployeeDepartmentsProvider->getTitles($criteria);
$resultArrayTitles = $paginationResultTitles->collection;
foreach ($resultArrayTitles as $result=>$record){
	$TitlesByName[$record->name] = $record;
	$Titles[$record->id] = $record->name;
}	

$paginationResultCategories = $hrEmployeeDepartmentsProvider->getCategories($criteria);
$resultArrayCategories = $paginationResultCategories->collection;
foreach ($resultArrayCategories as $result=>$record){
	if ($record->activeId){
		$CategoriesByName[$record->name] = $record;
		$Categories[$record->id] = $record->name;
	}
}	


include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
		$EmployeesByManager[$record->manager][]=$record;
		$EmployeesByFullName[$record->fullName]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
	if ($record->isManager && !MySQLDate($record->endDate)){
		$Managers[] = $record->firstName." ".$record->lastName;
	}

}

//retrieve Revenue codes
$RevenueCodesResults = $hrEmployeeProvider->getRevenueCodes($criteria);
foreach ($RevenueCodesResults as $results){
	$RevenueCodes[$results["HRRevenueCode_Code"]] = $results["HRRevenueCode_Name"];
}

//resort the products without display order by EmployeeID
ksort($EmployeesInActive);
$SelectedEmployeeID = $_GET['EmployeeID'];
$SelectedEmployeeName = $_GET['EmployeeName'];
if ($SelectedEmployeeName && !$SelectedEmployeeID){
	$SelectedEmployeeID = $EmployeesByFullName[$SelectedEmployeeName]->id;
}
$addEmployee = $_GET['addEmployee'];
$displayOrderId = $EmployeeByID[$SelectedEmployeeID]->displayOrderId;
if ($addEmployee){$displayOrderId = (count($EmployeesActive)+1);}

$officeLocationsResults = $hrEmployeeProvider->getOfficeLocations();
foreach ($officeLocationsResults as $id=>$location){
	$officeLocations[] = $location["name"];
}	

include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	if ($record->activeStatus == "Active"){
		$TrainingsByID[$record->id] = $record;
		$TrainingsByCatID[$record->categoryId][] = $record;
		if (!$record->categoryId){
			$TrainingsActive[$record->displayOrderId]=$record;
		}
		
		$categories = explode(",",$record->categories);
		foreach ($categories as $category){
			if (!in_array(trim($category),$categoryNames)){
				$categoryNames[] = trim($category);
			}
		}
		if ($record->isCertification){
			$Certifications[] = $record;
		}
		if ($record->partOfCertificationId){
			$TrainingsByCertification[$record->partOfCertificationId][] = $record;
		}
	}
}
$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

if ($SelectedEmployeeID){
	$criteria = new stdClass();
	$criteria->employeeId = $SelectedEmployeeID;
	$criteria->statusNot = "Removed";
	//print_pre($criteria);
	$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$TrainingMatrixCollection = $TrainingMatrixResults->collection;
	if (count($TrainingMatrixCollection)){
		foreach ($TrainingMatrixCollection as $id=>$collection){
			$TrainingMatrixAssigned[$collection->trainingId] =$collection;
		}
	}
	if ($EmployeeByID[$SelectedEmployeeID]->titleId){
		$criteria = new stdClass();
		$criteria->type = "Title";
		$criteria->typeId = $EmployeeByID[$SelectedEmployeeID]->titleId;
		$criteria->status = "Active";
		$trainingRecordCollection = $hrTrainingsProvider->getTrainingMatrixByType($criteria);
		foreach ($trainingRecordCollection->collection as $matrixRecord){
			if (!$TrainingMatrixAssigned[$matrixRecord->trainingId]){
				$TrainingTitleMatrix[$matrixRecord->trainingId] = $TrainingsByID[$matrixRecord->trainingId];
			}
		}
		//add any missing trainings
		if (count($TrainingTitleMatrix)){
			$trainingAlert =  "Will Add Missing Trainings based on Employee's Title:<br>";
			foreach ($TrainingTitleMatrix as $id=>$trainingInfo){
				$record = new StdClass();
				$record->employeeId = $SelectedEmployeeID;
				$record->employeeFullName= $EmployeeByID[$SelectedEmployeeID]->fullName;
				$record->affectiveDate = $TodaysDate;
				$record->status = "Assigned";
				$record->trainingId = $id;
				$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
				$responseCollections[] = $addMatrixResponse;
				if (count($responseCollections)){
					$trainingAlert .= "Added ".$trainingInfo->name."<br>";
				}
			}
		}
	}
	if ($EmployeeByID[$SelectedEmployeeID]->categoryId){
		$criteria = new stdClass();
		$criteria->type = "Category";
		$criteria->typeId = $EmployeeByID[$SelectedEmployeeID]->categoryId;
		$criteria->status = "Active";
		$trainingRecordCollection = $hrTrainingsProvider->getTrainingMatrixByType($criteria);
		foreach ($trainingRecordCollection->collection as $matrixRecord){
			if (!$TrainingMatrixAssigned[$matrixRecord->trainingId]){
				$TrainingCategoryMatrix[$matrixRecord->trainingId] = $TrainingsByID[$matrixRecord->trainingId];
			}
		}
		//add any missing trainings
		if (count($TrainingCategoryMatrix)){
			$trainingAlert =  "Will Add Missing Trainings based on Employee's Category:<br>";
			foreach ($TrainingCategoryMatrix as $id=>$trainingInfo){
				$record = new StdClass();
				$record->employeeId = $SelectedEmployeeID;
				$record->employeeFullName= $EmployeeByID[$SelectedEmployeeID]->fullName;
				$record->affectiveDate = $TodaysDate;
				$record->status = "Assigned";
				$record->trainingId = $id;
				$addMatrixResponse = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
				$responseCollections[] = $addMatrixResponse;
				if (count($responseCollections)){
					$trainingAlert .= "Added ".$trainingInfo->name."<br>";
				}
			}
		}
	}
	$criteria = new stdClass();
	$criteria->employeeId = $SelectedEmployeeID;
	$criteria->statusNot = "Removed";
	$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$TrainingMatrixCollection = $TrainingMatrixResults->collection;
	if (count($TrainingMatrixCollection)){
		foreach ($TrainingMatrixCollection as $id=>$collection){
			$TrainingMatrix[] = $collection;
		}
	}
	
}


?>
<?php if (!$addEmployee){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:380px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:30px;
  }
  div.orgChart{background-color: <?php echo ($DepartmentsByName[$EmployeeByID[$SelectedEmployeeID]->department]->color ? $DepartmentsByName[$EmployeeByID[$SelectedEmployeeID]->department]->color  : "#ffffe8");?>;}
  </style>
<br>
  
	<div class="fifteen columns">
		<?php include('_EmployeeComboBox.php');?>
		<div class="two columns">
			<button id="showEmployeeSelectForm" style="font-size:8pt;">Change Display Order</button>
		</div>
	</div>
	<fieldset id="employeeSelectForm">
		<legend>Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Active Employees</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($EmployeesActive as $employee=>$employeeInfo){
						echo "<li id=\"".$employeeInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-employees&EmployeeID=".$employeeInfo->id."\">".$employeeInfo->firstName." ".$employeeInfo->lastName."</a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>InActive Employees</b></div>
				<ul id="sortable2" class="connectedSortable">
				<?php 
					foreach ($EmployeesInActive as $employee=>$employeeInfo){
						echo "<li id=\"".$employeeInfo->id."\" class=\"ui-state-disabled\"><a href=\"?nav=manage-employees&EmployeeID=".$employeeInfo->id."\" style=\"text-decoration:none;\"><span class=\"employeeName\">".$employeeInfo->firstName." ".$employeeInfo->lastName."</span></a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addemployee?>
<?php if ($SelectedEmployeeID || $addEmployee){?>
	<form id="EmployeeUpdateForm" class="basic-form sixteen columns override_skel">
		<?php echo $trainingAlert;?>
		<fieldset>
			<legend>Employees Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addEmployee){?><input type="text" name="id" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Contact Info</legend>
						<div class="one columns">
							<img class="picture_id1" src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
							<img class="picture_id2" src="https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
						</div>
						<div class="seven columns">
							<div class="two columns">First Name:</div><div class="four columns"><input type="text" id="firstName" name="firstName" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->firstName;?>"></div>
							<br clear="both">
							<div class="two columns">Last Name:</div><div class="four columns"><input type="text" id="lastName" name="lastName" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->lastName;?>"></div>
							<br clear="both">
							<div class="two columns">Email:</div><div class="four columns"><input type="text" name="email" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>"></div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Hire Details</legend>
						<div class="five columns">
							<div class="two columns">Hire Date:</div><div class="two columns"><input type="text" class="date" name="hireDate" value="<?php echo MySQLDate($EmployeeByID[$SelectedEmployeeID]->hireDate);?>"></div>
							<br clear="both">
							<div class="two columns">End Date:</div><div class="two columns"><input type="text" class="date" name="endDate" value="<?php echo MySQLDate($EmployeeByID[$SelectedEmployeeID]->endDate);?>"></div>
							<br clear="both">
							<div class="two columns">Location:</div>
							<div class="two columns">
								<select name="officeLocation" id="officeLocation">
										<option value="">Choose a Location</option>
									<?php
										foreach ($officeLocations as $OfficeLocation){
											echo "<option value='".$OfficeLocation."'".($OfficeLocation==$EmployeeByID[$SelectedEmployeeID]->officeLocation ? " selected":"").">".$OfficeLocation."</option>";
										}
									?>
								</select>
							</div><?php if (!$ReadOnlyTrue){?><span class="edit ui-icon ui-icon-pencil" title="click to edit list" id="locationListEditIcon"></span><?php }?>
						</div>
					</fieldset>
					<fieldset>
						<legend>Position Info</legend>
						<div class="one columns">
							&nbsp;
						</div>
						<div class="seven columns">
							<div class="two columns"><a href="?nav=manage-departments&DepartmentID=<?php echo $EmployeeByID[$SelectedEmployeeID]->departmentId;?>" title="Click to edit information about <?php echo $EmployeeByID[$SelectedEmployeeID]->department;?>">Department</a>:</div>
							<div class="four columns">
								<select name="departmentId" id="departmentId">
										<option value="">Choose a Department</option>
									<?php
										foreach ($Departments as $DepartmentId=>$Department){
											echo "<option value='".$DepartmentId."'".($EmployeeByID[$SelectedEmployeeID]->departmentId ? ($DepartmentId==$EmployeeByID[$SelectedEmployeeID]->departmentId ? " selected":"") : ($Department==$EmployeeByID[$SelectedEmployeeID]->department ? " selected":"")).">".$Department."</option>";
										}
									?>
								</select>
								<input type="hidden" name="department" id="departmentName" value="<?php echo ($EmployeeByID[$SelectedEmployeeID]->departmentId > 0 ? $Departments[$EmployeeByID[$SelectedEmployeeID]->departmentId] : $EmployeeByID[$SelectedEmployeeID]->department);?>">
							</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-departments"><span class="edit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
							<br clear="both">
							<!--
							<div class="two columns"><a href="?nav=manage-titles&TitleID=<?php echo $EmployeeByID[$SelectedEmployeeID]->titleId;?>" title="Click to edit information about this department">Department</a>:</div>
							<div class="four columns">
								<select name="titleId" id="titleId">
										<option value="">Choose a Department</option>
									<?php
										foreach ($Titles as $TitleId=>$Title){
											echo "<option value='".$TitleId."'".($EmployeeByID[$SelectedEmployeeID]->titleId ? ($TitleId==$EmployeeByID[$SelectedEmployeeID]->titleId ? " selected":"") : ($Title==$EmployeeByID[$SelectedEmployeeID]->Title ? " selected":"")).">".$Title."</option>";
										}
									?>
								</select>
							</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-titles"><span class="edit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
							<br clear="both">
						
							<div class="two columns">Title:</div><div class="four columns"><input type="text" name="title" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->title;?>"></div>
							<br clear="both">
							<div class="two columns"><a href="?nav=manage-categories&CategoryID=<?php echo $EmployeeByID[$SelectedEmployeeID]->categoryId;?>" title="Click to edit information about this category type">Category</a>:</div>
							<div class="four columns">
								<select name="categoryId" id="categoryId">
										<option value="">Choose a Category Type</option>
									<?php
										foreach ($Categories as $CategoryId=>$Category){
											echo "<option value='".$CategoryId."'".($CategoryId==$EmployeeByID[$SelectedEmployeeID]->categoryId ? " selected":"").">".$Category."</option>";
										}
									?>
								</select>
							</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-categories"><span class="edit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
							<br clear="both">
							-->
							<div class="two columns"><a href="?nav=manage-employees&EmployeeName=<?php echo $EmployeeByID[$SelectedEmployeeID]->manager;?>" title="Click to edit information about <?php echo $EmployeeByID[$SelectedEmployeeID]->manager;?>">Supervisor</a>:</div>
							<div class="four columns">
								<select name="manager">
										<option value="">Choose a Supervisor</option>
									<?php
										foreach ($Managers as $Manager){
											echo "<option value='".$Manager."'".($Manager==$EmployeeByID[$SelectedEmployeeID]->manager ? " selected":"").">".$Manager."</option>";
										}
									?>
								</select>
							</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-employees"><span class="edit ui-icon ui-icon-pencil" title="click to edit list - update employee to indicate they manage people to be included in this list"></span></a><?php }?>
							<br clear="both">
							<div class="six columns">
								<input type="checkbox" id="isManager_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->isManager){ echo " checked";}?>><span id="EmployeeFirstName"><?php echo $EmployeeByID[$SelectedEmployeeID]->firstName;?></span> <span id="EmployeeLastName"><?php echo $EmployeeByID[$SelectedEmployeeID]->lastName;?></span> Manages People
								<input type="hidden" name="isManager" id="isManager_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->isManager;?>"><br>
								<input type="checkbox" id="hes_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->hes){ echo " checked";}?>>Assignable Energy Specialist
								<input type="hidden" name="hes" id="hes_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->hes;?>">
							</div>

						</div>
					</fieldset>
					<fieldset id="locationListEdit">
						<legend>Location List Edit</legend>
						<div class="five columns">
							<div id="locationEditListEntry">
								<input type='text' id="locationEditListInput" value='' style='width:100px; height: 22px;'><input type='button' id='editOfficeLocation' value='edit' style='font-size:8pt;'>
							</div>
							<div id="locationResults"></div>
						</div>
					</fieldset>
						<fieldset>
							<legend>Hours Tracking</legend>
							<div class="five columns">
								<input type="checkbox" id="hoursTrackingGBS_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->hoursTrackingGBS){ echo " checked";}?>> Tracks Hours with GBS
								<input type="hidden" name="hoursTrackingGBS" id="hoursTrackingGBS_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->hoursTrackingGBS;?>"><Br>
								<input type="checkbox" id="hoursTrackingGHS_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->hoursTrackingGHS){ echo " checked";}?>> Tracks Hours with GHS
								<input type="hidden" name="hoursTrackingGHS" id="hoursTrackingGHS_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->hoursTrackingGHS;?>"><br>
								<input type="checkbox" id="excludeFromHoursEmail_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->excludeFromHoursEmail){ echo " checked";}?>> Exclude From Non-Submitted Hours Email Alert<br>
								<input type="hidden" name="excludeFromHoursEmail" id="excludeFromHoursEmail_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->excludeFromHoursEmail;?>">
								<input type="checkbox" id="excludeFromProjectionSummary_checkbox"<?php if($EmployeeByID[$SelectedEmployeeID]->excludeFromProjectionSummary){ echo " checked";}?>> Exclude From Projection Summary<br>
								<input type="hidden" name="excludeFromProjectionSummary" id="excludeFromProjectionSummary_input" value="<?php echo $EmployeeByID[$SelectedEmployeeID]->excludeFromProjectionSummary;?>">
								<br clear="both">
								<div class="six columns">Revenue Code: 
									<select name="revenueCode">
											<option value="">Choose a Revenue Code</option>
										<?php
											foreach ($RevenueCodes as $RevenueCode=>$RevenueName){
												echo "<option value='".$RevenueCode."'".($RevenueCode==$EmployeeByID[$SelectedEmployeeID]->revenueCode ? " selected":"").">".$RevenueCode." ".$RevenueName."</option>";
											}
										?>
									</select>
								</div>
							</div>
						</fieldset>
					<?php //}?>
				</div>
			</div>
		</fieldset>
		<div id="employeeResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-employees&EmployeeID=<?php echo $SelectedEmployeeID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addEmployee){?>
					<a href="save-employee-add" id="save-employee-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="employee_add">
				<?php }else{?>
					<a href="save-employee-update" id="save-employee-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="employee_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
		<?php 
		if (!$addEmployee && count($EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName])){
		?>
			<fieldset>
				<legend>Employees Supervised By <?php echo $EmployeeByID[$SelectedEmployeeID]->fullName;?></legend>
				<div class="fifteen columns">
						<ul id="orgchart-source" style="display:none;">
							<?php
								echo "<li class=\"big\"><em>".$EmployeeByID[$SelectedEmployeeID]->fullName."</em>";
								echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeeByID[$SelectedEmployeeID]->email."&size=HR64x64\">
									<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeeByID[$SelectedEmployeeID]->email."&size=HR64x64\">";
								echo "<ul>";
								foreach ($EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName] as $id=>$ManageesName){
									echo "<li class=\"big\"><a href=\"?nav=manage-employees&EmployeeID=".$EmployeesByFullName[$ManageesName->fullName]->id."\">".$ManageesName->fullName."</a>";
									echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesByFullName[$ManageesName->fullName]->email."&size=HR64x64\">
									<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesByFullName[$ManageesName->fullName]->email."&size=HR64x64\">";
									echo "</li>";
								}
								echo "</ul>";
							?>
						</ul>
						<div id="orgchart-container" class="reset-this"></div>					

				</div>
			</fieldset>
		<?php }//if $EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName] ?>
	</form>
			<?php if (!$addEmployee){?>
			<fieldset>
				<legend>Trainings/Certifications</legend>
				<div class="fifteen columns">
					<div class="three columns" id="trainingMessage">&nbsp;</div>
					<div class="nine columns">
						Effective Date: <input type="text" class="date" id="AffectiveDate" name="affectiveDate" value="<?php echo $TodaysDate;?>">&nbsp;&nbsp;&nbsp;
						Status <select id="Status" name="status">
									<option value="Assigned">Assigned</option>
									<option value="Completed">Completed</option>
							<?php
								/*
								foreach ($TrainingStatusList as $TrainingStatus){
									echo "<option value=\"".$TrainingStatus->name."\">".$TrainingStatus->name."</option>";
								}
								*/
							?>
						</select>
					</div>
					
					<div class="six columns"> 
						<div class="three columns"><b>Currently Assigned</b><Br><span style="font-size:10pt;">&nbsp;</span></div>
						<ul id="sortable3" class="connectedSortable">
						<?php 
							foreach ($TrainingMatrix as $trainingMatrixId=>$matrixInfo){
								if ($TrainingsByID[$matrixInfo->trainingId]){
									$affectiveTimeStamp = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLTimeStampDisplay($matrixInfo->timeStamp));
									$affectiveDate = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLDate($matrixInfo->timeStamp));
									echo "<li id=\"".$matrixInfo->id."\" data-trainingId=\"".$matrixInfo->trainingId."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$matrixInfo->trainingName." <span class='certDescription' title='".$matrixInfo->status." ".$affectiveTimeStamp." by ".$matrixInfo->addedByAdmin." added into the system on ".MySQLTimeStampDisplay($matrixInfo->timeStamp)."'>".$matrixInfo->status." ".$affectiveDate."</span></a></li>";
								}
							}
						?>
						</ul>
					</div>
					<div class="one columns">&nbsp;</div>
					<div class="six columns"> 
						<div class="five columns"><b>Trainings/Certifications</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
						<ul id="sortable4" class="connectedSortable">
							categories:<br>
						<?php 
							foreach ($CategoriesByName as $catName=>$catInfo){
								echo "<li id=\"".$catInfo->id."\" class=\"ui-state-default ui-state-highlight\" style='height:15px;'><span class='trainingCategory' data-catid='".$catInfo->id."' style='cursor:pointer;'>".$catName."</span></li>";
								foreach ($TrainingsByCatID[$catInfo->id] as $training=>$trainingInfo){
									echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default catid".$catInfo->id."\" style='display:none;margin-left:20px;width:90%;'><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
								}
							}
							echo "<hr>non-categorized trainings:<br>";
						
						
							foreach ($TrainingsActive as $training=>$trainingInfo){
								echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
							}
						?>
						</ul>
					</div>
					<div class="twelve columns">
						<div id="TrainingCertificates" class="opened"><span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> Uploaded Certificates
							<div class="trainingDetails" style="display:block;">
								<div id="uploadedCerts"></div>
							</div>
						</div>
						<script>
							$(document).ready(function() {
								$.ajax({
									url: "<?php echo $CurrentServer.$adminFolder;?>tools/ApiFileLibrary.php?action=keyword_search&keyword=EmployeeID&EmployeeID=<?php echo $SelectedEmployeeID;?>",
									type: "GET",
									success: function(data){
										var displayDetails = "";
										$.each(data,function(key,result){
											var fileName = data[key].fileName,
												folderUrl = data[key].folderUrl,
												filePath = folderUrl+fileName,
												fileType = data[key].fileType,
												description = data[key].description,
												title = data[key].title;
												displayDetails = displayDetails + '<span><a href="'+filePath+'" target="file-preview" title="'+title+'"><img src="<?php echo $CurrentServer;?>images/file_pdf.png"></a>'+description+'</span><br>';
										});
										if (displayDetails == ""){
											$("#TrainingCertificates").hide();
										}
										$("#uploadedCerts").html(displayDetails);
									},
									error: function(jqXHR, textStatus, errorThrown){
										var message = $.parseJSON(jqXHR.responseText);
										$("#uploadedCerts").html(message).addClass("alert");
									}
								});

							});
						</script>
					</div>
				</div>
			</fieldset>
<?php }// end if (!$addemployee)?>

<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-employees&addEmployee=true" class="button-link">+ Add New Employee</a>
		</div>
	<?php }?>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>

<script>
$(function(){
	$(".trainingCategory").on('click',function(){
		var $this = $(this),
			thisCatId = $this.attr('data-catid'),
			thisClass = ".catid"+thisCatId;
			
			$(thisClass).toggle();
	});
	
	$("#departmentId").on("change",function(){
		$("#departmentName").val($( "#departmentId option:selected" ).text());
	});
	
	$('#orgchart-source').orgChart({container: $('#orgchart-container')});
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	var employeeSelectForm = $("#employeeSelectForm"),
		locationListEdit = $("#locationListEdit"),
		locationListEditIcon = $("#locationListEditIcon"),
		locationEditListEntry = $("#locationEditListEntry"),
		officeLocation = $("#officeLocation"),
		locationEditListInput = $("#locationEditListInput");
		
	employeeSelectForm.hide();	
	locationListEdit.hide();
	locationListEditIcon.on('click',function(){
		var officeLocationVal = officeLocation.val(),
			officeLocationValLength = officeLocationVal.length;
		if (!officeLocationValLength){
			officeLocationValLength = 10;
		}
		locationListEdit.toggle();	
		locationEditListInput.val(officeLocationVal);
	});
	<?php if (!$ReadOnlyTrue){?>
		officeLocation.on('change',function(){
			locationListEdit.hide();
			if (!officeLocation.val()){
				locationListEditIcon.removeClass('ui-icon-pencil').addClass('ui-icon-plus');
				locationListEditIcon.click();
			}else{
				locationListEditIcon.removeClass('ui-icon-plus').removeClass('ui-icon-pencil').addClass('ui-icon-pencil');
			}
		});
		
		$("#editOfficeLocation").click(function(e){
			$('#locationResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			var jsondata = '{"action":"location_update","OfficeLocationSrc":"'+officeLocation.val()+'","OfficeLocationVal":"'+locationEditListInput.val()+'"}';
			$.ajax({
				url: "ApiEmployeeManagement.php",
				type: "PUT",
				data: jsondata,
				success: function(data){
					$('#locationResults').html("Location List Edited").addClass("alert").addClass("info");
					//now update the pulldown and auto save the employee information
					if (officeLocation.val()){
						$('#officeLocation option').filter(function(){return $(this).html() == officeLocation.val(); }).val(locationEditListInput.val()).text(locationEditListInput.val());
					}else{
						$('#officeLocation').append($("<option></option>").attr("value", locationEditListInput.val()).text(locationEditListInput.val())).val(locationEditListInput.val());
					}
					$("#save-employee-update").click();
					
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#locationResults').html(message).addClass("alert");
				}
			});
		});
	<?php }?>
	
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedEmployeeID){?>
		$("#employeeSelectForm").hide();
	<?php }?>
	$("#showEmployeeSelectForm").click(function(){
		$("#employeeSelectForm").toggle();		
	});

	$("#employeeSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#EmployeeUpdateForm");
	 
	 formElement.validate({
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
				email: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-employee-update").click(function(e){

        $('#employeeResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#employeeResults').html("Saved").addClass("alert").addClass("info");
					location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#employeeResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-employee-add").click(function(e){

        $('#employeeResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#employeeResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					
					location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#employeeResults').html(message).addClass("alert");
                }
            });
        }
    });
	
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	
	
});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
				if ($(ui.item).parents('#sortable1').length > 0) {
					$(ui.item).switchClass('ui-state-disabled', 'ui-state-default');
				} else {
					$(ui.item).switchClass('ui-state-default', 'ui-state-disabled');
				}					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiEmployeeManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedEmployeeID){?>
			$( "#sortable3, #sortable4" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedTrainings = new Array();
					var removeTrainings = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisTrainingId = $thisLi.attr('data-trainingId');
						if (thisTrainingId){
							removeTrainings.push($thisLi.attr('id'));
						}else{
							assignedTrainings.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedTrainings.length){
						var AffectiveDate = $("#AffectiveDate").val();
						Status = $("#Status").val();

						DisplayArray.push({action: 'Insert_TrainingMatrix',affectiveDate: AffectiveDate, status: Status,employeeId:<?php echo $SelectedEmployeeID;?>,employeeFullName:'<?php echo urlencode($EmployeeByID[$SelectedEmployeeID]->fullName);?>',items:assignedTrainings});
					}
					if (removeTrainings.length){
						DisplayArray.push({action: 'Remove_TrainingMatrix',items:removeTrainings});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						//console.log(DisplayArray);
						$.ajax({
							url: "ApiEmployeeManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								$('#trainingMessage').fadeIn().html("Trainings "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#trainingMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
			
			
			$('#TrainingCertificates').on('click', 'span.details-open', function () {
				var div = $(this).parent('div');
				var details = $(this).next('div');

				if ( details.is(":visible") ) {
					details.hide();
					div.removeClass('opened');
				}
				else {
					details.show();
					div.addClass('opened');
				}
			} );
		
			
			
		<?php }//end if $SelectedEmployeeID ?>
		
		$("#firstName").keyup(function(){
			$("#EmployeeFirstName").html($(this).val());
		});
		$("#lastName").keyup(function(){
			$("#EmployeeLastName").html($(this).val());
		});
				
		$("#isManager_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#isManager_input").val(1);
			}else{
				$("#isManager_input").val(0);
			}
		});		
		$("#hoursTrackingGBS_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#hoursTrackingGBS_input").val(1);
			}else{
				$("#hoursTrackingGBS_input").val(0);
			}
		});		
		$("#hoursTrackingGHS_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#hoursTrackingGHS_input").val(1);
			}else{
				$("#hoursTrackingGHS_input").val(0);
			}
		});		
		$("#excludeFromHoursEmail_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#excludeFromHoursEmail_input").val(1);
			}else{
				$("#excludeFromHoursEmail_input").val(0);
			}
		});		
		$("#excludeFromProjectionSummary_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#excludeFromProjectionSummary_input").val(1);
			}else{
				$("#excludeFromProjectionSummary_input").val(0);
			}
		});		
		$("#hes_checkbox").change(function(){
			var $this = $(this);
			if($this.prop('checked')){
				$("#hes_input").val(1);
			}else{
				$("#hes_input").val(0);
			}
		});		
<?php }//not in readonly?>
	
});
</script>