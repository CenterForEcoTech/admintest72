<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$criteria->sortColumn = 'name';
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	if ($record->displayOrderId){
		$TrainingsActive[$record->displayOrderId]=$record;
	}
	
}
$TestingDivs .= '<div class="testing">$TrainingsByID is an array of trainings</div>';
//resort the products without display order by TrainingID
$SelectedTrainingMatrixID = $_GET['TrainingMatrixID'];
$addTraining = $_GET['addtrainingevent'];

$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

//get info about employees
include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}
}
$TestingDivs .= '<div class="testing">$EmployeeByID array of employee info</div>';
//print_pre($TrainingMatrixOtherEmployees);


if ($SelectedTrainingMatrixID){
	//get matrix information
	$criteria = new stdClass();
	$criteria->id = $SelectedTrainingMatrixID;
	$criteria->matrixId = $SelectedTrainingMatrixID;
	$paginationResult = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$trainingMatrixObj = $paginationResult->collection[0];
	$TestingDivs .= '<div class="testing">$trainingMatrixObj hold training Matrix info for this one training entry</div>';
	//print_pre($trainingMatrixObj);
	$SelectedEmployeeID = $trainingMatrixObj->employeeId;
	$SelectedEmployeeName = $trainingMatrixObj->employeeFullName;
	$SelectedTrainingID = $trainingMatrixObj->trainingId;
	$SelectedTrainingName = $trainingMatrixObj->trainingName;
	
	//get status history of training
	$historyPaginationResult = $hrTrainingsProvider->getTrainingMatrixStatusHistory($criteria);
	$trainingMatrixHistoryCollection = $historyPaginationResult->collection;
	$TestingDivs .= '<div class="testing">$trainingMatrixHistoryCollection holds training Matrix Status History info for this one training entry</div>';
	//print_pre($trainingMatrixHistoryCollection);
	
	
	//get matrix of employees with this training
	$criteria = new stdClass();
	$criteria->trainingId = $SelectedTrainingID;
	$criteria->statusNot = "Removed";
	$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$TrainingMatrixCollection = $TrainingMatrixResults->collection;
	if (count($TrainingMatrixCollection)){
		foreach ($TrainingMatrixCollection as $id=>$collection){
			//ignore same training same employeeFullName
			if ($collection->employeeId != $SelectedEmployeeID){
				$TrainingMatrixOtherEmployees[] = $collection;
			}
		}
	}
	$TestingDivs .= '<div class="testing">$TrainingMatrixOtherEmployees holds training Matrix info for other employees with this same training</div>';
	//print_pre($TrainingMatrixOtherEmployees);	

	//Get TrainingActivity Details
	$criteria = new stdClass();
	$criteria->employeeId = $SelectedEmployeeID;
	$criteria->trainingId = $SelectedTrainingID;
	$ActivityResultsCollection = $hrTrainingsProvider->getActivity($criteria);
	$TrainingActivityResults = $ActivityResultsCollection->collection;
	
}//end if selected Training

?>
<style>
.testing {display:none;}
.trainingDetails {display:none;}
</style>
Training Management
<?php echo $TestingDivs;?>

<?php if ($trainingMatrixHistoryCollection[0]->status != "Completed"){
	$showForm = true;
?>
	<form id="TrainingUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Training Entry</legend>
			<div class="fifteen columns">
			<?php if ($SelectedTrainingMatrixID){?>
				<div style="display:none;">
					ID=<input type="text" name="id" value="<?php echo $SelectedTrainingMatrixID;?>">
				</div>
				<div class="three columns">Training:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget"><a href="?nav=manage-trainings&TrainingID=<?php echo $SelectedTrainingID;?>"><?php echo $SelectedTrainingName;?></a></div>
					</div>
				</div>
				<br clear="all">
				<div class="three columns">Employee:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget"><a href="?nav=manage-employees&EmployeeID=<?php echo $SelectedEmployeeID;?>"><?php echo $SelectedEmployeeName;?></a></div>
					</div>
				</div>
				<br clear="all">

			<?php }else{ //end if MatrixID ?>
				<div class="three columns">Training:</div>
				<div class="five columns">
					<?php
						$trainingComboBoxFunction = '$("#TrainingID").val($(ui.item).val());';
						$trainingComboBoxHideLabel = true;
						include('_TrainingComboBox.php');
					?>
					<input type="hidden" name="trainingId" id="TrainingID">
				</div>
				<?php if (!$ReadOnlyTrue){?><a href="?nav=manage-trainings"><span class="ui-widgetedit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
					
				<br clear="all">
				<br clear="all">
				
				<div class="three columns">Employee:</div>
				<div class="five columns">
					<?php
						$employeeComboBoxFunction = '$("#EmployeeID").val($(ui.item).val());';
						$employeeComboBoxFunction .= '$("#EmployeeFullName").val($(ui.item).text());';
						$employeeComboBoxHideLabel = true;
						include('_EmployeeComboBox.php');
					?>
					<input type="hidden" name="employeeId" id="EmployeeID">
					<input type="hidden" name="employeeFullName" id="EmployeeFullName">
				</div>
				<?php if (!$ReadOnlyTrue){?><a href="?nav=manage-trainings"><span class="ui-widgetedit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>

				<br clear="all">
				<br clear="all">
<?php }//end if MatrixID ?>
			<?php include_once('_comboBoxIncludes.php');?>
				<div class="three columns">Status:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget">
							<select class="comboboxStatus" id="Status" name="status">
								<option></option>
								<?php
									foreach ($TrainingStatusList as $StatusObj){
										echo "<option value='".$StatusObj->name."'>".$StatusObj->name."</option>\n";
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<?php if (!$ReadOnlyTrue){?><span class="ui-widgetedit ui-icon ui-icon-plus" title="click to add to status type to list" id="statusListEditIcon"></span><?php }?>
				<div style="float:right;">
					<fieldset id="statusListEdit">
						<legend>Add Status Type</legend>
						<div class="five columns">
							<div id="statusEditListEntry">
								<input type='text' id="statusEditListInput" value='' style='width:200px; height: 22px;'>&nbsp;<input type='button' id='editStatus' value='add' style='font-size:8pt;'>
							</div>
							<div id="statusResults"></div>
						</div>
					</fieldset>
				</div>
				
				
				<div class="three columns">Effective Date:</div>
				<div class="five columns">&nbsp;
					<input type="text" class="date" name="affectiveDate">
				</div>
				<div class="twelve columns">*Differentiation Notes: Once Training Entry has been created you can edit the Training Entry Details for this specific employee to differentiate any additional informaiton about this training.</div>
			</div>
		</fieldset>
		<div id="trainingEntryResults" class="fifteen columns"></div>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-employees&EmployeeID=<?php echo $SelectedEmployeeID;?>" id="cancel" class="button-link">Cancel</a>
			<?php if (!$ReadOnlyTrue){?>
				<a href="save-training-entry" id="save-training-entry" class="button-link do-not-navigate"><?php echo ($SelectedTrainingMatrixID ? "Update" : "Save");?></a>
				<input type="hidden" name="action" value="trainingEntry_<?php echo ($SelectedTrainingMatrixID ? "update" : "add");?>">
			<?php }//if not read only ?>
		</div>
	</form>
<?php }//if not completed ?>
<?php if ($SelectedTrainingMatrixID || $addTraining){?>
		<fieldset id="TrainingEntryDetails">
			<legend>Training Entry Details</legend>
			<div class="sixteen columns">
				<div class="eight columns">
					<form id="trainingActivity" class="basic-form override_skel">
						<fieldset>
							<legend>Activity Details</legend>
							<div class="seven columns">
								<?php $Cost = ($TrainingsByID[$SelectedTrainingID]->cost != '0.00' ? "$".money($TrainingsByID[$SelectedTrainingID]->cost)." ".$TrainingsByID[$SelectedTrainingID]->costType : "" );?>
								Cost: $<input type="text" name="cost" style="width:70px;" value="<?php echo $TrainingActivityResults[0]->cost;?>"> (<?php echo ($Cost ? "If different than ".$Cost : "No cost indicated in training info");?>)<br>
								Employee Paid Amount:$<input type="text" name="employeePaid" style="width:70px;" value="<?php echo $TrainingActivityResults[0]->employeePaid;?>"><br>
								Company Paid Amount:$<input type="text" name="companyPaid" style="width:70px;" value="<?php echo $TrainingActivityResults[0]->companyPaid;?>"><br>
								Expiration Date:<input type="text" name="expirationDate" class="futuredate" value="<?php echo MySqlDate($TrainingActivityResults[0]->expirationDate);?>"><br>
								Location:<input type="text" name="location" style="width:70px;" value="<?php echo $TrainingActivityResults[0]->location;?>"><br>
								Hours Applied:<input type="text" name="hoursApplied" style="width:70px;" value="<?php echo $TrainingActivityResults[0]->hoursApplied;?>"><br>
								Notes:<br><textarea name="notes" style="width:400px;"><?php echo $TrainingActivityResults[0]->notes;?></textarea><br>
								<div style="display:none;">
									MatrixID: <input type="text" name="matrixId" value="<?php echo $SelectedTrainingMatrixID;?>"><br>
									EmployeeID: <input type="text" name="employeeId" value="<?php echo $SelectedEmployeeID;?>"><br>
									TrainingID: <input type="text" name="trainingId" value="<?php echo $SelectedTrainingID;?>"><br>
									ActivityID: <input type="text" name="id" value="<?php echo $TrainingActivityResults[0]->id;?>"><br>
								</div>
							</div>
							<div id="trainingActivityResults" class="seven columns"></div>
							<div class="seven columns">
								<?php if (!$ReadOnlyTrue){?>
									<a href="save-training-activity" id="save-training-activity" class="button-link do-not-navigate">Update Details</a>
									<input type="hidden" name="action" value="trainingActivity_<?php echo ($TrainingActivityResults[0]->id ? "update" : "add");?>">
								<?php }//if not read only ?>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="seven columns">
					<fieldset>
						<legend>Training Details</legend>
						<div class="six columns">
							<div><span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $SelectedTrainingName;?>
								<div class="trainingDetails">
									<?php $TraningDetailExclusionList = array("id","name","code","displayOrderId");
									foreach ($TrainingsByID[$SelectedTrainingID] as $key=>$val){
										if (!in_array($key,$TraningDetailExclusionList)){
											if (substr($key,0,2)=="is"){$key = "Is ".str_replace("is","",$key); $val = ($val ? "Yes" : "No");}
											if ($key == "partOfCertificationId"){$key="Part of Certification"; $val = ($val ? $TrainingsByID[$val]->name : "No");}
											echo ucfirst($key).": ".$val."<br>";
										}
									}
									?>
								</div>
							</div>
							<div class="<?php echo ($_GET["UploadedFile"] ? "opened" : "");?>"><span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> Uploaded Certificates
								<div class="trainingDetails" style="display:<?php echo ($_GET["UploadedFile"] ? "block" : "none");?>;">
									<div id="uploadedCerts"></div>
									<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=edit-file&EmployeeID=<?php echo $SelectedEmployeeID;?>&TrainingMatrixID=<?php echo $SelectedTrainingMatrixID;?>" id="upload" class="button-link">Add File</a>
								</div>
							</div>
							<script>
								$(document).ready(function() {
									$.ajax({
										url: "<?php echo $CurrentServer.$adminFolder;?>tools/ApiFileLibrary.php?action=keyword_search&keyword=EmployeeID&EmployeeID=<?php echo $SelectedEmployeeID;?>",
										type: "GET",
										success: function(data){
											var displayDetails = "";
											$.each(data,function(key,result){
												var fileName = data[key].fileName,
													folderUrl = data[key].folderUrl,
													filePath = folderUrl+fileName,
													fileType = data[key].fileType,
													description = data[key].description,
													title = data[key].title;
													displayDetails = displayDetails + '<span><a href="'+filePath+'" target="file-preview" title="'+title+'"><img src="<?php echo $CurrentServer;?>images/file_pdf.png"></a>'+description+'</span><br>';
											});
											$("#uploadedCerts").html(displayDetails);
										},
										error: function(jqXHR, textStatus, errorThrown){
											var message = $.parseJSON(jqXHR.responseText);
											$("#uploadedCerts").html(message).addClass("alert");
										}
									});

								});
							</script>
						</div>
					</fieldset>
					<br clear="all">
					<?php //print_pre($trainingMatrixHistoryCollection);?>
					<fieldset>
						<legend>Entry history</legend>
						<div class="six columns">
							<?php 
								$affectiveDate = (MySQLDate($trainingMatrixHistoryCollection[0]->affectiveDate) ? MySQLDate($trainingMatrixHistoryCollection[0]->affectiveDate) : date("m/d/Y g:i a", strtotime($trainingMatrixHistoryCollection[0]->timeStamp)));
								
							?>
							<div><span<?php if (count($trainingMatrixHistoryCollection) > 1){echo " class=\"details-open\"";}?>>&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $trainingMatrixHistoryCollection[0]->status;?> <?php echo $affectiveDate;?> 
								<div class="trainingDetails">
									<hr>
									<?php $TraningDetailExclusionList = array("id","name","code","displayOrderId");
									foreach ($trainingMatrixHistoryCollection as $HistoryObj){
										$affectiveDate = (MySQLDate($HistoryObj->affectiveDate) ? MySQLDate($HistoryObj->affectiveDate) : date("m/d/Y g:i a",strtotime($HistoryObj->timeStamp)));

										//ignore the first entry
											echo "<div title='Entered By ".$HistoryObj->addedByAdmin."'>".$HistoryObj->status." ".$affectiveDate."</div>";
									}
									?>
								</div>
							</div>
						</div>
					</fieldset>
					<br clear="all">
					<fieldset>
						<legend>Employee Details</legend>
						<div class="six columns">
							<div class="two columns">
								<img class="picture_id1" src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
								<img class="picture_id2" src="https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
							</div>
							<span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $SelectedEmployeeName;?>
							<div class="trainingDetails">
								<?php $TraningDetailExclusionList = array("id","fullName","code","displayOrderId","trainingCodes");
								foreach ($EmployeeByID[$SelectedEmployeeID] as $key=>$val){
									if (!in_array($key,$TraningDetailExclusionList)){
										if (substr($key,0,2)=="is"){$key = "Is ".str_replace("is","",$key); $val = ($val ? "Yes" : "No");}
										if (substr($key,-4)=="Date"){$key = str_replace("Date"," Date",$key); $val = MySQLDate($val);}
										if (substr($key,-3)=="GBS"){$key = "Hours Tracking in GBS"; $val = ($val ? "Yes" : "No");}
										echo ucfirst($key).": ".$val."<br>";
									}
								}
								?>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
<?php }?>
<script>
$(function(){
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});
	var futuredateClass = $(".futuredate");
	futuredateClass.width(80);
	futuredateClass.datepicker();

	var statusListEdit = $("#statusListEdit"),
		statusListEditIcon = $("#statusListEditIcon"),
		statusEditListEntry = $("#statusEditListEntry"),
		status = $("#Status"),
		statusEditListInput = $("#statusEditListInput");
		
	statusListEdit.css("visibility","hidden");
	statusListEditIcon.on('click',function(){
		if (statusListEdit.css("visibility") == "visible"){
			statusListEdit.css("visibility","hidden");
		}else{
			statusListEdit.css("visibility","visible");
		}
	});
	<?php if ($showForm){?>
	$( ".comboboxStatus" ).combobox({
		select: function(event, ui){
			statusListEdit.css("visibility","hidden");
		}
	});
	<?php }?>
	
	
	
	<?php if (!$ReadOnlyTrue){?>
		$("#editStatus").click(function(e){
			$('#statusResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			var jsondata = '{"action":"status_add","status":"'+statusEditListInput.val()+'"}';
			$.ajax({
				url: "ApiTrainingsManagement.php",
				type: "PUT",
				data: jsondata,
				success: function(data){
					//console.log(data);
					$('#statusResults').html("Status Added").addClass("alert").addClass("info");
					//now update the pulldown and auto save the employee information
					$('#Status').append($("<option></option>").attr("value", statusEditListInput.val()).text(statusEditListInput.val())).val(statusEditListInput.val());
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#statusResults').html(message).addClass("alert");
				}
			});
		});
	<?php }?>
	
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
		
	 var formElement = $("#TrainingUpdateForm");
	 
	 formElement.validate({
        rules: {
            trainingId: {
                required: true
            },
            employeeId: {
                required: true
            },
            status: {
                required: true
            },
			affectiveDate: {
				required: true
			}
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-training-entry").click(function(e){

        $('#trainingEntryResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiTrainingsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					//console.log(data)
                    $('#trainingEntryResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#trainingEntryResults').html(message).addClass("alert");
                }
            });
        }
    });
	var activityFormElement = $("#trainingActivity");
	
    $("#save-training-activity").click(function(e){

        $('#trainingActivityResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
		//console.log(JSON.stringify(activityFormElement.serializeObject()));
		$.ajax({
			url: "ApiTrainingsManagement.php",
			type: "PUT",
			data: JSON.stringify(activityFormElement.serializeObject()),
			success: function(data){
				//console.log(data);
				$('#trainingActivityResults').html("Saved").addClass("alert").addClass("info");
			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				$('#trainingActivityResults').html(message).addClass("alert");
			}
		});
    });

	
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	
	$('#TrainingEntryDetails').on('click', 'span.details-open', function () {
			var div = $(this).parent('div');
			var details = $(this).next('div');

			if ( details.is(":visible") ) {
				details.hide();
				div.removeClass('opened');
			}
			else {
				details.show();
				div.addClass('opened');
			}
		} );

});
</script>