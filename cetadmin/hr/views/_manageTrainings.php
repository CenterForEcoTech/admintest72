<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");



include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResultCategories = $hrEmployeeDepartmentsProvider->getCategories($criteria);
$resultArrayCategories = $paginationResultCategories->collection;
foreach ($resultArrayCategories as $result=>$record){
	if ($record->activeId){
		$CategoriesByName[$record->name] = $record;
		$Categories[$record->id] = $record->name;
	}
}	


include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
$categoryNamesDefaults = array("Accounting/Finance","Admin/Clerical","Customer Service","Engineering and Technical","HR Administration","IT/Computer","Leadership and Management","Marketing and Sales","Materials Management","Personal and Team Skills","Quality Assurance","Retail","Safety");
foreach ($categoryNamesDefaults as $catName){
	if (trim($catName)){
		$categoryNames[] = trim($catName);
	}
}
$purposeNames = array("CET/Job requirement","Employee development","Regulatory");
$eventNames = array("Orientation","Retraining","Scheduled");
$deliveryMethods = array("External","In-house","Web-based");
$TrainingsCount = 0;
foreach ($resultArray as $result=>$record){
	if ($record->activeStatus == "Active"){
		$TrainingsByID[$record->id] = $record;
		$TrainingsByCatID[$record->categoryId][] = $record;
		if (!$record->categoryId){
			$TrainingsActive[$record->name]=$record;
		}
		$TrainingsCount++;
		$categories = explode(",",$record->categories);
		foreach ($categories as $category){
			if (!in_array(trim($category),$categoryNames) && trim($category)!=''){
				$categoryNames[] = trim($category);
			}
		}
		if ($record->isCertification){
			$Certifications[] = $record;
		}
		if ($record->partOfCertificationId){
			$TrainingsByCertification[$record->partOfCertificationId][] = $record;
		}
	}else{
		$TrainingsInActive[$record->name]=$record;
	}
}
ksort($TrainingsActive);
ksort($categoryNames);
$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

//resort the products without display order by TrainingID
$SelectedTrainingID = $_GET['TrainingID'];
$addTraining = $_GET['addtraining'];
$displayOrderId = $TrainingsByID[$SelectedTrainingID]->displayOrderId;
if ($addTraining){$displayOrderId = ($TrainingsCount+1);}

//get employees with this training
if ($SelectedTrainingID){
	include_once($dbProviderFolder."HREmployeeProvider.php");
	$hrEmployeeProvider = new HREmployeeProvider($dataConn);
	$paginationResult = $hrEmployeeProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		//only active employees
		if ($record->displayOrderId > 0 && $record->endDate == "0000-00-00"){
			$TrainingEmployees[$record->fullName]["id"] = $record->id;
			$TrainingEmployees[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
			$EmployeesByID[$record->id]= $record;
		}

	}

	ksort($TrainingEmployees);
	
	//get matrix of employees with this training
	$criteria = new stdClass();
	$criteria->trainingId = $SelectedTrainingID;
	$criteria->statusNot = "Removed";
	$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$TrainingMatrixCollection = $TrainingMatrixResults->collection;
	if (count($TrainingMatrixCollection)){
		foreach ($TrainingMatrixCollection as $id=>$collection){
			$TrainingMatrix[] = $collection;
			if ($EmployeesByID[$collection->employeeId]){
				$EmployeesActive[$collection->employeeFullName]=$EmployeesByID[$collection->employeeId];
				$EmployeesByDepartmentUnsorted[$EmployeesByID[$collection->employeeId]->department][]=$EmployeesByID[$collection->employeeId];
				$MatrixInfo[$collection->employeeId][] = $collection;
			}
		}
	}

	include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
	$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
	$criteria = new stdClass();
	$criteria->showAll = true;
	$criteria->noLimit = true;
	$paginationResult = $hrEmployeeDepartmentsProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	foreach ($resultArray as $result=>$record){
		$DepartmentsByName[$record->name] = $record;
		$EmployeesByDepartment[$record->name] = $EmployeesByDepartmentUnsorted[$record->name];
		
	}
	
}//end if selected Training
?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4, #sortable5, #sortable6, #sortable7, #sortable8 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:150px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 {width: 800px;height:300px;}
  #sortable3 {border: 2px solid #6d9eeb;height:367px;}
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li, #sortable5 li, #sortable6 li, #sortable7 li, #sortable8 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
	cursor:pointer;
  }
  #sortable1 li {width:760px;}
  div.orgChart div.hasChildren {background-color      : #ffffcf;}
	<?php
		$DepartmentCount = 0;
		foreach ($EmployeesByDepartment as $DepartmentName=>$EmployeeInfo){
			echo "div.orgChart div.node.level1-node".$DepartmentCount." {background-color: ".$DepartmentsByName[$DepartmentName]->color.";}";
			$DepartmentCount++;
		}
	?>

  </style>
<?php if (!$addTraining){?>
	<?php //print_pre($TrainingsByCatID);?>
	<div class="fifteen columns"><bR>
		<button id="showTrainingsSelectForm">Choose A Different Training</button>
	</div>
	<fieldset id="trainingsSelectForm">
		<legend>Choose A Training In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="twelve columns"> 
				<div class="three columns"><b>Trainings</b></div><Br>
				<ul id="sortable1" class="connectedSortable">
				categories:<br>
				<?php 
					foreach ($CategoriesByName as $catName=>$catInfo){
						echo "<li id=\"".$catInfo->id."\" class=\"ui-state-default ui-state-highlight\"><a href=\"#\" class='trainingCategory' data-catid='".$catInfo->id."'>".$catName."</a></li>";
						foreach ($TrainingsByCatID[$catInfo->id] as $training=>$trainingInfo){
							echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default catid".$catInfo->id."\" style='display:none;margin-left:20px;width:90%;'><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
						}
					}
					echo "<hr>non-categorized trainings:<br>";

				
				
					foreach ($TrainingsActive as $training=>$trainingInfo){
						echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
					}
					echo "<hr>inactive trainings:<br>";
					foreach ($TrainingsInActive as $training=>$trainingInfo){
						echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedTrainingID || $addTraining){?>
	<form id="TrainingsUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Training Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addTraining){?><input type="text" name="id" id="trainingId" value="<?php echo $TrainingsByID[$SelectedTrainingID]->id;?>"><?php }?>
				</div>
				<div class="twelve columns">
					<div class="three columns">Name:</div><div class="eight columns"><input type="text" name="name" id="trainingName" value="<?php echo $TrainingsByID[$SelectedTrainingID]->name;?>" class="formItem"><br>
						<input type="checkbox" id="isCertification_checkbox"<?php if($TrainingsByID[$SelectedTrainingID]->isCertification){ echo " checked";}?> class="formItem"><span id="TrainingName"><?php echo $TrainingsByID[$SelectedTrainingID]->name;?></span> Is A Certification
						<input type="hidden" name="isCertification" id="isCertification_input" value="<?php echo $TrainingsByID[$SelectedTrainingID]->isCertification;?>" class="formItem"><Br>
						<input type="checkbox" id="isAnnuallyRequired_checkbox"<?php if($TrainingsByID[$SelectedTrainingID]->isAnnuallyRequired){ echo " checked";}?> class="formItem"> Is Required Annually (Expires after 1 year)
						<input type="hidden" name="isAnnuallyRequired" id="isAnnuallyRequired_input" value="<?php echo $TrainingsByID[$SelectedTrainingID]->isAnnuallyRequired;?>" class="formItem">
						
					</div>
					<br clear="all"><br>
					<div class="three columns tagLabel">Description:</div><div class="four columns"><textarea name="description" value="<?php echo $TrainingsByID[$SelectedTrainingID]->description;?>" class="formItem"><?php echo $TrainingsByID[$SelectedTrainingID]->description;?></textarea></div>
					<div style="display:none;">
						<br clear="all"><br>
						<div class="two columns">Code:</div><div class="four columns"><input type="text" name="code" id="trainingCode" value="<?php echo $TrainingsByID[$SelectedTrainingID]->code;?>" class="formItem"></div>
						<div id="codeCheck" class="five columns"></div>
					</div>
					<br clear="both">

					<div class="three columns"><a href="?nav=manage-categories&CategoryID=<?php echo $TrainingsByID[$SelectedTrainingID]->categoryId;?>" title="Click to edit information about this category type">Category</a>:</div>
					<div class="four columns">
						<select name="categoryId" id="categoryId">
								<option value="">Choose a Category Type</option>
							<?php
								foreach ($Categories as $CategoryId=>$Category){
									echo "<option value='".$CategoryId."'".($CategoryId==$TrainingsByID[$SelectedTrainingID]->categoryId ? " selected":"").">".$Category."</option>";
								}
							?>
						</select>
					</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-categories"><span class="edit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
					<br clear="both">
							
					<!--
					<div class="three columns"><a <?php if ($TrainingsByID[$SelectedTrainingID]->partOfCertificationId){?>href="?nav=manage-trainings&TrainingID=<?php echo $TrainingsByID[$SelectedTrainingID]->partOfCertificationId;?>" title="Click to edit information about this certification"<?php }else{echo "name='partcert'";}?>>Part of Certification</a>:</div>
					<div class="four columns">
						<select name="partOfCertificationId" class="formItem">
								<option value="">Choose a Certification</option>
							<?php
								foreach ($Certifications as $Certification){
									echo "<option value='".$Certification->id."'".($Certification->id==$TrainingsByID[$SelectedTrainingID]->partOfCertificationId ? " selected":"").">".$Certification->name."</option>";
								}
							?>
						</select>
					</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-trainings"><span class="edit ui-icon ui-icon-pencil" title="click to edit list - update trainings to indicate it is a certification to be included in this list"></span></a><?php }?>
					<br clear="both">
					-->
					
					<div class="three columns">Delivery Method:</div>
					<div class="four columns">
						<select name="deliveryMethod" class="formItem">
								<option value="">Choose a Delivery Method</option>
							<?php
								foreach ($deliveryMethods as $deliveryMethod){
									echo "<option value='".$deliveryMethod."'".($deliveryMethod==$TrainingsByID[$SelectedTrainingID]->deliveryMethod ? " selected":"").">".$deliveryMethod."</option>";
								}
							?>
						</select>
					</div>
					<br clear="both">
					<div class="three columns">Delivery Details:</div>
					<div class="four columns">
						<textarea name="deliveryDetails" class="formItem"><?php echo $TrainingsByID[$SelectedTrainingID]->deliveryDetails;?></textarea>
					</div>
					<br clear="both">
					<div class="three columns">Cost:</div>
					<div class="eight columns">
						<?php 
							$CostType = (trim($TrainingsByID[$SelectedTrainingID]->costType) != '' ? $TrainingsByID[$SelectedTrainingID]->costType : "PerPerson");
						?>
						$<input type="text" name="cost" id="trainingCost" value="<?php echo money($TrainingsByID[$SelectedTrainingID]->cost);?>" class="formItem" style="width:70px;">
						<input type="radio" name="costType" value="PerPerson"<?php echo ($CostType == "PerPerson" ? " checked" : "");?>>Per Person &nbsp;&nbsp;<input type="radio" name="costType" value="FlatFee"<?php echo ($CostType == "FlatFee" ? " checked" : "");?>>Flat Fee
					</div>
					<br clear="both">
					<div class="three columns">Status:</div>
					<div class="four columns">
						<select name="activeStatus" class="formItem">
							<?php
								echo "<option value='Active'".($TrainingsByID[$SelectedTrainingID]->activeStatus == "Active" ? " selected":"").">Active</option>";
								echo "<option value='Inactive'".($TrainingsByID[$SelectedTrainingID]->activeStatus == "Inactive" ? " selected":"").">Inactive</option>";
							?>
						</select>
					</div>
				</div>	
				<br clear="both">
				<div style="display:none;">
					<hr>
					<div class="fifteen columns">
						<div class="eight columns">
							<b>Currently Applied Catagories</b><Br><span style="font-size:10pt;">(Drag or type to add. Click x to remove)</span>
							<input type="text" title="click to create new category" name="categories" id="trainingCategories" value="<?php echo $TrainingsByID[$SelectedTrainingID]->categories;?>" class="formItem">
						</div>
						<div class="six columns">
							<div class="five columns"><b>Choose From All Categories</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
							<div class="six columns">
								<ul id="sortable7" class="connectedSortable">
								<?php 
									foreach ($categoryNames as $catName){
										echo "<li id=\"".$catName."\" data-tagit=\"trainingCategories\" class=\"ui-state-default\">".$catName."</a></li>";
									}
								?>
								</ul>
							</div>
						</div>
					</div>
					<br clear="both">
					<hr>
					<div class="fifteen columns">
						<div class="eight columns">
							<b>Currently Applied Purposes</b><Br><span style="font-size:10pt;">(Drag or type to add. Click x to remove)</span>
							<input type="text" title="click to create new purpose" name="purposes" id="trainingPurposes" value="<?php echo $TrainingsByID[$SelectedTrainingID]->purposes;?>" class="formItem">
						</div>
						<div class="six columns">
							<div class="five columns"><b>Choose From All Purposes</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
							<div class="six columns">
								<ul id="sortable6" class="connectedSortable">
								<?php 
									foreach ($purposeNames as $purpName){
										echo "<li id=\"".$purpName."\" data-tagit=\"trainingPurposes\" class=\"ui-state-default\">".$purpName."</a></li>";
									}
								?>
								</ul>
							</div>
						</div>
					</div>
					<br clear="both">
					<hr>
					<div class="fifteen columns">
						<div class="eight columns">
							<b>Currently Applied Events</b><Br><span style="font-size:10pt;">(Drag or type to add. Click x to remove)</span>
							<input type="text" title="click to create new event" name="events" id="trainingEvents" value="<?php echo $TrainingsByID[$SelectedTrainingID]->events;?>" class="formItem">
						</div>
						<div class="six columns">
							<div class="five columns"><b>Choose From All Events</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
							<div class="six columns">
								<ul id="sortable8" class="connectedSortable">
								<?php 
									foreach ($eventNames as $eventName){
										echo "<li id=\"".$eventName."\" data-tagit=\"trainingEvents\" class=\"ui-state-default\">".$eventName."</a></li>";
									}
								?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
		<div id="trainingsResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings&TrainingID=<?php echo $SelectedTrainingID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addTraining){?>
					<a href="save-training-add" id="save-training-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="training_add">
				<?php }else{?>
					<a href="save-training-update" id="save-training-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="training_update" id="saveAction">
					&nbsp;&nbsp;
					<a href="copy-training" title="Copy all details, including assigned staff, except for Training Name" id="copy-training" class="button-link do-not-navigate">Copy This Training</a>
					<input type="hidden" value="<?php echo $TrainingsByID[$SelectedTrainingID]->id;?>" id="copyFromId">
					<div id="dialog-confirm" title="Copy this training" style="display:none;">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Have you put in a new name for the copied training?</p>
					</div>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="delete-training" id="delete-training" class="button-link do-not-navigate" style="color:red;">Delete This Training</a>
						<div id="dialog-confirmDelete" title="Delete this training" style="display:none;">
							<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Deleting this will remove all staff records of this training also.  Are you sure you want to delete this training?</p>
						</div>					
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<?php if ($SelectedTrainingID){?>
			<?php if ($TrainingsByID[$SelectedTrainingID]->isCertification){?>
				<br clear="both">
				<br clear="both">
				<fieldset>
					<legend>Trainings Part of This Certification</legend>
					<div class="fifteen columns">
						<?php 
						foreach ($TrainingsByCertification[$SelectedTrainingID] as $TrainingInfo){
								echo "<a href=\"?nav=manage-trainings&TrainingID=".$TrainingInfo->id."\">".$TrainingInfo->name."</a><br>";
						}
						?>
					</div>
				</fieldset>
			<?php }//end if certification?>
			<input type="hidden" id="picture_id1result">
			<input type="hidden" id="picture_id2result">
			<fieldset>
				<legend>Assign Employees to this Training</legend>
				<div class="fifteen columns">
					<div class="three columns" id="trainingMessage">&nbsp;</div>
					<div class="ten columns">
						Effective Date: <input type="text" class="date" id="AffectiveDate" name="affectiveDate" value="">&nbsp;&nbsp;&nbsp;
						Status <select id="Status" name="status">
								<option value=""></option>
							<?php
								foreach ($TrainingStatusList as $TrainingStatus){
									echo "<option value=\"".$TrainingStatus->name."\">".$TrainingStatus->name."</option>";
								}
							?>
						</select>
						<br>(Indicate Effective Date and Status to display All Employees and Departments)
					</div>
					<div class="six columns"> 
						<div class="five columns"><b>Currently Assigned Employees</b><Br><span style="font-size:10pt;">&nbsp;</span></div>
						<ul id="sortable3" class="connectedSortable">
						<?php 
							ksort($EmployeesActive);
							foreach ($EmployeesActive as $employee=>$employeeInfo){
								foreach ($MatrixInfo[$employeeInfo->id] as $matrixInfo){
									$affectiveTimeStamp = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLTimeStampDisplay($matrixInfo->timeStamp));
									$affectiveDate = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLDate($matrixInfo->timeStamp));
									echo "<li id=\"".$employeeInfo->id."\" data-matrixId=\"".$matrixInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$employeeInfo->fullName." <span class='certDescription' title='".$matrixInfo->status." ".$affectiveTimeStamp." by ".$matrixInfo->addedByAdmin." added into the system on ".MySQLTimeStampDisplay($matrixInfo->timeStamp)."'>".$matrixInfo->status." ".$affectiveDate."</span></a></li>";
								}
							}
						?>
						</ul>
					</div>
					<div class="one columns">&nbsp;</div>
					<div class="six columns" id="EmployeeAndDepartmentSelector" style="display:block;">
						<div class="five columns"><b>All Employees</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
						<div class="six columns">
							<ul id="sortable4" class="connectedSortable">
							<?php 
								foreach ($EmployeesByID as $employee=>$employeeInfo){
									echo "<li id=\"".$employeeInfo->id."_".urlencode($employeeInfo->fullName)."\" class=\"ui-state-default\"><a href=\"?nav=manage-employees&EmployeeID=".$employeeInfo->id."\">".$employeeInfo->fullName."</a></li>";
								}
							?>
							</ul>
						</div>
						<br clear="all">
						<br clear="all">
						<div style="display:none;">
							<div class="five columns"><b>All Departments</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
							<div class="six columns">
								<ul id="sortable5" class="connectedSortable">
								<?php 
									foreach ($EmployeesByDepartment as $Department=>$EmployeeInfo){
										echo "<li id=\"".$Department."\" data-departmentname=\"".$Department."\" class=\"ui-state-default\"><a href=\"?nav=manage-departments&DepartmentName=".$Department."\">".$Department."</a></li>";
									}
								?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Employees with this Training</legend>
				<div class="fifteen columns">
					<?php 
					if (count($TrainingEmployees)){
						include('_EmployeeComboBox.php');
					}
					echo "<br clear=\"all\">";
					?>
						<ul id="orgchart-source" style="display:none;">

							<?php
								echo "<li>".$TrainingsByID[$SelectedTrainingID]->name;
								echo "<ul>";
								foreach ($EmployeesByDepartment as $Department=>$EmployeeInfo){
									echo "<li><a href=\"?nav=manage-departments&DepartmentName=".$Department."\">".$Department."</a>";
									if (count($EmployeeInfo)){
										echo "<ul>";
										foreach($EmployeeInfo as $ID=>$Info){
											echo "<li><a href=\"?nav=manage-employees&EmployeeID=".$Info->id."\">".$Info->fullName."</a></li>";
										}
										echo "</ul>";
									}
									echo "</li>";
								}
								echo "</ul>";
							?>
						</ul>
				</div>
				<br clear="all"><br>
				<div class="fifteen columns">
					<div id="orgchart-container" class="reset-this"></div>
				</div>
			</fieldset>
<?php }//end if selectedTrainingID ?>
	</form>
	
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings&addtraining=true" class="button-link">+ Add New Training</a>
		</div>
	<?php }?>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>
<script>
$(function(){
	$(".trainingCategory").on('click',function(){
		var $this = $(this),
			thisCatId = $this.attr('data-catid'),
			thisClass = ".catid"+thisCatId;
			
			$(thisClass).toggle();
	});
	
	
	$('#orgchart-source').orgChart({
		container: $('#orgchart-container'),
		showLevels: 2,
		stack: true,
		depth: 2
	});

	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0,
		onSelect: function(selected,evnt){
			$("#EmployeeAndDepartmentSelector").hide();
			$("#Status").val('');
		}
	});

	var trainingCategories = $("#trainingCategories");
	trainingCategories.tagit({
		availableTags:<?php echo json_encode($categoryNames);?>
	});
	var trainingPurposes = $("#trainingPurposes");
	trainingPurposes.tagit({
		availableTags:<?php echo json_encode($purposeNames);?>
	});
	var trainingEvents = $("#trainingEvents");
	trainingEvents.tagit({
		availableTags:<?php echo json_encode($eventNames);?>
	});
	
	var trainingId = $("#trainingId"),
		trainingName = $("#trainingName"),
		trainingCode = $("#trainingCode");
	
	trainingCode.blur(function(){
		//check to makesure code isn't already being used.
		var jsondata = '{"action":"check_code","code":'+trainingCode.val()+'"}';
		$.ajax({
			url: "ApiTrainingsManagement.php",
			type: "GET",
			data: {
				action : "check_code",
				code : trainingCode.val()
			},
			success: function(data){
				if (data.found =="yes" && data.id !=trainingId.val()){
					$('#codeCheck').html("Code assigned to "+data.name).addClass("alert");
					trainingCode.focus();
				}else{
					$('#codeCheck').html("").removeClass("alert");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				var message = $.parseJSON(jqXHR.responseText);
				$('#codeCheck').html(message).addClass("alert");
			}
		});
	});
	
	trainingName.keyup(function(){
		$("#TrainingName").html($(this).val());
	});
			
	$("#isCertification_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#isCertification_input").val(1);
		}else{
			$("#isCertification_input").val(0);
		}
	});		
	$("#isAnnuallyRequired_checkbox").change(function(){
		var $this = $(this);
		if($this.prop('checked')){
			$("#isAnnuallyRequired_input").val(1);
		}else{
			$("#isAnnuallyRequired_input").val(0);
		}
	});		

	$("#showTrainingsSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedTrainingID){?>
		$("#trainingsSelectForm").hide();
		$("#showTrainingsSelectForm").show();
	<?php }?>
	$("#showTrainingsSelectForm").click(function(){
		$("#trainingsSelectForm").show();		
		$(this).hide();
	});

	$("#trainingsSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#TrainingsUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
            director: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-training-update").click(function(e){
        $('#trainingsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiTrainingsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
					console.log(data);
                    $('#trainingsResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#trainingsResults').html(message).addClass("alert");
                }
            });
        }
    });
	var AffectiveDate = $("#AffectiveDate");
	var Status = $("#Status");
	Status.on('change',function(){
		if (AffectiveDate.val()){
			$("#EmployeeAndDepartmentSelector").show();
		}
	});
	
	var copyTraining = $("#copy-training");
    copyTraining.click(function(e){
        $('#trainingsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
		$( "#dialog-confirm" ).show();
		var data = {};
			data["action"] = "training_copy";
			data["name"] = $("#trainingName").val(),
			data["id"] = $("#copyFromId").val(),
			data["affectiveDate"] = $("#AffectiveDate").val(),
			data["status"] = $("#Status").val();
			
		$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:250,
		  width:640,
		  modal: true,
		  buttons: {
			"Yes and assign same staff": function() {
			  $( this ).dialog( "close" );
				$.ajax({
					url: "ApiTrainingsManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$('#trainingsResults').html("Copied...page will refresh").addClass("alert").addClass("info");
						copyTraining.hide();
						window.setTimeout(function(){
							window.location.href = "<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings&TrainingID="+data;
						},3000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#trainingsResults').html(message).addClass("alert");
					}
				});
				
			},
			"Yes and do not assign staff": function(){
			  $( this ).dialog( "close" );
				data["doNotCopyStaff"] = true;
				$.ajax({
					url: "ApiTrainingsManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$('#trainingsResults').html("Copied...page will refresh").addClass("alert").addClass("info");
						copyTraining.hide();
						window.setTimeout(function(){
							window.location.href = "<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings&TrainingID="+data;
						},3000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#trainingsResults').html(message).addClass("alert");
					}
				});
				
			},
			"Wait!  I forgot to change the Effective Date and Status": function(){
			  $( this ).dialog( "close" );
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});	
	});
	var DeleteTraining = $("#delete-training");
    DeleteTraining.click(function(e){
        $('#trainingsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
		$( "#dialog-confirmDelete" ).show();
		var data = {};
			data["action"] = "training_delete";
			data["name"] = $("#trainingName").val(),
			data["id"] = $("#copyFromId").val(),
		$( "#dialog-confirmDelete" ).dialog({
		  resizable: false,
		  height:250,
		  width:640,
		  modal: true,
		  buttons: {
			"Yes": function() {
			  $( this ).dialog( "close" );
				$.ajax({
					url: "ApiTrainingsManagement.php",
					type: "PUT",
					data: JSON.stringify(data),
					success: function(data){
						$('#trainingsResults').html("Deleted...page will refresh").addClass("alert").addClass("info");
						DeleteTraining.hide();
						window.setTimeout(function(){
							window.location.href = "<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-trainings";
						},3000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#trainingsResults').html(message).addClass("alert");
					}
				});
				
			},
			"No": function(){
			  $( this ).dialog( "close" );
			}
		  }
		});	
	});
	
	var formItems = $(".formItem");
    $("#save-training-add").click(function(e){

        $('#trainingsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiTrainingsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#trainingsResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));										
					formItems.each(function(){
						var $this = $(this);
						$this.val('');
					});
					trainingCategories.tagit("removeAll");
					trainingPurposes.tagit("removeAll");
					trainingEvents.tagit("removeAll");					
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#trainingsResults').html(message).addClass("alert");
                }
            });
        }
    });
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});


});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");
	var sortable5 = $("#sortable5");
	var sortable6 = $("#sortable6");
	var sortable7 = $("#sortable7");
	var sortable8 = $("#sortable8");
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiTrainingsManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedTrainingID){?>
			$( "#sortable3, #sortable4, #sortable5" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4, #sortable5" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedEmployees = new Array();
					var assignedDepartments = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisDepartmentName = $thisLi.attr('data-departmentname');
						if (thisDepartmentName){
							assignedDepartments.push($thisLi.attr('id'));
						}else{
							assignedEmployees.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedEmployees.length){
						var AffectiveDate = $("#AffectiveDate").val();
							Status = $("#Status").val();
						DisplayArray.push({action: 'Insert_TrainingMatrix',affectiveDate: AffectiveDate, status: Status, trainingId:<?php echo $SelectedTrainingID;?>,trainingName:'<?php echo $TrainingsByID[$SelectedTrainingID]->name;?>',trainingDisplayOrderId:<?php echo $TrainingsByID[$SelectedTrainingID]->displayOrderId;?>,items:assignedEmployees});
						//DisplayArray.push({action: 'Insert_TrainingMatrix',employeeId:<?php echo $SelectedEmployeeID;?>,employeeFullName:'<?php echo $EmployeeByID[$SelectedEmployeeID]->fullName;?>',items:assignedTrainings});
					}
					if (assignedDepartments.length){
						var AffectiveDate = $("#AffectiveDate").val();
							Status = $("#Status").val();
						DisplayArray.push({action: 'Insert_TrainingMatrixByDepartment',affectiveDate: AffectiveDate, status: Status, trainingId:<?php echo $SelectedTrainingID;?>,trainingName:'<?php echo $TrainingsByID[$SelectedTrainingID]->name;?>',trainingDisplayOrderId:<?php echo $TrainingsByID[$SelectedTrainingID]->displayOrderId;?>,items:assignedDepartments});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						console.log(DisplayArray);
						$.ajax({
							url: "ApiEmployeeManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								console.log(data);
								$('#trainingMessage').fadeIn().html("Trainings "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#trainingMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
		<?php }//end if selectedTraininID ?>
			
		<?php if ($SelectedTrainingID || $addTraining){?>
			$( "#sortable6, #sortable7" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable6, #sortable7, #sortable8" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisTagit = "#"+$thisLi.attr('data-tagit');
							$(thisTagit).tagit('createTag', $thisLi.attr('id'));
					});
					$("#save-training-update").click();
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
				}	  
			}).disableSelection();
			
		<?php }//end if $SelectedTrainingID || addTraining?>
	
		
				
<?php }//not in readonly?>
	
});
</script>