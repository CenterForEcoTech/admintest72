<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeDepartmentsProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentsByID[$record->id] = $record;
	$DepartmentsActive[$record->displayOrderId]=$record;
	$DepartmentByName[$record->name] = $record;
}
include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	$TrainingsActive[$record->displayOrderId]=$record;
}

$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = false;
$criteria->isManager = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentDirectors[] = $record->firstName." ".$record->lastName;
}
//resort the products without display order by DepartmentID
$SelectedDepartmentID = $_GET['DepartmentID'];
$SelectedDepartmentName = $_GET['DepartmentName'];
if ($SelectedDepartmentName && !$SelectedDepartmentID){
	$SelectedDepartmentID = $DepartmentByName[$SelectedDepartmentName]->id;
}

$addDepartment = $_GET['adddepartment'];
$displayOrderId = $DepartmentsByID[$SelectedDepartmentID]->displayOrderId;
if ($addDepartment){$displayOrderId = (count($DepartmentsActive)+1);}

//get employees in the department
if ($SelectedDepartmentID){
	$criteria->showAll = false;
	$criteria->isManager = false;
	$criteria->department = $DepartmentsByID[$SelectedDepartmentID]->name;
	$paginationResult = $hrEmployeeProvider->get($criteria);
	$resultArray = $paginationResult->collection;
	
	foreach ($resultArray as $result=>$record){
		if ($record->displayOrderId > 0){
			$EmployeesActive[$record->fullName]=$record;
			$DepartmentEmployees[$record->fullName]["id"] = $record->id;
			$DepartmentEmployees[$record->fullName]["name"] = $record->firstName." ".$record->lastName;
			$DepartmentEmployeesByManager[$record->manager][] = $record;
		}
	}
	ksort($DepartmentEmployees);
	//print_pre($DepartmentEmployees);

	$managerCount = count($DepartmentEmployeesByManager);
	$orgChartResults = $hrEmployeeProvider->getOrgChart($criteria,$managerCount);
	//print_pre($orgChartResults);
	$orgChartLayout = $hrEmployeeProvider->createOrgChartLayout($orgChartResults,$managerCount,$DepartmentsByID[$SelectedDepartmentID]->name,$DepartmentsByID[$SelectedDepartmentID]->director);
}//end if selected Department


?>
<?php if (!$addDepartment){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>css/colorpicker.css">
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 340px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:140px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 300px;
	font-size:small;
	height:12px;
  }
  .employeeDescription {font-weight:normal;}
  .employeeName {font-weight:bold;}
  div.orgChart{background-color: <?php echo ($DepartmentsByID[$SelectedDepartmentID]->color ? $DepartmentsByID[$SelectedDepartmentID]->color  : "#ffffe8");?>;}

  </style>
	<div class="fifteen columns"><bR>
		<button id="showDepartmentsSelectForm">Choose A Different Department</button>
	</div>
	<fieldset id="departmentsSelectForm">
		<legend>Choose A Department In The System or Drag to Change Display Order</legend>
		<div class="fifteen columns"> 
			<div class="five columns">&nbsp;</div><div class="three columns" id="updateMessage">&nbsp;</div><div class="four columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="three columns"><b>Departments</b></div>
				<ul id="sortable1" class="connectedSortable">
				<?php 
					foreach ($DepartmentsActive as $department=>$departmentInfo){
						echo "<li id=\"".$departmentInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-departments&DepartmentID=".$departmentInfo->id."\">".$departmentInfo->name."</a></li>";
					}
				?>
				</ul>
			</div>
		</div> 
	</fieldset>
<?php }?>
<?php if ($SelectedDepartmentID || $addDepartment){?>
	<form id="DepartmentsUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Department Details</legend>
			<div class="fifteen columns">
					<input type="hidden" name="displayOrderId" id="displayOrderId" value="<?php echo $displayOrderId;?>">

				<div style="display:none;">
					<?php if (!$addDepartment){?><input type="text" name="id" value="<?php echo $DepartmentsByID[$SelectedDepartmentID]->id;?>"><?php }?>
				</div>

				<div class="fifteen columns">
					<fieldset>
						<legend>Details</legend>
						<div class="seven columns">
							<div class="two columns">Name:</div><div class="four columns"><input type="text" name="name" value="<?php echo $DepartmentsByID[$SelectedDepartmentID]->name;?>"></div>
							<br clear="all">
							<div class="two columns"><a href="?nav=manage-employees&EmployeeName=<?php echo $DepartmentsByID[$SelectedDepartmentID]->director;?>" title="click to edit information about <?php echo $DepartmentsByID[$SelectedDepartmentID]->director;?>">Director</a>:</div>
							<div class="two columns">
								<select name="director">
										<option value="">Choose a Director</option>
									<?php
										foreach ($DepartmentDirectors as $Director){
											echo "<option value='".$Director."'".($Director==$DepartmentsByID[$SelectedDepartmentID]->director ? " selected":"").">".$Director."</option>";
										}
									?>
								</select>
							</div><?php if (!$ReadOnlyTrue){?><a href="?nav=manage-employees"><span class="edit ui-icon ui-icon-pencil" title="click to edit list - update employee to indicate they are a manager to be included in this list"></span></a><?php }?>
							<br clear="all">
							<div class="two columns">Color:</div><div class="four columns">
								<input type='text' id="colorPicker" value=""/>
								<input type='hidden' id="color" name="color" value="<?php echo $DepartmentsByID[$SelectedDepartmentID]->color;?>"/>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<div id="departmentsResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-departments&DepartmentID=<?php echo $SelectedDepartmentID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addDepartment){?>
					<a href="save-department-add" id="save-department-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="department_add">
				<?php }else{?>
					<a href="save-department-update" id="save-department-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="department_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
		<fieldset>
			<legend>Employees in this Department</legend>
			<div class="fifteen columns">
				<?php 
				if (count($DepartmentEmployees)){
					include('_EmployeeComboBox.php');
				}
				?>
					<ul id="orgchart-source" style="display:none;">
						<?php foreach ($orgChartLayout[0] as $Manager=>$Managees){
							
							echo "<li class=\"big\"><em><a href=\"?nav=manage-employees&Employee".($DepartmentEmployees[$Manager]["id"] ? "ID=".$DepartmentEmployees[$Manager]["id"] : "Name=".$Manager)."\">".$Manager."</a></em>";
							echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".($EmployeesActive[$Manager]->email ? $EmployeesActive[$Manager]->email : strtolower(str_replace(" ",".",$Manager))."@cetonline.org")."&size=HR64x64\">
								<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".($EmployeesActive[$Manager]->email ? $EmployeesActive[$Manager]->email : strtolower(str_replace(" ",".",$Manager))."@cetonline.org")."&size=HR64x64\">";

							echo "<ul>";
							foreach ($Managees as $ManagerName=>$ManageesName){
								if ($DepartmentEmployees[$ManageesName]){
									echo "<li class=\"big\"><a href=\"?nav=manage-employees&EmployeeID=".$DepartmentEmployees[$ManageesName]["id"]."\">".$ManageesName."</a>";
									echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageesName]->email."&size=HR64x64\">
									<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageesName]->email."&size=HR64x64\">";
									
									if (count($orgChartLayout[1][$ManageesName])){
										echo "<ul>";
										foreach($orgChartLayout[1][$ManageesName] as $ManageeList=>$ManageeListName){
											echo "<li class=\"big\"><a href=\"?nav=manage-employees&EmployeeID=".$DepartmentEmployees[$ManageeListName]["id"]."\">".$ManageeListName."</a>";
											echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageeListName]->email."&size=HR64x64\">
											<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageeListName]->email."&size=HR64x64\">";
											if (count($orgChartLayout[2][$ManageeListName])){
												echo "<ul>";
													foreach ($orgChartLayout[2][$ManageeListName] as $ManageesDetails=>$ManageesDetailsName){
														echo "<li class=\"big\"><a href=\"?nav=manage-employees&EmployeeID=".$DepartmentEmployees[$ManageesDetailsName]["id"]."\">".$ManageesDetailsName."</a>";
														echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageesDetailsName]->email."&size=HR64x64\">
														<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesActive[$ManageesDetailsName]->email."&size=HR64x64\">";
														echo "</li>";
													}
												echo "</ul>";
											}
											echo "</li>";
										}
										echo "</ul>";
									}
									echo "</li>";
								}//end if active employee
							}
							echo "</ul>";
						}?>
					</ul>
			</div>
			<br clear="all"><br>
			<div class="fifteen columns">
				<div id="orgchart-container" class="reset-this"></div>
			</div>
		</fieldset>
	</form>
		<fieldset>
		<legend>Trainings/Certifications</legend>
		<div class="fifteen columns">
			<div class="three columns" id="trainingMessage">&nbsp;</div>
			<div class="nine columns">
				Effective Date: <input type="text" class="date" id="AffectiveDate" name="affectiveDate" value="<?php echo $TodaysDate;?>">&nbsp;&nbsp;&nbsp;
				Status <select id="Status" name="status">
					<?php
						foreach ($TrainingStatusList as $TrainingStatus){
							echo "<option value=\"".$TrainingStatus->name."\">".$TrainingStatus->name."</option>";
						}
					?>
				</select>
			</div>
			
			<div class="six columns"> 
				<div class="five columns"><b>Drag Training Here to Assign</b><Br><span style="font-size:10pt;">(all staff in this department)</span></div>
				<ul id="sortable3" class="connectedSortable">
				<?php 
					foreach ($TrainingMatrix as $trainingMatrixId=>$matrixInfo){
						$affectiveTimeStamp = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLTimeStampDisplay($matrixInfo->timeStamp));
						$affectiveDate = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLDate($matrixInfo->timeStamp));
						echo "<li id=\"".$matrixInfo->id."\" data-trainingId=\"".$matrixInfo->trainingId."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$matrixInfo->trainingName." <span class='certDescription' title='".$matrixInfo->status." ".$affectiveTimeStamp." by ".$matrixInfo->addedByAdmin." added into the system on ".MySQLTimeStampDisplay($matrixInfo->timeStamp)."'>".$matrixInfo->status." ".$affectiveDate."</span></a></li>";
					}
				?>
				</ul>
			</div>
			<div class="one columns">&nbsp;</div>
			<div class="six columns"> 
				<div class="five columns"><b>Trainings/Certifications</b><Br><span style="font-size:10pt;"> (Ctrl + Click to select multiple items)</span></div>
				<ul id="sortable4" class="connectedSortable">
				<?php 
					foreach ($TrainingsActive as $training=>$trainingInfo){
						echo "<li id=\"".$trainingInfo->id."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainings&TrainingID=".$trainingInfo->id."\">".$trainingInfo->name.($trainingInfo->isCertification ? " <span class='certDescription'>(cert)</span>": "")."</a></li>";
					}
				?>
				</ul>
			</div>
		</div>
	</fieldset>
	
	
	
<?php }else{ //end if no department selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-departments&adddepartment=true" class="button-link">+ Add New Department</a>
		</div>
	<?php }?>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>
<script>
$(function(){
	$('#orgchart-source').orgChart({
		container: $('#orgchart-container'),
		interactive: true,
		showLevels: 2,
		stack: true,
		depth: <?php echo count($orgChartLayout);?>
	});
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});


	$("#showDepartmentsSelectForm").hide();
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	<?php if ($SelectedDepartmentID){?>
		$("#departmentsSelectForm").hide();
		$("#showDepartmentsSelectForm").show();
	<?php }?>
	$("#showDepartmentsSelectForm").click(function(){
		$("#departmentsSelectForm").show();		
		$(this).hide();
	});

	$("#departmentsSelectForm select").change(function(){
		$(this).closest("form").submit();
		$(this).closest("select").val('');
	});
	
	 var formElement = $("#DepartmentsUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            },
            director: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-department-update").click(function(e){

        $('#departmentsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeDepartmentsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#departmentsResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#departmentsResults').html(message).addClass("alert");
                }
            });
        }
    });
    $("#save-department-add").click(function(e){

        $('#departmentsResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiEmployeeDepartmentsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#departmentsResults').html("Saved").addClass("alert").addClass("info");
					var displayOrderValue = parseInt($('#displayOrderId').val());
					$("#displayOrderId").val((displayOrderValue+1));					

                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#departmentsResults').html(message).addClass("alert");
                }
            });
        }
    });
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});


});
$(function() {
	var sortables = $('.connectedSortable');
	var sortable1 = $("#sortable1");
	var sortable2 = $("#sortable2");
	var sortable3 = $("#sortable3");
	var sortable4 = $("#sortable4");
	
	<?php if (!$ReadOnlyTrue){?>
		$( "#sortable1, #sortable2" ).sortable({
			items: 'li',
			connectWith: ".connectedSortable",
			stop: function(event, ui) {
				var myOrderSortable1 = new Array();
				var myOrderSortable2 = new Array();
				var DisplayArray = new Array();
					
				sortable1.each(function() {
					myOrderSortable1 = myOrderSortable1.concat($(this).sortable('toArray'));
				});
				sortable2.each(function() {
					myOrderSortable2 = myOrderSortable2.concat($(this).sortable('toArray'));
				});
				DisplayArray.push({action: 'UpdateDisplay', items:myOrderSortable1});
				DisplayArray.push({action: 'UpdateDoNotDisplay', items:myOrderSortable2});
				//process the change for displayedID
				if (myOrderSortable1.length){
					$.ajax({
						url: "ApiEmployeeDepartmentsManagement.php",
						type: "PUT",
						data: JSON.stringify(DisplayArray),
						success: function(data){
							$('#updateMessage').fadeIn().html("Display Order Saved").addClass("alert").addClass("info").fadeOut(3000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#updateMessage').fadeIn().html(message).removeClass("info").addClass("alert");
						}
					});
				}
			}	  
		}).disableSelection();
		
		<?php if ($SelectedDepartmentID){?>
			$( "#sortable3, #sortable4" ).on('click', 'li', function (e) {
				if (e.ctrlKey || e.metaKey) {
					e.preventDefault();
					$(this).toggleClass("ui-state-hover");
				} else {
					$(this).addClass("ui-state-hover").siblings().removeClass('ui-state-hover');
				}
			});
			
			$( "#sortable3, #sortable4" ).sortable({
				items: 'li',
				connectWith: ".connectedSortable",
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if (!item.hasClass('ui-state-hover')) {
						item.addClass('ui-state-hover').siblings().removeClass('ui-state-hover');
					}
					
					//////////////////////////////////////////////////////////////////////
					//HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:
					
					//Clone the selected items into an array
					var elements = item.parent().children('.ui-state-hover').clone();
					
					//Add a property to `item` called 'multidrag` that contains the 
					//  selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.ui-state-hover').remove();
					
					//Now the selected items exist in memory, attached to the `item`,
					//  so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				
				stop: function(event, ui) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					//`elements` now contains the originally selected items from the source list (the dragged items)!!
					
					var assignedTrainings = new Array();
					var removeTrainings = new Array();
					$.each(elements,function(key,value){
						var $thisLi = $(value),
							thisTrainingId = $thisLi.attr('data-trainingId');
						if (thisTrainingId){
//							removeTrainings.push($thisLi.attr('id'));
						}else{
							assignedTrainings.push($thisLi.attr('id'));
						}
					});
					
					//Finally I insert the selected items after the `item`, then remove the `item`, since 
					//  item is a duplicate of one of the selected items.
					ui.item.after(elements).remove();
					
					var DisplayArray = new Array();
					if (assignedTrainings.length){
						var AffectiveDate = $("#AffectiveDate").val(),
							Status = $("#Status").val(),
							DepartmentName = '<?php echo urlencode($DepartmentsByID[$SelectedDepartmentID]->name);?>';

						DisplayArray.push({action: 'Insert_TrainingMatrixByDepartment',departmentName: DepartmentName,affectiveDate: AffectiveDate, status: Status,items:assignedTrainings});
					}
					if (removeTrainings.length){
//						DisplayArray.push({action: 'Remove_TrainingMatrix',items:removeTrainings});
					}
					//process the change for displayedID
					if (DisplayArray.length){
						//console.log(DisplayArray);
						$.ajax({
							url: "ApiEmployeeManagement.php",
							type: "PUT",
							data: JSON.stringify(DisplayArray),
							success: function(data){
								//console.log(data);
								$('#trainingMessage').fadeIn().html("Trainings "+data).addClass("alert").addClass("info").fadeOut(3000);
							},
							error: function(jqXHR, textStatus, errorThrown){
								var message = $.parseJSON(jqXHR.responseText);
								$('#trainingMessage').fadeIn().html(message).removeClass("info").addClass("alert");
							}
						});
					}
				}	  
			}).disableSelection();
		<?php }//end if $SelectedEmployeeID ?>
		
		
		
		
<?php }//not in readonly?>
});
</script>
<script type="text/javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/colorpicker.js"></script>
<script>
$(function(){
	$("#colorPicker").spectrum({
		color: "<?php echo ($DepartmentsByID[$SelectedDepartmentID]->color ? $DepartmentsByID[$SelectedDepartmentID]->color : "#fff");?>",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		preferredFormat: "hex",
		localStorageKey: "spectrum.demo",
		hideAfterPaletteSelect:true,
		change: function(color) {
			$("#color").val(color.toHexString());
			$("div.orgChart").css("background-color",color.toHexString());
		},
		palette: [
			["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
			["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
			["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
			["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
			["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
			["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
			["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
			["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]		
		]
	});
});
</script>
