<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeByEmail[strtolower($record->email)]=$record->lastName.", ".$record->firstName;
	if ($record->displayOrderId){
		$ActiveEmployeesEmails[] = $record->email;
	}
}
$Version = ($_GET['Version'] ? $_GET['Version'] : $Config_CurrentStaffManualVersion);
$criteria = new stdClass();
$criteria->version = $Version;
$paginationResult = $hrEmployeeProvider->getStaffManual($criteria);
$resultArray = $paginationResult->collection;
$SurveyManualCount = 0;
foreach ($resultArray as $result=>$record){
	$StaffManual[$record->id] = $record;
	$StaffManualEmail[] = strtolower($record->email);
	$SurveyManualCount++;
}
foreach($ActiveEmployeesEmails as $Email){
	if (!in_array(strtolower($Email),$StaffManualEmail)){
		$NoResponseFromList[] = $Email;
	}
}
?>
<hr>
<div class="instructions">Instructions for staff: Login to Staff Access portal at <?php echo $CurrentServer;?>cetstaff<br>If they have not yet acknowledged review of Staff Manual, they will be prompted</div>
<div class="row">
	<div class="fifteen columns">
		<h3>
			Staff Manual Review Results for Version 
			<select id="version" name="Version" style="width:100px;">
				<option value="2015_08"<?php echo ($Version == $Config_CurrentStaffManualVersion ? " selected" : "");?>><?php echo $Config_CurrentStaffManualVersion;?></option>
			</select>
		</h3>
		Staff Participation Count: <?php echo $SurveyManualCount;?><br>
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>TimeStamp</th>
					<th>Staff</th>
					<th>Version</th>
				</tr>
			</thead>
			<tbody>
				<?php	
					foreach ($StaffManual as $ID=>$Record){
						$thisTimeStamp = $Record->timeStamp;
						$newTimeStamp = date('Y-m-d H:i:s',strtotime("$thisTimeStamp + 3 hours"));
				?>
						<tr>
							<td><?php echo date("m/d/Y g:i a", strtotime($newTimeStamp));?></td>
							<td><?php echo $EmployeeByEmail[trim(strtolower($Record->email))];?></td>
							<td><?php echo $Record->version;?></td>
						</tr>
				<?php 
					}//end foreach MonthYearInfo 
				?>
			</tbody>
		</table>
	</div>
</div>
<Br>
<hr>
<Br>
<div class="row">
	<div class="ten columns">
		The following <?php echo count($NoResponseFromList);?> staff have not yet reviewed the staff manual:<Br>
		(version <?php echo $Version;?>)<Br>
		<table class="NoResponses">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($NoResponseFromList as $Email){?>
				<tr>
					<td><?php echo $EmployeeByEmail[trim(strtolower($Email))];?></td>
					<td><?php echo $Email;?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		$("#version").on('change',function(){
			var $this = $(this);
			location.href = "<?php echo $CurrentServer.$adminFolder;?>hr/?nav=staffmanualtracker&Version"+$this.val();
		});
		var tableRawData = $('.rawDataReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 10
		});
		var tableNoResponses = $('.NoResponses').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "asc"]],
			"bPaginate": true,
			"iDisplayLength": 100
		});
	});
</script>