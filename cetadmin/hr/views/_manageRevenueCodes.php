<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);

//retrieve Revenue codes
$RevenueCodesResults = $hrEmployeeProvider->getRevenueCodes($criteria);

foreach ($RevenueCodesResults as $results){
	$RevenueCodesByID[$results["HRRevenueCode_ID"]] = $results;
	$RevenueCodesByName[$results["HRRevenueCode_Name"]] = $results;
	$RevenueCodesByCode[$results["HRRevenueCode_Code"]] = $results;
	$RevenueCodes[$results["HRRevenueCode_Code"]] = $results["HRRevenueCode_Name"];
}

$SelectedRevenueCode = $_GET['SelectedRevenueCode'];
$addRevenueCode = $_GET['addRevenueCode'];


//get employees
$criteria->revenueCode = $SelectedRevenueCode;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeesWithCode[$record->fullName] = $record;
}


?>
<?php if (!$addRevenueCode){?>
	<br>
	<div class="fifteen columns">
		<?php include('_RevenueCodesComboBox.php');?>
	</div>
	<div class="fifteen columns">&nbsp;</div>
<?php } //end if not addRevenueCode?>
<?php if ($SelectedRevenueCode || $addRevenueCode){?>
	<style>
		.NonBillable1 {font-size:10pt;}
	</style>
	<form id="RevenueCodeUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Revenue Code Details</legend>
			<div class="fifteen columns">
				<div style="display:none;">
					<?php if (!$addRevenueCode){?><input type="text" name="HRRevenueCode_ID" value="<?php echo $RevenueCodesByCode[$SelectedRevenueCode]["HRRevenueCode_ID"];?>"><?php }?>
				</div>
				<div class="fourteen columns">
					<div class="two columns">Code:</div><div class="ten columns">
						<input type="text" id="code" name="HRRevenueCode_Code" style="width:100%;" value="<?php echo $RevenueCodesByCode[$SelectedRevenueCode]["HRRevenueCode_Code"];?>">
				</div>
				<div class="fourteen columns">
					<div class="two columns">Name:</div><div class="ten columns">
						<input type="text" id="name" name="HRRevenueCode_Name" style="width:100%;" value="<?php echo $RevenueCodesByCode[$SelectedRevenueCode]["HRRevenueCode_Name"];?>">
				</div>
			</div>
		</fieldset>
		<div id="revenueCodeResults" class="fifteen columns"></div>
		<?php if (!$ReadOnlyTrue){?>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-revenueCodes&RevenueCode=<?php echo $SelectedRevenueCode;?>" id="cancel" class="button-link">Cancel</a>
				<?php if ($addRevenueCode){?>
					<a href="save-revenuecode-add" id="save-revenuecode-add" class="button-link do-not-navigate">+Add</a>
					<input type="hidden" name="action" value="revenueCode_add">
				<?php }else{?>
					<a href="save-revenueCode-update" id="save-revenueCode-update" class="button-link do-not-navigate">Save</a>
					<input type="hidden" name="action" value="revenueCode_update">
				<?php }?>

			</div>
		<?php }//if not read only ?>
	</form>
	<?php if ($SelectedRevenueCode){?>
			<fieldset>
				<legend>Employees with this Revenue Code</legend>
				<div class="fifteen columns">
						<?php 
							ksort($EmployeesWithCode);
							foreach ($EmployeesWithCode as $name=>$info){
								echo '<a href="'.$CurrentServer.$adminFolder.'hr/?nav=manage-employees&EmployeeID='.$info->id.'">'.$name.'</a><br>';
							}
						?>
				</div>
			</fieldset>
	<?php }//end if SelectedRevenueCode ?>
<?php }else{ //end if no product selected?>
	<?php if (!$ReadOnlyTrue){?>
		<div class="fifteen columns">
			<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-revenueCodes&addRevenueCode=true" class="button-link">+ Add New Revenue Code</a>
		</div>
	<?php }?>
<?php }?>

<script>
$(function(){
	

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
	 var formElement = $("#RevenueCodeUpdateForm");
	 
	 formElement.validate({
        rules: {
            name: {
                required: true
            }
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
		$("#save-revenuecode-add").click(function(e){

			$('#revenueCodeResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				console.log(JSON.stringify(formElement.serializeObject()));
				$.ajax({
					url: "ApiRevenueCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#revenueCodeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#revenueCodeResults').show().html(message).addClass("alert");
					}
				});
			}
		});
	<?php if ($SelectedRevenueCode){?>
		$("#save-revenueCode-update").click(function(e){

			$('#revenueCodeResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			if (formElement.valid()){
				$.ajax({
					url: "ApiRevenueCodesManagement.php",
					type: "PUT",
					data: JSON.stringify(formElement.serializeObject()),
					success: function(data){
						$('#revenueCodeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#revenueCodeResults').show().html(message).addClass("alert");
					}
				});
			}
		});
		
	<?php }//end if SelectedRevenueCode ?>
});
</script>