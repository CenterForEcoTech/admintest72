<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."PublicProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");

$publicProvider = new PublicProvider($dataConn);
$paginationResult = $publicProvider->get();
$resultArray = $paginationResult->collection;
$SurveyOptedIn = 0;
foreach ($resultArray as $result=>$record){
	$EEO[$record->id] = $record;
	if ($record->participation != "Opted-out"){
		$GenderCount[$record->gender] = $GenderCount[$record->gender]+1;
		$SurveyOptedIn++;
		$EthnicityCount[$record->ethnicity] = $EthnicityCount[$record->ethnicity]+1;
	}
}
ksort($EthnicityCount);
$hrEmployeeProvider = new HREmployeeProvider($dataConn);


$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$EmployeeByEmail[strtolower($record->email)]=$record->lastName.", ".$record->firstName;
	if ($record->displayOrderId){
		$ActiveEmployeesEmails[] = $record->email;
	}
}

$paginationResult = $hrEmployeeProvider->getEEOSurvey();
$resultArray = $paginationResult->collection;
$SurveyOptedInStaff = 0;
$SurveyStaffCount = 0;
foreach ($resultArray as $result=>$record){
	$EEOStaff[$record->id] = $record;
	$EEOStaffEmail[] = strtolower($record->email);
	$SurveyStaffCount++;
	if ($record->participation != "Opted-out"){
		$GenderCountStaff[$record->gender] = $GenderCountStaff[$record->gender]+1;
		$SurveyOptedInStaff++;
		$EthnicityCountStaff[$record->ethnicity] = $EthnicityCountStaff[$record->ethnicity]+1;
	}
}
foreach($ActiveEmployeesEmails as $Email){
	if (!in_array(strtolower($Email),$EEOStaffEmail)){
		$NoResponseFromList[] = $Email;
	}
}
ksort($EthnicityCountStaff);
$EEOTitlesList = $hrEmployeeProvider->getEEOTitles();




?>
<hr>
<div class="row">
	<?php if (!$ReadOnlyTrue){?>
		<form method="post">
			<fieldset>
				<legend>Create Survey Link</legend>
				<div class="two columns">Job Title:</div>
				<div class="eight columns">
				<?php 
					$customComboBoxInput = "width:400px;";
					$comboBoxRemoveIfInvalid = " ";

					include_once('_comboBoxIncludes.php');
				?>
						<script>
						  $(function() {
							$( ".comboboxEEOTitles" ).combobox({
								select: function(event, ui){
									$("#Title").val($(ui.item).val());
								}
							});
						  });
						</script>	
						<input type="hidden" id="Title" name="Title">
					  <select class="comboboxEEOTitles" class="seven columns" data-hiddenid="Title">
						<option value="">Select one...</option>
						<?php 
							foreach ($EEOTitlesList as $EEOTitle){
								echo "<option value=\"".$EEOTitle."\">".$EEOTitle."</option>";
							}
						?>
					  </select>
					<br>		 
					Start typing or use the pulldown to indicate Job Title<br>Unique EEO Survey links are tied to Job Title
				</div>
				<div class="two columns">
					<input type="submit" name="CreateLink" value="Create Link" class="button-link"><br>
				</div>
			</fieldset>
		</form>
	<?php }//end ReadOnly?>
</div>
<Br>
<?php
$CreateLink = $_POST['CreateLink'];
$title = $_POST['Title'];
/*
* Script for generating URLs that can be accessed one single time.
*/

/* Generate a unique token: */
if ($CreateLink == "Create Link" && $title !=""){
	$token = md5(uniqid(rand(),1));
	$tokenCode = $token."{Title:".$title;
	$criteria = new stdClass();
	$criteria->token = $token;
	$criteria->title = $title;
	$insertTitleToken = $publicProvider->eeoRecruitmentLink($criteria,getAdminId());
	if ($insertTitleToken->success){
		/* Report the one-time URL to the user: */
		echo "Copy and paste the following into the email for the applicants applying for '".$title."' to access the EEO Form:<br><br>\n";
		echo "Please click the link below to voluntarily provide information that will assist us in complying with government reporting requirements under the Equal Employment Opportunity Act.<br><br>";
		echo "<a href='".$CurrentServer."EEO/".$token."' target='_blank'>".$CurrentServer."EEO/".$token."</a>";
	}else{
		echo "There was a problem creating the link, please try again";
	}
}
?>
<div class="row">
	<div class="fifteen columns">
		<h3>Recruitment EEO Survey Results</h3>
		Gender: 
		<?php 
			foreach ($GenderCount as $Gender=>$count){
				echo (trim($Gender)=="" ? "undeclared" : $Gender)." ".round($count/$SurveyOptedIn*100,1)."% ";
			}
		?><br>
		Ethnicity: 
		<?php 
			foreach ($EthnicityCount as $Ethnicity=>$count){
				echo (trim($Ethnicity)=="" ? "undeclared" : $Ethnicity)." ".round($count/$SurveyOptedIn*100,1)."%<br>";
			}
		?>
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>TimeStamp</th>
					<th style="min-width:100px;">Title</th>
					<th>Opted-out</th>
					<th>Gender</th>
					<th>Ethnicity</th>
					<th>Source</th>
				</tr>
			</thead>
			<tbody>
				<?php	
					foreach ($EEO as $ID=>$Record){
						$thisTimeStamp = $Record->timeStamp;
						$newTimeStamp = date('Y-m-d H:i:s',strtotime($thisTimeStamp." + 3 hours"));
				?>
						<tr>
							<td><span style="display:none;"><?php echo strtotime($newTimeStamp);?></span><?php echo date("m/d/Y", strtotime($newTimeStamp));?><br><?php echo date("g:i a", strtotime($newTimeStamp));?></td>
							<td><?php echo $Record->title;?></td>
							<td><?php echo $Record->participation;?></td>
							<td><?php echo $Record->gender;?></td>
							<td><?php echo str_replace(",",", ",$Record->ethnicity);?></td>
							<td><?php echo $Record->source;?></td>
						</tr>
				<?php 
					}//end foreach MonthYearInfo 
				?>
			</tbody>
		</table>
	</div>
</div>
<hr>
<div class="instructions">Instructions for staff: Login to Staff Access portal at <?php echo $CurrentServer;?>cetstaff<br>If they have not yet acknowledged review of Staff Manual, they will be prompted</div>
<div class="row">
	<div class="fifteen columns">
		<h3>Staff EEO Survey Results</h3>
		Staff Participation Count: <?php echo $SurveyStaffCount;?><br>
		Gender: 
		<?php 
			foreach ($GenderCountStaff as $Gender=>$count){
				echo (trim($Gender)=="" ? "undeclared" : $Gender)." ".round($count/$SurveyOptedInStaff*100,1)."% ";
			}
		?><br>
		Ethnicity: 
		<?php 
			foreach ($EthnicityCountStaff as $Ethnicity=>$count){
				echo (trim($Ethnicity)=="" ? "undeclared" : $Ethnicity)." ".round($count/$SurveyOptedInStaff*100,1)."% ";
			}
		?>
		<table class="rawDataReport display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>TimeStamp</th>
					<th>Staff</th>
					<th>Opted-out</th>
					<th>Gender</th>
					<th>Ethnicity</th>
				</tr>
			</thead>
			<tbody>
				<?php	
					foreach ($EEOStaff as $ID=>$Record){
						$thisTimeStamp = $Record->timeStamp;
						$newTimeStamp = date('Y-m-d H:i:s',strtotime("$thisTimeStamp + 3 hours"));
				?>
						<tr>
							<td><?php echo date("m/d/Y g:i a", strtotime($newTimeStamp));?></td>
							<td><?php echo $EmployeeByEmail[trim(strtolower($Record->email))];?></td>
							<td><?php echo $Record->participation;?></td>
							<td><?php echo $Record->gender;?></td>
							<td><?php echo str_replace(",",", ",$Record->ethnicity);?></td>
						</tr>
				<?php 
					}//end foreach MonthYearInfo 
				?>
			</tbody>
		</table>
	</div>
</div>
<Br>
<hr>
<Br>
<div class="row">
	<div class="ten columns">
		The following <?php echo count($NoResponseFromList);?> staff have not yet completed this EEO Survey:<Br>
		<table class="NoResponses">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($NoResponseFromList as $Email){?>
				<tr>
					<td><?php echo $EmployeeByEmail[trim(strtolower($Email))];?></td>
					<td><?php echo $Email;?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var tableRawData = $('.rawDataReport').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "dec"]],
			"bPaginate": true,
			"iDisplayLength": 10
		});
		var tableNoResponses = $('.NoResponses').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "asc"]],
			"bPaginate": true,
			"iDisplayLength": 100
		});
	});
</script>