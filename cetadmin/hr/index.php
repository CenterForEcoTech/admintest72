<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Admin - HR";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "manage-employees":
            $displayPage = "views/_manageEmployees.php";
            break;
        case "manage-trainings":
            $displayPage = "views/_manageTrainings.php";
            break;
        case "manage-departments":
            $displayPage = "views/_manageDepartments.php";
            break;
        case "manage-titles":
            $displayPage = "views/_manageTitles.php";
            break;
        case "manage-categories":
            $displayPage = "views/_manageCategories.php";
            break;
        case "manage-departments":
            $displayPage = "views/_manageCategories.php";
            break;
        case "manage-revenueCodes":
            $displayPage = "views/_manageRevenueCodes.php";
            break;
        case "manage-trainingentry":
            $displayPage = "views/_manageTrainingEntry.php";
            break;
        case "create-onetimeuselink":
            $displayPage = "views/_createOneTimeUseLink.php";
            break;			
        case "staffmanualtracker":
            $displayPage = "views/_staffManualTracker.php";
            break;	
		case "submitSFCase":
            $displayPage = "views/_submitSFCase.php";
            break;					
        case "file-library":
			$QueryStringApiFileLibrary = "?action=location_search&location=hr;";
            $displayPage = $siteRoot.$adminFolder."tools/views/_manageUploadedFiles.php";
            break;			
        case "edit-file":
			$nav = "file-library";
			$fileuploadpage=true;
			$uploadFileLocation = $adminFolder."hr/uploads/trainingcerts";
            ob_start();
            ?>
            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
            <?php
            $customCSS = ob_get_contents();
            ob_clean();
            $bodyCssClass = "bootstrap";
            $modelTemplate = "model/_editFileModel.php";
            $displayPage = "views/_editFile.php";
            break;
			
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>HR Administration</h1>';
include_once("_hrMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>