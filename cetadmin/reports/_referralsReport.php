<?php
include_once($dbProviderFolder."MemberProvider.php");
$merchantProvider = new MerchantMemberProvider($mysqli);
$summaryData = $merchantProvider->getReferredByTypes();
?>
<div class="container">
    <h2>Referrals of Merchants</h2>
    <h3>Summary of referrals by type</h3>
    <table>
        <thead>
        <tr>
            <th>Type</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($summaryData as $dataRow){
            ?>
            <tr>
                <td><?php echo $dataRow["Type"];?></td>
                <td style="text-align: right;"><?php echo $dataRow["num"];?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
    $referralData = $merchantProvider->getReferrers();
    ?>
    <hr solid>
    <h3>Referral List</h3>
    <table id="referral-list">
        <thead>
        <tr>
            <th>Type</th>
            <th>Referrer</th>
            <th>Merchant</th>
            <th>TimeStamp</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($referralData as $dataRow){
            ?>
            <tr data-id="<?php echo $dataRow["id"];?>" title="with a little effort we can show all merchants referred by <?php echo $dataRow["referrer"];?>">
                <td><?php echo $dataRow["type"];?></td>
                <td><?php echo $dataRow["referrer"];?></td>
                <td><?php echo $dataRow["name"];?></td>
                <td><?php echo $dataRow["timestamp"];?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function(){
        var referralListTable = $("#referral-list");
        referralListTable.dataTable();
    });
</script>

