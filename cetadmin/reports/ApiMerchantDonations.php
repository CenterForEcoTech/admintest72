<?php
include_once("../_config.php");
include_once($dbProviderFolder."MerchantDonationReportProvider.php");
include_once($rootFolder."_setupDisbursementConnection.php");

// ----------------
// TODO: convert this to MVC with rest api
// ----------------
class ReportDisplayTableRow{
    public $idLink;
    public $businessDisplay;
    public $lastRun;

    public function __construct(MerchantDonationReportStatus $record, $CurrentServer){
        $linkHref = "?nav=merchant-donations-report&id=".$record->merchantId;
        $this->idLink = "<a href='".$linkHref."'>".$record->merchantId."</a>";
        $this->businessDisplay = $record->getBusinessDisplay("<br>");
        $this->lastRun = $record->getLastRunDisplay("Never Run");
    }

    private static $columnMapping = array(
        0 => "idLink",
        1 => "businessDisplay",
        2 => "lastRun"
    );

    public static function getCriteria($datatablePost){
        $criteria = new PaginationCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

$provider = new AdminMerchantDonationReportProvider($mysqli, $mysqli_disb);
$criteria = ReportDisplayTableRow::getCriteria($_GET);
$paginatedMerchants = $provider->getMerchants($criteria);
$merchants = $paginatedMerchants->collection;

/* @var $nomination AdminNonprofitPreferredBusiness */

if ($_GET["export"] == "csv"){
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=nominations.csv");
    header("Content-Transfer-Encoding: binary");
    $nomination_data = array();
    $colHeaders = array();
    foreach ($nominations as $nomination){
        $nomination_data[] = NominationExportRow::getRow($nomination);
        if (!count($colHeaders)){
            foreach($nomination_data[0] as $column => $value){
                $colHeaders[] = $column;
            }
        }
    }

    $fileContents = exportCSV($nomination_data, $colHeaders, true);
    echo $fileContents;
} else {
    $results = array(
        "iTotalRecords" => $paginatedMerchants->totalRecords,
        "iTotalDisplayRecords" => $paginatedMerchants->totalRecords,
        "sEcho" => $_GET["sEcho"]
    );
    foreach($merchants as $merchant){
        $results["aaData"][] = new ReportDisplayTableRow($merchant, $CurrentServer);
    }
    if (!isset($results["aaData"])){
        $results["aaData"] = array();
    }
    output_json($results);
    die();
}