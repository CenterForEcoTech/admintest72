<h3>Merchant Donations</h3>
<table id="merchant-list">
    <thead>
    <tr>
        <th>Id</th>
        <th>Business Name</th>
        <th>Age</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var merchantListTable = $("#merchant-list"),
            apiUrl = "ApiMerchantDonations.php",
            lastRequestedCriteria = null,
            oTable = merchantListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aaSorting": [ ],
                "aoColumns" :[
                    {"mData": "idLink"},
                    {"mData": "businessDisplay"},
                    {"mData": "lastRun"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        $("#export-to-csv").click(function(e){
            var aoData = lastRequestedCriteria
            e.preventDefault();

            // add our export parameter
            aoData.push({
                "name" : "export",
                "value" : "csv"
            });
            window.location = apiUrl + "?" + $.param(aoData);
        });
    });
</script>