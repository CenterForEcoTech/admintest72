<?php
include_once("../../Global_Variables.php");
include_once($dbProviderFolder."AdminReportingProvider.php");

$StartDate = $_GET['StartDate'];
$EndDate = $_GET['EndDate'];
if (!$EndDate){$EndDate = $TodaysDate;}
$ReferralCode = $_GET['ReferralCode'];
$UserType = $_GET['UserType'];

$reportHeader = array("memberId","FirstName","LastName","Company","Email","Phone","UserType","RegistrationDate","ReferralCode");
$reportHeaderTextFields = array("memberId","FirstName","LastName","Company","Email","Phone","UserType","ReferralCode");
$Filters = array("StartDate"=>$StartDate,"EndDate"=>$EndDate,"ReferralCode"=>$ReferralCode,"UserType"=>$UserType);

$FormName = "Registration Report";
$ExportName = "RegistrationReport";

$adminReport = new ReportingProvider($mysqli);
$reportResult = $adminReport->registrationReport($Filters);
$referralCodes = $adminReport->getReferralCodes();

$referralSortOrder = "\"aaSorting\": [[6,\"desc\"]]"; //default to sort by RegistrationDate

foreach ($referralCodes as $referralCode){
	$referralCodeSelect .= "<option value=\"".$referralCode["CodesCampaign_Code"]."\"";
	if ($referralCode["CodesCampaign_Code"] == $ReferralCode){
		$referralSortOrder = "\"aaSorting\": [[7,\"desc\"]]";
		$referralCodeSelect .= " selected";
		$filterByCode = true;
	}
	$referralCodeSelect .= ">".$referralCode["CodesCampaign_Code"]."</option>";
}
?>
<h2><?php echo $FormName;?></h2>
<span class="excellink"><a href="#" onClick="document['csvexport'].submit();"><img border="0" src="../images/excel.jpg" alt="Export to Excel" height="30" align="center">Export</a><br></span>
<form method="get">
	<input type="hidden" name="nav" value="<?php echo $nav;?>">
	Date <input type="text" class="date" name="StartDate" value="<?php echo $StartDate;?>"> to <input type="text" class="date" name="EndDate" value="<?php echo $EndDate;?>">
	ReferralCode <select style="width:100px;" name="ReferralCode"><option value=""></option><?php echo $referralCodeSelect;?></select>
	UserType <select style="width:100px;" name="UserType"><option value=""></option><option value="1"<?php if ($UserType==1){echo " selected";}?>>Individual</option><option value="2"<?php if ($UserType==2){echo " selected";}?>>Merchant</option><option value="3"<?php if ($UserType==3){echo " selected";}?>>Organization</option></select>
	<input type="submit" value="Filter">
</form>
    <table id="results-table" width="100%">
        <thead>
            <tr>
				<?php 
					foreach ($reportHeader as $reportField){
						echo "<th>".$reportField."</th>";
					}
				?>
            </tr>
        </thead>
        <tbody>
			<?php
				if ($filterByCode){
					function ReferralCodeFilter($code){
						if ($code["ReferralCode"]){
							return $code;
						}
					}
					$reportResultsFiltered = array_filter($reportResult, "ReferralCodeFilter");			
				}else{
					$reportResultsFiltered = $reportResult;
				}
				foreach ($reportResultsFiltered as $result){
					echo "<tr>";
					foreach ($reportHeader as $reportField){
						$textFieldChecker = "";
						if (in_array($reportField,$reportHeaderTextFields)){$textFieldChecker = "|";}
						$displayData = $result[$reportField];
						if ($reportField=="Phone"){$displayData = format_tel_num($displayData);}
						if (substr($reportField,-4)=="Date"){$displayData = date("Y-m-d",strtotime($displayData));}
						echo "<td>".truncate_text($displayData,30)."</td>";
						$csv_output .=$textFieldChecker.$displayData.$textFieldChecker.",";
					}
					echo "</tr>\r\n";
					$csv_output = rtrim($csv_output,",")."NEWLINE";
				}
			?>
		</tbody>
    </table>
	<div style="display:none;">
		<form id="csvexport" name="csvexport" action="csvexport.php" method="post">
			<input type="submit" value="Export table to CSV">
			<input type="text" value="<?php echo $ExportName;?>" name="FileName">
			<input type="text" value="<?php echo implode(",",$reportHeader);?>" name="csv_hdr">
			<input type="text" value="<?php echo str_replace("\"","'",$csv_output);?>" name="csv_output">
		</form>
	</div>
<script>
$(function(){
	var dateClass = $(".date"),
		resultsTable = $("#results-table").dataTable(
		{
            "bJQueryUI": true,
			<?php echo $referralSortOrder;?>
        });
	dateClass.datepicker({
		maxDate:0
		});
	dateClass.width(80);

});
</script>