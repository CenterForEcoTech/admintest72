<h3>Merchant Nominations</h3>
<p>
    <a id="export-to-csv" href="#" title="export current records" class="button-link do-not-navigate">Export</a>
</p>
<table id="nomination-list">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Nonprofit Name"data-target-column="2"></td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Business Name"data-target-column="3"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th>Id</th>
        <th>Date</th>
        <th>Nonprofit Name</th>
        <th>Business Name</th>
        <th>Organization</th>
        <th>Business</th>
        <th>Comments</th>
        <th>Admin Comments</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<script type="text/javascript">
    $(function(){
        var nominationListTable = $("#nomination-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            apiUrl = "ApiNominations.php",
            lastRequestedCriteria = null,
            oTable = nominationListTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id", "sWidth": "5%"},
                    {"mData": "dateString", "sWidth": "5%"},
                    {"mData": "orgName"},
                    {"mData": "businessName"},
                    {"mData": "orgDisplay", "sWidth": "20%"},
                    {"mData": "businessDisplay", "sWidth": "20%"},
                    {"mData": "comments", "sWidth": "20%"},
                    {"mData": "adminComments", "sWidth": "20%", "sClass":"admin-comment"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] },
                    { "bVisible": false, "aTargets": [ 2, 3 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });

        $("#export-to-csv").click(function(e){
            var aoData = lastRequestedCriteria
            e.preventDefault();

            // add our export parameter
            aoData.push({
                "name" : "export",
                "value" : "csv"
            });
            window.location = apiUrl + "?" + $.param(aoData);
        });

        nominationListTable.on("click", "td.admin-comment", function(e){
            var cell = $(this),
                cellData = cell.html(),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("nomination-", ""),
                textArea = $("<textarea class='table-cell'></textarea>");
            textArea.click(function(e){
                e.stopPropagation();
            });
            textArea.blur(function(e){
                var dataText = textArea.val();
                $.ajax({
                    url: "ApiNominations.php",
                    type: "POST",
                    data:{
                        id: dataId,
                        admin_comments: dataText
                    },
                    success: function(data){
                        cell.html(dataText);
                    },
                    error: function(data){
                        cell.html(cellData);
                    }
                });
            });
            textArea.val(cellData);
            cell.html(textArea);
            textArea.focus();
        });
    });
</script>