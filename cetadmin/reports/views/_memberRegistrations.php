<h3>Member Registrations</h3>
<p>
    To Impersonate a member,
    <ol style="list-style-type: decimal;">
    <li><label>Type the password here: <input type="password" id="impersonate-pwd" class="auto-watermarked" title="password for impersonation"></label></li>
    <li>Click on the member's ID</li>
    <li>Once you get confirmation that you are logged in as that member, navigate to the website.</li>
    </ol>
</p>
<p>NOTE: Sorting on Entity ID is very slow--don't do it unless you really need to.</p>
<p>
    <a id="export-to-csv" href="#" title="export current records" class="button-link do-not-navigate">Export</a>
</p>
<style type="text/css">
    #registration-list *{
        font-size:12px;
    }
    td.impersonation:hover {cursor:pointer; outline:groove;}
</style>
<table id="registration-list">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Ind, Merch, Org" data-target-column="2"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Referral Code" data-target-column="6"></td>
    </tr>
    <tr>
        <th title="member id">Member Id</th>
        <th>Date</th>
        <th>Type</th>
        <th>Entity ID</th>
        <th>First</th>
        <th>Last</th>
        <th>Contact Info</th>
        <th>Referral</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<script type="text/javascript">
    $(function(){
        var registrationListTable = $("#registration-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            apiUrl = "ApiMemberRegistrations.php",
            lastRequestedCriteria = null,
            oTable = registrationListTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id", "sWidth": "10%", "sClass": "impersonation"},
                    {"mData": "dateString", "sWidth": "5%"},
                    {"mData": "memberType", "sWidth": "5%"},
                    {"mData": "entityIdDisplay", "sWidth": "10%"},
                    {"mData": "firstName"},
                    {"mData": "lastName"},
                    {"mData": "contactInfo", "sWidth": "30%"},
                    {"mData": "referralCode", "sWidth": "10%"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });

        $("#export-to-csv").click(function(e){
            var aoData = lastRequestedCriteria
            e.preventDefault();

            // add our export parameter
            aoData.push({
                "name" : "export",
                "value" : "csv"
            });
            window.location = apiUrl + "?" + $.param(aoData);
        });

        registrationListTable.on("click", "td.impersonation", function(e){
            var cell = $(this),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("registration-", ""),
                pwdField = $("#impersonate-pwd"),
                pwd = pwdField.val(),
                postData = {
                    "action": "impersonate",
                    "memberId": dataId,
                    "password": pwd
                };

            $.ajax({
                url: "ApiMemberRegistrations.php",
                type: "POST",
                data: JSON.stringify(postData),
                success: function(data){
                    alert(data.message);
                },
                error: function(response){
                    var data = JSON.parse(response.responseText);
                    alert(data.message);
                    pwdField.highlight();
                }
            });
        });
    });
</script>