<h3>Event Management</h3>
<p>See the next table below for draft events.</p>
<p>
    <a id="export-to-csv" href="#" title="export current records" class="button-link do-not-navigate">Export</a>
</p>
<style type="text/css">
    #event-list *, #event-draft-list *{
        font-size:12px;
    }
</style>
<table id="event-list">
    <thead>
    <tr class="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><label><input class="has-premiums" type="checkbox" value="1" checked> has Premiums</label></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th title="member id">Event Id</th>
        <th>Created Date</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Event Name</th>
        <th>Merchant ID</th>
        <th>Contact Info</th>
        <th>Premium Info</th>
        <th>Assets (Added / Pending)</th>
        <th>Admin Id</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<script type="text/javascript">
    $(function(){
        var tableElement = $("#event-list"),
            searchInputs = tableElement.find(".search-inputs input"),
            asInitVals = [],
            apiUrl = "ApiEventPremiums.php",
            lastRequestedCriteria = null,
            oTable = tableElement.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {
                        "mData": "eventId",
                        "sWidth": "5%",
                        "mRender": function ( data, type, full ) {
                            return '<a href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=edit-event&id='+data+'" title="Edit" target="_blank">'+data+'</a>';
                        }
                    },
                    {"mData": "dateString", "sWidth": "5%"},
                    {"mData": "startDate", "sWidth": "5%"},
                    {"mData": "endDate", "sWidth": "5%"},
                    {"mData": "eventName", "sWidth": "15%"},
                    {"mData": "entityIdDisplay", "sWidth": "5%"},
                    {"mData": "contactInfo", "sWidth": "20%"},
                    {"mData": "premiumInfo", "sWidth": "20%"},
                    {"mData": "numAssets", "sWidth": "5%"},
                    {"mData": "createdByAdmin", "sWidth": "5%"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    var hasPremiumsElement = tableElement.find("input.has-premiums:checked"),
                        hasPremiums = hasPremiumsElement.length ? hasPremiumsElement.val() : "";
                    aoData.push( { "name": "hasPremium", "value": hasPremiums } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );

        tableElement.find(".has-premiums").click(function(e){
            oTable.fnDraw();
        });

        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });

        $("#export-to-csv").click(function(e){
            var aoData = lastRequestedCriteria
            e.preventDefault();

            // add our export parameter
            aoData.push({
                "name" : "export",
                "value" : "csv"
            });
            window.location = apiUrl + "?" + $.param(aoData);
        });
    });
</script>
<h3>Draft Events</h3>
<p>The export does not include these elements</p>
<table id="event-draft-list">
    <thead>
    <tr class="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><label><input class="has-premiums" type="checkbox" value="1" checked> has Premiums</label></td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th title="member id">Draft Id</th>
        <th>Created Date</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Event Name</th>
        <th>Merchant ID</th>
        <th>Contact Info</th>
        <th>Premium Info</th>
        <th>Assets (Added / Pending)</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<script type="text/javascript">
    $(function(){
        var tableElement = $("#event-draft-list"),
            searchInputs = tableElement.find(".search-inputs input"),
            asInitVals = [],
            apiUrl = "ApiEventPremiums.php",
            lastRequestedCriteria = null,
            oTable = tableElement.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {
                        "mData": "eventId",
                        "sWidth": "5%",
                        "mRender": function ( data, type, full ) {
                            return '<a href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=edit-draft-event&id='+data+'" title="Edit" target="_blank">'+data+'</a>';
                        }
                    },
                    {"mData": "dateString", "sWidth": "5%"},
                    {"mData": "startDate", "sWidth": "5%"},
                    {"mData": "endDate", "sWidth": "5%"},
                    {"mData": "eventName", "sWidth": "15%"},
                    {"mData": "entityIdDisplay", "sWidth": "5%"},
                    {"mData": "contactInfo", "sWidth": "20%"},
                    {"mData": "premiumInfo", "sWidth": "20%"},
                    {"mData": "numAssets", "sWidth": "5%"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    var hasPremiumsElement = tableElement.find("input.has-premiums:checked"),
                        hasPremiums = hasPremiumsElement.length ? hasPremiumsElement.val() : "";
                    aoData.push( { "name": "hasPremium", "value": hasPremiums } );
                    aoData.push( { "name": "isDraft", "value": 1 } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );

        tableElement.find(".has-premiums").click(function(e){
            oTable.fnDraw();
        });

        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );
    });
</script>