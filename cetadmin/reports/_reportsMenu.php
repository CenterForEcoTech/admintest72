<div id="nav-menu">
    <ul>
        <li><a id="admin-registration-report" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=admin-registration-report" class="button-link">Admin Registration Report</a></li>
        <li><a id="transaction-report" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=transaction-report" class="button-link">Transaction Report </a></li>
        <li><a id="business-charity-list" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=business-charity-list" class="button-link">Business Charity List</a></li>
        <li><a id="referrals-report" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=referrals-report" class="button-link">Referrals</a></li>
        <li><a id="merchant-nominations-report" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=merchant-nominations-report" class="button-link">Merchant Nominations</a></li>
        <li><a id="merchant-donations" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=merchant-donations" class="button-link">Merchant Donations Report</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="member-registrations" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=member-registrations" class="button-link" title="Impersonate here!">Member Registrations</a></li>
        <li class="separator">&nbsp;</li>
        <li><a id="event-premiums" href="<?php echo $CurrentServer.$adminFolder;?>reports/?nav=event-premiums" class="button-link">Event Premiums</a></li>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>