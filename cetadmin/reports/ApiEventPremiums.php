<?php
include_once("../_config.php");
error_reporting(1); ini_set("display_errors", 1);
include_once($dbProviderFolder."MemberProvider.php"); // contains MerchantRecord and OrganizationRecord data contracts
include_once($dbProviderFolder."AdminReportingProvider.php");

include_once("model/_eventPremiumModel.php");
$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        $provider = new EventPremiumReportProvider($mysqli);
        if ($_GET['isDraft']){
            $provider = new DraftEventPremiumReportProvider($mysqli);
        }
        $criteria = EventPremiumDisplayTableRow::getCriteria($_GET);
        $paginatedRegistrations = $provider->get($criteria);
        $recordCollection = $paginatedRegistrations->collection;

        /* @var $record EventPremiumRecord */

        if ($_GET["export"] == "csv"){
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=EventPremiumReport.csv");
            header("Content-Transfer-Encoding: binary");
            $export_data = array();
            $colHeaders = array();
            foreach ($recordCollection as $record){
                $export_data[] = EventPremiumExportRow::getRow($record);
                if (!count($colHeaders)){
                    foreach($export_data[0] as $column => $value){
                        $colHeaders[] = $column;
                    }
                }
            }

            $fileContents = exportCSV($export_data, $colHeaders, true);
            echo $fileContents;
            die();
        } else {
            $results = array(
                "iTotalRecords" => $paginatedRegistrations->totalRecords,
                "iTotalDisplayRecords" => $paginatedRegistrations->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($recordCollection as $record){
                $results["aaData"][] = new EventPremiumDisplayTableRow($record, $CurrentServer);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
            output_json($results);
            die();
        }
        break;
    case "POST":
        $results->message = "POST is not supported at this time.";
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();
