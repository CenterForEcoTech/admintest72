<?php
include_once("../../Global_Variables.php");
include_once($dbProviderFolder."AdminReportingProvider.php");

$StartDate = $_GET['StartDate'];
$EndDate = $_GET['EndDate'];
if (!$EndDate){$EndDate = $TodaysDate;}
$MerchantID = $_GET['MerchantID'];
$CauseSelected = $_GET['CauseSelected'];
$SelectedOrgID = $_GET['SelectedOrgID'];


$reportHeader = array("Id","FirstName","LastName","Email","Phone","Business","Sale_Amount","Percent_Donated","Donation_Amount","Event_Date","Transaction_Date","Cause_Selected");
$reportHeaderTextFields = array("Id","FirstName","LastName","Email","Phone","Business","Cause_Selected","Selected_Cause");
$Filters = array("StartDate"=>$StartDate,"EndDate"=>$EndDate,"MerchantID"=>$MerchantID,"CauseSelected"=>$CauseSelected,"SelectedOrgID"=>$SelectedOrgID);

$FormName = "Transaction Report";
$ExportName = "TransactionReport";

$adminReport = new ReportingProvider($mysqli);
$reportResult = $adminReport->transactionReport($Filters);
$businessNames = $adminReport->getBusinessNames();
$causesSelected = $adminReport->getDistinctCauses();

$SortOrder = "\"aaSorting\": [[4,\"asc\"]]"; //default to sort by Transaction Date

foreach ($businessNames as $businessName){
	$businessNameSelect .= "<option value=\"".$businessName["MerchantInfo_ID"]."\"";
	if ($businessName["MerchantInfo_ID"] == $MerchantID){
		$SortOrder = "\"aaSorting\": [[4,\"desc\"]]";
		$businessNameSelect .= " selected";
	}
	$businessNameSelect .= ">".$businessName["MerchantInfo_Name"]."</option>";
}

foreach ($causesSelected as $causeSelected){
	$causesNameSelect .= "<option data-orgid=\"".$causeSelected["OrgID"]."\" value=\"".$causeSelected["Selected_Cause"]."\"";
	if ($causeSelected["Selected_Cause"] == $CauseSelected){
		$causesNameSelect .= " selected";
		$SortOrder = "\"aaSorting\": [[10,\"desc\"]]";
	}
	$causesNameSelect .= ">".$causeSelected["Selected_Cause"]."</option>";
}

?>
<style type="text/css">
    tr.review-link:hover {cursor:pointer; outline:groove;}
</style>
<h2><?php echo $FormName;?></h2>
<span class="excellink"><a href="#" onClick="document['csvexport'].submit();"><img border="0" src="../images/excel.jpg" alt="Export to Excel" height="30" align="center">Export</a><br></span>
<form method="get">
	<input type="hidden" name="nav" value="<?php echo $nav;?>">
	Event Start Date <input type="text" class="date" name="StartDate" value="<?php echo $StartDate;?>"> to <input type="text" class="date" name="EndDate" value="<?php echo $EndDate;?>">
	Merchant <select name="MerchantID"><option value=""></option><?php echo $businessNameSelect;?></select>
	Cause Selected <select name="CauseSelected" id="CauseSelected"><option value=""></option><?php echo $causesNameSelect;?></select>
	<input type="hidden" id="SelectedOrgID" name="SelectedOrgID" value="<?php echo $SelectedOrgID;?>">
	<input type="submit" value="Filter">
</form>
    <table id="results-table" width="100%">
        <thead width="100%">
            <tr width="100%">
				<?php 
					//$precentageWidth = (100/(count($reportHeader));
					foreach ($reportHeader as $reportField){
						if ($reportField == "Event_Date"){$reportField = "Event Start Date";}
						echo "<th width=\"10%\">".str_replace("_"," ",$reportField)."</th>";
					}
				?>
            </tr>
        </thead>
        <tbody>
			<?php
				//function removes transaction with no individual contact info
				//asked to be disabled from PTCard #39386931
				function BlankNameFilter($code){
					//if ($code["FirstName"] || $code["LastName"] || $code["Email"] || $code["Phone"]){
						return $code;
					//}
				}
				$reportResultsFiltered = array_filter($reportResult, "BlankNameFilter");
			
			
				if ($filterByBusiness){
					//$reportResult = array_diff($reportResult, array("ReferralCode"=>$ReferralCode));			
					function BusinessNameFilter($code){
						if ($code["MerchantInfo_Name"]){
							return $code;
						}
					}
					$reportResultsFiltered = array_filter($reportResult, "BusinessNameFilter");			
				}
				foreach ($reportResultsFiltered as $result){
					echo "<tr data-id='".$result['Id']."' class='review-link' title='click to review!'>";
					foreach ($reportHeader as $reportField){
						$textFieldChecker = "";
						if (in_array($reportField,$reportHeaderTextFields)){$textFieldChecker = "|";}
						$displayData = $result[$reportField];
						if ($reportField=="Phone"){$displayData = format_tel_num($displayData);}
						if ($reportField=="Cause_Selected"){
							//check to see if different Select Cause than 'Customer Choice'
							if ($displayData == "Customer Choice"){
								if ($result["Selected_Cause"]){
									$displayData = $result["Selected_Cause"];
								}
							}
						}
						if (substr($reportField,-4)=="Date"){$displayData = date("Y-m-d",strtotime($displayData));}
						echo "<td>".truncate_text($displayData,30)."</td>";
						$csv_output .=$textFieldChecker.$displayData.$textFieldChecker.",";
					}
					echo "</tr>\r\n";
					$csv_output = rtrim($csv_output,",")."NEWLINE";
				}
			?>
		</tbody>
    </table>

	<div style="display:none;">
		<form id="csvexport" name="csvexport" action="csvexport.php" method="post">
			<input type="submit" value="Export table to CSV">
			<input type="hidden" value="<?php echo $ExportName;?>" name="FileName">
			<input type="hidden" value="<?php echo implode(",",$reportHeader);?>" name="csv_hdr">
			<input type="text" value="<?php echo str_replace("\"","'",$csv_output);?>" name="csv_output">
		</form>
	</div>
<script>
$(function(){
	var dateClass = $(".date"),
		causeSelected = $("#CauseSelected"),
		resultsTable = $("#results-table").dataTable(
		{
            "bJQueryUI": true,
			"bAutoWidth":false,
			"aoColumnDefs": [
				{ "sWidth": "10%", "aTargets": ["_all"] }
			],
			<?php echo $SortOrder;?>
        });
	dateClass.datepicker({
		maxDate:0
		});
	dateClass.width(80);
	
	causeSelected.on('change',function(){
		var SelectedOrgID = $('#CauseSelected option:selected').data('orgid');
		if (SelectedOrgID){
			$("#SelectedOrgID").val(SelectedOrgID);
		}else{
			$("#SelectedOrgID").val('');
		}
	});

    $("#results-table").on("click", ".review-link", function(e){
        var transId = $(this).data("id"),
            url = "<?php echo $CurrentServer.$adminFolder;?>transactions/?nav=review&id=" + transId;
        window.open(url, "review-for-" + transId);
    });

});
</script>