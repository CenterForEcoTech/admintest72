<?php
include_once("../_config.php");
error_reporting(1); ini_set("display_errors", 1);
include_once($dbProviderFolder."MemberProvider.php"); // contains MerchantRecord and OrganizationRecord data contracts
include_once($dbProviderFolder."AdminReportingProvider.php");

include_once("model/_memberRegistrationModel.php");
$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":

        $provider = new MemberRegistrationProvider($mysqli);
        $criteria = RegistrationDisplayTableRow::getCriteria($_GET);
        $paginatedRegistrations = $provider->get($criteria);
        $registrations = $paginatedRegistrations->collection;

        /* @var $registration AdminNonprofitPreferredBusiness */
        /* @var $orgContact ContactRecord */

        if ($_GET["export"] == "csv"){
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=RegistrationReport.csv");
            header("Content-Transfer-Encoding: binary");
            $registration_data = array();
            $colHeaders = array();
            foreach ($registrations as $registration){
                $registration_data[] = RegistrationExportRow::getRow($registration);
                if (!count($colHeaders)){
                    foreach($registration_data[0] as $column => $value){
                        $colHeaders[] = $column;
                    }
                }
            }

            $fileContents = exportCSV($registration_data, $colHeaders, true);
            echo $fileContents;
            die();
        } else {
            $results = array(
                "iTotalRecords" => $paginatedRegistrations->totalRecords,
                "iTotalDisplayRecords" => $paginatedRegistrations->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($registrations as $registration){
                $results["aaData"][] = new RegistrationDisplayTableRow($registration, $CurrentServer);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
            output_json($results);
            die();
        }
        break;
    case "POST":
        $postParameters = json_decode($currentRequest->rawData);
        if ($postParameters->action == 'impersonate' && is_numeric($postParameters->memberId)){
            if ( isset($_SERVER["REMOTE_ADDR"]) ){
                $ip=$_SERVER["REMOTE_ADDR"];
            } elseif (isset($_SERVER["SERVER_ADDR"])){
                $ip=$_SERVER["SERVER_ADDR"];
            } else {
                $ip="Not Available";
            }
            include_once($dbProviderFolder."AuthProvider.php");
            $authProvider = new AuthProvider($mysqli);
            $authAttempt = $authProvider->authenticate("ct".$postParameters->memberId, $postParameters->password, $ip);
            if ($authAttempt->message){
                header("HTTP/1.0 400 Bad Request");
                $results->message = $authAttempt->message;
            } else {
                $CreateSessionVariablesProvider = new CreateSessionVariablesProvider($mysqli);
                $sessionData = $CreateSessionVariablesProvider->getSessionData($authAttempt);
                SessionManager::login($authAttempt, $sessionData);
                $results->message = "You have been logged in as ".SessionManager::getUserName();
                if (SessionManager::getMerchantName()){
                    $results->message .= " (".SessionManager::getMerchantName().")";
                }
                if (SessionManager::getOrganizationName()){
                    $results->message .= " (".SessionManager::getOrganizationName().")";
                }
            }
        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();
