<?php
class NominationDisplayTableRow{
    public $DT_RowId;
    public $id;
    public $dateString;
    public $orgName;
    public $orgDisplay;
    public $businessName;
    public $businessDisplay;
    public $comments;
    public $adminComments;

    public function __construct(AdminNonprofitPreferredBusiness $record, $CurrentServer){
        $this->DT_RowId = "nomination-".$record->id;
        $this->id = $record->id;
        $this->dateString = $record->date;
        $this->orgName = $record->orgRecord->name;
        $this->orgDisplay = $record->orgRecord->getCompactDump("<br>", true);
        $this->businessName = $record->businessRecord->name;
        $this->businessDisplay = $record->businessRecord->getCompactDump("<br>", true);
        if (strlen($record->businessRecord->pid)){
            $this->businessDisplay .= "<br><a href='".$CurrentServer."ct/".$record->businessRecord->pid."' target='pid'>".$record->businessRecord->pid."</a>";
        }
        $this->comments = $record->comments;
        $this->adminComments = $record->adminComments;
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "orgName",
        3 => "businessName",
        4 => "orgDisplay",
        5 => "businessDisplay",
        6 => "comments",
        7 => "adminComments"
    );

    public static function getCriteria($datatablePost){
        $criteria = new NominationQueryCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    case "orgDisplay":
                        $criteria->orgName = $searchValue;
                        break;
                    case "businessDisplay":
                        $criteria->businessName = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class NominationExportRow{
    public static function getRow(AdminNonprofitPreferredBusiness $record){
        return array(
            "id" => $record->id,
            "date" => $record->date,
            "comments" => $record->comments,
            "orgName" => $record->orgRecord->name,
            "orgAddress" => $record->orgRecord->address,
            "orgCity" => $record->orgRecord->city,
            "orgState" => $record->orgRecord->state,
            "orgZip" => $record->orgRecord->postalCode,
            "orgWebsite" => $record->orgRecord->website,
            "orgContact" => $record->orgRecord->primaryContact->firstname." ".$record->orgRecord->primaryContact->lastname,
            "orgContactPhone" => $record->orgRecord->primaryContact->phone,
            "orgContactEmail" => $record->orgRecord->primaryContact->email,
            "businessName" => $record->businessRecord->name,
            "businessAddress" => $record->businessRecord->address,
            "businessCity" => $record->businessRecord->city,
            "businessState" => $record->businessRecord->state,
            "businessZip" => $record->businessRecord->postalCode,
            "businessWebsite" => $record->businessRecord->website,
            "businessPhone" => $record->businessRecord->phone,
            "businessPid" => $record->businessRecord->pid,
            "adminComments" => $record->adminComments
        );
    }
}
?>