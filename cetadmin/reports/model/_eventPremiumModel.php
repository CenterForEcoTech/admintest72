<?php
class EventPremiumDisplayTableRow{

    public $DT_RowId;
    public $eventId;
    public $dateString;
    public $contactInfo;
    public $entityIdDisplay;
    public $premiumInfo;
    public $createdByAdmin;
    public $numAssets;
    public $eventName;
    public $startDate;
    public $endDate;

    public function __construct(EventPremiumRecord $record, $CurrentServer){
        global $CurrentServer, $adminFolder;

        if ($record->eventId){
            $this->DT_RowId = "event-".$record->eventId;
            $this->eventId = $record->eventId;
        } else {
            $this->DT_RowId = "draft-".$record->draftEventId;
            $this->eventId = $record->draftEventId;
        }
        $this->dateString = date("Y-m-d", strtotime($record->createdDate));
        $this->startDate = date("Y-m-d", strtotime($record->eventStart));
        $this->endDate = date("Y-m-d", strtotime($record->eventEnd));
        $this->eventName = $record->eventName;
        if ($record->merchantId){
            $merchantEditHref = $CurrentServer.$adminFolder."merchants/?nav=profile-edit&id=".$record->merchantId;
            $this->entityIdDisplay = "<a href='".$merchantEditHref."' target='_blank'>".$record->merchantId."</a>";
        }
        if ($record->createdByAdminId){
            $this->createdByAdmin = $record->createdByAdminId;
        }

        // construct contact info
        $contactInfoHtml = $record->merchantName;
        if ($record->address){
            $contactInfoHtml .= "<br>".$record->address;
        }
        if ($record->city){
            $contactInfoHtml .= "<br>".$record->city.", ".$record->state." ".$record->zip;
        }
        if ($record->firstName || $record->lastName){
            $contactInfoHtml .= "<br>".$record->firstName." ".$record->lastName;
        }
        if ($record->email){
            $contactInfoHtml .= "<br>".$record->email;
        }
        if ($record->phone){
            $contactInfoHtml .= "<br>".preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $record->phone);
        }
        if ($record->pid){
            $contactInfoHtml .= "<br> PID: ".$record->pid;
        }
        $this->contactInfo = $contactInfoHtml;

        // construct premium display
        $premiumInfoHtml = "";
        if ($record->premiumId){
            $premiumInfoHtml .= $record->premiumName." ($".$record->premiumPrice.")";
        }
        $this->premiumInfo = $premiumInfoHtml;


        $this->numAssets = $record->numAssets." / ".$record->pendingAssets;
    }

    private static $columnMapping = array(
        0 => "eventId",
        1 => "dateString",
        2 => "startDate",
        3 => "endDate",
        4 => "eventName",
        5 => "entityIdDisplay",
        6 => "contactInfo",
        7 => "premiumInfo",
        8 => "numAssets",
        9 => "createdByAdmin"
    );

    public static function getCriteria($datatablePost){
        $criteria = new EventPremiumQueryCriteria();
        $criteria->hasPremium = $datatablePost['hasPremium'] ? true : false;

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class EventPremiumExportRow{
    public static function getRow(EventPremiumRecord $record){
        return array(
            "event_id" => $record->eventId,
            "event_name" => $record->eventName,
            "timestamp" => $record->createdDate,
            "startDate" => $record->eventStart,
            "endDate" => $record->eventEnd,
            "firstName" => $record->firstName,
            "lastName" => $record->lastName,
            "phone" => $record->phone,
            "email" => $record->email,
            "merchantId" => $record->merchantId,
            "companyName" => $record->merchantName,
            "address" => $record->address,
            "city" => $record->city,
            "state" => $record->state,
            "zip" => $record->zip,
            "pid" => $record->pid,
            "premiumId" => $record->premiumId,
            "premiumName" => $record->premiumName,
            "premiumPrice" => $record->premiumPrice
        );
    }
}
?>