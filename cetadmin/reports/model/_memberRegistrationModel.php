<?php
class RegistrationDisplayTableRow{

    public $DT_RowId;
    public $id; // member_id
    public $dateString;
    public $memberType;
    public $entityIdDisplay;
    public $firstName;
    public $lastName;
    public $contactInfo;
    public $referralCode;
    public $pid;

    public function __construct(MemberRegistrationRecord $record, $CurrentServer){
        global $CurrentServer, $adminFolder;

        $this->DT_RowId = "registration-".$record->id;
        $this->id = $record->id;
        $this->dateString = date("Y-m-d", strtotime($record->timestamp));
        $this->memberType = $record->memberType;
        if ($record->merchantId){
            $merchantEditHref = $CurrentServer.$adminFolder."merchants/?nav=profile-edit&id=".$record->merchantId;
            $this->entityIdDisplay = "<a href='".$merchantEditHref."' target='_blank'>".$record->merchantId."</a>";
        } else if ($record->organizationId){
            $editHref = $CurrentServer.$adminFolder."charities/?nav=profile-edit&id=".$record->organizationId;
            $this->entityIdDisplay = "<a href='".$editHref."' target='_blank'>".$record->organizationId."</a>";
        } else {
            $this->entityIdDisplay = "";
        }
        $this->merchantId = $record->merchantId;
        $this->organizationId = $record->organizationId;
        $this->firstName = $record->firstName;
        $this->lastName = $record->lastName;
        $this->referralCode = $record->referralCode;
        $this->pid = $record->pid;

        // construct contact info
        $contactInfoHtml = "";
        if ($record->merchantId || $record->organizationId){
            // company
            $contactInfoHtml = $record->companyName;
            if ($record->address){
                $contactInfoHtml .= "<br>".$record->address;
            }
            if ($record->city){
                $contactInfoHtml .= "<br>".$record->city.", ".$record->state." ".$record->zip;
            }
            if ($record->email){
                $contactInfoHtml .= "<br>".$record->email;
            }
            if ($record->phone){
                $contactInfoHtml .= "<br>".$record->phone;
            }
            if ($record->orgEIN){
                $contactInfoHtml .= "<br> EIN: ".$record->orgEIN;
            }
            if ($record->referralCodeText){
                $contactInfoHtml .= "<br> Referral text: ".$record->referralCodeText;
            }
            if ($record->pid){
                $contactInfoHtml .= "<br> PID: ".$record->pid;
            }
        } else {
            // individual
            if ($record->email){
                $contactInfoHtml = $record->email;
            }
            if ($record->phone){
                $contactInfoHtml .= ($record->email) ? "<br>".$record->phone : $record->phone;
            }
        }
        $this->contactInfo = $contactInfoHtml;
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "memberType",
        3 => "entityIdDisplay",
        4 => "firstName",
        5 => "lastName",
        6 => "contactInfo",
        7 => "referralCode"
    );

    public static function getCriteria($datatablePost){
        $criteria = new RegistrationQueryCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    case "memberType":
                        $criteria->memberType = $searchValue;
                        break;
                    case "referralCode":
                        $criteria->referralCode = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class RegistrationExportRow{
    public static function getRow(MemberRegistrationRecord $record){
        return array(
            "id" => $record->id,
            "firstName" => $record->firstName,
            "lastName" => $record->lastName,
            "phone" => $record->phone,
            "email" => $record->email,
            "timestamp" => $record->timestamp,
            "memberType" => $record->memberType,
            "merchantId" => $record->merchantId,
            "organizationId" => $record->organizationId,
            "companyName" => $record->companyName,
            "address" => $record->address,
            "city" => $record->city,
            "state" => $record->state,
            "zip" => $record->zip,
            "orgEIN" => $record->orgEIN,
            "referralCode" => $record->referralCode,
            "referredByMemberId" => $record->referredByMemberId,
            "referralCodeText" => $record->referralCodeText,
            "pid" => $record->pid
        );
    }
}
?>