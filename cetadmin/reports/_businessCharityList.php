<?php
include_once("../../Global_Variables.php");
include_once($dbProviderFolder."AdminReportingProvider.php");

$StartDate = $_GET['StartDate'];
$EndDate = $_GET['EndDate'];
if (!$EndDate){$EndDate = $TodaysDate;}
$MerchantID = $_GET['MerchantID'];
$CauseSelected = $_GET['CauseSelected'];
$SelectedOrgID = $_GET['SelectedOrgID'];

$reportHeader = array("Business","Total_Donation","EIN","Cause_Selected","Selected_Cause");
$reportHeaderTextFields = array("Business","Cause_Selected","Selected_Cause");
$Filters = array("StartDate"=>$StartDate,"EndDate"=>$EndDate,"MerchantID"=>$MerchantID,"CauseSelected"=>$CauseSelected,"SelectedOrgID"=>$SelectedOrgID);

$FormName = "Business Charity Report";
$ExportName = "BusinessCharityReport";

$adminReport = new ReportingProvider($mysqli);
$reportResult = $adminReport->transactionReport($Filters,true); //true indicates group by OrgEIN
$businessNames = $adminReport->getBusinessNames();
$causesSelected = $adminReport->getDistinctCauses();

$SortOrder = "\"aaSorting\": [[1,\"desc\"]]"; //default to sort by Donation Amount
if ($StartDate){
	$SortOrder = "\"aaSorting\": [[0,\"asc\"]]";  //if sorting by transaction date then order by business
}
foreach ($businessNames as $businessName){
	$businessNameSelect .= "<option value=\"".$businessName["MerchantInfo_ID"]."\"";
	if ($businessName["MerchantInfo_ID"] == $MerchantID){
		$businessNameSelect .= " selected";
		$SortOrder = "\"aaSorting\": [[0,\"asc\"]]";
	}
	$businessNameSelect .= ">".$businessName["MerchantInfo_Name"]."</option>";
}

foreach ($causesSelected as $causeSelected){
	$causesNameSelect .= "<option data-orgid=\"".$causeSelected["OrgID"]."\" value=\"".$causeSelected["Cause_Selected"]."\"";
	if ($causeSelected["Cause_Selected"] == $CauseSelected){
		$SortOrder = "\"aaSorting\": [[4,\"desc\"]]";
		$causesNameSelect .= " selected";
	}
	$causesNameSelect .= ">".$causeSelected["Cause_Selected"]."</option>";
}

?>
<h2><?php echo $FormName;?></h2>
<span class="excellink"><a href="#" onClick="document['csvexport'].submit();"><img border="0" src="../images/excel.jpg" alt="Export to Excel" height="30" align="center">Export</a><br></span>
<form method="get">
	<input type="hidden" name="nav" value="<?php echo $nav;?>">
	Event Start Date <input type="text" class="date" name="StartDate" value="<?php echo $StartDate;?>"> to <input type="text" class="date" name="EndDate" value="<?php echo $EndDate;?>">
	Merchant <select name="MerchantID"><option value=""></option><?php echo $businessNameSelect;?></select>
	POS Cause Entered <select name="CauseSelected" id="CauseSelected"><option value=""></option><?php echo $causesNameSelect;?></select>
	<input type="hidden" name="SelectedOrgID" id="SelectedOrgID" value="<?php echo $SelectedOrgID;?>">
	<input type="submit" value="Filter">
</form>
    <table id="results-table" width="100%">
        <thead width="100%">
            <tr width="100%">
				<?php 
					//$precentageWidth = (100/(count($reportHeader));
					foreach ($reportHeader as $reportField){
						if ($reportField == "Cause_Selected"){
							$reportField = "POS Cause Entered";
						}
						echo "<th width=\"10%\">".str_replace("_"," ",$reportField)."</th>";
					}
				?>
            </tr>
        </thead>
        <tbody>
			<?php
				if ($filterByBusiness){
					//$reportResult = array_diff($reportResult, array("ReferralCode"=>$ReferralCode));			
					function BusinessNameFilter($code){
						if ($code["MerchantInfo_Name"]){
							return $code;
						}
					}
					$reportResultsFiltered = array_filter($reportResult, "BusinessNameFilter");			
				}else{
					$reportResultsFiltered = $reportResult;
				}
				foreach ($reportResultsFiltered as $result){
					echo "<tr>";
					foreach ($reportHeader as $reportField){
						$textFieldChecker = "";
						if (in_array($reportField,$reportHeaderTextFields)){$textFieldChecker = "|";}
						$displayData = $result[$reportField];
						if ($reportField=="Phone"){$displayData = format_tel_num($displayData);}
						if (substr($reportField,-4)=="Date"){$displayData = date("Y-m-d",strtotime($displayData));}
						echo "<td>".truncate_text($displayData,30)."</td>";
						$csv_output .=$textFieldChecker.$displayData.$textFieldChecker.",";
					}
					echo "</tr>\r\n";
					$csv_output = rtrim($csv_output,",")."NEWLINE";
				}
			?>
		</tbody>
    </table>

	<div style="display:none;">
		<form id="csvexport" name="csvexport" action="csvexport.php" method="post">
			<input type="submit" value="Export table to CSV">
			<input type="hidden" value="<?php echo $ExportName;?>" name="FileName">
			<input type="hidden" value="<?php echo implode(",",$reportHeader);?>" name="csv_hdr">
			<input type="text" value="<?php echo str_replace("\"","'",$csv_output);?>" name="csv_output">
		</form>
	</div>
<script>
$(function(){
	$("#CauseSelected").change(function(){
		var SelectedOption = $("#CauseSelected option:selected");
		$("#SelectedOrgID").val(SelectedOption.data("orgid"));
	});
	var dateClass = $(".date"),
		resultsTable = $("#results-table").dataTable(
		{
            "bJQueryUI": true,
			"bAutoWidth":false,
			"aoColumnDefs": [
				{ "sWidth": "10%", "aTargets": ["_all"] }
			],
			<?php echo $SortOrder;?>
        });
	dateClass.datepicker({
		maxDate:0
		});
	dateClass.width(80);

});
</script>