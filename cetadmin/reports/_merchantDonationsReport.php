<?php
$merchantId = $_GET["id"];
include_once($dbProviderFolder."MemberProvider.php");
include_once($dbProviderFolder."MerchantDonationReportProvider.php");
include_once($rootFolder."_setupDisbursementConnection.php");
$merchantProvider = new MerchantMemberProvider($mysqli);
$reportProvider = new MerchantDonationReportProvider($mysqli, $mysqli_disb);
$reportCriteria = new MerchantDonationReportCriteria();
$reportCriteria->merchantId = $merchantId;
$canRefresh = $reportProvider->canRun($reportCriteria);
if (isset($_GET["force-refresh"]) && $_GET["force-refresh"] == 1){
    $reportCriteria->fromDateTime = 0; // the beginning of time
}
if (isset($_GET["run"]) && $_GET["run"] == 1){
    $reportProvider->run($reportCriteria);
    $canRefresh = false;
}
$reportData = $reportProvider->get($reportCriteria);
$merchantProfile = $merchantProvider->getPublicProfile($merchantId);
?>
<h3><?php echo $merchantProfile->name;?></h3>


<a href="?nav=merchant-donations-report&id=<?php echo $merchantId;?>&force-refresh=1&run=1" class="button-link" title="refresh the cache">Run Report Now</a>
<?php if ($canRefresh){?>
    <span class="warning">The cache for this report is stale.</span>
<?php } else { ?>
    <span class="info">The cache is up to date.</span>
<?php } ?>

<div>
    <div class="two-column-left">
        <h4>Weekly Breakdown</h4>
        <table class="sales-data">
            <thead>
            <tr>
                <th>Date</th>
                <th>Eligible Sales</th>
                <th>Donations Generated</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /* @var $summaryRow DonationsSummaryRow */
                foreach($reportData->weeklyReport as $summaryRow){
                    $rowClass = "";
                    if ($summaryRow->isYearAggregate){
                        $rowClass = "subtotal";
                    } else if ($summaryRow->isGrandTotal){
                        $rowClass = "grand-total";
                    }
            ?>
                    <tr class="<?php echo $rowClass; ?>">
                        <td><?php echo $summaryRow->breakdownValue;?></td>
                        <td class="numeric">$<?php echo $summaryRow->eligibleSales;?></td>
                        <td class="numeric">$<?php echo $summaryRow->donationsGenerated;?></td>
                    </tr>
            <?php } // end for each weekly summary row?>
            </tbody>
        </table>
        
        <h4>Monthly Breakdown</h4>
        <table class="sales-data">
            <thead>
            <tr>
                <th>Month</th>
                <th>Eligible Sales</th>
                <th>Donations Generated</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /* @var $summaryRow DonationsSummaryRow */
            foreach($reportData->monthlyReport as $summaryRow){
                $rowClass = "";
                if ($summaryRow->isYearAggregate){
                    $rowClass = "subtotal";
                } else if ($summaryRow->isGrandTotal){
                    $rowClass = "grand-total";
                }
                ?>
            <tr class="<?php echo $rowClass; ?>">
                <td><?php echo $summaryRow->breakdownValue;?></td>
                <td class="numeric">$<?php echo $summaryRow->eligibleSales;?></td>
                <td class="numeric">$<?php echo $summaryRow->donationsGenerated;?></td>
            </tr>
                <?php } // end for each weekly summary row?>
            </tbody>
        </table>
    </div>

    <div class="two-column-right">
        <h4>Recipient Breakdown</h4>
        <table class="sales-data">
            <thead>
            <tr>
                <th>Recipient Organization</th>
                <th>Donations Generated</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /* @var $summaryRow DonationsSummaryRow */
            foreach($reportData->recipientReport as $summaryRow){
                $rowClass = "";
                if ($summaryRow->isYearAggregate){
                    $rowClass = "subtotal";
                } else if ($summaryRow->isGrandTotal){
                    $rowClass = "grand-total";
                }
                ?>
            <tr class="<?php echo $rowClass; ?>">
                <td><?php echo $summaryRow->breakdownValue;?></td>
                <td class="numeric">$<?php echo $summaryRow->donationsGenerated;?></td>
            </tr>
                <?php } // end for each weekly summary row?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#nav-menu").find("#merchant-donations").addClass("ui-state-highlight");
    });
</script>