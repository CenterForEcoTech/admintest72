<?php
include_once("../_config.php");
$pageTitle = "Admin - Reports";
?>
<html>
<?php include("../_header.php");
?>
<div id="reportContainer">
<h1>Reports Administration</h1>
<?php

$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
include_once("_reportsMenu.php");
if ($nav){
    switch ($nav){
        case "admin-registration-report":
            include("_adminRegistrationReport.php");
            break;
        case "member-registrations":
            include("views/_memberRegistrations.php");
            break;
        case "transaction-report":
            include("_transactionReport.php");
            break;
		case "business-charity-list":
			include("_businessCharityList.php");
			break;
        case "referrals-report":
            include("_referralsReport.php");
            break;
        case "merchant-nominations-report":
            include("views/_merchantNominations.php");
            break;
        case "merchant-donations":
            include("_merchantDonations.php");
            break;
        case "merchant-donations-report":
            include("_merchantDonationsReport.php");
            break;
        case "event-premiums":
            include("views/_eventPremiums.php");
            break;
        default:
            break;
    }
}
?>
</div> <!-- end container -->
<?php
include("../_footer.php");
?>