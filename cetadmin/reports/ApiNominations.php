<?php
include_once("../_config.php");
error_reporting(1); ini_set("display_errors", 1);
include_once($dbProviderFolder."MemberProvider.php"); // contains MerchantRecord and OrganizationRecord data contracts
include_once($dbProviderFolder."NominatedBusinessProvider.php");

include_once("model/_nominationsModel.php");
$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":

        $provider = new NominatedBusinessProvider($mysqli);
        $criteria = NominationDisplayTableRow::getCriteria($_GET);
        $paginatedNominations = $provider->getAdminNominations($criteria);
        $nominations = $paginatedNominations->collection;

        /* @var $nomination AdminNonprofitPreferredBusiness */
        /* @var $orgContact ContactRecord */

        if ($_GET["export"] == "csv"){
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=nominations.csv");
            header("Content-Transfer-Encoding: binary");
            $nomination_data = array();
            $colHeaders = array();
            foreach ($nominations as $nomination){
                $nomination_data[] = NominationExportRow::getRow($nomination);
                if (!count($colHeaders)){
                    foreach($nomination_data[0] as $column => $value){
                        $colHeaders[] = $column;
                    }
                }
            }

            $fileContents = exportCSV($nomination_data, $colHeaders, true);
            echo $fileContents;
            die();
        } else {
            $results = array(
                "iTotalRecords" => $paginatedNominations->totalRecords,
                "iTotalDisplayRecords" => $paginatedNominations->totalRecords,
                "sEcho" => $_GET["sEcho"]
            );
            foreach($nominations as $nomination){
                $results["aaData"][] = new NominationDisplayTableRow($nomination, $CurrentServer);
            }
            if (!isset($results["aaData"])){
                $results["aaData"] = array();
            }
            output_json($results);
            die();
        }
        break;
    case "POST":
        if ($_POST['id'] && $_POST['admin_comments']){
            // update comment in the db.
            $provider = new NominatedBusinessProvider($mysqli);
            $response = $provider->addAdminComment($_POST['id'], $_POST['admin_comments']);
            if ($response){
                $results->success = true;
            } else {
                header("HTTP/1.0 409 Conflict");
                $results->success = false;
            }
        }
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();
