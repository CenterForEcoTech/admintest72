<?php
if (!isset($pageTitle)){
    $pageTitle = "Admin Console";
}
if (!isset($bodyCssClass)){
    $bodyCssClass = "";
}
$bodyCssClass .= " ".$Config_environmentName;
?>
<html>
<head>
    <title><?php echo $pageTitle;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- jQuery - the core -->
    <link type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>css/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>css/jquery.dataTables_themeroller.css" rel="stylesheet" />

    <!-- markdown editor -->
    <link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer;?>js/wmd/wmd.css"/>

    <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/skeleton.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer?>css/jquery.tagit.css" />
    <link rel="stylesheet" href="<?php echo $CurrentServer.$adminFolder;?>style.css">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CurrentServer;?>images/ct_icon3.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/css/dataTables.tableTools.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $CurrentServer.$adminFolder;?>js/extensions/FixedColumns/css/dataTables.fixedColumns.css">

    <?php
    if (!empty($customCSS)){
        echo $customCSS;
    }
    ?>
    <!-- some javascript to support inline code-->
    <script type="text/javascript" src="<?php echo $CurrentServer.($fileuploadpage ? "js/jquery-file-upload/jquery-1.8.3.min.js" : $adminFolder."js/jquery-1.11.1.min.js"); ?>" ></script>
    <script src="<?php echo $CurrentServer.$adminFolder;?>js/jquery-ui-1.9.2.custom.min.js"></script>
    <!--[if !IE 7]>
    <style type="text/css">
        #admin-wrap {display:table;height:100%}
    </style>
    <![endif]-->
    <style type="text/css">
        body.prod{background-color:#EEF1FB;}
        body.demo{background-color:#F1FBEE;}
        body.stag{background-color:#FFFAE5;}
    </style>
</head>
<body id="admin" class="<?php echo $bodyCssClass;?>">
<div id="admin-wrap">

    <div id="admin-main">
        <?php require_once('_loginform.php');?>