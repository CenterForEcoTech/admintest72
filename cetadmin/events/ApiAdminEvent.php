<?php
include_once("../_config.php");
error_reporting(1); ini_set("display_errors", 1);
include_once($dbProviderFolder."AdminEventProvider.php");

include_once("model/_eventListModel.php");
$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $newRecord = json_decode($currentRequest->rawData);
        if ($newRecord->isDraft){
            $results = new ExecuteResponseRecord();
            $results->error = "DELETE is not supported for draft events yet.";
            header("HTTP/1.0 409 Conflict");
        } else {
            include_once($dbProviderFolder."MySqliEventManager.php");
            include_once($indexProviderFolder."IndexedEventManager.php");

            $IndexingProviderURL = $Config_IndexingURLPrivate;
            $IndexName = $Config_IndexingEventsIndex;

            $eventProvider = new EventProvider($mysqli);
            $mysqlEventManager = new MySqliEventManager($mysqli);
            $indexedEventManager = new IndexedEventManager($IndexingProviderURL, $IndexName);

            $eventRecord = $eventProvider->getEventById($newRecord->id);
            $arrayOfVenues = $eventProvider->getMerchantsForEvent($newRecord->id);

            $deleteResponse = $mysqlEventManager->deleteEvent($eventRecord, $arrayOfVenues, 0, getAdminId());
            if ($deleteResponse->success){
                $deleteDocResponse = $indexedEventManager->deleteEvent($eventRecord, $arrayOfVenues, 0, getAdminId());
            }

            if ($deleteResponse->success){
                $results = $deleteResponse;
                header("HTTP/1.0 200 Success");
            } else {
                $results = $deleteResponse;
                header("HTTP/1.0 409 Conflict");
            }
        }
        break;
    case "GET":
        $provider = new AdminEventProvider($mysqli);
        if ($_GET['isDraft']){
            $provider = new AdminEventDraftProvider($mysqli);
        }
        $criteria = EventListModel::getCriteria($_GET);
        $criteria->isMerchantEvent = true; // force only merchant events
        $paginatedRegistrations = $provider->get($criteria);
        $recordCollection = $paginatedRegistrations->collection;

        /* @var $record EventRecord */
        $results = array(
            "iTotalRecords" => $paginatedRegistrations->totalRecords,
            "iTotalDisplayRecords" => $paginatedRegistrations->totalRecords,
            "sEcho" => $_GET["sEcho"]
        );
        foreach($recordCollection as $record){
            $results["aaData"][] = new EventListModel($record);
        }
        if (!isset($results["aaData"])){
            $results["aaData"] = array();
        }
        break;
    case "POST":
        $results->message = "POST is not supported at this time.";
        break;
    case "PUT":
        $newRecord = json_decode($currentRequest->rawData);
        include_once("model/_editEventModel.php");

        $results = new ResponseWrapper();
        $model = new PostEventModel(new MerchantMemberProvider($mysqli), $results, array(json_decode($currentRequest->rawData)), new AdminEventModelConverter($mysqli));
        $model->process();

        if ($model->header){
            header($model->header);
        }
        break;
}
output_json($results);
die();
