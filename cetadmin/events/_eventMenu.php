<div id="nav-menu">
    <ul>
        <li><a id="event-list" href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=event-list" class="button-link">Published Events</a></li>
        <li><a id="event-draft-list" href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=event-draft-list" class="button-link">Draft Events</a></li>
        <li><a id="add-event" href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=add-event" class="button-link">Add Event</a></li>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            } else {
                if (currentPage === "edit-event"){
                    $("#event-list").addClass("ui-state-highlight");
                } else if (currentPage == 'edit-draft-event'){
                    $("#event-draft-list").addClass("ui-state-highlight");
                }
            }
        }
    });
</script>