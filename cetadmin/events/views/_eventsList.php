<?php
$pageTitle = ($_GET['isDraft']) ? "Draft Events" : "Merchant Events";
?>

<h3><?php echo $pageTitle;?></h3>
<table id="event-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Event</th>
        <th>Date & Time</th>
        <th>Merchant</th>
        <th>Charity</th>
        <th>Campaigns</th>
        <th>Online</th>
        <th>National</th>
        <th>Is CT</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
$(function(){
    var tableElement = $("#event-table"),
        apiUrl = "ApiAdminEvent.php",
        lastRequestedCriteria = null,
        oTable = tableElement.dataTable({
            "bJQueryUI": true,
            "bServerSide":true,
            "sAjaxSource": apiUrl,
            "aaSorting": [[ 0, "desc" ]],
            "aoColumns" :[
                {
                    "mData": "id",
                    "sWidth": "50px",
                    "mRender": function(data, type, full){
                        var navPrefix = "?nav=" + "<?php echo ($_GET['isDraft']) ? "edit-draft-event" : "edit-event"; ?>";
                            href = navPrefix + "&id=" + data,
                            renderedText = "<a href='" + href + "'>" + data + "</a>";
                        return renderedText;
                    }
                },
                {"mData": "name"},
                {"mData": "dates"},
                {
                    "mData": "merchant",
                    "sWidth": "250px",
                    "mRender": function(data, type, full){
                        var renderedText = "<a href='<?php echo $CurrentServer.$adminFolder;?>merchants/?nav=profile-edit&id=" + data.id + "' target='_blank'>" + data.name + "</a><br>";
                        for (var i = 0; i < data.addressLines.length; i++){
                            renderedText += data.addressLines[i] + "<br>";
                        }
                        return renderedText;
                    }
                },
                {
                    "mData": "cause",
                    "mRender": function(data, type, full){
                        var renderedText = "<a href='<?php echo $CurrentServer.$adminFolder?>charities/?nav=profile-edit&id=" + data.id + "' target='_blank'>" + data.name + "</a>";
                        if (data.id){
                            return renderedText;
                        }
                        return "";
                    }
                },
                {"mData": "campaigns"},
                {
                    "mData": "isOnline",
                    "sWidth": "50px",
                    "mRender": function(data, type, full){
                        if (data){
                            return "Yes";
                        }
                        return "";
                    }
                },
                {
                    "mData": "isNational",
                    "sWidth": "50px",
                    "mRender": function(data, type, full){
                        if (data){
                            return "Yes";
                        }
                        return "";
                    }
                },
                {
                    "mData": "isCtEvent",
                    "sWidth": "50px",
                    "mRender": function(data, type, full){
                        if (data){
                            return "Yes";
                        }
                        return "";
                    }
                }
            ],
            "aoColumnDefs": [
                { "bSearchable": false, "aTargets": [ 0 ] }
            ],
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "isDraft", "value": "<?php echo $_GET['isDraft'];?>" } );
            },
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                lastRequestedCriteria = aoData;
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            }
        });
});
</script>