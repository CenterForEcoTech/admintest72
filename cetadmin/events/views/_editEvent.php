<div class="container">
<?php
/* @var $model EditEventModel */
if ($model->isValid()){
    /* @var $record EventRecord */
    $record = $model->record;
    ?>
    <style type="text/css">
        #edit-event-form .demotable {background-color: #FFEECC;}
        #edit-event-form .demoted {background-color:#ADADAD;}
        .ui-state-preferred { border: 1px solid #2694e8; background: #3baae3 url(../css/images/ui-bg_glass_50_3baae3_1x400.png) 50% 50% repeat-x; font-weight: bold; color: #ffffff; }
        fieldset.merge-tags ul li span{font-weight:bold;}
        fieldset.merge-tags ul li {margin:5px 0;}
        #admin fieldset.merge-tags{background: #E5F9FF; width:95%;};
    </style>
    <div class="sixteen columns alpha">
    <h2 class="twelve columns alpha"><?php echo $model->getPageTitle();?></h2>
        <?php if ($record->id && !$record->isDraft) {?>
            <a href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=event-logs&event_id=<?php echo $record->id;?>" class="button-link" target="_blank" style="float:right;">Event Logs</a>
            <a href="<?php echo $CurrentServer;?>event/<?php echo $record->id;?>" class="button-link" target="_blank" style="float:right; margin:5px">View Published Event</a>
        <?php } ?>
    </div>
<?php if ($record->id && !$record->isDraft) {?>
    <div class="sixteen columns alpha">
        <a href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-migration&Records=viewSpecificEventID&viewSpecificEventID=<?php echo $record->id;?>" class="button-link" target="_blank" style="float:right;">View Indexed Document</a>
    </div>
    <div class="sixteen columns alpha" style="margin-top:5px;">
        <a href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-migration&Records=SpecificEventID&SpecificEventID=<?php echo $record->id;?>" class="button-link" target="_blank" style="float:right;">Reindex Event</a>
    </div>
<?php } ?>
    <form id="edit-event-form">
        <fieldset class="sixteen columns alpha">
            <legend>Venue</legend>
            <?php if (!$record->venue->id) {?>
                <p class="help">Once you select a merchant and save the record, you will not be able to change the merchant.</p>
                <script type="text/javascript">
                    var CETDASH = CETDASH || {};
                    CETDASH.MerchantSearchWidget = CETDASH.MerchantSearchWidget || {
                        showState: true,
                        dropOnFocus: false,
                        sourceUrl : "<?php echo $CurrentServer.$adminFolder;?>merchants/ApiMerchantProfile.php",
                        sourceType : "GET",
                        minLength: 3,
                        pageLength: 10,
                        onSelect: function( event, ui ) {
                            var record = ui.item.record,
                                formElement = $("#edit-event-form");
                            formElement.find("input[name='venue.id']").val(record.id);
                            formElement.find("a#venue-name").html(record.name).attr("href", "<?php echo $CurrentServer.$adminFolder;?>merchants/?nav=profile-edit&id=" + record.id);
                            formElement.find("#venue-address").html(record.address);
                            formElement.find("#venue-city").html(record.city + ",");
                            formElement.find("#venue-state").html(record.state);
                            formElement.find("#venue-zip").html(record.postalCode);
                        }
                    };
                </script>
                <style type="text/css">
                    .ui-widget-content .ui-icon.btn{
                        padding:0;
                    }
                </style>
                <?php include $siteRoot.$adminFolder."merchants/views/_merchantPickerWidget.php";?>
            <?php } ?>
            <div style="clear:both;">
                <div class="six columns alpha">
                    <div class="five columns" style="min-height:75px;">
                        <input type="hidden" name="venue.id" value="<?php echo $record->venue->id ? $record->venue->id : 0;?>">
                        <label for="merchant-search-box">Merchant:</label>
                        <div><a id="venue-name" href="<?php echo $CurrentServer.$adminFolder;?>merchants/?nav=profile-edit&id=<?php echo $record->venue->id;?>" target="_blank"><?php echo $record->venue->name;?></a></div>
                        <div id="venue-address"><?php echo $record->venue->streetAddr;?></div>
                        <div><span id="venue-city"><?php echo $record->venue->city;?></span> <span id="venue-state"><?php echo $record->venue->region;?></span> <span id="venue-zip"><?php echo $record->venue->postalCode;?></span></div>
                    </div>
                    <?php if (count($model->getEventTemplatePremiums())) {
                        $premiumRecords = $model->getEventTemplatePremiums();
                        if ($model->defaultPremiumId && !$record->venue->eventTemplatePremiumId){
                            // set default now
                            $record->venue->eventTemplatePremiumId = $model->defaultPremiumId;
                        }
                        ?>
                        <fieldset class="five columns alpha omega" style="margin-top:20px;">
                            <legend>Event Premiums</legend>
                            <?php foreach ($premiumRecords as $premiumRecord) {
                                /* @var $premiumRecord EventTemplatePremium */
                                $selected = $record->venue->eventTemplatePremiumId == $premiumRecord->id ? "checked" : "";
                                ?>
                                <label>
                                    <input type="radio" name="venue.eventTemplatePremiumId" value="<?php echo $premiumRecord->id;?>" <?php echo $selected;?>><?php echo $premiumRecord->name." ($".$premiumRecord->price.")"?>
                                </label>
                            <?php } ?>
                        </fieldset>

                    <?php } else { ?>
                        <input type="hidden" name="venue.eventTemplatePremiumId" value="<?php echo $record->venue->eventTemplatePremiumId;?>">
                    <?php } ?>
                </div>
                <fieldset class="nine columns demotable">
                    <legend>Online Sales</legend>
                    <label>
                        <input type="checkbox" name="isOnlineEvent" value="1" <?php if ($record->isOnlineEvent) { echo "checked"; } ?>>
                        Is Online Event?
                    </label>
                    <label>
                        <div>Order Online Label:</div>
                        <input type="text" name="onlineLocationLabel" class="auto-watermarked nine columns alpha" title="Order Online" value="<?php echo $record->onlineLocationLabel;?>">
                    </label>
                    <label>
                        <div>Order Online Link:</div>
                        <input type="text" name="onlineLocationLink" class="auto-watermarked nine columns alpha" title="https://merchant-website.com/store" value="<?php echo $record->onlineLocationLink;?>">
                    </label>
                    <label>
                        <input type="checkbox" name="isNationalEvent" value="1" <?php if ($record->isNationalEvent) { echo "checked"; } ?>>
                        Is National Event?
                    </label>
                </fieldset>
            </div>
            <fieldset class="fifteen columns alpha">
                <legend>Admin Only</legend>

                <label class="fifteen columns alpha omega">
                    <div>Custom Eventbrite Link:</div>
                    <input type="text" name="customAttendLink" class="eleven columns alpha auto-watermarked" title="https://www.eventbrite.com/event/1234?access=causetown" value="<?php echo $record->customAttendLink;?>">
                </label>
                <label>
                    <div>Eventbrite Event ID:</div>
                    <input type="text" name="venue.eventBriteEventID" value="<?php echo $record->venue->eventBriteEventID;?>">
                </label>
                <label>
                    <div>Facebook Event ID:</div>
                    <input type="text" name="venue.facebookEventID" value="<?php echo $record->venue->facebookEventID; ?>">
                </label>
            </fieldset>
        </fieldset>
        <fieldset class="sixteen columns alpha">
            <legend>Event Details</legend>
            <label class="eight columns alpha">
                <div>Name:</div>
                <input type="text" name="title" value="<?php echo $record->title;?>" class="eight columns alpha">
            </label>
            <label class="two columns alpha datefields">
                <div>Start Date:</div>
                <input type="text" class="auto-watermarked two columns alpha" title="mm/dd/yyyy" name="startDateString" value="<?php echo $record->startDate ? date("m/d/Y", $record->startDate) : "";?>">
            </label>
            <label class="two columns ">
                <div>Start Time:</div>
                <select name="startTime" class="two columns alpha ">
                    <?php
                    $selectedTime = $record->startDate ? strtotime(date("H:i", $record->startDate)) : strtotime("10:00");
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            </label>
            <label class="two columns alpha datefields">
                <div>End Date:</div>
                <input type="text" class="auto-watermarked two columns alpha" title="mm/dd/yyyy" name="endDateString" value="<?php echo $record->endDate ? date("m/d/Y", $record->endDate) : "";?>">
            </label>
            <label class="two columns ">
                <div>End Time:</div>
                <select name="endTime" class="two columns alpha ">
                    <?php
                    $selectedTime = $record->endDate ? strtotime(date("H:i", $record->endDate)) : strtotime("22:00");
                    $event_time = "00:00";
                    $time_block = 48;
                    $event_length = 30; // minutes
                    for($i = 0, $eTime = strtotime($event_time);
                        $i < $time_block;
                        $i++, $eTime = strtotime("+$event_length minutes", $eTime)){
                        $new_event_time = date('H:i', $eTime);
                        echo "<option value='".$new_event_time."'";
                        if ($eTime == $selectedTime){echo " selected";}
                        echo ">".date("g:i a", strtotime($new_event_time));
                        //add Midnight and Noon qualifiers
                        if ($new_event_time == "00:00"){echo " (midnight)";}
                        if ($new_event_time == "12:00"){echo " (noon)";}
                        echo "</option>\r\n";
                    }
                    ?>
                </select>
            </label>
            <div class="ten columns alpha">
                <label>
                    <div>Description:</div>
                    <textarea name="description" class="ten columns alpha" style="height: 300px;"><?php echo $record->description;?></textarea>
                </label>
                <fieldset class="merge-tags">
                    <legend>Merge Tags Help</legend>
                    <ul style="margin:0;padding:0;font-size:13px;">
                        <li><span>*|BUSINESS_NAME|*</span> = venue.name</li>
                        <li><span>*|CHARITY_AMOUNT|*</span> = 15% of purchases, or $1 for each item</li>
                    </ul>
                </fieldset>
            </div>
            <div class="five columns alpha omega">
                <fieldset class="five columns alpha demotable" style="margin-top:12px;width:310px;">
                    <legend>Causetown Event</legend>
                    <label>
                        <input type="checkbox" name="isCausetownEvent" value="1" <?php if ($record->isCausetownEvent || !$record->id) {echo "checked"; } ?>>
                        Is Causetown Event?
                    </label>
                    <label>
                        <input type="checkbox" name="suppressCharitySelectionForm" value="1" <?php if ($record->suppressCharitySelectionForm) {echo "checked"; } ?> data-orig="<?php echo $record->suppressCharitySelectionForm;?>">
                        Suppress Charity Selection Form
                    </label>
                    <label>
                        <input type="checkbox" name="suppressPrintFlier" value="1" <?php if ($record->suppressPrintFlier) {echo "checked"; } ?> data-orig="<?php echo $record->suppressPrintFlier;?>">
                        Suppress Print Flier
                    </label>
                    <?php if ($record->isDraft) {?>
                    <label>
                        <input type="checkbox" name="autoPublishFacebook" value="1" checked disabled class="demotable-child">
                        Auto Publish to Facebook
                    </label>
                    <label>
                        <input type="checkbox" name="autoPublishEventbrite" value="1" checked disabled  class="demotable-child">
                        Auto Publish to Eventbrite
                    </label>
                    <?php } ?>
                </fieldset>
                <fieldset style="width:310px;">
                    <legend>Campaign Tags</legend>
                    <?php if ($record->isDraft) { ?>
                        <p class="alert">FOR DRAFT EVENTS: Your edits here will not be saved.</p>
                    <?php } ?>
                    <label>
                        <div>Tags:</div>
                        <input id="tags" type="text" name="commaDelimitedTagNames" value="<?php echo $record->commaDelimitedTagNames;?>" class="five columns alpha">
                    </label>
                </fieldset>
            </div>
        </fieldset>
        <fieldset class="sixteen columns alpha">
            <legend>Donation Configuration</legend>
            <fieldset class="four columns alpha demotable">
                <legend>Percentage</legend>
                <label>
                    <input type="radio" name="venue.isFlatDonation" value="0" <?php if (!$record->venue->isFlatDonation) { echo "checked"; }?>>
                    Is Percentage of Sale
                </label>
                <label>
                    <div>Customer Choice:</div>
                    <select name="venue.customerChoicePercentage" class="three columns alpha">
                        <?php
                        $selectOptions = "";
                        for ($i = 0; $i <= 100; $i++){
                            $selected = ($record->venue->customerChoicePercentage == $i) ? " selected" : "";
                            $selectOptions .= "<option value='".$i."'".$selected.">".$i."%</option>\n";
                        }
                        ?>
                        <?php echo $selectOptions;?>
                    </select>
                </label>
                <label>
                    <div>Featured Cause:</div>
                    <select name="venue.featuredCausePercentage" class="three columns alpha">
                        <?php
                        $selectOptions = "";
                        for ($i = 0; $i <= 100; $i++){
                            $selected = ($record->venue->featuredCausePercentage == $i) ? " selected" : "";
                            $selectOptions .= "<option value='".$i."'".$selected.">".$i."%</option>\n";
                        }
                        ?>
                        <?php echo $selectOptions;?>
                    </select>
                </label>
            </fieldset>
            <fieldset class="five columns alpha omega demotable">
                <legend>Flat Donation</legend>

                <label>
                    <input type="radio" name="venue.isFlatDonation" value="1" <?php if ($record->venue->isFlatDonation) { echo "checked"; }?>>
                    Is FlatDonation
                </label>
                <label>
                    <div>Donation per Unit:</div>
                    <input type="text" name="venue.donationPerUnit" value="<?php echo $record->venue->donationPerUnit;?>" class="auto-watermarked" title="1.00">
                </label>
                <label>
                    <div>Action Phrase:</div>
                    <input type="text" name="venue.flatDonationActionPhrase" value="<?php echo $record->venue->flatDonationActionPhrase;?>" class="auto-watermarked" title="for every item sold">
                </label>
                <label>
                    <div>Unit Descriptor:</div>
                    <input type="text" name="venue.unitDescriptor" value="<?php echo $record->venue->unitDescriptor;?>" class="auto-watermarked" title="item(s)">
                </label>
                <label>
                    <input type="checkbox" name="venue.isFcOnly" value="1" <?php if ($record->venue->isFcOnly) { echo "checked"; }?>>
                    Is Featured Cause Only(has no effect if there is no cause assigned)
                </label>
            </fieldset>
            <fieldset class="fifteen columns alpha">
                <legend>Featured Cause</legend>
                <label for="charity-search-box">Selected Cause:</label>
                <script type="text/javascript">
                    var CETDASH = CETDASH || {};
                    CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                        flavor: "default",
                        showState: true,
                        limitToRegisteredOrgs: true,
                        dropOnFocus: false,
                        sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                        sourceType : "POST",
                        minLength: 3,
                        pageLength: 10,
                        onSelect: function( event, ui ) {
                            var record = ui.item.record,
                                formElement = $("#edit-event-form");
                            formElement.find("input[name='cause.id']").val(record.orgId);
                            formElement.find("a#cause-name").html(record.name).attr("href", "<?php echo $CurrentServer.$adminFolder;?>charities/?nav=profile-edit&id=" + record.orgId);
                            formElement.find("#remove-cause").css("display", "inline-block");
                            formElement.find(".charity-search-widget").hide();
                        }
                    };
                </script>
                <style type="text/css">
                    .ui-widget-content .ui-icon.btn{
                        padding:0;
                    }
                </style>
                <?php include $siteRoot."views/_charityPickerWidget.php";?>
                <div style="clear:both;">
                    <input type="hidden" name="cause.id" value="<?php echo $record->cause->id ? $record->cause->id : 0;?>">
                    <span><a id="cause-name" href="<?php echo $CurrentServer.$adminFolder;?>charities/?nav=profile-edit&id=<?php echo $record->cause->id;?>" target="_blank"><?php echo $record->cause->name;?></a></span>
                    <a href="RemoveCharity" id="remove-cause" class="button-link do-not-navigate ui-state-highlight">Remove</a>
                </div>
            </fieldset>
        </fieldset>

        <fieldset class="sixteen columns alpha">
            <legend>Actions</legend>
            <div id="actionResults" class="fifteen columns"></div>
            <input type="hidden" name="id" value="<?php echo $record->id ? $record->id : 0;?>">
            <input type="hidden" name="status" value="<?php echo $record->status ? $record->status : 'ACTIVE';?>">
            <input type="hidden" name="isDraft" value="<?php echo ($record->isDraft || !$record->id) ? 1 : 0;?>">
            <input type="hidden" name="eventTemplateId" value="<?php echo $record->eventTemplateId ? $record->eventTemplateId : 0;?>">

            <?php if ($record->id) {?>
                <a href="<?php echo $CurrentServer.$adminFolder;?>events/?nav=edit-<?php echo $record->isDraft ? "draft-" : "";?>event&id=<?php echo $record->id;?>" class="button-link ui-state-highlight">Cancel</a>
            <?php }
            if ($record->isDraft) { ?>
                <a href="SaveDraftEvent" id="save-draft-event-button" class="button-link do-not-navigate">Save Draft Event</a>
            <?php } ?>

            <?php if ($model->canSave() && !$model->canPublish()) {?>
                <a href="SaveEvent" id="save-event-button" class="button-link do-not-navigate" title="Save this event">Save Event</a>
            <?php } ?>

            <?php if ($model->canPublish()) {?>
                <a href="Publish" id="publish-event-button" class="button-link do-not-navigate ui-state-preferred" title="Publish this event">Publish Event</a>
            <?php } ?>

            <?php if ($model->canDelete()) {?>
                <a href="DeleteEvent" id="delete-event-button" class="button-link do-not-navigate ui-state-error" title="Delete this event" style="float:right;">Delete Event</a>
            <?php } ?>

            <?php if ($record->status !== "ACTIVE") {?>
                <p class="alert">This a <?php echo $record->status;?> Event</p>
            <?php } ?>
        </fieldset>
    </form>
    <div id="dialog-confirm-delete" title="Delete this event?" style="display:none;">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete this event?</p>
    </div>
    <script type="text/javascript">
        $(function(){
            var formElement = $("#edit-event-form"),
                titleEl = formElement.find("input[name='title']"),
                startDateEl = formElement.find("input[name='startDateString']"),
                startTimeEl = formElement.find("select[name='startTime']"),
                endDateEl = formElement.find("input[name='endDateString']"),
                endTimeEl = formElement.find("select[name='endTime']"),
                dateFieldsContainers = formElement.find(".datefields"),
                areDatesValid = function(validatingStart){
                    var startDateVal = startDateEl.val(),
                        startTimeVal = startTimeEl.val(),
                        endDateVal = endDateEl.val(),
                        endTimeVal = endTimeEl.val(),
                        isValid = true;

                    dateFieldsContainers.find("span.error").remove();

                    if (startDateVal && endDateVal){

                        var	startDate = new Date(startDateVal + " " + startTimeVal),
                            endDate = new Date(endDateVal + " " + endTimeVal);

                        if (endDate < startDate && (startTimeVal && endTimeVal)){
                            if (validatingStart){
                                CETDASH.forms.placeError("Must be earlier than End Time", startDateEl);
                            } else {
                                CETDASH.forms.placeError("Must be later than Start Time", endDateEl);
                            }
                            isValid = false;
                        }
                    } else{
                        if (!startDateVal){
                            CETDASH.forms.placeError("Start Date cannot be blank", startDateEl);
                        }
                        if (!endDateVal){
                            CETDASH.forms.placeError("End Date cannot be blank", endDateEl);
                        }
                        isValid = false;
                    }
                    return isValid;
                },
                saveEvent = function(formData){
                    $.ajax({
                        url: "ApiAdminEvent.php",
                        type: "PUT",
                        data: JSON.stringify(formData),
                        success: function(data){
                            var responseRecord;
                            $('#actionResults').html("Saved").addClass("alert").addClass("info");
                            if (data.response && $.isArray(data.response) && data.response.length > 0){
                                responseRecord = data.response[0];
                                if (responseRecord.createSuccessURL){
                                    window.location.href = responseRecord.createSuccessURL;
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            var response = $.parseJSON(jqXHR.responseText),
                                currentError, elementName, message,
                                validatedElement;
                            $('#actionResults').html(response.message).addClass("alert");
                            if (response.errors && $.isArray(response.errors) && response.errors.length){
                                CETDASH.forms.handleStandardErrors(formElement, response.errors);
                            }
                        }
                    });
                },
                causeIdField = formElement.find("input[name='cause.id']");

            // setup
            formElement.find(".demotable").each(function(index, element){
                var self = $(element),
                    trigger = self.find("input[type='checkbox'], input[type='radio']").first(),
                    triggerName = trigger.attr("name"),
                    childCheckboxes = self.find("input[type='checkbox']").not(".demotable-child"),
                    demotableChildren = self.find("input[type='checkbox'].demotable-child"),
                    resetElement = function(){
                        if (trigger.is(":checked")){
                            self.removeClass("demoted");
                            demotableChildren.each(function(index, element){
                                $(element).prop("checked", true);
                            });
                            if (triggerName == "isCausetownEvent"){
                                childCheckboxes.each(function(index, element){
                                    var thisCheckbox = $(element),
                                        origValue = thisCheckbox.data("orig");
                                    if (thisCheckbox.attr("name") !== triggerName){
                                        thisCheckbox.prop("checked", origValue ? true : false);
                                        thisCheckbox.prop("disabled", false);
                                    }
                                });
                            }
                        } else {
                            self.addClass("demoted");
                            demotableChildren.each(function(index, element){
                                $(element).prop("checked", false);
                            });
                            if (triggerName == "isCausetownEvent"){
                                childCheckboxes.each(function(index, element){
                                    var thisCheckbox = $(element);
                                    if (thisCheckbox.attr("name") !== triggerName){
                                        thisCheckbox.prop("checked", true);
                                        thisCheckbox.prop("disabled", true);
                                    }
                                });
                            }
                        }
                    };

                if (trigger.length){
                    resetElement();
                    trigger.on("change", function(e){
                        resetElement();
                        if (trigger.is(":checked") && trigger.attr("type") === "radio"){
                            formElement.find("input[name='"+ trigger.attr("name") +"']").each(function(index, element){
                                var self = $(element);
                                if (!self.is(":checked")){
                                    self.trigger("change");
                                }
                            });
                        }
                    });
                }
            });

            $("#tags").tagit({
                availableTags:<?php echo json_encode($model->getAvailableTags());?>
            });

            if (causeIdField.val() > 0){
                // cause is assigned
                formElement.find(".charity-search-widget").hide();
                formElement.find("#remove-cause").show();
            } else {
                formElement.find(".charity-search-widget").show();
                formElement.find("#remove-cause").hide();
            }

            // events
            formElement.on("click", "#remove-cause", function(e){
                e.preventDefault();
                causeIdField.val("");
                formElement.find("#cause-name").html("");
                $(this).hide();
                formElement.find(".charity-search-widget").show();
            });

            formElement.on("click", "#save-event-button, #publish-event-button", function(e){
                var formData = formElement.serializeObject();
                $('#actionResults').html("").removeClass("alert").removeClass("info");
                e.preventDefault();
                if (areDatesValid() && formElement.valid()){
                    saveEvent(formData);
                }
            });

            formElement.on("click", "#save-draft-event-button", function(e){
                var formData = formElement.serializeObject();
                $('#actionResults').html("").removeClass("alert").removeClass("info");
                e.preventDefault();
                if (areDatesValid() && formElement.valid()){
                    formData.saveAsDraft = 1;
                    saveEvent(formData);
                }
            });

            formElement.on("click", "#delete-event-button", function(){

                $( "#dialog-confirm-delete" ).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Delete": function() {
                            var url = "<?php echo $CurrentServer.$adminFolder;?>events/ApiAdminEvent.php",
                                data = {
                                    id: '<?php echo $record->id;?>',
                                    isDraft: '<?php echo $record->isDraft;?>'
                                };

                            // delete
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                data: JSON.stringify(data),
                                dataType: "json",
                                contentType: "application/json",
                                statusCode:{
                                    200: function(data){
                                        alert("Event has been deleted. The page will be refreshed");
                                        window.location.href = window.location.href;
                                    },
                                    409: function(jqXHR, textStatus, errorThrown){
                                        var data = $.parseJSON(jqXHR.responseText);
                                        if (data.message){
                                            alert(data.message);
                                        } else if (data.error){
                                            alert(data.error);
                                        } else {
                                            alert("Unexpected condition encountered. Please refresh the page and try again.")
                                        }
                                    }
                                }
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            });

            titleEl.blur(function(){
                formElement.validate().element(titleEl);
            });
            startDateEl.datepicker({
                minDate: 0,
                onSelect: function(dateText, inst) {
                    if (endDateEl.val()){
                        if (!areDatesValid(true)) {
                            endDateEl.val(dateText);
                        }
                    } else {
                        endDateEl.val(dateText);
                    }
                }
            });
            endDateEl.datepicker({
                minDate: 0,
                onSelect: function(dateText, inst) {
                    areDatesValid();
                }
            });
            startTimeEl.change(function(){
                areDatesValid(true);
            });
            endTimeEl.change(function(){
                areDatesValid();
            });

            // validation
            formElement.validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 75
                    }
                },
                messages: {
                    title: {
                        required: "Please give a title to your event.",
                        maxlength: "Title must be 75 characters or less."
                    }
                }
            });
        });
    </script>
    <?php
} else {?>
    <h2>Invalid Event Record</h2>
    <p class="alert">This form can only be used to edit Merchant Events. You may not edit any other event types here.</p>
<?php
}

if ($record->id && !$record->isDraft){
    $relevantEntity = SocialMessageSample::EVENT_ENTITY;
    $relevantEntityId = $record->id;
    include($rootFolder.$adminFolder."cms/views/_socialMessagesList.php");
}
?>
</div>