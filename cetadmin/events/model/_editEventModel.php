<?php
include_once($dbProviderFolder."AdminEventProvider.php");
include_once($dbProviderFolder."EventTransactionProvider.php");
include_once($dbProviderFolder."CampaignProvider.php");
include_once($dbProviderFolder."MemberProvider.php");
include_once($dbProviderFolder."EventTemplateProvider.php");
include_once($dbProviderFolder."EventTemplatePremiumProvider.php");
include_once($siteRoot."models/events/_merchantCreateEventModel.php");

class EditEventModel {

    public $record;
    public $hasTransactions;
    public $tagNames;
    public $premiums;
    public $defaultPremiumId;
    private $isValid = true;

    public function __construct($recordId, $eventTemplateId = null, $isDraft = false){
        global $mysqli, $Config_DefaultCreateEventTemplate;
        if (isset($recordId) && is_numeric($recordId) && $recordId > 0){
            $provider = new AdminEventProvider($mysqli);
            if ($isDraft){
                $provider = new AdminEventDraftProvider($mysqli);
            }
            $criteria = new EventCriteria();
            $criteria->eventId = $recordId;
            $paginationResult = $provider->get($criteria);
            $resultArray = $paginationResult->collection;
            if (count($resultArray)){
                /* @var $record EventRecord */
                $this->record = $resultArray[0];
                $transactionProvider = new EventTransactionProvider($mysqli);
                $this->hasTransactions = $transactionProvider->hasTransactions($this->record->id);
            } else {
                $this->isValid = false;
            }
        } else {
            $defaultEventTemplate = null;
            if (isset($eventTemplateId) && is_numeric($eventTemplateId) && $eventTemplateId > 0){
                $defaultEventTemplate = $this->getEventTemplateById($eventTemplateId);
            } else if (isset($eventTemplateId) && !is_numeric($eventTemplateId) && strlen($eventTemplateId) > 0){
                $defaultEventTemplate = $this->getEventTemplateByName($eventTemplateId);
            } else {
                $defaultEventTemplate = $this->getEventTemplateById($Config_DefaultCreateEventTemplate);
            }
            $this->record = new EventRecord();
            $this->record->venue = new Venue();
            $this->record->cause = new Cause();
            $this->record->isDraft = true;
            $this->record->status = 'ACTIVE';
            $this->record->setStartDate($defaultEventTemplate->startDate, $defaultEventTemplate->startTime);
            $this->record->setEndDate($defaultEventTemplate->endDate, $defaultEventTemplate->endTime);
            $this->record->venue->customerChoicePercentage = 20;
            $this->record->venue->featuredCausePercentage = 20;

            if ($defaultEventTemplate){
                $this->record->eventTemplateId = $defaultEventTemplate->id;
                $this->record->title = $defaultEventTemplate->eventName;
                $this->record->description = $defaultEventTemplate->description;
                $this->record->commaDelimitedTagNames = implode(",", $defaultEventTemplate->tagNamesArray);
            }
            if ($defaultEventTemplate->percentageID){
                $this->record->venue->customerChoicePercentage = $defaultEventTemplate->percentageID;
            }
            $this->record->commaDelimitedTagNames = implode(",", $defaultEventTemplate->tagNamesArray);
        }
    }

    private function getEventTemplateById($templateId){
        global $mysqli;
        if ($templateId > 0){
            $EventTemplateProvider = new EventTemplateProvider($mysqli);

            $eventTemplate = $EventTemplateProvider->getTemplateById($templateId, false);
            if (isset($eventTemplate->id)){
                return $eventTemplate;
            }
        }
        return null;
    }

    private function getEventTemplateByName($templateName){
        global $mysqli;
        if (strlen($templateName) > 0){
            $EventTemplateProvider = new EventTemplateProvider($mysqli);

            $eventTemplate = $EventTemplateProvider->getTemplate($templateName);
            if (isset($eventTemplate->id)){
                return $eventTemplate;
            }
        }
        return null;
    }

    public function isValid(){
        return $this->isValid;
    }

    public function getPageTitle(){
        if ($this->record->id){
            $pageTitle = ($this->record->isDraft) ? "Edit Draft Event" : "Edit Event";
        } else {
            $pageTitle = "Create Event";
        }
        $pageTitle .= $this->record->title ? ": ".$this->record->title : ": Untitled";
        return $pageTitle;
    }

    public function getAvailableTags(){
        global $mysqli;
        if (!isset($this->tagNames) || !is_array($this->tagNames)){
            $tagProvider = new CampaignProvider($mysqli);
            $criteria = new CampaignCriteria();
            $criteria->size = 0; // get them all
            $results = $tagProvider->get($criteria);
            $tags = $results->collection;
            $this->tagNames = array();
            foreach($tags as $tag){
                /* @var $tag CampaignRecord */
                $this->tagNames[] = $tag->name;
            }
        }

        return $this->tagNames;
    }

    public function getEventTemplatePremiums(){
        global $mysqli;
        if (($this->record->eventTemplateId || $this->record->venue->eventTemplatePremiumId) && (!isset($this->premiums) || !is_array($this->premiums))){
            $provider = new EventTemplatePremiumProvider($mysqli);
            if (!$this->record->eventTemplateId){
                $results = $provider->getPremiumsByPremiumId($this->record->venue->eventTemplatePremiumId);
            } else {
                $results = $provider->getPremiums($this->record->eventTemplateId);
            }
            if (count($results)){
                $this->premiums = $results;
                foreach ($results as $premiumRecord){
                    /* @var $premiumRecord EventTemplatePremium */
                    if ($this->record->eventTemplateId != $premiumRecord->eventTemplateId){
                        $this->record->eventTemplateId = $premiumRecord->eventTemplateId;
                    }
                    if ($premiumRecord->isDefault){
                        $this->defaultPremiumId = $premiumRecord->id;
                        break;
                    }
                }
            } else {
                $this->premiums = array();
            }
        }
        return $this->premiums;
    }

    public function canDelete(){
        return $this->record->id && $this->record->status == 'ACTIVE' && !$this->hasTransactions;
    }

    public function canSave(){
        return $this->record->status == 'ACTIVE';
    }

    public function canPublish(){
        return $this->record->isDraft && $this->record->status == 'ACTIVE';
    }
}

abstract class StandardEventModelConverter implements EventModelConverter{

    private $saveAsDraft = false;

    /* @var PostEventResponse */
    public $responseRecord;

    public function isSaveAsDraft(){
        return $this->saveAsDraft ? true : false;
    }

    public function convertFormPost($postedRecord){
        $this->saveAsDraft = $postedRecord->saveAsDraft;
        $this->responseRecord = new PostEventResponse();
        $model = new EditEventModel($postedRecord->id, $postedRecord->eventTemplateId, $postedRecord->isDraft);
        /* @var $record EventRecord */
        $record = $model->record;
        $postedValuesArray = get_object_vars($postedRecord);
        foreach ($postedValuesArray as $postedName => $postedValue){
            $nameParts = explode(".", $postedName);
            if (count($nameParts) > 1){
                // venue or cause
                if ($nameParts[0] == "venue"){
                    if (property_exists("Venue", $nameParts[1])){
                        switch ($nameParts[1]){
                            case "flatDonationActionPhrase":
                                $record->venue->flatDonationActionPhrase = htmlspecialchars($postedValue);
                                break;
                            case "unitDescriptor":
                                $record->venue->unitDescriptor = htmlspecialchars($postedValue);
                                break;
                            case "donationPerUnit":
                                $record->venue->donationPerUnit = is_numeric($postedValue) ? $postedValue : 0;
                                break;
                            case "isFlatDonation":
                                $record->venue->isFlatDonation = $postedValue ? 1 : 0;
                                break;
                            default:
                                $record->venue->$nameParts[1] = $postedValue;
                        }
                    }
                } else if ($nameParts[0] == "cause"){
                    if (property_exists("Cause", $nameParts[1])){
                        $record->cause->$nameParts[1] = $postedValue;
                    }
                }
                if (property_exists($this->responseRecord, $nameParts[1])){
                    $this->responseRecord->$nameParts[1] = $postedValue;
                }
            } else {
                if (property_exists("EventRecord", $postedName)){
                    $record->$postedName = $postedValue;
                }
                if (property_exists($this->responseRecord, $postedName)){
                    $this->responseRecord->$postedName = $postedValue;
                }
            }
        }

        // by this time, auto-mappable properties have been assigned. Handle exceptions and checkboxes with no values.
        $record->setStartDate($postedRecord->startDateString, $postedRecord->startTime);
        $record->setEndDate($postedRecord->endDateString, $postedRecord->endTime);
        if (!$postedRecord->isCausetownEvent){
            $record->isCausetownEvent = false;
        }
        if (!$postedRecord->isOnlineEvent){
            $record->isOnlineEvent = false;
        }
        if (!$postedRecord->isNationalEvent){
            $record->isNationalEvent = false;
        }
        if (!$postedRecord->{'venue.isFcOnly'}){
            $record->venue->isFcOnly = false;
        }
        if (!$postedRecord->suppressCharitySelectionForm){
            $record->suppressCharitySelectionForm = false;
        }
        if (!$postedRecord->suppressPrintFlier){
            $record->suppressPrintFlier = false;
        }
        if (!$postedValuesArray['venue.isFlatDonation']){
            $record->venue->flatDonationActionPhrase = "";
            $record->venue->unitDescriptor = "";
            $record->venue->donationPerUnit = "";
        }
        // if there is a causeId, but no other data, get that data now
        if ($record->cause->id && !$record->cause->name){
            $record->cause = $this->getEventCause($record->cause->id);
        }
        return $record;
    }

    abstract protected function getEventCause($causeId);

    private $requiredFields = array("title");
    private $dateFields = array("start", "end");
    public function validateData(ResponseWrapper $responseWrapper, $postedEvent, $eventIndex){
        if (!property_exists($postedEvent, "start")){
            $postedEvent->start = $postedEvent->startDateString." ".$postedEvent->startTime;
            $postedEvent->end = $postedEvent->endDateString." ".$postedEvent->endTime;
        }
        foreach ($this->requiredFields as $requiredField){
            $value = $postedEvent->$requiredField."";
            if (strlen($value) == 0){
                $responseWrapper->addValidationError($requiredField, "This is a required field.", $eventIndex);
            }
        }
        // date format check
        foreach ($this->dateFields as $dateField){
            $value = $postedEvent->$dateField."";
            $splitValue = explode(' ', trim($value));
            if (count($splitValue) != 2){
                $responseWrapper->addValidationError($dateField."DateString", "This must be a valid date and time.", $eventIndex);
            } else if (strlen($value) == 0 || !strtotime($value)){
                $responseWrapper->addValidationError($dateField."DateString", "This must be a valid date and time.", $eventIndex);
            }
        }

        $postedValuesArray = get_object_vars($postedEvent);
        // percentage check / flat donation
        if ($postedValuesArray['venue.isFlatDonation']){
            $donationPerUnit = $postedValuesArray["venue.donationPerUnit"];
            if (!is_numeric($donationPerUnit) || $donationPerUnit <= 0){
                $responseWrapper->addValidationError("venue.donationPerUnit", "This must be a dollar value greater than 0.", $eventIndex);
            }
            if (strlen($postedValuesArray["venue.flatDonationActionPhrase"]) == 0){
                $responseWrapper->addValidationError("venue.flatDonationActionPhrase", "Please specify an action phrase.", $eventIndex);
            }
            if (strlen($postedValuesArray["venue.unitDescriptor"]) == 0){
                $responseWrapper->addValidationError("venue.unitDescriptor", "Please specify a short description of the action item.", $eventIndex);
            }
        } else {
            $percentage = $postedValuesArray["venue.customerChoicePercentage"];
            $minValue = $postedValuesArray["venue.featuredCausePercentage"] ? 0 : 1;
            if (!is_numeric($percentage) || $percentage < $minValue || $percentage > 100){
                $responseWrapper->addValidationError("venue.customerChoicePercentage", "This must be an integer between ".$minValue." and 100.", $eventIndex);
            }
        }
        // title length check
        if (strlen($postedEvent->title) > 75){
            $responseWrapper->addValidationError("title", "This field must not exceed 75 characters.", $eventIndex);
        }
        // date congruence check
        if (!$responseWrapper->hasValidationError("startDateString", $eventIndex) && !$responseWrapper->hasValidationError("end", $eventIndex)){
            $startDate = strtotime($postedEvent->start);
            $endDate = strtotime($postedEvent->end);
            if ($endDate < $startDate){
                $responseWrapper->addValidationError("endDateString", "End Date and Time must be later than Start Date and Time", $eventIndex);
            }
        }
    }
}

class AdminEventModelConverter extends StandardEventModelConverter{

    private $adminRoot;
    /* @var mysqli */
    private $mysqli;
    public function __construct(mysqli $mysqli){
        global $CurrentServer, $adminFolder;
        $this->adminRoot = $CurrentServer.$adminFolder;
        $this->mysqli = $mysqli;
    }

    public function getDraftEventUrl($draftEventId){
        return $this->adminRoot."events/?nav=edit-draft-event&id=".$draftEventId;
    }

    public function getDraftEventSuccessUrl($draftEventId){
        return $this->getDraftEventUrl($draftEventId);
    }

    public function getEventUrl($eventId){
        return $this->adminRoot."events/?nav=edit-event&id=".$eventId;
    }

    public function getEventCreateSuccessUrl($eventId){
        return $this->getEventUrl($eventId);
    }

    public function getEventEditSuccessUrl($eventId){
        return $this->getEventUrl($eventId);
    }

    protected function getEventCause($causeId){
        $causeProvider = new OrganizationMemberProvider($this->mysqli);
        return $causeProvider->getPublicProfile($causeId);
    }
}