<?php
class EventListModel{
    public $DT_RowId;
    public $id;
    public $name;
    public $dates;
    public $merchant;
    public $cause;
    public $campaigns;
    public $isOnline;
    public $isNational;
    public $isCtEvent;

    public function __construct(EventRecord $record){
        $this->DT_RowId = "event-".$record->id;
        $this->id = $record->id;
        $this->name = $record->title;
        $this->dates = $record->getCombinedDateTimeString(true);
        $this->merchant = (object)array(
            "id" => $record->venue->id,
            "name" => $record->venue->name,
            "addressLines" => $record->venue->getMultiLineAddress()
        );
        $this->cause = (object)array(
            "id" => $record->cause->id,
            "name" => $record->cause->name
        );
        $this->campaigns = $record->commaDelimitedTagNames;
        $this->isOnline = $record->isOnlineEvent ? 1 : 0;
        $this->isNational = $record->isNationalEvent ? 1 : 0;
        $this->isCtEvent = $record->isCausetownEvent ? 1 : 0;
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "name",
        2 => "dates",
        3 => "merchant",
        4 => "cause",
        5 => "campaigns",
        6 => "isOnline",
        7 => "isNational",
        8 => "isCtEvent"
    );

    public static function getCriteria($datatablePost){
        $criteria = new EventCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 0){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}