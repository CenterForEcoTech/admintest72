<?php
include_once("../_config.php");
$pageTitle = "Admin - Event Management";
?>
<html>
<?php include("../_header.php");
?>
    <h1>Events General Administration</h1>
    <?php

    $nav = isset($_GET["nav"]) ? $_GET["nav"] : "event-list";
    include_once("_eventMenu.php");
    if ($nav){
        switch ($nav){
            case "edit-event":
                include("model/_editEventModel.php");
                $model = new EditEventModel($_GET['id'], $_GET['template']);
                include("views/_editEvent.php");
                break;
            case "add-event":
            case "edit-draft-event":
                include("model/_editEventModel.php");
                $_GET['isDraft'] = true;
                $model = new EditEventModel($_GET['id'], $_GET['template'], $_GET['isDraft']);
                include("views/_editEvent.php");
                break;
            case "event-draft-list":
                $_GET['isDraft'] = true;
                include("views/_eventsList.php");
                break;
            case "event-list":
            default:
                include("views/_eventsList.php");
                break;
        }
    }
?>
<?php
include("../_footer.php");
?>