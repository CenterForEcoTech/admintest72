<div id="nav-menu">
    <ul>
        <li><a id="indexing-event-migration" href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-migration" class="button-link">Event Migration</a></li>
        <li><a id="indexing-event-latlong" href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-latlong" class="button-link">Recreate Lat/Long for missing geocodes</a></li>
        <li><a id="indexing-event-delete" href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-delete" class="button-link">Delete Event</a></li>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>