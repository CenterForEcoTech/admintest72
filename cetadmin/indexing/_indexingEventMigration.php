<ul>
    <li><a href="?nav=indexing-event-migration&addScoringFunctions=1">Update Scoring Functions</a></li>
	<li id="SpecificEventID" >ReIndex Specific EventID <input type="text" name="event" value="<?php echo $_GET['SpecificEventID'];?>" style="width:40px;"><button class="buttonGo">Go</button>
    <li id="viewSpecificEventID">View Specific EventID <input type="text" name="event" value="<?php echo $_GET['viewSpecificEventID'];?>" style="width:40px;"><button class="buttonGo">Go</button>
    <li><a href="?nav=indexing-event-migration&Records=Upcoming">ReIndex Only Upcoming Events</a>
	<li><a href="?nav=indexing-event-migration&Records=All">ReIndex All Events</a> (including events that have passed)
</ul>
<script>
 $(function(){
	$(".buttonGo").on('click',function(){
        var element = $(this),
            container = element.closest("li"),
            actionId = container.attr("id"),
            actionValue = container.find("input[name='event']").val();
		if (actionValue){
			location.href = '?nav=indexing-event-migration&Records='+actionId+'&'+actionId+'='+actionValue;
		}else{
			alert('you must include an EventID');
		}
	});
});
</script>
<?php
if ($_GET['addScoringFunctions']){
    include_once('..\Global_Variables.php');
    //error_reporting(1);
    include_once($repositoryApiFolder."indexedSearchProvider/IndexedEventManager.php");
    $IndexingProviderURL = $Config_IndexingURLPrivate;
    $IndexName = $Config_IndexingEventsIndex;
    $manager = new IndexedEventManager($IndexingProviderURL, $IndexName);
    $manager->addScoringFunctions();
} else if ($_GET['Records']){
	$maxEventsToIndex = 100;
	$LimitStart = 0;
	if ($_GET['RecordCount']){
		$LimitStart = ($_GET['RecordCount']-$maxEventsToIndex);
		$LimitEnd = ($_GET['RecordCount']);
		$Limit["Start"] = $LimitStart;
		$Limit["End"] = $maxEventsToIndex;
	}

	include_once('..\Global_Variables.php');
	set_time_limit (600);
	//error_reporting(1);
	include_once($repositoryApiFolder."DataContracts.php");
	require_once($repositoryApiFolder."config.php");
	include_once($dbProviderFolder."EventProvider.php");

	$eventProvider = new EventProvider($mysqli);

	if ($_GET['Records'] == "Upcoming"){
		$UpcomingEvents = $eventProvider->getAll($Limit);
		$eventCount = count($UpcomingEvents);
		if (!$LimitEnd){$LimitEnd = $eventCount;}
	}elseif ($_GET['Records'] == "SpecificEventID"){
		$UpcomingEvents = $eventProvider->getAllByEvent($_GET['SpecificEventID']);
		$eventCount = count($UpcomingEvents);
		if (!$LimitEnd){$LimitEnd = $eventCount;}
    }elseif ($_GET['Records'] == "viewSpecificEventID"){
        include_once($siteRoot."_setupEventByLocationProvider.php");
        $criteria = new EventByLocationCriteria();
        $criteria->eventId = $_GET['viewSpecificEventID'];
        $result = $indexedEventProvider->get($criteria, "*", null, true);
        print_pre($result);
	}else{
		$UpcomingEvents = $eventProvider->getAllHistoricCount();
		$eventCount = $UpcomingEvents[0]["Events"]+$UpcomingEvents[1]["Events"];
		echo "There are ".$eventCount." events to migrate to be indexed.<br>";
		if ($_GET['RecordCount']){
			$UpcomingEvents = $eventProvider->getAllHistoric($Limit);
		}
		
	}

	if (($eventCount+1) > $maxEventsToIndex){
		echo "<form method=\"get\" id=\"RecordCountForm\"><input type=\"hidden\" name=\"nav\" value=\"".$_GET['nav']."\"><input type=\"hidden\" name=\"Records\" value=\"".$_GET['Records']."\">";
		echo "<select name=\"RecordCount\" onChange=\"document.getElementById('RecordCountForm').submit();\"><option value=\"\">Choose which records to migrate</option>";
		$repeat = floor($eventCount/$maxEventsToIndex);
		for ($i=1;$i <= $repeat;$i++){
			$checked = "";
			if ((int)$_GET['RecordCount'] == (int)($maxEventsToIndex*$i)){$checked = " selected";}
			echo "<option value=\"".($maxEventsToIndex*$i)."\"".$checked.">".(($maxEventsToIndex*$i)-$maxEventsToIndex)."-".($maxEventsToIndex*$i)."</option>";
		}
			$checked = "";
			if ((int)$_GET['RecordCount'] == (int)$eventCount){$checked = " selected";}
			echo "<option value=\"".$eventCount."\"".$checked.">".(($maxEventsToIndex*$i)-$maxEventsToIndex)."-".$eventCount."</option>";
		
		echo "</select>";
		echo "</form>";
	}
		//require_once($repositoryApiFolder.'Indexing_API.php');
		$IndexingProvider = new Indexing_Api($IndexingProviderURL);

		//Calling the Indexing_Index calls requires the IndexingProvider and an IndexName
		$IndexingThisIndex = new Indexing_Index($IndexingProvider,$IndexName);
	
	if ($proceedToIndex || $LimitEnd){
		$x = 0;
		foreach ($UpcomingEvents as $uEvents=>$upcomingEvent){
            /* @var $upcomingEvent EventRecord */
			$documents[$x] = $upcomingEvent->convertToIndexedDocument();
			$x++;
		}
		//print_pre($documents);
		//var_dump($documents);

		$IndexAddDocuments = $IndexingThisIndex->add_documents($documents);
		$DocAddedCount = 0;
		foreach ($IndexAddDocuments as $DocAdded){
			
			if ($DocAdded->added){
				$DocAddedCount++;
			} else {
				print_r($DocAdded);
			}
		}
		echo $DocAddedCount." events [".$LimitStart."-".$LimitEnd."] uploaded to be indexed.";
	}//end if ProceedToIndex
	
	//Check current index size
	$IndexSize = $IndexingThisIndex->get_size();
	if ($IndexSize){
		echo "<br>There are currently ".$IndexSize." events ready to be searched in Index '".$IndexName."'";
	}else{
		echo "<br>Currently unable to connect to the Indexing Server";
	}
}//end if Records
?>
