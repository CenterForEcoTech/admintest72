<?php
include_once("../_config.php");
$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        $results->message = "GET is not supported at this time";
        break;
    case "POST":
        $postedData = json_decode($currentRequest->rawData);
        if ($postedData->doc_id){
            include_once($repositoryApiFolder."indexedSearchProvider/IndexedEventManager.php");
            $manager = new IndexedEventManager($Config_IndexingURLPrivate, $Config_IndexingEventsIndex);
            $httpStatusCode = $manager->delete($postedData->doc_id);
            $results->success = ($httpStatusCode >= 200 && $httpStatusCode <= 299);
            if ($results->success){
                $results->message = $httpStatusCode.": Delete appears to have been successful!";
            } else {
                $results->message = $httpStatusCode.": Delete failed for an unknown reason.";
            }
        } else {
            $results->message = "You must specify a doc_id";
        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
output_json($results);
die();