<h3>Delete event from index</h3>

<p>Provide the doc_id of the record you wish to remove, then click "Delete" to remove it from the index. If you provide a doc_id by accident, you can always use the Event Migration option to re-index the event.</p>

<div>
    <label>
        doc_id:
        <input type="text" id="doc_id" title="e.g., 123_45 (event_id + merchant_id)" class="auto-watermarked">
    </label>
    <a href="" id="delete-event-button" class="button-link">Delete</a>
</div>
    
<div id="delete-event-message" class=""></div>

<script type="text/javascript">
    (function(){
        var docIdField = $("#doc_id"),
            deleteButton = $("#delete-event-button"),
            alertDiv = $("#delete-event-message"),
            url = "ApiIndexing.php",
            clearMessage = function(){
                alertDiv.html("").removeClass("error").removeClass("info").removeClass("warning");
            },
            setError = function(message){
                alertDiv.html(message).addClass("error");
            },
            setInfo = function(message){
                alertDiv.html(message).addClass("info");
            },
            setWarning = function(message){
                alertDiv.html(message).addClass("warning");
            },
            resetForm = function(){
                docIdField.val("");
            },
            isValid = function(){
                if (docIdField.val()){
                    return true;
                }
            },
            submit = function(){
                var data = {
                    "doc_id": docIdField.val()
                };
                clearMessage();
                if (isValid()){
                    $.ajax({
                        type: "POST",
                        data: JSON.stringify(data),
                        url: url,
                        success: function(data){
                            if (data.success){
                                resetForm();
                                setInfo("Success! " + data.message);
                            } else {
                                setWarning(data.message);
                            }
                        }
                    });
                } else {
                    setError("You must specify a doc_id.");
                }
            };

        deleteButton.click(function(e){
            e.preventDefault();
            submit();

        });
    })();
</script>