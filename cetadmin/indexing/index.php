<?php
include_once("../_config.php");
$pageTitle = "Admin - Indexing";
?>
<html>
<?php include("../_header.php");
?>
<div class="container">
<h1>Indexing Administration</h1>
<?php

$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
include_once("_indexingMenu.php");
if ($nav){
    switch ($nav){
        case "indexing-event-migration":
			include("_indexingEventMigration.php");
            break;
		case "indexing-event-latlong":
			include("_indexingGeoCodeCreate.php");
			break;
        case "indexing-event-delete":
            include("_deleteEvent.php");
            break;
        default:
            break;
    }
}
?>
</div> <!-- end container -->
<?php
include("../_footer.php");
?>