<?php
/**
 * This template always outputs json by calling the output_json function, which is expected to kill the process immediately.
 */

include_once("_config.php");
include_once("../assert_is_ajax.php");
include_once("../repository/mySqlProvider/Helper.php");
include_once("../functions.php");
//
// Verify that db connection configuration is set up
//
if (!isset($dbConfig)){
    echo "This script requires the database configuration file in siteconf.";
    die(); // no db config for source db.
}
if (!isset($dbConfigDisbursement)){
    echo "This script requires the database configuration for the disbursement database in siteconf.";
    die(); // no db config for the target db.
}

include_once($siteRoot."_setupDisbursementConnection.php");


if (!isset($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = 25;
}
if (!is_numeric($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = 25;
}
// get disbursements that are ready to be batched and sent to FirstGiving
//
// RULES:
//  MINIMUM $25
//  ORG is not a fiscally sponsored charity
$listSql = "
    SELECT
        d.id,
        d.amount,
        org.ein,
        org.name,
        org.is_fiscally_sponsored
    FROM disbursements d
    JOIN disbursement_orgs org on d.disbursement_org_id = org.id
    WHERE d.amount >= ".$Config_MinDisbursementAmt."
    AND (d.batch_id IS NULL OR d.batch_id = 0)
    -- AND org.is_fiscally_sponsored = 0
    -- AND org.ein is not null
    ORDER BY org.name";

$merchantResults = MySqliHelper::get_result($mysqli_disb, $listSql);

output_json($merchantResults);
die();
?>
