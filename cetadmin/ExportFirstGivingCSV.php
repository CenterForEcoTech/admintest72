<?php
/**
 * Based on the batch id (of a previously exported batch) or a set of disbursement ids (for specific nonprofits)
 * This generates a csv export of the resulting data in a format compatible with FirstGiving's batch api.
 *
 * See http://developers.firstgiving.com/documentation/disbursement-only/
 */

unset($disbursement_data); // we don't allow an external template to set this and override our checks

$exportPath = $_POST["filepath"];
$exportAll = isset($_POST["export_all"]);
$disbursementIds = $_POST["disbursementIds"];
$processName = "FGBatchExport";
$processIdentifier = $_REQUEST["PHPSESSID"]."__".time();
if (!isset($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = "null";
}
if (!is_numeric($Config_MinDisbursementAmt)){
    $Config_MinDisbursementAmt = "null"; // "null" is the only valid non numeric value
}

include_once("_config.php");
include_once("../repository/mySqlProvider/Helper.php");
include_once("../functions.php");
//
// Verify that db connection configuration is set up
//
if (!isset($dbConfig)){
    echo "This script requires the database configuration file in siteconf.";
    die(); // no db config for source db.
}
if (!isset($dbConfigDisbursement)){
    echo "This script requires the database configuration for the disbursement database in siteconf.";
    die(); // no db config for the target db.
}

include_once($siteRoot."_setupDisbursementConnection.php");

if (isset($exportPath)){
    $shortname = basename($exportPath);
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".$shortname."\";" );
    header("Content-Transfer-Encoding: binary");

    $fileContents = file_get_contents($exportPath);
    echo $fileContents;
    die();
}
if ($exportAll){
    unset($disbursementBatchId); // start fresh
    $sp_query = "CALL sp_mark_disbursed('".$processName."','".$processIdentifier."','all', ".$Config_MinDisbursementAmt.",@outBatchId)";
} else if (isset($disbursementIds) && count($disbursementIds) > 0){
    unset($disbursementBatchId); // start fresh
    $isValid = true;
    $paramIds = "";
    foreach ($disbursementIds as $disbursementId){
        if (is_numeric($disbursementId)){
            if (strlen($paramIds)){
                $paramIds .= ",";
            }
            $paramIds .= $disbursementId;
        }
    }
    $sp_query = "CALL sp_mark_disbursed('".$processName."','".$processIdentifier."','".$paramIds."',".$Config_MinDisbursementAmt.",@outBatchId)";
}

if ($sp_query){
    $result = $mysqli_disb->query($sp_query);
    $batchIdSql = "
        SELECT id
        FROM disbursement_batches
        WHERE processor_name = ?
        AND process_identifier = ?";
    $batchSqlResult = MySqliHelper::get_result($mysqli_disb, $batchIdSql, "ss", array($processName, $processIdentifier));
    if (count($batchSqlResult)){
        $disbursementBatchId = $batchSqlResult[0]["id"];
    }
}
if (isset($disbursementBatchId) && is_numeric($disbursementBatchId)){
    $defaultSettings = array(
        $Config_FGDonorEmailAddress => "mayor@causetown.org",
        $Config_FGDonorFirstName => $SiteName,
        $Config_FGDonorLastName => "",
        $Config_FGDonorTitle => "",
        $Config_FGDonorAddress1 => "107 S West St #548",
        $Config_FGDonorAddress2 => "",
        $Config_FGDonorCity => "Alexandria",
        $Config_FGDonorState => "VA",
        $Config_FGDonorCountry => "US",
        $Config_FGDonorPostcode => "22314",
        $Config_FirstGiving_ApiKey => ""
    );
    foreach($defaultSettings as $key => $value){
        if (!isset(${$key})){
            ${$key} = $value;
        }
    }

    $selectSql = "
            SELECT
              d.id as 'TransactionId',
              org.ein as 'CharityEin',
              d.amount as 'DonationAmount',
              '".$Config_FGDonorEmailAddress."' as 'DonorEmailAddress',
              '".$Config_FGDonorFirstName."' as 'DonorFirstName',
              '".$Config_FGDonorLastName."' as 'DonorLastName',
              '".$Config_FGDonorTitle."' as 'DonorTitle',
              '".$Config_FGDonorAddress1."' as 'DonorAddressLine1',
              '".$Config_FGDonorAddress2."' as 'DonorAddressLine2',
              '".$Config_FGDonorCity."' as 'DonorCity',
              '".$Config_FGDonorState."' as 'DonorState',
              '".$Config_FGDonorCountry."' as 'DonorCountry',
              '".$Config_FGDonorPostcode."' as 'DonorPostcode',
              '' as 'CreditCardType',
              '' as 'CreditCardLast4Digits',
              '".$Config_FirstGiving_APIKEY."' as 'ApiKey',
              org.organization_info_id as 'CausetownCharityIdentifier',
              org.is_fiscally_sponsored as 'FiscallySponsored'
            FROM disbursements d
            JOIN disbursement_orgs org ON d.disbursement_org_id = org.id
            WHERE batch_id = ?";

    $colHeaders = array(
        'TransactionId',
        'CharityEin',
        'DonationAmount',
        'DonorEmailAddress',
        'DonorFirstName',
        'DonorLastName',
        'DonorTitle',
        'DonorAddressLine1',
        'DonorAddressLine2',
        'DonorCity',
        'DonorState',
        'DonorCountry',
        'DonorPostcode',
        'CreditCardType',
        'CreditCardLast4Digits',
        'ApiKey',
        'CausetownCharityIdentifier',
        'FiscallySponsored'
    );

    $disbursement_data = MySqliHelper::get_result($mysqli_disb, $selectSql, "i", array($disbursementBatchId));
}

if (isset($disbursement_data) && count($disbursement_data)){
    $currentDate = date("Y-m-d");
    $filebase = $processName."_".$currentDate."_".$disbursementBatchId;
    $extension = ".csv";
    $filename = $filebase.$extension;
    $fileExists = true;
    $attempts = 0;
    while ($fileExists){
        $savePath = "disbursements/".$filename;
        if (file_exists($savePath)){
            $attempts++;
            $filename = $filebase." (".$attempts.")".$extension;
        } else {
            $fileExists = false;
        }
    }
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".$filename."\";" );
    header("Content-Transfer-Encoding: binary");

    $fileContents = exportCSV($disbursement_data, $colHeaders, true);
    file_put_contents($savePath, $fileContents);
    echo $fileContents;
} else {
?>
<h2>Oops!</h2>
<p>
    You are here due to a disbursement spreadsheet download that managed to find nothing to export!
    This might happen if you and some other person simultaneously launched a download of the same dataset,
    and if you are here, chances are the other person got the download.
</p>
<p>The other possibility is that you did not select any disbursements to export.</p>
<p>Use the browser's back button, refresh the page, and try again.</p>
<?php
}
?>