<?php
$path = getcwd();
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";}else{$path = "../";}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($path."_config.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."InventoryProvider.php");
$productProvider = new ProductProvider($dataConn);

$criteria = new stdClass();
$criteria->didNotSend = true;

$productReceiptDetails = $productProvider->getUnAcknowledgedMovementRecordEmail($criteria);
if (count($productReceiptDetails)){
	//if wanted to send notification from dreamhost server
	/*
	$path = getcwd();
	$pathSeparator = "/";
	if (!strpos($path,"xampp")){
		$thisPath = ".:/home/cetdash/pear/share/pear";
		set_include_path($thisPath);
	}else{
		$pathSeparator = "\\";
	}
	*/
	
	require_once "Mail.php";  
	foreach($productReceiptDetails as $receiptDetail){
		$sendMail = false;
		$emailRecordId = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_ID"];
		$to = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_To"];
		$from = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_From"];
		$fromAuth = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_FromAuth"];
		$fromAuthParts = explode('QQQ',$fromAuth);
		if (count($fromAuthParts)){
			$sendMail = true;
			$username = $fromAuthParts[0];
			$password = $fromAuthParts[1];
		}
		$body = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_Text"];
		$body = $body."\n\r\n\rTo indicate shipment received: ".$CurrentServer."productReceipt/".$emailRecordId;					
		$body = $body."\n\r\n\rTo note any discrepancies: ".$CurrentServer."productDiscrepancy/".$emailRecordId. "\n\r";					
		$headers = $receiptDetail["GHSInventoryProductsMovementHistoryRecord_Headers"];
		if ($sendMail){
			echo "Attempt to Send Mail to ".$to." ID=".$emailRecordId;
			$recipients = $to.",IT Admin <it@cetonline.org>";
			$subject = "Inventory added on ".$TodaysDate; 
			$host = "smtp.office365.com"; 
			$port = "587"; 
			$headers = array (
				'From' => $from,   
				'To' => $to, 
				'Reply-to' => $from,
				'Subject' => $subject
			); 
			$smtp = Mail::factory(
				'smtp',   
				array (
					'host' => $host,     
					'port' => $port,     
					'auth' => true,     
					'username' => $username,     
					'password' => $password
				)
			);  
			$mail = $smtp->send($recipients, $headers, $body); 
			$updateEmailRecordId = $productProvider->movementRecordEmailUpdate($emailRecordId,$mail);
			print_pre($mail);
			echo "\r\n ";
		}
	}
}else{
	echo "All Product Transfers have been emailed.";
}
?>