<?php
set_time_limit (600);
$path = getcwd();
$pathSeparator = "/";
if (strpos($path,"xampp")){
	$pathSeparator = "\\";
	$testingServer = true;
}
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";}else{$path = "..";}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($path.$pathSeparator."_config.php");

$checkType = $_GET['checkType'];
$outputText = ($checkType == "attachments" ? false : true);
//echo $checkType."<Br>";
//echo "outputText=".$outputText;
$destinationFolder = $path.$pathSeparator."muni".$pathSeparator."rebateAttachments".$pathSeparator."AWS".$pathSeparator;
$sinceFile = $destinationFolder."since.txt";
$sinceDateStamp = file_get_contents($sinceFile); //TODO use a query to find out when last checked
if ($outputText){echo "since: ".date("m/d/Y H:i:s", strtotime($sinceDateStamp))."<br>\r\n";}
$response["since"] = date("m/d/Y H:i:s", strtotime($sinceDateStamp));
include_once($dbProviderFolder."MuniProvider.php");
include_once($siteRoot."_setupDataConnection.php");
$muniProvider = new MuniProvider($dataConn);

//adjustfieldvalues to conform to current system
//radioField1 is the EnergyStar Appliance
$FieldAdjustments["5899ebddda01a9006cfad05e"]["radioField1"] = array("clothesWasher25"=>"Clothes Washer","freezer25"=>"Freezer","roomAC25"=>"Room A/C","heatPumpWaterHeater300"=>"Heat Pump Water Heater","refrigerator25"=>"Refrigerator","dishwasher25"=>"Dishwasher","dehumidifier25"=>"Dehumidifier","airPurifier25"=>"Air Purifier");

//for Efficiency eligibleMeasures
$FieldAdjustments["589a1108da01a9006cfad161"]["centralACorHeatPumpInformationtobecompletedbycontractor"] = array("blowerDoorTestAirSealing50UpTo300"=>"Blower Door Test & Air Sealing / 50% up to $300","insulation50UpTo300"=>"Insulation / 50% up to $300","heatingSystem50UpTo300"=>"Heating System / 50% up to $300");
//for Cool Home 
$FieldAdjustments["5899dad6110e2100735d76ad"]["centralACorHeatPumpInformationtobecompletedbycontractor"] = array("newConstruction"=>"New Construction","replacementSystems"=>"Replacement System","addingExistingDuctwork"=>"Adding to existing ductwork","newOrAdditionalDuctworkOrAirConditioning"=>"New or additional ductwork and air conditioning");
$FieldAdjustments["5899dad6110e2100735d76ad"]["measureTypeandDescription"] = array(
	"189250"=>array("name"=>"Ductless18","rebateAmount"=>250),
	"2011500"=>array("name"=>"Ductless20","rebateAmount"=>500),
	"161385250"=>array("name"=>"CentralAC16","rebateAmount"=>250),
	"181396500"=>array("name"=>"CentralAC18","rebateAmount"=>500)
	);
	
//for Residential Heat HotWater	
$FieldAdjustments["5899e658110e2100735d76e9"]["measureTypeDescriptionandRebate"] = array(
	"95150300"=>array("name"=>"HEF_300","rebateAmount"=>300),
	"97150600"=>array("name"=>"HEF_600","rebateAmount"=>600),
	"85300500"=>array("name"=>"HydronicBoiler_500","rebateAmount"=>500),
	"903001000"=>array("name"=>"HydronicBoiler_1000","rebateAmount"=>1000),
	"953001500"=>array("name"=>"HydronicBoiler_1500","rebateAmount"=>1500),
	"901200"=>array("name"=>"Integrated_Water_Heater_1200","rebateAmount"=>1200),
	"951600"=>array("name"=>"Integrated_Water_Heater_1600","rebateAmount"=>1600),
	"400"=>array("name"=>"IndirectWater_400","rebateAmount"=>400),
	"06250"=>array("name"=>"WaterStorage_50","rebateAmount"=>50),
	"067100"=>array("name"=>"WaterStorage_100","rebateAmount"=>100),
	"82500"=>array("name"=>"TanklessWater_500","rebateAmount"=>500),
	"94800"=>array("name"=>"TanklessWater_800","rebateAmount"=>800)
	);
	
	
//for Residential Heat HotWater	
$FieldAdjustments["589a11cf110e2100735d77ee"]["measureTypeDescriptionandRebate"] = array(
	"1"=>array("name"=>"HEF_300","rebateAmount"=>300),
	"2"=>array("name"=>"HEF_600","rebateAmount"=>600),
	"3"=>array("name"=>"Boiler_500","rebateAmount"=>500),
	"4"=>array("name"=>"Boiler_1000","rebateAmount"=>1000),
	"5"=>array("name"=>"Boiler_1500","rebateAmount"=>1500),
	"6"=>array("name"=>"Integrated_Water_Heater_1200","rebateAmount"=>1200),
	"7"=>array("name"=>"Integrated_Water_Heater_1600","rebateAmount"=>1600),
	"8"=>array("name"=>"Indirect_Hot_Water_400","rebateAmount"=>400),
	"9"=>array("name"=>"Water_Storage_50","rebateAmount"=>50),
	"10"=>array("name"=>"Water_Storage_100","rebateAmount"=>100),
	"11"=>array("name"=>"Tankless_Water_500","rebateAmount"=>500),
	"12"=>array("name"=>"Tankless_Water_800","rebateAmount"=>800)
	);
/*
High Efficiency Furnace/≥95% AFUE, ECM motor ≤ 150 MBH/$300 Rebate
High Efficiency Furnace/≥97% AFUE, ECM motor ≤ 150 MBH/$600 Rebate
Hydronic Boiler/≥85% AFUE, ≤ 300 MBH/$500
Hydronic Boiler/≥90% AFUE, ≤ 300 MBH/$1,000
Hydronic Boiler/≥95% AFUE, ≤ 300 MBH/$1,500
Integrated water heater and Condensing Boiler/≥90% AFUE/$1,200
Integrated water heater and Condensing Boiler/≥95% AFUE/$1,600
Indirect Hot Water Heating/Attached to Natural Gas Boiler/$400
Water Heating Storage/0.62 EF/$50
Water Heating Storage/Energy Star Labeled (0.67 EF)/$100
Tankless Water Heating/≥ .82 EF/$500
Tankless Water Heating/≥ .94 EF/$800
*/	
	
include_once($path.$pathSeparator.'muni'.$pathSeparator.'models'.$pathSeparator.'sprypointFieldsAWS.php');
		$url = "https://j995yguu5f.execute-api.us-east-1.amazonaws.com/prod/submissions?token=oVuEHme8RuwuzJcxR399EePw9gKZMugz&since=".$sinceDateStamp;
		//echo $url."<hr>";
//		$curlHeaders = array('X-Auth-Token: 214efdac-733b-11e5-8bcf-feff819cdc9f');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
//		curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
		if(!curl_exec($ch)){
			if ($outputText){echo curl_error($ch) . '" - Code: ' . curl_errno($ch);}
			if ($outputText){echo "\r\nUsing hard coded sample set\r\n";}
			$contents = '[{"_id":{"$oid":"561fec3893783ecd8780bc1b"},"origin":"10.113.200.123","fields":{"Store_State":["TEST - DNP"],"Address":["TEST - DNP"],"Store_City":["TEST - DNP"],"Rebate_Zip":[""],"Residential_Electric_Utility_Account_Number":["TEST - DNP"],"Store_Address":["TEST - DNP"],"Serial_Number":["TEST - DNP"],"Rebate_City":[""],"Purchase_Date":["10/15/2015"],"Purchase_Price":["TEST - DNP"],"employee_initials":[""],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"Work_Phone":[""],"City":["TEST - DNP"],"Your_Name":["TEST - DNP"],"Home_Phone":["TEST - DNP"],"Store_Name":["TEST - DNP"],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Energy_Star_Appliance":[""],"Model_Number":["TEST - DNP"],"Rebate_Address":["TEST - DNP"],"State":["TEST - DNP"],"Brand":["TEST - DNP"],"Store_Zip":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Home_Appliance_Rebate_Form","isRead":false,"created":"2015-10-15T18:11.04.312Z"},{"_id":{"$oid":"561fec1493783ecd8780bc19"},"origin":"10.113.200.123","fields":{"New_Pump_Manufacturer":["TEST - DNP"],"Replaced_Pump_Horsepower":[""],"Replaced_Pump_Manufacturer":[""],"Address":["TEST - DNP"],"Residential_Municipal_Electric_Utility_Account_Number":["TEST - DNP"],"New_Pump_Model_Number":["TEST - DNP"],"New_Pump_Horsepower":["TEST - DNP"],"Rebate_Zip":[""],"Contractor_Zip":["TEST - DNP"],"Rebate_City":[""],"Controller_Manufacturer":["TEST - DNP"],"Purchase_Date":["10/15/2015"],"Type_of_New_Pool_Pump":["Two-speed"],"Purchase_Price":["99"],"Contractor_Company_Name":["TEST - DNP"],"Replaced_Pump_Model_Number":[""],"employee_initials":[""],"Installation_Date":["10/15/2015"],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"City":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Your_Name":["TEST - DNP"],"Home_Phone":["TEST - DNP"],"Type_of_Replaced_Pool_Pump":[""],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Model_Number":["TEST - DNP"],"Rebate_Address":[""],"Contact_Person":["TEST - DNP"],"State":["TEST - DNP"],"Contractor_State":["TEST - DNP"],"Contractor_Phone":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_High_Efficiency_Pool_Pump_Rebate_Application","isRead":false,"created":"2015-10-15T18:10.28.463Z"},{"_id":{"$oid":"561febe393783ecd8780bc17"},"origin":"10.113.200.123","fields":{"Install_Cost":["99"],"Business_Phone":["TEST - DNP"],"Contractor_Email_Address":["kstrang@sprypoint.com"],"Company_Name":["TEST - DNP"],"Address":["TEST - DNP"],"TXV_or_EXV_Installed":["TXV"],"AHRI_Rated_EER":[""],"Customer_Name":["TEST - DNP"],"Rebate_Zip":[""],"Electric_Account_Number":["TEST - DNP"],"Contractor_Zip":["TEST - DNP"],"Mini_Split":["Yes"],"Rebate_City":[""],"Install_Date":["10/15/2015"],"Rebate_Name":[""],"Is_technician_NATE_Certified":["Yes"],"Manufacturer":["99"],"employee_initials":[""],"New_Unit_Size":[""],"Customer_Initials":["TEST - DNP"],"Rebate_State":[""],"AHRI_Rated_SEER":[""],"City":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Rebate_Amount":["99"],"New_Equipment_Installed":[""],"Home_Phone":["TEST - DNP"],"Zip":["TEST - DNP"],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Coil_Model_Number":[""],"Rebate_Address":[""],"Contact_Person":["TEST - DNP"],"State":["TEST - DNP"],"HSPF":[""],"Telephone_Number":["TEST - DNP"],"Contractor_State":["TEST - DNP"],"AHRI_Ref_Number":["99"],"Condenser_Model_Number":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Cool_Homes_Rebate_Application","isRead":false,"created":"2015-10-15T18:09.39.069Z"},{"_id":{"$oid":"561feba593783ecd8780bc15"},"origin":"10.71.199.148","fields":{"Anticipated_Rebate_tankless_water_heater":[""],"Input_Size_furnace":[""],"Anticipated_Rebate_boiler":[""],"Anticipated_Rebate_water_heater":[""],"Rebate_Amount_furnace":[""],"Landlord_Owner_Name":[""],"Fax_Number":[""],"Efficiency_Rating_boiler":[""],"License_Number":["TEST - DNP"],"Landlord_Owner_Phone_Number":[""],"Installed_Cost":["TEST - DNP"],"Efficiency_Rating_furnace":[""],"Quantity_Installed_water_heater":[""],"Quantity_Installed_boiler":[""],"Property_Owned_or_Leased":["Owned"],"Anticipated_Rebate_storage_water_heater":[""],"Total_Anticipated_Rebate":["TEST - DNP"],"Rebate_Amount_boiler":[""],"Quantity_Installed_furnace":[""],"Model_Number_water_heater":[""],"Contractor_Zip":["TEST - DNP"],"Landlord_Owner_Address":[""],"Anticipated_Rebate_furnace":[""],"Model_Number_furnace":[""],"Model_Number_storage_water_heater":[""],"Installed_Zip":["TEST - DNP"],"Landlord_Owner_Zip":[""],"Manufacturer_boiler":[""],"Landlord_Owner_City":[""],"Quantity_Installed_storage_water_heater":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Installation_Date":["10/15/2015"],"Quantity_Installed_tankless_water_heater":[""],"Installed_City":["TEST - DNP"],"Gas_Account_Number":["TEST - DNP"],"Account_Holder_Name":["TEST - DNP"],"Contractor_Eml":["kstrang@sprypoint.com"],"Contractor_Address":["TEST - DNP"],"Model_Number_tankless_water_heater":[""],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Tax_ID_Number":[""],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Customer_initials":["TEST - DNP"],"Manufacturer_storage_water_heater":[""],"Input_Size_storage_water_heater":[""],"Rebate_Amount_water_heater":[""],"Installed_Address":["TEST - DNP"],"Input_Size_water_heater":[""],"Manufacturer_water_heater":[""],"Phone_Number":["TEST - DNP"],"Rebate_Amount_tankless_water_heater":[""],"Manufacturer_furnace":[""],"Rebate_Amount_storage_water_heater":[""],"Manufacturer_tankless_water_heater":[""],"Efficiency_Rating_water_heater":[""],"Input_Size_tankless_water_heater":[""],"Model_Number_boiler":[""],"Contractor_State":["TEST - DNP"],"Landlord_Owner_State":[""],"Landlord_Owner_Eml":[""],"Efficiency_Rating_storage_water_heater":[""],"Landlord_Owner_Fax_Number":[""],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Input_Size_boiler":[""],"Efficiency_Rating_tankless_water_heater":[""],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Residential_High_Efficiency_Heating_and_Water_Heating_Rebate_Program","isRead":false,"created":"2015-10-15T18:08.37.938Z"},{"_id":{"$oid":"561feb6e93783ecd8780bc13"},"origin":"10.71.199.148","fields":{"Owner_Landlord_Fax_Number":[""],"Fax_Number":["TEST - DNP"],"License_Number":["TEST - DNP"],"Mailing_Zip":[""],"Contractor_Zip":["TEST - DNP"],"Provide_Description_of_Measure":["TEST - DNP"],"Mailing_State":[""],"Mailing_Address":[""],"Name_of_Municipal_Utility":["TEST - DNP"],"Owner_Landlord_Eml":[""],"Installed_Zip":["TEST - DNP"],"Owner_Landlord_State":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Customer_Initials":["TEST - DNP"],"Installed_City":["TEST - DNP"],"Mailing_City":[""],"Account_Holder_First_Name":["TEST - DNP"],"Contractor_Eml":["kstrang@sprypoint.com"],"Contractor_Address":["TEST - DNP"],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Tax_ID_Number":[""],"Date":["10/15/2015"],"Owner_Landlord_Name":[""],"Contractor_City":["TEST - DNP"],"Owner_Landlord_City":[""],"Installed_Address":["TEST - DNP"],"Phone_Number":["TEST - DNP"],"Owner_Landlord_Address":[""],"Owner_Landlord_Zip":[""],"Contractor_State":["TEST - DNP"],"Account_Holder_Last_Name":["TEST - DNP"],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Owner_Landlord_Phone_Number":[""],"Utility_Account_Number":["TEST - DNP"],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Home_Efficiency_Incentive_Program","isRead":false,"created":"2015-10-15T18:07.42.824Z"},{"_id":{"$oid":"561feb3a93783ecd8780bc11"},"origin":"10.71.199.148","fields":{"Facility_Name":["TEST - DNP"],"Approximate_Age_of_Facility":["99"],"Brief_Overview_of_Space_Use_and_Processes":["TEST - DNP"],"Input_Size_furnace":[""],"Facility_State":["TEST - DNP"],"Anticipated_Rebate_boiler":[""],"Anticipated_Rebate_Storage_Water_Heater":[""],"Model_Number_Water_Heater":[""],"Facility_Zip":["TEST - DNP"],"Installed_Address_Facility":["TEST - DNP"],"Quantity_Installed_Storage_Water_Heater":[""],"Rebate_Amount_furnace":[""],"Fax_Number":["TEST - DNP"],"Efficiency_Rating_boiler":[""],"License_Number":["TEST - DNP"],"Model_Number_Tankless_Water_Heater":[""],"Installed_Cost":["99"],"Efficiency_Rating_furnace":[""],"Quantity_Installed_boiler":[""],"Facility_City":["TEST - DNP"],"Property_Owned_or_Leased":["TEST - DNP"],"Anticipated_Rebate_Water_Heater":[""],"Total_Anticipated_Rebate":["99"],"Rebate_Amount_boiler":[""],"Quantity_Installed_furnace":[""],"Main_Building_Square_Footage":["99"],"Contractor_Zip":["TEST - DNP"],"Efficiency_Rating_Tankless_Water_Heater":[""],"Facility_Email":[""],"Anticipated_Rebate_furnace":[""],"Model_Number_furnace":[""],"Mailing_Address":[""],"Rebate_Amount_Storage_Water_Heater":[""],"Installed_Zip":["TEST - DNP"],"Manufacturer_Storage_Water_Heater":[""],"Manufacturer_boiler":[""],"Rebate_Amount_Tankless_Water_Heater":[""],"Anticipated_Rebate_Tankless_Water_Heater":[""],"Manufacturer_Water_Heater":[""],"employee_initials":[""],"Installed_State":["TEST - DNP"],"Installation_Date":["10/15/2015"],"Customer_Initials":["TEST - DNP"],"Efficiency_Rating_Storage_Water_Heater":[""],"Installed_City":["TEST - DNP"],"Gas_Account_Number":["TEST - DNP"],"Model_Number_Storage_Water_Heater":[""],"Contractor_Email":["kstrang@sprypoint.com"],"City":[""],"Account_Holder_Name":["TEST - DNP"],"Contractor_Address":["TEST - DNP"],"Rebate_Amount_Water_Heater":[""],"Input_Size_Tankless_Water_Heater":[""],"Quantity_Installed_Water_Heater":[""],"Contractor_Phone_Number":["TEST - DNP"],"Contractor_Name":["TEST - DNP"],"Zip":[""],"Date":["10/15/2015"],"Contractor_City":["TEST - DNP"],"Facility_Type":["Commercial (Wholesale/Retail)"],"Facility_Contact":["TEST - DNP"],"Efficiency_Rating_Water_Heater":[""],"Installed_Address":["TEST - DNP"],"Manufacturer_Tankless_Water_Heater":[""],"Input_Size_Storage_Water_Heater":[""],"State":[""],"Phone_Number":["TEST - DNP"],"Quantity_Installed_Tankless_Water_Heater":[""],"Phone_Number_Facility":["TEST - DNP"],"Manufacturer_furnace":[""],"Model_Number_boiler":[""],"Other_Buildings_Square_Footage":["999"],"Contractor_State":["TEST - DNP"],"Rebate_Made_Payable_To":["TEST - DNP"],"Federal_Tax_ID_Number":["TEST - DNP"],"Input_Size_boiler":[""],"Input_Size_Water_Heater":[""],"email_address":["kstrang@sprypoint.com"]},"formType":"RE_Commercial_and_Industrial_High_Efficiency_Heating_and_Water","isRead":false,"created":"2015-10-15T18:06.50.401Z"}]';
		}else{		
			$contents = curl_exec($ch);
		}
		curl_close($ch);
	
		$jsonContentsApplicationData = json_decode($contents);
		//print_pre($jsonContentsApplicationData);
		//print_pre($sprypointFields["5899ebddda01a9006cfad05e"]);
		foreach ($jsonContentsApplicationData as $applicationTypes){
//			if ($applicationTypes->id == "589a11cf110e2100735d77ee"){
				$formType = $applicationTypes->title;
				$formId = $applicationTypes->id;
				$formsById[$formId]=$formType;
				$formsByType[$formType]=$formId;
				if ($outputText){echo $formType." ".count($applicationTypes->submissions)."<Br>";}
				if (count($applicationTypes->submissions)){
					//echo $applicationTypes->id."=".$applicationTypes->title." ";
					//echo count($applicationTypes->submissions)."<br>";
					foreach ($applicationTypes->submissions as $record){
						//print_pre($record);
						$oid = $record->id;
//					if ($oid == "59e4ac38ece7f70009ee8342"){
						$createdDate = date("Y-m-d",strtotime($record->created));
						$recordData = $record->data;
						$recordData->formType = $formType;
						$recordData->formId = $formId;
						$recordData->created = $record->created;
						$formData[$oid] = $recordData;
						$customerNames[$oid] = str_replace("%26","_",urlencode(trim($recordData->customerName)))."_".$createdDate;
						$customerAccounts[trim($recordData->westfieldGasElectricAccountNumber)] = 1;
						//echo $recordData->customerName." ";
						$attachmentRecord = $record;
						$attachments = new stdClass();
						if (count($recordData->pleaseuploadyourcurrentelectricbillanddatedreceipt)){
							$attachments = $recordData->pleaseuploadyourcurrentelectricbillanddatedreceipt;
							foreach ($attachments as $attachment){
								if ($attachment->key){
									//echo $attachment->name." ";
									$attachmentsToGet[$oid][$attachment->key]=$formId;
									$attachmentsToGet[$oid][$attachment->name]=$formId;
								}
							}
						}
						//echo "<hr>";
//					}
					}
					//echo "<hr>";
				}
//			}//end if applicationTypesId
		}
		$attachmentCount = 0;
		if ($outputText){echo "There are: ".count($formData)." forms to proccess<bR>\r\n";}
		if ($outputText){echo "There are: ".count($customerAccounts)." customer Accounts<br>\r\n";}
		//print_pre($formsById);
		//print_pre($formsByType);
		if (count($formData)){
			foreach ($formData as $oid=>$formInfo){
				$thisOid = $oid;
				$thisFormType = strtolower($formInfo->formType);
				$thisFormId = $formInfo->formId;
				$thisFilesArray = $attachmentsToGet[$oid];
				$received["oid"][] = $thisOid;
				$theseFields = array();
				$theseNewFields = array();
				$fieldName = "";
				foreach ($formInfo as $fieldName=>$fieldValues){
					if (is_object($fieldValues) || is_array($fieldValues)){
						if ($fieldName == "rebateInformation"){
							$measureTitles = array(
								"Manufacturer"=>"manufacturer",
								"Model"=>"modelNo",
								"Rating"=>"efficiencyRating",
								"Input"=>"inputSizeBtUhr",
								"Total_Rebate"=>"totalRebateAmount",
								"Installed_Cost"=>"installedCost"
							);
							foreach ($fieldValues as $measure=>$measureValue){
								$measureNameClean = preg_replace('/[[:^digit:]]/', '', $measureValue->measureTypeDescriptionandRebate);
								if ($measureNameClean){
									$measureName = $FieldAdjustments[$thisFormId]["measureTypeDescriptionandRebate"][$measureNameClean]["name"];
									if (trim($measureName) == ""){
										if ($outputText){
											echo $fieldName."<br>\r\n";
											echo "1".$measureValue->measureTypeDescriptionandRebate."=".$measureNameClean." not found<bR>\r\n";
											print_pre($fieldValues);
											print_pre($measureValue);
										}
									}
									$rebateAmount = (string)$FieldAdjustments[$thisFormId]["measureTypeDescriptionandRebate"][$measureNameClean]["rebateAmount"];								
										foreach ($measureTitles as $title=>$formField){
											//echo $measureName."_".$title."=".$sprypointFields[$thisFormId][$measureName."_".$title]."=".$measureValue->{$formField}."<br>";
											$spryPointField = $sprypointFields[$thisFormId][$measureName."_".$title];
											if (trim($spryPointField) == ""){
												if ($outputText){echo "2".$measureName."_".$title." not found<br>\r\n";}
											}
											$theseNewFields[$sprypointFields[$thisFormId][$measureName."_".$title]] = trim($measureValue->{$formField});
										}
										if (trim($sprypointFields[$thisFormId][$measureName."_Quantity"]) == ""){
											if ($outputText){echo "3".$measureName."Quantity"." not found<br>\r\n";}
										}
									$theseNewFields[$sprypointFields[$thisFormId][$measureName."_Quantity"]] = trim($formInfo->qtyInstalled);

									/*
									echo $measure->measureTypeDescriptionandRebate."<br>";
									echo $measure->manufacturer."<br>";
									echo $measure->efficiencyRating."<br>";
									echo $measure->modelNo."<br>";
									echo $measure->inputSizeBtUhr."<br>";
									echo $measure->installedCost."<br>";
									echo $measure->totalRebateAmount."<br>";
									echo $measureName."Quantity=".$formInfo->qtyInstalled."<br>";
									*/
								}
							}//end if $measureNameClean
						}
						if ($fieldName== "centralACorHeatPumpInformationtobecompletedbycontractor"){
							$adjustedValueNames = array();
							foreach ($fieldValues as $fieldValueName=>$fieldValueValue){
								if ($fieldValueValue){
									$adjustedValueNames[] = ($FieldAdjustments[$thisFormId][$fieldName][$fieldValueName] ? $FieldAdjustments[$thisFormId][$fieldName][$fieldValueName]: $fieldValueName);
								}
							}
							$theseNewFields[$sprypointFields[$thisFormId][$fieldName]] = implode(",",$adjustedValueNames);
						}
						//echo $fieldName;
						//print_pre($fieldValues);
						$theseNewFields["FieldsToBeParsed"][$fieldName] = $fieldValues;
						foreach ($fieldValues as $fieldCounter=>$fieldParts){
							foreach ($fieldParts as $fieldValueName=>$fieldValueValue){
								$fieldName1 = $fieldName."_".$fieldCounter."_".$fieldValueName;
								//echo $fieldName1."=".$fieldValueValue."<br>";
								$fieldValue = ($fieldValueValue ? $fieldValueValue : NULL);
								$theseRawFields[$fieldName1] = trim($fieldValue);
								if (!$sprypointFields[$thisFormId][$fieldName1]){
									//echo $thisFormType." - ".$fieldName1." not found<br>";
									//echo "\r\n------------\r\n".$thisFormType," ".$fieldName."=".$fieldValue."\r\n------------\r\n";
								}
								//$theseNewFields[$sprypointFields[$thisFormId][$fieldName1]] = trim($fieldValue);
							}
						}
					}else{
						$processFields = true;
						if ($fieldName == "measureTypeandDescription"){
							$fieldValues = preg_replace('/[[:^digit:]]/', '', $fieldValues);
							$measureName = $FieldAdjustments[$thisFormId][$fieldName][$fieldValues]["name"];
							$rebateAmount = $FieldAdjustments[$thisFormId][$fieldName][$fieldValues]["rebateAmount"];
							$formInfo->rebateAmount = $rebateAmount;
							$measureTitles = array(
								"Manufacturer"=>"manufacturer",
								"CondenserModelNumber"=>"condenserModelNo",
								"CoilModelNumber"=>"coilModelNo",
								"SEER"=>"seer",
								"EER"=>"eer",
								"HSPF"=>"hspf",
								"SizeTons"=>"sizetons",
								"InstallCost"=>"installCost",
								"Rebate"=>"rebateAmount",
								"QtyInstalled"=>"qtyInstalled",
								"TotalRebate"=>"totalRebate");
								
	
							foreach ($measureTitles as $title=>$formField){
								//echo $measureName.$title."=".$formInfo->{$formField}."<br>";
								$theseNewFields[$sprypointFields[$thisFormId][$measureName.$title]] = trim($formInfo->{$formField});
								
							}
							$processFields = false;
						}
						
						if ($processFields){
							//echo $fieldName."=".$fieldValues."<br>";
							$fieldValue = ($fieldValues ? $fieldValues : NULL);
							$theseFields[$fieldName] = trim($fieldValue);
							$theseRawFields[$fieldName] = trim($fieldValue);
							if (!$sprypointFields[$thisFormId][$fieldName]){
								if ($outputText){echo $thisFormType." - ".$fieldName." not found<br>\r\n";}
								//echo "\r\n------------\r\n".$thisFormType," ".$fieldName."=".$fieldValue."\r\n------------\r\n";
							}
							//adjustfieldvalues to conform to current system
							$adjustedValue = ($FieldAdjustments[$thisFormId][$fieldName][$fieldValue] ? $FieldAdjustments[$thisFormId][$fieldName][$fieldValue]: $fieldValue);

							$theseNewFields[$sprypointFields[$thisFormId][$fieldName]] = trim($adjustedValue);
						}//end if processfields
					}
				}
				$theseNewFields["utilityName"] = "Westfield Gas & Electric";
				$theseNewFields["rebateMadePayableTo"] = $formInfo->customerName;
				
				ksort($theseFields);
				ksort($theseNewFields);
				$theseNewFields["formType"] = $sprypointFormTypes[$thisFormId];
				$theseNewFields["oid"] = $thisOid;
				$theseNewFields["rawFormData"] = json_encode($theseRawFields)." files:".json_encode($thisFilesArray);
				$readyToBeProcessed[$thisOid]["formType"]=$sprypointFormTypes[$thisFormId];
				$readyToBeProcessed[$thisOid]["fields"]=$theseFields;
				$readyToBeProcessed[$thisOid]["CETfields"]=$theseNewFields;
				$readyToBeProcessed[$thisOid]["files"] = $thisFilesArray;
				$readyToBeProcessed[$thisOid]["formInfo"] = $formInfo;
				
			}
			
			
		}else{
			if ($outputText){echo "There were no forms to process<Br>\r\n";}
		}
		//print_pre($readyToBeProcessed);
		if ($checkType != "attachments"){
			foreach ($readyToBeProcessed as $oid=>$CETFields){
				//echo "processing $oid<br>";

				$results = new stdClass();
				$newRecord = new stdClass();
				$formName = $CETFields["formType"];
				$formId = $CETFields["formId"];
				//echo "This OID was just received ".$oid."<Br>";
				$newRecord->formName = $formName;
				//print_pre($CETFields["CETfields"]);
				if (count($CETFields["CETfields"])){
					foreach ($CETFields["CETfields"] as $fieldName=>$fieldValue){
						$newRecord->$fieldName = $fieldValue;
					}
				}else{
					if ($outputText){echo $formName." for ".$oid." has no CETfields";}
				}
				if ($formName == "CommercialHeat"){$newRecord->customerType = "commercial";}
				$newRecord->action = "rebate_add";
				include($path.$pathSeparator.'muni'.$pathSeparator.'_ApiCustomerData_PUTForm.php');
				if ($rebateInsertReponse->insertedId){
					$updateOidResults = $muniProvider->updateOIDStatus($formName,$oid);
					$oidToRebateId[$oid][$formId]=$rebateInsertReponse->insertedId;
				}else{
					if ($outputText){print_pre($rebateInsertReponse);}
				}
			}
		}//only process attachments not rebates
		
		//print_pre($attachmentsToGet);
		if (count($attachmentsToGet)){
			foreach ($attachmentsToGet as $oid=>$attachments){
				//echo $oid.":<br>";
				foreach ($attachments as $key=>$formId){
					$attachmentCount++;
					$formName = $sprypointFormTypes[$formId];
					//echo $key."=".$formId."=".$formName."<br>";
					$rebateId = $oidToRebateId[$oid][$formId];
					if (trim($key)){
						//echo $key ."=". $type."<br>";
						
						//get the AWS SignedURL for the file
						$url = "https://j995yguu5f.execute-api.us-east-1.amazonaws.com/prod/getfile?token=oVuEHme8RuwuzJcxR399EePw9gKZMugz&filekey=".$key;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, 0);
						curl_setopt($ch, CURLOPT_HTTPGET, 1);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
						if(!curl_exec($ch)){
							if ($outputText){
								echo curl_error($ch) . '" - Code: ' . curl_errno($ch);
								echo "\r\nUsing hard coded sample set\r\n";
							}
							$contents = '[{"signed_url": "https://com-sprypoint-formio-uploads.s3.amazonaws.com/pdf-1b3e79c2-de33-4aff-aa16-a2b6c9548042.pdf?AWSAccessKeyId=ASIAIKCMIT6NERJL2RRQ&Expires=1488823824&Signature=FVsSB%2BiTU%2FiKsEDpdllj0qNbF2A%3D&x-amz-security-token=FQoDYXdzEPH%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDH9sRpj4hEewCuU1FyL%2BAfN7arDxOIXCPq35NBNN0V%2BJ3Et%2F9F4ISeql7wRclOs5aT9vDDYQoWaeOoXIetz8z0PWXTZDSp4bQiuWLsFj68PclyAV5wWWO9TuyeCP2XCmMjIuVdwUJig1ntgy6EIrmT6Xx8TcjawuX1uAo0U1ailzWZ6McGqYB85t2EyBa5PIdVA2XKzpvLEwbrzu2oZZzaZWC78tbaKTtpoZgZuI4sRCr9bmRtwNxPayN9cDAO7h1%2BtNJPsAiuzmfyub5TgUswEZWM2wLBbTkwlnmEw8IYo3vvd4j9PmubBV10tQ1vZzNWmV1GoUWGCvjAd9YWoa5B3k988pvA1Gf8dUBEfBKMGV9sUF"}]';
						}else{		
							$contents = curl_exec($ch);
						}
						curl_close($ch);
					
						$jsonContentsFileLinks = json_decode($contents);
						$signedURL = $jsonContentsFileLinks->signed_url;
						//echo "<a href=".$signedURL.">".$signedURL."</a><br>";
						
						//now get the actual file to save
						$url = $signedURL;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, 0);
						curl_setopt($ch, CURLOPT_HTTPGET, 1);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						if ($testingServer){curl_setopt($ch, CURLOPT_CAINFO, "C:\\xampp\\htdocs\\repos\\admintest\\cetadmin\\muni\\cacert.pem");}
						$data = curl_exec($ch);
						$error = curl_error($ch);
						$error_no = curl_errno($ch);
						curl_close($ch);
						if(!$data){
							if ($outputText){
								echo $error . '" - Code: ' . $error_no;
								echo "\r\nAttached files were not retrieved for oid=".$oid."\r\n";
							}
						}else{		
							$fileName = $key;

							$destinationName = $formName."_".$customerNames[$oid]."_".$fileName;
							$destination = $destinationFolder.$destinationName;
							$file = fopen($destination, "w+");
							fputs($file, $data);
							fclose($file);
							//now update the record to have the file information
							$destinationNames[$oid][$formName][] = $destinationName;
						}
					}
				}
			}
		}//end if attachmentsToGet
		
		//update the rebates with the file names
		//print_pre($destinationNames);
		if (count($destinationNames)){
			foreach ($destinationNames as $oid=>$formNameInfo){
				$destinationName = "";
				foreach ($formNameInfo as $formName=>$destinationNames){
					$destinationName = implode(",",$destinationNames);
					//echo $formName.",".$destinationName.",".$oid."<br>";
					$updateFileResults = $muniProvider->updateFileStatus($formName,$destinationName,$oid);
				}
			}
		}
		
		
		
		$myfile = fopen($sinceFile, "w") or die("Unable to open file!");
		fwrite($myfile, date("c"));
		fclose($myfile);
		$response["count"] = $attachmentCount;
		if ($outputText){echo "There are: ";}
		echo $attachmentCount;
		if ($outputText){echo " attachments";}
?>