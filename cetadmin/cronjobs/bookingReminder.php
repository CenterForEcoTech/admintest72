<?php
$path = getcwd();
$pathSeparator = "/";
if (strpos($path,"xampp")){
	$pathSeparator = "\\";
	$testingServer = true;
}else{
	$thisPath = ".:/home/cetdash/pear/share/pear";
	set_include_path($thisPath);
	
}
if (!strpos($path,"xampp")){$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";}else{$path = "..";}
//$path = "/home/cetdash/cetdashboard.info/admintest/cetadmin/";
include_once($path.$pathSeparator."_config.php");
set_time_limit (6000);

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResidentialProvider.php");
$residentialProvider = new ResidentialProvider($dataConn);
$criteria = new stdClass();
$criteria->energySpecialistNot = "Cancelled";
$criteria->status = "Confirmed";
$criteria->apptDate = date("Y-m-d");
$scheduledResults = $residentialProvider->getScheduledData($criteria);
foreach ($scheduledResults->collection as $record){
	$dateOfAppt = date("Y-m-d",strtotime($record->apptDate));
	$startTimeStr = strtotime($record->apptDate);
	$ES = $record->energySpecialist;
	//print_pre($record);
	$tomorrow = strtotime(date("m/d/Y")." + 1 day");
	$dayAfterTomorrow = strtotime(date("m/d/Y")." + 2 days"); //was 2
	$nextWeekStart = strtotime(date("m/d/Y")." + 7 days");
	$nextWeekEnd = strtotime(date("m/d/Y")." + 8 days");  //was 8
	if ($startTimeStr > $tomorrow && $startTimeStr < $dayAfterTomorrow){
		$scheduledDataResults[] = $record;
	}
	if ($startTimeStr > $nextWeekStart && $startTimeStr < $nextWeekEnd){
		$scheduledDataResults[] = $record;
	}
}

//print_pre($scheduledDataResults);

require_once $path.'/gbs/salesforce/config.php';
include_once($path.'/gbs/salesforce/soap_connect.php');
//get the Energy company from Salesforce
$query = "SELECT OwnerId, Name, Notes_For_Energy_Audit__c, What_to_Expect__c FROM MMWEC_Energy_Company__c";
//echo $query;
$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
//print_pre($response);
foreach ($response->records as $record){
	$MMWECEnergyCompanyByName[$record->Name] = $record;
}


//print_pre($scheduledDataResults);
if (!count($scheduledDataResults)){
	echo "No Reminders to be sent";
}else{
	$isReminder = true;
	//send emails if there are some to send
	require_once "Mail.php";  
	require_once "Mail/mime.php";  
	//boolean addAttachment(string $file, string $c_type = 'application/pdf', string $name = 'wififlier.pdf', boolean $isfile = true, string $encoding = 'base64', string $disposition = 'attachment', string $charset = '', string $language = '', string $location = '/attachments/wififlier.pdf', string $n_encoding = null, string $f_encoding = null, string $description = '', string $h_charset = null);
	
	$host = "smtp.office365.com"; 
	$port = "587"; 
	
	$from = "Center For EcoTechnology <no-reply@cetonline.org>"; 
	$username = "no-reply@cetonline.org"; 
	$password = "Dark76wall!12";  
	
	
	foreach ($scheduledDataResults as $newRecord){
		if ($newRecord->email){
			//echo "Type: ".$apptType;
			$to = $newRecord->customerName." <".$newRecord->email.">";
			$bcc = ",IT Admin <it@cetonline.org>,Kacie Dean <Kacie.Dean@cetonline.org>";
			if ($testingServer){
				//$to = "IT Admin <it@cetonline.org>";
				$to = "Lauren Holway <lauren.holway@cetonline.org>";
				$bcc = "";
			}
			$recipients = $to.$bcc;

			$crlf = "\r\n";
			//$fileatt = "./wififlier.pdf";
			//$fileatttype = "application/pdf"
			//$file = fopen($fileatt,'rb');
			//$data = fread($file, filesize($fileatt));
			//fclose ($file);
			//$semi_rand = md5(time());
			//$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
			$customerName = ucwords(strtolower($newRecord->customerName));
			$apptType = $newRecord->visitType;
			$ES = $newRecord->energySpecialist;
			$siteId = $newRecord->siteId;
			$apptDate = $newRecord->apptDate;
			
			$apptStartTime = date("g:iA",strtotime($newRecord->apptDate));
			$address = str_replace(" Ma "," MA ",ucwords(strtolower($newRecord->address)));
			
			$htmlBodyTemplateIntro = "";
			$htmlBodyTemplateMiddle = "";
			$htmlBodyTemplateEnd = "";		
				$subjectTitle = $apptType;
				switch ($apptType){
					case "Renter":
						$templateFile = "bookingTemplate_RENTER.php";
						$exceltemplateFile = "bookingTemplate_RENTER.xlsx";
						$subjectTitle = "Energy Audit";
						break;
					case "Return Visit":
						$templateFile = "bookingTemplate_Return_Visit.php";
						$exceltemplateFile = "bookingTemplate_Return_Visit.xlsx";
						$subjectTitle = "Energy Audit Return";
						break;
					case "Combustion Safety Revisit":
						$templateFile = "bookingTemplate_CST.php";
						$subjectTitle = "Combustion Safety Revisit";
						break;
					case "HEA":
						$templateFile = "bookingTemplate_HEA.php";
						$subjectTitle = "Energy Assessment";
						$file = "wififlier.pdf";
						break;
					case "HEAAU":
						$templateFile = "bookingTemplate_HEA.php";
						$subjectTitle = "Energy Assessment";
						break;
					case "Inspection":
						$templateFile = "bookingTemplate_CST.php";
						$subjectTitle = "Inspection";
						break;
					case "SHV":
						$templateFile = "bookingTemplate_SHV.php";
						$subjectTitle = "Special Home";
						break;
					case "WGE":
						$templateFile = "bookingTemplate_WGE.php";
						$subjectTitle = "WGE Energy Audit";
						break;
					case "WiFi":
						$templateFile = "bookingTemplate_WiFi.php";
						$subjectTitle = "Wireless Thermostat";
						// Attachment - added by Lauren
						$file = "attachments/wififlier.pdf";
						break;
					case "Virtual HEA":
						$templateFile = "bookingTemplate_Generic.php";
						$subjectTitle = "Virtual Energy Assessment";
						break;
					case "Virtual SHV":
						$templateFile = "bookingTemplate_Generic.php";
						$subjectTitle = "Virtual Energy Assessment";
						break;	
					case "Post Virtual HEA":
						$templateFile = "bookingTemplate_Generic.php";
						$subjectTitle = "Virtual Energy Assessment";
						break;	
						
					case "MMWEC-Audit":
						//uses require salesforce files at top of this page
						//$NotesForEnergyAudit = "31415"
						//$electricProvider = $newRecord->electricProvider;
						//$WhatToExpect = $electricProvider->What_to_Expect__c;
						//$NotesForEnergyAudit = $record->Notes_For_Energy_Audit__c;
						//$WhatToExpect = $record->What_to_Expect__c;
						$templateFile = "bookingTemplate_MMWEC-Audit.php";
						$exceltemplateFile = "bookingTemplate_MMWEC-Audit.xlsx";
						$subjectTitle = "Energy Audit";
						
						break;
						
					default:
						$templateFile = "bookingTemplate_Generic.php";
						$exceltemplateFile = "bookingTemplate_Generic.xlsx";
						$subjectTitle = "Energy Audit";
						break;
				}
			include($path.'/residential/model/'.$templateFile);
			
			$htmlBodyTemplate = $htmlBodyTemplateIntro.(!$isReminder ? $htmlBodyTemplateMiddle : "").$htmlBodyTemplateEnd;
			$textBodyTemplate = $textBodyTemplateIntro.(!$isReminder ? $textBodyTemplateMiddle : "").$textBodyTemplateEnd;

			
			$htmlBody = $htmlBodyTemplate;
			$txtBody = $textBodyTemplate;
			//$subject = (!$isReminder ? "Confirmed: " : "Reminder: ").$newRecord->visitType." visit on ".date("m/d/Y", strtotime($newRecord->apptDate))." with ".$newRecord->energySpecialist;
			$subject = (!$isReminder ? "Confirmed: " : "Reminder: ").$subjectTitle." visit on ".date("m/d/Y", strtotime($newRecord->apptDate))." with ".$newRecord->energySpecialist;
			
			
			$headers = array (
				'From' => $from,
				'To' => $to, 
				//'Reply-to' => "Bailey Gadreault <Bailey.Gadreault@cetonline.org>",				
				//'Reply-to' => "Kacie Dean <Kacie.Dean@cetonline.org>",
				'Reply-to' => "Customer Support <customersupport@cetonline.org>",
				'Subject' => $subject
			); 
			$mime = new Mail_mime($crlf);
			$mime->setTXTBody($txtBody);
			$mime->setHTMLBody($htmlBody);
			$file = "/wififlier.pdf";
			$mime->addAttachment($file, 'text/plain'); //added by Lauren
			$body = $mime->get();
			$headers = $mime->headers($headers);
					
			$smtp = Mail::factory(
				'smtp',   
				array (
					'host' => $host,     
					'port' => $port,     
					'auth' => true,     
					'username' => $username,     
					'password' => $password
				)
			);  
			$messages["apptType"][$apptType]++;
				$mail = $smtp->send($recipients, $headers, $body);
				if ($mail){ 	
					$messages["success"][] = $newRecord;
					$messages["apptType"][$apptType]++;
				}else{
					$messages["failed"][] = $headers;
				}
		}//end if email
	}
	echo "Successfully sent ".count($messages["success"])." reminders<Br>\r\n";
	if ($testingServer){
		print_pre($messages["apptType"]);
	}
	echo "There were ".(count($messages["failed"]) ? : "0")." sending failures<Br>\r\n";
	if (count($messages["failed"])){
		print_pre($messages["failed"]);
	}
}

?>