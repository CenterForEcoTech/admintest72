<?php
ini_set('memory_limit', '1200M');

error_reporting(E_ALL);
$time_start = microtime(true); 
include_once("../_config.php");

$SharepointFiles = str_replace("//","/",$siteRoot).$adminFolder."sharepoint/cache/";
ob_start;
include_once($repositoryApiFolder."utils/DirectoryScanner.php");
$files = scanDir::scan($SharepointFiles, false, false);

$fileName = fopen("SharepointALL.csv","a");
$fileCount = 0;
$fileCounter = $fileCount+$_GET['end'];
$endLocation = $fileCounter+20;
$fileCountStart = $_GET['start'];
$startLocation = $fileCountStart+20;
foreach ($files as $file){
	if ($fileCount > $fileCountStart && $fileCount < $fileCounter){
		$fileLocation = str_replace("\\","",$file);
		$fileLocation = str_replace("//","/",$fileLocation);
		$fileDate = explode("_",$fileLocation);
		$datePart = $fileDate[1];
		if (strpos($datePart,"-")){
			$dateParts = explode("-",$datePart);
			$datePart = $dateParts[0];
		}
		$timeStamp = date("Y-m-d g:i:s",$datePart);
		$checkingFile = "Starting #".$fileCount." ".$fileLocation." ".$timeStamp."<br>";
		$jsonFile = file_get_contents ($fileLocation);
		$json = json_decode($jsonFile, true); // decode the JSON into an associative array
		echo $checkingFile;
		if (count($json)){
			foreach ($json as $rowID=>$jsonData){
				$thisRow = array($timeStamp,$jsonData["site_x0020_id"],$jsonData["id"]);
				fputcsv($fileName,$thisRow);

				if (!in_array($jsonData["id"],$SharepointArrayBySiteId[$jsonData["site_x0020_id"]])){$SharepointArrayBySiteId[$jsonData["site_x0020_id"]][] = $jsonData["id"];}
				if (!in_array($jsonData["site_x0020_id"],$SharepointArrayById[$jsonData["id"]])){$SharepointArrayById[$jsonData["id"]][] = $jsonData["site_x0020_id"];}
	//			echo "<tr><td>".$jsonData["id"]."</td><td>".$jsonData["site_x0020_id"]."</td><td>".$fileLocation."</td></tr>";
			}
		}
	}
	$fileCount++;
}
fclose($fileName);
/*
$fileName = fopen("SharepointIdBySiteId".$endLocation.".csv","w");
foreach ($SharepointArrayBySiteId as $SiteId=>$data){
	$thisRow = array($SiteId,implode("|",$data));
	fputcsv($fileName,$thisRow);
}
fclose($fileName);
*/
?>
		<div>Proceeding to Next File<span id="dots"></span></div>
		<script type="text/javascript">
			location.href="parseSharepointId.php?start=<?php echo $startLocation;?>&end=<?php echo $endLocation;?>";
		</script>
