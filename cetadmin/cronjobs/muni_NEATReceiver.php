<?php
include_once("../_config.php");
include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
$results = new stdClass();
$newRecord = json_decode($currentRequest->rawData);

if ($newRecord->testing){
	$results = array("received"=>true,"WhoIsAwesome"=>"Yosh");
}else{
	include_once('../muni/views/_auditFormNEATParameters.php');
	switch(strtoupper($currentRequest->verb)){
		case "DELETE":
			echo "this is a delete";
			break;
		case "GET":
			echo "this is GET";
			break;
		case "POST":
			$SIRData["posted"] = true;
			include_once('../muni/views/_NEATProcessor.php');
			$results = $SIRData;
			
			break;
		case "PUT":
			echo "this is PUT";
			break;
	}
}
echo json_encode($results);
//output_json($results);
die();
?>