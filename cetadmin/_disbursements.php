<h2>Test the disbursement scripts</h2>

<p>You are here because you are a tester and want to manually push invoiced transactions through the disbursement pipeline.</p>
<p>You might also be here to determine if running these steps out of order (asynchronously) will cause problems in the system.</p>

<fieldset>
    <h3>Load the incoming queue</h3>
    <p>
        Click this button after creating invoices to move data from the Causetown database into the disbursment queue.
        The purpose of this step is to cache data so processing of transaction data does not impede performance on the main Causetown database tables.
        We are also moving the data into a more transaction based database system, which is not as fast for read performance,
        but is much more stable against system failures.
    </p>
    <p>
        In the UI, once it is built, this step should prevent a customer from choosing a recipient on the affected transactions.
    </p>
    <p>
        We expect this script to be launched daily after the paid invoice remote call, or hourly on a maximum number of transactions per run, depending upon performance impact.
    </p>

    <div class="alert current-action-log"></div>
    <input type="button" id="action-load-incoming-queue" value="Load Incoming Queue">
</fieldset>

<fieldset>
    <h3>Process the incoming queue</h3>
    <p>
        Click this button after loading the incoming queue to move funds into customer accounts (pots of money). Prior to executing this step,
        funds are in "limbo" between transactions (customer no longer allowed to choose recipient) and Pots of money.
    </p>
    <p>
        We expect this script to be launched daily after the paid invoice remote call, or hourly on a maximum number of transactions per run, depending upon performance impact.
    </p>

    <div class="alert current-action-log"></div>
    <input type="button" id="action-process-incoming-queue" value="Process Incoming Queue">
</fieldset>

<fieldset>
    <h3>Apply the allocation plans</h3>
    <p>
        In an automated system, this step is handled with the next step in order to apply a customer's preferred allocations
        prior to moving funds that are ready to disburse into the specific nonprofit accounts.

        Running the next step out of order will only cause the nonprofit accounts to have less funds, assuming there are customer
        allocations to be applied, and presumably, the system will "catch up" later as required.
    </p>

    <p>
        We expect this step to include a "lock" on a user's ability to allocate funds in their &ldquo;<?php echo $givingBankName;?>.&rdquo; Current
        plans are to schedule this process once per day and, like a bank, announce a general policy that between certain
        hours, plans may not be adjusted due to system maintenance updates.
    </p>

    <div class="alert current-action-log"></div>
    <input type="button" id="action-apply-allocations" value="Apply Allocation Plans">
</fieldset>

<fieldset>
    <h3>Move funds to Nonprofit Accounts</h3>
    <p>
        In an automated system, this step is handled with the previous step in order to apply a customer's preferred allocations
        prior to moving funds that are ready to disburse into the specific nonprofit accounts.

        Running the this step out of order will only cause the nonprofit accounts to have less funds, assuming there are customer
        allocations to be applied, and presumably, the system will "catch up" later as required.
    </p>
    <p>
        These funds, once moved, are no longer in a customer's &ldquo;<?php echo $givingBankName;?>,&rdquo; but are not yet "ready to disburse"--that happens in the next step (the "Disburse Funds" tab).
    </p>
    <p>
        This step, once executed, allows all cached data in prior steps to be purged as required as part of system maintenance.
        At this time, there are no plans to do any data purging, and in the future, data are likely to be purged only annually in order
        to maintain enough backlog data to run quality check reports.
    </p>

    <div class="alert current-action-log"></div>
    <input type="button" id="action-load-disbursements" value="Move Funds to Nonprofit Accounts">
</fieldset>

<script type="text/javascript">
    $(function(){
        var logAction = function(element, text){
                element.append("<p>" + text+ "</p>");
            },
            callAction = function(url, name, alertElement){
                var now = new Date();
                logAction(alertElement, name + " process triggered at " + now.toLocaleDateString() + " " + now.toLocaleTimeString());
                $.ajax({
                    url: url,
                    success: function(){
                        var now = new Date();
                        logAction(alertElement, name + " processed " + now.toLocaleDateString() + " " + now.toLocaleTimeString());
                    }
                });
            };

        $("#action-load-incoming-queue").click(function(){
            callAction("act_loadIncomingQueue.php", "Load Incoming Queue", $(this).prev(".current-action-log"));
        });
        $("#action-process-incoming-queue").click(function(){
            callAction("act_processIncomingQueue.php", "Load Pots of Money", $(this).prev(".current-action-log"));
        });
        $("#action-apply-allocations").click(function(){
            callAction("act_applyAllocations.php", "Allocate plans", $(this).prev(".current-action-log"));
        });
        $("#action-load-disbursements").click(function(){
            callAction("act_loadDisbursements.php", "Move Funds to Nonprofit accounts", $(this).prev(".current-action-log"));
        });
    });
</script>