<?php 
$ThisMenuSuperAdmin = (count(array_intersect($adminSecurityGroups,$Security_SuperAdmin_Resources)) ? true : false);
if ($ThisMenuSuperAdmin){
	$ReadOnlyTrue = false;	
}
	$reportDropDownItemsTop = "90";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Resources))){?>
			<li><a id="conversionCalculations" href="<?php echo $CurrentServer.$adminFolder;?>resources/?nav=conversionCalculations" class="button-link">Conversion Calculations</a></li>
			<!--
			<li id="ReportMenuDropDown"><a id="reports" href="<?php echo $CurrentServer.$adminFolder;?>resources/?nav=reports" class="button-link">Reports</a>
				<?php //($nav != "reports" ? include('_reportsTabs.php') : "");?>
			</li>
			-->
<?php }?>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#reportDropDownItems").hide();
		});
		$("#reports.button-link").on("mouseover",function(){
			$("#reportDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "reports":
					$nav = "reports";
					$showReportsDropDown = true;
					break;
				case "report-bycode":
					$navDropDown = $nav;
					$nav = "reports";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
		
		
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>