var CETDASH = CETDASH || {};

jQuery.fn.highlight = function() {
    $(this).each(function() {
        var el = $(this),
            wrap = $("<div/>");
        el.before(wrap)
        wrap
            .width(el.innerWidth())
            .height(el.innerHeight())
            .css({
                "position": "absolute",
                "background-color": "#ffff99",
                "opacity": ".7"
            })
            .fadeOut(1500, function(){
                wrap.remove();
            });
    });
}
CETDASH.forms = CETDASH.forms || {};
CETDASH.forms.placeError = function(message, jqueryElement){
    var labelContainer = jqueryElement.closest("label"),
        divContainer = jqueryElement.closest("div.paired.fields"),
        labelAlertElement = labelContainer.find(".alert"),
        divAlertElement = divContainer.find(".alert"),
        errorElement;
    if (message instanceof jQuery){
        errorElement = message;
    } else {
        errorElement = $("<span class='error'>"+message+"</span>");
    }
    if (labelAlertElement.length){
        labelAlertElement.html("");
        errorElement.appendTo(labelAlertElement);
    } else if (divAlertElement.length) {
        divAlertElement.html("");
        errorElement.appendTo(divAlertElement);
    } else {
        errorElement.appendTo(labelContainer);
    }
    jqueryElement.highlight();
}
CETDASH.forms.handleStandardErrors = function(container, errorObjectArray){
    var form = container,
        errorArray = errorObjectArray,
        fieldName, message, fieldElement;
    $.each(errorArray, function(index, error){
        if (error.name && error.message){
            fieldName = error.name;
            message = error.message;
            fieldElement = form.find("input[name='"+fieldName+"'],select[name='"+fieldName+"'],textarea[name='"+fieldName+"']");
            if (fieldElement.length){
                CETDASH.forms.placeError(message, fieldElement);
            }
        }
    });
}


CETDASH._scrollTimeout = undefined;
CETDASH.scrollTo = function(element, delay){
    if (CETDASH._scrollTimeout){
        clearTimeout(CETDASH._scrollTimeout);
    }
    if (!$.isNumeric(delay)){
        delay = 200;
    }
    CETDASH._scrollTimeout = setTimeout(function(){
        //alert("scrolling to " + $(element).attr("id"))
        $('html, body').animate({
            scrollTop: $(element).offset().top - 20
        }, 500);
    }, delay);
};

$.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
{
    if ( sNewSource !== undefined && sNewSource !== null ) {
        oSettings.sAjaxSource = sNewSource;
    }

    // Server-side processing should just call fnDraw
    if ( oSettings.oFeatures.bServerSide ) {
        this.fnDraw();
        return;
    }

    this.oApi._fnProcessingDisplay( oSettings, true );
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];

    this.oApi._fnServerParams( oSettings, aData );

    oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
        /* Clear the old information from the table */
        that.oApi._fnClearTable( oSettings );

        /* Got the data - add it to the table */
        var aData =  (oSettings.sAjaxDataProp !== "") ?
            that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;

        for ( var i=0 ; i<aData.length ; i++ )
        {
            that.oApi._fnAddData( oSettings, aData[i] );
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();

        that.fnDraw();

        if ( bStandingRedraw === true )
        {
            oSettings._iDisplayStart = iStart;
            that.oApi._fnCalculateEnd( oSettings );
            that.fnDraw( false );
        }

        that.oApi._fnProcessingDisplay( oSettings, false );

        /* Callback user function - for event handlers etc */
        if ( typeof fnCallback == 'function' && fnCallback !== null )
        {
            fnCallback( oSettings );
        }
    }, oSettings );
};