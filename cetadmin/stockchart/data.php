<?php
header("content-type: application/json"); 
echo $_GET['callback'];    
include_once("../_config.php");
include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");

$currentRequest = new Request();
//$results = new stdClass();
$type = $_GET['type'];
if ($type == "warehouse" || $type == "product" || $type == "productOnHand"){
	include_once($dbProviderFolder."InventoryProvider.php");
	
	if ($type == "warehouse"){
		$criteria = new WarehouseCriteria();
		$criteria->name=$_GET['warehouse'];
		$criteria->efi=$_GET['EFI'];
		$criteria->forChart=true;
		$warehouseProvider = new WarehouseProvider($dataConn);
		$warehouseCount = $warehouseProvider->getWarehouseCount($criteria);
		$Collection = $warehouseCount->collection;
		foreach ($Collection as $warehouse=>$warehouseObj){
			$asOfDate = strtotime($warehouseObj->asOfDate)*1000;
			$results[] = array($asOfDate,$warehouseObj->qty);
//			$transactionDate = strtotime($warehouseObj->transactionDate." 00:00:00")*1000;
//			$results[] = array($transactionDate,$warehouseObj->qty);
		}
	}
	if ($type == "product"){
		$productProvider = new ProductProvider($dataConn);
		$productMinHistory = $productProvider->getMinimumHistory($_GET['EFI']);
		$Collection = $productMinHistory->collection;
		foreach ($Collection as $product=>$productObj){
			$asOfDate = strtotime($productObj->GHSInventoryProductsMinimumHistory_TimeStamp)*1000;
			$results[] = array($asOfDate,$productObj->GHSInventoryProductsMinimumHistory_Qty);
		}

	}	
	if ($type == "productOnHand"){
		$criteria = new stdClass();
		$criteria->efihistory=true;
		$criteria->efi=$_GET['EFI'];
		$warehouseProvider = new WarehouseProvider($dataConn);
		$warehouseCount = $warehouseProvider->getWarehouseCount($criteria);
		$Collection = $warehouseCount->collection;
		//first consolodate same date but different time counts
		foreach ($Collection as $warehouse=>$warehouseObj){
			$warehouseName = $warehouseObj->warehouseName;
			if ($warehouseName == "Nonotuck" || substr($warehouseName,0,3)=="CET"){
				$qty = $warehouseObj->qty;
				$asOfDate = $warehouseObj->asOfDate;
				$asOfDateParts = explode(" ",$asOfDate);
				$ProductCountDates[$warehouseName][$asOfDateParts[0]]=1;
				$AsOfDates[$asOfDateParts[0]] = 1;
				if (strtotime($ProductCount[$warehouseName][$asOfDateParts[0]]["time"]) < strtotime($asOfDateParts[1])){
					$ProductCount[$warehouseName][$asOfDateParts[0]]["asofdate"]=$asOfDateParts[0];
					$ProductCount[$warehouseName][$asOfDateParts[0]]["time"]=$asOfDateParts[1];
					$ProductCount[$warehouseName][$asOfDateParts[0]]["qty"]=$qty;
				}
			}
		}
		ksort($AsOfDates);
		foreach ($ProductCountDates as $warehouse=>$asofdate){
//			echo $warehouse ." has ".count($asofdate)." movement dates\n";
		}
		//Now Add add the warehouses together
		foreach ($AsOfDates as $AsOfDateFinder=>$count){
//			echo "\n".$AsOfDateFinder."\n";
			foreach ($ProductCount as $Warehouse=>$AsOfDate){
				if ($AsOfDate[$AsOfDateFinder]["qty"]){
					$PriorQty[$Warehouse] = $AsOfDate[$AsOfDateFinder]["qty"];
//					echo " ".$Warehouse." has ".$AsOfDateFinder." date ".$AsOfDate[$AsOfDateFinder]["qty"]."\n";
				}else{
//					echo " ".$Warehouse." using prior quantity ".$PriorQty[$Warehouse]."\n";
				}
				$qtyTotal = $PriorQty[$Warehouse];
				$ProductTotal[$AsOfDateFinder] = $ProductTotal[$AsOfDateFinder]+$qtyTotal;
				
				/*
				foreach ($AsOfDate as $ThisDate=>$ThisDateInfo){
					$qtyTotal = $ThisDateInfo["qty"];
					$ProductTotal[$AsOfDateFinder] = $ProductTotal[$AsOfDateFinder]+$qtyTotal;
				}
				*/
			}
			
		}
		ksort($ProductTotal);
		foreach ($ProductTotal as $AsOfDate=>$Qty){
			$asOfDate = strtotime($AsOfDate)*1000;
			$results[] = array($asOfDate,$Qty);
		}
	}	
}
if ($type == "forecast"){
	include_once($dbProviderFolder."ForecastingProvider.php");
	$report = $_GET['report'];
	if ($type == "forecast" && $report=="PL"){
		$forecastingProvider = new ForecastingProvider($dataConn);
		$criteria = new stdClass();
		$criteria->department = $_GET['department'];
		$criteria->category = $_GET['category'];
		$criteria->type = $_GET['categoryType'];
		$criteria->startDate = $_GET['startDate'];
		$criteria->endDate = $_GET['endDate'];
		$resultArray = $forecastingProvider->getPL($criteria);
		foreach ($resultArray as $result){
			$forecast = round($result->forecast,0,PHP_ROUND_HALF_UP);
			$amount = round($result->actual,0,PHP_ROUND_HALF_UP);
			$forecastInfo = false;
			if (!$amount && $forecast > 0){
				$amount = $forecast;
			}
			
			$middleOfMonth = date("Y-m-1",strtotime($result->date));
			$asOfDate = strtotime($middleOfMonth)*1000;
			$results[] = array($asOfDate,(int)$amount);
		}
	}
	if ($report == "apptanalysis"){
		include_once($dbProviderFolder."CSGDataProvider.php");
		$CSGDataProvider = new CSGDataProvider($dataConn);
		$StartDate = $_GET['StartDate'];
		$EndDate = date("Y-m-t",strtotime($StartDate));

		$criteria = new stdClass();
		$criteria->startDate = $StartDate;
		$criteria->endDate = $EndDate;
		$result = $CSGDataProvider->getApptTypeAnlysis($criteria);
		//print_pre($results);
		if (count($result)){
			//remove the file so there should only be one schedulereport file in the directory
			foreach ($result as $ID=>$Data){
				$ApptTypeArray[$Data["yearMonth"]][$Data["apptType"]][$Data["asOfDate"]] = $Data["count"];
			}
		}

		foreach ($ApptTypeArray as $YearMonth=>$AptType){
			$PriorMonthEndDate = date("Y-m-t",strtotime($YearMonth." -1 month"));
			$PriorMonthMidDate = date("Y-m-15",strtotime($YearMonth." -1 month"));
			$CurrentMonthEndDate = date("Y-m-t",strtotime($YearMonth));
			$CurrentMonthMidDate = date("Y-m-15",strtotime($YearMonth));
			$thisMonthDates = array();
			foreach ($AptType["HEA"] as $thismonthDate=>$HEACount){
				$thisMonthDates[] = $thismonthDate;
				$asOfDate = strtotime($thismonthDate)*1000;
				$results[] = array($asOfDate,(int)$HEACount);
			}
	/*
			$ClosestPriorMonthMidDate = find_closest($thisMonthDates, $PriorMonthMidDate);
			$asOfDate = strtotime($ClosestPriorMonthMidDate)*1000;
			$results[] = array($asOfDate,(int)$AptType["HEA"][$ClosestPriorMonthMidDate]);
			
			$ClosestPriorMonthEndDate = find_closest($thisMonthDates, $PriorMonthEndDate);
			$asOfDate = strtotime($ClosestPriorMonthEndDate)*1000;
			$results[] = array($asOfDate,(int)$AptType["HEA"][$ClosestPriorMonthEndDate]);
			
			$ClosestCurrentMonthMidDate = find_closest($thisMonthDates, $CurrentMonthMidDate);
			$asOfDate = strtotime($ClosestCurrentMonthMidDate)*1000;
			$results[] = array($asOfDate,(int)$AptType["HEA"][$ClosestCurrentMonthMidDate]);

			$ClosestCurrentMonthEndDate = find_closest($thisMonthDates, $CurrentMonthEndDate);
			$asOfDate = strtotime($ClosestCurrentMonthEndDate)*1000;
			$results[] = array($asOfDate,(int)$AptType["HEA"][$ClosestCurrentMonthEndDate]);
*/			
			
			$MonthlyData[$YearMonth]["PriorMid"][$ClosestPriorMonthMidDate] = $AptType["HEA"][$ClosestPriorMonthMidDate];
			$MonthlyData[$YearMonth]["PriorEnd"][$ClosestPriorMonthEndDate] = $AptType["HEA"][$ClosestPriorMonthEndDate];
			$MonthlyData[$YearMonth]["CurrentMid"][$ClosestCurrentMonthMidDate] = $AptType["HEA"][$ClosestCurrentMonthMidDate];
			$MonthlyData[$YearMonth]["CurrentEnd"][$ClosestCurrentMonthEndDate] = $AptType["HEA"][$ClosestCurrentMonthEndDate];
			//print_pre($MonthlyData);
		}
	}
}
echo "(".json_encode($results).")";
?>