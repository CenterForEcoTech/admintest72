<h2>Permalink Management Console</h2>
<?php
if (isset($_POST["old_pid"])){
    $oldPid = htmlspecialchars($_POST["old_pid"]);
    $newPid = htmlspecialchars($_POST["new_pid"]);
    include_once($dbProviderFolder."PermaLinkProvider.php");
    $provider = new PermaLinkProviderImpl($mysqli);
    $oldPidRecord = $provider->get($oldPid);
    $newPidAvailable = $provider->isAvailable($newPid);
    if (!$newPidAvailable){
        $alert = "\"".$newPid."\" is already used. Please select a different one.";
    } else if (!is_numeric($oldPidRecord->entityId) || $oldPidRecord->entityId <= 0) {
        $alert = "\"".$oldPid."\" does not appear to be a valid existing Permalink ID.";
    } else {
        $addRequest = new AddPermaLinkRequest();
        $addRequest->setPid($newPid);
        $addRequest->setEntityId($oldPidRecord->entityId);
        $addRequest->setEntityName($oldPidRecord->entityName);
        $addResponse = $provider->put($addRequest);
        if (!$addResponse->success){
            if ($addResponse->message){
                $alert = $addResponse->message;
            } else {
                $alert = "Failed for an unknown reason!";
            }
        } else {
            $newUrl = $CurrentServer."ct/".$newPid;
            $alert = "Seems to have worked! Try it: <a href='".$newUrl."' target='_blank'>".$newUrl."</a>";
        }
    }
}
?>

<h3>Assign Custom Permalink</h3>
<p>Use this quick form to assign a new permalink for a merchant, organization, or individual. You may not edit the permalink once you create it so please be very careful! Also, this page has no validation on it. If the old permalink doesn't exist, nothing will happen.</p>
<p>You are advised to ensure that the permalink is valid by navigating to the page using this URL pattern: <?php echo $CurrentServer."ct/{pid}"; ?>, where "{pid}" refers to the Permalink ID provided by the user (merchant, org, or individual).</p>

<p class="alert">Permalinks are "permanent." That means the old permalink will keep on working!</p>
<ul>
    <li>Judiciously ensure they are relevant for the entity (merchant, org, or individual).</li>
    <li>Be aware that some names and phrases/slogans may have copyright issues. Google it first!</li>
    <li>You might want to recommend a "tiebreaker" or location so the first one to ask doesn't "win" and create discontent.</li>
</ul>
<form action="#MerchantManagement" method="POST">
    <fieldset style="display:inline-block;">
        <label>
            <div>Old Permalink ID:</div>
            <input type="text" name="old_pid" id="old-pid">
        </label>
        <label>
            <div>New Permalink ID:</div>
            <input type="text" name="new_pid" id="new-pid"> (e.g. maggie-moos-ice-cream-shirlington)

            <p class="alert"><?php echo $alert;?></p>

            <div class="alert">Permalink ID rules:</div>
            <ul>
                <li>Must be between 6-128 characters</li>
                <li>No spaces</li>
                <li>Only plain letters, numbers, and hyphens allowed. No other punctuation.</li>
                <li>Case insensitive and unique in the database across all Permalink ID values.</li>
            </ul>
        </label>
        <input type="submit" value="Submit">
    </fieldset>
</form>