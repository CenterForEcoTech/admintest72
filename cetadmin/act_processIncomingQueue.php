<?php
include_once("_config.php");

include_once("../assert_is_ajax.php");
include_once("../repository/mySqlProvider/Helper.php");

$processName = "manual-queue-processor";
//
// Verify that db connection configuration is set up
//
if (!isset($dbConfig)){
    echo "This script requires the database configuration file in siteconf.";
    die(); // no db config for source db.
}
if (!isset($dbConfigDisbursement)){
    echo "This script requires the database configuration for the disbursement database in siteconf.";
    die(); // no db config for the target db.
}

include_once($siteRoot."_setupDisbursementConnection.php");

$sp_query = "CALL sp_process_incoming_queue('".$processName."')";
$result = $mysqli_disb->query($sp_query);
?>