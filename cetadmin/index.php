<?php
include_once("_config.php");
$_SESSION['isStaff'] = false;
if (isAjaxRequest()){
    // front controller for ajax calls
    $action = $_POST["action"];
    switch ($action){
        case "login":
            include("_loginauth.php");
            break;
        case "logout":
            include("_logout.php");
            break;
        default:
            break;
    }
    die();// ajax calls echo then die
} else {
    include('_header.php');
}
?>
<h1>Admin Console</h1>

<?php if (isAdmin()){
    include_once("_adminConsoleTabs.php");
}else{
    include_once("_notloggedin.php");
}//end if isAdmin
include('_footer.php');

?>