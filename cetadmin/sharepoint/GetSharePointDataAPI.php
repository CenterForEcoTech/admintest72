<?php
error_reporting(1);
set_time_limit(600);
include_once("../_config.php");
include_once($siteRoot."functions.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ForecastingProvider.php");
$forecastingProvider = new ForecastingProvider($dataConn);
$forecastingVariables = $forecastingProvider->getVariables();

include_once($siteRoot.$adminFolder.'sharepoint/SharePointAPIFiles.php');


$currentRequest = new Request();
$parameters = $currentRequest->parameters;
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		if ($parameters["action"] == "ContractsToComplete"){
			include_once($dbProviderFolder."WarehouseProvider.php");
			$warehouseProvider = new WarehouseProvider($dataConn);
			$paginationResult = $warehouseProvider->get($criteria);
			$resultArray = $paginationResult->collection;
			foreach ($resultArray as $resultID=>$record){
				$NameParts = explode(" ",$record->description);
				$ShortName = $NameParts[0]." ".substr($NameParts[1],0,1);
				$WarehouseByName[$record->name] = $ShortName;
				$WarehouseByShortName[$ShortName] = $record->name;
				if ($record->displayOrderId){
					$ActiveWarehouseByName[]=$ShortName;
					$ActiveWarehouseByID[]=strtoupper($record->name);
				}
			}
			
			
			
			//get the expected monthly average contract amount from exec_forecast_averages_es_contracts
			$criteria = new stdClass();
			$criteria->mostRecent = true;
			$lastContractAverages = $forecastingProvider->lastAveragesContractsES($criteria);
			$lastContractorAverages = $forecastingProvider->lastAveragesContractorsES($criteria);
			//create expectations for averages of Crew HEA Visits
			if (count($lastContractAverages)){
				foreach  ($lastContractAverages as $crewAvg){
					$crew = $crewAvg->crew;
					if (in_array($crew,$ActiveWarehouseByID)){
						$siteVisits = $crewAvg->siteVisits;
						$daysToBill = $crewAvg->daysToBill;
						$signedContracts = $crewAvg->signed;
						$billedAmount = $crewAvg->billedAmount;
						$NATGRIDBilledCount = $crewAvg->NATGRIDBilledCount;
						$WMECOBilledCount = $crewAvg->WMECOBilledCount;
						$BGASBilledCount = $crewAvg->BGASBilledCount;
						$BGASBilledAmount = $crewAvg->BGASBilledAmount;
						$CrewForecaster[$crew]["Expected Site Visits"] = $siteVisits;		
						$ExpectedVisitsPerMonth = $ExpectedVisitsPerMonth+$siteVisits;		
						$ISMS["Expected Site Visits"] = 1;
						$CrewForecaster[$crew]["Days To Bill"] = $daysToBill;		
						$ISMS["Days To Bill"] = 1;
						$CrewForecaster[$crew]["ContractsSigned"] = $signedContracts;		
						$CrewForecaster[$crew]["BilledAmount"] = $billedAmount;		
						$CrewForecaster[$crew]["NATGRIDBilledCount"] = $NATGRIDBilledCount;		
						$CrewForecaster[$crew]["WMECOBilledCount"] = $WMECOBilledCount;		
						$CrewForecaster[$crew]["BGASBilledCount"] = $BGASBilledCount;		
						$CrewForecaster[$crew]["BGASBilledAmount"] = $BGASBilledAmount;		
						$BGASBilledAmountTotal = ($BGASBilledAmountTotal+$BGASBilledAmount); 
						$MonthlyWMECOContractCounts = ($MonthlyWMECOContractCounts+$WMECOBilledCount);
						$MonthlyNGRIDContractCounts = ($MonthlyNGRIDContractCounts+$NATGRIDBilledCount);
						$MonthlyContractCounts = ($MonthlyContractCounts+$NATGRIDBilledCount+$WMECOBilledCount);
						$extrapolated["NATGRID"] = bcmul($forecastingVariables->IICCompletionFeesNATGRID,$MonthlyNGRIDContractCounts);
						$extrapolated["WMECO"] = bcmul($forecastingVariables->IICContractManagementFeeWMECO,$MonthlyWMECOContractCounts);
						$extrapolated["BGAS"] = bcmul(($forecastingVariables->IICWeatherizationFeeBGAS/100),$BGASBilledAmountTotal);
					}
				}
			}
			if (count($lastContractorAverages)){
				foreach  ($lastContractorAverages as $contractorAvg){
					$contractor = $contractorAvg->contractor;
					if (strpos(" ".$contractor,"ZZZ")){$contractor = "ALL";}
					$ContractorForecaster[$contractor]["DaysToBill"] = $contractorAvg->daysToBill;		
					$ContractorForecaster[$contractor]["DaysToAccept"] = $contractorAvg->daysToAccept;		
					$ContractorForecaster[$contractor]["DaysToIntall"] = $contractorAvg->daysToInstall;
					$ContractorForecaster[$contractor]["DaysFromWorkOrdersSentToBill"] = ($contractorAvg->daysToAccept+$contractorAvg->daysToInstall+$contractorAvg->daysToBill);
				}
			}
			
			$StartDate = $parameters["startDate"];
			$EndDate = $parameters["endDate"];
			$includeExtrapolatedDataCurrentMonth = $parameters["includeExtrapolatedDataCurrentMonth"];
			$includeExtrapolatedDataFutureMonth = $parameters["includeExtrapolatedDataFutureMonth"];
			//get number of months in range
			$datetime1 = date_create(date("Y-m-01",strtotime($StartDate)));
			$datetime2 = date_create(date("Y-m-01",strtotime($EndDate)));
			$interval = date_diff($datetime1, $datetime2);
			$numMonths = $interval->m;
			$n = 0;
			$YearMonthArray = array();
			while ($n <=$numMonths){
				$YearMonthRange[] = date("Y-m-01",strtotime($StartDate." +".$n." months"));
				$n++;
			}
			
			foreach ($Views as $ID=>$GUI){
				$ArrayName = "SPRecord".$ID;
				$thisFileContents = null;
				$thisFileContents = file_get_contents($cachedFiledLocationName."-".$ID.".json");
				${$ArrayName} = json_decode($thisFileContents,true);

				foreach (${$ArrayName} as $SPData){
					$InstallDate = $SPData["install_x0020_date"];
					$Contractor = $SPData["contractor"];
					$Utility = $SPData["utility"];
					$WorkOrderSentDate = ($SPData["work_x0020_order_x0020_sent"] ? date("Y-m-d",strtotime($SPData["work_x0020_order_x0020_sent"])) : "0000-00-00");
					$ThisBillableDate = "0000-00-00";
					
					//Look for Billed in this month
					if ($SPData["utility_x0020_billing_x0020_date"]){
						$AirSealingBilledDateRaw = explode(" ",$SPData["utility_x0020_billing_x0020_date"]);
						$ThisDate = $AirSealingBilledDateRaw[0];
						if (MySQLDate($ThisDate) && (strtotime($ThisDate) >= strtotime($StartDate) && strtotime($ThisDate) <= strtotime($EndDate))){
							$ThisBillableDate = $ThisDate;
						}
					}
					if ($SPData["insulation_x0020_billing_x0020_d"]){
						$InsulationBilledDateRaw = explode(" ",$SPData["insulation_x0020_billing_x0020_d"]);
						$ThisDate = $InsulationBilledDateRaw[0];
						if (MySQLDate($ThisDate) && (strtotime($ThisDate) >= strtotime($StartDate) && strtotime($ThisDate) <= strtotime($EndDate))){
							$ThisBillableDate = $ThisDate;
						}
					}
					$Notes[] = "If either Air Sealing or Insulation has a billed date, record is considered Billed in this month";
					
					//look for installed but not yet billed but likely to be billed because we know how long it takes for a contractor to bill from installed date
					if ($InstallDate && !MySQLDate($ThisBillableDate)){
						$InstallDateParts = explode(" ",$InstallDate);
						//if not yet billed information use average
						$DaysToBill = ($ContractorForecaster[$Contractor]["DaysToBill"] > 0 ? $ContractorForecaster[$Contractor]["DaysToBill"] : $ContractorForecaster["ALL"]["DaysToBill"]);
						$LikelyBillDate = date("Y-m-d",strtotime($InstallDateParts[0]." +".$DaysToBill." days"));
						if ($LikelyBillDate && (strtotime($LikelyBillDate) >= strtotime($StartDate) && strtotime($LikelyBillDate) <= strtotime($EndDate))){
							$ThisBillableDate = $LikelyBillDate;
							$LikelyBillDates[$Contractor][]=$InstallDateParts[0]."+".$DaysToBill."=".$LikelyBillDate;
						}
					}
					$Notes[] = "If Install Date but not yet billed, the Contractor Average Days to Bill is added to the Install Date to predict the likely Bill Date to be included as revenue for that likely Bill Date month";
					
					if (MySQLDate($ThisBillableDate) && (strtotime($ThisBillableDate) >= strtotime($StartDate) && strtotime($ThisBillableDate) <= strtotime($EndDate))){
	//				if ($InstallDate && (strtotime($InstallDate) >= strtotime($StartDate) && strtotime($InstallDate) <= strtotime($EndDate))){
	//				if ($ThisBillableDate){
						$ContractCount++;
						$LastModified[] = strtotime($SPData["modified"]);
						$YearMonth = date("Y-m",strtotime($ThisBillableDate));
						$YearMonthFound[] = date("Y-m-01",strtotime($ThisBillableDate));
						$AirSealingSignedDate = "";
						if ($SPData["a_x002f_s_x0020_date_x0020_signe"]){
							$AirSealingSignedDateRaw = explode(" ",$SPData["a_x002f_s_x0020_date_x0020_signe"]);
							$AirSealingSignedDate = $AirSealingSignedDateRaw[0];
						}
						$AirSealingBilledDate = "";
						if ($SPData["utility_x0020_billing_x0020_date"]){
							$AirSealingBilledDateRaw = explode(" ",$SPData["utility_x0020_billing_x0020_date"]);
							$AirSealingBilledDate = $AirSealingBilledDateRaw[0];
						}
						$AirSealingOriginalContractAmount = ($SPData["a_x002f_s_x0020_original_x0020_c"] > 0 ? $SPData["a_x002f_s_x0020_original_x0020_c"] : 0);
						$AirSealingFinalContractAmount = ($SPData["final_x0020__x0024_"] > 0 ? $SPData["final_x0020__x0024_"] : 0 );
						
						$InsulationSignedDate="";
						if($SPData["insulation_x0020_date_x0020_sign"]){
							$InsulationSignedDateRaw = explode(" ",$SPData["insulation_x0020_date_x0020_sign"]);
							$InsulationSignedDate = $InsulationSignedDateRaw[0];
						}
						$InsulationBilledDate = "";
						if ($SPData["insulation_x0020_billing_x0020_d"]){
							$InsulationBilledDateRaw = explode(" ",$SPData["insulation_x0020_billing_x0020_d"]);
							$InsulationBilledDate = $InsulationBilledDateRaw[0];
						}
						$InsulationOriginalContractAmount = ($SPData["insulation_x0020_original_x0020_"] > 0 ? $SPData["insulation_x0020_original_x0020_"] : 0);
						$InsulationFinalContractAmount = ($SPData["insulation_x0020_final_x0020_con"] > 0 ? $SPData["insulation_x0020_final_x0020_con"] : 0);
						$ThisAmount = 0;
						switch($Utility){
							case "National Grid":
								$Utility = "NATGRID";
								//check or airsealing only
								$ContractCount = false;
								if (trim($AirSealingSignedDate) && !trim($InsulationSignedDate)){
									$ThisAmount = $forecastingVariables->IICFeesAirSealingOnlyNATGRID;
									$Formulas["Contracts"]["NATGRID Air-Sealing Only Fee"] = "$".money($ThisAmount)." x Number of NATGRID Air-Sealing Only Contracts Billed this month";
									$Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["Total"] = $Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["Total"]+$ThisAmount;
									if (trim($AirSealingBilledDate)){
										$ContractCount = true;
										$Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["Billed"] = $Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["Billed"]+$ThisAmount;
										$Contracts[$YearMonth]["Total"]["Billed"] = $Contracts[$YearMonth]["Total"]["Billed"]+$ThisAmount;
									}else{
										$Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["NotYetBilled"] = $Contracts[$YearMonth][$Utility]["IICFeesAirSealingOnly_NATGRID"]["NotYetBilled"]+$ThisAmount;
										$Contracts[$YearMonth]["Total"]["NotYetBilled"] = $Contracts[$YearMonth]["Total"]["NotYetBilled"]+$ThisAmount;
									}
								}else{
									if (trim($InsulationSignedDate)){
										$ThisAmount = $forecastingVariables->IICCompletionFeesNATGRID;
										$Formulas["Contracts"]["NATGRID Insulation Fee"]= "$".money($ThisAmount)." x Number of NATGRID Insulation Contracts Billed this month";
										$Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["Total"] = $Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["Total"]+$ThisAmount;
										if (trim($InsulationBilledDate)){
											$ContractCount = true;
											$Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["Billed"] = $Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["Billed"]+$ThisAmount;
											$Contracts[$YearMonth]["Total"]["Billed"] = $Contracts[$YearMonth]["Total"]["Billed"]+$ThisAmount;
										}else{
											$Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["NotYetBilled"] = $Contracts[$YearMonth][$Utility]["IICCompletionFees_NATGRID"]["NotYetBilled"]+$ThisAmount;
											$Contracts[$YearMonth]["Total"]["NotYetBilled"] = $Contracts[$YearMonth]["Total"]["NotYetBilled"]+$ThisAmount;
										}
									}
								}
								if ($ContractCount){$Contracts[$YearMonth][$Utility]["count"]++;}
								break;
							case "Berkshire Gas":
								$Utility = "BGAS";
								$AirSealingAmount = ($AirSealingFinalContractAmount > 0 ? $AirSealingFinalContractAmount : $AirSealingOriginalContractAmount);
								$InsulationAmount = ($InsulationFinalContractAmount > 0 ? $InsulationFinalContractAmount : $InsulationOriginalContractAmount);
								$Notes[] = "BGAS: If Final Contract Amount is left blank, Original Contract amount is used";
								
								$ContractAmount = ($AirSealingAmount+$InsulationAmount);
								$ThisAmount = ($ContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
								$Formulas["Contracts"]["BGAS Contract Revenue"] = $forecastingVariables->IICWeatherizationFeeBGAS."% of (Air-Sealing + Insulation Contracts Amount)";
								$Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Total"] = $Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Total"]+$ThisAmount;
								$ContractCount = false;
								if ($AirSealingFinalContractAmount > 0){
									$ContractCount = true;
									$Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Billed"] = $Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Billed"]+($AirSealingFinalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
									$Contracts[$YearMonth]["Total"]["Billed"] = $Contracts[$YearMonth]["Total"]["Billed"]+($AirSealingFinalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
								}else{
									$Contracts[$YearMonth][$Utility]["IIC_BGAS"]["NotYetBilled"] = $Contracts[$YearMonth][$Utility]["IIC_BGAS"]["NotYetBilled"]+($AirSealingOriginalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
									$Contracts[$YearMonth]["Total"]["NotYetBilled"] = $Contracts[$YearMonth]["Total"]["NotYetBilled"]+($AirSealingOriginalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
								}
								if ($InsulationFinalContractAmount > 0){
									$ContractCount = true;
									$Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Billed"] = $Contracts[$YearMonth][$Utility]["IIC_BGAS"]["Billed"]+($InsulationFinalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
									$Contracts[$YearMonth]["Total"]["Billed"] = $Contracts[$YearMonth]["Total"]["Billed"]+($InsulationFinalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
								}else{
									$Contracts[$YearMonth][$Utility]["IIC_BGAS"]["NotYetBilled"] = $Contracts[$YearMonth][$Utility]["IIC_BGAS"]["NotYetBilled"]+($InsulationOriginalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
									$Contracts[$YearMonth]["Total"]["NotYetBilled"] = $Contracts[$YearMonth]["Total"]["NotYetBilled"]+($InsulationOriginalContractAmount*($forecastingVariables->IICWeatherizationFeeBGAS/100));
								}
								if ($ContractCount){$Contracts[$YearMonth][$Utility]["count"]++;}

							break;
							case "WMECO":
								$Utility = "WMECO";
								$ContractCount = false;
								if (trim($AirSealingSignedDate) || trim($InsulationSignedDate)){
									$ThisAmount = $forecastingVariables->IICContractManagementFeeWMECO;
									$Formulas["Contracts"]["EverSource Contract Fee"] = "$".money($ThisAmount)." x (Number of EverSource Air-Sealing + Insulation Contracts)";
									$Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["Total"] = $Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["Total"]+$ThisAmount;
								}
								if (trim($AirSealingBilledDate) || trim($InsulationBilledDate)){
									$ContractCount = true;
									$Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["Billed"] = $Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["Billed"]+$ThisAmount;
									$Contracts[$YearMonth]["Total"]["Billed"] = $Contracts[$YearMonth]["Total"]["Billed"]+$ThisAmount;
								}else{
									$Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["NotYetBilled"] = $Contracts[$YearMonth][$Utility]["IICContractManagementFee_WMECO"]["NotYetBilled"]+$ThisAmount;
									$Contracts[$YearMonth]["Total"]["NotYetBilled"] = $Contracts[$YearMonth]["Total"]["NotYetBilled"]+$ThisAmount;
								}
								if ($ContractCount){$Contracts[$YearMonth][$Utility]["count"]++;}
							break;		
						}
						$Contracts[$YearMonth]["Total"]["Totals"] = $Contracts[$YearMonth]["Total"]["Totals"]+$ThisAmount;
						$ContractTotals = $ContractTotals+$ThisAmount;	
					}
				}//end foreach sprecord
				${$ArrayName} = null;
			}//end foreach view
			//empty $SPRecords to free up memory
			$SPRecords = array();
			ksort($Contracts);
			foreach ($Contracts as $YearMonth=>$Utilities){
				$asOfDate = strtotime(date("Y-m-6", strtotime($YearMonth)))*1000;
				//echo "$".money($TotalAmount)."<br>";
				$RawRevenue[]=array($asOfDate,(int)$Contracts[$YearMonth]["Total"]["Totals"]);
			}
			foreach ($YearMonthRange as $YearMonth){
				$extrapolateData = false;
				if (strtotime(date("Y-m-01",strtotime($StartDate))) == strtotime(date("Y-m-01",strtotime($YearMonth))) && $includeExtrapolatedDataCurrentMonth > 0){

					$extrapolateData = true;
				}
				if (strtotime(date("Y-m-01",strtotime($StartDate))) < strtotime(date("Y-m-01",strtotime($YearMonth))) && $includeExtrapolatedDataFutureMonth > 0){
					$extrapolateData = true;
				}
				
				if ($extrapolateData){
					$ThisYearTotal = 0;
					$asOfDate = strtotime(date("Y-m-6", strtotime($YearMonth)))*1000;
					$YearMonth = date("Y-m",strtotime($YearMonth));
					//echo "$".money($TotalAmount)."<br>";
					if ($Contracts[$YearMonth]["NATGRID"]["IICCompletionFees_NATGRID"]["Total"]<$extrapolated["NATGRID"]){
						$ThisYearTotal = (int)$extrapolated["NATGRID"];
						$Contracts[$YearMonth]["NATGRID"]["IICCompletionFees_NATGRID"]["Total"]=(int)$extrapolated["NATGRID"];
						$Contracts[$YearMonth]["NATGRID"]["IICCompletionFees_NATGRID"]["NotYetBilled"]=(int)$extrapolated["NATGRID"];
					}
					if ($Contracts[$YearMonth]["WMECO"]["IICContractManagementFee_WMECO"]["Total"]<$extrapolated["WMECO"]){
						$ThisYearTotal = $ThisYearTotal+(int)$extrapolated["WMECO"];
						$Contracts[$YearMonth]["WMECO"]["IICContractManagementFee_WMECO"]["Total"]=(int)$extrapolated["WMECO"];
						$Contracts[$YearMonth]["WMECO"]["IICContractManagementFee_WMECO"]["NotYetBilled"]=(int)$extrapolated["WMECO"];
					}
					if ($Contracts[$YearMonth]["BGAS"]["IIC_BGAS"]["Total"]<$extrapolated["BGAS"]){
						$ThisYearTotal = $ThisYearTotal+(int)$extrapolated["BGAS"];
						$Contracts[$YearMonth]["BGAS"]["IIC_BGAS"]["Total"]=(int)$extrapolated["BGAS"];
						$Contracts[$YearMonth]["BGAS"]["IIC_BGAS"]["NotYetBilled"]=(int)$extrapolated["BGAS"];
					}
					if ($Contracts[$YearMonth]["Total"]["Totals"] <  $ThisYearTotal){
						$Contracts[$YearMonth]["Total"]["Totals"] = (int)$ThisYearTotal;
//						$Contracts[$YearMonth]["Total"]["NotYetBilled"] = (int)$ThisYearTotal;
						$RawRevenue[$asOfDate] =(int)$ThisYearTotal;
					}
				}
			}
			$Notes = array_unique($Notes);
			$Formulas = array_unique($Formulas);
			$result["RawContracts"] = $RawRevenue;
			$result["Notes"] = $Notes;
			$result["Formulas"] = $Formulas;
			$result["Contracts"] = $Contracts;
			$result["ContractTotals"] = $ContractTotals;
			$result["AsofDate"] = date("m/d/Y g:i a",max($LastModified));
			$result["responseText"] = "As of ".date("m/d/Y g:i a",max($LastModified))." there are ".$ContractCount." Sites with contract(s) to be/have been installed between ".$StartDate." and ".$EndDate."<Br>".
				"The combined expected amount from Contract work will be: $".money($ContractTotals)."<br>".
				"Which breaks down to:<br>";
			foreach ($Contracts as $YearMonth=>$Utilities){
				$result["responseText"] .= $YearMonth."=$".money($Contracts[$YearMonth]["Total"]["Totals"])."<br>&nbsp;&nbsp;&nbsp;&nbsp; [Already Billed: $".money($Contracts[$YearMonth]["Total"]["Billed"])." Not Yet Billed: $".money($Contracts[$YearMonth]["Total"]["NotYetBilled"])."]<br>";
			}						
		}
		$results = $result;
		header("HTTP/1.0 201 Created");
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		echo "this is PUT";
        break;
}
//ksort($WorkOrderSentDates);
//print_pre($extrapolated);
output_json($results);
die();
?>