<?php
include_once("../_config.php");
include_once($dbProviderFolder."MemberProvider.php");
include_once($dbProviderFolder."SocialMediaHandleProvider.php");
include_once($dbProviderFolder."AdminEventProvider.php");
include_once("model/_merchantListModel.php");
include_once($siteRoot."_setupEventByLocationProvider.php");

function updateIndexedEvents($merchantId){
    global $mysqli,$repositoryApiFolder, $Config_IndexingURLPrivate, $Config_IndexingEventsIndex, $isDevMode, $indexedEventProvider, $eventByLocationProvider;
    if ($merchantId && isset($indexedEventProvider)){
        $criteria = new EventByLocationCriteria();
        $criteria->upcomingOnly = false;
        $criteria->merchantId = $merchantId;
        $criteria->size = 0; // get all of them

        /* @var $eventProvider EventProvider */
        $eventProvider = new AdminEventProvider($mysqli);
        $response = $eventProvider->get($criteria);

        $eventsToUpdate = $response->collection;

        if (count($eventsToUpdate)){
            $indexProviderFolder = $repositoryApiFolder."indexedSearchProvider/";
            include_once($indexProviderFolder."IndexedEventManager.php");
            $IndexingProviderURL = $Config_IndexingURLPrivate;
            $IndexName = $Config_IndexingEventsIndex;
            $indexedEventManager = new IndexedEventManager($IndexingProviderURL, $IndexName);

            $memberID = 0; // admin
            foreach ($eventsToUpdate as $eventToUpdate){
                /* @var $eventToUpdate EventRecord */
                if ($isDevMode){
                    getEventLogger()->log_action($eventToUpdate->id, 'Calling updateEvent on '.get_class($indexedEventManager), $memberID, getAdminId());
                }

                /* @var $eventManager EventManager */
                $indexedEventManager->updateEvent($eventToUpdate, $memberID, getAdminId());

            }
        }
    } else {
        trigger_error("ApiMerchantProfile.php updateIndexedEvents called without valid MerchantID", E_USER_ERROR);
    }
}

$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        if ($_GET['action'] == 'get_images'){
            include_once($dbProviderFolder."MerchantImageProvider.php");
            include_once("model/_merchantImagesModel.php");
            $merchantImageProvider = new MerchantImageProvider($mysqli);
            $criteria = MerchantImagesTableRow::getCriteria($_GET);
            $paginationResult = $merchantImageProvider->get($criteria);
            $resultArray = $paginationResult->collection;
            if ($_GET['id'] && count($resultArray)){
                $results = new MerchantImagesTableRow($resultArray[0]);
            } else {

                $results = array(
                    "iTotalRecords" => $paginationResult->totalRecords,
                    "iTotalDisplayRecords" => $paginationResult->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($resultArray as $record){
                    $results["aaData"][] = new MerchantImagesTableRow($record);
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
        } else if ($_GET["id"]){
            // TODO: retrieve merchant profile
        } else {
            $provider = new MerchantMemberProvider($mysqli);
            $criteria = MerchantListTableRow::getCriteria($_GET);
            $paginatedMerchants = $provider->get($criteria);
            $merchants = $paginatedMerchants->collection;

            if ($_GET["autocomplete"]){
                $results = $paginatedMerchants;
            } else if ($_GET["export"] == "csv"){
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=MerchantsReport.csv");
                header("Content-Transfer-Encoding: binary");
                $merchant_data = array();
                $colHeaders = array();
                foreach ($merchants as $merchant){
                    $merchant_data[] = MerchantExportRow::getRow($merchant);
                    if (!count($colHeaders)){
                        foreach($merchant_data[0] as $column => $value){
                            $colHeaders[] = $column;
                        }
                    }
                }

                $fileContents = exportCSV($merchant_data, $colHeaders, true);
                echo $fileContents;
                die();
            } else {
                $socialHandleProvider = new SocialMediaHandleProvider($mysqli);
                $socialCriteria = new SocialMediaHandleCriteria();

                $results = array(
                    "iTotalRecords" => $paginatedMerchants->totalRecords,
                    "iTotalDisplayRecords" => $paginatedMerchants->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($merchants as $merchant){
                    $model = new MerchantListTableRow($merchant);
                    $socialCriteria->merchantId = $merchant->id;
                    $socialCriteria->activeOnly = true;
                    $socialHandles = $socialHandleProvider->get($socialCriteria);
                    foreach ($socialHandles->collection as $handle){
                        $model->addSocialMediaHandle($handle);
                    }
                    $results["aaData"][] = $model;
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
        }
        break;
    case "POST":
        if ($_POST['action'] == 'set_sort_order'){
            $merchantImageId = $_POST['id'];
            $sortOrder = $_POST['sortOrder'];
            $merchantId = $_POST['merchantId'];
            if (is_numeric($merchantImageId) && $merchantImageId > 0){
                include_once($dbProviderFolder."MerchantImageProvider.php");
                $merchantImageProvider = new MerchantImageManager($mysqli, null);
                $criteria = new MerchantImageCriteria();
                $criteria->id = $merchantImageId;
                $criteria->merchantId = $merchantId;
                $criteria->size = 0; // for now, get all records
                $result = $merchantImageProvider->get($criteria);
                if (count($result->collection)){
                    $merchantImageRecord = $result->collection[0];
                    $merchantImageRecord->sortOrder = $sortOrder;
                    $response = $merchantImageProvider->setSortOrder($merchantImageRecord);
                    if (!$response->success && $response->error){
                        header("HTTP/1.0 400 Error updating sort order");
                        $results->message = $response->error;
                    } else {
                        $results->success = true;
                    }
                } else {
                    header("HTTP/1.0 400 No Record Found");
                    $results->success = false;
                }
            } else {
                header("HTTP/1.0 400 Bad Request");
            }
        } else if ($_POST['action'] == 'social_media') {
            $merchantId = $_POST['merchantId'];
            $fbHandle = SocialMediaHandle::createFacebookHandle($_POST['facebookPageUrl']);
            $fbHandle->setMerchantId($merchantId);
            $twitterHandle = SocialMediaHandle::createTwitterHandle($_POST['twitterHandle']);
            $twitterHandle->setMerchantId($merchantId);
            $results = array();

            $socialProvider = new SocialMediaHandleProvider($mysqli);
            $savedRecord = $socialProvider->setSingleHandle($fbHandle);
            if ($savedRecord){
                $results[] = $savedRecord;
            }
            $savedRecord = $socialProvider->setSingleHandle($twitterHandle);
            if ($savedRecord){
                $results[] = $savedRecord;
            }

        } else if ($_POST["id"] && isset($_POST["image_id"])){
            $merchantId = $_POST["id"];
            $memberProvider = new MerchantMemberProvider($mysqli);
            $updateResponse = $memberProvider->setImageId($merchantId, $_POST["image_id"]);
            if ($updateResponse->success && $updateResponse->affectedRows > 0){
                $results->success = true;
                updateIndexedEvents($merchantId);

                $merchantCriteria = new MerchantProfilesCriteria();
                $merchantCriteria->id = $merchantId;
                $queryResult = $memberProvider->get($merchantCriteria);
                if (count($queryResult->collection)){
                    /* @var $record Venue */
                    $record = $queryResult->collection[0];
                    // now, set a property for the image url for serialization
                    $record->logoFullPath = $record->getLogoImageUrl($CurrentServer, $siteRoot);
                    $results->record = $record;
                }
            } else {
                $results->success = false;
                $results->message = "failed to update image";
            }
        } else if ($_POST["id"] && isset($_POST["billing_code_id"])){
            $merchantId = $_POST["id"];
            $memberProvider = new MerchantMemberProvider($mysqli);
            $updateResponse = $memberProvider->setBillingCodeId($merchantId, $_POST["billing_code_id"], 0, getAdminId());
            if ($updateResponse->success && $updateResponse->affectedRows > 0){
                $results->success = true;

                $merchantCriteria = new PaginationCriteria();
                $merchantCriteria->id = $merchantId;
                $queryResult = $memberProvider->get($merchantCriteria);
                if (count($queryResult->collection)){
                    /* @var $record Venue */
                    $record = $queryResult->collection[0];

                    // not assign the new model object to dynamically update referral code
                    $results->record = new MerchantListTableRow($record);
                }
            } else if ($updateResponse->success && !$updateResponse->affectedRows){
                $results->success = true;
            } else{
                $results->success = false;
                $results->message = "failed to update billing code";
            }
        } else if ($_POST['edit'] && isset($_POST['id'])){
            $memberProvider = new MerchantMemberProvider($mysqli);
            $merchantId = $_POST['id'];
            $companyRecord = $memberProvider->getPublicProfile($_POST['id']);
            $memberRecord = new MerchantMemberRecord();
            $memberRecord->id = $companyRecord->memberId;

            $companyRecord->name = htmlspecialchars(htmlspecialchars_decode($_POST["company_name"]));
            $companyRecord->address = htmlspecialchars(htmlspecialchars_decode($_POST["company_address"]));
            $companyRecord->city = htmlspecialchars(htmlspecialchars_decode($_POST["company_city"]));
            $companyRecord->state = $_POST["company_state"];
            $companyRecord->postalCode = $_POST["company_zip"];
            $companyRecord->country = $_POST['company_country'];
            $companyRecord->phone = $_POST["company_phone"];
            $companyRecord->website = $_POST["company_website"];
            $companyRecord->descriptionText = $_POST["company_description"];
            $companyRecord->isDescriptionPublic = strtolower($_POST["company_descriptionshow"]) == "yes";
            $companyRecord->donationshow = $_POST["company_donationshow"];
            $companyRecord->eventBriteVenueId = $_POST['company_eventbritevenueid'];
            $companyRecord->customTransactionEmailText = $_POST['company_customTransactionEmailText'];
            $companyRecord->neighborhood = $_POST["company_neighborhood"];
            $companyRecord->keywords = $_POST['company_keywords'];

            $companyRecord->lat = $_POST['company_lat'];
            $companyRecord->long = $_POST['company_long'];

            //recreate lat long on address update
            if ($_POST['recalc_geo']){
                $GeoCode = GetGeoCode(chkstring($_POST["company_address"]).",".chkstring($_POST["company_city"]).",".chkstring($_POST["company_state"])." ".chkstring($_POST["company_zip"]));
                $GeoCodeLatLong = explode(",",$GeoCode);
                $getLat = $GeoCodeLatLong[0];
                $getLong = $GeoCodeLatLong[1];
                $companyRecord->lat = $getLat;
                $companyRecord->long = $getLong;
            }

            /* @var $companyContactRecord ContactRecord */
            $companyContactRecord = $companyRecord->primaryContact;
            if (!$memberRecord->id){
                $companyContactRecord->firstname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactfirstname"]));
                $companyContactRecord->lastname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactlastname"]));
                $companyContactRecord->title = htmlspecialchars(htmlspecialchars_decode($_POST["company_contacttitle"]));
                $companyContactRecord->setPhone($_POST["company_contactphone"]);
                $companyContactRecord->email = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
                $companyContactRecord->emailConfirm = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            }

            // ========= server-side validation ===========
            // Rule: Required fields:
            //  company name, address, city, state, zip
            $requiredFields = array(
                "company_name",
                "company_address",
                "company_city",
                "company_state",
                "company_zip"
            );
            $validationMessages = array();
            $results->validationMessages = $validationMessages;
            foreach ($requiredFields as $fieldName){
                if ($_POST[$fieldName]){
                    // good
                } else {
                    $validationMessages[] = (object)array(
                        "name" => $fieldName,
                        "message" => "This is a required field"
                    );
                }
            }
            if (count($validationMessages) == 0){
                //Update Members Profile
                $results->success = $memberProvider->updateMerchantAsMember($companyRecord, $memberRecord);
                if ($results->success){
                    $results->id = $companyRecord->id;
                    $memberProvider->updateCustomTransactionEmailText($companyRecord->id, $_POST['company_customTransactionEmailText']);

                    updateIndexedEvents($merchantId);
                } else {
                    $results->message = "Update failed";
                }
            }
        } else if ($_POST['add'] && !$_POST['id']){
            $memberProvider = new MerchantMemberProvider($mysqli);

            $companyRecord = new MerchantRecord();
            $companyRecord->name = htmlspecialchars(htmlspecialchars_decode($_POST["company_name"]));
            $companyRecord->address = htmlspecialchars(htmlspecialchars_decode($_POST["company_address"]));
            $companyRecord->city = htmlspecialchars(htmlspecialchars_decode($_POST["company_city"]));
            $companyRecord->state = $_POST["company_state"];
            $companyRecord->postalCode = $_POST["company_zip"];
            $companyRecord->country = $_POST['company_country'];
            $companyRecord->phone = $_POST["company_phone"];
            $companyRecord->website = $_POST["company_website"];
            $companyRecord->descriptionText = $_POST["company_description"];
            $companyRecord->isDescriptionPublic = strtolower($_POST["company_descriptionshow"]) == "yes";
            $companyRecord->donationshow = $_POST["company_donationshow"];
            $companyRecord->eventBriteVenueId = $_POST['company_eventbritevenueid'];
            $companyRecord->customTransactionEmailText = $_POST['company_customTransactionEmailText'];
            $companyRecord->cityGridIdName = $_POST["company_citygridname"];
            $companyRecord->cityGridProfileId = $_POST["company_citygridid"];
            $companyRecord->neighborhood = $_POST["company_neighborhood"];
            $companyRecord->keywords = $_POST['company_keywords'];
            $companyRecord->officialname = $_POST["company_officialname"];

            $companyRecord->lat = $_POST['company_lat'];
            $companyRecord->long = $_POST['company_long'];

            //recreate lat long on address update
            if ($_POST['recalc_geo']){
                $GeoCode = GetGeoCode(chkstring($_POST["company_address"]).",".chkstring($_POST["company_city"]).",".chkstring($_POST["company_state"])." ".chkstring($_POST["company_zip"]));
                $GeoCodeLatLong = explode(",",$GeoCode);
                $getLat = $GeoCodeLatLong[0];
                $getLong = $GeoCodeLatLong[1];
                $companyRecord->lat = $getLat;
                $companyRecord->long = $getLong;
            }

            $companyContactRecord = new ContactRecord();
            $companyContactRecord->firstname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactfirstname"]));
            $companyContactRecord->lastname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactlastname"]));
            $companyContactRecord->title = htmlspecialchars(htmlspecialchars_decode($_POST["company_contacttitle"]));
            $companyContactRecord->setPhone($_POST["company_contactphone"]);
            $companyContactRecord->email = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            $companyContactRecord->emailConfirm = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            $companyRecord->primaryContact = $companyContactRecord;

            // ========= server-side validation ===========
            // Rule: Required fields:
            //  company name, address, city, state, zip
            $requiredFields = array(
                "company_name",
                "company_address",
                "company_city",
                "company_state",
                "company_zip"
            );
            $results->validationMessages = array();
            foreach ($requiredFields as $fieldName){
                if ($_POST[$fieldName]){
                    // good
                } else {
                    $results->validationMessages[] = (object)array(
                        "name" => $fieldName,
                        "message" => "This is a required field"
                    );
                }
            }
            if (count($results->validationMessages) == 0){
                $existingPermalink = $memberProvider->getRegisteredMerchantPermalink($companyRecord);
                if ($existingPermalink){
                    $results->message = "This Merchant already exists";
                    $results->id = $existingPermalink->entityId;
                }
            }
            if (count($results->validationMessages) == 0 && !$results->id){
                //Add Members Profile
                $addResponse = $memberProvider->addMerchant($companyRecord);
                if ($addResponse->success){
                    $results->id = $addResponse->insertedId;
                    $memberProvider->updateCustomTransactionEmailText($results->id, $_POST['company_customTransactionEmailText']);
                } else {
                    $results->message = $addResponse->error;
                }
            }
        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
if ($results->message || count($results->validationMessages)){
    header("HTTP/1.0 409 Conflict");
}
output_json($results);
die();