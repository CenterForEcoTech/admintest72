<?php
include_once("../_config.php");
$pageTitle = "Admin - Merchants";
?>
<html>
<?php include("../_header.php");
?>
<div class="container">
    <h1>Merchants</h1>
    <?php

    $nav = isset($_GET["nav"]) ? $_GET["nav"] : "error-logs";
    include_once("_merchantsMenu.php");
    if ($nav){
        switch ($nav){
            case "profile-edit":
                include("model/_editProfileModel.php");
                include("views/_editProfile.php");
                break;
            case "profiles":
            default:
                include("views/_profiles.php");
                break;
        }
    }
    ?>
</div> <!-- end container -->
<?php
include("../_footer.php");
?>