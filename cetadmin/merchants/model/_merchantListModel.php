<?php
class MerchantListTableRow{
    public $DT_RowId;
    public $idLink;
    public $businessDisplay;
    public $billingCodeDisplay;
    public $logoPath;
    public $memberId;
    public $numTransactions;
    public $socialMediaHandles = array();

    public function __construct(MerchantRecord $record){
        global $CurrentServer, $siteRoot;
        $linkHref = "?nav=profile-edit&id=".$record->id;
        $this->DT_RowId = "merchant-".$record->id;
        $this->idLink = "<a href='".$linkHref."'>".$record->id."</a>";
        $this->businessDisplay = $record->name;
        $this->billingCodeDisplay = self::getBillingCodeDisplay($record);
        $this->memberId = $record->memberId;
        $this->numTransactions = $record->numTransactions ? $record->numTransactions : "";

            // company
        $contactInfoHtml = $record->name;
        if ($record->address){
            $contactInfoHtml .= "<br>".$record->address;
        }
        if ($record->city){
            $contactInfoHtml .= "<br>".$record->city.", ".$record->state." ".$record->postalCode;
        }
        if ($record->email){
            $contactInfoHtml .= "<br>".$record->primaryContact->email;
        }
        if ($record->phone){
            $contactInfoHtml .= "<br>".$record->phone;
        }
        if ($record->referralCodeText){
            $contactInfoHtml .= "<br> Referral text: ".$record->referralCodeText;
        }
        if ($record->pid){
            $contactInfoHtml .= "<br> PID: ".$record->pid;
        }
        $this->businessDisplay = $contactInfoHtml;
        $imageUrl = $record->getLogoImageUrl($CurrentServer, $siteRoot);
        if ($imageUrl){
            $this->logoPath = "<img src='".$imageUrl."' class='logo-image'>";
        }
    }

    private static function getBillingCodeDisplay($record){
        $text = "";
        if ($record->billingCode){
            $text .= $record->billingCode."<br>";
            if ($record->billingType == 'flatfee'){
                $text .= "($".$record->billingRate." per month)";
            } else if ($record->billingType == 'annual') {
                $text .= "($".$record->billingRate." per year)";
            } else {
                $text .= "(".$record->billingRate."% of sales)";
            }
            $validDateText = self::getBillingValidString($record);
            if ($validDateText){
                $text .= "<br>".$validDateText;
            }
            $text = "<span class='billing-desc' data-billing-id='".$record->billingCodeId."'>".$text."</span>";
        }
        return $text;
    }

    protected static function getBillingValidString($record){
        $text = "";
        if ($record->validTo && strtotime($record->validTo) < time()){
            $text .= "Expired on ".$record->validTo;
        } else if ($record->validFrom && time() < strtotime($record->validFrom)){
            $text .= "Begins on ".$record->validFrom;
        } else if ($record->validFrom && $record->validTo){
            $text .= "Valid from ".$record->validFrom." to ".$record->validTo;
        } else if ($record->validFrom){
            $text .= "Valid since ".$record->validFrom;
        } else if ($record->validTo){
            $text .= "Expires on ".$record->validTo;
        }
        return $text;
    }

    public function addSocialMediaHandle(SocialMediaHandle $handle){
        $this->socialMediaHandles[] = $handle;
    }

    private static $columnMapping = array(
        0 => "idLink",
        1 => "businessDisplay",
        2 => "numTransactions",
        3 => "billingCodeDisplay",
        4 => "logoPath",
        5 => "memberId",
        6 => "socialMediaHandles"
    );

    public static function getCriteria($datatablePost){
        $criteria = new MerchantProfilesCriteria();
        if ($datatablePost['state']){
            $criteria->state = $datatablePost['state'];
        }
        if ($datatablePost['keyword']){
            $criteria->keyword = $datatablePost['keyword'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] !== '' && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "memberId":
                        $criteria->memberId = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class MerchantExportRow{
    public static function getRow(MerchantRecord $record){
        return array(
            "id" => $record->id,
            "firstName" => $record->primaryContact->firstName,
            "lastName" => $record->primaryContact->lastName,
            "phone" => $record->phone,
            "email" => $record->primaryContact->email,
            "timestamp" => $record->timestamp,
            "memberId" => $record->memberId,
            "merchantId" => $record->id,
            "organizationId" => "",
            "companyName" => $record->name,
            "address" => $record->address,
            "city" => $record->city,
            "state" => $record->state,
            "zip" => $record->postalCode,
            "orgEIN" => "",
            "referralCode" => $record->referralCodeText,
            "referredByMemberId" => $record->referredByMemberId,
            "referralCodeText" => $record->referralCodeText,
            "pid" => $record->pid
        );
    }
}