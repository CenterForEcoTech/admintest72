<?php
class MerchantImagesTableRow{
    public $DT_RowId;
    public $imagePath;
    public $caption;
    public $status;
    public $sortOrder;

    public function __construct(MerchantImageRecord $record){
        global $CurrentServer, $siteRoot;
        $this->DT_RowId = "merchant-image-".$record->id;
        $this->status = $record->status;
        $this->caption = $record->getCaptionDisplay();
        $this->sortOrder = $record->sortOrder;

        $imageUrl = $record->getImageUrl($CurrentServer, $siteRoot);
        if ($imageUrl){
            $this->imagePath = "<img src='".$imageUrl."' class='logo-image'>";
        }
    }

    private static $columnMapping = array(
        0 => "imagePath",
        1 => "caption",
        2 => "status",
        3 => "sortOrder"
    );

    public static function getCriteria($datatablePost){
        $criteria = new MerchantImageCriteria();
        if ($datatablePost['merchantId']){
            $criteria->merchantId = $datatablePost['merchantId'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] !== '' && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}