<?php
include_once($dbProviderFolder."MemberProvider.php");
if ($_GET['id'] && is_numeric($_GET['id'])){
    $provider = new MerchantMemberProvider($mysqli);
    $merchantRecord = $provider->getPublicProfile($_GET['id']);
} else {
    $contactRecord = new ContactRecord();
    $merchantRecord = new MerchantRecord();
    $merchantRecord->primaryContact = $contactRecord;
}