<?php
/**
 * This is the view for just the widget. No titles. No Ajax.
 *
 * This view depends upon a javascript hook that does the heavy lifting.
 */
?>
<link href='http<?php echo $secureServer;?>://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $CurrentServer;?>css/select2.css">
<style type="text/css">
    .merchant-search-widget{clear:both;position: relative}
    .merchant-search-widget #merchant-state-filter.hidden {display:none;}
    .merchant-search-widget .loading-spinner{position:absolute;background:transparent url(../../images/loadingSearch.gif) no-repeat 0 0;width:30px;height:30px;}
    .ui-menu .ui-menu-item{
        border-top: 1px dotted #dddddd;
    }
    .ui-menu .ui-menu-item a{
        font-size:14px;
        padding: 6px 5px;
    }
    .ui-menu .ui-menu-item a span {
        margin-left:3px;
    }
    .ui-menu .ui-menu-item a span.value{
        font-weight:bold;
        margin-left:0px;
        font-size:15px;
    }
    .ui-menu .ui-menu-item a span.n-case{
        text-transform: capitalize;
    }
    .ui-menu .ui-menu-item a span.is-reg{
        padding-left:20px;
        background: transparent url(/images/star.png) no-repeat left top;
    }
    .ui-menu .ui-menu-item a.ui-state-focus span{
        font-weight:normal;
    }
    .ui-menu .ui-menu-item a.ui-state-focus span.value{
        font-weight:bold;
    }
    .ui-menu .btn{
        float:right;
        cursor:pointer;
        font-size:13px;
        margin:3px;
    }
    .select2-results li{
        line-height: 20px;
        margin-bottom:0px;
        font-size:15px;
    }
    .merchant-search-widget .filter-go{
        margin:0;
        width:45px;
        padding:0;
    }
    .merchant-search-widget .filter-go .round-green-button,
    .merchant-search-widget .filter-go .round-gray-button{
        max-width:45px;
        position:absolute;
        top:6px;
        right:-7px;
    }
    #cboxWrapper .merchant-search-widget .filter-go .round-green-button{
        float:none;
        position:absolute;
        top:0px;
        right:0px;
    }
    .merchant-search-widget label{
        display:inline;
    }
    .round-green-button {
        height: 44px;
        width: 45px;
        background: transparent url(../../images/or_graphic.png) no-repeat left top;
        position: relative;
    }
    .round-green-button span, .round-gray-button span {
        position: absolute;
        overflow: hidden;
        top: 9px;
        left: 14px;
        color: #ffffff;
        font: bold 22px 'Yanone Kaffeesatz', sans-serif;
    }
    .merchant-search-widget label{
        margin-top:3px;
    }
    .location-filter {
        position:relative;
        width:290px;
        display:inline-block;
    }
    #cboxWrapper .location-filter{
        margin-top:3px;
    }
    @media only screen and (max-width: 767px){
        .location-filter{
            width: 315px;
        }
    }
    @media only screen and (max-width: 479px) {
        .merchant-search-widget #merchant-state-filter{
            max-width:200px;
        }
        .location-filter{
            width:265px;
        }
    }
</style>
<div class="merchant-search-widget">
    <label class="columns alpha omega">
        <span class="highlight">
            <input id="merchant-search-box" type="text" class="auto-watermarked" title="Name">
        </span>
        <span class="loading-spinner hidden"></span>
    </label>
    <div class="columns location-filter hidden">
        <label class="columns alpha omega">
        <span id="merchant-state-filter" class="highlight">
            <select id="merchant-state-select" class="input-style">
                <option value="">All States</option>
                <?php
                $stateList = StateNameAndAbbrConverter::getStateList();
                foreach ($stateList as $value => $display) { ?>
                    <option value="<?php echo $value;?>"><?php echo $display;?></option>
                <?php } ?>
            </select>
        </span>
        </label>
        <label class="filter-go one column">
            <span class="round-green-button">
                <span>Go</span>
            </span>
        </label>
    </div>

    <input type="submit" value="Search" class="hidden" />
</div>
<script type="text/javascript" src="<?php echo $CurrentServer;?>js/select2.js"></script>
<script type="text/javascript">
var CETDASH = CETDASH || {};
CETDASH.MerchantSearchWidget = CETDASH.MerchantSearchWidget || {};
CETDASH._MerchantSearchWidget = CETDASH._MerchantSearchWidget || {};
CETDASH._MerchantSearchWidget.defaults = {
    showState: false,
    useSelect2: true,
    dropOnFocus: false,
    sourceUrl : "",
    sourceType : "POST",
    minLength: 3,
    pageLength: 5,
    delay: 300,
    onSearch: function(event,ui){},
    onSelect: function( event, ui ) {
        var searchInput = $(this);
        searchInput.val(ui.item.value);
        return false;
    }
};
CETDASH._MerchantSearchWidget.options = $.extend(CETDASH._MerchantSearchWidget.defaults, CETDASH.MerchantSearchWidget);
$(function(){
    var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints,
        searchContainer = $(".merchant-search-widget"),
        insideColorbox = searchContainer.closest("#cboxContent").length > 0,
        spinner = searchContainer.find(".loading-spinner"),
        searchInput = $( "#merchant-search-box"),
        stateInput = searchContainer.find("#merchant-state-filter select#merchant-state-select"),
        filterButton = searchContainer.find(".filter-go"),
        autocompleteObject,
        lastStart = 0,
        nextStart = 0,
        currentPageLength = 10,
        hasMore = false,
        resetPagination = function(){
            lastStart = 0;
            nextStart = 0;
            currentPageLength = CETDASH._MerchantSearchWidget.options.pageLength;
            hasMore = false;
        },
        showPrev = function(){
            return lastStart > 0;
        },
        showNext = function(){
            return hasMore;
        },
        currentTerm = "",
        currentResponseCallback,
        getMoreData = function(reverse){
            var currentStart = (reverse) ? lastStart - currentPageLength : nextStart;
            spinner.show();
            $.ajax({
                type:CETDASH._MerchantSearchWidget.options.sourceType,
                url: CETDASH._MerchantSearchWidget.options.sourceUrl,
                dataType: "json",
                data: {
                    autocomplete: 1,
                    state: stateInput.val(),
                    keyword : currentTerm,
                    start : currentStart,
                    size: currentPageLength
                },
                success: function(data) {
                    var items = $.map( data.collection, function( item ) {
                        return {
                            value: item.name,
                            record: item
                        }
                    });
                    if (reverse){
                        nextStart = currentStart;
                        lastStart -= currentPageLength;
                    } else {
                        lastStart = currentStart;
                        nextStart += currentPageLength;
                    }
                    if (data.hasMore){
                        hasMore = true;
                    } else {
                        hasMore = false;
                    }
                    currentResponseCallback(items);
                    spinner.hide();
                }
            });
        },
        renderMenu = function( ul, items ) {
            var that = this,
                paginationELement;
            if (showPrev() || showNext()){
                paginationELement = $("<li class='nav'></li>");
                if (showNext()){
                    paginationELement.append("<span class='btn next ui-icon ui-icon-circle-triangle-e' title='next'>next</span>");
                }
                if (showPrev()){
                    paginationELement.append("<span class='btn prev ui-icon ui-icon-circle-triangle-w' title='previous'>prev</span>");
                }
                paginationELement.appendTo(ul);
            }
            $.each( items, function( index, item ) {
                // jquery.ui.autocomplete.js version 1.9 calls _renderDataItem; we need 1.8 compatibility
                that._renderItem( ul, item );
            });
            if (showPrev() || showNext()){
                paginationELement = $("<li class='nav'></li>");
                if (showNext()){
                    paginationELement.append("<span class='btn next ui-icon ui-icon-circle-triangle-e' title='next'>next</span>");
                }
                if (showPrev()){
                    paginationELement.append("<span class='btn prev ui-icon ui-icon-circle-triangle-w' title='previous'>prev</span>");
                }
                paginationELement.appendTo(ul);
            }
        },
        renderItem = function( ul, item ) {
            var record = item.record,
                itemHtmlArray = [],
                appendRenderedName = function(name, merchantId){
                    var needsCaseTransform = name !== name.toUpperCase(),
                        cssClass = "value" + (needsCaseTransform ? " n-case" : "" ),
                        hoverTitle = "";
                    appendRenderedLine(name, cssClass, hoverTitle);
                },
                appendRenderedLine = function(value, cssClass, hoverTitle){
                    var valueToPush = value,
                        titleValue = hoverTitle ? hoverTitle : "";
                    if (value){
                        if (cssClass){
                            valueToPush = "<span class='" + cssClass + "' title='"+ titleValue +"'>" + value + "</span>";
                        }
                        itemHtmlArray.push(valueToPush);
                    }
                };
            appendRenderedName(record.name, record.id);
            appendRenderedLine(record.address.toLowerCase(), "n-case");
            appendRenderedLine(record.city.toLowerCase() + ", " + record.state + " " + record.postalCode, "n-case");
            // jquery.ui.autocomplete 1.9 calls _renderDataItem which appends the item to the element; but due to 1.8 compatibility, we append the item here so that on select, the value is actually selected
            return $( "<li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + itemHtmlArray.join("<br>") + "</a>" )
                .appendTo( ul );
        };

    if (CETDASH._MerchantSearchWidget.options.showState){
        $(".location-filter").css("display", "inline-block");
        if (CETDASH._MerchantSearchWidget.options.useSelect2 && !supportsTouch && !insideColorbox){
            $("#merchant-state-select").select2({
                width: "element",
                dropdownCssClass: "input-style",
                placeholderOption: "first",
                allowClear: true,
                matcher: function(term, text, option) {
                    var optValue = option.attr("value").toUpperCase(),
                        searchTerm = term.toUpperCase();
                    return text.toUpperCase().indexOf(searchTerm) >= 0 || optValue.indexOf(searchTerm) >= 0;
                }
            });
        }
    }

    searchInput.autocomplete({
        minLength: CETDASH._MerchantSearchWidget.options.minLength,
        source: function(request,response){
            resetPagination();
            currentTerm = request.term;
            currentResponseCallback = response;
            getMoreData();
        },
        delay: CETDASH._MerchantSearchWidget.options.delay,
        //position: {
        //    collision: "fit flip"
        //},
        open: function(event, ui){
            $("ul.ui-autocomplete div.nav").each(function(){
                var navElement = $(this),
                    buttons = navElement.find(".btn");

                navElement.removeClass("ui-menu-item");
                buttons.removeClass("ui-corner-all");
            });
            return false;
        },
        search: CETDASH._MerchantSearchWidget.options.onSearch,
        select: CETDASH._MerchantSearchWidget.options.onSelect
    }).focus(function(){
            var self = $(this),
                searchTerm = self.val(),
                widget = self.data("autocomplete");

            if (searchTerm !== currentTerm){
                currentTerm = searchTerm;
            }
            if (CETDASH._MerchantSearchWidget.options.dropOnFocus && currentTerm){
                widget.search(currentTerm);
            }
        });

    autocompleteObject = searchInput.data("ui-autocomplete");
    if (autocompleteObject === undefined){
        autocompleteObject = searchInput.data("autocomplete");
    }

    autocompleteObject._renderMenu = renderMenu;
    autocompleteObject._renderItem = renderItem;

    $("body").on("click", ".ui-autocomplete .nav .btn", function(e){
        var btn = $(this);
        if (btn.hasClass("prev")){
            getMoreData(true);
        } else {
            getMoreData();
        }
    });

    filterButton.on("click", function(e){
        e.preventDefault();
        if (currentTerm){
            searchInput.focus();
            searchInput.autocomplete("search", currentTerm);
        }
    });
});
</script>