<?php
/* @var $merchantRecord MerchantRecord */
if (isset($merchantRecord) && $merchantRecord->id > 0){
    include_once($repositoryApiFolder."DataContracts.php");
    ?>
    <style type="text/css">
        td.editable:hover {cursor:pointer;outline:groove;}
    </style>
    <h3>Merchant Images</h3>
    <p>
        <?php if (getMerchantId() == $merchantRecord->id) {?>
        <a href="<?php echo $CurrentServer;?>Merchant/ManageImages/new" class="button-link" title="opens in a new window" target="merchant-image-admin">+ Add Image</a>
        <span class="help">Click on a row to edit. Click on the sort order to change sort order value.</span>
        <?php } else { ?>
            <input type="password" id="impersonate-pwd" class="auto-watermarked" title="impersonation password">
            <a href="#accessDenied" data-pwd-field="#impersonate-pwd" class="impersonate-merchant button-link do-not-navigate" target="merchant-image-admin">Impersonate Merchant</a>
            <span class="help">You must impersonate this merchant before adding or editing any images. Click on the sort order to change sort order value.</span>
        <?php } ?>
    </p>
    <table id="merchant-image-table">
        <thead>
        <tr>
            <th>Image</th>
            <th>Caption</th>
            <th>Status</th>
            <th>Sort Order</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script type="text/javascript">
        var CETDASH = CETDASH || {};
        CETDASH.onChildClosed = function(){
            $("#merchant-image-table").dataTable().fnDraw(false);
        };
        $(function(){
            var imagesDataTable = $("#merchant-image-table"),
                apiUrl = "<?php echo $CurrentServer.$adminFolder;?>merchants/ApiMerchantProfile.php",
                oTable = imagesDataTable.dataTable({
                    "bJQueryUI": true,
                    "bServerSide":true,
                    "sAjaxSource": apiUrl,
                    "aaSorting": [ ],
                    "aoColumns" :[
                        { "mData": "imagePath" },
                        { "mData": "caption" },
                        { "mData": "status" },
                        { "mData": "sortOrder", "sWidth": "20px", "sClass": "editable"}
                    ],
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "merchantId", "value": "<?php echo $merchantRecord->id;?>" } );
                        aoData.push( { "name": "action", "value": "get_images" } );
                    },
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        } );
                    }
                });

            imagesDataTable.on("click", "td.editable", function(e){
                var cell = $(this),
                    cellData = cell.html(),
                    rowId = cell.closest("tr").attr("id"),
                    dataId = rowId.replace("merchant-image-", ""),
                    input = $("<input type='text' class='table-cell'>");

                input.click(function(e){
                    e.stopPropagation();
                });

                input.blur(function(e){
                    var dataText = input.val();
                    if (dataText !== cellData){
                        if (!$.isNumeric(dataText)){
                            alert("Sort Order must be a numeric value!");
                            input.focus();
                            input.highlight();
                        } else {
                            $.ajax({
                                url: apiUrl,
                                type: "POST",
                                data: {
                                    id: dataId,
                                    merchantId: <?php echo $merchantRecord->id;?>,
                                    sortOrder: dataText,
                                    action: 'set_sort_order'
                                },
                                success: function(data){
                                    cell.html(dataText);
                                    oTable.fnDraw(true);
                                },
                                error: function(data){
                                    cell.html(cellData);
                                }
                            });
                        }
                    }
                });
                input.val(cellData);
                cell.html(input);
                input.focus();
                input.select();
            });

            <?php if (getMerchantId() == $merchantRecord->id) {?>
            imagesDataTable.on("click", "td:not(.editable)", function(e){
                var row = $(this).closest("tr"),
                    rowId = row.attr("id"),
                    dataId = rowId.replace("merchant-image-", "");
                window.open("<?php echo $CurrentServer?>Merchant/ManageImages/" + dataId , "merchant-image-admin");
            });
            <?php } else { ?>
            $(".impersonate-merchant").click(function(e){
                var self = $(this),
                    memberId = <?php echo ($merchantRecord->memberId) ? $merchantRecord->memberId : 0; ?>,
                    pwdField = $(self.data("pwd-field")),
                    pwd = pwdField.val(),
                    postData = {
                        'action':'impersonate',
                        'memberId': memberId,
                        'password': pwd
                    };
                e.preventDefault();
                $.ajax({
                    url:'<?php echo $CurrentServer.$adminFolder;?>reports/ApiMemberRegistrations.php',
                    type:'POST',
                    data: JSON.stringify(postData),
                    success: function(data){
                        alert(data.message);
                        window.location.reload();
                    },
                    error: function(response){
                        var data = JSON.parse(response.responseText);
                        alert(data.message);
                        pwdField.highlight();
                    }
                });
            });
            <?php } ?>
        });
    </script>
<?php } // is relevant ?>