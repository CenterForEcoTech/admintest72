<?php
/* @var $merchantRecord MerchantRecord */
/* @var $merchantRecord->primaryContact ContactRecord */
?>
<h3>Edit Merchant Profile</h3>
<p class="help">NOTICE: This form only edits a merchant's profile information. A new merchant may be added to the system, but will not be registered as a member using this form. Nor can you modify a member's email, phone, or password (on the member record).</p>
<style type="text/css">
    form label{ display:block;clear:both;}
    .required:after{color:red;content:"*";}
    .required{color:inherit;}
    form fieldset{display:block;clear:both;}
    form input{margin-bottom:4px;}
    form label span{display:inline;}
    .hidden{display:none !important;}
</style>
<?php if ($merchantRecord->id !== getMerchantId()) {?>
<p>
    <input type="password" id="impersonate-pwd-top" class="auto-watermarked" title="impersonation password">
    <a href="#accessDenied" data-pwd-field="#impersonate-pwd-top" class="impersonate-merchant button-link do-not-navigate" target="merchant-image-admin">Impersonate Merchant</a>
</p>
<?php } ?>
<form id="edit-merchant-form">
    <fieldset class="citygrid-search seven columns <?php echo ($merchantRecord->id) ? "hidden" : "";?>">
        <label>Search for the Business</label>
        <?php include($siteRoot."_cityGridSearchModule.php");?>
    </fieldset>
    <div id="already-registered-message" class="alert"></div>
    <div id="general-validation-message" class="alert"></div>
    <fieldset>
        <legend>Read Only</legend>
        <label>
            <div class="two columns">PID:</div>
            <span id="company_pid_display" class="twelve columns"><a href="<?php echo $CurrentServer."ct/".$merchantRecord->pid;?>" target="_blank"><?php echo $merchantRecord->pid;?></a></span>
        </label>
        <label>
            <div class="two columns">City Grid Id:</div>
            <span class="twelve columns"><span id="company_citygridid_display"><?php echo $merchantRecord->cityGridIdName;?></span> / <span id="company_citygridprofileid_display"><?php echo $merchantRecord->cityGridProfileId;?></span></span>
        </label>
        <label>
            <div class="four columns">Referral Code Text:</div>
            <span class="ten columns"><?php echo $merchantRecord->referralCodeText;?></span>
        </label>
        <label>
            <div class="four columns">&nbsp;</div>
            <input type="checkbox" disabled <?php echo ($merchantRecord->currentreferralid) ? "checked" : "";?>>
            <span>Billing Referral Code</span>
        </label>
        <label>
            <div class="four columns">&nbsp;</div>
            <input type="checkbox" disabled <?php echo ($merchantRecord->referredByMemberId) ? "checked" : "";?>>
            <span>Referred by Org</span>
        </label>
    </fieldset>
    <input type="hidden" name="edit" value="<?php echo $merchantRecord->id ? "1" : "";?>">
    <input type="hidden" name="add" value="<?php echo $merchantRecord->id ? "" : "1";?>">
    <input type="hidden" name="id" value="<?php echo $merchantRecord->id;?>">
    <input type="hidden" name="company_pid" value="<?php echo $merchantRecord->pid;?>">
    <input type="hidden" name="company_officialname" value="<?php echo $merchantRecord->officialname;?>">
    <input type="hidden" name="company_citygridname" value="<?php echo $merchantRecord->cityGridIdName;?>">
    <input type="hidden" name="company_citygridid" value="<?php echo $merchantRecord->cityGridProfileId;?>">
    <fieldset class="fourteen columns">
        <legend>Basic Information</legend>
        <label>
            <div class="two columns required">Name:</div>
            <input class="eight columns" type="text" name="company_name" value="<?php echo $merchantRecord->name;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">Address:</div>
            <input class="eight columns affects-geo" type="text" name="company_address" value="<?php echo $merchantRecord->address;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">City:</div>
            <input class="seven columns affects-geo" type="text" name="company_city" value="<?php echo $merchantRecord->city;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">State:</div>
            <input class="four columns affects-geo" type="text" name="company_state" value="<?php echo $merchantRecord->state;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">Zip:</div>
            <input class="three columns affects-geo" type="text" name="company_zip" value="<?php echo $merchantRecord->postalCode;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">Country:</div>
            <input class="two columns" type="text" name="company_country" value="<?php echo $merchantRecord->country ? $merchantRecord->country : "US";?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Business Phone:</div>
            <input class="four columns" type="text" name="company_phone" value="<?php echo $merchantRecord->phone;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Business Website:</div>
            <input class="seven columns" type="text" name="company_website" value="<?php echo $merchantRecord->website;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Business Description:</div>
            <textarea class="eight columns" name="company_description"><?php echo $merchantRecord->rawDescriptionText;?></textarea>
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">&nbsp;</div>
            <input type="checkbox" name="company_descriptionshow" value="yes" <?php echo $merchantRecord->isDescriptionPublic ? "checked" : "";?>>
            <span>Show description on public profile?</span>
        </label>
    </fieldset>
    <fieldset class="fifteen columns">
        <legend>Search Tags</legend>
        <p>Use a comma or a TAB key to complete a tag entry.</p>
        <label>
            <div class="three columns">Keywords:</div>
            <input class="tag-it" name="company_keywords" value="<?php echo $merchantRecord->keywords;?>">
        </label>
        <label>
            <div class="three columns">Neighborhood:</div>
            <input type="text" class="tag-it  eight columns" name="company_neighborhood" value="<?php echo $merchantRecord->neighborhood;?>">
        </label>
    </fieldset>
    <fieldset>
        <legend>Geographic Coordinates</legend>
        <label>
            <div class="two columns">&nbsp;</div>
            <input type="checkbox" name="recalc_geo" value="1" <?php echo ($merchantRecord->lat && $merchantRecord->long) ? "" : "checked"?>>
            <span>Recalculate geographic coordinates.</span>
        </label>
        <label>
            <div class="two columns">Latitude:</div>
            <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_lat" value="<?php echo $merchantRecord->lat;?>" title="Lat (calculated if left blank)">
        </label>
        <label>
            <div class="two columns">Longitude:</div>
            <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_long" value="<?php echo $merchantRecord->long;?>" title="Long (calculated if left blank)">
        </label>
    </fieldset>
    <?php
    $contactFieldStatus = "";
    if ($merchantRecord->memberId){
        $contactFieldStatus = "disabled";
    }
    ?>
    <fieldset>
        <legend>Contact Info</legend>
        <p class="help">If contact fields are disabled, this means this merchant is registered, and you must edit their contact info using the standard edit profile form.</p>
        <label>
            <div class="two columns">First Name:</div>
            <input class="seven columns" type="text" name="company_contactfirstname" value="<?php echo $merchantRecord->primaryContact->firstname;?>" <?php echo $contactFieldStatus?>>
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Last Name:</div>
            <input class="seven columns" type="text" name="company_contactlastname" value="<?php echo $merchantRecord->primaryContact->lastname;?>"<?php echo $contactFieldStatus?>>
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Job title:</div>
            <input class="seven columns" type="text" name="company_contacttitle" value="<?php echo $merchantRecord->primaryContact->title;?>"<?php echo $contactFieldStatus?>>
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Phone:</div>
            <input class="seven columns" type="text" name="company_contactphone" value="<?php echo $merchantRecord->primaryContact->phone;?>"<?php echo $contactFieldStatus?>>
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Email:</div>
            <input class="seven columns" type="text" name="company_contactemail" value="<?php echo $merchantRecord->primaryContact->email;?>"<?php echo $contactFieldStatus?>>
            <div class="alert"></div>
        </label>
    </fieldset>
    <fieldset>
        <legend>Other</legend>
        <label>
            <div class="four columns">Eventbrite Venue ID:</div>
            <input class="seven columns" type="text" name="company_eventbritevenueid" value="<?php echo $merchantRecord->eventBriteVenueId;?>">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">CustomTransaction Email Text:</div>
            <textarea class="eight columns" name="company_customTransactionEmailText"><?php echo $merchantRecord->rawCustomTransactionEmailText;?></textarea>
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">&nbsp;</div>
            <input type="checkbox" name="company_donationshow" value="yes" <?php echo $merchantRecord->donationshow ? "checked" : "";?>>
            <span>Show donations on public profile?</span>
        </label>
    </fieldset>
    <div>
        <a href="#" id="save-merchant-button" class="button-link">Save</a>
    </div>
</form>
<script type="text/javascript">
    $(function(){
        var regForm = $("#edit-merchant-form"),
            pidDisplayElement = regForm.find("span#company_pid_display"),
            cityGridIdDisplay = regForm.find("span#company_citygridid_display"),
            cityGridProfileIdDisplay = regForm.find("span#company_citygridprofileid_display"),
            pidElement = regForm.find("input[name='company_pid']"),
            officialnameElement = regForm.find("input[name='company_officialname']"),
            cityGridIDElement = regForm.find("input[name='company_citygridname']"),
            cityGridProfileIdElement = regForm.find("input[name='company_citygridid']"),
            neighborhoodElement = regForm.find("input[name='company_neighborhood']"),

            nameElement = regForm.find("input[name='company_name']"),
            addressElement = regForm.find("input[name='company_address']"),
            cityElement = regForm.find("input[name='company_city']"),
            stateElement = regForm.find("input[name='company_state']"),
            postalCodeElement = regForm.find("input[name='company_zip']"),
            phoneElement = regForm.find("input[name='company_phone']"),
            countryElement = regForm.find("input[name='company_country']"),
            websiteElement = regForm.find("input[name='company_website']"),
            descriptionElement = regForm.find("input[name='company_description']"),

            latElement = regForm.find("input[name='company_lat']"),
            longElement = regForm.find("input[name='company_long']"),

            contactPhoneElement = regForm.find("input[name='company_contactphone']"),
            dataIsDirty = false,

            setCityGridData = function(cityGridData){
                if (cityGridData){
                    dataIsDirty = true;
                    nameElement.val(cityGridData.BizName);
                    addressElement.val(cityGridData.Address);
                    cityElement.val(cityGridData.City);
                    stateElement.val(cityGridData.State);
                    officialnameElement.val(cityGridData.BizName);
                    postalCodeElement.val(cityGridData.Zip);
                    latElement.val(cityGridData.Lat);
                    longElement.val(cityGridData.Long);
                    cityGridIDElement.val(cityGridData.CityGridID);
                    cityGridIdDisplay.html(cityGridData.CityGridID);
                    cityGridProfileIdElement.val(cityGridData.CityGridProfileID);
                    cityGridProfileIdDisplay.html(cityGridData.CityGridProfileID);
                    neighborhoodElement.val(cityGridData.Neighborhood);
                    phoneElement.val(cityGridData.Phone);
                    contactPhoneElement.val(cityGridData.Phone);
                    websiteElement.val(cityGridData.Website);
                    descriptionElement.val(cityGridData.Description);
                } else {
                    pidElement.val("");
                    pidDisplayElement.html("");
                    nameElement.val("");
                    addressElement.val("");
                    cityElement.val("");
                    stateElement.val("");
                    postalCodeElement.val("");
                    latElement.val("");
                    longElement.val("");
                    cityGridIDElement.val("");
                    cityGridIdDisplay.html("");
                    cityGridProfileIdElement.val("");
                    cityGridProfileIdDisplay.html("0");
                    officialnameElement.val("");
                    neighborhoodElement.val("");
                    phoneElement.val("");
                    contactPhoneElement.val("");
                    websiteElement.val("");
                    descriptionElement.val("");
                    dataIsDirty = false;
                }
            },
            merchantAutoCompleteOnSelect = function(event, ui) {
                var lookupURL = '<?php echo $CurrentServer;?>Library/BizNameInfoJSON?CityGridProfileID='+ui.item.CityGridProfileID,
                    checkRegistrationURL = "<?php echo $CurrentServer;?>ApiMerchants",
                    checkRegistrationData;

                setCityGridData();
                $.getJSON(lookupURL, function(data) {
                    var businessData = ui.item;
                    if (data.length){
                        businessData.Website = data[0].Website;
                    }
                    checkRegistrationData = {
                        "company_pid":pidElement.val(),
                        "company_citygridid":businessData.CityGridProfileID,
                        "company_name":businessData.BizName,
                        "company_city":businessData.City,
                        "company_state":businessData.State,
                        "action": "check"
                    };
                    $.ajax({
                        url: checkRegistrationURL,
                        data: checkRegistrationData,
                        type: "GET",
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data){
                            var alertMessageTemplate = "It seems that your business is already in with <?php echo $SiteName;?>. \
                                    <a href='?nav=profile-edit&id={loadNow}'>Click here to load the record for edit.</a>",
                                message;
                            $("#already-registered-message").html("");
                            if (data.isRegistered && data.id){
                                // merchant already registered
                                message = alertMessageTemplate.replace(/{loadNow}/g, data.id);
                                $("#already-registered-message").html(message);
                            } else {
                                if (data.pid){
                                    pidElement.val(data.pid);
                                    pidDisplayElement.html(data.pid);
                                }
                                setCityGridData(businessData);
                            }
                        }
                    });
                });
            },
            merchantAutoCompleteBeforeSearch = function(){
                if (dataIsDirty){
                    setCityGridData();
                }
            },

        // login availability
            loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
            loginAvailableString = "Login Available",
            checkLoginAvail = function(value, alertElement, element, confirmElement){
                $.get(loginAvailUrl,
                    {
                        IndividualLogin: value
                    },
                    function(data){
                        if (data !== loginAvailableString){
                            alertElement.html(data);
                            if (confirmElement){
                                confirmElement.val("");
                            }
                            element.focus();
                        }
                    });
            };

        CETDASH.BusinessLookup.apply({
            apiUrl : "<?php echo $CurrentServer;?>Library/BizLookup",
            beforeSearch: merchantAutoCompleteBeforeSearch,
            onSelect : merchantAutoCompleteOnSelect
        });

        regForm.on("click.CETDASH", "#save-merchant-button", function(e){
            var url = "ApiMerchantProfile.php",
                button = $(e.target);
            e.preventDefault();
            button.addClass("ui-state-disabled");
            $.ajax({
                url: url,
                data: regForm.serialize(),
                type: "POST",
                success: function(data){
                    button.removeClass("ui-state-disabled");
                    window.location = "?nav=profile-edit&id=" + data.id;
                    CETDASH.scrollTo(window);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var data = $.parseJSON(jqXHR.responseText),
                        generalValidationElement = $("#general-validation-message");
                    button.removeClass("ui-state-disabled");
                    generalValidationElement.html("");
                    if (data.validationMessages && $.isArray(data.validationMessages) && data.validationMessages.length){
                        CETDASH.forms.handleStandardErrors(regForm, data.validationMessages);
                    } else if (data.message){
                        generalValidationElement.html(data.message);
                    }
                }
            });
        });

        regForm.on("change.CETDASH", ".affects-geo", function(e){
            var editedElement = $(this),
                onlyIfBlank = editedElement.hasClass("if-blank"),
                overrideCheckbox = regForm.find("input[name='recalc_geo']");
            if (onlyIfBlank){
                if (editedElement.val().trim() === ""){
                    overrideCheckbox.prop("checked", true);
                } else {
                    overrideCheckbox.prop("checked", false);
                }
            } else {
                overrideCheckbox.prop("checked", true);
            }
        });

        $("input[name='company_contactphone'], input[name='company_phone']").blur(function(){
            var el = $(this),
                value = el.val().replace(/[^\d]/g, ""),
                alertElement = el.closest("label").find(".alert"),
                areacodes = Array(<?php echo $Config_areacodes;?>),
                areacode = Number(value.substr(0,3)),
                areacodeFound = $.inArray(areacode, areacodes) >= 0;
            alertElement.html("");

            el.val(value); // replace with just numbers

            if (value){
                //check to see if first three digits are a valid area code
                if (!areacodeFound){
                    alertElement.html("Area Code is invalid");
                    el.focus();
                    return false;
                }else {
                    if (value.length != 10){
                        if (value.length < 10){
                            alertElement.html("Phone numbers must be 10 digits");
                            el.focus();
                            return false;
                        }else{
                            alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                            el.val(value.substr(0,10));
                            return false;
                        }
                    } else {
                        if (el.attr("name") === "company_contactphone"){
                            checkLoginAvail(value, alertElement, el);
                        }
                        return true;
                    }
                }
            }else{
                return true;
            }
        });

        $("input[name='company_contactemail']").blur(function(){
            var el = $(this),
                elId = el.attr("id"),
                confirmEl = $("#" + elId + "Confirm"),
                value = el.val(),
                emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,
                alertElement = el.closest("label").find(".alert");
            alertElement.html("");

            if ($.trim(value) && emailReg.test(value)){
                checkLoginAvail(value, alertElement, el, confirmEl);
            }
        });
        $(".tag-it").tagit({
            allowSpaces: true
        });

    });
</script>
<?php
include_once("_merchantImages.php");
?>