<?php
include_once($dbProviderFolder."ReferralCodeHelper.php");
$billingCodes = ReferralCodeHelper::getAllBillingCodes($mysqli);
?>
<style type="text/css">
    .ui-autocomplete > li { height: 40px; z-index:9999;}
    td.logo-image:hover,
    td.billing-code:hover,
    td.transaction-count:hover,
    td.social-media:hover {cursor:pointer;outline:groove;}
</style>
<h3>Merchant Profiles</h3>
    <p>Click in the Logo column to add or edit the logo. Click in the Id column to edit the profile. Click <a href="?nav=profile-edit" class="button-link">Add Merchant</a> to add a new merchant.</p>
<p>Click the "transactions" column to view that merchant's transactions.</p>

<p>
    To Impersonate a member,
<ol style="list-style-type: decimal;">
    <li><label>Type the password here: <input type="password" id="impersonate-pwd" class="auto-watermarked" title="password for impersonation"></label></li>
    <li>Click on the member's ID</li>
    <li>Once you get confirmation that you are logged in as that member, navigate to the website.</li>
</ol>
</p>
<p>
    <a id="export-to-csv" href="#" title="export current records" class="button-link do-not-navigate">Export</a>
</p>
<style type="text/css">
    td.impersonation:hover {cursor:pointer; outline:groove;}
</style>
<table id="merchant-list">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Member Id" data-target-column="4"></td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th>Id</th>
        <th>Business Name</th>
        <th># Transactions</th>
        <th>Billing Code</th>
        <th>Logo</th>
        <th>Member Id</th>
        <th>Social Media Handles</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="hidden" id="pick-logo-form" title="Add/Delete Merchant Logo">
    <p>Use the autocomplete box to search for the uploaded image from the library <em>(save without selecting to delete the current logo)</em>.</p>
    <input type="text" id="image-search-box" title="search for image" class="auto-watermarked">
    <input type="hidden" id="image_id">
    <input type="hidden" id="merchant_id">
    <p>
        <a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=image-library" class="button-link" target="_blank">Go to Image Library</a>
    </p>
</div>

<div class="hidden" id="pick-billing-code-form" title="Select Billing Plan">
    <p>Select a billing plan for this merchant.</p>
    <select name="billing_code_id">
        <?php
        foreach ($billingCodes as $billingCode){
            $display = $billingCode->billing_code;
            if ($billingCode->rate_type == "flatfee"){
                $display .= " ($".$billingCode->rate_amount." per month)";
            } else if ($billingCode->rate_type == "annual"){
                $display .= " ($".$billingCode->rate_amount." per year)";
            } else {
                $display .= " (".$billingCode->rate_amount."% of sales)";
            }
            echo "<option value='".$billingCode->id."'>".$display."</option>";
        }
        ?>
    </select>
    <input type="hidden" name="merchant_id">
</div>

<div class="hidden" id="edit-social-media-form" title="Edit Social Media Handles">
    <p>Assign social media handles please</p>
    <label>
        <div>Facebook Page Url or Page ID:</div>
        <input type="text" name="facebookPageUrl">
    </label>
    <label>
        <div>Twitter handle:</div>
        <input type="text" name="twitterHandle">
    </label>
    <input type="hidden" name="merchant_id">
</div>

<script type="text/javascript">
    $(function(){
        var merchantListTable = $("#merchant-list"),
            apiUrl = "ApiMerchantProfile.php",
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            lastRequestedCriteria = null,
            renderSocialMediaText = function(record){
                var template = "<div class='{networkName}-handle' data-orig='{origText}'>{networkName}: {handle}</div>",
                    text = template.replace(/{networkName}/g, record.socialNetworkName)
                        .replace(/{handle}/g, record.socialHandle)
                        .replace(/{origText}/g, record.originalIdentifierText);
                return text;
            },
            oTable = merchantListTable.dataTable({
                "bJQueryUI": true,
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aaSorting": [[ 0, "desc" ]],
                "aoColumns" :[
                    {"mData": "idLink"},
                    {"mData": "businessDisplay"},
                    {"mData": "numTransactions", "sWidth" : "50px", "sClass": "transaction-count"},
                    {"mData": "billingCodeDisplay", "sClass": "billing-code"},
                    {"mData": "logoPath", "sClass":"logo-image"},
                    {"mData": "memberId", "sWidth": "5%", "sClass": "impersonation"},
                    {
                        "mData": "socialMediaHandles",
                        "sWidth": "5%",
                        "sClass": "social-media",
                        "mRender": function(data, type, full){
                            var renderedText = "", record;
                            for (var i = 0; i < data.length; i ++ ){
                                record = data[i];
                                renderedText += renderSocialMediaText(record);
                            }
                            return renderedText;
                        }
                    }
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0, 2, 5 ] },
                    { "bSortable": false, "aTargets": [ 2, 6 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            }),
            pickLogoForm = $("#pick-logo-form"),
            autoCompleteField = pickLogoForm.find("#image-search-box"),
            imageIdField = pickLogoForm.find("#image_id"),
            pickLogoMerchantIdField = pickLogoForm.find("#merchant_id"),
            pickBillingCodeForm = $("#pick-billing-code-form"),
            pickBillingCodeMerchantIdField = pickBillingCodeForm.find("input[name='merchant_id']"),
            billingCodeIdField = pickBillingCodeForm.find("select[name='billing_code_id']"),
            editSocialMediaForm = $("#edit-social-media-form"),
            facebookPageUrlField = editSocialMediaForm.find("input[name='facebookPageUrl']"),
            twitterHandlefield = editSocialMediaForm.find("input[name='twitterHandle']"),
            editSocialMediaMerchantIdField = editSocialMediaForm.find("input[name='merchant_id']");

        autoCompleteField.autocomplete({
            delay:100,
            minLength: 0,
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $CurrentServer;?>mayor/cms/ApiImageLibrary.php",
                    data: {
                        action: "keyword_search",
                        keyword: request.term
                    },
                    success: function(data) {
                        response($.map(data.collection, function(el, index) {
                            return {
                                value: el.file_name,
                                id: el.id,
                                url: el.imageFullPath,
                                label: el.title
                            };
                        }));
                    }
                });
            },
            select: function(e, ui){
                imageIdField.val(ui.item.id);
            }
        }).data("autocomplete")._renderItem = function(ul, item){
            return $("<li />")
                .data("item.autocomplete", item)
                .append("<a><img src='"+ item.url +"' class='small-logo-image'/> "+ item.label +"</a>")
                .appendTo(ul);
        };

        pickLogoForm.dialog({
            autoOpen: false,
            height: 350,
            width:450,
            modal:true,
            buttons: {
                "Save": function(){
                    var imageId = imageIdField.val(),
                        merchantId = pickLogoMerchantIdField.val();

                    $.ajax({
                        url: "ApiMerchantProfile.php",
                        type: "POST",
                        data: {
                            "id":merchantId,
                            "image_id":imageId
                        },
                        success: function(data){
                            var rowSelector = "#merchant-" + merchantId,
                                row = $(rowSelector),
                                cell = row.find("td.logo-image"),
                                imgTagTemplate = "<img src='{srcUrl}' height='61px'>";
                            if (data.record && cell.length){
                                if (data.record.logoFullPath){
                                    cell.html(imgTagTemplate.replace(/{srcUrl}/g, data.record.logoFullPath));
                                } else {
                                    cell.html("");
                                }
                            }
                            pickLogoForm.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    pickLogoForm.dialog("close");
                }
            },
            close: function(){
                autoCompleteField.val("");
                imageIdField.val("");
                pickLogoMerchantIdField.val("");
            }
        });

        merchantListTable.on("click", "td.logo-image", function(e){
            var cell = $(this),
                cellData = cell.html(),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("merchant-", "");
            pickLogoMerchantIdField.val(dataId);
            pickLogoForm.dialog("open");
        });

        pickBillingCodeForm.dialog({
            autoOpen: false,
            height: 250,
            width:400,
            modal:true,
            buttons: {
                "Save": function(){
                    var billingCodeId = billingCodeIdField.val(),
                        merchantId = pickBillingCodeMerchantIdField.val();

                    $.ajax({
                        url: "ApiMerchantProfile.php",
                        type: "POST",
                        data: {
                            "id":merchantId,
                            "billing_code_id":billingCodeId
                        },
                        success: function(data){
                            var rowSelector = "#merchant-" + merchantId,
                                row = $(rowSelector),
                                cell = row.find("td.billing-code");
                            if (data.record && cell.length){
                                if (data.record.billingCodeDisplay){
                                    cell.html(data.record.billingCodeDisplay);
                                } else {
                                    cell.html("");
                                }
                            }
                            pickBillingCodeForm.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    pickBillingCodeForm.dialog("close");
                }
            },
            close: function(){
                billingCodeIdField.val(0);
                pickBillingCodeMerchantIdField.val("");
            }
        });

        merchantListTable.on("click", "td.billing-code", function(e){
            var cell = $(this),
                cellData = cell.html(),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("merchant-", ""),
                planId = cell.find(".billing-desc").attr('data-billing-id');
            pickBillingCodeMerchantIdField.val(dataId);
            if (planId){
                billingCodeIdField.val(planId);
            }
            pickBillingCodeForm.dialog("open");
        });

        merchantListTable.on("click", "td.transaction-count", function(e){
            var cell = $(this),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("merchant-", ""),
                transactionReportUrl = '<?php echo $CurrentServer.$adminFolder;?>reports/?nav=transaction-report&MerchantID=' + dataId,
                windowName = 'transactions-for-'+dataId;
            e.preventDefault();
            window.open(transactionReportUrl, windowName);
        });

        editSocialMediaForm.dialog({
            autoOpen: false,
            height: 320,
            width:400,
            modal:true,
            buttons: {
                "Save": function(){
                    var facebookPageUrl = facebookPageUrlField.val(),
                        twitterHandle = twitterHandlefield.val(),
                        merchantId = editSocialMediaMerchantIdField.val();

                    $.ajax({
                        url: "ApiMerchantProfile.php",
                        type: "POST",
                        data: {
                            "merchantId":merchantId,
                            "facebookPageUrl":facebookPageUrl,
                            "twitterHandle":twitterHandle,
                            "action":"social_media"
                        },
                        success: function(data){
                            var rowSelector = "#merchant-" + merchantId,
                                row = $(rowSelector),
                                cell = row.find("td.social-media"),
                                record, renderedText = "";
                            if ($.isArray(data) && cell.length) {
                                if (data.length){
                                    for (var i = 0; i < data.length; i++){
                                        record = data[i];
                                        renderedText += renderSocialMediaText(record);
                                    }
                                    cell.html(renderedText);
                                } else {
                                    cell.html("");
                                }
                            }
                            editSocialMediaForm.dialog("close");
                        }
                    });
                },
                "Cancel": function(){
                    editSocialMediaForm.dialog("close");
                }
            },
            close: function(){
                facebookPageUrlField.val("");
                twitterHandlefield.val("");
                editSocialMediaMerchantIdField.val("");
            }
        });

        merchantListTable.on("click", "td.social-media", function(e){
            var cell = $(this),
                rowId = cell.closest("tr").attr("id"),
                dataId = rowId.replace("merchant-", ""),
                facebookPageUrl = cell.find(".facebook-handle").data("orig"),
                twitterHandle = cell.find(".twitter-handle").data("orig");
            editSocialMediaMerchantIdField.val(dataId);
            if (facebookPageUrl){
                facebookPageUrlField.val(facebookPageUrl);
            }
            if (twitterHandle){
                twitterHandlefield.val(twitterHandle);
            }
            editSocialMediaForm.dialog("open");
        });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $("#export-to-csv").click(function(e){
            var aoData = lastRequestedCriteria.slice(0);
            e.preventDefault();

            // add our export parameter
            aoData.push({
                "name" : "export",
                "value" : "csv"
            });
            window.location = apiUrl + "?" + $.param(aoData);
        });

        merchantListTable.on("click", "td.impersonation", function(e){
            var cell = $(this),
                dataId = cell.html(),
                pwdField = $("#impersonate-pwd"),
                pwd = pwdField.val(),
                postData = {
                    "action": "impersonate",
                    "memberId": dataId,
                    "password": pwd
                };
            if (dataId > 0){
                $.ajax({
                    url:'<?php echo $CurrentServer.$adminFolder;?>reports/ApiMemberRegistrations.php',
                    type: "POST",
                    data: JSON.stringify(postData),
                    success: function(data){
                        alert(data.message);
                    },
                    error: function(response){
                        var data = JSON.parse(response.responseText);
                        alert(data.message);
                        pwdField.highlight();
                    }
                });
            } else {
                alert("You cannot impersonate an unregistered merchant! Sorry!");
            }
        });
    });
</script>