<?php
include_once("_config.php");
include_once("../assert_is_ajax.php");
include_once("../functions.php");

$files = array();
$handle = opendir("disbursements/");
//var_dump($handle);
while ($item = readdir($handle)){
    //echo $item;
    if (substr($item, -4) == ".csv"){
        $file = "disbursements/".$item;
        $files[filemtime($file)] = $file;
    }
}
closedir($handle);
krsort($files);
//var_dump($files);
$results = array();
foreach($files as $file){
    $shortname = basename($file);
    $result = (object) array(
        "path" => $file,
        "name" => $shortname,
        "date" => date("Y-m-d h:i:s a", filemtime($file)),
        "size" => filesize($file)
    );
    $results[] = $result;
}
//var_dump($results);
output_json($results);
die();
?>