<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "DELETE is not supported";
        break;
    case "GET":
        include_once($siteRoot."repository/mySqlProvider/LoggingProviders.php");
        include_once("model/_logModel.php");
        $provider = new EventLoggingProvider($mysqli);
        $model = new EventLogTableRow();
        $criteria = $model->getCriteria($_GET);
        if ($_GET['event_id']){
            $criteria->event_id = $_GET['event_id'];
        }
        $paginatedErrors = $provider->get($criteria);
        $logItems = $paginatedErrors->collection;
        $results = array(
            "iTotalRecords" => $paginatedErrors->totalRecords,
            "iTotalDisplayRecords" => $paginatedErrors->totalRecords,
            "sEcho" => $_GET["sEcho"]
        );
        foreach($logItems as $logItem){
            $results["aaData"][] = new EventLogTableRow($logItem);
        }
        if (!isset($results["aaData"])){
            $results["aaData"] = array();
        }
        output_json($results);
        die();
        break;
    case "POST":
        echo "POST is not supported";
        break;
    case "PUT":
        echo "PUT is not supported";
        break;
}
output_json($results);
die();
?>