<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "DELETE is not supported";
        break;
    case "GET":
        include_once($siteRoot."repository/mySqlProvider/LoggingProviders.php");
        include_once("model/_loginHistoryModel.php");
        $provider = new LoginHistoryProvider($mysqli);
        $criteria = LoginHistoryTableRow::getCriteria($_GET);
        $paginatedResult = $provider->get($criteria);
        $items = $paginatedResult->collection;
        $results = array(
            "iTotalRecords" => $paginatedResult->totalRecords,
            "iTotalDisplayRecords" => $paginatedResult->totalRecords,
            "sEcho" => $_GET["sEcho"]
        );
        foreach($items as $item){
            $results["aaData"][] = new LoginHistoryTableRow($item);
        }
        if (!isset($results["aaData"])){
            $results["aaData"] = array();
        }
        output_json($results);
        die();
        break;
    case "POST":
        echo "POST is not supported";
        break;
    case "PUT":
        echo "PUT is not supported";
        break;
}
output_json($results);
die();
?>