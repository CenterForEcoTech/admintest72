<div id="nav-menu">
    <ul>
        <li><a id="error-logs" href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=error-logs" class="button-link">Error Logs</a></li>
        <li><a id="event-logs" href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=event-logs" class="button-link">Event Logs</a></li>
        <li><a id="email-logs" href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=email-logs" class="button-link">Email Logs</a></li>
        <li><a id="admin-logs" href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=admin-logs" class="button-link">Admin Logs</a></li>
        <li><a id="login-history" href="<?php echo $CurrentServer.$adminFolder;?>monitoring/?nav=login-history" class="button-link">Login History</a></li>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>" class="button-link back-to-console">Admin Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>