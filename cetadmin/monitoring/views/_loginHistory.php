<h3>Login History</h3>

<table id="login-history-list" style="font-size:0.9em;">
    <thead>
    <tr>
        <th style="width:35px;">Id</th>
        <th style="width:35px;">Type</th>
        <th>Member Id</th>
        <th>Admin Id</th>
        <th>Date</th>
        <th style="width:35px;">Contact</th>
        <th style="width:35px;">Company</th>
        <th style="width:35px;">#IP</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var tableElement = $("#login-history-list"),
            apiUrl = "ApiLoginHistory.php",
            lastRequestedCriteria = null,
            oTable = tableElement.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "iDisplayLength":50,
                "aoColumns" :[
                    {"mData": "id", "sWidth":"45px", "sClass": "numeric"},
                    {"mData": "memberType", "sWidth":"10px",'bSortable': false},
                    {"mData": "memberId", "sWidth":"10px", "sClass": "numeric",'bSortable': false},
                    {"mData": "adminId", "sWidth":"10px", "sClass": "numeric",'bSortable': false},
                    {"mData": "dateString", "sWidth":"185px"},
                    {
                        "mData": "contactInfo",
                        "mRender": function( data, type, full ){
                            var text = data.name;
                            if (data.email){
                                text += "<br>" + data.email;
                            }
                            if (data.phone){
                                text += "<br>" + data.phone;
                            }
                            return text;
                        },
                        'bSortable': false
                    },
                    {
                        "mData": "companyInfo",
                        "mRender": function( data, type, full ){
                            var text = data.name;
                            if (data.city){
                                text += "<br>" + data.city + ", " + data.state + " " + data.zip;
                            }
                            if (data.phone){
                                text += "<br>" + data.phone;
                            }
                            if (data.ein){
                                text += "<br>EIN: " + data.ein;
                            }
                            if (data.parent_ein){
                                text += "<br>Parent EIN: " + data.parent_ein;
                            }
                            return text;
                        },
                        'bSortable': false
                    },
                    {"mData": "ip", "sWidth":"10px", 'bSortable': false}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });
    });
</script>