<h3>Error Logs</h3>
<p>Unless otherwise verified, these logs list only PHP errors that have not otherwise been handled by the code. These errors would typically
display on the screen if allowed. The purpose of these logs is to allow administrators to be aware of and monitor error conditions that are
otherwise unreportable.</p>

<table id="error-log-list" style="font-size:0.9em;">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th style="width:35px;">Id</th>
        <th style="width:150px;">Date</th>
        <th>Type</th>
        <th>Message</th>
        <th>File</th>
        <th style="width:35px;">Line</th>
        <th style="width:105px;">IP</th>
        <th style="width:35px;" title="number of occurrences of the same message, file, line, and ip">#</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var errorLogTable = $("#error-log-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            apiUrl = "ApiErrorLogs.php",
            lastRequestedCriteria = null,
            oTable = errorLogTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id"},
                    {"mData": "dateString"},
                    {"mData": "errorTypeString"},
                    {"mData": "message"},
                    {"mData": "file"},
                    {"mData": "line"},
                    {"mData": "ip"},
                    {"mData": "count"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });
    });
</script>