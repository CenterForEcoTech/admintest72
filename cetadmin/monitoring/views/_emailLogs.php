<h3>Email Logs</h3>
    <p>Click on a row to see the email body.</p>

<table id="email-log-list" style="font-size:0.9em;">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th>Id</th>
        <th>Date</th>
        <th>Member</th>
        <th>From</th>
        <th>To</th>
        <th>Subject</th>
        <th>Message</th>
        <th>Status</th>
        <th>Errors</th>
        <th>Updated</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var logTable = $("#email-log-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            apiUrl = "ApiEmailLogs.php",
            lastRequestedCriteria = null,
            oTable = logTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id", "sWidth": "5%"},
                    {"mData": "dateString", "sWidth": "5%"},
                    {"mData": "member_id", "sWidth": "5%"},
                    {"mData": "email_from"},
                    {"mData": "email_to", "sWidth": "20%"},
                    {"mData": "subject"},
                    {"mData": "message", "sClass": "message-text"},
                    {"mData": "status", "sWidth": "5%"},
                    {"mData": "errors"},
                    {"mData": "updated"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] },
                    { "bVisible": false, "aTargets": [3, 6, 9]}
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });

        logTable.on("click", "tbody tr", function(e){
            var row = oTable.fnGetPosition(this),
                aData = oTable.fnGetData(row),
                content = "<div>" + aData.message + "</div>",
                subject = "#" +aData.id + ": " + aData.subject;
            $(content).dialog({
                title: subject,
                modal: true,
                minWidth: 600,
                maxWidth: 1200
            });
        });
    });
</script>