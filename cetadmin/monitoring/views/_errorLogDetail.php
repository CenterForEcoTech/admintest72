<h3>Error Log Detail</h3>
<?php
$id = $_GET["id"];
if (empty($id) || !is_numeric($id)){
    echo "No Record Specified.";
} else {
    include_once($siteRoot."repository/mySqlProvider.ErrorLoggingProvider.php");
    include_once("model/_errorLogModel.php");
    $provider = new ErrorLoggingProvider($mysqli);
    $record = $provider->getRecord($id);
    if ($record){
        $model = new ErrorLogDetail($record);
?>
        <fieldset class="sixteen columns">
            <legend>Basic</legend>
            <label>
                <div class="three columns">Id:</div>
                <span class="twelve columns"><?php echo $model->id;?></span>
            </label>
            <label>
                <div class="three columns">Message:</div>
                <span class="twelve columns"><?php echo $model->message;?></span>
            </label>
            <label>
                <div class="three columns">File:</div>
                <span class="twelve columns"><?php echo $model->file;?></span>
            </label>
            <label>
                <div class="three columns">Line:</div>
                <span class="twelve columns"><?php echo $model->line;?></span>
            </label>
            <label>
                <div class="three columns">Level:</div>
                <span class="twelve columns"><?php echo $model->errorLevel." (".$model->errorTypeString.")";?></span>
            </label>
            <label>
                <div class="three columns">IP:</div>
                <span class="twelve columns"><?php echo $model->ip;?></span>
            </label>
            <label>
                <div class="three columns">Count:</div>
                <span class="twelve columns"><?php echo $model->count;?></span>
            </label>
            <label>
                <div class="three columns">First Occurrence:</div>
                <span class="twelve columns"><?php echo $model->firstOccurrenceDate;?></span>
            </label>
            <label>
                <div class="three columns">Last Occurrence:</div>
                <span class="twelve columns"><?php echo $model->dateString;?></span>
            </label>
        </fieldset>

<?php
        foreach ($record->context as $contextGrouping => $contextVariables){
            if ($contextGrouping != "ip" && count($contextVariables)){
?>
            <fieldset class="sixteen columns">
                <legend><?php echo $contextGrouping;?></legend>
                <table border='1' style="border-collapse: collapse" >
                    <thead>
                        <th>Variable</th>
                        <th style="text-align:left;">Value</th>
                    </thead>
                    <tbody>
<?php
                foreach ($contextVariables as $key => $value){
?>
                    <tr>
                        <td><?php echo $key;?></td>
                        <td><?php echo $value;?></td>
                    </tr>
<?php
                } // end foreach contextVariables
?>
                    </tbody>
                </table>
            </fieldset>
<?php
            }// end if context variables
        } // end foreach context
?>

<?php
    } else {
        echo "No matching record found.";
    } // end if no record returned from back end
} // end if no id
?>
<script type="text/javascript">
    $(function(){
        $("#nav-menu").find("#error-logs").addClass("ui-state-highlight");
    });
</script>