<h3>Admin Error Logs</h3>

<table id="admin-log-list" style="font-size:0.9em;">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default"><input class="auto-watermarked" title="containing text" data-target-column="2"></td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th style="width:10%;">Id</th>
        <th style="width:20%;">Date</th>
        <th style="width:10%;">Page</th>
        <th style="width:40%;">Message</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var errorLogTable = $("#admin-log-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = new Array(),
            apiUrl = "ApiAdminLogs.php",
            lastRequestedCriteria = null,
            oTable = errorLogTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id"},
                    {"mData": "dateString"},
                    {"mData": "page"},
                    {"mData": "message"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });
    });
</script>