<h3>Event Logs</h3>

<table id="event-log-list" style="font-size:0.9em;">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked datepicker" title="On or after date" data-target-column="1"></td>
        <td class="ui-state-default"><input class="auto-watermarked" title="event id" data-target-column="2" value="<?php echo $_GET['event_id'];?>"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th style="width:10%;">Id</th>
        <th style="width:20%;">Date</th>
        <th style="width:10%;">Event</th>
        <th style="width:40%;">Message</th>
        <th style="width:10%;">Member</th>
        <th style="width:10%;">Admin</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $(function(){
        var errorLogTable = $("#event-log-list"),
            searchInputs = $("#search-inputs input"),
            asInitVals = [],
            apiUrl = "ApiEventLogs.php",
            lastRequestedCriteria = null,
            oTable = errorLogTable.dataTable({
                "bJQueryUI": true,
                "aaSorting": [[ 0, "desc" ]],
                "bServerSide":true,
                "sAjaxSource": apiUrl,
                "aoColumns" :[
                    {"mData": "id"},
                    {"mData": "dateString"},
                    {"mData": "event_id"},
                    {"mData": "message"},
                    {"mData": "member_id"},
                    {"mData": "admin_id"}
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 0 ] }
                ],
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "event_id", "value": "<?php echo $_GET['event_id'];?>" } );
                },
                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                    lastRequestedCriteria = aoData;
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    } );
                }
            });

        searchInputs.keyup( function () {
            var element = $(this),
                currentColumn = searchInputs.index(this),
                targetColumn = element.attr("data-target-column");
            oTable.fnFilter( this.value, targetColumn );
        } );
        /*
        * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
        * the footer
        */
        searchInputs.each( function (i) {
            asInitVals[i] = this.value;
        } );

        searchInputs.focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        searchInputs.blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[searchInputs.index(this)];
            }
        } );

        $(".datepicker").datepicker({
            onClose: function(){
                $(this).keyup();
            }
        });
    });
</script>