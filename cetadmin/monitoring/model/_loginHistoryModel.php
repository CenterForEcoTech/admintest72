<?php

class LoginHistoryTableRow{
    public $id;
    public $memberType;
    public $memberId;
    public $adminId;
    public $dateString;
    public $contactInfo;
    public $companyInfo;
    public $ip;

    public function __construct($record){
        $this->id = $record->id;
        $this->memberType = $record->member_type;
        $this->memberId = $record->member_id;
        $this->adminId = $record->admin_id;
        $this->dateString = date("Y-m-d H:i:s", strtotime($record->timestamp));
        $this->ip = $record->ip;
        $this->contactInfo = (object)array(
            "name" => $record->contact_name,
            "email" => $record->contact_email,
            "phone" => preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $record->contact_phone)
        );
        $this->companyInfo = (object)array(
            "name" => $record->name,
            "city" => $record->city,
            "state" => $record->state,
            "zip" => $record->zip,
            "phone" => preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1.$2.$3", $record->phone),
            "ein" => $record->ein,
            "parent_ein" => $record->parent_ein
        );
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "memberType",
        2 => "memberId",
        3 => "adminId",
        4 => "dateString",
        5 => "contactInfo",
        6 => "companyInfo",
        7 => "ip"
    );

    public static function getCriteria($datatablePost){
        $criteria = new LoginHistoryCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}