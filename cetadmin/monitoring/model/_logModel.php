<?php

abstract class LogModelBase{
    public $id;
    public $dateString;
    public $message;

    public function __construct($record = null){
        if ($record){
            $this->id = $record->id;
            $this->dateString = date("Y-m-d H:i:s", $record->timestamp);
            $this->message = $record->message;
        }
    }

    /**
     * @abstract
     * @return LoggingCriteriaBase
     */
    abstract function getNewCriteria();

    /**
     * @abstract
     * @return array
     */
    abstract function getColumnMapping();

    public function getCriteria($datatablePost){
        $criteria = $this->getNewCriteria();
        $columnMapping = $this->getColumnMapping();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = $columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort($columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach ($columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class EventLogTableRow extends LogModelBase{
    public $event_id;
    public $member_id;
    public $admin_id;

    public function __construct($record = null){
        if ($record){
            parent::__construct($record);
            $this->dateString = date("Y-m-d H:i:s", strtotime($record->timestamp));
            $this->event_id = $record->event_id;
            $this->member_id = $record->member_id;
            $this->admin_id = $record->admin_id;
        }
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "event_id",
        3 => "message",
        4 => "member_id",
        5 => "admin_id"
    );

    public function getColumnMapping(){
        return self::$columnMapping;
    }

    public function getNewCriteria(){
        return new EventLoggingCriteria();
    }
}

class AdminLogTableRow extends LogModelBase{
    public $page;

    public function __construct($record = null){
        if ($record){
            parent::__construct($record);
            $this->dateString = date("Y-m-d H:i:s", strtotime($record->timestamp));
            $this->page = $record->page;
        }
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "page",
        3 => "message"
    );

    public function getColumnMapping(){
        return self::$columnMapping;
    }

    public function getNewCriteria(){
        return new AdminLoggingCriteria();
    }
}

class EmailLogTableRow extends LogModelBase{
    public $member_id;
    public $email_from;
    public $email_to;
    public $subject;
    public $status;
    public $errors;
    public $updated;

    public function __construct($record = null){
        if ($record){
            parent::__construct($record);
            $this->dateString = date("Y-m-d H:i:s", strtotime($record->timestamp));
            $this->member_id = $record->member_id;
            $this->message = htmlspecialchars_decode($record->message);
            $this->email_from = $record->email_from;
            $this->email_to = htmlspecialchars($record->email_to);
            $this->subject = $record->subject;
            $this->status = $record->status;
            $this->errors = $record->errors;
            $this->updated = date("Y-m-d H:i:s", strtotime($record->updated));
        }
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "member_id",
        3 => "email_from",
        4 => "email_to",
        5 => "subject",
        6 => "message",
        7 => "status",
        8 => "errors",
        9 => "updated"
    );

    public function getColumnMapping(){
        return self::$columnMapping;
    }

    public function getNewCriteria(){
        return new EmailLoggingCriteria();
    }
}