<?php

class ErrorLogTableRow{
    public $id;
    public $dateString;
    public $errorLevel;
    public $errorTypeString;
    public $message;
    public $file;
    public $line;
    public $ip;
    public $count;

    public function __construct(LoggedError $record){
        $this->id = '<a href="?nav=error-log-detail&id='.$record->id.'" target="logview">'.$record->id.'</a>';
        $this->dateString = date("Y-m-d H:i:s", $record->timestamp);
        $this->errorLevel = $record->errorLevel;
        $this->message = $record->message;
        $this->file = $record->file;
        $this->line = $record->line;
        $this->ip = $record->context->ip;
        $this->count = $record->count;
        $this->errorTypeString = translateErrorType($this->errorLevel);
    }

    private static $columnMapping = array(
        0 => "id",
        1 => "dateString",
        2 => "errorLevel",
        3 => "message",
        4 => "file",
        5 => "line",
        6 => "ip",
        7 => "count"
    );

    public static function getCriteria($datatablePost){
        $criteria = new ErrorLoggingCriteria();

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "dateString":
                        $criteria->date = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class ErrorLogDetail extends ErrorLogTableRow{
    /** @var ErrorContext */
    public $context;
    public $firstOccurrenceDate;

    public function __construct(LoggedError $record){
        parent::__construct($record);
        // override some settings
        $this->id = $record->id;
        $this->context = $record->context;
        $this->firstOccurrenceDate = $record->firstOccurrenceDate;
    }
}