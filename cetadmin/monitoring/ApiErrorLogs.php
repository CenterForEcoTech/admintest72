<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."functions.php");
$currentRequest = new Request();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "DELETE is not supported";
        break;
    case "GET":
        include_once($siteRoot."repository/mySqlProvider/ErrorLoggingProvider.php");
        include_once("model/_errorLogModel.php");
        $provider = new ErrorLoggingProvider($mysqli);
        $criteria = ErrorLogTableRow::getCriteria($_GET);
        $paginatedErrors = $provider->get($criteria);
        $errorItems = $paginatedErrors->collection;
        $results = array(
            "iTotalRecords" => $paginatedErrors->totalRecords,
            "iTotalDisplayRecords" => $paginatedErrors->totalRecords,
            "sEcho" => $_GET["sEcho"]
        );
        foreach($errorItems as $errorItem){
            $results["aaData"][] = new ErrorLogTableRow($errorItem);
        }
        if (!isset($results["aaData"])){
            $results["aaData"] = array();
        }
        output_json($results);
        die();
        break;
    case "POST":
        echo "POST is not supported";
        break;
    case "PUT":
        echo "PUT is not supported";
        break;
}
output_json($results);
die();
?>