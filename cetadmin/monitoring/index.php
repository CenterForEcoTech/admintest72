<?php
include_once("../_config.php");
$pageTitle = "Admin - Monitoring";
?>
<html>
<?php include("../_header.php");
?>
<div class="container">
    <h1>Monitoring and Logs</h1>
    <?php

    $nav = isset($_GET["nav"]) ? $_GET["nav"] : "error-logs";
    include_once("_monitoringMenu.php");
    if ($nav){
        switch ($nav){
            case "login-history":
                include("views/_loginHistory.php");
                break;
            case "event-logs":
                include("views/_eventLogs.php");
                break;
            case "admin-logs":
                include("views/_adminErrorLogs.php");
                break;
            case "email-logs":
                include("views/_emailLogs.php");
                break;
            case "error-log-detail":
                include("views/_errorLogDetail.php");
                break;
            case "error-logs":
            default:
                include("views/_errorLogs.php");
                break;
        }
    }
    ?>
</div> <!-- end container -->
<?php
include("../_footer.php");
?>