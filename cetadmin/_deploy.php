<?php
$headlog = nl2br(file_get_contents('deployment_head.log'));
$deployments = nl2br(file_get_contents('deployments.log'));
$deployments = explode("*************************",$deployments);
$latestDeployment = $deployments[count($deployments)-2];
$latestDeploymentBranch = explode("'",$latestDeployment);
$latestBranch = $latestDeploymentBranch[1];

function selectEnvironmentForDeploy($envArray){
    global $Config_environmentName;
	global $latestBranch;
	
    if (in_array($latestBranch, $envArray)){
        echo "selected";
    }
}

$deployBranchArray = array('default','Staging','Lauren','Yosh','Consultant');
?>

<h2>Deploy updates</h2>
<p>
    Select the branch you want to deploy and click "Deploy Updates." There will be a slight pause while the update processes ,<br>
    then you will be taken to a different page where you will have the option to undo the deployment.<br>From there use the back button if you do not wish to undo the deployment.
</p>
<form method="post" action="deploy.php">
    <input type="hidden" name="readystate" value="yesImready">
    Choose which branch:
    <select name="repo">
		<?php
			foreach ($deployBranchArray as $deployBranch){
				echo "<option value='".$deployBranch."'".($deployBranch == $latestBranch ? " selected" : "").">".$deployBranch.($deployBranch == $latestBranch ? " [Currently Deployed]" : "")."</option>";
				
			}
		
		?>
    </select>
    <input type="submit" value="Deploy Updates">
</form>
<?php
		
		echo "Currently Deployed Branch: ".$latestBranch."<br>";
		$headlog_array = explode("changeset:",$headlog);

    //there's no deployment log available, query hg for latest info & output
    if(empty($headlog)){
      $hg_log_latest = null;
      exec('hg log -l 1', $hg_log_latest);
      echo "<ul>\n";
      foreach($hg_log_latest as $k => $v){
        echo "<li>" . $v . "</li>\n";
      }
      echo "</ul>\n";
    }
    
		$c = 0;
		while ($c < count($headlog_array)){
			$changeSetParts = explode("<br />",$headlog_array[$c]);
			if ($latestBranch == "default"){
				if (!strpos($headlog_array[$c],"branch:")){
					echo "<span title='".str_replace("<br />","",$headlog_array[$c])."' style='cursor:pointer;text-decoration:underline;'>ChangeSet: ".$changeSetParts[0]."</span>";
					foreach ($changeSetParts as $changeSet){
						echo strpos($changeSet,"user:")? "<br>".$changeSet:"";
						echo strpos($changeSet,"date:")? "<br>".$changeSet:"";
						echo strpos($changeSet,"summary:")? "<br>".$changeSet:"";
						
					}
				}
			}else{
				if (strpos(str_replace(" ","",$headlog_array[$c]),"branch:".$latestBranch,0)){
					echo "<span title='".str_replace("<br />","",$headlog_array[$c])."' style='cursor:pointer;text-decoration:underline;'>ChangeSet: ".$changeSetParts[0]."</span>";
					foreach ($changeSetParts as $changeSet){
						echo strpos($changeSet,"user:")? "<br>".$changeSet:"";
						echo strpos($changeSet,"date:")? "<br>".$changeSet:"";
						echo strpos($changeSet,"summary:")? "<br>".$changeSet:"";
						
					}
				}
			}
			$c++;
		}


?>