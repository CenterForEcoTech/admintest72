<h2>Invoicing</h2>

<fieldset>
	<h3>Manual Invoicing by Merchant</h3>
	<p>This only creates the Invoice(under the covers, so to speak). It does not provide any other facility for you to view it. You would typically use this feature to test the disbursement process.</p>
	<p>This supports manually processing live invoices to an external system.</p>
	<p>To invoice a merchant, click the Refresh List button to load a list of merchants who can be invoiced right now.</p>
	<input type="button" id="refresh-manual-invoicing" value="Refresh List">
	<span class="warning">* In a production environment, clicking the refresh button can interfere with site performance during heavy traffic.</span>
	<form class="normal">
        <fieldset id="create-invoice" style="width:95%;">
            <legend>Create an Invoice</legend>
		    <div id="manual-invoicing"><?php // container populated by ajax ?></div>
        </fieldset>
	</form>
</fieldset>

<fieldset>
	<h3>Pay Invoices</h3>
	<p>Come here and mark an unpaid invoice as paid.</p>
	<p>Coming Soon!</p>
</fieldset>

<script type="text/template" id="form-header">
	<label>
		<div>Step 1: Select an Invoice Provider<span class="required">*</span></div>
        <span class="styled-select">
            <select id="invoice-provider" name="invoice_provider" class="required">
                <?php if ($isProd){ ?>
                    <option value="BillCom" selected>Bill.Com Live Integration</option>
                <?php } else { ?>
                    <option value="">-- Select One --</option>
                    <option value="mock">Mock Invoice Provider</option>
                    <option value="BillCom">Bill.Com Live Integration</option>
                <?php } ?>
            </select>
        </span>
	</label>
	<label class="info hidden mock-only">
		<input type="checkbox" name="mark_as_paid"> Mark invoices as paid immediately <span class="help">(only works for mock invoicer)</span>
	</label>
    <hr dashed>
    <label><div>Step 2: Generate invoices for specific merchants</div></label>
</script>
<script type="text/template" id="invoice-button">
	<div class="invoices">
		<label style="display:inline;">{1} [{4}]</label>
        <input type='button' data-merchant-id='{0}' data-event-id='{3}' class='invoice-merchant-action' value='Generate ({2} transactions)'>
        <span class="alert"></span>
	</div>
</script>
<script type="text/javascript">
	$(function(){
		var manualInvDiv = $("#manual-invoicing"),
			formHeaderTemplate = $("#form-header").text(),
			htmlRowTemplate = $("#invoice-button").text(),
			url = "manualInvoicing.php",
			loadManualInvoicing = function(){
				var noneText = "<p>No merchants ready to be invoiced!</p>";
                manualInvDiv.html("<p class='alert'>Loading... Please please please wait...</p>");
				$.post(
					url,
					{
						action: "get"
					},
					function(data){
						manualInvDiv.html("");
						if (data.length){
							manualInvDiv.append(formHeaderTemplate);
							$.each(data, function(index, item){
                                var escapedName, textToAppend;
                                if (item.Name){}
								escapedName = item.Name.replace("'", '&apos;').replace('"', '&quot;'),
								textToAppend = htmlRowTemplate
										.replace("{0}", item.ID)
										.replace("{1}", escapedName)
										.replace("{2}", item.Count);
                                if (item.EventID){
                                    textToAppend = textToAppend.replace("{3}", item.EventID);
                                } else {
                                    textToAppend = textToAppend.replace("{3}", 0);
                                }
                                if (item.EventTitle){
                                    textToAppend = textToAppend.replace("{4}", item.EventTitle);
                                } else {
                                    textToAppend = textToAppend.replace("{4}", "all");
                                }
								manualInvDiv.append(textToAppend);
							});
						} else {
							manualInvDiv.append(noneText);
						}
					}
				);
			};

		// init
		$("#refresh-manual-invoicing").click(function(){
			loadManualInvoicing();
		});

        $("#create-invoice").on("change", "#invoice-provider", function(){
            if ($(this).val() == "mock"){
                $(".mock-only").show();
            } else {
                $(".mock-only").hide();
            }
        });

		manualInvDiv.on("click", ".invoice-merchant-action", function(e){
			var element = $(this),
				merchantId = element.attr("data-merchant-id"),
				eventId = element.attr("data-event-id"),
				formDataArray = element.closest("form").serializeArray(),
				alertElement = element.next(".alert");
			e.preventDefault();

			formDataArray.push({
				name: "merchantId",
				value: merchantId
			});
			formDataArray.push({
				name: "eventId",
				value: eventId
			});
			formDataArray.push({
				name: "action",
				value: "generate-invoice"
			});

			$.post(
				url,
				$.param(formDataArray),
				function(data){
					if (data.message){
						alertElement.html(data.message);
					}
					if (data.success){
						// if invoiced, remove the on-click handler & disable the button
						element.removeClass("invoice-merchant-action");
						element.prop("disabled", true);
					}
				}
			);
		});
	});
</script>