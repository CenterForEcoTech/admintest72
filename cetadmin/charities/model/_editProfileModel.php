<?php
include_once($dbProviderFolder."MemberProvider.php");
if ($_GET['id'] && is_numeric($_GET['id'])){
    $provider = new OrganizationMemberProvider($mysqli);
    $record = $provider->getPublicProfile($_GET['id']);
} else {
    $contactRecord = new ContactRecord();
    $record = new OrganizationRecord();
    $record->primaryContact = $contactRecord;
}