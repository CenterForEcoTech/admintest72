<?php
class CharityListTableRow{
    public $DT_RowId;
    public $orgId;
    public $contactInfo;
    public $memberId;
    public $numTransactions;
    public $socialMediaHandles = array();

    public function __construct(OrganizationRecord $record){
        $this->DT_RowId = "charity-".$record->id;
        $this->orgId = $record->id;
        $this->memberId = $record->memberId;
        $this->numTransactions = $record->numTransactions;

        // company
        $contactInfoHtml = $record->name;
        if ($record->address){
            $contactInfoHtml .= "<br>".$record->address;
        }
        if ($record->city){
            $contactInfoHtml .= "<br>".$record->city.", ".$record->state." ".$record->postalCode;
        }
        if ($record->email){
            $contactInfoHtml .= "<br>".$record->primaryContact->email;
        }
        if ($record->phone){
            $contactInfoHtml .= "<br>".$record->phone;
        }
        if ($record->referralCodeText){
            $contactInfoHtml .= "<br> Referral text: ".$record->referralCodeText;
        }
        if ($record->pid){
            $contactInfoHtml .= "<br> PID: ".$record->pid;
        }
        $this->contactInfo = $contactInfoHtml;
    }

    public function addSocialMediaHandle(SocialMediaHandle $handle){
        $this->socialMediaHandles[] = $handle;
    }

    private static $columnMapping = array(
        0 => "orgId",
        1 => "contactInfo",
        2 => "memberId",
        3 => "numTransactions",
        4 => "socialMediaHandles"
    );

    public static function getCriteria($datatablePost){
        $criteria = new OrganizationProfilesCriteria();
        if ($datatablePost['fiscalSponsorEin']){
            $criteria->fiscalSponsorEin = $datatablePost['fiscalSponsorEin'];
        }

        // pagination
        $criteria->start = $datatablePost["iDisplayStart"];
        $criteria->size = $datatablePost["export"] == "csv" ? 0 : $datatablePost["iDisplayLength"];

        // sorting -- at this time, support only one column
        if ($datatablePost["iSortingCols"]){
            // assign the first column indicated as the default
            $sortColumnIndex = $datatablePost["iSortCol_0"];
            $sortColumnOrder = $datatablePost["sSortDir_0"]; // asc or desc
            $criteria->sortColumn = self::$columnMapping[$sortColumnIndex];
            $criteria->sortOrder = $sortColumnOrder;

            // if there are more than one, add them
            if ($datatablePost["iSortingCols"] > 1){
                for ($i = 0; $i < $datatablePost["iSortingCols"]; $i++){
                    $sortColumnIndex = $datatablePost["iSortCol_".$i];
                    $sortColumnOrder = $datatablePost["sSortDir_".$i]; // asc or desc
                    $multiSort = new MultiColumnSort(self::$columnMapping[$sortColumnIndex], $sortColumnOrder);
                    $criteria->multiColumnSort[] = $multiSort;
                }
            }
        }

        // searching -- regex not supported
        if ($datatablePost["sSearch"]){
            $criteria->keyword = $datatablePost["sSearch"];
        }
        foreach (self::$columnMapping as $index => $fieldname){
            $postName = "sSearch_".$index;
            $searchableName = "bSearchable_".$index;
            if ($datatablePost[$postName] !== '' && $datatablePost[$searchableName]){
                $searchValue = $datatablePost[$postName];
                switch ($fieldname){
                    case "memberId":
                        $criteria->memberId = $searchValue;
                        break;
                    default:
                        $criteria->$fieldname = $searchValue;
                }
            }
        }

        return $criteria;
    }
}

class CharityExportRow{
    public static function getRow(OrganizationRecord $record){
        return array(
            "id" => $record->id,
            "firstName" => $record->primaryContact->firstName,
            "lastName" => $record->primaryContact->lastName,
            "phone" => $record->phone,
            "email" => $record->primaryContact->email,
            "timestamp" => $record->timestamp,
            "memberId" => $record->memberId,
            "merchantId" => $record->id,
            "organizationId" => "",
            "companyName" => $record->name,
            "address" => $record->address,
            "city" => $record->city,
            "state" => $record->state,
            "zip" => $record->postalCode,
            "orgEIN" => "",
            "referralCode" => $record->referralCodeText,
            "referredByMemberId" => $record->referredByMemberId,
            "referralCodeText" => $record->referralCodeText,
            "pid" => $record->pid,
            "isFiscallySponsored" => $record->isFiscallySponsored,
            "fiscalSponsor" => $record->fiscalSponsor
        );
    }
}