<?php
include_once("../_config.php");
include_once($dbProviderFolder."MemberProvider.php");
include_once($dbProviderFolder."SocialMediaHandleProvider.php");
include_once($siteRoot."_setupExternalDataConnection.php");
include_once($dbProviderFolder."MySqliCharitySearchProvider.php");
include_once("model/_charityListTableRow.php");

$currentRequest = new Request();

$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results->message = "DELETE is not supported at this time.";
        break;
    case "GET":
        if ($_GET["id"]){
            // TODO: retrieve profile
        } else {
            $provider = new OrganizationMemberProvider($mysqli);
            $criteria = CharityListTableRow::getCriteria($_GET);
            $paginatedResults = $provider->get($criteria);
            $charities = $paginatedResults->collection;

            if ($_GET["export"] == "csv"){
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=CharitiesReport.csv");
                header("Content-Transfer-Encoding: binary");
                $charity_data = array();
                $colHeaders = array();
                foreach ($charities as $charity){
                    $charity_data[] = CharityExportRow::getRow($charity);
                    if (!count($colHeaders)){
                        foreach($charity_data[0] as $column => $value){
                            $colHeaders[] = $column;
                        }
                    }
                }

                $fileContents = exportCSV($charity_data, $colHeaders, true);
                echo $fileContents;
                die();
            } else {
                $socialHandleProvider = new SocialMediaHandleProvider($mysqli);
                $socialCriteria = new SocialMediaHandleCriteria();

                $results = array(
                    "iTotalRecords" => $paginatedResults->totalRecords,
                    "iTotalDisplayRecords" => $paginatedResults->totalRecords,
                    "sEcho" => $_GET["sEcho"]
                );
                foreach($charities as $charity){
                    $model = new CharityListTableRow($charity);
                    $socialCriteria->orgId = $charity->id;
                    $socialCriteria->activeOnly = true;
                    $socialHandles = $socialHandleProvider->get($socialCriteria);
                    foreach ($socialHandles->collection as $handle){
                        $model->addSocialMediaHandle($handle);
                    }
                    $results["aaData"][] = $model;
                }
                if (!isset($results["aaData"])){
                    $results["aaData"] = array();
                }
            }
        }
        break;
    case "POST":
        if ($_POST['edit'] && isset($_POST['id'])){
            $memberProvider = new OrganizationMemberProvider($mysqli);
            $companyRecord = $memberProvider->getPublicProfile($_POST['id']);
            $memberRecord = new OrganizationMemberRecord();
            $memberRecord->id = $companyRecord->memberId;

            $companyRecord->name = htmlspecialchars(htmlspecialchars_decode($_POST["company_name"]));
            $companyRecord->address = htmlspecialchars(htmlspecialchars_decode($_POST["company_address"]));
            $companyRecord->city = htmlspecialchars(htmlspecialchars_decode($_POST["company_city"]));
            $companyRecord->state = $_POST["company_state"];
            $companyRecord->postalCode = $_POST["company_zip"];
            $companyRecord->country = $_POST['company_country'];
            $companyRecord->phone = $_POST["company_phone"];
            $companyRecord->website = $_POST["company_website"];
            $companyRecord->descriptionText = $_POST["company_description"];
            $companyRecord->isDescriptionPublic = strtolower($_POST["company_descriptionshow"]) == "yes";

            $companyRecord->lat = $_POST['company_lat'];
            $companyRecord->long = $_POST['company_long'];

            //recreate lat long on address update
            if ($_POST['recalc_geo']){
                $GeoCode = GetGeoCode(chkstring($_POST["company_address"]).",".chkstring($_POST["company_city"]).",".chkstring($_POST["company_state"])." ".chkstring($_POST["company_zip"]));
                $GeoCodeLatLong = explode(",",$GeoCode);
                $getLat = $GeoCodeLatLong[0];
                $getLong = $GeoCodeLatLong[1];
                $companyRecord->lat = $getLat;
                $companyRecord->long = $getLong;
            }

            /* @var $companyContactRecord ContactRecord */
            $companyContactRecord = $companyRecord->primaryContact;
            if (!$memberRecord->id){
                $companyContactRecord->firstname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactfirstname"]));
                $companyContactRecord->lastname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactlastname"]));
                $companyContactRecord->title = htmlspecialchars(htmlspecialchars_decode($_POST["company_contacttitle"]));
                $companyContactRecord->setPhone($_POST["company_contactphone"]);
                $companyContactRecord->email = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
                $companyContactRecord->emailConfirm = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            }

            // ========= server-side validation ===========
            // Rule: Required fields:
            //  company name, address, city, state, zip
            $requiredFields = array(
                "company_name",
                "company_address",
                "company_city",
                "company_state",
                "company_zip"
            );
            $validationMessages = array();
            $results->validationMessages = $validationMessages;
            foreach ($requiredFields as $fieldName){
                if ($_POST[$fieldName]){
                    // good
                } else {
                    $validationMessages[] = (object)array(
                        "name" => $fieldName,
                        "message" => "This is a required field"
                    );
                }
            }
            if (count($validationMessages) == 0){
                //Update Members Profile
                $results->success = $memberProvider->updateOrganizationAsMember($companyRecord, $memberRecord);
                if ($results->success){
                    $results->id = $companyRecord->id;
                    $memberProvider->updateOrganizationDescription($companyRecord);
                } else {
                    $results->message = "Update failed";
                }
            }
        } else if ($_POST['add'] && !$_POST['id']){
            $memberProvider = new OrganizationMemberProvider($mysqli);

            $companyRecord = new OrganizationRecord();
            $companyRecord->name = htmlspecialchars(htmlspecialchars_decode($_POST["company_name"]));
            $companyRecord->address = htmlspecialchars(htmlspecialchars_decode($_POST["company_address"]));
            $companyRecord->city = htmlspecialchars(htmlspecialchars_decode($_POST["company_city"]));
            $companyRecord->state = $_POST["company_state"];
            $companyRecord->postalCode = $_POST["company_zip"];
            $companyRecord->country = $_POST['company_country'];
            $companyRecord->phone = $_POST["company_phone"];
            $companyRecord->website = $_POST["company_website"];
            $companyRecord->descriptionText = $_POST["company_description"];
            $companyRecord->isDescriptionPublic = strtolower($_POST["company_descriptionshow"]) == "yes";
            $companyRecord->officialname = $_POST["company_officialname"];
            $companyRecord->isFiscallySponsored = $_POST['isFiscallySponsored'] ? true : false;
            $companyRecord->ein = ($companyRecord->isFiscallySponsored) ? "" : $_POST['ein'];
            $companyRecord->fiscalSponsorEin = ($companyRecord->isFiscallySponsored) ? $_POST['ein'] : "";
            $companyRecord->differentnamethanfiscalsponsor = ($companyRecord->isFiscallySponsored) ? "true" : "";
            $companyRecord->ncesSchoolId = $_POST['ncesSchoolId'];

            $companyRecord->lat = $_POST['company_lat'];
            $companyRecord->long = $_POST['company_long'];

            //recreate lat long on address update
            if ($_POST['recalc_geo']){
                $GeoCode = GetGeoCode(chkstring($_POST["company_address"]).",".chkstring($_POST["company_city"]).",".chkstring($_POST["company_state"])." ".chkstring($_POST["company_zip"]));
                $GeoCodeLatLong = explode(",",$GeoCode);
                $getLat = $GeoCodeLatLong[0];
                $getLong = $GeoCodeLatLong[1];
                $companyRecord->lat = $getLat;
                $companyRecord->long = $getLong;
            }

            $companyContactRecord = new ContactRecord();
            $companyContactRecord->firstname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactfirstname"]));
            $companyContactRecord->lastname = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactlastname"]));
            $companyContactRecord->title = htmlspecialchars(htmlspecialchars_decode($_POST["company_contacttitle"]));
            $companyContactRecord->setPhone($_POST["company_contactphone"]);
            $companyContactRecord->email = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            $companyContactRecord->emailConfirm = htmlspecialchars(htmlspecialchars_decode($_POST["company_contactemail"]));
            $companyRecord->primaryContact = $companyContactRecord;

            // ========= server-side validation ===========
            // Rule: Required fields:
            //  company name, address, city, state, zip
            $requiredFields = array(
                "company_name",
                "company_address",
                "company_city",
                "company_state",
                "company_zip"
            );
            $results->validationMessages = array();
            foreach ($requiredFields as $fieldName){
                if ($_POST[$fieldName]){
                    // good
                } else {
                    $results->validationMessages[] = (object)array(
                        "name" => $fieldName,
                        "message" => "This is a required field"
                    );
                }
            }
            if (count($results->validationMessages) == 0){
                $orgRecord = $memberProvider->getExistingOrg($companyRecord);
                if ($orgRecord->id){
                    $results->message = "This Organization already exists";
                    $results->id = $orgRecord->id;
                }
            }
            if (count($results->validationMessages) == 0 && !$results->id){
                //Add Members Profile
                $addResponse = $memberProvider->addOrganization(new MySqliCharityLookupProvider($externalDataConn),$companyRecord);
                if ($addResponse->success){
                    $results->id = $addResponse->insertedId;
                } else {
                    $results->message = $addResponse->error;
                }
            }
        } else if ($_POST['action'] == 'social_media') {
            $orgId = $_POST['orgId'];
            $fbHandle = SocialMediaHandle::createFacebookHandle($_POST['facebookPageUrl']);
            $fbHandle->setOrganizationId($orgId);
            $twitterHandle = SocialMediaHandle::createTwitterHandle($_POST['twitterHandle']);
            $twitterHandle->setOrganizationId($orgId);
            $results = array();

            $socialProvider = new SocialMediaHandleProvider($mysqli);
            $savedRecord = $socialProvider->setSingleHandle($fbHandle);
            if ($savedRecord){
                $results[] = $savedRecord;
            }
            $savedRecord = $socialProvider->setSingleHandle($twitterHandle);
            if ($savedRecord){
                $results[] = $savedRecord;
            }

        }
        break;
    case "PUT":
        $results->message = "PUT is not supported at this time";
        break;
}
if ($results->message || count($results->validationMessages)){
    header("HTTP/1.0 409 Conflict");
}
output_json($results);
die();