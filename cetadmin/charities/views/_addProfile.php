<h3>Add Organization Profile</h3>
<p class="help">
    NOTICE: This form only adds an organization so that it can be edited.
    <a class="help-modal do-not-navigate" href="create-charity-help">Help on Creating Charity</a>
</p>

<style type="text/css">
    form label{ display:block;clear:both;}
    .required:after{color:red;content:"*";}
    .required{color:inherit;}
    form fieldset{display:block;clear:both;}
    form input{margin-bottom:4px;}
    form label span{display:inline;}
    .hidden{display:none !important;}
</style>
<form id="edit-charity-form">
    <div id="general-validation-message" class="alert"></div>
    <fieldset class="fourteen columns">
        <legend>Search for IRS charity</legend>
        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                sourceType : "POST",
                showState: true,
                minLength: 3,
                pageLength: 10,
                onSearch: function(event,ui){
                    var form = $("#edit-charity-form");
                    form.find("input.clear-on-search").each(function(index, element){
                        var $element = $(element);
                        if ($element.attr("type") === "checkbox"){
                            $element.prop("disabled", false).prop("checked", false);
                        } else {
                            $element.val("");
                        }
                    });
                },
                onSelect: function( event, ui ) {
                    var record = ui.item.record,
                        form = $("#edit-charity-form"),
                        getInput = function(name){
                            return form.find("input[name='"+name+"']");
                        },
                        loadRecord = function(record){
                            if (record.orgId){
                                getInput("isFiscallySponsored").prop("checked", true).prop("disabled", true);
                                getInput("orgId").val(record.orgId);
                            }
                            getInput("ein").val(record.ein);
                            getInput("company_name").val(record.name);
                            getInput("company_officialname").val(record.name);
                            getInput("company_address").val(record.address1);
                            getInput("company_city").val(record.city);
                            getInput("company_state").val(record.region);
                            getInput("company_zip").val(record.postalCode);
                        };

                    if (record.isChildOrg){
                        alert("Sorry, the organization you chose is a child organization that already exists.");
                    } else if (record.orgId){
                        loadRecord(record);
                    } else {
                        $.ajax({
                            url: CETDASH.CharitySearchWidget.sourceUrl,
                            type: "POST",
                            data: {
                                action: "ein-lookup",
                                ein: record.ein
                            },
                            success: function(data){
                                var message = "";
                                if (data.orgId){
                                    record.orgId = data.orgId;
                                }
                                loadRecord(record);
                            },
                            dataType: 'json'
                        });
                    }
                }
            };
        </script>
        <?php include $siteRoot."views/_charityPickerWidget.php";?>
    </fieldset>
    <fieldset class="fourteen columns">
        <legend>External Identifiers</legend>
        <label>
            <div class="two columns">EIN:</div>
            <input type="text" name="ein" class="clear-on-search">
        </label>
        <label>
            <div class="two columns">&nbsp;</div>
            <input type="checkbox" name="isFiscallySponsored" value="1" class="clear-on-search">
            <span>Is Fiscally Sponsored by the organization identified by the EIN above.</span>
        </label>
        <label>
            <div class="three columns">NCES School ID:</div>
            <input type="text" name="ncesSchoolId">
        </label>
    </fieldset>
    <input type="hidden" name="add" value="1">
    <input type="hidden" name="orgId" class="clear-on-search">
    <fieldset class="fourteen columns">
        <legend>Basic Information</legend>
        <label>
            <div class="two columns required">Name:</div>
            <input class="eight columns" type="text" name="company_name">
            <div class="alert"></div>
            <a class="help-modal do-not-navigate" href="name-help">Help on Creating Charity</a>
        </label>
        <label>
            <div class="three columns required">Official Name:</div>
            <input class="seven columns" type="text" name="company_officialname">
        </label>
        <label>
            <div class="two columns required">Address:</div>
            <input class="eight columns affects-geo" type="text" name="company_address">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">City:</div>
            <input class="seven columns affects-geo" type="text" name="company_city">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">State:</div>
            <input class="four columns affects-geo" type="text" name="company_state">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">Zip:</div>
            <input class="three columns affects-geo" type="text" name="company_zip">
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns required">Country:</div>
            <input class="two columns" type="text" name="company_country" value="US">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Charity Phone:</div>
            <input class="four columns" type="text" name="company_phone">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Charity Website:</div>
            <input class="seven columns" type="text" name="company_website">
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">Charity Description:</div>
            <textarea class="eight columns" name="company_description"></textarea>
            <div class="alert"></div>
        </label>
        <label>
            <div class="four columns">&nbsp;</div>
            <input type="checkbox" name="company_descriptionshow" value="yes" >
            <span>Show description on public profile?</span>
        </label>
    </fieldset>
    <fieldset>
        <legend>Geographic Coordinates</legend>
        <label>
            <div class="two columns">&nbsp;</div>
            <input type="checkbox" name="recalc_geo" value="1" checked>
            <span>Recalculate geographic coordinates.</span>
        </label>
        <label>
            <div class="two columns">Latitude:</div>
            <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_lat" title="Lat (calculated if left blank)">
        </label>
        <label>
            <div class="two columns">Longitude:</div>
            <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_long" title="Long (calculated if left blank)">
        </label>
    </fieldset>
    <fieldset>
        <legend>Contact Info</legend>
        <label>
            <div class="two columns">First Name:</div>
            <input class="seven columns" type="text" name="company_contactfirstname" >
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Last Name:</div>
            <input class="seven columns" type="text" name="company_contactlastname" >
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Job title:</div>
            <input class="seven columns" type="text" name="company_contacttitle" >
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Phone:</div>
            <input class="seven columns" type="text" name="company_contactphone" >
            <div class="alert"></div>
        </label>
        <label>
            <div class="two columns">Email:</div>
            <input class="seven columns" type="text" name="company_contactemail" >
            <div class="alert"></div>
        </label>
    </fieldset>
    <div>
        <a href="#" id="save-button" class="button-link">Save</a>
    </div>
</form>
<div style="display:none;">
    <div id="create-charity-help" title="Create Charity">
        <h3>Creating a new charity</h3>
        <p>This form allows you to create a new charity (it does not register the charity for you).</p>
        <p>Use the search box to find an IRS charity that you want to create, or for which you want to create a child entity.</p>
        <p>Or, manually enter the data for the new charity. If you use the IRS charity search, the address will typically be filled in for you.</p>
    </div>
    <div id="name-help" title="Names">
        <h3>Name and Official Name</h3>
        <p>For 501(c)(3) nonprofit organizations and schools, "Name" should be their commonly known organization name, and "Official Name" should be their official IRS or NCES name.</p>
        <p>For a child organization, "Name" should be the child's name, and "Official Name" should be the parent's official IRS name.</p>
    </div>
</div>
<script type="text/javascript">
$(function(){
    var regForm = $("#edit-charity-form"),

    // login availability
        loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
        loginAvailableString = "Login Available",
        checkLoginAvail = function(value, alertElement, element, confirmElement){
            $.get(loginAvailUrl,
                {
                    IndividualLogin: value
                },
                function(data){
                    if (data !== loginAvailableString){
                        alertElement.html(data);
                        if (confirmElement){
                            confirmElement.val("");
                        }
                        element.focus();
                    }
                });
        };

    regForm.on("click.CETDASH", "#save-button", function(e){
        var url = "ApiCharityProfile.php",
            isFiscallySponsored = $("input[name='isFiscallySponsored']"),
            forceFiscallySponsored = isFiscallySponsored.prop("disabled");
        isFiscallySponsored.prop("disabled", false);
        e.preventDefault();
        $.ajax({
            url: url,
            data: regForm.serialize(),
            type: "POST",
            success: function(data){
                window.location = "?nav=profile-edit&id=" + data.id;
            },
            error: function(jqXHR, textStatus, errorThrown){
                var data = $.parseJSON(jqXHR.responseText),
                    generalValidationElement = $("#general-validation-message");
                isFiscallySponsored.prop("disabled", forceFiscallySponsored);
                generalValidationElement.html("");
                if (data.validationMessages && $.isArray(data.validationMessages) && data.validationMessages.length){
                    CETDASH.forms.handleStandardErrors(regForm, data.validationMessages);
                } else if (data.message){
                    generalValidationElement.html(data.message);
                }
                CETDASH.scrollTo(generalValidationElement);
            }
        });
    });

    regForm.on("change.CETDASH", ".affects-geo", function(e){
        var editedElement = $(this),
            onlyIfBlank = editedElement.hasClass("if-blank"),
            overrideCheckbox = regForm.find("input[name='recalc_geo']");
        if (onlyIfBlank){
            if (editedElement.val().trim() === ""){
                overrideCheckbox.prop("checked", true);
            } else {
                overrideCheckbox.prop("checked", false);
            }
        } else {
            overrideCheckbox.prop("checked", true);
        }
    });

    $("input[name='company_contactphone'], input[name='company_phone']").blur(function(){
        var el = $(this),
            value = el.val().replace(/[^\d]/g, ""),
            alertElement = el.closest("label").find(".alert"),
            areacodes = Array(<?php echo $Config_areacodes;?>),
            areacode = Number(value.substr(0,3)),
            areacodeFound = $.inArray(areacode, areacodes) >= 0;
        alertElement.html("");

        el.val(value); // replace with just numbers

        if (value){
            //check to see if first three digits are a valid area code
            if (!areacodeFound){
                alertElement.html("Area Code is invalid");
                el.focus();
                return false;
            }else {
                if (value.length != 10){
                    if (value.length < 10){
                        alertElement.html("Phone numbers must be 10 digits");
                        el.focus();
                        return false;
                    }else{
                        alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                        el.val(value.substr(0,10));
                        return false;
                    }
                } else {
                    if (el.attr("name") === "company_contactphone"){
                        checkLoginAvail(value, alertElement, el);
                    }
                    return true;
                }
            }
        }else{
            return true;
        }
    });

    $("input[name='company_contactemail']").blur(function(){
        var el = $(this),
            elId = el.attr("id"),
            confirmEl = $("#" + elId + "Confirm"),
            value = el.val(),
            emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,
            alertElement = el.closest("label").find(".alert");
        alertElement.html("");

        if ($.trim(value) && emailReg.test(value)){
            checkLoginAvail(value, alertElement, el, confirmEl);
        }
    });
});
</script>