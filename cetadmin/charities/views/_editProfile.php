<?php
/* @var $record OrganizationRecord */
/* @var $record->primaryContact ContactRecord */
?>
    <h3>Edit Organization Profile</h3>
    <p class="help">NOTICE: This form only edits a organization's profile information. Nor can you modify a member's email, phone, or password (on the member record).</p>
    <style type="text/css">
        form label{ display:block;clear:both;}
        .required:after{color:red;content:"*";}
        .required{color:inherit;}
        form fieldset{display:block;clear:both;}
        form input{margin-bottom:4px;}
        form label span{display:inline;}
        .hidden{display:none !important;}
    </style>
<?php if ($record->id !== getOrganizationId()) {?>
    <p>
        <input type="password" id="impersonate-pwd-top" class="auto-watermarked" title="impersonation password">
        <a href="#accessDenied" data-pwd-field="#impersonate-pwd-top" class="impersonate-org button-link do-not-navigate">Impersonate Member</a>
    </p>
<?php } ?>
    <form id="edit-charity-form">
        <div id="general-validation-message" class="alert"></div>
        <fieldset>
            <legend>Read Only</legend>
            <label>
                <div class="two columns">PID:</div>
                <span id="company_pid_display" class="twelve columns"><a href="<?php echo $CurrentServer."ct/".$record->pid;?>" target="_blank"><?php echo $record->pid;?></a></span>
            </label>
            <?php
            $einDisplay = $record->ein;
            if ($record->isFiscallySponsored){
                $einDisplay = "(Fiscally sponsored: <a href='?nav=profile-edit&id=".$record->parentOrgId."' target='_blank'>".$record->fiscalSponsorEin. "</a>)";
            }
            ?>
            <label>
                <div class="two columns">EIN:</div>
                <span class="twelve columns"><span id="ein_display"><?php echo $einDisplay;?></span></span>
            </label>
            <?php
            $ncesDisplay = "";
            if ($record->ncesSchoolId){
                $ncesDisplay = "<a href='http://nces.ed.gov/ccd/schoolsearch/school_list.asp?SchoolID=$record->ncesSchoolId' target='_blank'>$record->ncesSchoolId</a>";
            }
            ?>
            <label>
                <div class="three columns">NCES School ID:</div>
                <span class="eleven columns"><span id="ein_display"><?php echo $ncesDisplay;?></span></span>
            </label>
        </fieldset>
        <input type="hidden" name="edit" value="<?php echo $record->id ? "1" : "";?>">
        <input type="hidden" name="add" value="<?php echo $record->id ? "" : "1";?>">
        <input type="hidden" name="id" value="<?php echo $record->id;?>">
        <input type="hidden" name="company_pid" value="<?php echo $record->pid;?>">
        <input type="hidden" name="company_officialname" value="<?php echo $record->officialname;?>">
        <fieldset class="fourteen columns">
            <legend>Basic Information</legend>
            <label>
                <div class="two columns required">Name:</div>
                <input class="eight columns" type="text" name="company_name" value="<?php echo $record->name;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns required">Address:</div>
                <input class="eight columns affects-geo" type="text" name="company_address" value="<?php echo $record->address;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns required">City:</div>
                <input class="seven columns affects-geo" type="text" name="company_city" value="<?php echo $record->city;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns required">State:</div>
                <input class="four columns affects-geo" type="text" name="company_state" value="<?php echo $record->state;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns required">Zip:</div>
                <input class="three columns affects-geo" type="text" name="company_zip" value="<?php echo $record->postalCode;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns required">Country:</div>
                <input class="two columns" type="text" name="company_country" value="<?php echo $record->country ? $record->country : "US";?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="four columns">Charity Phone:</div>
                <input class="four columns" type="text" name="company_phone" value="<?php echo $record->phone;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="four columns">Charity Website:</div>
                <input class="seven columns" type="text" name="company_website" value="<?php echo $record->website;?>">
                <div class="alert"></div>
            </label>
            <label>
                <div class="four columns">Charity Description:</div>
                <textarea class="eight columns" name="company_description"><?php echo $record->rawDescriptionText;?></textarea>
                <div class="alert"></div>
            </label>
            <label>
                <div class="four columns">&nbsp;</div>
                <input type="checkbox" name="company_descriptionshow" value="yes" <?php echo $record->isDescriptionPublic ? "checked" : "";?>
                <span>Show description on public profile?</span>
            </label>
        </fieldset>
        <fieldset>
            <legend>Geographic Coordinates</legend>
            <label>
                <div class="two columns">&nbsp;</div>
                <input type="checkbox" name="recalc_geo" value="1" <?php echo ($record->lat && $record->long) ? "" : "checked"?>>
                <span>Recalculate geographic coordinates.</span>
            </label>
            <label>
                <div class="two columns">Latitude:</div>
                <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_lat" value="<?php echo $record->lat;?>" title="Lat (calculated if left blank)">
            </label>
            <label>
                <div class="two columns">Longitude:</div>
                <input class="four columns auto-watermarked affects-geo if-blank" type="text" name="company_long" value="<?php echo $record->long;?>" title="Long (calculated if left blank)">
            </label>
        </fieldset>
        <?php
        $contactFieldStatus = "";
        if ($record->memberId){
            $contactFieldStatus = "disabled";
        }
        ?>
        <fieldset>
            <legend>Contact Info</legend>
            <p class="help">If contact fields are disabled, this means this charity is registered, and you must edit their contact info using the standard edit profile form.</p>
            <label>
                <div class="two columns">First Name:</div>
                <input class="seven columns" type="text" name="company_contactfirstname" value="<?php echo $record->primaryContact->firstname;?>" <?php echo $contactFieldStatus?>>
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns">Last Name:</div>
                <input class="seven columns" type="text" name="company_contactlastname" value="<?php echo $record->primaryContact->lastname;?>"<?php echo $contactFieldStatus?>>
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns">Job title:</div>
                <input class="seven columns" type="text" name="company_contacttitle" value="<?php echo $record->primaryContact->title;?>"<?php echo $contactFieldStatus?>>
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns">Phone:</div>
                <input class="seven columns" type="text" name="company_contactphone" value="<?php echo $record->primaryContact->phone;?>"<?php echo $contactFieldStatus?>>
                <div class="alert"></div>
            </label>
            <label>
                <div class="two columns">Email:</div>
                <input class="seven columns" type="text" name="company_contactemail" value="<?php echo $record->primaryContact->email;?>"<?php echo $contactFieldStatus?>>
                <div class="alert"></div>
            </label>
        </fieldset>
        <div>
            <a href="#" id="save-button" class="button-link">Save</a>
        </div>
    </form>
    <script type="text/javascript">
    $(function(){
        var regForm = $("#edit-charity-form"),

        // login availability
            loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
            loginAvailableString = "Login Available",
            checkLoginAvail = function(value, alertElement, element, confirmElement){
                $.get(loginAvailUrl,
                    {
                        IndividualLogin: value
                    },
                    function(data){
                        if (data !== loginAvailableString){
                            alertElement.html(data);
                            if (confirmElement){
                                confirmElement.val("");
                            }
                            element.focus();
                        }
                    });
            };

        regForm.on("click.CETDASH", "#save-button", function(e){
            var url = "ApiCharityProfile.php";
            e.preventDefault();
            $.ajax({
                url: url,
                data: regForm.serialize(),
                type: "POST",
                success: function(data){
                    window.location = "?nav=profile-edit&id=" + data.id;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var data = $.parseJSON(jqXHR.responseText),
                        generalValidationElement = $("#general-validation-message");
                    generalValidationElement.html("");
                    if (data.validationMessages && $.isArray(data.validationMessages) && data.validationMessages.length){
                        CETDASH.forms.handleStandardErrors(regForm, data.validationMessages);
                    } else if (data.message){
                        generalValidationElement.html(data.message);
                    }
                }
            });
        });

        regForm.on("change.CETDASH", ".affects-geo", function(e){
            var editedElement = $(this),
                onlyIfBlank = editedElement.hasClass("if-blank"),
                overrideCheckbox = regForm.find("input[name='recalc_geo']");
            if (onlyIfBlank){
                if (editedElement.val().trim() === ""){
                    overrideCheckbox.prop("checked", true);
                } else {
                    overrideCheckbox.prop("checked", false);
                }
            } else {
                overrideCheckbox.prop("checked", true);
            }
        });

        $("input[name='company_contactphone'], input[name='company_phone']").blur(function(){
            var el = $(this),
                value = el.val().replace(/[^\d]/g, ""),
                alertElement = el.closest("label").find(".alert"),
                areacodes = Array(<?php echo $Config_areacodes;?>),
                areacode = Number(value.substr(0,3)),
                areacodeFound = $.inArray(areacode, areacodes) >= 0;
            alertElement.html("");

            el.val(value); // replace with just numbers

            if (value){
                //check to see if first three digits are a valid area code
                if (!areacodeFound){
                    alertElement.html("Area Code is invalid");
                    el.focus();
                    return false;
                }else {
                    if (value.length != 10){
                        if (value.length < 10){
                            alertElement.html("Phone numbers must be 10 digits");
                            el.focus();
                            return false;
                        }else{
                            alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                            el.val(value.substr(0,10));
                            return false;
                        }
                    } else {
                        if (el.attr("name") === "company_contactphone"){
                            checkLoginAvail(value, alertElement, el);
                        }
                        return true;
                    }
                }
            }else{
                return true;
            }
        });

        $("input[name='company_contactemail']").blur(function(){
            var el = $(this),
                elId = el.attr("id"),
                confirmEl = $("#" + elId + "Confirm"),
                value = el.val(),
                emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,
                alertElement = el.closest("label").find(".alert");
            alertElement.html("");

            if ($.trim(value) && emailReg.test(value)){
                checkLoginAvail(value, alertElement, el, confirmEl);
            }
        });

        $(".impersonate-org").click(function(e){
            var self = $(this),
                memberId = <?php echo ($record->memberId) ? $record->memberId : 0; ?>,
                pwdField = $(self.data("pwd-field")),
                pwd = pwdField.val(),
                postData = {
                    'action':'impersonate',
                    'memberId': memberId,
                    'password': pwd
                };
            e.preventDefault();
            $.ajax({
                url:'<?php echo $CurrentServer.$adminFolder;?>reports/ApiMemberRegistrations.php',
                type:'POST',
                data: JSON.stringify(postData),
                success: function(data){
                    alert(data.message);
                    window.location.reload();
                },
                error: function(response){
                    var data = JSON.parse(response.responseText);
                    alert(data.message);
                    pwdField.highlight();
                }
            });
        });
    });
    </script>
<?php if ($record->hasChildren){ ?>
    <h4>Child Entities</h4>
    <table id="charity-list">
        <thead>
        <tr id="search-inputs">
            <td class="ui-state-default">&nbsp;</td>
            <td class="ui-state-default">&nbsp;</td>
            <td class="ui-state-default">&nbsp;</td>
        </tr>
        <tr>
            <th>Id</th>
            <th>Contact Info</th>
            <th>Member Id</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
    <script type="text/javascript">
        $(function(){
            var tableElement = $("#charity-list"),
                apiUrl = "ApiCharityProfile.php",
                lastRequestedCriteria = null,
                oTable = tableElement.dataTable({
                    "bJQueryUI": true,
                    "bServerSide":true,
                    "sAjaxSource": apiUrl,
                    "aaSorting": [[ 1, "asc" ]],
                    "aoColumns" :[
                        {
                            "mData": "orgId",
                            "mRender": function ( data, type, full ) {
                                return '<a href="?nav=profile-edit&id='+data+'" target="_blank">'+data+'</a>';
                            }
                        },
                        {"mData": "contactInfo"},
                        {"mData": "memberId", "sWidth": "5%"}
                    ],
                    "aoColumnDefs": [
                        { "bSearchable": false, "aTargets": [ 0, 2 ] }
                    ],
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "fiscalSponsorEin", "value": "<?php echo $record->ein;?>" } );
                    },
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        lastRequestedCriteria = aoData;
                        oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        } );
                    }
                });
        });
    </script>
<?php } ?>