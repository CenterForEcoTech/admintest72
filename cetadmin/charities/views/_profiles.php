<style type="text/css">
    .ui-autocomplete > li { height: 40px; z-index:9999;}
    td.impersonation:hover,
    td.transaction-count:hover,
    td.social-media:hover {cursor:pointer;outline:groove;}
</style>
<h3>Organization Profiles</h3>
<p>Click in the Id column to edit the profile.</p>
<p>Click "transactions" to view this charity's related transactions.</p>

<p>
    To Impersonate a member,
<ol style="list-style-type: decimal;">
    <li><label>Type the password here: <input type="password" id="impersonate-pwd" class="auto-watermarked" title="password for impersonation"></label></li>
    <li>Click on the member's ID</li>
    <li>Once you get confirmation that you are logged in as that member, navigate to the website.</li>
</ol>
</p>
<p>
    <a id="export-to-csv" href="#" title="export current records" class="button-link do-not-navigate">Export</a>
</p>
<table id="charity-list">
    <thead>
    <tr id="search-inputs">
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default"><input class="auto-watermarked" title="Member Id" data-target-column="4"></td>
        <td class="ui-state-default">&nbsp;</td>
        <td class="ui-state-default">&nbsp;</td>
    </tr>
    <tr>
        <th>Id</th>
        <th>Contact Info</th>
        <th>Member Id</th>
        <th>Transactions</th>
        <th>Social Media Handles</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="hidden" id="edit-social-media-form" title="Edit Social Media Handles">
    <p>Assign social media handles please</p>
    <label>
        <div>Facebook Page Url or Page ID:</div>
        <input type="text" name="facebookPageUrl">
    </label>
    <label>
        <div>Twitter handle:</div>
        <input type="text" name="twitterHandle">
    </label>
    <input type="hidden" name="org_id">
</div>

<script type="text/javascript">
$(function(){
    var tableElement = $("#charity-list"),
        apiUrl = "ApiCharityProfile.php",
        searchInputs = $("#search-inputs input"),
        asInitVals = new Array(),
        lastRequestedCriteria = null,
        renderSocialMediaText = function(record){
            var template = "<div class='{networkName}-handle' data-orig='{origText}'>{networkName}: {handle}</div>",
                text = template.replace(/{networkName}/g, record.socialNetworkName)
                    .replace(/{handle}/g, record.socialHandle)
                    .replace(/{origText}/g, record.originalIdentifierText);
            return text;
        },
        oTable = tableElement.dataTable({
            "bJQueryUI": true,
            "bServerSide":true,
            "sAjaxSource": apiUrl,
            "aaSorting": [[ 0, "desc" ]],
            "aoColumns" :[
                {
                    "mData": "orgId",
                    "mRender": function ( data, type, full ) {
                        return '<a href="?nav=profile-edit&id='+data+'">'+data+'</a>';
                    }
                },
                {"mData": "contactInfo"},
                {"mData": "memberId", "sWidth": "5%", "sClass": "impersonation"},
                {"mData": "numTransactions", "sWidth" : "50px", "sClass": "transaction-count"},
                {
                    "mData": "socialMediaHandles",
                    "sWidth": "5%",
                    "sClass": "social-media",
                    "mRender": function(data, type, full){
                        var renderedText = "", record;
                        for (var i = 0; i < data.length; i ++ ){
                            record = data[i];
                            renderedText += renderSocialMediaText(record);
                        }
                        return renderedText;
                    }
                }
            ],
            "aoColumnDefs": [
                { "bSearchable": false, "aTargets": [ 0, 2, 3 ] },
                { "bSortable": false, "aTargets": [ 3, 4 ] }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                lastRequestedCriteria = aoData;
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            }
        }),
        editSocialMediaForm = $("#edit-social-media-form"),
        facebookPageUrlField = editSocialMediaForm.find("input[name='facebookPageUrl']"),
        twitterHandlefield = editSocialMediaForm.find("input[name='twitterHandle']"),
        editSocialMediaOrgIdField = editSocialMediaForm.find("input[name='org_id']");

    tableElement.on("click", "td.transaction-count", function(e){
        var cell = $(this),
            rowId = cell.closest("tr").attr("id"),
            dataId = rowId.replace("charity-", ""),
            transactionReportUrl = '<?php echo $CurrentServer.$adminFolder;?>reports/?nav=transaction-report&SelectedOrgID=' + dataId,
            windowName = 'transactions-for-'+dataId;
        e.preventDefault();
        window.open(transactionReportUrl, windowName);
    });

    editSocialMediaForm.dialog({
        autoOpen: false,
        height: 320,
        width:400,
        modal:true,
        buttons: {
            "Save": function(){
                var facebookPageUrl = facebookPageUrlField.val(),
                    twitterHandle = twitterHandlefield.val(),
                    orgId = editSocialMediaOrgIdField.val();

                $.ajax({
                    url: "ApiCharityProfile.php",
                    type: "POST",
                    data: {
                        "orgId":orgId,
                        "facebookPageUrl":facebookPageUrl,
                        "twitterHandle":twitterHandle,
                        "action":"social_media"
                    },
                    success: function(data){
                        var rowSelector = "#charity-" + orgId,
                            row = $(rowSelector),
                            cell = row.find("td.social-media"),
                            record, renderedText = "";
                        if ($.isArray(data) && cell.length) {
                            if (data.length){
                                for (var i = 0; i < data.length; i++){
                                    record = data[i];
                                    renderedText += renderSocialMediaText(record);
                                }
                                cell.html(renderedText);
                            } else {
                                cell.html("");
                            }
                        }
                        editSocialMediaForm.dialog("close");
                    }
                });
            },
            "Cancel": function(){
                editSocialMediaForm.dialog("close");
            }
        },
        close: function(){
            facebookPageUrlField.val("");
            twitterHandlefield.val("");
            editSocialMediaOrgIdField.val("");
        }
    });

    oTable.on("click", "td.social-media", function(e){
        var cell = $(this),
            rowId = cell.closest("tr").attr("id"),
            dataId = rowId.replace("charity-", ""),
            facebookPageUrl = cell.find(".facebook-handle").data("orig"),
            twitterHandle = cell.find(".twitter-handle").data("orig");
        editSocialMediaOrgIdField.val(dataId);
        if (facebookPageUrl){
            facebookPageUrlField.val(facebookPageUrl);
        }
        if (twitterHandle){
            twitterHandlefield.val(twitterHandle);
        }
        editSocialMediaForm.dialog("open");
    });

    searchInputs.keyup( function () {
        var element = $(this),
            targetColumn = element.attr("data-target-column");
        oTable.fnFilter( this.value, targetColumn );
    } );
    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    searchInputs.each( function (i) {
        asInitVals[i] = this.value;
    } );

    searchInputs.focus( function () {
        if ( this.className == "search_init" )
        {
            this.className = "";
            this.value = "";
        }
    } );

    searchInputs.blur( function (i) {
        if ( this.value == "" )
        {
            this.className = "search_init";
            this.value = asInitVals[searchInputs.index(this)];
        }
    } );

    $("#export-to-csv").click(function(e){
        var aoData = lastRequestedCriteria.slice(0);
        e.preventDefault();

        // add our export parameter
        aoData.push({
            "name" : "export",
            "value" : "csv"
        });
        window.location = apiUrl + "?" + $.param(aoData);
    });

    tableElement.on("click", "td.impersonation", function(e){
        var cell = $(this),
            dataId = cell.html(),
            pwdField = $("#impersonate-pwd"),
            pwd = pwdField.val(),
            postData = {
                "action": "impersonate",
                "memberId": dataId,
                "password": pwd
            };
        if (dataId > 0){
            $.ajax({
                url:'<?php echo $CurrentServer.$adminFolder;?>reports/ApiMemberRegistrations.php',
                type: "POST",
                data: JSON.stringify(postData),
                success: function(data){
                    alert(data.message);
                },
                error: function(response){
                    var data = JSON.parse(response.responseText);
                    alert(data.message);
                    pwdField.highlight();
                }
            });
        } else {
            alert("You cannot impersonate an unregistered organization! Sorry!");
        }
    });
});
</script>