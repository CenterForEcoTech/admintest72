<?php
/**
 * This template always outputs json by calling the output_json function, which is expected to kill the process immediately.
 */

include_once("_config.php");
include_once("../assert_is_ajax.php");
include_once("../repository/mySqlProvider/Helper.php");
include_once("../functions.php");
$actionValue = strtolower($_POST["action"]);
$getList = $actionValue == "get";
$generateInvoice = $actionValue == "generate-invoice";

// TODO (mwoo 6/14/2012): put this behind an API. not doing it now because BillCom impl isn't easy to move at this time

// get merchants that are ready to be invoiced and return json
if ($getList){
	$listSql = "
		SELECT
			EventTransaction_MerchantID as ID,
			MerchantInfo_Name as Name,
			count(*) as Count
		FROM event_transaction
		JOIN merchant_info ON EventTransaction_MerchantID = MerchantInfo_ID
		WHERE EventTransaction_InvoiceID = 0
		AND EventTransaction_Status != 'Deleted'
		GROUP BY EventTransaction_MerchantID
		ORDER BY MerchantInfo_Name";

	$merchantResults = MySqliHelper::get_result($mysqli, $listSql);

	output_json($merchantResults);
	die();
}

// generate invoice
if ($generateInvoice && $_POST["invoice_provider"]){
	$markAsPaid = isset($_POST["mark_as_paid"]) ? 1 : 0;
	$provider = $_POST["invoice_provider"];
	$merchantId = $_POST["merchantId"];
	$eventId = $_POST["eventId"];
	if (!is_numeric($merchantId)){
		output_json(array( "message" => "invalid merchant id"));
		die();
	}

	$checkMerchantSql = "
		SELECT 1
		FROM merchant_info
		WHERE MerchantInfo_ID = ?";

	$merchantResult = MySqliHelper::get_result($mysqli, $checkMerchantSql, "i", $merchantId);
	if (count($merchantResult) == 0){
		output_json(array( "message" => "invalid merchant id"));
	} else {
        if ($provider == "BillCom"){
            include_once($rootFolder."invoicing/_setupForBillCom.php");
        } else {
            include_once($rootFolder."invoicing/_setupForMockIntegration.php");
        }

        // TODO: allow more than one merchant (requires refactoring of Email_Send.php)
        $merchantIdArray = array($merchantId);
        include($rootFolder."invoicing/_invoiceMerchants.php");

        $successMessage = "";
        $success = false;
        if (isset($resultBatch) && count($resultBatch)){
            // TODO: handle multiple invoices and messaging on the UI.
            // FIXME: This iterates over a batch, but only sets one status value!
            foreach ($resultBatch as $invoiceRecord){
                /* @var $invoiceRecord InvoiceRecord */
                if ($invoiceRecord->getExternalInvoiceId()){
                    $successMessage = "Invoice Generated";
                    $success = true;
                } else {
                    $successMessage = "Failure sending invoice to external integration.";
                }
            }
        }
        if (isset($resultBatch) && $markAsPaid && $provider == "mock"){
            // mark these as paid if specified
            foreach ($resultBatch as $invoiceRecord){
                /* @var $invoiceRecord InvoiceRecord */
                if ($invoiceRecord->getExternalInvoiceId()){
                    /* @var $invoiceProvider InvoiceProvider */
                    $invoiceProvider->markAsPaid($invoiceRecord);
                }
            }
            if ($success){
                $successMessage .= " and Marked Paid.";
            }
        }
        if (isset($logMessages) && count($logMessages)){
            output_json(array( "message" => $logMessages[0], "success" => $success));
        } else {
            output_json(array( "message" => $successMessage, "success" => $success));
        }

    }
}

output_json(array( "message" => "invalid"));
die();
?>
