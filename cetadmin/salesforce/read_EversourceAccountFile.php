<?php
require_once dirname(__FILE__) . '/../excelExportClasses/PHPExcel/IOFactory.php';

$trackingFile = "Eversource_Customer_Extract.xlsx";
$inputFileName = 'exports/'.$trackingFile;

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get customer_account worksheet dimensions
try {
    $sheet = $objPHPExcel->getSheet(0);
} catch (PHPExcel_Exception $e) {
}
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$headerRow = $sheet->rangeToArray('A1:'.$highestColumn.'1', NULL, TRUE, FALSE);
foreach ($headerRow[0] as $col=>$val){
	$headerNumbers[($col+1)]=$val;
}

//  Loop through each row of the worksheet in turn
for ($row = 2; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		$rowsRaw[($row-3)][$headerNumbers[($k+1)]] = $v;
	}
}
//cleanup rows
foreach ($rowsRaw as $rowId=>$rowDetails){
	$rowsCustomerAccounts[] = $rowDetails;
}

echo "<pre>";	
print_r($rowsCustomerAccounts);	

?>