<?php
require_once 'config.php';

session_start();
require_once('rest_functions.php');

$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];
if (!isset($access_token) || $access_token == "") {
	die("Error - access token missing from session!");
}
if (!isset($instance_url) || $instance_url == "") {
	die("Error - instance URL missing from session!");
}
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

if (checkSession($instance_url, $reportId, $access_token)){
	echo "<pre>";
		$reportId = "00OU0000002mva5"; //883 Account - Last Billing Month
		$report883 = retrieve_report_883($instance_url, $reportId, $access_token);
		$fieldNamesArray = $report883["fieldNamesArray"];
		//print_r($fieldNamesArray);
		$reportRowsUnSorted = $report883["rows"];
		$reportRowsRaw = array_orderby($reportRowsUnSorted, 'Upgrade__c.Name', SORT_ASC, 'Upgrade__c.Green_Prospect_Type__c', SORT_ASC);
		//print_r($reportRows);
																		

		$AllMeasuresHeader = 
			array(
				"A"=>array("title"=>"Order#","object"=>"calculate_rowId","width"=>"7"),
				"B"=>array("title"=>"Project ID","object"=>"calculate_projectId","width"=>"10"),
				"C"=>array("title"=>"Project Name","object"=>"calculate_actualAccountName","width"=>"30"),
				"D"=>array("title"=>"Town","object"=>"calculate_town","width"=>"11"),
				"E"=>array("title"=>"Included in Invoice (Month)","object"=>"calculate_invoiceMonth","width"=>"14"),
				"F"=>array("title"=>"Contractor","object"=>"calculate_contractor","width"=>"16"),
				"G"=>array("title"=>"EFI #","object"=>"EFI Item Number","width"=>"12"),
				
				"H"=>array("title"=>"IN(CLINC) or IM(CLCCC)?","object"=>"calculate_clinc","width"=>"11"),
				"I"=>array("title"=>"Measure Detail","object"=>"Measure Description","width"=>"34"),
				"J"=>array("title"=>"Measure Description","object"=>"Measure Category","width"=>"41"),
				"K"=>array("title"=>"Unit Quantity","object"=>"Qty","width"=>"8"),
				"L"=>array("title"=>"Unit Cost","object"=>"Cost/Unit","width"=>"12"),
				"M"=>array("title"=>"Unit Incentive","object"=>"Incentive/Unit","width"=>"12"),
				"N"=>array("title"=>"Annual kWh Savings/Unit","object"=>"kWh Savings/Unit","width"=>"12"),
				"O"=>array("title"=>"Measure Life","object"=>"Measure Life","width"=>"8"),
				"P"=>array("title"=>"Dmd kWh /Unit","object"=>"Demand kWh Reduction","width"=>"8"),
				"Q"=>array("title"=>"Annual kWh Savings","object"=>"Annual kWh Savings","width"=>"8"),
				"R"=>array("title"=>"Lifetime kWh Savings","object"=>"Lifetime kWh Savings","width"=>"8"),
				"S"=>array("title"=>"Total Cost","object"=>"Total Cost","width"=>"14"),
				"T"=>array("title"=>"Total Incentive","object"=>"Total Incentive","width"=>"14")
			);


		$AllJobsHeader = 
			array(
				"A"=>array("title"=>"Order#","object"=>"calculate_rowId"),
				"B"=>array("title"=>"Project ID","object"=>"Upgrade__c.Multifamily_Job_ID__c"),
				"C"=>array("title"=>"Project Name","object"=>"Upgrade__c.Name"),
				"D"=>array("title"=>"Green Prospect Type","object"=>"Upgrade__c.Green_Prospect_Type__c"),
				"E"=>array("title"=>"Address","object"=>"Account.BillingAddress"),
				"F"=>array("title"=>"Town","object"=>"Account.BillingCity"),
				"G"=>array("title"=>"Zip Code","object"=>"Account.BillingPostalCode"),
				
				"H"=>array("title"=>"Phone","object"=>"Account.Phone"),
				"I"=>array("title"=>"Master Meters?","object"=>"calculate_masterMeters"),
				"J"=>array("title"=>"Date of Install","object"=>"Upgrade__c.GP_Stage_Completed__c"),
				"K"=>array("title"=>"Included in Invoice (Month)","object"=>"calculate_invoiceMonth"),
				"L"=>array("title"=>"Primary Heating Fuel","object"=>"Account.Heating_Fuel_Type__c"),
				"M"=>array("title"=>"Total Dwelling Units","object"=>"Account.Total_Dwelling_Units__c"),
				"N"=>array("title"=>"# Units in Contract","object"=>"Upgrade__c.Dwelling_Units_Affected__c"),
				"O"=>array("title"=>"Job Included Audit?","object"=>"calculate_includedAudit"),
				"P"=>array("title"=>"Type of Audit","object"=>"calculate_typeAudit")
			);
			
			
		$rowColors = array("FFE680","F2D9E6","E6F2FF","B3E6CC","F0F0E0","B0F0F","E0E8F0","60C8A0","D07090","D0C0D0","90C830","F0FFFF","60A0A0");	
		$rowCounter = 2; //skip first row for header
		$projectIdCount = 0;
		$colorCount = 0;
		//add some rows
		$Adjustments = array();
		foreach ($reportRowsRaw as $rowId=>$rowDetails){
			$projectName = $rowDetails["Upgrade__c.Name"];
			$JobNameParts = explode("-",$projectName);
			$JobName = trim($JobNameParts[0]);
			$accountId = $rowDetails["accountId"];
			$greenProspects[$JobName][]=$accountId;
			//print_r($greenProspects); //ARRAY DESCRIBE ids associated to an account when doing a lookup for attachments associated to this as the parentId;
			$address = $rowDetails["Account.BillingAddress"];
			$completedDateStrToTime = strtotime($rowDetails["Upgrade__c.GP_Stage_Completed__c"]);
			$invoiceMonth = date("F",strtotime(date("Y")."-".date("m",$completedDateStrToTime)+(date("d",$completedDateStrToTime) > 21 ? 1 : 0)."-01"));
			if ($JobName == $JobNameLast){
				$auditType = $auditType;
				$auditIncludedDisplay = $auditIncludedDisplay;
			}else{
				$auditType = "";
				$auditIncludedDisplay = "";
				$rowColor[$JobName] = $colorCount;
				$colorCount++;
			}
			
			$auditIncluded = (strpos(strtolower($rowDetails["Upgrade__c.Green_Prospect_Type__c"]),"audit") ? true : false);
			if ($auditIncluded){
				$auditType = ($auditType ? $auditType : ($rowDetails["Account.Heating_Fuel_Type__c"]=="Electric" ? "WholeBldg" : "PiggyBack"));
				$auditIncludedDisplay = "yes";
			}else{
				$auditType = "n/a";
				$auditIncludedDisplay = "no";
			}
			
			foreach ($AllJobsHeader as $column=>$headerData){
				$dataDisplay = $rowDetails[$headerData["object"]];
				if (strpos(" ".$headerData["object"],"calculate")){
					$calculateType = str_replace("calculate_","",$headerData["object"]);
					switch ($calculateType){
						case "rowId":
							$dataDisplay = ($rowCounter-1);
							break;
						case "invoiceMonth":
							$dataDisplay = $invoiceMonth;
							break;
						case "includedAudit":
							$dataDisplay = $auditIncludedDisplay;
							break;
						case "typeAudit":
							$dataDisplay = $auditType;
							break;
						default:
							$dataDisplay = $headerData["object"];
							break;
					}
				}else{
					$objectType = $headerData["object"];
					switch ($objectType){
						case "Upgrade__c.Multifamily_Job_ID__c";
							if ($dataDisplay == "-"){
								if ($JobName != $JobNameLast){
									$projectIdCount++;
								}
								$dataDisplay = date("Ym-").$projectIdCount;
								$Adjustments["ALL_JOBS"][$JobName]["ProjectID"] = "Project ID set to ".$dataDisplay;
								//print_r($Adjustments); //ARRAY DESCRIBE show anytime we alter data for export what was changed;
							}
							$ProjectIDs[$JobName] = $dataDisplay;
							//print_r($ProjectIDs); //ARRAY DESCRIBE show projectId based on short Account Name
							$AccountNamesByProjectId[$dataDisplay] = $JobName;
							//print_r($AccountNamesByProjectId);  //ARRAY DESCRIBE lookup short accountName by projectId
							break;
						case "Account.BillingAddress":
							$dataDisplay = str_replace("<br>",", ",$dataDisplay);
							break;
						case "Account.BillingCity":
							$AccountTownLookup[$JobName] = $dataDisplay;
							//print_r($AccountTownLookup); //ARRAY DESCRIBE lookup town by accountName used in ALL_MEASURES;
							break;
						case "Account.BillingPostalCode":
							if ($dataDisplay == "-"){
								$dataDisplay = retrieve_zipcode($address,GOOGLE_APIKEY);
								$Adjustments["ALL_JOBS"][$JobName]["PostalCode"] = "PostalCode set to ".$dataDisplay;
								//print_r($Adjustments); //ARRAY DESCRIBE show anytime we alter data for export what was changed;
							}
							break;
						case "Account.Heating_Fuel_Type__c":
							if ($dataDisplay == "Electric"){
								$dataDisplay = "E";
							}elseif ($dataDisplay == "Natural Gas"){
								$dataDisplay = "G";
							}else{
								$dataDisplay = $dataDisplay;
							}
							break;
						case "Upgrade__c.Green_Prospect_Type__c":
							if ($dataDisplay == "883 Audit"){
								$AccountHasAudit[$JobName]=array("accountId"=>$accountId,"GreenProspect"=>$dataDisplay,"HeatingFuel"=>$rowDetails["Account.Heating_Fuel_Type__c"]);
								//print_r($AccountHasAudit); //ARRAY DESCRIBE get list of Accounts with audits to make sure all audits are also included on measures since they do not have an attachment file
							}
							break;
						default:
							$dataDisplay = $dataDisplay;
							break;
					}
				}
				$reportRows[$rowId][$column.$rowCounter]=$dataDisplay;
				//print_r($reportRows); //ARRAY DESCRIBE these are the rows used for ALL_JOBS sheet
			}
			$JobNameLast = $JobName;
			$rowCounter++;	
		}
		
		//now get attachments for each of the projects
		include_once('soap_connect.php');
		foreach ($greenProspects as $accountName=>$parentIds){
			$ids = implode("','",$parentIds);
			
			$query = "SELECT Id,Name,ParentId FROM Attachment where ParentId IN ('".$ids."')";
			$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
			//print_r($response);
			if (count($response->records)){
				foreach ($response->records as $record) {
					$parentId = $record->ParentId;
					$name = $record->Name;
					$id = $record->Id;
					$fileParts = explode(".",$name);
					$Attachments[$parentId][] = array("Id"=>$id,"Name"=>$name,"AccountName"=>$accountName,"FileParts"=>$fileParts);
					//print_r($Attachments); //ARRAY DESCRIBE parentIds and infor used for gathering attachments and renaming them
				}
			}else{
				$Attachments[$parentId][] = array("NoAttachments"=>true,"AccountName"=>$accountName,"HeatingFuel"=>$AccountHasAudit[$accountName]["HeatingFuel"],"GreenProspect"=>$AccountHasAudit[$accountName]["GreenProspect"]);
			}
			$query = "SELECT Name FROM Account WHERE Id IN (SELECT Account__c FROM Upgrade__c WHERE Id ='".$parentIds[0]."')";
			//$query = "SELECT Account__c FROM Upgrade__c WHERE Id ='".$parentIds[0]."'";
			$response = $mySforceConnection->query($query); //sending request and getting response using SOAP
			foreach ($response->records as $record) {
				$AccountNamesLookup[$accountName] = $record->Name;
				//print_r($AccountNamesLookup); //ARRAY DESCRIBE used to get the full account name from the Green Prospect shorthand form
			}
		}
		
		foreach ($Attachments as $parentId=>$attachmentList){
			foreach ($attachmentList as $attachmentInfo){
				if ($attachmentInfo["NoAttachments"]){
					$attachmentResults[$attachmentInfo["Name"]]["NoAttachments"] = true;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["HeatingFuel"] = $attachmentInfo["HeatingFuel"];
					$attachmentResults[$attachmentInfo["Name"]]["GreenProspect"] = $attachmentInfo["GreenProspect"];
				}else{
					//echo $instance_url." ".$attachmentInfo["Id"]." ".$attachmentInfo["Name"]."<Br>";
					$attachment = retrieve_attachment($instance_url,$attachmentInfo, $access_token);
					$attachmentResults[$attachmentInfo["Name"]]["SavedName"] = $attachment;
					$attachmentResults[$attachmentInfo["Name"]]["AccountName"] = $attachmentInfo["AccountName"];
					$attachmentResults[$attachmentInfo["Name"]]["FileName"] = $attachmentInfo["FileParts"][0];
					$attachmentResults[$attachmentInfo["Name"]]["FileType"] = $attachmentInfo["FileParts"][(count($attachmentInfo["FileParts"])-1)];
					//print_r($attachmentResults); //ARRAY DESCRIBE results and details of downloaded files
				}
			}
		}
		//Get contractor names from pdf files
		foreach ($attachmentResults as $attachmentName=>$attachmentParts){
			if (strpos($attachmentName,"Invoice") && $attachmentParts["FileType"] == "pdf"){
				$accountName = $attachmentParts["AccountName"];
				$fileName = $attachmentParts["FileName"];
				$accountNameParts = explode(" ",$accountName);
				$accountNames = array();
				$accountNameToTest = "";
				if (count($accountNameParts)){
					foreach ($accountNameParts as $part){
						$accountNameToTest .= $part;
						$accountNames[] = $accountNameToTest;
					}
				}else{
					$accountNames[] = $accountName;
				}
				$accountNames = array_reverse($accountNames);
				$fileNameToExplode = $fileName;
				foreach ($accountNames as $accountNameToRemove){
					$fileNameToExplode = str_replace($accountNameToRemove,"",$fileNameToExplode);
				}
					$contractor = explode("Invoice",$fileNameToExplode);
				$ContractorListByAccountName[$attachmentParts["AccountName"]]["contractor"] = $contractor[0];
				$ContractorListByAccountName[$attachmentParts["AccountName"]]["fileName"] = $attachmentParts["SavedName"];
				// print_r($ContractorListByAccountName); //ARRAY DESCRIBE Which account has which Contractor;
			}
		}
		//Get list of attachments attributes to contractors
		foreach ($attachmentResults as $attachmentName=>$attachmentParts){
			if (strpos($attachmentName,"ConLighting")){
				$ContractorAttachmentLookupByFileName[$attachmentParts["SavedName"]]=$ContractorListByAccountName[$attachmentParts["AccountName"]]["contractor"];
				//print_r($ContractorAttachmentLookupByFileName); //ARRAY DESCRIBE Which savedfilename has which contractor;
			}
		}
			
		ob_flush();
		flush();
		echo "Received Data:<table border=1 cellpadding=0 cellspacing=0><tr>";
		foreach ($fieldNamesArray as $fieldName=>$fieldInfo){
			echo "<th>".$fieldInfo["label"]."</th>";
		}
		echo "<th>Attachment ParentId</th>";
		echo "</tr>";
		foreach ($reportRowsRaw as $rowId=>$rowDetails){
			echo "<tr>";
			foreach ($rowDetails as $fieldName=>$fieldValue){
				echo "<td>".$fieldValue."</td>";
				//echo "<td>".$fieldNamesArray[$fieldName]["label"].": ".$fieldValue."<br>";
			}
			echo "</tr>";
			
		}
		echo "</table>";
		echo "The Following Projects had Audits included in the Salesforce Data:";
		foreach ($AccountHasAudit as $projectName=>$projectData){
			echo "<br>".$projectName." - ".$projectData["GreenProspect"]." ".$projectData["HeatingFuel"];
		}
		echo "<br><br>Now attachments for all the Projects are being downloaded";
		ob_flush();
		flush();

		echo "<Br><br>The Following Attachments were downloaded:<br>";
		foreach ($attachmentResults as $AttachmentName=>$AttachmentSavedItem){
			$AttachmentSavedName = $AttachmentSavedItem["SavedName"];
			$AttachmentAccountName = $AttachmentSavedItem["AccountName"];
			$NoAttachments = $AttachmentSavedItem["NoAttachments"];
			if (substr($AttachmentSavedName,-4)=="xlsx"){
				$ToParse[$AttachmentAccountName][] = $AttachmentSavedName;
				//print_r($ToParse); //list of files to read through and parse into ALL_MEASURES
			}
			echo ($AttachmentName == $AttachmentSavedName ? $AttachmentSavedName : $AttachmentName ." saved as ".$AttachmentSavedName)."<br>";
			if ($NoAttachments){
				$ToParse[$AttachmentAccountName."_NoAttachment"] = $AttachmentSavedItem;
				echo $AttachmentAccountName." had no attachments but here is what it did have: ";
				print_r($AttachmentSavedItem);
			}
			ob_flush();
			flush();
		}
		//used to convert Column Number to Alphabet letter when applying values to new spreadsheet
		function getNameFromNumber($num) {
			$numeric = ($num - 1) % 26;
			$letter = chr(65 + $numeric);
			$num2 = intval(($num - 1) / 26);
			if ($num2 > 0) {
				return getNameFromNumber($num2) . $letter;
			} else {
				return $letter;
			}
		}
		//parse the downloaded files
		foreach ($ToParse as $accountname=>$fileNames){
			if (strpos($accountname,"_NoAttachment")){
				$measureDetail = ($fileNames["HeatingFuel"] == "Electric" ? "Whole Building Energy Assessment" : "PiggyBack Site Visit");
				$auditCost = ($fileNames["HeatingFuel"] == "Electric" ? 375 : 100);
				$rowsRaw = array(
					"EFI Item Number"=>"n/a",
					"Measure Description"=>$measureDetail,
					"Measure Category"=>$measureDetail,
					"Qty"=>1,
					"Cost/Unit"=>$auditCost,
					"Incentive/Unit"=>$auditCost,
					"kWh Savings/Unit"=>"0",
					"Measure Life"=>"0",
					"Demand kWh Reduction"=>"",
					"Annual kWh Savings"=>"0",
					"Lifetime kWh Savings"=>"0",
					"Total Cost"=>$auditCost,
					"Total Incentive"=>$auditCost
				);
				$rowsClean = array();
				$rowsClean[] = $rowsRaw;
				$parsedRows[str_replace("_NoAttachment","",$accountname)][$fileNames["GreenProspect"]] = $rowsClean;
			}else{
				foreach ($fileNames as $fileName){
					include('read_attachmentfile.php');
					$parsedRows[$accountname][$fileName] = $rowsClean;
					//print_r($parsedRows); //ARRAY DESCRIBE rows parsed into an array
				}
			}
		}
		echo "<br><br>All attached files have been parsed and now the ALL_MEASURES and ALL_JOBS file is being created.<br>";
		ob_flush();
		flush();
		
		$rowCounter =2;	//reset to use second row
		$rowId = 0;
		foreach ($ProjectIDs as $accountName=>$projectId){
			if (count($parsedRows[$accountName])){ //if just an audit is recorded there will not be an attachment
				foreach ($parsedRows[$accountName] as $savedFileName=>$rowDetail){
					foreach ($rowDetail as $rowDetails){
						$isAudit = ($rowDetails["Measure Category"] == "Whole Building Energy Assessment" || $rowDetails["Measure Category"] =="PiggyBack Site Visit" ? true : false);
						foreach ($AllMeasuresHeader as $column=>$headerData){
							//$dataDisplay = ($rowDetails[$headerData["object"]] ? $rowDetails[$headerData["object"]] : "");
							$dataDisplay = $rowDetails[$headerData["object"]];
							if (strpos(" ".$headerData["object"],"calculate")){
								$calculateType = str_replace("calculate_","",$headerData["object"]);
								switch ($calculateType){
									case "rowId":
										$dataDisplay = ($rowCounter-1);
										break;
									case "projectId":
										$dataDisplay = $projectId;
										break;
									case "actualAccountName":
										$dataDisplay = $AccountNamesLookup[$accountName];
										break;
									case "town":
										$dataDisplay = $AccountTownLookup[$accountName];
										break;
									case "contractor":
										$dataDisplay = ($ContractorAttachmentLookupByFileName[$savedFileName] ? $ContractorAttachmentLookupByFileName[$savedFileName] : "CET");
										break;
									case "invoiceMonth":
										$dataDisplay = $invoiceMonth;
										break;
									case "clinc":
										$dataDisplay = ($isAudit ? "IM" : "IN"); 
										break;
									default:
										$dataDisplay = $headerData["object"];
										break;
								}
							}else{
								$objectType = $headerData["object"];
								switch ($objectType){
									case "Measure Category":
										$dataDisplay = ($dataDisplay ? $dataDisplay : $rowDetails["Measure Description"]);
										break;
									default:
										$dataDisplay = $dataDisplay;
										break;
								}
							}
							$measureRows[$rowId][$column.$rowCounter]=$dataDisplay;
							//print_r($reportRows); //ARRAY DESCRIBE these are the rows used for ALL_JOBS sheet
						}//end foreach AllMeasuresHeader
						$rowCounter++;
						$rowId++;
					}//end foreach rowDeatils
				}//end foreach parsedRows
			}else{
				//Do with Only Audit
			}//end if count
		}
		
		include_once('reports/report883.php');
		echo "Parsed File: ".$EverSourceTrackingFileLink;

		//echo "Here are the parsed rows<br>";
		//print_r($parsedRows);

		
		echo "<Br><br>The Following Adjustments were made:";
		print_r($Adjustments);
	echo "</pre>";
}else{
	header('Location: '.AUTH_URL);
}
?>