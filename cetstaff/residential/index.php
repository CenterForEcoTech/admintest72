<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Staff - Residential";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "ESScheduler":
            $displayPage = "views/_ESScheduler.php";
            break;
        case "EZDocImporter":
            $displayPage = "views/_EZDocImporter.php";
            break;
        case "HES-DataReviewer":
			$navDropDown = $nav;
			$displayPage = "views/_EZDocDataReviewer.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Residential Administration</h1>';
include_once("_residentialMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>