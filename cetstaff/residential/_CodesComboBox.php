<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxCodes" ).combobox({
				select: function(event, ui){
					<?php if (!$codesComboBoxFunction){?>
					window.location = "?nav=manage-codes&CodeID="+$(ui.item).val();
					<?php }else{
						echo $codesComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
				<?php if(!$codesComboBoxHideLabel){?>
				<label>Start typing code name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxCodes parameters" id="SelectedCode">
				<option value="">Select one...</option>
				<?php 
					foreach ($CodesActive as $code=>$codeInfo){
						echo "<option value=\"".$codeInfo->id."\"".($SelectedCodesID==$codeInfo->id ? " selected" : "").">".$codeInfo->name." ".trim(str_replace("-","",str_replace($codeInfo->name,"",$codeInfo->description)))."</option>";
					}
				?>
			  </select>
			</div>
		</div>