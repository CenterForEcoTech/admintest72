<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GHS))){?>
			<li><a id="ESScheduler" href="<?php echo $CurrentServer.$staffFolder;?>residential/?nav=ESScheduler" class="button-link">ES Scheduler</a></li>
			<li><a id="EZDocTemplate" href="<?php echo $CurrentServer.$adminFolder;?>residential/HESDataFiles/EZDocTemplate.xlsm" class="button-link">EZ-Doc Template</a></li>
			<li><a id="EZDocImporter" href="<?php echo $CurrentServer.$staffFolder;?>residential/?nav=EZDocImporter" class="button-link">EZ-Doc Importer (HES Energy Audits)</a></li>
			<li><a id="HES-DataReviewer" href="<?php echo $CurrentServer.$staffFolder;?>residential/?nav=HES-DataReviewer" class="button-link">EZ-Doc Data Reviewer</a></li>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$staffFolder;?>" class="button-link back-to-console">Staff Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>