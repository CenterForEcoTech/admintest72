<div class="quick-links">
    <h4>Quick Links</h4>
    <ul>
        <li><a href="<?php echo $CurrentServer.$adminFolder;?>inventory/">Inventory</a></li>
        <li><a href="https://www.pivotaltracker.com/n/projects/1198808">Dashboard Project Queue</a></li>
		<span style="display:none;">
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>monitoring/">Logs</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>merchants/">Merchants</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>charities/">Charities</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>events/">Events</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-campaigns">Campaigns</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>cms/?nav=manage-event-templates">Event Templates</a></li>
			<li><a href="<?php echo $CurrentServer.$adminFolder;?>indexing/?nav=indexing-event-migration">Index Event</a></li>
		</span>
    </ul>
</div>