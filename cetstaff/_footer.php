</div><!-- close admin-main -->
</div><!-- close admin-wrap -->
<div id="admin-footer">
    <ul>
        <li><a href="<?php echo $CurrentServer;?>">Main Web Site</a></li>
    </ul>
	<?php echo 'Current PHP version: ' . phpversion();?>
</div>
<!-- JS
	================================================== -->
<script src="<?php echo $CurrentServer;?>js/jquery.ui.timepicker.js"></script>
<script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.watermark.min.js"></script>
<script type="text/javascript" src="<?php echo $CurrentServer;?>/js/wmd/jquery.wmd.min.js"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.validate.js" type="text/javascript" language="javascript"></script>
<script src="<?php echo $CurrentServer.$adminFolder;?>js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/js/dataTables.tableTools.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/extensions/FixedHeader/js/dataTables.fixedHeader.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.tag-it.js"></script>

<script src="<?php echo $CurrentServer;?>js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<?php echo $CurrentServer.$adminFolder;?>js/mayor.site.js"></script>
<script src='<?php echo $CurrentServer.$adminFolder;?>js/moment.min.js'></script>
<script src='<?php echo $CurrentServer.$adminFolder;?>js/fullcalendar.js'></script>
<script src='<?php echo $CurrentServer.$adminFolder;?>js/scheduler.min.js'></script>
<script type="text/javascript">

    $('#widget').draggable();
    var CETDASH = CETDASH || {}; // namespace
    CETDASH.colorbox = $.colorbox; // declared globally so modals can be closed by code loaded within them
    // jquery.validate customizations
    $.validator.setDefaults({
        errorPlacement: function(error, element) {
            var labelContainer = element.closest("label"),
                divContainer = element.closest("div.paired.fields"),
                labelAlertElement = labelContainer.find(".alert"),
                divAlertElement = divContainer.find(".alert");
            if (labelAlertElement.length){
                error.appendTo(labelAlertElement);
            } else if (divAlertElement.length) {
                error.appendTo(divAlertElement);
            } else {
                element.after(error);
            }
        },
        focusCleanup: false
    });
    $(function(){
        // site-wide datepicker default
        $(".auto-datepicker").datepicker({
            minDate:0
        });
        // site-wide timepicker default
        $(".auto-timepicker").timepicker({
            stepMinute: 30,
            ampm: true,
            hourGrid: 4,
            minuteGrid:30
        });
        $(".auto-timepicker,.auto-datepicker").on("keydown", function(e){
            if (e.keyCode == 8 || e.keyCode == 32 || e.keyCode > 40){
                e.preventDefault(); // don't let users hand edit a time using the auto-timepicker
            }
        });
        // site-wide watermark default
        $(".auto-watermarked").each(function(){
            var self = $(this),
                watermarkText = self.attr("title");
            if (watermarkText){
                self.watermark(watermarkText);
            }
        });
        $(".button-link").button();
        // automatically suppress navigation on links with the do-not-navigate class
        $('body').on("click", ".do-not-navigate", function(e){e.preventDefault()});
        // help modals
        $('body').on("click", ".help-modal", function(){
            var clicked = $(this),
                targetId = clicked.attr("href"),
                targetElement = $("#" + targetId);
            if (targetElement){
                targetElement.dialog({width:650});
            }
        });
        $('body').on("click", ".help-modal-wide", function(){
            var clicked = $(this),
                targetId = clicked.attr("href"),
                targetElement = $("#" + targetId);
            if (targetElement){
                targetElement.dialog({width:750});
            }
        });
    });

    /*
      Plugin to serialize form
    */
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
	$.fn.serializeNonNullObject = function()
	{
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (this.value){
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				}
			} else {
				if (this.value){
					o[this.name] = this.value || '';
				}
			}
		});
		return o;
	};
</script>
<!-- End Document
================================================== -->
</body>
</html>