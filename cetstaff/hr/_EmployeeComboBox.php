<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxEmployee" ).combobox({
				select: function(event, ui){
					<?php if (!$employeeComboBoxFunction){?>
					window.location = "?nav=manage-employees&EmployeeID="+$(ui.item).val();
					<?php }else{
						echo $employeeComboBoxFunction;
					}
					?>
		//			console.log($(ui.item).val());
				}
			});
		  });
		</script>	
		<div class="eight columns">
			<div class="ui-widget"><?php echo $_SESSION["AdminAuthObject"]["adminFullName"];?>
				<?php if(!$employeeComboBoxHideLabel){?>
				<label>Start typing employee name or click the down arrow: </label>
				<?php }?>
			  <select class="comboboxEmployee">
				<option value="">Select one...</option>
				<?php 
					$ThisSignedInEmployee = strtolower(trim($_SESSION["AdminAuthObject"]["adminFullName"]));
				
					foreach ($EmployeesActive as $employee=>$employeeInfo){
						$EmployeeFullName = trim($employeeInfo->firstName." ".$employeeInfo->lastName);
						$EmployeeFullNames[] = $EmployeeFullName;
						if (strtolower($EmployeeFullName) == $ThisSignedInEmployee){$EmployeeID = $employeeInfo->id;$DisplayEmployeeName = $EmployeeFullName;}
						echo "<option value=\"".$employeeInfo->id."\"".($EmployeeID == $employeeInfo->id || $EmployeeFullName == $ThisSignedInEmployee ? " selected" : "").">".trim($employeeInfo->firstName." ".$employeeInfo->lastName)."</option>";
					}
				?>
			  </select>
			</div>
		</div>