<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HRTrainingsProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		if (trim($action) == 'check_code'){
			$criteria->code = $_GET['code'];
			$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
			$response = $hrTrainingsProvider->get($criteria);
			if ($response->totalRecords){
				$results->found = "yes";
				$results->name = $response->collection[0]->name;
				$results->id = $response->collection[0]->id;
                header("HTTP/1.0 200 Success");
			} else {
				$results->found = "no";
                header("HTTP/1.0 200 Success");
			}
		}
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'training_update'){
	            $hrTrainingsProvider = new HRTrainingsProvider($dataConn);
				$response = $hrTrainingsProvider->update($newRecord, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else if (trim($newRecord->action) == 'status_add'){
			$status = $newRecord->status;
            $hrTrainingsProvider = new HRTrainingsProvider($dataConn);
			$response = $hrTrainingsProvider->addTrainingStatus($status, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'training_add'){
	            $hrTrainingsProvider = new HRTrainingsProvider($dataConn);
				$response = $hrTrainingsProvider->add($newRecord, getAdminId());
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else if (trim($newRecord->action) == 'trainingEntry_add'){
			$record = new stdClass();
			$record->trainingId = $newRecord->trainingId;
			$record->employeeId = $newRecord->employeeId;
			$record->employeeFullName = $newRecord->employeeFullName;
			$record->status = $newRecord->status;
			$record->hoursAssigned = $newRecord->hoursAssigned;
			$record->affectiveDate = $newRecord->affectiveDate;
			
			include_once($dbProviderFolder."HRTrainingsProvider.php");
			$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
			$response = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = " Error Saving".$response;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'trainingEntry_update'){
			$record = new stdClass();
			$record->id = $newRecord->id;
			$record->status = $newRecord->status;
			$record->affectiveDate = $newRecord->affectiveDate;
			
			include_once($dbProviderFolder."HRTrainingsProvider.php");
			$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
			$response = $hrTrainingsProvider->updateTrainingMatrix($record,getAdminId());
			if ($response->success){
				$results = $response;
				header("HTTP/1.0 201 Created");
			} else {
				$results = " Error Saving".$response;
				header("HTTP/1.0 409 Conflict");
			}
		}else{
            $hrTrainingsProvider = new HRTrainingsProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrTrainingsProvider->updateDisplayOrder($id,($displayID+1), getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrTrainingsProvider->updateDisplayOrder($id,0, getAdminId());
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>