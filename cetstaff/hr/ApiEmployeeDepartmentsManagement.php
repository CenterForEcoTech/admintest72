<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'department_update'){
	            $hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
				$response = $hrEmployeeDepartmentsProvider->update($newRecord, getAdminId());
				
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else if (trim($newRecord->action) == 'department_add'){
	            $hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
				$response = $hrEmployeeDepartmentsProvider->add($newRecord, getAdminId());
				if ($response->success){
					$results = $response->success;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}else{
            $hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeDepartmentsProvider->updateDisplayOrder($id,($displayID+1), getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeDepartmentsProvider->updateDisplayOrder($id,0, getAdminId());
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>