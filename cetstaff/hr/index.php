<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Staff - HR";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "review-employees":
            $displayPage = "views/_reviewProfile.php";
            break;
        case "eeo-survey":
            $displayPage = "views/_eeoSurvey.php";
            break;
        case "manualreview":
            $displayPage = "views/_manualReview.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '<div class="container">
<h1>HR Module</h1>';
include_once("_hrMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>