<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HREmployeeProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        echo "this is a delete";
        break;
    case "GET":
		echo "this is get";
        break;
    case "POST":
        if (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'employee_update'){
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$response = $hrEmployeeProvider->update($newRecord, getAdminId());
			
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'employee_add'){
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$response = $hrEmployeeProvider->add($newRecord, getAdminId());
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'location_update'){
			$location_old = $newRecord->OfficeLocationSrc;
			$location_new = $newRecord->OfficeLocationVal;
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$response = $hrEmployeeProvider->updateOfficeLocations($location_old,$location_new);
			if ($response->success){
				$results = $response->success;
				header("HTTP/1.0 201 Created");
			} else {
				$results = $response->error;
				header("HTTP/1.0 409 Conflict");
			}
		}else if (trim($newRecord->action) == 'Insert_TrainingMatrix'){
				$action = $newRecord->action;
				$items = $newRecord->items;
				$employeeId = $newRecord->employeeId;
				/*
				$responseCollection[] = $action.$employeeId.$items;
				foreach ($items as $displayID=>$id){
					//$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,0, getAdminId());
					$responseCollection[] = "Add TrainingID ".$displayID."to EmployeeID.".$employeeId;
				}
*/				
			$results = 	$action;
			header("HTTP/1.0 201 Created");
			/*
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = "unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
			*/
		}else{
            $hrEmployeeProvider = new HREmployeeProvider($dataConn);
			foreach ($newRecord as $obj=>$item){
				$action = $item->action;
				$items = $item->items;
				if ($action == "UpdateDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,($displayID+1), getAdminId());
					}
				}
				if ($action == "UpdateDoNotDisplay"){
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrEmployeeProvider->updateDisplayOrder($id,0, getAdminId());
					}
				}
				if ($action == 'Insert_TrainingMatrix'){
					include_once($dbProviderFolder."HRTrainingsProvider.php");
					$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
					if ($item->employeeId){
						$record->employeeId = $item->employeeId;
						$record->employeeFullName= $item->employeeFullName;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						foreach ($items as $displayID=>$id){
							$record->trainingId = $id;
							$responseCollection[] = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
							if (count($responseCollection)){
								$responseCollection[0] = "Added";
							}
						}
					}
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						foreach ($items as $displayID=>$id){
							$record->employeeInfo = $id;
							$responseCollection[] = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
							if (count($responseCollection)){
								$responseCollection[0] = "Added";
							}
						}
					}
				}
				if ($action == 'Insert_TrainingMatrixByDepartment'){
					include_once($dbProviderFolder."HRTrainingsProvider.php");
					$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
					if ($item->departmentName){
						$criteria->showAll = true;
						$criteria->noLimit = true;
						$paginationResult = $hrTrainingsProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						$categoryNames = array();
						//get Training Info
						foreach ($resultArray as $result=>$record){
							$TrainingsByID[$record->id] = $record;
						}
						
						//get department Employees
						$criteria = new stdClass();
						$criteria->showAll = false;
						$criteria->isManager = false;
						$criteria->department = urldecode($item->departmentName);
						$paginationResult = $hrEmployeeProvider->get($criteria);
						$resultArray = $paginationResult->collection;
						
						//loop through all the trainings
						foreach ($items as $displayID=>$Trainingid){
							$record = new stdClass();
							$record->trainingId = $TrainingsByID[$Trainingid]->id;
							$record->trainingName= $TrainingsByID[$Trainingid]->name;
							$record->trainingDisplayOrderId= $TrainingsByID[$Trainingid]->displayOrderId;
							$record->affectiveDate = $item->affectiveDate;
							$record->status = $item->status;
							
							//loop through all employees
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$responseCollection[] = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
							}
						}
					}
					
					if ($item->trainingId){
						$record->trainingId = $item->trainingId;
						$record->trainingName= $item->trainingName;
						$record->trainingDisplayOrderId= $item->trainingDisplayOrderId;
						$record->affectiveDate = $item->affectiveDate;
						$record->status = $item->status;
						foreach ($items as $displayID=>$department){
							$criteria = new stdClass();
							$criteria->showAll = false;
							$criteria->isManager = false;
							$criteria->department = $department;
							$paginationResult = $hrEmployeeProvider->get($criteria);
							$resultArray = $paginationResult->collection;
							foreach ($resultArray as $employeeObj){
								$record->employeeId = $employeeObj->id;
								$record->employeeFullName = $employeeObj->fullName;
								$record->employeeInfo = $employeeObj->id."_".urlencode($employeeObj->fullName);
								$responseCollection[] = $hrTrainingsProvider->addTrainingMatrix($record,getAdminId());
								if (count($responseCollection)){
									$responseCollection[0] = "Added";
								}
							}
						}
					}
				}
				
				if ($action == 'Remove_TrainingMatrix'){
					include_once($dbProviderFolder."HRTrainingsProvider.php");
					$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
					foreach ($items as $displayID=>$id){
						$responseCollection[] = $hrTrainingsProvider->removeTrainingMatrix($id,getAdminId());
						if (count($responseCollection)){
							$responseCollection[0] = $id."Removed";
						}
					}
				}
			
			}
			if (count($responseCollection)){
				$results = $responseCollection[0];
				header("HTTP/1.0 201 Created");
			} else {
				$results = $action."unable to update display order";
				header("HTTP/1.0 409 Conflict");
			}
		}
        break;
}
output_json($results);
die();
?>