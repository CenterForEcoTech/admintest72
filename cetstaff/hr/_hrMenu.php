<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_HR))){?>
			<li><a id="review-employees" href="<?php echo $CurrentServer.$staffFolder;?>hr/?nav=review-employees" class="button-link">Review Profile</a></li>
			<?php if ($EEOSurveyNotComplete){?>
				<li><a id="eeo-survey" href="<?php echo $CurrentServer.$staffFolder;?>hr/?nav=eeo-survey" class="button-link">EEO Survey</a></li>
			<?php }?>
			<?php if ($CurrentManualNotReviewed){?>
				<li><a id="manualreview" href="<?php echo $CurrentServer.$staffFolder;?>hr/?nav=manualreview" class="button-link">Staff Manual</a></li>
			<?php }?>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$staffFolder;?>" class="button-link back-to-console">Staff Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>