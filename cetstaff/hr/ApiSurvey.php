<?php
include_once("../_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HREmployeeProvider.php");
$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results = "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		$results = "this is get ".$action;
        break;
    case "POST":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'eeoform_submit'){
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$response = $hrEmployeeProvider->eeoSubmit($newRecord);
			$results = $newRecord;
			$results->error = "";
				$response->success = true;
				if ($response->success){
					$results = $response;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}
		if (trim($newRecord->action) == 'staffManual_submit'){
			$hrEmployeeProvider = new HREmployeeProvider($dataConn);
			$response = $hrEmployeeProvider->staffManualSubmit($newRecord);
			$results = $newRecord;
			$results->error = "";
				$response->success = true;
				if ($response->success){
					$results = $response;
					header("HTTP/1.0 201 Created");
				} else {
					$results = $response->error;
					header("HTTP/1.0 409 Conflict");
				}
		}
        break;
    case "PUT":
        $results = "this is a put";
        break;
}
output_json($results);
die();
?>