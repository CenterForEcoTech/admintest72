<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	if ($record->displayOrderId){
		$TrainingsActive[$record->displayOrderId]=$record;
	}
	
}
$TestingDivs .= '<div class="testing">$TrainingsByID is an array of trainings</div>';
//resort the products without display order by TrainingID
$SelectedTrainingMatrixID = $_GET['TrainingMatrixID'];
$addTraining = $_GET['addtrainingevent'];

$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

//get info about employees
include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria = new stdClass();
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
	}
}
$TestingDivs .= '<div class="testing">$EmployeeByID array of employee info</div>';
//print_pre($TrainingMatrixOtherEmployees);


if ($SelectedTrainingMatrixID){
	//get matrix information
	$criteria = new stdClass();
	$criteria->id = $SelectedTrainingMatrixID;
	$criteria->matrixId = $SelectedTrainingMatrixID;
	$paginationResult = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$trainingMatrixObj = $paginationResult->collection[0];
	$TestingDivs .= '<div class="testing">$trainingMatrixObj hold training Matrix info for this one training entry</div>';
	//print_pre($trainingMatrixObj);
	$SelectedEmployeeID = $trainingMatrixObj->employeeId;
	$SelectedEmployeeName = $trainingMatrixObj->employeeFullName;
	$SelectedTrainingID = $trainingMatrixObj->trainingId;
	$SelectedTrainingName = $trainingMatrixObj->trainingName;
	
	//get status history of training
	$historyPaginationResult = $hrTrainingsProvider->getTrainingMatrixStatusHistory($criteria);
	$trainingMatrixHistoryCollection = $historyPaginationResult->collection;
	$TestingDivs .= '<div class="testing">$trainingMatrixHistoryCollection holds training Matrix Status History info for this one training entry</div>';
	//print_pre($trainingMatrixHistoryCollection);
	
	
	//get matrix of employees with this training
	$criteria = new stdClass();
	$criteria->trainingId = $SelectedTrainingID;
	$criteria->statusNot = "Removed";
	$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
	$TrainingMatrixCollection = $TrainingMatrixResults->collection;
	if (count($TrainingMatrixCollection)){
		foreach ($TrainingMatrixCollection as $id=>$collection){
			//ignore same training same employeeFullName
			if ($collection->employeeId != $SelectedEmployeeID){
				$TrainingMatrixOtherEmployees[] = $collection;
			}
		}
	}
	$TestingDivs .= '<div class="testing">$TrainingMatrixOtherEmployees holds training Matrix info for other employees with this same training</div>';
	//print_pre($TrainingMatrixOtherEmployees);	

}//end if selected Training

?>
<style>
.testing {display:none;}
.trainingDetails {display:none;}
</style>
Training Management
<?php echo $TestingDivs;?>

<?php if ($trainingMatrixHistoryCollection[0]->status != "Completed"){
	$showForm = true;
?>
	<form id="TrainingUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Training Entry</legend>
			<div class="fifteen columns">
			<?php if ($SelectedTrainingMatrixID){?>
				<div style="display:none;">
					ID=<input type="text" name="id" value="<?php echo $SelectedTrainingMatrixID;?>">
				</div>
				<div class="three columns">Training:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget"><a href="?nav=manage-trainings&TrainingID=<?php echo $SelectedTrainingID;?>"><?php echo $SelectedTrainingName;?></a></div>
					</div>
				</div>
				<br clear="all">
				<div class="three columns">Employee:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget"><a href="?nav=manage-employees&EmployeeID=<?php echo $SelectedEmployeeID;?>"><?php echo $SelectedEmployeeName;?></a></div>
					</div>
				</div>
				<br clear="all">

			<?php }else{ //end if MatrixID ?>
				<div class="three columns">Training:</div>
				<div class="five columns">
					<?php
						$trainingComboBoxFunction = '$("#TrainingID").val($(ui.item).val());';
						$trainingComboBoxHideLabel = true;
						include('_TrainingComboBox.php');
					?>
					<input type="hidden" name="trainingId" id="TrainingID">
				</div>
				<?php if (!count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly))){?><a href="?nav=manage-trainings"><span class="ui-widgetedit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>
					
				<br clear="all">
				<br clear="all">
				
				<div class="three columns">Employee:</div>
				<div class="five columns">
					<?php
						$employeeComboBoxFunction = '$("#EmployeeID").val($(ui.item).val());';
						$employeeComboBoxFunction .= '$("#EmployeeFullName").val($(ui.item).text());';
						$employeeComboBoxHideLabel = true;
						include('_EmployeeComboBox.php');
					?>
					<input type="hidden" name="employeeId" id="EmployeeID">
					<input type="hidden" name="employeeFullName" id="EmployeeFullName">
				</div>
				<?php if (!count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly))){?><a href="?nav=manage-trainings"><span class="ui-widgetedit ui-icon ui-icon-pencil" title="click to edit list"></span></a><?php }?>

				<br clear="all">
				<br clear="all">
<?php }//end if MatrixID ?>
			<?php include_once('_comboBoxIncludes.php');?>
				<div class="three columns">Status:</div>
				<div class="five columns">
					<div class="eight columns">
						<div class="ui-widget">
							<select class="comboboxStatus" id="Status" name="status">
								<option></option>
								<?php
									foreach ($TrainingStatusList as $StatusObj){
										echo "<option value='".$StatusObj->name."'>".$StatusObj->name."</option>\n";
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<?php if (!count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly))){?><span class="ui-widgetedit ui-icon ui-icon-plus" title="click to add to status type to list" id="statusListEditIcon"></span><?php }?>
				<div style="float:right;">
					<fieldset id="statusListEdit">
						<legend>Add Status Type</legend>
						<div class="five columns">
							<div id="statusEditListEntry">
								<input type='text' id="statusEditListInput" value='' style='width:200px; height: 22px;'>&nbsp;<input type='button' id='editStatus' value='add' style='font-size:8pt;'>
							</div>
							<div id="statusResults"></div>
						</div>
					</fieldset>
				</div>
				
				
				<div class="three columns">Affective Date:</div>
				<div class="five columns">&nbsp;
					<input type="text" class="date" name="affectiveDate">
				</div>
			</div>
		</fieldset>
		<div id="trainingEntryResults" class="fifteen columns"></div>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$adminFolder;?>hr/?nav=manage-employees&EmployeeID=<?php echo $SelectedEmployeeID;?>" id="cancel" class="button-link">Cancel</a>
				<?php if (!count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly))){?>
					<a href="save-tranining-entry" id="save-tranining-entry" class="button-link do-not-navigate"><?php echo ($SelectedTrainingMatrixID ? "Update" : "Save");?></a>
					<input type="hidden" name="action" value="trainingEntry_<?php echo ($SelectedTrainingMatrixID ? "update" : "add");?>">
				<?php }//if not read only ?>
			</div>
	</form>
<?php }//if not completed ?>
<?php if ($SelectedTrainingMatrixID || $addTraining){?>
		<fieldset id="TrainingEntryDetails">
			<legend>Training Entry Details</legend>
			<div class="fifteen columns">
				<div class="fifteen columns">
					<div class="six columns">
						<fieldset>
							<legend>Training Details</legend>
							<div class="five columns">
								<div><span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $SelectedTrainingName;?>
									<div class="trainingDetails">
										<?php $TraningDetailExclusionList = array("id","name","code","displayOrderId");
										foreach ($TrainingsByID[$SelectedTrainingID] as $key=>$val){
											if (!in_array($key,$TraningDetailExclusionList)){
												if (substr($key,0,2)=="is"){$key = "Is ".str_replace("is","",$key); $val = ($val ? "Yes" : "No");}
												if ($key == "partOfCertificationId"){$key="Part of Certification"; $val = ($val ? $TrainingsByID[$val]->name : "No");}
												echo ucfirst($key).": ".$val."<br>";
											}
										}
										?>
									</div>
								</div>
							</div>
						</fieldset>
						<br clear="all">
						<?php //print_pre($trainingMatrixHistoryCollection);?>
						<fieldset>
							<legend>Entry history</legend>
							<div class="five columns">
								<?php 
									$affectiveDate = (MySQLDate($trainingMatrixHistoryCollection[0]->affectiveDate) ? MySQLDate($trainingMatrixHistoryCollection[0]->affectiveDate) : date("m/d/Y g:i a", strtotime($trainingMatrixHistoryCollection[0]->timeStamp)));
									
								?>
								<div><span<?php if (count($trainingMatrixHistoryCollection) > 1){echo " class=\"details-open\"";}?>>&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $trainingMatrixHistoryCollection[0]->status;?> <?php echo $affectiveDate;?> 
									<div class="trainingDetails">
										<hr>
										<?php $TraningDetailExclusionList = array("id","name","code","displayOrderId");
										foreach ($trainingMatrixHistoryCollection as $HistoryObj){
											$affectiveDate = (MySQLDate($HistoryObj->affectiveDate) ? MySQLDate($HistoryObj->affectiveDate) : date("m/d/Y g:i a",strtotime($HistoryObj->timeStamp)));

											//ignore the first entry
												echo "<div title='Entered By ".$HistoryObj->addedByAdmin."'>".$HistoryObj->status." ".$affectiveDate."</div>";
										}
										?>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="seven columns">
						<?php //print_pre($EmployeeByID[$SelectedEmployeeID]);?>
						<fieldset>
							<legend>Employee Details</legend>
							<div class="seven columns">
								<div class="seven columns">
								<div class="two columns">
									<img class="picture_id1" src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
									<img class="picture_id2" src="https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
								</div>
									<span class="details-open">&nbsp;&nbsp;&nbsp;&nbsp;</span> <?php echo $SelectedEmployeeName;?>
									<div class="trainingDetails">
										<?php $TraningDetailExclusionList = array("id","fullName","code","displayOrderId","trainingCodes");
										foreach ($EmployeeByID[$SelectedEmployeeID] as $key=>$val){
											if (!in_array($key,$TraningDetailExclusionList)){
												if (substr($key,0,2)=="is"){$key = "Is ".str_replace("is","",$key); $val = ($val ? "Yes" : "No");}
												if (substr($key,-4)=="Date"){$key = str_replace("Date"," Date",$key); $val = MySQLDate($val);}
												echo ucfirst($key).": ".$val."<br>";
											}
										}
										?>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</fieldset>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
<?php }?>
<script>
$(function(){
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	var statusListEdit = $("#statusListEdit"),
		statusListEditIcon = $("#statusListEditIcon"),
		statusEditListEntry = $("#statusEditListEntry"),
		status = $("#Status"),
		statusEditListInput = $("#statusEditListInput");
		
	statusListEdit.css("visibility","hidden");
	statusListEditIcon.on('click',function(){
		if (statusListEdit.css("visibility") == "visible"){
			statusListEdit.css("visibility","hidden");
		}else{
			statusListEdit.css("visibility","visible");
		}
	});
	<?php if ($showForm){?>
	$( ".comboboxStatus" ).combobox({
		select: function(event, ui){
			statusListEdit.css("visibility","hidden");
		}
	});
	<?php }?>
	
	
	
	<?php if (!count(array_intersect($adminSecurityGroups,$Security_Group_ReadOnly))){?>
		$("#editStatus").click(function(e){
			$('#statusResults').html("").removeClass("alert").removeClass("info");
			e.preventDefault();
			var jsondata = '{"action":"status_add","status":"'+statusEditListInput.val()+'"}';
			$.ajax({
				url: "ApiTrainingsManagement.php",
				type: "PUT",
				data: jsondata,
				success: function(data){
					console.log(data);
					$('#statusResults').html("Status Added").addClass("alert").addClass("info");
					//now update the pulldown and auto save the employee information
					$('#Status').append($("<option></option>").attr("value", statusEditListInput.val()).text(statusEditListInput.val())).val(statusEditListInput.val());
				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					$('#statusResults').html(message).addClass("alert");
				}
			});
		});
	<?php }?>
	
	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	
		
	 var formElement = $("#TrainingUpdateForm");
	 
	 formElement.validate({
        rules: {
            trainingId: {
                required: true
            },
            employeeId: {
                required: true
            },
            status: {
                required: true
            },
			affectiveDate: {
				required: true
			}
        }
    });
	$("#cancel").click(function(e){
		e.preventDefault();
		parent.history.back(); 
		return false;							   
	});
	
	
    $("#save-tranining-entry").click(function(e){

        $('#trainingEntryResults').html("").removeClass("alert").removeClass("info");
        e.preventDefault();
        if (formElement.valid()){
			//console.log(JSON.stringify(formElement.serializeObject()));
            $.ajax({
                url: "ApiTrainingsManagement.php",
                type: "PUT",
                data: JSON.stringify(formElement.serializeObject()),
                success: function(data){
                    $('#trainingEntryResults').html("Saved").addClass("alert").addClass("info");
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#trainingEntryResults').html(message).addClass("alert");
                }
            });
        }
    });
	
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	
	$('#TrainingEntryDetails').on('click', 'span.details-open', function () {
			var div = $(this).parent('div');
			var details = $(this).next('div');

			if ( details.is(":visible") ) {
				details.hide();
				div.removeClass('opened');
			}
			else {
				details.show();
				div.addClass('opened');
			}
		} );

});
</script>