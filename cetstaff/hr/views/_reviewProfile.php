<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."HREmployeeDepartmentsProvider.php");
$hrEmployeeDepartmentsProvider = new HREmployeeDepartmentsProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeDepartmentsProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$DepartmentsByName[$record->name] = $record;
}	



include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	if ($record->displayOrderId){
		$EmployeesActive[$record->displayOrderId]=$record;
		$EmployeesByManager[$record->manager][]=$record;
		$EmployeesByFullName[$record->fullName]=$record;
	}else{
		$EmployeesInActive[$record->firstName]=$record;
	}
	if ($record->isManager){
		$Managers[] = $record->firstName." ".$record->lastName;
	}

}
//resort the products without display order by EmployeeID
ksort($EmployeesInActive);
$SelectedEmployeeName = $_SESSION['AdminAuthObject']['adminFullName'];
if ($SelectedEmployeeName && !$SelectedEmployeeID){
	$SelectedEmployeeID = $EmployeesByFullName[$SelectedEmployeeName]->id;
}

$officeLocationsResults = $hrEmployeeProvider->getOfficeLocations();
foreach ($officeLocationsResults as $id=>$location){
	$officeLocations[] = $location["name"];
}	

include_once($dbProviderFolder."HRTrainingsProvider.php");
$hrTrainingsProvider = new HRTrainingsProvider($dataConn);
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrTrainingsProvider->get($criteria);
$resultArray = $paginationResult->collection;
$categoryNames = array();
foreach ($resultArray as $result=>$record){
	$TrainingsByID[$record->id] = $record;
	$TrainingsActive[$record->displayOrderId]=$record;
	$categories = explode(",",$record->categories);
	foreach ($categories as $category){
		if (!in_array(trim($category),$categoryNames)){
			$categoryNames[] = trim($category);
		}
	}
	if ($record->isCertification){
		$Certifications[] = $record;
	}
	if ($record->partOfCertificationId){
		$TrainingsByCertification[$record->partOfCertificationId][] = $record;
	}

}
$TrainingStatusList = $hrTrainingsProvider->getTrainingStatusList();

$criteria = new stdClass();
$criteria->employeeId = $SelectedEmployeeID;
$criteria->statusNot = "Removed";
$TrainingMatrixResults = $hrTrainingsProvider->getTrainingMatrix($criteria);
$TrainingMatrixCollection = $TrainingMatrixResults->collection;
if (count($TrainingMatrixCollection)){
	foreach ($TrainingMatrixCollection as $id=>$collection){
		$TrainingMatrix[] = $collection;
	}
}
?>
<?php if (!$addEmployee){?>
  <link rel="stylesheet" href="<?php echo $CurrentServer;?>css/jquery.orgchart.css"/>
  <style>
  #sortable1, #sortable2, #sortable3, #sortable4 {
    border: 1px solid #eee;
    width: 640px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
	height:380px;
	overflow-y: auto;
	overflow-x: hidden;
  }
  #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li{
    margin: 1px 0px 1px 5px;
    padding: 1px 5px 5px 5px;
    width: 600px;
	font-size:small;
	height:12px;
  }
  div.orgChart{background-color: <?php echo ($DepartmentsByName[$EmployeeByID[$SelectedEmployeeID]->department]->color ? $DepartmentsByName[$EmployeeByID[$SelectedEmployeeID]->department]->color  : "#ffffe8");?>;}
  </style>
<br>
  
<?php } //end if not addemployee?>
<?php if ($SelectedEmployeeID || $addEmployee){?>
	<form id="EmployeeUpdateForm" class="basic-form sixteen columns override_skel">
		<fieldset>
			<legend>Employees Details</legend>
			<div class="fifteen columns">
				<div class="fifteen columns">
					<fieldset>
						<legend>Contact Info</legend>
						<div class="one columns">
							<img class="picture_id1" src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
							<img class="picture_id2" src="https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=<?php echo $EmployeeByID[$SelectedEmployeeID]->email;?>&size=HR64x64">
						</div>
						<div class="seven columns">
							<div class="two columns">First Name:</div><div class="four columns"><?php echo $EmployeeByID[$SelectedEmployeeID]->firstName;?></div>
							<br clear="both">
							<div class="two columns">Last Name:</div><div class="four columns"><?php echo $EmployeeByID[$SelectedEmployeeID]->lastName;?></div>
							<br clear="both">
							<div class="two columns">Email:</div><div class="four columns"><?php echo $EmployeeByID[$SelectedEmployeeID]->email;?></div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Hire Details</legend>
						<div class="five columns">
							<div class="two columns">Hire Date:</div><div class="two columns"><?php echo MySQLDate($EmployeeByID[$SelectedEmployeeID]->hireDate);?></div>
							<br clear="both">
							<div class="two columns">End Date:</div><div class="two columns"><?php echo MySQLDate($EmployeeByID[$SelectedEmployeeID]->endDate);?></div>
							<br clear="both">
							<div class="two columns">Location:</div>
							<div class="two columns"><?php echo $EmployeeByID[$SelectedEmployeeID]->officeLocation;?>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Position Info</legend>
						<div class="one columns">
							&nbsp;
						</div>
						<div class="seven columns">
							<div class="two columns">Department:</div>
							<div class="four columns">
								<?php echo $EmployeeByID[$SelectedEmployeeID]->department;?>
							</div>
							<br clear="both">
							<div class="two columns">Title:</div><div class="four columns"><?php echo $EmployeeByID[$SelectedEmployeeID]->title;?></div>
							<br clear="both">
							<div class="two columns">Supervisor:</div>
							<div class="four columns">
									<?php echo $EmployeeByID[$SelectedEmployeeID]->manager;?>
							</div>
							<br clear="both">
							<div class="six columns">
								<?php 
									if($EmployeeByID[$SelectedEmployeeID]->isManager){
										echo $EmployeeByID[$SelectedEmployeeID]->firstName." ".$EmployeeByID[$SelectedEmployeeID]->lastName." Manages People";
									}
								?>
							</div>

						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
		<input type="hidden" id="picture_id1result">
		<input type="hidden" id="picture_id2result">
		<?php 
		if (count($EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName])){
		?>
			<fieldset>
				<legend>Employees Supervised By <?php echo $EmployeeByID[$SelectedEmployeeID]->fullName;?></legend>
				<div class="fifteen columns">
						<ul id="orgchart-source" style="display:none;">
							<?php
								echo "<li class=\"big\"><em>".$EmployeeByID[$SelectedEmployeeID]->fullName."</em>";
								echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeeByID[$SelectedEmployeeID]->email."&size=HR64x64\">
									<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeeByID[$SelectedEmployeeID]->email."&size=HR64x64\">";
								echo "<ul>";
								foreach ($EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName] as $id=>$ManageesName){
									echo "<li class=\"big\"><a href=\"?nav=manage-employees&EmployeeID=".$EmployeesByFullName[$ManageesName->fullName]->id."\">".$ManageesName->fullName."</a>";
									echo "<br clear=\"all\"><img class=\"picture_id1\" src=\"https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesByFullName[$ManageesName->fullName]->email."&size=HR64x64\">
									<img class=\"picture_id2\" src=\"https://pod51042.outlook.com/owa/service.svc/s/GetPersonaPhoto?email=".$EmployeesByFullName[$ManageesName->fullName]->email."&size=HR64x64\">";
									echo "</li>";
								}
								echo "</ul>";
							?>
						</ul>
						<div id="orgchart-container" class="reset-this"></div>					

				</div>
			</fieldset>
		<?php }//if $EmployeesByManager[$EmployeeByID[$SelectedEmployeeID]->fullName] ?>
	</form>
			<fieldset>
				<legend>Trainings/Certifications</legend>
				<div class="twelve columns"> 
					<div class="three columns"><b>Currently Assigned</b><Br><span style="font-size:10pt;">&nbsp;</span></div>
					<ul id="sortable3" class="connectedSortable">
					<?php 
						foreach ($TrainingMatrix as $trainingMatrixId=>$matrixInfo){
							$affectiveTimeStamp = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLTimeStampDisplay($matrixInfo->timeStamp));
							$affectiveDate = (MySQLDate($matrixInfo->affectiveDate) ? MySQLDate($matrixInfo->affectiveDate) : MySQLDate($matrixInfo->timeStamp));
							echo "<li id=\"".$matrixInfo->id."\" data-trainingId=\"".$matrixInfo->trainingId."\" class=\"ui-state-default\"><a href=\"?nav=manage-trainingentry&TrainingMatrixID=".$matrixInfo->id."\">".$matrixInfo->trainingName." <span class='certDescription' title='".$matrixInfo->status." ".$affectiveTimeStamp." by ".$matrixInfo->addedByAdmin." added into the system on ".MySQLTimeStampDisplay($matrixInfo->timeStamp)."'>".$matrixInfo->status." ".$affectiveDate."</span></a></li>";
						}
					?>
					</ul>
				</div>
			</fieldset>
<?php }?>
<script src="<?php echo $CurrentServer;?>js/jquery.orgchart.js"></script>

<script>
$(function(){
	$('#orgchart-source').orgChart({container: $('#orgchart-container')});
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({
		maxDate:0
	});

	$('select').css('height','22px');
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});

	
	$("#picture_id1result").val('true');
	$("#picture_id2result").val('true');
	$('.picture_id1').error(function() {
		$(this).hide();
		$("#picture_id1result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	$('.picture_id2').error(function() {
		$(this).hide();
		$("#picture_id2result").val('false');
		if ($("#picture_id1result").val()=='false' && $("#picture_id2result").val()=='false'){
			$(".big").removeClass("big");
		}
	});
	
	
});
</script>