<div class="row">
	<div class="twelve columns">
		I acknowledge that this day I have been given access to a copy of the Center for EcoTechnology's <a id="staffManualDownload" href="https://cetonline.sharepoint.com/CET%20Central/ADMIN%20GENERAL%20and%20HR/Employee%20Handbook%200815.pdf" target="StaffManual" class="button-link">Personnel Handbook</a>, revised <?php echo date("F, Y",strtotime($Config_CurrentStaffManualVersion));?>, 
		which supersedes any previous versions of the Handbook I may have received and read. 
		I understand that I am responsible for reading these policies. 
		If I have any questions regarding these policies, or the content or interpretation of any policy in the manual, 
		I will bring them to the attention of my manager and/or human resources. 
		<br>
		<br>
		I further understand that the policies contained in this Handbook may be changed from time to time and do not represent conditions of employment but rather serve 
		as an informational guidelines for my own benefit. 
		The most current version of the Handbook can be found on the CET SharePoint Intranet, "<a href="https://cetonline.sharepoint.com/CET%20Central/_layouts/15/start.aspx#/SitePages/Home.aspx" target="cetCentral">CET Central</a>." 
		Physical copies can be found in the Human Resources office at 320 Riverside Drive 1A, Northampton, MA, and at the main posting boards in each CET location.
		<Br>
		<br>
		It is my understanding that the contents of this Handbook do not constitute a contract of employment or confer any express or implied promises to me.
		Furthermore, no member of Center for EcoTechnology's management, other than the President or his/her specific designee, 
		possesses the authority to enter into any agreement for employment, expressed or implied, for any specified period of time and may only do so in writing. 
		I further understand that my employment remains on an at-will basis and can be terminated, with or without cause, and with or without notice, at any time, 
		at the option of either the Center for EcoTechnology or myself.
	</div>
</div>
