<style>
.bold {font-weight:bold;}
</style>
<div class="row">
	<div class="twelve columns">
		<a href="http://www.cetonline.org" target="cet"><img src="<?php echo $CurrentServer;?>images/CET_logo.png"></a>
	</div>
</div>
<?php 
	if (strtotime($hireDate) > strtotime($Config_CurrentStaffManualVersion)){
		include_once('views/_manualReviewNewStaff.php');
	}else{
		include_once('views/_manualReviewCurrentStaff.php');
	}
?>
<input type="hidden" id="downloaded" value="">
<form id="staffManualForm">
	<div class="row">
		<div class="ten columns bold">
			I have reviewed the contents of this page and the manual:
		</div>
		<div class="two columns">
			<input type="checkbox" id="reviewed">
			<input type="hidden" name="email" value="<?php echo $_SESSION['AdminEmail'];?>">
			<input type="hidden" name="version" value="<?php echo $Config_CurrentStaffManualVersion;?>">
			<input type="hidden" name="action" value="staffManual_submit">
		</div>
	</div>
</form>
	<div class="row">
		<div class="eight columns" id="formResults">&nbsp;</div>
	</div>
<script>
	$(document).ready(function() {
		var reviewed = $("#reviewed"),
			submitbutton = $("#submitbutton"),
			formElement = $("#staffManualForm");
		
		$("#staffManualDownload").click(function(event){
			event.preventDefault();
			var url = $(this).attr('href');
			$("#downloaded").val('1');
			window.open(url,'_blank');
		});
	
		reviewed.on('click',function(){
			var data = JSON.stringify(formElement.serializeObject()),
				validated = false;
			<?php 
				if (strtotime($hireDate) > strtotime($Config_CurrentStaffManualVersion)){
			?>
				var downloaded = $("#downloaded").val();
				if (downloaded){validated = true;}
			<?php
				}else{
			?>
				var downloaded = $("#downloaded").val(),
					newPolicies = $("#newPolicies").prop("checked"),
					updatedPolicies = $("#updatedPolicies").prop("checked"),
					newTopics = $("#newTopics").prop("checked");
				if (newPolicies && updatedPolicies && newTopics && downloaded){
					
					validated = true;
				}
				
			<?php
				}
			?>
			if (validated){
				$('#formResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert");
				$.ajax({
					url: "ApiSurvey.php",
					type: "POST",
					data: data,
					success: function(data){
						if (data.success){
							$('#formResults').html("Thank you for reviewing the Staff Manual").addClass("alert").addClass("info");
							formElement.slideToggle();
						}else{
							$('#formResults').html(data.error).addClass("alert");
							formElement.slideToggle();
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						var message = $.parseJSON(jqXHR.responseText);
						$('#formResults').html(message).addClass("alert");
					}
				});
			}else{
				alert('You must ackowledge all checkboxes and download the Handbook');
				$(this).attr('checked',false);
			}//end if validated
		});
	});
</script>