<div class="row">
	<div class="twelve columns">
		<h4>Personnel Handbook Review [Revised <?php echo MySQLDate($Config_CurrentStaffManualVersion);?>]</h4>
		<div>
			A new version of the Employee Handbook is now available. 
			It has been entirely rewritten and reorganized. 
			This version contains more information about the working conditions at the Center for EcoTechnology. 
			Some policies have changed in substance - you may already be familiar with these changes. 
			Other policies are new or newly changed and you may not be familiar with them.
		</div>
		<br>
		<div>
			You will find significant changes in each of the categories below. This summary is not inclusive.
		</div>
		<br>
		<div>
			Your check in each box acknowledges that you understand you are responsible for learning about the changes and how they affect the terms and conditions of your employment at the Center for EcoTechnology.<br>
			<br>
			<input type="checkbox" id="newPolicies" class="newPolicy">	<b>New policies introduced since the Personnel Handbook 2013</b>
				<ul style="padding-left:50px;list-style:disc;display:none;">
					<li>Personal Information Security
					<li>Three Hour Rule
					<li>Inclement Weather
					<li>Early Return to Work
					<li>Retirement Planning
					<li>Employee Assistance Program
					<li>Bereavement Leave
					<li>Small Necessities Leave Act
					<li>Voting Leave
					<li>Massachusetts Parental Leave Act (formerly MMLA)
					<li>Domestic Violence Leave
					<li>Confidentiality
					<li>Communication and Conflict
					<li>General Safety Rules
				</ul>
			<br>
			<input type="checkbox" id="updatedPolicies" class="newPolicy"> <b>Updated policies since the Personnel Handbook 2013</b>
				<ul style="padding-left:50px;list-style:disc;display:none;">
					<li>Initial Employment Period	Clarification, formerly Probation Period
					<li>Timekeeping	Change 
					<li>Summary of Paid Time Off Policies	Clarification
					<li>Sick/Personal Time	Change in benefit coverage
					<li>Vacation Time	Change in accrual system
					<li>Job Performance Counseling	Clarification
					<li>Jury Duty/Subpoena/Summons	Change to include subpoena, summons
					<li>Leaves of Absence Policies	Change in mandatory use of accruals
					<li>Travel Policies	New requirements and recommendations 
					<li>Computer, Mobile Device, Email and Internet Usage Policy Clarification
					<li>Social Networking Policy	Clarification
					<li>Ethics and Conduct General Policies	Clarification
					<li>Drug, Alcohol and Tobacco Free Workplace	Clarification
					<li>Complaints and Grievances	Change of process
					<li>Driver Safety 	Clarification
					<li>Anti-Harassment Policy	Clarification 
					<li>APPENDIX with FMLA, Domestic Violence Leave, EEO, and Whistleblower
				</ul>
			<br>
			<input type="checkbox" id="newTopics" class="newPolicy"> <b>Based on common practice, new topics, for this Employee Handbook.</b>
				<ul style="padding-left:50px;list-style:disc;display:none;">
					<li>Work Schedule and Location	
					<li>Salary / Wage Reviews
					<li>Expense Reimbursement
					<li>Safety Commitment
					<li>Customer Service
					<li>Lockers and Locked File Storage
					<li>Information Sharing
					<li>Performance Evaluation
					<li>Training and Development
					<li>Hiring from Within
					<li>Employee Discounts
					<li>Security Cameras
				</ul>
			<hr>
			Please <a href="https://cetonline.sharepoint.com/CET%20Central/ADMIN%20GENERAL%20and%20HR/Employee%20Handbook%200815.pdf" id="staffManualDownload" target="StaffManual" class="button-link">download</a> and read this new version of the Handbook you are receiving today.<br>
			<br>
			I acknowledge that this day I have received a copy of the Center for EcoTechnology's Personnel Handbook, revised <?php echo MySQLDate($Config_CurrentStaffManualVersion);?>, 
			which supersedes any previous versions of the Handbook I may have received and read. 
			I understand that I am responsible for reading these policies. 
			If I have any questions regarding these policies, or the content or interpretation of any policy in the manual, 
			I will bring them to the attention of my manager and/or human resources.<br>
			<br>
			It is my understanding that the contents of this Handbook do not constitute a contract of employment or confer any express or implied promises to me. 
			Furthermore, no member of Center for EcoTechnology's management, other than the President or his/her specific designee, 
			possesses the authority to enter into any agreement for employment, expressed or implied, for any specified period of time and may only do so in writing.<br> 
			<br>
			I further understand that the policies contained in this Handbook may be changed from time to time and 
			do not represent conditions of employment but rather serve as an informational guidelines for my own benefit. 
			The most current version of the Handbook can be found on the CET Intranet. 
			I further understand that my employment remains on an at-will basis and can be terminated, with or without cause, and with or without notice, at any time, 
			at the option of either the Center for EcoTechnology or myself.
	</div>
</div>
<script>
	$(document).ready(function() {
		$(".newPolicy").on('click',function(event){
			var $this = $(this);
			$this.nextAll('ul:first').toggle();
		});
	});
</script>