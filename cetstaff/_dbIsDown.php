<?php
$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
if ($load_getHeader){
    $PageTitle = "503 - Site Temporarily Unavailable";
    $PageHeader1 = "Site Maintenance";

}
if ($load_getContent){
    ?>
<p>
    <?php echo $SiteName;?> database appears to be down! This means the following:

    <ul>
    <li>The web server appears to be functioning.</li>
    <li>No Error logging is occuring at present.</li>
    <li>All web pages are displaying a Site Down message, including the RSS feed.</li>
    <li>The Contact link in the footer opens Zendesk support window.</li>
    </ul>
</p>
<script type="text/javascript">
    $(function(){
        $(".contact-us-link").click(function(e){
            e.preventDefault();
            Zenbox.show();
        });
    });
</script>

<?php }?>