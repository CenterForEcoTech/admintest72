<?php
$adminSecurityGroups = explode(",",$_SESSION['AdminAuthObject']['adminSecurityGroups']);
$Security_Tabs_HR = array('Staff'); // can access the Tools Tab
$Security_Tabs_Hours = array('Staff'); // can access the Tools Tab
$Security_Tabs_GBS = array('GBSStaff'); // can access the Tools Tab
$Security_Tabs_GHS = array('GHSStaff'); // can access the Tools Tab
$Security_Tabs_Muni = array('GHSStaff'); // can access the Tools Tab
$Security_Tabs_Resources = array('Staff'); // can access the Tools Tab

$Security_Menu_HR = array('Staff'); //can access Tools Menus
$Security_Menu_Hours = array('Staff'); //can access Tools Menus
$Security_Menu_GBS = array('GBSStaff'); //can access Tools Menus
$Security_Menu_GHS = array('GHSStaff'); //can access Tools Menus
$Security_Menu_Muni = array('GHSStaff'); //can access Tools Menus
$Security_Menu_Resources = array('Staff'); //can access Tools Menus

$Security_Group_ReadOnly = array(); //cannot submit forms

//check staff Todos on login
include_once("_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."HREmployeeProvider.php");
$hrEmployeeProvider = new HREmployeeProvider($dataConn);
$criteria= new stdClass();
$criteria->email = $_SESSION['AdminEmail'];
//get Staff HireDate to decide which MaualToCheck for
if ($criteria->email){
	$staffDetails = $hrEmployeeProvider->get($criteria);
	$hireDate = $staffDetails->collection[0]->hireDate;
	$EmployeeID = $staffDetails->collection[0]->id;
	//check to see if EEO survey was completed
	$eeoSurvey = $hrEmployeeProvider->getEEOSurvey($criteria);
	//check to see if StaffManual was reviewed
	$criteria->version = $Config_CurrentStaffManualVersion;
	$staffManual = $hrEmployeeProvider->getStaffManual($criteria);
//	$criteria->hireDate = $hireDate;
	/* remove at the request of Lisa Dufour on 4/5/2017
	if (!$eeoSurvey->totalRecords){$hasTodo = true; $TodoList[] = "You have not taken the <a href='hr/?nav=eeo-survey'>EEO Survey</a> yet.";$EEOSurveyNotComplete = true;}
	if (!$staffManual->totalRecords){$hasTodo = true; $TodoList[] = "You have not indicated that you have reviewed the current <a href='hr/?nav=manualreview'>Staff Manual</a> [updated as of ".MySQLDate($Config_CurrentStaffManualVersion)."]";$CurrentManualNotReviewed = true;}
	*/
}
//$hasTodo = false;

?>