<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."ResourcesProvider.php");
$resourcesProvider = new ResourcesProvider($dataConn);
$lastDayOfThisMonth = date("m/t/Y");

$calculationsResult = $resourcesProvider->getCalculations();
?>
<style>
.minWidth150 {
	min-width:150px;
}
.minWidth100 {
	min-width:100px;
}
</style>
	<h3>Conversion Calculations</h3>
	<fieldset>
		<legend>Instructions</legend>
		Click the 'Copy' button and paste into the Conversions sheet of your spreadsheet or click 'CSV' to save this table as a csv file
	</fieldset>
	<table id="tableInfo" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Unit</th>
				<th>=</th>
				<th>Conversion Calculation</th>
				<th>Conversion Unit</th>
				<th>For</th>
				<th>Source</th>
				<th>Link</th>
				<th>Notes</th>	
				<th>Valid Thru</th>
				<th>Last Updated</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ($calculationsResult as $id=>$data){
					echo "<tr>";
					echo "<td class='minWidth100'>".$data->fromUnit."</td>";
					echo "<td>=</td>";
					echo "<td class='minWidth100'>".$data->calculations."</td>";
					echo "<td class='minWidth100'>".$data->toUnit."</td>";
					echo "<td class='minWidth100'>".$data->usedFor.($data->notUsed ? " (not currently used)": "")."</td>";
					echo "<td>".$data->infoSource."</td>";
					echo "<td class='minWidth100'>".$data->infoLink."</td>";
					echo "<td class='minWidth100'>".$data->notes."</td>";
					echo "<td>".$data->validThru."</td>";
					echo "<td>".$data->lastUpdated."</td>";
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
	
	<script type="text/javascript">
	$(function () {
		
		var table = $('#tableInfo').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 50,
		});

	});
	</script>