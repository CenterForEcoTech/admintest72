<?php
include_once($siteRoot."_setupDataConnection.php");
include_once($dbProviderFolder."GBSProvider.php");
include_once($dbProviderFolder."HREmployeeProvider.php");
$gbsProvider = new GBSProvider($dataConn);
$hrEmployeeProvider = new HREmployeeProvider($dataConn);

$EmployeeID = ($_GET["EmployeeID"] ? $_GET["EmployeeID"] : false);
$SelectedGroupsID = ($_GET["GroupID"] ? $_GET["GroupID"] : false);
$SelectedCodesID = ($_GET["CodeID"] ? $_GET["CodeID"] : false);
$Status = ($_GET["Status"] ? $_GET["Status"] : "Approved");
$HideCodes = ($_GET["HideCodes"] == "1" ? true : false);
$OnlySupervised = ($_GET["OnlySupervised"] == "1" ? true : false);
if ($Status == "null"){$Status = "Approved";}
$StartOfThisMonth = date("m/01/Y");
$EndOfThisMonth = date("m/t/Y");
$StartDate = ($_GET['StartDate'] ? $_GET['StartDate'] : $StartOfThisMonth);
$EndDate = ($_GET['EndDate'] ? $_GET['EndDate'] : $EndOfThisMonth);

//Get GBSSupervised
$paginationsResults = $gbsProvider->getSupervisedBy();
$resultArray = $paginationsResults->collection;
foreach($resultArray as $result=>$record){
	$SupervisedBy[$record->employeeEmail] = $record->supervisorName;
}

$CurrentSupervisorName = $_SESSION["AdminAuthObject"]["adminFullName"];
//echo "Currently Logged in As :".$CurrentSupervisorName ;
//$CurrentSupervisorName = "Mike Kania";
//$CurrentSupervisorName = "Sean Pontani";
//$CurrentSupervisorName = "Lorenzo Macaluso";
//$CurrentSupervisorName = "John Majercak";

$criteria = new stdClass();
$criteria->hoursTrackingGBS = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$EmployeeByID[$record->id] = $record;
	$nonDepartmentSupervisor = ($SupervisedBy[$record->email] ? $SupervisedBy[$record->email] : "Sean Pontani");
	//$manager = (trim($record->department) != $ThisDepartment ? $nonDepartmentSupervisor : $record->manager);
	$manager = $record->manager;
	$includeThisRecord = true;
	$EmployeesByManager[$manager][]=$record->lastName."_".$record->firstName;
	if ($OnlySupervised){
		if ($manager == $CurrentSupervisorName){
			$includeThisRecord = true;
			$EmployeesByThisManager[] = $record->lastName."_".$record->firstName;

		}else{
			$includeThisRecord = false;
		}
	}
	if ($includeThisRecord){
		if ($record->displayOrderId){
			$EmployeesActive[$record->displayOrderId]=$record;
		}else{
			$EmployeesInActive[$record->firstName]=$record;
		}
	}
}
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $hrEmployeeProvider->get($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$AllEmployeesByID[$record->id] = $record;
}

//Get Groups
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getGroups($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$GroupsByID[$record->id] = $record;
	$GroupsByName[$record->name]=$record;
}
ksort($GroupsByName);

//Get CodeData
$criteria = new stdClass();
$criteria->showAll = true;
$criteria->noLimit = true;
$paginationResult = $gbsProvider->getCodes($criteria);
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$CodesByID[$record->id] = $record;
	$CodesByName[$record->name]=$record;
	$CodesInGroup[$record->groupId][] = $record->id;
	$CodesInGroupByName[$record->groupId][] = $record->name;
	if ($record->displayOrderId){
		$CodesActive[$record->name]=$record;
	}
}
ksort($CodesActive);
ksort($CodesByName);
//get hours
if ($Status != "Approved"){
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeId = $SelectedCodesID;
	$criteria->employeeId = $EmployeeID;
	$criteria->status = ($Status != 'All' ? $Status : false);
	//print_pre($criteria);
	$paginationResult = $gbsProvider->getHours($criteria);
	$resultArray = $paginationResult->collection;
	$RecordCount = 0;
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
			if (!in_array($Hours->codeId,$CodesInGroup[$SelectedGroupsID])){
				$includeData = false;
			}
		}
		if ($includeData){
			$EmployeeName= $AllEmployeesByID[$Hours->employeeId]->lastName."_".$AllEmployeesByID[$Hours->employeeId]->firstName;
			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
				$EmployeeData[$EmployeeName][$Hours->codeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$Hours->codeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$Hours->codeId]["totalHours"],$Hours->hours,2);
				$CodesData[$CodesByID[$Hours->codeId]->name]=$Hours->codeId;
				$CodesDataTotals[$Hours->codeId] = bcadd($CodesDataTotals[$Hours->codeId],$Hours->hours,2);
				
				if ($CodesByID[$Hours->codeId]->nonbillable){
					$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
					$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
				}else{
					$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
					$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
				}
				if ($CodesByID[$Hours->codeId]->name == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}			
			}//end includeThisData
		}
	}
	ksort($CodesData);
}else{
	$criteria = new stdClass();
	$criteria->startDate = date("Y-m-d",strtotime($StartDate));
	$criteria->endDate = date("Y-m-d",strtotime($EndDate));
	$criteria->codeName = ($SelectedCodesID ? $CodesByID[$SelectedCodesID]->name : false);
	$criteria->lastName = ($EmployeeID ? $AllEmployeesByID[$EmployeeID]->lastName : false);
	$paginationResult = $gbsProvider->getApprovedHours($criteria);
	$resultArray = $paginationResult->collection;
	$RecordCount = 0;
	foreach ($resultArray as $Hours){
		$includeData = true;
		if ($SelectedGroupsID){
			if (!in_array($Hours->codeName,$CodesInGroupByName[$SelectedGroupsID])){
				$includeData = false;
			}
		}
		if ($includeData){
			$EmployeeName= $Hours->employeeName;
			$includeThisData = true;
			if ($OnlySupervised){
				if (in_array($EmployeeName,$EmployeesByThisManager)){
					$includeThisData = true;
				}else{
					$includeThisData = false;
				}
			}
			if ($includeThisData){
				$RecordCount++;
	//			$CodeData[$Hours->codeId][]=$Hours;
				$codeId = $CodesByName[$Hours->codeName]->id;
				$EmployeeData[$EmployeeName][$codeId]["raw"][]=$Hours;
				$EmployeeData[$EmployeeName][$codeId]["totalHours"]=bcadd($EmployeeData[$EmployeeName][$codeId]["totalHours"],$Hours->hours,2);
				$CodesData[$Hours->codeName]=$codeId;
				$CodesDataTotals[$codeId] = bcadd($CodesDataTotals[$codeId],$Hours->hours,2);
				
				if ($Hours->codeName == "501"){
					$EmployeeAggregateData[$EmployeeName]["501Hours"]=bcadd($EmployeeAggregateData[$EmployeeName]["501Hours"],$Hours->hours,2);
					$TotalAggregateData["501Hours"]=bcadd($TotalAggregateData["501Hours"],$Hours->hours,2);
				}else{
					if ($CodesByName[$Hours->codeName]->nonbillable){
						$EmployeeAggregateData[$EmployeeName]["totalUnBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalUnBillable"],$Hours->hours,2);
						$TotalAggregateData["totalUnBillable"]=bcadd($TotalAggregateData["totalUnBillable"],$Hours->hours,2);
					}else{
						$EmployeeAggregateData[$EmployeeName]["totalBillable"]=bcadd($EmployeeAggregateData[$EmployeeName]["totalBillable"],$Hours->hours,2);
						$TotalAggregateData["totalBillable"]=bcadd($TotalAggregateData["totalBillable"],$Hours->hours,2);
					}
				}
			}//end includeThisData
		}
	}
	ksort($CodesData);
	
}
ksort($EmployeeData);

$DivColumns = "four";
$DivColumns1 = "four";
$employeeComboBoxDivColumns = $DivColumns;
$employeeComboBoxHideLabel = true;
$employeeComboBoxFunction = '$("#SelectedEmployee").val($(ui.item).val()).change();';
$codesComboBoxDivColumns = $DivColumns;
$codesComboBoxHideLabel = true;
$codesComboBoxFunction = '$("#SelectedCode").val($(ui.item).val()).change();';
$groupsComboBoxDivColumns = $DivColumns;
$groupsComboBoxHideLabel = true;
$groupsComboBoxFunction = '$("#SelectedGroup").val($(ui.item).val()).change();';
$customComboBoxInput = "width:150px;font-weight:normal;font-size:8pt;";
?>
<style>
.deleteRow {position:relative;width:10px;height:10px;float:right;top:-20px;right:10px;
	background-image:url("<?php echo $CurrentServer;?>images/redx-small.png");
	background-size: 10px 10px;
    background-repeat: no-repeat;
	background-position: left top;
	cursor:pointer;
}

</style>
	<h3>GBS Hours Report</h3>
	<div class="twenty columns">
		<fieldset>
			<legend>Filters</legend>
			<div class="two columns">
				Start Date<bR>
				<input type="text" class="date auto-watermarked historyFilter" id="StartDate" value="<?php echo ($_GET['StartDate'] ? $_GET['StartDate'] : $StartDate);?>" title="Start Date">
			</div>
			<div class="two columns">
				End Date<br>
				<input type="text" class="date auto-watermarked historyFilter" id="EndDate" value="<?php echo ($_GET['EndDate'] ? $_GET['EndDate'] : $EndDate);?>" title="End Date">
			</div>
			<div class="two columns">
				Status<br>
				<select class="parameters" id="SelectedStatus" style="width:100px;">
					<option value="Approved"<?php echo ($Status=="Approved" ? " selected" : "");?>>Approved</option>
					<option value="Submitted"<?php echo ($Status=="Submitted" ? " selected" : "");?>>Submitted</option>
					<option value="Saved"<?php echo ($Status=="Saved" ? " selected" : "");?>>Saved</option>
					<option value="All"<?php echo ($Status=="All" ? " selected" : "");?>>All</option>
				</select>
			</div>
			<div class="three columns">
				<Br>
				<input class="parameters" type="checkbox" id="HideCodes"<?php echo ($HideCodes ? " checked" : "");?>>Hide Codes<br>
			</div>
			<div class="two columns">
				<br>
				<button id="resetbutton">Reset Filters</button>
			</div>
			<br clear="all">
			<div class="<?php echo $DivColumns1;?> columns">
				Employee<br>
				<?php include('../hr/_EmployeeComboBox.php');?>
				<?php if ($EmployeeID){?>
					<div class="deleteRow" data-box="SelectedEmployee">&nbsp;</div>
				<?php }?>
				<Br>
				<input class="parameters" type="checkbox" id="OnlySupervised"<?php echo ($OnlySupervised ? " checked" : "");?>>Only Those You Supervise
			</div>
			<div class="<?php echo $DivColumns1;?> columns">
				Code<br>
				<?php include('_CodesComboBox.php');?>
				<?php if ($SelectedCodesID){?>
					<div class="deleteRow" data-box="SelectedCode">&nbsp;</div>
				<?php }?>
			</div>
			<div class="<?php echo $DivColumns1;?> columns">
				Group<br>
				<?php include('_GroupsComboBox.php');?>
				<?php if ($SelectedGroupsID){?>
					<div class="deleteRow" data-box="SelectedGroup">&nbsp;</div>
				<?php }?>
			</div>
		</fieldset>
		<br clear="all">
		<?php echo $RecordCount." records for ".count($EmployeeData)." employees";?>
	</div>
	<br clear="all">
	<div>
	
		<style>
			table.dataTable tbody {}
			.StaffName {min-width:85px;border:1pt solid #aed0ea;}
			.hourData {border:1pt solid #aed0ea;min-width:40px;}
			.CodeDataFooter {font-weight:normal;font-size:8pt;text-align:center;}
		</style>
		<Br clear="all">
		<div class="four columns" style="margin-top:7px;margin-right:-10px;">
			<br><br>
			<table class="reportFixedColumns display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="StaffName">Last Name</th>
						<th class="StaffName">First Name</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\">".$LastName."</td>";
							echo "<td class=\"StaffName\">".$FirstName."</td>";
						}
					?>
				</tbody>
			</table>
		</div>
		<style>
			th {min-width:70px;}
		</style>
		<div class="ten columns">
			<table class="reportResults display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="StaffName" style="display:none;">Last Name</th>
						<th class="StaffName" style="display:none;">First Name</th>
						<?php 
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									echo "<th class=\"CodeDataHeader\">".$codeName."</th>";
								}
							}
						?>
						<th>Total Billable</th>
						<th title="Includes 501">Total Unbillable</th>
						<th>501 Hrs</th>
						<th title="Total Billable + Total Unbillable - 501">Total Wrk_Hrs</th>
						<th title="Not Including 501">#Days Works</th>
						<th>%Billable Hrs</th>
						<th>%Unbillable Hrs</th>
						<th>AVG Bill/Day</th>
						<th>AVG Unbillable/Day</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="StaffName" style="display:none;">&nbsp;</td>
						<td class="StaffName" style="display:none;">Totals</td>
						<?php 
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									echo "<td class=\"CodeDataFooter\">".$CodesDataTotals[$codeId]."</td>";
								}
							}
							$Billable = $TotalAggregateData["totalBillable"];
							$Unbillable = $TotalAggregateData["totalUnBillable"];
							$Hour501 = $TotalAggregateData["501Hours"];
							$TotalHoursWorked = bcadd($Billable,$Unbillable,2);
							$TotalDaysWorked = bcdiv($TotalHoursWorked,8,2);
							$PercentBilled = bcdiv($Billable,$TotalHoursWorked,4);
							$PercentUnBillable = bcdiv($Unbillable,$TotalHoursWorked,4);
							echo "<td class=\"CodeDataFooter\">".$Billable."</td>";
							echo "<td class=\"CodeDataFooter\">".$Unbillable."</td>";
							echo "<td class=\"CodeDataFooter\">".$Hour501."</td>";
							echo "<td class=\"CodeDataFooter\">".$TotalHoursWorked."</td>";
							echo "<td class=\"CodeDataFooter\">".$TotalDaysWorked."</td>";
							echo "<td class=\"CodeDataFooter\">".bcmul($PercentBilled,100,2)."%</td>";
							echo "<td class=\"CodeDataFooter\">".bcmul($PercentUnBillable,100,2)."%</td>";
							echo "<td class=\"CodeDataFooter\">".bcdiv($Billable,$TotalDaysWorked,2)."</td>";
							echo "<td class=\"CodeDataFooter\">".bcdiv($Unbillable,$TotalDaysWorked,2)."</td>";
							
						?>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						foreach ($EmployeeData as $EmployeeLastFirstName=>$HoursData){
							$EmployeeName = explode("_",$EmployeeLastFirstName);
							$LastName = $EmployeeName[0];
							$FirstName = $EmployeeName[1];
							echo "<tr>";
							echo "<td class=\"StaffName\" style=\"display:none;\">".$LastName."</td>";
							echo "<td class=\"StaffName\" style=\"display:none;\">".$FirstName."</td>";
							if (!$HideCodes){
								foreach ($CodesData as $codeName=>$codeId){
									echo "<td align=\"center\" class=\"hourData\">".$HoursData[$codeId]["totalHours"]."</td>";
								}
							}
							$Billable = $EmployeeAggregateData[$EmployeeLastFirstName]["totalBillable"];
							$Unbillable = $EmployeeAggregateData[$EmployeeLastFirstName]["totalUnBillable"];
							$Hour501 = $EmployeeAggregateData[$EmployeeLastFirstName]["501Hours"];
							$TotalHoursWorked = bcadd($Billable,$Unbillable,2);
							$TotalDaysWorked = bcdiv($TotalHoursWorked,8,2);
							$PercentBilled = bcdiv($Billable,$TotalHoursWorked,4);
							$PercentUnBillable = bcdiv($Unbillable,$TotalHoursWorked,4);
							echo "<td>".$Billable."</td>";
							echo "<td>".$Unbillable."</td>";
							echo "<td>".$Hour501."</td>";
							echo "<td>".$TotalHoursWorked."</td>";
							echo "<td>".$TotalDaysWorked."</td>";
							echo "<td>".bcmul($PercentBilled,100,2)."%</td>";
							echo "<td>".bcmul($PercentUnBillable,100,2)."%</td>";
							echo "<td>".bcdiv($Billable,$TotalDaysWorked,2)."</td>";
							echo "<td>".bcdiv($Unbillable,$TotalDaysWorked,2)."</td>";
							
							echo "</tr>";
							
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
	
<script>
	$(document).ready(function() {
		var dateClass = $(".date");
		dateClass.width(80);
		$("#StartDate").datepicker({
			onSelect: function (date) {
				var date2 = $('#StartDate').datepicker('getDate');
				date2.setDate(date2.getDate() + 30);
				$('#EndDate').datepicker('setDate', date2);
				//sets minDate to dt1 date + 1
				$('#EndDate').datepicker('option', 'minDate', date2);
				updateFilters();
			}
		});
		$('#EndDate').datepicker({
			onClose: function () {
				var dt1 = $('#StartDate').datepicker('getDate');
				var dt2 = $('#EndDate').datepicker('getDate');
				//check to prevent a user from entering a date below date of dt1
				if (dt2 <= dt1) {
					var minDate = $('#EndDate').datepicker('option', 'minDate');
					$('#EndDate').datepicker('setDate', minDate);
				}
				updateFilters();
			}
		});		
		var getFilters = function(){
			var startDate = $("#StartDate").val(),
				endDate = $("#EndDate").val(),
				employeeId = $("#SelectedEmployee").val(),
				groupId = $("#SelectedGroup").val(),
				codeId = $("#SelectedCode").val(),
				status = $("#SelectedStatus").val(),
				hideCodes = ($("#HideCodes").prop("checked") ? 1 : 0),
				onlySupervised = ($("#OnlySupervised").prop("checked") ? 1 : 0),

				filterValues = new Array(startDate,endDate,employeeId,groupId,codeId,status,hideCodes,onlySupervised);
				
				return filterValues;
		};
		
		var updateFilters = function(){
			var Filters=getFilters();
			window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>gbs/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&EmployeeID="+Filters[2]+"&GroupID="+Filters[3]+"&CodeID="+Filters[4]+"&Status="+Filters[5]+"&HideCodes="+Filters[6]+"&OnlySupervised="+Filters[7]+"&nav=<?php echo $navDropDown;?>#";
		}
		$(".parameters").on('change',function(){
			updateFilters();
		});
		$("#resetbutton").on('click',function(){
			$("#StartDate").val('');
			$("#EndDate").val('');
			$(".parameters").val('');
			updateFilters();
			
		});

		
		var table = $('.reportResults').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"sScrollX":	"100%",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		var table2 = $('.reportFixedColumns').DataTable({
			"sScrollX":	"100%",
			"bJQueryUI": true,
			"bSearchable":false,
			"bFilter":false,
			"bAutoWidth": true,
			"bSort": false,
			"iDisplayLength": 100,
			"paging":   false,
			"ordering": false,
			"bScrollCollapse": true,
			"info":     false
		});
		$(".deleteRow").on("click",function(){
			var $this = $(this),
				thisBox = $this.attr('data-box');
				thisBoxID = "#"+thisBox;
				$(thisBoxID).val('').change();
			
		});
	});
</script>