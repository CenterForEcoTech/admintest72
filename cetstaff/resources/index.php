<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Staff - Resources";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "conversionCalculations":
            $displayPage = "views/_conversionCalculations.php";
            break;
		case "reports":
			$displayPage = "_reportsTabs.php";
			break;
        case "report-bycode":
            $displayPage = "views/_reportByCode.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>CET Resources</h1>';
include_once("_resourcesMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>