<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxUtilitiesPrimary" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "
								$('.comboboxUtilitiesSecondaryDiv').show();
								
								//clear each option if its selected
								$('.comboboxUtilitiesSecondary option').each(function() { 
									$(this).removeAttr('selected')
								});
								$('#secondaryUtilityImg').html('');
								$('.comboboxMeasures option').each(function() { 
									$(this).removeAttr('selected')
								});

								//set the first option as selected
								$('.comboboxUtilitiesSecondary option:first').attr('selected', 'selected');
								$('.comboboxMeasures option:first').attr('selected', 'selected');

								//set the text of the input field to the text of the first option
								$(\"input[data-hiddenid='secondaryUtility']\").val($(\".comboboxUtilitiesSecondary option:first\").text());
								//set the text of the input field to the text of the first option
								$(\"input[data-hiddenid='measure']\").val($(\".comboboxMeasure option:first\").text());
								$('.comboboxMeasureDiv').hide();

								
								updateFilters();								
							";
							//echo "alert($(ui.item).val());";
						} 
					?>
				}
			});

		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxUtilitiesPrimary parameters " id="SelectedPrimaryUtility" data-hiddenid="primaryUtility">
				<option value="">Choose Primary Utility</option>
				<?php 
					foreach ($UtilityPrimaries as $PrimaryUtilityName){
						echo "<option value=\"".$PrimaryUtilityName."\"".($SelectedPrimaryUtility==$PrimaryUtilityName ? " selected" : "").">".$PrimaryUtilityName."</option>";
					}
				?>
			  </select>
			</div>
		</div>