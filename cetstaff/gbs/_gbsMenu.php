<?php 
	$reportDropDownItemsTop = "130";
	$invoicingToolsDropDownItemsTop = "130";
	$buttonlinkminiMarginTop = "0";
	include_once($siteRoot.$adminFolder.'_reportsStyle.php');
?>
<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_GBS))){?>
<!--			<li><a id="manage-hoursentry" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Entry</a></li>-->
			<li><a id="manage-bullets" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=manage-bullets" class="button-link">Manage Static Report Bullets</a></li>
<!--			<li><a id="eap-templatecreator" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=eap-templatecreator" class="button-link">EAP Templates</a></li>-->
			<li id="InvoicingToolMenuDropDown"><a id="invoicingTools" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTools" class="button-link">Commercial InvoicingTools</a>
				<?php ($nav != "invoicingTools" ? include('_invoicingToolsTab.php') : "");?>
			</li>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$staffFolder;?>" class="button-link back-to-console">Staff Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
		$(".button-link").on("mouseover",function(){
			$("#invoicingToolDropDownItems").hide();
		});
		$("#invoicingTools.button-link").on("mouseover",function(){
			$("#invoicingToolDropDownItems").show();
		});
		$(".button-linkmini").on("mouseover",function(){
			$(this).addClass("ui-state-hover");
		});
		$(".button-linkmini").on("mouseout",function(){
			$(this).removeClass("ui-state-hover");
		});
		<?php
			//override current page for dropdown items
			switch($nav){
				case "invoicingTools":
					$nav = "invoicingTools";
					$showInvoicingToolsDropDown = true;
					break;
				case "invoicingTool-RWTAPipeline":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				case "invoicingTool-ColumbiaGas":
					$navDropDown = $nav;
					$nav = "invoicingTools";
					break;
				default:
					$nav = $nav;
					break;
			}
		?>
		
		
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>