<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxMeasures" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "updateFilters();";
							//echo "alert($(ui.item).val());";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxMeasures parameters" id="SelectedMeasure" data-hiddenid="measure">
				<option value="">Choose Measure(s)</option>
				<?php 
					foreach ($Measures as $Measure){
						echo "<option value=\"".$Measure."\"".($SelectedMeasure==$Measure ? " selected" : "").">".$Measure."</option>";
					}
				?>
			  </select>
			</div>
		</div>