<?php
ini_set("display_errors", "on"); error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");

include_once($dbProviderFolder."GBSProvider.php");
$gbsProvider = new GBSProvider($dataConn);
$paginationResult = $gbsProvider->getEAPTemplates();
$resultArray = $paginationResult->collection;
foreach ($resultArray as $result=>$record){
	$displayText = str_replace("  "," ",trim($record->displayText));
	$MeasureList[$record->measure] = 1;
	$UtilityPrimaryList[$record->primaryUtility] = 1;
	$UtilitySecondaryList[$record->secondaryUtility] = 1;
	$UtilityPermutationsList[$record->primaryUtility."/".$record->secondaryUtility] = 1;
	$DisplayTextList[$record->primaryUtility][$displayText] = $record->measure;
	$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["displayText"] = $displayText;
	$AEPTemplates[$record->measure][$record->primaryUtility][$record->secondaryUtility]["id"] = $record->id;
	$AEPTemplatesByID[$record->id] = $record;
	$AEPTemplatesByMeasure[$record->measure][]=$record;
}
ksort($AEPTemplatesByMeasure);
foreach ($MeasureList as $Measure=>$count){
	$Measures[] = $Measure;
}
foreach ($UtilityPrimaryList as $UtilityPrimary=>$count){
	$UtilityPrimaries[] = $UtilityPrimary;
}
foreach ($UtilitySecondaryList as $UtilitySecondary=>$count){
	$UtilitySecondaries[] = $UtilitySecondary;
}
foreach ($UtilityPermutationsList as $UtilityCombo=>$count){
	$UtilityPermutations[] = $UtilityCombo;
}
ksort($UtilityPermutations);
foreach ($DisplayTextList as $Utility=>$DisplayInfo){
	foreach ($DisplayInfo as $DisplayText=>$measure){
		if (count($DisplayTexts[$measure])){
			if (!in_array(trim($DisplayText),$DisplayTexts[$measure])){
				$DisplayTexts[$measure][] = trim($DisplayText);
			}
		}else{
			$DisplayTexts[$measure][] = trim($DisplayText);
		}
	}
}
$SelectedEAPTemplateID = $_GET['EAPTemplateID'];
$addTemplate = $_GET['addTemplate'];

?>
<style>
.displayTextDiv {border:1pt solid black; margin:5pt;float:left;padding:5pt;cursor:pointer;}
.eapTemplateDisplayText {min-width:300px;cursor:pointer;}
.selectedTemplateDisplayText {border:1pt solid #CCCC00;background-color:#E6E600;}
</style>
		<form id="EAPUpdateForm" class="basic-form override_skel">
			<fieldset class="row">
				<legend>EAP Templates</legend>
				<div class="fifteen columns">
					<div class="fourteen columns">
						<div class="row">
							<div class="ten columns">
								<?php 
									$codesComboBoxDivColumns = "nine";
									$customComboBoxInput = "width: 400px;";
									$comboBoxRemoveIfInvalid = "";
									$codesComboBoxFunction = "";
									include('_eapPrimaryUtilities_ComboBox.php');
								?>
							</div>
						</div>
						<div class="row comboboxUtilitiesSecondaryDiv" style="display:none;">
							<div class="ten columns">
								<?php 
									include('_eapSecondaryUtilities_ComboBox.php');
								?>
							</div>
						</div>
						<div class="row comboboxMeasureDiv" style="display:none;">
							<div class="ten columns">
								<?php 
									$codesComboBoxFunction = "";
									include('_eapMeasures_ComboBox.php');
								?>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<div class="row">
				<table border="0" cellpadding="5">
					<tr>
						<td><div class="six columns" id="primaryUtilityImg"></div></td>
						<td><div class="six columns" id="secondaryUtilityImg"></div></td>
					</tr>
					<tr>
						<td colspan="2"><div class="twelve columns" id="measureImg"></div></td>
					</tr>
				</table>
			</div>
			<div id="codeResults" class="fifteen columns"></div>
			<div class="fifteen columns">
				<a href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=eap-templatecreator" id="cancel" class="button-link">Reset</a>
			</div>
			<fieldset>
		</form>
<div class="row">
	<div class="sixteen columns">
		<table class="EAPTemplates">
			<thead>
				<tr>
					<th style="box-sizing:border-box;">Measure</th>
					<?php foreach ($UtilityPermutations as $UtilityPermuation){
							$Utilities = explode("/",$UtilityPermuation);
							$PrimaryUtility = $Utilities[0];
							switch ($PrimaryUtility){
								case "Berkshire Gas":
									$backgroundColor = "#FFC000";
									$borderColor = "#CC9A00";
									break;
								case "Columbia Gas (Small)":
									$backgroundColor = "#FF0000";
									$borderColor = "#CC0000";
									break;
								case "Columbia Gas (Large)":
									$backgroundColor = "#92D050";
									$borderColor = "#75A640";
									break;
								case "National Grid Gas":
									$backgroundColor = "#00B0F0";
									$borderColor = "#008DC0";
									break;
							}
					?>
						<th style="color:black;background-image:none;background-color:<?php echo $backgroundColor;?>;border:1pt solid <?php echo $borderColor;?>"><?php echo $UtilityPermuation;?></th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($AEPTemplatesByMeasure as $measure=>$templateInfo){?>
				<tr>
					<td valign="top" style="min-width:200px;"><?php echo $measure;?></td>
					<?php 
						foreach ($UtilityPermutations as $UtilityPermuation){
							$Utilities = explode("/",$UtilityPermuation);
							$displayText = $AEPTemplates[$measure][$Utilities[0]][$Utilities[1]]["displayText"];
							$templateID = $AEPTemplates[$measure][$Utilities[0]][$Utilities[1]]["id"];
							strlen($displayText);
							echo "<td valign='top' title='".$measure."-".$UtilityPermuation."' class='eapTemplateDisplayText".($SelectedEAPTemplateID == $templateID ? " selectedTemplateDisplayText" : "")."' data-templateid='".$templateID."'>".$displayText."</td>";
						}
					?>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" language="javascript" class="init">
	function getFilters(){
		var primaryUtility = $("#SelectedPrimaryUtility").val(),
			secondaryUtility = $("#SelectedSecondaryUtility").val(),
			measure = $("#SelectedMeasure").val(),
			filterValues = new Array(primaryUtility,secondaryUtility,measure);
			if (primaryUtility == "Berkshire Gas"){
				$("#primaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/bgas.png">');
			}
			if (primaryUtility == "Columbia Gas (Small)" || primaryUtility == "Columbia Gas (Large)"){
				$("#primaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/columbiagas.png">');
			}
			if (primaryUtility == "National Grid Gas"){
				$("#primaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/nationalgrid.png" width="178">');
			}
			if (secondaryUtility == "National Grid"){
				$("#secondaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/nationalgrid.png" width="178">');
			}
			if (secondaryUtility == "WMECO"){
				$("#secondaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/wmeco.png" width="178">');
			}
			if (secondaryUtility == "NSTAR"){
				$("#secondaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/nstar.png" width="178">');
			}
			if (secondaryUtility == "Municipal"){
				$("#secondaryUtilityImg").html('<img src="<?php echo $CurrentServer;?>images/eap/municipal.png" width="178">');
			}
			return filterValues;
	}
		
	function updateFilters(){
		var Filters=getFilters(),
			measure = Filters[2];
		//console.log(Filters);
		if (measure){
			dataItem = {};
			dataItem["action"] = "get_measuretext";
			dataItem["primaryUtility"] = Filters[0];
			dataItem["secondaryUtility"] = Filters[1];
			dataItem["measure"] = Filters[2];
			var data = JSON.stringify(dataItem);
			$.ajax({
                url: "ApiEAPManagement.php",
                type: "POST",
                data: data,
                success: function(data){
					if (data.success){
						$('#measureImg').append(data.displayText+"<br><br>");
					}
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#measureImg').append(message);
                }
            });

			
		}
		//get the text for the measure
		
		
		//window.location.href = "<?php echo $CurrentServer.($staffFolder ? $staffFolder : $adminFolder);?>gbs/?StartDate="+Filters[0]+"&EndDate="+Filters[1]+"&EmployeeID="+Filters[2]+"&GroupID="+Filters[3]+"&CodeID="+Filters[4]+"&Status="+Filters[5]+"&HideCodes="+Filters[6]+"&OnlySupervised="+Filters[7]+"&nav=<?php echo $navDropDown;?>#";
	}

	$(function(){
		//set the comboboxes
		$("input[data-hiddenid='primaryUtility']").val($(".comboboxUtilitiesPrimary option:first").text());
		$("input[data-hiddenid='secondaryUtility']").val($(".comboboxUtilitiesSecondary option:first").text());
		$("input[data-hiddenid='measure']").val($(".comboboxMeasures option:first").text());

		$(".parameters").on('change',function(){
			updateFilters();
		});
	
		var tableEAPTemplates = $('.EAPTemplates').DataTable({
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "<?php echo $CurrentServer.$adminFolder;?>js/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},				
			"scrollX": true,
			"bJQueryUI": true,
			"bSearchable":true,
			"bFilter":true,
			"bAutoWidth": true,
			"bSort": true,
			"order": [[0, "asc"]],
			"bPaginate": true,
			"iDisplayLength": 500,
			"aoColumnDefs": [
                { "bWidth": '200px', "aTargets": [0] }
            ]
		});
		new $.fn.dataTable.FixedColumns( tableEAPTemplates );

		$(".eapTemplateDisplayText").on('click',function(){
			console.log(this);
			var $this = $(this);
			window.location = '?nav=manage-eaptemplates&EAPTemplateID='+$this.attr('data-templateid');
		});
		
		var displayTextDiv = $(".displayTextDiv");
		displayTextDiv.on("mouseover",function(){
			$(this).css('background-color','lightblue');
		}).on("mouseout",function(){
			$(this).css('background-color','white');
		}).on("click",function(){
			$("#displayText").val($(this).text());
		});

		var codeSelectForm = $("#codeSelectForm");
			
		codeSelectForm.hide();	
		
		$('select').css('height','22px');
		var dropdownlist = $('select');
		dropdownlist.css('width','auto');
		$('input[type="text"]').css('height','22px');
		$('input[type="checkbox"]').css('width','13px');
		$(".input_text_tiny").css({'width':'25px','height':'20px'});
		$(".input_text_small").css({'width':'45px','height':'20px'});
		
		 var formElement = $("#EAPUpdateForm");
		 
		 formElement.validate({
			rules: {
				measure: {
					required: true
				},
				primaryUtility: {
					required: true
				},
				secondaryUtility: {
					required: true
				},
				displayText: {
					required: true
				}
			}
		});
		
			$("#save-code-add").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var data = JSON.stringify(formElement.serializeObject());
					console.log(data);
					$.ajax({
						url: "ApiEAPManagement.php",
						type: "PUT",
						data: data,
						success: function(data){
							console.log(data);
							$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
		<?php if ($SelectedEAPTemplateID){?>
			$("#save-code-update").click(function(e){

				$('#codeResults').html("").removeClass("alert").removeClass("info");
				e.preventDefault();
				if (formElement.valid()){
					var data = JSON.stringify(formElement.serializeObject());
					console.log(data);
					$.ajax({
						url: "ApiEAPManagement.php",
						type: "PUT",
						data: data,
						success: function(data){
							console.log(data);
							$('#codeResults').show().html("Saved").addClass("alert").addClass("info").fadeOut(5000);
						},
						error: function(jqXHR, textStatus, errorThrown){
							var message = $.parseJSON(jqXHR.responseText);
							$('#codeResults').show().html(message).addClass("alert");
						}
					});
				}
			});
			
		<?php }//end if SelectedCodesID ?>
		<?php echo $measureClassHide;?>
	});
</script>