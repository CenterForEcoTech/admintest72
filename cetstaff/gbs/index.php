<?php
ini_set('max_execution_time',600);
include_once("../_config.php");
include_once("../_datatableModelBase.php");
$_SESSION['isStaff'] = true;
$pageTitle = "Staff - GBS";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
		case "eap-templatecreator":
            $displayPage = "views/_eapTemplateCreator.php";
			break;
        case "manage-hoursentry":
            $displayPage = "views/_manageHoursEntry.php";
            break;
        case "manage-bullets":
            $displayPage = "views/_manageBullets.php";
            break;
        case "invoicingTools":
            $displayPage = "_invoicingToolsTab.php";
            break;
        case "invoicingTool-RWTAPipeline":
            $displayPage = "views/_invoicingTools_RWTAPipeline.php";
            break;			
        case "invoicingTool-MassCEC":
            $displayPage = "views/_invoicingTools_MassCEC.php";
            break;			
        case "invoicingTool-ColumbiaGas":
            $displayPage = "views/_invoicingTools_ColumbiaGas.php";
            break;			
        case "invoicingTool-BGas":
            $displayPage = "views/_invoicingTools_BGas.php";
            break;			
        case "invoicingTool-Bullets":
            $displayPage = "views/_invoicingTools_Bullets.php";
            break;			
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '<div class="container">
<h1>GBS</h1>';
include_once("_gbsMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>