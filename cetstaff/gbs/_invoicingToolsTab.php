<div id="<?php echo (!$showInvoicingToolsDropDown ? "invoicingToolDropDownItems" : "invoicingToolItems");?>">
	<a id="report-bgas" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTool-BGas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">BGas</a><br>
	<a id="report-columbiagas" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTool-ColumbiaGas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Columbia Gas Pipeline</a><br>
	<a id="report-bycode" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTool-RWTAPipeline" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">RW TA Pipeline</a><br>
	<a id="report-masscec" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTool-MassCEC" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">MassCEC</a><br>
	<hr>
	<a id="report-bullets" href="<?php echo $CurrentServer.$staffFolder;?>gbs/?nav=invoicingTool-Bullets" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button-linkmini">Last Month Bullets</a><br>

</div>