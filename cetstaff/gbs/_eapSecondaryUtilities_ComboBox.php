<?php include_once('_comboBoxIncludes.php');?>
		<script>
		  $(function() {
			$( ".comboboxUtilitiesSecondary" ).combobox({
				select: function(event, ui){
					<?php 
						if ($codesComboBoxFunction){
							echo $codesComboBoxFunction;
						}else{
							echo "
								$('.comboboxMeasureDiv').show();
								
								//clear each option if its selected
								$('.comboboxMeasures').each(function() { 
									$(this).removeAttr('selected')
								});

								//set the first option as selected
								$('.comboboxMeasures option:first').attr('selected', 'selected');
								//set the text of the input field to the text of the first option
								$(\"input[data-hiddenid='measure']\").val($(\".comboboxMeasures option:first\").text());
								
								
								updateFilters();								
							";
							//echo "alert($(ui.item).val());";
						} 
					?>
				}
			});
		  });
		</script>	
		<div class="<?php echo ($codesComboBoxDivColumns ? $codesComboBoxDivColumns : "eight");?> columns">
			<div>
			  <select class="comboboxUtilitiesSecondary parameters " id="SelectedSecondaryUtility" data-hiddenid="secondaryUtility">
				<option value="">Choose Secondary Utility</option>
				<?php 
					foreach ($UtilitySecondaries as $SecondaryUtilityName){
						echo "<option value=\"".$SecondaryUtilityName."\"".($SelectedSecondaryUtility==$SecondaryUtilityName ? " selected" : "").">".$SecondaryUtilityName."</option>";
					}
				?>
			  </select>
			</div>
		</div>