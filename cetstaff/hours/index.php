<?php
include_once("../_config.php");
include_once("../_datatableModelBase.php");

$pageTitle = "Staff - Hours";
$nav = isset($_GET["nav"]) ? $_GET["nav"] : "";
if ($nav){
    switch ($nav){
        case "manage-hoursentry":
            $displayPage = "views/_manageHoursEntry.php";
            break;
        default:
            break;
    }
}
// ----------------
// Render page here
// ----------------
include("../_header.php");
echo '
<div class="container">
<h1>Staff Hours Administration</h1>';
include_once("_hoursMenu.php");
if ($modelTemplate){
    include($modelTemplate);
}
include($displayPage);

echo '
</div> <!-- end container -->';
include("../_footer.php");
?>