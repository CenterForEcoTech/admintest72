<div id="<?php echo ($adminConsoleTabs ? $navMenuID : "nav-menu");?>">
    <ul>
		<?php if (count(array_intersect($adminSecurityGroups,$Security_Menu_Hours))){?>
			<li><a id="manage-hoursentry" href="<?php echo $CurrentServer.$staffFolder;?>hours/?nav=manage-hoursentry&StartDate=<?php echo date("m/01/Y");?>&EndDate=<?php echo date("m/t/Y");?>&WeekPickerNumber=null&ShowAllCodes=true" class="button-link">Hours Entry</a></li>
		<?php }?>
        <li><a href="<?php echo $CurrentServer.$staffFolder;?>" class="button-link back-to-console">Staff Console</a></li>
    </ul>
</div>
<div style="clear:both;"></div>
<?php if (!$adminConsoleTabs){?>
<script type="text/javascript">
    $(function(){
        var currentPage = '<?php echo $nav;?>',
            activeTab;
        if (currentPage){
            activeTab = $("#nav-menu").find("#" + currentPage);
            if (activeTab.length){
                activeTab.addClass("ui-state-highlight");
            }
        }
    });
</script>
<?php }?>