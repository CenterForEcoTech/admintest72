<?php
$_SESSION['isStaff'] = true;
include_once("_config.php");
$linkPage = ($_GET['linkPage'] ? $_GET['linkPage'] : false);
$CustomerID = ($_GET['CustomerID'] ? "&CustomerID=".$_GET['CustomerID'] : "");
if (isAdmin() && $linkPage == "Muni"){
	header('location: '.$CurrentServer.$adminFolder.'muni/?nav=auditForm&actualAudit=true'.$CustomerID);
}
if (isAjaxRequest()){
    // front controller for ajax calls
    $action = $_POST["action"];
    switch ($action){
        case "login":
            include("_loginauth.php");
            break;
        case "logout":
            include("_logout.php");
            break;
        default:
            break;
    }
    die();// ajax calls echo then die
} else {
    include('_header.php');
}
?>
<h1>Staff Console</h1>

<?php if (isAdmin()){
    include_once("_adminConsoleTabs.php");
}else{
    include_once("_notloggedin.php");
}//end if isAdmin
include('_footer.php');
?>