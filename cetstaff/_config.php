<?php
$staffFolder = "cetstaff/";
$adminFolder = "cetstaff/";
$actualAdminFolder = "cetadmin/";
include_once("_getRootFolder.php");

$dbDownTemplate = $rootFolder.$adminFolder."_dbIsDown.php";
include_once($rootFolder.'Global_Variables.php');
$adminFolder = "cetadmin/";
if (!isAdmin() && $_SERVER['SCRIPT_FILENAME'] != $rootFolder.$staffFolder.'index.php'){
    header("HTTP/1.0 401 Unauthorized");
    if (isAjaxRequest()){
        die();
    }
    header("location:".$CurrentServer.$staffFolder); // redirect to root.
}
$isAdminConsole = true;

$authData = SessionManager::getAdminAuthObject();
$adminSecurityGroupId = $authData->adminSecurityGroupId;
include_once("_adminSecurityGroups.php");
?>