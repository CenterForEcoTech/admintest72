<?php
if (!isAdmin()){
// =============
// not logged in
// =============
    ?>
<form id="login-form" class="auth-status">
    <input type="text" id="admin-username" name="username" title="Email" class="auto-watermarked">
    <input type="password" id="admin-password" name="password" title="Sharepoint Password" class="auto-watermarked">
    <input type="submit" id="login-submit" class="do-not-navigate button-link" value="Login">

    <div class="alert"></div>
</form>
<script>
    $(function(){
        var form = $("#login-form"),
            username = $("#admin-username"),
            password = $("#admin-password"),
            alertDiv = form.find(".alert");
        $("#login-submit").on('click', function(e){
			e.preventDefault();
			var $this = $(this);
            alertDiv.text('');
			$this.val('Validating...');
			$this.attr("disabled", "disabled");
			$this.addClass('disabled');
            if (username.val() && password.val()){
                $.ajax({
                    type: "POST",
                    data: {
                        action: "login",
                        username: username.val(),
                        password: password.val()
                    },
                    success: function(data){
						//console.log('thisdata #'+data.trim()+'#');
                        if (data.trim() == "Logged In" || data.trim() == '"Logged In"'){
                            location.reload(true);
                        } else {
                            alertDiv.text(data);
                        }
						$this.removeAttr("disabled"); 
						$this.removeClass('disabled');
						$this.val('Login');
                    },
					error: function(data){
						alertDiv.text(data.responseText);
						$this.removeAttr("disabled"); 
						$this.removeClass('disabled');
						$this.val('Login');
					}	
                });
            } else{
                alertDiv.text('Enter a Username and Password');
				$this.removeAttr("disabled"); 
				$this.removeClass('disabled');
				$this.val('Login');
            }
        });

        username.focus();
		
    });
</script>
<?php

}else{

// =============
// logged in
// =============
    /* @var $authData AdminAuthAttempt */
    $authData = SessionManager::getAdminAuthObject();
    $hoverText = $authData->adminFullName." is Logged In with email: ".$authData->adminEmail
            ." with the AdminID of ".$authData->adminId." in the SecurityGroupID ".$authData->adminSecurityGroupId;
    ?>
<span class="auth-status">Welcome <span class="info" title="<?php echo $hoverText;?>"><?php echo $authData->adminFullName;?></span> <a href="#logout" id="admin-logout" class="log-out do-not-navigate" title="Log out">Log out</a></span>

<div id="loginresults"></div>
<script>
	$(function(){
		$("#admin-logout").on('click', function(){
            $.ajax({
                url: "<?php echo $CurrentServer.$staffFolder;?>",
                type: "POST",
                data: {
                    action: "logout"
                },
                success: function(data){
					location.href = "<?php echo $CurrentServer;?>";
                    //location.reload();
                }
            });
		});
	});
</script>
<?php } // end if logged in ?>