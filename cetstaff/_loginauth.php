<?php
if ( isset($_SERVER["REMOTE_ADDR"]) ){
	$ip=$_SERVER["REMOTE_ADDR"];
} elseif (isset($_SERVER["SERVER_ADDR"])){
	$ip=$_SERVER["SERVER_ADDR"];
} else {
	$ip="Not Available";
}
if ($ip == '::1'){
    $ip = gethostbyname('cetonline.org');
}
$username = $_POST['username'];
$password = $_POST['password'];

$password=str_replace("&","&amp;",$password); //escape out invalid XML characters

require_once($rootFolder.$adminFolder.'authentication/SPOClient.php');

$url = $Config_SSOAuthenticationURL;
if ($username && $password){
	$siteLinkLogin = connectSPO($url,$username,$password);
	if ($siteLinkLogin){
		/*
		include_once($dbProviderFolder."AuthProvider.php");
		$authProvider = new AuthProvider($mysqli);
		$authAttempt = $authProvider->adminRetrieve($username,$ip);
		*/
		$userNameParts = explode("@",$username);
		$userName = explode(".",$userNameParts[0]);
		$authAttempt->adminFullName = ucwords($userName[0]." ".$userName[1]);
		$authAttempt->adminEmail = $username;
		$authAttempt->adminId = $username;
		$authAttempt->adminSecurityGroupId = 'Staff';
		$SecurityGroups[] = "Staff";
		include_once($siteRoot."_setupDataConnection.php");
		include_once($siteRoot."functions.php");
		$currentRequest = new Request();
		include_once($dbProviderFolder."HREmployeeProvider.php");
		$hrEmployeeProvider = new HREmployeeProvider($dataConn);
		$criteria= new stdClass();
		$criteria->email = $username;
		$employeeInfo = $hrEmployeeProvider->get($criteria);
		
		/* deprecated use of departments 2/2018 by Lisa D
		if ($employeeInfo->collection[0]->department == "Green Business Services" || $employeeInfo->collection[0]->department == "Green Business Services" || $employeeInfo->collection[0]->hoursTrackingGBS || $employeeInfo->collection[0]->department == "Operations"){
			$SecurityGroups[] = "GBSStaff";
		}
		if ($employeeInfo->collection[0]->department == "Green Home Services" || $employeeInfo->collection[0]->hoursTrackingGHS){
			$SecurityGroups[] = "GHSStaff";
		}
		*/
		$SecurityGroups[] = "GBSStaff";
		$SecurityGroups[] = "GHSStaff";
		$SecurityGroupList = implode(",",$SecurityGroups);
		$authAttempt->adminSecurityGroups = $SecurityGroupList;
		SessionManager::loginAdmin($authAttempt);	
		$_SESSION['AdminID'] = $username;
		$_SESSION['AdminEmail'] = $username;
		$_SESSION['AdminUsername'] = $username;
		output_json("Logged In");
	}else{
		output_json("Not Validated");
	}
}
?>