<?php $adminConsoleTabs = true;?>
<ul id="admin-tabs" class="tabs">
	<?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_HR))){$setTab="Staff";?><li><a id="tab-HR" href="#HR">HR</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GHS))){$setTab="GHS";?><li><a id="tab-GHS" href="#GHS">Residential (GHS)</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GBS))){$setTab="GBS";?><li><a id="tab-GBS" href="#GBS">Commercial (GBS)</a></li><?php }?>
	<?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Hours))){$setTab="Hours";?><li><a id="tab-Hours" href="#Hours">Hours Entry</a></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Resources))){$setTab=($setTab=="Staff" ? "Resources" : $setTab);?><li><a id="tab-Resources" href="#Resources">Resources</a></li><?php }?>
	<?php if ($hasTodo){
		$setTab = "Todo";
	?>
		<li><a id="tab-Todo" href="#Todo">To-Do</a></li>
	<?php }?>
</ul>

<ul class="tabs-content">
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_HR))){?><li id="HR">HR Menu:<?php $navMenuID="navMenu4"; include("hr/_hrMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Hours))){?><li id="Hours">Hours Menu:<?php $navMenuID="navMenu7"; include("hours/_hoursMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GHS))){?><li id="GHS">Residential Menu:<?php $navMenuID="navMenu5"; include("residential/_residentialMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_GBS))){?><li id="GBS">Commercial Menu:<?php $navMenuID="navMenu6"; include("gbs/_gbsMenu.php");?></li><?php }?>
    <?php if (count(array_intersect($adminSecurityGroups,$Security_Tabs_Resources))){?><li id="Resources">Resources Menu:<?php $navMenuID="navMenu8"; include("resources/_resourcesMenu.php");?></li><?php }?>
	<?php if ($hasTodo){?>
		<li id="Todo">
			<?php 
				foreach ($TodoList as $Todo){
					echo $Todo."<br>";
				}
			?>
		
		</li>
	<?php }?>

</ul>
<script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
    $(function(){
        var hash = window.location.hash,
            tabHashes = new Array("#HR","#GHS","#GBS","#Resources","#Todo","#Hours"),
            isTabHash = $.inArray(hash, tabHashes) !== -1,
            setTab = function(tabid){
                var tab = $("#tab-" + tabid),
                    content = $("#" + tabid);
                tab.addClass("active");
                content.addClass("active").show();
            };

        if (isTabHash){
            // let tabs.js take care of this
            setTab(hash.substring(1));
            // if it's the first tab, we need to reposition user at the top
            $("html,body").animate({
                scrollTop: 0
            });
        } else if (!(hash.length)){
            setTab("<?php echo $setTab;?>");
        }
		$(".back-to-console").hide();
    });
</script>