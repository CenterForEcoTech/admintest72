<?php
$isDevMode = false; // expect this to be set to true in siteconf/dbconf.php for dev instances

//Defines source for images, header, and archive function
$Config_DevFolder = substr(dirname(__FILE__), strlen($_SERVER["DOCUMENT_ROOT"]));
if (substr($Config_DevFolder,-1) != "/"){
	$Config_DevFolder .= "/";
}
$Config_DevFolder = "/".str_replace("\\","/",$Config_DevFolder);

$serverRootUrl = "http";
//IF going to use only SSL for the site then uncomment this section
if (isset($_SERVER["HTTPS"]) || array_key_exists("HTTPS", $_SERVER)) {
	if ($_SERVER["HTTPS"] == "on") {$secureServer = "s";}
}

$serverRootUrl .= $secureServer."://".$_SERVER["SERVER_NAME"];
if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
  $serverRootUrl .= ":".$_SERVER["SERVER_PORT"];
}
//echo $Config_DevFolder;
$CurrentServer = $serverRootUrl.$Config_DevFolder;

$siteRoot = str_replace("//","/",$_SERVER['DOCUMENT_ROOT'].$Config_DevFolder);
$repositoryApiFolder = $_SERVER['DOCUMENT_ROOT'].$Config_DevFolder."repository/";
$dbProviderFolder = $repositoryApiFolder."mySqlProvider/";
$indexProviderFolder = $repositoryApiFolder."indexedSearchProvider/";
$adminFolder = "cetadmin/";
$campaignImageUrlPath = $CurrentServer."images/Tags/";
$campaignImageFilePath = $siteRoot."images/Tags/";
$productImageUrlPath = $CurrentServer."images/Tags/";
$productImageFilePath = $siteRoot."images/Tags/";

// -------------
// $isProd is to be used to TURN OFF functionality ONLY. Do not use this as a reliable indicator of operating in a production environment.
// -------------
$isProd = strpos($CurrentServer,"cetdashboard.org");

//Title to be displayed in the Browser window
$SiteTitle = "CET Dashboard";
$SiteName = "CET Dashboard";
$SiteDescription = "Internal Resource for CET";
$SiteTesting = true;

//SMTP configuration
//Will work out of the box but with b2hmanagement.com domain as the sender though you may adjust to your own SMTP server for further control
$SMTPEmailSSLhost = "ssl://smtp.gmail.com";
$SMTPEmailhost = "smtp.gmail.com";
$SMTPEmailport = "465";
$SMTPEmailusername = "CTalerts@causetown.org";
$SMTPFromEmail = "CTalerts@causetown.org";
$SMTPFromEmailName = "Causetown";
$SMTPEmailpassword = "alert4users";


if (!isset($dbConfig)){
    include_once("siteconf/dbconf.php");
}
if (!isset($dbDownTemplate)){
    $dbDownTemplate = "_dbIsDown.php";
}
$mysqli = new mysqli($dbConfig->serv,$dbConfig->user,$dbConfig->pass,$dbConfig->inst);
if ($mysqli->connect_error || $_GET["test-db-down"] == "88-whale") {
    // TODO: send notification to team that db is down, but do it in some way that won't spam

    $message = date("r")." FATAL ERROR: Database: ".'Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    fwrite(STDERR, $message);
    $displayTemplateFile = $dbDownTemplate;
    header("HTTP/1.0 503 Service Unavailable");
    include_once("RenderPage.php");
    exit;
}
// TODO: get rid of mysql in favor of mysqli
$conn = mysqli_connect($dbConfig->serv,$dbConfig->user,$dbConfig->pass);
mysqli_select_db($conn, $dbConfig->inst);

$SQL_Configs = "SELECT * FROM configs";
$result_Configs = mysqli_query($result_Configs, $SQL_Configs);
$config = 0;
while ($config < mysqli_num_rows($result_Configs)){
	//Create a variable variable with the ConfigName as the name of the variable and the DefaultValue as the value
	mysqli_data_seek($result_Configs,$config);
 $fetch = mysqli_fetch_array($result_Configs);
 $ConfigName = $fetch["Config_Name"];
	mysqli_data_seek($result_Configs,$config);
 $fetch = mysqli_fetch_array($result_Configs);
 $ConfigValue = $fetch["Config_DefaultValue"];
	if ($ConfigValue == "true"){$ConfigValue = 1;}
	if ($ConfigValue == "false"){$ConfigValue = 0;}
	${$ConfigName} = $ConfigValue;
	$config++;
}

//END User Adjusted Variables
?>