<div>
    <form class="override_skel" method="post" id="FindOrgForm" action="<?php echo $CurrentServer."Organization/SignUp_ContactInfo";?>" name="FindOrgForm">

        <script type="text/javascript">
            var CETDASH = CETDASH || {};
            CETDASH.CharitySearchWidget = CETDASH.CharitySearchWidget || {
                sourceUrl : "<?php echo $CurrentServer;?>ApiCharitySearch",
                sourceType : "POST",
                minLength: 3,
                pageLength: 10,
                onSearch: function(event,ui){
                    var einField = $("#EIN"),
                        einLookupResults = $("#EINLookupResults");

                    einLookupResults.html("");
                    einField.val('');
                    $("#submit-button").addClass("inactive");
                },
                onSelect: function( event, ui ) {
                    var searchInput = $(this),
                        einElement = $("#EIN"),
                        record = ui.item.record;

                    einElement.css("color:black;");
                    einElement.val(record.ein);

                    $("#submit-button").removeClass("inactive");

                    searchInput.val(ui.item.value);
                    return false;
                }
            };
        </script>
        <label for="charity-search-box">
            <div>Search for any IRS approved 501(c)(3) public charity
                <a href="#combined-search-help" class="help-modal-icon colorbox-inline">Help on Search</a></div>
        </label>
            <?php include($siteRoot."views/_charityPickerWidget.php");?>
        <input type="hidden" name="EIN" id="EIN">
        <div style="clear:both;">
        <label>
            <input type="checkbox" id="are-you-fiscally-sponsored" name="company_differentnamethanfiscalsponsor" value="true"> This is our fiscal sponsor
        </label>
        </div>
        <div id="EINLookupResults" class="alert"><?php if(isset($_GET['InvalidEIN'])){echo "This EIN is invalid";}?></div>

        <div class="step_btn">
            <span>Step 1</span>
            <a href="#" id="submit-button" title="next" class="inactive">Next &gt;</a>
        </div>
    </form>

</div>
<?php include("_NonprofitSearchRegistrationHelp.php");?>
<script type="text/template" id="org-already-registered">
    <p>
        <span class="alert">It seems that &ldquo;{orgName}&rdquo; has already registered with <?php echo $SiteName;?>.</span>
        Please <a href="<?php echo $CurrentServer."RequestLogin/Organization/";?>{orgId}" class="colorbox-ajax">click here to send a password reset link</a>
        to the email address we have on file. Feel free to <a href="<?php echo $CurrentServer."ContactUs";?>">contact us</a> if you need further assistance.
    </p>
    <p>
        If you are a fiscally sponsored charity, you can select your fiscal sponsor, then check the &ldquo;This is our fiscal sponsor&rdquo; checkbox to proceed.
    </p>
</script>
<script type="text/javascript">
    $(function(){
        var einLookupUrl = "<?php echo $CurrentServer."EIN_Lookup_Proxy/{0}"?>",
            einLookupResults = $("#EINLookupResults"),
            einField = $("#EIN"),
            nextStepElement = $("#submit-button"),
            usesFiscalSponsor = $("#are-you-fiscally-sponsored"),
            alreadyRegisteredTemplate = $("#org-already-registered").html(),
            EINForm = $("#FindOrgForm"),
            validateEIN = function(){
                var einValue = einField.val().replace('-',''),
                    isCheckable = einValue.length == 9;
                einField.val(einValue);
                if (!isCheckable){
                    if (einValue.length){
                        einLookupResults.html("EIN must be 9 characters long");
                    } else {
                        einLookupResults.html("EIN must be assigned a value");
                    }
                    return false;
                }
                return true;
            },
            submitFormAction = function(){
                EINForm.submit();
            },
            lookupEIN = function(submitForm){
                var einValue = einField.val();
                if (!validateEIN()){
                    return false;
                }

                //override if Indicated as uses a fiscal sponsor
                if (usesFiscalSponsor.is(':checked')){
                    if (submitForm){
                        submitFormAction();
                    }
                    return true;
                }

                $.ajax({
                    url: einLookupUrl.replace("{0}", einValue),
                    success: function(data){
                        var message = "";
                        if (data.orgId){
                            message = alreadyRegisteredTemplate.replace("{orgName}", data.orgName).replace("{orgId}", data.orgId);
                            einLookupResults.html(message);
                        } else {
                            if (submitForm){
                                submitFormAction();
                            }
                        }
                    },
                    dataType: 'json'
                });
            };

        nextStepElement.click(function(e){
            e.preventDefault();
            if (!$(this).hasClass("inactive")){
                lookupEIN(true);
            } else {
                alert("Please select a valid unregistered charity first.");
            }
        });

        usesFiscalSponsor.click(function(e){
            einLookupResults.html("");
            if (einField.val() && usesFiscalSponsor.is(":checked")){
                $("#submit-button").removeClass("inactive");
            }
        });

        //run the lookupEIN if just the enter key is pressed on the form if they knew the EIN already
        $(document).keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                lookupEIN(true);
            }
        });
    });

</script>