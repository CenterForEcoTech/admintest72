<?php
/**
 * This is the help that goes with help bubbles for when a nonprofit is registering with causetown.
 */
?>
<div style="display:none;">
    <div id="combined-search-help" class="help-modal">
        <h3>Public Charity Search</h3>
        <p>
            Please search for your 501(c)(3) nonprofit organization by entering its official name or 9-digit EIN.
        </p>
        <p>
            If your organization is fiscally sponsored by another nonprofit, please search for the sponsoring organization,
            then check the &ldquo;This is our fiscal sponsor&rdquo; checkbox before clicking Next.
            (You will type in your own organization&rsquo;s name on the next step.)
        </p>
        <p>
            If your organization is neither a 501(c)(3) public charity (registered with the IRS) nor are you sponsored by one,
            we are currently unable to distribute funds to your organization.
        </p>
    </div>
    <div id="search-help" class="help-modal">
        <h3>Public Charity Search</h3>
        <p>
            Search for the name of your 501(c)(3) nonprofit organization and we&rsquo;ll automatically retrieve your contact information.
        </p>
        <!--
        <p>
            If your name doesn&rsquo;t come up immediately, try using the &ldquo;Advanced Features&rdquo; option and add a
            zip code or other filters to help us narrow our search.
        </p>
        -->
        <p>
            If your organization is fiscally sponsored, search for the name of your sponsoring organization, then be sure to
            check the &ldquo;EIN is for fiscal sponsor&rdquo; checkbox. You&rsquo;ll have the chance to type in your name
            on the next step.
        </p>
    </div>
    <div id="ein-help" class="help-modal">
        <h3>EIN for Tax&ndash;Exempt Organization</h3>
        <p>
            Although anyone can use our system to raise funds for their favorite causes,
            we are currently only able to distribute funds to organizations determined to be active 501(c)(3) public charities,
            as determined by the United States Internal Revenue Service.
        </p>
        <p>
            Many groups, including schools, college student clubs, and similar organizations are supported by 501(c)(3) public charities.
            If your charity falls into one of these groups, determine the EIN for your fiscal sponsor and enter that here, then
            check the &ldquo;EIN is for fiscal sponsor&rdquo; checkbox to register.
            If you aren&rsquo;t sure, <a href="<?php echo $CurrentServer;?>ContactUs">Contact Us</a> and we&rsquo;ll do our best to help you out.
        </p>
    </div>
    <div id="fiscalsponsor-help" class="help-modal">
        <h3>Are you fiscally sponsored by a 501(c)(3) public charity?</h3>
        <p>
            We feel <?php echo $SiteName;?> should work for any nonprofit and understand that many wonderful organizations operate under a fiscal sponsor.
            Check off that the EIN found is for your fiscal sponsor, and you&rsquo;ll be able to register in the system, but until your status is verified by the
            fiscal sponsor you&rsquo;ll have limited capabilities in the system.
        </p>
        <p>
            Mark this check box that the EIN is for your fiscal sponsor, and they will be contacted right away to approve your status.
        </p>
    </div>
    <div id="no-ein-help" class="help-modal">
        <h3>Don&rsquo;t have your own EIN?</h3>
        <p>
            Although anyone can use our system to raise funds for their favorite causes, we are currently only able to distribute funds
            to organizations determined to be active [501(c)(3) public charities], as determined by the United States Internal Revenue Service.
            Many groups, including schools, college student clubs, and similar organizations are supported by 501(c)(3) public charities.
            If you aren&rsquo;t sure, <a href="<?php echo $CurrentServer;?>ContactUs">Contact Us</a> and we&rsquo;ll do our best to help you out.
        </p>
        <p>
            Organizations without an EIN will need to supply their fiscal sponsor&rsquo;s information for distributing funds.
        </p>
    </div>
</div>