<?php
//requires the following variable to successfully process
/*
    $EmailTo
    $EmailSubject
    $EmailMsg
    $MemberID

    // optional variables to override default settings
    $EmailFrom (from ContactUs page)
    $EmailFromName
    $AddCommonFooter (default = true)
    $AsyncEmail (default = false)
*/
//
// Suppress errors
//
$originalErrorReportingLevel = error_reporting();
error_reporting(0);
//
// Pre-process
//
    $AddCommonFooter = (isset($AddCommonFooter)) ? $AddCommonFooter : true;
    if ($AddCommonFooter) {
        //Include common Footer:
        $EmailMsg = $EmailMsg."<br><Br>\r\n\r\nStay connected!<br><a href=\"http://www.facebook.com/cetonline\">facebook.com/cetonline\</a>";
    }
    $EmailMemberID = getMemberId();
    //set recording variables for log and sending
    if (!$EmailMemberID){$EmailMemberID = 0;}
    if (!$EmailTo){$EmailTo = SessionManager::getUserEmail();}
    $originalReplyToName = $replytoName;
    if (!$replyto){
        $replyto = (isset($Config_SMTPReplyToEmail) && filter_var($Config_SMTPReplyToEmail, FILTER_VALIDATE_EMAIL)) ? $Config_SMTPReplyToEmail : $SMTPFromEmail;
        $replytoName = $SMTPFromEmailName;
    }
    if (!$EmailFrom){
        $EmailFrom = $SMTPFromEmail;
        $EmailFromName = $SMTPFromEmailName;
    }
if (!isset($AsyncEmail)){
    $AsyncEmail = false;
}
//
// Setup Email
//
include_once($dbProviderFolder."MySqliEmailLogger.php");
$emailLogger = new MySqliEmailLogger($mysqli);
include_once($repositoryApiFolder."EmailHelper.php");
$emailMessage = new EmailMessage();
$emailMessage->MemberID = $EmailMemberID;
$emailMessage->addAddress($EmailTo, $EmailToName."");
$emailMessage->setFrom($EmailFrom, $EmailFromName."");
$emailMessage->setReplyTo($replyto, $replytoName."");
//add any BCC
if ($EmailBCC){
    $BCCEmails = explode(",",rtrim($EmailBCC,","));
    $b = 0;
    while ($b < count($BCCEmails)){
        // More than one BCC, just keep adding them!
        $emailMessage->addBCC($BCCEmails[$b]);
        $b++;
    }
}
// add subject
$emailMessage->Subject = $EmailSubject;
// add body
if ($SendPlainTextEmail){
    $emailMessage->setPlainTextBody($EmailMsg);
}else{
    $emailMessage->setHtmlBody($EmailMsg);
}
//
// Send email
//
if ($AsyncEmail && false){
    // TODO: implement a message queue and/or cron job to handle this
    $logResponse = $emailLogger->logDeferred($emailMessage);
    if ($logResponse->insertedId){
        $emailMessage->DeferredLogId = $logResponse->insertedId;
    }
    fsock_method_async($CurrentServer."Library/ApiSendEmail.php", json_encode($emailMessage), "PUT");
} else {
    $throwErrors = true;
    $emailProvider = new EmailProvider($emailLogger, $SMTPEmailhost, $SMTPEmailport, $SMTPEmailusername, $SMTPEmailpassword, $throwErrors);
    $emailResponse = $emailProvider->smtpSend($emailMessage, $isDevMode_Email);
    //
    // Handle response
    //
    if ($emailResponse->emailSent || $isDevMode_Email){
        $ThisEmailAlert = "\r\n\r\n<h4 style=\"color:green;\">Successfully Sent.</h4><br>\r\n";
        $SimpleEmailAlert = "<h4 style=\"color:green;\">Roger That! The following message was sent:</h4>
                    From: ".$EmailFrom."<br>\r\n
                    Subject: ".$EmailSubject."<br>\r\n
                    Message: ".$EmailMsg."<br><br>\r\n";
    }
    if ($emailResponse->message){
        $ThisEmailAlert .= "<p>" . $emailResponse->message . "</p>";
    }
}
//
// Reset error reporting
//
error_reporting($originalErrorReportingLevel);
?>