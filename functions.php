<?php
/**
 * Count the number of working days between two dates.
 *
 * This function calculate the number of working days between two given dates,
 * taking account of the Public festivities, Easter and Easter Morning days,
 * the day of the Patron Saint (if any) and the working Saturday.
 *
 * @param   string  $date1    Start date ('YYYY-MM-DD' format)
 * @param   string  $date2    Ending date ('YYYY-MM-DD' format)
 * @param   boolean $workSat  TRUE if Saturday is a working day
 * @param   string  $patron   Day of the Patron Saint ('MM-DD' format)
 * @return  integer           Number of working days ('zero' on error)
 *
 * @author Massimo Simonini <massiws@gmail.com>
 */
function getWorkdays($date1, $date2, $workSat = FALSE, $patron = NULL, $excludeEaster = NULL) {
  if (!defined('SATURDAY')) define('SATURDAY', 6);
  if (!defined('SUNDAY')) define('SUNDAY', 0);
  // Array of all public festivities
  $publicHolidays = array('01-01', '05-28', '07-04', '09-01', '11-24', '11-25', '12-26');
  //New Years, Memorial Day, 4th of July, Labor Day, Thanksgiving, Day after Thanksgiving, Christmas Day
  
  // The Patron day (if any) is added to public festivities
  if ($patron) {
    $publicHolidays[] = $patron;
  }
  /*
   * Array of all Easter Mondays in the given interval
   */
   $easterMondays = array();
  if ($excludeEaster){
	  $yearStart = date('Y', strtotime($date1));
	  $yearEnd   = date('Y', strtotime($date2));
	  for ($i = $yearStart; $i <= $yearEnd; $i++) {
		$easter = date('Y-m-d', easter_date($i));
		list($y, $m, $g) = explode("-", $easter);
		$monday = mktime(0,0,0, date($m), date($g)+1, date($y));
		$easterMondays[] = $monday;
	  }
  }
  $start = strtotime($date1);
  $end   = strtotime($date2);
  $workdays = 0;
  for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i)) {
    $day = date("w", $i);  // 0=sun, 1=mon, ..., 6=sat
    $mmgg = date('m-d', $i);
    if ($day != SUNDAY &&
      !in_array($mmgg, $publicHolidays) &&
      !in_array($i, $easterMondays) &&
      !($day == SATURDAY && $workSat == FALSE)) {
        $workdays++;
    }
  }
  return intval($workdays);
}



//round to the nearest .25
function floorToFraction($number, $denominator = 4){
    $x = $number * $denominator;
    $x = round($x);
    $x = $x / $denominator;
    return $x;
}

/**
*
* Author: CodexWorld
* Function Name: getDistance()
* $addressFrom => From address.
* $addressTo => To address.
* $unit => Unit type.
*
**/
function getDistance($addressFrom, $addressTo, $unit,$driving = true){
    //Change address format
    $formattedAddrFrom = str_replace(' ','+',$addressFrom);
    $formattedAddrTo = str_replace(' ','+',$addressTo);
    
	if (!$driving){
		//Send request and receive json data
		$geocodeFrom = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$formattedAddrFrom.'&sensor=false');
		$outputFrom = json_decode($geocodeFrom);
		$geocodeTo = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$formattedAddrTo.'&sensor=false');
		$outputTo = json_decode($geocodeTo);
		
		//Get latitude and longitude from geo data
		$latitudeFrom = $outputFrom->results[0]->geometry->location->lat;
		$longitudeFrom = $outputFrom->results[0]->geometry->location->lng;
		$latitudeTo = $outputTo->results[0]->geometry->location->lat;
		$longitudeTo = $outputTo->results[0]->geometry->location->lng;
		
		//Calculate distance from latitude and longitude
		$theta = $longitudeFrom - $longitudeTo;
		$dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344).' km';
		} else if ($unit == "N") {
			return ($miles * 0.8684).' nm';
		} else {
			return $miles.' mi';
		}
	}else{
		$googleDrivingMatrix = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$formattedAddrFrom.'&destinations='.$formattedAddrTo.'&units=imperial');
		$output = json_decode($googleDrivingMatrix);
		//print_pre($output);
		$distance = $output->rows[0]->elements[0]->distance->text;
		return $distance;
	}
}


//implode a mulitdimensional array
function implode_r($g, $p) {
    return is_array($p) ?
           implode($g, array_map(__FUNCTION__, array_fill(0, count($p), $g), $p)) : 
           $p;
}

function object_to_array($obj){

    //we want to preserve the object name to the array
    //so we get the object name in case it is an object before we convert to an array (which we lose the object name)
    $obj_name = false;
    if(is_object($obj)){
        $obj_name = get_class($obj);
        $obj = (array) $obj;
    }

    //if obj is now an array, we do a recursion
    //if obj is not, just return the value
    if(is_array($obj)) {

        $new = array();

        //initiate the recursion
        foreach($obj as $key => $val) {
            //we don't want those * infront of our keys due to protected methods
            $new[$key] = object_to_array($val);
        }

        //now if the obj_name exists, then the new array was previously an object
        //the new array that is produced at each stage should be prefixed with the object name
        //so we construct an array to contain the new array with the key being the object name
        if(!empty($obj_name)){
            $new = array(
                $obj_name => $new,
            );
        }

    }else{

        $new = $obj;

    }

    return $new;

}


//str_replace last occurance
function str_replace_last( $search , $replace , $str ) {
    if( ( $pos = strrpos( $str , $search ) ) !== false ) {
        $search_length  = strlen( $search );
        $str    = substr_replace( $str , $replace , $pos , $search_length );
    }
    return $str;
}

function exceldatetotimestamp($xl,$dateFormat)  //Y-m-d format returned
{
//if ($xl){
	$timestamp = ($xl+1 - 25569) * 86400;
//}else{
//	$timestamp = "";
//}	
if (date('Y-m-d', $timestamp) == '2036-02-06'){
	
	$timestamp = ($dateFormat == "mm/dd/YYYY" ? "00/00/0000" : "0000-00-00");
}else{
	$timestamp = ($dateFormat == "mm/dd/YYYY" ? date('m/d/Y', $timestamp) : date('Y-m-d', $timestamp));
}
return $timestamp;
}


function array_orderby()
{
	$args = func_get_args();
	$data = array_shift($args);
	foreach ($args as $n => $field) {
		if (is_string($field)) {
			$tmp = array();
			foreach ($data as $key => $row)
				$tmp[$key] = $row[$field];
			$args[$n] = $tmp;
			}
	}
	$args[] = &$data;
	call_user_func_array('array_multisort', $args);
	return array_pop($args);
}


//used to convert Column Number to Alphabet letter when applying values to new spreadsheet
function getNameFromNumber($num) {
	$numeric = ($num - 1) % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval(($num - 1) / 26);
	if ($num2 > 0) {
		return getNameFromNumber($num2) . $letter;
	} else {
		return $letter;
	}
}


/* creates a compressed zip file */
function create_zip($files = array(),$destination = '',$overwrite = false,$folderLocation = NULL) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	touch($destination);
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if(!$zip->open($destination,(file_exists($destination) ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE))) {
			return false;
			
		}
		//add the files
		foreach($valid_files as $file) {
			$fileLocal = str_replace($folderLocation,"",$file);
			$zip->addFile($file,$fileLocal);
			
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}

//mainly used to convert money with $ and , to just a decimal
function toInt($str)
{
    return preg_replace("/([^0-9\\.])/i", "", $str);
}

//Get user name from a CET email address yosh.schulman@cetonline.org
function emailName($cetEmailAddress){
	$emailParts = explode("@",$cetEmailAddress);
	$nameParts = explode(".",$emailParts[0]);
	if (count($nameParts)){
		$emailName = ucfirst($nameParts[0])." ".ucfirst($nameParts[1]);
		return $emailName;
	}else{
		return NULL;
	}
}
//Get user initials from a CET email address yosh.schulman@cetonline.org
function emailInitials($cetEmailAddress){
	$emailParts = explode(".",$cetEmailAddress);
	if (count($emailParts)){
		$emailInitials = strtoupper(substr($emailParts[0],0,1).substr($emailParts[1],0,1));
		$emailInitials = "<span title='".$cetEmailAddress."'>".$emailInitials."</span>";
		return $emailInitials;
	}else{
		return NULL;
	}
}

//Given a date in time, find the date in the array that is closest to it
function find_closest($array, $date){
	//$count = 0;
	foreach($array as $day)
	{
		//$interval[$count] = abs(strtotime($date) - strtotime($day));
			$interval[] = abs(strtotime($date) - strtotime($day));
		//$count++;
	}

	asort($interval);
	$closest = key($interval);

	return $array[$closest];

}


function erase_val(&$myarr) {
    $myarr = array_map(create_function('$n', 'return null;'), $myarr);
}

function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
}
  
function strpos_arr($haystack, $needle) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $what) {
        if(($pos = strpos($haystack, $what))!==false) return $pos;
    }
    return false;
}

function convert_to_csv($input_array, $output_file_name, $delimiter)
{
    $temp_memory = fopen($output_file_name, 'w');
    /** loop through array  */
    foreach ($input_array as $line) {
        /** default php csv handler **/
        fputcsv($temp_memory, $line, $delimiter);
    }
    /** rewrind the "file" with the csv lines **/
    fseek($temp_memory, 0);
    /** modify header to be downloadable csv file **/
	if (fclose($output_file_name)){
		return true;
	}else{
		return false;
	}
}

function datediff($time1, $time2, $unit = 'day', $roundDirection = 'up') {

    try {
        $datetime1 = new DateTime ($time1);
    } catch (Exception $e) {
    }
    try {
        $datetime2 = new DateTime ($time2);
    } catch (Exception $e) {
    }
    $interval = $datetime1->diff ( $datetime2 );

	//hours:
	if($unit=='hours'){
		return $interval->h ;
	}
	//full days:
	if($unit=='day'){
		return $interval->days ;
	}
	//full weeks:
	else if($unit=='week'){ 
		if ($roundDirection == "up"){
			return ceil($interval->days /7) ;
		}else{
			return floor($interval->days /7) ;
		}
	}
	//full months:
	else if($unit=='month'){ 
		return ($interval->y * 12) + $interval->m;
	}
	//full years:
	else if($unit=='year'){ 
		return $interval->y;
	}
	else{
		return null;
	}
}

//dedup multidimeninsion array
function super_unique($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = super_unique($value);
    }

  }
  return $result;
}	



// sort $a2 by key, using the order of $orderWareHouseByID
function my_uksort($a, $b) {
    global $orderWareHouseByID;
    return $orderWareHouseByID[$a] < $orderWareHouseByID[$b] ? -1 : 1;
}

function center_geolocation($geolocations){
	/*
	Provide a relatively accurate center lat, lon returned as a list pair, given
	a list of list pairs.
	ex: in: geolocations = ((lat1,lon1), (lat2,lon2),)
		out: (center_lat, center_lon)

	*/
	$x = 0;
	$y = 0;
	$z = 0;
	
	$totalLocations = count($geolocations);

    foreach($geolocations as $geoObject){
        if ($geoObject->lat){
            $lat = (float)($geoObject->lat)*(M_PI/180);
            $lon = (float)($geoObject->lng)*(M_PI/180);
            $x += cos($lat) * cos($lon);
            $y += cos($lat) * sin($lon);
            $z += sin($lat);
        }else{
            $totalLocations -=1;
        }
    }
	$x = (float)($x/$totalLocations);
	$y = (float)($y/$totalLocations);
	$z = (float)($z/$totalLocations);
	
	$centerLong = (atan2($y, $x))*(180/M_PI);
	$centerLat = (atan2($z, sqrt(($x * $x) + ($y * $y))))*(180/M_PI);
	return "(".$centerLat.", ".$centerLong.")";	
}

function print_pre($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}
//sub sorts an array much like SQL SORT BY colum1, column2
//where $field is array('thisfirstfield','thissecondfield','etc');
function sortArray($data, $field)
  {
    if(!is_array($field)) $field = array($field);
    usort($data, function($a, $b) use($field) {
      $retval = 0;
      foreach($field as $fieldname) {
        if($retval == 0) $retval = strnatcmp($a[$fieldname],$b[$fieldname]);
      }
      return $retval;
    });
    return $data;
  }

//converts an stdClass Object to an array
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

//converts an array to stdClass Object
function arrayToObject($d) {
	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return (object) array_map(__FUNCTION__, $d);
	}
	else {
		// Return object
		return $d;
	}
} 


// used on admin reports to maintain column width
function truncate_text($text, $limit, $type=NULL) {
	if ($type == "word"){
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
	}else{
      if (strlen($text) > $limit) {
          $text = '<span title="'.$text.'">'.substr($text, 0, $limit) . '...</span>';
      }
	}
      return $text;
}


function get_FaceBook_timezone_offset($origin_tz) {
	$offset = get_timezone_offset('America/Los_Angeles', $origin_tz);
	$offset_inHours = round($offset/3600);
	if ($offset_inHours > 1){return " +".$offset_inHours." hours";}
	if ($offset_inHours < 1){return "";}
	if ($offset_inHours < 2){return " +1 hour";}
}
//return the differences of offset between timezones
function get_timezone_offset($remote_tz, $origin_tz = null) {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    try {
        $origin_dt = new DateTime("now", $origin_dtz);
    } catch (Exception $e) {
    }
    try {
        $remote_dt = new DateTime("now", $remote_dtz);
    } catch (Exception $e) {
    }
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

function passCrypt($pass){
	return sha1(md5(md5(sha1(md5(sha1(sha1(md5($pass))))))));
}

//BillCom functions
//String fix for urencoding that doesn't work with their api
function BillComCharReplace($str){
	$Str = str_replace("&amp;","And",$str);
	$Str = str_replace("&","And",$Str);
	return $Str;
}

class StateNameAndAbbrConverter{
    // https://gist.github.com/2776900
    static $us_state_abbrevs_names = array(
        'AL'=>'ALABAMA',
        'AK'=>'ALASKA',
        'AS'=>'AMERICAN SAMOA',
        'AZ'=>'ARIZONA',
        'AR'=>'ARKANSAS',
        'CA'=>'CALIFORNIA',
        'CO'=>'COLORADO',
        'CT'=>'CONNECTICUT',
        'DE'=>'DELAWARE',
        'DC'=>'DISTRICT OF COLUMBIA',
        'FM'=>'FEDERATED STATES OF MICRONESIA',
        'FL'=>'FLORIDA',
        'GA'=>'GEORGIA',
        'GU'=>'GUAM GU',
        'HI'=>'HAWAII',
        'ID'=>'IDAHO',
        'IL'=>'ILLINOIS',
        'IN'=>'INDIANA',
        'IA'=>'IOWA',
        'KS'=>'KANSAS',
        'KY'=>'KENTUCKY',
        'LA'=>'LOUISIANA',
        'ME'=>'MAINE',
        'MH'=>'MARSHALL ISLANDS',
        'MD'=>'MARYLAND',
        'MA'=>'MASSACHUSETTS',
        'MI'=>'MICHIGAN',
        'MN'=>'MINNESOTA',
        'MS'=>'MISSISSIPPI',
        'MO'=>'MISSOURI',
        'MT'=>'MONTANA',
        'NE'=>'NEBRASKA',
        'NV'=>'NEVADA',
        'NH'=>'NEW HAMPSHIRE',
        'NJ'=>'NEW JERSEY',
        'NM'=>'NEW MEXICO',
        'NY'=>'NEW YORK',
        'NC'=>'NORTH CAROLINA',
        'ND'=>'NORTH DAKOTA',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'OH'=>'OHIO',
        'OK'=>'OKLAHOMA',
        'OR'=>'OREGON',
        'PW'=>'PALAU',
        'PA'=>'PENNSYLVANIA',
        'PR'=>'PUERTO RICO',
        'RI'=>'RHODE ISLAND',
        'SC'=>'SOUTH CAROLINA',
        'SD'=>'SOUTH DAKOTA',
        'TN'=>'TENNESSEE',
        'TX'=>'TEXAS',
        'UT'=>'UTAH',
        'VT'=>'VERMONT',
        'VI'=>'VIRGIN ISLANDS',
        'VA'=>'VIRGINIA',
        'WA'=>'WASHINGTON',
        'WV'=>'WEST VIRGINIA',
        'WI'=>'WISCONSIN',
        'WY'=>'WYOMING',
        'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
        'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
        'AP'=>'ARMED FORCES PACIFIC');

    private static function convert_state($value) {
        $key = strtoupper($value);
        $array = strlen($key) == 2 ? self::$us_state_abbrevs_names : array_flip(self::$us_state_abbrevs_names);
        if (isset($array[$key])){
            return $array[$key];
        }
        return $value; // no conversion
    }

    public static function abbrToName($abbr){
        if (strlen($abbr) == 2){
            $convertedValue = self::convert_state($abbr);
            if ($convertedValue != $abbr){
                return ucwords(strtolower($convertedValue));
            }
        }
        return $abbr; // no conversion
    }

    public static function nameToAbbr($name){
        if (strlen($name) == 2 ){
            $key = strtoupper($name);
            if (isset(self::$us_state_abbrevs_names[$key])){
                return $key;
            }
            return $name; // no conversion
        }
        return self::convert_state($name);
    }

    public static function getStateList(){
        $listCollection = array();
        foreach (self::$us_state_abbrevs_names as $abbr => $allcapsName){
            $listCollection[$abbr] = ucwords(strtolower($allcapsName));
;        }
        return $listCollection;
    }
}

//conform states to uppercase two characters
function BillComStateFix($name){
    return StateNameAndAbbrConverter::nameToAbbr($name);
}

function StateAbbrToFull($abbr){
    return StateNameAndAbbrConverter::abbrToName($abbr);
}


function GetWelcomeEmailContext($UserName,$ActivationCodeURL,$OptionalMessage='',$OptionalPSNote=''){
    $emailToName = (strpos($UserName, "@")) ? "Friend" : $UserName;
    $CurrentServer = $GLOBALS['CurrentServer'];
    $SiteName = $GLOBALS['SiteName'];
    $emailContent = (object)Array(
        "emailToName" => $emailToName,
        "activationCodeUrl" => $ActivationCodeURL,
        "siteHomeUrl" => $CurrentServer."Home",
        "contactUsUrl" => $CurrentServer."ContactUs",
        "optionalMessage" => $OptionalMessage,
        "optionalPsNote" => $OptionalPSNote,
        "siteName" => $SiteName
    );
    return $emailContent;
}

//protect email
function encode_email($e)
{
	if (!strpos($e, "@")){
		$e = $e."@".$GLOBALS['Config_ContactDomain'];
	}
	for ($i = 0; $i < strlen($e); $i++) { $output .= '&#'.ord($e[$i]).';'; }
	return $output;
}

//extract email from string
function extract_emails_from($string){
  preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
  return $matches[0];
}

//create dynamic variables based on table
function CreateSQLVariables($result){
	$i = 0;
	while ($i < mysql_num_fields($result)){
		global ${mysql_field_name($result,$i)};
		${mysql_field_name($result,$i)} = mysql_result($result,0,mysql_field_name($result,$i));
		//echo mysql_field_name($result,$i)."=".mysql_result($result,0,mysql_field_name($result,$i))."<br>";
		$i++;
	}
}

function CreateGlobalArrayVariables($assocArray){
	foreach ($assocArray as $key => $value){
		global ${$key};
		${$key} = $value;
	}
}

//take 10digits and turn into phone display
function format_tel_num($number){
	return preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($number)), 2);
}

//adds an onFocus remove the text and change color to black
function onFocus($str){
	return " onFocus=\"if(this.value=='".$str."'){this.value='';this.style.color='black';}\" onBlur=\"if(this.value==''){this.value='".$str."';this.style.color='silver';}\"";
}

//checks to see if the $check variable is submitted via post first...if not then checks to get the variable via get method..if not return empty string
function GetParameter($check){
	$VariablePresent = false;
	if(isset($_POST[$check])){
		return $_POST[$check];
		$VariablePresent = true;
	}
	if(isset($_GET[$check])){
		return $_GET[$check]; 
		$VariablePresent = true;
	}
	if(!$VariablePresent){
		return '';
	}
}

function MySQLTimeStamp($thisTimeStamp){
	$timeArr = explode(" ",$thisTimeStamp);
	return MySQLDate($timeArr[0]);
}
function MySQLTimeStampTime($thisTimeStamp){
	$timeArr = explode(" ",$thisTimeStamp);
	return date("g:i a",strtotime($timeArr[1]));
}
function MySQLTimeStampDisplay($thisTimeStamp){
	$date = MySQLTimeStamp($thisTimeStamp);
	$time = MySQLTimeStampTime($thisTimeStamp);
	return $date." ".$time;
}

function MySqlDate($thisdate) {
//Use this when displaying a MySQL date into the fields for the end user to view in mm/dd/yyyy format
	// Explode contents to an aray
	$dateArr = explode('/',$thisdate);

	//check to see if date is coming from mysql database and create the array
	if ($dateArr[1] == ''){
		$dateArr = explode('-',$thisdate);
		// Grab some details
		$year = $dateArr[0];
		$month = $dateArr[1];
		$day = $dateArr[2];
		$ShowThisDisplayDate = $month."/".$day."/".$year;
	} else {
		$year = $dateArr[2];
		$month = $dateArr[0];
		$day = $dateArr[1];
		$ShowThisDisplayDate = $month."/".$day."/".$year;
	}
	if (trim($ShowThisDisplayDate) != "//" && trim($ShowThisDisplayDate) != "00/00/0000"){
		return $month."/".$day."/".$year;
	}
}

function MySqlDateCompare($thisdate) {
//Use this when Comparing a MySQL date into the fields for the end user to view in mm/dd/yyyy format
	// Explode contents to an aray
	$dateArr = explode('/',$thisdate);

	//check to see if date is coming from mysql database and create the array
	if ($dateArr[1] == ''){
		$dateArr = explode('-',$thisdate);
	}
	// Grab some details
	$year = $dateArr[0];
	$month = $dateArr[1];
	$day = $dateArr[2];
	return $month."/".$day."/".$year;
}

function MySqlDateUpdate($thisdate){
//Use this when updating the MySQL database with a date to convert a mm/dd/yyyy format into yyyy-mm-dd format
	// Explode contents to an aray
	$dateArr = explode('/',$thisdate);
	
	//check to see if coming from mysqldate already
	if (!$dateArr[1]){
		$ShowThisUpdateDate = $thisdate;
	}else{
		// Grab some details
		$month = $dateArr[0];
		$day = $dateArr[1];
		$year = $dateArr[2];
		$ShowThisUpdateDate = $year."-".$month."-".$day;
	}
	if (trim($ShowThisUpdateDate) != "--"){
		return $ShowThisUpdateDate;
	}else {
		return "0000-00-00";
	}
}

function IsValidDate($dateString){
	$dateRegex = "/[01]{0,1}[0-9]{1}\/[0-3]{0,1}[0-9]{1}\/[12]{1}[0-9]{3}/";
	return preg_match($dateRegex, $dateString) && strtotime($dateString);
}

function IsValidTime($timeString){
	$timeRegex = "/[012]{1}[0-9]{1}:[0-5]{1}[0-9]{1}/";
	return preg_match($timeRegex, $timeString) && strtotime($timeString);
}

function roundUpToHalfHour($time) {
    $minutes = date('i', $time);
    $minutesToAdd = 30 - ($minutes % 30);
    $secondsToAdd = ($minutesToAdd < 30) ? $minutesToAdd * 60 : 0;
    return $time + $secondsToAdd;
}

//remove problematic characters for database
function Chkstring($str){
	$ReturnStr = addslashes(htmlspecialchars($str));
	return $ReturnStr;
}

//replaces money_format() which doesn't work on all machines
function money($amount,$separator=true,$simple=false){
    return
        (true===$separator?
            (false===$simple?
                number_format($amount,2,'.',','):
                str_replace('.00','',money($amount))
            ):
            (false===$simple?
                number_format($amount,2,'.',''):
                str_replace('.00','',money($amount,false))
            )
        );
}


//Get the Lat and Long based on address used for mapping
//address is in format street,city,state
function GetGeoCode($address){
	$address = str_replace(" ","+",$address);
	$url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
	//echo $url;
	//if(CheckOnlineStatus($url)=="Offline"){return "Bad URL";}else{return "Connected";}
	$output=file_get_contents($url);
	$out= json_decode($output);
	$lat = $out->results[0]->geometry->location->lat;
	$long = $out->results[0]->geometry->location->lng;
	return $lat.",".$long;
	//echo '<br>Lat is '. $lat;
	//echo '<br>Long is '. $long;
}

/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  this routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). it is being used to calculate     :*/
/*::  the distance between two zip codes or postal codes using our           :*/
/*::  zipcodeworld(tm) and postalcodeworld(tm) products.                     :*/
/*::                                                                         :*/
/*::  definitions:                                                           :*/
/*::    south latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  passed to function:                                                    :*/
/*::    lat1, lon1 = latitude and longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = latitude and longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'm' is statute miles                                   :*/
/*::                  'k' is kilometers (default)                            :*/
/*::                  'n' is nautical miles                                  :*/
/*::  united states zip code/ canadian postal code databases with latitude & :*/
/*::  longitude are available at http://www.zipcodeworld.com                 :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@zipcodeworld.com                   :*/
/*::                                                                         :*/
/*::  official web site: http://www.zipcodeworld.com                         :*/
/*::                                                                         :*/
/*::  hexa software development center © all rights reserved 2004            :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
		return ($miles * 0.8684);
	} else {
		return $miles;
	}
}

function haversineDistance($lat1, $long1, $lat2, $long2, $unit) {

    $earthRadius = 3959; // miles
    $unit = strtoupper($unit);
    if ($unit == "K") {
        $earthRadius = ($earthRadius * 1.609344);
    } else if ($unit == "N") {
        $earthRadius = ($earthRadius * 0.8684);
    }
    $distance = $earthRadius * acos(
        cos( deg2rad($lat1) ) *
            cos( deg2rad($lat2) ) *
            cos( deg2rad($long2) - deg2rad($long1)) +
            sin( deg2rad($lat1) ) * sin( deg2rad($lat2) )
    );
    return $distance;
}

//echo distance(32.9697, -96.80322, 29.46786, -98.53506, "m") . " miles<br>";
//echo distance(32.9697, -96.80322, 29.46786, -98.53506, "k") . " kilometers<br>";
//echo distance(32.9697, -96.80322, 29.46786, -98.53506, "n") . " nautical miles<br>";

function isAjaxRequest(){
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function render_template($filename, $variables) {
	extract($variables);
	ob_start();
	require( "templates/" . $filename . ".php");
	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function output_json($object, $dieImmediately = true){
	// http://www.dzone.com/snippets/php-headers-serve-json
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	echo json_encode($object);
	if ($dieImmediately){
		die();
	}
}

/**
 * exportCSV takes an array of arrays (recordset) and converts it to CSV. By default, expects to output directly as a download
 * @param $data - the recordset as an array of arrays
 * @param array $col_headers - optional column headers
 * @param bool $return_string - true to return the output as a string
 * @return string
 */
function exportCSV($data, $col_headers = array(), $return_string = false)
{
    $stream = ($return_string) ? fopen ('php://temp/maxmemory', 'w+') : fopen ('php://output', 'w');

    if (!empty($col_headers))
    {
        fputcsv($stream, $col_headers);
    }

    foreach ($data as $record)
    {
        fputcsv($stream, $record);
    }

    if ($return_string)
    {
        rewind($stream);
        $retVal = stream_get_contents($stream);
        fclose($stream);
        return $retVal;
    }
    else
    {
        fclose($stream);
    }
}

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

class Request {
    public $url_elements;
    public $verb;
    public $parameters;
    public $rawData;
    public $jsonData;

    public function __construct() {
        $this->verb = $_SERVER['REQUEST_METHOD'];
        $this->url_elements = explode('/', $_SERVER['PATH_INFO']);
        $this->parseIncomingParams();
        // initialise json as default format
        $this->format = 'json';
        if(isset($this->parameters['format'])) {
            $this->format = $this->parameters['format'];
        }
        return true;
    }

    public function parseIncomingParams() {
        $parameters = array();

        // first of all, pull the GET vars
        if (isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $parameters);
        }

        // now how about PUT/POST bodies? These override what we got from GET
        $body = file_get_contents("php://input");
        $this->rawData = $body;
        $content_type = false;
        if(isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        // firefox adds charset to content type, so we have to tease out the content type
        // we're only using json, so let's start there
        if (startsWith($content_type, "application/json")){
            $content_type = "application/json";
        }

        switch($content_type) {
            case "application/json":
                $body_params = json_decode($body);
                $this->jsonData = $body_params;
                if($body_params) {
                    foreach($body_params as $param_name => $param_value) {
                        $parameters[$param_name] = $param_value;
                    }
                }
                $this->format = "json";
                break;
            case "application/x-www-form-urlencoded":
                parse_str($body, $postvars);
                foreach($postvars as $field => $value) {
                    $parameters[$field] = $value;

                }
                $this->format = "html";
                break;
            default:
                // we could parse other supported formats here
                break;
        }
        $this->parameters = $parameters;
    }

    public function getAsRecord(){
        return (object)json_decode($this->rawData);
    }
}

/**
 * Copied largely from http://petewarden.typepad.com/searchbrowser/2008/06/how-to-post-an.html
 * Modified from original at http://w-shadow.com/blog/2007/10/16/how-to-run-a-php-script-in-the-background/
 * @param $url
 * @param $params
 * @return bool
 */
function fsock_post_async($url, $params)
{
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);
    return fsock_method_async($url, $post_string);
}
// NOTE: this is NOT asynchronous, and to an SSL target, is very slow
function fsock_method_async($url, $data_string, $method = "POST", $returnResponse = false){
    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    if (!$fp){
        return false;
    }

    $out = $method." ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($data_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($data_string)) $out.= $data_string;

    fwrite($fp, $out);
    $response = true;
    if ($returnResponse){
        $response = fread($fp, 1000);
    }
    fclose($fp);
    return $response;
}

require_once('repository/SessionManager.php');

// -------
// user session helpers
// -------
function isLoggedIn(){
    return SessionManager::isLoggedIn();
}

function isMerchant(){
    return SessionManager::isMerchant();
}

function isIndividual(){
    return SessionManager::isIndividual();
}

function isOrganization(){
    return SessionManager::isOrganization();
}

function isAdmin(){
//	return true;
    return SessionManager::isAdmin();
}
function isStaff(){
	if (isset($_SESSION['AdminID']) && trim($_SESSION['AdminID']) !=""){
		return true;
	}else{
		return false;
	}
}

function getAdminId(){
    return SessionManager::getAdminId();
}

function getMemberId(){
    return SessionManager::getMemberId();
}

function getMerchantId(){
    return SessionManager::getMerchantId();
}

function getOrganizationId(){
    return SessionManager::getOrganizationId();
}

function pushValidationMessage($message){
    SessionManager::pushValidationMessage($message);
}

function peekValidationMessage(){
    return SessionManager::peekValidationMessage();
}

function popValidationMessage(){
    return SessionManager::popValidationMessage();
}
?>