<style>
.bold {font-weight:bold;}
</style>
<div class="row">
	<div class="twelve columns">
		<a href="http://www.cetonline.org" target="cet"><img src="<?php echo $CurrentServer;?>images/CET_logo.png"></a>
	</div>
</div>
<div class="row">
	<div class="twelve columns">
		The following information is requested by the federal government in order to monitor our compliance with various civil rights laws.
		You are not required to furnish this information, but are encouraged to do so.
		The law requires that we may not discriminate based upon this information, nor whether you choose to furnish it.
		The information you provide will only be used for equal employment and diversity record keeping and reporting required by law. The information you provide is also recorded anonymously. 
		If you do not wish to furnish the above information, please check the box below.
		
	</div>
</div>
<form id="eeoForm">
	<div class="row">
		<div class="six columns bold">
			I do not wish to furnish this information:
		</div>
		<div class="six columns">
			<input type="hidden" name="token" value="<?php echo $token;?>">
			<input type="checkbox" id="participation" name="participation" value="Opted-Out">
		</div>
	</div>
	<div id="eeoSurvey">
		<div class="row">
			<div class="two columns bold">
				Gender:
			</div>
			<div class="two columns">
				<select name="gender" id="gender" class="three columns">
					<option value="">Please Choose</option>
					<option value="Female">Female</option>
					<option value="Male">Male</option>
				</select>
			</div>
		</div>

		<div class="row">
			<div class="two columns bold">
				Ethnicity:
			</div>
			<div class="ten columns">
				<select name="ethnicity" id="ethnicity">
					<option value="">Are you Hispanic or Latino?</option>
					<option value="">No, I am not Hispanic or Latino.</option>
					<option value="Hispanic or Latino">Yes, I am Hispanic or Latino</option>
				</select><br>
				A person of Cuban, Mexican, Puerto Rican, Central or South American, or other Spanish culture or origin, regardless of race.

			</div>
		</div>
		<div class="row">
			<div class="two columns">&nbsp;</div>
		</div>
			<div class="row">
				<div class="two columns bold">Race:</div>
				<div class="ten columns bold">
					IMPORTANT - Only complete this section if you selected<br>
					"No, I am not Hispanic or Latino" in the Ethnicity section above:<br><Br>
				</div>
			</div>
		<div id="race">
			<div class="row">
				<div class="two columns bold">&nbsp;</div>
				<div class="ten columns bold">
					<span style="font-weight:normal;">What is your race? Select ONE of the following categorie(s):</span>
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="American Indian or Alaskan Native"></div>
				<div class="ten columns">
					<u>American Indian or Alaskan Native</u><br>
					A person having origins in any of the original peoples of North America and South America (including Central America), and who maintains tribal affiliations or community attachment.
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="Asian"></div>
				<div class="ten columns">
					<u>Asian</u><br>
					A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian subcontinent including, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam.
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="Black or African American"></div>
				<div class="ten columns">
					<u>Black or African American</u><br>
					 A person having origins in any of the Black, racial groups of Africa.
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="Native Hawaiian or other Pacific Islander"></div>
				<div class="ten columns">
					<u>Native Hawaiian or other Pacific Islander</u><br>
					 A person having origins in any of the original peoples of Hawaii, Guam, Samoa, or other Pacific Islands.
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="White"></div>
				<div class="ten columns">
					<u>White</u><br>
					a person having origins in any of the original peoples of Europe, North Africa, or the Middle East.
				</div>
			</div>
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="one column"><input type="radio" name="ethnicity" value="Two or More Races"></div>
				<div class="ten columns">
					<u>Two or More Races</u><br>
					All persons who identify with more than one of the above five races
				</div>
			</div>
			<!--
			<div class="row">
				<div class="two columns">&nbsp;</div>
				<div class="eleven columns">
					Other (specify) <input type="text" name="ethnicityOther">
				</div>
			</div>
			-->
		</div>
		<Br><br>
	</div>
	<div class="row">
		<div class="two columns bold">
			Recruitment Source:
		</div>
		<div class="ten columns">
			<select name="source" id="source">
				<option value="">Where did you hear about us?</option>
				<option value="Career Builder">Career Builder</option>
				<option value="Career Center - Berkshires">Career Center - Berkshires</option>
				<option value="Career Center - Franklin/Hampshire">Career Center - Franklin/Hampshire</option>
				<option value="CET Webpage">CET Webpage</option>
				<option value="Craigslist">Craigslist</option>
				<option value="Facebook">Facebook</option>
				<option value="Idealist">Idealist</option>
				<option value="Indeed">Indeed</option>
				<option value="Job Quest">Job Quest</option>
				<option value="JobsInTheValley.com">JobsInTheValley.com</option>
				<option value="LinkedIn">LinkedIn</option>
				<option value="MassLive">MassLive</option>
				<option value="Monster">Monster</option>
				<option value="NetImpact">NetImpact</option>
				<option value="Orion">Orion</option>
				<option value="ZipRecruiter">ZipRecruiter</option>
				<option value="Other">Other</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="twelve columns">
			<input type="hidden" name="action" value="eeoform_submit">
			<a href="submit-form" id="submitbutton" class="button-link do-not-navigate" style="display:none;">Submit</a>
		</div>
	</div>
</form>
	<div class="row">
		<div class="eight columns" id="formResults">&nbsp;</div>
	</div>
<script>
	$(document).ready(function() {
		var ethnicitySelect = $("#ethnicity"),
			ethnicityElements = $("input[name=ethnicity]"),
			participation = $("#participation"),
			submitbutton = $("#submitbutton"),
			gender = $("#gender");
		
		participation.on('click',function(){
			var $this = $(this);
			if ($this.prop("checked")){
				$("#eeoSurvey").hide();
				$this.closest('form').find("input[type=text], select").val("");
				$this.closest('form').find("input[type=radio]").prop("checked", false);
				submitbutton.show();
			}else{
				$("#eeoSurvey").show();
				submitbutton.hide();
			}
				
		});
		gender.on("change",function(){
			submitbutton.show();
		});
		ethnicityElements.on("click",function(){
			submitbutton.show();
		});
		ethnicitySelect.on("change",function(){
			submitbutton.show();
			var $this = $(this);
			if ($this.val()){
				$.each(ethnicityElements,function(key,obj){
					var thisObj = $(obj);
					thisObj.prop( "checked", false );
				});
				$("#race").hide();
			}else{
				$("#race").show();
			}
		});
		
		
		
		var formElement = $("#eeoForm");
		submitbutton.on('click',function(){
            $('#formResults').html("Processing").removeClass("alert").removeClass("info").addClass("alert");
			var data = JSON.stringify(formElement.serializeObject());
			console.log(data);
			$.ajax({
                url: "<?php echo $CurrentServer;?>EeoFormSubmit",
                type: "PUT",
                data: data,
                success: function(data){
					console.log(data);
					if (data.success){
						$('#formResults').html("Survey Successfully Submitted").addClass("alert").addClass("info");
						formElement.slideToggle();
					}else{
						$('#formResults').html(data.error).addClass("alert");
						formElement.slideToggle();
					}
                },
                error: function(jqXHR, textStatus, errorThrown){
                    var message = $.parseJSON(jqXHR.responseText);
                    $('#formResults').html(message).addClass("alert");
                }
            });
		});
	});
</script>