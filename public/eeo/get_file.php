<?php
include_once('../_config.php');
include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$pageTitle = "CET - Public Access";
include("../_header.php");
echo '<div class="container">';

include_once($dbProviderFolder."PublicProvider.php");
$publicProvider = new PublicProvider($dataConn);

$token = $_GET['q'];
$ipAddress  = $_SERVER['REMOTE_ADDR'];
$valid = 0;
if( strlen($token)<32 )
{
        die("Invalid token!");
}
$criteria = new stdClass();
$criteria->token = $token;
$criteria->ipAddress = $ipAddress;

$titleTokenResults = $publicProvider->eeoTitleTokenSearch($criteria);
$titleTokens = $titleTokenResults->collection;
if (!count($titleTokens)){
        die("Invalid token!");
}


$searchResults = $publicProvider->eeoLinkSearch($criteria);
$results = $searchResults->collection;
if (count($results)){
	foreach ($results as $id=>$record){
		if (trim($record->title) == ""){
			$valid = 1;
		}
	}
}else{
	$insertResults = $publicProvider->eeoLink($criteria);
	if ($insertResults->success){$valid=true;}else{$valid=0;}
}

/* Define the secret file: */
$secretfile = "_eeoForm.php";

/* If there was a valid token in $token, output the secret file: */
if( $valid ){
     include_once($secretfile);
}else{
	include_once('_usedLink.php');
}
echo '
</div> <!-- end container -->';
include("../_footer.php");

?>