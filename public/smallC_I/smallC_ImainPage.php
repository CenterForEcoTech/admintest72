<?php
ini_set("display_errors", "on"); 
error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
// include_once($siteRoot."repository/mySqlProvider/PublicProvider.php");
?>
<?php
$setTab = "Customer_Info";
$TabCodes = array("Customer Info" =>"Customer_Info" ,"Building Info"=>"Building_Info","Mechanical"=> "Mechanical" );
foreach ($TabCodes as $TabName=>$TabCode){
	$TabHashes[] = $TabCode;
	$TabListItems[] .= "<li><a id='tab-".$TabCode."' href='#".$TabCode."'>".$TabName."</a></li>";
}
echo "<fieldset>";
echo "<ul id='admin-tabs' class='tabs'>";
		foreach ($TabListItems as $TabItem){
			echo $TabItem;
		}
echo "</ul>";		
echo "<div class='tabs-content'>";
			include_once("tabs/cusInfoPage.php");	
			include_once("tabs/buildInfoPage.php");
			include_once("tabs/MechanicalPage.php");
echo "</div>";
echo "<input type='button' id='submit' value = 'submit'>";
echo "<input type='button' id='TESTbttn' value = 'TEST'>";
echo "</fieldset>";
?>
<?php include_once('/../tabScript.php'); ?>
<script type="text/javascript">
var calculateDimensions = function(thisField){
		var totalArea = parseInt($("#building_FloorArea").val())* parseInt($("#building_Floors").val());
		var totalVolume = parseInt($("#building_CeilingHeight").val()) * totalArea;
		$("#building_TotalArea").val(totalArea);
		$("#building_TotalVolume").val(totalVolume);
		// console.log(thisField+" "+totalVolume+"_______"+totalArea);
	};
$(function(){
	var EzDocForm = [];
	$("#submit").on("click",function(){
		$(".formContainer").each(function(index,element){
			var formArray = [];
			$(element).find('form').each(function(index,element){
				var tab = $('#'+$(element).attr('id')).serializeObject();
				formArray.push(tab);
			});
			EzDocForm.push(formArray);
		});
		var data = JSON.stringify(EzDocForm);
		//TODO:Setup Ajax call to insert data into table
		// EzDocForm_Mech.serializeObject();
		// console.log(data);	
		$.ajax({
				url: "DocDataUpload.php",
				type: "POST",
				data: data,
				success: function(resultdata){
					// console.log(resultdata);

				}
				
		});
	});
	$("#TESTbttn").click(function(){

		$("input").each(function(index,element){
			if($(element).attr("type")!='button'){
				$(element).val($(element).attr("id")+"TEST");
			}
		});
		$("select").each(function(index,element){
			var id = "#"+ $(element).attr("id");
			// console.log($id);
			$(id).prepend('<option value='+id+'>'+id+'</option>');
			$(id + " option").eq(0).prop('selected',true);
		});
	});
	$("#building_CeilingHeight").on("change",function(){calculateDimensions("height");});
	$("#building_Floors").on("change",function(){calculateDimensions("floors");});
	$("#building_FloorArea").on("change",function(){calculateDimensions("area");});

});
</script>