<div class="tab-content formContainer" id="Building_Info">
<form id='EzDocForm_Build' class='basic-form sixteen columns override_skel'>
	<fieldset>
		<?php
			$fields = array(
				'Year Build' => 'YearBuild','Ceiling Height'=>'CeilingHeight','# Bedrooms'=>'Bedrooms','# Floors'=>'Floors',
				'# Occupants'=> 'Occupants','Floor Area'=>'FloorArea','Orientation'=>'Orientation',
				'Finished Basement Area'=>'FinishedBasementArea','Primary Heating Fuel'=>'PrimaryHeatingFuel',
				'Manufactured Home'=>'ManufacturedHome','Building Type'=>'BuildingType');
			$fieldsWritten = 0;
			echo "<div class = 'row'>";
			foreach ($fields as $shownName => $id) {
				# code...	
				$fieldsWritten++;
				if($fieldsWritten%2 == 1){
					echo "</div><div class = 'row'>";
				}
				echo '<div class="four columns">'.$shownName.':</div><div class="three columns"><input type="text" name="building_'.$id.'" id="building_'.$id.'"></div>';
				// echo "</div>";
			}
			echo "</div>";
		?>
	</fieldset>
	<div class="row">
		<div class="three columns build_Calculations">Total Floor Area:</div>
		<div class="build_Calculations two columns"><input type="text" name="building_TotalArea" id="building_TotalArea" ></div>
		<div class="two columns build_Calculations"  >Total Volume:</div>
		<div class="build_Calculations two columns"  ><input type="text" name="building_TotalVolume" id="building_TotalVolume" ></div>
	</div>	
</form>
</div>