<style type="text/css">
	.ui-widget{	font-size: 1em;}
	.ui-accordion .ui-accordion-content {padding: 1em;}
</style>
<div class="tab-content formContainer" id="Mechanical">
<!-- <fieldset>	 -->
<div id='EzDocForm_Mech' class='basic-form sixteen columns override_skel '>
<?php
	$sectionsAndNames=array(
		'Existing Heating Unit' => array(
			'Equipment Type'=>'EHU_EquipmentType','Distribution Type'=>'EHU_DistributionType','Location'=>'EHU_Location','Load'=>'EHU_Load',
			'Fuel Type'=>'EHU_FuelType','Year'=>'EHU_Year','Make'=>'EHU_Make','Input(kBtu/h)'=>'EHU_Input','Model'=>'EHU_Model',
			'Capacity(kBtu/h)'=>'EHU_Capacity','Vent Damper'=>'EHU_VentDamper','Serial Number'=>'EHU_SerialNumber',
			'Combustion Type'=>'EHU_CombustionType','Distribution Efficiency'=>'EHU_DistributionEfficiency',
			'Has Solar Collector'=>'EHU_SolarCollector','Pipe Insulation'=>'EHU_PipeInsulation','Hot Water Temp'=>'EHU_HotWaterTemp'),
		'Heating Unit Recommendations' => array(
			'Status Code' => 'HUR_StatusCode','Part'=>'HUR_Part','Location'=>'HUR_Location','Load'=>'HUR_Load','Fuel Type'=>'HUR_FuelType',
			'Input (kBtu/h)'=>'HUR_Input','Capacity (kBtu/h)'=>'HUR_Capacity','Combustion Type'=>'HUR_CombustionType',
			'Has Solar Collector'=>'HUR_SolarCollector','Pipe Insulation'=>'HUR_PipeInsulation','Hot Water Temp'=>'HUR_HotWaterTemp'),
		'Existing Heating Unit 2' => array(
			'Equipment Type'=>'EHU2_EquipmentType','Distribution Type'=>'EHU2_DistributionType','Location'=>'EHU2_Location',
			'Load'=>'EHU2_Load','Fuel Type'=>'EHU2_FuelType','Year'=>'EHU2_Year','Make'=>'EHU2_Make','Input(kBtu/h)'=>'EHU2_Input',
			'Model'=>'EHU2_Model','Capacity(kBtu/h)'=>'EHU2_Capacity','Vent Damper'=>'EHU2_VentDamper','Serial Number'=>'EHU2_SerialNumber',
			'Combustion Type'=>'EHU2_CombustionType','Distribution Efficiency'=>'EHU2_DistributionEfficiency',
			'Has Solar Collector'=>'EHU2_SolarCollector','Pipe Insulation'=>'EHU2_PipeInsulation','Hot Water Temp'=>'EHU2_HotWaterTemp'),
		'Heating Unit 2 Recommendations' => array(
			'Status Code' => 'HUR2_StatusCode','Part'=>'HUR2_Part','Location'=>'HUR2_Location','Load'=>'HUR2_Load',
			'Fuel Type'=>'HUR2_FuelType','Input (kBtu/h)'=>'HUR2_Input','Capacity (kBtu/h)'=>'HUR2_Capacity',
			'Combustion Type'=>'HUR2_CombustionType','Has Solar Collector'=>'HUR2_SolarCollector','Pipe Insulation'=>'HUR2_PipeInsulation',
			'Hot Water Temp'=>'HUR2_HotWaterTemp'),
		'Heating and DHW' => array(
			'Equipment Type'=>'H-DHW_EquipmentType','Distribution Type'=>'H-DHW_DistributionType','Location'=>'H-DHW_Location',
			'Load'=>'H-DHW_Load','Fuel Type'=>'H-DHW_FuelType','Year'=>'H-DHW_Year','Make'=>'H-DHW_Make','Input(kBtu/h)'=>'H-DHW_Input',
			'Model'=>'H-DHW_Model','Capacity(kBtu/h)'=>'H-DHW_Capacity','Vent Damper'=>'H-DHW_VentDamper',
			'Serial Number'=>'H-DHW_SerialNumber','Combustion Type'=>'H-DHW_CombustionType','Distribution Efficiency'=>
			'H-DHW_DistributionEfficiency','Has Solar Collector'=>'H-DHW_SolarCollector','Pipe Insulation'=>'H-DHW_PipeInsulation',
			'Hot Water Temp'=>'H-DHW_HotWaterTemp'),
		'Heating and DHW Recommendations'=>array(
			'Status Code'=>'H-DHWR_StatusCode','Part'=>'H-DHWR_Part','Location'=>'H-DHWR_Location','Load'=>'H-DHWR_Load',
			'Fuel Type'=>'H-DHWR_FuelType','Input(kBtu/h)'=>'H-DHWR_Input','Capacity(kBtu/h)'=>'H-DHWR_Capacity',
			'Combustion Type'=>'H-DHWR_CombustionType'),
		'DHW'=>array(
			'Equipment Type'=>'DHW_EquipmentType','Location'=>'DHW_Location','Load'=>'DHW_Load','Year'=>'DHW_Year',
			'Fuel Type'=>'DHW_FuelType','Make'=>'DHW_Make','Energy Factor'=>'DHW_EnergyFactor','Model'=>'DHW_Model',
			'Recovery Efficiency'=>'DHW_RecoveryEfficiency','Serial Number'=>'DHW_SerialNumber','Input(kBtu/h)'=>'DHW_Input',
			'Primary DHW'=>'DHW_Primary','DHW Vent Type'=>'DHW_VentType','Has Solar Collector'=>'DHW_SolarCollector',
			'Pipe Insulation'=>'DHW_PipeInsulation','Tank Gallon'=>'DHW_TankGallon','Hot Water Temp'=>'DHW_HotWaterTemp',
			'Tank Wrap'=>'DHW_TankWrap','Pilot Type'=>'DHW_PilotType'),
		'DHWR'=>array(
			'Status Code'=>'DHWR_StatusCode','Qty'=>'DHWR_Qty','Part'=>'DHWR_Part','Location'=>'DHWR_Location','Load'=>'DHWR_Load',
			'Fuel Type'=>'DHWR_FuelType','Energy Factor'=>'DHWR_EnergyFactor','Recovery Efficiency'=>'DHWR_RecoveryEfficiency',
			'Input(kBtu/h)'=>'DHWR_Input','Primary DHW'=>'DHWR_Primary','Pipe Insulation'=>'DHWR_PipeInsulation',
			'Has Solar Collector'=>'DHWR_SolarCollector','Tank Gallons'=>'DHWR_TankGallons','Hot Water Temp'=>'DHWR_HotWaterTemp',
			'Tank Wrap'=>'DHWR_TankWrap','Pilot Type'=>'DHWR_PilotType'),
		'Central A/C'=>array(
			'Location'=>'CenAC_Location','Load'=>'CenAC_Load','Usage'=>'CenAC_Usage','Net Efficiency'=>'CenAC_NetEfficiency',
			'Year'=>'CenAC_Year','Make'=>'CenAC_Make','Model'=>'CenAC_Model','Tons'=>'CenAC_Tons','Serial Number'=>'CenAC_SerialNumber',
			'Capacity(kBtu/h)'=>'CenAC_Capacity','Sensible Cap(kBtu/h)'=>'CenAC_SenibleCap','Airflow'=>'CenAC_Airflow',
			'Distribution Efficiency'=>'CenAC_DistribustionEfficiency','Variable Capacity'=>'CenAC_VariableCapacity',
			'Commissioned'=>'CenAC_Commissioned'),
		'Central A/C Recommendations'=>array(
			'Status Code'=>'CenACR_StatusCode','Part'=>'CenACR_Part','Location'=>'CenACR_Location','Load'=>'CenACR_Load',
			'Usage'=>'CenACR_Usage','Tons'=>'CenACR_Tons','Capacity(kBtu/h)'=>'CenACR_Capacity','Sensible Cap(kBtu/h)'=>'CenACR_SenibleCap',
			'Airflow'=>'CenACR_Airflow','Variable Capacity'=>'CenACR_VariableCapacity','Commissioned'=>'CenACR_Commissioned')
	);
	$typeList = array(
		'EHU_EquipmentType'=>array('',
			'Boiler','Steam Boiler','Furnace','Space Heater'),
		'EHU_DistributionType'=>array('',
			'Duct','High Velocity Duct','Ductless','Baseboard','Radiant','Radiator','Gravity Hot Water','Hydro Air'),
		'EHU_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'EHU_CombustionType'=>array('',
			'Atmospheric','Sealed','Direct Vent','Power Vent','Condensing'),
		'EHU_VentDamper'=>array('',
			'Yes','No'),
		'EHU_SolarCollector'=>array('',
			'Yes','No'),
		'HUR_StatusCode'=>array('',
			'On Hold','Recommended'),
		'HUR_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'HUR_FuelType'=>array('',
			'Natural Gas','Electricity','Oil Kerosene','LP Gas','Wood','Coal','Wood Pellets','Corn Pellets'),
		'HUR_CombustionType'=>array('',
			'Atmospheric','Sealed','Direct Vent','Power Vent','Condensing'),
		'HUR_SolarCollector'=>array('',
			'Yes','No'),
		'HUR_PipeInsulation'=>array('',
			'Yes','No'),
		'EHU2_EquipmentType'=>array('',
			'Boiler','Steam Boiler','Furnace','Space Heater'),
		'EHU2_DistributionType'=>array('',
			'Duct','High Velocity Duct','Ductless','Baseboard','Radiant','Radiator','Gravity Hot Water','Hydro Air'),
		'EHU2_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'EHU2_CombustionType'=>array('',
			'Atmospheric','Sealed','Direct Vent','Power Vent','Condensing'),
		'EHU2_VentDamper'=>array('',
			'Yes','No'),
		'EHU2_SolarCollector'=>array('',
			'Yes','No'),
		'HUR2_StatusCode'=>array('',
			'On Hold','Recommended'),
		'HUR2_Part'=>array(''),
		'HUR2_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'HUR2_FuelType'=>array('',
			'Natural Gas','Electricity','Oil Kerosene','LP Gas','Wood','Coal','Wood Pellets','Corn Pellets'),
		'HUR2_CombustionType'=>array('',
			'Atmospheric','Sealed','Direct Vent','Power Vent','Condensing'),
		'HUR2_SolarCollector'=>array('',
			'Yes','No'),
		'HUR2_PipeInsulation'=>array('',
			'Yes','No'),
		'H-DHW_EquipmentType'=>array('',
			'BoilerAndTanklessCoil','BoilerAndIndirectStorageTank','combo'),
		'H-DHW_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'H-DHW_FuelType'=>array('',
			'Natural Gas','Electricity','Oil Kerosene','LP Gas','Wood','Coal','Wood Pellets','Corn Pellets'),
		'H-DHW_CombustionType'=>array('',
			'Atmospheric','Sealed','Direct Vent','Power Vent','Condensing'),
		'H-DHW_VentDamper'=>array('',
			'Yes','No'),
		'H-DHW_SolarCollector'=>array('',
			'Yes','No'),
		'H-DHWR_StatusCode'=>array('',
			'On Hold','Recommended'),
		'H-DHWR_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'H-DHWR_FuelType'=>array('',
			'Natural Gas','Electricity','Oil Kerosene','LP Gas','Wood','Coal','Wood Pellets','Corn Pellets'),
		'DHW_EquipmentType'=>array('',
			'Boiler','Steam Boiler','Furnace','Space Heater'),
		'DHW_Location'=>array('',
			'Attic','Basement','Crawlspace','Exterior','Exterior Wall','Garage','Living Space','Mechanical Closet','Roof Deck'),
		'DHW_FuelType'=>array(''),
		'DHW_Primary'=>array(''),
		'DHW_VentType'=>array(''),
		'DHW_SolarCollector'=>array(''),
		'DHW_TankWrap'=>array(''),
		'DHW_PilotType'=>array('')

	);

	echo "<div id='accordianTabs' class='fifteen columns'>";
	$tabNumb = 0;
	foreach ($sectionsAndNames as $header => $fields) {
		$tabNumb++;
		$fieldsWritten = 0;	
		echo "<h3>".$header."</h3>";
		echo "<form class='mechTab' id='tab".$tabNumb."'>";
		echo "<div>";
		echo "<div class = 'row'>";
		foreach ($fields as $visibleName => $id) {
			$fieldsWritten++;
			if($fieldsWritten%2 == 1){
				echo "</div><div class = 'row'>";
			}
			if(array_key_exists($id, $typeList)){
				$options = $typeList[$id];
				echo '<div class="four columns">'.$visibleName.'</div>';
				echo '<select class ="three columns" name="mechanical_'.$id.'" id="mechanical_'.$id.'">';
					foreach ($options as $key) {
						echo '<option value="'.$key.'">'.$key.'</option>';
					}	
				echo "</select>";
			}else{
				echo '<div class="four columns">'.$visibleName.':</div><input class ="three columns" type="text" name="mechanical_'.$id.'" id="mechanical_'.$id.'">';
			}
		}
		echo "</div>";
		echo "</div>";
		echo "</form>";
	}
	echo "</div>";
?>
<!-- Mechanical -->

<!-- </fieldset> -->
</div>
</div>
<script type="text/javascript">
	$( "#accordianTabs" ).accordion({
	  collapsible: true,
	  icons: false
	});
</script>