<div class="tab-content formContainer" id = "Customer_Info">
<form id='EzDocForm_Cus' class='basic-form sixteen columns override_skel'>
	<fieldset >
		<?php
			$fields = array(
				'First Name' =>'FirstName' ,'Last Name'=>'LastName','Address'=>'Address','City'=>'City','State'=>'State',
				'Zip'=>'Zip','Phone'=>'Phone','Email'=>'Email','Appointment Type'=>'AppointmentType','Appointment Date'=>'AppointmentDate',
				'Appointment Start'=>'AppointmentStart','PreWeatherization'=>'PreWeatherization','SiteId'=>'SiteId',
				'ProjectID'=>'ProjectID','Whole Building Incentive'=>'WholeBuildingIncentive', 'Moderate Income'=> 'ModerateIncome',
				'Electric Provider'=>'ElectricProvider','Electric Customer Account'=>'ElectricCustomerAccount','Gas Provider'=>'GasProvider',
				'Gas Customer Account'=>'GasCustomerAccount','Customer Relation to Site'=>'SiteRelation',
				'Gas Customer LocationID'=>'GasCustomerLocationID','Auditor'=>'Auditor');
			$fieldsWritten = 0;
			echo "<div class = 'row'>";
			foreach ($fields as $shownName => $id) {
				# code...
				// echo "<div class='row'>";
				$fieldsWritten++;
				if($fieldsWritten%2 == 1){
					echo "</div><div class = 'row'>";
				}
				echo '<div class="four columns">'.$shownName.':</div><div class="three columns"><input type="text" name="cust_'.$id.'" id="cust_'.$id.'"></div>';
				// echo "</div>";
			}
			echo "</div>";
		?>
	</fieldset>
</form>

</div>