<?php
ini_set("display_errors", "on"); 
error_reporting(1);
include_once($siteRoot."_setupDataConnection.php");
?>

<?php ?>
<link rel="stylesheet" href="../css/jquery.fileupload-ui.css"><html>
	<div class="three columns" id="passwordEntryForm">
		Please enter password
		<input type="text" name="password" id="password">
		<input type="button" name="enterPassword" id="enterPassword" value="submit">
		<div class="error" style="display: none;" id="passwordError">Please enter a valid password</div>
	</div>
	<form id="changeRequestForm" class="basic-form sixteen columns override_skel" style="display: none;" >
		
		<h4>
			<button data-value="furnace" class="formType btn-success fileinput-button">Early Furnace Replacement</button>
			<button data-value="boiler" class="formType btn-success fileinput-button" >Early Boiler Replacement</button>
		</h4>
		<fieldset class="mainForm" style="display: none;">			
			<div>
				<div class="sixteen columns">
					<fieldset>
						<legend>Customer Information</legend>
						<div class="ten columns">
							<div class="row">
								<div class="three columns">Site Id: </div><div class="four columns" ><input type="text" id="siteId" name="Site_ID" value="Multifamily"></div>	
							</div>
							<div class="row">
							<div class="three columns">Site Visit Date:</div><div class="four columns"><input type="text" id="siteVisitDate" name="siteVisitDate" class="date">	</div>
							</div>						
							<div class="row">
								<div class="three columns">Energy Specialist:</div>
								<div class="four columns">
									<select name="energySpecialist" id="energySpecialist">
										<option value="Jamie Overby">Jamie Overby</option>
										<option value="Bill Laughly">Bill Laughly</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="three columns">Account Holder Name:</div><div class="four columns" "><input type="text" id="accHolderName" name="accHolderName"></div>
							</div>
							<div class="row">
								<div class="three columns">Customer Address:</div><div class="four columns" ><input type="text" id="customerAddress" name="customerAddress"></div>	
							</div>
							<div class="row">
								<div class="three columns">City:</div><div class="four columns" ><input type="text" id="city" name="city"></div>	
							</div>
							<div class="row">
								<div class="three columns">Zip:</div><div class="four columns" ><input type="text" id="zip" name="zip"></div>	
							</div>
							<div class="row">
								<div class="three columns">Phone Number:</div><div class="four columns" ><input type="text" id="phoneNumber" name="phoneNumber"></div>	
							</div>
							<div class="row">
								<div class="three columns">Email:</div><div class="four columns" ><input type="email" name="email" id="email"></div>
							</div>
							<div class="row">
								<div class="four columns">Check if Mailing Address for Rebate is Different from Above</div><div class="one columns"><input type="checkbox" id="rebateAddressFormNeeded"></div>
							</div>
						</div>
						<span>
							Pick Photos to Upload
							<input type="file" name="uploadFile" id="uploadFile" multiple>
						</span>
					</fieldset>
					<fieldset id="rebateAddressForm" style="display: none;">
						<legend>Mailing Address for Rebate</legend>
						<div class="ten columns">
							<div class="row">
								<div class="three columns">Person/Company:</div><div class="four columns" "><input type="text" id="rebateName" name="rebateName"></div>
							</div>
							<div class="row">
								<div class="three columns">Mailing Address:</div><div class="four columns" ><input type="text" id="rebateAddress" name="rebateAddress"></div>	
							</div>
							<div class="row">
								<div class="three columns">City:</div><div class="four columns" ><input type="text" id="rebateCity" name="rebateCity"></div>	
							</div>
							<div class="row">
								<div class="three columns">Zip:</div><div class="four columns" ><input type="text" id="rebateZip" name="rebateZip"></div>	
							</div>
							<div class="row">
								<div class="three columns">Phone Number:</div><div class="four columns" ><input type="text" id="rebatePhoneNumber" name="rebatePhoneNumber"></div>	
							</div>
							<div class="row">
								<div class="three columns">Email:</div><div class="four columns" ><input type="email" name="rebateEmail" id="rebateEmail"></div>	
							</div>					
						</div>

					</fieldset>

					<fieldset>
						<legend> Exising Equipment</legend>
							<div class="row">
								<div class="three columns">Heating Equipment</div>
								<div class="three columns">Year</div>
								<div class="three columns">Manufacturer</div>
								<div class="three columns">Model Number</div>
								<div class="three columns">Serial Number</div>
							</div>
							<div class="row">
								<div class="three columns"><select class="equipField" id = boilerType1 name="boilerType1">
									<option value= "" ></option>
									<option value="Steam" class="equipmentOption">Steam</option>
									<option value="Forced Hot Water" class="equipmentOption"> Forced Hot Water</option>	
									<option value="Furnace" class="equipmentOptionF" style="display: none;">Furnace</option>								
								</select></div>
								<div class = "three columns "><input class="equipField" type="text" name="boilerYear1" id="boilerYear1"></div>
								<div class="three columns "><input class="equipField" type="text" name="boilerManufacturer1" id="boilerManufacturer1"></div>
								<div class="three columns "><input class="equipField" type="text" name="boilerModelNumber1" id="boilerModelNumber1"></div>
								<div class="three columns "><input class="equipField" type="text" name="boilerSerialNumber1" id="boilerSerialNumber1"></div>
							</div>
							<div class="row">
								<div class="three columns "><select class="equipField" id = "boilerType2" name = "boilerType2">
									<option value= "" ></option>
									<option value="Steam" class="equipmentOption">Steam</option>
									<option value="Forced Hot Water" class="equipmentOption"> Forced Hot Water</option>
									<option value="Furnace" class="equipmentOptionF" style="display: none;">Furnace</option>								
								</select></div>
								<div class = "three columns "><input class="equipField" type="text" name="boilerYear2" id="boilerYear2"></div>
								<div class="three columns "><input type="text" class="equipField" name="boilerManufacturer2" id="boilerManufacturer2"></div>
								<div class="three columns "><input type="text" class="equipField" name="boilerModelNumber2" id="boilerModelNumber2"></div>
								<div class="three columns "><input type="text" class="equipField" name="boilerSerialNumber2" id="boilerSerialNumber2"></div>
							</div>
					</fieldset>
					
						
				</div>
			</div>
		
			<div class="row"> 
				<div style="position: relative;float: left;" >
					<span  class="two columns"><a href="" id="print-form" class="button-link do-not-navigate submit">Process</a></span>
					<span class="info five columns" id="processedText" style="display: none;"> Form was processed and emailed</span>
					<img style= "width: 50px;height: 50px;display: none" src = "<?php echo $CurrentServer.$adminFolder;?>images/loading.gif" class = "submit" >
					<input type="hidden" name="action" id = "action" value="boiler">
					<input type="hidden" name="fileLocations" id="fileLocations" value="">
				</div>
				<div  style="position:relative; float: right;">
					<button class="btn-danger do-not-navigate" id="resetForm"><span>Reset Form</span></button>
				</div>
			</div>
			<div class="row">
				<span class="three columns"><a download  href="" id="download-form" class="button-link" style="display: none;"> Download </a>	</span>		
			</div>
		</fieldset>
	</form>
	
<script>
$(function(){
	$("#login-form").hide();
	$('select').css('height','22px');
	
	var dateClass = $(".date");
	dateClass.width(80);
	dateClass.datepicker({

	});
	$("#resetForm").on("click",function(e){
		e.preventDefault();
		$("#changeRequestForm")[0].reset();
	});
	var dropdownlist = $('select');
	dropdownlist.css('width','auto');
	$('input[type="text"]').css('height','22px');
	$('input[type="checkbox"]').css('width','13px');
	$(".input_text_tiny").css({'width':'25px','height':'20px'});
	$(".input_text_small").css({'width':'45px','height':'20px'});
	$("#rebateAddressFormNeeded").on("click",function(){		
		$("#rebateAddressForm").toggle();
	});
	$("#enterPassword").on("click",function(e){
		e.preventDefault();
		var password = $("#password").val();
		$.ajax({
			url:"checkPassword.php",
			type:"POST",
			data:password,
			success:function(resultdata){
					// console.log(resultdata);
				if(resultdata == "true"){
					$("#passwordEntryForm").hide();
					$("#changeRequestForm").show();
				}else{
					$("#passwordError").show();
				}
			}
		});
	});

	$(".formType").on("click",function(e){
		$(".mainForm").slideUp(300);
		var $this = $(this);
		e.preventDefault();

		$("#action").val($this.attr("data-value"));
		var fields =$(".equipField");
		if($("#action").val()=="boiler"){
			$(".equipmentOption").show();
			$(".equipmentOptionF").hide();
		}else{
			$(".equipmentOption").hide();
			$(".equipmentOptionF").show();
		}
				
		$.each(fields,function(key,val){
			var thisItem = $(val);
			thisItem.val('');			
		});
		$(".mainForm").slideDown(500);
	});

	$("#changeRequestForm").validate({
		rules:{
			siteId : "required",
			siteVisitDate: "required",
			energySpecialist: "required",
			accHolderName: "required",
			customerAddress: "required",
			accHolderName: "required",
			customerAddress: "required",
			city: "required",
			zip: "required",
			email: {email:true},
			rebateName:{required: {depends: function(e){return $("#rebateAddressFormNeeded").is(":checked");}}},
			rebateAddress:{required: {depends: function(e){return $("#rebateAddressFormNeeded").is(":checked");}}},
			rebateCity:{required: {depends: function(e){return $("#rebateAddressFormNeeded").is(":checked");}}},
			rebateZip:{required: {depends: function(e){return $("#rebateAddressFormNeeded").is(":checked");}}},
			boilerType1:"required",
			boilerYear1:"required",
			boilerManufacturer1: "required",
			boilerModelNumber1: "required",
			boilerSerialNumber1: "required"
		}
	});
	
	$("#uploadFile").on("change",function(e){
		e.preventDefault();
		var data = new FormData();
		var files = e.target.files;
		
		$.each(files, function(key, value){
			data.append(key, value);
		});
		$.ajax({
			url: "ReplacementRequestFormFill.php?action=uploadPhotoAttachment",
			type: "POST",
			data: data,
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			success: function(resultdata){
				$("#fileLocations").val(resultdata);
				// console.log($("#fileLocations").val());
			}
		});
		// console.log("TEST");
	});
	var changeRequestForm = $("#changeRequestForm");
	$("#print-form").on("click",function(e){
		e.preventDefault();
		if(changeRequestForm.valid()){
			var data = JSON.stringify(changeRequestForm.serializeObject());
			$(".submit").toggle();
			$("#download-form").hide();
			
			// console.log(data);
			$.ajax({
				url: "ReplacementRequestFormFill.php?action="+$("#action").val(),
				type: "POST",
				data: data,
				success: function(resultdata){
					//$("#fileLocation").val(resultdata);
					$(".submit").toggle();
					$("#download-form").show();
					$("#processedText").show().fadeOut(5000);
					$("#download-form").attr('href',resultdata);
					// console.log(resultdata);

				},
				error: function(jqXHR, textStatus, errorThrown){
					var message = $.parseJSON(jqXHR.responseText);
					// console.log(message);	
					//$(thisAlert).show().html(message).addClass("alert");
				}
			});
		}else{
			window.alert("There was informaion missing so your form could not be processed");
		}
	});

});
	 
	
	
	
   
  
</script>
	<script src="../js/highstock/highstock.js"></script>
	<script src="../js/highstock/modules/exporting.js"></script>
	<br clear="all">
	<div id="containerChart"></div>
	
	<div id="containerChartCount"></div>
