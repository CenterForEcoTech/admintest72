<?php
$criteria = new stdClass();
$criteria->token = $token;
$criteria->ipAddress = $ipAddress;
$productReceiptDetails = $productProvider->getOneMovementRecordEmail($token);
$acknowledgeDate = MySQLDate($productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_AcknowledgedOnDate"]);
if (!$acknowledgeDate){
	$productReceipt = $productProvider->movementRecordReceived($criteria);
}
$Text = nl2br($productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_Text"]);
$TextParts = explode(":",$Text);
$ES = $TextParts[0];
$pageTitle = "CET - Product Receipt";
include("../_header.php");
echo '<div class="container">';
echo '
<div class="row">
	<div class="twelve columns">
		<img src="'.$CurrentServer.'images/CET_logo.png">
	</div>
</div>
<div class="row">
	<div class="twelve columns">'. $Text.'</div>
</div>
<div class="row">
	<div class="twelve columns">Acknowledgment of receipt of these items was indicated on '.($acknowledgeDate ? $acknowledgeDate : date("m/d/Y")).'</div>
</div>';

if ($type == "discrepancy"){
	echo '<div class="row">
			<div class="twelve columns">
				Include a brief note as to the discrepancy you found:<br>
				<textarea id="discrepancy">'.$productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_DiscrepancyNote"].'</textarea><br>
				<input type="hidden" id="from" value="'.$productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_To"].'"><br>
				<button id="sendNote">Send Note</button>
			</div>
		</div>';
}else{
	echo "<div class='row'><div class='twelve columns'>";
	if (trim($productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_DiscrepancyNote"])){
		echo "The following discrepancy was noted:<br>".$productReceiptDetails["GHSInventoryProductsMovementHistoryRecord_DiscrepancyNote"];
	}else{
		echo "You can close this browser window now";
	}
	echo "</div></div>";
	
}
echo "<div id='messageresult' class='alert info'></div>";
	$criteria = new stdClass();
	$criteria->warehouseName = $ES;
	echo "<hr style='border:dotted;'>";
 include_once('_productReceipt_StaffForm.php');		
echo '</div> <!-- end container -->'; 


include("../_footer.php");
?>
<script>
    $(function(){
		$("#sendNote").on('click',function(){
			var $this = $(this);
			$this.html('processing...');
			$this.attr('disabled',true);
			data = {};
			data["id"] = <?php echo $token;?>;
			data["action"] = "productmovementdiscrepancy";
			data["discrepancy"] = $("#discrepancy").val();
			data["from"] = $("#from").val();
			datastring = JSON.parse(JSON.stringify(data));
				$.ajax({
					type: "POST",
					url: "<?php echo $CurrentServer;?>productDiscrepancySubmit",
					data: datastring,
					dataType: "json",
					success: function(data) {			
						$this.html('message sent');
						$("#messageresult").html('Your note has been sent.  You can close this window now.');
					}
				});
		});
	});
</script>