<?php
include_once('../_config.php');
include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");

include_once($dbProviderFolder."InventoryProvider.php");
$productProvider = new ProductProvider($dataConn);
$type = $_GET['type'];
$token = $_GET['q'];
$ipAddress  = $_SERVER['REMOTE_ADDR'];

if ($type == "staff"){
	$warehouseProvider = new WarehouseProvider($dataConn);
	$criteria = new stdClass();
	$criteria->id = $token;
	$paginationResult = $warehouseProvider->get($criteria);
	$ES = $paginationResult->collection[0]->description;
	$criteria = new stdClass();
	$criteria->warehouseName = $ES;

 include_once('_productReceipt_StaffForm.php');		
}else{
 include_once('_productReceipt_SingleForm.php');	
}
?>