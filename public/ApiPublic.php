<?php
include_once("_config.php");
include_once($siteRoot."assert_is_ajax.php");

include_once($siteRoot."_setupDataConnection.php");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
include_once($dbProviderFolder."PublicProvider.php");

$results = new stdClass();

switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        $results = "this is a delete";
        break;
    case "GET":
		$action = $_GET['action'];
		if ($action = "checkToken"){
		
		
		$results = "this is get ".$action;
		}
        break;
    case "POST":
        if ($_POST['action'] === 'productmovementdiscrepancy'){
			include_once($dbProviderFolder."InventoryProvider.php");
			$productProvider = new ProductProvider($dataConn);
			$criteria = new stdClass();
			$criteria->token = $_POST['id'];
			$criteria->discrepancy = $_POST['discrepancy'];
			$criteria->ipAddress = $_SERVER['REMOTE_ADDR'];
			$productReceipt = $productProvider->movementRecordReceived($criteria);
			if ($productReceipt->success){
				require_once "Mail.php";  
				$from = "noreply@cetdashboard.info"; 
				$to = "Inventory Discrepancy <InventoryDiscrepancy@cetonline.org>"; 
				$recipients = $to;
				$subject = "Inventory Discrepancy Note"; 
				$body = "A discrepancy with an inventory receipt has been noted.  Please use this link for more details: ".$CurrentServer."productDiscrepancyReport/".$_POST['id'];  
				$host = "mail.cetdashboard.info"; 
				$port = "587"; 
				$username = "noreply@cetdashboard.info"; 
				$password = '9vQ?PY^q';  
				$headers = array (
					'From' => $from,   
					'To' => $to,
					'Subject' => $subject
				); 
				$smtp = Mail::factory(
					'smtp',   
					array (
						'host' => $host,     
						'port' => $port,     
						'auth' => true,     
						'username' => $username,     
						'password' => $password
					)
				);  
				$mail = $smtp->send($recipients, $headers, $body); 
			}
			$results = $productReceipt;
        }elseif ($_POST['action'] === 'productmovementacknowledged'){
			include_once($dbProviderFolder."InventoryProvider.php");
			$productProvider = new ProductProvider($dataConn);
			$criteria = new stdClass();
			$criteria->token = $_POST['id'];
			$criteria->ipAddress = $_SERVER['REMOTE_ADDR'];
			$productReceipt = $productProvider->movementRecordReceived($criteria);
			$results = $productReceipt;
        }elseif (empty($_POST)){
			$results = "empty post";
        } else {
			$results = $_POST;
        }
        break;
    case "PUT":
		$newRecord = json_decode($currentRequest->rawData);
		if (trim($newRecord->action) == 'eeoform_submit'){
			$publicProvider = new PublicProvider($dataConn);
			$token = $newRecord->token;
			$ipAddress  = $_SERVER['REMOTE_ADDR'];


			//getTitleInfo
			$criteria = new stdClass();
			$criteria->token = $token;
			$titleTokenResults = $publicProvider->eeoTitleTokenSearch($criteria);
			$titleTokens = $titleTokenResults->collection;
			foreach ($titleTokens as $id=>$record){
				$title = $record->title;
				$valid=true;
			}

			
			
			/* If there was a valid token in $token, output the secret file: */
			if( $valid){
				$response = $publicProvider->eeoSubmit($newRecord,$title,$ipAddress);
				$results = $newRecord;
				if ($response->success){
					//update the eeoLinks with the title to prevent from using link again
					$criteria->title = $title;
					$criteria->ipAddress = $ipAddress;
					$insertResults = $publicProvider->eeoLinkUpdate($criteria);
					
					$results = $response;
					header("HTTP/1.0 201 Created");
				} else {
					$results = "There was a problem submitting this form";
					header("HTTP/1.0 409 Conflict");
				}
			}else{
				$results->error = $insertResults;
				header("HTTP/1.0 201 Created");
			}
			
		}
        break;
}
output_json($results);
die();
?>