<div style="display:none;">
    <div id="define-customer-choice" class="help-modal">
        <?php if (function_exists("isMerchant") && isMerchant() || "Event_Template_V2.php" == $displayTemplateFile){?>
        <h3>What is a Customer Choice Event?</h3>
        <p>
            <?php echo $SiteName;?> makes it easy for a business to offer an event where many nonprofits can rally supporters to raise funds. This is Customer Choice.
        </p>
        <p>
            Your business might be approached by many nonprofits. Instead of having to turn them down because of a scheduling conflict, offer a Customer Choice event that is
            set up on your terms at a timing that works for your business, and tell nonprofits to rally their supporters to attend at that time!
        </p>
        <p>
            Customer Choice events leverage leading research suggesting that 90% of customers want purchases to benefit causes they care about.
        </p>
        <p>
            <strong>TIP:</strong> Offering 20% or more will get more nonprofits excited and interested in sending their followers to your business!
        </p>
        <?php } // end if merchant?>
        <?php if (function_exists("isOrganization") && isOrganization()){?>
        <h3>Why rally followers for Customer Choice?</h3>
        <p>
            By asking your followers to support you by attending Customer Choice Events, you offer them the flexibility to choose events
            that fit their lifestyle, interests, location, and convenience while still coming out to support your cause.
        </p>
        <p>
            Use the social media sharing tools on the events you want to drive your supporters to. With Customer Choice, you can line up
            several events, in several locations, across a flexible timespan, greatly increasing the likelihood of attendance.
        </p>
        <?php } // end if organization?>
        <?php if ("Event_Template_V2.php" != $displayTemplateFile && (function_exists("isIndividual") && isIndividual() || function_exists("isLoggedIn") && !isLoggedIn())){?>
        <h3>Why Attend a Customer Choice Event?</h3>
        <p>
            You can support your favorite charity when you attend a Customer Choice Event and submit your email or phone number when you mention <?php echo $SiteName;?>.
            Some businesses will already have several of their own favorite charities lined up for selection if you prefer to remain private.
        </p>
        <?php } // end if individual?>
        <h3>What charities are eligible?</h3>
        <p>
            Our patent-pending platform automatically includes more than one million public charities registered with the IRS.
        </p>
        <p>
            Nonprofits with a fiscal sponsor can also register with <?php echo $SiteName;?> to become eligible to receive a donation from a business.
        </p>
    </div>
</div>
<?php
if (!$loginIsRequiredForThisPage){
    include_once("views/_embedThis.php");
}

if (!$isDevMode && $Config_GoogleAnalyticsAccount){ ?>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<?php echo $Config_GoogleAnalyticsAccount;?>']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <?php } // google analytics only when not in dev mode and there's an account number ?>
    <?php if ($SiteTesting){?>
    <div style="color:white;background-color:white;text-align:center;"><?php echo str_replace(".php","",$PageName);?></div>
    <?php }?>

<!--Support-->
<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.4/zenbox.js"></script>
<style type="text/css" media="screen, projection">
    @import url(//assets.zendesk.com/external/zenbox/v2.4/zenbox.css);
</style>
<script type="text/javascript">
    if (typeof(Zenbox) !== "undefined") {
        Zenbox.init({
            dropboxID:   "20049508",
            url:         "https://cetonline.zendesk.com",
            tabID:       "ask_us",
            tabImageURL: "<?=$CurrentServer?>images/SupportTab.png",
            tabColor:    "white",
            tabPosition: "Right",
            hide_tab: "true"
        });
    }
</script>
<!-- JS
	================================================== -->
<?php if (!isset($Footer_excludeJqueryUi) || $Footer_excludeJqueryUi == false) { ?>
    <script src="<?php echo $CurrentServer;?>js/jquery-ui-1.8.20.custom.min.js"></script>
<?php } ?>
<?php if (isset($Footer_includeFlexslider)) { ?>
    <script src="<?php echo $CurrentServer;?>js/jquery.flexslider-min.js"></script>
<?php } ?>
<?php if (isset($Footer_includeInfiniteScroll)) { ?>
    <script src="<?php echo $CurrentServer;?>js/jquery.infinitescroll.min.js"></script>
<?php } ?>
<?php if (isset($Footer_includeMasonry)) { ?>
    <script src="<?php echo $CurrentServer;?>js/masonry.pkgd.min.js"></script>
    <script src="<?php echo $CurrentServer;?>js/imagesloaded.pkgd.min.js"></script>
<?php } ?>
<script src="<?php echo $CurrentServer;?>js/jquery.ui.timepicker.js"></script>
<script src="<?php echo $CurrentServer;?>js/tabs.js"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.watermark.min.js?x=1"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.colorbox-min.js"></script>
<script src="<?php echo $CurrentServer;?>js/jquery.validate.js" type="text/javascript" language="javascript"></script>

<script src="<?php echo $CurrentServer;?>js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<?php echo $CurrentServer;?>js/causetown.site.js?y=8"></script>

<!-- End Document
================================================== -->
</body>
</html>