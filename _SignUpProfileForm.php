<?php
/**
 * This template is only for MERCHANT SIGN UP. _SignUpProfileForm_Organization.php is for organizations, and was split off a long time ago.
 */
//check to see if this was a redirect from an alert failure
if (peekValidationMessage()!=""){
	echo "<span class=\"alert\">".popValidationMessage()."</span>";
}
if (!isset($BizNameFormAction)){
	$BizNameFormAction = "MerchantSignUp";
}
$allowCitygridLookup = (isset($allowCitygridLookup)) ? $allowCitygridLookup : ($BizNameFormAction == "MerchantSignUp");
if ($_GET['confirm-pid'] && isset($merchantRecord) && $merchantRecord->id > 0){
    $allowCitygridLookup = false;
    $SubmitValue = "Submit";
} else {
    $merchantRecord = new MerchantRecord();
    $contactRecord = new ContactRecord();
    $merchantRecord->primaryContact = $contactRecord;
}
if (!isset($SubmitValue)){
	$StepText = "Step 2";
	$SubmitValue = "Submit";
}
?>
<form id="sign-up-form" class="override_skel" action="<?php echo $CurrentServer.$BizNameFormAction;?>" method="post">
	<div id="profile-fields">
		<fieldset class="seven columns">
			<div class="legend">Name</div>
			<label id="name-autocomplete">
				<span class="highlight">
					<input type="text" id="company_name" name="company_name" value="<?php echo $merchantRecord->name;?>" class="auto-watermarked" title="search for your company" />
				</span>
				<div class="alert" id="already-registered-message"></div>
			</label>
		</fieldset>
		<fieldset class="seven columns">
			<div class="legend">Business Address Information</div>
			<label>
				<div>Address:</div>
				<span class="highlight">
					<input type="text" id="company_address" name="company_address" value="<?php echo $merchantRecord->address;?>" class="affects-geo" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>City:</div>
				<span class="highlight">
					<input type="text" id="company_city" name="company_city" value="<?php echo $merchantRecord->city;?>" class="affects-geo" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>State:</div>
				<span class="highlight">
					<input type="text" id="company_state" name="company_state" value="<?php echo $merchantRecord->state;?>" class="affects-geo" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Zip:</div>
				<span class="highlight">
					<input type="text" id="company_zip" name="company_zip" value="<?php echo $merchantRecord->postalCode;?>" class="affects-geo" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Phone:</div>
				<span class="highlight">
					<input type="text" id="company_phone" name="company_phone" value="<?php echo $merchantRecord->phone;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Website:</div>
				<span class="highlight">
					<input type="text" id="company_website" name="company_website" value="<?php echo $merchantRecord->website;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<label style="display:none;">
				<div>Business Description:</div>
				<span class="highlight">
					<textarea id="company_description" name="company_description"><?php echo $merchantRecord->rawDescriptionText;?></textarea>
				</span>
				<div class="alert"></div>
			</label>
		</fieldset>

		<fieldset class="seven columns">
			<div class="legend">Primary Contact for <?php echo $SiteName;?> Events</div>
			<label>
				<div>First Name:</div>
				<span class="highlight">
					<input type="text" id="company_contactfirstname" name="company_contactfirstname" value="<?php echo $merchantRecord->primaryContact->firstname;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Last Name:</div>
				<span class="highlight">
					<input type="text" id="company_contactlasstname" name="company_contactlasstname" value="<?php echo $merchantRecord->primaryContact->lastname;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Title:</div>
				<span class="highlight">
					<input type="text" id="company_contacttitle" name="company_contacttitle" value="<?php echo $merchantRecord->primaryContact->title;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Phone:</div>
				<span class="highlight">
					<input type="text" id="company_contactphone" name="company_contactphone" value="<?php echo $merchantRecord->primaryContact->phone;?>"  />
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Email:</div>
				<span class="highlight">
					<input type="text" id="company_contactemail" name="company_contactemail" value="<?php echo $merchantRecord->primaryContact->email;?>"/>
				</span>
				<div class="alert"></div>
			</label>
			<label>
				<div>Email Confirm:</div>
				<span class="highlight">
					<input type="text" id="company_contactemailconfirm" name="company_contactemailconfirm" value="<?php echo $merchantRecord->primaryContact->email;?>" />
				</span>
				<div class="alert"></div>
			</label>
			<?php
			//only show option to create referral code at signup, not something they can alter directly in their profile contact information.  Eventually they will be able to alter the CurrentReferralID if changing payment profiles but not RegisteredReferralID
			if (!$OrganizationInfo_ID && !$MerchantInfo_ID){
                $referralCookieValue = $_COOKIE['CETDashPID'] ? $_COOKIE['CETDashPID'] : $_COOKIE['CETDashCampaignCode'];
			?>
			<label>
				<div>
					Referral Code:
	                <a href="#referral-code-help" data-target="referral-code" class="colorbox-inline help-modal-icon">Help on Referral Code</a>
				</div>
				<span class="highlight">
					<input type="text" id="company_referralcode" name="company_referralcode" value="<?php echo $referralCookieValue;?>" />

				</span>
				<div class="alert"></div>
			</label>
			<?php }//end if already registered ?>
		</fieldset>
        <div id="submit-area" class="seven columns">
		<?php if ($StepText){?>
		<div id="submit-button" class="step_btn btn right">
			<span><?php echo $StepText;?></span>
			<a id="submit-button-trigger" href="#" title="next"><?php echo $SubmitValue;?></a>
		</div>
		<?} else {?>
		<div id="submit-button" class="btn right">
			<input id="submit-button-trigger" type="submit" value="<?php echo $SubmitValue;?>">
		</div>
		<?php } ?>

        <div class="six columns alpha omega" style="float:right;text-align: right;">
            <?php echo render_template("TermsAgreement.snippet", array( "siteName" => $SiteName, "serverUrl" => $CurrentServer )); ?>
        </div>
        </div>
	</div>
</form>
<span id="js-warning" class="alert">Please enable javascript to continue.</span>
<div style="display:none;">
    <div id="referral-code-help" class="help-modal">

        <h4>Referral Code</h4>
        <p>
            This field is optional.
		</p>
		<p>
			Referral codes help us understand how you heard about <?php echo $SiteName;?> and to customize your experience according to the campaign
			or nonprofit partner that invited you to <?php echo $SiteName;?>. If this field is automatically pre-populated when you register, 
			please leave the code that appears. If you were referred to <?php echo $SiteName;?> by an organization and no value appears in this field, 
			you may ask the agency for its referral code and insert it into the box.
        </p>
    </div>
</div>
<?php
/**
 * ============================
 * Standard Profile Form
 * ============================
 */
?>
<script>
	$("#js-warning").hide();
	$("#sign-up-form").show();

	$(function() {
		var formElement = $("#sign-up-form"),
			submitButton = $("#submit-button-trigger"),
			emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,

			// login availability
			loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
			loginAvailableString = "Login Available",
			checkLoginAvail = function(value, alertElement, element, confirmElement){
				$.get(loginAvailUrl,
					{
						IndividualLogin: value
					},
					function(data){
						if (data !== loginAvailableString){
							alertElement.html(data);
							if (confirmElement){
								confirmElement.val("");
							}
							//element.focus();
						}
					});
            },
            checkEmail = function(el){
                var elId = el.attr("id"),
                    confirmEl = $("#" + elId + "Confirm"),
                    value = el.val(),
                    alertElement = el.closest("label").find(".alert");
                alertElement.html("");

                if ($.trim(value) && emailReg.test(value)){
                    checkLoginAvail(value, alertElement, el, confirmEl);
                }
            },
            checkPhone = function(el, setfocus){
                var value = el.val().replace(/[^\d]/g, ""),
                    alertElement = el.closest("label").find(".alert"),
                    areacodes = Array(<?php echo $Config_areacodes;?>),
                    areacode = Number(value.substr(0,3)),
                    areacodeFound = $.inArray(areacode, areacodes) >= 0;
                alertElement.html("");

                el.val(value); // replace with just numbers

                if (value){
                    //check to see if first three digits are a valid area code
                    if (!areacodeFound){
                        alertElement.html("Area Code is invalid");
                        if (setfocus){
                            el.focus();
                        }
                        return false;
                    }else {
                        if (value.length != 10){
                            if (value.length < 10){
                                alertElement.html("Phone numbers must be 10 digits");
                                if (setfocus){
                                    el.focus();
                                }
                                return false;
                            }else{
                                alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                                el.val(value.substr(0,10));
                                return false;
                            }
                        } else {
                            if (el.attr("id") === "company_contactphone"){
                                checkLoginAvail(value, alertElement, el);
                            }
                            return true;
                        }
                    }
                }else{
                    return true;
                }
            },
            company_contactEmail = $("#company_contactemail");

        company_contactEmail.blur(function(){
			var el = $(this);
            checkEmail(el);
		});

		$("#company_contactphone, #company_phone").blur(function(){
            var el = $(this),
                value = el.val().replace(/[^\d]/g, ""),
                alertElement = el.closest("label").find(".alert"),
                areacodes = Array(<?php echo $Config_areacodes;?>),
                areacode = Number(value.substr(0,3)),
                areacodeFound = $.inArray(areacode, areacodes) >= 0;
            alertElement.html("");

            el.val(value); // replace with just numbers

            if (value){
                //check to see if first three digits are a valid area code
                if (!areacodeFound){
                    alertElement.html("Area Code is invalid");
                    //el.focus();
                    return false;
                }else {
                    if (value.length != 10){
                        if (value.length < 10){
                            alertElement.html("Phone numbers must be 10 digits");
                            //el.focus();
                            return false;
                        }else{
                            alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                            el.val(value.substr(0,10));
                            return false;
                        }
                    } else {
                        if (el.attr("id") === "company_contactphone"){
                            checkLoginAvail(value, alertElement, el);
                        }
                        return true;
                    }
                }
            }else{
                return true;
            }
		});

		// event handlers

		$("#submit-button-trigger").click(function(e){
			e.preventDefault();
			$(this).closest("form").submit();
		});

		// page init

		jQuery.validator.addMethod("isstate", function(value) {
			value = value.replace(/\./g, "");
			document.getElementById('company_state').value=value;
			var states = ['AK','AL','AR','AS','AZ','CA','CO','CT','DC','DE','FL','GA','GU','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MH','MI','MN','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','PR','PW','RI','SC','SD','TN','TX','UT','VA','VI','VT','WA','WI','WV','WY','ALABAMA','ALASKA','ARIZONA','ARKANSAS','CALIFORNIA','COLORADO','CONNECTICUT','DELAWARE','FLORIDA','GEORGIA','HAWAII','IDAHO','ILLINOIS','INDIANA','IOWA','KANSAS','KENTUCKY','LOUISIANA','MAINE','MARYLAND','MASSACHUSETTS','MICHIGAN','MINNESOTA','MISSISSIPPI','MISSOURI','MONTANA','NEBRASKA','NEVADA','NEW HAMPSHIRE','NEW JERSEY','NEW MEXICO','NEW YORK','NORTH DAKOTA','NORTH CAROLINA','OHIO','OKLAHOMA','OREGON','PENNSYLVANIA','RHODE ISLAND','SOUTH CAROLINA','SOUTH DAKOTA','TENNESSEE','TEXAS','UTAH','VERMONT','VIRGINIA','WASHINGTON','WEST VIRGINIA','WISCONSIN','WYOMING']
			var in_array = $.inArray(value.toUpperCase(), states);
			if (in_array == -1) {
				return false;
			}else{
				return true;
			}
		}, "Data provided is not a valid US State");

		$.validator.addMethod("phoneUS", function(value, element){
			var numValue = value.replace(/[^\d]/g, ""),
				areacodes = Array(<?php echo $Config_areacodes;?>),
				areacode = Number(value.substr(0,3)),
				areacodeFound = $.inArray(areacode, areacodes) >= 0;

			if (numValue){
				//check to see if first three digits are a valid area code
				if (!areacodeFound){
					return false;
				}else {
					if (numValue.length != 10){
						return false;
					} else {
						return true;
					}
				}
			}else{
				return true;
			}
		}, "Must be a valid phone number in a valid area code.");

		$("#sign-up-form").validate({
			rules: {
				company_contactemailconfirm: {
					equalTo: "#company_contactemail",
					required: true,
					email: true
				},
				company_contactemail: {
					required: true,
					email: true
				},
				company_contactphone: {required: true, phoneUS: true},
				company_contactlasstname: {required: true},
				company_contactfirstname: {required: true},
				company_phone: {required: true, phoneUS: true},
				company_zip: {required: true,digits: true,minlength:5,maxlength:5},
				company_state: {required: true,isstate: true},
				company_city: {required: true},
				company_address: {required: true},
				company_name: {required: true}
			},
			messages: {
				company_contactemail: {
					required: "The Email is required.",
					email: "A valid Email is essential."
				},
				company_contactemailconfirm: {
					equalTo: "Please use the same email as above.",
					required: "Please verify the email address.",
					email: "You have not entered a recognizable email address."
				}
			}
		});
		//end validation rules

        formElement.on("change.CETDASH", ".affects-geo", function(e){
            var editedElement = $(this),
                onlyIfBlank = editedElement.hasClass("if-blank"),
                overrideCheckbox = formElement.find("input[name='recalc_geo']");
            if (onlyIfBlank){
                if (editedElement.val().trim() === ""){
                    overrideCheckbox.val("1");
                    overrideCheckbox.prop("checked", true);
                } else {
                    overrideCheckbox.val("");
                    overrideCheckbox.prop("checked", false);
                }
            } else {
                overrideCheckbox.val("1");
                overrideCheckbox.prop("checked", true);
            }
        });

        (function(){
            checkEmail(company_contactEmail);
            checkPhone($("#company_contactphone"));
        })();
	});
</script>

<?php
/**
 * ==============================
 * Registration form
 * ==============================
 */
if ($isRegistration){
?>
<script type="text/template" id="citygrid-hiddenfields">
	<input id="company_officialname" name="company_officialname" type="hidden" value="<?php echo $merchantRecord->officialname;?>">
	<input id="CountryName" name="company_country" type="hidden" value="<?php echo $merchantRecord->country;?>">
	<input id="Lat" name="company_lat" type="hidden" value="<?php echo $merchantRecord->lat;?>" class=" affects-geo if-blank">
	<input id="Long" name="company_long" type="hidden" value="<?php echo $merchantRecord->long;?>" class=" affects-geo if-blank">
    <input type="hidden" name="recalc_geo" value="<?php echo ($merchantRecord->lat && $merchantRecord->long) ? "" : "1";?>">
	<input id="CityGridIDName" name="company_citygridname" type="hidden" value="<?php echo $merchantRecord->cityGridIdName;?>">
	<input id="CityGridProfileIDName" name="company_citygridid" type="hidden" value="<?php echo $merchantRecord->cityGridProfileId;?>">
	<input id="Neighborhood" name="company_neighborhood" type="hidden" value="<?php echo $merchantRecord->neighborhood?>">
    <input id="CityGridCategories" name="company_keywords" type="hidden" value="<?php echo $merchantRecord->keywords;?>">
    <input name="company_descriptionshow" type="hidden" value="<?php echo $merchantRecord->isDescriptionPublic ? "yes" : "";?>">
    <input name="company_donationshow" type="hidden" value="<?php echo $merchantRecord->donationshow?>">
    <input type="hidden" id="company_pid" name="company_pid" value="<?php echo $_GET['confirm-pid'] ? htmlspecialchars($_GET['confirm-pid']) : "";?>">
    <input type="hidden" id="company_id" name="company_id" value="<?php echo $merchantRecord->id ? $merchantRecord->id : "";?>">
</script>
<script type="text/javascript">
	$($("#citygrid-hiddenfields").html()).prependTo($("#profile-fields"));
</script>
<?php
}
if ($allowCitygridLookup){
?>
<script type="text/template" id="city-lookup">
	<div id="location-search" class="six columns centered">
		<label class="centered">
			<span class="highlight">
				<input type="text" id="city" class="auto-complete five columns auto-watermarked" title="enter business city or zip code" />
			</span>
		</label>
		<div id="step-one" class="step_btn btn" style="float:right;">
			<span>Step 1</span>
			<a id="step-one-trigger" href="#" title="next">Next ></a>
		</div>
	</div>
</script>
<script type="text/template" id="citygrid-attribution">
	<div class="third-party-attribution">Powered by <img src="<?php echo $CurrentServer;?>images/citygrid_logo.jpg" style="height:15px;"></div>
</script>
<script type="text/javascript">
	// page init
	$("#profile-fields").hide();

	<?php /* This is only for the registration form */?>
	$(function(){
		var formElement = $("#sign-up-form"),
			cityLookup = $($("#city-lookup").html()),
			citygridAttribution = $($("#citygrid-attribution").html()),

			// cached URLs
			cityAutocompleteSourceURL = "<?php echo $CurrentServer;?>LookupCity",
			merchantAutocompleteSourceURL = "<?php echo $CurrentServer;?>Library/BizLookup",

			// cached elements
			bizImageDiv = $('<div id="DisplayBizNameInfo"></div>'),
			cityAutocompleteElement, stepOneButton,
			merchantAutocompleteElement = $("#company_name"),
			addressElement = $("#company_address"),
			officialnameElement = $("#company_officialname"),
			cityElement = $("#company_city"),
			stateElement = $("#company_state"),
			postalCodeElement = $("#company_zip"),
			phoneElement = $("#company_phone"),
			contactPhoneElement = $("#company_contactphone"),
			websiteElement = $("#company_website"),
			descriptionElement = $("#company_description"),
			profileFields = $("#profile-fields"),

			// citygrid lookup fields -- these are assigned further below after the elements are injected into the form
			countryElement, latElement, longElement, cityGridIDElement, cityGridProfileIdElement, neighborhoodElement, keywordElement,

			setCityGridData = function(cityGridData){
				if (cityGridData){
					addressElement.val(cityGridData.Address);
					officialnameElement.val(cityGridData.BizName);
					postalCodeElement.val(cityGridData.Zip);
					latElement.val(cityGridData.Lat);
					longElement.val(cityGridData.Long);
					cityGridIDElement.val(cityGridData.CityGridID);
					cityGridProfileIdElement.val(cityGridData.CityGridProfileID);
					neighborhoodElement.val(cityGridData.Neighborhood);
                    keywordElement.val(cityGridData.commaDelimitedCategories);
					phoneElement.val(cityGridData.Phone);
					contactPhoneElement.val(cityGridData.Phone);
					websiteElement.val(cityGridData.Website);
					descriptionElement.val(cityGridData.Description);
					cityElement.val(cityGridData.City);
					stateElement.val(cityGridData.State);
				} else {
					addressElement.val("");
					officialnameElement.val("");
					postalCodeElement.val("");
					latElement.val("");
					longElement.val("");
					cityGridIDElement.val("");
					cityGridProfileIdElement.val("");
					neighborhoodElement.val("");
                    keywordElement.val("");
					phoneElement.val("");
					contactPhoneElement.val("");
					websiteElement.val("");
					descriptionElement.val("");
				}
			},

			// variable to hold autocomplete location
			merchantLocation = "";

		// inject supporting elements for autocompletes
		bizImageDiv.prependTo(profileFields);
		cityLookup.prependTo(formElement);
		citygridAttribution.appendTo($("#name-autocomplete"));

		// now, re-assign cached elements
		cityAutocompleteElement = $("#city");
		stepOneButton = $("#step-one-trigger");
		countryElement = $("#CountryName");
		latElement = $("#Lat");
		longElement = $("#Long");
		cityGridIDElement = $("#CityGridIDName");
		cityGridProfileIdElement = $("#CityGridProfileIDName");
		neighborhoodElement = $("#Neighborhood");
        keywordElement = $("#CityGridCategories");

		merchantAutocompleteElement.addClass("auto-complete");

		// autocompletes
		cityAutocompleteElement.autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: cityAutocompleteSourceURL,
					dataType: "json",
					data: {
						term: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								location: item.City + "," + item.State,
								city: item.City,
								state: item.State,
								country: item.Country,
								lat: (item.Lat ? item.Lat : ""),
								long: (item.Long ? item.Long : ""),
								zip: (item.Zip ? item.Zip : ""),
								label: item.value,
								value: item.value
							}
						}));
					}
				});
			},
			minLength: 3,
			delay: 10,
			select: function( event, ui ) {
				var selectedItem = ui.item,
					stateValue = selectedItem.state;
				if (stateValue == "Washington, D.C."){
					stateValue = "DC";
				}
				merchantLocation = selectedItem.location;
				cityElement.val(selectedItem.city);
				stateElement.val(stateValue.replace(/\./g, ""));
				countryElement.val(selectedItem.country);
				latElement.val(selectedItem.lat);
				longElement.val(selectedItem.long);
				postalCodeElement.val(selectedItem.zip);
				stepOneButton.removeClass("inactive");
			},
			close : function (event, ui) {
				return false;
			}
		});
        stepOneButton.click(function(e){
			e.preventDefault();
            if ($(this).hasClass("inactive")){
                return; // not valid
            }
			profileFields.show();
			merchantAutocompleteElement.focus();
			$("#location-search").hide();
			profileFields.scrollTop();
		});
		//end CityAutocomplete

		merchantAutocompleteElement.autocomplete({
			source: function(request, response) {
				$.ajax({
					url: merchantAutocompleteSourceURL,
					dataType: "json",
					data: {
						term : request.term,
						bizlocation : merchantLocation
					},
					success: function(data) {
						response( $.map( data, function( item ) {
							return {
								label: item.BizName + " - " + item.Address,
								value: item.BizName,
								YelpID: item.YelpID,
								CityGridID: item.CityGridID,
								CityGridProfileID: item.CityGridProfileID
							}
						}));
						if (!data.BizName){
							bizImageDiv.html("");
							setCityGridData();
						}
					}
				});
			},
			minLength: 3,
			delay: 10,
			select: function(event, ui) {
				var lookupURL = '<?php echo $CurrentServer;?>Library/BizNameInfoJSON?CityGridProfileID='+ui.item.CityGridProfileID,
					bizImageTemplate = "<img src='{0}' width='200' align='left' hspace='10'>",
					bizImage = "",
                    checkRegistrationURL = "<?php echo $CurrentServer;?>ApiMerchants",
                    checkRegistrationData;


				$.getJSON(lookupURL, function(CityGrid) {
					if (CityGrid[0].Image_url){
						bizImage = bizImageTemplate.replace("{0}", CityGrid[0]);
					}
					//bizImageDiv.html(bizImage);
					setCityGridData(CityGrid[0]);
                    checkRegistrationData = {
                        "company_pid":formElement.find("input[name='company_pid']").val(),
                        "company_citygridid":formElement.find("input[name='company_citygridid']").val(),
                        "company_name":formElement.find("input[name='company_name']").val(),
                        "company_city":formElement.find("input[name='company_city']").val(),
                        "company_state":formElement.find("input[name='company_state']").val(),
                        "action": "check"
                    };
                    $.ajax({
                        url: checkRegistrationURL,
                        data: checkRegistrationData,
                        type: "GET",
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data){
                            <?php
                                // TODO: either integrate this template into the event_template exclusively, or find a way
                                //       to inject the alert message text in case this template is used for other purposes.
                            ?>
                            var alertMessageTemplate = "It seems that your business has already registered with <?php echo $SiteName;?>. \
                                Please <a href='#' class='login-action-from-register'>login now</a> to create an event with this template.\
                                You may also click here to  <a href='{resetLink}' class='colorbox-ajax '>send a password reset link</a> to the email address associated with this business.",
                                message;
                            $("#already-registered-message").html("");
                            if (data.isRegistered && data.resetLink){
                                // merchant already registered
                                message = alertMessageTemplate.replace(/{resetLink}/g, data.resetLink);
                                $("#already-registered-message").html(message);
                                $("#edit-business-address").hide();
                            } else {
                                if (data.pid){
                                    $("#company_pid").val(data.pid);
                                }
                                if (data.id){
                                    $("#company_id").val(data.id);
                                }
                                $("#merchant-search").slideUp();
                                $("#contact-info").slideDown();
                                $("#company_contactfirstname").focus();
                            }
                        }
                    });
				});
			}
		});
		//end BizName autocomplete

		// page init
		stepOneButton.addClass("inactive");
	});
</script>
<?php
}// end if is signup page
?>