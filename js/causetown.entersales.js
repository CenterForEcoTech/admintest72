var CAUSETOWN = CAUSETOWN || {};

CAUSETOWN.EnterEventSalesForm = function(){
    var charityNameLookup = {}, // holds quick lookup on id
        nextQueueId = 1,
        submittedQueue = {},
        renderedRecord = null,
        defaults = {
            apiUrl: null, // url to be called
            charityLookupUrl: null,
            charityLookupDelay: 500, // default delay by half a second
            favoriteCharities: [], // list of favorite charities to be rendered
            todaysEvents: [], // list of events available for entering sales
            pastEvents: [], // list of past events available for entering sales
            noEventFoundCallback: null, // function to call if no event found
            removeTransactionCallback: function(transcode){},
            formTemplateSelector: "#enter-sales-form-tmpl",
            flatDonationTemplateSelector: "#flat-donation-form-tmpl",
            onTheFlyTemplateSelector: "#on-the-fly-tmpl",
            submittedTransactionTemplateSelector: "#submitted-transaction-tmpl",
            customerChoiceTemplateSelector: "#customer-choice-tmpl",
            allowSelectCharity: false,
            charityChoiceTemplateSelector: "#charity-choice-tmpl",
            submitQueueSelector: "#submit-queue",
            showSelectCharity: false
        },
        settings = defaults,
        rememberedPercentage = 20,
        rememberedDate,
        rememberedCharity,
        methods = {
            init: function(options){
                settings = $.extend({}, defaults, options);
            },
            formatDate: function(d){
                function pad(n){return n<10 ? '0'+n : n}
                return pad(d.getMonth()+1)+'/'
                    + pad(d.getDate())+'/'
                    + d.getFullYear();
            },
            renderCharityChoice: function(type, charity, eventId){
                var recipientWrapper = $("#select-recipient-" + eventId),
                    charityChoiceTemplate = $(settings.charityChoiceTemplateSelector).html(),
                    featuredCauseId = recipientWrapper.data("featured-cause-id"),
                    charityChoiceText,
                    appendedElement;
                if (type === 'featured' || featuredCauseId != charity.id){
                    charityChoiceText = charityChoiceTemplate
                        .replace(/{type}/g, type)
                        .replace(/{id}/g, charity.id)
                        .replace(/{charityName}/g, charity.name)
                        .replace(/{charityPercentage}/g, charity.percentage);
                    appendedElement = $(charityChoiceText);
                    recipientWrapper.append(appendedElement);
                    if (!charity.percentage){
                        appendedElement.find(".percentage-container").hide();
                    }
                    charityNameLookup[charity.id] = charity.name;
                }
                if (type === 'featured'){
                    recipientWrapper.data("featured-cause-id", charity.id);
                }
            },
            renderCustomerChoice: function(eventId, percentage){
                var recipientWrapper = $("#select-recipient-" + eventId),
                    charityChoiceTemplate = $(settings.customerChoiceTemplateSelector).html(),
                    charityChoiceText,
                    appendedElement;
                charityChoiceText = charityChoiceTemplate
                    .replace(/{eventId}/g, eventId)
                    .replace(/{charityPercentage}/g, percentage);
                appendedElement = $(charityChoiceText);
                recipientWrapper.append(appendedElement);
                if (!percentage){
                    appendedElement.find(".percentage-container").hide();
                }
            },
            attachCharityPickerAutoComplete: function(container){
                var element = container.find(".org-name-picker"),
                    currentValue = element.val();
                if (settings.charityLookupUrl){
                    element.autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: settings.charityLookupUrl,
                                dataType: "json",
                                data: {
                                    term : request.term
                                },
                                success: function(data) {
                                    response( $.map( data, function( item ) {
                                        return {
                                            label: item.OrgName + " - " + item.Address + " " + item.City +" "+ item.State,
                                            value: item.OrgName,
                                            OrgID: item.OrgID,
                                            OrgEIN: item.OrgEIN
                                        }
                                    }));
                                }
                            });
                        },
                        minLength: 2,
                        delay: settings.charityLookupDelay,
                        select: function(event, ui) {
                            var wrapper = $(this).closest(".customer-choice-selection"),
                                einElement = wrapper.find("input[name='OrgEIN']"),
                                orgIdElement = wrapper.find("input[name='recipient_id']"),
                                orgNameElement = $(this);
                            orgIdElement.val($.isNumeric(ui.item.OrgID) ? ui.item.OrgID : 0);
                            einElement.val(ui.item.OrgEIN);
                            orgNameElement.val(ui.item.value);
                            currentValue = ui.item.value;
                            if (ui.item.OrgID){
                                charityNameLookup[ui.item.OrgID] = ui.item.value;
                            } else {
                                charityNameLookup[ui.item.OrgEIN] = ui.item.value;
                            }
                        }
                    });

                    // if user focuses on charity picker, select its associated radio button
                    element.focus(function(){
                        var wrapper = $(this).closest(".customer-choice-selection"),
                            radioButton = wrapper.find("input[name='recipient_id']");
                        radioButton.prop('checked', true);
                    });

                    // when user exits, if the data value in the field is different from the current value, erase the saved data
                    element.blur(function(){
                        var wrapper = $(this).closest(".customer-choice-selection"),
                            einElement = wrapper.find("input[name='OrgEIN']"),
                            orgIdElement = wrapper.find("input[name='recipient_id']"),
                            orgNameElement = $(this);
                        if (orgNameElement.val() !== currentValue){
                            einElement.val("");
                            orgIdElement.val(0);
                            orgNameElement.val("");
                        }

                    });
                }
            },
            renderForm: function(container, currentRecord){
                var formTemplate = $(settings.formTemplateSelector).html(),
                    formText, dateInput, percentageSelect;

                if (currentRecord){
                    renderedRecord = currentRecord;
                    if (renderedRecord.isFlatDonation){
                        formTemplate = $(settings.flatDonationTemplateSelector).html();
                    }
                    formText = formTemplate
                        .replace(/{eventTitle}/g, currentRecord.title)
                        .replace(/{eventDate}/g, currentRecord.dateDisplay)
                        .replace(/{eventId}/g, currentRecord.id)
                        .replace(/{donationPerUnit}/g, currentRecord.donationPerUnit)
                        .replace(/{unitDescriptor}/g, currentRecord.unitDescriptor);
                    container.html(formText);

                    if (currentRecord.featuredCause && currentRecord.featuredCause.id){
                        methods.renderCharityChoice("featured", currentRecord.featuredCause, currentRecord.id);
                    }
                    if (currentRecord.customerChoicePercentage || currentRecord.isFlatDonation){
                        methods.initCharities(container, currentRecord.customerChoicePercentage, currentRecord.id);
                    }

                    methods.initForm(container);
                } else {
                    // on-the-fly
                    if ($.isFunction(settings.noEventFoundCallback)){
                        settings.noEventFoundCallback(container);
                    } else {
                        // render our form.
                        renderedRecord = null;
                        formText = $(settings.onTheFlyTemplateSelector).html();
                        container.html(formText);
                        dateInput = $("#event-date-picker");
                        percentageSelect = $("#customer-choice-select");
                        if (dateInput.length){
                            dateInput.val(rememberedDate);
                        }
                        methods.initCharities(container, rememberedPercentage, 0);

                        container.find(".auto-datepicker").each(function(){
                            var element = $(this);
                            element.datepicker({
                                minDate:-30,
                                maxDate:0,
                                onSelect: function(dateText, inst){
                                    rememberedDate = dateText;
                                }
                            });
                        });
                        percentageSelect.val(rememberedPercentage);
                        percentageSelect.change(function(e){
                            var oldPercentage = rememberedPercentage;
                            rememberedPercentage = percentageSelect.val();
                            container.find(".percentage-value").each(function(){
                                $(this).html(rememberedPercentage);
                            });
                        });
                        methods.initForm(container);
                    }
                }
            },
            initCharities: function(container, percentage, eventId){
                var customerWatermarkText = "Required for Receipt or Customer Choice";
                container.find("input[name='customer']").attr("title", customerWatermarkText);
                if (settings.favoriteCharities && settings.favoriteCharities.length){
                    $.each(settings.favoriteCharities, function(index, item){
                        var favCharity = {
                            id : item.id,
                            name : item.name,
                            percentage : percentage
                        };
                        methods.renderCharityChoice("favorite", favCharity, eventId);
                    });
                }
                methods.renderCustomerChoice(eventId, percentage);
            },
            initForm: function(container){
                var flatUnitsInput = container.find("input[name='flat_units']"),
                    customerChoiceSection = container.find(".customer-choice-selection");
                container.find(".auto-watermarked").each(function(){
                    var self = $(this),
                        watermarkText = self.attr("title");
                    if (watermarkText){
                        self.watermark(watermarkText);
                    }
                });
                container.find("input[name='eligible_amount']").focus();

                // customer choice is selected by default unless there's a featured cause
                if (rememberedCharity){
                    container.find("input[name='recipient_id']").each(function(i,item){
                        var element = $(this),
                            charityValue = element.val();
                        if (charityValue === rememberedCharity){
                            element.prop("checked", true);
                        }
                    });
                    if (container.find("input[name='recipient_id']:checked").length === 0){
                        // must have been customer choice
                        container.find(".customer-choice-selection input[name='recipient_id']").prop("checked", true);
                    }
                } else {
                    container.find(".customer-choice-selection input[name='recipient_id']").prop("checked", true);
                    container.find(".select-recipient input.featured").prop("checked", true);
                }
                container.on("change", "input[name='recipient_id']", function(e){
                    var checkedElement = container.find("input[name='recipient_id']:checked");
                    if (checkedElement.length){
                        rememberedCharity = checkedElement.first().val();
                    }
                });

                // charity picker
                if (settings.allowSelectCharity){
                    methods.attachCharityPickerAutoComplete(container);
                    customerChoiceSection.on("click", ".show-charity-picker", function(e){
                        e.preventDefault();
                        customerChoiceSection.find(".cc-addtl-text").hide();
                        customerChoiceSection.find(".pos-picker").show();
                        settings.showSelectCharity = true;
                    });
                    customerChoiceSection.on("click", ".delete-icon", function(e){
                        e.preventDefault();
                        customerChoiceSection.find(".cc-addtl-text").show();
                        customerChoiceSection.find(".pos-picker").hide();
                        settings.showSelectCharity = false;
                    });
                    if (settings.showSelectCharity){
                        customerChoiceSection.find(".pos-picker").show();
                    } else {
                        customerChoiceSection.find(".cc-addtl-text").show();
                    }
                }

                // if flat donation, change initial focus and default charity
                if (flatUnitsInput.length){
                    flatUnitsInput.focus();
                    if (!rememberedCharity){
                        container.find(".select-recipient input.recipient-choice").first().prop("checked", true);
                    }
                }
            },
            findData : function(serializedArray, key){
                var found = $.grep(serializedArray, function(element, index){
                    return element.name == key;
                });
                if (found.length){
                    return found[0].value;
                }
                return null;
            },
            submit: function(formContainer){
                var submittedDivTemplate = $("#submission-message-tmpl").html(),
                    submittingDivTemplate = $(settings.submittedTransactionTemplateSelector).html(),
                    submitQueueId = nextQueueId++,
                    form = formContainer.find("form"),
                    arrayData = form.serializeArray(),
                    eventId = methods.findData(arrayData, "event"),
                    recipientId = methods.findData(arrayData, "recipient_id"),
                    orgEIN = methods.findData(arrayData, "OrgEIN"),
                    customer = methods.findData(arrayData, "customer"),
                    amount = methods.findData(arrayData, "eligible_amount"),
                    selectedCharityId = (recipientId) ? recipientId : orgEIN,
                    selectedCharityName = charityNameLookup[selectedCharityId],
                    customerText = (customer) ? customer : "No Customer Specified",
                    eventDate = methods.findData(arrayData, "eventDate"),
                    percentage = methods.findData(arrayData, "customerChoicePercentage"),
                    flatDonationUnits = methods.findData(arrayData, "flat_units"),
                    donationPerUnit = methods.findData(arrayData, "donationPerUnit"),
                    submittingContentText,
                    displayDonationAmount = amount;
                    if (flatDonationUnits && donationPerUnit){
                        displayDonationAmount = (flatDonationUnits * donationPerUnit).toFixed(2);
                    }
                    submittingContentText = submittingDivTemplate
                        .replace(/{number}/g, submitQueueId)
                        .replace(/{eligibleAmount}/g, displayDonationAmount)
                        .replace(/{orgId}/g, recipientId)
                        .replace(/{orgEIN}/g, orgEIN)
                        .replace(/{customer}/g, customerText);
                if (selectedCharityName){
                    submittingContentText = submittingContentText.replace(/{recipientText}/g, selectedCharityName);
                } else if (orgEIN && charityNameLookup[orgEIN]) {
                    submittingContentText = submittingContentText.replace(/{recipientText}/g, charityNameLookup[orgEIN]);
                } else {
                    submittingContentText = submittingContentText.replace(/{recipientText}/g, "Customer Choice");
                }
                $(settings.submitQueueSelector).prepend(submittingContentText);
                form.remove();
                submittedQueue[submitQueueId] = arrayData;
                formContainer.html(submittedDivTemplate.replace(/{eventId}/g, eventId));
                formContainer.find(".btn.submit-another a").focus();
                if (eventId == 0){
                    formContainer.find(".no-on-the-fly").each(function(){
                        $(this).hide();
                    });
                }
                $.ajax({
                    url : settings.apiUrl,
                    data :{
                        event: eventId,
                        eligible_amount: Number((amount + "").replace(/[^0-9\.]+/g,"")), // TODO: replace with region-specific converter
                        recipient_id: recipientId,
                        OrgEIN: orgEIN,
                        customer: customer,
                        eventDate: eventDate,
                        percentage: percentage,
                        flat_units: flatDonationUnits
                    },
                    type: "POST",
                    success: function(data){
                        var statusDiv = $("#submitted-transaction-" + submitQueueId),
                            statusAlert = statusDiv.find(".status.alert.info"),
                            deleteButton = statusDiv.find(".btn.administer"),
                            okButton = statusDiv.find(".btn.good");
                        statusAlert.hide();
                        deleteButton.removeClass("hidden");
                        okButton.removeClass("hidden");
                        deleteButton.click(function(e){
                            e.preventDefault();
                            if (!$(this).hasClass("inactive")){
                                methods.removeTransaction(data.record.transCode, eventId, statusDiv, submitQueueId);
                            }
                        });
                        okButton.click(function(e){
                            e.preventDefault();
                            if (!$(this).hasClass("inactive")){
                                statusDiv.slideUp();
                                delete submittedQueue[submitQueueId];
                            }
                        });
                    },
                    error : function(jqXHR, textStatus, errorThrown){
                        var statusDiv = $("#submitted-transaction-" + submitQueueId),
                            statusAlert = statusDiv.find(".status.alert.info"),
                            response = $.parseJSON(jqXHR.responseText),
                            messageTemplate = "{property}: {message};",
                            statusAlertText = "";
                        $.each(response.validationMessages, function(index, item){
                            statusAlertText += messageTemplate.replace(/{property}/g, item.property).replace(/{message}/g, item.message);
                        });
                        statusAlert.html(statusAlertText).removeClass("info");
                    }
                });
            },
            removeTransaction: function(transcode, eventId, statusDiv, submitQueueId){
                var undoButton = statusDiv.find(".btn.administer"),
                    okButton = statusDiv.find(".btn.good"),
                    removeSaleText = $("#remove-sale-tmpl").html(),
                    removeSaleDiv,deleteButton,cancelButton,
                    submitSaleButtons = $(".form-submitter:visible"),
                    enableButton = function(btn){
                        btn.removeClass("inactive");
                        btn.find("a").prop("disabled", false).removeClass("inactive");
                    },
                    disableButton = function(btn){
                        btn.addClass("inactive");
                        btn.find("a").prop("disabled", true).addClass("inactive");
                    },
                    abortUndo = function(){
                        enableButton(undoButton);
                        enableButton(okButton);
                        removeSaleDiv.slideUp('slow', function(){ removeSaleDiv.remove();});
                        enableButton(submitSaleButtons);
                    };

                disableButton(submitSaleButtons);
                disableButton(undoButton);
                disableButton(okButton);
                statusDiv.append(removeSaleText);
                removeSaleDiv = statusDiv.find(".validate-undo");
                removeSaleDiv.slideDown();
                // handle cancel
                cancelButton = removeSaleDiv.find(".btn.cancel");
                cancelButton.click(function(e){
                    e.preventDefault();
                    if (!cancelButton.hasClass("inactive")){
                        abortUndo();
                    }
                });
                // handle undo
                deleteButton = removeSaleDiv.find(".btn.delete");
                deleteButton.click(function(e){
                    var pwdField = removeSaleDiv.find("input[type='password']");
                    e.preventDefault();
                    if (!deleteButton.hasClass("inactive")){
                        disableButton(cancelButton);
                        disableButton(deleteButton);
                        $.ajax({
                            url : settings.apiUrl,
                            data : JSON.stringify({
                                eventId : eventId,
                                transcode : transcode,
                                password : pwdField.val()
                            }),
                            contentType: "application/json",
                            type : "DELETE",
                            success: function(data){
                                var messageDiv = statusDiv.find("span.status.alert");
                                if ($.isFunction(settings.removeTransactionCallback)){
                                    settings.removeTransactionCallback(transcode);
                                }
                                undoButton.remove();
                                okButton.remove();
                                enableButton(submitSaleButtons);
                                messageDiv.html("Sale Removed!")
                                removeSaleDiv.slideUp('slow', function(){ removeSaleDiv.remove();});
                                messageDiv.show().fadeOut(3000, function(){
                                    statusDiv.slideUp('slow', function(){
                                        statusDiv.remove();
                                        delete submittedQueue[submitQueueId];
                                    });
                                });
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                var response = $.parseJSON(jqXHR.responseText),
                                    messageDiv = statusDiv.find("span.status.alert");
                                // we just close the form and display a fading error
                                messageDiv.html("Invalid Password");
                                messageDiv.show().fadeOut(3000);
                                abortUndo();
                            }
                        });
                    }
                });
            }
        };

    methods.init();
    rememberedDate = methods.formatDate(new Date());
    rememberedPercentage = 20;
    rememberedCharity = null;

    // public methods
    return {
        init: function(options){
            methods.init(options);
        },
        renderForm: function(container, eventRecord){
            return methods.renderForm(container, eventRecord);
        },
        getCurrentRecord: function(){
            return renderedRecord;
        },
        submit: function(container){
            return methods.submit(container);
        },
        addCharityName: function(charityId, charityName){
            charityNameLookup[charityId] = charityName;
        }
    };
}();

CAUSETOWN.EnterSalesTabs = function(){
    var enterSalesTab = null, enterSalesLinkTemplate, enterSalesContent, enterSalesTabOriginalId,
        eventsListTab = null, eventDisplayLinkTemplate, eventsListContent,
        eventsListLoaded,
        currentRecord,
        defaults = {
            enterSalesSelector: "#enterSales",
            enterSalesContainerSelector: "#enter-sales",
            eventListSelector: "#eventsList",
            eventListContainerSelector: "#event-list",
            enterSalesLinkTemplateSelector: "#enter-sales-link-tmpl",
            eventDisplayLinkTemplateSelector: "#event-display-link-tmpl",
            renderFormCallback : CAUSETOWN.EnterEventSalesForm.renderForm, // callback to render the form
            noEventCallback: CAUSETOWN.EnterEventSalesForm.renderForm, // callback to render what happens if there is no event for that day
            todaysEvents: [],
            pastEvents: [],
            futureEvents: []
        },
        settings = defaults,
        methods = {
            init: function(options){
                settings = $.extend({}, defaults, options);
                enterSalesTab = $(settings.enterSalesSelector);
                enterSalesTabOriginalId = enterSalesTab.attr("id");
                enterSalesContent = $(settings.enterSalesContainerSelector);
                eventsListTab = $(settings.eventListSelector);
                eventsListContent = $(settings.eventListContainerSelector);
                eventsListLoaded = false;
                enterSalesLinkTemplate = $(settings.enterSalesLinkTemplateSelector);
                if (enterSalesLinkTemplate.html){
                    enterSalesLinkTemplate = enterSalesLinkTemplate.html();
                }
                eventDisplayLinkTemplate = $(settings.eventDisplayLinkTemplateSelector);
                if (eventDisplayLinkTemplate.html){
                    eventDisplayLinkTemplate = eventDisplayLinkTemplate.html();
                }
                // clean up bad data
                if (!$.isArray(settings.todaysEvents)){
                    settings.todaysEvents = [];
                }
                if (!$.isArray(settings.pastEvents)){
                    settings.pastEvents = [];
                }
                if (!$.isArray(settings.futureEvents)){
                    settings.futureEvents = [];
                }
            },
            renderListLines: function(eventListArray, template){
                var contentText = "";
                $.each(eventListArray, function(index, value){
                    var linkText = template
                        .replace(/{eventId}/g, value.id)
                        .replace(/{eventDate}/g, value.dateDisplay)
                        .replace(/{eventTitle}/g, value.title);
                    contentText += linkText;
                });
                return contentText;
            },
            renderEventListLines: function(eventListArray){
                return methods.renderListLines(eventListArray, enterSalesLinkTemplate);
            },
            renderEventDisplayLines: function(eventListArray){
                return methods.renderListLines(eventListArray, eventDisplayLinkTemplate);
            },
            findEventById: function(eventId){
                var matchingList;

                matchingList = $.grep(settings.todaysEvents, function(item, index){
                    return item.id == eventId;
                });
                if (matchingList.length){
                    return matchingList[0];
                } else {
                    matchingList = $.grep(settings.pastEvents, function(item, index){
                        return item.id == eventId;
                    });
                    if (matchingList.length){
                        return matchingList[0];
                    }
                }
                return null;
            },
            renderForm: function(eventRecord){
                if (eventRecord && $.isFunction(settings.renderFormCallback)){
                    settings.renderFormCallback(enterSalesContent, eventRecord);
                    enterSalesTab.attr("id", "#" + eventRecord.id);
                } else if ($.isFunction(settings.noEventCallback)){
                    settings.noEventCallback(enterSalesContent);
                    enterSalesTab.attr("id", enterSalesTabOriginalId);
                }
            },
            handleHash: function(hash){
                var activeClass = "active",
                    clearActiveTab = function(){
                        $("ul.tabs li a").removeClass(activeClass);
                    },
                    setActiveTab = function(tabHash){
                        var trimmedHash = tabHash.replace(/Tab/, ""),
                            activeTab = $(trimmedHash),
                            activeTabHref = activeTab.attr("href"),
                            candidateEventId = trimmedHash.replace(/#/, "");
                        if ($.isNumeric(candidateEventId)){
                            currentRecord = methods.findEventById(candidateEventId);
                            methods.renderForm(currentRecord);
                            clearActiveTab();
                            enterSalesTab.addClass(activeClass);
                            enterSalesContent.show().addClass(activeClass).siblings().hide().removeClass('active');
                        } else  if (activeTabHref && activeTabHref.charAt(0)=="#"){
                            activeTab.click();
                        } else {
                            enterSalesTab.click();
                        }
                    };
                setActiveTab(hash);
            }
        };

    methods.init();

    // enter sales tab clicked
    if (enterSalesTab.length){
        enterSalesTab.click(function(){
            var multipleEventsHeader = "<h5>Enter Sales for Today's Events</h5><ul>",
                multipleEventsFooter = "</ul>",
                contentText,
                candidateEventId = window.location.hash.replace(/#/,"");
            if ($.isNumeric(candidateEventId)){
                currentRecord = methods.findEventById(candidateEventId);
                methods.renderForm(currentRecord);
                methods.removeHash();
            } else if (settings.todaysEvents.length){
                if (settings.todaysEvents.length == 1){
                    currentRecord = settings.todaysEvents[0];
                    methods.renderForm(currentRecord);
                } else {
                    contentText = multipleEventsHeader;
                    contentText += methods.renderEventListLines(settings.todaysEvents);
                    contentText += multipleEventsFooter;
                    enterSalesContent.html(contentText);
                }
            } else {
                if ($.isFunction(settings.noEventCallback)){
                    settings.noEventCallback(enterSalesContent);
                }
            }
        });
    }

    // load event list when tab clicked
    if (eventsListTab.length){
        eventsListTab.click(function(){
            var contentText = "";
            if (eventsListLoaded){
                return;
            }
            if (settings.todaysEvents.length){
                contentText += "<h5>Enter Sales for Today&rsquo;s Events</h5><ul>";
                contentText += methods.renderEventListLines(settings.todaysEvents);
                contentText += "</ul>";
            }
            if (settings.futureEvents.length){
                contentText += "<h5>Upcoming Events</h5><ul>";
                contentText += methods.renderEventDisplayLines(settings.futureEvents);
                contentText += "</ul>";
            }
            if (settings.pastEvents.length){
                contentText += "<h5>Enter Sales for Previous Events</h5><ul>";
                contentText += methods.renderEventListLines(settings.pastEvents);
                contentText += "</ul>";
            }
            eventsListContent.html(contentText);
            eventsListLoaded = true;
        });
    }

    // add handler to add hash
    $("ul.tabs li a").click(function(e){
        var clickedTab = $(this),
            contentLocation = $(this).attr('href'),
            candidateEventId = clickedTab.attr("id").replace(/#/, "").replace(/Tab/, "");

        //Let go if not a hashed one
        if(contentLocation && contentLocation.charAt(0)=="#") {
            if (contentLocation == "#enter-sales" && !$.isNumeric(candidateEventId)){
                // don't show if it's the default tab
                CAUSETOWN.removeHash();
            } else {
                window.location.hash = $(this).attr("id") + "Tab";
            }
        }
    });

    $(".tabs-content").on("click", "span.show-enter-sales-tab", function(){
        enterSalesContent.show().addClass("active").siblings().hide().removeClass('active');
    });

    return {
        init: function(options){
            methods.init(options);
        },
        handleHash : function(hash){
            methods.handleHash(hash);
        },
        renderForm: function(eventRecord){
            methods.renderForm(eventRecord);
        }
    }
}();

CAUSETOWN.handleHash = CAUSETOWN.EnterSalesTabs.handleHash;

CAUSETOWN.SalesHistoryTab = function(){
    var defaults = {
            apiUrl: null,
            invoicingAllowed: false,
            salesHistoryTabSelector: "#sales-history",
            loadingDivTemplateSelector: "#history-tab-loading-tmpl",
            historyTableCoreTemplateSelector: "#history-tab-loaded-tmpl",
            historyTableRowTemplateSelector: "#history-table-row-tmpl"
        },
        settings = defaults,
        methods = {
            init: function(options){
                settings = $.extend({}, defaults, options);
            },
            removeTransaction: function(transcode){
                var currentRecord = CAUSETOWN.EnterEventSalesForm.getCurrentRecord(),
                    salesHistoryContent = $(settings.salesHistoryTabSelector);
                if (salesHistoryContent.hasClass("active") && currentRecord){
                    methods.renderHistory(currentRecord.id, currentRecord.title, currentRecord.dateDisplay);
                }
            },
            clickHistoryButton: function(e){
                var button = $(e.currentTarget),
                    eventId = button.attr("data-event-id"),
                    currentRecord = CAUSETOWN.EnterEventSalesForm.getCurrentRecord(),
                    salesHistoryContent = $(settings.salesHistoryTabSelector),
                    loadingDivTemplate = $(settings.loadingDivTemplateSelector).html();
                e.preventDefault();
                salesHistoryContent.html(loadingDivTemplate);
                methods.renderHistory(eventId, currentRecord.title, currentRecord.dateDisplay);
            },
            renderHistory: function(eventId, eventTitle, eventDateDisplay){
                var salesHistoryContent = $(settings.salesHistoryTabSelector),
                    historyTableCoreTemplate = $(settings.historyTableCoreTemplateSelector).html(),
                    historyTableRowTemplate = $(settings.historyTableRowTemplateSelector).html(),
                    historyTabText, historyTableBody, historyRowText, invoiceButton;
                // we never display history tab handle
                salesHistoryContent.show().addClass('active').siblings().hide().removeClass('active');
                // now load the data
                $.ajax({
                    url : settings.apiUrl,
                    type : "GET",
                    data : {
                        event : eventId
                    },
                    success : function(data){
                        historyTabText = historyTableCoreTemplate
                            .replace(/{eventTitle}/g, eventTitle)
                            .replace(/{eventDate}/g, eventDateDisplay)
                            .replace(/{totalSalesFigure}/g, data.totalSales)
                            .replace(/{totalDonationsFigure}/g, data.totalBenefit);
                        salesHistoryContent.html(historyTabText);
                        historyTableBody = salesHistoryContent.find(".transaction-table tbody");
                        $.each(data.transactions, function(index, row){
                            historyRowText = historyTableRowTemplate
                                .replace(/{recipientText}/g, row.recipientText)
                                .replace(/{timestamp}/g, row.timestamp)
                                .replace(/{invoiceStatusText}/g, row.invoiceStatusText)
                                .replace(/{eligibleAmount}/g, row.eligibleAmount)
                                .replace(/{percentage}/g, row.percentageAmount)
                                .replace(/{generatedDonation}/g, row.generatedAmount)
                                .replace(/{customerText}/g, row.customerText)
                                .replace(/{transcode}/g, row.transCode);
                            historyTableBody.append(historyRowText);
                        });
                        if (settings.invoicingAllowed && data.uninvoicedItemCount > 0){
                            methods.handleInvoicing(salesHistoryContent, data, eventId);
                        }
                    }
                });
            },
            handleInvoicing: function(salesHistoryContent, data, eventId){
                // handle invoice
                var invoiceButton = salesHistoryContent.find(".btn.administer.create-invoice"),
                    invoiceLoadingSpan;
                if (invoiceButton && invoiceButton.hasClass){
                    invoiceLoadingSpan = invoiceButton.next(".alert.info.loading");
                    invoiceButton.removeClass("hidden");
                    invoiceButton.click(function(e){
                        e.preventDefault();
                        if (!invoiceButton.hasClass("inactive")){
                            invoiceButton.addClass("hidden");
                            invoiceLoadingSpan.removeClass("hidden");
                            $.ajax({
                                url : settings.apiUrl,
                                type : "POST",
                                data : {
                                    "action" : "invoice",
                                    "event" : eventId
                                },
                                success : function(data){
                                    var invoiceId = data.InvoiceID,
                                        invoiceDate = data.InvoicedDate,
                                        invoiceStatusText = "Invoice# " + invoiceId +" invoiced on " + invoiceDate;
                                    if (data.InvoiceID){
                                        $.each(data.TransCodes, function(index, item){
                                            var element = $("#Invoiced_" + item.TransCode);
                                            element.html(invoiceStatusText);
                                        });
                                    } else {
                                        // error?
                                        invoiceButton.removeClass("hidden");
                                    }
                                    invoiceLoadingSpan.addClass("hidden");
                                    invoiceButton.after("<span class='alert info'>"+data.message+"</span>");
                                }
                            });
                        }
                    });
                }
            }
            // end methods
        };

    methods.init();

    return {
        init: function(options){
            methods.init(options);
        },
        clickHistoryButton: function(e){
            return methods.clickHistoryButton(e);
        },
        removeTransaction: function(transcode){
            return methods.removeTransaction(transcode);
        }
    }
}();