
//checks to see if logged in before proceeding to pages that require a user to be logged in
function CheckIfSignedIn(MemberID,nextpage){
	var nextpage = nextpage;
	var MemberID = MemberID;
	if (!MemberID){
		$("div#panel").slideDown("slow");
		$("#toggle a").toggle();
	}else{
		var newurl = 'Index.php?PageName='+nextpage;
		window.location.href = newurl;
	}

}

function EINnormalize(EIN){
	var EIN = EIN;
	//check the string around character 9
	if (EIN.length == 9){
		EIN = EIN.replace("-", "");
		document.getElementById('EIN').value=EIN;
		document.getElementById('NextStep').disabled = false;
	}
}

	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
			num = "0";

		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();

		if(cents<10)
			cents = "0" + cents;

		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+','+
			num.substring(num.length-(4*i+3));
			var ReturnVal = (((sign)?'':'-') + num + '.' + cents);
			if (ReturnVal == "0.00"){
				ReturnVal = "";
			}
			return ReturnVal;
	}
