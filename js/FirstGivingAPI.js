var FG_GRAPHWIDGET=new Object();
if(undefined==FG_GRAPHWIDGET_PARAMS){
	var FG_GRAPHWIDGET_PARAMS={}
}
var port=8983;
if(location.protocol=="https:"){
	port=8984
}
FG_GRAPHWIDGET.endpoint=location.protocol+"//graphapi.firstgiving.com:"+port+"/solr/select?";
FG_GRAPHWIDGET.pcEndpoint=location.protocol+"//graphapi.firstgiving.com";
FG_GRAPHWIDGET.cdn=location.protocol+"//assets.firstgiving.com/graphwidget";
FG_GRAPHWIDGET.cbFunc="FG_GRAPHWIDGET.renderResults";
FG_GRAPHWIDGET.localesource=2;
FG_GRAPHWIDGET.charityMap={
	A:"Arts, Culture & Humanities",
	B:"Education",C:"Environment",
	D:"Animal-Related",
	E:"Health Care",
	F:"Mental Health & Crisis Intervention",
	G:"Voluntary Health Associations & Medical Disciplines",
	H:"Medical Research",
	I:"Crime & Legal-Related",
	J:"Employment",
	K:"Food, Agriculture, Nutrition",
	L:"Housing & Shelter",
	M:"Public Safety, Disaster Preparedness & Relief",
	N:"Recreation & Sports",
	O:"Youth Development",
	P:"Human Services",
	Q:"International, Foreign Affairs",
	R:"Civil Rights, Social Action & Advocacy",
	S:"Community Improvement & Capacity Building",
	T:"Philanthropy, Voluntarism & Grantmaking Foundations",
	U:"Science & Technology",
	V:"Social Science",
	W:"Public & Societal Benefit",
	X:"Religion-Related",
	Y:"Mutual & Membership Benefit",
	Z:"Unknown"};
FG_GRAPHWIDGET.localeLogos={
	1:"/static/images/just_giving_logo.png",
	2:"/static/images/first_giving_logo.png"};
FG_GRAPHWIDGET.defaultPageSize=10;
FG_GRAPHWIDGET.submitSearch=function(e){
	if(!e){e=0}
	var a=e*this.defaultPageSize;
	var f="";
	if(!jQuery("#fgNameStart").hasClass("fgHinted")){
		f=jQuery("#fgNameStart").val().toLowerCase()
	}
	if(f==""){
		FG_GRAPHWIDGET.clearResults();
		return false
	}
	f=encodeURIComponent(f)+"*";
	var d="&q={!boost b=pow(10000,boost_delta)} "+f+" AND object_type:charity AND object_source_locale_id:"+FG_GRAPHWIDGET.localesource;
	var c=jQuery("#fgNtee").val();
	if(c!=""){d+=" AND category_code:"+c+"*"}
	if(jQuery("#fgAdvancedDToggle").attr("checked")){
		var b=jQuery("#fgZip").val();
		if(b!=""){
			if(b.length!=5){
				return false
			}
			var g=jQuery("#fgZipRadius").val();
			if(g==""||g==0){g=1}
			d+="&fq={!geofilt sfield=latlong}&pt="+jQuery("#fgZipLatLong").val()+"&d="+Math.round(g/0.62137)
		}
	}
	jQuery("#fgResultsLoaderAnim").show();
	jQuery.ajax({
		type:"get",
		url:this.endpoint+"qt=standard&wt=json&rows="+this.defaultPageSize+"&start="+a+d,
		dataType:"jsonp",
		jsonp:"json.wrf",
		jsonpCallback:this.cbFunc
    })
};
FG_GRAPHWIDGET.clearResults=function(){
	jQuery("#fgResults").hide();
	jQuery("#fgResults ul").html("");
	jQuery("#fgPagination").html("")
};
FG_GRAPHWIDGET.renderResults=function(b){
    var h=null;
    var a=null;
    var d="";
    var j=b.response;
    var l=this;
    var g="";
    jQuery("#fgResultsLoaderAnim").hide();
    if(j.numFound>0){
        jQuery.each(j.docs,function(m,n){
            if(n.category_code==""){a="Z"}else{a=n.category_code.substr(0,1)}
            h="select_"+n.id_uuid;
            d+='<li id="select_'+n.id_uuid+'" class="fgResult" onClick=""><img class="fgNTEEIcon" src="'+l.cdn+"/static/images/"+a.toLowerCase()+'.png"><div class="fgCharityHeader"><span>'+n.object_name+"</span>";
            if(FG_GRAPHWIDGET.localesource==1){
                g="Registered Charity No. "+n.id_sigma
            }else{
                if(FG_GRAPHWIDGET.localesource==4){
                    g=n.city+", "+n.state+", "+n.zip+"<br>"+(n.category_title=="Unknown"?"":n.category_title)
                }else{
                    g=n.city+", "+n.state+", "+n.zip+"<br>"+(a=="Z"?"":l.charityMap[a])
                }
            }
            d+='<div class="fgNteeCatTitle">'+g+'</div><div style="display:none" id="payload_'+n.id_uuid+'">'+JSON.stringify(n)+"</div></div>";
            if(undefined!=n.object_description&&n.object_description!=""){
                d+='<div class="fgDesc">'+n.object_description+"</div></li>"}
            }
        );
        jQuery("#fgResults").html("<ul>"+d+"</ul>");
        jQuery("#fgResults").show();
        var k="";
        var i=(j.start/this.defaultPageSize);
        var e=Math.floor(j.numFound/this.defaultPageSize);
        var f=(i>0)?'<a href="javascript:void(0)" onclick="javascript:FG_GRAPHWIDGET.submitSearch('+(i-1)+')"><img src="'+l.cdn+'/static/images/pagination_arrow.png" id="fgPreviousResults"/></a>':"";
        var c=(i<e)?'<a href="javascript:void(0)" onclick="javascript:FG_GRAPHWIDGET.submitSearch('+(i+1)+')"><img src="'+l.cdn+'/static/images/pagination_arrow_next.png" id="fgNextResults"/></a>':'<img src="'+l.cdn+'/static/images/pagination_arrow_next.png" id="fgNextResults"/>';
        k=f+" Page "+(i+1)+c+" of "+(e+1);
        k+=" ("+j.numFound+" results)";
        jQuery("#fgPagination").html(k)
    }else{
        this.clearResults()
    }
};
FG_GRAPHWIDGET._btnEv=function(b,a){};
FG_GRAPHWIDGET._resEv=function(c,a,b){};
FG_GRAPHWIDGET.bindEvent=function(a,b){
    if(a=="button"){
        this._btnEv=b
    }else{
        if(a=="result"){
            this._resEv=b
        }
    }
};
FG_GRAPHWIDGET.setZipcode=function(a){
    if(null!=a&&undefined!=a.payload.zipcode){
        jQuery("#fgZip").val(a.payload.zipcode);
        jQuery("#fgZipLatLong").val(a.payload.latitude+","+a.payload.longitude)
    }
};
FG_GRAPHWIDGET.init=function(b){
    var a=this;
    jQuery("#fgGraphWidgetContainer").show();
    jQuery("#fgGraphWidgetContainer").html(b);
    if(undefined!=FG_GRAPHWIDGET_PARAMS){
        if(undefined!=FG_GRAPHWIDGET_PARAMS.button){
            jQuery("#fgNameStart").attr("size","35");
            jQuery("#fgButtonWrapper").html('<input type="button" id="fgActionButton" value="Next" value="go!" onclick="FG_GRAPHWIDGET._btnEv(jQuery(\'#fg_selected_charity_uuid\').val());">');
            if(undefined!=FG_GRAPHWIDGET_PARAMS.button.action){
                FG_GRAPHWIDGET.bindEvent("button",FG_GRAPHWIDGET_PARAMS.button.action)
            }
            if(undefined!=FG_GRAPHWIDGET_PARAMS.button.label){
                jQuery("#fgActionButton").val(FG_GRAPHWIDGET_PARAMS.button.label)
            }
        }
        if(undefined!=FG_GRAPHWIDGET_PARAMS.localesource){
            FG_GRAPHWIDGET.localesource=FG_GRAPHWIDGET_PARAMS.localesource
        }
    }
    jQuery("#fgWidgetHeader").attr("src",FG_GRAPHWIDGET.cdn+"/static/images/logo_header.png");
    jQuery("#fgResultsLoader img").attr("src",FG_GRAPHWIDGET.cdn+"/static/images/ajax-loader_new.gif");
    jQuery("#fgAdvancedFeaturesButton img").attr("src",FG_GRAPHWIDGET.cdn+"/static/images/gear_icon.png");
    jQuery("#fg-logo").css("background-image","url("+FG_GRAPHWIDGET.cdn+FG_GRAPHWIDGET.localeLogos[FG_GRAPHWIDGET.localesource]+")");
    if(FG_GRAPHWIDGET.localesource!==2){
        jQuery("#fgAdvancedControls").hide()
    }
    var c=jQuery("#fgNtee");
    jQuery.each(a.charityMap,function(d,e){
        c.append('<option value="'+d+'">'+e+"</option>")
    });
	jQuery.ajax({
		type:"get",
		url:a.pcEndpoint+"/graph/charity?mypostcode&callback=FG_GRAPHWIDGET.setZipcode",
		dataType:"jsonp",jsonp:"FG_GRAPHWIDGET.setZipcode"
	});
	jQuery(".fgResult").live("mouseover mouseout click",function(f){
		switch(f.type){
			case"mouseover":
				jQuery(this).addClass("highlightResult");
				break;
			case"mouseout":
				jQuery(this).removeClass("highlightResult");
				break;
			case"click":
				var d=jQuery(this).attr("id").replace("select_","");
				var g=jQuery.parseJSON(jQuery(this).find("#payload_"+d).text());
				var e=g.object_name;jQuery("#fg_selected_charity_uuid").val(d);
				jQuery("#fgNameStart").val(e);
				FG_GRAPHWIDGET._resEv(d,e,g.id_sigma);
				FG_GRAPHWIDGET.clearResults();
				break;
			default:
				break
		}
	});
	jQuery(".sToggle").click(function(){
		FG_GRAPHWIDGET.clearResults();
		jQuery("#fgNameStart").val("");
		jQuery("#sType").val(jQuery(this).val())});
		jQuery(".queryable").keyup(function(d){
			d.preventDefault();
			if((d.keyCode>=46&&d.keyCode<=90)||d.keyCode==8){
				a.submitSearch()
			}
		}
	);
	jQuery(".fgHinted").click(function(d){
		d.preventDefault();
		if(jQuery(this).hasClass("fgHinted")){
			jQuery(this).removeClass("fgHinted");
			jQuery(this).val("")
		}
	});
	jQuery("#fgAdvancedDToggle").click(function(){
		jQuery("#fgZipRadius").attr("disabled",!jQuery(this).attr("checked"));
		jQuery("#fgZip").attr("disabled",!jQuery(this).attr("checked"));
		FG_GRAPHWIDGET.submitSearch()
	});
	jQuery("#fgZip").keyup(function(g){
		g.preventDefault();
		var f=jQuery(this).val().length;
		if(f!=5&&f!=0){
			return false
		}
		if(f==0){
			FG_GRAPHWIDGET.submitSearch()
		}else{
			var d=jQuery(this);
			jQuery.ajax({
				type:"get",
				url:FG_GRAPHWIDGET.pcEndpoint+"/graph/charity?mypostcode&pc="+d.val()+"&callback=FG_GRAPHWIDGET.zipUpdateSubmit",
				dataType:"jsonp",
				jsonp:"FG_GRAPHWIDGET.zipUpdateSubmit",
				error:function(e){
					jQuery("#fgResults").html("Error")
				}
			})
		}
	});
	jQuery("#fgZipRadius").keyup(function(d){
		d.preventDefault();
		if(jQuery(this).val().length==0){return false}
		a.submitSearch()
	});
	jQuery("#fgNtee").change(function(d){
		d.preventDefault();
		FG_GRAPHWIDGET.submitSearch()
	});
	jQuery("#fgAdvancedControls").click(function(){
		jQuery("#fgAdvancedFeatures").toggle();
		jQuery("#fgAdvancedArrow").toggleClass("open")
	})
};
FG_GRAPHWIDGET.zipUpdateSubmit=function(a){
	FG_GRAPHWIDGET.setZipcode(a);
	this.submitSearch()
};
FG_GRAPHWIDGET.loadWidget=function(){
	if(undefined!=FG_GRAPHWIDGET_PARAMS){
		if(undefined!=FG_GRAPHWIDGET_PARAMS.cdn){FG_GRAPHWIDGET.cdn=FG_GRAPHWIDGET_PARAMS.cdn}
		if(undefined!=FG_GRAPHWIDGET_PARAMS.results){
			if(undefined!=FG_GRAPHWIDGET_PARAMS.results.selectaction){FG_GRAPHWIDGET.bindEvent("result",FG_GRAPHWIDGET_PARAMS.results.selectaction)}
		}
		if(undefined!=FG_GRAPHWIDGET_PARAMS.endpoint){FG_GRAPHWIDGET.endpoint=FG_GRAPHWIDGET_PARAMS.endpoint}
		if(undefined!=FG_GRAPHWIDGET_PARAMS.pcEndpoint){FG_GRAPHWIDGET.pcEndpoint=FG_GRAPHWIDGET_PARAMS.pcEndpoint}
	}
	if(undefined==FG_GRAPHWIDGET_PARAMS||undefined==FG_GRAPHWIDGET_PARAMS.nocss||(undefined!=FG_GRAPHWIDGET_PARAMS.nocss&&FG_GRAPHWIDGET_PARAMS.nocss==false)){
		var a=document.createElement("link");
		a.setAttribute("href",this.cdn+"/static/css/style.css");
		a.setAttribute("rel","stylesheet");
		a.setAttribute("type","text/css");
		document.getElementsByTagName("head")[0].appendChild(a)
	}
	jQuery.ajax({
		type:"get",
		url:FG_GRAPHWIDGET.cdn+"/static/partial.json",
		dataType:"jsonp",
		jsonp:"FG_GRAPHWIDGET.init",
		error:function(b){
			jQuery("#fgResults").html("Error")
		}
	})
};
if(typeof jQuery=="undefined"){
	var script=document.createElement("script");
	script.type="text/javascript";
	if(undefined==FG_GRAPHWIDGET_PARAMS||undefined==FG_GRAPHWIDGET_PARAMS.cdn){
		cdn=location.protocol+"//assets.firstgiving.com/graphwidget"
	}else{
		cdn=FG_GRAPHWIDGET_PARAMS.cdn
	}
	if(script.readyState){
		script.onreadystatechange=function(){
			if(script.readyState=="loaded"||script.readyState=="complete"){
				script.onreadystatechange=null;
				FG_GRAPHWIDGET.loadWidget()
			}
		}
	}else{
		script.onload=function(){
			FG_GRAPHWIDGET.loadWidget()
		}
	}
	script.src=cdn+"/static/js/jquery-1.6.2.min.js";
	document.body.appendChild(script)
}else{
	FG_GRAPHWIDGET.loadWidget()
};