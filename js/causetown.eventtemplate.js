var CAUSETOWN = CAUSETOWN || {}; // namespace
CAUSETOWN.CurrentServer = CAUSETOWN.CurrentServer || "/";
CAUSETOWN.EventForm = CAUSETOWN.EventForm || {};
CAUSETOWN.EventForm.formId = CAUSETOWN.EventForm.formId || "";
CAUSETOWN.EventForm.saveAsDraft = CAUSETOWN.EventForm.saveAsDraft || true;
CAUSETOWN.EventForm.draftButtonValue = CAUSETOWN.EventForm.draftButtonValue || "";
CAUSETOWN.EventForm.templateId = CAUSETOWN.EventForm.templateId || "";
CAUSETOWN.RegForm = CAUSETOWN.RegForm || {};
CAUSETOWN.RegForm.onOpenEdit = function(){
    window.location.hash = "#edit-registration";
};
CAUSETOWN.EventTemplate = CAUSETOWN.EventTemplate || {};
CAUSETOWN.EventTemplate._state = CAUSETOWN.EventTemplate._state || {
    isLoggedIn: false,
    isRegistered: false,
    completedRegistrationOnce: false,
    isEventCustomized: false,
    hasPremiums: false,
    eventSubmitButton: "Publish",
    intermediateButtonValue: "Publish",
    finalButtonValue: "Publish"
},
CAUSETOWN.EventTemplate.skipJoinButton = CAUSETOWN.EventTemplate.skipJoinButton || false;
CAUSETOWN.EventTemplate.scrollTopElement = CAUSETOWN.EventTemplate.scrollTopElement || $("#slide-display-position");
CAUSETOWN.EventTemplate.setRegformValid = function(regFormValid){
    if (regFormValid){
        CAUSETOWN.EventTemplate.scrollTopElement = $("#event-preview-position");
        CAUSETOWN.EventTemplate._state.isRegistered = true;
        CAUSETOWN.EventTemplate._state.completedRegistrationOnce = true;
        if (window.location.hash !== "#customize-event"){
            window.location.hash = "#customize-event";
        }
    } else {
        CAUSETOWN.EventTemplate._state.isRegistered = false;
        CAUSETOWN.EventTemplate.scrollTopElement = $("#slide-display-position");
        switch(window.location.hash){
            case "#customize-event":
            case "#customize-event-edit":
                window.location.hash = "#edit-registration";
                break;
            case "#join":
            case "#register":
            case "#login":
            case "#edit-registration":
                break;
            default:
                window.location.hash = "#register";
        }
    }
};
CAUSETOWN.EventTemplate.scrollTop = function(){
    CAUSETOWN.scrollTo(CAUSETOWN.EventTemplate.scrollTopElement);
};
CAUSETOWN.EventTemplate.setAddressData = function(addressData, noticeMessage){
    var wrap = $(".location-display"),
        warningWrap = wrap.find(".warning"),
        addressWrap = wrap.find(".address"),
        name = wrap.find(".business-name"),
        street = wrap.find(".street-address"),
        city = wrap.find(".city-state"),
        phone = wrap.find(".business-phone"),
        noticeElement = wrap.find(".alert.info"),
        formattedPhone,invalidBusinessFields,
        regFormAddressWrap = $("#address-display .address");
    if (addressData){
        if (addressData.Resetting){
            name.html("");
            street.html("");
            city.html("");
            phone.html("");
            noticeElement.html("");
        } else {
            formattedPhone = addressData.Phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1.$2.$3");
            name.html(addressData.BizName);
            street.html(addressData.Address);
            city.html(addressData.City + ", " + addressData.State + " " + addressData.Zip);
            phone.html(formattedPhone);
            if (noticeMessage){
                if (noticeMessage == "Logged In"){
                    CAUSETOWN.EventTemplate._state.isLoggedIn = true;
                } else {
                    noticeElement.html(noticeMessage);
                }
            }
            if (regFormAddressWrap && regFormAddressWrap.is(":visible")){
                // regform has its own address display
            } else {
                addressWrap.slideDown(function(){
                    $('html, body').animate({
                        scrollTop: wrap.offset().top
                    }, 500);
                });
            }
            warningWrap.slideUp();
        }
        if ($.isFunction(CAUSETOWN.EventForm.setAddressData)){
            CAUSETOWN.EventForm.setAddressData(addressData);
        }
        if (CAUSETOWN.EventTemplate._state.isLoggedIn){
            window.location.href = "#customize-event";
        }
    }
}

CAUSETOWN.EventForm.hideForm = function(){
    var form = $("#" + CAUSETOWN.EventForm.formId);
    form.children("div.hide-on-preview").hide();
    form.find("#action-buttons").show();
    form.find("#view-preview-button").hide();
    form.find(".submit-button-trigger").show();
    form.find(".hide-on-customize").show();
    form.show();
    CAUSETOWN.scrollTo($("#event-preview-position"));
};
CAUSETOWN.EventForm.showForm = function(){
    var form = $("#" + CAUSETOWN.EventForm.formId),
        previewButton = form.find("#view-preview-button");
    form.children("div.hide-on-preview").show();
    previewButton.attr("href", "#customize-event").removeClass("do-not-navigate").show();
    form.find(".hide-on-customize").hide();
    form.show();
    CAUSETOWN.scrollTo($("#event-preview-position"));
};
CAUSETOWN.EventForm.onLoadHandler = function(){
    var customizeButton = $("#customize-event-button");
    if (CAUSETOWN.EventForm.saveAsDraft){
        $("#save-as-draft-button").hide();
    }
    customizeButton.closest("div").hide();
    //customizeButton.closest(".btn").addClass("link").removeClass("btn");
}
CAUSETOWN.EventForm.onSubmitHandler = function(e){
    var formElement = $("#" + CAUSETOWN.EventForm.formId),
        submitButton = formElement.find("input[type=submit][clicked=true]")
    e.preventDefault();
    if (!CAUSETOWN.EventTemplate._state.hasPremiums || submitButton.val() == CAUSETOWN.EventTemplate._state.finalButtonValue){
        CAUSETOWN.EventTemplate.handleEventSubmission(e);
    } else {
        CAUSETOWN.EventTemplate._state.isEventCustomized = true;
        CAUSETOWN.EventTemplate.showPremiums();
    }
};
CAUSETOWN.EventTemplate.handleEventSubmission = function(e){
    var form = $("#" + CAUSETOWN.EventForm.formId),
        regForm = CAUSETOWN.RegForm.getForm ? CAUSETOWN.RegForm.getForm() : [],
        submitButtonWrap = form.find("input[type='submit']").closest(".submit-div"),
        submitButtonValue = form.find("input[type=submit][clicked=true]").val(),
        messageDiv = $("#action-message"),
        saveAsDraft = submitButtonValue === CAUSETOWN.EventForm.draftButtonValue,
        premiumChoice = form.find("input[name='premium_level']:checked"),
        record = {
            "id": form.find("input[name='id']").val(),
            "saveAsDraft": saveAsDraft,
            "templateId": CAUSETOWN.EventForm.templateId,
            "title": form.find("input[name='title']").val(),
            "description": form.find("textarea[name='description']").val(),
            "start": form.find("input[name='startDate']").val() + " " + form.find("select[name='startTime']").val(),
            "end": form.find("input[name='endDate']").val() + " " + form.find("select[name='endTime']").val(),
            "featuredCausePercentage": form.find("select[name='featuredCausePercentage']").val(),
            "percentage": form.find("select[name='customerChoicePercentage']").val(),
            "isDraft": false,
            "isFlatDonation": form.find("input[name='isFlatDonation']").val(),
            "donationPerUnit": form.find("input[name='donationPerUnit']").val(),
            "unitDescriptor": form.find("input[name='unitDescriptor']").val(),
            "donationActionPhrase": form.find("input[name='donationActionPhrase']").val(),
            "orgId": form.find("input#orgId").val(),
            "orgEin": form.find("input#orgEin").val(),
            "orgName": form.find("input#orgName").val(),
            "eventTemplatePremiumId" : (premiumChoice.length) ? premiumChoice.val() : 0,
            "isOnlineEvent" : form.find("input[name='isOnlineEvent']:checked").val(),
            "isNationalEvent" :form.find("input[name='isNationalEvent']:checked").length ? 1 : 0,
            "onlineLocationLink": form.find("input[name='onlineLocationLink']").val(),
            "onlineLocationLabel": form.find("input[name='onlineLocationLabel']").val()
        },
        submitUrl = CAUSETOWN.CurrentServer + "ApiEvents",
        successProcess = function(data){
            var successURL,
                isDraft,
                successes = $.map(data.response, function(item, index){
                    if (item.success){
                        return item;
                    }
                }),
                response, record,
                registrationSuccessDiv = $("#registration-success");
            if (successes.length){
                response = successes[0];
                successURL = response.createSuccessURL;
                isDraft = response.saveAsDraft;
                record = response.record;
                if (successURL && window.location.href !== successURL){
                    if (isDraft){
                        window.location.href = successURL + "#new";
                    } else {
                        window.location.href = successURL;
                    }
                } else {
                    $("body").hide(0);
                    $(".template-process").hide();
                    $.ajax({
                        type: 'POST',
                        url: CAUSETOWN.CurrentServer + 'RenderEventBrief',
                        data: JSON.stringify([record]),
                        success: function(data){
                            var container = registrationSuccessDiv.find("#draft-event-list");
                            $("body").fadeIn(100);
                            container.html(data);
                            registrationSuccessDiv.find("li.draft").removeClass("draft");
                            container.find(".eleven.columns").removeClass("eleven").addClass("fourteen");
                            registrationSuccessDiv.show(300,function(){
                                CAUSETOWN.scrollTo($("#registration-success"));
                            });
                        }
                    });
                }
            } else {
                // no successes?
                if (data.response.length && data.response[0].message){
                    alertMessage = data.response[0].message;
                    messageDiv.html(alertMessage);
                } else {
                    alert("An unexpected error occurred. Please contact us if this problem continues to occur.");
                }
                $("#submit-button-wait").remove();
                submitButtonWrap.show();
            }
        },
        failProcess = function(jqXHR, textStatus, errorThrown){
            var data = $.parseJSON(jqXHR.responseText),
                waitMessage = $("#submit-button-wait");

            waitMessage.remove();
            submitButtonWrap.show();
            if (CAUSETOWN.EventForm.onSubmitFailHandler && $.isFunction(CAUSETOWN.EventForm.onSubmitFailHandler)){
                CAUSETOWN.EventForm.onSubmitFailHandler(jqXHR, textStatus, errorThrown);
            }
        },
        submitEvent = function(){
            if ($("input[name='registered_member_id']")){
                record.registered_member_id = $("input[name='registered_member_id']").val();
            }
            $.ajax({
                url: submitUrl,
                type: "POST",
                data: JSON.stringify([record]),
                dataType: "json",
                contentType: "application/json",
                statusCode:{
                    200: successProcess,
                    201: successProcess,
                    400: failProcess,
                    409: failProcess
                }
            });
        };
    e.preventDefault();
    messageDiv.html('').removeClass('info');
    submitButtonWrap.hide();
    $("#submitting-message").remove(); // this is the one on the regform
    submitButtonWrap.after("<span id='submit-button-wait' class='alert info'>Processing . . .</span>");

    if (!regForm || regForm.length === 0){
        // no registration
        CAUSETOWN.EventTemplate._state.isEventCustomized = true;
        submitEvent();
    } else {
        // define the callback function for the regform success callback
        CAUSETOWN.EventTemplate.regFormSuccessCallback = function(){
            CAUSETOWN.EventTemplate._state.isEventCustomized = true;
            submitEvent();
        }
        // now submit the regform
        if (regForm.valid && regForm.valid()){
            regForm.submit();
        }
    }
};
CAUSETOWN.EventTemplate.clickReturnToView = function(){
    var formElement = $("#" + CAUSETOWN.EventForm.formId),
        instructions = $(".instructions"),
        joinButton = $("#join-event-button");
    joinButton.closest("div").show();
    instructions.show();
    $("#location-data-display").hide();
    $("#customize-event-button").closest("div").hide();
    formElement.hide();
    $("#register-or-login").hide();
};
CAUSETOWN.EventTemplate.clickJoinButton = function(){
    var formElement = $("#" + CAUSETOWN.EventForm.formId),
        instructions = $(".instructions"),
        joinButton = $("#join-event-button"),
        loginBanner = $("#register-or-login");
    joinButton.closest("div").hide();
    instructions.hide();
    $("#location-data-display").show();
    if (CAUSETOWN.EventTemplate._state.isLoggedIn){
        $("#location-data-display").hide();
        $("#customize-event-button").closest("div").show();
        formElement.show();
        CAUSETOWN.EventForm.hideForm();
        window.location.hash = "#customize-event";
    } else if (CAUSETOWN.EventTemplate._state.completedRegistrationOnce){

    } else {
        loginBanner.show();
        CAUSETOWN.scrollTo($("#slide-display-position"));
        formElement.hide();
        $(".action-buttons").hide();
        $("#customize-event-button").closest("div").hide();
        CAUSETOWN.EventTemplate.setRegformValid(false);
    }
};
CAUSETOWN.EventTemplate.clickRegisterButton = function(){
    var registerButton = $(".register-action"),
        formElement = $("#" + CAUSETOWN.EventForm.formId),
        instructions = $(".instructions"),
        joinButton = $("#join-event-button");
    if (CAUSETOWN.EventTemplate._state.isLoggedIn || CAUSETOWN.EventTemplate._state.isRegistered){
        joinButton.closest("div").hide();
        instructions.hide();
        $("#customize-event-button").closest("div").show();
        if (CAUSETOWN.EventTemplate._state.isLoggedIn){
            $("#location-data-display").hide();
            formElement.show();
            CAUSETOWN.EventForm.hideForm();
            if (!CAUSETOWN.EventTemplate._state.isEventCustomized){
                window.location.hash = "#customize-event";
            }
        } else {
            $("#location-data-display").show();
        }
    } else {
        if (!registerButton.is(":visible")){
            CAUSETOWN.EventTemplate.clickJoinButton();
        }
        registerButton.trigger("click");
        $("#register-or-login").hide();
        $(".action-buttons").hide();
        $("#customize-event-button").closest("div").hide();
        setTimeout(function(){
            $("#cancel-registration").attr("href", "#join");
        }, 1000);
    }
};
CAUSETOWN.EventTemplate.clickOpenLoginButton = function(){
    var loginButton = $(".login-action");
    if (!loginButton.is(":visible")){
        CAUSETOWN.EventTemplate.clickJoinButton();
    }
    $("#register-or-login").hide();
    var e = $.Event("click");
    e.target = loginButton;
};
CAUSETOWN.EventTemplate.cancelRegistration = function(){
    CAUSETOWN.EventTemplate._state.isRegistered = false;
    CAUSETOWN.EventTemplate._state.completedRegistrationOnce = false;
    window.location.hash = "#join";
};
CAUSETOWN.EventTemplate.openManualEdit = function(){
    var formElement = $("#" + CAUSETOWN.EventForm.formId),
        registerButton = $(".register-action"),
        contactSampleField = $("#company_contactfirstname");
    $("#register-or-login").hide();
    $("#customize-event-button").closest("div").hide();
    formElement.hide();
    if (CAUSETOWN.EventTemplate._state.isLoggedIn){

    } else if (CAUSETOWN.EventTemplate._state.completedRegistrationOnce){
        $("location-data-display").show();
    } else {
        if (!contactSampleField.is(":visible")){
            if (!registerButton.is(":visible")){
                CAUSETOWN.EventTemplate.clickJoinButton();
                registerButton.trigger("click");
                $("#register-or-login").hide();
                setTimeout(function(){
                    $("#cancel-registration").attr("href", "#join");
                    $("#no-can-find").trigger("click");
                }, 1000);
            } else {
                $("#no-can-find").trigger("click");
            }
        }
    }
};
CAUSETOWN.EventTemplate.customizeEvent = function(){
    var formElement = $("#" + CAUSETOWN.EventForm.formId),
        loginBanner = $("#register-or-login"),
        submitButton = formElement.find("input[type=submit][clicked=true]");

    $(".premium-pane").slideUp();
    $("#register-or-login").hide();
    if (CAUSETOWN.EventTemplate._state.isLoggedIn || CAUSETOWN.EventTemplate._state.completedRegistrationOnce){
        $("#join-event-button").closest("div").hide();
        $(".instructions").hide();
        if (CAUSETOWN.EventTemplate._state.isLoggedIn){
            $("#location-data-display").hide();
        } else {
            $("#location-data-display").show();
        }
        $("#customize-event-button").closest("div").show();
        formElement.show();
        submitButton.val(CAUSETOWN.EventTemplate._state.intermediateButtonValue);
        $("#event-preview-panel").show();
        CAUSETOWN.EventForm.hideForm();
        $(".action-buttons").show();
        CAUSETOWN.scrollTo($("#event-preview-position"));
    } else {
        loginBanner.show();
        CAUSETOWN.scrollTo($("#slide-display-position"));
        window.location.hash = "#join";
    }
};
CAUSETOWN.EventForm.customizeEvent = function(){
    if (CAUSETOWN.EventTemplate._state.completedRegistrationOnce){
        $("#location-data-display").show();
    }
    CAUSETOWN.EventForm.showForm();
    $(".premium-pane").slideUp();

    $("textarea").each(function(i,el){
        var element = $(el);
        element.height(0);
        $(el).height( el.scrollHeight );
    });
    $("#event-preview-panel").hide();
    window.location.hash = "#customize-event-edit";
};
CAUSETOWN.EventTemplate.customizeEventEdit = function(){
    $("#register-or-login").hide();
    CAUSETOWN.EventTemplate.customizeEvent();
    if (window.location.hash == "#customize-event-edit"){
        CAUSETOWN.EventForm.customizeEvent();
    }
};
CAUSETOWN.EventTemplate.showPremiums = function(){
    var form = $("#" + CAUSETOWN.EventForm.formId),
        submitButton = form.find("input[type=submit][clicked=true]");
    $("#register-or-login").hide();
    if (CAUSETOWN.EventTemplate._state.isEventCustomized){
        $("#location-data-display").hide();
        $("#event-preview-panel").hide();
        CAUSETOWN.EventForm.hideForm();
        $(".premium-pane").show();
        submitButton.val(CAUSETOWN.EventTemplate._state.finalButtonValue);
        CAUSETOWN.EventTemplate.scrollTop();
        window.location.hash = "#premiums";
    } else {
        window.location.hash = "#join";
    }
};
CAUSETOWN.EventTemplate.hidePremiums = function(){
    var form = $("#" + CAUSETOWN.EventForm.formId),
        submitButton = form.find("input[type=submit][clicked=true]");
    $(".premium-pane").slideUp();
    CAUSETOWN.EventForm.customizeEvent();
    $("#event-preview-panel").show();
    CAUSETOWN.EventForm.hideForm();
    submitButton.val(CAUSETOWN.EventTemplate._state.intermediateButtonValue);
    CAUSETOWN.EventTemplate.scrollTop();
};
CAUSETOWN.EventTemplate._handleHash = function(hash){
    switch(hash){
        case "#join" :
            CAUSETOWN.EventTemplate.clickJoinButton();
            break;
        case "#register" :
            CAUSETOWN.EventTemplate.clickRegisterButton();
            break;
        case "#login" :
            CAUSETOWN.EventTemplate.clickOpenLoginButton();
            break;
        case "#edit-registration":
            CAUSETOWN.EventTemplate.openManualEdit();
            break;
        case "#customize-event":
            CAUSETOWN.EventTemplate.customizeEvent();
            break;
        case "#customize-event-edit":
            CAUSETOWN.EventTemplate.customizeEventEdit();
            break;
        case "#premiums":
            CAUSETOWN.EventTemplate.showPremiums();
            break;
        default:
            CAUSETOWN.EventTemplate.clickReturnToView();
        // do nothing
    }
}

$(function(){
    var eventPreviewPanel = $("#event-preview-panel");

    // some surgery to the event form for the event template page
    eventPreviewPanel.find("h3").hide();
    $("#location-data-display").hide();
    $("#location-data-display").find("fieldset.address").remove();

    // hack to get the text value of the clicked submit button
    $("form input[type=submit]").click(function() {
        $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });

    // message to tell user they need to login or register first
    $("#accept-terms-wrap").click(function(){
        if ($("#accept-terms").prop("disabled")){
            alert("You must log in or complete your registration in order to proceed.");
        }
    });

    $("#back-to-customize").click(function(e){
        CAUSETOWN.EventTemplate.hidePremiums();
    });
});

$(window).load(function(){
    var hash = window.location.hash;
    if (CAUSETOWN.EventTemplate.skipJoinButton){
        CAUSETOWN.EventTemplate.clickJoinButton();
    }

    // handle hash
    CAUSETOWN.EventTemplate._handleHash(hash);
    $(window).bind('hashchange', function() {
        hash = window.location.hash;
        CAUSETOWN.EventTemplate._handleHash(hash);
    });

    // hack to make the submit button always enabled
    $("#create-event-form").find(".submit-button-trigger").addClass("enabled");
    if ($.isFunction(CAUSETOWN.EventForm.showSubmitButton)){
        CAUSETOWN.EventForm.showSubmitButton();
    }
});