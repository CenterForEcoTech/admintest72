CAUSETOWN = CAUSETOWN || {};
CAUSETOWN.FavoriteBusinesses = function(){
    var defaults = {
            apiUrl : "",
            commentSelector : "",
            searchLocationSelector : "",
            searchNameSelector : "",
            submitSelector : "",
            resetDisplayData : function(){},
            resetFormData : function(){}
        },
        settings = defaults,
        variables = {
            commentElement : $(settings.commentSelector),
            searchLocationElement : $(settings.searchLocationSelector),
            searchNameElement : $(settings.searchNameSelector),
            submitButton : $(settings.submitSelector),
            dataRecord : null
        },
        methods = {
            init: function(options, reset){
                methods.removeHandlers();
                if (reset){
                    settings = $.extend({}, defaults, options);
                } else {
                    $.extend(settings, options);
                }
                methods.initVars();
                methods.initHandlers();
                methods.resetDisplayData();
            },
            initVars: function(){
                variables.commentElement = $(settings.commentSelector);
                variables.searchLocationElement = $(settings.searchLocationSelector);
                variables.searchNameElement = $(settings.searchNameSelector);
                variables.submitButton = $(settings.submitSelector);
                variables.dataRecord = null;
            },
            removeHandlers: function(){
                if (variables.submitButton.length){
                    variables.submitButton.unbind('click.favoriteBusinesses');
                }
            },
            initHandlers: function(){
                if (variables.submitButton.length){
                    variables.submitButton.bind('click.favoriteBusinesses', function(){
                        var dataRecord = variables.dataRecord || {};

                        if (dataRecord){
                            dataRecord.action = "add_business";
                            if (variables.commentElement.length && variables.commentElement.val){
                                dataRecord.comments = variables.commentElement.val();
                            }
                            if (variables.searchLocationElement.length && variables.searchLocationElement.val){
                                dataRecord.searchLocation = variables.searchLocationElement.val();
                            }
                            if (variables.searchNameElement.length && variables.searchNameElement.val){
                                dataRecord.searchName = variables.searchNameElement.val();
                            }
                            $.ajax({
                                url : settings.apiUrl,
                                type : "POST",
                                data : $.param(dataRecord),
                                success : function(data){
                                    methods.resetDisplayData();
                                }
                            });
                        }
                    });
                }
            },
            resetDisplayData : function(){
                methods.setData(null);
                if ($.isFunction(settings.resetFormData)){
                    settings.resetFormData();
                }
                if ($.isFunction(settings.resetDisplayData)){
                    settings.resetDisplayData();
                }
            },
            setData: function(data){
                variables.dataRecord = data;
                if (data){
                    variables.submitButton.removeClass("inactive");
                    variables.submitButton.prop("disabled", false);
                } else {
                    variables.submitButton.addClass("inactive");
                    variables.submitButton.prop("disabled", true);
                }
            },
            deleteBusiness: function(businessId, success){
                if (businessId){
                    $.ajax({
                        url : settings.apiUrl,
                        type : "POST",
                        data : {
                            businessId : businessId,
                            action : "delete_business"
                        },
                        success : function(data){
                            if ($.isFunction(success)){
                                success(data);
                            }
                        }
                    });
                }
            }
        };

    return {
        init: function(options, reset){
            methods.init(options, reset);
        },
        apply: function(options){
            methods.init(options);
        },
        deleteBusiness: function(businessId, success){
            methods.deleteBusiness(businessId, success);
        },
        setData: function(data){
            methods.setData(data);
        }
    }
}();