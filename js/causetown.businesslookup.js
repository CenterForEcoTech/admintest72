var CAUSETOWN = CAUSETOWN || {};
CAUSETOWN.BusinessLookup = function(){
    var defaults = {
            locationSelector : "",
            defaultLocation : "",
            nameSelector : "",
            apiUrl : "",
            beforeSearch : function(){},
            onSelect : function(event, ui){}
        },
        settings = defaults,
        variables = {
            nameElement : $(settings.nameSelector),
            locationElement : $(settings.locationSelector)
        },
        methods = {
            init: function(options, reset){
                methods.removeHandlers();
                if (reset){
                    settings = $.extend({}, defaults, options);
                } else {
                    $.extend(settings, options);
                }
                methods.initVars();
                methods.initHandlers();
            },
            initVars: function(){
                variables.nameElement = $(settings.nameSelector);
                variables.locationElement = $(settings.locationSelector);
            },
            removeHandlers: function(){
                if (variables.nameElement.length){
                    variables.nameElement.autocomplete( "destroy" );
                    variables.nameElement.removeClass("auto-complete");
                }
            },
            initHandlers: function(){
                var uiFrontElement;
                if (variables.nameElement.length){
                    variables.nameElement.addClass("auto-complete");
                    variables.nameElement.autocomplete({
                        source: function(request, response) {
                            if ($.isFunction(settings.beforeSearch)){
                                settings.beforeSearch();
                            }
                            if (settings.apiUrl){
                                $.ajax({
                                    url: settings.apiUrl,
                                    dataType: "json",
                                    data: {
                                        term : request.term,
                                        bizlocation : variables.locationElement.val()
                                    },
                                    success: function(data) {
                                        response( $.map( data, function( item ) {
                                            item.label = item.BizName + " - " + item.Address;
                                            item.value = item.BizName;
                                            return item;
                                        }));
                                    }
                                });
                            } else {
                                alert("component is not properly initialized");
                            }
                        },
                        minLength: 2,
                        delay: 500,
                        select: function(event, ui) {
                            if ($.isFunction(settings.onSelect)){
                                settings.onSelect(event, ui);
                            }
                        }
                    });

                    // newer versions of jquery-ui do this automatically
                    uiFrontElement = variables.nameElement.closest(".ui-front");
                    if (uiFrontElement.length){
                        variables.nameElement.autocomplete( "option", "appendTo", uiFrontElement );
                    }
                }
            },
            reset : function(){
                if (variables.nameElement.length && variables.nameElement.val){
                    variables.nameElement.val("");
                }
                if (variables.locationElement.length && variables.locationElement.val){
                    variables.locationElement.val(settings.defaultLocation);
                }
            }
        };

    return {
        init: function(options, reset){
            methods.init(options, reset);
        },
        apply: function(options){
            methods.init(options);
        },
        reset: function(){
            methods.reset();
        }
    }
}();