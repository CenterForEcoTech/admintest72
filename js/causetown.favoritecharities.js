CAUSETOWN = CAUSETOWN || {};
CAUSETOWN.FavoriteCharities = function(){
    var defaults = {
            apiUrl : "",
            charityLookupUrl : "",
            resetDisplayData : function(){},
            formSelector : "#charity-form",
            nameSelector : "#charity-name",
            einSelector : "#charity-ein",
            idSelector : "#charity-id",
            submitSelector : "#add-charity"
        },
        settings = defaults,
        variables = {
            formElement : $(settings.formSelector),
            nameElement : $(settings.nameSelector),
            einElement : $(settings.einSelector),
            idElement : $(settings.idSelector),
            submitButton : $(settings.submitSelector)
        },
        methods = {
            init: function(options, reset){
                methods.removeHandlers();
                if (reset){
                    settings = $.extend({}, defaults, options);
                } else {
                    $.extend(settings, options);
                }
                methods.initVars();
                methods.initHandlers();
                methods.resetDisplayData();
            },
            initVars: function(){
                variables.formElement = $(settings.formSelector);
                variables.nameElement = $(settings.nameSelector);
                variables.einElement = $(settings.einSelector);
                variables.idElement = $(settings.idSelector);
                variables.submitButton = $(settings.submitSelector);
            },
            removeHandlers: function(){
                if (variables.submitButton.length){
                    variables.submitButton.unbind('click.favoriteCharities');
                }
                if (variables.nameElement.length){
                    variables.nameElement.autocomplete( "destroy" );
                }
            },
            initHandlers: function(){
                if (variables.submitButton.length){
                    variables.submitButton.bind('click.favoriteCharities', function(){
                        if (variables.idElement.val()){
                            $.ajax({
                                url : settings.apiUrl,
                                type : "POST",
                                data : variables.formElement.serialize(),
                                success : function(data){
                                    methods.resetDisplayData();
                                }
                            });
                        }
                    });
                }

                if (variables.nameElement.length){
                    variables.nameElement.autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: settings.charityLookupUrl,
                                dataType: "json",
                                data: {
                                    term : request.term
                                },
                                success: function(data) {
                                    response( $.map( data, function( item ) {
                                        return {
                                            label: item.OrgName + " - " + item.Address + " " + item.City +" "+ item.State,
                                            value: item.OrgName,
                                            OrgID: item.OrgID,
                                            OrgEIN: item.OrgEIN
                                        }
                                    }));
                                }
                            });
                        },
                        minLength: 2,
                        delay: 500,
                        select: function(event, ui) {
                            methods.setValues(ui.item.OrgID, ui.item.OrgEIN, ui.item.OrgName);
                        },
                        change: function(event, ui){
                            if (ui.item){
                                methods.setValues(ui.item.OrgID, ui.item.OrgEIN, ui.item.OrgName);
                            } else {
                                methods.setValues("","","");
                            }
                        }
                    });
                }
            },
            setValues: function(orgID, orgEIN, orgName){
                variables.idElement.val(orgID);
                variables.einElement.val(orgEIN);
                variables.nameElement.val(orgName);
                if (variables.submitButton.length){
                    if (orgID || orgEIN){
                        variables.submitButton.removeClass("inactive");
                        variables.submitButton.prop("disabled", false);
                    } else {
                        variables.submitButton.addClass("inactive");
                        variables.submitButton.prop("disabled", true);
                    }
                }
            },
            resetDisplayData : function(){
                methods.setValues("","","");
                if ($.isFunction(settings.resetDisplayData)){
                    settings.resetDisplayData();
                }
            },
            deleteCharity: function(orgId, success){
                if (orgId){
                    $.ajax({
                        url : settings.apiUrl,
                        type : "POST",
                        data : {
                            orgId : orgId,
                            action : "delete_charity"
                        },
                        success : function(data){
                            if ($.isFunction(success)){
                                success(data);
                            }
                        }
                    });
                }
            }
        };

    return {
        init: function(options, reset){
            methods.init(options, reset);
        },
        apply: function(options){
            methods.init(options);
        },
        deleteCharity: function(orgID, success){
            methods.deleteCharity(orgID, success);
        }
    }
}();