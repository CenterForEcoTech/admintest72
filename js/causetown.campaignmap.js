var CAUSETOWN = CAUSETOWN || {};

CAUSETOWN.MapMarkerData = function(lat, lng, title, popup, customPin){
    this.lat = lat;
    this.lng = lng;
    this.title = title;
    this.popup = popup;
    this.customPin = customPin;
};

CAUSETOWN.LeafletCampaignMapUtil = function(options, normalPin, spiderPin){
    var defaults = {
            center: new L.LatLng(39.8282, -95.5795),
            minZoom: 4,
            zoom: 4,
            scrollWheelZoom: false,
            layers: []
        },
        settings = $.extend({}, defaults, options),
        map = undefined,
        rawDataArray = [],
        processedDataArray = [],
        // set up pin handler
        markerGroup,
        pinHandler = {
            reset: function(){
                if (markerGroup){
                    markerGroup.clearLayers();
                    if (spideringDataListener){
                        spideringDataListener.reset();
                    }
                }
            },
            add: function(mapMarkerData){
                var markerPin = (mapMarkerData.customPin) ? mapMarkerData.customPin : normalPin,
                    marker = new L.Marker(new L.latLng(mapMarkerData.lat, mapMarkerData.lng), {search: mapMarkerData.title, icon: markerPin} );
                marker.bindPopup(mapMarkerData.popup);

                if (!markerGroup){
                    markerGroup = new L.MarkerClusterGroup({ disableClusteringAtZoom: 14,maxClusterRadius:30 });
                    markerGroup.on({
                        'mouseover': function(e) {
                            var layer = e.layer;
                            layer.closePopup();
                            layer.openPopup();
                        }
                    });
                    markerGroup.on({
                        'click': function(e) {
                            var layer = e.layer;
                            var latlng = new L.LatLng(layer._latlng.lat, layer._latlng.lng);
                            layer._map.setView(latlng,18,false);
                        }
                    });
                    map.addLayer(markerGroup);
                }
                markerGroup.addLayer(marker);
                if (spideringDataListener){
                    spideringDataListener.add(marker, markerPin);
                }
            }
        },

        // set up spidering handler
        oms,
        spideringDataListener = {
            reset:  function(){
                if (oms){
                    oms.clearMarkers();
                }
            },
            add: function(marker, markerPin){
                if (!oms){
                    oms = new OverlappingMarkerSpiderfier(map);
                    oms.addListener('spiderfy', function(markers) {
                        for (var i = 0, len = markers.length; i < len; i ++) {
                            //markers[i].setIcon(spiderPin);
                        }
                        map.closePopup();
                        var latlng = new L.LatLng(markers[0]._latlng.lat, markers[0]._latlng.lng);
                        map.setView(latlng,18,false);
                    });
                    oms.addListener('unspiderfy', function(markers) {
                        for (var i = 0, len = markers.length; i < len; i ++) markers[i].setIcon(markerPin);
                    });
                }
                oms.addMarker(marker);
            }
        },
        mapListeners = [pinHandler],
        resetData = function(){
            processedDataArray = [];
            $.each(mapListeners, function(index, listener){
                if (listener.reset && $.isFunction(listener.reset)){
                    listener.reset();
                }
            });
        },
        addData = function(mapMarkerData){
            processedDataArray.push(mapMarkerData);
            $.each(mapListeners, function(index, listener){
                if (listener.add && $.isFunction(listener.add)){
                    listener.add(mapMarkerData);
                }
            });
        },
        processData = function(conversionFunction){
            if ($.isFunction(conversionFunction)){
                $.each(rawDataArray, function(index, item){
                    var mapMarkerData = conversionFunction(item);
                    if (mapMarkerData instanceof CAUSETOWN.MapMarkerData){
                        addData(mapMarkerData);
                    }
                });
            }
        };

    return {
        createMap: function(elementId, baseLayer){
            settings.layers.push(baseLayer);
            map = L.map(elementId, settings);
            return this;
        },
        loadRecords: function(data, conversionFunction){
            if (data && data.length){
                resetData();
                rawDataArray = data;
                processData(conversionFunction);
            }
            return this;
        },
        getMetadata: function(){
            return {
                zoom: map.getZoom(),
                center: map.getCenter()
            }
        }
    };
};

CAUSETOWN.PinIconFactory = function(shadowUrl){
    var settings = {
            shadowUrl: shadowUrl,
            iconSize:     [12, 20],
            shadowSize:   [22, 20],
            iconAnchor:   [6,20],
            shadowAnchor: [6,20],
            popupAnchor:  [0,-20]
        },
        PinIcon = L.Icon.extend({
            options: settings
        });

    return {
        create: function(iconUrl){
            return new PinIcon({iconUrl: iconUrl});
        }
    };
};


