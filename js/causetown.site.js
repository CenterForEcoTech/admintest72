if (top === self){
    $("#header_nav ul").fadeIn();
}

/*
 ================================================== -->
 <script src="<?php echo $CurrentServer;?>js/jquery-ui-1.8.20.custom.min.js"></script>
 <script src="<?php echo $CurrentServer;?>js/jquery.ui.timepicker.js"></script>
 <script src="<?php echo $CurrentServer;?>mobile/javascripts/tabs.js"></script>
 <script src="<?php echo $CurrentServer;?>js/jquery.watermark.min.js"></script>
 <script src="<?php echo $CurrentServer;?>js/jquery.colorbox-min.js"></script>
 <script src="<?php echo $CurrentServer;?>js/jquery.validate.js" type="text/javascript" language="javascript"></script>

 <script src="<?php echo $CurrentServer;?>js/jquery.ui.touch-punch.min.js"></script>
*/

var CAUSETOWN = CAUSETOWN || {}; // declare namespace
CAUSETOWN.renderWide = CAUSETOWN.renderWide || false;
CAUSETOWN._isEmbedded = self !== top;

// remove hash cleans up the "#" when setting window.location.hash to empty string
CAUSETOWN.removeHash = function() {
    var scrollV, scrollH, loc = window.location;
    if ("pushState" in history)
        history.pushState("", document.title, loc.pathname + loc.search);
    else {
        // Prevent scrolling by storing the page's current scroll offset
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;

        loc.hash = "";

        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
}

CAUSETOWN._scrollTimeout = undefined;
CAUSETOWN.scrollTo = function(element, delay){
    if (CAUSETOWN._scrollTimeout){
        clearTimeout(CAUSETOWN._scrollTimeout);
    }
    if (!$.isNumeric(delay)){
        delay = 200;
    }
    CAUSETOWN._scrollTimeout = setTimeout(function(){
        //alert("scrolling to " + $(element).attr("id"))
        $('html, body').animate({
            scrollTop: $(element).offset().top - 20
        }, 500);
    }, delay);
};

$('#widget').draggable(); // jquery.ui.touch-punch - to allow slider to work in mobile
CAUSETOWN.colorbox = $.colorbox; // declared globally so modals can be closed by code loaded within them
CAUSETOWN.handleHash = CAUSETOWN.handleHash || {}; // allow custom handler set by page

jQuery.fn.highlight = function() {
    $(this).each(function() {
        var el = $(this),
            wrap = $("<div/>");
        el.before(wrap)
        wrap
            .width(el.innerWidth())
            .height(el.innerHeight())
            .css({
                "position": "absolute",
                "background-color": "#ffff99",
                "opacity": ".7"
            })
            .fadeOut(1500, function(){
                wrap.remove();
            });
    });
}
CAUSETOWN.forms = CAUSETOWN.forms || {};
CAUSETOWN.forms.placeError = function(message, jqueryElement){
    var labelContainer = jqueryElement.closest("label"),
        divContainer = jqueryElement.closest("div.paired.fields"),
        labelAlertElement = labelContainer.find(".alert"),
        divAlertElement = divContainer.find(".alert"),
        errorElement;
    if (message instanceof jQuery){
        errorElement = message;
    } else {
        errorElement = $("<span class='error'>"+message+"</span>");
    }
    if (labelAlertElement.length){
        labelAlertElement.html("");
        errorElement.appendTo(labelAlertElement);
    } else if (divAlertElement.length) {
        divAlertElement.html("");
        errorElement.appendTo(divAlertElement);
    } else {
        errorElement.appendTo(jqueryElement);
    }
    jqueryElement.highlight();
}
CAUSETOWN.forms.handleStandardErrors = function(container, errorObjectArray){
    var form = container,
        errorArray = errorObjectArray,
        fieldName, message, fieldElement;
    $.each(errorArray, function(index, error){
        if (error.name && error.message){
            fieldName = error.name;
            message = error.message;
            fieldElement = form.find("input[name='"+fieldName+"'],select[name='"+fieldName+"'],textarea[name='"+fieldName+"']");
            if (fieldElement.length){
                CAUSETOWN.forms.placeError(message, fieldElement);
            }
        }
    });
}

// jquery.validate customizations
$.validator.setDefaults({
    errorPlacement: CAUSETOWN.forms.placeError,
    focusCleanup: false
});
// document onload for all pages
$(function(){
    // modals
    $('body').on('click', '.colorbox', function(event){
        var options = {
            opacity: 0.8,
            href: $(this).attr("href"),
            onComplete: function(){
                window.setTimeout('$.colorbox.resize()',10);
            }
        };
        if (CAUSETOWN._isEmbedded){
            options.top = event.pageY - 200;
        }
        $.colorbox(options);
        event.preventDefault();
    }); // default
    $('body').on('click', '.colorbox-inline', function(event){
        var options = {
            inline:true,
            opacity: 0.8,
            href: $(this).attr("href"),
            onComplete: function(){
                window.setTimeout('$.colorbox.resize()',10);
            }
        };
        if (CAUSETOWN._isEmbedded){
            options.top = event.pageY - 200;
        }
        $.colorbox(options);
        event.preventDefault();
    }); // inline
    $("body").on('click', '.colorbox-inline-delay', function(event){
        var link = $(this),
            targetId = link.attr("href"),
            target = $(targetId),
            options = {
                inline:true,
                opacity: 0.8,
                href: link.attr("href"),
                onComplete: function(){
                    window.setTimeout('$.colorbox.resize()',10);
                }
            };

        if (CAUSETOWN._isEmbedded){
            options.top = event.pageY - 200;
        }
        event.preventDefault();
        if (target.length){
            event.stopPropagation();
            // target visible
            $.colorbox(options);
        } else {
            // target not visible
            setTimeout(function(){
                var targetId = link.attr("href"),
                    target = $(targetId);
                if (target.length){
                    $.colorbox(options);
                }
            }, 200);
        }
    });
    $('body').on('click', '.img-container .colorbox-inline-2, #catalog-container .box .colorbox-inline-2', function(event){
        var self = $(this),
            responsiveParent = self.closest(".columns"),
            actualResponsiveWidth = responsiveParent.width(),
            responsiveWidth = actualResponsiveWidth < 700 ? actualResponsiveWidth : 700,
            responsiveHeight = responsiveParent.height(),
            width = responsiveWidth > 300 ? responsiveWidth - 40 : responsiveWidth,
            height = responsiveHeight < 400 && responsiveWidth <= 300 ? responsiveHeight : false,
            options = {
                inline:true,
                opacity: 0.8,
                href: $(this).attr("href"),
                width: (width) ? width : false,
                height: false,
                onComplete: function(){
                    if (width && height){
                        // don't resize if both dimensions are set
                    } else {
                        window.setTimeout('$.colorbox.resize()',10);
                    }

                    $("#cboxContent").find("a[href^=http]").each(function(){
                        $(this).attr({
                            target: "img_link",
                            title: "Opens in a new window"
                        });
                    });
                }
            };
        if (CAUSETOWN._isEmbedded){
            options.top = event.pageY - 100;
        }
        $.colorbox(options);
        event.preventDefault();
    }); // inline
    // delegate for ajax content
    $('body').on('click', '.colorbox-ajax', function(event){
        $.colorbox({
            opacity: 0.8,
            href: $(this).attr("href"),
            onComplete: function(){
                window.setTimeout('$.colorbox.resize()',10);
            }
        });
        event.preventDefault();
    });

    // site-wide datepicker default
    $(".auto-datepicker").datepicker({
        minDate:0
    });
    // site-wide timepicker default
    $(".auto-timepicker").timepicker({
        stepMinute: 30,
        ampm: true,
        hourGrid: 4,
        minuteGrid:30
    });
    $(".auto-timepicker,.auto-datepicker").on("keydown", function(e){
        if (e.keyCode == 8 || e.keyCode == 32 || e.keyCode > 40){
            e.preventDefault(); // don't let users hand edit a time using the auto-timepicker
        }
    });
    // site-wide watermark default
    $(".auto-watermarked").each(function(){
        var self = $(this),
            watermarkText = self.attr("title");
        if (watermarkText){
            self.watermark(watermarkText);
        }
    });
    // automatically suppress navigation on links with the do-not-navigate class
    $('body').on("click", ".do-not-navigate, .inactive", function(e){e.preventDefault()});

    // handle tab into help (the help icon needs to have a target, which indicates the input field that "should" get focus instead of the icon
    $('body').on('focus', '.help-modal-icon, .never-focus', function(){
        var that = $(this),
            target = that.attr("data-target"),
            targetElement = $("#" + target);
        if (targetElement.length){
            targetElement.focus();
        } else {
            // try using it as a selector
            targetElement = $(target);
            if (targetElement.length){
                targetElement.first().focus();
            }
        }
    });

    // handle hash
    if ($.isFunction(CAUSETOWN.handleHash)){
        CAUSETOWN.handleHash(window.location.hash);
        $(window).bind('hashchange', function() {
            hash = window.location.hash;
            CAUSETOWN.handleHash(hash);
        });
    }

    // show elements hidden if no javascript enabled
    $(".hide-no-js").show();

    if ($.fn.flexslider){
        $('.flexslider-thumbnails').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
        if ($("#flexslider-carousel").length){
           // set up carousel instead

            $('#flexslider-carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 80,
                itemMargin: 5,
                asNavFor: '#flexslider-slider'
            });

            $('#flexslider-slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: true,
                sync: "#flexslider-carousel",
                pauseOnAction: true, // default setting
                after: function(slider) {
                    /* auto-restart player if paused after action */
                    if (!slider.playing) {
                        slider.play();
                    }
                }
            });
        }
    }

    // hide header/footer if loaded in an iframe
    if (CAUSETOWN._isEmbedded){
        // hide header/footer, but leave one set of "stitches" -- mostly because they are hard to get rid of
        $("#wrapper").find("#footer").hide();
        // make all links open new window
        $("a#logo[href^=http]").each(function(){
            if(this.href.indexOf(location.hostname) > -1) {
                $(this).attr({
                    target: "causetown_window",
                    title: "Opens in a new window"
                });
            }
        });

        // event brief links open in a new page
        $("body").on("click", ".event_text a", function(e){
            var self = $(this),
                href = self.attr("href");
            if (href.indexOf("#") == 0){
                // this is probably a dialog; ignore
            } else {
                window.open(href, "_blank");
                e.preventDefault();
            }
        });
    }

    // dynamic widening of the main content by mangling the responsive classes
    (function widenLayout(){
        var responsiveConversionMapping = [
                {
                    selector: "#right_rail.four.columns",
                    oldClass: "four",
                    newClass: "fifteen"
                },
                {
                    selector: ".thirteen.columns",
                    oldClass: "thirteen",
                    newClass: "sixteen"
                },
                {
                    selector: ".twelve.columns",
                    oldClass: "twelve",
                    newClass: "fifteen"
                },
                {
                    selector: ".eleven.columns",
                    oldClass: "eleven",
                    newClass: "fourteen"
                },
                {
                    selector: ".ten.columns",
                    oldClass: "ten",
                    newClass: "thirteen"
                },
                {
                    selector: ".nine.columns",
                    oldClass: "nine",
                    newClass: "twelve"
                },
                {
                    selector: ".eight.columns",
                    oldClass: "eight",
                    newClass: "eleven"
                },
                {
                    selector: ".seven.columns",
                    oldClass: "seven",
                    newClass: "ten"
                },
                {
                    selector: ".six.columns",
                    oldClass: "six",
                    newClass: "nine"
                },
                {
                    selector: ".five.columns",
                    oldClass: "five",
                    newClass: "seven"
                },
                {
                    selector: ".four.columns",
                    oldClass: "four",
                    newClass: "seven"
                },
                {
                    selector: ".three.columns",
                    oldClass: "three",
                    newClass: "six"
                },
                // override
                {
                    selector: ".paired.fields.seven.columns",
                    oldClass: "seven",
                    newClass: "four"
                },
                {
                    selector: ".submit-div.nine.columns.omega",
                    oldClass: "nine",
                    newClass: "six"
                }
            ],
            conversionSpec,
            container = $("#body_section");
        if (CAUSETOWN.renderWide){
            for (var i = 0; i < responsiveConversionMapping.length; i++){
                conversionSpec = responsiveConversionMapping[i];
                container.find(conversionSpec.selector).removeClass(conversionSpec.oldClass).addClass(conversionSpec.newClass)
            }
            container.find("#right_rail div").hide();
        }
    })();
});

CAUSETOWN.util = CAUSETOWN.util || {};
CAUSETOWN.util.formatCurrency = function(numberString) {
    var num = numberString.toString().replace(/\$|\,/g,''),
        sign, cents;
    if(isNaN(num))
        num = "0";

    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();

    if(cents<10)
        cents = "0" + cents;

    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+
            num.substring(num.length-(4*i+3));
    var ReturnVal = (((sign)?'':'-') + num + '.' + cents);
    if (ReturnVal == "0.00"){
        ReturnVal = "";
    }
    return ReturnVal;
}

/*
  Plugin to serialize form
*/
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
$.fn.serializeNonNullObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
			if (this.value){
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			}
        } else {
			if (this.value){
				o[this.name] = this.value || '';
			}
        }
    });
    return o;
};
/*
    Plugin to implement "blur" on a form
 */
$.fn.containerBlur = function(callback){
    var onContainerBlur,
        formElement = this,
        callbackFunction = $.isFunction(callback) ? callback : null;

    if (callbackFunction && formElement.find){
        formElement
            .on("blur.causetown", "input, select, textarea", function(e){
                onContainerBlur = setTimeout(callbackFunction, 100);
            })
            .on("focus.causetown", "input:not(.blur-form)', select, textarea", function(e){
                clearTimeout(onContainerBlur);
            });
    }
    return this;
}

function trace( message ) {
	if ( window['console'] !== undefined ) {
		console.log( message );
	}
}