$(document).ready(function() {
	
	// Expand Panel
	$("#open").click(function(){
		$("div#panel").slideDown("slow");
	});	
	$("#open2").click(function(){
		$("div#Menus").slideDown("slow");
	});	
	
	// Collapse Panel
	$("#close").click(function(){
		$("div#panel").slideUp("slow");	
	});		
	
	$("#close2").click(function(){
		$("div#Menus").slideUp("slow");	
	});		

	// Switch buttons from "Log In | Register" to "Close Panel" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});		
		
	// Switch buttons from "Menus" to "Close" on click
	$("#toggle2 a").click(function () {
		$("#toggle2 a").toggle();
	});		
	
});