// Graph Search Widget
//
// (c) FirstGiving.com 2010
// Michael Pearson <michael@firstgiving.com>
//
var FG_GRAPHWIDGET = new Object();

if (undefined == FG_GRAPHWIDGET_PARAMS) {
    //alert('FG_GRAPHWIDGET_PARAMS: ' + FG_GRAPHWIDGET_PARAMS);
    var FG_GRAPHWIDGET_PARAMS = {};
}

var port = 8983;
if (location.protocol == 'https:') {
    port = 8984;
}

//FG_GRAPHWIDGET.endpoint = location.protocol + '//localhost:51616';
FG_GRAPHWIDGET.endpoint = location.protocol + '//graphapi.firstgiving.com';

// postcode endpoint (not supported as far as I can tell)
FG_GRAPHWIDGET.pcEndpoint = location.protocol + '';

FG_GRAPHWIDGET.cdn = location.protocol + '//graphapi-widget.firstgiving.com';

FG_GRAPHWIDGET.cbFunc = 'FG_GRAPHWIDGET.renderResults';

FG_GRAPHWIDGET.localesource = 2;

FG_GRAPHWIDGET.charityMap = {
    'A': 'Arts, Culture & Humanities',
    'B': 'Education',
    'C': 'Environment',
    'D': 'Animal-Related',
    'E': 'Health Care',
    'F': 'Mental Health & Crisis Intervention',
    'G': 'Voluntary Health Associations & Medical Disciplines',
    'H': 'Medical Research',
    'I': 'Crime & Legal-Related',
    'J': 'Employment',
    'K': 'Food, Agriculture, Nutrition',
    'L': 'Housing & Shelter',
    'M': 'Public Safety, Disaster Preparedness & Relief',
    'N': 'Recreation & Sports',
    'O': 'Youth Development',
    'P': 'Human Services',
    'Q': 'International, Foreign Affairs',
    'R': 'Civil Rights, Social Action & Advocacy',
    'S': 'Community Improvement & Capacity Building',
    'T': 'Philanthropy, Voluntarism & Grantmaking Foundations',
    'U': 'Science & Technology',
    'V': 'Social Science',
    'W': 'Public & Societal Benefit',
    'X': 'Religion-Related',
    'Y': 'Mutual & Membership Benefit',
    'Z': 'Unknown'
};

FG_GRAPHWIDGET.localeLogos = {
    1: '/static/images/just_giving_logo.png',
    2: '/static/images/first_giving_logo.png'
}


FG_GRAPHWIDGET.defaultPageSize = 10;

FG_GRAPHWIDGET.typeAheadMode = 'next';

FG_GRAPHWIDGET.minSearchCharacters = 2; // specify how many characters must be entered before trigging a search.

FG_GRAPHWIDGET.submitSearch = function (page) {


    // I think this is saying "if they cleared the text box, and we are in the typeAheadMode, then clear the results."
    if (((jQuery('#fgNameStart').val() == '') || (jQuery('#fgNameStart').val().length < FG_GRAPHWIDGET.minSearchCharacters)) && FG_GRAPHWIDGET.typeAheadMode == 'next') {
        FG_GRAPHWIDGET.clearResults();
        if (!(jQuery('#fgAdvancedDToggle').attr('checked')))
        {
            return false;
        }
    }

    // find code
    var nteeCode = jQuery('#fgNtee').val();

    var zipcode = '';
    var zipradius = '';
    // if zip is not empty...
    if (jQuery('#fgAdvancedDToggle').attr('checked')) {
        var zVal = jQuery('#fgZip').val();

        if (zVal != '') {
            if (zVal.length != 5) return false;
            zipcode = zVal; // assign to zipcode in outter scope after validated here.
            var distance = jQuery('#fgZipRadius').val();
            if (distance == '' || distance == 0) {
                distance = 1;
            }
            zipradius = distance; // assign to zipradius in outter scope after validated here.
        }
    }

    jQuery('#fgResultsLoaderAnim').show();

    // ENDPOINT URL and CALLACK
    var url = this.endpoint + '/?callback=?';

    // CATEGORY
    if (nteeCode != '') {
        url += '&category=' + nteeCode;
    }

    // ORGANIZATION NAME SEARCH TEXT
    var searchText = jQuery('#fgNameStart').val().toLowerCase();
    if (!jQuery('#fgNameStart').hasClass('fgHinted')) {
        if (searchText != '') {
            url += '&name=' + jQuery('#fgNameStart').val().toLowerCase();
        }
    }

    // ZIP CODE
    if (zipcode != '') {
        url += '&zipcode=' + zipcode;
    }

    // ZIP RADIUS
    if (zipradius != '') {
        url += '&zipradius=' + zipradius;
    }

    // PAGE OFFSET
    if (!page) {
        page = 0;
    }
    var pageOffSet = page * this.defaultPageSize;
    url += '&pageoffset=' + pageOffSet;

    if (this.defaultPageSize > 0)
        url += '&rowcount=' + this.defaultPageSize;

    if (this.localesource > 0)
        url += '&localeid=' + this.localesource;

    jQuery.getJSON(url, function (data) { FG_GRAPHWIDGET.test(data); });

}

FG_GRAPHWIDGET.test = function (resp) {

    var data = '';
    var self = this;
    var pageNum = 0;
    var totalPages = 0;
    var pageSize = this.defaultPageSize;
    var rowsReturned = 0;
    var pageOffset = 0;


    // replace 1=1 with number of rows returned > 0
    if (1 == 1) {
        jQuery.each(resp, function () {
            jQuery.each(this, function (idx, charity) {
                //                alert('idx=' + idx);
                //                alert('organization_name=' + charity.organization_name);
                //                alert('id_uuid=' + charity.id_uuid);
                //                alert('category_code=' + charity.category_code);
                //                alert('city=' + charity.city);
                //                alert('region=' + charity.region);
                //                alert('title=' + charity.title);
                //                alert('zipcode=' + charity.zipcode);
                //                alert('organization_abstract=' + charity.organization_abstract);
                //                alert('government_id=' + charity.government_id);

                if (rowsReturned == 0) {
                    pageOffset = parseInt(charity.offset); // will either be zero, or any multiple of the configured page size.
                    rowsReturned = parseInt(charity.numFound);
                    pageNum = pageOffset / pageSize;
                    totalPages = Math.floor(rowsReturned / pageSize);
                }

                var nteeParent = null;
                var charityAddress = '';
                if (charity.region == null) {
                    charity.region = '';
                }

                //jQuery('#fgResultsLoaderAnim').hide();

                if (charity.category_code == null)
                    charity.category_code = 'Z';
                if (charity.category_code == '') {
                    nteeParent = 'Z';
                } else {
                    nteeParent = charity.category_code.substr(0, 1);
                }

                data += '<li id="select_' + charity.id_uuid + '" class="fgResult" onClick="">' +
                    '<img class="fgNTEEIcon" src="' + self.cdn + '/static/images/' + nteeParent.toLowerCase() + '.png">' +
                    '<div class="fgCharityHeader">' +
                    '<span>' + charity.organization_name + '</span>';

                if (FG_GRAPHWIDGET.localesource == 1) {
                    charityAddress = 'Registered Charity No. ' + charity.government_id;
                } else if (FG_GRAPHWIDGET.localesource == 4) {
                    charityAddress = charity.city + ', ' + charity.region + ', ' + charity.zipcode + '<br>' + (charity.title == 'Unknown' ? '' : charity.title);
                } else {
                    charityAddress = charity.city + ', ' + charity.region + ', ' + charity.zipcode + '<br>' + (nteeParent == 'Z' ? '' : self.charityMap[nteeParent]);
                }

                data += '<div class="fgNteeCatTitle">' + charityAddress + '</div>' +
                    '<div style="display:none" id="payload_' + charity.id_uuid + '">' + JSON.stringify(charity) + '</div>' +
                    '</div>';

                if (undefined != charity.organization_abstract && charity.organization_abstract != '') {
                    data += '<div class="fgDesc">' + charity.organization_abstract + '</div></li>';
                }
            });
        });

        jQuery('#fgResultsLoaderAnim').hide();
        jQuery('#fgResults').html('<ul>' + data + '</ul>');

        jQuery('#fgResults').show();

        // build simple fgPagination controls
        var pageControl = '';

        var prevCtl = (pageNum > 0) ? '<a href="javascript:void(0)" onclick="javascript:FG_GRAPHWIDGET.submitSearch(' + (pageNum - 1) + ')"><img src="' + self.cdn + '/static/images/pagination_arrow.png" id="fgPreviousResults"/></a>' : '';
        var nextCtl = (pageNum < totalPages) ? '<a href="javascript:void(0)" onclick="javascript:FG_GRAPHWIDGET.submitSearch(' + (pageNum + 1) + ')"><img src="' + self.cdn + '/static/images/pagination_arrow_next.png" id="fgNextResults"/></a>' : '<img src="' + self.cdn + '/static/images/pagination_arrow_next.png" id="fgNextResults"/>';

        pageControl = prevCtl + ' Page ' + (pageNum + 1) + nextCtl + ' of ' + (totalPages + 1);
        pageControl += ' (' + rowsReturned + ' results)';

        jQuery('#fgPagination').html(pageControl);

    } else {
        this.clearResults();
    }
}

FG_GRAPHWIDGET.clearResults = function () {
    jQuery('#fgResults').hide();
    jQuery('#fgResults ul').html('');
    jQuery('#fgPagination').html('');
}

// go button callback
FG_GRAPHWIDGET._btnEv = function (uuid, charity_name) { }
FG_GRAPHWIDGET._resEv = function (uuid, charity_name, ein) { }

// @todo bind real events.
FG_GRAPHWIDGET.bindEvent = function (target, callback) {
    if (target == 'button') {
        this._btnEv = callback;
    } else if (target == 'result') {
        this._resEv = callback;
    }
}

// This is broken out as it's a jsonp callback when polling the remote service
// for this clients postcode.
FG_GRAPHWIDGET.setZipcode = function (data) {

    if (null != data && undefined != data.payload.zipcode) {
        jQuery('#fgZip').val(data.payload.zipcode);
        jQuery('#fgZipLatLong').val(data.payload.latitude + ',' + data.payload.longitude);
    }
}


// GET ZIPCODE FROM WCF
FG_GRAPHWIDGET.setZipcodeFromWCF = function (data) {
    jQuery('#fgZip').val(data[0]["zipcode"]);
    jQuery('#fgZipLatLong').val(data[0]["latitude"] + ',' + data[0]["longitude"]);
}

FG_GRAPHWIDGET.init = function (partial) {

    var self = this;


    jQuery('#fgGraphWidgetContainer').show();

    jQuery('#fgGraphWidgetContainer').html(partial);

    // process partial options
    if (undefined != FG_GRAPHWIDGET_PARAMS) {
        if (undefined != FG_GRAPHWIDGET_PARAMS.button) {
            jQuery('#fgNameStart').attr('size', '35');
            // Shrink the search input and Inject an action button.
            jQuery('#fgButtonWrapper').html('<input type="button" id="fgActionButton" value="Next" value="go!" onclick="FG_GRAPHWIDGET._btnEv(jQuery(\'#fg_selected_charity_uuid\').val());">');

            if (undefined != FG_GRAPHWIDGET_PARAMS.button.action) {
                FG_GRAPHWIDGET.bindEvent('button', FG_GRAPHWIDGET_PARAMS.button.action);
            }

            if (undefined != FG_GRAPHWIDGET_PARAMS.button.label) {
                jQuery('#fgActionButton').val(FG_GRAPHWIDGET_PARAMS.button.label);
            }
        }

        if (undefined != FG_GRAPHWIDGET_PARAMS.localesource) {
            FG_GRAPHWIDGET.localesource = FG_GRAPHWIDGET_PARAMS.localesource;
        }

        if (undefined != FG_GRAPHWIDGET_PARAMS.typeAheadMode) {
            FG_GRAPHWIDGET.typeAheadMode = FG_GRAPHWIDGET_PARAMS.typeAheadMode;
        }
    }


    // Inject Widget HTML
    jQuery('#fgWidgetHeader').attr('src', FG_GRAPHWIDGET.cdn + '/static/images/logo_header.png');
    jQuery('#fgResultsLoader img').attr('src', FG_GRAPHWIDGET.cdn + '/static/images/ajax-loader_new.gif');
    jQuery('#fgAdvancedFeaturesButton img').attr('src', FG_GRAPHWIDGET.cdn + '/static/images/gear_icon.png');
    jQuery('#fg-logo').css('background-image', 'url(' + FG_GRAPHWIDGET.cdn + FG_GRAPHWIDGET.localeLogos[FG_GRAPHWIDGET.localesource] + ')');

    // advanced features not available for non-US locales by default
    if (FG_GRAPHWIDGET.localesource !== 2) {
        jQuery('#fgAdvancedControls').hide();
    }

    // setup our dropdown.
    var nteeSelect = jQuery('#fgNtee');
    jQuery.each(self.charityMap, function (idx, attrib) {
        nteeSelect.append('<option value="' + idx + '">' + attrib + '</option>');
    });

    // ENDPOINT URL and CALLACK
    var url = this.endpoint + '/?callback=?&getzip=1';
    // SET ZIPCODE BASED ON HOST IP
    jQuery.getJSON(url, function (data) { FG_GRAPHWIDGET.setZipcodeFromWCF(data); });

    jQuery('.fgResult').live('mouseover mouseout click', function (event) {
        switch (event.type) {
            case 'mouseover':
                jQuery(this).addClass('highlightResult');
                break;
            case 'mouseout':
                jQuery(this).removeClass('highlightResult');
                break;
            case 'click':
                //var charityName = jQuery(this).find('span').first().text();
                var charityUUID = jQuery(this).attr('id').replace('select_', '');
                var payload = jQuery.parseJSON(jQuery(this).find('#payload_' + charityUUID).text());
                var charityName = payload["organization_name"];

                jQuery('#fg_selected_charity_uuid').val(charityUUID);
                jQuery('#fgNameStart').val(charityName);
                charityUUID = charityUUID.substring(9, charityUUID.length);
                FG_GRAPHWIDGET._resEv(charityUUID, charityName, payload["government_id"]);
                FG_GRAPHWIDGET.clearResults();
                break;
            default: break;
        }
    });

    // Change search criteria
    jQuery('.sToggle').click(function () {
        FG_GRAPHWIDGET.clearResults();
        jQuery('#fgNameStart').val('');
        jQuery('#sType').val(jQuery(this).val());
    });

    // query string changed
    jQuery('.queryable').keyup(function (e) {
        e.preventDefault();

        // alphanumeric, delete, backspace
        if ((e.keyCode >= 46 && e.keyCode <= 90)
            || e.keyCode == 8
            ) {
            self.submitSearch();
        }
    });

    // Drop input hints when they're clicked and assume focus.
    jQuery('.fgHinted').click(function (e) {
        e.preventDefault();
        if (jQuery(this).hasClass('fgHinted')) {
            jQuery(this).removeClass('fgHinted');
            jQuery(this).val('');
        }
    });

    // ---------- Geo Targetting

    // Geo targetting toggle.
    jQuery('#fgAdvancedDToggle').click(function () {
        jQuery('#fgZipRadius').attr('disabled', !jQuery(this).attr('checked'));
        jQuery('#fgZip').attr('disabled', !jQuery(this).attr('checked'));
        FG_GRAPHWIDGET.submitSearch();
    });

    // zip changed
    jQuery('#fgZip').keyup(function (e) {
        e.preventDefault();
        var zl = jQuery(this).val().length;
        // on 5 or 0 zip length submit the search.
        if (zl != 5 && zl != 0) return false;

        FG_GRAPHWIDGET.submitSearch();
    });

    // zip_radius changed
    jQuery('#fgZipRadius').keyup(function (e) {
        e.preventDefault();
        if (jQuery(this).val().length == 0) return false;
        self.submitSearch();
    });

    // ---------- Category change
    jQuery('#fgNtee').change(function (e) {
        e.preventDefault();
        FG_GRAPHWIDGET.submitSearch();
    });

    // ---------- Advanced Features Toggle
    jQuery('#fgAdvancedControls').click(function () {
        jQuery('#fgAdvancedFeatures').toggle();
        jQuery('#fgAdvancedArrow').toggleClass('open');
    });


};

FG_GRAPHWIDGET.zipUpdateSubmit = function (resp) {
    FG_GRAPHWIDGET.setZipcode(resp);
    this.submitSearch();
}

// Loads the partial and calls init when done.
FG_GRAPHWIDGET.loadWidget = function () {
    // process options

    //alert('loadWidget endpoint: ' + FG_GRAPHWIDGET.endpoint);
    //alert('pcEndpoint: ' + FG_GRAPHWIDGET.pcEndpoint);
    //alert('cdn: ' + FG_GRAPHWIDGET.cdn);

    if (undefined != FG_GRAPHWIDGET_PARAMS) {

        // CDN pointer.  This shouldn't ever be used outside of development.
        // and is an unsupported feature.
        if (undefined != FG_GRAPHWIDGET_PARAMS.cdn) {
            FG_GRAPHWIDGET.cdn = FG_GRAPHWIDGET_PARAMS.cdn;
        }

        if (undefined != FG_GRAPHWIDGET_PARAMS.results) {

            if (undefined != FG_GRAPHWIDGET_PARAMS.results.selectaction) {

                FG_GRAPHWIDGET.bindEvent('result', FG_GRAPHWIDGET_PARAMS.results.selectaction);
            }

        }

        if (undefined != FG_GRAPHWIDGET_PARAMS.endpoint) {
            FG_GRAPHWIDGET.endpoint = FG_GRAPHWIDGET_PARAMS.endpoint;

        }

        // unsupported feature. (postcode service endpoint)
        if (undefined != FG_GRAPHWIDGET_PARAMS.pcEndpoint) {
            FG_GRAPHWIDGET.pcEndpoint = FG_GRAPHWIDGET_PARAMS.pcEndpoint;

        }
    }



    // Inject CSS unless 'nocss' has been specified.
    if (undefined == FG_GRAPHWIDGET_PARAMS || undefined == FG_GRAPHWIDGET_PARAMS.nocss ||
        (undefined != FG_GRAPHWIDGET_PARAMS.nocss && FG_GRAPHWIDGET_PARAMS.nocss == false)) {

        var s = document.createElement('link');
        s.setAttribute('href', this.cdn + '/static/css/style.css');
        s.setAttribute('rel', 'stylesheet');
        s.setAttribute('type', 'text/css');
        document.getElementsByTagName('head')[0].appendChild(s);

    }

    jQuery.ajax({
        type: 'get',
        url: FG_GRAPHWIDGET.cdn + '/static/partial.json',
        dataType: 'jsonp',
        jsonp: "FG_GRAPHWIDGET.init",

        error: function (resp) {

            jQuery('#fgResults').html('Error');
            //Alert(resp.responseText);

        }

    });

}



// Init. Here we inject jQuery if it doesn't exist, then load the widget
if (typeof jQuery == 'undefined') {

    var script = document.createElement("script");
    script.type = "text/javascript";

    if (undefined == FG_GRAPHWIDGET_PARAMS || undefined == FG_GRAPHWIDGET_PARAMS.cdn) {
        //cdn = location.protocol + '//localhost:65066';
        cdn = location.protocol + '//graphapi-widget.firstgiving.com';

    } else {
        cdn = FG_GRAPHWIDGET_PARAMS.cdn;

    }

    // Event listeners.
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                FG_GRAPHWIDGET.loadWidget();
            }

        };
    } else {  //Others
        script.onload = function () {
            FG_GRAPHWIDGET.loadWidget();
        };
    }

    script.src = cdn + '/static/js/jquery-1.6.2.min.js';
    //script.src = 'http://graphapidev1.widget.firstgiving.com/static/js/jquery-1.6.2.min.js';

    document.body.appendChild(script);

} else {
    FG_GRAPHWIDGET.loadWidget();
}