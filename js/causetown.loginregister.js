// callback for login success/fail
var CAUSETOWN = CAUSETOWN || {}; // namespace
CAUSETOWN.CurrentServer = CAUSETOWN.CurrentServer || "/";
CAUSETOWN.EventForm = CAUSETOWN.EventForm || {};
CAUSETOWN.EventForm.hideSubmitButton = CAUSETOWN.EventForm.hideSubmitButton || $.noop;
CAUSETOWN.EventTemplate = CAUSETOWN.EventTemplate || {};
CAUSETOWN.EventTemplate.cancelRegistration = CAUSETOWN.EventTemplate.cancelRegistration || $.noop;
CAUSETOWN.EventTemplate.regFormSuccessCallback = CAUSETOWN.EventTemplate.regFormSuccessCallback || {};
CAUSETOWN.EventTemplate.setAddressData = CAUSETOWN.EventTemplate.setAddressData || $.noop;
CAUSETOWN.LoginForm = CAUSETOWN.LoginForm || {};
CAUSETOWN.LoginForm.successHandler = function(data){
    var merchantApiUrl = CAUSETOWN.CurrentServer + "ApiMerchants",
        merchantRecord,
        acceptCheckbox = $("#accept-terms");
    if (!CAUSETOWN.LoginForm.clickedBanner){
        CAUSETOWN.LoginForm.clickedBanner = $(".login-banner").first();
    }
    if (!CAUSETOWN.LoginForm.alertSpan){
        CAUSETOWN.LoginForm.alertSpan = CAUSETOWN.LoginForm.clickedBanner.closest("div").find(".alert-login");
    }
    if (data.memberTypeDescription && data.memberTypeDescription == 'Merchant'){
        $.ajax({
            url: merchantApiUrl,
            success: function(data){
                addressData = {
                    "BizName": data.record.name,
                    "Phone": data.record.phone,
                    "Address": data.record.address,
                    "City": data.record.city,
                    "State": data.record.state,
                    "Zip": data.record.postalCode
                };
                CAUSETOWN.EventTemplate.setAddressData(addressData, "Logged In");
            }
        });

        CAUSETOWN.LoginForm.alertSpan.removeClass('info').addClass('success').html('You are now logged in as a '+data.memberTypeDescription).fadeTo(3000,0);
        $(".login-banner").slideUp(); // close all login banners

        acceptCheckbox.prop("checked", false).prop("disabled", false);
        $(".submit-button-trigger").addClass("enabled");
        acceptCheckbox.click();
        //$("#save-as-draft-button").show();
    } else {
        CAUSETOWN.LoginForm.clickedBanner.effect("bounce", { times:3 }, 300);
        CAUSETOWN.LoginForm.alertSpan.removeClass('info').html('Only Businesses can create an event from this template');
    }
    if (CAUSETOWN.LoginForm.container){
        CAUSETOWN.LoginForm.container.slideUp(function(){
            CAUSETOWN.LoginForm.container.html("");
        });
    }
} // end success handler

CAUSETOWN.RegForm = CAUSETOWN.RegForm || {};
CAUSETOWN.RegForm.cancelHandler = function(){
    if (confirm("Are you sure you want to start over? This will reset the entire registration form.")){
        CAUSETOWN.RegForm.setAddressDataCallback();
        //CAUSETOWN.EventForm.hideSubmitButton("Please login or register.");
        $("#accept-terms").prop("checked",false).prop("disabled", true);
        $(".login-banner").slideDown(function(){
            CAUSETOWN.scrollTo($("#slide-display-position"));
        });
        CAUSETOWN.LoginForm.clickedBanner = undefined;
        CAUSETOWN.LoginForm.container.slideUp(function(){
            CAUSETOWN.LoginForm.container.html("");
        });
        CAUSETOWN.EventTemplate.cancelRegistration();
    }
};
CAUSETOWN.RegForm.handleValidateActions = function(){
    var regForm = CAUSETOWN.RegForm.getForm(),
        acceptCheckbox = $("#accept-terms");
    if (regForm.valid && regForm.valid() && CAUSETOWN.EventTemplate._state.completedRegistrationOnce){
        acceptCheckbox.prop("checked", false).prop("disabled", false);
        acceptCheckbox.click();
        $("#create-event-form").find(".submit-button-trigger").addClass("enabled");
        if ($.isFunction(CAUSETOWN.EventForm.showSubmitButton)){
            CAUSETOWN.EventForm.showSubmitButton();
        }
    } else {
        acceptCheckbox.prop("checked", true).prop("disabled", true);
        acceptCheckbox.click();
        if ($.isFunction(CAUSETOWN.EventForm.hideSubmitButton)){
            //CAUSETOWN.EventForm.hideSubmitButton("Please login or register.");
        }
    }
};
CAUSETOWN.RegForm.getForm = CAUSETOWN.RegForm.getForm || $.noop;
CAUSETOWN.RegForm.checkRegistration = CAUSETOWN.RegForm.checkRegistration || $.noop;
CAUSETOWN.RegForm.onLoadHandler = function(){
    var regForm = CAUSETOWN.RegForm.getForm(),
        cancelButtons = regForm.find(".close-register"),
        bottomCancelButton = regForm.find("#cancel-registration"),
        submitButtonWrap = $("#submit-registration").closest(".btn"),
        agreementText = $("#agreement-text"),
        finishRegistration = $("#hide-contact-section");
    submitButtonWrap.remove();
    agreementText.hide();
    cancelButtons.hide();
    bottomCancelButton.html("Start over").show();

    regForm.containerBlur(function() {
        CAUSETOWN.RegForm.handleValidateActions();
    });
    finishRegistration.closest(".btn").show();
    regForm.on("click", "#hide-contact-section", function(e){
        var container = $(this).closest("fieldset"),
            invalidBusinessFields,
            closeBusinessInfo = $("#finish-edit-business-address");
        container.find("label.error").removeClass("error");

        e.preventDefault();
        if (!$("#sign-up-form").valid()){
            invalidBusinessFields = container.find("label.error");
            if (invalidBusinessFields.length){
                return false;
            }
        } else {
            if (closeBusinessInfo.is(":visible") || $("#company_addresss").is(":visible")){
                closeBusinessInfo.trigger("click");
            }
            container.children("label").slideUp();
            $("#edit-contact-info").closest(".btn").show();

            CAUSETOWN.RegForm.checkRegistration(
                function(){
                    if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
                        CAUSETOWN.EventTemplate.setRegformValid(true);
                        CAUSETOWN.EventTemplate.scrollTop();
                    }
                    window.location.hash = "#customize-event";
                },
                function(){
                    if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
                        CAUSETOWN.EventTemplate.setRegformValid(false);
                        CAUSETOWN.EventTemplate.scrollTop();
                    }
                }
            );
        }
    });
    regForm.on("click", "#edit-contact-info", function(e){
        if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
            CAUSETOWN.EventTemplate.setRegformValid(false);
            CAUSETOWN.EventTemplate.scrollTop();
        }
        $(this).closest("fieldset").children("label").slideDown();
        $(this).closest(".btn").hide();
    });
    regForm.on("click", "#finish-edit-business-address", function(e){
        e.preventDefault();
        if ($("#sign-up-form").valid()){
            CAUSETOWN.RegForm.checkRegistration(
                function(){
                    if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
                        CAUSETOWN.EventTemplate.setRegformValid(true);
                        CAUSETOWN.EventTemplate.scrollTop();
                    }
                    window.location.hash = "#customize-event";
                },
                function(){
                    if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
                        CAUSETOWN.EventTemplate.setRegformValid(false);
                        CAUSETOWN.EventTemplate.scrollTop();
                    }
                }
            );
        }
    });
    regForm.on("click", "#edit-business-address", function(e){
        if (CAUSETOWN.EventTemplate.setRegformValid && $.isFunction(CAUSETOWN.EventTemplate.setRegformValid)){
            CAUSETOWN.EventTemplate.setRegformValid(false);
            CAUSETOWN.EventTemplate.scrollTop();
        }
    });
};
CAUSETOWN.RegForm.successHandler = function(data){
    var addressData;
    if (data.record){
        $("input[name='registered_member_id']").val(data.record.id);
        addressData = {
            "BizName": data.company.name,
            "Phone": data.company.phone,
            "Address": data.company.address,
            "City": data.company.city,
            "State": data.company.state,
            "Zip": data.company.postalCode
        };
        //CAUSETOWN.EventTemplate.setAddressData(addressData, "Your activation email has been sent. You may save this event as a draft and log in later to publish it.");
        CAUSETOWN.EventTemplate.setAddressData(addressData);
        CAUSETOWN.LoginForm.container.slideUp(function(){
            CAUSETOWN.LoginForm.container.html("");
        });
        if ($.isFunction(CAUSETOWN.EventTemplate.regFormSuccessCallback)){
            CAUSETOWN.EventTemplate.regFormSuccessCallback();
        }
    }
};
CAUSETOWN.RegForm.setAddressDataCallback = function(addressData){
    if (addressData){
        CAUSETOWN.EventTemplate.setAddressData(addressData);
    } else {
        CAUSETOWN.EventTemplate.setAddressData({
            "BizName" : "Example: Your Business Here",
            "Address" : "123 Some Street",
            "City" : "Some City",
            "State" : "CA",
            "Zip" : "90210",
            "Phone" : "000-555-1234",
            "Resetting" : true
        });
    }
};

$(function(){
    var loginFormUrl = CAUSETOWN.CurrentServer + "ajax_loginForm",
        registerFormUrl = CAUSETOWN.CurrentServer + "ajax_merchantSignupForm";

    //hide the checkbox for terms and conditions
    //$('#accept-terms-label').hide();
    $("#accept-terms").prop("checked",false).prop("disabled", true);

    $(document).on("click", ".login-action", function(e){
        if (CAUSETOWN.LoginForm.clickedBanner){
            CAUSETOWN.LoginForm.clickedBanner = undefined;
            CAUSETOWN.LoginForm.container.slideUp(function(){
                CAUSETOWN.LoginForm.container.html("");
            });
        } else {
            CAUSETOWN.LoginForm.clickedBanner = $(this).closest(".login-banner");
            CAUSETOWN.LoginForm.container = CAUSETOWN.LoginForm.clickedBanner.closest("div").find(".login-or-register");
            CAUSETOWN.LoginForm.container.load(loginFormUrl, function(){
                CAUSETOWN.LoginForm.container.slideDown();
                $("#username").focus();
            });
        }
    });

    $(document).on("click", ".login-action-from-register", function(e){
        e.preventDefault();
        CAUSETOWN.LoginForm.container.slideUp(function(){
            CAUSETOWN.LoginForm.container.load(loginFormUrl, function(){
                $(".login-banner").slideDown();
                CAUSETOWN.LoginForm.container.slideDown();
                $("#username").focus();
            });
        });
    });

    $(document).on("click", ".register-action", function(e){
        CAUSETOWN.LoginForm.container = $(this).closest(".login-banner").closest("div").find(".login-or-register");
        CAUSETOWN.LoginForm.container.load(registerFormUrl, function(){
            if (CAUSETOWN.LoginForm.clickedBanner){
                CAUSETOWN.LoginForm.clickedBanner = undefined;
                CAUSETOWN.LoginForm.container.slideUp();
            }
            CAUSETOWN.LoginForm.container.slideDown(400,function(){
                $("#city-grid-zip").focus();
            });
        });
        //$("#accept-terms").prop("checked",false).prop("disabled", false);
        $(".login-banner").slideUp();
    });
});