var CAUSETOWN = CAUSETOWN || {};
CAUSETOWN.FindEvents = function(){
    var defaults = {
            "keywordAutocompleteURL": "",
            "locationAutocompleteURL": "",
            "findEventsURL": "",
            "initialLocation": "",
            "initialLat": "",
            "initialLong": "",
            "exactZip":"",
            "pid": "",
            "historyManager": {}
        },
        settings = defaults,
        form = $("#indexFilterForm"),
        formElements = {
            "self": $("#indexFilterForm"),
            "keyword": form.find("input[name='KW']"),
            "location": form.find("input[name='L']"),
            "size": form.find("select[name='PL']"),
            "go": form.find(".filter-go .round-green-button")
        },
        cacheData = {
            "pageLoaded": false,
            "statePopped": false,
            "rePosition": 0,
            "keywordAutocomplete": {},
            "locationAutocomplete": {},
            "location":"",
            "lat": '',
            "long": '',
            "startIndex": 0,
            "exactZip": '',
            "showProcessing": true,
            "lastKnownState": {},
            "lastKnownQueryString": ""
        },
        pageElements = {
            "filterDisplay": form.find(".message.alert.info span").first(),
            "removeFilter": form.find(".message.alert.info a").first(),
            "eventList": $("#event-list"),
            "processing": $(".indexfilter-processing")
        },
        templates = {
            "filter": 'Filtering by {filterText}'
        },
        methods = {
            "init": function(options){
                settings = $.extend({}, defaults, options);
                cacheData.location = settings.initialLocation ? settings.initialLocation : cacheData.location;
                cacheData.lat = settings.initialLat ? settings.initialLat : cacheData.lat;
                cacheData.long = settings.initialLong ? settings.initialLong : cacheData.long;
                cacheData.exactZip = settings.exactZip ? settings.exactZip : cacheData.exactZip;
                methods.initEventHandlers();
            },
            "htmlEncode": function(value){
                return $('<div/>').text(value).html();
            },
            "buildFilterString": function(criteria){
                var stringBuffer = [],
                    keywordSnippet = (criteria.KW) ? "keyword: " + methods.htmlEncode(criteria.KW) : "",
                    locationSnippet = (criteria.L) ? "location: " + methods.htmlEncode(criteria.L) : cacheData.location,
                    filterSnippet, filterString = "";
                if (keywordSnippet){
                    stringBuffer.push(keywordSnippet);
                }
                if (keywordSnippet && locationSnippet){
                    stringBuffer.push(", ");
                }
                if (locationSnippet){
                    stringBuffer.push(locationSnippet);
                }
                filterSnippet = stringBuffer.join("");
                if (filterSnippet){
                    filterString = templates.filter.replace(/{filterText}/g, filterSnippet);
                }
                return filterString;
            },
            "submitSearch": function(callbackFunction){
                var criteria = {
                        "KW": formElements.keyword.val(),
                        "L": formElements.location.val() ? formElements.location.val() : cacheData.location,
                        "I": cacheData.startIndex,
                        "PL": formElements.size.val(),
                        "EZ": cacheData.exactZip,
                        "lat": cacheData.lat,
                        "long": cacheData.long
                    },
                    filterString = methods.buildFilterString(criteria),
                    queryString = filterString ? $.param(criteria) : "",
                    stateObject = $.extend({}, criteria);
                if (form.is(":visible") && settings.findEventsURL && criteria.L){
                    criteria.pid = settings.pid;
                    stateObject.pid = settings.pid; // don't put this in url
                    stateObject.rePosition = cacheData.rePosition;
                    stateObject.queryString = queryString;
                    formElements.go.addClass("round-gray-button").removeClass("round-green-button");
                    methods.detachAutocompletes();
                    if (cacheData.showProcessing){
                        pageElements.processing.show();
                    }
                    pageElements.removeFilter.hide();
                    cacheData.location = criteria.L;
                    formElements.location.val(criteria.L);
                    if (cacheData.statePopped){
                        pageElements.processing.find(".init").addClass("hidden");
                        pageElements.processing.find(".ongoing").removeClass("hidden");
                    }
                    $.ajax({
                        url: settings.findEventsURL,
                        data: criteria,
                        type: 'POST',
                        dataType: 'html',
                        success: function(data){
                            cacheData.lastKnownState = stateObject;
                            cacheData.lastKnownQueryString = queryString;
                            pageElements.eventList.html(data);
                            formElements.go.addClass("round-green-button").removeClass("round-gray-button");
                            pageElements.filterDisplay.html(filterString);
                            pageElements.removeFilter.show();
                            methods.attachAutocompletes();
                            pageElements.processing.hide();
                            pageElements.processing.find(".init").addClass("hidden");
                            pageElements.processing.find(".ongoing").removeClass("hidden");
                            if (!cacheData.statePopped){
                                if (!cacheData.pageLoaded){
                                    cacheData.lastKnownQueryString = ""; // no query string on initial load
                                    cacheData.rePosition = 0;
                                    cacheData.pageLoaded = true;
                                } else if (queryString){
                                    // default behavior
                                } else {
                                    cacheData.lastKnownQueryString = ""; // no query string anyhow, but set it as string
                                }
                            }
                            if ($.isFunction(callbackFunction)){
                                callbackFunction(stateObject);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown ){
                            methods.attachAutocompletes();
                        }
                    });
                } else {
                    pageElements.eventList.html("Enter your location to get started");
                    if ($.isFunction(callbackFunction)){
                        callbackFunction(stateObject);
                    }
                }
            },
            "loadData": function(stateObject, callbackFunction){
                if (stateObject){
                    formElements.keyword.val(stateObject.KW);
                    cacheData.location = (stateObject.L) ? stateObject.L : cacheData.location;
                    formElements.location.val(cacheData.location);
                    formElements.size.val(stateObject.PL);
                    cacheData.startIndex = stateObject.I;
                    cacheData.exactZip = stateObject.EZ;
                    cacheData.lat = stateObject.lat;
                    cacheData.long = stateObject.long;
                }
                if (!cacheData.statePopped || stateObject){
                    methods.submitSearch(function(newStateObject){
                        var eventListPosition = pageElements.eventList.offset();
                        if (cacheData.rePosition){
                            window.scrollTo(eventListPosition.left,eventListPosition.top);
                            cacheData.rePosition = 0;
                        }
                        if ($.isFunction(callbackFunction)){
                            callbackFunction(newStateObject);
                        }
                    });
                }
            },
            "resetFilter": function(){
                pageElements.filterDisplay.html("");
                pageElements.removeFilter.hide();
                formElements.keyword.val("");
                formElements.location.val("");
                cacheData.exactZip = "";
                cacheData.startIndex = 0;
                if (!cacheData.statePopped){
                    cacheData.lastKnownState = { reset: true };
                    cacheData.lastKnownQueryString = ""; // never set query string for a reset
                }
                pageElements.eventList.html("Enter your location to get started");
            },
            "attachAutocompletes": function(){
                // keyword autocomplete (with caching)
                formElements.keyword.autocomplete({
                    source: function( request, response ) {
                        var term = request.term;
                        if ( term in cacheData.keywordAutocomplete ) {
                            response( cacheData.keywordAutocomplete[ term ] );
                            return;
                        }

                        $.getJSON( settings.keywordAutocompleteURL, request, function( data, status, xhr ) {
                            cacheData.keywordAutocomplete[ term ] = data.suggestions;
                            response( data.suggestions );
                        });
                    }
                });

                // location autocomplete with caching
                formElements.location.autocomplete({
                    source: function( request, response ) {
                        var term = request.term;
                        if ( term in cacheData.locationAutocomplete ) {
                            response( cacheData.locationAutocomplete[ term ] );
                            return;
                        }

                        $.getJSON( settings.locationAutocompleteURL, request, function( data, status, xhr ) {
                            cacheData.locationAutocomplete[ term ] = $.map( data, function( item ) {
                                return item.value;
                            });
                            response( cacheData.locationAutocomplete[ term ] );
                        });
                    },
                    minLength: 3,
                    delay: 300,
                    close: function(e,ui){
                        return false;
                    }
                });
            },
            "detachAutocompletes": function(){
                formElements.keyword.autocomplete('destroy');
                formElements.location.autocomplete('destroy');
            },
            "initEventHandlers": function(){

                methods.attachAutocompletes();

                // event handling
                form.keypress(function(e){
                    if (e.which == 13){
                        e.preventDefault();
                        cacheData.startIndex = 0;
                        methods.submitSearch(methods.saveHistoryState);
                        return false; // necessary to stop IE from making the "DING!!" sound
                    }
                });
                form.on("change", ".submit-on-change", function(){
                    cacheData.startIndex = 0;
                    methods.submitSearch(methods.saveHistoryState);
                });
                form.on("click", ".filter-go .round-green-button", function(e){
                    cacheData.startIndex = 0;
                    methods.submitSearch(methods.saveHistoryState);
                });
                form.on("click", ".indexfilter-resetlink", function(e){
                    e.preventDefault();
                    methods.resetFilter();
                    methods.saveHistoryState();
                });

                pageElements.eventList.on("click", ".event-nav", function(e){
                    var self = $(this),
                        buttons = pageElements.eventList.find(".pagination-buttons");
                    e.preventDefault();
                    cacheData.startIndex = self.attr("data-start");
                    cacheData.rePosition = 1;
                    cacheData.showProcessing = false;
                    buttons.find(".btn").hide();
                    buttons.find(".loading-spinner").show();
                    methods.loadData(null, function(){
                        cacheData.showProcessing = true;
                        methods.saveHistoryState();
                    });
                });

                // unhide form
                form.show();
            },
            "saveHistoryState": function(){
                var historyManager = settings.historyManager;
                if (historyManager &&
                    'saveHistoryState' in historyManager && $.isFunction(historyManager.saveHistoryState) &&
                    'setNewState' in historyManager && $.isFunction(historyManager.setNewState)){
                    historyManager.setNewState();
                    historyManager.saveHistoryState();
                }
            }
        };

    return {
        "init": function(options){
            methods.init(options);
        },
        "loadData": function(stateObject, callbackFunction){
            var newCallback = function(newStateObject){
                cacheData.pageLoaded = true;
                cacheData.statePopped = false;
                if ($.isFunction(callbackFunction)){
                    callbackFunction(newStateObject);
                }
            };
            if (stateObject){
                if ('reset' in stateObject){
                    cacheData.statePopped = true;
                    methods.resetFilter();
                    return;
                }
                if ('L' in stateObject){
                    cacheData.statePopped = true;
                    if ('rePosition' in stateObject && stateObject.rePosition !== null){
                        cacheData.rePosition = stateObject.rePosition;
                    }
                } else {
                    cacheData.statePopped = false;
                }
            }
            methods.loadData(stateObject, newCallback);
        },
        "setStatePopped": function(){
            cacheData.statePopped = true;
        },
        "getState": function(){
            // shallow clone
            return $.extend({}, cacheData.lastKnownState);
        },
        "getQueryString": function(){
            return cacheData.lastKnownQueryString;
        }
    };
}();

CAUSETOWN.ProposedEvents = function(){
    var defaults = {
            "proposedEventsURL": "",
            "noEventsMessage":"",
            "pid":"",
            "historyManager": {}
        },
        settings = defaults,
        pageElements = {
            "eventList": $("#proposed-event-list")
        },
        cacheData = {
            "startIndex": 0,
            "pageLength": 10,
            "pageLoaded": false,
            "statePopped": false,
            "rePosition": false,
            "lastKnownState": {},
            "lastKnownQueryString": ""
        },
        methods = {
            "init": function(options){
                settings = $.extend({}, defaults, options);

                pageElements.eventList.on("click", ".event-nav", function(e){
                    var self = $(this),
                        buttons = pageElements.eventList.find(".pagination-buttons");
                    e.preventDefault();
                    cacheData.startIndex = self.attr("data-start");
                    cacheData.rePosition = 1;
                    buttons.find(".btn").hide();
                    buttons.find(".loading-spinner").show();
                    methods.loadData(null, function(){
                        methods.saveHistoryState();
                    });
                });
            },
            "submitSearch": function(callbackFunction){
                var criteria = {
                        "startIndex": cacheData.startIndex,
                        "pageLength": cacheData.pageLength
                    },
                    queryString = $.param(criteria),
                    stateObject = $.extend({}, criteria);
                if (pageElements.eventList.is(":visible") && settings.proposedEventsURL && criteria.pageLength){
                    criteria.pid = settings.pid; // don't put this in url
                    stateObject.pid = settings.pid;
                    stateObject.rePosition = cacheData.rePosition;
                    stateObject.queryString = queryString;
                    $.ajax({
                        url: settings.proposedEventsURL,
                        data: criteria,
                        type: 'POST',
                        dataType: 'html',
                        success: function(data){
                            cacheData.lastKnownState = stateObject;
                            cacheData.lastKnownQueryString = queryString;
                            if (data){
                                pageElements.eventList.html(data);
                            } else {
                                pageElements.eventList.html(settings.noEventsMessage);
                            }
                            if (!cacheData.statePopped){
                                if (!cacheData.pageLoaded){
                                    cacheData.lastKnownQueryString = ""; // never set query string on initial load
                                    cacheData.rePosition = 0;
                                    cacheData.pageLoaded = true;
                                } else if (queryString){
                                    // default behavior
                                } else {
                                    cacheData.lastKnownQueryString = ""; // no query string anyway; set it to string
                                }
                            }
                            if ($.isFunction(callbackFunction)){
                                callbackFunction(stateObject);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown ){
                            // not being handled
                        }
                    });
                }
            },
            "loadData": function(stateObject, callbackFunction){
                if (stateObject){
                    cacheData.startIndex = stateObject.startIndex;
                    cacheData.rePosition = stateObject.rePosition !== null ? stateObject.rePosition : cacheData.rePosition;
                }
                if (!cacheData.statePopped || stateObject){
                    methods.submitSearch(function(newStateObject){
                        var eventListPosition = pageElements.eventList.offset();
                        if (cacheData.rePosition){
                            window.scrollTo(eventListPosition.left,eventListPosition.top);
                            cacheData.rePosition = 0;
                        }
                        if ($.isFunction(callbackFunction)){
                            callbackFunction(newStateObject);
                        }
                    });
                }
            },
            "saveHistoryState": function(){
                var historyManager = settings.historyManager;
                if (historyManager &&
                    'saveHistoryState' in historyManager && $.isFunction(historyManager.saveHistoryState) &&
                    'setNewState' in historyManager && $.isFunction(historyManager.setNewState)){
                    historyManager.setNewState();
                    historyManager.saveHistoryState();
                }
            }
        };

    return {
        "init": function(options){
            methods.init(options);
        },
        "loadData": function(stateObject, callbackFunction){
            var newCallback = function(newStateObject){
                cacheData.pageLoaded = true;
                cacheData.statePopped = false;
                if ($.isFunction(callbackFunction)){
                    callbackFunction(newStateObject);
                }
            };
            if (stateObject){
                if ('pageLength' in stateObject){
                    cacheData.statePopped = true;
                }
            } else {
                cacheData.statePopped = false;
            }
            methods.loadData(stateObject, newCallback);
        },
        "setStatePopped": function(){
            cacheData.statePopped = true;
        },
        "getState": function(){
            // shallow clone
            return $.extend({}, cacheData.lastKnownState);
        },
        "getQueryString": function(){
            return cacheData.lastKnownQueryString;
        }
    };
}();