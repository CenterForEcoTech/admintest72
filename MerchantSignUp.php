<?php
include_once("Global_Variables.php");
$companyPid = $_GET['confirm'] ? htmlspecialchars($_GET['confirm']) : "";
?>
<form id="sign-up-form" class="override_skel hidden">
    <input id="company_pid" name="company_pid" type="hidden" value="<?php echo $companyPid;?>">
    <input id="company_officialname" name="company_officialname" type="hidden">
    <input id="CountryName" name="company_country" type="hidden" value="US">
    <input id="Lat" name="company_lat" type="hidden">
    <input id="Long" name="company_long" type="hidden">
    <input id="CityGridIDName" name="company_citygridname" type="hidden">
    <input id="CityGridProfileIDName" name="company_citygridid" type="hidden">
    <input id="Neighborhood" name="company_neighborhood" type="hidden">
    <input id="CityGridCategories" name="company_keywords" type="hidden">
    <fieldset id="merchant-search" >
            <a href="stopRegister" class="close-register do-not-navigate help" title="Close Register Form" style="float:right;">Cancel</a>
        <h3 id="register-header">Register as a Business</h3>
        <span id="reg-alert" class="alert"></span>
        <div>
            <p class="help eleven columns">Start by entering your zip code and searching for your business.</p>
            <?php include("_cityGridSearchModule.php"); ?>

            <label id="manual-button" class="three columns alpha omega">
            <span class="btn right administer">
                <a href="addManually" id="no-can-find" class="do-not-navigate" title="Can&apos;t find your business?">Add Manually</a>
            </span>
            </label>
        </div>
        <p style="clear:both;float:right;">Already registered? <a class="button login-action-from-register" href="#login">Log in now or get a password reset link</a>.</p>
        <div id="login-actions"></div>
    </fieldset>
    <div class="eleven columns alpha">
        <div id="address-display">
            <fieldset class="address hidden ten columns">
                <div class="seven columns">
                    <p class="business-name"></p>
                    <p class="street-address"></p>
                    <p class="city-state"></p>
                    <p class="business-phone"></p>
					<p class="business-website"></p>
                </div>

                <div class="three columns">
                    <span class="btn right administer">
                        <a href="editLocation" id="edit-business-address" class="do-not-navigate">Edit Address</a>
                    </span>
                </div>
                <div class="ten columns"><span id="already-registered-message"></span></div>
            </fieldset>
        </div>
    </div>

                <span id="cancel-registration-button" class="btn right reset" style="margin-bottom:10px;">
                    <a href="stopRegister" id="cancel-registration" class="close-register do-not-navigate" title="Close Register Form">Cancel</a>
                </span>

    <div id="editable-fields" class="hidden eleven columns alpha omega">
        <fieldset class="ten columns">
            <div class="legend">Business Name</div>
            <div class="ten columns alpha omega">
                <span class="btn right administer">
                    <a href="changeLocation" id="not-my-business" class="do-not-navigate" title="This is not my business">Edit Name</a>
                </span>
                <label class="manual hidden">
                    <span class="highlight">
                        <input type="text" id="company_name" name="company_name" >
                    </span>
                    <span class="alert"></span>
                </label>
                <p class="business-name"></p>
                <div class="legend">Business Address Information</div>
                <span class="btn right">
                    <a href="editLocation" id="finish-edit-business-address" class="do-not-navigate">Done Editing</a>
                </span>
                <label>
                    <div>Address:</div>
                    <span class="highlight">
                        <input type="text" id="company_address" name="company_address" >
                    </span>
                    <span class="alert"></span>
                </label>
                <label>
                    <div>City:</div>
                    <span class="highlight">
                        <input type="text" id="company_city" name="company_city" >
                    </span>
                    <span class="alert"></span>
                </label>
                <label>
                    <div>State:</div>
                    <span class="highlight">
                        <input type="text" id="company_state" name="company_state" >
                    </span>
                    <span class="alert"></span>
                </label>
                <label>
                    <div>Zip:</div>
                    <span class="highlight">
                        <input type="text" id="company_zip" name="company_zip" >
                    </span>
                    <span class="alert"></span>
                </label>
                <label>
                    <div>Business Phone:</div>
                    <span class="highlight">
                        <input type="text" id="company_phone" name="company_phone" >
                    </span>
                    <span class="alert"></span>
                </label>
                <label>
                    <div>Website:</div>
                    <span class="highlight">
                        <input type="text" id="Website" name="company_website" >
                    </span>
                    <span class="alert"></span>
                </label>
				<label style="display:none;">
					<div>Business Description:</div>
					<span class="highlight">
						<textarea id="company_description" name="company_description"></textarea>
					</span>
					<div class="alert"></div>
				</label>
            </div>
        </fieldset>
    </div>

    <div id="contact-info" class="eleven columns alpha omega hidden">
        <fieldset class="ten columns">
        <span class="btn right administer hidden">
            <a href="editContactInfo" id="edit-contact-info" class="do-not-navigate" title="View/Modify contact information">Edit Contact Info</a>
        </span>
        <div class="legend">Primary Contact for <?php echo $SiteName;?> Events</div>
        <p class="help nine columns">For use by <?php echo $SiteName;?> staff only</p>
        <label>
            <div>First Name:</div>
            <span class="highlight">
                <input type="text" id="company_contactfirstname" name="company_contactfirstname" >
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>Last Name:</div>
            <span class="highlight">
                <input type="text" id="company_contactlasstname" name="company_contactlasstname" >
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>Title:</div>
            <span class="highlight">
                <input type="text" id="company_contacttitle" name="company_contacttitle" class="auto-watermarked" title="Manager, Owner, etc.">
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>Contact Phone:</div>
            <span class="highlight">
                <input type="text" id="company_contactphone" name="company_contactphone">
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>Email:</div>
            <span class="highlight">
                <input type="text" id="company_contactemail" name="company_contactemail" class="auto-watermarked" title="email@domain.com">
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>Email Confirm:</div>
            <span class="highlight">
                <input type="text" id="company_contactemailconfirm" name="company_contactemailconfirm" >
            </span>
            <span class="alert"></span>
        </label>
        <label>
            <div>
				Referral Code:
				<a href="#referral-code-help" data-target="referral-code" class="colorbox-inline help-modal-icon">Help on Referral Code</a>
			</div>
            <span id="submitting-message" style="float:right" class="notice info hidden">Processing . . .</span>
            <span class="alert"></span>
            <div class="five columns alpha omega">
                <?php
                $referralCookieValue = $_COOKIE['CETDashPID'] ? $_COOKIE['CETDashPID'] : $_COOKIE['CETDashCampaignCode'];
                ?>
                <span class="highlight">
                    <input type="text" id="company_referralcode" name="company_referralcode" class="no-reset blur-form" value="<?php echo $referralCookieValue;?>" />
                </span>
                <span class="alert"></span>
            </div>
            <div class="five columns alpha omega">
                <span class="btn right">
                    <a href="submitReg" id="submit-registration" class="do-not-navigate" title="Click to get your activation email now.">Register Now</a>
                </span>
                <span class="btn right hidden">
                    <a href="hideSection" id="hide-contact-section" class="do-not-navigate" title="Hide this section">Done Editing</a>
                </span>
            </div>

        </label>
            <div id="agreement-text" style="float:right;text-align:right;">
                <?php echo render_template("TermsAgreement.snippet", array( "siteName" => $SiteName, "serverUrl" => $CurrentServer )); ?>
            </div>
        </fieldset>
    </div>
</form>
<span class="js-warning alert">Please enable javascript to continue.</span>
<div style="display:none;">
    <div id="referral-code-help" class="help-modal">

        <h4>Referral Code</h4>
        <p>
            This field is optional.
		</p>
		<p>
			Referral codes help us understand how you heard about <?php echo $SiteName;?> and to customize your experience according to the campaign
			or nonprofit partner that invited you to <?php echo $SiteName;?>. If this field is automatically pre-populated when you register, 
			please leave the code that appears. If you were referred to <?php echo $SiteName;?> by an organization and no value appears in this field, 
			you may ask the agency for its referral code and insert it into the box.
        </p>
    </div>
</div>
<?php
/**
 * ============================
 * Standard Profile Form
 * ============================
 */
?>
<script>
    $(".js-warning").hide();
    $("#sign-up-form").show();

    $(function() {
        var emailReg = /^([^@\s]+@([\w-]+\.)+[\w-]{2,4})?$/,

        // login availability
            loginAvailUrl = "<?php echo $CurrentServer;?>MobileChangeLogin",
            loginAvailableString = "Login Available",
            checkLoginAvail = function(value, alertElement, element, confirmElement){
                $.get(loginAvailUrl,
                    {
                        IndividualLogin: value
                    },
                    function(data){
                        if (data !== loginAvailableString){
                            alertElement.html(data);
                            if (confirmElement){
                                confirmElement.val("");
                            }
                            element.focus();
                        }
                    });
            };

        $("#company_contactemail").blur(function(){
            var el = $(this),
                elId = el.attr("id"),
                confirmEl = $("#" + elId + "Confirm"),
                value = el.val(),
                alertElement = el.closest("label").find(".alert");
            alertElement.html("");

            if ($.trim(value) && emailReg.test(value)){
                checkLoginAvail(value, alertElement, el, confirmEl);
            }
        });

        $("#company_contactphone, #company_phone").blur(function(){
            var el = $(this),
                value = el.val().replace(/[^\d]/g, ""),
                alertElement = el.closest("label").find(".alert"),
                areacodes = Array(<?php echo $Config_areacodes;?>),
                areacode = Number(value.substr(0,3)),
                areacodeFound = $.inArray(areacode, areacodes) >= 0;
            alertElement.html("");

            el.val(value); // replace with just numbers

            if (value){
                //check to see if first three digits are a valid area code
                if (!areacodeFound){
                    alertElement.html("Area Code is invalid");
                    el.focus();
                    return false;
                }else {
                    if (value.length != 10){
                        if (value.length < 10){
                            alertElement.html("Phone numbers must be 10 digits");
                            el.focus();
                            return false;
                        }else{
                            alertElement.html('Phone numbers must be 10 digits.\r\nPhone number has been truncated.');
                            el.val(value.substr(0,10));
                            return false;
                        }
                    } else {
                        if (el.attr("id") === "company_contactphone"){
                            checkLoginAvail(value, alertElement, el);
                        }
                        return true;
                    }
                }
            }else{
                return true;
            }
        });

        // page init

        jQuery.validator.addMethod("isstate", function(value) {
            value = value.replace(/\./g, "");
            document.getElementById('company_state').value=value;
            var states = ['AK','AL','AR','AS','AZ','CA','CO','CT','DC','DE','FL','GA','GU','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MH','MI','MN','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','PR','PW','RI','SC','SD','TN','TX','UT','VA','VI','VT','WA','WI','WV','WY','ALABAMA','ALASKA','ARIZONA','ARKANSAS','CALIFORNIA','COLORADO','CONNECTICUT','DELAWARE','FLORIDA','GEORGIA','HAWAII','IDAHO','ILLINOIS','INDIANA','IOWA','KANSAS','KENTUCKY','LOUISIANA','MAINE','MARYLAND','MASSACHUSETTS','MICHIGAN','MINNESOTA','MISSISSIPPI','MISSOURI','MONTANA','NEBRASKA','NEVADA','NEW HAMPSHIRE','NEW JERSEY','NEW MEXICO','NEW YORK','NORTH DAKOTA','NORTH CAROLINA','OHIO','OKLAHOMA','OREGON','PENNSYLVANIA','RHODE ISLAND','SOUTH CAROLINA','SOUTH DAKOTA','TENNESSEE','TEXAS','UTAH','VERMONT','VIRGINIA','WASHINGTON','WEST VIRGINIA','WISCONSIN','WYOMING']
            var in_array = $.inArray(value.toUpperCase(), states);
            if (in_array == -1) {
                return false;
            }else{
                return true;
            }
        }, "Data provided is not a valid US State");

        $.validator.addMethod("phoneUS", function(value, element){
            var numValue = value.replace(/[^\d]/g, ""),
                areacodes = Array(<?php echo $Config_areacodes;?>),
                areacode = Number(value.substr(0,3)),
                areacodeFound = $.inArray(areacode, areacodes) >= 0;

            if (numValue){
                //check to see if first three digits are a valid area code
                if (!areacodeFound){
                    return false;
                }else {
                    if (numValue.length != 10){
                        return false;
                    } else {
                        return true;
                    }
                }
            }else{
                return true;
            }
        }, "Must be a valid phone number in a valid area code.");

        $("#sign-up-form").validate({
            rules: {
                company_contactemailconfirm: {
                    equalTo: "#company_contactemail",
                    required: true,
                    email: true
                },
                company_contactemail: {
                    required: true,
                    email: true
                },
                company_contactphone: {required: true, phoneUS: true},
                company_contactlasstname: {required: true},
                company_contactfirstname: {required: true},
                company_phone: {required: true, phoneUS: true},
                company_zip: {required: true,digits: true,minlength:5,maxlength:5},
                company_state: {required: true,isstate: true},
                company_city: {required: true},
                company_address: {required: true},
                company_name: {required: true}
            },
            messages: {
                company_contactemail: {
                    required: "The Email is required.",
                    email: "A valid Email is essential."
                },
                company_contactemailconfirm: {
                    equalTo: "Please use the same email as above.",
                    required: "Please verify the email address.",
                    email: "You have not entered a recognizable email address."
                }
            }
        });
        //end validation rules
    });
</script>
<script type="text/javascript">
    var CETDASH = CETDASH || {};
    CETDASH.RegForm = CETDASH.RegForm || {};
    CETDASH.RegForm.echoMessage = function(key, message){
        var form = $("#sign-up-form"),
            inputElement,
            alertElement,
            alertMessage = message;
        if (message){
            alertMessage = message;
            if (key){
                inputElement = form.find("input[name='"+key+"']");
                if (inputElement){
                    alertElement = inputElement.closest("label").find(".alert");
                }
            }
        } else {
            alertMessage = key;
        }
        if (alertElement == undefined){
            alertElement = $("#reg-alert");
        }
        alertElement.html(alertMessage);
        CETDASH.scrollTo(alertElement.prev("h3"));
    };
    CETDASH.RegForm.clearMessage = function(key){
        var form = $("#sign-up-form"),
            inputElement = form.find("input[name='"+key+"']"),
            alertElement;
        if (inputElement){
            alertElement = inputElement.closest("label").find(".alert");
            alertElement.html("");
        } else {
            $(".alert").each(function(){
                $(this).html("");
            });
        }
    };
    CETDASH.RegForm.setAddressDataCallback = CETDASH.RegForm.setAddressDataCallback || function(data){};
    CETDASH.RegForm.setAddressDisplay = function(data){
        var wrap = $("#address-display .address"),
            name = wrap.find(".business-name"),
            editFormName = $("#editable-fields .business-name"),
            street = wrap.find(".street-address"),
            city = wrap.find(".city-state"),
            phone = wrap.find(".business-phone"),
            website = wrap.find(".business-website"),
            formattedPhone,invalidBusinessFields;
        $("already-registered-message").html("");
        if (data){
            formattedPhone = data.Phone ? data.Phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1.$2.$3") : "";
            name.html(data.BizName);
            editFormName.html(data.BizName);
            street.html(data.Address);
            city.html(data.City + ", " + data.State + " " + data.Zip);
            phone.html(formattedPhone);
            website.html(data.Website);
            wrap.slideDown();
            $("#editable-fields").find("label.error").removeClass("error");
            if (!$("#sign-up-form").valid()){
                invalidBusinessFields = $("#editable-fields").find("label.error");
                if (invalidBusinessFields.length){
                    $("#edit-business-address").click();
                    return false;
                }
            }
        } else {
            if (wrap.is(":visible")){
                wrap.slideUp();
            }
            name.html("");
            street.html("");
            city.html("");
            phone.html("");
            website.html("");
        }
        if ($.isFunction(CETDASH.RegForm.setAddressDataCallback)){
            CETDASH.RegForm.setAddressDataCallback(data);
        }
        return true;
    };
    CETDASH.RegForm.getForm = function(){
        return $("#sign-up-form");
    }
    // callbacks
    CETDASH.RegForm.cancelHandler = CETDASH.RegForm.cancelHandler || {};
    CETDASH.RegForm.successHandler = CETDASH.RegForm.successHandler || {};
    CETDASH.RegForm.onLoadHandler = CETDASH.RegForm.onLoadHandler || {};
    CETDASH.RegForm.submitHandler = CETDASH.RegForm.submitHandler || function(e){
        var regForm = $("#sign-up-form"),
            cancelButtons = regForm.find(".close-register"),
            merchantRegRecord = $("#sign-up-form").serialize(),
            signUpURL = "<?php echo $CurrentServer;?>MerchantSignUp",
            submitButtonWrap = $("#submit-registration").closest(".btn"),
            agreementText = $("#agreement-text"),
            submittingMessage = $("#submitting-message");

        submitButtonWrap.hide();
        cancelButtons.hide();
        agreementText.hide();
        submittingMessage.show();
        CETDASH.RegForm.clearMessage();
        if (regForm.valid()){
            $.ajax({
                url: signUpURL,
                type: "POST",
                data: merchantRegRecord,
                dataType: "json",
                success: function(data){
                    var addressData;
                    if (data.record && data.record.id){
                        // success!
                        addressData = {
                            "BizName": data.company.name,
                            "Phone": data.company.phone,
                            "Address": data.company.address,
                            "City": data.company.city,
                            "State": data.company.state,
                            "Zip": data.company.postalCode,
                            "Website": data.company.website
                        };
                        $("#editable-fields").slideUp(function(){
                            $("#editable-fields").remove();
                        });
                        $("#contact-info").slideUp(function(){
                            $("#contact-info").remove();
                        });
                        cancelButtons.remove();
                        $("#edit-business-address").remove();
                        CETDASH.RegForm.setAddressDisplay(addressData);
                        if ($.isFunction(CETDASH.RegForm.successHandler)){
                            CETDASH.RegForm.successHandler(data);
                        }
                    } else {
                        CETDASH.RegForm.echoMessage(data.key, data.message);
                        submitButtonWrap.show();
                        submittingMessage.hide();
                    }
                }
            });
        }
    };
    CETDASH.RegForm.onOpenEdit = CETDASH.RegForm.onOpenEdit || {};
    CETDASH.RegForm.checkRegistration = function(successCallback, failCallback){
        var checkRegistrationURL = "<?php echo $CurrentServer;?>ApiMerchants",
            regForm = $("#sign-up-form"),
            checkRegistrationData = {
                "company_pid":regForm.find("input[name='company_pid']").val(),
                "company_citygridid":regForm.find("input[name='company_citygridid']").val(),
                "company_name":regForm.find("input[name='company_name']").val(),
                "company_city":regForm.find("input[name='company_city']").val(),
                "company_state":regForm.find("input[name='company_state']").val(),
                "action": "check"
            };

        $.ajax({
            url: checkRegistrationURL,
            data: checkRegistrationData,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function(data){
                <?php
                    // TODO: either integrate this template into the event_template exclusively, or find a way
                    //       to inject the alert message text in case this template is used for other purposes.
                ?>
                var alertMessageTemplate = "It seems that your business has already registered with <?php echo $SiteName;?>. \
                                Please <a href='#login' class='login-action-from-register'>log in now</a> to create an event with this template.\
                                You may also click here to  <a href='{resetLink}' class='colorbox-ajax '>get a password reset link</a> at the email address associated with this business.",
                    message;
                $("#already-registered-message").html("");
                if (data.isRegistered && data.resetLink){
                    // merchant already registered
                    message = alertMessageTemplate.replace(/{resetLink}/g, data.resetLink);
                    $("#already-registered-message").html(message);
                    $("#edit-business-address").hide();
                    if ($.isFunction(failCallback)){
                        failCallback(data);
                    }
                } else {
                    if (data.pid){
                        $("#company_pid").val(data.pid);
                    }
                    $("#merchant-search").slideUp();
                    $("#contact-info").slideDown();
                    $("#company_contactfirstname").focus();
                    if ($.isFunction(CETDASH.RegForm.onOpenEdit)){
                        CETDASH.RegForm.onOpenEdit();
                    }
                    if ($.isFunction(successCallback)){
                        successCallback(data);
                    }
                }
            }
        });
    };
    $(function(){
        var regForm = $("#sign-up-form"),
            addressElement = $("#company_address"),
            officialnameElement = $("#company_officialname"),
            cityElement = $("#company_city"),
            stateElement = $("#company_state"),
            postalCodeElement = $("#company_zip"),
            phoneElement = $("#company_phone"),
            contactPhoneElement = $("#company_contactphone"),
            origContactPhoneValue,
            websiteElement = $("#Website"),
			descriptionElement = $("#company_description"),
            nameElement = $("#company_name"),

            latElement = $("#Lat"),
            longElement = $("#Long"),
            cityGridIDElement = $("#CityGridIDName"),
            cityGridProfileIdElement = $("#CityGridProfileIDName"),
            neighborhoodElement = $("#Neighborhood"),
            keywordElement = $("#CityGridCategories"),
            dataIsDirty = false,

            setCityGridData = function(cityGridData){
                if (cityGridData){
                    dataIsDirty = true;
                    nameElement.val(cityGridData.BizName);
                    addressElement.val(cityGridData.Address);
                    cityElement.val(cityGridData.City);
                    stateElement.val(cityGridData.State);
                    officialnameElement.val(cityGridData.BizName);
                    postalCodeElement.val(cityGridData.Zip);
                    latElement.val(cityGridData.Lat);
                    longElement.val(cityGridData.Long);
                    cityGridIDElement.val(cityGridData.CityGridID);
                    cityGridProfileIdElement.val(cityGridData.CityGridProfileID);
                    neighborhoodElement.val(cityGridData.Neighborhood);
                    keywordElement.val(cityGridData.commaDelimitedCategories);
                    phoneElement.val(cityGridData.Phone);
                    contactPhoneElement.val(cityGridData.Phone);
                    origContactPhoneValue = cityGridData.Phone;
                    websiteElement.val(cityGridData.Website);
                    descriptionElement.val(cityGridData.Description);
                } else {
                    nameElement.val("");
                    addressElement.val("");
                    cityElement.val("");
                    stateElement.val("");
                    officialnameElement.val("");
                    postalCodeElement.val("");
                    latElement.val("");
                    longElement.val("");
                    cityGridIDElement.val("");
                    cityGridProfileIdElement.val("");
                    neighborhoodElement.val("");
                    keywordElement.val("");
                    phoneElement.val("");
                    contactPhoneElement.val("");
                    websiteElement.val("");
                    descriptionElement.val("");
                    dataIsDirty = false;
                }
                CETDASH.RegForm.setAddressDisplay(cityGridData);
            },

            setupManualEdit = function(){
                $("#not-my-business").hide();
                $("#finish-edit-business-address").css("visibility", "none");
                $("#editable-fields .business-name").hide();
                $("#editable-fields .manual").show();
                $("#sign-up-form input").each(function(){
                    var element = $(this);
                    if (!element.hasClass("no-reset")){
                        element.val("");
                    }
                });
                $("#sign-up-form").valid();
                $("#company_name").focus();
                if ($.isFunction(CETDASH.RegForm.onOpenEdit)){
                    CETDASH.RegForm.onOpenEdit();
                }
            },

            merchantAutoCompleteOnSelect = function(event, ui) {
                var lookupURL = '<?php echo $CurrentServer;?>Library/BizNameInfoJSON?CityGridProfileID='+ui.item.CityGridProfileID;

                $.getJSON(lookupURL, function(data) {
                    var businessData = ui.item;
                    if (data.length){
                        businessData.Website = data[0].Website;
                        businessData.commaDelimitedCategories = data[0].commaDelimitedCategories;
                    }
                    setCityGridData(businessData);
                    CETDASH.RegForm.checkRegistration();
                });
            },

            merchantAutoCompleteBeforeSearch = function(){
                if (dataIsDirty){
                    setCityGridData();
                }
            };

        CETDASH.BusinessLookup.apply({
            apiUrl : "<?php echo $CurrentServer;?>Library/BizLookup",
            beforeSearch: merchantAutoCompleteBeforeSearch,
            onSelect : merchantAutoCompleteOnSelect
        });

        $("#edit-business-address").click(function(e){
            $("#finish-edit-business-address").show();
            $("#address-display").slideUp();
            $("#editable-fields").slideDown();
        });

        $("#finish-edit-business-address").click(function(e){
            var editedData = {
                "BizName": nameElement.val(),
                "Phone": phoneElement.val(),
                "Address": addressElement.val(),
                "City": cityElement.val(),
                "State": stateElement.val(),
                "Zip": postalCodeElement.val(),
                "Website": websiteElement.val()
            };
            if (CETDASH.RegForm.setAddressDisplay(editedData)){
                $("#address-display").slideDown();
                $("#editable-fields").slideUp();
            }
            if (nameElement.val()){
                $("#editable-fields .business-name").show();
                $("#editable-fields .manual").hide();
            }
            if (origContactPhoneValue == contactPhoneElement.val()){
                // user hasn't changed it, so go ahead and update it
                contactPhoneElement.val(phoneElement.val());
                origContactPhoneValue = phoneElement.val();
            }
        });

        $("#not-my-business").click(function(e){
            if (confirm("This will clear the form and require you to fill out the complete address. If you wish to make a minor edit, you may do so after completing the registration.\n\n Are you sure you want to clear the form?")){
                setupManualEdit();
            }
        });

        $("#no-can-find").click(function(e){
            $("#merchant-search").slideUp();
            $("#editable-fields").slideDown();
            $("#contact-info").slideDown();
            setupManualEdit();
        });

        regForm.on("click", ".close-register", function(e){
            if ($.isFunction(CETDASH.RegForm.cancelHandler)){
                CETDASH.RegForm.cancelHandler();
            }
        });

        $("#submit-registration").click(function(e){
            regForm.submit(e);
        });

        regForm.submit(function(e){
            e.preventDefault();
            if (regForm.valid()){
                if ($.isFunction(CETDASH.RegForm.submitHandler)){
                    CETDASH.RegForm.submitHandler();
                } else {
                    return true;
                }
            }
        });

        if ($.isFunction(CETDASH.RegForm.onLoadHandler)){
            CETDASH.RegForm.onLoadHandler();
        }
    });
</script>