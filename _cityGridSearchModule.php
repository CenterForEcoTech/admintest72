<?php
/**
 * see MerchantSignUp.php for example usage
 *
 */

if (!isset($locationLabel)){
    $locationLabel = "Zip";
}
if (!isset($nameLabel)){
    $nameLabel = "Search Name";
}
if (!isset($nameWatermark)){
    $nameWatermark = "search by name";
}

?>
<label class="four columns alpha">
    <div class="legend"><?php echo $locationLabel;?></div>
    <span class="highlight">
        <input type="text" id="city-grid-zip" class="five columns auto-watermarked" title="zip code" />
    </span>
</label>

<label class="four columns">
    <div class="legend"><?php echo $nameLabel;?></div>
    <span class="highlight ui-front">
        <input type="text" id="city-grid-name" class="five columns auto-watermarked auto-complete" title="<?php echo $nameWatermark;?>" />
    </span>
    <div class="third-party-attribution">Powered by <img src="<?php echo $CurrentServer;?>images/citygrid_logo.jpg" style="height:15px;"></div>
</label>
<?php if (!isAjaxRequest() && !$isAdminConsole) {
    $Footer_excludeJqueryUi = true;
?>
    <script src="<?php echo $CurrentServer;?>js/jquery-ui-1.8.20.custom.min.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo $CurrentServer;?>js/causetown.businesslookup.js"></script>
<script type="text/javascript">
    CETDASH.BusinessLookup.init({
        nameSelector : "#city-grid-name",
        locationSelector : "#city-grid-zip",
        apiUrl : "<?php echo $CurrentServer;?>Library/BizLookup"
    }, true); // true resets the module
</script>