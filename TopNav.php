<ul class="hidden">
	<?php
    include_once("Global_Variables.php");
	$homeURL = $CurrentServer;
	?>
</ul>
<?php
// NOTICE: Switching the logo here does not work: you must make modifications to the css as well as the height of the interior image below.
//          See the commit diffs for 6/25/2012 for specific details.
// NOTICE: Switching the logo here was definitely NOT a simple switch. next time look at commits for 2/22/2013.
$officialLogo = "images/logo.png";
$betaLogo = "images/logo_beta.png";
?>
