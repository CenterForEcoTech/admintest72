<?php
// create site nav
if (isLoggedIn()) {
	$showChangePassword = isset($showChangePassword) && $showChangePassword;
    $createEventsURL = $CurrentServer."CreateEvent";
    $createEventsText = "Create Events";
	if (isOrganization()){
		$myEventsURL = $CurrentServer."Organization/YourEvents";
		$myProfileURL = $CurrentServer."Organization/Profile";
		$FindEventsURL = $CurrentServer."FindEvents";
        $createEventsURL = $CurrentServer."ProposeEvent";
        $createEventsText = "Propose Event";
		$showFindEvent = true;

		$extraClass = "sales";
		$extraURL = $CurrentServer."FindEvents";
		$extraText = "Find Events";
	}
	if (isMerchant()){
		$myEventsURL = $CurrentServer."Merchant/YourEvents";
		$myProfileURL = $CurrentServer."Merchant/Profile";
		$myProfileURL = $CurrentServer."ct/".SessionManager::getMerchantPID();
		$extraClass = "sales";
		$extraURL = $CurrentServer."Merchant/EnterSales";
		$extraText = "Enter Sales";
	}
	if (isIndividual()){
		$myEventsURL = $CurrentServer."Individual/YourEvents";
		$myProfileURL = $CurrentServer."Individual/Profile";
	}
	if (isLoggedIn()){
        ob_start();
		?>
	<ul class="site_nav">
		<?php if (isset($extraClass)){?>
			<li><a class="<?php echo $extraClass;?>" href="<?php echo $extraURL;?>" title="<?php echo $extraText; ?>"><span><?php echo $extraText;?></span></a></li>
		<?php }?>
		<li><a class="profile" href="<?php echo $myProfileURL;?>" title="My Profile"><span>My Profile</span></a></li>
		<li><a class="my_events" href="<?php echo $myEventsURL;?>" title="My Events"><span>My Events</span></a></li>
		<li><a class="create_events" href="<?php echo $createEventsURL;?>" title="<?php echo $createEventsText;?>"><?php echo $createEventsText;?></a></li>
		<li><a class="donations" href="<?php echo $CurrentServer;?>MyGivingBank" title="My <?php echo $givingBankName;?>"><span>My <?php echo $givingBankName;?></span></a></li>
		<?php if ($showChangePassword){?>
			<li><a class="password" href="<?php echo $CurrentServer;?>Password" title="Change Password"><span>Change Password</span></a></li>
		<?php }?>
	</ul>
	<?php

        $siteNav = ob_get_contents();
        ob_clean();
    }
}

// Render if there is anything to render
if ($customSideRailTop || $siteNav || $customSideRailBottom){
    echo "<div>"; // only create this div if there is content because otherwise, during responsive, it demands a minimum height creating massive white space
    if (isset($customSideRailTop)){ echo $customSideRailTop; }
    if (isset($siteNav)) {echo $siteNav;}
    if (isset($customSideRailBottom)){ echo $customSideRailBottom; }
    echo "</div>";
}
?>