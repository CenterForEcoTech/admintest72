<?php
//turn off all error reporting
error_reporting(0);
ini_set("display_errors", "on"); error_reporting(1);

//turn on all error reporting
//if not in use comment out next few lines
error_reporting(E_ALL & E_STRICT);
ini_set('display_errors', '1');
ini_set('log_errors', '0');
ini_set('error_log', './');
//end error reporting
//these variables are necessary for the User_register page
$host = $_SERVER['HTTP_HOST'];
$hostParts = explode(".", $_SERVER['HTTP_HOST']);
$subdomain = count($hostParts) > 1 ? strtolower($hostParts[0]) : "";
$self = $_SERVER['PHP_SELF'];

include_once($rootFolder.'config.php');
//DO NOT ADJUST THE FOLLOWING SYSTEM VARIABLES

//
// Session Management
//
include_once($rootFolder."repository/SessionManager.php");
//SessionManager::start("CETDash");  //was causing problems sharing sessions between pages.
session_start();

//set the default timezone
date_default_timezone_set('America/New_York');
$TodaysDate = date("n/j/Y");

//special functions created by Yosh for this site
require_once('functions.php');
//Variables from/for other pages
//$PageTitle = "-".GetParameter('PageTitle'); // make this context specific
$PageName = isset($_GET['PageName']) ? $_GET['PageName'] : "";
$EmailFooter = $SiteTitle." is Powered By ".$SiteName;
$Config_EmailFooter = $Config_EmailFooter."<br>".$SiteTitle." is Powered By ".$SiteName;

//set currency display
setlocale(LC_MONETARY, 'en_US');


//if just from loginvalidate page, then use the Members_StartPage as the start Page
if (isset($_GET['LoggedIn'])){
	$PageName = SessionManager::getStartPage();
	
	if (SessionManager::getLastPage()){
		$PageName = "LastPage";
	}
}

if(isLoggedIn()) {
	$welcomeName = SessionManager::getUserName()." ".SessionManager::getMerchantName().SessionManager::getOrganizationName();
}else{
	$welcomeName = "Guest";
}

//some server details, don't edit!

//If there is a PageName format it to display a php page otherwise use the Index Contact 
if ($PageName) {$PageName=$PageName.".php";} else {$PageName = "Index_content.php";}

//Make sure the user is logged in otherwise revert to Index
if (!$PageName && $welcomeName == "Guest") {
	$PageName = "Index_content.php";
}

//Drop in admin tools for error recording, etc
include_once($dbProviderFolder."AdminProvider.php");
$rootFolder = str_replace("repository/","",$rootFolder);
$AdminProvider = new AdminProvider($mysqli);
// TODO: remove direct calls to EventProvider.php from the site as wel as redudant calls to load config.php
require_once($repositoryApiFolder."config.php"); // turns out there's code on the site that is expecting this to load all the time
//Geolocation variables
$surface_distance_coeffient = 69.174;  //to convert to KM multiple by (1/0.6214)
$earths_radius = 3958; //to convert to KM multipley by (1/0.6214)
if ($Config_IndexDistanceType == "km"){
	$surface_distance_coeffient = $surface_distance_coeffient*(1/0.6214);
	$earths_radius = $earths_radius*(1/0.6214);
}
?>