<?php
if (!isLoggedIn()){
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		$PageTitle = "Create ".$SiteName." Listing";
		$PageHeader1 = "Log In or Register";
	}
	if ($load_getContent){
?>
	<p>
		A <?php echo $SiteName; ?> listing will provide the necessary information for people to know what they have found and how to use it.
		Please login at the top right of this page or register using one of the buttons below to
		get started.
	</p>
	<div id="register_now_buttons">
		<a href="<?php echo $CurrentServer;?>Organization/SignUp" class="non_profits"><span>Nonprofits</span></a>
		<a href="<?php echo $CurrentServer;?>Merchant/SignUp" class="local_businesses"><span>Businesses</span></a>
		<a href="<?php echo $CurrentServer;?>Individual/SignUp" class="consumers"><span>Individuals</span></a>
	</div>
	<?php SessionManager::setStartPage("CreateEvent_Start");?>
	<?php
	}
}else{
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
		if (isMerchant()){
			include('Merchant_CreateEvent_Dates.php');
		}
		if (isOrganization()){
			include('Organization_CreateEvent_Start.php');
		}
		if (isIndividual()){
			include('Individual_CreateEvent_Start.php');
		}
	}
	if ($load_getContent){
		if (isMerchant()){
			$MerchantID = getMerchantId();

			include('Merchant_CreateEvent_Dates.php');
			//$CreateEventPageToUse = 'Merchant_CreateEvent_Dates.php';
			//include('_CreateEvent_CheckPermissionToCreateEvent.php');

		}
		if (isOrganization()){
			include('Organization_CreateEvent_Start.php');
		}
		if (isIndividual()){
			include('Individual_CreateEvent_Start.php');
		}
	}
}//end if logged in
?>