ALTER TABLE `hr_employee_name` ADD `HREmployeeName_RevenueCode` VARCHAR(25) NULL DEFAULT NULL AFTER `HREmployeeName_HoursTrackingGHS`;

CREATE TABLE `hr_revenuecodes` (
  `HRRevenueCode_ID` int(11) NOT NULL,
  `HRRevenueCode_Code` varchar(25) DEFAULT NULL,
  `HRRevenueCode_Name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_revenuecodes`
--

INSERT INTO `hr_revenuecodes` (`HRRevenueCode_ID`, `HRRevenueCode_Code`, `HRRevenueCode_Name`) VALUES
(1, '40202', 'Technical Field Services'),
(2, '40205', 'Operations Services'),
(3, '40259', 'Marketing'),
(4, '40262', 'Information Services'),
(5, '40952', 'Passthrough'),
(6, '40220', 'ISM Labor'),
(7, '40222', 'ISM Materials');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hr_revenuecodes`
--
ALTER TABLE `hr_revenuecodes`
  ADD PRIMARY KEY (`HRRevenueCode_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hr_revenuecodes`
--
ALTER TABLE `hr_revenuecodes`
  MODIFY `HRRevenueCode_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;