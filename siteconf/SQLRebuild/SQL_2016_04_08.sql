-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2016 at 12:38 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cet_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `muni_rebate_codes`
--

DROP TABLE IF EXISTS `muni_rebate_codes`;
CREATE TABLE `muni_rebate_codes` (
  `MuniRebateCode_ID` int(2) NOT NULL,
  `MuniRebateCode_Type` varchar(50) DEFAULT NULL,
  `MuniRebateCode_PartCode` varchar(50) DEFAULT NULL,
  `MuniRebateCode_FieldName` varchar(100) DEFAULT NULL,
  `MuniRebateCode_Description` varchar(50) DEFAULT NULL,
  `MuniRebateCode_SavingsType` varchar(25) DEFAULT NULL,
  `MuniRebateCode_SavingsPerYear` varchar(25) DEFAULT NULL,
  `MuniRebateCode_PeakDemandSavingsPerYear` decimal(3,2) DEFAULT NULL,
  `MuniRebateCode_Lifetime` varchar(10) DEFAULT NULL,
  `MuniRebateCode_MCFSavings` varchar(25) DEFAULT NULL,
  `MuniRebateCode_LifetimeSavings` varchar(25) DEFAULT NULL,
  `MuniRebateCode_PeakDemandSavingsLifetime` decimal(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `muni_rebate_codes`
--

INSERT INTO `muni_rebate_codes` (`MuniRebateCode_ID`, `MuniRebateCode_Type`, `MuniRebateCode_PartCode`, `MuniRebateCode_FieldName`, `MuniRebateCode_Description`, `MuniRebateCode_SavingsType`, `MuniRebateCode_SavingsPerYear`, `MuniRebateCode_PeakDemandSavingsPerYear`, `MuniRebateCode_Lifetime`, `MuniRebateCode_MCFSavings`, `MuniRebateCode_LifetimeSavings`, `MuniRebateCode_PeakDemandSavingsLifetime`) VALUES
(1, 'Energy_Efficiency', 'BDAS', 'MuniResRebateEfficiency_AirSealingApprovedAmount', 'BLOWER DOOR AIR SEALING', '', '', NULL, '', '', '', NULL),
(2, 'Energy_Efficiency', 'INS', 'MuniResRebateEfficiency_InsulationApprovedAmount', 'INSULATION', '', '', NULL, '', '', '', NULL),
(3, 'Energy_Efficiency', 'HEAT', 'MuniResRebateEfficiency_HeatingApprovedAmount', 'HEATING SYSTEM', '', '', NULL, '', '', '', NULL),
(4, 'Pool_Pump', 'POOL', 'MuniResRebatePoolPump_NewManufacturer', 'POOL PUMP', 'kWh', '952', NULL, '10', '', '9520', NULL),
(5, 'Appliance', 'CW', 'MuniResRebateAppliance_Applianceclotheswasher', 'CLOTHES WASHER', 'kWh', '275', '0.04', '12', '', '3300', '0.48'),
(6, 'Appliance', 'REFRIG', 'MuniResRebateAppliance_Appliancerefrigerator', 'REFRIGERATOR', 'kWh', '118', '0.01', '12', '', '1416', '0.12'),
(7, 'Appliance', 'FREEZER', 'MuniResRebateAppliance_Appliancefreezer', 'FREEZER', 'kWh', '43.7', '0.01', '12', '', '524.4', '0.12'),
(8, 'Appliance', 'DW', 'MuniResRebateAppliance_Appliancedishwasher', 'DISHWASHER', 'kWh', '66', '0.01', '10', '', '660', '0.10'),
(9, 'Appliance', 'ROOMAC', 'MuniResRebateAppliance_Applianceroomac', 'ROOM AC', 'kWh', '113', '0.32', '9', '', '1017', '2.88'),
(10, 'Appliance', 'DH', 'MuniResRebateAppliance_Appliancedehumidifier', 'DEHUMIDIFIER', 'kWh', '239', '0.04', '12', '', '2868', '0.48'),
(11, 'Appliance', 'AP', 'MuniResRebateAppliance_Applianceairpurifier', 'AIR PURIFIER', 'kWh', '391', '0.08', '9', '', '3519', '0.72'),
(12, 'Appliance', 'HPWH', 'MuniResRebateAppliance_Applianceheatpumpwaterheater', 'HEAT PUMP WATER HEATER', 'kWh', '1654', '0.34', '10', '', '16540', '3.40'),
(13, 'AC_Heat_Pump', 'AC250', 'MuniResRebateCoolHome_Measure_CentralAC16Approved', 'CENTRAL AC 250', 'kWh', '198.8', '0.55', '16', '', '3180.8', '8.80'),
(14, 'AC_Heat_Pump', 'AC500', 'MuniResRebateCoolHome_Measure_CentralAC18Approved', 'CENTRAL AC 500', 'kWh', '198.8', '0.55', '16', '', '3180.8', '8.80'),
(15, 'AC_Heat_Pump', 'ASHP250', 'MuniResRebateCoolHome_Measure_CentralAC16Approved', 'AS HEAT PUMP 250', 'kWh', '450.3', '0.19', '14', '', '6304.2', '2.66'),
(16, 'AC_Heat_Pump', 'ASHP500', 'MuniResRebateCoolHome_Measure_CentralAC18Approved', 'AS HEAT PUMP 500', 'kWh', '1077.', '0.65', '16', '', '17244.8', '10.40'),
(17, 'AC_Heat_Pump', 'DMSHP250', 'MuniResRebateCoolHome_Measure_Ductless18Approved', 'DUCTLESS MINI SPLIT HP 250', 'kWh', '286', '0.34', '18', '', '5148', '6.12'),
(18, 'AC_Heat_Pump', 'DMSHP500', 'MuniResRebateCoolHome_Measure_Ductless20Approved', 'DUCTLESS MINI SPLIT HP 500', 'kWh', '330', '0.45', '18', '', '5940', '8.10'),
(19, 'Com_Heat_Hot_Water', 'COMFUR>95', 'MuniComRebateHeatHotwater_Measure_Furnace95Approved', 'FURNACE >95', 'MMBTU', '8.1', NULL, '17', '7.90236', '134.3401', NULL),
(20, 'Com_Heat_Hot_Water', 'COMFUR>97', 'MuniComRebateHeatHotwater_Measure_Furnace97Approved', 'FURNACE >97', 'MMBTU', '9.2', NULL, '17', '8.97552', '152.5838', NULL),
(21, 'Com_Heat_Hot_Water', 'COMBOIL>85', 'MuniComRebateHeatHotwater_Measure_Boiler85Approved', 'BOILER >85', 'MMBTU', '10.4', NULL, '20', '10.1462', '202.9248', NULL),
(22, 'Com_Heat_Hot_Water', 'COMBOIL>90', 'MuniComRebateHeatHotwater_Measure_Boiler90Approved', 'BOILER >90', 'MMBTU', '11.4', NULL, '20', '11.1218', '222.4368', NULL),
(23, 'Com_Heat_Hot_Water', 'COMBOIL>95', 'MuniComRebateHeatHotwater_Measure_Boiler95Approved', 'BOILER >95', 'MMBTU', '14.1', NULL, '20', '13.7559', '275.1192', NULL),
(24, 'Com_Heat_Hot_Water', 'COMINTWHBOILER>90', 'MuniComRebateHeatHotwater_Measure_Integrated90Approved', 'INTWHBOILER>90', 'MMBTU', '10.3', NULL, '19', '10.0486', '190.9249', NULL),
(25, 'Com_Heat_Hot_Water', 'COMINTWHBOILER>95', 'MuniComRebateHeatHotwater_Measure_Integrated95Approved', 'INTWHBOILER>95', 'MMBTU', '12.8', NULL, '19', '12.4876', '237.2659', NULL),
(26, 'Com_Heat_Hot_Water', 'COMIWH', 'MuniComRebateHeatHotwater_Measure_IndirectWaterApproved', 'INDIRECT WH', 'MMBTU', '8', NULL, '20', '7.8048', '156.096', NULL),
(27, 'Com_Heat_Hot_Water', 'COMSWH>62', 'MuniComRebateHeatHotwater_Measure_H2OStorage62Approved', 'STORAGE WH >.62', 'MMBTU', '3', NULL, '11', '2.9268', '32.1948', NULL),
(28, 'Com_Heat_Hot_Water', 'COMSWH>67', 'MuniComRebateHeatHotwater_Measure_H2OStorage67Approved', 'STORAGE WH >.67', 'MMBTU', '3.6', NULL, '11', '3.51216', '38.63376', NULL),
(29, 'Com_Heat_Hot_Water', 'COMTWH>82', 'MuniComRebateHeatHotwater_Measure_Tankless82Approved', 'TANKLESSWH>.82', 'MMBTU', '9.4', NULL, '19', '9.17064', '174.2421', NULL),
(30, 'Com_Heat_Hot_Water', 'COMTWH>94', 'MuniComRebateHeatHotwater_Measure_Tankless94Approved', 'TANKLESSWH>.94', 'MMBTU', '9.9', NULL, '19', '9.65844', '183.5103', NULL),
(31, 'Res_Heat_Hot_Water', 'RESFUR>95', 'MuniResRebateHeatHotwater_Measure_Furnace95Approved', 'FURNACE >95', 'MMBTU', '8.1', NULL, '17', '7.90236', '134.3401', NULL),
(32, 'Res_Heat_Hot_Water', 'RESFUR>97', 'MuniResRebateHeatHotwater_Measure_Furnace97Approved', 'FURNACE >97', 'MMBTU', '9.2', NULL, '17', '8.97552', '152.5838', NULL),
(33, 'Res_Heat_Hot_Water', 'RESBOIL>85', 'MuniResRebateHeatHotwater_Measure_Boiler85Approved', 'BOILER >85', 'MMBTU', '10.4', NULL, '20', '10.1462', '202.9248', NULL),
(34, 'Res_Heat_Hot_Water', 'RESBOIL>90', 'MuniResRebateHeatHotwater_Measure_Boiler90Approved', 'BOILER >90', 'MMBTU', '11.4', NULL, '20', '11.1218', '222.4368', NULL),
(35, 'Res_Heat_Hot_Water', 'RESBOIL>95', 'MuniResRebateHeatHotwater_Measure_Boiler95Approved', 'BOILER >95', 'MMBTU', '14.1', NULL, '20', '13.7559', '275.1192', NULL),
(36, 'Res_Heat_Hot_Water', 'RESINTWHBOILER>90', 'MuniResRebateHeatHotwater_Measure_Integrated90Approved', 'INTWHBOILER>90', 'MMBTU', '10.3', NULL, '19', '10.0486', '190.9249', NULL),
(37, 'Res_Heat_Hot_Water', 'RESINTWHBOILER>95', 'MuniResRebateHeatHotwater_Measure_Integrated95Approved', 'INTWHBOILER>95', 'MMBTU', '12.8', NULL, '19', '12.4876', '237.2659', NULL),
(38, 'Res_Heat_Hot_Water', 'RESIWH', 'MuniResRebateHeatHotwater_Measure_IndirectWaterApproved', 'INDIRECT WH', 'MMBTU', '8', NULL, '20', '7.8048', '156.096', NULL),
(39, 'Res_Heat_Hot_Water', 'RESSWH>62', 'MuniResRebateHeatHotwater_Measure_H2OStorage62Approved', 'STORAGE WH >.62', 'MMBTU', '3', NULL, '11', '2.9268', '32.1948', NULL),
(40, 'Res_Heat_Hot_Water', 'RESSWH>67', 'MuniResRebateHeatHotwater_Measure_H2OStorage67Approved', 'STORAGE WH >.67', 'MMBTU', '3.6', NULL, '11', '3.51216', '38.63376', NULL),
(41, 'Res_Heat_Hot_Water', 'RESTWH>82', 'MuniResRebateHeatHotwater_Measure_Tankless82Approved', 'TANKLESSWH>.82', 'MMBTU', '9.4', NULL, '19', '9.17064', '174.2421', NULL),
(42, 'Res_Heat_Hot_Water', 'RESTWH>94', 'MuniResRebateHeatHotwater_Measure_Tankless94Approved', 'TANKLESSWH>.94', 'MMBTU', '9.9', NULL, '19', '9.65844', '183.5103', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `muni_rebate_codes`
--
ALTER TABLE `muni_rebate_codes`
  ADD PRIMARY KEY (`MuniRebateCode_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `muni_rebate_codes`
--
ALTER TABLE `muni_rebate_codes`
  MODIFY `MuniRebateCode_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;