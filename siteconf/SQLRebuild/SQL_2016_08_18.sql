-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2016 at 04:24 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cet_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `gbs_corridor`
--

DROP TABLE IF EXISTS `gbs_corridor`;
CREATE TABLE `gbs_corridor` (
  `id` int(11) NOT NULL,
  `City` varchar(50) DEFAULT NULL,
  `State` varchar(2) DEFAULT NULL,
  `Zip` varchar(5) DEFAULT NULL,
  `Corridor` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gbs_corridor`
--

INSERT INTO `gbs_corridor` (`id`, `City`, `State`, `Zip`, `Corridor`) VALUES
(1, 'Avon', 'CT', '06001', 'Rt. 91'),
(2, 'Bethany', 'CT', '06524', 'Rt. 95'),
(3, 'Bethel', 'CT', '06801', 'Rt. 95'),
(4, 'Bloomfield', 'CT', '06002', 'Rt. 91'),
(6, 'Branford', 'CT', '06405', 'Rt. 95'),
(9, 'Bridgeport', 'CT', '06610', 'Rt. 95'),
(10, 'Bridgeport', 'CT', '06604', 'Rt. 95'),
(11, 'Bridgeport', 'CT', '06606', 'Rt. 95'),
(12, 'Bristol', 'CT', '06010', 'Rt. 91'),
(14, 'Brookfield', 'CT', '06804', 'Rt. 95'),
(15, 'Cheshire', 'CT', '06410', 'Rt. 95'),
(19, 'Colchester', 'CT', '06415', 'Rt. 95'),
(23, 'Cos Cob', 'CT', '06807', 'Rt. 95'),
(24, 'Cromwell', 'CT', '06416', 'Rt. 95'),
(26, 'Danbury', 'CT', '06810', 'Rt. 95'),
(31, 'Darien', 'CT', '06820', 'Rt. 95'),
(32, 'East Canaan', 'CT', '06024', 'Rt. 91'),
(33, 'East Hampton', 'CT', '06424', 'Rt. 95'),
(34, 'East Hartford', 'CT', '06108', 'Rt. 91'),
(36, 'East Haven', 'CT', '06512', 'Rt. 95'),
(40, 'Ellington', 'CT', '06029', 'Rt. 91'),
(41, 'Fairfield', 'CT', '06825', 'Rt. 95'),
(42, 'Farmington', 'CT', '06032', 'Rt. 91'),
(44, 'Glastonbury', 'CT', '06033', 'Rt. 91'),
(46, 'Granby', 'CT', '06035', 'Rt. 91'),
(47, 'Greenwich', 'CT', '06830', 'Rt. 95'),
(48, 'Guilford', 'CT', '06437', 'Rt. 95'),
(49, 'Hamden', 'CT', '06514', 'Rt. 95'),
(52, 'Hamden', 'CT', '06517', 'Rt. 95'),
(53, 'Hartford', 'CT', '06114', 'Rt. 91'),
(54, 'Hartford', 'CT', '06120', 'Rt. 91'),
(56, 'Hartford', 'CT', '06110', 'Rt. 91'),
(57, 'Litchfield', 'CT', '06759', 'Rt. 91'),
(58, 'Manchester', 'CT', '06042', 'Rt. 91'),
(61, 'Manchester', 'CT', '06040', 'Rt. 91'),
(63, 'Meriden', 'CT', '06450', 'Rt. 95'),
(64, 'Middletown', 'CT', '06457', 'Rt. 95'),
(65, 'Milford', 'CT', '06460', 'Rt. 95'),
(66, 'Milford', 'CT', '06461', 'Rt. 95'),
(73, 'Milldale', 'CT', '06467', 'Rt. 91'),
(74, 'Monroe', 'CT', '06468', 'Rt. 95'),
(76, 'New Britain', 'CT', '06051', 'Rt. 91'),
(77, 'New Haven', 'CT', '06512', 'Rt. 95'),
(78, 'New Haven', 'CT', '06511', 'Rt. 95'),
(79, 'New Haven', 'CT', '06519', 'Rt. 95'),
(80, 'New London', 'CT', '06320', 'Rt. 95'),
(81, 'New Milford', 'CT', '06776', 'Rt. 91'),
(82, 'Newtown', 'CT', '06470', 'Rt. 95'),
(83, 'North Branford', 'CT', '06471', 'Rt. 95'),
(84, 'North Haven', 'CT', '06473', 'Rt. 95'),
(85, 'North Windham', 'CT', '06256', 'Rt. 91'),
(86, 'Norwalk', 'CT', '06851', 'Rt. 95'),
(87, 'Norwalk', 'CT', '06854', 'Rt. 95'),
(89, 'Norwich', 'CT', '06360', 'Rt. 95'),
(92, 'Old Lyme', 'CT', '06371', 'Rt. 95'),
(93, 'Oxford', 'CT', '06478', 'Rt. 95'),
(94, 'Plainville', 'CT', '06062', 'Rt. 91'),
(96, 'Plantsville', 'CT', '06479', 'Rt. 91'),
(99, 'Prospect', 'CT', '06712', 'Rt. 95'),
(100, 'Ridgefield', 'CT', '06877', 'Rt. 95'),
(101, 'Rocky Hill', 'CT', '06067', 'Rt. 91'),
(102, 'Shelton', 'CT', '06484', 'Rt. 95'),
(103, 'South Windsor', 'CT', '06074', 'Rt. 91'),
(108, 'Southbury', 'CT', '06488', 'Rt. 95'),
(111, 'Southington', 'CT', '06489', 'Rt. 91'),
(112, 'Stafford Springs', 'CT', '06076', 'Rt. 91'),
(114, 'Stamford', 'CT', '06902', 'Rt. 95'),
(120, 'Stamford', 'CT', '06905', 'Rt. 95'),
(122, 'Stamford', 'CT', '06906', 'Rt. 95'),
(123, 'Stratford', 'CT', '06615', 'Rt. 95'),
(126, 'Torrington', 'CT', '06790', 'Rt. 91'),
(129, 'Trumbull', 'CT', '06611', 'Rt. 95'),
(130, 'Wallingford', 'CT', '06492', 'Rt. 95'),
(132, 'Waterford', 'CT', '06385', 'Rt. 95'),
(133, 'Watertown', 'CT', '06795', 'Rt. 91'),
(134, 'West Haven', 'CT', '06516', 'Rt. 95'),
(135, 'Wilton', 'CT', '06897', 'Rt. 95'),
(136, 'Windsor Locks', 'CT', '06096', 'Rt. 91'),
(138, 'Winsted', 'CT', '06098', 'Rt. 91'),
(139, 'Wolcott', 'CT', '06716', 'Rt. 95'),
(140, 'Woodbury', 'CT', '06798', 'Rt. 91');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_corridor`
--
ALTER TABLE `gbs_corridor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CityStateZip` (`City`,`State`,`Zip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_corridor`
--
ALTER TABLE `gbs_corridor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;