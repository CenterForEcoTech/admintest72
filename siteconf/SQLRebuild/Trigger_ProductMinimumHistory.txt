DROP TRIGGER IF EXISTS product_history;
DELIMITER $$
CREATE TRIGGER product_history AFTER UPDATE ON ghs_inventory_products 
FOR EACH ROW
	BEGIN
		IF NEW.GHSInventoryProducts_MinimumQty <> OLD.GHSInventoryProducts_MinimumQty THEN
			INSERT INTO ghs_inventory_products_minimumhistory (GHSInventoryProductsMinimumHistory_EFI, GHSInventoryProductsMinimumHistory_Qty, GHSInventoryProductsMinimumHistory_PerDays) VALUES	(OLD.GHSInventoryProducts_EFI, OLD.GHSInventoryProducts_MinimumQty, OLD.GHSInventoryProducts_MinimumDays);
		END IF;
		IF NEW.GHSInventoryProducts_CurrentDaysOnHandQty <> OLD.GHSInventoryProducts_CurrentDaysOnHandQty THEN
			INSERT INTO ghs_inventory_products_daysonhandhistory (GHSInventoryProductsDaysOnHandHistory_EFI, GHSInventoryProductsDaysOnHandHistory_Qty, GHSInventoryProductsDaysOnHandHistory_PerDays) VALUES	(OLD.GHSInventoryProducts_EFI, OLD.GHSInventoryProducts_CurrentDaysOnHandQty, OLD.GHSInventoryProducts_CurrentDaysOnHandDays);
		END IF;		
	END $$
	
DELIMITER ;