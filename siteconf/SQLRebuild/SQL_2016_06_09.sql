INSERT INTO `gbs_billingrates` (`GBSBillingRate_ID`, `GBSBillingRate_InvoiceName`, `GBSBillingRate_Name`, `GBSBillingRate_Rate`, `GBSBillingRate_EmployeeMembers`, `GBSBillingRate_LastUpdatedBy`, `GBSBillingRate_LastUpdatedDate`) VALUES
(28, 'CovantaHaverhill', 'Senior Staff', '90.00', 'Macaluso_Lorenzo', NULL, '2016-06-09 18:24:48'),
(29, 'CovantaHaverhill', 'Admin Staff', '65.00', 'Fuller_Ted', NULL, '2016-06-09 18:24:48'),
(30, 'CovantaSemass', 'Project Manager', '90.00', 'Macaluso_Lorenzo,Kania_Mike,Orsman_Dave', 'yosh.schulman@cetonline.org', '2016-06-09 18:31:16'),
(31, 'CovantaSemass', 'Business Specialist', NULL, 'Billings_Heather', 'yosh.schulman@cetonline.org', '2016-06-09 18:29:05'),
(32, 'CovantaSemass', 'Business Support Specialist', '65.00', 'Fuller_Ted,Coe_Ben,Budd_Liz,Fabel_Emily,Denardo_Megan,Foley_Cate,Gaylord_Emily', 'yosh.schulman@cetonline.org', '2016-06-09 18:31:46'),
(33, 'CovantaSemass', 'EcoFellows', NULL, 'Mansell_Coryanne', 'yosh.schulman@cetonline.org', '2016-06-09 18:29:22');