DROP TRIGGER IF EXISTS hr_training_update;
DELIMITER $$
CREATE TRIGGER hr_training_update AFTER UPDATE ON hr_training_info
FOR EACH ROW
	BEGIN
		UPDATE hr_training_matrix SET 
			HRTrainingMatrix_TrainingName=NEW.HRTrainingInfo_Name,
			HRTrainingMatrix_TrainingDisplayOrderID=NEW.HRTrainingInfo_DisplayOrderID
		WHERE 
			HRTrainingMatrix_TrainingID=OLD.HRTrainingInfo_ID;
	END $$
	
DELIMITER ;