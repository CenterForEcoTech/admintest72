ALTER TABLE `gbs_billingrates` ADD `GBSBillingRate_InvoiceName` VARCHAR(250) NULL DEFAULT NULL AFTER `GBSBillingRate_ID`;

UPDATE  `cet_data`.`gbs_billingrates` SET  `GBSBillingRate_InvoiceName` =  'MFEP' WHERE  `gbs_billingrates`.`GBSBillingRate_ID` =1;

UPDATE  `cet_data`.`gbs_billingrates` SET  `GBSBillingRate_InvoiceName` =  'MFEP' WHERE  `gbs_billingrates`.`GBSBillingRate_ID` =2;

UPDATE  `cet_data`.`gbs_billingrates` SET  `GBSBillingRate_InvoiceName` =  'MFEP' WHERE  `gbs_billingrates`.`GBSBillingRate_ID` =3;