ALTER TABLE `hr_hours_gbs_only_submitted` CHANGE `Cc1` `Cc1` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

UPDATE `muni_report_boilerplate` SET `MuniReportBoilerPlate_Message` = 'Upgrade your <b>Central Air Conditioning</b> system to an energy efficient unit can reduce your home cooling energy usage by 20-30%. <b>Rebates available up to $500 for high efficient units.</b>' WHERE `muni_report_boilerplate`.`MuniReportBoilerPlate_ID` = 4;
