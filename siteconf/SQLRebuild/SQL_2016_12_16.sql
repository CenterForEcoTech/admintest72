ALTER TABLE `hr_revenuecodes` ADD `HRRevenueCode_LastEditedBy` VARCHAR(250) NULL DEFAULT NULL AFTER `HRRevenueCode_Name`;
ALTER TABLE `invoicing_codes` ADD `InvoicingCodes_Notes` TEXT NULL AFTER `InvoicingCodes_LastUpdatedDate`;

CREATE TABLE `invoicing_statushistory` (
  `InvoicingStatusHistory_ID` int(11) NOT NULL,
  `InvoicingStatusHistory_CodeID` int(11) NOT NULL,
  `InvoicingStatusHistory_TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `InvoicingStatusHistory_Type` varchar(50) DEFAULT NULL,
  `InvoicingStatusHistory_Old` text,
  `InvoicingStatusHistory_New` text,
  `InvoicingStatusHistory_EditedBy` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoicing_statushistory`
--
ALTER TABLE `invoicing_statushistory`
  ADD PRIMARY KEY (`InvoicingStatusHistory_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoicing_statushistory`
--
ALTER TABLE `invoicing_statushistory`
  MODIFY `InvoicingStatusHistory_ID` int(11) NOT NULL AUTO_INCREMENT;
  
  
ALTER TABLE `invoicing_statushistory` ADD `InvoicingStatusHistory_DisplayName` VARCHAR(250) NULL DEFAULT NULL AFTER `InvoicingStatusHistory_CodeID`; 