DROP TRIGGER IF EXISTS hr_trainingmatrix_insert;
DELIMITER $$
CREATE TRIGGER hr_trainingmatrix_insert AFTER INSERT ON hr_training_matrix
FOR EACH ROW
	BEGIN
		INSERT INTO hr_training_matrixstatus 
			(HRTrainingMatrixStatus_MatrixID,
			HRTrainingMatrixStatus_Status,
			HRTrainingMatrixStatus_AffectiveDate,
			HRTrainingMatrixStatus_AddedByAdmin)
		VALUES
			(NEW.HRTrainingMatrix_ID,
			NEW.HRTrainingMatrix_Status,
			NEW.HRTrainingMatrix_AffectiveDate,
			NEW.HRTrainingMatrix_AddedByAdmin);
	END $$
	
DELIMITER ;