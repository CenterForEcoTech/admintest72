DROP TRIGGER IF EXISTS cet_admin.config_updates;
DELIMITER $$
CREATE TRIGGER cet_admin.config_updates AFTER UPDATE ON cet_admin.configs
FOR EACH ROW
	BEGIN
		IF OLD.Config_Name='Config_DaysForInstallCount' THEN
			UPDATE `cet_data`.ghs_inventory_products AS cetdata SET cetdata.GHSInventoryProducts_MinimumDays=NEW.Config_DefaultValue WHERE cetdata.GHSInventoryProducts_DisplayOrderID > 0;
		END IF;
		IF OLD.Config_Name='Config_DaysForDaysOnHandCount' THEN
			UPDATE `cet_data`.ghs_inventory_products AS cetdata SET cetdata.GHSInventoryProducts_CurrentDaysOnHandDays=NEW.Config_DefaultValue WHERE cetdata.GHSInventoryProducts_DisplayOrderID > 0;
		END IF;
	END $$
	
DELIMITER ;