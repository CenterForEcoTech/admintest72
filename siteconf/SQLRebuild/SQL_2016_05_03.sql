-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2016 at 06:02 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cet_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `gbs_sf_gptypes`
--

CREATE TABLE `gbs_sf_gptypes` (
  `GPType_ID` int(11) NOT NULL,
  `GPType_Name` varchar(250) DEFAULT NULL,
  `GPType_BulletCode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gbs_sf_gptypes`
--

INSERT INTO `gbs_sf_gptypes` (`GPType_ID`, `GPType_Name`, `GPType_BulletCode`) VALUES
(1, '560 BMP New', '560B'),
(2, '560 BMP Solar Thermal', '560B'),
(3, '560 BMP Tip Sheet', '560B'),
(4, '560 BMP Update', '560B'),
(5, '560 Case Study Energy Efficiency', '560B'),
(6, '560 Case Study Solar', '560B'),
(7, '560 Content Development Program Materials', '560A'),
(8, '560 Database', '560D'),
(9, '560 Energy Audit-AgEMP', '560IA'),
(10, '560 Hotline Inquiry', '560A'),
(11, '560 Marketing Publication', '560A'),
(12, '560 Marketing-Social Media', '560A'),
(13, '560 Outreach Call', '560A'),
(14, '560 Outreach Energy Efficiency', '560A'),
(15, '560 Outreach Meeting/Event', '560A'),
(16, '560 Outreach Newsletter', '560A'),
(17, '560 Outreach Solar', '560A'),
(18, '560 Project', '560C'),
(19, '560 REAP Assistance', '560C'),
(20, '560 Renewable Project', '560C'),
(21, '560 TA', '560C'),
(22, '560 Update Website', '560A'),
(23, '560 Walkthrough-EAP', '560C'),
(24, '560 Workshop', '560A'),
(25, '560 Workshop Energy Efficiency', '560A'),
(26, '560 Workshop Solar', '560A'),
(27, '563 Outreach', '560A'),
(28, '521 Prescriptive Gas', '560C'),
(29, '521 Gas DI', '560C'),
(30, '521 Custom Gas', '560C'),
(31, '525 Prescriptive Gas', '560C'),
(32, '525D Gas DI', '560C'),
(33, '525 Custom Gas', '560C'),
(34, '526 DI - Lighting', '560C'),
(35, '526 DI - Refrigeration', '560C'),
(36, '526 Custom Electric', '560C'),
(37, '526 Custom Electric', '560C'),
(38, '810B Custom Gas', '560C'),
(39, '810B Gas DI', '560C'),
(40, '810B Prescriptive Gas', '560C');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_sf_gptypes`
--
ALTER TABLE `gbs_sf_gptypes`
  ADD PRIMARY KEY (`GPType_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_sf_gptypes`
--
ALTER TABLE `gbs_sf_gptypes`
  MODIFY `GPType_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;