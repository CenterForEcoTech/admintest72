-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 07:21 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cet_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `gbs_billingrates`
--

CREATE TABLE `gbs_billingrates` (
  `GBSBillingRate_ID` int(11) NOT NULL,
  `GBSBillingRate_Name` varchar(250) DEFAULT NULL,
  `GBSBillingRate_Rate` decimal(5,2) DEFAULT NULL,
  `GBSBillingRate_EmployeeMembers` text,
  `GBSBillingRate_LastUpdatedBy` text,
  `GBSBillingRate_LastUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gbs_billingrates`
--

INSERT INTO `gbs_billingrates` (`GBSBillingRate_ID`, `GBSBillingRate_Name`, `GBSBillingRate_Rate`, `GBSBillingRate_EmployeeMembers`, `GBSBillingRate_LastUpdatedBy`, `GBSBillingRate_LastUpdatedDate`) VALUES
(1, 'Program Director', '90.00', 'Macaluso_Lorenzo,Nylen_Nancy', 'yosh.schulman@cetonline.org', '2016-03-31 17:10:11'),
(2, 'Green Business Specialist and Manager', '75.00', 'Kania_Mike,Heifner_Craig,Laughner_Jake,Orsman_Dave,Fabel_Emily,Budd_Liz', 'yosh.schulman@cetonline.org', '2016-03-31 17:20:34'),
(3, 'Administrative Staff', '55.00', 'Mansel_Coryann,Denardo_Megan,Fuller_Ted', 'yosh.schulman@cetonline.org', '2016-03-31 17:20:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_billingrates`
--
ALTER TABLE `gbs_billingrates`
  ADD PRIMARY KEY (`GBSBillingRate_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_billingrates`
--
ALTER TABLE `gbs_billingrates`
  MODIFY `GBSBillingRate_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;