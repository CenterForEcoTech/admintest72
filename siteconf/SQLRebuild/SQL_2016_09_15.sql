CREATE TABLE `invoicing_codes` (
  `InvoicingCodes_ID` int(11) NOT NULL,
  `InvoicingCodes_DisplayName` varchar(250) DEFAULT NULL,
  `InvoicingCodes_Codes` varchar(250) DEFAULT NULL,
  `InvoicingCodes_ProcessRequirements` varchar(250) DEFAULT 'HoursApproved',
  `InvoicingCodes_Status` int(3) NOT NULL DEFAULT '0',
  `InvoicingCodes_LastUpdatedBy` varchar(250) DEFAULT NULL,
  `InvoicingCodes_LastUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicing_codes`
--

INSERT INTO `invoicing_codes` (`InvoicingCodes_ID`, `InvoicingCodes_DisplayName`, `InvoicingCodes_Codes`, `InvoicingCodes_ProcessRequirements`, `InvoicingCodes_Status`, `InvoicingCodes_LastUpdatedBy`, `InvoicingCodes_LastUpdatedDate`) VALUES
(1, 'BGAS', '523,810,811', 'HoursApproved', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:32:20'),
(2, 'BigY', '540', 'HoursApproved,PassthroughFile', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:41:38'),
(3, 'Columbia Gas', '525', 'HoursApproved', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:32:54'),
(4, 'Covanta', '516,519', 'HoursApproved,PassthroughFile', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:41:38'),
(5, 'Green Team', '518', 'HoursApproved,PassthroughFile', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:41:38'),
(6, 'MFEP', '560,561,562', 'HoursApproved', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:36:20'),
(7, 'Recycling Works', '531,532,533,534,535,536,537,538,539', 'HoursApproved,PassthroughFile', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:41:38'),
(8, 'Small Contract Hours', '550,552,570,591,592,620,630', 'HoursApproved', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:36:05'),
(9, 'TRC', '515', 'HoursApproved', 0, 'yosh.schulman@cetonline.org', '2016-09-15 14:36:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoicing_codes`
--
ALTER TABLE `invoicing_codes`
  ADD PRIMARY KEY (`InvoicingCodes_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoicing_codes`
--
ALTER TABLE `invoicing_codes`
  MODIFY `InvoicingCodes_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
  
ALTER TABLE `gbs_billingrates` ADD `GBSBillingRate_InvoiceDisplayName` VARCHAR(250) NULL DEFAULT NULL AFTER `GBSBillingRate_InvoiceName`;

UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'BGas 810/811' WHERE `GBSBillingRate_InvoiceName`='BGAS_8';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'BGas 523' WHERE `GBSBillingRate_InvoiceName`='BGAS_5';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'MFEP' WHERE `GBSBillingRate_InvoiceName`='MFEP';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'Fink' WHERE `GBSBillingRate_InvoiceName`='Fink';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'Recycling Works' WHERE `GBSBillingRate_InvoiceName`='RecyclingWorks';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'Green Team' WHERE `GBSBillingRate_InvoiceName`='GreenTeam';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'Covanta Haverhill' WHERE `GBSBillingRate_InvoiceName`='CovantaHaverhill';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'Covanta Semass' WHERE `GBSBillingRate_InvoiceName`='CovantaSemass';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'College' WHERE `GBSBillingRate_InvoiceName`='College';
UPDATE `gbs_billingrates` set `GBSBillingRate_InvoiceDisplayName` = 'BigY' WHERE `GBSBillingRate_InvoiceName`='BigY';