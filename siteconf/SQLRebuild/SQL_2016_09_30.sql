DROP TABLE IF EXISTS `csg_hub_inspection_refused`;
CREATE TABLE `csg_hub_inspection_refused` (
  `CSGHUBInspectionRefused_ID` int(11) NOT NULL,
  `CSGHUBInspectionRefused_Status` varchar(250) DEFAULT NULL,
  `CSGHUBInspectionRefused_SiteID` varchar(250) NOT NULL,
  `CSGHUBInspectionRefused_ByAdminID` varchar(250) NOT NULL,
  `CSGHUBInspectionRefused_TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `csg_hub_inspection_refused`
--
ALTER TABLE `csg_hub_inspection_refused`
  ADD PRIMARY KEY (`CSGHUBInspectionRefused_ID`),
  ADD UNIQUE KEY `refusedStatusSiteID` (`CSGHUBInspectionRefused_SiteID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `csg_hub_inspection_refused`
--
ALTER TABLE `csg_hub_inspection_refused`
  MODIFY `CSGHUBInspectionRefused_ID` int(11) NOT NULL AUTO_INCREMENT;