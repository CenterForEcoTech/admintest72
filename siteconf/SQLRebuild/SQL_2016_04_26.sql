INSERT INTO `gbs_billingrates` (`GBSBillingRate_ID`, `GBSBillingRate_InvoiceName`, `GBSBillingRate_Name`, `GBSBillingRate_Rate`, `GBSBillingRate_EmployeeMembers`, `GBSBillingRate_LastUpdatedBy`, `GBSBillingRate_LastUpdatedDate`) VALUES
(4, 'BGAS_5', 'Program Director/Manager', '100.00', 'Macaluso_Lorenzo,Kania_Mike', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:33'),
(5, 'BGAS_5', 'Energy Analyst', '90.00', 'Lafley_Bill,Blazek_Cedar', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:42'),
(6, 'BGAS_5', 'Program Admin', '55.00', 'Denardo_Megan,Fuller_Ted', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:45'),
(8, 'BGAS_5', 'Staff Engineer', '115.00', 'Laughner_Jake,Heffner_Craig', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:52'),
(9, 'BGAS_5', 'Senior Staff', '80.00', 'Budd_Liz,Cahillane_Jamie,Couture_David', 'yosh.schulman@cetonline.org', '2016-04-26 13:36:07'),
(10, 'BGAS_8', 'Program Director/Manager', '100.00', 'Macaluso_Lorenzo,Kania_Mike', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:33'),
(11, 'BGAS_8', 'Energy Analyst', '90.00', 'Lafley_Bill,Blazek_Cedar', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:42'),
(12, 'BGAS_8', 'Program Admin', '55.00', 'Denardo_Megan,Fuller_Ted', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:45'),
(13, 'BGAS_8', 'Energy Specialist', '80.00', 'Cuozzo_Claire,Cahillane_Jamie,Couture_David', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:48'),
(16, 'BGAS_8', 'Staff Engineer', '115.00', 'Laughner_Jake,Heffner_Craig', 'yosh.schulman@cetonline.org', '2016-04-25 19:58:52');


CREATE TABLE `gbs_bgas_customer_accounts` (
  `BusinessPartner` varchar(50) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `Account` varchar(20) NOT NULL DEFAULT '',
  `Premise` varchar(20) DEFAULT NULL,
  `Rate` varchar(100) DEFAULT NULL,
  `FirstName` varchar(100) DEFAULT NULL,
  `LastName` varchar(100) DEFAULT NULL,
  `StreetAddress` varchar(100) DEFAULT NULL,
  `Unit` varchar(10) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `HouseNumber` varchar(20) DEFAULT NULL,
  `StreetName` varchar(50) DEFAULT NULL,
  `State` varchar(2) DEFAULT NULL,
  `ZipCode` varchar(20) DEFAULT NULL,
  `Meter` varchar(10) DEFAULT NULL,
  `Device` varchar(10) DEFAULT NULL,
  `BillingPortion` varchar(10) DEFAULT NULL,
  `BillingPeriodBegin` varchar(20) DEFAULT NULL,
  `BillingPeriodEnd` varchar(20) DEFAULT NULL,
  `Amount` varchar(20) DEFAULT NULL,
  `Therms` varchar(20) DEFAULT NULL,
  `BGASAccount_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_bgas_customer_accounts`
--
ALTER TABLE `gbs_bgas_customer_accounts`
  ADD PRIMARY KEY (`BGASAccount_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_bgas_customer_accounts`
--
ALTER TABLE `gbs_bgas_customer_accounts`
  MODIFY `BGASAccount_ID` int(11) NOT NULL AUTO_INCREMENT;