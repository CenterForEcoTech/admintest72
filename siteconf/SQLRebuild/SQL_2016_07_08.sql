CREATE TABLE `gbs_preinvoiced` (
  `GBSPreInvoiced_ID` int(11) NOT NULL,
  `GBSPreInvoiced_SalesforceID` varchar(250) DEFAULT NULL,
  `GBSPreInvoiced_AccountName` varchar(250) DEFAULT NULL,
  `GBSPreInvoiced_InvoiceDate` date DEFAULT '0000-00-00',
  `GBSPreInvoiced_Type` varchar(250) DEFAULT NULL,
  `GBSPreInvoiced_UpdatedLastDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GBSPreInvoiced_IncentiveAmount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_preinvoiced`
--
ALTER TABLE `gbs_preinvoiced`
  ADD PRIMARY KEY (`GBSPreInvoiced_ID`),
  ADD UNIQUE KEY `SalesForce` (`GBSPreInvoiced_SalesforceID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_preinvoiced`
--
ALTER TABLE `gbs_preinvoiced`
  MODIFY `GBSPreInvoiced_ID` int(11) NOT NULL AUTO_INCREMENT;