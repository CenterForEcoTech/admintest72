-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2016 at 07:33 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cet_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `gbs_bullets`
--

DROP TABLE IF EXISTS `gbs_bullets`;
CREATE TABLE `gbs_bullets` (
  `GBSBullet_ID` int(11) NOT NULL,
  `GBSBullet_Report` varchar(250) DEFAULT NULL,
  `GBSBullet_InvoiceCode` varchar(10) DEFAULT NULL,
  `GBSBullet_Content` text,
  `GBSBullet_LastUpdatedTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gbs_bullets`
--

INSERT INTO `gbs_bullets` (`GBSBullet_ID`, `GBSBullet_Report`, `GBSBullet_InvoiceCode`, `GBSBullet_Content`, `GBSBullet_LastUpdatedTimeStamp`) VALUES
(1, 'RecyclingWorks', '531', 'New inquiries coming in; managing steady call volume due to waste bans and TA requests.', '2016-05-27 16:45:08'),
(2, 'RecyclingWorks', '531', 'Following up with previous and current month''s requests.', '2016-05-27 16:45:08'),
(3, 'RecyclingWorks', '532', 'RecyclingWorks continues to promote College & University recycling forums through technical assistance with colleges and universities.', '2016-05-27 16:49:16'),
(4, 'RecyclingWorks', '532', 'RecyclingWorks continues to conduct outreach to the College & University audience.', '2016-05-27 16:49:16'),
(5, 'RecyclingWorks', '533', 'RecyclingWorks continues to conduct outreach to local Chambers of Commerce to organize workshops on RecyclingWorks and utility incentives as well as potential newsletter coverage.', '2016-05-27 16:49:16'),
(6, 'RecyclingWorks', '533', 'Outreach continues to food processing facilities. Potential Technical Assistance opportunities are being explored.', '2016-05-27 16:49:16'),
(7, 'RecyclingWorks', '533', 'EcoFellows continue to promote RecyclingWorks at tabling events.', '2016-05-27 16:49:16'),
(8, 'RecyclingWorks', '533', 'CET continues to promote RecyclingWorks through the CET Facebook page and both CET and RecyclingWorks Twitter accounts.', '2016-05-27 16:49:16'),
(9, 'RecyclingWorks', '533', 'RecyclingWorks continues outreach to business journals.', '2016-05-27 16:49:16'),
(10, 'RecyclingWorks', '535', 'RecyclingWorks continues to manage additions and alterations to the newsletter distribution list.', '2016-05-27 16:49:16'),
(11, 'RecyclingWorks', '535', 'RecyclingWorks continues to address broken links and conduct maintenance.', '2016-05-27 16:49:16'),
(12, 'RecyclingWorks', '535', 'RecyclingWorks continues to post event listings on the website.', '2016-05-27 16:49:16'),
(13, 'RecyclingWorks', '535', 'Find-A-Recycler database entries are continuously expanded and updated as required.', '2016-05-27 16:53:00'),
(14, 'RecyclingWorks', '535', 'RecyclingWorks is conducting direct outreach to processors, especially of Construction & Demolition waste, mattresses, and textiles for database listing, with further outreach through social media format.', '2016-05-27 16:53:00'),
(15, 'RecyclingWorks', '535', 'RecyclingWorks continues outreach to processors and haulers to verify database listing accuracy.', '2016-05-27 16:53:00'),
(16, 'RecyclingWorks', '536', 'RecyclingWorks continues to compile interested and potential stakeholders for C&D BMP process.', '2016-05-27 16:53:00'),
(17, 'RecyclingWorks', '536', 'RecyclingWorks continues to promote Food Donation BMPs to stakeholders and direct incoming inquiries to Food Donation BMP resources with promotion of technical assistance.', '2016-05-27 16:53:00'),
(18, 'RecyclingWorks', '536', 'RecyclingWorks is currently conducting technical assistance to a MassBIO pharmaceutical company to determine material types and processing capacity for developing future BMPs for bioplastics', '2016-05-27 16:53:00'),
(19, 'RecyclingWorks', '536', 'ReuseConex, International Reuse Conference & Expo in October 2016 will be incorporated into C&D BMP planning.', '2016-05-27 16:53:00'),
(20, 'RecyclingWorks', '538C', 'Inquiries from facilities regarding Compost Site Technical Assistance have been addressed with instructions for next steps sent to site operators.', '2016-05-27 16:53:51'),
(21, 'RecyclingWorks', '538C', 'RecyclingWorks continues to conduct direct outreach to viable facilities.', '2016-05-27 16:53:51'),
(22, 'RecyclingWorks', '538C', 'RecyclingWorks finalized procedure for Compost Site TA to include Tier 2 with eligible sites to be evaluated, with four sites in the process of applying for Tier 2.', '2016-05-27 16:53:51'),
(23, 'RecyclingWorks', '538C', 'RecyclingWorks is directing grant and loan inquiries to MassDEP''s Recycling Loan Fund.', '2016-05-27 16:54:28'),
(24, 'RecyclingWorks', '538C', 'RecyclingWorks is conducting outreach regarding food materials diversion data from MassDEP list of MA processing sites and promoting technical assistance.', '2016-05-27 16:54:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_bullets`
--
ALTER TABLE `gbs_bullets`
  ADD PRIMARY KEY (`GBSBullet_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_bullets`
--
ALTER TABLE `gbs_bullets`
  MODIFY `GBSBullet_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;