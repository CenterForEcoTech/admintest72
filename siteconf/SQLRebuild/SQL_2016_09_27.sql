DROP TABLE IF EXISTS `ghs_inventory_products`;
CREATE TABLE `ghs_inventory_products` (
  `GHSInventoryProducts_ID` int(11) NOT NULL,
  `GHSInventoryProducts_EFI` varchar(50) DEFAULT NULL,
  `GHSInventoryProducts_Description` varchar(250) DEFAULT NULL,
  `GHSInventoryProducts_CSGCode` varchar(250) DEFAULT NULL,
  `GHSInventoryProducts_BulbStyle` varchar(50) DEFAULT NULL,
  `GHSInventoryProducts_BulbType` varchar(50) DEFAULT NULL,
  `GHSInventoryProducts_UnitsPerPack` int(11) NOT NULL DEFAULT '0',
  `GHSInventoryProducts_UnitsPerCase` int(11) NOT NULL DEFAULT '0',
  `GHSInventoryProducts_MinimumQty` int(11) NOT NULL DEFAULT '0',
  `GHSInventoryProducts_OrderQty` int(5) DEFAULT '0',
  `GHSInventoryProducts_MinimumDays` int(11) NOT NULL DEFAULT '21',
  `GHSInventoryProducts_CurrentDaysOnHandQty` int(11) NOT NULL DEFAULT '0',
  `GHSInventoryProducts_CurrentDaysOnHandDays` int(11) NOT NULL DEFAULT '120',
  `GHSInventoryProducts_Cost` decimal(5,2) NOT NULL DEFAULT '0.00',
  `GHSInventoryProducts_Retail` decimal(5,2) NOT NULL DEFAULT '0.00',
  `GHSInventoryProducts_ActiveID` int(2) NOT NULL DEFAULT '1',
  `GHSInventoryProducts_CanBeOrderedID` int(2) NOT NULL DEFAULT '1',
  `GHSInventoryProducts_DisplayOrderID` int(5) NOT NULL DEFAULT '0',
  `GHSInventoryProducts_Notes` text,
  `GHSInventoryProducts_MeasureName` varchar(250) DEFAULT NULL,
  `GHSInventoryProducts_BGasName` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ghs_inventory_products`
--

INSERT INTO `ghs_inventory_products` (`GHSInventoryProducts_ID`, `GHSInventoryProducts_EFI`, `GHSInventoryProducts_Description`, `GHSInventoryProducts_CSGCode`, `GHSInventoryProducts_BulbStyle`, `GHSInventoryProducts_BulbType`, `GHSInventoryProducts_UnitsPerPack`, `GHSInventoryProducts_UnitsPerCase`, `GHSInventoryProducts_MinimumQty`, `GHSInventoryProducts_OrderQty`, `GHSInventoryProducts_MinimumDays`, `GHSInventoryProducts_CurrentDaysOnHandQty`, `GHSInventoryProducts_CurrentDaysOnHandDays`, `GHSInventoryProducts_Cost`, `GHSInventoryProducts_Retail`, `GHSInventoryProducts_ActiveID`, `GHSInventoryProducts_CanBeOrderedID`, `GHSInventoryProducts_DisplayOrderID`, `GHSInventoryProducts_Notes`, `GHSInventoryProducts_MeasureName`, `GHSInventoryProducts_BGasName`) VALUES
(1, '1100.128', '13W TCP Spiral', '013_TCP_ECOSPRING,013_MAX_SP,014_TCP_MINI', 'Spirals', 'CFL', 48, 48, 0, 0, 21, 39, 21, '1.10', '5.35', 1, 0, 1, NULL, NULL, NULL),
(2, '1100.138', '20W TCP Spiral', '020_TCP_ECOSPRING,020HR_SPIRAL', 'Spirals', 'CFL', 48, 48, 0, 0, 21, 5, 21, '1.60', '5.85', 1, 0, 2, NULL, NULL, NULL),
(3, '1100.139', '23W TCP Spiral', '023_TCP_ECOSPRING,023_TCP_SPIRAL', 'Spirals', 'CFL', 48, 48, 0, 0, 21, 22, 21, '1.60', '5.85', 1, 0, 3, NULL, NULL, NULL),
(4, '1100.728', '16W TCP Dimmable A-Lamp', '016_TCP_DIMALAMP', 'A-Lamps', 'LED', 48, 48, 0, 0, 21, 0, 120, '5.66', '0.00', 0, 0, 0, NULL, NULL, NULL),
(5, '1100.755', '9W TCP Globe', '009_TCP_GLOBE,009_GREEN_GLOBE', 'Globes', 'LED', 24, 24, 0, 0, 21, 9, 21, '2.65', '6.90', 1, 0, 8, NULL, NULL, NULL),
(6, '1100.758', '9W TCP Candle Base', '011_TCP_TORP', 'Decorative', 'CFL', 48, 48, 0, 0, 21, 10, 21, '2.60', '7.10', 1, 0, 0, NULL, NULL, NULL),
(7, '1100.784', 'TCP 14w Globe', '014_TCP_GLOBE', '', '', 24, 24, 0, 0, 0, 0, 120, '2.75', '7.25', 0, 0, 0, NULL, NULL, NULL),
(8, '1100.794', '14W TCP A Lamp', '014_TCP_A,014_TCP_SP', 'A-Lamps', 'CFL', 48, 48, 0, 0, 21, 14, 21, '2.65', '6.90', 1, 0, 9, NULL, NULL, NULL),
(9, '1100.948', 'Feit 32w 3-Way', '032_FEIT_3WAY,033_EM_3WAY,033HR_3WAY', '', '', 12, 12, 0, 0, 21, 0, 120, '6.65', '0.00', 1, 0, 0, NULL, NULL, NULL),
(10, '1100.158', '11w Phillips A19 LED', '011_PH_A_LED', '', '', 6, 6, 0, 0, 21, 0, 120, '14.25', '0.00', 1, 0, 0, NULL, NULL, NULL),
(11, '1100.7291', '12w Greenlite LED', '012_GREENLITE_LED', '', '', 4, 4, 0, 0, 21, 0, 120, '0.00', '0.00', 1, 0, 0, NULL, NULL, NULL),
(12, '1100.0172', '10w TCP LED A Lamp', '010_TCP_A_LED,010_MAX_A_LED,010_PH_SF_LED', 'A-Lamps', 'LED', 48, 48, 576, 1152, 21, 109, 21, '3.40', '7.90', 1, 1, 4, NULL, NULL, NULL),
(13, '1160.607', 'GE 15w Dimable R30', '015GE_DIMFLOOD,015PH_REFLECT,015_MAX_DIM_FLOOD', '', '', 3, 3, 0, 0, 21, 0, 120, '11.13', '0.00', 1, 0, 0, NULL, NULL, NULL),
(14, '1160.659', 'TCP 14w BR30 Flood', '014_TCP_FLOOD,014_TCP_DIM', '', '', 16, 16, 0, 0, 21, 0, 120, '3.19', '0.00', 1, 0, 0, NULL, NULL, NULL),
(15, '3000.161', '1.75gpm Earth Showerhead', 'ShowerheadEarth_17,ShowerheadTurbo_20', '', '', 50, 50, 50, 100, 21, 9, 21, '3.50', '10.00', 1, 1, 17, NULL, 'SH', 'Showerhead'),
(16, '3010.020', '1.5gmp Aerator', 'WFP2a', '', '', 10, 10, 50, 100, 21, 23, 21, '0.49', '2.00', 1, 1, 13, '', 'A', 'Aerator (1.5 gpm)'),
(17, '3010.100', '2.2gpm FlipAerator', 'AeratorFlip_22', '', '', 10, 10, 0, 0, 21, 0, 21, '2.02', '5.00', 1, 1, 14, NULL, NULL, NULL),
(18, '5000.229', 'Honeywell Thermostat', 'Honeywell Thermostat,HWellThermostat_Furnace,HWellThermostat_Boiler,HWellThermostat_Combo,LuxThermostat_Boiler,LuxThermostat_Combo,LuxThermostat_Furnace', '', '', 6, 6, 36, 72, 21, 5, 21, '22.25', '32.00', 1, 1, 25, NULL, NULL, NULL),
(19, '7005.609', 'Tricklestar PowerStrip', 'TrickleStarPowerStrip', '', '', 18, 18, 108, 108, 21, 11, 21, '14.67', '21.20', 1, 1, 28, NULL, NULL, NULL),
(20, '1100.539', '5W TCP LED Globe', '005_TCP_GLOBE_LED', 'Globes', 'LED', 12, 12, 72, 144, 21, 10, 21, '7.05', '11.55', 1, 1, 7, NULL, NULL, NULL),
(21, '1100.8087', '8w TCP LED Dimmable Globe', '008_TCP_GLOBE_LED', '', '', 12, 12, 0, 0, 0, 0, 120, '10.00', '0.00', 0, 0, 0, NULL, NULL, NULL),
(22, '1160.538', '10W TCP LED R30 Flood', '010_TCP_FLOOD_LED,010_PH_FLOOD_LED,010_TCP_20FLOOD_LED,009_PH_AFFLOOD_LED', '', 'LED', 12, 12, 144, 288, 21, 39, 21, '5.40', '9.90', 1, 1, 12, NULL, NULL, NULL),
(23, '1100.955', 'Feit 13.5w LED', '013_FEIT_LED', '', '', 12, 12, 0, 0, 21, 0, 120, '0.00', '0.00', 0, 0, 0, NULL, NULL, NULL),
(24, '3000.160', '2.0gpm Turbo Showerhead', '', '', '', 1, 1, 0, 0, 21, 0, 120, '0.00', '0.00', 0, 0, 0, NULL, NULL, NULL),
(25, '5000.100', 'PSP511 Lux Thermostat', 'MultiFamily', '', '', 1, 1, 0, 0, 21, 0, 120, '23.78', '33.29', 0, 0, 22, NULL, 'Lux PSP511 thermostat', 'Lux PSP511 thermostat (Heating and Cooling)'),
(26, '4000.032', '1/2 " Pipe Insulation 12''', 'PIPINS12', '', '', 1, 1, 0, 0, 21, 0, 120, '2.95', '0.00', 0, 0, 0, NULL, NULL, NULL),
(27, '4000.033', '3/4" Pipe Insulation 12''', 'PIPINS34', NULL, NULL, 1, 1, 0, 0, 21, 0, 120, '3.15', '0.00', 1, 1, 0, NULL, NULL, NULL),
(29, '1160.771', '16w TCP R30 Dimmable', '016_TCP_DIM_FLOOD', '', '', 12, 12, 0, 0, 21, 0, 120, '5.80', '0.00', 1, 0, 0, NULL, NULL, NULL),
(30, '1100.828', '30W Maxlite 3-Way', '030_MAX_3WAY,027_MAX_SP', 'Spirals', 'CFL', 12, 12, 0, 0, 21, 4, 21, '4.05', '8.30', 1, 0, 10, NULL, NULL, NULL),
(31, '1100.0178', '5w TCP LED med Base DIM', 'MultiFamily', '', 'LED', 12, 12, 0, 0, 21, 0, 120, '7.20', '11.45', 1, 0, 16, NULL, NULL, NULL),
(32, '1100.0181', '5W TCP LED Candle Base', '005_TCP_TORP_LED,005_MAX_TORP,004_MAX_TORP_LED', 'Decorative', 'LED', 48, 48, 288, 576, 21, 36, 21, '4.95', '9.45', 1, 1, 5, NULL, NULL, NULL),
(33, '1160.534', '8W TCP LED R20 Flood', '008_TCP_20FLOOD_LED', '', 'LED', 48, 48, 96, 192, 21, 10, 21, '6.65', '11.15', 1, 1, 11, NULL, NULL, NULL),
(34, '1100.695', '8w LED globe Feit', '008_FEIT_LED_GLOBE', 'Globe', 'LED', 1, 12, 0, 0, 21, 0, 0, '8.35', '12.60', 0, 0, 0, NULL, NULL, NULL),
(35, '1100.8283', '5W LED 3-Way Phillips', '020_PH_3WAY_LED', 'A-Lamps', 'LED', 6, 6, 30, 30, 21, 0, 120, '13.50', '18.59', 1, 1, 15, NULL, NULL, NULL),
(36, '3000.803', 'Waterpik Charm Handheld Showerhead', 'MultiFamily', '', '', 2, 2, 0, 0, 21, 0, 120, '17.73', '28.39', 1, 0, 19, NULL, 'HH', 'Showerhead'),
(37, '1100.1552', '17w Philips LED A-Lamp', 'MultiFamily', 'A-Lamps', 'LED', 1, 6, 0, 0, 21, 0, 120, '8.95', '15.60', 1, 0, 20, NULL, NULL, NULL),
(38, '1500.01', 'Leviton Socket Extender 1', 'MultiFamily', '', '', 1, 0, 0, 0, 21, 0, 120, '1.43', '2.89', 1, 0, 18, NULL, NULL, NULL),
(39, '3030.5', 'Niagara Spray Valve', 'MultiFamily', '', '', 1, 0, 0, 0, 21, 0, 120, '29.30', '41.95', 1, 0, 21, NULL, 'SPRY', 'Spray valve'),
(40, '4000.032', '1/2" x 3'' DHW pipe insulation', 'MultiFamily', '', '', 4, 24, 0, 0, 21, 0, 120, '2.61', '0.00', 1, 0, 0, NULL, NULL, NULL),
(41, '5000.15', 'Lux PSP722E 7 day tstat', 'MultiFamily', '', '', 1, 0, 0, 0, 21, 0, 120, '51.65', '72.39', 0, 0, 23, NULL, 'Lux PSP722 thermostat', 'Lux PSP722 thermostat'),
(42, '1100.1553', '19w Philips LED A-Lamp', 'MultiFamily', 'A-Lamps', 'LED', 1, 6, 0, 0, 21, 0, 120, '9.50', '15.20', 1, 0, 6, NULL, NULL, NULL),
(43, '5000.997D', 'Hub36 Wireless Gateway', 'TBD', '', '', 1, 1, 0, 0, 21, 0, 120, '92.78', '129.89', 1, 0, 24, NULL, 'Building 36 WiFi Hub', 'Wifi Hub'),
(44, '5000.997C', 'B36-T10 Thermostat', 'TBD', '', '', 1, 1, 0, 0, 21, 0, 120, '85.70', '119.98', 1, 0, 26, NULL, 'Building 36 WiFi Thermostat', 'WiFi-stat - No Cooling'),
(45, '5000.9981', 'B36 Zwave Repeater DSD37', 'TBD', '', '', 1, 1, 0, 0, 21, 0, 120, '23.33', '23.33', 1, 0, 27, NULL, NULL, NULL),
(46, '3000.18', 'Niagara Earth Massage 1.5 gpm Handheld Showerhead', '', '', '', 2, 2, 0, 0, 21, 0, 120, '9.66', '28.39', 1, 0, 0, NULL, NULL, NULL);

--
-- Triggers `ghs_inventory_products`
--
DROP TRIGGER IF EXISTS `product_history`;
DELIMITER $$
CREATE TRIGGER `product_history` AFTER UPDATE ON `ghs_inventory_products` FOR EACH ROW BEGIN
		IF NEW.GHSInventoryProducts_MinimumQty <> OLD.GHSInventoryProducts_MinimumQty THEN
			INSERT INTO ghs_inventory_products_minimumhistory (GHSInventoryProductsMinimumHistory_EFI, GHSInventoryProductsMinimumHistory_Qty, GHSInventoryProductsMinimumHistory_PerDays) VALUES	(OLD.GHSInventoryProducts_EFI, OLD.GHSInventoryProducts_MinimumQty, OLD.GHSInventoryProducts_MinimumDays);
		END IF;
		IF NEW.GHSInventoryProducts_CurrentDaysOnHandQty <> OLD.GHSInventoryProducts_CurrentDaysOnHandQty THEN
			INSERT INTO ghs_inventory_products_daysonhandhistory (GHSInventoryProductsDaysOnHandHistory_EFI, GHSInventoryProductsDaysOnHandHistory_Qty, GHSInventoryProductsDaysOnHandHistory_PerDays) VALUES	(OLD.GHSInventoryProducts_EFI, OLD.GHSInventoryProducts_CurrentDaysOnHandQty, OLD.GHSInventoryProducts_CurrentDaysOnHandDays);
		END IF;		
	END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ghs_inventory_products`
--
ALTER TABLE `ghs_inventory_products`
  ADD PRIMARY KEY (`GHSInventoryProducts_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ghs_inventory_products`
--
ALTER TABLE `ghs_inventory_products`
  MODIFY `GHSInventoryProducts_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;