--
-- Table structure for table `gbs_invoicing_mfep_allocations`
--

CREATE TABLE `gbs_invoicing_mfep_allocations` (
  `MFEPAllocations_ID` int(11) NOT NULL,
  `MFEPAllocations_Name` varchar(250) DEFAULT NULL,
  `MFEPAllocations_Multiplier` varchar(20) DEFAULT NULL,
  `MFEPAllocations_AverageHourlyRate` varchar(10) DEFAULT NULL,
  `MFEPAllocations_LastUpdatedBy` varchar(250) DEFAULT NULL,
  `MFEPAllocations_LastUpdatedTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gbs_invoicing_mfep_allocations`
--

INSERT INTO `gbs_invoicing_mfep_allocations` (`MFEPAllocations_ID`, `MFEPAllocations_Name`, `MFEPAllocations_Multiplier`, `MFEPAllocations_AverageHourlyRate`, `MFEPAllocations_LastUpdatedBy`, `MFEPAllocations_LastUpdatedTimeStamp`) VALUES
(1, 'NRCS Cig', '0.41234826', '30.06', 'yosh.schulman@cetonline.org', '2016-03-29 13:45:14'),
(2, 'RBEG', '0.123018814', '65', 'yosh.schulman@cetonline.org', '2016-03-29 13:45:14'),
(3, 'MDAR', '0.464632926', '75', 'Yosh', '2016-03-29 13:45:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gbs_invoicing_mfep_allocations`
--
ALTER TABLE `gbs_invoicing_mfep_allocations`
  ADD PRIMARY KEY (`MFEPAllocations_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gbs_invoicing_mfep_allocations`
--
ALTER TABLE `gbs_invoicing_mfep_allocations`
  MODIFY `MFEPAllocations_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;