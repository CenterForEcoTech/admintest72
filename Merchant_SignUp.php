<?php
	$load_getHeader = (isset($load_getHeader)) ? $load_getHeader : true;
	$load_getContent = (isset($load_getContent)) ? $load_getContent : true;
	if ($load_getHeader){
        include_once($dbProviderFolder."MemberProvider.php");
		$PageTitle = "Sign up as a Business/Merchant";
		$PageHeader1 = "Drive Sales AND Do Good";
		$pageSubtitle = "It&rsquo;s easy, quick, and FREE to register.";

        if ($_GET['confirm-pid']){
            $candidatePid = htmlspecialchars($_GET['confirm-pid']);
            $permalinkProvider = new PermaLinkProviderImpl($mysqli);
            $merchantId = $permalinkProvider->getMerchantId($candidatePid);
            if ($merchantId){
                $merchantProvider = new MerchantMemberProvider($mysqli);
                $merchantRecord = $merchantProvider->getPublicProfile($merchantId);
                if ($merchantRecord->memberId){
                    // already registered!!
                    $merchantRecord = null;
                }
            }
            if (!isset($merchantRecord) || empty($merchantRecord)){
                header("location:".$_SERVER["REDIRECT_URL"]);
            }
        }
	}
if ($load_getContent){
    include('_initializePrivateCms.php');
    $content = $sitePrivateCms->getPage("merchant_signup");
?>

	<div id="sign-up-info" class="twelve columns">
		<div class="four columns">
			<div class="snapshot">
                <a href="<?php echo $CurrentServer;?>images/MollysTable_390.jpg" class="colorbox" title="Donna Nagle, Owner, Molly's Table">
                    <img class="frame no-tablet" src="<?php echo $CurrentServer;?>images/MollysTable_200.jpg" width="200" height="334">
                    <img class="frame on-tablet" src="<?php echo $CurrentServer;?>images/MollysTable_138.jpg" width="138" height="230">
                </a>
                <q class="caption">&ldquo;It was rewarding to support charities ranging from the school&rsquo;s booster club to the local brain&ndash;injury support group while enjoying my best weekday sales ever!&rdquo;</q>
                <div class="caption">&mdash;Donna Nagle, Owner, Molly&rsquo;s Table</div>
			</div>

		</div>
		<div class="seven columns">
            <p class="wordpress-content"><?php echo $content;?></p>
            <?php
            // you can test the new form by adding "?demo=1" in the URL
            $useOldForm = true && !isset($_GET["demo"]);
            if ($useOldForm) {?>
            <p>Start by telling us where your business is located.</p>
            <?php
                $isRegistration = true;
                $BaseLink = "mobile/mobile_";
                include_once('_SignUpProfileForm.php');
            ?>
                <p class="help">Already registered? <a href="<?php echo $CurrentServer."Login";?>">Login Here!</a></p>
            <?php } // end use old form ?>
		</div>
<?php if (!$useOldForm) { ?>
        <div class="twelve columns">
            <?php
            include_once('MerchantSignUp.php');
            ?>
            <script type="text/javascript">
                $("#close-register").hide();
                $("#register-header").hide();
            </script>
        </div>
<?php } // end use new form ?>
	</div>
<?php } // end load get content?>
