<?php
// DEAD CODE
trigger_error("DEAD CODE: Index_content.php being called.", E_USER_ERROR);
?>

<div class="no-mobile home-trip">
	<a id="businesses-signup" class="rounded-corners" href="<?php echo $CurrentServer;?>Merchant/SignUp" alt="Learn More as a Local Business" title="Learn More as a Local Business">
		<h3>Local<br> Businesses</h3>
		<!--<img src="<?php echo $CurrentServer;?>images/merchants.jpg" width="150">-->
		<p>Schedule a <?php echo $SiteName; ?> Event and  Donate a Percentage of Sales</p>
	</a>
	<a id="nonprofits-signup" class="rounded-corners" href="<?php echo $CurrentServer;?>Organization/SignUp" alt="Learn More as a Nonprofit" title="Learn More as a Nonprofit">
		<h3>Nonprofits</h3>
		<!--<img src="<?php echo $CurrentServer;?>images/nonprofits.jpg" width="140">-->
		<p>Partner with Local Businesses for Innovative Fundraising</p>
	</a>
	<a id="consumers-signup" class="rounded-corners" href="<?php echo $CurrentServer;?>Individual/SignUp" alt="Learn More as a Consumer" title="Learn More as a Consumer">
		<h3>Consumers</h3>
		<!--<img src="<?php echo $CurrentServer;?>images/consumers.jpg" width="150">-->
		<p>Help Your Favorite Nonprofit Simply by Shopping Local</p>
	</a>
</div>
<div class="mobile-alt">
	<a href="<?php echo $CurrentServer;?>Merchant/SignUp" alt="Learn More as a Local Business" title="Learn More as a Local Business"    />Register as a Local Business</a><bR>
	<a href="<?php echo $CurrentServer;?>Organization/SignUp" alt="Learn More as a Nonprofit" title="Learn More as a Nonprofit"    />Register as a Nonprofit</a><Br>
	<a href="<?php echo $CurrentServer;?>Individual/SignUp" alt="Learn More as a Consumer" title="Learn More as a Consumer"    />Register as a Consumer</a><br>
</div>
<div class="clear"></div>
<div class="no-mobile vertical-space"></div>
<div class="no-mobile vertical-space"></div>
<div id="upcoming-events">
	<h3>Upcoming Events</h3>
	<div id="event-list">

	</div>
	<a id="find-more-link" href="<?php echo $CurrentServer."FindEvents"?>">Find more upcoming events <span>&gt;&gt;</span> </a>
</div>
<script type="text/javascript">
	$(function(){
		var userlat = "",
			userlong = "",
			eventList = $("#event-list"),
			eventURL = "<?php echo $CurrentServer."UpcomingEvents"; ?>",
			pageLength = 3,
			showPagination = false,
			loadEvents = function(lat, long){
				eventList.load(eventURL, {hidePagination: !showPagination, userlat: lat, userlong: long, pageLength: pageLength});
				$("#upcoming-events").show();
			};

		//loadEvents();
		<?php if ($Config_GeoLocationAsk){?>
		if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(
				function(position){
					userlat = position.coords.latitude;
					userlong = position.coords.longitude;
					loadEvents(userlat,userlong);
				},
				function(error){
					var errorMsg = "";
					switch(error.code) {
						case error.PERMISSION_DENIED:
							errorMsg = "User does not want to share location.";
							break;
						case error.POSITION_UNAVAILABLE:
							errorMsg = "User wants to share, but no location available.";
							break;
						case error.TIMEOUT:
							errorMsg = "Whatever supplies location has timed out.";
							break;
						default:
							errorMsg = "Who knows what happened...";
							break;
					}
					//alert(errorMsg);
				}
			);
		} else {
			//alert("browser does not support geolocation");
		}
		<?php }//end asking if GeoLocation?>
	});
</script>