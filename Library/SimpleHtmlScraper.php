<?php

class SimpleHtmlScraper{
    private $url;
    private $scrapedContent;
    private $domDocument;
    private $finder;

    public function __construct($url){
        $this->url = $url;
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $rawContent = curl_exec($ch);
        curl_close($ch);
		libxml_use_internal_errors(true);
        $this->scrapedContent = $this->cleanEncoding($rawContent);
        $this->domDocument = new DOMDocument();
        $this->domDocument->loadHTML($this->scrapedContent);
        $this->finder = new DomXPath($this->domDocument);
    }

    private function cleanEncoding($string){
        $encodedContent = mb_convert_encoding($string, 'utf-8', mb_detect_encoding($string));
        $entityEncodedContent = mb_convert_encoding($encodedContent, 'html-entities', 'utf-8');
        return $entityEncodedContent;
    }

    public function getNodesByClassName($classname){
        $nodes = $this->finder->query("//*[contains(@class, '$classname')]");
        $entries = array();
        foreach ($nodes as $node){
            $extractedString = $node->C14N();
            $entries[] = $this->cleanEncoding($extractedString);
        }
        return $entries;
    }

    public static function get($urlToScrape, $scrapeClassName){
        $scrapeResults = "";
        $scraper = new SimpleHtmlScraper($urlToScrape);
        $nodes = $scraper->getNodesByClassName($scrapeClassName);
        foreach ($nodes as $node){
            $scrapeResults .= $node;
        }
        return $scrapeResults;
    }
}
// -------------
// USAGE EXAMPLE
// -------------


//$urlToScrape = "http://blog.causetown.org/do-good-and-drive-local-business-sales-on-make-a-difference-day/";
//include('Library/SimpleHtmlScraper.php');
//$content = SimpleHtmlScraper::get($urlToScrape);
//echo "<div class='wordpress-content'>".$content."</div>";

/**
 * This scraper is specifically for a case where the cms is embedded in the website, hence we have access to the wp-api.
 * However, if the page does not exist, or cannot be retrieved using the api, the url is a fallback.
 */
class WordPressContentScraper {

    /**
     * Registered pages mapped to two values:
     *  1 - the url part if we need to go scrape the page old-school style
     *  2 - the page title if we are using the wordpress API to get the content
     * @var array
     */
    private static $REGISTERED_PAGES = array(
        "how_it_works"          => array("/how-it-works/",                      "HowItWorks"),
        "faq"                   => array("/faq/",                               "FAQ"),
        "individual_start"      => array("/homepage/homepage_individuals/",     "HomePage_Individuals"),
        "merchant_start"        => array("/homepage/homepage_merchants/",       "HomePage_Merchants"),
        "organization_start"    => array("/homepage/homepage_organizations/",   "HomePage_Organizations"),
        "about_us"              => array("/about-us/",                          "OurTeam"),
        "privacy_policy"        => array("/privacypolicy/",                     "PrivacyPolicy"),
        "terms_and_conditions"  => array("/terms-and-conditions/",              "TermsandConditions"),
        "merchant_signup"       => array("/signup/signup_merchants/",           "SignUp_Merchants"),
        "campaigns"             => array("/campaigns/",                         "Campaigns"),
        "associations"          => array("/associations/",                      "Associations")
    );

    private $cmsBaseUrl;
    private $wpApiPath;

    public function __construct($cmsBaseUrl, $wpApiPath){
        $this->cmsBaseUrl = $cmsBaseUrl;
        $this->wpApiPath = $wpApiPath;
    }

    public function getPage($registeredPage){
        $pageParts = self::$REGISTERED_PAGES[$registeredPage];
        if (empty($pageParts)){
            trigger_error("WordpressContentScraper.getPage failed getting registered page '".$registeredPage."'", E_USER_ERROR);
            return "";
        }
        $pagePart = $pageParts[0];
        $title = $pageParts[1];
        $urlToScrape = $this->cmsBaseUrl.$pagePart;
        return self::scrape($urlToScrape, $this->wpApiPath, $title, "page");
    }

    public static function scrape($urlToScrape, $wpApiPath = null, $title = null, $type = "page"){
        $content = null;
        if (!empty($wpApiPath) && !empty($title) && file_exists($wpApiPath)){
            define('WP_USE_THEMES', false);
            require($wpApiPath);
            if (function_exists("get_page_by_title") && function_exists("wpautop")){
                $page = get_page_by_title($title, OBJECT, $type);
                if (!empty($page)){
                    $content = wpautop($page->post_content);
                }
            }
        }
        if (empty($content)){
            $content = SimpleHtmlScraper::get($urlToScrape, "entry-content");
        }
        if (strpos($content, "<form ")){
            $content = "";
            trigger_error("WordpressContentScraper.scrape found an illegal form element in path ".$urlToScrape, E_USER_ERROR);
        }
        return $content;
    }
}

// -------------
// USAGE EXAMPLE
// -------------


//$urlToScrape = "http://".$Config_CMSAuthenication."@cms.causetown.org/private/cms/about-us/";
//$pageTitle = "OurTeam";
//include('Library/SimpleHtmlScraper.php');
//$content = WordPressContentScraper::get($urlToScrape, $Config_WordPressAPILocation, $pageTitle);
//echo "<div class='wordpress-content'>".$content."</div>";

?>