<?php
/**
 * This file requires certain variables to be set in the global space or a global variable called $socialEventRecord
 * that is of type EventRecord (see DataContracts.php).
 *
 * This template will take the data in $socialEventRecord and build a new EventRecord with the required data for the
 * API call. Otherwise, it will use the global variables and create a new EventRecord with the required data for the API call.
 *
 * Using global variables is deprecated but supported for legacy templates.
 */
//if (!$isDevMode_Eventbrite){

    include_once($repositoryApiFolder."DataContracts.php");

    //Post to EventBrite
    $APIKey = $Config_Eventbrite_APIKey;
    $APIUser_key = $Config_Eventbrite_Userkey;
    $EventOrganizerID = $Config_Eventbrite_OrganizerID;
    if (!isset($Config_EventBrite_EventPrivacyType)){
        $Config_EventBrite_EventPrivacyType = 1; // default to public
    }

    //
    // API Calls
    //
    include_once($dbProviderFolder."GeoLookupProvider.php");
    include_once($dbProviderFolder."MySqliEventManager.php");
    include_once($repositoryApiFolder."EventBrite_API.php");
    include_once("_setupGeolookupConnection.php");
    $locationProvider = new GeoLookupProvider($geoConn);
    $eventManager = new MySqliEventManager($mysqli);
    if (!$isDevMode_Eventbrite){
        $eventBriteApi = new EventBriteApi($APIKey, $APIUser_key, $EventOrganizerID,$locationProvider);
    } else {
        $eventBriteApi = new LoggingEventBriteApi($APIKey, $APIUser_key, $EventOrganizerID,$locationProvider);
        $eventBriteApi->setEventId($socialEventRecord->id);
        $eventBriteApi->setMemberId(getMemberId());
    }

    // create or get venue id
    $ebVenueId = $socialEventRecord->venue->eventBriteVenueID;
    if (!$ebVenueId){
        $ebVenueId = $eventBriteApi->getVenueId($socialEventRecord->venue);
        $eventManager->setEventBriteVenueID($socialEventRecord->venue->id, $ebVenueId);
    }

    // create the event
    $asPrivate = $Config_EventBrite_EventPrivacyType != 1;
    $asDraft = false;
    $createResponse = $eventBriteApi->createEvent($socialEventRecord, $asPrivate, $asDraft);
    if ($createResponse->message){
        $EventBriteEventError = $createResponse->message;
    }
    $socialEventRecord = $createResponse->record;
    if ($socialEventRecord->id && $socialEventRecord->venue->eventBriteEventID){
        $eventManager->setEventBriteID($socialEventRecord);
        $EventBriteEventID = $socialEventRecord->venue->eventBriteEventID;
        $ebTicketId = $eventBriteApi->createTicket($socialEventRecord);
    }

//} else {
//	$EventBriteEventError = "You are operating in development mode.";
//}
?>