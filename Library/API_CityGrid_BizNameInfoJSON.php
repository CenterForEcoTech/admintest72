<?php
/*
 * DEAD -- only called by PartnerPicker.php, which is itself DEAD
 */
trigger_error("Calling dead file API_CityGrid_BizNameInfoJSON.php", E_USER_ERROR);
die();
include_once("../assert_is_ajax.php");
//Following code allows is a workaround for local server issues that use IP6protocal
if ($_SERVER['REMOTE_ADDR'] == '::1'){
	$_SERVER['REMOTE_ADDR'] = gethostbyname('cetonline.org');
}
if (!$CityGridProfileID){
	$CityGridProfileID = $_GET['CityGridProfileID'];
}
//echo "ID=".$CityGridProfileID;

if(!$PartnerPicker){
	//take 10digits and turn into phone display
	function format_tel_num($number){
		return preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($number)), 2);
	}
}	
//only do this if there is a profile to display

//Do a search to see if this merchant is already in the system
//TODO create this search and return JSON values so it can be processed to alert the user if this business has already been registered in this system or not

if ($CityGridProfileID){
$return_arr = array();
	$PublisherID =10000001282;

	$xmlsrc = "http://api.citygridmedia.com/content/places/v2/detail?listing_id=".$CityGridProfileID."&client_ip=".$_SERVER['REMOTE_ADDR']."&publisher=".$PublisherID."&format=json";
	//echo $xmlsrc;
	// Send CityGrid API Call
	$ch = curl_init($xmlsrc);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	$data = curl_exec($ch); // Yelp response
	curl_close($ch);

	// Handle CityGrid response data
	$results = json_decode($data);
	//print_r($results);
	//echo $signed_url;

	$Locations = $results->locations; //array of all locations from result
    if (count($Locations)){
        $CityGridID = $Locations[0]->public_id;
            $row_array['CityGridID'] = $CityGridID;
            $row_array['CityGridPublicID'] = $Locations[0]->public_id; // duplicating CityGridID to be absolutely sure which it is
            //echo $CityGridID."<bR>\r\n";
        $CityGridProfileID = $Locations[0]->id;
            $row_array['CityGridProfileID'] = $CityGridProfileID;
            //echo $CityGridProfileID."<bR>\r\n";
        $CityGridProfile = $Locations[0]->urls->profile_url;
            $row_array['CityGridProfile'] = $CityGridProfile;
            //echo $CityGridProfile."<bR>\r\n";
        $BizName = $Locations[0]->name;
            $row_array['BizName'] = $BizName;
            //echo $BizName."<br>";
        $Address = $Locations[0]->address->street;
            $row_array['Address'] = $Address;
            //echo $Address."<br>";
        $City = $Locations[0]->address->city;
            $row_array['City'] = $City;
            //echo $City." ";
        $State = $Locations[0]->address->state;
            $row_array['State'] = $State;
            //echo $State." ";
        $Zip = $Locations[0]->address->postal_code;
            $row_array['Zip'] = $Zip;
            //echo $Zip."<br>";
        $Lat = $Locations[0]->address->latitude;
            $row_array['Lat'] = $Lat;
            //echo $Lat."<br>";
        $Long = $Locations[0]->address->longitude;
            $row_array['Long'] = $Long;
            //echo $Long."<br>";
        $Neighborhoods = $Locations[0]->neighborhoods;
            if (count($Neighborhoods)){
                $row_array['Neighborhood'] = implode(", ", $Neighborhoods);
            }

            //echo $Neighborhood[0]."<br>";
        $image_url = $Locations[0]->images[0]->image_url;
            if ($image_url){
                $row_array['Image_url'] = $image_url;
                //echo "<img src=\"".$image_url."\"><br>";
            }
        $Phone = $Locations[0]->contact_info->display_phone;
            $row_array['Phone'] = format_tel_num($Phone);
        $Website = $Locations[0]->contact_info->display_url;
        if ($Website){
            // let's lowercase it if it's allcaps
            $pattern = "/(http.?:\/\/)?(.*)/i";
            preg_match($pattern, $Website, $matches);
            if (count($matches) == 3){
                $url = $matches[2];
                $allcapsUrl = strtoupper($url);
                if ($allcapsUrl === $url){
                    $Website = strtolower($Website);
                }
                // a little clean up
                if (strlen($matches[1]) == 0){
                    // protocol is missing! let's add a default
                    $Website = "http://".$Website;
                }
            }
        }
            $row_array['Website'] = $Website."";
        $Description = $Locations[0]->customer_content->customer_message->value;
            $row_array['Description'] = str_replace('\n', PHP_EOL.PHP_EOL, rtrim($Description,'\n'));
            //echo format_tel_num($Phone)."<br>";
        //$Review = "Yosh".$Locations[0]->review_info->reviews[0]->review_rating."!";
        $ReviewRating = $Locations[0]->review_info->reviews[0]->review_rating;
        $ReviewTitle = $Locations[0]->review_info->reviews[0]->review_title;
        $ReviewText = $Locations[0]->review_info->reviews[0]->review_text;
            $row_array['ReviewRating'] = $ReviewRating;
            $row_array['ReviewTitle'] = $ReviewTitle;
            $row_array['ReviewText'] = $ReviewText;
        //echo $Locations[$L]->name."<br>";

        array_push($return_arr,$row_array);
    }
	if(!$PartnerPicker){
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
		echo json_encode($return_arr);
	}
	
//echo $CityGrid->locations->location[0]->name;
}//end if CityGridProfileID
?>