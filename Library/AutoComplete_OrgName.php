<?php
include_once("_getRootFolder.php");
include_once($rootFolder."assert_is_ajax.php");
$term=$_GET['term'];
if ($term){
    $numericSearchTerm = preg_replace('/\D/', '', $term);
    include_once($rootFolder.'Global_Variables.php');
    include_once($repositoryApiFolder."ServiceAPI.php");
    include_once($dbProviderFolder."MySqliCharitySearchProvider.php");

    include_once($siteRoot."_setupSearchConnection.php");
    $provider = new MySqliFullTextCharitySearchProvider($searchConn);
    $criteria = new CharitySearchCriteria();
    $criteria->term = $_GET['term'];
    $criteria->start = 0;
    $criteria->size = 0;
    $result = $provider->search($criteria);
    /* @var $result PaginatedResult */
    $searchResults = array();
    // TODO: make modifications for the front end getting the new charity search widget;
    //       for now, map new data to old object model
    foreach ($result->collection as $charitySearchObject){
        /* @var $charitySearchObject CharitySearchObject */
        $result = (object)array(
            "OrgID" => $charitySearchObject->orgId,
            "Address" => $charitySearchObject->address1,
            "City" => $charitySearchObject->city,
            "State" => $charitySearchObject->region,
            "Zip" => $charitySearchObject->postalCode,
            "OrgName" => $charitySearchObject->display,
            "OrgEIN" => $charitySearchObject->ein,
            "IsFiscallySponsored" => $charitySearchObject->isChildOrg
        );
        $searchResults[] = $result;
    }
    output_json($searchResults);
}//end if term
?>