<?php

$unsigned_url = "http://api.yelp.com/v2/business/".$YelpID;
//echo $unsigned_url;

// Token object built using the OAuth library
$token = new OAuthToken($token, $token_secret);

// Consumer object built using the OAuth library
$consumer = new OAuthConsumer($consumer_key, $consumer_secret);

// Yelp uses HMAC SHA1 encoding
$signature_method = new OAuthSignatureMethod_HMAC_SHA1();

// Build OAuth Request using the OAuth PHP library. Uses the consumer and token object created above.
$oauthrequest = OAuthRequest::from_consumer_and_token($consumer, $token, 'GET', $unsigned_url);

// Sign the request
$oauthrequest->sign_request($signature_method, $consumer, $token);

// Get the signed URL
$signed_url = $oauthrequest->to_url();

// Send Yelp API Call
$ch = curl_init($signed_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, 0);
$data = curl_exec($ch); // Yelp response
curl_close($ch);

// Handle Yelp response data
$response = json_decode($data);
//echo $signed_url;

// Print it for debugging
if (!$response->name){
	//print_r($response);
}
	//print_r($response);

	//echo "Name: ".$response->name."<br>";
	//echo "YelpID: ".$response->id."<bR><hr>";
	$YelpID = $response->id;
	$BizName = $response->name;
	$image_url = $response->image_url;	//URL of photo for this business
	$url = $response->url;	//URL for business page on Yelp
	$mobile_url = $response->mobile_url;	//URL for mobile business page on Yelp
	$Phone = $response->phone;	//Phone number for this business with international dialing code (e.g. +442079460000)
	$display_phone = $response->display_phone;	//Phone number for this business formatted for display
	$categories = $response->categories; //Provides a list of category name, alias pairs that this business is associated with. For example, [["Local Flavor", "localflavor"], ["Active Life", "active"], ["Mass Media", "massmedia"]] 
	$rating = $response->rating;	//number	Rating for this business (value ranges from 1, 1.5, ... 4.5, 5)
	$rating_img_url = $response->rating_img_url;	//URL to star rating image for this business (size = 84x17)
	$rating_img_url_small = $response->rating_img_url_small;	//URL to small version of rating image for this business (size = 50x10)
	$rating_img_url_larg = $response->rating_img_url_large;	//URL to large version of rating image for this business (size = 166x30)
	$Latitude= $response->location->coordinate->latitude;	//number	Latitude for this business
	$Longitude = $response->location->coordinate->longitude;	//number	Longitude for this business
	$Address = $response->location->address;	//list	Address for this business. Only includes address fields.
	$AddressFull = $response->location->display_address;	//list	Address for this business formatted for display. Includes all address fields, cross streets and city, state_code, etc.
	$City = $response->location->city;	//City for this business
	$State = $response->location->state_code;	//ISO 3166-2 state code for this business
	$Zip = $response->location->postal_code;	//string	Postal code for this business
	$Country = $response->location->country_code;	//string	ISO 3166-1 country code for this business
	$CrossStreets = $response->location->cross_streets;	//string	Cross streets for this business
	$Neighborhoods = $response->location->neighborhoods;	//list	List that provides neighborhood(s) information for business
	$reviews = $response->reviews[0]->excerpt;
	$reviews_image = $response->reviews[0]->user->image_url;
?>