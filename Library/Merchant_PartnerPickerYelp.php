<?php
// KILLING THIS TEMPLATE UNTIL FURTHER NOTICE (SQL INJECTION)
trigger_error("Merchant_PartnerPickerYelp.php unauthorized call.", E_USER_ERROR);
die();
?>
		<script type="text/javascript">
		$(function() {
			$('#BizName').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizName.php",
						dataType: "json",
						data: {
							term : request.term,
							bizlocation : $('#BizLocation').val()
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#YelpID').val(ui.item.YelpID);
					$('#MerchantID').val(ui.item.MerchantID);
					document.getElementById('YelpID').style.color='black';
					document['PartnerPickerForm'].submit();
					calcHeight();
				}
			});
			$('#SearchCity').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'City'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchCity').val(ui.item.value);
					document['PartnerPickerForm'].submit();
					calcHeight();
				}
			});
			$('#SearchState').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'State'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchState').val(ui.item.value);
					document['PartnerPickerForm'].submit();
					calcHeight();
				}
			});
			$('#SearchZip').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'Zip'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchZip').val(ui.item.value);
					document['PartnerPickerForm'].submit();
					calcHeight();
				}
			});
		});

		function calcHeight(){
			var t=setTimeout("resizeHeight()",500);
			var x=setTimeout("resizeHeight()",500);
		}
		
		function resizeHeight(){
			//find the height of the internal page
			var the_height=document.getElementById('PartnerPickerIframe').contentWindow.document.body.scrollHeight;

			//change the height of the iframe
			document.getElementById('PartnerPickerIframe').height=the_height;
		}


		</script>

		<div id="Organization_CreateEvent_BizMatch">
			<form name="PartnerPickerForm" id="PartnerPickerForm" action="PartnerPicker.php?OrgEventID=<?php echo $_GET['OrgEventID'];?>" target="PartnerPickerIframe" method="post">
			<input type="hidden" id="OrgEventID" name="OrganizationEvent_ID" value="<?php echo $_GET['OrgEventID'];?>">
			<input type="hidden" id="PartnerRating" name="PartnerRating" value="">
			<input type="hidden" id="BizLocation" name="BizLocation" value="Chicago">
			<input type="hidden" id="YelpID" name="YelpID" value="">
			<input type="hidden" id="MerchantID" name="MerchantID" value="">
			<div class="InputInput InputWide">Already have a business partner at <?php echo $SiteName;?> in mind?  Use our PartnerPicker to find the perfect match and we'll make the connection to try and make your perfect event happen.</div>
			<div class="InputRowSpace"></div>
			<div class="InputTitle">&nbsp;</div><div class="InputInput InputNarrow"><input type="text" class="InputOrgEvent" id="BizName" name="BizName" value="Search By Business Name or Key Words"<?php echo onFocus("Search By Business Name or Key Words");?>></div>

			<div class="InputRowSpace">OR</div>

			<div class="InputTitle">&nbsp;</div>
			<div class="InputInput InputWide">
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchCity" name="SearchCity" value="Search By City"<?php echo onFocus("Search By City");?>>
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchState" name="SearchState" value="State"<?php echo onFocus("State");?>>
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchZip" name="SearchZip" value="or Zip"<?php echo onFocus("or Zip");?>>
				Distance <select name="SearchDistance"><option value="3">3</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Miles	
			</div>
				
			<div class="InputRowSpace"></div>

			<div class="InputInput InputVeryNarrow">
				<div class="UnderLineTitle">Yelp Rating</div>
				<input type="radio" name="ParterYelpRating" value="1" onClick="document.getElementById('PartnerRating').value='1';document['PartnerPickerForm'].submit();calcHeight();"><img src="http://media1.px.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_1.png" class="PickableImage"><Br clear="all">
				<input type="radio" name="ParterYelpRating" value="2" onClick="document.getElementById('PartnerRating').value='2';document['PartnerPickerForm'].submit();calcHeight();"><img src="http://media1.px.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_2.png" class="PickableImage"><Br clear="all">
				<input type="radio" name="ParterYelpRating" value="3" onClick="document.getElementById('PartnerRating').value='3';document['PartnerPickerForm'].submit();calcHeight();"><img src="http://media1.px.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_3.png" class="PickableImage"><Br clear="all">
				<input type="radio" name="ParterYelpRating" value="4" onClick="document.getElementById('PartnerRating').value='4';document['PartnerPickerForm'].submit();calcHeight();"><img src="http://media1.px.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_4.png" class="PickableImage"><Br clear="all">
				<input type="radio" name="ParterYelpRating" value="5" onClick="document.getElementById('PartnerRating').value='5';document['PartnerPickerForm'].submit();calcHeight();"><img src="http://media1.px.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_5.png" class="PickableImage"><Br clear="all">

			</div>
			</form>
			<div class="InputInput InputWide">
				<iframe src="PartnerPicker.php?OrgEventID=<?php echo $_GET['OrgEventID'];?>" name="PartnerPickerIframe" SCROLLING="no" id="PartnerPickerIframe" class="PartnerPickerIframe" frameborder="0" allowtransparency="true" background-color="transparent"></iframe>
			</div>
		</div>
