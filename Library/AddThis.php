<?php
// http://support.addthis.com/customer/portal/articles/381263-addthis-client-api#rendering-preferred-services
?>
<!--AddThis-->
<?php if (!$AddThisJavascriptLoaded){?>
	<script type="text/javascript">
		var addthis_config = {"data_track_clickback":true};
	</script>
	<script type="text/javascript" src="http<?php echo $secureServer;?>://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4ec762c77baf8b7a" rev="shown" rel="forceLoad"></script>
<?php }?>
<?php
if (!$AddThisURL){$AddThisURL = " addthis:url=\"".$CurrentServer.$AddThisURLExtra."\"";}
if (!$AddThisTitle){$AddThisTitle = " addthis:title=\"".$SiteTitle.$AddThisTitleExtra."\"";}
if (!$AddThisDescription){$AddThisDescription = " addthis:description=\"".$SiteDescription.$AddThisDescriptionExtra."\"";}

$AddThisCustomURL = $AddThisURL.$AddThisTitle.$AddThisDescription;
?>

<?php if ($pillStyleOnly){?>
<a class="addthis_counter addthis_pill_style"<?php echo $AddThisCustomURL;?>></a>
<?php } else if (true) {?>
    <!-- AddThis Button BEGIN -->
    <div class="addthis_toolbox addthis_default_style "<?php echo $AddThisCustomURL;?>>
        <a class="addthis_button_preferred_1"></a>
        <a class="addthis_button_preferred_2"></a>
        <a class="addthis_button_preferred_3"></a>
        <a class="addthis_button_preferred_4"></a>
        <a class="addthis_button_compact"></a>
        <a class="addthis_counter addthis_bubble_style"></a>
    </div>
    <!-- AddThis Button END -->
<?php } else { ?>
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style"<?php echo $AddThisCustomURL;?>>
<a class="addthis_button_facebook_like"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<!-- AddThis Button END -->
<?php } ?>