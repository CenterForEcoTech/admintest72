<?php
include_once('../assert_is_ajax.php');
include_once('../siteconf/dbconf.php');
include_once("../_setupGeolookupConnection.php");
include_once("../repository/mySqlProvider/GeoLookupProvider.php");

$geoLookupProvider = new GeoLookupProvider($geoConn);
$return_arr = $geoLookupProvider->lookupCity($_GET['term']);
if (count($return_arr) == 0){
    echo "no result";
}
echo json_encode($return_arr);
?>