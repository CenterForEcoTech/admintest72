{"locations":
	[
		{
			"id":696307520,
			"reference_id":"1",
			"impression_id":"0009000000a885f36ed204480cb193f80a3cc850e8",
			"display_ad":true,
			"infousa_id":554756551,
			"name":"HARNEY & SONS FINE TEAS",
			"teaser":null,
			"address":{
				"street":"5723 ROUTE 22",
				"delivery_point":null,
				"city":"MILLERTON",
				"state":"NY",
				"postal_code":"12546",
				"cross_street":"",
				"latitude":41.941347,
				"longitude":-73.528583},
			"contact_info":{
				"display_phone":"8457892100",
				"display_url":"HARNEY.COM",
				"social_media":null},
			"markets":["Poughkeepsie, NY Metro"],
			"neighborhoods":[],
			"urls":{
				"profile_url":"http://www.citysearch.com/profile/696307520/millerton_ny/harney_sons_fine_teas.html",
				"reviews_url":"http://www.citysearch.com/review/696307520",
				"video_url":null,
				"website_url":"http://pfpc.citygridmedia.com/content/places/v2/click?q=Eg5LbGeFZEmz9alMRG3UqHCSHTWWqqpiUFE37OfVvOS4DKxjtAevqWXgLmxbKGzO-K6rUPs2iuNNLuEDm647X2A0rNm93FESCsmHF1eVGT-ndFZd5c8ElNiQMRxS4HRjZ1SpcXMNfLFXmYMcem5oWaBhFiFiK4KN8msbOyqvzEn991gC-OQI0UwEqWz5LLCOli8QA8_ro2_XmDmcg2wTiwYkkkQLnFIkeskQ_xJl9q08sSqLxh8vM5pA8rSAmeHTQJfY7KflA8wpVt4NzHpHPCRUtK-OLMe8Z7aZN1wM6XbpmikldMi_6Y5kUGM5ikRkpTvnfYtR4GPQQkqyq_Cg5EVs8NAM1mmk",
				"menu_url":null,
				"reservation_url":null,
				"map_url":"http://www.citysearch.com/profile/map/696307520/millerton_ny/harney_sons_fine_teas.html",
				"send_to_friend_url":"http://www.citysearch.com/profile/sendto/696307520/millerton_ny/harney_sons_fine_teas.html",
				"email_link":null,
				"custom_link_1":null,
				"custom_link_2":null,
				"web_comment_url":null,
				"web_article_url":null,
				"web_profile_url":null,
				"web_rates_url":null,
				"gift_url":null,
				"request_quote_url":null,
				"virtual_tour_url":null,
				"book_limo_url":null,
				"order_url":null,
				"custom_link_3":null,
				"custom_link_4":null},
			"impression_url":null,
			"customer_content":{
				"customer_message":{
					"attribution_source":null,
					"attribution_logo":null,
					"attribution_text":null,
					"value":null,
					"attribution_url":null},
				"bullets":null,
				"customer_message_url":null},
			"offers":[],
			"categories":[
				{
					"name_id":2333,
					"name":"Coffee & Tea Manufacturers",
					"parent_id":2295,
					"parent":"Food Manufacturers",
					"primary":false,
					"groups":[]
				},
				{
					"name_id":5699,
					"name":"Wholesale Coffee & Tea",
					"parent_id":5693,
					"parent":"Wholesale Foods",
					"primary":true,
					"groups":[]
				}],
			"attributes":[],
			"business_hours":"",
			"parking":"",
			"tips":[],
			"images":[],
			"editorials":[],
			"years_in_business":null,
			"last_update_time":"2011-09-08T00:00:00-07:00",
			"public_id":"harney-sons-fine-teas-millerton-2",
			"business_operation_status":"open",
			"attribution_source":null,
			"attribution_logo":null,
			"attribution_text":null,
			"attribution_url":null,
			"claimed":false,
			"geographies":[
				{
					"id":112644,
					"value":"North East",
					"type":"city"
				},
				{
					"id":41867,
					"value":"12546",
					"type":"postal.code"
				},
				{
					"id":77500,
					"value":"Dutchess",
					"type":"county"
				},
				{
					"id":82227,
					"value":"New York",
					"type":"state"
				},
				{
					"id":81973,
					"value":"Poughkeepsie, NY Metro",
					"type":"market"
				}],
			"review_info":{
				"overall_review_rating":null,
				"total_user_reviews":0,
				"total_user_reviews_shown":0,
				"reviews":[]}
		}
	]
}

Alamo Rent A Car provides a wide selection of vehicles for personal and business car rental needs at locations in more than 42 countries worldwide. \n

Car rental options range from economy and compact cars to convertibles, SUVs, luxury cars, minivans, pickup trucks, cargo vans, 15 passenger vans, and hybrids. 
Alamo offers exceptional rates on a wide selection of rental cars. 
Alamo.com offers internet-only car rental exclusives, daily, weekly, weekend and one-way rental car specials. 
Planning and reserving a rental car for your vacation or business trip couldn't be easier.  \n

Skip the line at the airport rental car counters with online Check-Ins. 
Just choose your rental car and check-out at the exit booth. 
Online Check-In is available at select Alamo Rent A Car airport locations.  
Full list of locations available on the website\n
