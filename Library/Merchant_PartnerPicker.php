<?php
// KILLING THIS TEMPLATE UNTIL FURTHER NOTICE (SQL INJECTION)
trigger_error("Merchant_PartnerPicker.php unauthorized call.", E_USER_ERROR);
die();
?>
		<script type="text/javascript">
		$(function() {
			$('#BizName').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizName.php",
						dataType: "json",
						data: {
							term : request.term
						},
						success: function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.BizName + " - " + item.Address + " " + item.City +" "+ item.State,
									value: item.BizName,
									YelpID: item.YelpID,
									CityGridID: item.CityGridID,
									MerchantID: item.MerchantID
								}
							}));
							if (!data.BizName){
								document.getElementById('YelpID').value = '';
								document.getElementById('CityGridID').value = '';
								document.getElementById('MerchantID').value = '';
							}

						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#YelpID').val(ui.item.YelpID);
					$('#CityGridID').val(ui.item.CityGridID);
					$('#MerchantID').val(ui.item.MerchantID);
					document.getElementById('YelpID').style.color='black';
					DisplayPartners();
					//document['PartnerPickerForm'].submit();
					//calcHeight();
				}
			});
			$('#SearchCity').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'City'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchCity').val(ui.item.value);
					DisplayPartners();
					//document['PartnerPickerForm'].submit();
					//calcHeight();
				}
			});
			$('#SearchState').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'State'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchState').val(ui.item.value);
					DisplayPartners();
					//document['PartnerPickerForm'].submit();
					//calcHeight();
				}
			});
			$('#SearchZip').autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "Library/AutoComplete_BizCityStateZip.php",
						dataType: "json",
						data: {
							term : request.term,
							searchtype : 'Zip'
						},
						success: function(data) {
							response(data);
						}
					});
				},
				minLength: 2,
				delay: 50,
				select: function(event, ui) {
					$('#SearchZip').val(ui.item.value);
					DisplayPartners();
					//document['PartnerPickerForm'].submit();
					//calcHeight();
				}
			});

       
		});
		
		function DisplayPartners(){
				/* get some values from elements on the page: */
				var State = document.getElementById('SearchState').value;
				var City = document.getElementById('SearchCity').value;
				var Zip = document.getElementById('SearchZip').value;
				var CityGrid = document.getElementById('CityGridID').value;
				var Merchant = document.getElementById('MerchantID').value;
				var Biz = document.getElementById('BizName').value;
				<?php if ($_GET['EventID']){?>
					var EventID = <?php echo $_GET['EventID'];?>;
				<?php }else{?>
					var SearchBizValue = 1;
				<?php }?>

				/* Send the data using post and put the results in a div */
				$.post('PartnerPicker.php',
					{ 
						SearchState: State,
						SearchCity: City,
						SearchZip: Zip,
						CityGridID: CityGrid,
						MerchantID: Merchant,
						BizName: Biz,
						<?php if ($_GET['EventID']){?>
							Event_ID: EventID
						<?php }else{?>
							SearchBiz: SearchBizValue
						<?php }?>
		
					},
					function( data ) {
						$( "#PartnerResults" ).empty().append(data);
					}
				);
		}

		</script>

		<div id="Organization_CreateEvent_BizMatch">
			<?php echo $SearchIntro;?>
			<input type="hidden" id="EventID" name="OrganizationEvent_ID" value="<?php echo $_GET['EventID'];?>">
			<input type="hidden" id="OrgID" name="OrganizationID" value="<?php echo $_GET['OrgID'];?>">
			<input type="hidden" id="PartnerRating" name="PartnerRating" value="">
			<input type="hidden" id="YelpID" name="YelpID" value="">
			<input type="hidden" id="CityGridID" name="CityGridID" value="">
			<input type="hidden" id="MerchantID" name="MerchantID" value="">
			<div class="InputRowSpace"></div>
			<div class="InputTitle">&nbsp;</div><div class="InputInput InputNarrow"><input type="text" class="InputOrgEvent" id="BizName" name="BizName" value="Search By Business Name or Key Words" onFocus="if(this.value=='Search By Business Name or Key Words'){this.value='';};" onBlur="if (this.value==''){this.value='Search By Business Name or Key Words';document.getElementById('YelpID').value = '';document.getElementById('CityGridID').value = '';document.getElementById('MerchantID').value = '';}DisplayPartners();"></div>

			<div class="InputRowSpace">OR</div>

			<div class="InputTitle">&nbsp;</div>
			<div class="InputInput InputWide">
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchCity" name="SearchCity" value="Search By City" onFocus="if (this.value=='Search By City'){this.value='';}" onBlur="if(this.value==''){this.value='Search By City'}DisplayPartners();">
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchState" name="SearchState" value="State" onFocus="if (this.value=='State'){this.value='';}" onBlur="if(this.value==''){this.value='State'}DisplayPartners();">
				<input type="text" class="InputOrgEvent InputOrgEventVeryNarrow" id="SearchZip" name="SearchZip" value="or Zip" onFocus="if (this.value=='or Zip'){this.value='';}" onBlur="if(this.value==''){this.value='or Zip'}DisplayPartners();">
				Distance <select name="SearchDistance"><option value="3">3</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Miles	
			</div>
			<div class="InputRowSpace"></div>
			<div id="PartnerResults" style="border:0pt solid red;"><div>
		</div>
