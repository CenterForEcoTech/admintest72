<?php if (isOrganization()){?>
	<div class="btn">
		<a class="button" href="<?php echo $CurrentServer;?>ProposeEvent">Propose a new event</a>
	</div>
    <div class="btn administer">
        <a class="button" href="<?php echo $CurrentServer;?>Organization/ProposedEvents" >Proposed Events</a>
    </div>
<?php }?>
<?php
$SortBy = str_replace("DESC"," DESC",$_GET['SortBy']);
if (!$SortBy){$SortBy ="Event_Date".$DateDesc.", Event_StartTime";}

//if SortBy == ProposedBy figure out who proposed it
if (strpos(" ".$SortBy,"ProposedBy")){
	if ($SortBy == "ProposedBy DESC"){
		$SortDesc = " ASC";
	}else{
		$SortDesc = " DESC";
	}
	$SortBy = "OrganizationInfo_Name".$SortDesc.",MerchantInfo_Name".$SortDesc.",IndividualInfo_FirstName".$SortDesc;
}
//split off SortBy so we don't have a long chain of SortBy
$URLString = explode("SortBy",$_SERVER['REQUEST_URI']);
$URLString[0] = str_replace("Home?LoggedIn",SessionManager::getMemberType(),$URLString[0]);
$URLString[0] = rtrim($URLString[0],"_&");
if (strpos($URLString[0],"Individual/") || strpos($URLString[0],"Merchant/") || strpos($URLString[0],"Organization/")){
	$URLString_Connector = "&";
}else{
	$URLString_Connector = "_";
}
$merchantId = getMerchantId();
$orgId = getOrganizationId();
$individualId = SessionManager::getIndividualId();
?>
			<?php
				if (!$SQL_Events){
					$SQL_Events = "
					    SELECT event_info.*,merchant_info.*,organization_info.*,individual_info.*
					    FROM ((event_info
					    LEFT JOIN merchant_info ON MerchantInfo_ID=Event_ProposedByMerchantID)
					    LEFT JOIN organization_info ON OrganizationInfo_ID=Event_ProposedByOrgID)
					    LEFT JOIN individual_info ON IndividualInfo_ID=Event_ProposedByIndividualID
					    WHERE Event_Status = 'ACTIVE' AND  Event_EndDate >= '".MysqlDateUpdate($TodaysDate)."'".$WhereEventModifier." ORDER BY ".$SortBy;
				}else{
					$SQL_Events = $SQL_Events." ORDER BY ".$SortBy;
				}
				//echo $SQL_Events;
				$result_Events = mysqli_query($result_Events, $SQL_Events);
				if (!mysqli_num_rows($result_Events)){
					$NoEventsFound = true;
					//echo "There are no events found.";
				}else{
					$NoEventsFound = false;
				?>
					<table cellspacing="0" cellpadding="0" border="1" id="Table_Events">
						<thead>
						<tr>
							<th><a href="<?php echo $URLString[0].$URLString_Connector;?>SortBy=Event_Title<?php if($SortBy=="Event_Title"){echo "DESC";}?>"><strong>Title</strong></a></th>
							<th><a href="<?php echo $URLString[0].$URLString_Connector;?>SortBy=Event_Date<?php if($SortBy=="Event_Date"){echo "DESC";}?>"><strong>Start</strong></a></th>
							<th><strong>End</strong></th>
							<th><?php
								if ($IndividualEvents){
									echo "<strong>Status</strong>";
								}else{
									echo "<strong>Featured Cause</strong><br>";
									/* hide the way to sort this column
									echo "<a href=\"".$URLString[0]."&SortBy=OrganizationInfo_Name";
									if($SortBy=="OrganizationInfo_Name"){echo " DESC";}
									echo "\">Org</a> | ";
									echo "<a href=\"".$URLString[0]."&SortBy=MerchantInfo_Name";
									if($SortBy=="MerchantInfo_Name"){echo " DESC";}
									echo "\">Merchant</a> | ";
									echo "<a href=\"".$URLString[0]."&SortBy=IndividualInfo_FirstName";
									if($SortBy=="IndividualInfo_FirstName"){echo " DESC";}
									echo "\">Individual</a>";
									*/
								}
								?>
							</th>
							<th><strong>Hosted By</strong></th>
							<th><strong>Zip Code</strong></th>
						</tr>
						</thead>
						<tbody>
				<?php
				
					$i = 0;
					while ($i < mysqli_num_rows($result_Events)){
						//reset who is hosting the event
						$HostedBy = "";
						$HostedByLink = "";
						$HostedByLinkEnd = "";
						$ZipLocal = "";
						$ProposedBy = "";
						mysqli_data_seek($result_Events,$i);
						$fetch = mysqli_fetch_array($result_Events);
						$ZipLocal = $fetch["MerchantInfo_Zip"];
					
						//determined who proposed this event
						if (mysql_result($result_Events,$i,"Event_ProposedByOrgID")){
							//$ProposedBy = mysql_result($result_Events,$i,"OrganizationInfo_Name");
							$ProposedBy = "<span title=\"".mysql_result($result_Events,$i,"OrganizationInfo_City").", ".mysql_result($result_Events,$i,"OrganizationInfo_State")." ".mysql_result($result_Events,$i,"OrganizationInfo_Zip")."\">".mysql_result($result_Events,$i,"OrganizationInfo_Name")." (".mysql_result($result_Events,$i,"OrganizationInfo_City").", ".mysql_result($result_Events,$i,"OrganizationInfo_State").")</span>";							
						}
						//if (mysql_result($result_Events,$i,"Event_ProposedByIndividualID")){$ProposedBy = mysql_result($result_Events,$i,"IndividualInfo_FirstName")." ".substr(mysql_result($result_Events,$i,"IndividualInfo_LastName"),0,1).".";}
						//if (mysql_result($result_Events,$i,"Event_ProposedByMerchantID")){$ProposedBy = mysql_result($result_Events,$i,"MerchantInfo_Name");}

						if (mysql_result($result_Events,$i,"Event_ProposedByIndividualID")){$ThisStatus="Proposed By You";}
						if (mysql_result($result_Events,$i,"Event_ProposedByMerchantID")){mysqli_data_seek($result_Events,$i);
						$fetch = mysqli_fetch_array($result_Events);
						$HostedBy = $fetch["MerchantInfo_Name"];}
						//echo mysql_result($result_Events,$i,"Event_ID"). "= ".$HostedBy;
						$MerchantAccepted = false;
							//if($HostedBy != $ProposedBy){
								$SQL_Hosted = "SELECT event_match.*, merchant_info.*, event_transaction.* FROM (event_match LEFT JOIN merchant_info ON MerchantInfo_ID=EventMatch_MerchantID) LEFT JOIN event_transaction ON EventTransaction_EventID=EventMatch_EventID WHERE EventMatch_EventID=".mysql_result($result_Events,$i,"Event_ID")." OR EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID");
								//$SQL_Hosted = "SELECT event_match.*, event_transaction.*, merchant_info.* FROM (event_transaction LEFT JOIN event_match ON EventTransaction_EventID=EventMatch_EventID) LEFT JOIN merchant_info ON MerchantInfo_ID=EventTransaction_MerchantID WHERE EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID")." OR EventMatch_EventID=".mysql_result($result_Events,$i,"Event_ID");
								$SQL_Transacted = "SELECT merchant_info.*, event_transaction.* FROM event_transaction LEFT JOIN merchant_info ON EventTransaction_MerchantID=MerchantInfo_ID WHERE EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID");
								//echo $SQL_Hosted."<br>";
								$result_Hosted = mysqli_query($result_Hosted, $SQL_Hosted);
								$result_Transacted = mysqli_query($result_Transacted, $SQL_Transacted);
								if (mysqli_num_rows($result_Hosted)>0 || mysqli_num_rows($result_Transacted)>0){
									$h = 0;
									while ($h < mysqli_num_rows($result_Hosted)){
										if (!strpos(" ".$HostedBy,mysql_result($result_Hosted,$h,"MerchantInfo_Name"))){
											//echo "EventID=".mysql_result($result_Events,$i,"Event_ID")." has transactions".strpos(" ".$HostedBy,mysql_result($result_Hosted,$h,"MerchantInfo_Name"));
											$HostedBy = $HostedBy . mysql_result($result_Hosted,$h,"MerchantInfo_Name")."<hr>";
											if (mysql_result($result_Hosted,$h,"MerchantInfo_Zip")){
												$ZipLocal = $ZipLocal.mysql_result($result_Hosted,$h,"MerchantInfo_Zip")."<hr>";
											}
										}
											
										//don't allow merchant to accept the same event twice
										if (mysql_result($result_Hosted,$h,"EventMatch_MerchantID") == getMerchantId()){
											$MerchantAccepted = true;
										}
										$h++;
									}//end looping through matches to the event
									$h = 0;
									while ($h < mysqli_num_rows($result_Transacted)){
										if (!strpos(" ".$HostedBy,mysql_result($result_Transacted,$h,"MerchantInfo_Name"))){
											//echo "EventID=".mysql_result($result_Events,$i,"Event_ID")." has transactions".strpos(" ".$HostedBy,mysql_result($result_Hosted,$h,"MerchantInfo_Name"));
											$HostedBy = $HostedBy . mysql_result($result_Transacted,$h,"MerchantInfo_Name")."<hr>";
											if (mysql_result($result_Transacted,$h,"MerchantInfo_Zip")){
												$ZipLocal = $ZipLocal.mysql_result($result_Transacted,$h,"MerchantInfo_Zip")."<hr>";
											}
										}
										$h++;
									}//end looping through matches to the event
									
									if ($merchantId && mysql_result($result_Events,$i,"Event_ProposedByMerchantID") != $merchantId){
										$HostedByLink2 = "<a href=\"".$CurrentServer."Merchant/Event/Accept/".mysql_result($result_Events,$i,"Event_ID")."\">";
										$HostedByLinkEnd2 = "</a>";
										if (!$MerchantAccepted){
											if (substr($HostedBy,-4)=="<hr>"){$HostedBySpacer = "";}else{$HostedBySpacer = "<hr>";}
											$HostedBy = $HostedBy.$HostedBySpacer.$HostedByLink2."Click to Also Host".$HostedByLinkEnd2;
										}
									}
								} else {
									$HostedByLink = "";
									$HostedByLinkEnd = "";
									if (!$HostedBy){$HostedBy = "Not Yet Hosted";}
									if ($merchantId){
										if (mysql_result($result_Events,$i,"Event_ProposedByMerchantID")){
											$HostedByLink = "";
											$HostedByLinkEnd = "";
											$HostedBy = $HostedBy;
										}else{
											$HostedByLink = "<a href=\"".$CurrentServer."Merchant/Event/Accept/".mysql_result($result_Events,$i,"Event_ID")."\">";
											$HostedByLinkEnd = "</a>";
											$HostedBy = "Click to Host";
										}
									}
									if (!$merchantId && !$orgId){
										//$HostedByLink = "<a href=\"How_ToHostEvent.php\" class=\"nyroModal\">";
										//$HostedByLinkEnd = "</a>";
									}
									mysqli_data_seek($result_Events,$i);
									$fetch = mysqli_fetch_array($result_Events);
									$ZipLocal = $fetch["MerchantInfo_Zip"];
								}//end if matches found to host event
							//}//end if Hosted by someone other than the proposer
							
							$PendingEvent = false;
							//hide pending (ie not hosted events) for Individuals 
							if ($HostedBy == "Not Yet Hosted" && $HidePendingEvents){
								$PendingEvent = true;
							}
							
							//hide events not created by organizations
							if ((mysql_result($result_Events,$i,"Event_ProposedByOrgID") && mysql_result($result_Events,$i,"Event_ProposedByOrgID") != $orgId) && $ShowOnlyMerchantOrSelfCreatedEvents){
								$PendingEvent = true;
							}


							//Only Show rows for items not accepted or if for Individual history
							if ((!$MerchantAccepted || $IndividualEvents || $MerchantEvents) && !$PendingEvent){
								//if ($i_count == 0){$RowClass = " class=\"AltRow\"";}else{$RowClass="";}
								//echo "<tr".$RowClass.">\r\n";
								$TableRow[$i]["RowData"] = "\t<td valign=\"top\" title=\"".htmlspecialchars(mysql_result($result_Events,$i,"Event_Description"))."\">";
								if ($HostedBy == "Not Yet Hosted" && (mysql_result($result_Events,$i,"Event_ProposedByOrgID")==$orgId || mysql_result($result_Events,$i,"Event_ProposedByIndividualID")==$individualId)){
									$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer;
										if (mysql_result($result_Events,$i,"Event_ProposedByOrgID")==$orgId && $orgId){
											$TableRow[$i]["RowData"] .= "Organization/EventEdit/";
										}
										if (mysql_result($result_Events,$i,"Event_ProposedByIndividualID")==$individualId && $individualId){
											$TableRow[$i]["RowData"] .= "Individual/EventEdit/";
										}
									$TableRow[$i]["RowData"] .= mysql_result($result_Events,$i,"Event_ID")."\"><img src=\"".$CurrentServer."images/edit.png\" border=\"0\"></a>";
								}
								if ($MerchantEvents){
									//check to see if event has happened yet
									if (strtotime(date("m/d/Y g:i a")) < strtotime(date("m/d/Y g:i a",strtotime(mysql_result($result_Events,$i,"Event_Date")." ".mysql_result($result_Events,$i,"Event_StartTime"))))){
										//if proposed by Merchant then edit, else view info
										if (mysql_result($result_Events,$i,"Event_ProposedByMerchantID") == $merchantId){
											$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer."ManageEvents/Edit/".mysql_result($result_Events,$i,"Event_ID")."\"><img src=\"".$CurrentServer."images/edit.png\" border=\"0\"></a>";
										}else{
											$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer.$BaseLink."event/".mysql_result($result_Events,$i,"Event_ID")."\">";
										}
									}else{
										$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer."Merchant/SalesHistory/".mysql_result($result_Events,$i,"Event_ID")."\">";
										//prior to friendly URLS
										//echo "<a href=\"".$CurrentServer."Index.php?PageName=mobile/mobile_Merchant_Transaction&EventID=".mysql_result($result_Events,$i,"Event_ID")."&ActiveTab=History&ActiveTabShow=History&EventMatchID=".mysql_result($result_Events,$i,"EventMatch_ID")."\">";
									}
								}elseif ($OrganizationEvents){
									$GeneratedAmount = 0;
									$AmountNotAssigned = 0;
									//like to transaction history if transactions exist
                                    include_once($dbProviderFolder."EventTransactionProvider.php");
                                    $eventTransactionProvider = new EventTransactionProvider($mysqli);
                                    $donationInfo = $eventTransactionProvider->getOrganizationDonations($orgId,mysql_result($result_Events,$i,"Event_ID"));
									//$SQL_Trans = "SELECT SUM(EventTransaction_GeneratedAmount) AS GeneratedAmount FROM event_transaction WHERE EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID")." AND (EventTransaction_OrgID=".$orgId." OR EventTransaction_OrgEIN='".$_SESSION['EIN']."') AND EventTransaction_Status!='Deleted'";
									//echo $SQL_Trans;
									//$SQL_TransNotAssigned = "SELECT SUM(EventTransaction_GeneratedAmount) AS NotYetAssignedAmount FROM event_transaction WHERE EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID")." AND (EventTransaction_OrgID=0 AND EventTransaction_OrgEIN='')  AND EventTransaction_Status!='Deleted'";
									
									//echo  $SQL_Trans;
									//$result_Trans = mysql_query($SQL_Trans);
									//$result_TransNotAssigned = mysql_query($SQL_TransNotAssigned);
									$GeneratedAmount = $donationInfo->totalToThisOrg;
									$AmountNotAssigned = $donationInfo->totalNotAssigned;
									$AmountNotAssignedDisplay = "";
									if ($AmountNotAssigned && false){
										$AmountNotAssignedDisplay = "<br>Amount Not Yet Assigned: $".money($AmountNotAssigned);
									}

									if ($GeneratedAmount > 0){
										$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer."Organization/EventTotals/".mysql_result($result_Events,$i,"Event_ID")."\" style=\"font-weight:bold;\">";
										$EventDisplayTotal = " $".money($GeneratedAmount);
									}else{
										$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer."event/".mysql_result($result_Events,$i,"Event_ID")."\">";
										$EventDisplayTotal = "";
									}
								}else{
									$TableRow[$i]["RowData"] .= "<a href=\"".$CurrentServer."event/".mysql_result($result_Events,$i,"Event_ID")."\">";
								}
								$TableRow[$i]["RowData"] .= mysql_result($result_Events,$i,"Event_Title").$EventDisplayTotal."</a>".$AmountNotAssignedDisplay."</td>\r\n";
								$TableRow[$i]["RowData"] .= "\t<td valign=\"top\">".date("M d", strtotime(mysql_result($result_Events,$i,"Event_Date")));
								//check to see if next year or not
								if (date("Y", strtotime(mysql_result($result_Events,$i,"Event_Date"))) > date("Y", strtotime($TodaysDate)) || $ShowYearInDates){
									$TableRow[$i]["RowData"] .= ", ".date("Y", strtotime(mysql_result($result_Events,$i,"Event_Date")));
								}
								$TableRow[$i]["RowData"] .= " ".date("g:i a", strtotime(mysql_result($result_Events,$i,"Event_StartTime")))."</td>\r\n";
								$TableRow[$i]["RowData"] .= "\t<td valign=\"top\">";
								//check to see if end date is different to show date or not
								$ShowEndDate = false;
								if (strtotime(mysql_result($result_Events,$i,"Event_EndDate")) > strtotime(mysql_result($result_Events,$i,"Event_Date"))){
									$TableRow[$i]["RowData"] .= date("M d", strtotime(mysql_result($result_Events,$i,"Event_EndDate")));
									$ShowEndDate = true;
								}
								if ((date("Y", strtotime(mysql_result($result_Events,$i,"Event_EndDate"))) > date("Y", strtotime($TodaysDate)) || $ShowYearInDates) && $ShowEndDate){
									$TableRow[$i]["RowData"] .= ", ".date("Y", strtotime(mysql_result($result_Events,$i,"Event_Date")));
								}
								$TableRow[$i]["RowData"] .= " ".date("g:i a", strtotime(mysql_result($result_Events,$i,"Event_EndTime")))."</td>\r\n";
								//Get PreferredOrg information
								mysqli_data_seek($result_Events,$i);
								$fetch = mysqli_fetch_array($result_Events);
								$Event_PreferredOrgID = $fetch["Event_PreferredOrgID"];
								$PreferredOrg = "";
								if ($Event_PreferredOrgID){
									$SQL_PrefOrg = "SELECT OrganizationInfo_Name,OrganizationInfo_City,OrganizationInfo_State,OrganizationInfo_Zip FROM organization_info WHERE OrganizationInfo_ID=".$Event_PreferredOrgID;
									//echo mysql_result($result_Events,$i,"Event_Title")."-".$Event_PreferredOrgID."<Br>";
									$result_PrefOrg = mysqli_query($result_PrefOrg, $SQL_PrefOrg);
									$PreferredOrg = "<span title=\"".mysql_result($result_PrefOrg,0,"OrganizationInfo_City").", ".mysql_result($result_PrefOrg,0,"OrganizationInfo_State")." ".mysql_result($result_PrefOrg,0,"OrganizationInfo_Zip")."\">".mysql_result($result_PrefOrg,0,"OrganizationInfo_Name")." (".mysql_result($result_PrefOrg,0,"OrganizationInfo_City").", ".mysql_result($result_PrefOrg,0,"OrganizationInfo_State").")</span>";							
								}
								//if ($PreferredOrg){$PreferredOrg = "<br>Featured Cause: ".$PreferredOrg;}
								if ($IndividualEvents){
									//See if they attended the event
									$SQL_Attend = "SELECT * FROM event_transaction WHERE EventTransaction_EventID=".mysql_result($result_Events,$i,"Event_ID")." AND EventTransaction_CustomerID=".getMemberId();
									$results_Attend = mysqli_query($results_Attend, $SQL_Attend);
									if (mysqli_num_rows($results_Attend)){
										$ThisStatus = "<a href=\"".$CurrentServer."Individual/GivingAccount/".mysql_result($result_Events,$i,"Event_ID")."\" class=\"colorbox\">Attended</a>";
									}
									if (!$ThisStatus && !$PreferredOrg){$ThisStatus = "Customer Picks the Cause";}else{$ThisStatus = $ThisStatus.$PreferredOrg;}
									$TableRow[$i]["RowData"] .= "\t<td>".$ThisStatus."</td>\r\n";
								}else{
									if (!$ProposedBy && !$PreferredOrg){$ThisStatus = "Customer Picks the Cause";}else{$ThisStatus = $ProposedBy.$PreferredOrg;}
									$TableRow[$i]["RowData"] .= "\t<td valign=\"top\">".$ThisStatus."</td>\r\n";
								}
								$TableRow[$i]["RowData"] .= "\t<td valign=\"top\">".$HostedByLink.preg_replace('/<hr>$/', '', $HostedBy).$HostedByLinkEnd."</td>\r\n";
								$TableRow[$i]["RowData"] .= "\t<td valign=\"top\">".preg_replace('/<hr>$/', '', $ZipLocal)."</td>\r\n";
								//Logic to show rows
								if 	($individualId && $HostedBy == "Not Yet Hosted"){
									$TableRow[$i]["HideThisRow"] = true;
								}
								
								if (mysql_result($result_Events,$i,"Event_ProposedByOrgID") && mysql_result($result_Events,$i,"Event_ProposedByOrgID") != $orgId && $HostedBy == "Not Yet Hosted"){
									$TableRow[$i]["HideThisRow"] = true;
								}

								/*coding for displaying Organization information based on OrgID or just EIN
								if (mysql_result($result_Events,$i,"Event_PreferredOrgID")){
									echo "\t<td><a href=\"Organization_Info_DetailsDisplay.php?OrgID=".mysql_result($result_Events,$i,"OrganizationInfo_ID")."\" class=\"nyroModal\">".mysql_result($result_Events,$i,"OrganizationInfo_Name")."</a></td>\r\n";
								}else if (mysql_result($result_Events,$i,"Event_PreferredOrgEIN")){
									$EIN = mysql_result($result_Events,$i,"Event_PreferredOrgEIN");
									include_once('Library/API_FirstGiving_EIN.php');
									echo "\t<td>".ucwords(strtolower($OrganizationName))."</td>\r\n";
								} else {
									echo "\t<td></td>\r\n";
								}
								*/
								//echo "</tr>\r\n";
								//if ($i_count == 1){$i_count =0;}else{$i_count++;}
							}
						$i++;
					}
				}//end if Merchant Events
				
				//now display the rows
				$t = 0;
				while ($t < $i){
					$RowClass = ($t % 2) ? " class='AltRow'" : "";
					if (!$TableRow[$t]["HideThisRow"] && $TableRow[$t]["RowData"]){
						if ($i_count == 0){$RowClass = " class=\"AltRow\"";}else{$RowClass="";}
						echo "<tr".$RowClass.">\r\n";
						echo $TableRow[$t]["RowData"];
						echo "</tr>\r\n";
						if ($i_count == 1){$i_count =0;}else{$i_count++;}
					}
					$t++;
				}
if (!$NoEventsFound){
			?>
</tbody>
		</table>
<?php }?>