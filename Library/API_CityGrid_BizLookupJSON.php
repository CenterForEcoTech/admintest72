<?php
/*
 * DEAD -- replaced with /repository/CityGrid_API_v2.php
 */
trigger_error("Call to dead file API_CityGrid_BizLookupJSON.php", E_USER_ERROR);
function getTime() 
    { 
    $a = explode (' ',microtime()); 
    return(double) $a[0] + $a[1]; 
    } 
$Start = getTime(); 

//only use these variable if not already set from ALLAPI
if (!$ALLAPI){
	$return_arr = array();
	$BizName = str_replace(" ","+",$_GET['term']);
	$BizLocation = str_replace(" ","+",$_GET['bizlocation']);
}
//take 10digits and turn into phone display
function format_tel_num($number){
    return preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($number)), 2);
} 

$PublisherID =10000001282;

$xmlsrc = "http://api.citygridmedia.com/content/places/v2/search/where?where=".$BizLocation."&what=".$BizName."&sort=alpha&format=json&publisher=".$PublisherID;
//echo $xmlsrc;

// Send CityGrid API Call

$ch = curl_init($xmlsrc);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, 0);
$data = curl_exec($ch); // Yelp response
curl_close($ch);


// Handle CityGrid response data
$Locations = json_decode($data);
//echo $signed_url;

//print_r($Locations);
//	echo "<hr>";
if ($Locations->results->locations){
	foreach($Locations->results->locations as $p)
	{
        // TODO: refactor the citygridID versus citygridProfileID because they are totally confusing. One is numeric and the other is not.
        //      either call it the same name as what citygrid calls it, or be explicit about the type.
		$CityGridID = $p->public_id;
        $row_array['CityGridPublicID'] = $p->public_id; // duplicating CityGridID to be absolutely sure which it is
		$row_array['CityGridID'] = trim(" ".$CityGridID);
		$row_array['BizName'] = trim(" ".$p->name);
		$row_array['value'] = trim(" ".$p->name);
		$row_array['Address'] = trim(" ".$p->address->street);
		$row_array['CityGridProfileID'] = trim(" ".$p->id);
        // add some data so we don't have to do the profile call
        $row_array['City'] = trim(" ".$p->address->city);
        $row_array['State'] = trim(" ".$p->address->state);
        $row_array['Zip'] = trim(" ".$p->address->postal_code);
        $row_array['Neighborhood'] = trim(" ".$p->neighborhood);
        $row_array['Lat'] = trim(" ".$p->latitude);
        $row_array['Long'] = trim(" ".$p->longitude);
        $row_array['Phone'] = trim(" ".$p->phone_number);
        $row_array['Website'] = trim(" ".$p->website);
		//echo $Locations[$L]->name."<br>";
		if (!in_array($row_array, $return_arr)){
			array_push($return_arr,$row_array);
		}
	}
}else{
	$return_array['Error'] = $data;
	//print_r($return_array);
}
//only use these variable if not already set from ALLAPI
if (!$ALLAPI){
	echo json_encode($return_arr);
}

$End = getTime(); 
//echo "Time taken = ".number_format(($End - $Start),2)." secs"; 
?>