<div class="ProgressBarTable">
		<div class="ProgressHeader">
		<?php
			$ProgressHeaderItem = explode(",",$ProgressHeaderItems);
			$pi = 0;
			while ($pi < count($ProgressHeaderItem)){
				$Width = 100/count($ProgressHeaderItem);
				echo "\t<div class=\"ProgressHeaderItem\" style=\"width:".$Width."%;\">\r\n";
				echo "\t\t".$ProgressHeaderItem[$pi]."\r\n";
				echo "\t</div>\r\n";
				$pi++;
			}
		?>
		</div>
		<div class="ProgressRow">
			<div class="ProgressRowItem ProgressCompleted" style="width:<?php echo $PercentComplete;?>%;">&nbsp;</div>
		</div>
	</div>
