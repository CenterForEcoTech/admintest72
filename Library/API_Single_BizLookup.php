<?php
include_once("../assert_is_ajax.php");
include_once("../functions.php");
//Following code allows is a workaround for local server issues that use IP6protocal
if ($_SERVER['REMOTE_ADDR'] == '::1'){
    $_SERVER['REMOTE_ADDR'] = gethostbyname('cetonline.org');
}
if (!$CityGridProfileID){
    $CityGridProfileID = $_GET['CityGridProfileID'];
}

if ($CityGridProfileID){
    include_once("../repository/CityGrid_API_v2.php");
    $provider = new CityGrid_API_v2_Detail("10000001282");
    $provider->listing_id = $CityGridProfileID;
    $provider->client_ip = $_SERVER['REMOTE_ADDR'];
    $data = $provider->execute();
    output_json($data);
}
?>