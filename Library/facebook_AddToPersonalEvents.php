<?php
trigger_error("facebook_AddToPersonalEvents.php template called. NOTICE: this template uses outdated logic that should be reevaluated, especially regarding the event description.", E_USER_NOTICE);
if (!isset($Config_DisplayDebugMessages)){
    $Config_DisplayDebugMessages = false;
}
//TODO, this page should really be an RSVP page, but during alpha we don't give access to CauseTown facebook page so users have to create their own Event to their profiles

/**
 * How this page appears to work:
 * =============================
 *
 * Restrictions: calls to facebook only happen if the IsDevMode_Facebook is false.
 *
 * 1) User clicks the "Add this to Facebook" image/link
 * 		a) If the event is a merchant-created customer choice event, that merchant's location is added to the form post
 * 		b) If the event is a featured cause event that may have one or more locations, the user is presented with a drop down to choose the location
 * 			- once chosen, that location is added to the form post
 * 2) Once the location data has been added to the form post, the browser is redirected (HTTPGET) to this page again with
 * 		a) CheckPermission = true (meaning, please go authenticate with facebook)
 * 		b) VenueID = merchantInfo_ID (identifying the location)
 * 			-- THEREFORE: technically, prior to this step, it is unnecessary to load the form fields
 * 3) When the page is reloaded (HTTPGET),
 * 		a) The oauth call is made to facebook and passed the url to return to this same page again
 * 			-- this call is made using an HTTPGET to the facebook oauth, with the url to this page in order to return the user here again
 * 			-- this call includes the "atoken" parameter which is necessary when the facebook call sends the user back
 * 		b) If the facebook auth is successful, it sends the user back to this page using the same URL, except with the atoken parameter
 * 			-- if the facebook auth is unsuccessful, the user is left hanging high and dry on a facebook error page (booo)
 * 4) After facebook authentication, we then retrieve the access token from facebook in the second pass which is required before
 * 		doing anything else.
 * 5) At this point, it is still an HTTPGET call, and we have the facebook access token. Our purpose is to create an event in Facebook
 * 		a) We load the form with the hidden fields, use the VenueID that has now been passed to and from Facebook twice to identify
 * 		the specific location the user selected.
 * 		b) On Page load, javascript gets the data for the desired location and loads the hidden form fields, then submits the form
 * 			-- this form submit actualy sends the user back to this template AGAIN, this time as an HTTPPOST that includes the event data
 * 6) Now, we have an HTTPPOST call to this page that includes the get parameters as well as the form post parameters, which we
 * 		send off to facebook.
 * 		-- For this page to not go into an infinite loop, the "msg" variable must be set because the absence of a value here is what
 * 		causes the form post to happen.
 *
 * TODO: simplify this. The final form post is unnecessary as we have all the data we require without having to force javacript to do anything
 * 		In fact, we don't even need the form. All we need is the VenueID.
 * 		If possible, handle the entire procedure through a proxy that won't leave the user hanging high and dry in case of a failure to authenticate.
 *
 * TODO: fix the refresh-page-reposts-form problem
 */
$CheckPermission = $_GET['CheckPermission'];
if (!$VenueID){$VenueID = $_GET["VenueID"];} //used to update form with selected Venue for Event
$fbPageID = $_POST["fbPageID"]; //used if user has manage_page permissions to indicate which page to post event to 
if (!$VenueID){$VenueID = 0;}
$VenueCounterID = $EventVenueCounter[$VenueID];
//echo "VenueID=".$VenueID."<Br>";
//echo "EventVenueCounter[".$VenueID."]=".$EventVenueCounter[$VenueID]."<br>";
//echo "VenueCounterID=".$VenueCounterID."<br>";
$my_url = $FaceBookEventRedirectURL."&VenueID=".$VenueID."&CheckPermission=".$CheckPermission; // mainly this should be the same URL to THIS page
$code = $_REQUEST["code"];
$app_id = $Config_FaceBook_AppID;
$app_secret = $Config_FaceBook_AppSecret;
$atoken = $_GET["atoken"];
if(!$isDevMode_Facebook){
	//
	// First pass call to Facebook, which expects to call back to this page. At this point, atoken is not passed in
	// but notice that the redirect_uri passed to facebook contains atoken, so when Facebook sends the user back, we don't
	// execute this conditional code.
	//
	// This code exits this script immediately, so no other code below will execute on this pass.
	//
	// We expect Facebook's callback to include "code" in the request.
	//
	//First Pass
	if(empty($code) && $CheckPermission && !$atoken) {
		$auth_url = "http://www.facebook.com/dialog/oauth?client_id=".$app_id."&atoken=true&scope=manage_pages,create_event&redirect_uri=".urlencode($my_url);
		echo("<script>top.location.href='" . $auth_url . "'</script>");
		//echo("<script>alert('" . $auth_url . "');</script>");
	}
	//
	// Second pass call to facebook actually retrieves the access token.
	//
	if ($CheckPermission){
		//echo("<script>alert('CheckPermission=" . $CheckPermission . "');</script>");
		$token_url = "https://graph.facebook.com/oauth/access_token?client_id=".$app_id."&client_secret=".$app_secret."&code=".$code."&VenueID=".$VenueID."&redirect_uri=".urlencode($my_url);
		//echo("<script>alert('tokenurl=" . $token_url . "');</script>");
		$access_token = file_get_contents($token_url);
		//echo("<script>setMerchant(".$VenueID.");</script>");
		//echo("<script>alert('" . $access_token . "');</script>");
	}
}

//Third Pass
if($access_token){
	//let's check to see if they have any pages they manage first
	$url = "https://graph.facebook.com/me/accounts?".$access_token;
	// Start the Graph API call
	$PageAccessArray = json_decode(file_get_contents($url), true);
	//print_r($PageAccessArray);
	
	$ManagePageCount = 0;
	foreach ($PageAccessArray as $PageAccessResultsData){
		foreach ($PageAccessResultsData as $PageAccessResultsArray){
			//print_r($PageAccessResultsArray);
			if ($PageAccessResultsArray["category"] !='Application' && $PageAccessResultsArray["name"] !='h'){
				if ($fbPageID){
					if ($PageAccessResultsArray["id"] == $fbPageID){
						$PublishEventUrl = "https://graph.facebook.com/".$fbPageID."/events?access_token=" . $PageAccessResultsArray["access_token"];
						$FBPage = strtolower(urlencode($PageAccessResultsArray["name"]))."/"; //so link will go to the account
						$FBPage = "pages/".$FBPage.$fbPageID."?sk=";
					}
				}else{
					$ManagePageCount++;
					//echo "Name=".$PageAccessResultsArray["name"]."<br>";
					//echo "AccessToken=".$PageAccessResultsArray["access_token"]."<br>";
					//echo "PageID=".$PageAccessResultsArray["id"]."<br>";
					$ManagePageChoices .= "<input type=\"button\" value=\"".$PageAccessResultsArray["name"]."\" onClick=\"FBPageID(".$PageAccessResultsArray["id"].");\" style=\"cursor:pointer;\"><br>";
				}
			}
		}
	}
	if ($ManagePageCount){
		if ($ManagePageCount > 1){$ManagePageMessage = " a few Facebook Pages";}else{$ManagePageMessage = " a Facebook Page";}
		$msg = "You can add this event to your personal Facebook page or any Facebook Page that you manage. Where should we add this event?<br>";
		$msg .= "<input type=\"button\" value=\"Your Personal Profile\" onClick=\"FBPageID(123);\" style=\"cursor:pointer;\"><br>".$ManagePageChoices."<br>";
	}
}//end if right time to check


//
// Third/Forth pass, process the form post.
//
if( !empty($_POST) && (empty($_POST['name']) || empty($_POST['start_time']) || empty($_POST['end_time'])) ) {
	$msg = "Please check your inputs!";
} elseif(!empty($_POST)) {
	if (!$ManagePageCount){
	
		$msg = "Attepting to add to Your Facebook";
		if (!$PublishEventUrl){
			$FBPage = "";
			$PublishEventUrl = "https://graph.facebook.com/me/events?" . $access_token;
		}
		$params = array();
		// Prepare Event fields
		foreach($_POST as $key=>$value){
			if(strlen($value)){
				$params[$key] = $value;
			}
		}
        if ($Config_DisplayDebugMessages){
		    print_r($params);
        }

		if(!$isDevMode_Facebook){
			// Start the Graph API call
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$PublishEventUrl);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			$result = curl_exec($ch);
            if ($Config_DisplayDebugMessages){
			    echo "Result=".$result;
            }
			$decoded = json_decode($result, true);
			curl_close($ch);
            if ($Config_DisplayDebugMessages){
                print_r($decoded);
            }
			
			if(is_array($decoded) && isset($decoded['id'])) {
				// Event created successfully, now we can
				// a) save event id to DB AND/OR
				// b) show success message AND/OR
				// c) optionally, delete image from our server (if any)
				$msg = "<a href=\"http://www.facebook.com/".$FBPage."events\" target=\"Facebook\">Click here to view the event and invite others!</a>";

				//Add Posting to database
				//if user is not logged in then capture their ip address
				if (!getMemberId()){
					if ( isset($_SERVER["REMOTE_ADDR"]) ){
						$MemberID=$_SERVER["REMOTE_ADDR"];
					} elseif (isset($_SERVER["REMOTE_ADDR"])){
						$MemberID=$_SERVER["SERVER_ADDR"];
					} else {
						$MemberID="Not Available";
					}
				} else {
					$MemberID = getMemberId();
				}

				$SQL_EventPost = "INSERT INTO event_posting (EventPosting_EventID,EventPosting_MemberID,EventPosting_Location,EventPosting_LocationID) VALUES (".$EventID.",'".$MemberID."','Facebook','".$decoded['id']."')";
				$SQLEventPost = mysqli_query($SQLEventPost, $SQL_EventPost);
				//echo $SQL_EventPost;
			} else {
				$msg = "We were unable to automatically add it to your facebook events";
			}

		} else {
			$msg = "If we weren't in dev mode, we'd have tried to post to facebook!";
		}// end if not dev mode
	}// end if managedcount
}//end if data posted

// if the start date is at least one hour in the future, then allow user to create event in facebook
// otherwise, facebook is very unhappy

if (!isset($locationProvider)){
    include_once($repositoryAPIFolder."DataContracts.php");
    include_once($dbProviderFolder."GeoLookupProvider.php");
    include_once($siteRoot."_setupGeolookupConnection.php");
    $locationProvider = new GeoLookupProvider($geoConn);
}
if ($_POST['zip']){$TimeZoneVenueZip = $_POST['zip'];}else{$TimeZoneVenueZip = $EventVenue[$VenueCounterID]["zip"];}
$TimeZoneVenue = new Venue();
$TimeZoneVenue->postalCode = $TimeZoneVenueZip;
//Adjustment necessary because FB interprest all times with timezones (which they require as of 10/2012) as PST
$eventTimezone = $locationProvider->getTimeZone($TimeZoneVenue);
try {
    $startDateWithTz = new DateTime(date("Y-m-d H:i:s", $eventRecord->startDate), $eventTimezone);
} catch (Exception $e) {
}
try {
    $endDateWithTz = new DateTime(date("Y-m-d H:i:s", $eventRecord->endDate), $eventTimezone);
} catch (Exception $e) {
}
try {
    $currentTimeWithTz = new DateTime(date("Y-m-d H:i:s"), $eventTimezone);
} catch (Exception $e) {
}
if (!$ConfigIgnoreFaceBookTimeZone){
	$startDateFormated = date_format($startDateWithTz, 'c');
	$endDateFormated = date_format($endDateWithTz, 'c');
} else {
	$startDateFormated = date("Y-m-d H:i:s", $eventRecord->startDate);
	$endDateFormated = date("Y-m-d H:i:s", $eventRecord->endDate);		
}

$oneHourInFuture = $currentTimeWithTz->add(new DateInterval("PT1H"));
if ($oneHourInFuture < $startDateWithTz){
?>
<form action="" method="post" name="FaceBookEventForm" id="FaceBookEventForm">
	<div style="display:<?php if($Config_DisplayDebugMessages){echo "block";}else{echo "none";}?>;">
	<input type="text" id="event-name" name="name" value="<?php echo ($_POST['name'] ? $_POST['name'] : $EventTitle);?>">
	<input type="text" id="event-desc" name="EventDescriptionOriginal" value="<?php echo ($_POST['EventDescriptionOriginal'] ? $_POST['EventDescriptionOriginal'] : $EventDescription);?>">
	<input type="text" id="event-start" name="start_time" value="<?php echo ($_POST['start_time'] ? $_POST['start_time'] : $startDateFormated);?>">
	<input type="text" id="event-end" name="end_time" value="<?php echo ($_POST['end_time'] ? $_POST['end_time'] : $endDateFormated);?>">
	<input type="text" id="venue-name" name="location" value="<?php echo ($_POST['location'] ? $_POST['location'] : $EventLocation[$VenueCounterID]["Location"]);?>">
	<input type="text" id="venue-addr" name="street" value="<?php echo ($_POST['street'] ? $_POST['street'] : $EventVenue[$VenueCounterID]["street"]);?>">
	<input type="text" id="venue-city" name="city" value="<?php echo ($_POST['city'] ? $_POST['city'] : $EventVenue[$VenueCounterID]["city"]);?>">
	<input type="text" id="venue-state" name="state" value="<?php echo ($_POST['state'] ? $_POST['state'] : $EventVenue[$VenueCounterID]["state"]);?>">
	<input type="text" id="venue-zip" name="zip" value="<?php echo ($_POST['zip'] ? $_POST['zip'] : $EventVenue[$VenueCounterID]["zip"]);?>">
	<input type="text" id="venue-country" name="country" value="<?php echo ($_POST['country'] ? $_POST['country'] : ($EventVenue[$VenueCounterID]["country"] ? $EventVenue[0]["country"] : "United States"));?>">
	<input type="text" id="facebook-desc" name="description" value="<?php echo ($_POST['description'] ? $_POST['description'] : $EventPosting[$VenueCounterID]["FacebookDesc"]);?>">
	<input type="text" name="privacy_type" value="<?php echo $Config_FaceBook_EventPrivacyType;?>">
	<input type="text" id="fbPageID" name="fbPageID" value="<?php echo $_POST['fbPageID'];?>">
	</div>
	<?php
	if (empty($code) && !$CheckPermission){?>
		<a href="#" id="add-to-facebook"><img src="<?php echo $CurrentServer."images/Facebook_AddEvent.png"?>"></a>
		<?php
		if (count($merchants)>1){?>
			<select id="select-event-for-facebook" class="event-select-element" style="display:none;">
				<option>Choose Location</option>
				<?php foreach ($merchants as $merchant){?>
					<option value="<?php echo $merchant->id?>"><?php echo $merchant->name." at ".$merchant->getSingleLineAddress();?></option>
				<?php }?>
			</select>
		<?php
		}
	}
	?>
</form>
	<script type="text/javascript">
		$("#msg").html('<?php echo $msg;?>');
		function FBPageID(fbPageID){
			var fbPageID = fbPageID;
			$("#fbPageID").val(fbPageID);
			$("#FaceBookEventForm").attr("action",window.location);
			$("#FaceBookEventForm").submit();
		}
		
		$(function(){
			var selectElement = $("#select-event-for-facebook"),
				eventInfo = <?php echo json_encode($eventRecord);?>,
				eventStartDate = "<?php echo $startDateFormated;?>",
				eventEndDate = "<?php echo $endDateFormated;?>",
				eventUrl = "<?php echo $FaceBookEventRedirectURL;?>",
				merchantList = <?php echo json_encode($merchants);?>,
				postbackVenueId = <?php echo $VenueID;?>,
				getMerchant = function(merchantId){
					var merchant = eventInfo.venue,
						possibleMerchants = (merchantList != null) ? $.map(merchantList, function(merchant, index){
							return (merchant.id == merchantId) ? merchant : null;
						}) : null;
					if (possibleMerchants.length){
						return possibleMerchants[0];
					}
					return merchant;
				},
				setData = function(merchant){
					var country = merchant.country,
						facebookDesc = merchant.name,
						donationText = "",
						donateTextTemplate = " will donate {0}% of your purchase to {1}",
						customerChoiceBeneficiary = "your favorite cause";
					if (country === '' || country === 'US'){
						country = "United States";
					}
					if (merchant.url){
						facebookDesc = "<a href='"+ merchant.url +"'>"+ merchant.name +"</a>";
					}
					if (merchant.featuredCausePercentage){
						donationText = donateTextTemplate.replace("{0}", merchant.featuredCausePercentage).replace("{1}", eventInfo.cause.name);
					}
					if (merchant.customerChoicePercentage){
						if (donationText){
							donationText = donationText + " or ";
						}
						donationText = donationText + donateTextTemplate.replace("{0}", merchant.customerChoicePercentage).replace("{1}", customerChoiceBeneficiary);
					}
					if (donationText){
						facebookDesc = facebookDesc + donationText + "! "+eventInfo.description;
					}
					facebookDesc = facebookDesc + merchant.publicProfileLink;
					//facebookDesc = facebookDesc + " More Info at " + eventUrl;
					$("#event-name").val(eventInfo.title);
					$("#event-desc").val(eventInfo.description);
					$("#event-start").val(eventStartDate);
					$("#event-end").val(eventEndDate);
					$("#venue-name").val(merchant.name);
					$("#venue-addr").val(merchant.streetAddr);
					$("#venue-city").val(merchant.city);
					$("#venue-state").val(merchant.region);
					$("#venue-country").val(country);
					$("#facebook-desc").val(facebookDesc);
				},
				setMerchant = function(venueId){
					var merchant = getMerchant(venueId);
					setData(merchant);
				};

			$("#add-to-facebook").click(function(e){
				var defaultMerchant, venueId;
				e.preventDefault();
				if (selectElement.length){
					selectElement.show();
				} else {
					//defaultMerchant = getMerchant();
					//alert('hi');
					//venueId = defaultMerchant.id;
					//setData(defaultMerchant);
					<?php if ($VenueID){$ThisVenueID = $VenueID;}else{$ThisVenueID = $EventVenue[$VenueCounterID]["VenueID"];}?>
					$("#FaceBookEventForm").attr("action","<?php echo $FaceBookEventRedirectURL."&CheckPermission=true&VenueID=".$ThisVenueID;?>");
					$("#FaceBookEventForm").submit();
					//FBTEST top.location.href = "<?php echo $FaceBookEventRedirectURL."?CheckPermission=true&VenueID="?>" + venueId;
				}
			});

			if (selectElement.length){
				selectElement.change(function(){
					var venueId = $(this).val(),
						merchant = getMerchant(venueId);

					if (merchant){
						setData(merchant);
						//alert("<?php echo $FaceBookEventRedirectURL."?CheckPermission=true&VenueID="?>" + venueId);
						$("#FaceBookEventForm").attr("action","<?php echo $FaceBookEventRedirectURL."?Which=Second&CheckPermission=true&VenueID="?>" + venueId);
						$("#FaceBookEventForm").submit();
						//FBTEST top.location.href =  "<?php echo $FaceBookEventRedirectURL."?CheckPermission=true&VenueID="?>" + venueId;
					}
				});
			}
<?php if ($CheckPermission && !$msg){ ?>
			if (postbackVenueId){
				$("#msg").html('Communicating with Facebook...');
				<?php //if (count($merchants)>1){echo "setMerchant(postbackVenueId);";}?>
				$("#FaceBookEventForm").submit();
			}
<?php }?>
		});
	</script>
<?php
} // end showing for events in future
?>