<?php

$merchantsWithFbId = array();
foreach ($merchants as $merchant){
    if ($merchant->facebookEventID){
        $merchantsWithFbId[] = $merchant;
    }
}

if ($eventRecord->venue->facebookEventID || count($merchantsWithFbId) == 1){
    // there is only one
    $SingleFacebookEventID = $eventRecord->venue->facebookEventID;
    if (count($merchantsWithFbId) == 1){
        $SingleFacebookEventID = $merchantsWithFbId[0]->facebookEventID;
    }
    ?>
<a href="http://www.facebook.com/events/<?php echo $SingleFacebookEventID;?>" target="_blank"><img src="<?php echo $CurrentServer."images/Facebook_RSVPEvent.png"?>"></a>
<script type="text/javascript">
    $("#ShowRSVP").removeClass("hidden");
</script>
<?php
} else if (count($merchantsWithFbId) > 1){
    // there is more than one
    ?>
<a href="#" id="add-to-facebook"><img src="<?php echo $CurrentServer."images/Facebook_RSVPEvent.png"?>"></a>
<select id="select-event-for-facebook" class="event-select-element hidden">
    <option>Choose Location</option>
    <?php foreach ($merchantsWithFbId as $merchant){
        /* @var $merchant Venue */ ?>
    <option value="<?php echo $merchant->facebookEventID?>"><?php echo $merchant->name." at ".$merchant->getSingleLineAddress();?></option>
<?php } ?>
</select>
<script type="text/javascript">
    $(function(){
        var selectElement = $("#select-event-for-facebook"),
            addtofacebook = $("#add-to-facebook");

        addtofacebook.click(function(e){
            e.preventDefault();
            selectElement.show();
        });
        selectElement.change(function(){
            var venueFacebookId = $(this).val();

            if (venueFacebookId){
                //alert('http://www.facebook.com/events/'+venueFacebookId+'/');
                window.open('http://www.facebook.com/events/'+venueFacebookId+'/');
            }
        });
    });
    $("#ShowRSVP").removeClass("hidden");
</script>
<?php
} // end if there's a facebook event to link to
?>