<?php
// expected to be called by an async process
ignore_user_abort(true);
set_time_limit(0);
// TODO: put in some auth so that not anyone can send email
include_once("../Global_Variables.php");

//error_reporting(1);
//ini_set("display_errors", "ON");
include_once($siteRoot."functions.php");
$currentRequest = new Request();
$results = new stdClass();
switch(strtoupper($currentRequest->verb)){
    case "DELETE":
        header("HTTP/1.0 501 Not Implemented");
        break;
    case "GET":
        header("HTTP/1.0 501 Not Implemented");
        break;
    case "POST":
        header("HTTP/1.0 501 Not Implemented");
        break;
    case "PUT":
        $deserializedObject = json_decode($currentRequest->rawData); // serialized EmailMessage object

        include_once($dbProviderFolder."MySqliEmailLogger.php");
        include_once($repositoryApiFolder."EmailHelper.php");
        $emailMessage = EmailMessage::convert($deserializedObject);
        //error_log(json_encode($emailMessage));
        $throwErrors = true;
        $emailLogger = new MySqliEmailLogger($mysqli);
        $emailProvider = new EmailProvider($emailLogger, $SMTPEmailhost, $SMTPEmailport, $SMTPEmailusername, $SMTPEmailpassword, $throwErrors);
        $emailResponse = $emailProvider->smtpSend($emailMessage, $isDevMode_Email);

        if ($emailResponse->emailSent){
            $results = "Email Sent";
            header("HTTP/1.0 200 OK");
        } else {
            if ($emailResponse->isServerError()){
                $results = "Internal Server Error";
                header("HTTP/1.0 500 Internal Server Error");
            } else {
                $results = $emailResponse->message;
                header("HTTP/1.0 400 Bad Request");
            }
        }
        break;
}
//error_log(json_encode($results));
output_json($results);
die();
?>