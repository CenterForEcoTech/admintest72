<?php
include_once("../assert_is_ajax.php");
include_once("../functions.php");

$BizName = str_replace(" ","+",$_GET['term']);
$BizLocation = str_replace(" ","+",$_GET['bizlocation']);

//exceptions rule for $BizLocation
if ($BizLocation == "Washington,Washington,+D.C."){
	$BizLocation = "Washington,+DC";
}

include_once("../repository/CityGrid_API_v2.php");
$provider = new CityGrid_API_v2_SearchWhere("10000001282");
$provider->what = $BizName;
$provider->where = $BizLocation;
//$provider->sort = CityGrid_API_v2::ALPHA_SORT;
$data = $provider->execute();
// TODO: modify the front end to handle pagination, and possibly get rid of the legacy record at the same time!
output_json($data->collection);

?>