<?php
$merchantsWithEbId = array();
foreach ($merchants as $merchant){
    if ($merchant->eventBriteEventID){
        $merchantsWithEbId[] = $merchant;
    }
}
if (count($merchantsWithEbId) == 1){
    // exactly one
    $EventBriteEventID = $merchantsWithEbId[0]->eventBriteEventID;
}
if ($eventRecord->customAttendLink){ ?>

    <a href="<?php echo $eventRecord->customAttendLink;?>" target="_blank"><img src="<?php echo $CurrentServer."images/EventBriteRSVP.png"?>"></a>
    <script type="text/javascript">
        $("#ShowRSVP").removeClass("hidden");
    </script>
<?php } else if ($eventRecord->venue->eventBriteEventID || count($merchantsWithEbId) > 0){

    if ($eventRecord->venue->eventBriteEventID){?>
        <a href="https://www.eventbrite.com/event/<?php echo $eventRecord->venue->eventBriteEventID;?>" target="EB<?php echo $eventRecord->venue->eventBriteEventID;?>"><img src="<?php echo $CurrentServer."images/EventBriteRSVP.png"?>"></a>
        <script type="text/javascript">
            $("#ShowRSVP").removeClass("hidden");
        </script>
    <?php } else { ?>
        <a href="#" id="rsvp-eventbrite"><img src="<?php echo $CurrentServer."images/EventBriteRSVP.png"?>"></a>
        <select id="select-event-for-eventbrite" class="event-select-element hidden">
            <option>Choose Location</option>
            <?php foreach ($merchantsWithEbId as $merchant){
            ?>
                <option value="https://www.eventbrite.com/event/<?php echo $merchant->eventBriteEventID;?>"><?php echo $merchant->name." at ".$merchant->getSingleLineAddress();?></option>
            <?php } ?>
        </select>
        <script type="text/javascript">
            $(function(){
                var selectElement = $("#select-event-for-eventbrite");

                $("#rsvp-eventbrite").click(function(e){
                    e.preventDefault();
                    selectElement.show();
                });
                selectElement.change(function(){
                    var selectedUrl = $(this).val();
                    if (selectedUrl.length){
                        window.open(selectedUrl);
                    } else {
                        alert("This event doesn't have an id in Eventbrite.");
                    }
                });
            });
            $("#ShowRSVP").removeClass("hidden");
        </script>
    <?php }
}//end if $EventBriteEventID 
?>