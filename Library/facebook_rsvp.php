<?php
include_once('../Global_Variables.php'); 
$app_id = $Config_FaceBook_AppID;
$app_secret = $Config_FaceBook_AppSecret;

$my_url = "http".$secureServer."://CauseTown.org/dev/Library/facebook_rsvp.php";// mainly this should be the same URL to THIS page
$event_id = 174633572656557;
$rsvp_status = "";
 

$code = $_REQUEST["code"];
 
?>
<!doctype html>
<html>
<head>
<title>Create An Event</title>
<style>
label {float: left; width: 100px;}
input[type=text],textarea {width: 210px;}
#msg {border: 1px solid #000; padding: 5px; color: red;}
</style>
</head>
<body>
<?php if( isset($msg) ) { ?>
<p id="msg"><?php echo $msg; ?></p>
<?php } ?>
<form action="" method="post">
    <p>
        <label for="privacy_type">RSVP:</label>
        <input type="radio" name="rsvp" value="attending" <?php if($rsvp_status==="attending") echo "checked='checked'"; ?>/>Attending&nbsp;&nbsp;&nbsp;
        <input type="radio" name="rsvp" value="maybe" <?php if($rsvp_status==="maybe" || $rsvp_status==="unsure") echo "checked='checked'"; ?>/>Maybe&nbsp;&nbsp;&nbsp;
        <input type="radio" name="rsvp" value="declined" <?php if($rsvp_status==="declined") echo "checked='checked'"; ?>/>Not Attending&nbsp;&nbsp;&nbsp;
    </p>
    <p><input type="submit" value="RSVP to this event" /></p>
</form>
</body>
</html>