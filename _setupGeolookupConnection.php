<?php
if (!isset($dbConfigGeolookup)){
    trigger_error("_setupGeolookupConnection.php This script requires the database configuration for the geolookup database in siteconf.", E_USER_ERROR);
    die(); // no db config for the target db.
}

$geoConn = new mysqli($dbConfigGeolookup->serv,$dbConfigGeolookup->user,$dbConfigGeolookup->pass,$dbConfigGeolookup->inst);
if ($geoConn->connect_error) {
    trigger_error("_setupGeolookupConnection.php: Failed to connect to MySQL: ".$geoConn->connect_error, E_USER_ERROR);
}
